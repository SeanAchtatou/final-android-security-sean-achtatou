package com.km.tool.mms.pdu;

import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;

public class PduParser {
    static final /* synthetic */ boolean $assertionsDisabled = (!PduParser.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    private static final int END_STRING_FLAG = 0;
    private static final int LENGTH_QUOTE = 31;
    private static final String LOG_TAG = "PduParser";
    private static final int LONG_INTEGER_LENGTH_MAX = 8;
    private static final int QUOTE = 127;
    private static final int QUOTED_STRING_FLAG = 34;
    private static final int SHORT_INTEGER_MAX = 127;
    private static final int SHORT_LENGTH_MAX = 30;
    private static final int TEXT_MAX = 127;
    private static final int TEXT_MIN = 32;
    private static final int TYPE_QUOTED_STRING = 1;
    private static final int TYPE_TEXT_STRING = 0;
    private static final int TYPE_TOKEN_STRING = 2;

    public PduHeaders parseHeaders(byte[] pduData) {
        PduHeaders headers = parseHeaders(new ByteArrayInputStream(pduData));
        if (headers == null) {
            return null;
        }
        if (checkMandatoryHeader(headers)) {
            return headers;
        }
        log("check mandatory headers failed!");
        return null;
    }

    /* JADX INFO: Multiple debug info for r24v1 com.km.tool.mms.pdu.EncodedStringValue: [D('value' com.km.tool.mms.pdu.EncodedStringValue), D('value' byte[])] */
    /* JADX INFO: Multiple debug info for r24v4 long: [D('value' long), D('value' int)] */
    /* access modifiers changed from: protected */
    public PduHeaders parseHeaders(ByteArrayInputStream pduDataStream) {
        EncodedStringValue from;
        byte[] address;
        if (pduDataStream == null) {
            return null;
        }
        boolean keepParsing = true;
        PduHeaders headers = new PduHeaders();
        while (keepParsing && pduDataStream.available() > 0) {
            int headerField = extractByteValue(pduDataStream);
            switch (headerField) {
                case 129:
                case 130:
                case 151:
                    EncodedStringValue value = parseEncodedStringValue(pduDataStream);
                    if (value == null) {
                        break;
                    } else {
                        byte[] address2 = value.getTextString();
                        if (address2 != null) {
                            String str = new String(address2);
                            int endIndex = str.indexOf("/");
                            if (endIndex > 0) {
                                str = str.substring(0, endIndex);
                            }
                            try {
                                value.setTextString(str.getBytes());
                            } catch (NullPointerException e) {
                                log("null pointer error!");
                                return null;
                            }
                        }
                        try {
                            headers.appendEncodedStringValue(value, headerField);
                            break;
                        } catch (NullPointerException e2) {
                            log("null pointer error!");
                            break;
                        } catch (RuntimeException e3) {
                            log(String.valueOf(headerField) + "is not Encoded-String-Value header field!");
                            return null;
                        }
                    }
                case 131:
                case 139:
                case 152:
                case PduHeaders.REPLY_CHARGING_ID:
                case PduHeaders.APPLIC_ID:
                case PduHeaders.REPLY_APPLIC_ID:
                case PduHeaders.AUX_APPLIC_ID:
                case PduHeaders.REPLACE_ID:
                case PduHeaders.CANCEL_ID:
                    byte[] value2 = parseWapString(pduDataStream, 0);
                    if (value2 == null) {
                        break;
                    } else {
                        try {
                            headers.setTextString(value2, headerField);
                            break;
                        } catch (NullPointerException e4) {
                            log("null pointer error!");
                            break;
                        } catch (RuntimeException e5) {
                            log(String.valueOf(headerField) + "is not Text-String header field!");
                            return null;
                        }
                    }
                case 132:
                    byte[] contentType = parseContentType(pduDataStream, new HashMap<>());
                    if (contentType != null) {
                        try {
                            headers.setTextString(contentType, 132);
                        } catch (NullPointerException e6) {
                            log("null pointer error!");
                        } catch (RuntimeException e7) {
                            log(String.valueOf(headerField) + "is not Text-String header field!");
                            return null;
                        }
                    }
                    keepParsing = $assertionsDisabled;
                    break;
                case 133:
                case 142:
                case PduHeaders.REPLY_CHARGING_SIZE:
                    try {
                        headers.setLongInteger(parseLongInteger(pduDataStream), headerField);
                        break;
                    } catch (RuntimeException e8) {
                        log(String.valueOf(headerField) + "is not Long-Integer header field!");
                        return null;
                    }
                case 134:
                case 143:
                case 144:
                case 145:
                case 146:
                case 148:
                case 149:
                case 153:
                case 155:
                case 156:
                case PduHeaders.STORE:
                case PduHeaders.MM_STATE:
                case PduHeaders.STORE_STATUS:
                case PduHeaders.STORED:
                case PduHeaders.TOTALS:
                case PduHeaders.QUOTAS:
                case PduHeaders.DISTRIBUTION_INDICATOR:
                case PduHeaders.RECOMMENDED_RETRIEVAL_MODE:
                case PduHeaders.CONTENT_CLASS:
                case PduHeaders.DRM_CONTENT:
                case PduHeaders.ADAPTATION_ALLOWED:
                case PduHeaders.CANCEL_STATUS:
                    int value3 = extractByteValue(pduDataStream);
                    try {
                        headers.setOctet(value3, headerField);
                        break;
                    } catch (IllegalArgumentException e9) {
                        log("Set invalid Octet value: " + value3 + " into the header filed: " + headerField);
                        return null;
                    } catch (RuntimeException e10) {
                        log(String.valueOf(headerField) + "is not Octet header field!");
                        return null;
                    }
                case 135:
                case 136:
                case 157:
                    parseValueLength(pduDataStream);
                    int token = extractByteValue(pduDataStream);
                    try {
                        long timeValue = parseLongInteger(pduDataStream);
                        if (129 == token) {
                            timeValue += System.currentTimeMillis() / 1000;
                        }
                        try {
                            headers.setLongInteger(timeValue, headerField);
                            break;
                        } catch (RuntimeException e11) {
                            log(String.valueOf(headerField) + "is not Long-Integer header field!");
                            return null;
                        }
                    } catch (RuntimeException e12) {
                        log(String.valueOf(headerField) + "is not Long-Integer header field!");
                        return null;
                    }
                case 137:
                    parseValueLength(pduDataStream);
                    if (128 == extractByteValue(pduDataStream)) {
                        from = parseEncodedStringValue(pduDataStream);
                        if (!(from == null || (address = from.getTextString()) == null)) {
                            String str2 = new String(address);
                            int endIndex2 = str2.indexOf("/");
                            if (endIndex2 > 0) {
                                str2 = str2.substring(0, endIndex2);
                            }
                            try {
                                from.setTextString(str2.getBytes());
                            } catch (NullPointerException e13) {
                                log("null pointer error!");
                                return null;
                            }
                        }
                    } else {
                        try {
                            from = new EncodedStringValue(PduHeaders.FROM_INSERT_ADDRESS_TOKEN_STR.getBytes());
                        } catch (NullPointerException e14) {
                            log(String.valueOf(headerField) + "is not Encoded-String-Value header field!");
                            return null;
                        }
                    }
                    try {
                        headers.setEncodedStringValue(from, 137);
                        break;
                    } catch (NullPointerException e15) {
                        log("null pointer error!");
                        break;
                    } catch (RuntimeException e16) {
                        log(String.valueOf(headerField) + "is not Encoded-String-Value header field!");
                        return null;
                    }
                case 138:
                    pduDataStream.mark(1);
                    int messageClass = extractByteValue(pduDataStream);
                    if (messageClass >= 128) {
                        if (128 != messageClass) {
                            if (129 != messageClass) {
                                if (130 != messageClass) {
                                    if (131 != messageClass) {
                                        break;
                                    } else {
                                        headers.setTextString(PduHeaders.MESSAGE_CLASS_AUTO_STR.getBytes(), 138);
                                        break;
                                    }
                                } else {
                                    headers.setTextString(PduHeaders.MESSAGE_CLASS_INFORMATIONAL_STR.getBytes(), 138);
                                    break;
                                }
                            } else {
                                headers.setTextString(PduHeaders.MESSAGE_CLASS_ADVERTISEMENT_STR.getBytes(), 138);
                                break;
                            }
                        } else {
                            try {
                                headers.setTextString(PduHeaders.MESSAGE_CLASS_PERSONAL_STR.getBytes(), 138);
                                break;
                            } catch (NullPointerException e17) {
                                log("null pointer error!");
                                break;
                            } catch (RuntimeException e18) {
                                log(String.valueOf(headerField) + "is not Text-String header field!");
                                return null;
                            }
                        }
                    } else {
                        pduDataStream.reset();
                        byte[] messageClassString = parseWapString(pduDataStream, 0);
                        if (messageClassString == null) {
                            break;
                        } else {
                            try {
                                headers.setTextString(messageClassString, 138);
                                break;
                            } catch (NullPointerException e19) {
                                log("null pointer error!");
                                break;
                            } catch (RuntimeException e20) {
                                log(String.valueOf(headerField) + "is not Text-String header field!");
                                return null;
                            }
                        }
                    }
                case 140:
                    int messageType = extractByteValue(pduDataStream);
                    switch (messageType) {
                        case 137:
                        case 138:
                        case 139:
                        case 140:
                        case 141:
                        case 142:
                        case 143:
                        case 144:
                        case 145:
                        case 146:
                        case 147:
                        case 148:
                        case 149:
                        case 150:
                        case 151:
                            return null;
                        default:
                            try {
                                headers.setOctet(messageType, headerField);
                                continue;
                            } catch (IllegalArgumentException e21) {
                                log("Set invalid Octet value: " + messageType + " into the header filed: " + headerField);
                                return null;
                            } catch (RuntimeException e22) {
                                log(String.valueOf(headerField) + "is not Octet header field!");
                                return null;
                            }
                    }
                case 141:
                    int version = parseShortInteger(pduDataStream);
                    try {
                        headers.setOctet(version, 141);
                        break;
                    } catch (IllegalArgumentException e23) {
                        log("Set invalid Octet value: " + version + " into the header filed: " + headerField);
                        return null;
                    } catch (RuntimeException e24) {
                        log(String.valueOf(headerField) + "is not Octet header field!");
                        return null;
                    }
                case 147:
                case 150:
                case 154:
                case PduHeaders.STORE_STATUS_TEXT:
                case PduHeaders.RECOMMENDED_RETRIEVAL_MODE_TEXT:
                case PduHeaders.STATUS_TEXT:
                    EncodedStringValue value4 = parseEncodedStringValue(pduDataStream);
                    if (value4 == null) {
                        break;
                    } else {
                        try {
                            headers.setEncodedStringValue(value4, headerField);
                            break;
                        } catch (NullPointerException e25) {
                            log("null pointer error!");
                            break;
                        } catch (RuntimeException e26) {
                            log(String.valueOf(headerField) + "is not Encoded-String-Value header field!");
                            return null;
                        }
                    }
                case PduHeaders.PREVIOUSLY_SENT_BY:
                    parseValueLength(pduDataStream);
                    try {
                        parseIntegerValue(pduDataStream);
                        EncodedStringValue previouslySentBy = parseEncodedStringValue(pduDataStream);
                        if (previouslySentBy == null) {
                            break;
                        } else {
                            try {
                                headers.setEncodedStringValue(previouslySentBy, PduHeaders.PREVIOUSLY_SENT_BY);
                                break;
                            } catch (NullPointerException e27) {
                                log("null pointer error!");
                                break;
                            } catch (RuntimeException e28) {
                                log(String.valueOf(headerField) + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        }
                    } catch (RuntimeException e29) {
                        log(String.valueOf(headerField) + " is not Integer-Value");
                        return null;
                    }
                case PduHeaders.PREVIOUSLY_SENT_DATE:
                    parseValueLength(pduDataStream);
                    try {
                        parseIntegerValue(pduDataStream);
                        try {
                            headers.setLongInteger(parseLongInteger(pduDataStream), PduHeaders.PREVIOUSLY_SENT_DATE);
                            break;
                        } catch (RuntimeException e30) {
                            log(String.valueOf(headerField) + "is not Long-Integer header field!");
                            return null;
                        }
                    } catch (RuntimeException e31) {
                        log(String.valueOf(headerField) + " is not Integer-Value");
                        return null;
                    }
                case PduHeaders.MM_FLAGS:
                    parseValueLength(pduDataStream);
                    extractByteValue(pduDataStream);
                    parseEncodedStringValue(pduDataStream);
                    break;
                case PduHeaders.ATTRIBUTES:
                case 174:
                case PduHeaders.ADDITIONAL_HEADERS:
                default:
                    log("Unknown header");
                    break;
                case PduHeaders.MBOX_TOTALS:
                case PduHeaders.MBOX_QUOTAS:
                    parseValueLength(pduDataStream);
                    extractByteValue(pduDataStream);
                    try {
                        parseIntegerValue(pduDataStream);
                        break;
                    } catch (RuntimeException e32) {
                        log(String.valueOf(headerField) + " is not Integer-Value");
                        return null;
                    }
                case PduHeaders.MESSAGE_COUNT:
                case PduHeaders.START:
                case PduHeaders.LIMIT:
                    try {
                        headers.setLongInteger(parseIntegerValue(pduDataStream), headerField);
                        break;
                    } catch (RuntimeException e33) {
                        log(String.valueOf(headerField) + "is not Long-Integer header field!");
                        return null;
                    }
                case PduHeaders.ELEMENT_DESCRIPTOR:
                    parseContentType(pduDataStream, null);
                    break;
            }
        }
        return headers;
    }

    private static void log(String text) {
        Log.v(LOG_TAG, text);
    }

    protected static int parseUnsignedInt(ByteArrayInputStream pduDataStream) {
        if ($assertionsDisabled || pduDataStream != null) {
            int result = 0;
            int temp = pduDataStream.read();
            if (temp == -1) {
                return temp;
            }
            while ((temp & 128) != 0) {
                result = (result << 7) | (temp & 127);
                temp = pduDataStream.read();
                if (temp == -1) {
                    return temp;
                }
            }
            return (result << 7) | (temp & 127);
        }
        throw new AssertionError();
    }

    protected static int parseValueLength(ByteArrayInputStream pduDataStream) {
        if ($assertionsDisabled || pduDataStream != null) {
            int temp = pduDataStream.read();
            if ($assertionsDisabled || -1 != temp) {
                int first = temp & 255;
                if (first <= SHORT_LENGTH_MAX) {
                    return first;
                }
                if (first == LENGTH_QUOTE) {
                    return parseUnsignedInt(pduDataStream);
                }
                throw new RuntimeException("Value length > LENGTH_QUOTE!");
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static EncodedStringValue parseEncodedStringValue(ByteArrayInputStream pduDataStream) {
        EncodedStringValue returnValue;
        if ($assertionsDisabled || pduDataStream != null) {
            pduDataStream.mark(1);
            int charset = 0;
            int temp = pduDataStream.read();
            if ($assertionsDisabled || -1 != temp) {
                int first = temp & 255;
                pduDataStream.reset();
                if (first < TEXT_MIN) {
                    parseValueLength(pduDataStream);
                    charset = parseShortInteger(pduDataStream);
                }
                byte[] textString = parseWapString(pduDataStream, 0);
                if (charset != 0) {
                    try {
                        returnValue = new EncodedStringValue(charset, textString);
                    } catch (Exception e) {
                        return null;
                    }
                } else {
                    returnValue = new EncodedStringValue(textString);
                }
                return returnValue;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static byte[] parseWapString(ByteArrayInputStream pduDataStream, int stringType) {
        if ($assertionsDisabled || pduDataStream != null) {
            pduDataStream.mark(1);
            int temp = pduDataStream.read();
            if ($assertionsDisabled || -1 != temp) {
                if (1 == stringType && QUOTED_STRING_FLAG == temp) {
                    pduDataStream.mark(1);
                } else if (stringType == 0 && 127 == temp) {
                    pduDataStream.mark(1);
                } else {
                    pduDataStream.reset();
                }
                return getWapString(pduDataStream, stringType);
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static boolean isTokenCharacter(int ch) {
        if (ch < 33 || ch > 126) {
            return $assertionsDisabled;
        }
        switch (ch) {
            case QUOTED_STRING_FLAG /*34*/:
            case 40:
            case 41:
            case 44:
            case 47:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 91:
            case 92:
            case 93:
            case 123:
            case 125:
                return $assertionsDisabled;
            default:
                return true;
        }
    }

    protected static boolean isText(int ch) {
        if ((ch >= TEXT_MIN && ch <= 126) || (ch >= 128 && ch <= 255)) {
            return true;
        }
        switch (ch) {
            case CharacterSets.ISO_8859_6:
            case CharacterSets.ISO_8859_7:
            case 13:
                return true;
            case CharacterSets.ISO_8859_8:
            case CharacterSets.ISO_8859_9:
            default:
                return $assertionsDisabled;
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 134 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static byte[] getWapString(java.io.ByteArrayInputStream r4, int r5) {
        /*
            r3 = -1
            boolean r2 = com.km.tool.mms.pdu.PduParser.$assertionsDisabled
            if (r2 != 0) goto L_0x000d
            if (r4 != 0) goto L_0x000d
            java.lang.AssertionError r2 = new java.lang.AssertionError
            r2.<init>()
            throw r2
        L_0x000d:
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream
            r0.<init>()
            int r1 = r4.read()
            boolean r2 = com.km.tool.mms.pdu.PduParser.$assertionsDisabled
            if (r2 != 0) goto L_0x0048
            if (r3 != r1) goto L_0x0048
            java.lang.AssertionError r2 = new java.lang.AssertionError
            r2.<init>()
            throw r2
        L_0x0022:
            r2 = 2
            if (r5 != r2) goto L_0x003e
            boolean r2 = isTokenCharacter(r1)
            if (r2 == 0) goto L_0x002e
            r0.write(r1)
        L_0x002e:
            int r1 = r4.read()
            boolean r2 = com.km.tool.mms.pdu.PduParser.$assertionsDisabled
            if (r2 != 0) goto L_0x0048
            if (r3 != r1) goto L_0x0048
            java.lang.AssertionError r2 = new java.lang.AssertionError
            r2.<init>()
            throw r2
        L_0x003e:
            boolean r2 = isText(r1)
            if (r2 == 0) goto L_0x002e
            r0.write(r1)
            goto L_0x002e
        L_0x0048:
            if (r3 == r1) goto L_0x004c
            if (r1 != 0) goto L_0x0022
        L_0x004c:
            int r2 = r0.size()
            if (r2 <= 0) goto L_0x0057
            byte[] r2 = r0.toByteArray()
        L_0x0056:
            return r2
        L_0x0057:
            r2 = 0
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.km.tool.mms.pdu.PduParser.getWapString(java.io.ByteArrayInputStream, int):byte[]");
    }

    protected static int extractByteValue(ByteArrayInputStream pduDataStream) {
        if ($assertionsDisabled || pduDataStream != null) {
            int temp = pduDataStream.read();
            if ($assertionsDisabled || -1 != temp) {
                return temp & 255;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static int parseShortInteger(ByteArrayInputStream pduDataStream) {
        if ($assertionsDisabled || pduDataStream != null) {
            int temp = pduDataStream.read();
            if ($assertionsDisabled || -1 != temp) {
                return temp & 127;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static long parseLongInteger(ByteArrayInputStream pduDataStream) {
        if ($assertionsDisabled || pduDataStream != null) {
            int temp = pduDataStream.read();
            if ($assertionsDisabled || -1 != temp) {
                int count = temp & 255;
                if (count > 8) {
                    throw new RuntimeException("Octet count greater than 8 and I can't represent that!");
                }
                long result = 0;
                int i = 0;
                while (i < count) {
                    int temp2 = pduDataStream.read();
                    if ($assertionsDisabled || -1 != temp2) {
                        result = (result << 8) + ((long) (temp2 & 255));
                        i++;
                    } else {
                        throw new AssertionError();
                    }
                }
                return result;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static long parseIntegerValue(ByteArrayInputStream pduDataStream) {
        if ($assertionsDisabled || pduDataStream != null) {
            pduDataStream.mark(1);
            int temp = pduDataStream.read();
            if ($assertionsDisabled || -1 != temp) {
                pduDataStream.reset();
                if (temp > 127) {
                    return (long) parseShortInteger(pduDataStream);
                }
                return parseLongInteger(pduDataStream);
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    protected static int skipWapValue(ByteArrayInputStream pduDataStream, int length) {
        if ($assertionsDisabled || pduDataStream != null) {
            int readLen = pduDataStream.read(new byte[length], 0, length);
            if (readLen < length) {
                return -1;
            }
            return readLen;
        }
        throw new AssertionError();
    }

    /* JADX INFO: Multiple debug info for r0v7 byte[]: [D('lastLen' int), D('name' byte[])] */
    /* JADX INFO: Multiple debug info for r0v11 int: [D('lastLen' int), D('firstValue' int)] */
    /* JADX INFO: Multiple debug info for r1v6 int: [D('charsetStr' byte[]), D('tempPos' int)] */
    /* JADX INFO: Multiple debug info for r0v22 int: [D('charset' int), D('firstValue' int)] */
    /* JADX INFO: Multiple debug info for r0v25 byte[]: [D('lastLen' int), D('start' byte[])] */
    /* JADX INFO: Multiple debug info for r0v29 int: [D('lastLen' int), D('first' int)] */
    /* JADX INFO: Multiple debug info for r0v31 byte[]: [D('first' int), D('type' byte[])] */
    /* JADX INFO: Multiple debug info for r0v34 int: [D('index' int), D('first' int)] */
    /* JADX INFO: Multiple debug info for r0v36 byte[]: [D('index' int), D('type' byte[])] */
    protected static void parseContentTypeParams(ByteArrayInputStream pduDataStream, HashMap<Integer, Object> map, Integer length) {
        if (!$assertionsDisabled && pduDataStream == null) {
            throw new AssertionError();
        } else if ($assertionsDisabled || length.intValue() > 0) {
            int startPos = pduDataStream.available();
            int lastLen = length.intValue();
            while (lastLen > 0) {
                int param = pduDataStream.read();
                if ($assertionsDisabled || -1 != param) {
                    lastLen--;
                    switch (param) {
                        case 129:
                            pduDataStream.mark(1);
                            int firstValue = extractByteValue(pduDataStream);
                            pduDataStream.reset();
                            if ((firstValue <= TEXT_MIN || firstValue >= 127) && firstValue != 0) {
                                int firstValue2 = (int) parseIntegerValue(pduDataStream);
                                if (map != null) {
                                    map.put(129, Integer.valueOf(firstValue2));
                                }
                            } else {
                                byte[] charsetStr = parseWapString(pduDataStream, 0);
                                try {
                                    map.put(129, Integer.valueOf(CharacterSets.getMibEnumValue(new String(charsetStr))));
                                } catch (UnsupportedEncodingException e) {
                                    Log.e(LOG_TAG, Arrays.toString(charsetStr), e);
                                    map.put(129, 0);
                                }
                            }
                            lastLen = length.intValue() - (startPos - pduDataStream.available());
                            break;
                        case 131:
                        case 137:
                            pduDataStream.mark(1);
                            int first = extractByteValue(pduDataStream);
                            pduDataStream.reset();
                            if (first > 127) {
                                int first2 = parseShortInteger(pduDataStream);
                                if (first2 < PduContentTypes.contentTypes.length) {
                                    map.put(131, PduContentTypes.contentTypes[first2].getBytes());
                                }
                            } else {
                                byte[] type = parseWapString(pduDataStream, 0);
                                if (!(type == null || map == null)) {
                                    map.put(131, type);
                                }
                            }
                            lastLen = length.intValue() - (startPos - pduDataStream.available());
                            break;
                        case 133:
                        case 151:
                            byte[] name = parseWapString(pduDataStream, 0);
                            if (!(name == null || map == null)) {
                                map.put(151, name);
                            }
                            lastLen = length.intValue() - (startPos - pduDataStream.available());
                            break;
                        case 138:
                        case 153:
                            byte[] start = parseWapString(pduDataStream, 0);
                            if (!(start == null || map == null)) {
                                map.put(153, start);
                            }
                            lastLen = length.intValue() - (startPos - pduDataStream.available());
                            break;
                        default:
                            Log.v(LOG_TAG, "Not supported Content-Type parameter");
                            if (-1 != skipWapValue(pduDataStream, lastLen)) {
                                lastLen = 0;
                                break;
                            } else {
                                Log.e(LOG_TAG, "Corrupt Content-Type");
                                break;
                            }
                    }
                } else {
                    throw new AssertionError();
                }
            }
            if (lastLen != 0) {
                Log.e(LOG_TAG, "Corrupt Content-Type");
            }
        } else {
            throw new AssertionError();
        }
    }

    /* JADX INFO: Multiple debug info for r2v1 int: [D('length' int), D('temp' int)] */
    /* JADX INFO: Multiple debug info for r0v5 int: [D('index' int), D('contentType' byte[])] */
    /* JADX INFO: Multiple debug info for r1v9 int: [D('endPos' int), D('first' int)] */
    /* JADX INFO: Multiple debug info for r1v11 int: [D('endPos' int), D('parameterLen' int)] */
    protected static byte[] parseContentType(ByteArrayInputStream pduDataStream, HashMap<Integer, Object> map) {
        byte[] contentType;
        byte[] contentType2;
        if ($assertionsDisabled || pduDataStream != null) {
            pduDataStream.mark(1);
            int temp = pduDataStream.read();
            if ($assertionsDisabled || -1 != temp) {
                pduDataStream.reset();
                int cur = temp & 255;
                if (cur < TEXT_MIN) {
                    int temp2 = parseValueLength(pduDataStream);
                    int startPos = pduDataStream.available();
                    pduDataStream.mark(1);
                    int temp3 = pduDataStream.read();
                    if ($assertionsDisabled || -1 != temp3) {
                        pduDataStream.reset();
                        int first = temp3 & 255;
                        if (first >= TEXT_MIN && first <= 127) {
                            contentType2 = parseWapString(pduDataStream, 0);
                        } else if (first > 127) {
                            int index = parseShortInteger(pduDataStream);
                            if (index < PduContentTypes.contentTypes.length) {
                                contentType2 = PduContentTypes.contentTypes[index].getBytes();
                            } else {
                                pduDataStream.reset();
                                contentType2 = parseWapString(pduDataStream, 0);
                            }
                        } else {
                            Log.e(LOG_TAG, "Corrupt content-type");
                            return PduContentTypes.contentTypes[0].getBytes();
                        }
                        int parameterLen = temp2 - (startPos - pduDataStream.available());
                        if (parameterLen > 0) {
                            parseContentTypeParams(pduDataStream, map, Integer.valueOf(parameterLen));
                        }
                        if (parameterLen < 0) {
                            Log.e(LOG_TAG, "Corrupt MMS message");
                            return PduContentTypes.contentTypes[0].getBytes();
                        }
                        contentType = contentType2;
                    } else {
                        throw new AssertionError();
                    }
                } else if (cur <= 127) {
                    contentType = parseWapString(pduDataStream, 0);
                } else {
                    contentType = PduContentTypes.contentTypes[parseShortInteger(pduDataStream)].getBytes();
                }
                return contentType;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    /* JADX INFO: Multiple debug info for r0v3 com.km.tool.mms.pdu.EncodedStringValue: [D('messageType' int), D('rrFrom' com.km.tool.mms.pdu.EncodedStringValue)] */
    /* JADX INFO: Multiple debug info for r0v5 byte[]: [D('rrFrom' com.km.tool.mms.pdu.EncodedStringValue), D('rrMessageId' byte[])] */
    /* JADX INFO: Multiple debug info for r0v7 int: [D('rrReadStatus' int), D('rrMessageId' byte[])] */
    /* JADX INFO: Multiple debug info for r4v2 com.km.tool.mms.pdu.EncodedStringValue[]: [D('headers' com.km.tool.mms.pdu.PduHeaders), D('rrTo' com.km.tool.mms.pdu.EncodedStringValue[])] */
    /* JADX INFO: Multiple debug info for r0v10 long: [D('messageType' int), D('roDate' long)] */
    /* JADX INFO: Multiple debug info for r0v15 byte[]: [D('roFrom' com.km.tool.mms.pdu.EncodedStringValue), D('roMessageId' byte[])] */
    /* JADX INFO: Multiple debug info for r0v17 int: [D('roReadStatus' int), D('roMessageId' byte[])] */
    /* JADX INFO: Multiple debug info for r4v7 com.km.tool.mms.pdu.EncodedStringValue[]: [D('roTo' com.km.tool.mms.pdu.EncodedStringValue[]), D('headers' com.km.tool.mms.pdu.PduHeaders)] */
    /* JADX INFO: Multiple debug info for r4v13 byte[]: [D('aiTransactionId' byte[]), D('headers' com.km.tool.mms.pdu.PduHeaders)] */
    /* JADX INFO: Multiple debug info for r0v21 long: [D('messageType' int), D('diDate' long)] */
    /* JADX INFO: Multiple debug info for r0v26 int: [D('diMessageId' byte[]), D('diStatus' int)] */
    /* JADX INFO: Multiple debug info for r4v15 com.km.tool.mms.pdu.EncodedStringValue[]: [D('headers' com.km.tool.mms.pdu.PduHeaders), D('diTo' com.km.tool.mms.pdu.EncodedStringValue[])] */
    /* JADX INFO: Multiple debug info for r0v29 byte[]: [D('messageType' int), D('rcContentType' byte[])] */
    /* JADX INFO: Multiple debug info for r0v31 long: [D('rcContentType' byte[]), D('rcDate' long)] */
    /* JADX INFO: Multiple debug info for r0v33 int: [D('messageType' int), D('nriStatus' int)] */
    /* JADX INFO: Multiple debug info for r4v23 byte[]: [D('nriTransactionId' byte[]), D('headers' com.km.tool.mms.pdu.PduHeaders)] */
    /* JADX INFO: Multiple debug info for r0v36 byte[]: [D('messageType' int), D('niContentLocation' byte[])] */
    /* JADX INFO: Multiple debug info for r0v38 long: [D('niExpiry' long), D('niContentLocation' byte[])] */
    /* JADX INFO: Multiple debug info for r0v43 long: [D('niMessageSize' long), D('niMessageClass' byte[])] */
    /* JADX INFO: Multiple debug info for r4v26 byte[]: [D('headers' com.km.tool.mms.pdu.PduHeaders), D('niTransactionId' byte[])] */
    /* JADX INFO: Multiple debug info for r0v47 int: [D('messageType' int), D('scResponseStatus' int)] */
    /* JADX INFO: Multiple debug info for r4v32 byte[]: [D('headers' com.km.tool.mms.pdu.PduHeaders), D('scTransactionId' byte[])] */
    /* JADX INFO: Multiple debug info for r0v50 byte[]: [D('messageType' int), D('srContentType' byte[])] */
    /* JADX INFO: Multiple debug info for r0v52 com.km.tool.mms.pdu.EncodedStringValue: [D('srFrom' com.km.tool.mms.pdu.EncodedStringValue), D('srContentType' byte[])] */
    /* JADX INFO: Multiple debug info for r4v35 byte[]: [D('srTransactionId' byte[]), D('headers' com.km.tool.mms.pdu.PduHeaders)] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0158 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static boolean checkMandatoryHeader(com.km.tool.mms.pdu.PduHeaders r4) {
        /*
            if (r4 != 0) goto L_0x0004
            r4 = 0
        L_0x0003:
            return r4
        L_0x0004:
            r0 = 140(0x8c, float:1.96E-43)
            int r0 = r4.getOctet(r0)
            r1 = 141(0x8d, float:1.98E-43)
            int r1 = r4.getOctet(r1)
            if (r1 != 0) goto L_0x0014
            r4 = 0
            goto L_0x0003
        L_0x0014:
            switch(r0) {
                case 128: goto L_0x0019;
                case 129: goto L_0x0037;
                case 130: goto L_0x004b;
                case 131: goto L_0x0086;
                case 132: goto L_0x009c;
                case 133: goto L_0x00e6;
                case 134: goto L_0x00b6;
                case 135: goto L_0x012c;
                case 136: goto L_0x00f1;
                default: goto L_0x0017;
            }
        L_0x0017:
            r4 = 0
            goto L_0x0003
        L_0x0019:
            r0 = 132(0x84, float:1.85E-43)
            byte[] r0 = r4.getTextString(r0)
            if (r0 != 0) goto L_0x0023
            r4 = 0
            goto L_0x0003
        L_0x0023:
            r0 = 137(0x89, float:1.92E-43)
            com.km.tool.mms.pdu.EncodedStringValue r0 = r4.getEncodedStringValue(r0)
            if (r0 != 0) goto L_0x002d
            r4 = 0
            goto L_0x0003
        L_0x002d:
            r0 = 152(0x98, float:2.13E-43)
            byte[] r4 = r4.getTextString(r0)
            if (r4 != 0) goto L_0x0158
            r4 = 0
            goto L_0x0003
        L_0x0037:
            r0 = 146(0x92, float:2.05E-43)
            int r0 = r4.getOctet(r0)
            if (r0 != 0) goto L_0x0041
            r4 = 0
            goto L_0x0003
        L_0x0041:
            r0 = 152(0x98, float:2.13E-43)
            byte[] r4 = r4.getTextString(r0)
            if (r4 != 0) goto L_0x0158
            r4 = 0
            goto L_0x0003
        L_0x004b:
            r0 = 131(0x83, float:1.84E-43)
            byte[] r0 = r4.getTextString(r0)
            if (r0 != 0) goto L_0x0055
            r4 = 0
            goto L_0x0003
        L_0x0055:
            r0 = 136(0x88, float:1.9E-43)
            long r0 = r4.getLongInteger(r0)
            r2 = -1
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0063
            r4 = 0
            goto L_0x0003
        L_0x0063:
            r0 = 138(0x8a, float:1.93E-43)
            byte[] r0 = r4.getTextString(r0)
            if (r0 != 0) goto L_0x006d
            r4 = 0
            goto L_0x0003
        L_0x006d:
            r0 = 142(0x8e, float:1.99E-43)
            long r0 = r4.getLongInteger(r0)
            r2 = -1
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x007b
            r4 = 0
            goto L_0x0003
        L_0x007b:
            r0 = 152(0x98, float:2.13E-43)
            byte[] r4 = r4.getTextString(r0)
            if (r4 != 0) goto L_0x0158
            r4 = 0
            goto L_0x0003
        L_0x0086:
            r0 = 149(0x95, float:2.09E-43)
            int r0 = r4.getOctet(r0)
            if (r0 != 0) goto L_0x0091
            r4 = 0
            goto L_0x0003
        L_0x0091:
            r0 = 152(0x98, float:2.13E-43)
            byte[] r4 = r4.getTextString(r0)
            if (r4 != 0) goto L_0x0158
            r4 = 0
            goto L_0x0003
        L_0x009c:
            r0 = 132(0x84, float:1.85E-43)
            byte[] r0 = r4.getTextString(r0)
            if (r0 != 0) goto L_0x00a7
            r4 = 0
            goto L_0x0003
        L_0x00a7:
            r0 = 133(0x85, float:1.86E-43)
            long r0 = r4.getLongInteger(r0)
            r2 = -1
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 != 0) goto L_0x0158
            r4 = 0
            goto L_0x0003
        L_0x00b6:
            r0 = 133(0x85, float:1.86E-43)
            long r0 = r4.getLongInteger(r0)
            r2 = -1
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x00c5
            r4 = 0
            goto L_0x0003
        L_0x00c5:
            r0 = 139(0x8b, float:1.95E-43)
            byte[] r0 = r4.getTextString(r0)
            if (r0 != 0) goto L_0x00d0
            r4 = 0
            goto L_0x0003
        L_0x00d0:
            r0 = 149(0x95, float:2.09E-43)
            int r0 = r4.getOctet(r0)
            if (r0 != 0) goto L_0x00db
            r4 = 0
            goto L_0x0003
        L_0x00db:
            r0 = 151(0x97, float:2.12E-43)
            com.km.tool.mms.pdu.EncodedStringValue[] r4 = r4.getEncodedStringValues(r0)
            if (r4 != 0) goto L_0x0158
            r4 = 0
            goto L_0x0003
        L_0x00e6:
            r0 = 152(0x98, float:2.13E-43)
            byte[] r4 = r4.getTextString(r0)
            if (r4 != 0) goto L_0x0158
            r4 = 0
            goto L_0x0003
        L_0x00f1:
            r0 = 133(0x85, float:1.86E-43)
            long r0 = r4.getLongInteger(r0)
            r2 = -1
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0100
            r4 = 0
            goto L_0x0003
        L_0x0100:
            r0 = 137(0x89, float:1.92E-43)
            com.km.tool.mms.pdu.EncodedStringValue r0 = r4.getEncodedStringValue(r0)
            if (r0 != 0) goto L_0x010b
            r4 = 0
            goto L_0x0003
        L_0x010b:
            r0 = 139(0x8b, float:1.95E-43)
            byte[] r0 = r4.getTextString(r0)
            if (r0 != 0) goto L_0x0116
            r4 = 0
            goto L_0x0003
        L_0x0116:
            r0 = 155(0x9b, float:2.17E-43)
            int r0 = r4.getOctet(r0)
            if (r0 != 0) goto L_0x0121
            r4 = 0
            goto L_0x0003
        L_0x0121:
            r0 = 151(0x97, float:2.12E-43)
            com.km.tool.mms.pdu.EncodedStringValue[] r4 = r4.getEncodedStringValues(r0)
            if (r4 != 0) goto L_0x0158
            r4 = 0
            goto L_0x0003
        L_0x012c:
            r0 = 137(0x89, float:1.92E-43)
            com.km.tool.mms.pdu.EncodedStringValue r0 = r4.getEncodedStringValue(r0)
            if (r0 != 0) goto L_0x0137
            r4 = 0
            goto L_0x0003
        L_0x0137:
            r0 = 139(0x8b, float:1.95E-43)
            byte[] r0 = r4.getTextString(r0)
            if (r0 != 0) goto L_0x0142
            r4 = 0
            goto L_0x0003
        L_0x0142:
            r0 = 155(0x9b, float:2.17E-43)
            int r0 = r4.getOctet(r0)
            if (r0 != 0) goto L_0x014d
            r4 = 0
            goto L_0x0003
        L_0x014d:
            r0 = 151(0x97, float:2.12E-43)
            com.km.tool.mms.pdu.EncodedStringValue[] r4 = r4.getEncodedStringValues(r0)
            if (r4 != 0) goto L_0x0158
            r4 = 0
            goto L_0x0003
        L_0x0158:
            r4 = 1
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: com.km.tool.mms.pdu.PduParser.checkMandatoryHeader(com.km.tool.mms.pdu.PduHeaders):boolean");
    }
}
