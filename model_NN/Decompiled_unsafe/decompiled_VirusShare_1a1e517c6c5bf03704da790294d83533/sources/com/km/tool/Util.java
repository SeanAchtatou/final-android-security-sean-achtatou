package com.km.tool;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Util {
    public static String substring(String text, String startStr, String endStr) {
        if (text == null) {
            return null;
        }
        int startIndex = text.indexOf(startStr);
        System.out.println("startIndex=" + startIndex);
        if (startIndex >= 0) {
            int endIndex = text.indexOf(endStr, startStr.length() + startIndex);
            System.out.println("endIndex=" + endIndex);
            if (endIndex >= 0) {
                return text.substring(startStr.length() + startIndex, endIndex);
            }
        }
        return null;
    }

    public static String stringEliminate(String text, String eliminateStr) {
        if (text == null) {
            return null;
        }
        String[] strs = text.split(eliminateStr);
        StringBuffer sb = new StringBuffer();
        for (String append : strs) {
            sb.append(append);
        }
        return sb.toString();
    }

    public static int Str2Int(String aStr) {
        if (aStr == null) {
            return 0;
        }
        int temp = 0;
        try {
            temp = Integer.parseInt(aStr);
        } catch (Exception e) {
        }
        return temp;
    }

    public static String read_Uni(InputStream is) {
        try {
            byte[] word_uni = new byte[is.available()];
            is.skip(2);
            is.read(word_uni);
            is.close();
            StringBuffer stringbuffer = new StringBuffer("");
            int j = 0;
            while (j < word_uni.length) {
                int j2 = j + 1;
                byte b = word_uni[j];
                j = j2 + 1;
                stringbuffer.append((char) ((b & ApnManager.TYPE_UNCONNECT) | ((word_uni[j2] << 8) & 65280)));
            }
            return stringbuffer.toString();
        } catch (IOException e) {
            System.out.println(e);
            return null;
        } finally {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:55:0x00a3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String readIs2String(java.io.InputStream r9) throws java.io.IOException {
        /*
            r8 = 254(0xfe, float:3.56E-43)
            r7 = 3
            r6 = 2
            r2 = 0
            byte[] r0 = com.km.tool.Connect.readFully(r9)     // Catch:{ all -> 0x00a0 }
            int r1 = r0.length     // Catch:{ all -> 0x00a0 }
            if (r1 < r7) goto L_0x003e
            r4 = 0
            byte r4 = r0[r4]     // Catch:{ all -> 0x00a0 }
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = 239(0xef, float:3.35E-43)
            if (r4 != r5) goto L_0x003e
            r4 = 1
            byte r4 = r0[r4]     // Catch:{ all -> 0x00a0 }
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = 187(0xbb, float:2.62E-43)
            if (r4 != r5) goto L_0x003e
            r4 = 2
            byte r4 = r0[r4]     // Catch:{ all -> 0x00a0 }
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = 191(0xbf, float:2.68E-43)
            if (r4 != r5) goto L_0x003e
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x00a0 }
            r4 = 3
            int r5 = r1 - r7
            java.lang.String r6 = "UTF-8"
            r3.<init>(r0, r4, r5, r6)     // Catch:{ all -> 0x00a0 }
            java.lang.String r4 = "UTF-8"
            android.util.Log.i(r4, r3)     // Catch:{ all -> 0x00a8 }
            if (r9 == 0) goto L_0x003c
            r9.close()
            r9 = 0
        L_0x003c:
            r2 = r3
        L_0x003d:
            return r3
        L_0x003e:
            if (r1 < r6) goto L_0x008e
            r4 = 0
            byte r4 = r0[r4]     // Catch:{ all -> 0x00a0 }
            r4 = r4 & 255(0xff, float:3.57E-43)
            if (r4 != r8) goto L_0x0067
            r4 = 1
            byte r4 = r0[r4]     // Catch:{ all -> 0x00a0 }
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = 255(0xff, float:3.57E-43)
            if (r4 != r5) goto L_0x0067
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x00a0 }
            r4 = 2
            int r5 = r1 - r6
            java.lang.String r6 = "UTF-16BE"
            r3.<init>(r0, r4, r5, r6)     // Catch:{ all -> 0x00a0 }
            java.lang.String r4 = "UTF-16BE"
            android.util.Log.i(r4, r3)     // Catch:{ all -> 0x00a8 }
            if (r9 == 0) goto L_0x0065
            r9.close()
            r9 = 0
        L_0x0065:
            r2 = r3
            goto L_0x003d
        L_0x0067:
            r4 = 0
            byte r4 = r0[r4]     // Catch:{ all -> 0x00a0 }
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = 255(0xff, float:3.57E-43)
            if (r4 != r5) goto L_0x008e
            r4 = 1
            byte r4 = r0[r4]     // Catch:{ all -> 0x00a0 }
            r4 = r4 & 255(0xff, float:3.57E-43)
            if (r4 != r8) goto L_0x008e
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x00a0 }
            r4 = 2
            int r5 = r1 - r6
            java.lang.String r6 = "UTF-16LE"
            r3.<init>(r0, r4, r5, r6)     // Catch:{ all -> 0x00a0 }
            java.lang.String r4 = "UTF-16LE"
            android.util.Log.i(r4, r3)     // Catch:{ all -> 0x00a8 }
            if (r9 == 0) goto L_0x008c
            r9.close()
            r9 = 0
        L_0x008c:
            r2 = r3
            goto L_0x003d
        L_0x008e:
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x00a0 }
            r3.<init>(r0)     // Catch:{ all -> 0x00a0 }
            java.lang.String r4 = "GB2312"
            android.util.Log.i(r4, r3)     // Catch:{ all -> 0x00a8 }
            if (r9 == 0) goto L_0x009e
            r9.close()
            r9 = 0
        L_0x009e:
            r2 = r3
            goto L_0x003d
        L_0x00a0:
            r4 = move-exception
        L_0x00a1:
            if (r9 == 0) goto L_0x00a7
            r9.close()
            r9 = 0
        L_0x00a7:
            throw r4
        L_0x00a8:
            r4 = move-exception
            r2 = r3
            goto L_0x00a1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.km.tool.Util.readIs2String(java.io.InputStream):java.lang.String");
    }

    public static String readFile2Str(String fileName) throws IOException {
        return readIs2String(new FileInputStream(fileName));
    }

    public static String phoneNumberModify(String number) {
        if (number == null) {
            return null;
        }
        if (number.length() <= 11) {
            return number;
        }
        if (number.startsWith("0086")) {
            return number.substring(4);
        }
        if (number.startsWith("+86") || number.startsWith("086")) {
            return number.substring(3);
        }
        return number.startsWith("86") ? number.substring(2) : number;
    }
}
