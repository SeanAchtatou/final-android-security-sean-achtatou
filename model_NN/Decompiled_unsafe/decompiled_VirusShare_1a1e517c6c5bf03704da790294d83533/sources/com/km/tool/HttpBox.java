package com.km.tool;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class HttpBox {
    private boolean cmwap;
    private int connectTimeout;
    private int contentLenght;
    private String contentType;
    private HashMap<String, String> header;
    private HttpURLConnection http;
    private byte[] inData;
    private URL mUrl;
    private byte[] outData;
    private boolean post;
    private int readTimeout;
    private int responseCode;
    private URL url;

    public int getResponseCode() {
        return this.responseCode;
    }

    public HttpBox() {
        this.cmwap = true;
    }

    public HttpBox(String _url) throws MalformedURLException {
        this(_url, null, null);
    }

    public HttpBox(String _url, byte[] params) throws MalformedURLException {
        this(_url, params, "application/x-www-form-urlencoded");
    }

    public HttpBox(String _url, byte[] _data, String _contentType) throws MalformedURLException {
        this.cmwap = true;
        setUrl(_url);
        postData(_data, _contentType);
    }

    public void connect() throws IOException {
        String _url;
        Log.i("Enter", "connect");
        try {
            String host = this.url.getHost();
            if (this.cmwap) {
                _url = this.url.toString().replace(host, "10.0.0.172");
            } else {
                _url = null;
            }
            if (_url != null) {
                this.url = new URL(_url);
            }
            this.http = (HttpURLConnection) this.url.openConnection();
            if (this.cmwap) {
                addHeader("Host", "10.0.0.172");
                addHeader("X-Online-Host", host);
            }
            Log.i("X-Online-Host", this.url.getHost());
        } catch (Exception e) {
            Log.i("Exception", e.toString());
        }
        setHeader();
        HttpURLConnection.setFollowRedirects(false);
        this.http.setConnectTimeout(this.connectTimeout);
        this.http.setReadTimeout(this.readTimeout);
        if (this.post) {
            this.http.setRequestMethod("POST");
            write();
        } else {
            this.http.setRequestMethod("GET");
        }
        Log.i("bef", "getContentLength");
        this.contentLenght = this.http.getContentLength();
        Log.i("end", "ContentLength = " + this.contentLenght);
        this.responseCode = this.http.getResponseCode();
        Log.i("end", "responseCode = " + this.responseCode);
        autoTurn();
        Log.i("Exit", "connect");
    }

    private void autoTurn() throws IOException {
        if (this.responseCode == 301 || this.responseCode == 302) {
            Log.i("dir", "302");
            String newUrlStr = this.http.getHeaderField("location");
            if (newUrlStr.startsWith("http://")) {
                setUrl(newUrlStr);
            } else {
                setUrl("http://" + this.url.getHost() + ":" + this.url.getPort() + newUrlStr);
            }
            Log.i("bef", this.url.toString());
            this.http.disconnect();
            Log.i("end", this.url.toString());
            this.http = null;
            this.responseCode = 0;
            this.contentLenght = 0;
            connect();
        }
    }

    public void read() throws IOException {
        InputStream is = this.http.getInputStream();
        this.inData = Connect.readFully(is);
        is.close();
    }

    private void setHeader() {
        if (this.header != null && this.header.size() > 0) {
            String[] keys = new String[this.header.size()];
            this.header.keySet().toArray(keys);
            for (int i = 0; i < keys.length; i++) {
                this.http.setRequestProperty(keys[i], this.header.get(keys[i]));
            }
        }
    }

    private void write() throws IOException {
        if (this.outData != null) {
            this.http.setDoOutput(true);
            OutputStream os = this.http.getOutputStream();
            os.write(this.outData);
            os.flush();
            os.close();
        }
    }

    public void setUrl(String _url) throws MalformedURLException {
        this.url = new URL(_url);
    }

    public void setRequestMethod(boolean _post) {
        this.post = _post;
    }

    public void postData(byte[] _data, String _contentType) {
        this.outData = _data;
        if (_data != null) {
            this.outData = _data;
            if (_contentType == null || _contentType.trim().equals("")) {
                this.contentType = "application/octet-stream";
            } else {
                this.contentType = _contentType;
            }
            this.post = true;
            addHeader("Content-Type", this.contentType);
            addHeader("Content-Length", String.valueOf(this.outData.length));
            return;
        }
        this.post = false;
    }

    public void addHeader(String key, String value) {
        if (this.header == null) {
            this.header = new HashMap<>();
        }
        this.header.put(key, value);
    }

    public void close() {
        if (this.http != null) {
            this.http.disconnect();
            this.http = null;
        }
        if (this.header != null) {
            this.header.clear();
            this.header = null;
        }
        this.url = null;
        this.outData = null;
        this.contentType = null;
        this.inData = null;
    }

    public int getConnectTimeout() {
        return this.connectTimeout;
    }

    public void setConnectTimeout(int _connectTimeout) {
        this.connectTimeout = _connectTimeout;
    }

    public int getReadTimeout() {
        return this.readTimeout;
    }

    public void setReadTimeout(int _readTimeout) {
        this.readTimeout = _readTimeout;
    }

    public HttpURLConnection getHttp() {
        return this.http;
    }

    public int getContentLenght() {
        return this.contentLenght;
    }

    public byte[] getInData() {
        return this.inData;
    }
}
