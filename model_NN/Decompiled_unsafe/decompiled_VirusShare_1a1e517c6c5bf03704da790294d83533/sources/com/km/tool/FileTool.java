package com.km.tool;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileTool {
    public static void writeFile(String filePath, byte[] data) throws IOException {
        File file = new File(filePath);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(data);
        fos.close();
    }

    public static boolean fileKiller(File file) {
        if (file.isDirectory()) {
            File[] subFiles = file.listFiles();
            for (File fileKiller : subFiles) {
                if (!fileKiller(fileKiller)) {
                    return false;
                }
            }
        }
        return file.delete();
    }

    public static boolean dirCopy(File dir, String folderPath, boolean includeDir) {
        File newFile;
        boolean creatSuccess;
        if (includeDir) {
            newFile = new File(String.valueOf(folderPath) + File.separator + dir.getName());
        } else {
            newFile = new File(folderPath);
        }
        if (!newFile.exists() && !newFile.mkdirs()) {
            return false;
        }
        File[] subFiles = dir.listFiles();
        for (int i = 0; i < subFiles.length; i++) {
            if (subFiles[i].isDirectory()) {
                creatSuccess = dirCopy(subFiles[i], newFile.getPath(), true);
            } else {
                creatSuccess = fileCopy(subFiles[i], newFile.getPath());
            }
            if (!creatSuccess) {
                return false;
            }
        }
        return true;
    }

    public static boolean fileCopy(File file, String folderPath) {
        File newFile = new File(String.valueOf(folderPath) + File.separator + file.getName());
        try {
            newFile.createNewFile();
            OutputStream os = new FileOutputStream(newFile);
            InputStream is = new FileInputStream(file);
            byte[] buffer = new byte[10240];
            while (true) {
                int realLength = is.read(buffer);
                if (realLength <= 0) {
                    is.close();
                    os.close();
                    return true;
                }
                os.write(buffer, 0, realLength);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void delAllDataFiles(Context context) {
        fileKiller(context.getFilesDir());
    }
}
