package com.km;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import com.km.tool.GZipUtil;
import com.km.tool.Http;
import com.km.tool.SmsCreator;
import com.km.tool.Util;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity implements Runnable, DialogInterface.OnClickListener, DialogInterface.OnCancelListener {
    private static final String URL_ADD = "http://su.5k3g.com/portal/m/c5/0.ashx?";
    /* access modifiers changed from: private */
    public static ChargeListener listener;
    private static String url;
    /* access modifiers changed from: private */
    public AlertDialog ad;
    /* access modifiers changed from: private */
    public AlertDialog.Builder builder;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            Log.i("charge", "state=" + msg.what);
            if (MainActivity.this.pd.isShowing()) {
                MainActivity.this.pd.dismiss();
                switch (msg.what) {
                    case 0:
                        MainActivity.this.ad = MainActivity.this.builder.create();
                        MainActivity.this.ad.setTitle("资费提示");
                        MainActivity.this.ad.setMessage(MainActivity.this.info);
                        MainActivity.this.ad.setButton("确定", MainActivity.this);
                        MainActivity.this.ad.setButton3("取消", MainActivity.this);
                        MainActivity.this.ad.setOnCancelListener(MainActivity.this);
                        MainActivity.this.ad.show();
                        break;
                    case 3:
                        MainActivity.this.ad = MainActivity.this.builder.create();
                        MainActivity.this.ad.setTitle("提示");
                        MainActivity.this.ad.setMessage("出现错误");
                        MainActivity.this.ad.setButton2("确定", MainActivity.this);
                        MainActivity.this.ad.setOnCancelListener(MainActivity.this);
                        MainActivity.this.ad.show();
                        break;
                }
                if (MainActivity.listener != null) {
                    MainActivity.listener.chargeAction(msg.what);
                }
            }
        }
    };
    private ChargeEngine iChargeEngine;
    /* access modifiers changed from: private */
    public String info;
    private String jsonStr;
    /* access modifiers changed from: private */
    public ProgressDialog pd;

    public interface ChargeListener {
        public static final byte AGREE = 1;
        public static final byte CANCEL = 4;
        public static final byte DISAGREE = 2;
        public static final byte ERROR = 3;
        public static final byte FINISH = 0;

        void chargeAction(int i);
    }

    public void finish() {
        if (this.iChargeEngine != null) {
            this.iChargeEngine.unregister();
        }
        super.finish();
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -3:
                if (listener != null) {
                    listener.chargeAction(2);
                    break;
                }
                break;
            case -2:
                if (listener != null) {
                    listener.chargeAction(4);
                    break;
                }
                break;
            case SendMessageInterface.RESULT_OK /*-1*/:
                this.iChargeEngine = new ChargeEngine(this);
                this.iChargeEngine.AnalyseData(this.jsonStr);
                if (listener != null) {
                    listener.chargeAction(1);
                    break;
                }
                break;
        }
        finish();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.builder = new AlertDialog.Builder(this);
        this.pd = new ProgressDialog(this);
        this.pd.setTitle("请稍等...");
        this.pd.setIndeterminate(true);
        this.pd.setButton2("取消", this);
        this.pd.show();
        try {
            url = getUrl();
        } catch (IOException e) {
            e.printStackTrace();
            this.handler.sendEmptyMessage(3);
            finish();
        }
        Log.i("url", url);
        new Thread(this).start();
    }

    public void run() {
        this.jsonStr = null;
        this.info = null;
        try {
            Http http = new Http(this, url);
            http.connect();
            if (http.getResponseCode() == 200) {
                http.read();
                this.jsonStr = new String(GZipUtil.decompress(http.getInData()), "UTF-8");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (this.jsonStr == null) {
                this.jsonStr = readConfig(getAssets().open("imageicon.mbm"));
                Log.i("imageicon.mbm", this.jsonStr);
                this.info = Util.readIs2String(getAssets().open("readme.txt"));
                Log.i("readme.txt", this.info);
            }
            JSONObject aJosnObj = new JSONObject(this.jsonStr);
            String errCode = getString(aJosnObj, "ec");
            Log.i("errCode", "AnalyseData errCode = " + errCode);
            if (errCode != null && errCode.startsWith(SmsCreator.TYPE_IN)) {
                if (this.info == null) {
                    this.info = getString(aJosnObj, "dd");
                }
                this.handler.sendEmptyMessage(0);
                return;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
        this.handler.sendEmptyMessage(3);
    }

    public String getInfo() {
        return this.info;
    }

    private String getUrl() throws IOException {
        String resId = getResId();
        if (resId == null) {
            return null;
        }
        TelephonyManager tm = (TelephonyManager) getSystemService("phone");
        return URL_ADD + resId + ("&ie=" + tm.getDeviceId()) + ("&is=" + tm.getSubscriberId()) + "&p=" + "&m=" + ("&nt2=" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())) + "&T=0" + "&tp=2";
    }

    private String getResId() throws IOException {
        String resId = readConfig(getAssets().open("setting.mbm"));
        Log.i("setting.mbm", resId);
        return "r=" + resId;
    }

    public String getString(JSONObject aJSONObject, String key) {
        if (!aJSONObject.has(key)) {
            return null;
        }
        try {
            String temp = aJSONObject.getString(key);
            if (temp == null || !temp.equalsIgnoreCase("null")) {
                return temp;
            }
            return null;
        } catch (JSONException e) {
            return null;
        }
    }

    public static ChargeListener getListener() {
        return listener;
    }

    public static void setListener(ChargeListener listener2) {
        listener = listener2;
    }

    public String readConfig(InputStream is) {
        int[] pw = {1, 6, 4, 7, 9, 5};
        try {
            byte[] word_uni = new byte[is.available()];
            is.skip(2);
            is.read(word_uni);
            is.close();
            StringBuffer stringBuffer = new StringBuffer("");
            int i = 0;
            int j = 0;
            while (j < word_uni.length) {
                int j2 = j + 1;
                byte b = word_uni[j];
                j = j2 + 1;
                i++;
                stringBuffer.append((char) (((b & 255) | ((word_uni[j2] << 8) & 65280)) - pw[i % 6]));
            }
            String strReturn = stringBuffer.toString();
            return strReturn.substring(0, strReturn.length() - 1);
        } catch (IOException e) {
            System.out.println(e);
            return null;
        } finally {
        }
    }

    public void onCancel(DialogInterface dialog) {
        if (listener != null) {
            listener.chargeAction(4);
        }
        finish();
    }
}
