package km.home.config;

import android.util.Xml;
import java.io.IOException;
import java.io.InputStream;
import km.home.R;
import km.home.config.Config;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ConfigXmlParser {
    private Config config;

    public ConfigXmlParser(InputStream is) throws XmlPullParserException, IOException {
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(is, null);
        configParse(parser);
    }

    private void configParse(XmlPullParser parser) throws XmlPullParserException, IOException {
        int eventType = parser.getEventType();
        while (eventType != 1) {
            switch (eventType) {
                case 1:
                    return;
                case R.styleable.ApplicationsStackLayout_marginTop:
                    String name = parser.getName();
                    if (!name.equals("config")) {
                        if (!name.equals("workspace")) {
                            if (!name.equals("menu")) {
                                if (!name.equals("app")) {
                                    break;
                                } else {
                                    Config.ConfigApp app = this.config.creatApp();
                                    this.config.apps.add(app);
                                    while (true) {
                                        if (parser.next() == 3 && "app".equals(parser.getName())) {
                                            break;
                                        } else if (parser.getEventType() == 2) {
                                            if ("intent".equals(parser.getName())) {
                                                app.intent = parser.nextText();
                                            } else if ("icon".equals(parser.getName())) {
                                                app.icon = parser.nextText();
                                            }
                                        }
                                    }
                                }
                            } else {
                                Config.ConfigMenu menu = this.config.creatMenu();
                                this.config.menus.add(menu);
                                while (true) {
                                    if (parser.next() == 3 && "menu".equals(parser.getName())) {
                                        break;
                                    } else if (parser.getEventType() == 2) {
                                        if ("name".equals(parser.getName())) {
                                            menu.name = parser.nextText();
                                        } else if ("icon".equals(parser.getName())) {
                                            menu.icon = parser.nextText();
                                        }
                                    }
                                }
                            }
                        } else {
                            while (true) {
                                if (parser.next() == 3 && "workspace".equals(parser.getName())) {
                                    break;
                                } else if (parser.getEventType() == 2) {
                                    if ("wallpaper".equals(parser.getName())) {
                                        this.config.workSpaceWallpaper = parser.nextText();
                                    } else if ("background".equals(parser.getName())) {
                                        this.config.workSpaceBackground = parser.nextText();
                                    }
                                }
                            }
                        }
                    } else {
                        this.config = new Config();
                        if (parser.getAttributeCount() > 0) {
                            for (int i = 0; i < parser.getAttributeCount(); i++) {
                                String att = parser.getAttributeName(i);
                                if (att.equals("id")) {
                                    this.config.id = parser.getAttributeValue(i);
                                } else if (att.equals("cn_name")) {
                                    this.config.cnName = parser.getAttributeValue(i);
                                } else if (att.equals("en_name")) {
                                    this.config.enName = parser.getAttributeValue(i);
                                } else if (att.equals("ver")) {
                                    this.config.ver = parser.getAttributeValue(i);
                                }
                            }
                        }
                        parser.next();
                        break;
                    }
                    break;
                case 3:
                    String name2 = parser.getName();
                    break;
            }
            eventType = parser.next();
        }
    }

    public Config getConfig() {
        return this.config;
    }
}
