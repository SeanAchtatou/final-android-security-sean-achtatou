package km.home.config;

import java.util.Vector;

public class Config {
    public Vector<ConfigApp> apps = new Vector<>();
    public String cnName;
    public String drawerBackground;
    public int drawerTextColor;
    public int drawerTextSize;
    public String enName;
    public String id;
    public Vector<ConfigMenu> menus = new Vector<>();
    public String ver;
    public String workSpaceBackground;
    public int workSpaceTextColor;
    public int workSpaceTextSize;
    public String workSpaceWallpaper;

    public ConfigApp creatApp() {
        return new ConfigApp();
    }

    public ConfigMenu creatMenu() {
        return new ConfigMenu();
    }

    public class ConfigApp {
        public String icon;
        public String intent;

        public ConfigApp() {
        }
    }

    public class ConfigMenu {
        public String icon;
        public String name;

        public ConfigMenu() {
        }
    }
}
