package km.home;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.Layout;
import android.util.AttributeSet;
import android.widget.TextView;

public class BubbleTextView extends TextView {
    private static final float CORNER_RADIUS = 8.0f;
    private static final float PADDING_H = 5.0f;
    private static final float PADDING_V = 1.0f;
    private Paint mPaint;
    private final RectF mRect = new RectF();

    public BubbleTextView(Context context) {
        super(context);
        init();
    }

    public BubbleTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BubbleTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        this.mPaint = new Paint(1);
        this.mPaint.setColor(getContext().getResources().getColor(R.color.bubble_dark_background));
    }

    public void onDraw(Canvas canvas) {
        Layout layout = getLayout();
        RectF rect = this.mRect;
        int left = getCompoundPaddingLeft();
        int top = getExtendedPaddingTop();
        rect.set((((float) left) + layout.getLineLeft(0)) - PADDING_H, ((float) (layout.getLineTop(0) + top)) - PADDING_V, ((float) left) + layout.getLineRight(0) + PADDING_H, ((float) (layout.getLineBottom(0) + top)) + PADDING_V);
        canvas.drawRoundRect(rect, CORNER_RADIUS, CORNER_RADIUS, this.mPaint);
        super.onDraw(canvas);
    }
}
