package km.home;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import km.home.config.Config;
import km.theme.ThemeAdapter;
import km.theme.ThemeInfo;
import km.tools.Util;

public class Home extends Activity implements Runnable, DialogInterface.OnClickListener {
    private static final String DEFAULT_FAVORITES_PATH = "etc/favorites.xml";
    private static final String KEY_SAVE_GRID_OPENED = "grid.opened";
    private static final String LOG_TAG = "Home";
    private static final int MAX_RECENT_TASKS = 20;
    private static final int MENU_SEARCH = 3;
    private static final int MENU_SETTINGS = 4;
    private static final int MENU_THEME = 5;
    private static final int MENU_WALLPAPER_SETTINGS = 2;
    private static final String TAG_CLASS = "class";
    private static final String TAG_FAVORITE = "favorite";
    private static final String TAG_FAVORITES = "favorites";
    private static final String TAG_PACKAGE = "package";
    /* access modifiers changed from: private */
    public static ArrayList<ApplicationInfo> mApplications;
    private static LinkedList<ApplicationInfo> mFavorites;
    private static boolean mWallpaperChecked;
    private Config config;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what != 0 && msg.what == 1) {
                ((ArrayAdapter) Home.this.mGrid.getAdapter()).notifyDataSetInvalidated();
            }
        }
    };
    private final BroadcastReceiver mApplicationsReceiver = new ApplicationsIntentReceiver(this, null);
    /* access modifiers changed from: private */
    public GridView mGrid;
    private LayoutAnimationController mHideLayoutAnimation;
    private LayoutAnimationController mShowLayoutAnimation;
    private final BroadcastReceiver mWallpaperReceiver = new WallpaperIntentReceiver(this, null);
    private SharedPreferences sp;
    private Thread th;
    private AlertDialog themeChoice;
    private String themePath;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setDefaultKeyMode(3);
        setContentView((int) R.layout.home);
        registerIntentReceivers();
        this.config = UnlockActivity.themeMgr.getConfig();
        this.themePath = UnlockActivity.themeMgr.getThemePath();
        this.sp = UnlockActivity.themeMgr.getSp();
        setBackground();
        mApplications = new ArrayList<>();
        bindApplications();
        if (this.th == null) {
            this.th = new Thread(this);
            this.th.start();
        }
    }

    private void setBackground() {
        Drawable drawable;
        BitmapDrawable bd = new BitmapDrawable(String.valueOf(UnlockActivity.themeMgr.getCurrentThemePath(this.config)) + File.separator + this.config.workSpaceBackground);
        View findViewById = findViewById(R.id.home);
        if (bd.getBitmap() == null) {
            drawable = getWallpaper();
        } else {
            drawable = bd;
        }
        findViewById.setBackgroundDrawable(drawable);
    }

    public void run() {
        loadApplications(false);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if ("android.intent.action.MAIN".equals(intent.getAction())) {
            getWindow().closeAllPanels();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        int count = mApplications.size();
        for (int i = 0; i < count; i++) {
            mApplications.get(i).icon.setCallback(null);
        }
        unregisterReceiver(this.mWallpaperReceiver);
        unregisterReceiver(this.mApplicationsReceiver);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void registerIntentReceivers() {
        registerReceiver(this.mWallpaperReceiver, new IntentFilter("android.intent.action.WALLPAPER_CHANGED"));
        IntentFilter filter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        filter.addAction("android.intent.action.PACKAGE_REMOVED");
        filter.addAction("android.intent.action.PACKAGE_CHANGED");
        filter.addDataScheme(TAG_PACKAGE);
        registerReceiver(this.mApplicationsReceiver, filter);
    }

    private void bindApplications() {
        if (this.mGrid == null) {
            this.mGrid = (GridView) findViewById(R.id.all_apps);
        }
        this.mGrid.setAdapter((ListAdapter) new ApplicationsAdapter(this, mApplications));
        this.mGrid.setSelection(0);
        this.mGrid.setOnItemClickListener(new ApplicationLauncher(this, null));
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 3:
                    return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        BitmapDrawable wallpaperDrawable = null;
        BitmapDrawable searchDrawable = null;
        BitmapDrawable settingDrawable = null;
        BitmapDrawable themeDrawable = null;
        if (this.config != null) {
            String path = String.valueOf(this.themePath) + File.separator + this.config.cnName + "_" + this.config.id + File.separator;
            for (int i = 0; i < this.config.menus.size(); i++) {
                Config.ConfigMenu cm = this.config.menus.elementAt(i);
                if ("menu_search".equals(cm.name)) {
                    searchDrawable = new BitmapDrawable(String.valueOf(path) + cm.icon);
                } else if ("menu_wallpaper".equals(cm.name)) {
                    wallpaperDrawable = new BitmapDrawable(String.valueOf(path) + cm.icon);
                } else if ("menu_setting".equals(cm.name)) {
                    settingDrawable = new BitmapDrawable(String.valueOf(path) + cm.icon);
                } else if ("menu_theme".equals(cm.name)) {
                    themeDrawable = new BitmapDrawable(String.valueOf(path) + cm.icon);
                }
            }
        }
        if (wallpaperDrawable == null || wallpaperDrawable.getBitmap() == null) {
            menu.add(0, 2, 0, (int) R.string.menu_wallpaper).setIcon(17301567).setAlphabeticShortcut('W');
        } else {
            menu.add(0, 2, 0, (int) R.string.menu_wallpaper).setIcon(wallpaperDrawable).setAlphabeticShortcut('W');
        }
        if (searchDrawable == null || searchDrawable.getBitmap() == null) {
            menu.add(0, 3, 0, (int) R.string.menu_search).setIcon(17301600).setAlphabeticShortcut('s');
        } else {
            menu.add(0, 3, 0, (int) R.string.menu_search).setIcon(searchDrawable).setAlphabeticShortcut('s');
        }
        if (settingDrawable == null || settingDrawable.getBitmap() == null) {
            menu.add(0, 4, 0, (int) R.string.menu_settings).setIcon(17301577).setIntent(new Intent("android.settings.SETTINGS"));
        } else {
            menu.add(0, 4, 0, (int) R.string.menu_settings).setIcon(settingDrawable).setIntent(new Intent("android.settings.SETTINGS"));
        }
        if (themeDrawable == null || themeDrawable.getBitmap() == null) {
            menu.add(0, 5, 0, (int) R.string.menu_theme).setIcon(17301570);
            return true;
        }
        menu.add(0, 5, 0, (int) R.string.menu_theme).setIcon(themeDrawable);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                startWallpaper();
                return true;
            case 3:
                onSearchRequested();
                return true;
            case 4:
            default:
                return super.onOptionsItemSelected(item);
            case 5:
                showTheme();
                return true;
        }
    }

    private void startWallpaper() {
        startActivity(Intent.createChooser(new Intent("android.intent.action.SET_WALLPAPER"), getString(R.string.menu_wallpaper)));
    }

    private void startTheme() {
        Intent intent = new Intent(this, ThemeListActivity.class);
        intent.addFlags(536870912);
        startActivity(intent);
    }

    private void showTheme() {
        if (this.themeChoice == null) {
            this.themeChoice = Util.createDialogMenu(this, "选择主题", getThemeAdapter(), this);
        }
        this.themeChoice.show();
    }

    public void onClick(DialogInterface dialog, int which) {
        changeTheme(((ThemeAdapter) ((AlertDialog) dialog).getListView().getAdapter()).getList().get(which));
    }

    private void changeTheme(ThemeInfo themeInfo) {
        UnlockActivity.themeMgr.changeTheme(String.valueOf(themeInfo.path) + File.separator + "config.xml");
        Intent intent = new Intent(this, UnlockActivity.class);
        intent.addFlags(536870912);
        startActivity(intent);
        finish();
    }

    private ListAdapter getThemeAdapter() {
        String value;
        SharedPreferences sp2 = UnlockActivity.themeMgr.getSp();
        ArrayList<ThemeInfo> list = new ArrayList<>();
        for (String key : sp2.getAll().keySet()) {
            if (!(key == null || key.equals(ThemeManager.CURRENT_THEME) || (value = sp2.getString(key, null)) == null)) {
                ThemeInfo themeInfo = new ThemeInfo();
                themeInfo.id = key;
                themeInfo.path = String.valueOf(UnlockActivity.themeMgr.getThemePath()) + File.separator + value;
                list.add(themeInfo);
            }
        }
        return new ThemeAdapter(this, list);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0005, code lost:
        if (km.home.Home.mApplications != null) goto L_0x0007;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void loadApplications(boolean r16) {
        /*
            r15 = this;
            monitor-enter(r15)
            if (r16 == 0) goto L_0x0009
            java.util.ArrayList<km.home.ApplicationInfo> r12 = km.home.Home.mApplications     // Catch:{ all -> 0x0037 }
            if (r12 == 0) goto L_0x0009
        L_0x0007:
            monitor-exit(r15)
            return
        L_0x0009:
            android.content.pm.PackageManager r11 = r15.getPackageManager()     // Catch:{ all -> 0x0037 }
            android.content.Intent r10 = new android.content.Intent     // Catch:{ all -> 0x0037 }
            java.lang.String r12 = "android.intent.action.MAIN"
            r13 = 0
            r10.<init>(r12, r13)     // Catch:{ all -> 0x0037 }
            java.lang.String r12 = "android.intent.category.LAUNCHER"
            r10.addCategory(r12)     // Catch:{ all -> 0x0037 }
            r12 = 0
            java.util.List r2 = r11.queryIntentActivities(r10, r12)     // Catch:{ all -> 0x0037 }
            if (r2 == 0) goto L_0x0030
            int r4 = r2.size()     // Catch:{ all -> 0x0037 }
            java.util.ArrayList<km.home.ApplicationInfo> r12 = km.home.Home.mApplications     // Catch:{ all -> 0x0037 }
            r12.clear()     // Catch:{ all -> 0x0037 }
            r0 = 0
            r5 = 0
            r8 = 0
            r6 = 0
        L_0x002e:
            if (r6 < r4) goto L_0x003a
        L_0x0030:
            android.os.Handler r12 = r15.handler     // Catch:{ all -> 0x0037 }
            r13 = 1
            r12.sendEmptyMessage(r13)     // Catch:{ all -> 0x0037 }
            goto L_0x0007
        L_0x0037:
            r12 = move-exception
            monitor-exit(r15)
            throw r12
        L_0x003a:
            km.home.ApplicationInfo r1 = new km.home.ApplicationInfo     // Catch:{ all -> 0x0037 }
            r1.<init>()     // Catch:{ all -> 0x0037 }
            java.lang.Object r7 = r2.get(r6)     // Catch:{ all -> 0x0037 }
            android.content.pm.ResolveInfo r7 = (android.content.pm.ResolveInfo) r7     // Catch:{ all -> 0x0037 }
            java.lang.CharSequence r12 = r7.loadLabel(r11)     // Catch:{ all -> 0x0037 }
            r1.title = r12     // Catch:{ all -> 0x0037 }
            android.content.ComponentName r12 = new android.content.ComponentName     // Catch:{ all -> 0x0037 }
            android.content.pm.ActivityInfo r13 = r7.activityInfo     // Catch:{ all -> 0x0037 }
            android.content.pm.ApplicationInfo r13 = r13.applicationInfo     // Catch:{ all -> 0x0037 }
            java.lang.String r13 = r13.packageName     // Catch:{ all -> 0x0037 }
            android.content.pm.ActivityInfo r14 = r7.activityInfo     // Catch:{ all -> 0x0037 }
            java.lang.String r14 = r14.name     // Catch:{ all -> 0x0037 }
            r12.<init>(r13, r14)     // Catch:{ all -> 0x0037 }
            r13 = 270532608(0x10200000, float:3.1554436E-29)
            r1.setActivity(r12, r13)     // Catch:{ all -> 0x0037 }
            r5 = 0
            android.content.pm.ActivityInfo r12 = r7.activityInfo     // Catch:{ all -> 0x0037 }
            java.lang.String r8 = r12.name     // Catch:{ all -> 0x0037 }
            r9 = 0
        L_0x0065:
            km.home.config.Config r12 = r15.config     // Catch:{ all -> 0x0037 }
            java.util.Vector<km.home.config.Config$ConfigApp> r12 = r12.apps     // Catch:{ all -> 0x0037 }
            int r12 = r12.size()     // Catch:{ all -> 0x0037 }
            if (r9 < r12) goto L_0x008d
        L_0x006f:
            if (r5 != 0) goto L_0x0079
            android.content.pm.ActivityInfo r12 = r7.activityInfo     // Catch:{ all -> 0x0037 }
            android.graphics.drawable.Drawable r12 = r12.loadIcon(r11)     // Catch:{ all -> 0x0037 }
            r1.icon = r12     // Catch:{ all -> 0x0037 }
        L_0x0079:
            java.util.ArrayList<km.home.ApplicationInfo> r12 = km.home.Home.mApplications     // Catch:{ all -> 0x0037 }
            r12.add(r1)     // Catch:{ all -> 0x0037 }
            if (r6 == 0) goto L_0x008a
            int r12 = r6 % 6
            if (r12 != 0) goto L_0x008a
            android.os.Handler r12 = r15.handler     // Catch:{ all -> 0x0037 }
            r13 = 1
            r12.sendEmptyMessage(r13)     // Catch:{ all -> 0x0037 }
        L_0x008a:
            int r6 = r6 + 1
            goto L_0x002e
        L_0x008d:
            km.home.config.Config r12 = r15.config     // Catch:{ all -> 0x0037 }
            java.util.Vector<km.home.config.Config$ConfigApp> r12 = r12.apps     // Catch:{ all -> 0x0037 }
            java.lang.Object r0 = r12.elementAt(r9)     // Catch:{ all -> 0x0037 }
            km.home.config.Config$ConfigApp r0 = (km.home.config.Config.ConfigApp) r0     // Catch:{ all -> 0x0037 }
            java.lang.String r12 = r0.intent     // Catch:{ all -> 0x0037 }
            int r12 = r12.indexOf(r8)     // Catch:{ all -> 0x0037 }
            r13 = -1
            if (r12 == r13) goto L_0x00d0
            android.graphics.drawable.BitmapDrawable r3 = new android.graphics.drawable.BitmapDrawable     // Catch:{ all -> 0x0037 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x0037 }
            km.home.ThemeManager r13 = km.home.UnlockActivity.themeMgr     // Catch:{ all -> 0x0037 }
            km.home.config.Config r14 = r15.config     // Catch:{ all -> 0x0037 }
            java.lang.String r13 = r13.getCurrentThemePath(r14)     // Catch:{ all -> 0x0037 }
            java.lang.String r13 = java.lang.String.valueOf(r13)     // Catch:{ all -> 0x0037 }
            r12.<init>(r13)     // Catch:{ all -> 0x0037 }
            java.lang.String r13 = java.io.File.separator     // Catch:{ all -> 0x0037 }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ all -> 0x0037 }
            java.lang.String r13 = r0.icon     // Catch:{ all -> 0x0037 }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ all -> 0x0037 }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x0037 }
            r3.<init>(r12)     // Catch:{ all -> 0x0037 }
            android.graphics.Bitmap r12 = r3.getBitmap()     // Catch:{ all -> 0x0037 }
            if (r12 == 0) goto L_0x006f
            r1.icon = r3     // Catch:{ all -> 0x0037 }
            r5 = 1
            goto L_0x006f
        L_0x00d0:
            int r9 = r9 + 1
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: km.home.Home.loadApplications(boolean):void");
    }

    private class WallpaperIntentReceiver extends BroadcastReceiver {
        private WallpaperIntentReceiver() {
        }

        /* synthetic */ WallpaperIntentReceiver(Home home, WallpaperIntentReceiver wallpaperIntentReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            Home.this.getWindow().setBackgroundDrawable(Home.this.getWallpaper());
        }
    }

    private class ApplicationsIntentReceiver extends BroadcastReceiver {
        private ApplicationsIntentReceiver() {
        }

        /* synthetic */ ApplicationsIntentReceiver(Home home, ApplicationsIntentReceiver applicationsIntentReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            Home.this.loadApplications(false);
        }
    }

    private class ApplicationsAdapter extends ArrayAdapter<ApplicationInfo> {
        public ApplicationsAdapter(Context context, ArrayList<ApplicationInfo> apps) {
            super(context, 0, apps);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            ApplicationInfo info = (ApplicationInfo) Home.mApplications.get(position);
            if (convertView == null) {
                convertView = Home.this.getLayoutInflater().inflate((int) R.layout.new_application, parent, false);
            }
            ((ImageView) convertView.findViewById(R.id.icon)).setImageDrawable(info.icon);
            ((TextView) convertView.findViewById(R.id.label)).setText(info.title);
            return convertView;
        }
    }

    private class ApplicationLauncher implements AdapterView.OnItemClickListener {
        private ApplicationLauncher() {
        }

        /* synthetic */ ApplicationLauncher(Home home, ApplicationLauncher applicationLauncher) {
            this();
        }

        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
            Home.this.startActivity(((ApplicationInfo) parent.getItemAtPosition(position)).intent);
        }
    }

    private class ClippedDrawable extends Drawable {
        private final Drawable mWallpaper;

        public ClippedDrawable(Drawable wallpaper) {
            this.mWallpaper = wallpaper;
        }

        public void setBounds(int left, int top, int right, int bottom) {
            super.setBounds(left, top, right, bottom);
            this.mWallpaper.setBounds(left, top, this.mWallpaper.getIntrinsicWidth() + left, this.mWallpaper.getIntrinsicHeight() + top);
        }

        public void draw(Canvas canvas) {
            this.mWallpaper.draw(canvas);
        }

        public void setAlpha(int alpha) {
            this.mWallpaper.setAlpha(alpha);
        }

        public void setColorFilter(ColorFilter cf) {
            this.mWallpaper.setColorFilter(cf);
        }

        public int getOpacity() {
            return this.mWallpaper.getOpacity();
        }
    }
}
