package km.home;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import java.util.Calendar;
import km.home.ThemeManager;

public class UnlockActivity extends Activity implements View.OnClickListener, ThemeManager.ThemeSetupListener {
    private static final String[] MONTH_NAMES = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private static final String[] WEEK_NAMES = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    public static ThemeManager themeMgr;
    private boolean bTimeFormatIs24h;
    private IntentFilter filter;
    private LinearLayout llTopView;
    private BroadcastReceiver mIntentReceiver;
    private SharedPreferences sp;
    /* access modifiers changed from: private */
    public TextView tvDate;
    /* access modifiers changed from: private */
    public TextView tvTime;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Log.i("UnlockActivity", "onCreate");
        if (themeMgr == null) {
            themeMgr = new ThemeManager(this, this);
        }
        setDefaultKeyMode(3);
        getWindow().setFlags(1024, 1024);
        this.sp = getSharedPreferences("homesimple_pref", 0);
        this.bTimeFormatIs24h = this.sp.getBoolean("slidetounlock_is24hformat", true);
        this.filter = new IntentFilter("android.intent.action.TIME_TICK");
        this.filter.addAction("android.intent.action.TIME_SET");
        this.filter.addAction("android.intent.action.TIMEZONE_CHANGED");
    }

    private void init() {
        setContentView((int) R.layout.unlock);
        this.tvDate = (TextView) findViewById(R.id.unlock_date);
        this.tvTime = (TextView) findViewById(R.id.unlock_time);
        this.tvDate.setBackgroundColor(1426063360);
        this.tvDate.setTextColor(-285212673);
        this.tvTime.setBackgroundColor(1426063360);
        this.tvTime.setTextColor(-285212673);
        showTimeAndDate(this.tvTime, this.tvDate);
        this.llTopView = (LinearLayout) findViewById(R.id.unlock);
        this.llTopView.setBackgroundDrawable(getDefaultWallpaper());
        ((Button) findViewById(R.id.unlock_button)).setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent intent = new Intent(this, Home.class);
        intent.addFlags(536870912);
        startActivity(intent);
    }

    private Drawable getDefaultWallpaper() {
        Drawable wallpaper = peekWallpaper();
        if (wallpaper == null) {
            return getWallpaper();
        }
        return wallpaper;
    }

    private String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        int moth = calendar.get(2);
        return WEEK_NAMES[calendar.get(7) - 1] + ", " + MONTH_NAMES[moth] + " " + calendar.get(5);
    }

    private String getCurrentTime() {
        String s2;
        Calendar calendar = Calendar.getInstance();
        int i = calendar.get(11);
        int j = calendar.get(12);
        if (j < 10) {
            s2 = "0" + Integer.toString(j);
        } else {
            s2 = Integer.toString(j);
        }
        if (this.bTimeFormatIs24h) {
            return String.valueOf(Integer.toString(i)) + ":" + s2;
        }
        if (i < 12) {
            return String.valueOf(Integer.toString(i)) + ":" + s2 + " AM";
        }
        return String.valueOf(Integer.toString(i - 12)) + ":" + s2 + " PM";
    }

    /* access modifiers changed from: private */
    public void showTimeAndDate(TextView textview, TextView textview1) {
        Log.d("SlideToUnlock", "showTimeAndDate");
        String s = getCurrentTime();
        if (textview != null) {
            textview.setText(s);
        }
        String s1 = getCurrentDate();
        if (textview1 != null) {
            textview1.setText(s1);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyevent) {
        Log.d("SlideToUnlock", "dispatchKeyEvent");
        if (keyevent.getAction() == 0) {
            switch (keyevent.getKeyCode()) {
                case 4:
                    return false;
            }
        }
        return super.dispatchKeyEvent(keyevent);
    }

    public void onDestroy() {
        Log.d("SlideToUnlock", "onDestroy");
        super.onDestroy();
    }

    public void onPause() {
        Log.d("SlideToUnlock", "onPause");
        unregisterReceiver(this.mIntentReceiver);
        this.mIntentReceiver = null;
        super.onPause();
    }

    public void onRestart() {
        Log.d("SlideToUnlock", "onRestart");
        super.onRestart();
    }

    public void onResume() {
        Log.d("SlideToUnlock", "onResume");
        if (this.mIntentReceiver == null) {
            this.mIntentReceiver = new TimeReceiver(this, null);
        }
        registerReceiver(this.mIntentReceiver, this.filter);
        if (this.llTopView != null) {
            this.llTopView.setBackgroundDrawable(getDefaultWallpaper());
        }
        super.onResume();
    }

    public void onStart() {
        Log.d("SlideToUnlock", "onStart");
        super.onStart();
    }

    public void onStartTrackingTouch(SeekBar seekbar) {
        Log.d("SlideToUnlock", "onStartTrackingTouch");
    }

    public void onStop() {
        Log.d("SlideToUnlock", "onStop");
        super.onStop();
    }

    public void onStopTrackingTouch(SeekBar seekbar) {
        Log.d("SlideToUnlock", "onStopTrackingTouch");
    }

    private class TimeReceiver extends BroadcastReceiver {
        private TimeReceiver() {
        }

        /* synthetic */ TimeReceiver(UnlockActivity unlockActivity, TimeReceiver timeReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            Log.d("SlideToUnlock", "BroadcastReceiver onReceive");
            UnlockActivity.this.showTimeAndDate(UnlockActivity.this.tvTime, UnlockActivity.this.tvDate);
        }
    }

    public void finish() {
        themeMgr = null;
        Log.i("unlock", "finish");
        super.finish();
    }

    public void themeSetupAction(int state) {
        switch (state) {
            case 1:
                init();
                return;
            default:
                finish();
                return;
        }
    }
}
