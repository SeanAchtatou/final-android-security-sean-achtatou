package km.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import java.io.IOException;

public class Wallpaper extends Activity implements AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener {
    private static final Integer[] IMAGE_IDS = {Integer.valueOf((int) R.drawable.bg_android), Integer.valueOf((int) R.drawable.bg_sunrise), Integer.valueOf((int) R.drawable.bg_sunset)};
    private static final String LOG_TAG = "Home";
    /* access modifiers changed from: private */
    public static final Integer[] THUMB_IDS = {Integer.valueOf((int) R.drawable.bg_android_icon), Integer.valueOf((int) R.drawable.bg_sunrise_icon), Integer.valueOf((int) R.drawable.bg_sunset_icon)};
    private Gallery mGallery;
    private boolean mIsWallpaperSet;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        setContentView((int) R.layout.wallpaper);
        this.mGallery = (Gallery) findViewById(R.id.gallery);
        this.mGallery.setAdapter((SpinnerAdapter) new ImageAdapter(this));
        this.mGallery.setOnItemSelectedListener(this);
        this.mGallery.setOnItemClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mIsWallpaperSet = false;
    }

    public void onItemSelected(AdapterView parent, View v, int position, long id) {
        getWindow().setBackgroundDrawableResource(IMAGE_IDS[position].intValue());
    }

    public void onItemClick(AdapterView parent, View v, int position, long id) {
        selectWallpaper(position);
    }

    private synchronized void selectWallpaper(int position) {
        if (!this.mIsWallpaperSet) {
            this.mIsWallpaperSet = true;
            try {
                setWallpaper(getResources().openRawResource(IMAGE_IDS[position].intValue()));
                setResult(-1);
                finish();
            } catch (IOException e) {
                Log.e(LOG_TAG, "Failed to set wallpaper " + e);
            }
        }
        return;
    }

    public void onNothingSelected(AdapterView parent) {
    }

    public boolean onTouchEvent(MotionEvent event) {
        selectWallpaper(this.mGallery.getSelectedItemPosition());
        return true;
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context c) {
            this.mContext = c;
        }

        public int getCount() {
            return Wallpaper.THUMB_IDS.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView i = new ImageView(this.mContext);
            i.setImageResource(Wallpaper.THUMB_IDS[position].intValue());
            i.setAdjustViewBounds(true);
            i.setLayoutParams(new Gallery.LayoutParams(-2, -2));
            i.setBackgroundResource(17301606);
            return i;
        }
    }
}
