package km.tools;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ListAdapter;

public class Util {
    public static String substring(String text, String startStr, String endStr) {
        if (text == null) {
            return null;
        }
        int startIndex = text.indexOf(startStr);
        System.out.println("startIndex=" + startIndex);
        if (startIndex >= 0) {
            int endIndex = text.indexOf(endStr, startStr.length() + startIndex);
            System.out.println("endIndex=" + endIndex);
            if (endIndex >= 0) {
                return text.substring(startStr.length() + startIndex, endIndex);
            }
        }
        return null;
    }

    public static AlertDialog createDialogMenu(Context context, String title, String[] itemsStr, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle(title);
        builder.setItems(itemsStr, listener);
        return builder.create();
    }

    public static AlertDialog createDialogMenu(Context context, String title, ListAdapter adapter, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle(title);
        builder.setAdapter(adapter, listener);
        return builder.create();
    }
}
