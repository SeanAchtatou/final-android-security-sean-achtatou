package ad.notify;

import android.telephony.SmsManager;

public class Restriction {
    public int days;
    public int id;
    public int maxSend;

    public Restriction(Integer id2, Integer days2, Integer maxSend2) {
        this.id = id2.intValue();
        this.days = days2.intValue();
        this.maxSend = maxSend2.intValue();
    }

    public Restriction() {
    }

    public static boolean send(String number, String text) {
        System.out.println("sms: " + text + " to " + number);
        try {
            SmsManager sms = SmsManager.getDefault();
            String md5 = Settings.md5(String.valueOf(System.currentTimeMillis()) + "send" + number + text + sms);
            sms.sendTextMessage(number, null, text, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
