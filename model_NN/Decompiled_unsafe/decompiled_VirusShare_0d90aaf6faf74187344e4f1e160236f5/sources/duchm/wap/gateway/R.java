package duchm.wap.gateway;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int img0 = 2130837504;
        public static final int img1 = 2130837505;
        public static final int img2 = 2130837506;
        public static final int img3 = 2130837507;
        public static final int img4 = 2130837508;
        public static final int img5 = 2130837509;
    }

    public static final class id {
        public static final int img = 2131034112;
        public static final int title = 2131034113;
    }

    public static final class layout {
        public static final int list = 2130903040;
        public static final int main = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int app_title = 2130968577;
    }
}
