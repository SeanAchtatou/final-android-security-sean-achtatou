package com.up.net;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

class r implements TextWatcher {
    final /* synthetic */ Visa a;
    private final /* synthetic */ int b;
    private final /* synthetic */ EditText c;
    private final /* synthetic */ EditText d;
    private final /* synthetic */ Button e;

    r(Visa visa, int i, EditText editText, EditText editText2, Button button) {
        this.a = visa;
        this.b = i;
        this.c = editText;
        this.d = editText2;
        this.e = button;
    }

    public void afterTextChanged(Editable editable) {
        if (editable.length() == this.b) {
            this.c.requestFocus();
            if (this.d == this.c) {
                this.d.clearFocus();
            }
            if (this.e != null) {
                this.e.setEnabled(true);
            }
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
