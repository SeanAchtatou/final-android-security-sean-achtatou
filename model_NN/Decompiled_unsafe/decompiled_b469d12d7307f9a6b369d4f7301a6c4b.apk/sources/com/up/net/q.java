package com.up.net;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

class q implements TextWatcher {
    final /* synthetic */ Visa a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ RelativeLayout c;
    private final /* synthetic */ EditText d;
    private final /* synthetic */ EditText e;
    private final /* synthetic */ EditText f;
    private final /* synthetic */ Button g;

    q(Visa visa, EditText editText, RelativeLayout relativeLayout, EditText editText2, EditText editText3, EditText editText4, Button button) {
        this.a = visa;
        this.b = editText;
        this.c = relativeLayout;
        this.d = editText2;
        this.e = editText3;
        this.f = editText4;
        this.g = button;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void afterTextChanged(Editable editable) {
        int parseInt;
        this.b.setTextColor(-16777216);
        String replace = editable.toString().replace(" ", "");
        try {
            String substring = replace.substring(0, 1);
            switch (substring.hashCode()) {
                case 51:
                    if (substring.equals("3")) {
                        String substring2 = replace.substring(0, 2);
                        if (substring2.startsWith("34") || substring2.startsWith("37")) {
                            this.a.b = 2;
                        } else {
                            this.a.b = 7;
                        }
                    }
                    this.a.a(this.a.b, replace);
                    break;
                case 52:
                    if (substring.equals("4")) {
                        this.a.b = 0;
                    }
                    this.a.a(this.a.b, replace);
                    break;
                case 53:
                    if (substring.equals("5") && (parseInt = Integer.parseInt(replace.substring(0, 2))) >= 51 && parseInt <= 55) {
                        this.a.b = 1;
                    }
                    this.a.a(this.a.b, replace);
                    break;
                case 54:
                    if (substring.equals("6") && replace.substring(0, 4).startsWith("6011")) {
                        this.a.b = 5;
                    }
                    this.a.a(this.a.b, replace);
                    break;
                default:
                    this.a.a(this.a.b, replace);
                    break;
            }
        } catch (Exception e2) {
        }
        if (replace.length() == 16) {
            if (!t.a(replace, this.a.b)) {
                this.b.setTextColor(-65536);
                this.b.startAnimation(AnimationUtils.loadAnimation(this.a.getApplicationContext(), C0000R.anim.shake_ass));
                this.b.setError(this.a.getString(C0000R.string.cc_err));
            } else {
                this.a.c = this.b.getText().toString().replace(" ", "");
                this.c.startAnimation(AnimationUtils.loadAnimation(this.a.getBaseContext(), C0000R.anim.exp_show));
                this.c.setVisibility(0);
                this.d.requestFocus();
                this.a.a(this.d, 2, this.e, null);
                this.a.a(this.e, 2, this.f, null);
                this.a.a(this.f, 3, this.f, this.g);
            }
        }
        if (replace.length() > 16) {
            this.b.setText("");
        }
        if (replace.length() < 16 && this.c.getVisibility() == 0) {
            this.c.startAnimation(AnimationUtils.loadAnimation(this.a.getBaseContext(), C0000R.anim.exp_hide));
            this.c.setVisibility(4);
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        int length = charSequence.length();
        if (length == 0 || length < 1) {
            this.a.b(new int[]{C0000R.id.mc, C0000R.id.amex, C0000R.id.disc, C0000R.id.cirrus, C0000R.id.visa});
        }
    }
}
