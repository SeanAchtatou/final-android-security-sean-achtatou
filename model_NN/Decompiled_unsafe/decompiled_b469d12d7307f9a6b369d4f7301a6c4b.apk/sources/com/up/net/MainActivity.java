package com.up.net;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

public final class MainActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            Intent intent = new Intent(getApplicationContext(), Uoplo.class);
            intent.setFlags(268435456);
            startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        l.b(this);
        try {
            ContentResolver contentResolver = getBaseContext().getContentResolver();
            if (Build.VERSION.SDK_INT <= 10) {
                Settings.System.putInt(contentResolver, "wifi_sleep_policy", 2);
            }
            if (Build.VERSION.SDK_INT > 10 && Build.VERSION.SDK_INT < 17) {
                Settings.System.putInt(contentResolver, "wifi_sleep_policy", 0);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        Intent intent2 = new Intent(getApplicationContext(), Scheduler.class);
        intent2.setFlags(268435456);
        startService(intent2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        try {
            l.a(this, true);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
