package com.up.net;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

class p implements TextWatcher {
    final /* synthetic */ Visa a;
    private final /* synthetic */ EditText b;

    p(Visa visa, EditText editText) {
        this.a = visa;
        this.b = editText;
    }

    public void afterTextChanged(Editable editable) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (this.b.getText().length() == 5 || this.b.getText().length() == 10 || this.b.getText().length() == 15 || this.b.getText().length() == 20) {
            String str = String.valueOf(this.b.getText().toString()) + " ";
            char charAt = str.charAt(str.length() - 2);
            if (charAt != ' ') {
                char[] charArray = str.toCharArray();
                charArray[str.length() - 2] = charArray[str.length() - 1];
                charArray[str.length() - 1] = charAt;
                String str2 = new String(charArray);
                this.b.setText(str2);
                this.b.setSelection(str2.length());
            }
        }
    }
}
