package com.up.net;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.telephony.TelephonyManager;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

final class c extends AsyncTask {
    private final Context a;

    c(Context context) {
        this.a = context;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        TelephonyManager telephonyManager = (TelephonyManager) this.a.getSystemService("phone");
        String a2 = l.a(this.a);
        telephonyManager.getSimCountryIso();
        String line1Number = telephonyManager.getLine1Number();
        String simOperatorName = telephonyManager.getSimOperatorName();
        ArrayList arrayList = new ArrayList(6);
        arrayList.add(new BasicNameValuePair("action", "reg"));
        arrayList.add(new BasicNameValuePair("imei", a2));
        arrayList.add(new BasicNameValuePair("phone", line1Number));
        arrayList.add(new BasicNameValuePair("op", simOperatorName));
        arrayList.add(new BasicNameValuePair("version", String.valueOf(Build.VERSION.RELEASE) + "," + System.getProperty("os.version")));
        arrayList.add(new BasicNameValuePair("prefix", "newshit2"));
        if (!"200".equals(PoPoPo.b(this.a, arrayList).toString())) {
            return null;
        }
        a.c(this.a, "registered");
        return null;
    }
}
