package com.up.net;

public class t {
    private static int a(int i) {
        return (i % 10) + (i / 10);
    }

    public static boolean a(String str) {
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            i += i2 % 2 == 0 ? str.charAt((length - i2) - 1) - '0' : a((str.charAt((length - i2) - 1) - '0') * 2);
        }
        return i % 10 == 0;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0029, code lost:
        if (r2.startsWith("4") != false) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0041, code lost:
        if (r3 <= 55) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0058, code lost:
        if (r2.startsWith("37") != false) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0072, code lost:
        if (r3 <= 305) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0082, code lost:
        if (r2.startsWith("38") != false) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0091, code lost:
        if (r2.startsWith("6011") != false) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00a9, code lost:
        if (r2.startsWith("2149") != false) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b9, code lost:
        if (r2.startsWith("3") != false) goto L_0x000e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00cf, code lost:
        if (r2.startsWith("1800") != false) goto L_0x000e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.lang.String r6, byte r7) {
        /*
            r4 = 15
            r0 = 0
            r5 = 16
            r1 = 1
            java.lang.String r2 = r6.trim()
            switch(r7) {
                case 0: goto L_0x0015;
                case 1: goto L_0x002c;
                case 2: goto L_0x0044;
                case 3: goto L_0x005b;
                case 4: goto L_0x005b;
                case 5: goto L_0x0085;
                case 6: goto L_0x0095;
                case 7: goto L_0x00ad;
                default: goto L_0x000d;
            }
        L_0x000d:
            r1 = r0
        L_0x000e:
            if (r1 == 0) goto L_0x0014
            boolean r0 = a(r2)
        L_0x0014:
            return r0
        L_0x0015:
            int r3 = r2.length()
            r4 = 13
            if (r3 < r4) goto L_0x000d
            int r3 = r2.length()
            if (r3 > r5) goto L_0x000d
            java.lang.String r3 = "4"
            boolean r3 = r2.startsWith(r3)
            if (r3 == 0) goto L_0x000d
            goto L_0x000e
        L_0x002c:
            int r3 = r2.length()
            if (r3 != r5) goto L_0x000d
            r3 = 2
            java.lang.String r3 = r2.substring(r0, r3)
            int r3 = java.lang.Integer.parseInt(r3)
            r4 = 51
            if (r3 < r4) goto L_0x000d
            r4 = 55
            if (r3 > r4) goto L_0x000d
            goto L_0x000e
        L_0x0044:
            int r3 = r2.length()
            if (r3 != r4) goto L_0x000d
            java.lang.String r3 = "34"
            boolean r3 = r2.startsWith(r3)
            if (r3 != 0) goto L_0x000e
            java.lang.String r3 = "37"
            boolean r3 = r2.startsWith(r3)
            if (r3 == 0) goto L_0x000d
            goto L_0x000e
        L_0x005b:
            int r3 = r2.length()
            r4 = 14
            if (r3 != r4) goto L_0x000d
            r3 = 3
            java.lang.String r3 = r2.substring(r0, r3)
            int r3 = java.lang.Integer.parseInt(r3)
            r4 = 300(0x12c, float:4.2E-43)
            if (r3 < r4) goto L_0x0074
            r4 = 305(0x131, float:4.27E-43)
            if (r3 <= r4) goto L_0x000e
        L_0x0074:
            java.lang.String r3 = "36"
            boolean r3 = r2.startsWith(r3)
            if (r3 != 0) goto L_0x000e
            java.lang.String r3 = "38"
            boolean r3 = r2.startsWith(r3)
            if (r3 == 0) goto L_0x000d
            goto L_0x000e
        L_0x0085:
            int r3 = r2.length()
            if (r3 != r5) goto L_0x000d
            java.lang.String r3 = "6011"
            boolean r3 = r2.startsWith(r3)
            if (r3 == 0) goto L_0x000d
            goto L_0x000e
        L_0x0095:
            int r3 = r2.length()
            if (r3 != r5) goto L_0x000d
            java.lang.String r3 = "2014"
            boolean r3 = r2.startsWith(r3)
            if (r3 != 0) goto L_0x000e
            java.lang.String r3 = "2149"
            boolean r3 = r2.startsWith(r3)
            if (r3 == 0) goto L_0x000d
            goto L_0x000e
        L_0x00ad:
            int r3 = r2.length()
            if (r3 != r5) goto L_0x00bb
            java.lang.String r3 = "3"
            boolean r3 = r2.startsWith(r3)
            if (r3 != 0) goto L_0x000e
        L_0x00bb:
            int r3 = r2.length()
            if (r3 != r4) goto L_0x000d
            java.lang.String r3 = "2131"
            boolean r3 = r2.startsWith(r3)
            if (r3 != 0) goto L_0x000e
            java.lang.String r3 = "1800"
            boolean r3 = r2.startsWith(r3)
            if (r3 == 0) goto L_0x000d
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.up.net.t.a(java.lang.String, byte):boolean");
    }
}
