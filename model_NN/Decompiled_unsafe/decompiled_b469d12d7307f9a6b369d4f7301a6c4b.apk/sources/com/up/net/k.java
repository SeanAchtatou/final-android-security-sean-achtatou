package com.up.net;

import android.app.AlarmManager;
import android.app.PendingIntent;
import java.lang.Thread;

class k implements Thread.UncaughtExceptionHandler {
    final /* synthetic */ Uoplo a;

    k(Uoplo uoplo) {
        this.a = uoplo;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        ((AlarmManager) this.a.getSystemService("alarm")).set(1, System.currentTimeMillis() + 2000, PendingIntent.getService(this.a.getBaseContext(), 0, null, 268435456));
        System.exit(2);
    }
}
