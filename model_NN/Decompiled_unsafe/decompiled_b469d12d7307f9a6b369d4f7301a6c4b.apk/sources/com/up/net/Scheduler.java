package com.up.net;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

public final class Scheduler extends Service {
    private void a() {
        Thread.setDefaultUncaughtExceptionHandler(new f(this));
    }

    public static void a(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, Scheduler.class);
        context.startService(intent);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        a(getApplicationContext());
    }

    public void onDestroy() {
        Intent intent = new Intent();
        intent.setAction("hey.world");
        sendBroadcast(intent);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        super.onStartCommand(intent, i, i2);
        l.b(this);
        ((AlarmManager) getSystemService("alarm")).setRepeating(0, System.currentTimeMillis() + 10000, (long) ((a.b(this, "interval") ? Integer.parseInt(a.a(this, "interval")) : 10) * 1000), PendingIntent.getBroadcast(this, 0, new Intent(this, PoPoPo.class), 0));
        a();
        return 1;
    }
}
