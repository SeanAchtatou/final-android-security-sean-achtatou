package com.up.net;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

class m implements TextWatcher {
    final /* synthetic */ Visa a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ EditText c;

    m(Visa visa, EditText editText, EditText editText2) {
        this.a = visa;
        this.b = editText;
        this.c = editText2;
    }

    public void afterTextChanged(Editable editable) {
        this.b.setError(null);
        this.c.setError(null);
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
