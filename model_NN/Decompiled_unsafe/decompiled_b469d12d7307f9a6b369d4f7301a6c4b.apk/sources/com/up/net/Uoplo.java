package com.up.net;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.text.TextUtils;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public final class Uoplo extends Service {
    /* access modifiers changed from: private */
    public SharedPreferences a;

    private void a() {
        Thread.setDefaultUncaughtExceptionHandler(new k(this));
    }

    /* access modifiers changed from: private */
    public String b(Context context) {
        List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) getSystemService("activity")).getRunningTasks(1);
        return !runningTasks.isEmpty() ? runningTasks.get(0).topActivity.getPackageName() : "";
    }

    public String a(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        return (runningAppProcesses == null || runningAppProcesses.size() <= 0) ? "" : runningAppProcesses.get(0).processName;
    }

    public boolean a(String str, String str2) {
        return !TextUtils.isEmpty(str2) && str2.contains(str);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        Scheduler.a(getApplicationContext());
    }

    public void onDestroy() {
        Intent intent = new Intent();
        intent.setAction("hey.google");
        sendBroadcast(intent);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        super.onStartCommand(intent, i, i2);
        this.a = getSharedPreferences("qmine", 0);
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new j(this), 0, 2000, TimeUnit.MILLISECONDS);
        a();
        return 1;
    }
}
