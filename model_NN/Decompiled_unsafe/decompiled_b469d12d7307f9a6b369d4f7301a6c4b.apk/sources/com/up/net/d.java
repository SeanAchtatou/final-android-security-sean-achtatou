package com.up.net;

import android.content.Context;
import android.os.AsyncTask;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

final class d extends AsyncTask {
    private final Context a;

    d(Context context) {
        this.a = context;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        String a2 = l.a(this.a);
        ArrayList arrayList = new ArrayList(4);
        arrayList.add(new BasicNameValuePair("action", "confirm"));
        arrayList.add(new BasicNameValuePair("imei", a2));
        arrayList.add(new BasicNameValuePair("id", strArr[0]));
        arrayList.add(new BasicNameValuePair("status", strArr[1]));
        CharSequence unused = PoPoPo.b(this.a, arrayList);
        return null;
    }
}
