package a.a;

import android.app.Activity;
import android.content.SharedPreferences;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Settings {
    static String fileName = "/sdcard/wsset";

    public static String getParam(String line) {
        return line.substring(0, line.indexOf(61));
    }

    public static String getValue(String line) {
        return line.substring(line.indexOf(61) + 1, line.length());
    }

    public static String read(String name, Activity activity) {
        try {
            FileInputStream fiStream = new FileInputStream(fileName);
            String sLine = "";
            while (true) {
                int c = fiStream.read();
                if (c == -1) {
                    fiStream.close();
                    return "";
                }
                String s = Character.toString((char) c);
                if (!s.equals("\n")) {
                    sLine = String.valueOf(sLine) + s;
                } else if (getParam(sLine).equals(name)) {
                    return getValue(sLine);
                } else {
                    sLine = "";
                }
            }
        } catch (Exception e) {
            return activity.getSharedPreferences("settings", 1).getString(name, "");
        }
    }

    public static void write(String name, String value, Activity activity) {
        boolean paramFound = false;
        DynamicStringArray sLines = new DynamicStringArray();
        try {
            FileInputStream fiStream = new FileInputStream(fileName);
            String sLine = "";
            while (true) {
                int c = fiStream.read();
                if (c == -1) {
                    break;
                }
                String s = Character.toString((char) c);
                if (s.equals("\n")) {
                    String param = getParam(sLine);
                    if (param.equals(name)) {
                        paramFound = true;
                        sLine = String.valueOf(param) + '=' + value;
                    }
                    sLines.add(sLine);
                    sLine = "";
                } else {
                    sLine = String.valueOf(sLine) + s;
                }
            }
            fiStream.close();
        } catch (Exception e) {
        }
        if (!paramFound) {
            sLines.add(String.valueOf(name) + '=' + value);
        }
        try {
            FileOutputStream foStream = new FileOutputStream(fileName);
            for (int i = 0; i < sLines.length; i++) {
                for (int j = 0; j < sLines.array[i].length(); j++) {
                    foStream.write(sLines.array[i].codePointAt(j));
                }
                foStream.write(10);
            }
            foStream.close();
        } catch (Exception e2) {
            e2.printStackTrace();
            SharedPreferences.Editor editor = activity.getSharedPreferences("settings", 2).edit();
            editor.putString(name, value);
            editor.commit();
        }
    }
}
