package a.a;

public class DynamicByteArray {
    private static final int CAPACITY_INCREMENT = 10;
    private static final int INITIAL_CAPACITY = 10;
    public byte[] array;
    private final int capacityIncrement;
    public int length;

    public DynamicByteArray(int initialCapacity, int capacityIncrement2) {
        this.length = 0;
        this.capacityIncrement = capacityIncrement2;
        this.array = new byte[initialCapacity];
    }

    public DynamicByteArray() {
        this(10, 10);
    }

    public int add(byte i) {
        int offset = this.length;
        if (offset == this.array.length) {
            byte[] old = this.array;
            this.array = new byte[(this.capacityIncrement + offset)];
            System.arraycopy(old, 0, this.array, 0, offset);
        }
        byte[] bArr = this.array;
        int i2 = this.length;
        this.length = i2 + 1;
        bArr[i2] = i;
        return offset;
    }

    public void removeAt(int offset) {
        if (offset >= this.length) {
            throw new ArrayIndexOutOfBoundsException("offset too big");
        } else if (offset < this.length) {
            System.arraycopy(this.array, offset + 1, this.array, offset, (this.length - offset) - 1);
            this.length--;
        }
    }
}
