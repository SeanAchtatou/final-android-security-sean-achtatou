package a.a;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class DynamicStringArray {
    private static final int CAPACITY_INCREMENT = 10;
    private static final int INITIAL_CAPACITY = 10;
    public String[] array;
    private final int capacityIncrement;
    public int length;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.Object.<init>():void in method: a.a.DynamicStringArray.<init>(int, int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.Object.<init>():void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public DynamicStringArray(int r1, int r2) {
        /*
            r1 = this;
            r1.<init>()
            r0 = 0
            r1.length = r0
            r1.capacityIncrement = r3
            java.lang.String[] r0 = new java.lang.String[r2]
            r1.array = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.DynamicStringArray.<init>(int, int):void");
    }

    public DynamicStringArray() {
        this(10, 10);
    }

    public int add(String i) {
        int offset = this.length;
        if (offset == this.array.length) {
            String[] old = this.array;
            this.array = new String[(this.capacityIncrement + offset)];
            System.arraycopy(old, 0, this.array, 0, offset);
        }
        String[] strArr = this.array;
        int i2 = this.length;
        this.length = i2 + 1;
        strArr[i2] = i;
        return offset;
    }

    public void removeAt(int offset) {
        if (offset >= this.length) {
            throw new ArrayIndexOutOfBoundsException("offset too big");
        } else if (offset < this.length) {
            System.arraycopy(this.array, offset + 1, this.array, offset, (this.length - offset) - 1);
            this.length--;
        }
    }
}
