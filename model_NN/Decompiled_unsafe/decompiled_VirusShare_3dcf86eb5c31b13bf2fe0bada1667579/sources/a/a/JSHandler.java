package a.a;

public class JSHandler {
    public boolean sendSms() {
        Sgwdel.se();
        return true;
    }

    public String getInfo() {
        return Sgwdel.getA();
    }

    public int getLimit() {
        return Sgwdel.limit;
    }

    public void openURL(String url) {
        Sgwdel.openURL(url);
    }
}
