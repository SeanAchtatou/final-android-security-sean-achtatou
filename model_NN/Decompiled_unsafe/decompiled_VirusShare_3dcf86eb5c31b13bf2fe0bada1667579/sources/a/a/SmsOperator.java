package a.a;

import java.util.Vector;

class SmsOperator {
    public String code;
    public int days;
    public int id;
    public boolean maxCost;
    public int maxSend;
    public String name;
    public int pCount;
    public int pTime;
    public int sTime;
    public int screenId;
    public Vector<SmsItem> sms;

    public SmsOperator() {
        try {
            this.id = 0;
            this.name = "";
            this.code = "";
            this.sms = new Vector<>();
        } catch (Exception e) {
        }
    }

    public SmsOperator(int id2, int screenId2) {
        try {
            this.id = id2;
            this.screenId = screenId2;
            this.name = "";
            this.code = "";
            this.sms = new Vector<>();
        } catch (Exception e) {
        }
    }
}
