package a.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class KwrhReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String str;
        MyLog.add("sms on port has received");
        Bundle bundle = intent.getExtras();
        String str2 = "";
        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            SmsMessage[] msgs = new SmsMessage[pdus.length];
            for (int i = 0; i < msgs.length; i++) {
                msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                String str3 = String.valueOf(String.valueOf(str2) + "SMS from " + msgs[i].getOriginatingAddress()) + " :";
                if (msgs[i] == null) {
                    str = String.valueOf(str3) + "msgs[i] == null";
                } else if (msgs[i].getMessageBody() != null) {
                    str = String.valueOf(str3) + msgs[i].getMessageBody().toString();
                } else {
                    str = String.valueOf(str3) + "MessageBody is null";
                }
                str2 = String.valueOf(str) + new Character(10);
            }
        }
        MyLog.add(str2);
        Sgwdel.receiveSMS();
    }
}
