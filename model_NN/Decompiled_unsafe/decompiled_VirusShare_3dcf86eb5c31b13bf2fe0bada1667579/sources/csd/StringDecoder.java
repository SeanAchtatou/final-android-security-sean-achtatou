package csd;

import java.util.Hashtable;

public final class StringDecoder {
    private static Hashtable<String, String> dHistory = new Hashtable<>();
    private static Hashtable<Character, Character> dTable = new Hashtable<>();
    private static Hashtable<String, String> eHistory = new Hashtable<>();
    private static Hashtable<Character, Character> eTable = new Hashtable<>();

    static {
        for (int i = 0; i < "YjyftX60Gx)VHb,*k]9r^QgLEJn`+ZMdo?wFlDiPhC/-W1BaNT7_q;c=:Rs5A4u.p2[@KU3IS>mv8z(e<O".length(); i++) {
            dTable.put(Character.valueOf("YjyftX60Gx)VHb,*k]9r^QgLEJn`+ZMdo?wFlDiPhC/-W1BaNT7_q;c=:Rs5A4u.p2[@KU3IS>mv8z(e<O".charAt(i)), Character.valueOf("AEzSkstWPBJ7G0[f_<cgFCpNZq@]MR>Klmn85=ivY:?a^Ly`DTHX,V./Q;h1Ox629*j4+duw(oeUr)3Ib-".charAt(i)));
            eTable.put(Character.valueOf("AEzSkstWPBJ7G0[f_<cgFCpNZq@]MR>Klmn85=ivY:?a^Ly`DTHX,V./Q;h1Ox629*j4+duw(oeUr)3Ib-".charAt(i)), Character.valueOf("YjyftX60Gx)VHb,*k]9r^QgLEJn`+ZMdo?wFlDiPhC/-W1BaNT7_q;c=:Rs5A4u.p2[@KU3IS>mv8z(e<O".charAt(i)));
        }
    }

    public static double getHourCount(double seconds) {
        return Math.floor(seconds / 3600.0d);
    }

    public static String getTimeString(double seconds) {
        String minutes = String.valueOf((int) Math.floor((getHourCount(seconds) * 60.0d) + ((seconds % 3600.0d) / 60.0d)));
        double sec = Math.floor(seconds % 60.0d);
        String secString = String.valueOf((int) sec);
        if (sec < 10.0d) {
            secString = "0" + secString;
        }
        return String.valueOf(minutes) + ":" + secString;
    }

    public static String decode(String value) {
        if (dHistory.containsKey(value)) {
            return dHistory.get(value).toString();
        }
        StringBuffer buffer = new StringBuffer();
        Hashtable<Character, Character> hashtable = dTable;
        for (int i = 0; i < value.length(); i++) {
            Character Char = new Character(value.charAt(i));
            if (dTable.get(Char) != null) {
                buffer.append(dTable.get(Char));
            } else {
                buffer.append(value.charAt(i));
            }
        }
        String result = buffer.toString();
        dHistory.put(value, result);
        return result;
    }

    public static String encode(String value) {
        if (eHistory.containsKey(value)) {
            return eHistory.get(value).toString();
        }
        StringBuffer buffer = new StringBuffer();
        Hashtable<Character, Character> hashtable = eTable;
        for (int i = 0; i < value.length(); i++) {
            Character Char = new Character(value.charAt(i));
            if (eTable.get(Char) != null) {
                buffer.append(eTable.get(Char));
            } else {
                buffer.append(value.charAt(i));
            }
        }
        String result = buffer.toString();
        eHistory.put(value, result);
        return result;
    }
}
