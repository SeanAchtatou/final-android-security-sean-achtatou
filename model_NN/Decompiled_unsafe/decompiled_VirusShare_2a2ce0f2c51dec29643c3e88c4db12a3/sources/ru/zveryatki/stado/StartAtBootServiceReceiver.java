package ru.zveryatki.stado;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartAtBootServiceReceiver extends BroadcastReceiver {
    private static final String SERVICE_CLASS = "ru.zveryatki.stado.NewsReaderService";

    public void onReceive(Context context, Intent intent) {
        System.out.println("Get broadcast intent " + intent);
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            System.out.println("!!!Start Service!!!");
            Intent serviceIntent = new Intent();
            serviceIntent.setAction(SERVICE_CLASS);
            context.startService(serviceIntent);
        } else if (!isMyServiceRunning(context, SERVICE_CLASS)) {
            System.out.println("!!!Start Service Again!!!");
            Intent serviceIntent2 = new Intent();
            serviceIntent2.setAction(SERVICE_CLASS);
            context.startService(serviceIntent2);
        }
    }

    private boolean isMyServiceRunning(Context context, String fullName) {
        for (ActivityManager.RunningServiceInfo service : ((ActivityManager) context.getSystemService("activity")).getRunningServices(Integer.MAX_VALUE)) {
            if (fullName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
