package com.safesys.myvpn2;

import android.os.SystemClock;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

class af {
    Process a = null;
    DataOutputStream b = null;
    DataInputStream c = null;
    DataInputStream d = null;
    final /* synthetic */ VpnSettings e;

    af(VpnSettings vpnSettings) {
        this.e = vpnSettings;
    }

    public void a(String str, int i) {
        try {
            if (this.b != null) {
                this.b.writeBytes(str);
                this.b.writeBytes("\n");
                this.b.flush();
                SystemClock.sleep((long) i);
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public boolean a() {
        try {
            this.a = Runtime.getRuntime().exec("su");
            this.b = new DataOutputStream(this.a.getOutputStream());
            this.c = new DataInputStream(this.a.getInputStream());
            this.d = new DataInputStream(this.a.getErrorStream());
            String str = this.e.getFilesDir() + "/.ttest";
            this.b.writeBytes("/system/bin/ls > " + str + "\n");
            this.b.flush();
            SystemClock.sleep(1000);
            File file = new File(str);
            if (file.length() <= 0) {
                return false;
            }
            file.delete();
            return true;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public void b() {
        try {
            if (this.a != null) {
                this.a.waitFor();
            }
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c() {
        /*
            r2 = this;
            java.io.DataOutputStream r0 = r2.b
            if (r0 == 0) goto L_0x0033
            java.io.DataOutputStream r0 = r2.b     // Catch:{ IOException -> 0x0039 }
            java.lang.String r1 = "exit\n"
            r0.writeBytes(r1)     // Catch:{ IOException -> 0x0039 }
            java.io.DataOutputStream r0 = r2.b     // Catch:{ IOException -> 0x0039 }
            r0.flush()     // Catch:{ IOException -> 0x0039 }
            r2.b()     // Catch:{ IOException -> 0x0039 }
            java.io.DataInputStream r0 = r2.c     // Catch:{ Exception -> 0x0034, all -> 0x003e }
            r0.close()     // Catch:{ Exception -> 0x0034, all -> 0x003e }
            r0 = 0
            r2.c = r0     // Catch:{ IOException -> 0x0039 }
        L_0x001b:
            java.io.DataInputStream r0 = r2.d     // Catch:{ Exception -> 0x0043, all -> 0x0048 }
            r0.close()     // Catch:{ Exception -> 0x0043, all -> 0x0048 }
            r0 = 0
            r2.d = r0     // Catch:{ IOException -> 0x0039 }
        L_0x0023:
            java.io.DataOutputStream r0 = r2.b     // Catch:{ Exception -> 0x004d, all -> 0x0052 }
            r0.close()     // Catch:{ Exception -> 0x004d, all -> 0x0052 }
            r0 = 0
            r2.b = r0     // Catch:{ IOException -> 0x0039 }
        L_0x002b:
            r0 = 0
            r2.a = r0     // Catch:{ IOException -> 0x0039 }
            java.lang.Process r0 = r2.a     // Catch:{ IOException -> 0x0039 }
            r0.destroy()     // Catch:{ IOException -> 0x0039 }
        L_0x0033:
            return
        L_0x0034:
            r0 = move-exception
            r0 = 0
            r2.c = r0     // Catch:{ IOException -> 0x0039 }
            goto L_0x001b
        L_0x0039:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0033
        L_0x003e:
            r0 = move-exception
            r1 = 0
            r2.c = r1     // Catch:{ IOException -> 0x0039 }
            throw r0     // Catch:{ IOException -> 0x0039 }
        L_0x0043:
            r0 = move-exception
            r0 = 0
            r2.d = r0     // Catch:{ IOException -> 0x0039 }
            goto L_0x0023
        L_0x0048:
            r0 = move-exception
            r1 = 0
            r2.d = r1     // Catch:{ IOException -> 0x0039 }
            throw r0     // Catch:{ IOException -> 0x0039 }
        L_0x004d:
            r0 = move-exception
            r0 = 0
            r2.b = r0     // Catch:{ IOException -> 0x0039 }
            goto L_0x002b
        L_0x0052:
            r0 = move-exception
            r1 = 0
            r2.b = r1     // Catch:{ IOException -> 0x0039 }
            throw r0     // Catch:{ IOException -> 0x0039 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn2.af.c():void");
    }
}
