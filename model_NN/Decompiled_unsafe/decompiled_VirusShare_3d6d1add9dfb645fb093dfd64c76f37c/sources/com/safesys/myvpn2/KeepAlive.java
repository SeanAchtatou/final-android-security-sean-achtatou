package com.safesys.myvpn2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import com.safesys.myvpn2.a.h;
import java.util.Timer;

public final class KeepAlive extends BroadcastReceiver {
    /* access modifiers changed from: private */
    public static final String a = KeepAlive.class.getName();
    private static Timer b = new Timer("com.safesys.myvpn.HeartbeatTimer", true);
    private static z c;
    private static /* synthetic */ int[] d;

    private static void a(SharedPreferences sharedPreferences) {
        if (sharedPreferences.getBoolean("com.safesys.myvpn.pref.keepAlive", true) && c == null) {
            int b2 = b(sharedPreferences);
            Log.d(a, "start heartbeat every (ms)" + b2);
            c = new z(null);
            b.schedule(c, (long) b2, (long) b2);
        }
    }

    private void a(String str, h hVar, SharedPreferences sharedPreferences) {
        Log.d(a, String.valueOf(str) + " state ==> " + hVar);
        switch (b()[hVar.ordinal()]) {
            case 4:
                a(sharedPreferences);
                return;
            case 5:
                c();
                return;
            default:
                return;
        }
    }

    private static int b(SharedPreferences sharedPreferences) {
        return am.a(am.valueOf(sharedPreferences.getString("com.safesys.myvpn.pref.keepAlive.period", am.TEN_MIN.toString())));
    }

    static /* synthetic */ int[] b() {
        int[] iArr = d;
        if (iArr == null) {
            iArr = new int[h.values().length];
            try {
                iArr[h.CANCELLED.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[h.CONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[h.CONNECTING.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[h.DISCONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[h.IDLE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[h.UNKNOWN.ordinal()] = 7;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[h.UNUSABLE.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            d = iArr;
        }
        return iArr;
    }

    private static synchronized void c() {
        synchronized (KeepAlive.class) {
            if (c != null) {
                c.cancel();
                Log.d(a, "removed heartbeat timerTask: " + b.purge());
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        if ("vpn.connectivity".equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("profile_name");
            if (!stringExtra.equals(ao.a(context))) {
                Log.d(a, "ignores non-active profile event: " + stringExtra);
                return;
            }
            a(stringExtra, ao.a(intent), PreferenceManager.getDefaultSharedPreferences(context));
        }
    }
}
