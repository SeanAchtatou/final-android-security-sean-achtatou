package com.safesys.myvpn2;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.ConditionVariable;
import android.util.Log;
import com.safesys.myvpn2.a.f;
import com.safesys.myvpn2.a.h;
import com.safesys.myvpn2.a.m;
import com.safesys.myvpn2.a.o;

public class p {
    private ah a;
    private f b;
    private o c;
    /* access modifiers changed from: private */
    public Context d;

    public p(Context context) {
        this.d = context;
    }

    private void c(m mVar) {
        Log.i("MyVPN", "check status of vpn: " + mVar);
        ConditionVariable conditionVariable = new ConditionVariable();
        conditionVariable.close();
        if (f().a((ServiceConnection) new r(this, conditionVariable, mVar)) && !conditionVariable.block(1000)) {
            a(mVar.u(), h.IDLE, 0);
        }
    }

    private ah e() {
        if (this.a == null) {
            this.a = ah.a(this.d);
        }
        return this.a;
    }

    private f f() {
        if (this.b == null) {
            this.b = new f(this.d);
        }
        return this.b;
    }

    /* access modifiers changed from: private */
    public o g() {
        if (this.c == null) {
            this.c = new o(this.d);
        }
        return this.c;
    }

    public void a() {
        m c2 = e().c();
        if (c2 == null) {
            throw new t("connect failed, no active vpn");
        }
        a(c2);
    }

    public void a(m mVar) {
        Log.i("MyVPN", "connect to: " + mVar);
        mVar.m();
        m o = mVar.o();
        f().a();
        if (!f().a((ServiceConnection) new s(this, o))) {
            Log.e("MyVPN", "bind service failed");
            a(o.u(), h.IDLE, 101);
        }
    }

    public void a(String str, h hVar, int i) {
        Intent intent = new Intent("vpn.connectivity");
        intent.putExtra("profile_name", str);
        intent.putExtra("connection_state", hVar);
        if (i != 0) {
            intent.putExtra("err", i);
        }
        this.d.sendBroadcast(intent);
    }

    public void b() {
        Log.i("MyVPN", "disconnect active vpn");
        if (!f().a((ServiceConnection) new u(this))) {
            Log.e("MyVPN", "bind service failed");
            c();
        }
    }

    public void b(m mVar) {
        e().a(mVar);
        a(mVar.u(), mVar.A(), 0);
    }

    public void c() {
        m c2 = e().c();
        if (c2 != null) {
            c(c2);
        }
    }

    public void d() {
        for (m c2 : e().d()) {
            c(c2);
        }
    }
}
