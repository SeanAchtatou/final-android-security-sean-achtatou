package com.safesys.myvpn2;

enum am {
    FIVE_MIN(300000),
    TEN_MIN(600000),
    FIFTEEN_MIN(900000),
    THIRTY_MIN(1800000),
    TEST_5_SEC(5000);
    
    private int f;

    private am(int i) {
        this.f = i;
    }
}
