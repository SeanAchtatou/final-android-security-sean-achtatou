package com.safesys.myvpn2;

import android.content.DialogInterface;
import android.util.Log;

class e implements DialogInterface.OnDismissListener {
    final /* synthetic */ VpnSettings a;

    e(VpnSettings vpnSettings) {
        this.a = vpnSettings;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        Log.d("MyVPN", "onDismiss DLG_ABOUT");
        this.a.removeDialog(2);
    }
}
