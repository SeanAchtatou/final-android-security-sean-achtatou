package com.safesys.myvpn2;

import android.net.http.AndroidHttpClient;
import android.util.Log;
import java.io.IOException;
import java.util.TimerTask;
import org.apache.http.client.methods.HttpHead;

class z extends TimerTask {
    private static final String[] a = {"http://www.google.com", "http://www.android.com", "http://www.bing.com", "http://www.yahoo.com"};
    private static int b;

    private z() {
    }

    /* synthetic */ z(z zVar) {
        this();
    }

    private static String a() {
        return a[b()];
    }

    private static synchronized int b() {
        int i;
        synchronized (z.class) {
            b = b == a.length ? 0 : b;
            i = b;
            b = i + 1;
        }
        return i;
    }

    public void run() {
        String a2 = a();
        Log.i(KeepAlive.a, "start heartbeat, target=" + a2);
        AndroidHttpClient newInstance = AndroidHttpClient.newInstance("MyVPN");
        try {
            Log.i(KeepAlive.a, "heartbeat resp: " + newInstance.execute(new HttpHead(a2)).getStatusLine());
        } catch (IOException e) {
            Log.e(KeepAlive.a, "heartdbeat error", e);
        } finally {
            newInstance.close();
        }
    }
}
