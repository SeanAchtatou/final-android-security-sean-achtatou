package com.safesys.myvpn2;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.safesys.myvpn2.a.l;
import com.safesys.myvpn2.a.m;
import com.safesys.myvpn2.a.p;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class ah {
    private static ah a;
    private Context b;
    private String c;
    private List d = new ArrayList();

    private ah(Context context) {
        this.b = context;
    }

    public static ah a(Context context) {
        if (a == null) {
            a = new ah(context);
            a.g();
        }
        return a;
    }

    private void a(l lVar, Object obj, ObjectInputStream objectInputStream) {
        if (obj != null) {
            m a2 = m.a(lVar, this.b);
            if (a2.b(obj)) {
                a2.a(obj, objectInputStream);
                this.d.add(a2);
                return;
            }
            Log.e("MyVPN", "saved profile '" + obj + "' is NOT compatible with " + lVar);
        }
    }

    private void a(File file, String str) {
        aa.a(this.b.openFileInput(str), new FileOutputStream(new File(file, str)));
    }

    private void a(ObjectInputStream objectInputStream) {
        while (true) {
            try {
                a((l) objectInputStream.readObject(), objectInputStream.readObject(), objectInputStream);
            } catch (EOFException e) {
                Log.d("MyVPN", "reach the end of profiles file");
                return;
            }
        }
    }

    private void a(String str, String str2) {
        aa.b(new FileInputStream(new File(str, str2)), e(str2));
    }

    private boolean a(File file) {
        return file.exists() && file.isFile() && file.length() > 0;
    }

    private FileOutputStream e(String str) {
        return this.b.openFileOutput(str, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void e() {
        /*
            r4 = this;
            r0 = 0
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ all -> 0x0017 }
            java.lang.String r2 = "active_profile_id"
            java.io.FileOutputStream r2 = r4.e(r2)     // Catch:{ all -> 0x0017 }
            r1.<init>(r2)     // Catch:{ all -> 0x0017 }
            java.lang.String r0 = r4.c     // Catch:{ all -> 0x0021 }
            r1.writeObject(r0)     // Catch:{ all -> 0x0021 }
            if (r1 == 0) goto L_0x0016
            r1.close()
        L_0x0016:
            return
        L_0x0017:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x001b:
            if (r1 == 0) goto L_0x0020
            r1.close()
        L_0x0020:
            throw r0
        L_0x0021:
            r0 = move-exception
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn2.ah.e():void");
    }

    private m f(String str) {
        for (m mVar : this.d) {
            if (mVar.t().equals(str)) {
                return mVar;
            }
        }
        return null;
    }

    private void f() {
        ObjectOutputStream objectOutputStream;
        Throwable th;
        try {
            objectOutputStream = new ObjectOutputStream(e("profiles"));
            try {
                for (m a2 : this.d) {
                    a2.a(objectOutputStream);
                }
                if (objectOutputStream != null) {
                    objectOutputStream.close();
                }
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            objectOutputStream = null;
            th = th4;
            if (objectOutputStream != null) {
                objectOutputStream.close();
            }
            throw th;
        }
    }

    private File g(String str) {
        File file = new File(str);
        ao.a(file);
        return file;
    }

    private void g() {
        try {
            h();
            i();
            Log.d("MyVPN", "loaded, activeId=" + this.c + ", profiles=" + this.d);
        } catch (Exception e) {
            Log.e("MyVPN", "load profiles failed", e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void h() {
        /*
            r5 = this;
            r0 = 0
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ all -> 0x001c }
            android.content.Context r2 = r5.b     // Catch:{ all -> 0x001c }
            java.lang.String r3 = "active_profile_id"
            java.io.FileInputStream r2 = r2.openFileInput(r3)     // Catch:{ all -> 0x001c }
            r1.<init>(r2)     // Catch:{ all -> 0x001c }
            java.lang.Object r0 = r1.readObject()     // Catch:{ all -> 0x0026 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0026 }
            r5.c = r0     // Catch:{ all -> 0x0026 }
            if (r1 == 0) goto L_0x001b
            r1.close()
        L_0x001b:
            return
        L_0x001c:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0020:
            if (r1 == 0) goto L_0x0025
            r1.close()
        L_0x0025:
            throw r0
        L_0x0026:
            r0 = move-exception
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn2.ah.h():void");
    }

    private void h(String str) {
        File file = new File(str, "active_profile_id");
        File file2 = new File(str, "profiles");
        if (!a(file) || !a(file2)) {
            throw new x("no valid data found in: " + str, C0000R.string.err_imp_nodata, new Object[0]);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void i() {
        /*
            r5 = this;
            r0 = 0
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ all -> 0x0017 }
            android.content.Context r2 = r5.b     // Catch:{ all -> 0x0017 }
            java.lang.String r3 = "profiles"
            java.io.FileInputStream r2 = r2.openFileInput(r3)     // Catch:{ all -> 0x0017 }
            r1.<init>(r2)     // Catch:{ all -> 0x0017 }
            r5.a(r1)     // Catch:{ all -> 0x0021 }
            if (r1 == 0) goto L_0x0016
            r1.close()
        L_0x0016:
            return
        L_0x0017:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x001b:
            if (r1 == 0) goto L_0x0020
            r1.close()
        L_0x0020:
            throw r0
        L_0x0021:
            r0 = move-exception
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn2.ah.i():void");
    }

    private void j() {
        this.c = null;
        this.d.clear();
    }

    public m a(String str) {
        for (m mVar : this.d) {
            if (mVar.u().equals(str)) {
                return mVar;
            }
        }
        return null;
    }

    public void a() {
        Log.d("MyVPN", "save, activeId=" + this.c + ", profiles=" + this.d);
        try {
            e();
            f();
        } catch (IOException e) {
            Log.e("MyVPN", "save profiles failed", e);
        }
    }

    public void a(m mVar) {
        Log.i("MyVPN", "active vpn set to: " + mVar);
        this.c = mVar.t();
    }

    public String b() {
        return this.c;
    }

    public void b(m mVar) {
        mVar.k();
        this.d.add(mVar);
    }

    public void b(String str) {
        if (this.d.isEmpty()) {
            Log.i("MyVPN", "profile list is empty, will not export");
            return;
        }
        a();
        File g = g(str);
        try {
            a(g, "active_profile_id");
            a(g, "profiles");
        } catch (Exception e) {
            throw new x("backup failed", e, C0000R.string.err_exp_failed, new Object[0]);
        }
    }

    public m c() {
        if (this.c == null) {
            return null;
        }
        return f(this.c);
    }

    public void c(m mVar) {
        String u = mVar.u();
        if (TextUtils.isEmpty(u)) {
            throw new p("profile name is empty.", C0000R.string.err_empty_name, new Object[0]);
        }
        for (m mVar2 : this.d) {
            if (mVar != mVar2 && u.equals(mVar2.u())) {
                throw new p("duplicated profile name '" + u + "'.", C0000R.string.err_duplicated_profile_name, u);
            }
        }
        mVar.c();
    }

    public void c(String str) {
        h(str);
        try {
            a(str, "active_profile_id");
            a(str, "profiles");
            j();
            g();
        } catch (Exception e) {
            throw new x("restore failed", e, C0000R.string.err_imp_failed, new Object[0]);
        }
    }

    public Date d(String str) {
        File file = new File(str, "active_profile_id");
        if (!a(file)) {
            return null;
        }
        return new Date(file.lastModified());
    }

    public List d() {
        return Collections.unmodifiableList(this.d);
    }

    public void d(m mVar) {
        String t = mVar.t();
        Log.d("MyVPN", "delete vpn: " + mVar + ", removed=" + this.d.remove(mVar));
        if (t.equals(this.c)) {
            this.c = null;
            Log.d("MyVPN", "deactivate vpn: " + mVar);
        }
    }
}
