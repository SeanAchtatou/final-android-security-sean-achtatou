package com.safesys.myvpn2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

class as extends BroadcastReceiver {
    final /* synthetic */ VpnSettings a;

    as(VpnSettings vpnSettings) {
        this.a = vpnSettings;
    }

    public void onReceive(Context context, Intent intent) {
        if ("vpn.connectivity".equals(intent.getAction())) {
            this.a.c(intent);
        } else {
            Log.d("MyVPN", "VPNSettings receiver ignores intent:" + intent);
        }
    }
}
