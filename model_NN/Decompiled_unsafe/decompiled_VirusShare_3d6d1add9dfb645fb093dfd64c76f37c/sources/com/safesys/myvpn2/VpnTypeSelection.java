package com.safesys.myvpn2;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.safesys.myvpn2.a.l;
import java.util.ArrayList;
import java.util.HashMap;

public class VpnTypeSelection extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.vpn_type_list);
        ListView listView = (ListView) findViewById(C0000R.id.listVpnTypes);
        ArrayList arrayList = new ArrayList();
        for (l lVar : l.values()) {
            HashMap hashMap = new HashMap();
            hashMap.put("name", String.valueOf(getString(C0000R.string.add)) + " " + getString(lVar.b()));
            hashMap.put("desc", getString(lVar.c()));
            arrayList.add(hashMap);
        }
        listView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, C0000R.layout.vpn_type, new String[]{"name", "desc"}, new int[]{C0000R.id.txtVpnType, C0000R.id.txtVpnTypeDesc}));
        listView.setOnItemClickListener(new aj(this));
    }
}
