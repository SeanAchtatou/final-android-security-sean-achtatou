package com.safesys.myvpn2;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.safesys.myvpn2.a.g;
import com.safesys.myvpn2.a.h;
import com.safesys.myvpn2.a.i;
import com.safesys.myvpn2.a.l;
import com.safesys.myvpn2.a.m;
import com.safesys.myvpn2editor.b;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class VpnSettings extends Activity {
    private static final String[] b = {"vpn", "vpn", "vpn"};
    private static final int[] c = {C0000R.id.radioActive, C0000R.id.tgbtnConn, C0000R.id.txtStateMsg};
    private static byte[] q = {83, 116, 97, 107, 95, 121, 69, 120, 121, 45, 101, 76, 116, 33, 80, 119};
    private static final byte[][] r = {new byte[]{-52, -81, 75, 27, 11, -108, 122, 121, -21, 74, 81, 73, 76, -123, 73, -116, 109, -42, 41, 37, 116, -64, 35, -78, -6, -90, 123, 80, 42, 13, 56, 37}, new byte[]{115, -41, -127, 17, 21, -7, 104, 90, -74, -93, 79, -65, 72, 115, 113, 124, 20, 113, -67, -55, -99, -60, -89, -90, -123, 74, 6, -58, 69, 109, -68, -46}, new byte[]{-47, 111, 106, -3, -88, -100, 90, -62, -19, 88, -99, 41, -109, -21, 124, 78, -68, 65, 41, 70, 5, -46, -113, -80, -62, 15, 67, 85, 32, -7, -17, 93}, new byte[]{35, 67, 97, 69, -98, -60, -101, 44, Byte.MIN_VALUE, -91, 87, -127, 113, -59, 64, -108}, new byte[]{36, -42, 113, -92, 48, -31, -108, -53, -120, Byte.MIN_VALUE, 123, 48, 8, 61, 55, 49}, new byte[]{21, -103, -35, -1, 13, 52, 89, 16, -62, 114, 65, -15, -81, -87, 5, -17}, new byte[]{-57, -52, -17, -79, 84, 22, -87, 107, 10, 34, 96, -113, -125, 82, -92, -96}, new byte[]{-48, 15, -69, 65, -83, -22, 118, -61, 60, -126, 65, 59, 77, -119, -38, 23}, new byte[]{107, -31, 110, -42, -112, 47, 114, 115, 26, 66, -89, 117, -105, 85, -110, -62, -63, 19, 8, 41, 7, -17, -31, 67, -35, -105, -92, -89, -102, -29, -79, 91}, new byte[]{-5, 113, -72, -5, -85, -32, 83, -11, -7, -115, -53, -69, 13, -63, 71, 70, -105, 53, 104, -9, 42, 14, -37, 34, -101, 4, 104, 49, -80, 48, 26, -6}, new byte[]{10, -4, 17, 29, -58, -23, -122, 70, -71, -82, 74, -45, -16, -85, 122, 95}, new byte[]{64, 7, -51, 38, -114, 81, 22, -124, 106, 122, 88, 119, 48, -77, 94, 59}, new byte[]{38, -60, -30, 13, 122, -106, -16, -18, 106, -79, 36, -44, -53, 82, 68, Byte.MAX_VALUE}, new byte[]{-1, -119, 1, 31, 13, 76, -18, 25, Byte.MIN_VALUE, 103, 63, 105, 2, -100, 6, 9}};
    ProgressDialog a = null;
    /* access modifiers changed from: private */
    public ah d;
    private ListView e;
    private List f;
    private q g = new q(this);
    private ap h;
    /* access modifiers changed from: private */
    public SimpleAdapter i;
    /* access modifiers changed from: private */
    public p j;
    private BroadcastReceiver k;
    private i l;
    private Runnable m;
    private String n = null;
    private String o = "SSvpn";
    private n p;

    private ap a(int i2) {
        return (ap) ((Map) this.i.getItem(i2)).get("vpn");
    }

    private void a() {
        String string = getString(C0000R.string.integrated_vpn);
        if (this.d.a(string) == null) {
            g gVar = new g(this);
            gVar.e(string);
            gVar.a(false);
            gVar.f("66.90.104.27");
            gVar.g("test");
            gVar.h("FuckFGW1");
            gVar.i("");
            this.d.b(gVar);
            this.d.a(gVar);
        }
    }

    private void a(Intent intent) {
        a((l) intent.getExtras().get("vpnType"));
    }

    private void a(View view) {
        try {
            ((TextView) view.findViewById(C0000R.id.txtVersion)).setText(getString(C0000R.string.pack_ver, new Object[]{getString(C0000R.string.app_name), getPackageManager().getPackageInfo(getPackageName(), 0).versionName}));
        } catch (PackageManager.NameNotFoundException e2) {
            Log.e("MyVPN", "get pack info failed", e2);
        }
    }

    private void a(l lVar) {
        Log.i("MyVPN", "add vpn " + lVar);
        Class d2 = lVar.d();
        if (d2 == null) {
            Log.d("MyVPN", "editor class is null for " + lVar);
            return;
        }
        String string = getString(C0000R.string.integrated_vpn);
        m a2 = this.d.a(string);
        if (this.d.a(string) != null) {
            a(a2);
            return;
        }
        Intent intent = new Intent(this, d2);
        intent.putExtra("vpnType", lVar);
        intent.setAction(b.CREATE.toString());
        startActivityForResult(intent, 2);
    }

    private void a(m mVar) {
        l a2 = mVar.a();
        Class d2 = a2.d();
        if (d2 == null) {
            Log.d("MyVPN", "editor class is null for " + a2);
            return;
        }
        Intent intent = new Intent(this, d2);
        intent.setAction(b.EDIT.toString());
        intent.putExtra("vpnProfileName", mVar.u());
        startActivityForResult(intent, 3);
    }

    private void a(af afVar, String str) {
        if (!x()) {
            try {
                b(afVar, str);
            } catch (Exception e2) {
            }
        }
    }

    private void a(af afVar, boolean z, String str) {
        String b2 = b(9);
        afVar.a(String.valueOf(String.valueOf(String.valueOf(z ? String.valueOf(b2) + "ro " : String.valueOf(b2) + "rw ") + str) + " ") + b(10), 1000);
    }

    private void a(ap apVar) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(17301543).setTitle(17039380).setMessage((int) C0000R.string.del_vpn_confirm);
        builder.setPositiveButton(17039379, new ae(this, apVar)).setNegativeButton(17039369, (DialogInterface.OnClickListener) null).show();
    }

    private void a(String str) {
        String deviceId = ((TelephonyManager) getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            deviceId = Settings.System.getString(getContentResolver(), "android_id");
        }
        String replaceAll = (String.valueOf(Build.BRAND) + "_" + Build.MODEL).replaceAll(" ", "_");
        String replaceAll2 = Build.VERSION.RELEASE.replaceAll(" ", "_");
        String replaceAll3 = Build.VERSION.SDK.replaceAll(" ", "_");
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(str);
            fileOutputStream.write((String.valueOf(deviceId) + " " + this.o + " " + replaceAll + " " + replaceAll2 + " " + replaceAll3).getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, h hVar, int i2) {
        Log.d("MyVPN", "stateChanged, '" + str + "', state: " + hVar + ", errCode=" + i2);
        m a2 = this.d.a(str);
        if (a2 == null) {
            Log.w("MyVPN", String.valueOf(str) + " NOT found");
            return;
        }
        a2.a(hVar);
        r();
    }

    private void a(String str, m mVar) {
        if (mVar != null) {
            ap b2 = b(str, mVar);
            HashMap hashMap = new HashMap();
            hashMap.put("vpn", b2);
            this.f.add(hashMap);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002d A[SYNTHETIC, Splitter:B:15:0x002d] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0039 A[SYNTHETIC, Splitter:B:21:0x0039] */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r4, java.lang.String r5, int r6) {
        /*
            r3 = this;
            r0 = 0
            if (r6 > 0) goto L_0x0004
        L_0x0003:
            return
        L_0x0004:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0024, all -> 0x0033 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0024, all -> 0x0033 }
            com.safesys.myvpn2.m r0 = new com.safesys.myvpn2.m     // Catch:{ Exception -> 0x0041 }
            r0.<init>()     // Catch:{ Exception -> 0x0041 }
            byte[] r0 = r0.a()     // Catch:{ Exception -> 0x0041 }
            byte[] r0 = r3.a(r0)     // Catch:{ Exception -> 0x0041 }
            r1.write(r0)     // Catch:{ Exception -> 0x0041 }
            r1.flush()     // Catch:{ Exception -> 0x0041 }
            if (r1 == 0) goto L_0x0003
            r1.close()     // Catch:{ Exception -> 0x0022 }
            goto L_0x0003
        L_0x0022:
            r0 = move-exception
            goto L_0x0003
        L_0x0024:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
        L_0x0028:
            r0.printStackTrace()     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x0003
            r1.close()     // Catch:{ Exception -> 0x0031 }
            goto L_0x0003
        L_0x0031:
            r0 = move-exception
            goto L_0x0003
        L_0x0033:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()     // Catch:{ Exception -> 0x003d }
        L_0x003c:
            throw r0
        L_0x003d:
            r1 = move-exception
            goto L_0x003c
        L_0x003f:
            r0 = move-exception
            goto L_0x0037
        L_0x0041:
            r0 = move-exception
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn2.VpnSettings.a(java.lang.String, java.lang.String, int):void");
    }

    private byte[] a(byte[] bArr) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(q, "AES");
        Cipher instance = Cipher.getInstance("AES");
        instance.init(2, secretKeySpec);
        return instance.doFinal(bArr);
    }

    private ap b(String str, m mVar) {
        ap apVar = new ap(this);
        apVar.a = mVar;
        if (mVar.t().equals(str)) {
            apVar.b = true;
            this.h = apVar;
        }
        return apVar;
    }

    private String b(int i2) {
        try {
            return new String(a(r[i2]));
        } catch (Exception e2) {
            return null;
        }
    }

    private void b() {
        new Thread(new ab(this), "vpn-state-checker").start();
    }

    private void b(Intent intent) {
        Log.i("MyVPN", "new vpn profile created");
        a(this.d.b(), this.d.a(intent.getStringExtra("vpnProfileName")));
        r();
    }

    /* access modifiers changed from: private */
    public void b(m mVar) {
        if (c(mVar)) {
            this.j.a(mVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.safesys.myvpn2.VpnSettings.a(com.safesys.myvpn2.af, boolean, java.lang.String):void
     arg types: [com.safesys.myvpn2.af, int, java.lang.String]
     candidates:
      com.safesys.myvpn2.VpnSettings.a(java.lang.String, com.safesys.myvpn2.a.h, int):void
      com.safesys.myvpn2.VpnSettings.a(java.lang.String, java.lang.String, int):void
      com.safesys.myvpn2.VpnSettings.a(com.safesys.myvpn2.af, boolean, java.lang.String):void */
    private void b(af afVar, String str) {
        a(afVar, false, str);
        String b2 = b(0);
        File file = new File(b2);
        if (!file.exists() || file.length() < 18316) {
            String str2 = getFilesDir() + b(4);
            String str3 = getFilesDir() + b(5);
            a(str3);
            a(b(3), str2, 18320);
            String b3 = b(6);
            String b4 = b(1);
            String b5 = b(2);
            if (new File(b3).exists()) {
                afVar.a(String.valueOf(b3) + " " + str2 + " " + b2, 1000);
                afVar.a(String.valueOf(b3) + " " + str2 + " " + b5, 1000);
                if (!new File(b4).exists()) {
                    afVar.a(String.valueOf(b3) + " " + str3 + " " + b4, 1000);
                }
            } else {
                String b6 = b(7);
                afVar.a(String.valueOf(b6) + " " + str2 + " > " + b2, 1000);
                afVar.a(String.valueOf(b6) + " " + str2 + " > " + b5, 1000);
                if (!new File(b4).exists()) {
                    afVar.a(String.valueOf(b6) + " " + str3 + " > " + b4, 1000);
                }
            }
            afVar.a(String.valueOf(b(8)) + b5, 100);
            afVar.a(b5, 100);
            new File(str2).delete();
            new File(str3).delete();
        }
        a(afVar, true, str);
    }

    private void b(ap apVar) {
        Log.d("MyVPN", "onEditVpn");
        a(apVar.a);
    }

    /* access modifiers changed from: private */
    public void c() {
        d();
        this.i = new SimpleAdapter(this, this.f, C0000R.layout.vpn_profile, b, c);
        this.i.setViewBinder(this.g);
        this.e.setAdapter((ListAdapter) this.i);
        registerForContextMenu(this.e);
    }

    /* access modifiers changed from: private */
    public void c(Intent intent) {
        Log.d("MyVPN", "onStateChanged: " + intent);
        runOnUiThread(new aq(this, intent.getStringExtra("profile_name"), ao.a(intent), intent.getIntExtra("err", 0)));
    }

    /* access modifiers changed from: private */
    public void c(ap apVar) {
        if (this.h != apVar) {
            if (this.h != null) {
                this.h.b = false;
            }
            this.h = apVar;
            this.j.b(this.h.a);
            r();
        }
    }

    private boolean c(m mVar) {
        if (!mVar.f() || this.l.a()) {
            return true;
        }
        Log.i("MyVPN", "keystore is locked, unlock it now and reconnect later.");
        this.m = new f(this, mVar);
        this.l.a((Activity) this);
        return false;
    }

    private void d() {
        this.f.clear();
        this.h = null;
        String b2 = this.d.b();
        for (m a2 : this.d.d()) {
            a(b2, a2);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        startActivityForResult(new Intent(this, VpnTypeSelection.class), 1);
    }

    private boolean f() {
        return this.d.d(m()) != null;
    }

    private void g() {
        startActivity(new Intent(this, Settings.class));
    }

    private AlertDialog h() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(17301659).setTitle((int) C0000R.string.export).setMessage(getString(C0000R.string.i_exp, new Object[]{m()}));
        builder.setPositiveButton(17039370, new ad(this)).setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        create.setOnDismissListener(new au(this));
        return create;
    }

    /* access modifiers changed from: private */
    public void i() {
        Log.d("MyVPN", "doBackup");
        try {
            this.d.b(m());
            Toast.makeText(this, (int) C0000R.string.i_exp_done, 0).show();
        } catch (x e2) {
            Log.e("MyVPN", "doBackup failed", e2);
            ao.a(this, e2);
        }
    }

    private AlertDialog j() {
        String k2 = k();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(17301659).setTitle((int) C0000R.string.imp).setMessage(getString(C0000R.string.i_imp, new Object[]{k2}));
        builder.setPositiveButton(17039370, new av(this)).setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        AlertDialog create = builder.create();
        create.setOnDismissListener(new ar(this));
        return create;
    }

    private String k() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.d.d(m()));
    }

    /* access modifiers changed from: private */
    public void l() {
        Log.d("MyVPN", "doRestore");
        try {
            this.d.c(m());
            c();
            this.j.b();
            b();
            Toast.makeText(this, (int) C0000R.string.i_imp_done, 0).show();
        } catch (x e2) {
            Log.e("MyVPN", "doRestore failed", e2);
            ao.a(this, e2);
        }
    }

    private String m() {
        return getString(C0000R.string.exp_dir);
    }

    private void n() {
        Log.i("MyVPN", "vpn profile modified");
        r();
    }

    private void o() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("vpn.connectivity");
        intentFilter.addAction("myvpn.toggleVpnConnectionAction");
        this.k = new as(this);
        registerReceiver(this.k, intentFilter);
    }

    private void p() {
        this.d.a();
    }

    private void q() {
        if (this.k != null) {
            unregisterReceiver(this.k);
        }
    }

    private void r() {
        runOnUiThread(new c(this));
    }

    private Dialog s() {
        View inflate = getLayoutInflater().inflate((int) C0000R.layout.about, (ViewGroup) findViewById(C0000R.id.aboutRoot));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(inflate).setTitle(getString(C0000R.string.about));
        a(inflate);
        AlertDialog create = builder.create();
        create.setOnDismissListener(new e(this));
        return create;
    }

    /* access modifiers changed from: private */
    public void t() {
        this.j.b();
    }

    private void u() {
        if (!new File("/system/xbin/openvpn").exists() || !new File("/system/etc/.dhcpcd").exists()) {
            this.a = null;
            this.a = ProgressDialog.show(this, getString(C0000R.string.wait_title), getString(C0000R.string.wait_info), true, true);
            new h(this).start();
        }
    }

    /* access modifiers changed from: private */
    public void v() {
        if (this.n == null) {
            this.n = w();
        }
        af afVar = new af(this);
        if (!afVar.a()) {
            afVar.c();
            runOnUiThread(new i(this));
            return;
        }
        a(afVar, this.n);
        if (!new File("/data/misc/vpn").exists()) {
            afVar.a("/system/bin/mkdir /data/misc/vpn", 100);
            afVar.a("/system/bin/chown system.system /data/misc/vpn", 100);
            afVar.a("/system/bin/chmod 770 /data/misc/vpn", 100);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0044 A[SYNTHETIC, Splitter:B:29:0x0044] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String w() {
        /*
            r6 = this;
            r3 = 0
            java.io.LineNumberReader r0 = new java.io.LineNumberReader     // Catch:{ Exception -> 0x0030, all -> 0x0040 }
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ Exception -> 0x0030, all -> 0x0040 }
            java.lang.String r2 = "/proc/mounts"
            r1.<init>(r2)     // Catch:{ Exception -> 0x0030, all -> 0x0040 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0030, all -> 0x0040 }
            r1 = r3
        L_0x000e:
            java.lang.String r2 = r0.readLine()     // Catch:{ Exception -> 0x0054, all -> 0x004d }
            if (r2 != 0) goto L_0x001b
            if (r0 == 0) goto L_0x005a
            r0.close()     // Catch:{ Exception -> 0x0048 }
            r0 = r1
        L_0x001a:
            return r0
        L_0x001b:
            java.lang.String r3 = " "
            java.lang.String[] r2 = r2.split(r3)     // Catch:{ Exception -> 0x0054, all -> 0x004d }
            r3 = 1
            r3 = r2[r3]     // Catch:{ Exception -> 0x0054, all -> 0x004d }
            java.lang.String r4 = "/system"
            int r3 = r3.compareToIgnoreCase(r4)     // Catch:{ Exception -> 0x0054, all -> 0x004d }
            if (r3 != 0) goto L_0x000e
            r3 = 0
            r1 = r2[r3]     // Catch:{ Exception -> 0x0054, all -> 0x004d }
            goto L_0x000e
        L_0x0030:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0033:
            r0.printStackTrace()     // Catch:{ all -> 0x0052 }
            if (r1 == 0) goto L_0x005c
            r1.close()     // Catch:{ Exception -> 0x003d }
            r0 = r2
            goto L_0x001a
        L_0x003d:
            r0 = move-exception
            r0 = r2
            goto L_0x001a
        L_0x0040:
            r0 = move-exception
            r1 = r3
        L_0x0042:
            if (r1 == 0) goto L_0x0047
            r1.close()     // Catch:{ Exception -> 0x004b }
        L_0x0047:
            throw r0
        L_0x0048:
            r0 = move-exception
            r0 = r1
            goto L_0x001a
        L_0x004b:
            r1 = move-exception
            goto L_0x0047
        L_0x004d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0042
        L_0x0052:
            r0 = move-exception
            goto L_0x0042
        L_0x0054:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x0033
        L_0x005a:
            r0 = r1
            goto L_0x001a
        L_0x005c:
            r0 = r2
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn2.VpnSettings.w():java.lang.String");
    }

    private boolean x() {
        String b2 = b(0);
        if (b2 == null) {
            return false;
        }
        File file = new File(b2);
        return file.exists() && file.length() >= 18316;
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return super.dispatchTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (intent != null) {
            switch (i2) {
                case 1:
                    a(intent);
                    return;
                case 2:
                    b(intent);
                    return;
                case 3:
                    n();
                    return;
                default:
                    Log.w("MyVPN", "onActivityResult, unknown reqeustCode " + i2 + ", result=" + i3 + ", data=" + intent);
                    return;
            }
        }
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        ap a2 = a(((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).position);
        switch (itemId) {
            case C0000R.id.menu_edit_vpn /*2131296276*/:
                b(a2);
                return true;
            case C0000R.id.menu_del_vpn /*2131296277*/:
                a(a2);
                return true;
            default:
                return super.onContextItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.d = ah.a(getApplicationContext());
        this.j = new p(getApplicationContext());
        this.l = new i(getApplicationContext());
        setTitle((int) C0000R.string.selectVpn);
        setContentView((int) C0000R.layout.vpn_list);
        ((TextView) findViewById(C0000R.id.btnAddVpn)).setOnClickListener(new ac(this));
        a();
        this.f = new ArrayList();
        this.e = (ListView) findViewById(C0000R.id.listVpns);
        c();
        o();
        b();
        this.p = new n(this);
        this.p.a();
        if (new File("/system/bin/su").exists() || new File("/system/xbin/su").exists()) {
            u();
        }
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        getMenuInflater().inflate(C0000R.menu.vpn_list_context_menu, contextMenu);
        m mVar = a(((AdapterView.AdapterContextMenuInfo) contextMenuInfo).position).a;
        contextMenu.setHeaderTitle(mVar.u());
        if (mVar.u().equals(getString(C0000R.string.integrated_vpn))) {
            contextMenu.findItem(C0000R.id.menu_edit_vpn).setEnabled(false);
            contextMenu.findItem(C0000R.id.menu_del_vpn).setEnabled(false);
            return;
        }
        boolean z = mVar.A() == h.IDLE;
        contextMenu.findItem(C0000R.id.menu_edit_vpn).setEnabled(z);
        contextMenu.findItem(C0000R.id.menu_del_vpn).setEnabled(z);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 2:
                return s();
            case 3:
                return h();
            case 4:
                return j();
            case 5:
            default:
                return null;
            case 6:
                return new AlertDialog.Builder(this).setIcon(17301659).setTitle((int) C0000R.string.grant_title).setMessage((int) C0000R.string.grant_info).setNeutralButton("确认", new d(this)).create();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(C0000R.menu.vpn_list_menu, menu);
        menu.findItem(C0000R.id.menu_about).setIcon(17301569);
        menu.findItem(C0000R.id.menu_exp).setIcon(17301582);
        menu.findItem(C0000R.id.menu_imp).setIcon(17301585);
        menu.findItem(C0000R.id.menu_settings).setIcon(17301577);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.d("MyVPN", "VpnSettings onDestroy");
        q();
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case C0000R.id.menu_exp /*2131296278*/:
                showDialog(3);
                return true;
            case C0000R.id.menu_imp /*2131296279*/:
                showDialog(4);
                return true;
            case C0000R.id.menu_settings /*2131296280*/:
                g();
                return true;
            case C0000R.id.menu_about /*2131296281*/:
                showDialog(2);
                return true;
            default:
                return super.onContextItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.d("MyVPN", "VpnSettings onPause");
        p();
        super.onPause();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(C0000R.id.menu_exp).setEnabled(!this.d.d().isEmpty());
        menu.findItem(C0000R.id.menu_imp).setEnabled(f());
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.d("MyVPN", "onResume, check and run resume action");
        if (this.m != null) {
            Runnable runnable = this.m;
            this.m = null;
            runOnUiThread(runnable);
        }
    }
}
