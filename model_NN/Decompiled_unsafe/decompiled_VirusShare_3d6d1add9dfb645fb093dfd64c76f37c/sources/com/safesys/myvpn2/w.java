package com.safesys.myvpn2;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class w extends WebViewClient {
    w() {
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        webView.loadUrl(str);
        return true;
    }
}
