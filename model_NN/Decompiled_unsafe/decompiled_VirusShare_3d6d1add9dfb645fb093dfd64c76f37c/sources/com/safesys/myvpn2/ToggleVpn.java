package com.safesys.myvpn2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.safesys.myvpn2.a.h;
import com.safesys.myvpn2.a.i;
import com.safesys.myvpn2.a.m;

public class ToggleVpn extends Activity {
    private static /* synthetic */ int[] e;
    private ah a;
    private i b;
    private Runnable c;
    private h d;

    private void a(Intent intent) {
        this.d = (h) intent.getSerializableExtra("activeVpnState");
        switch (a()[this.d.ordinal()]) {
            case 4:
                d();
                return;
            case 5:
                c();
                return;
            default:
                Log.i("MyVPN.ToggleVpn", "intent not handled, currentState=" + this.d);
                finish();
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a(m mVar) {
        if (b(mVar)) {
            e();
            finish();
        }
    }

    static /* synthetic */ int[] a() {
        int[] iArr = e;
        if (iArr == null) {
            iArr = new int[h.values().length];
            try {
                iArr[h.CANCELLED.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[h.CONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[h.CONNECTING.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[h.DISCONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[h.IDLE.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[h.UNKNOWN.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[h.UNUSABLE.ordinal()] = 6;
            } catch (NoSuchFieldError e8) {
            }
            e = iArr;
        }
        return iArr;
    }

    private ah b() {
        if (this.a == null) {
            this.a = ah.a(getApplicationContext());
        }
        return this.a;
    }

    private boolean b(m mVar) {
        if (!mVar.f() || this.b.a()) {
            return true;
        }
        Log.i("MyVPN.ToggleVpn", "keystore is locked, unlock it now and reconnect later.");
        this.c = new y(this, mVar);
        this.b.a((Activity) this);
        return false;
    }

    private void c() {
        Log.d("MyVPN.ToggleVpn", "connect ...");
        m c2 = b().c();
        if (c2 == null) {
            Toast.makeText(this, getString(C0000R.string.err_no_active_vpn), 0).show();
            Log.e("MyVPN.ToggleVpn", "connect failed, no active vpn");
            return;
        }
        a(c2);
    }

    private void d() {
        Log.d("MyVPN.ToggleVpn", "disconnect ...");
        e();
        finish();
    }

    private void e() {
        sendBroadcast(new Intent("myvpn.toggleVpnConnectionAction"));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = new i(getApplicationContext());
        a(getIntent());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.d("MyVPN.ToggleVpn", "onResume, check and run resume action");
        if (this.c != null) {
            Runnable runnable = this.c;
            this.c = null;
            runOnUiThread(runnable);
        }
    }
}
