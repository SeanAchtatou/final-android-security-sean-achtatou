package com.safesys.myvpn2;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.safesys.myvpn2.a.h;

final class q implements SimpleAdapter.ViewBinder {
    private static /* synthetic */ int[] b;
    final /* synthetic */ VpnSettings a;

    q(VpnSettings vpnSettings) {
        this.a = vpnSettings;
    }

    private String a(h hVar) {
        switch (a()[hVar.ordinal()]) {
            case 1:
                return this.a.getString(C0000R.string.connecting);
            case 2:
                return this.a.getString(C0000R.string.disconnecting);
            default:
                return "";
        }
    }

    private void a(RadioButton radioButton, ap apVar) {
        radioButton.setOnCheckedChangeListener(null);
        radioButton.setText(apVar.a.u());
        radioButton.setChecked(apVar.b);
        radioButton.setOnCheckedChangeListener(apVar);
    }

    private void a(TextView textView, ap apVar) {
        String a2 = a(apVar.a.A());
        textView.setVisibility(TextUtils.isEmpty(a2) ? 4 : 0);
        textView.setText(a2);
    }

    private void a(ToggleButton toggleButton, ap apVar) {
        toggleButton.setOnCheckedChangeListener(null);
        toggleButton.setChecked(apVar.a.A() == h.CONNECTED);
        toggleButton.setEnabled(ao.a(apVar.a));
        toggleButton.setOnCheckedChangeListener(apVar);
    }

    static /* synthetic */ int[] a() {
        int[] iArr = b;
        if (iArr == null) {
            iArr = new int[h.values().length];
            try {
                iArr[h.CANCELLED.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[h.CONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[h.CONNECTING.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[h.DISCONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[h.IDLE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[h.UNKNOWN.ordinal()] = 7;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[h.UNUSABLE.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            b = iArr;
        }
        return iArr;
    }

    public boolean setViewValue(View view, Object obj, String str) {
        if (!(obj instanceof ap)) {
            return false;
        }
        ap apVar = (ap) obj;
        if (view instanceof RadioButton) {
            a((RadioButton) view, apVar);
            return true;
        } else if (view instanceof ToggleButton) {
            a((ToggleButton) view, apVar);
            return true;
        } else if (view instanceof TextView) {
            a((TextView) view, apVar);
            return true;
        } else {
            Log.d("MyVPN", "unknown view, not bound: v=" + view + ", data=" + str);
            return false;
        }
    }
}
