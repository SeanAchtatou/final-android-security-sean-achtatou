package com.safesys.myvpn2.a;

import android.content.Context;
import android.util.Log;
import dalvik.system.PathClassLoader;
import java.lang.reflect.Method;

public abstract class j implements Cloneable {
    private static PathClassLoader a;
    private transient Context b;
    private String c;
    private Class d;
    private Object e;
    private n f;

    protected j(Context context, String str) {
        this.b = context;
        this.c = str;
        this.f = new n();
        a();
    }

    protected j(Context context, String str, n nVar) {
        this.b = context;
        this.c = str;
        this.f = nVar;
        a();
    }

    private void a() {
        try {
            a(this.b);
            this.d = c(this.c);
            this.e = this.f.a(this.d, this.b);
        } catch (Exception e2) {
            throw new e("init classloader failed", e2);
        }
    }

    private static void a(Context context) {
        if (a == null) {
            a = new PathClassLoader(context.getPackageManager().getApplicationInfo("com.android.settings", 0).sourceDir, ClassLoader.getSystemClassLoader());
        }
    }

    protected static final Class c(String str) {
        return Class.forName(str, true, a);
    }

    /* access modifiers changed from: protected */
    public Object a(String str, Object... objArr) {
        try {
            return b(str, objArr).invoke(this.e, objArr);
        } catch (Exception e2) {
            throw new e("failed to invoke mehod '" + str + "' on stub", e2);
        }
    }

    /* access modifiers changed from: protected */
    public Method a(Class cls, String str, Object... objArr) {
        int i = 0;
        Class[] clsArr = new Class[objArr.length];
        int length = objArr.length;
        int i2 = 0;
        while (i < length) {
            clsArr[i2] = objArr[i].getClass();
            i++;
            i2++;
        }
        Method method = cls.getMethod(str, clsArr);
        method.setAccessible(true);
        return method;
    }

    public void a(Object obj) {
        this.e = obj;
    }

    /* access modifiers changed from: protected */
    public Method b(String str, Object... objArr) {
        return a(this.d, str, objArr);
    }

    public Context p() {
        return this.b;
    }

    public Class q() {
        return this.d;
    }

    public Object r() {
        return this.e;
    }

    /* renamed from: s */
    public j clone() {
        j jVar;
        CloneNotSupportedException e2;
        try {
            j jVar2 = (j) super.clone();
            try {
                jVar2.a();
                return jVar2;
            } catch (CloneNotSupportedException e3) {
                e2 = e3;
                jVar = jVar2;
                Log.e("MyVPN", "clone failed", e2);
                return jVar;
            }
        } catch (CloneNotSupportedException e4) {
            CloneNotSupportedException cloneNotSupportedException = e4;
            jVar = null;
            e2 = cloneNotSupportedException;
            Log.e("MyVPN", "clone failed", e2);
            return jVar;
        }
    }
}
