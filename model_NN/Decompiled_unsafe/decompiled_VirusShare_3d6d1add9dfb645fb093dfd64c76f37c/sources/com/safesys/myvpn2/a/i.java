package com.safesys.myvpn2.a;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class i extends j {
    public i(Context context) {
        super(context, "android.security.KeyStore", new c());
    }

    public void a(Activity activity) {
        try {
            activity.startActivity(new Intent("android.credentials.UNLOCK"));
        } catch (ActivityNotFoundException e) {
            Log.e("MyVPN", "unlock credentials failed", e);
        }
    }

    public boolean a() {
        int intValue = ((Integer) a("test", new Object[0])).intValue();
        Log.d("MyVPN", "KeyStore.test result is: " + intValue);
        return intValue == 1;
    }

    public boolean a(String str) {
        return ((Boolean) a("delete", str)).booleanValue();
    }

    public boolean a(String str, String str2) {
        return ((Boolean) a("put", str, str2)).booleanValue();
    }
}
