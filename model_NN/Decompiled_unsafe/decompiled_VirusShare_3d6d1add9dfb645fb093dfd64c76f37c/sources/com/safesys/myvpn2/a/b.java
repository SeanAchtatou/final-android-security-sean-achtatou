package com.safesys.myvpn2.a;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.safesys.myvpn2.C0000R;

public class b extends g {
    public b(Context context) {
        super(context, "android.net.vpn.L2tpIpsecPskProfile");
    }

    private String C() {
        return "VPN_i" + t();
    }

    public l a() {
        return l.L2TP_IPSEC_PSK;
    }

    public void a(String str) {
        a("setPresharedKey", str);
    }

    public String b() {
        return (String) a("getPresharedKey", new Object[0]);
    }

    public void c() {
        super.c();
        if (TextUtils.isEmpty(b())) {
            throw new p("presharedKey is empty", C0000R.string.err_empty_psk, new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
        super.d();
        String b = b();
        String C = C();
        if (!n().a(C, b)) {
            Log.e("MyVPN", "keystore write failed: key=" + C);
        }
    }

    public boolean e() {
        return super.e() || !TextUtils.isEmpty(b());
    }

    public boolean f() {
        return true;
    }

    /* renamed from: g */
    public b h() {
        b bVar = (b) super.o();
        bVar.a(C());
        return bVar;
    }
}
