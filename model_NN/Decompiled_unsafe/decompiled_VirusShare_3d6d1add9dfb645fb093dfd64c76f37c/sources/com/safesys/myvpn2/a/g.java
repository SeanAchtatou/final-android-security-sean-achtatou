package com.safesys.myvpn2.a;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.safesys.myvpn2.C0000R;
import com.safesys.myvpn2.x;

public class g extends m {
    private i a;

    public g(Context context) {
        super(context, "android.net.vpn.L2tpProfile");
    }

    protected g(Context context, String str) {
        super(context, str);
    }

    private String b() {
        return "VPN_l" + t();
    }

    public l a() {
        return l.L2TP;
    }

    public void a(boolean z) {
        try {
            q().getMethod("setSecretEnabled", Boolean.TYPE).invoke(r(), Boolean.valueOf(z));
        } catch (Throwable th) {
            throw new x("setSecretEnabled failed", th);
        }
    }

    public void b(String str) {
        a("setSecretString", str);
    }

    public void c() {
        super.c();
        if (i() && TextUtils.isEmpty(j())) {
            throw new p("secret is empty", C0000R.string.err_empty_secret, new Object[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
        String b = b();
        if (i()) {
            if (!n().a(b, j())) {
                Log.e("MyVPN", "keystore write failed: key=" + b);
                return;
            }
            return;
        }
        n().a(b);
    }

    public boolean e() {
        return i() && !TextUtils.isEmpty(j());
    }

    public boolean f() {
        return i();
    }

    /* renamed from: h */
    public g o() {
        g gVar = (g) super.o();
        boolean i = i();
        gVar.a(i);
        if (i) {
            gVar.b(b());
        }
        return gVar;
    }

    public boolean i() {
        return ((Boolean) a("isSecretEnabled", new Object[0])).booleanValue();
    }

    public String j() {
        return (String) a("getSecretString", new Object[0]);
    }

    public void k() {
        super.k();
        d();
    }

    public void l() {
        super.l();
        d();
    }

    public void m() {
        super.m();
        d();
    }

    /* access modifiers changed from: protected */
    public i n() {
        if (this.a == null) {
            this.a = new i(p());
        }
        return this.a;
    }
}
