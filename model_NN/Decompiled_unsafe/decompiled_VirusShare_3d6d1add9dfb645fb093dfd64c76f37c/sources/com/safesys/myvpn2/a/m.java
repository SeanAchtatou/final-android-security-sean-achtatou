package com.safesys.myvpn2.a;

import android.content.Context;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.UUID;

public abstract class m extends j {
    private String a;
    private String b;
    private h c = h.UNKNOWN;

    protected m(Context context, String str) {
        super(context, str);
    }

    public static m a(l lVar, Context context) {
        Class a2 = lVar.a();
        if (a2 == null) {
            throw new IllegalArgumentException("profile class is null for " + lVar);
        }
        try {
            return (m) a2.getConstructor(Context.class).newInstance(context);
        } catch (Exception e) {
            throw new e("failed to create instance for " + lVar, e);
        }
    }

    public h A() {
        return this.c;
    }

    /* renamed from: B */
    public m s() {
        m mVar = (m) super.clone();
        mVar.d(t());
        mVar.e(u());
        mVar.f(v());
        mVar.i(y());
        return mVar;
    }

    public abstract l a();

    public void a(h hVar) {
        this.c = hVar;
    }

    public void a(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeObject(a());
        objectOutputStream.writeObject(r());
        objectOutputStream.writeObject(this.a);
        objectOutputStream.writeObject(this.b);
    }

    public void a(Object obj, ObjectInputStream objectInputStream) {
        a(obj);
        this.a = (String) objectInputStream.readObject();
        this.b = (String) objectInputStream.readObject();
    }

    public boolean b(Object obj) {
        return q().equals(obj.getClass());
    }

    public void c() {
    }

    public void d(String str) {
        a("setId", str);
    }

    public void e(String str) {
        a("setName", str);
    }

    public boolean e() {
        return false;
    }

    public void f(String str) {
        a("setServerName", str);
    }

    public boolean f() {
        return false;
    }

    public void g(String str) {
        this.a = str;
    }

    public void h(String str) {
        this.b = str;
    }

    public void i(String str) {
        a("setDomainSuffices", str);
    }

    public void k() {
        d(UUID.randomUUID().toString());
    }

    public void l() {
    }

    public void m() {
    }

    public m o() {
        return s();
    }

    public String t() {
        return (String) a("getId", new Object[0]);
    }

    public String toString() {
        return String.valueOf(t()) + "#" + u();
    }

    public String u() {
        return (String) a("getName", new Object[0]);
    }

    public String v() {
        return (String) a("getServerName", new Object[0]);
    }

    public String w() {
        return this.a;
    }

    public String x() {
        return this.b;
    }

    public String y() {
        return (String) a("getDomainSuffices", new Object[0]);
    }

    public Class z() {
        try {
            return c("android.net.vpn.VpnProfile");
        } catch (ClassNotFoundException e) {
            throw new e("load class failed", e);
        }
    }
}
