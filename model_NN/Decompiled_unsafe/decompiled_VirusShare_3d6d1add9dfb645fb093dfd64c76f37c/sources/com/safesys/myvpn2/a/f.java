package com.safesys.myvpn2.a;

import android.content.Context;
import android.content.ServiceConnection;

public class f extends j {
    public f(Context context) {
        super(context, "android.net.vpn.VpnManager", new d());
    }

    public void a() {
        a("startVpnService", new Object[0]);
    }

    public boolean a(ServiceConnection serviceConnection) {
        try {
            return ((Boolean) q().getMethod("bindVpnService", ServiceConnection.class).invoke(r(), serviceConnection)).booleanValue();
        } catch (Throwable th) {
            throw new e("bindVpnService failed", th);
        }
    }
}
