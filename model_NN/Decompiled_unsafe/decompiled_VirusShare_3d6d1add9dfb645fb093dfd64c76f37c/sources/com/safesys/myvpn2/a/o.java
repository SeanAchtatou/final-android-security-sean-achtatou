package com.safesys.myvpn2.a;

import android.content.Context;
import android.os.IBinder;

public class o extends j {
    public o(Context context) {
        super(context, "android.net.vpn.IVpnService$Stub", new a());
    }

    private void b(IBinder iBinder) {
        a(q().getMethod("asInterface", IBinder.class).invoke(null, iBinder));
    }

    public void a(IBinder iBinder) {
        b(iBinder);
        a("disconnect", new Object[0]);
    }

    public boolean a(IBinder iBinder, m mVar) {
        b(iBinder);
        return ((Boolean) q().getMethod("connect", mVar.z(), String.class, String.class).invoke(r(), mVar.r(), mVar.w(), mVar.x())).booleanValue();
    }

    public void b(IBinder iBinder, m mVar) {
        b(iBinder);
        q().getMethod("checkStatus", mVar.z()).invoke(r(), mVar.r());
    }
}
