package com.safesys.myvpn2.a;

import android.content.Context;
import com.safesys.myvpn2.x;

public class k extends m {
    public k(Context context) {
        super(context, "android.net.vpn.PptpProfile");
    }

    public l a() {
        return l.PPTP;
    }

    public void a(boolean z) {
        try {
            q().getMethod("setEncryptionEnabled", Boolean.TYPE).invoke(r(), Boolean.valueOf(z));
        } catch (Throwable th) {
            throw new x("setEncryptionEnabled failed", th);
        }
    }

    public boolean b() {
        return ((Boolean) a("isEncryptionEnabled", new Object[0])).booleanValue();
    }
}
