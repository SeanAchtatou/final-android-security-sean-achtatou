package com.safesys.myvpn2.a;

import com.safesys.myvpn2.C0000R;
import com.safesys.myvpn2editor.L2tpIpsecPskProfileEditor;
import com.safesys.myvpn2editor.L2tpProfileEditor;
import com.safesys.myvpn2editor.PptpProfileEditor;

public enum l {
    PL2TP("L2TP", C0000R.string.vpn_myvpn, C0000R.string.vpn_myvpn_info, g.class, L2tpProfileEditor.class),
    PPTP("PPTP", C0000R.string.vpn_pptp, C0000R.string.vpn_pptp_info, k.class, PptpProfileEditor.class),
    L2TP("L2TP", C0000R.string.vpn_l2tp, C0000R.string.vpn_l2tp_info, g.class, L2tpProfileEditor.class),
    L2TP_IPSEC_PSK("L2TP/IPSec PSK", C0000R.string.vpn_l2tp_psk, C0000R.string.vpn_l2tp_psk_info, b.class, L2tpIpsecPskProfileEditor.class);
    
    private String e;
    private Class f;
    private int g;
    private int h;
    private Class i;

    private l(String str, int i2, int i3, Class cls, Class cls2) {
        this.e = str;
        this.h = i2;
        this.g = i3;
        this.f = cls;
        this.i = cls2;
    }

    public Class a() {
        return this.f;
    }

    public int b() {
        return this.h;
    }

    public int c() {
        return this.g;
    }

    public Class d() {
        return this.i;
    }
}
