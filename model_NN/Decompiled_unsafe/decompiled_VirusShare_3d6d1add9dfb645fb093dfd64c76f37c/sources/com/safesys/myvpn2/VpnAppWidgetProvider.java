package com.safesys.myvpn2;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class VpnAppWidgetProvider extends AppWidgetProvider {
    public void onDisabled(Context context) {
        Log.d("MyVPN", "VpnAppWidgetProvider onDisabled");
        context.stopService(new Intent(context, VpnConnectorService.class));
        super.onDisabled(context);
    }

    public void onEnabled(Context context) {
        super.onEnabled(context);
        Log.d("MyVPN", "VpnAppWidgetProvider enabled");
        context.startService(new Intent(context, VpnConnectorService.class));
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        Log.d("MyVPN", "VpnAppWidgetProvider onUpdate");
        context.startService(new Intent(context, VpnConnectorService.class));
    }
}
