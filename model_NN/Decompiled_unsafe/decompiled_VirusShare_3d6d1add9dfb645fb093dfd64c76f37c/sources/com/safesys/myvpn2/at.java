package com.safesys.myvpn2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

final class at extends BroadcastReceiver {
    final /* synthetic */ VpnConnectorService a;

    private at(VpnConnectorService vpnConnectorService) {
        this.a = vpnConnectorService;
    }

    /* synthetic */ at(VpnConnectorService vpnConnectorService, at atVar) {
        this(vpnConnectorService);
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("myvpn.toggleVpnConnectionAction".equals(action)) {
            this.a.a(intent);
        } else if ("vpn.connectivity".equals(action)) {
            this.a.b(intent);
        } else {
            Log.w(VpnConnectorService.a, "VpnStateReceiver ignores unknown intent:" + intent);
        }
    }
}
