package com.safesys.myvpn2;

import android.app.Activity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;

final class ai {
    protected WebView a;
    protected Activity b;
    String[] c = null;
    int d;

    public ai(Activity activity) {
        this.b = activity;
        this.a = new WebView(this.b);
        a(this.a);
    }

    public static void a(WebView webView) {
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setBuiltInZoomControls(true);
        webView.setWebViewClient(new w());
        webView.setWebChromeClient(new v());
    }

    private void b(String str) {
        this.c = new String[100];
        int i = 0;
        String str2 = str;
        while (i < str2.length()) {
            int indexOf = str2.indexOf("<a href=\"");
            int indexOf2 = str2.indexOf("\">", indexOf);
            String[] strArr = this.c;
            int i2 = this.d;
            this.d = i2 + 1;
            strArr[i2] = str2.substring(indexOf + 9, indexOf2);
            str2 = str2.substring(indexOf2 + 2, str2.length());
            i = i + indexOf2 + 2;
        }
    }

    public void a(int i) {
        if (this.d != 0 && this.c != null) {
            int i2 = i % this.d;
            if (this.c[i2] == null) {
                return;
            }
            if (this.c[i2].startsWith("http://")) {
                this.a.loadUrl(this.c[i2]);
                return;
            }
            this.a.loadUrl(String.valueOf(this.a.getUrl()) + this.c[i2]);
        }
    }

    public void a(String str) {
        if (this.c != null) {
            this.c = null;
        }
        this.d = 0;
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 15000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        HttpGet httpGet = new HttpGet(str);
        httpGet.addHeader("User-Agent", this.a.getSettings().getUserAgentString());
        httpGet.addHeader("Connection", "Keep-Alive");
        httpGet.addHeader("Accept", "*, */*");
        try {
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            if (execute.getStatusLine().getStatusCode() == 200) {
                b(EntityUtils.toString(execute.getEntity(), "utf-8"));
            }
        } catch (Exception e) {
        }
        this.a.stopLoading();
        this.a.clearView();
        this.a.loadUrl(str);
    }
}
