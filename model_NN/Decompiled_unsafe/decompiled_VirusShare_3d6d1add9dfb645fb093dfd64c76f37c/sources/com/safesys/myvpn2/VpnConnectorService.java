package com.safesys.myvpn2;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.safesys.myvpn2.a.h;

public class VpnConnectorService extends Service {
    /* access modifiers changed from: private */
    public static final String a = VpnConnectorService.class.getName();
    private static final ComponentName b = new ComponentName("com.safesys.myvpn", "com.safesys.myvpn.VpnAppWidgetProvider");
    private static /* synthetic */ int[] g;
    private Context c;
    private at d;
    private h e = h.IDLE;
    private p f;

    private void a(RemoteViews remoteViews) {
        Intent intent = new Intent(this.c, ToggleVpn.class);
        intent.putExtra("activeVpnState", this.e);
        remoteViews.setOnClickPendingIntent(C0000R.id.frmToggleVpnStatue, PendingIntent.getActivity(this.c, 2, intent, 1));
    }

    private void a(String str, h hVar) {
        if (!str.equals(ao.a(this.c))) {
            Log.d(a, "updateViews, ignores non-active profile event for " + str);
            return;
        }
        this.e = hVar;
        f();
    }

    static /* synthetic */ int[] b() {
        int[] iArr = g;
        if (iArr == null) {
            iArr = new int[h.values().length];
            try {
                iArr[h.CANCELLED.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[h.CONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[h.CONNECTING.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[h.DISCONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[h.IDLE.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[h.UNKNOWN.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[h.UNUSABLE.ordinal()] = 6;
            } catch (NoSuchFieldError e8) {
            }
            g = iArr;
        }
        return iArr;
    }

    private void c() {
        this.d = new at(this, null);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("vpn.connectivity");
        intentFilter.addAction("myvpn.toggleVpnConnectionAction");
        registerReceiver(this.d, intentFilter);
    }

    private void d() {
        if (this.d != null) {
            unregisterReceiver(this.d);
        }
    }

    private void e() {
        try {
            this.f.a();
        } catch (t e2) {
            Toast.makeText(this, (int) C0000R.string.err_no_active_vpn, 0).show();
            Log.e(a, "connect failed, no active vpn");
        }
    }

    private void f() {
        RemoteViews remoteViews = new RemoteViews(this.c.getPackageName(), g());
        a(remoteViews);
        AppWidgetManager.getInstance(this.c).updateAppWidget(b, remoteViews);
    }

    private int g() {
        switch (b()[this.e.ordinal()]) {
            case 1:
            case 2:
                return C0000R.layout.vpn_widget_transition;
            case 3:
            default:
                return C0000R.layout.vpn_widget_disconnected;
            case 4:
                return C0000R.layout.vpn_widget_connected;
        }
    }

    public void a(Intent intent) {
        switch (b()[this.e.ordinal()]) {
            case 4:
                this.f.b();
                return;
            case 5:
                e();
                return;
            default:
                Log.i(a, "toggleVpnState intent not handled, currentState=" + this.e + ", intent=" + intent);
                return;
        }
    }

    public void b(Intent intent) {
        String stringExtra = intent.getStringExtra("profile_name");
        h a2 = ao.a(intent);
        Log.d(a, String.valueOf(stringExtra) + " stateChanged: " + this.e + "->" + a2 + ", errCode=" + intent.getIntExtra("err", 0));
        a(stringExtra, a2);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        Log.i(a, "VpnConnectorService created");
        this.c = getApplicationContext();
        this.f = new p(this.c);
        f();
        c();
        this.f.c();
    }

    public void onDestroy() {
        Log.i(a, "VpnConnectorService destroyed");
        d();
        super.onDestroy();
    }
}
