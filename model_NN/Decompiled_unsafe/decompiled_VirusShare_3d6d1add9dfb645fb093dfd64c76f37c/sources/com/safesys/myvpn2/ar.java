package com.safesys.myvpn2;

import android.content.DialogInterface;
import android.util.Log;

class ar implements DialogInterface.OnDismissListener {
    final /* synthetic */ VpnSettings a;

    ar(VpnSettings vpnSettings) {
        this.a = vpnSettings;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        Log.d("MyVPN", "onDismiss DLG_RESTORE");
        this.a.removeDialog(4);
    }
}
