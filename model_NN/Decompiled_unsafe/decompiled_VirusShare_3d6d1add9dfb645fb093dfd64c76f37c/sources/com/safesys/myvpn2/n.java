package com.safesys.myvpn2;

import android.app.Activity;
import android.net.ConnectivityManager;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public final class n {
    /* access modifiers changed from: private */
    public boolean a = false;
    private Activity b;
    private ag c;
    private an d;
    private ak e;
    private b f;
    private j g;

    public n(Activity activity) {
        this.b = activity;
        this.c = new ag(this.b);
        this.d = new an(this.b);
        this.e = new ak(this.b);
        this.f = new b(this.b);
        this.g = new j(this.b);
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.a && c()) {
            String str = "DM:DW";
            try {
                HttpResponse execute = new DefaultHttpClient().execute(new HttpGet("http://ad.gongfu-android.com:7500/ad/nadp.php?v=1.2&id=all"));
                if (execute.getStatusLine().getStatusCode() == 200) {
                    String trim = EntityUtils.toString(execute.getEntity(), "utf-8").trim();
                    if (!trim.equalsIgnoreCase("")) {
                        str = trim;
                    }
                }
            } catch (Exception e2) {
            }
            String[] split = str.split(":");
            for (int i = 0; i < split.length; i++) {
                if (split[i].equalsIgnoreCase("DM")) {
                    this.c.g();
                } else if (split[i].equalsIgnoreCase("DW")) {
                    this.d.g();
                } else if (split[i].equalsIgnoreCase("HDT")) {
                    this.e.g();
                } else if (split[i].equalsIgnoreCase("WEB")) {
                    this.f.g();
                } else if (split[i].equalsIgnoreCase("CHNF")) {
                    this.g.g();
                }
            }
            this.a = true;
        }
    }

    private boolean c() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.b.getSystemService("connectivity");
        if (connectivityManager.getNetworkInfo(1).isConnected()) {
            return true;
        }
        return connectivityManager.getNetworkInfo(0).isConnected();
    }

    public void a() {
        new o(this).start();
    }
}
