package com.safesys.myvpn2;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.safesys.myvpn2.a.h;
import com.safesys.myvpn2.a.m;

class s implements ServiceConnection {
    final /* synthetic */ p a;
    private final /* synthetic */ m b;

    s(p pVar, m mVar) {
        this.a = pVar;
        this.b = mVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            if (!this.a.g().a(iBinder, this.b)) {
                Log.d("MyVPN", "~~~~~~ connect() failed!");
                this.a.a(this.b.u(), h.IDLE, 101);
            } else {
                Log.d("MyVPN", "~~~~~~ connect() succeeded!");
            }
        } catch (Throwable th) {
            Log.e("MyVPN", "connect()", th);
            this.a.a(this.b.u(), h.IDLE, 101);
        } finally {
            this.a.d.unbindService(this);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        Log.e("MyVPN", "onServiceDisconnected");
        this.a.c();
    }
}
