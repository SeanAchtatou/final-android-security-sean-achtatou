package com.safesys.myvpn2;

import java.io.InputStream;
import java.io.OutputStream;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class aa {
    private static final byte[] a = {65, 98, 95, 45, 54, 89, 53, 33, 41, 103, 43, 68, 85, 72, 110, 115};

    private aa() {
    }

    private static Cipher a(int i) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(a, "AES");
        IvParameterSpec ivParameterSpec = new IvParameterSpec(a);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(i, secretKeySpec, ivParameterSpec);
        return instance;
    }

    private static void a(int i, InputStream inputStream, OutputStream outputStream) {
        CipherOutputStream cipherOutputStream = null;
        try {
            cipherOutputStream = b(i, inputStream, outputStream);
        } finally {
            inputStream.close();
            if (cipherOutputStream != null) {
                cipherOutputStream.close();
            }
        }
    }

    public static void a(InputStream inputStream, OutputStream outputStream) {
        a(1, inputStream, outputStream);
    }

    private static CipherOutputStream b(int i, InputStream inputStream, OutputStream outputStream) {
        CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, a(i));
        byte[] bArr = new byte[128];
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                return cipherOutputStream;
            }
            cipherOutputStream.write(bArr, 0, read);
        }
    }

    public static void b(InputStream inputStream, OutputStream outputStream) {
        a(2, inputStream, outputStream);
    }
}
