package com.safesys.myvpn2;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import com.safesys.myvpn2.a.l;

class aj implements AdapterView.OnItemClickListener {
    final /* synthetic */ VpnTypeSelection a;

    aj(VpnTypeSelection vpnTypeSelection) {
        this.a = vpnTypeSelection;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        l lVar = l.values()[i];
        Log.i("MyVPN", lVar + " picked");
        Intent intent = new Intent(this.a, VpnSettings.class);
        intent.putExtra("vpnType", lVar);
        this.a.setResult(-1, intent);
        this.a.finish();
    }
}
