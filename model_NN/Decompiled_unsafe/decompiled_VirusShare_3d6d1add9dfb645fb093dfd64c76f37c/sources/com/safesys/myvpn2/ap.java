package com.safesys.myvpn2;

import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.ToggleButton;
import com.safesys.myvpn2.a.m;

final class ap implements CompoundButton.OnCheckedChangeListener {
    m a;
    boolean b;
    final /* synthetic */ VpnSettings c;

    ap(VpnSettings vpnSettings) {
        this.c = vpnSettings;
    }

    private void a(boolean z) {
        if (this.b != z) {
            this.b = z;
            if (this.b) {
                this.c.c(this);
            }
        }
    }

    private void b(boolean z) {
        if (z) {
            this.c.b(this.a);
        } else {
            this.c.t();
        }
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (compoundButton instanceof RadioButton) {
            a(z);
        } else if (compoundButton instanceof ToggleButton) {
            b(z);
        }
    }

    public String toString() {
        return this.a.u();
    }
}
