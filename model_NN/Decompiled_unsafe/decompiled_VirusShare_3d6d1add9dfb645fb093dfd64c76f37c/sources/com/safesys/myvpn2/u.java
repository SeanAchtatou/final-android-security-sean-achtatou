package com.safesys.myvpn2;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

class u implements ServiceConnection {
    final /* synthetic */ p a;

    u(p pVar) {
        this.a = pVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            this.a.g().a(iBinder);
        } catch (Exception e) {
            Log.e("MyVPN", "disconnect()", e);
            this.a.c();
        } finally {
            this.a.d.unbindService(this);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        Log.e("MyVPN", "onServiceDisconnected");
        this.a.c();
    }
}
