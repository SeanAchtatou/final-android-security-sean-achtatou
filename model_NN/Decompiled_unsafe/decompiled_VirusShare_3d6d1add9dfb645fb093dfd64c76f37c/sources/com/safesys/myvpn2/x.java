package com.safesys.myvpn2;

public class x extends RuntimeException {
    private int a;
    private Object[] b;

    public x(String str) {
        super(str);
    }

    public x(String str, int i, Object... objArr) {
        super(str);
        this.a = i;
        this.b = objArr;
    }

    public x(String str, Throwable th) {
        super(str, th);
    }

    public x(String str, Throwable th, int i, Object... objArr) {
        super(str, th);
        this.a = i;
        this.b = objArr;
    }

    public int a() {
        return this.a;
    }

    public Object[] b() {
        return this.b;
    }
}
