package com.safesys.myvpn2;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.ConditionVariable;
import android.os.IBinder;
import android.util.Log;
import com.safesys.myvpn2.a.h;
import com.safesys.myvpn2.a.m;

class r implements ServiceConnection {
    final /* synthetic */ p a;
    private final /* synthetic */ ConditionVariable b;
    private final /* synthetic */ m c;

    r(p pVar, ConditionVariable conditionVariable, m mVar) {
        this.a = pVar;
        this.b = conditionVariable;
        this.c = mVar;
    }

    public synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.b.open();
        try {
            this.a.g().b(iBinder, this.c);
            this.a.d.unbindService(this);
        } catch (Exception e) {
            Log.e("MyVPN", "checkStatus()", e);
            this.a.a(this.c.u(), h.IDLE, 0);
            this.a.d.unbindService(this);
        } catch (Throwable th) {
            this.a.d.unbindService(this);
            throw th;
        }
        return;
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.b.open();
        this.a.a(this.c.u(), h.IDLE, 0);
        this.a.d.unbindService(this);
    }
}
