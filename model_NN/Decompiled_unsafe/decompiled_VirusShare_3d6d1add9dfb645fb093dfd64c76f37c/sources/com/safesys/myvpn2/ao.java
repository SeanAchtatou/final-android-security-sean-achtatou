package com.safesys.myvpn2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import com.safesys.myvpn2.a.h;
import com.safesys.myvpn2.a.m;
import java.io.File;
import java.io.Serializable;

public final class ao {
    private ao() {
    }

    public static h a(Intent intent) {
        Serializable serializableExtra = intent.getSerializableExtra("connection_state");
        return serializableExtra != null ? h.valueOf(serializableExtra.toString()) : h.IDLE;
    }

    public static String a(Context context) {
        m c = ah.a(context).c();
        if (c != null) {
            return c.u();
        }
        return null;
    }

    public static void a(Activity activity, x xVar) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCancelable(true).setMessage(activity.getString(xVar.a(), xVar.b()));
        AlertDialog create = builder.create();
        create.setOwnerActivity(activity);
        create.show();
    }

    public static void a(File file) {
        String externalStorageState = Environment.getExternalStorageState();
        if (!"mounted".equals(externalStorageState)) {
            throw new x("no writable storage found, state=" + externalStorageState, C0000R.string.err_no_writable_storage, new Object[0]);
        }
        if (!file.exists()) {
            file.mkdirs();
        }
        if (!file.exists()) {
            throw new x("failed to mkdir: " + file, C0000R.string.err_exp_write_storage_failed, new Object[0]);
        }
    }

    public static boolean a(m mVar) {
        h A = mVar.A();
        return A == h.CONNECTED || A == h.IDLE;
    }
}
