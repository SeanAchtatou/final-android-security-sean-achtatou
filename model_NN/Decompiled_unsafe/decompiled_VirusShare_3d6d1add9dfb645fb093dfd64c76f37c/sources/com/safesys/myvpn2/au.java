package com.safesys.myvpn2;

import android.content.DialogInterface;
import android.util.Log;

class au implements DialogInterface.OnDismissListener {
    final /* synthetic */ VpnSettings a;

    au(VpnSettings vpnSettings) {
        this.a = vpnSettings;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        Log.d("MyVPN", "onDismiss DLG_BACKUP");
        this.a.removeDialog(3);
    }
}
