package com.safesys.myvpn2;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;

public class Settings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private ListPreference a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(C0000R.xml.pref);
        this.a = (ListPreference) getPreferenceScreen().findPreference("com.safesys.myvpn.pref.keepAlive.period");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        CharSequence entry = this.a.getEntry();
        this.a.setSummary(getString(C0000R.string.keepalive_period_sum, new Object[]{entry}));
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if (str.equals("com.safesys.myvpn.pref.keepAlive.period")) {
            CharSequence entry = this.a.getEntry();
            this.a.setSummary(getString(C0000R.string.keepalive_period_sum, new Object[]{entry}));
        }
    }
}
