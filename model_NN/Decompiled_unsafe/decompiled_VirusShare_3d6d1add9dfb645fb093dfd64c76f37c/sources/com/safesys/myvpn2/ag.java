package com.safesys.myvpn2;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

final class ag extends g {
    private String q;
    private String r = ("android,," + this.m + ",," + this.n + ",");
    private String s;
    private int t;
    private int u;
    private int v;
    private String w;
    private String x;
    private String y;
    private JSONObject z;

    public ag(Activity activity) {
        super(activity);
        this.a = "DM";
        if (this.l != null) {
            this.r = String.valueOf(this.r) + this.l;
        }
        this.r = String.valueOf(this.r) + ",,,";
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.g.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.t = displayMetrics.widthPixels;
        this.u = displayMetrics.heightPixels;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        int indexOf;
        int i;
        int indexOf2;
        if (str != null && (indexOf = str.indexOf("<a href=")) >= 0 && (indexOf2 = str.indexOf("\"", (i = indexOf + 9))) >= 0) {
            String substring = str.substring(i, indexOf2);
            if (!substring.matches("http://")) {
                String substring2 = this.x.substring(0, this.x.indexOf("?"));
                String.valueOf(substring2.substring(0, substring2.lastIndexOf("/") + 1)) + substring;
            }
        }
    }

    private String h() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.g.getSystemService("connectivity")).getActiveNetworkInfo();
        return (activeNetworkInfo == null || activeNetworkInfo.getType() == 1) ? "wifi" : activeNetworkInfo.getType() == 0 ? activeNetworkInfo.getExtraInfo() != null ? activeNetworkInfo.getExtraInfo() : activeNetworkInfo.getSubtypeName() : "GPRS";
    }

    private void i() {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
        HttpPost httpPost = new HttpPost(this.w);
        httpPost.addHeader("User-Agent", this.p);
        httpPost.addHeader("Connection", "Keep-Alive");
        try {
            if (defaultHttpClient.execute(httpPost).getStatusLine().getStatusCode() != 200) {
            }
        } catch (Exception e) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0081  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void j() {
        /*
            r10 = this;
            r8 = 13
            r7 = 0
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient
            org.apache.http.params.HttpParams r1 = r10.e()
            r0.<init>(r1)
            org.apache.http.client.methods.HttpPost r1 = new org.apache.http.client.methods.HttpPost
            java.lang.String r2 = r10.y
            r1.<init>(r2)
            java.lang.String r2 = "User-Agent"
            java.lang.String r3 = r10.p
            r1.addHeader(r2, r3)
            java.lang.String r2 = "Connection"
            java.lang.String r3 = "Keep-Alive"
            r1.addHeader(r2, r3)
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            org.apache.http.message.BasicNameValuePair r3 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r4 = "ua"
            java.lang.String r5 = r10.r
            r3.<init>(r4, r5)
            r2.add(r3)
            org.apache.http.message.BasicNameValuePair r3 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r4 = "ipb"
            java.lang.String r5 = r10.e
            r3.<init>(r4, r5)
            r2.add(r3)
            org.apache.http.message.BasicNameValuePair r3 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r4 = "rt"
            java.lang.String r5 = "1"
            r3.<init>(r4, r5)
            r2.add(r3)
            org.json.JSONObject r3 = r10.z     // Catch:{ JSONException -> 0x00e1 }
            java.lang.String r4 = "sid"
            java.lang.String r3 = r3.getString(r4)     // Catch:{ JSONException -> 0x00e1 }
            org.json.JSONObject r4 = r10.z     // Catch:{ JSONException -> 0x00ec }
            java.lang.String r5 = "identifier"
            java.lang.String r4 = r4.getString(r5)     // Catch:{ JSONException -> 0x00ec }
            r9 = r4
            r4 = r3
            r3 = r9
        L_0x005d:
            org.apache.http.message.BasicNameValuePair r5 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r6 = "rp_md5"
            r5.<init>(r6, r4)
            r2.add(r5)
            org.apache.http.message.BasicNameValuePair r4 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r5 = "cid"
            java.lang.String r6 = r10.q
            r4.<init>(r5, r6)
            r2.add(r4)
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            int r5 = r4.length()
            if (r5 <= r8) goto L_0x0086
            r5 = 0
            java.lang.String r4 = r4.substring(r5, r8)
        L_0x0086:
            org.json.JSONObject r5 = new org.json.JSONObject
            r5.<init>()
            java.lang.String r6 = "id"
            r5.put(r6, r3)     // Catch:{ JSONException -> 0x00e7 }
            java.lang.String r3 = "ts"
            r5.put(r3, r4)     // Catch:{ JSONException -> 0x00e7 }
            java.lang.String r3 = "type"
            java.lang.String r4 = "load_success"
            r5.put(r3, r4)     // Catch:{ JSONException -> 0x00e7 }
            java.lang.String r3 = "idtype"
            java.lang.String r4 = "ad"
            r5.put(r3, r4)     // Catch:{ JSONException -> 0x00e7 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x00e7 }
            java.lang.String r4 = "["
            r3.<init>(r4)     // Catch:{ JSONException -> 0x00e7 }
            java.lang.String r4 = r5.toString()     // Catch:{ JSONException -> 0x00e7 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x00e7 }
            java.lang.String r4 = "]"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ JSONException -> 0x00e7 }
            java.lang.String r3 = r3.toString()     // Catch:{ JSONException -> 0x00e7 }
        L_0x00bc:
            org.apache.http.message.BasicNameValuePair r4 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r5 = "jstr"
            r4.<init>(r5, r3)
            r2.add(r4)
            org.apache.http.client.entity.UrlEncodedFormEntity r3 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x00ea }
            java.lang.String r4 = "utf-8"
            r3.<init>(r2, r4)     // Catch:{ Exception -> 0x00ea }
            r1.setEntity(r3)     // Catch:{ Exception -> 0x00ea }
            org.apache.http.HttpResponse r0 = r0.execute(r1)     // Catch:{ Exception -> 0x00ea }
            org.apache.http.StatusLine r0 = r0.getStatusLine()     // Catch:{ Exception -> 0x00ea }
            int r0 = r0.getStatusCode()     // Catch:{ Exception -> 0x00ea }
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 == r1) goto L_0x00e0
        L_0x00e0:
            return
        L_0x00e1:
            r3 = move-exception
            r3 = r7
        L_0x00e3:
            r4 = r3
            r3 = r7
            goto L_0x005d
        L_0x00e7:
            r3 = move-exception
            r3 = r7
            goto L_0x00bc
        L_0x00ea:
            r0 = move-exception
            goto L_0x00e0
        L_0x00ec:
            r4 = move-exception
            goto L_0x00e3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.safesys.myvpn2.ag.j():void");
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            Thread.sleep((long) (d(1500) + 500));
        } catch (Exception e) {
        }
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
        HttpGet httpGet = new HttpGet(this.x);
        httpGet.addHeader("User-Agent", this.p);
        httpGet.addHeader("Connection", "Keep-Alive");
        try {
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            if (execute.getStatusLine().getStatusCode() == 200) {
                String entityUtils = EntityUtils.toString(execute.getEntity(), "utf-8");
                i();
                new al(this, entityUtils).start();
                try {
                    Thread.sleep(1000);
                } catch (Exception e2) {
                }
                j();
            }
        } catch (Exception e3) {
        }
    }

    public void a(String str, String str2) {
        super.a(str, str2);
        c(20);
        this.v = d(20) + 3;
        String[] split = this.f.split("\\.");
        this.s = split[split.length - 1];
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        ArrayList arrayList = new ArrayList();
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(e());
        HttpPost httpPost = new HttpPost("http://r.domob.cn/a/");
        arrayList.add(new BasicNameValuePair("rt", "1"));
        arrayList.add(new BasicNameValuePair("ipb", this.e));
        arrayList.add(new BasicNameValuePair("ua", this.r));
        arrayList.add(new BasicNameValuePair("l", "zh-cn"));
        arrayList.add(new BasicNameValuePair("f", "jsonp"));
        arrayList.add(new BasicNameValuePair("e", "UTF-8"));
        arrayList.add(new BasicNameValuePair("sdk", "1"));
        arrayList.add(new BasicNameValuePair("v", "20110901-android-20110806"));
        arrayList.add(new BasicNameValuePair("idv", String.valueOf(this.h) + "," + this.i + "," + this.j));
        arrayList.add(new BasicNameValuePair("network", h()));
        arrayList.add(new BasicNameValuePair("cid", this.q));
        String valueOf = String.valueOf(System.currentTimeMillis());
        if (valueOf.length() > 13) {
            valueOf = valueOf.substring(0, 13);
        }
        arrayList.add(new BasicNameValuePair("ts", valueOf));
        arrayList.add(new BasicNameValuePair("so", "v"));
        arrayList.add(new BasicNameValuePair("sw", String.valueOf(this.t)));
        arrayList.add(new BasicNameValuePair("sh", String.valueOf(this.u)));
        arrayList.add(new BasicNameValuePair("sd", "1.0"));
        arrayList.add(new BasicNameValuePair("k", ""));
        arrayList.add(new BasicNameValuePair("dim", "320x48"));
        arrayList.add(new BasicNameValuePair("pb[identifier]", this.f));
        arrayList.add(new BasicNameValuePair("pb[version]", String.valueOf(this.v)));
        arrayList.add(new BasicNameValuePair("stat[reqs]", "1"));
        arrayList.add(new BasicNameValuePair("stat[time]", "0"));
        arrayList.add(new BasicNameValuePair("c", "gif,fsi,ltx,iad"));
        arrayList.add(new BasicNameValuePair("pb[name]", this.s));
        arrayList.add(new BasicNameValuePair("sv", "010301"));
        httpPost.addHeader("User-Agent", this.p);
        httpPost.addHeader("Connection", "Keep-Alive");
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "utf-8"));
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() != 200) {
                return false;
            }
            String entityUtils = EntityUtils.toString(execute.getEntity(), "utf-8");
            if (this.z != null) {
                this.z = null;
            }
            this.z = new JSONObject(entityUtils);
            JSONObject jSONObject = this.z.getJSONArray("ac").getJSONObject(0);
            this.w = this.z.getString("rp_url");
            this.x = String.valueOf(jSONObject.getString("d")) + "&sv=010301&ot=" + jSONObject.getString("opentype");
            this.y = "http://e.domob.cn/report";
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.q = String.valueOf(a(37)) + "+" + a(42) + "+" + a(21) + "==";
    }
}
