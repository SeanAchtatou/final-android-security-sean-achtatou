package com.safesys.myvpn2editor;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.safesys.myvpn2.C0000R;
import com.safesys.myvpn2.a.g;
import com.safesys.myvpn2.a.l;
import com.safesys.myvpn2.a.m;

public class L2tpProfileEditor extends VpnProfileEditor {
    private CheckBox a;
    /* access modifiers changed from: private */
    public EditText b;
    /* access modifiers changed from: private */
    public TextView c;

    private void f() {
        if (((l) getIntent().getExtras().get("vpnType")) == l.PL2TP) {
            Log.d("XXX", "That's OK!");
            this.a.setVisibility(8);
            this.a.setChecked(false);
            this.b.setVisibility(8);
            this.c.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public m a() {
        return new g(getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup) {
        this.a = new CheckBox(this);
        this.a.setText(getString(C0000R.string.l2tp_secret_enabled));
        viewGroup.addView(this.a);
        this.c = new TextView(this);
        this.c.setText(getString(C0000R.string.l2tp_secret));
        viewGroup.addView(this.c);
        this.b = new EditText(this);
        this.b.setImeOptions(5);
        this.b.setTransformationMethod(new PasswordTransformationMethod());
        viewGroup.addView(this.b);
        this.c.setEnabled(false);
        this.b.setEnabled(false);
        this.a.setOnCheckedChangeListener(new a(this));
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        g gVar = (g) d();
        boolean isChecked = this.a.isChecked();
        gVar.a(isChecked);
        gVar.b(isChecked ? this.b.getText().toString().trim() : "");
    }

    /* access modifiers changed from: protected */
    public void c() {
        g gVar = (g) d();
        this.a.setChecked(gVar.i());
        if (gVar.i()) {
            this.b.setText(gVar.j());
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        if (bundle != null) {
            super.onRestoreInstanceState(bundle);
            this.a.setChecked(bundle.getBoolean("secretEnabled"));
            this.b.setText(bundle.getCharSequence("secret"));
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("secretEnabled", this.a.isChecked());
        bundle.putCharSequence("secret", this.b.getText());
    }
}
