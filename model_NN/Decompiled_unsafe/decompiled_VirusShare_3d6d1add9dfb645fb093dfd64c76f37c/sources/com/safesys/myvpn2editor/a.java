package com.safesys.myvpn2editor;

import android.widget.CompoundButton;

class a implements CompoundButton.OnCheckedChangeListener {
    final /* synthetic */ L2tpProfileEditor a;

    a(L2tpProfileEditor l2tpProfileEditor) {
        this.a = l2tpProfileEditor;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        this.a.c.setEnabled(z);
        this.a.b.setEnabled(z);
    }
}
