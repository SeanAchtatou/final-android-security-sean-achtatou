package com.safesys.myvpn2editor;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.CheckBox;
import com.safesys.myvpn2.C0000R;
import com.safesys.myvpn2.a.k;
import com.safesys.myvpn2.a.m;

public class PptpProfileEditor extends VpnProfileEditor {
    private CheckBox a;

    /* access modifiers changed from: protected */
    public m a() {
        return new k(getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup) {
        this.a = new CheckBox(this);
        this.a.setText(getString(C0000R.string.encrypt_enabled));
        viewGroup.addView(this.a);
    }

    /* access modifiers changed from: protected */
    public void b() {
        ((k) d()).a(this.a.isChecked());
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.a.setChecked(((k) d()).b());
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        if (bundle != null) {
            super.onRestoreInstanceState(bundle);
            this.a.setChecked(bundle.getBoolean("encrypt"));
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("encrypt", this.a.isChecked());
    }
}
