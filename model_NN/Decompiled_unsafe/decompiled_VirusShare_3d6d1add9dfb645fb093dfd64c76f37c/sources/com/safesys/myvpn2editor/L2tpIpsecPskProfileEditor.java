package com.safesys.myvpn2editor;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.safesys.myvpn2.C0000R;
import com.safesys.myvpn2.a.b;
import com.safesys.myvpn2.a.m;

public class L2tpIpsecPskProfileEditor extends L2tpProfileEditor {
    private EditText a;

    /* access modifiers changed from: protected */
    public m a() {
        return new b(getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup) {
        TextView textView = new TextView(this);
        textView.setText(getString(C0000R.string.psk));
        viewGroup.addView(textView);
        this.a = new EditText(this);
        this.a.setImeOptions(5);
        this.a.setTransformationMethod(new PasswordTransformationMethod());
        viewGroup.addView(this.a);
        super.a(viewGroup);
    }

    /* access modifiers changed from: protected */
    public void b() {
        ((b) d()).a(this.a.getText().toString().trim());
        super.b();
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.a.setText(((b) d()).b());
        super.c();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        if (bundle != null) {
            super.onRestoreInstanceState(bundle);
            this.a.setText(bundle.getCharSequence("psk"));
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putCharSequence("psk", this.a.getText());
    }
}
