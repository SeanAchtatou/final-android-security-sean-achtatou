package com.safesys.myvpn2editor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.safesys.myvpn2.C0000R;
import com.safesys.myvpn2.a.h;
import com.safesys.myvpn2.a.i;
import com.safesys.myvpn2.a.l;
import com.safesys.myvpn2.a.m;
import com.safesys.myvpn2.a.p;
import com.safesys.myvpn2.ah;
import com.safesys.myvpn2.x;

public abstract class VpnProfileEditor extends Activity {
    private static /* synthetic */ int[] p;
    private b a;
    private m b;
    private EditText c;
    private EditText d;
    private EditText e;
    private EditText f;
    private EditText g;
    private ah h;
    private i i;
    private Runnable j;
    private Object[] k;
    private TextView l;
    private TextView m;
    private TextView n;
    private TextView o;

    private void a(Intent intent) {
        this.a = b.valueOf(intent.getAction());
        switch (e()[this.a.ordinal()]) {
            case 1:
                this.b = a();
                break;
            case 2:
                this.b = this.h.a((String) intent.getExtras().get("vpnProfileName"));
                h();
                break;
            default:
                throw new x("failed to init VpnProfileEditor, unknown editAction: " + this.a);
        }
        setTitle(this.b.a().b());
    }

    private void a(p pVar) {
        this.k = pVar.b();
        showDialog(pVar.a());
    }

    private void b(ViewGroup viewGroup) {
        this.l = new TextView(this);
        this.l.setText(getString(C0000R.string.vpnname));
        viewGroup.addView(this.l);
        this.c = new EditText(this);
        this.c.setImeOptions(5);
        viewGroup.addView(this.c);
        this.m = new TextView(this);
        this.m.setText(getString(C0000R.string.server));
        viewGroup.addView(this.m);
        this.d = new EditText(this);
        this.d.setImeOptions(5);
        viewGroup.addView(this.d);
        a(viewGroup);
        this.n = new TextView(this);
        this.n.setText(getString(C0000R.string.dns_suffices));
        viewGroup.addView(this.n);
        this.o = new TextView(this);
        this.o.setText(getString(C0000R.string.comma_sep));
        this.o.setTextColor(-6710887);
        this.o.setTextSize(1, 12.0f);
        viewGroup.addView(this.o);
        this.e = new EditText(this);
        this.e.setImeOptions(5);
        viewGroup.addView(this.e);
        TextView textView = new TextView(this);
        textView.setText(getString(C0000R.string.username));
        viewGroup.addView(textView);
        this.f = new EditText(this);
        this.f.setImeOptions(5);
        viewGroup.addView(this.f);
        TextView textView2 = new TextView(this);
        textView2.setText(getString(C0000R.string.password));
        viewGroup.addView(textView2);
        this.g = new EditText(this);
        this.g.setImeOptions(6);
        this.g.setTransformationMethod(new PasswordTransformationMethod());
        viewGroup.addView(this.g);
        g();
        f();
    }

    static /* synthetic */ int[] e() {
        int[] iArr = p;
        if (iArr == null) {
            iArr = new int[b.values().length];
            try {
                iArr[b.CREATE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[b.DELETE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[b.EDIT.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            p = iArr;
        }
        return iArr;
    }

    private void f() {
        if (((l) getIntent().getExtras().get("vpnType")) == l.PL2TP) {
            this.l.setVisibility(8);
            this.m.setVisibility(8);
            this.n.setVisibility(8);
            this.o.setVisibility(8);
            this.e.setVisibility(8);
            this.e.setText("");
            this.c.setVisibility(8);
            this.c.setText((int) C0000R.string.integrated_vpn2);
            this.d.setVisibility(8);
            this.d.setText("vpn.best188.net");
        }
    }

    private void g() {
        ((Button) findViewById(C0000R.id.btnSave)).setOnClickListener(new c(this));
        ((Button) findViewById(C0000R.id.btnCancel)).setOnClickListener(new d(this));
    }

    private void h() {
        this.c.setText(this.b.u());
        this.d.setText(this.b.v());
        this.e.setText(this.b.y());
        this.f.setText(this.b.w());
        this.g.setText(this.b.x());
        c();
    }

    /* access modifiers changed from: private */
    public void i() {
        try {
            j();
            k();
        } catch (p e2) {
            a(e2);
        }
    }

    private void j() {
        this.b.e(this.c.getText().toString().trim());
        this.b.f(this.d.getText().toString().trim());
        this.b.i(this.e.getText().toString().trim());
        this.b.g(this.f.getText().toString().trim());
        this.b.h(this.g.getText().toString().trim());
        this.b.a(h.IDLE);
        b();
        this.h.c(this.b);
    }

    /* access modifiers changed from: private */
    public void k() {
        if (l()) {
            if (this.a == b.CREATE) {
                this.h.b(this.b);
            } else {
                this.b.l();
            }
            m();
            finish();
        }
    }

    private boolean l() {
        if (!this.b.e() || this.i.a()) {
            return true;
        }
        Log.i("MyVPN", "keystore is locked, unlock it now and redo saving later.");
        this.j = new e(this);
        this.i.a((Activity) this);
        return false;
    }

    private void m() {
        Intent intent = new Intent();
        intent.putExtra("vpnProfileName", this.b.u());
        setResult(-1, intent);
    }

    /* access modifiers changed from: private */
    public void n() {
        finish();
    }

    /* access modifiers changed from: protected */
    public abstract m a();

    /* access modifiers changed from: protected */
    public abstract void a(ViewGroup viewGroup);

    /* access modifiers changed from: protected */
    public abstract void b();

    /* access modifiers changed from: protected */
    public abstract void c();

    /* access modifiers changed from: protected */
    public m d() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.vpn_profile_editor);
        this.h = ah.a(getApplicationContext());
        this.i = new i(getApplicationContext());
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        b(linearLayout);
        ((ScrollView) findViewById(C0000R.id.editorScrollView)).addView(linearLayout);
        a(getIntent());
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true).setMessage("");
        return builder.create();
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        Object[] objArr = this.k;
        this.k = null;
        ((AlertDialog) dialog).setMessage(getString(i2, objArr));
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        if (bundle != null) {
            this.c.setText(bundle.getCharSequence("name"));
            this.d.setText(bundle.getCharSequence("server"));
            this.e.setText(bundle.getCharSequence("dns"));
            this.f.setText(bundle.getCharSequence("user"));
            this.g.setText(bundle.getCharSequence("password"));
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.d("MyVPN", "VpnProfileEditor.onResume, check and run resume action");
        if (this.j != null) {
            Runnable runnable = this.j;
            this.j = null;
            runOnUiThread(runnable);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putCharSequence("name", this.c.getText());
        bundle.putCharSequence("server", this.d.getText());
        bundle.putCharSequence("dns", this.e.getText());
        bundle.putCharSequence("user", this.f.getText());
        bundle.putCharSequence("password", this.g.getText());
    }
}
