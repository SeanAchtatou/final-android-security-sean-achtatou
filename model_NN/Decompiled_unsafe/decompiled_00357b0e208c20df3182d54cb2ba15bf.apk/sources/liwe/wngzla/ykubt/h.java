package liwe.wngzla.ykubt;

import android.os.AsyncTask;
import java.lang.reflect.Method;

final class h {
    static volatile boolean a = false;
    static boolean b = false;
    static long c = 0;
    private static String d;
    private static int e = 0;

    static {
        try {
            Object invoke = g.b(839181).getMethod(f.a(1048716), new Class[0]).invoke(g.b(839136), new Object[0]);
            Method method = g.b(839136).getMethod(f.a(1048702), g.b(839141));
            f.p.invoke(method, true);
            method.invoke(invoke, f.a(1048933));
            method.invoke(invoke, f.a(1048576));
            method.invoke(invoke, f.a(1048577));
            d = invoke.toString();
        } catch (Throwable th) {
        }
    }

    static void a(int i, Object obj) {
        if (i == 1) {
            b = true;
        }
        try {
            Class<?> b2 = g.b(839151);
            Object newInstance = b2.newInstance();
            Method method = b2.getMethod(f.a(1048712), g.b(839141), g.b(839143));
            f.p.invoke(method, true);
            method.invoke(newInstance, f.a(1048632), g.a());
            method.invoke(newInstance, f.a(1048629), obj);
            method.invoke(newInstance, f.a(1048633), Integer.valueOf(i));
            Method method2 = g.b(839154).getMethod(f.a(1048837), g.b(839183), f.n);
            f.p.invoke(method2, true);
            c(i, method2.invoke(null, j.a(newInstance.toString().getBytes(f.a(1048648)), f.a(1048581)), 0));
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public static void c(int i, Object obj) {
        a aVar = new a();
        aVar.d = i;
        if (e > 10) {
            e = 0;
        }
        Class<a> cls = a.class;
        try {
            Method method = cls.getMethod(f.a(1048842), g.b(839195), g.b(839179));
            f.p.invoke(method, true);
            method.invoke(aVar, g.b(839191).getField(f.a(1048978)).get(null), new Object[]{d, obj});
        } catch (Throwable th) {
        }
    }

    private static final class a extends AsyncTask<Object, Void, Void> {
        Object a;
        Object b;
        int c;
        int d;
        private String e;

        private a() {
            this.b = null;
            this.e = null;
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
            a();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onCancelled(Void voidR) {
            super.onCancelled(voidR);
            a();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: liwe.wngzla.ykubt.g.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object
         arg types: [java.lang.Class<?>, java.lang.Class[]]
         candidates:
          liwe.wngzla.ykubt.g.a(java.lang.Object, java.lang.Object):void
          liwe.wngzla.ykubt.g.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0383  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x03b0  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x03e1  */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void doInBackground(java.lang.Object... r23) {
            /*
                r22 = this;
                r6 = 0
                r5 = 0
                r4 = 0
                r3 = 0
                r2 = 0
                r2 = r23[r2]     // Catch:{ Exception -> 0x031d }
                r7 = 1
                r7 = r23[r7]     // Catch:{ Exception -> 0x031d }
                r0 = r22
                r0.a = r7     // Catch:{ Exception -> 0x031d }
                r7 = 839162(0xccdfa, float:1.175916E-39)
                java.lang.Class r7 = liwe.wngzla.ykubt.g.b(r7)     // Catch:{ Exception -> 0x031d }
                r8 = 1
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x031d }
                r9 = 0
                r10 = 839141(0xccde5, float:1.175887E-39)
                java.lang.Class r10 = liwe.wngzla.ykubt.g.b(r10)     // Catch:{ Exception -> 0x031d }
                r8[r9] = r10     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Constructor r7 = r7.getConstructor(r8)     // Catch:{ Exception -> 0x031d }
                r8 = 1
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x031d }
                r9 = 0
                r8[r9] = r2     // Catch:{ Exception -> 0x031d }
                java.lang.Object r2 = r7.newInstance(r8)     // Catch:{ Exception -> 0x031d }
                r7 = 839162(0xccdfa, float:1.175916E-39)
                java.lang.Class r7 = liwe.wngzla.ykubt.g.b(r7)     // Catch:{ Exception -> 0x031d }
                r8 = 1048784(0x1000d0, float:1.46966E-39)
                java.lang.String r8 = liwe.wngzla.ykubt.f.a(r8)     // Catch:{ Exception -> 0x031d }
                r9 = 0
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r7 = r7.getMethod(r8, r9)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r8 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r9 = 1
                java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r11 = 1
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r11)     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                r8.invoke(r7, r9)     // Catch:{ Exception -> 0x031d }
                r8 = 0
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x031d }
                java.lang.Object r6 = r7.invoke(r2, r8)     // Catch:{ Exception -> 0x031d }
                r2 = 839238(0xcce46, float:1.176023E-39)
                java.lang.Class r7 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Exception -> 0x031d }
                r2 = 1048783(0x1000cf, float:1.469658E-39)
                java.lang.String r2 = liwe.wngzla.ykubt.f.a(r2)     // Catch:{ Exception -> 0x031d }
                r8 = 1
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x031d }
                r9 = 0
                r10 = 839141(0xccde5, float:1.175887E-39)
                java.lang.Class r10 = liwe.wngzla.ykubt.g.b(r10)     // Catch:{ Exception -> 0x031d }
                r8[r9] = r10     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r2 = r7.getMethod(r2, r8)     // Catch:{ Exception -> 0x031d }
                r8 = 1048789(0x1000d5, float:1.469666E-39)
                java.lang.String r8 = liwe.wngzla.ykubt.f.a(r8)     // Catch:{ Exception -> 0x031d }
                r9 = 1
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                java.lang.Class r11 = java.lang.Boolean.TYPE     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r8 = r7.getMethod(r8, r9)     // Catch:{ Exception -> 0x031d }
                r9 = 1048788(0x1000d4, float:1.469665E-39)
                java.lang.String r9 = liwe.wngzla.ykubt.f.a(r9)     // Catch:{ Exception -> 0x031d }
                r10 = 1
                java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Exception -> 0x031d }
                r11 = 0
                java.lang.Class r12 = java.lang.Boolean.TYPE     // Catch:{ Exception -> 0x031d }
                r10[r11] = r12     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r9 = r7.getMethod(r9, r10)     // Catch:{ Exception -> 0x031d }
                r10 = 1048790(0x1000d6, float:1.469668E-39)
                java.lang.String r10 = liwe.wngzla.ykubt.f.a(r10)     // Catch:{ Exception -> 0x031d }
                r11 = 1
                java.lang.Class[] r11 = new java.lang.Class[r11]     // Catch:{ Exception -> 0x031d }
                r12 = 0
                java.lang.Class<?> r13 = liwe.wngzla.ykubt.f.n     // Catch:{ Exception -> 0x031d }
                r11[r12] = r13     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r10 = r7.getMethod(r10, r11)     // Catch:{ Exception -> 0x031d }
                r11 = 1048791(0x1000d7, float:1.469669E-39)
                java.lang.String r11 = liwe.wngzla.ykubt.f.a(r11)     // Catch:{ Exception -> 0x031d }
                r12 = 1
                java.lang.Class[] r12 = new java.lang.Class[r12]     // Catch:{ Exception -> 0x031d }
                r13 = 0
                java.lang.Class<?> r14 = liwe.wngzla.ykubt.f.n     // Catch:{ Exception -> 0x031d }
                r12[r13] = r14     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r11 = r7.getMethod(r11, r12)     // Catch:{ Exception -> 0x031d }
                r12 = 1048785(0x1000d1, float:1.469661E-39)
                java.lang.String r12 = liwe.wngzla.ykubt.f.a(r12)     // Catch:{ Exception -> 0x031d }
                r13 = 0
                java.lang.Class[] r13 = new java.lang.Class[r13]     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r12 = r7.getMethod(r12, r13)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r13 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r14 = 1
                java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ Exception -> 0x031d }
                r15 = 0
                r16 = 1
                java.lang.Boolean r16 = java.lang.Boolean.valueOf(r16)     // Catch:{ Exception -> 0x031d }
                r14[r15] = r16     // Catch:{ Exception -> 0x031d }
                r13.invoke(r2, r14)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r13 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r14 = 1
                java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ Exception -> 0x031d }
                r15 = 0
                r16 = 1
                java.lang.Boolean r16 = java.lang.Boolean.valueOf(r16)     // Catch:{ Exception -> 0x031d }
                r14[r15] = r16     // Catch:{ Exception -> 0x031d }
                r13.invoke(r11, r14)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r13 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r14 = 1
                java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ Exception -> 0x031d }
                r15 = 0
                r16 = 1
                java.lang.Boolean r16 = java.lang.Boolean.valueOf(r16)     // Catch:{ Exception -> 0x031d }
                r14[r15] = r16     // Catch:{ Exception -> 0x031d }
                r13.invoke(r9, r14)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r13 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r14 = 1
                java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ Exception -> 0x031d }
                r15 = 0
                r16 = 1
                java.lang.Boolean r16 = java.lang.Boolean.valueOf(r16)     // Catch:{ Exception -> 0x031d }
                r14[r15] = r16     // Catch:{ Exception -> 0x031d }
                r13.invoke(r8, r14)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r13 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r14 = 1
                java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ Exception -> 0x031d }
                r15 = 0
                r16 = 1
                java.lang.Boolean r16 = java.lang.Boolean.valueOf(r16)     // Catch:{ Exception -> 0x031d }
                r14[r15] = r16     // Catch:{ Exception -> 0x031d }
                r13.invoke(r10, r14)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r13 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r14 = 1
                java.lang.Object[] r14 = new java.lang.Object[r14]     // Catch:{ Exception -> 0x031d }
                r15 = 0
                r16 = 1
                java.lang.Boolean r16 = java.lang.Boolean.valueOf(r16)     // Catch:{ Exception -> 0x031d }
                r14[r15] = r16     // Catch:{ Exception -> 0x031d }
                r13.invoke(r12, r14)     // Catch:{ Exception -> 0x031d }
                r13 = 1
                java.lang.Object[] r13 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x031d }
                r14 = 0
                r15 = 1048643(0x100043, float:1.469462E-39)
                java.lang.String r15 = liwe.wngzla.ykubt.f.a(r15)     // Catch:{ Exception -> 0x031d }
                java.lang.String r15 = r15.toUpperCase()     // Catch:{ Exception -> 0x031d }
                r13[r14] = r15     // Catch:{ Exception -> 0x031d }
                r2.invoke(r6, r13)     // Catch:{ Exception -> 0x031d }
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x031d }
                r13 = 0
                r14 = 1
                java.lang.Boolean r14 = java.lang.Boolean.valueOf(r14)     // Catch:{ Exception -> 0x031d }
                r2[r13] = r14     // Catch:{ Exception -> 0x031d }
                r9.invoke(r6, r2)     // Catch:{ Exception -> 0x031d }
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x031d }
                r9 = 0
                r13 = 1
                java.lang.Boolean r13 = java.lang.Boolean.valueOf(r13)     // Catch:{ Exception -> 0x031d }
                r2[r9] = r13     // Catch:{ Exception -> 0x031d }
                r8.invoke(r6, r2)     // Catch:{ Exception -> 0x031d }
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x031d }
                r8 = 0
                r9 = 0
                java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x031d }
                r2[r8] = r9     // Catch:{ Exception -> 0x031d }
                r10.invoke(r6, r2)     // Catch:{ Exception -> 0x031d }
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x031d }
                r8 = 0
                r9 = 0
                java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x031d }
                r2[r8] = r9     // Catch:{ Exception -> 0x031d }
                r11.invoke(r6, r2)     // Catch:{ Exception -> 0x031d }
                r2 = 839141(0xccde5, float:1.175887E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Exception -> 0x031d }
                r8 = 1048843(0x10010b, float:1.469742E-39)
                java.lang.String r8 = liwe.wngzla.ykubt.f.a(r8)     // Catch:{ Exception -> 0x031d }
                r9 = 1
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r11 = 839141(0xccde5, float:1.175887E-39)
                java.lang.Class r11 = liwe.wngzla.ykubt.g.b(r11)     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r2 = r2.getMethod(r8, r9)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r8 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r9 = 1
                java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r11 = 1
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r11)     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                r8.invoke(r2, r9)     // Catch:{ Exception -> 0x031d }
                r0 = r22
                java.lang.Object r8 = r0.a     // Catch:{ Exception -> 0x031d }
                r9 = 1
                java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r11 = 1048648(0x100048, float:1.469469E-39)
                java.lang.String r11 = liwe.wngzla.ykubt.f.a(r11)     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                java.lang.Object r2 = r2.invoke(r8, r9)     // Catch:{ Exception -> 0x031d }
                r8 = 0
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x031d }
                java.lang.Object r5 = r12.invoke(r6, r8)     // Catch:{ Exception -> 0x031d }
                r8 = 839236(0xcce44, float:1.17602E-39)
                java.lang.Class r8 = liwe.wngzla.ykubt.g.b(r8)     // Catch:{ Exception -> 0x031d }
                r9 = 1048787(0x1000d3, float:1.469664E-39)
                java.lang.String r9 = liwe.wngzla.ykubt.f.a(r9)     // Catch:{ Exception -> 0x031d }
                r10 = 1
                java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Exception -> 0x031d }
                r11 = 0
                r12 = 839183(0xcce0f, float:1.175946E-39)
                java.lang.Class r12 = liwe.wngzla.ykubt.g.b(r12)     // Catch:{ Exception -> 0x031d }
                r10[r11] = r12     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r8 = r8.getMethod(r9, r10)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r9 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r10 = 1
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x031d }
                r11 = 0
                r12 = 1
                java.lang.Boolean r12 = java.lang.Boolean.valueOf(r12)     // Catch:{ Exception -> 0x031d }
                r10[r11] = r12     // Catch:{ Exception -> 0x031d }
                r9.invoke(r8, r10)     // Catch:{ Exception -> 0x031d }
                r9 = 1
                java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r9[r10] = r2     // Catch:{ Exception -> 0x031d }
                r8.invoke(r5, r9)     // Catch:{ Exception -> 0x031d }
                r2 = 1048710(0x100086, float:1.469556E-39)
                java.lang.String r2 = liwe.wngzla.ykubt.f.a(r2)     // Catch:{ Exception -> 0x031d }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r2 = r7.getMethod(r2, r8)     // Catch:{ Exception -> 0x031d }
                r8 = 0
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x031d }
                r2.invoke(r6, r8)     // Catch:{ Exception -> 0x031d }
                r2 = 1048792(0x1000d8, float:1.46967E-39)
                java.lang.String r2 = liwe.wngzla.ykubt.f.a(r2)     // Catch:{ Exception -> 0x031d }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r2 = r7.getMethod(r2, r8)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r8 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r9 = 1
                java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r11 = 1
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r11)     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                r8.invoke(r2, r9)     // Catch:{ Exception -> 0x031d }
                r8 = 0
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x031d }
                java.lang.Object r2 = r2.invoke(r6, r8)     // Catch:{ Exception -> 0x031d }
                java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Exception -> 0x031d }
                int r2 = r2.intValue()     // Catch:{ Exception -> 0x031d }
                r0 = r22
                r0.c = r2     // Catch:{ Exception -> 0x031d }
                r2 = 839163(0xccdfb, float:1.175918E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Exception -> 0x031d }
                java.lang.Object r3 = r2.newInstance()     // Catch:{ Exception -> 0x031d }
                r0 = r22
                int r2 = r0.c     // Catch:{ Exception -> 0x031d }
                r8 = 200(0xc8, float:2.8E-43)
                if (r2 != r8) goto L_0x0510
                r2 = 1048786(0x1000d2, float:1.469662E-39)
                java.lang.String r2 = liwe.wngzla.ykubt.f.a(r2)     // Catch:{ Exception -> 0x031d }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r2 = r7.getMethod(r2, r8)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r7 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r8 = 1
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x031d }
                r9 = 0
                r10 = 1
                java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)     // Catch:{ Exception -> 0x031d }
                r8[r9] = r10     // Catch:{ Exception -> 0x031d }
                r7.invoke(r2, r8)     // Catch:{ Exception -> 0x031d }
                r7 = 0
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x031d }
                java.lang.Object r4 = r2.invoke(r6, r7)     // Catch:{ Exception -> 0x031d }
                r2 = 8192(0x2000, float:1.14794E-41)
                byte[] r7 = new byte[r2]     // Catch:{ Exception -> 0x031d }
                r2 = 839237(0xcce45, float:1.176022E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Exception -> 0x031d }
                r8 = 1048726(0x100096, float:1.469578E-39)
                java.lang.String r8 = liwe.wngzla.ykubt.f.a(r8)     // Catch:{ Exception -> 0x031d }
                r9 = 1
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r11 = 839183(0xcce0f, float:1.175946E-39)
                java.lang.Class r11 = liwe.wngzla.ykubt.g.b(r11)     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r8 = r2.getMethod(r8, r9)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r2 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r9 = 1
                java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r11 = 1
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r11)     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                r2.invoke(r8, r9)     // Catch:{ Exception -> 0x031d }
                r2 = 839163(0xccdfb, float:1.175918E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Exception -> 0x031d }
                r9 = 1048787(0x1000d3, float:1.469664E-39)
                java.lang.String r9 = liwe.wngzla.ykubt.f.a(r9)     // Catch:{ Exception -> 0x031d }
                r10 = 3
                java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Exception -> 0x031d }
                r11 = 0
                r12 = 839183(0xcce0f, float:1.175946E-39)
                java.lang.Class r12 = liwe.wngzla.ykubt.g.b(r12)     // Catch:{ Exception -> 0x031d }
                r10[r11] = r12     // Catch:{ Exception -> 0x031d }
                r11 = 1
                java.lang.Class<?> r12 = liwe.wngzla.ykubt.f.n     // Catch:{ Exception -> 0x031d }
                r10[r11] = r12     // Catch:{ Exception -> 0x031d }
                r11 = 2
                java.lang.Class<?> r12 = liwe.wngzla.ykubt.f.n     // Catch:{ Exception -> 0x031d }
                r10[r11] = r12     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r9 = r2.getMethod(r9, r10)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r2 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r10 = 1
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x031d }
                r11 = 0
                r12 = 1
                java.lang.Boolean r12 = java.lang.Boolean.valueOf(r12)     // Catch:{ Exception -> 0x031d }
                r10[r11] = r12     // Catch:{ Exception -> 0x031d }
                r2.invoke(r9, r10)     // Catch:{ Exception -> 0x031d }
            L_0x02f1:
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r2[r10] = r7     // Catch:{ Exception -> 0x031d }
                java.lang.Object r2 = r8.invoke(r4, r2)     // Catch:{ Exception -> 0x031d }
                java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Exception -> 0x031d }
                int r2 = r2.intValue()     // Catch:{ Exception -> 0x031d }
                r10 = -1
                if (r2 == r10) goto L_0x03ee
                r10 = 3
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x031d }
                r11 = 0
                r10[r11] = r7     // Catch:{ Exception -> 0x031d }
                r11 = 1
                r12 = 0
                java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x031d }
                r10[r11] = r12     // Catch:{ Exception -> 0x031d }
                r11 = 2
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x031d }
                r10[r11] = r2     // Catch:{ Exception -> 0x031d }
                r9.invoke(r3, r10)     // Catch:{ Exception -> 0x031d }
                goto L_0x02f1
            L_0x031d:
                r2 = move-exception
                liwe.wngzla.ykubt.g.a(r2)     // Catch:{ all -> 0x05cc }
                r7 = 1
                liwe.wngzla.ykubt.h.a = r7     // Catch:{ all -> 0x05cc }
                liwe.wngzla.ykubt.g.a(r2)     // Catch:{ all -> 0x05cc }
                if (r5 == 0) goto L_0x0354
                r2 = 839236(0xcce44, float:1.17602E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x08b2 }
                r7 = 1048705(0x100081, float:1.469549E-39)
                java.lang.String r7 = liwe.wngzla.ykubt.f.a(r7)     // Catch:{ Throwable -> 0x08b2 }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x08b2 }
                java.lang.reflect.Method r2 = r2.getMethod(r7, r8)     // Catch:{ Throwable -> 0x08b2 }
                java.lang.reflect.Method r7 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x08b2 }
                r8 = 1
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x08b2 }
                r9 = 0
                r10 = 1
                java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)     // Catch:{ Throwable -> 0x08b2 }
                r8[r9] = r10     // Catch:{ Throwable -> 0x08b2 }
                r7.invoke(r2, r8)     // Catch:{ Throwable -> 0x08b2 }
                r7 = 0
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x08b2 }
                r2.invoke(r5, r7)     // Catch:{ Throwable -> 0x08b2 }
            L_0x0354:
                if (r4 == 0) goto L_0x0381
                r2 = 839237(0xcce45, float:1.176022E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x08af }
                r5 = 1048705(0x100081, float:1.469549E-39)
                java.lang.String r5 = liwe.wngzla.ykubt.f.a(r5)     // Catch:{ Throwable -> 0x08af }
                r7 = 0
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x08af }
                java.lang.reflect.Method r2 = r2.getMethod(r5, r7)     // Catch:{ Throwable -> 0x08af }
                java.lang.reflect.Method r5 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x08af }
                r7 = 1
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x08af }
                r8 = 0
                r9 = 1
                java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)     // Catch:{ Throwable -> 0x08af }
                r7[r8] = r9     // Catch:{ Throwable -> 0x08af }
                r5.invoke(r2, r7)     // Catch:{ Throwable -> 0x08af }
                r5 = 0
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x08af }
                r2.invoke(r4, r5)     // Catch:{ Throwable -> 0x08af }
            L_0x0381:
                if (r3 == 0) goto L_0x03ae
                r2 = 839163(0xccdfb, float:1.175918E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x08ac }
                r4 = 1048705(0x100081, float:1.469549E-39)
                java.lang.String r4 = liwe.wngzla.ykubt.f.a(r4)     // Catch:{ Throwable -> 0x08ac }
                r5 = 0
                java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x08ac }
                java.lang.reflect.Method r2 = r2.getMethod(r4, r5)     // Catch:{ Throwable -> 0x08ac }
                java.lang.reflect.Method r4 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x08ac }
                r5 = 1
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x08ac }
                r7 = 0
                r8 = 1
                java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch:{ Throwable -> 0x08ac }
                r5[r7] = r8     // Catch:{ Throwable -> 0x08ac }
                r4.invoke(r2, r5)     // Catch:{ Throwable -> 0x08ac }
                r4 = 0
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x08ac }
                r2.invoke(r3, r4)     // Catch:{ Throwable -> 0x08ac }
            L_0x03ae:
                if (r6 == 0) goto L_0x03db
                r2 = 839238(0xcce46, float:1.176023E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x08a9 }
                r3 = 1048793(0x1000d9, float:1.469672E-39)
                java.lang.String r3 = liwe.wngzla.ykubt.f.a(r3)     // Catch:{ Throwable -> 0x08a9 }
                r4 = 0
                java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x08a9 }
                java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x08a9 }
                java.lang.reflect.Method r3 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x08a9 }
                r4 = 1
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x08a9 }
                r5 = 0
                r7 = 1
                java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ Throwable -> 0x08a9 }
                r4[r5] = r7     // Catch:{ Throwable -> 0x08a9 }
                r3.invoke(r2, r4)     // Catch:{ Throwable -> 0x08a9 }
                r3 = 0
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x08a9 }
                r2.invoke(r6, r3)     // Catch:{ Throwable -> 0x08a9 }
            L_0x03db:
                r0 = r22
                int r2 = r0.c
                if (r2 == 0) goto L_0x03e9
                r0 = r22
                int r2 = r0.c
                r3 = 200(0xc8, float:2.8E-43)
                if (r2 == r3) goto L_0x0682
            L_0x03e9:
                r22.a()
                r2 = 0
            L_0x03ed:
                return r2
            L_0x03ee:
                r2 = 839163(0xccdfb, float:1.175918E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Exception -> 0x031d }
                r7 = 1048795(0x1000db, float:1.469675E-39)
                java.lang.String r7 = liwe.wngzla.ykubt.f.a(r7)     // Catch:{ Exception -> 0x031d }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r2 = r2.getMethod(r7, r8)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r7 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r8 = 1
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x031d }
                r9 = 0
                r10 = 1
                java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)     // Catch:{ Exception -> 0x031d }
                r8[r9] = r10     // Catch:{ Exception -> 0x031d }
                r7.invoke(r2, r8)     // Catch:{ Exception -> 0x031d }
                r7 = 839154(0xccdf2, float:1.175905E-39)
                java.lang.Class r7 = liwe.wngzla.ykubt.g.b(r7)     // Catch:{ Exception -> 0x031d }
                r8 = 1048838(0x100106, float:1.469735E-39)
                java.lang.String r8 = liwe.wngzla.ykubt.f.a(r8)     // Catch:{ Exception -> 0x031d }
                r9 = 2
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r11 = 839141(0xccde5, float:1.175887E-39)
                java.lang.Class r11 = liwe.wngzla.ykubt.g.b(r11)     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                r10 = 1
                java.lang.Class<?> r11 = liwe.wngzla.ykubt.f.n     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r7 = r7.getMethod(r8, r9)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r8 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r9 = 1
                java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r11 = 1
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r11)     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                r8.invoke(r7, r9)     // Catch:{ Exception -> 0x031d }
                r8 = 839141(0xccde5, float:1.175887E-39)
                java.lang.Class r8 = liwe.wngzla.ykubt.g.b(r8)     // Catch:{ Exception -> 0x031d }
                r9 = 2
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x031d }
                r10 = 0
                r11 = 839183(0xcce0f, float:1.175946E-39)
                java.lang.Class r11 = liwe.wngzla.ykubt.g.b(r11)     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                r10 = 1
                r11 = 839141(0xccde5, float:1.175887E-39)
                java.lang.Class r11 = liwe.wngzla.ykubt.g.b(r11)     // Catch:{ Exception -> 0x031d }
                r9[r10] = r11     // Catch:{ Exception -> 0x031d }
                java.lang.Object r8 = liwe.wngzla.ykubt.g.a(r8, r9)     // Catch:{ Exception -> 0x031d }
                r9 = 839232(0xcce40, float:1.176015E-39)
                java.lang.Class r9 = liwe.wngzla.ykubt.g.b(r9)     // Catch:{ Exception -> 0x031d }
                r10 = 1048716(0x10008c, float:1.469564E-39)
                java.lang.String r10 = liwe.wngzla.ykubt.f.a(r10)     // Catch:{ Exception -> 0x031d }
                r11 = 1
                java.lang.Class[] r11 = new java.lang.Class[r11]     // Catch:{ Exception -> 0x031d }
                r12 = 0
                r13 = 839179(0xcce0b, float:1.17594E-39)
                java.lang.Class r13 = liwe.wngzla.ykubt.g.b(r13)     // Catch:{ Exception -> 0x031d }
                r11[r12] = r13     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r9 = r9.getMethod(r10, r11)     // Catch:{ Exception -> 0x031d }
                java.lang.reflect.Method r10 = liwe.wngzla.ykubt.f.p     // Catch:{ Exception -> 0x031d }
                r11 = 1
                java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ Exception -> 0x031d }
                r12 = 0
                r13 = 1
                java.lang.Boolean r13 = java.lang.Boolean.valueOf(r13)     // Catch:{ Exception -> 0x031d }
                r11[r12] = r13     // Catch:{ Exception -> 0x031d }
                r10.invoke(r9, r11)     // Catch:{ Exception -> 0x031d }
                r10 = 1
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x031d }
                r11 = 0
                r12 = 2
                java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x031d }
                r13 = 0
                r14 = 0
                r15 = 2
                java.lang.Object[] r15 = new java.lang.Object[r15]     // Catch:{ Exception -> 0x031d }
                r16 = 0
                r17 = 1
                r0 = r17
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x031d }
                r17 = r0
                r18 = 0
                r19 = 2
                r0 = r19
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x031d }
                r19 = r0
                r20 = 0
                r21 = 0
                r0 = r21
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x031d }
                r21 = r0
                r0 = r21
                java.lang.Object r2 = r2.invoke(r3, r0)     // Catch:{ Exception -> 0x031d }
                r19[r20] = r2     // Catch:{ Exception -> 0x031d }
                r2 = 1
                r20 = 1048648(0x100048, float:1.469469E-39)
                java.lang.String r20 = liwe.wngzla.ykubt.f.a(r20)     // Catch:{ Exception -> 0x031d }
                r19[r2] = r20     // Catch:{ Exception -> 0x031d }
                r17[r18] = r19     // Catch:{ Exception -> 0x031d }
                r0 = r17
                java.lang.Object r2 = r9.invoke(r8, r0)     // Catch:{ Exception -> 0x031d }
                r15[r16] = r2     // Catch:{ Exception -> 0x031d }
                r2 = 1
                r16 = 0
                java.lang.Integer r16 = java.lang.Integer.valueOf(r16)     // Catch:{ Exception -> 0x031d }
                r15[r2] = r16     // Catch:{ Exception -> 0x031d }
                java.lang.Object r2 = r7.invoke(r14, r15)     // Catch:{ Exception -> 0x031d }
                byte[] r2 = (byte[]) r2     // Catch:{ Exception -> 0x031d }
                byte[] r2 = (byte[]) r2     // Catch:{ Exception -> 0x031d }
                r7 = 1048581(0x100005, float:1.469375E-39)
                java.lang.String r7 = liwe.wngzla.ykubt.f.a(r7)     // Catch:{ Exception -> 0x031d }
                byte[] r2 = liwe.wngzla.ykubt.j.a(r2, r7)     // Catch:{ Exception -> 0x031d }
                r12[r13] = r2     // Catch:{ Exception -> 0x031d }
                r2 = 1
                r7 = 1048648(0x100048, float:1.469469E-39)
                java.lang.String r7 = liwe.wngzla.ykubt.f.a(r7)     // Catch:{ Exception -> 0x031d }
                r12[r2] = r7     // Catch:{ Exception -> 0x031d }
                r10[r11] = r12     // Catch:{ Exception -> 0x031d }
                java.lang.Object r2 = r9.invoke(r8, r10)     // Catch:{ Exception -> 0x031d }
                r0 = r22
                r0.b = r2     // Catch:{ Exception -> 0x031d }
            L_0x0510:
                r2 = 0
                liwe.wngzla.ykubt.h.a = r2     // Catch:{ Exception -> 0x031d }
                if (r5 == 0) goto L_0x0540
                r2 = 839236(0xcce44, float:1.17602E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x08bb }
                r7 = 1048705(0x100081, float:1.469549E-39)
                java.lang.String r7 = liwe.wngzla.ykubt.f.a(r7)     // Catch:{ Throwable -> 0x08bb }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x08bb }
                java.lang.reflect.Method r2 = r2.getMethod(r7, r8)     // Catch:{ Throwable -> 0x08bb }
                java.lang.reflect.Method r7 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x08bb }
                r8 = 1
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x08bb }
                r9 = 0
                r10 = 1
                java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)     // Catch:{ Throwable -> 0x08bb }
                r8[r9] = r10     // Catch:{ Throwable -> 0x08bb }
                r7.invoke(r2, r8)     // Catch:{ Throwable -> 0x08bb }
                r7 = 0
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x08bb }
                r2.invoke(r5, r7)     // Catch:{ Throwable -> 0x08bb }
            L_0x0540:
                if (r4 == 0) goto L_0x056d
                r2 = 839237(0xcce45, float:1.176022E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x08b8 }
                r5 = 1048705(0x100081, float:1.469549E-39)
                java.lang.String r5 = liwe.wngzla.ykubt.f.a(r5)     // Catch:{ Throwable -> 0x08b8 }
                r7 = 0
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x08b8 }
                java.lang.reflect.Method r2 = r2.getMethod(r5, r7)     // Catch:{ Throwable -> 0x08b8 }
                java.lang.reflect.Method r5 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x08b8 }
                r7 = 1
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x08b8 }
                r8 = 0
                r9 = 1
                java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)     // Catch:{ Throwable -> 0x08b8 }
                r7[r8] = r9     // Catch:{ Throwable -> 0x08b8 }
                r5.invoke(r2, r7)     // Catch:{ Throwable -> 0x08b8 }
                r5 = 0
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x08b8 }
                r2.invoke(r4, r5)     // Catch:{ Throwable -> 0x08b8 }
            L_0x056d:
                if (r3 == 0) goto L_0x059a
                r2 = 839163(0xccdfb, float:1.175918E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x08b5 }
                r4 = 1048705(0x100081, float:1.469549E-39)
                java.lang.String r4 = liwe.wngzla.ykubt.f.a(r4)     // Catch:{ Throwable -> 0x08b5 }
                r5 = 0
                java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x08b5 }
                java.lang.reflect.Method r2 = r2.getMethod(r4, r5)     // Catch:{ Throwable -> 0x08b5 }
                java.lang.reflect.Method r4 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x08b5 }
                r5 = 1
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x08b5 }
                r7 = 0
                r8 = 1
                java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch:{ Throwable -> 0x08b5 }
                r5[r7] = r8     // Catch:{ Throwable -> 0x08b5 }
                r4.invoke(r2, r5)     // Catch:{ Throwable -> 0x08b5 }
                r4 = 0
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x08b5 }
                r2.invoke(r3, r4)     // Catch:{ Throwable -> 0x08b5 }
            L_0x059a:
                if (r6 == 0) goto L_0x03db
                r2 = 839238(0xcce46, float:1.176023E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x05c9 }
                r3 = 1048793(0x1000d9, float:1.469672E-39)
                java.lang.String r3 = liwe.wngzla.ykubt.f.a(r3)     // Catch:{ Throwable -> 0x05c9 }
                r4 = 0
                java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x05c9 }
                java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x05c9 }
                java.lang.reflect.Method r3 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x05c9 }
                r4 = 1
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x05c9 }
                r5 = 0
                r7 = 1
                java.lang.Boolean r7 = java.lang.Boolean.valueOf(r7)     // Catch:{ Throwable -> 0x05c9 }
                r4[r5] = r7     // Catch:{ Throwable -> 0x05c9 }
                r3.invoke(r2, r4)     // Catch:{ Throwable -> 0x05c9 }
                r3 = 0
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x05c9 }
                r2.invoke(r6, r3)     // Catch:{ Throwable -> 0x05c9 }
                goto L_0x03db
            L_0x05c9:
                r2 = move-exception
                goto L_0x03db
            L_0x05cc:
                r2 = move-exception
                if (r5 == 0) goto L_0x05fa
                r7 = 839236(0xcce44, float:1.17602E-39)
                java.lang.Class r7 = liwe.wngzla.ykubt.g.b(r7)     // Catch:{ Throwable -> 0x08a6 }
                r8 = 1048705(0x100081, float:1.469549E-39)
                java.lang.String r8 = liwe.wngzla.ykubt.f.a(r8)     // Catch:{ Throwable -> 0x08a6 }
                r9 = 0
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Throwable -> 0x08a6 }
                java.lang.reflect.Method r7 = r7.getMethod(r8, r9)     // Catch:{ Throwable -> 0x08a6 }
                java.lang.reflect.Method r8 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x08a6 }
                r9 = 1
                java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Throwable -> 0x08a6 }
                r10 = 0
                r11 = 1
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r11)     // Catch:{ Throwable -> 0x08a6 }
                r9[r10] = r11     // Catch:{ Throwable -> 0x08a6 }
                r8.invoke(r7, r9)     // Catch:{ Throwable -> 0x08a6 }
                r8 = 0
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x08a6 }
                r7.invoke(r5, r8)     // Catch:{ Throwable -> 0x08a6 }
            L_0x05fa:
                if (r4 == 0) goto L_0x0627
                r5 = 839237(0xcce45, float:1.176022E-39)
                java.lang.Class r5 = liwe.wngzla.ykubt.g.b(r5)     // Catch:{ Throwable -> 0x08a3 }
                r7 = 1048705(0x100081, float:1.469549E-39)
                java.lang.String r7 = liwe.wngzla.ykubt.f.a(r7)     // Catch:{ Throwable -> 0x08a3 }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x08a3 }
                java.lang.reflect.Method r5 = r5.getMethod(r7, r8)     // Catch:{ Throwable -> 0x08a3 }
                java.lang.reflect.Method r7 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x08a3 }
                r8 = 1
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x08a3 }
                r9 = 0
                r10 = 1
                java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)     // Catch:{ Throwable -> 0x08a3 }
                r8[r9] = r10     // Catch:{ Throwable -> 0x08a3 }
                r7.invoke(r5, r8)     // Catch:{ Throwable -> 0x08a3 }
                r7 = 0
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x08a3 }
                r5.invoke(r4, r7)     // Catch:{ Throwable -> 0x08a3 }
            L_0x0627:
                if (r3 == 0) goto L_0x0654
                r4 = 839163(0xccdfb, float:1.175918E-39)
                java.lang.Class r4 = liwe.wngzla.ykubt.g.b(r4)     // Catch:{ Throwable -> 0x08a0 }
                r5 = 1048705(0x100081, float:1.469549E-39)
                java.lang.String r5 = liwe.wngzla.ykubt.f.a(r5)     // Catch:{ Throwable -> 0x08a0 }
                r7 = 0
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x08a0 }
                java.lang.reflect.Method r4 = r4.getMethod(r5, r7)     // Catch:{ Throwable -> 0x08a0 }
                java.lang.reflect.Method r5 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x08a0 }
                r7 = 1
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x08a0 }
                r8 = 0
                r9 = 1
                java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)     // Catch:{ Throwable -> 0x08a0 }
                r7[r8] = r9     // Catch:{ Throwable -> 0x08a0 }
                r5.invoke(r4, r7)     // Catch:{ Throwable -> 0x08a0 }
                r5 = 0
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x08a0 }
                r4.invoke(r3, r5)     // Catch:{ Throwable -> 0x08a0 }
            L_0x0654:
                if (r6 == 0) goto L_0x0681
                r3 = 839238(0xcce46, float:1.176023E-39)
                java.lang.Class r3 = liwe.wngzla.ykubt.g.b(r3)     // Catch:{ Throwable -> 0x089d }
                r4 = 1048793(0x1000d9, float:1.469672E-39)
                java.lang.String r4 = liwe.wngzla.ykubt.f.a(r4)     // Catch:{ Throwable -> 0x089d }
                r5 = 0
                java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x089d }
                java.lang.reflect.Method r3 = r3.getMethod(r4, r5)     // Catch:{ Throwable -> 0x089d }
                java.lang.reflect.Method r4 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x089d }
                r5 = 1
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x089d }
                r7 = 0
                r8 = 1
                java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch:{ Throwable -> 0x089d }
                r5[r7] = r8     // Catch:{ Throwable -> 0x089d }
                r4.invoke(r3, r5)     // Catch:{ Throwable -> 0x089d }
                r4 = 0
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x089d }
                r3.invoke(r6, r4)     // Catch:{ Throwable -> 0x089d }
            L_0x0681:
                throw r2
            L_0x0682:
                r0 = r22
                int r2 = r0.d
                r3 = 1
                if (r2 != r3) goto L_0x06c1
                r2 = 839146(0xccdea, float:1.175894E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x089a }
                r3 = 1048744(0x1000a8, float:1.469603E-39)
                java.lang.String r3 = liwe.wngzla.ykubt.f.a(r3)     // Catch:{ Throwable -> 0x089a }
                r4 = 0
                java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x089a }
                java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x089a }
                java.lang.reflect.Method r3 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x089a }
                r4 = 1
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x089a }
                r5 = 0
                r6 = 1
                java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ Throwable -> 0x089a }
                r4[r5] = r6     // Catch:{ Throwable -> 0x089a }
                r3.invoke(r2, r4)     // Catch:{ Throwable -> 0x089a }
                r3 = 0
                r4 = 0
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x089a }
                java.lang.Object r2 = r2.invoke(r3, r4)     // Catch:{ Throwable -> 0x089a }
                java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ Throwable -> 0x089a }
                long r2 = r2.longValue()     // Catch:{ Throwable -> 0x089a }
                liwe.wngzla.ykubt.h.c = r2     // Catch:{ Throwable -> 0x089a }
            L_0x06be:
                r2 = 0
                liwe.wngzla.ykubt.h.b = r2
            L_0x06c1:
                r0 = r22
                java.lang.Object r2 = r0.b
                if (r2 == 0) goto L_0x0897
                r2 = 1048865(0x100121, float:1.469773E-39)
                java.lang.String r2 = liwe.wngzla.ykubt.f.a(r2)     // Catch:{ Throwable -> 0x0896 }
                java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Throwable -> 0x0896 }
                r3 = 1
                java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x0896 }
                r4 = 0
                r5 = 839141(0xccde5, float:1.175887E-39)
                java.lang.Class r5 = liwe.wngzla.ykubt.g.b(r5)     // Catch:{ Throwable -> 0x0896 }
                r3[r4] = r5     // Catch:{ Throwable -> 0x0896 }
                java.lang.reflect.Constructor r2 = r2.getConstructor(r3)     // Catch:{ Throwable -> 0x0896 }
                r3 = 1
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0896 }
                r4 = 0
                r0 = r22
                java.lang.Object r5 = r0.b     // Catch:{ Throwable -> 0x0896 }
                r3[r4] = r5     // Catch:{ Throwable -> 0x0896 }
                java.lang.Object r4 = r2.newInstance(r3)     // Catch:{ Throwable -> 0x0896 }
                r2 = 0
                r3 = r2
            L_0x06f3:
                r2 = 839150(0xccdee, float:1.1759E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x0896 }
                r5 = 1048704(0x100080, float:1.469547E-39)
                java.lang.String r5 = liwe.wngzla.ykubt.f.a(r5)     // Catch:{ Throwable -> 0x0896 }
                r6 = 0
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x0896 }
                java.lang.reflect.Method r2 = r2.getMethod(r5, r6)     // Catch:{ Throwable -> 0x0896 }
                r5 = 0
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0896 }
                java.lang.Object r2 = r2.invoke(r4, r5)     // Catch:{ Throwable -> 0x0896 }
                java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Throwable -> 0x0896 }
                int r2 = r2.intValue()     // Catch:{ Throwable -> 0x0896 }
                if (r3 >= r2) goto L_0x0897
                r2 = 1048865(0x100121, float:1.469773E-39)
                java.lang.String r2 = liwe.wngzla.ykubt.f.a(r2)     // Catch:{ Throwable -> 0x0896 }
                java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Throwable -> 0x0896 }
                r5 = 1048756(0x1000b4, float:1.46962E-39)
                java.lang.String r5 = liwe.wngzla.ykubt.f.a(r5)     // Catch:{ Throwable -> 0x0896 }
                r6 = 1
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x0896 }
                r7 = 0
                java.lang.Class<?> r8 = liwe.wngzla.ykubt.f.n     // Catch:{ Throwable -> 0x0896 }
                r6[r7] = r8     // Catch:{ Throwable -> 0x0896 }
                java.lang.reflect.Method r2 = r2.getMethod(r5, r6)     // Catch:{ Throwable -> 0x0896 }
                java.lang.reflect.Method r5 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x0896 }
                r6 = 1
                java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x0896 }
                r7 = 0
                r8 = 1
                java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch:{ Throwable -> 0x0896 }
                r6[r7] = r8     // Catch:{ Throwable -> 0x0896 }
                r5.invoke(r2, r6)     // Catch:{ Throwable -> 0x0896 }
                r5 = 1
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0896 }
                r6 = 0
                java.lang.Integer r7 = java.lang.Integer.valueOf(r3)     // Catch:{ Throwable -> 0x0896 }
                r5[r6] = r7     // Catch:{ Throwable -> 0x0896 }
                java.lang.Object r5 = r2.invoke(r4, r5)     // Catch:{ Throwable -> 0x0896 }
                r2 = 839151(0xccdef, float:1.175901E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x0896 }
                r6 = 1048757(0x1000b5, float:1.469622E-39)
                java.lang.String r6 = liwe.wngzla.ykubt.f.a(r6)     // Catch:{ Throwable -> 0x0896 }
                r7 = 1
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x0896 }
                r8 = 0
                r9 = 839141(0xccde5, float:1.175887E-39)
                java.lang.Class r9 = liwe.wngzla.ykubt.g.b(r9)     // Catch:{ Throwable -> 0x0896 }
                r7[r8] = r9     // Catch:{ Throwable -> 0x0896 }
                java.lang.reflect.Method r6 = r2.getMethod(r6, r7)     // Catch:{ Throwable -> 0x0896 }
                java.lang.reflect.Method r2 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x0896 }
                r7 = 1
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x0896 }
                r8 = 0
                r9 = 1
                java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)     // Catch:{ Throwable -> 0x0896 }
                r7[r8] = r9     // Catch:{ Throwable -> 0x0896 }
                r2.invoke(r6, r7)     // Catch:{ Throwable -> 0x0896 }
                r2 = 839151(0xccdef, float:1.175901E-39)
                java.lang.Class r2 = liwe.wngzla.ykubt.g.b(r2)     // Catch:{ Throwable -> 0x0896 }
                r7 = 1048756(0x1000b4, float:1.46962E-39)
                java.lang.String r7 = liwe.wngzla.ykubt.f.a(r7)     // Catch:{ Throwable -> 0x0896 }
                r8 = 1
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0896 }
                r9 = 0
                r10 = 839141(0xccde5, float:1.175887E-39)
                java.lang.Class r10 = liwe.wngzla.ykubt.g.b(r10)     // Catch:{ Throwable -> 0x0896 }
                r8[r9] = r10     // Catch:{ Throwable -> 0x0896 }
                java.lang.reflect.Method r7 = r2.getMethod(r7, r8)     // Catch:{ Throwable -> 0x0896 }
                java.lang.reflect.Method r2 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x0896 }
                r8 = 1
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0896 }
                r9 = 0
                r10 = 1
                java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)     // Catch:{ Throwable -> 0x0896 }
                r8[r9] = r10     // Catch:{ Throwable -> 0x0896 }
                r2.invoke(r7, r8)     // Catch:{ Throwable -> 0x0896 }
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0896 }
                r8 = 0
                r9 = 1048634(0x10003a, float:1.469449E-39)
                java.lang.String r9 = liwe.wngzla.ykubt.f.a(r9)     // Catch:{ Throwable -> 0x0896 }
                r2[r8] = r9     // Catch:{ Throwable -> 0x0896 }
                java.lang.Object r2 = r6.invoke(r5, r2)     // Catch:{ Throwable -> 0x0896 }
                java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Throwable -> 0x0896 }
                int r2 = r2.intValue()     // Catch:{ Throwable -> 0x0896 }
                if (r2 != 0) goto L_0x07cf
            L_0x07ca:
                int r2 = r3 + 1
                r3 = r2
                goto L_0x06f3
            L_0x07cf:
                r8 = 839190(0xcce16, float:1.175956E-39)
                java.lang.Class r8 = liwe.wngzla.ykubt.g.b(r8)     // Catch:{ Throwable -> 0x0896 }
                r9 = 1048730(0x10009a, float:1.469584E-39)
                java.lang.String r9 = liwe.wngzla.ykubt.f.a(r9)     // Catch:{ Throwable -> 0x0896 }
                r10 = 2
                java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Throwable -> 0x0896 }
                r11 = 0
                r12 = 839143(0xccde7, float:1.17589E-39)
                java.lang.Class r12 = liwe.wngzla.ykubt.g.b(r12)     // Catch:{ Throwable -> 0x0896 }
                r10[r11] = r12     // Catch:{ Throwable -> 0x0896 }
                r11 = 1
                java.lang.Class<?> r12 = liwe.wngzla.ykubt.f.n     // Catch:{ Throwable -> 0x0896 }
                r10[r11] = r12     // Catch:{ Throwable -> 0x0896 }
                java.lang.reflect.Method r8 = r8.getMethod(r9, r10)     // Catch:{ Throwable -> 0x0896 }
                java.lang.reflect.Method r9 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x0896 }
                r10 = 1
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Throwable -> 0x0896 }
                r11 = 0
                r12 = 1
                java.lang.Boolean r12 = java.lang.Boolean.valueOf(r12)     // Catch:{ Throwable -> 0x0896 }
                r10[r11] = r12     // Catch:{ Throwable -> 0x0896 }
                r9.invoke(r8, r10)     // Catch:{ Throwable -> 0x0896 }
                liwe.wngzla.ykubt.a r8 = new liwe.wngzla.ykubt.a     // Catch:{ Throwable -> 0x0896 }
                r8.<init>()     // Catch:{ Throwable -> 0x0896 }
                r8.a = r2     // Catch:{ Throwable -> 0x0896 }
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0896 }
                r9 = 0
                r10 = 1048642(0x100042, float:1.46946E-39)
                java.lang.String r10 = liwe.wngzla.ykubt.f.a(r10)     // Catch:{ Throwable -> 0x0896 }
                r2[r9] = r10     // Catch:{ Throwable -> 0x0896 }
                java.lang.Object r2 = r6.invoke(r5, r2)     // Catch:{ Throwable -> 0x0896 }
                java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Throwable -> 0x0896 }
                int r2 = r2.intValue()     // Catch:{ Throwable -> 0x0896 }
                r8.b = r2     // Catch:{ Throwable -> 0x0896 }
                java.lang.Class<liwe.wngzla.ykubt.a> r2 = liwe.wngzla.ykubt.a.class
                r6 = 1048842(0x10010a, float:1.46974E-39)
                java.lang.String r6 = liwe.wngzla.ykubt.f.a(r6)     // Catch:{ Throwable -> 0x0893 }
                r9 = 2
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Throwable -> 0x0893 }
                r10 = 0
                r11 = 839195(0xcce1b, float:1.175963E-39)
                java.lang.Class r11 = liwe.wngzla.ykubt.g.b(r11)     // Catch:{ Throwable -> 0x0893 }
                r9[r10] = r11     // Catch:{ Throwable -> 0x0893 }
                r10 = 1
                r11 = 839179(0xcce0b, float:1.17594E-39)
                java.lang.Class r11 = liwe.wngzla.ykubt.g.b(r11)     // Catch:{ Throwable -> 0x0893 }
                r9[r10] = r11     // Catch:{ Throwable -> 0x0893 }
                java.lang.reflect.Method r2 = r2.getMethod(r6, r9)     // Catch:{ Throwable -> 0x0893 }
                java.lang.reflect.Method r6 = liwe.wngzla.ykubt.f.p     // Catch:{ Throwable -> 0x0893 }
                r9 = 1
                java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Throwable -> 0x0893 }
                r10 = 0
                r11 = 1
                java.lang.Boolean r11 = java.lang.Boolean.valueOf(r11)     // Catch:{ Throwable -> 0x0893 }
                r9[r10] = r11     // Catch:{ Throwable -> 0x0893 }
                r6.invoke(r2, r9)     // Catch:{ Throwable -> 0x0893 }
                r6 = 2
                java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x0893 }
                r9 = 0
                r10 = 839191(0xcce17, float:1.175957E-39)
                java.lang.Class r10 = liwe.wngzla.ykubt.g.b(r10)     // Catch:{ Throwable -> 0x0893 }
                r11 = 1048978(0x100192, float:1.469931E-39)
                java.lang.String r11 = liwe.wngzla.ykubt.f.a(r11)     // Catch:{ Throwable -> 0x0893 }
                java.lang.reflect.Field r10 = r10.getField(r11)     // Catch:{ Throwable -> 0x0893 }
                r11 = 0
                java.lang.Object r10 = r10.get(r11)     // Catch:{ Throwable -> 0x0893 }
                r6[r9] = r10     // Catch:{ Throwable -> 0x0893 }
                r9 = 1
                r10 = 1
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Throwable -> 0x0893 }
                r11 = 0
                r12 = 1
                java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Throwable -> 0x0893 }
                r13 = 0
                r14 = 1048644(0x100044, float:1.469463E-39)
                java.lang.String r14 = liwe.wngzla.ykubt.f.a(r14)     // Catch:{ Throwable -> 0x0893 }
                r12[r13] = r14     // Catch:{ Throwable -> 0x0893 }
                java.lang.Object r5 = r7.invoke(r5, r12)     // Catch:{ Throwable -> 0x0893 }
                r10[r11] = r5     // Catch:{ Throwable -> 0x0893 }
                r6[r9] = r10     // Catch:{ Throwable -> 0x0893 }
                r2.invoke(r8, r6)     // Catch:{ Throwable -> 0x0893 }
                goto L_0x07ca
            L_0x0893:
                r2 = move-exception
                goto L_0x07ca
            L_0x0896:
                r2 = move-exception
            L_0x0897:
                r2 = 0
                goto L_0x03ed
            L_0x089a:
                r2 = move-exception
                goto L_0x06be
            L_0x089d:
                r3 = move-exception
                goto L_0x0681
            L_0x08a0:
                r3 = move-exception
                goto L_0x0654
            L_0x08a3:
                r4 = move-exception
                goto L_0x0627
            L_0x08a6:
                r5 = move-exception
                goto L_0x05fa
            L_0x08a9:
                r2 = move-exception
                goto L_0x03db
            L_0x08ac:
                r2 = move-exception
                goto L_0x03ae
            L_0x08af:
                r2 = move-exception
                goto L_0x0381
            L_0x08b2:
                r2 = move-exception
                goto L_0x0354
            L_0x08b5:
                r2 = move-exception
                goto L_0x059a
            L_0x08b8:
                r2 = move-exception
                goto L_0x056d
            L_0x08bb:
                r2 = move-exception
                goto L_0x0540
            */
            throw new UnsupportedOperationException("Method not decompiled: liwe.wngzla.ykubt.h.a.doInBackground(java.lang.Object[]):java.lang.Void");
        }

        private void a() {
            if (this.d == 1) {
                h.b = false;
            } else if (this.d == 2 || this.d == 5) {
                h.c(this.d, this.a);
            }
        }
    }
}
