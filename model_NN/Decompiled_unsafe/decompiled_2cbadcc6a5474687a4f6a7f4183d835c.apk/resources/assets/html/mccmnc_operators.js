mccmnc_operators = {
  "206": {
    "country": "be",
    "operators": {}
  },
  "208": {
    "country": "fr",
    "operators": {}
  },
  "246": {
    "country": "lt",
    "operators": {}
  },
  "247": {
    "country": "lv",
    "operators": {}
  },
  "250": {
    "country": "ru",
    "operators": {
      "12": "baikalwestcom",
      "17": "uralsvjazinform",
      "20": "tele2",
      "28": "beeline",
      "50": "mts",
      "99": "beeline",
      "01": "mts",
      "02": "megafon"
    }
  },
  "255": {
    "country": "ua",
    "operators": {
      "01": "mts",
      "06": "life",
      "07": "3mob"
    }
  },
  "257": {
    "country": "by",
    "operators": {
      "01": "velcom",
      "02": "mts"
    }
  },
  "259": {
    "country": "md",
    "operators": {}
  },
  "262": {
    "country": "de",
    "operators": {}
  },
  "282": {
    "country": "ge",
    "operators": {}
  },
  "283": {
    "country": "am",
    "operators": {}
  },
  "289": {
    "country": "ab",
    "operators": {}
  },
  "400": {
    "country": "az",
    "operators": {}
  },
  "401": {
    "country": "kz",
    "operators": {}
  },
  "437": {
    "country": "kg",
    "operators": {}
  }
}