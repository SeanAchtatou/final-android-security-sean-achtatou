package r;

public class r {
    private byte[] a;
    private byte[] b;
    private byte[] c;

    public r(byte[] bArr) {
        this.c = bArr;
        if (bArr.length < 1 || bArr.length > 256) {
            throw new IllegalArgumentException("key must be between 1 and 256 bytes");
        }
    }

    private String a(String str) {
        return str.replaceAll("\\\\n", "\\\n").replaceAll("\\\\r", "\\\r").replaceAll("\\\\t", "\\\t");
    }

    private void a() {
        this.a = new byte[256];
        this.b = new byte[256];
        for (int i = 0; i < 256; i++) {
            this.a[i] = (byte) i;
            this.b[i] = this.c[i % this.c.length];
        }
        byte b2 = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            b2 = (b2 + this.a[i2] + this.b[i2]) & 255;
            byte[] bArr = this.a;
            bArr[i2] = (byte) (bArr[i2] ^ this.a[b2]);
            byte[] bArr2 = this.a;
            bArr2[b2] = (byte) (bArr2[b2] ^ this.a[i2]);
            byte[] bArr3 = this.a;
            bArr3[i2] = (byte) (bArr3[i2] ^ this.a[b2]);
        }
    }

    public static String d(String str) {
        try {
            int length = (str.length() - 40) / 2;
            byte[] a2 = a.a(str.substring(length, length + 40), 16);
            byte[] a3 = a.a(str.substring(0, length) + str.substring(length + 40), 16);
            r rVar = new r(a2);
            return rVar.a(new String(rVar.encrypt(a3)));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public byte[] decrypt(byte[] bArr) {
        return encrypt(bArr);
    }

    public byte[] encrypt(byte[] bArr) {
        a();
        byte[] bArr2 = new byte[bArr.length];
        byte b2 = 0;
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            i = (i + 1) & 255;
            b2 = (b2 + this.a[i]) & 255;
            byte[] bArr3 = this.a;
            bArr3[i] = (byte) (bArr3[i] ^ this.a[b2]);
            byte[] bArr4 = this.a;
            bArr4[b2] = (byte) (bArr4[b2] ^ this.a[i]);
            byte[] bArr5 = this.a;
            bArr5[i] = (byte) (bArr5[i] ^ this.a[b2]);
            bArr2[i2] = (byte) (this.a[(this.a[i] + this.a[b2]) & 255] ^ bArr[i2]);
        }
        return bArr2;
    }
}
