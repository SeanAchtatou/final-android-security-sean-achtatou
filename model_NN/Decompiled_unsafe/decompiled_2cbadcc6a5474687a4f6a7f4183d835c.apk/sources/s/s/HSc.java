package s.s;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.PowerManager;
import r.r;

public class HSc extends Service {
    private BroadcastReceiver a = new d(this);
    /* access modifiers changed from: private */
    public ActivityManager b;
    private Runnable c = new e(this);
    private Thread d = null;

    private void a() {
        if (((PowerManager) getSystemService(r.d("kMXdIa2Lj6LutafprC_lbHJ1Rk53_mGh5M-pNVqK0cWL8Ww="))).isScreenOn()) {
            b();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.d == null || !this.d.isAlive()) {
            this.d = new Thread(this.c);
            this.d.start();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.d != null && this.d.isAlive()) {
            this.d.interrupt();
        }
    }

    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException(r.d("6463v2-6kMxr-X6NK5cUULZF2S3-RmmqyeXdjft33VAY447G2osxW5zeAXHaoNsxRg=="));
    }

    public void onCreate() {
        super.onCreate();
        this.b = (ActivityManager) getSystemService(r.d("EyAKiWOcZMpb4WcK1G5CVprJP-Fyp1_0oVoF8NAc6R_tGgi_gEE="));
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(r.d("P-hlGY_Y1EIQaJDO6nLBN6PGtX8gXnValaFTtQlauxs1zLgtc4PC0lxBSnX8Rme3_cojfVYIJhzVAtB8hZE="));
        intentFilter.addAction(r.d("oB_uVMPlbb6-C79qzDxLGTTitZZNw0hZ7vrskI8szrWzRbhr7rfml1iGZqcT3v75pST1jG91Eg5MbnJfUQ=="));
        registerReceiver(this.a, intentFilter);
        a();
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
