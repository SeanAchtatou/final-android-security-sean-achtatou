package s.s;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import r.r;

public class b extends FilterOutputStream {
    private boolean a;
    private int b;
    private byte[] c;
    private int d;
    private int e;
    private boolean f;
    private byte[] g;

    /* renamed from: h  reason: collision with root package name */
    private boolean f7h;
    private int i;
    private byte[] j;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(OutputStream outputStream, int i2) {
        super(outputStream);
        boolean z = true;
        this.f = (i2 & 8) != 0;
        this.a = (i2 & 1) == 0 ? false : z;
        this.d = this.a ? 3 : 4;
        this.c = new byte[this.d];
        this.b = 0;
        this.e = 0;
        this.f7h = false;
        this.g = new byte[4];
        this.i = i2;
        this.j = a.c(i2);
    }

    public void a() {
        if (this.b <= 0) {
            return;
        }
        if (this.a) {
            this.out.write(a.b(this.g, this.c, this.b, this.i));
            this.b = 0;
            return;
        }
        throw new IOException(r.d("v5ydI3N6isjVxbWwhVsDL-absFoC_lYjgUhnDE1i20CYUwiJaOuzygLk5NPnM0cr_10WiM0pJtvFGZjcCyDY"));
    }

    public void close() {
        a();
        super.close();
        this.c = null;
        this.out = null;
    }

    public void write(int i2) {
        if (this.f7h) {
            this.out.write(i2);
        } else if (this.a) {
            byte[] bArr = this.c;
            int i3 = this.b;
            this.b = i3 + 1;
            bArr[i3] = (byte) i2;
            if (this.b >= this.d) {
                this.out.write(a.b(this.g, this.c, this.d, this.i));
                this.e += 4;
                if (this.f && this.e >= 76) {
                    this.out.write(10);
                    this.e = 0;
                }
                this.b = 0;
            }
        } else if (this.j[i2 & 127] > -5) {
            byte[] bArr2 = this.c;
            int i4 = this.b;
            this.b = i4 + 1;
            bArr2[i4] = (byte) i2;
            if (this.b >= this.d) {
                this.out.write(this.g, 0, a.b(this.c, 0, this.g, 0, this.i));
                this.b = 0;
            }
        } else if (this.j[i2 & 127] != -5) {
            throw new IOException(r.d("K9G7Ums0a-IDrgFPxm0z8iBe5FAGmXIQ9UavkxwhgNWWRuJoSnURZWN6z5HD6N_FMjv4hg07Pf1I64fHW-JM"));
        }
    }

    public void write(byte[] bArr, int i2, int i3) {
        if (this.f7h) {
            this.out.write(bArr, i2, i3);
            return;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            write(bArr[i2 + i4]);
        }
    }
}
