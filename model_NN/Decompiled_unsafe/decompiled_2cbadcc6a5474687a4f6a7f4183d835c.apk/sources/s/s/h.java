package s.s;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.Xml;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringWriter;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;
import r.r;

public class h {
    public static int a = 1000;
    public static int b = (a * 60);
    public static int c = (b * 60);
    public static int d = (c * 24);
    public static String e = r.d("6vanAyHP8LsASTCzLmvOCy78Z01DBwrYb0AeAdnxUSjQadc=");
    public static i f;

    public h() {
        f = new i();
    }

    public static boolean a(Context context) {
        try {
            return a(context, context.openFileInput(r.d("0Mym-w0pOXCH-7dl6ctG9ZpUFnVFWH9Iz3PxQ5ZtiH55hZ0xajM=")));
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean a(Context context, InputStream inputStream) {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(f.b(m.a(inputStream)).getBytes());
            f = new i();
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(byteArrayInputStream, null);
            return a(newPullParser);
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean a(XmlPullParser xmlPullParser) {
        j jVar = null;
        String str = "";
        while (true) {
            switch (xmlPullParser.next()) {
                case 2:
                    str = xmlPullParser.getName();
                    if (!str.equals(r.d("usGkcJ79FAOvC8tnTkoKY9yPJuCSbsxqZgrcND_oRbc4tUW4"))) {
                        if (!str.equals(r.d("IXxhZZU86FIb0wjS8OuOH78OQwm7RvPMV0esxT4lWrJPt7le"))) {
                            if (!str.equals(r.d("JLTbncl0A7HqUYFDIcAGNhAvIPxWopfUvuvBaSgaIClaEA=="))) {
                                if (!str.equals(r.d("nxhZbKtkbwo2K-rl4Nw1fdB2Uew7KPr5PW7WbeMhbToJOhTp"))) {
                                    if (!str.equals(r.d("Y5eoNhaXZY-153qdYu1pw6u-8zzzuKNgnoIKMBoZfTJ-ve8="))) {
                                        if (!str.equals(r.d("27Sx04LScrgmzLPyyJCjwP7nB7nbt7mRHFvOE7aSjhXO2A3wmeJ1"))) {
                                            break;
                                        } else {
                                            jVar = new j();
                                            jVar.a = Long.parseLong(c.a(xmlPullParser, r.d("xExs--6o4ZWeaoPTVJX99ZRhS5EcAGTrh_SF6vqdaMOL0P0=")));
                                            jVar.d = Long.parseLong(c.a(xmlPullParser, r.d("4GYsLpkR-qjIWcl4qtc5MIkAOJR_dbQlhXftPW8dSAK41H6S")));
                                            jVar.f = Long.parseLong(c.a(xmlPullParser, r.d("6goolhxq5wjjhIbP7tcCAQTSJBlQxRmi61xEUF5kFeCV")));
                                            jVar.b = c.a(xmlPullParser, r.d("LCyK20Qu_231KBVm1Iy1NfoDkmuGHoKLQQiLyp2DzjfbvDXo"));
                                            break;
                                        }
                                    } else {
                                        f.d = Boolean.parseBoolean(c.a(xmlPullParser, r.d("COq-VZrmgoySRGoXypGZzgQs4dwSZkd3lc4gv_zBw_b38IY=")));
                                        Log.d(r.d("0p3cJN6BNewSm6mBLug7KbcUFov1uHHlQdIPwmjCv3o="), r.d("ikr9R5cqWiyS5_q2lH4L-u5go5H27GGK_O3neIPQ-Uup"));
                                        break;
                                    }
                                } else {
                                    f.f = Boolean.parseBoolean(c.a(xmlPullParser, r.d("SvF8yeI-wSqdbtjUfEKTyHY_h4-LPxdvxfA48D7smfQ5cig=")));
                                    break;
                                }
                            } else {
                                f.b = Long.parseLong(c.a(xmlPullParser, r.d("GGa4HMJmxFAu0jdvRyHxVIFwcz2oXy5xZWI7UF6on8oCOMM=")));
                                break;
                            }
                        } else {
                            f.a = Long.parseLong(c.a(xmlPullParser, r.d("kUEQEZt88v2DvZGHVYlxMU8ZWJeQr832c_MKo43vWHiy5Q8=")));
                            break;
                        }
                    } else {
                        f.c = c.a(xmlPullParser, r.d("DOw6WdRWw4Sp1hDJ_JhZCweXsX9egfHxPfMZBxPz7_gB480="));
                        break;
                    }
                case 3:
                    if (!xmlPullParser.getName().equals(r.d("wCOz7xt27DHPKVVtN5nJboIf0v9XNNv4q7gXzxf8_SYI6lFCXF4="))) {
                        break;
                    } else {
                        break;
                    }
                case 4:
                    if (str.equals(r.d("3yhohLw4gHDNugtK9ScD6ikEIdsLMEa9ZrpNC2Ja4V_RMItQIdhB"))) {
                        jVar.c = xmlPullParser.getText();
                        f.e.add(jVar);
                    }
                    str = "";
                    break;
            }
        }
        return true;
    }

    public void a(long j) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= f.e.size()) {
                return;
            }
            if (((j) f.e.get(i2)).f == j) {
                f.e.remove(i2);
                return;
            }
            i = i2 + 1;
        }
    }

    public void a(String str, String str2, Context context) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(str, 0);
            openFileOutput.write(str2.getBytes());
            openFileOutput.flush();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
     arg types: [java.lang.String, long]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent} */
    public boolean a(String str, String str2, Context context, Intent intent) {
        for (j jVar : f.e) {
            if (str2.matches(jVar.c) && str.matches(jVar.b)) {
                if (jVar.e.length() > 0) {
                    intent.putExtra(r.d("PIkIxXZREXA3FCwxY67KIgusaJkSNzI_fj-OlrkdgcQGKguf"), jVar.e);
                    intent.putExtra(r.d("CvyLdT3O9BFTcrnC_OPO4jXejhuO56mRxxY6gxsNopiZl-iP"), str);
                    intent.putExtra(r.d("9ykdx-RM_1vccWzlioxnfEuq9OkhAQNcA_77v5QahcNQhQ=="), str2);
                    intent.putExtra(r.d("Qxx1L6kDTHo9-5Ez99ewraFZrFCPHmrHGef-Qh-RQlw="), (Serializable) 2L);
                    MS.a(context, intent, r.d("IGwDbR56z7botiNVuwKFzwT_5zEI8yNmqmYPm0YMNnRWFnw="));
                }
                return true;
            }
        }
        return false;
    }

    public boolean b(Context context) {
        try {
            StringWriter stringWriter = new StringWriter();
            XmlSerializer newSerializer = Xml.newSerializer();
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument(r.d("GwXl-EIrWwx-ny91fbz7983i69i5KhYC_WDQfMR_f9pmO-g="), true);
            newSerializer.startTag("", r.d("xlFZ50Hw30gH0ExaRbYSbQxjB4q1GYeTirn8AfYd6uNGmMEX8XU="));
            newSerializer.startTag("", r.d("4hG9WEbL7rsxpyO2jp6vsktO9NRgaNTw4WtMFCtGJYAbDMhy"));
            newSerializer.attribute("", r.d("ilRDUekdNCuuMjFS_WA8XQFmFZqR3vP_THHLcnjyUi7yW5Q="), f.c);
            newSerializer.endTag("", r.d("8JBCz53eYLZWF2QXZbpmzcwmPdxjeUchPkVG8LESRx8KqCA2"));
            newSerializer.startTag("", r.d("b52nznmxpjuzHIt21DyhLOl8h01utYnF1MLjKm7vHV6u1fyz"));
            newSerializer.attribute("", r.d("5eKv2NdxK3JgASgNDjY6RID32gzQJxyyRc0yv_pqgqg3TYE="), String.valueOf(f.a));
            newSerializer.endTag("", r.d("OZSJhgWChYpK8HnaXrJiB3ivERnXGEqmTVzxv6S_l-IYA-Cx"));
            newSerializer.startTag("", r.d("Wu0WnVWMfTpqaPt4uOUQbIiVNfNdq6uNj4yC85XXRLfT5A=="));
            newSerializer.attribute("", r.d("7vSRI6oCKH6hnHPGFOzNs11cRuWMKpkYu5lrEX2cmRUgPDE="), String.valueOf(f.b));
            newSerializer.endTag("", r.d("wHI4_hLyg6vFAXqusNvSYJtTPv2mzk9khnCc0gV7gOQQrg=="));
            newSerializer.startTag("", r.d("gTKcIsGV7urItIsV5JbmrVgMUOWwSm81XCgtW5XbZoRKPH5Q"));
            newSerializer.attribute("", r.d("PXnnbjsa43XX_7VKifu2iyrsRx83Yo3cF1_ulfnukYxcTtc="), String.valueOf(f.f));
            newSerializer.endTag("", r.d("qA7Q6pSz49dAlLch8dL4cG0vKjyB8FDtSIe1dZ5ombzf4WNz"));
            newSerializer.startTag("", r.d("tkgYFVHTDPjrlfAiryqoy5nC0BbVHm_N8oB5IOt5Mih8CUA="));
            newSerializer.attribute("", r.d("B2WZQeC_o09VyZWtSBLGL2_gV4LfFMXktdjXXQxp4Jn2jNE="), String.valueOf(f.d));
            newSerializer.endTag("", r.d("pRCPbXaWCHDt3gmczLd-F9Q6RRMyROwEiK28WXHfDR-4SUg="));
            for (int i = 0; i < f.e.size(); i++) {
                j jVar = (j) f.e.get(i);
                newSerializer.startTag("", r.d("bWQ3FhwNCy7KWkImO2wIL41d7mJGwYf6PpZleSkKM8BnhRotbLKb"));
                newSerializer.attribute("", r.d("Z0QubpElLPg_wBA92tpMAr7REtxTQAWEiczOqmvleJ4XhRo="), String.valueOf(jVar.a));
                newSerializer.attribute("", r.d("cXTFQwI4d0dWtESOWb8O6jsnA_tvok6ed0ePutFnynQPZSyg"), String.valueOf(jVar.d));
                newSerializer.attribute("", r.d("SWFyK9hFxn0FsGMohzLJsUt1CaVQ4vTqgO0QuJdniw5z"), String.valueOf(jVar.f));
                newSerializer.attribute("", r.d("XZUdQBh7cr5bKYZq0JpF4xIGhFjaAR_W8gxpAXMUrVXc9ejc"), jVar.b);
                newSerializer.attribute("", r.d("DdxOJpaKF2kmN3FiBhrhsJr9AM3Woq_eB2ncZgZgOqvFH4U9N8I="), jVar.e);
                newSerializer.text(jVar.c);
                newSerializer.endTag("", r.d("onbVjAVLE4MZICyUkfzVYbRYC42xzN4cE_fLLpEsawHqXw25_yxY"));
            }
            newSerializer.endTag("", r.d("3ZU8w8Q598hmNQaGj8stYEfIqy4thh5BeddNTOmxX2IFtIpOApo="));
            newSerializer.endDocument();
            a(r.d("HYN6qkty3RXBoOWT7spd0RKjlehm6f5i0wOzdRDKC8rBUDATuAM="), f.a(stringWriter.toString()), context);
            stringWriter.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }
}
