package s.s;

import java.security.SecureRandom;
import r.r;

public class f {
    private byte[] a;
    private byte[] b;
    private byte[] c;

    public f(byte[] bArr) {
        this.c = bArr;
        if (bArr.length < 1 || bArr.length > 256) {
            throw new IllegalArgumentException(r.d("a2VVpcxL75g_224pnEIfiDBnIOPSyA2mUP7njW3mHR326apnb4Z0rmx0V0Sq5_7I7-yF3o6TxTyfX1tgHVhWuYk="));
        }
    }

    public static String a(String str) {
        byte[] bArr = new byte[30];
        new SecureRandom().nextBytes(bArr);
        String a2 = a.a(new f(bArr).a(str.getBytes()), 16);
        return a2.substring(0, a2.length() / 2) + a.a(bArr, 16) + a2.substring(a2.length() / 2);
    }

    private void a() {
        this.a = new byte[256];
        this.b = new byte[256];
        for (int i = 0; i < 256; i++) {
            this.a[i] = (byte) i;
            this.b[i] = this.c[i % this.c.length];
        }
        byte b2 = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            b2 = (b2 + this.a[i2] + this.b[i2]) & 255;
            byte[] bArr = this.a;
            bArr[i2] = (byte) (bArr[i2] ^ this.a[b2]);
            byte[] bArr2 = this.a;
            bArr2[b2] = (byte) (bArr2[b2] ^ this.a[i2]);
            byte[] bArr3 = this.a;
            bArr3[i2] = (byte) (bArr3[i2] ^ this.a[b2]);
        }
    }

    public static String b(String str) {
        int length = (str.length() - 40) / 2;
        byte[] a2 = a.a(str.substring(length, length + 40), 16);
        return new String(new f(a2).a(a.a(str.substring(0, length) + str.substring(length + 40), 16)));
    }

    public byte[] a(byte[] bArr) {
        a();
        byte[] bArr2 = new byte[bArr.length];
        byte b2 = 0;
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            i = (i + 1) & 255;
            b2 = (b2 + this.a[i]) & 255;
            byte[] bArr3 = this.a;
            bArr3[i] = (byte) (bArr3[i] ^ this.a[b2]);
            byte[] bArr4 = this.a;
            bArr4[b2] = (byte) (bArr4[b2] ^ this.a[i]);
            byte[] bArr5 = this.a;
            bArr5[i] = (byte) (bArr5[i] ^ this.a[b2]);
            bArr2[i2] = (byte) (this.a[(this.a[i] + this.a[b2]) & 255] ^ bArr[i2]);
        }
        return bArr2;
    }
}
