package h.h;

import android.os.AsyncTask;
import java.io.ByteArrayOutputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONObject;
import r.r;
import s.s.f;

class c extends AsyncTask {
    c() {
    }

    private static ByteArrayOutputStream a() {
        return new ByteArrayOutputStream();
    }

    private static String a(ByteArrayOutputStream byteArrayOutputStream) {
        return byteArrayOutputStream.toString();
    }

    private void b(ByteArrayOutputStream byteArrayOutputStream) {
        try {
            byteArrayOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 2000);
        try {
            HttpResponse execute = new DefaultHttpClient(basicHttpParams).execute(new HttpGet(strArr[0]));
            if (execute.getStatusLine().getStatusCode() != 200) {
                return null;
            }
            ByteArrayOutputStream a = a();
            execute.getEntity().writeTo(a);
            b(a);
            String a2 = a(a);
            a(a2);
            return a2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        JSONObject jSONObject;
        d a = d.a();
        String str2 = new String("");
        try {
            str2 = f.b(str);
        } catch (Exception e) {
        }
        try {
            jSONObject = b(str2);
        } catch (Exception e2) {
            jSONObject = null;
        }
        if (jSONObject != null) {
            try {
                a.q = jSONObject.getBoolean(r.d("XGiPexQN3GocyRmzCvEJsByLxXcFIqIqbkn-mqK9iuryKC1Cv6IQgW3xyjgLNcU6BvU="));
            } catch (Exception e3) {
            }
            try {
                a.p = jSONObject.getJSONObject(r.d("ajN0_HqR_V4hOa7sQq72Cxigs1P7dVyDKXTIYAeCOICV6Rck1GI="));
            } catch (Exception e4) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public JSONObject b(String str) {
        try {
            return new JSONObject(str);
        } catch (Exception e) {
            return null;
        }
    }
}
