package h.h;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import r.r;

public class b extends SQLiteOpenHelper {
    public b(Context context) {
        super(context, r.d("GesYJdlECdGmGnNEZ5xbUI6dUuYhdBrlEnz0wY1aHzy1zg=="), (SQLiteDatabase.CursorFactory) null, 1);
    }

    public int a(Cursor cursor) {
        return cursor.getCount();
    }

    public int a(String str, long j) {
        Cursor rawQuery = getReadableDatabase().rawQuery(r.d("7-d3PkdeYm7OhDyt9jSLi8vmwiz8IlhFSDDX0QR2UA2QAGXiim1-bwuFLQr80NvER_qlTV8gr5ufmaWsevCbz9DeoP9r3iX7mkgQHC_2uEYOf6rUBcUI7D6M5LPoBLcufv_Mn8V_81u_xu_0Aw==") + j + r.d("fuiM9saT80zCSRxmYhQWkyhPG5Z0Hy9Cb8CFmWgMLuwzU2cAGfzJOLtSG-OW") + r.d("RnYzzNN-eDXogpYyTUwwa4Gue-LiiYWq3PM2ko4Vifw=") + r.d("4fGRp_A2Qomx5KGu8KZgKXWoK7JcnQlPhCFDHm40pMRwWw==") + str + r.d("4Ab4Pn1x8RpOfvPjatBR8Na_nuVj0ZSEQnkgGw99ED=="), null);
        int a = a(rawQuery);
        rawQuery.close();
        return a;
    }

    public long a(k kVar) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(r.d("ZG76yki6mzZnItubBbmQF38g2SNxKbjKok-ecm9nz5o="), kVar.a());
        return writableDatabase.insert(r.d("rWsEvVujLrsDjuxXS4BmaG-yNtS0hsOzJ0J6qh5gMgJZqbnLJw=="), null, contentValues);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL(r.d("jMBWni6rR1sJQvqrazRvNP4IIjdyk1qweJqdUxfv8BVT-Hitia7oTAw_QEE4MBOpgl__zj_B7CAj7WF4Ud2bxAQvdqEXPFrclEgN12bTNgqD3JqY-uxxz553CScMxwy7Z4k6FoWWRvYIsIuSGkwD7cvN50cU"));
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL(r.d("gXVNFWVICrihFemxvn7UPw60CdoryMnWuJCXfolDDGGEo5haWac9fsLtKRpFbcjxsUXsSCnywL1ny_EqSiUCWrkjwHgmmRAimrXCwxsNO1H6YNF49uAau07krNvPR95dod0RY7OcWRxjoJ3TFI_EDt4w-2-a31pvwHiOgdMyaBveo1fusy-issJB"));
        onCreate(sQLiteDatabase);
    }
}
