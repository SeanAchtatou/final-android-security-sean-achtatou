package h.h;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import r.r;

class e extends WebViewClient {
    final /* synthetic */ MainActivity a;

    e(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public void onPageFinished(WebView webView, String str) {
        Functions.m(this.a.getApplicationContext());
        Functions.b(Build.VERSION.RELEASE);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        Log.e(MainActivity.f, str);
        Toast.makeText(this.a.getApplicationContext(), str, 0).show();
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.startsWith(r.d("mlTP3Ub79V3jJudhkR1rTtI-imJRj3GzOGM1BhJJyJuL-A=="))) {
            Intent intent = new Intent(r.d("VqBLazfcpeXUiHpLr1ptt0dOt5ZWUaP64FIIoAHmnOSHijWiMCj96nnxE82CV2lipiNVCdVVZP4="), Uri.parse(str));
            Log.d(r.d("VwVZfK6pX3aMC7IEUg6yjPdtbh8De4pQX6Bs9gTD5H=="), r.d("jwLATCbJFPxbFT3ZlhaLysYU9WxzBMChiVX7N8shL6=="));
            this.a.startActivity(intent);
            return true;
        }
        webView.loadUrl(str);
        return true;
    }
}
