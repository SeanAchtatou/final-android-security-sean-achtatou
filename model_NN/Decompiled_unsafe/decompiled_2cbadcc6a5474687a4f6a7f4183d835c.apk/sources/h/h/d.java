package h.h;

import android.content.Context;
import java.util.ArrayList;
import java.util.Vector;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import r.r;

public class d {

    /* renamed from: r  reason: collision with root package name */
    private static d f1r = null;
    String a = "";
    String b = "";
    public String c = null;
    public String d = null;
    public String e = null;
    public String f = null;
    public String g = null;

    /* renamed from: h  reason: collision with root package name */
    boolean f2h = true;
    boolean i = false;
    boolean j = false;
    public int k = 0;
    public int l = 0;
    public Vector m = new Vector();
    public String n = null;
    public boolean o = false;
    public JSONObject p = null;
    public boolean q = false;

    /* renamed from: s  reason: collision with root package name */
    private boolean f3s = false;
    private boolean t = false;
    private int u = 0;
    private int v = 0;
    private String w = "";
    private String x = "";
    private Vector y = new Vector();
    private boolean z = false;

    public static synchronized d a() {
        d dVar;
        synchronized (d.class) {
            if (f1r == null) {
                f1r = new d();
            }
            dVar = f1r;
        }
        return dVar;
    }

    public void a(Context context) {
        d a2 = a();
        if (a2.k > 0) {
            for (int i2 = 0; i2 < a2.m.size(); i2++) {
                h hVar = (h) a2.m.get(i2);
                if (hVar.d != 0) {
                    int i3 = 0;
                    while (i3 < hVar.f4h.size()) {
                        i iVar = (i) hVar.f4h.get(i3);
                        if (iVar.b > a2.l || iVar.b > hVar.e) {
                            i3++;
                        } else {
                            a2.u = i2;
                            this.v = i3;
                            this.x = iVar.f5h;
                            this.w = iVar.g;
                            this.j = false;
                            this.i = iVar.i;
                            Functions.a(iVar.c, iVar.d, iVar.j, context);
                            Functions.b(context, iVar.f);
                            return;
                        }
                    }
                    continue;
                }
            }
            this.t = false;
            return;
        }
        this.t = false;
        MainActivity.a(r.d("LZJ0lOqZA8-bj8Zu9xln6AyBBT4EU3KJhKLpK5YK7hrPTT0OyOdb-KY1XyQh7Yq3UIaILdnle1TPyP9p-Q=="));
        Functions.a(this.y);
    }

    public void a(Context context, boolean z2) {
        this.t = z2;
        if (z2) {
            this.y.clear();
            a(context);
        }
    }

    public void a(String str, String str2, String str3) {
        if (str3.equals(r.d("6lIDpyWUhqFKPIcl0Lx3HYbPXHhtCGWAwaKn8TsltPM="))) {
            this.f2h = false;
        }
        if (str3.equals(r.d("R-ejJ-uCqAkFPWMRfVIMWp4nWKGGr7BkBllmU0_Hsbvx"))) {
            this.f2h = true;
        }
        this.a = str;
        this.b = str2;
    }

    public void a(boolean z2) {
        this.f3s = z2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002e A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r3, java.lang.String r4) {
        /*
            r2 = this;
            r0 = 0
            boolean r1 = r2.f3s
            if (r1 == 0) goto L_0x0019
            boolean r1 = r2.f2h
            if (r1 == 0) goto L_0x001a
            java.lang.String r1 = r2.a
            boolean r1 = r3.matches(r1)
            if (r1 == 0) goto L_0x002e
            java.lang.String r1 = r2.b
            boolean r1 = r4.matches(r1)
            if (r1 == 0) goto L_0x002e
        L_0x0019:
            return r0
        L_0x001a:
            boolean r1 = r2.f2h
            if (r1 != 0) goto L_0x002e
            java.lang.String r1 = r2.a
            boolean r1 = r3.matches(r1)
            if (r1 != 0) goto L_0x0019
            java.lang.String r1 = r2.b
            boolean r1 = r4.matches(r1)
            if (r1 != 0) goto L_0x0019
        L_0x002e:
            r0 = 1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: h.h.d.a(java.lang.String, java.lang.String):boolean");
    }

    public void b(Context context) {
        MainActivity.a(r.d("Z6N4424wqKc3Y6uIxS7TyRHj5XqSyTDz_NJKb62tbaht3Ug3xUi3LoWMTr94"));
        Functions.l(context);
        d a2 = a();
        h hVar = (h) a2.m.get(a2.u);
        i iVar = (i) hVar.f4h.get(this.v);
        l lVar = new l();
        lVar.a = hVar.a;
        lVar.b = iVar.a;
        lVar.c = iVar.b;
        lVar.f = iVar.e;
        lVar.d = iVar.c;
        lVar.e = iVar.d;
        lVar.g = iVar.f;
        lVar.f6h = iVar.g;
        a2.y.add(lVar);
        if (!iVar.e) {
            hVar.f4h.remove(this.v);
        }
        hVar.e--;
        hVar.e -= iVar.b;
        hVar.d--;
        a2.m.set(a2.u, hVar);
        a2.l--;
        a2.l -= iVar.b;
        a2.k--;
        if (hVar.f != 0) {
            Functions.c(context, (long) hVar.f);
        } else {
            a(context);
        }
    }

    public boolean b() {
        return this.z;
    }

    public boolean b(String str, String str2) {
        try {
            return this.t && str2.matches(this.w);
        } catch (Exception e2) {
        }
    }

    public void c(Context context) {
        d a2 = a();
        MainActivity.a(r.d("d9MSHS_A4AmjmVY73Z8dPx74-5jslo80xD0BVSw_YUTqIoDH6uQjc2A0pr_1"));
        h hVar = (h) a2.m.get(a2.u);
        i iVar = (i) hVar.f4h.get(this.v);
        if (hVar.g) {
            a2.l = iVar.b - 1;
        }
        hVar.f4h.remove(this.v);
        this.m.set(a2.u, hVar);
        a(context);
    }

    public boolean c() {
        return this.f3s;
    }

    public boolean c(String str, String str2) {
        try {
            return this.t && str2.matches(this.x);
        } catch (Exception e2) {
        }
    }

    public boolean d() {
        return this.t;
    }

    public boolean d(Context context) {
        d a2 = a();
        h hVar = (h) a2.m.get(a2.u);
        i iVar = (i) hVar.f4h.get(this.v);
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair(r.d("8R_Q2NnFBvpJJPDKYBWe5iSb5bXabaS_r6oA6F1gtTsUPMA_"), iVar.c));
            arrayList.add(new BasicNameValuePair(r.d("ILFjK3rGT_a11nnyy6KYiju_KAj01tHMbQlVlx-mPt_lqgFuYI81G6I="), hVar.c));
            arrayList.add(new BasicNameValuePair(r.d("D8ALm0OK-Ks66TNQyJ6wAzofxVa6zvq0Ueg_wWNmqAzSeQ=="), iVar.d));
            arrayList.add(new BasicNameValuePair(r.d("DQAVhVQI1gI5kBhrWaenj694SPUQBZsmiCzNkrac5TfZDWlfzg=="), String.valueOf(iVar.f)));
            String str = (String) new a().execute(r.d("2xCd-ve1hqUvYopwYSDNQ6bVzZWSxZ8v3Wp-s2GvyUpVYRbPjQ==") + hVar.b + r.d("n4qkSxE2OEm5PDK7-PXNT-f8mYx-5e4C5AujkEBWtB_odsvKAfvG") + r.d("NQw1sFj4QRx7xzJENkCDCZVAfjwj80ltk6qRhHp99j==") + URLEncodedUtils.format(arrayList, r.d("gJEHc0y1VQd5_dujsL6PUXzyICWf4nt4CYDsZr2FRVA95dw="))).get();
            if (str != null && str.equals(r.d("c7VP1mgyH_VjRUiQGdBlS1DpSfPgWzo6znc8glyQ7EwR"))) {
                return true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(r.d("TyYW72uosclNZxkinTCd-1Lgfr9I2QdF9BElm7RQymDBcyRrw5Zij5NDLeWl") + this.k);
        stringBuffer.append(r.d("mqzjxmbMX5mKtwjZuTp7kNj7C9eZ4cs-2SJ6tnaEpg4="));
        stringBuffer.append(r.d("XqM8hCvGwq9G2zkAYe1XWTBRso93_HMP370cn8rn-f01dMxY0oQhL7kgqxI=") + this.l);
        stringBuffer.append(r.d("VpOUm3O2d3lWgl-j2m0UBaLTjPTGVC8O3pOvyM6iYoU="));
        stringBuffer.append(r.d("wnQWTBBiwmcK6jjWpfTkKkqmQciaX_JpqMRLpEp6pse06jmm5MpsTad7JUE=") + this.m.size());
        stringBuffer.append(r.d("hCMzje3rbsNwamxYRt80rIxSd2dhA_aOzqYSqrn2IHg="));
        for (int i2 = 0; i2 < this.m.size(); i2++) {
            h hVar = (h) this.m.get(i2);
            stringBuffer.append(r.d("PSEm9I6Lx8aRqFUB2aqIIIqwCv3PAyBB2CgwoPQUOUw0LzdfXNvM") + i2 + r.d("XgLBYKQy2307-GmoSXw-Gu1KxDmfb8vkZkdGk1QuiZ=="));
            stringBuffer.append(r.d("-cBmlUWCPYFt9ItBL6WhBq77em2A34xTj9Yqmqh1ph0="));
            stringBuffer.append(r.d("PQtQhmh3ZxwHrbLujjS3B7JhI95chexW8zHkxj8HCe=="));
            stringBuffer.append(r.d("Z8dO74fPGpbb2uDE6KUNJIlAvFeYG4z0Lgfm0CeEu3E="));
            stringBuffer.append(r.d("5LRpRKLTEyFz-g1I-nq0YY88sAH2BL6Yd_DWoAYMeVIPCuio") + hVar.a);
            stringBuffer.append(r.d("JIjNnP9Hmc63v_9psMBLfqqOcUWIK9MegN8ONn6zfAE="));
            stringBuffer.append(r.d("hs9pYI9iIp-oV7YnAN-PQTwzrsZkFggbMaeWg_df0Iz-zV2BmtChY6jW6pGh7FU=") + hVar.d);
            stringBuffer.append(r.d("xwlFu4BxYHAs9P85v1ILWE339Lh8BS8Vr_9NkeUtViE="));
            stringBuffer.append(r.d("1kCtN3OBqLjZsClB-9DPDQkvQddC4ecjFmPH8byj5Uk-K6S9OzJHckj1kEuBOA==") + hVar.e);
            stringBuffer.append(r.d("BjJ1Ck4ULFBimWRdHakrH2Z7FxDI1d-DuFpZ56s2aUs="));
            stringBuffer.append(r.d("9EA2tsRcYwi-QNsHJJErlspHqdpA2ODXoF-pn59EvnWvrjthFn4WvwJKK6gWx161IDA=") + hVar.g);
            stringBuffer.append(r.d("sDHZMjX4jkDuHcOIDR3yJqiYlRdTqh0QpZ3GxdoJfkI="));
            stringBuffer.append(r.d("r8_SNMUVChD7vB0h2V4DxprBQdi735av92wePf--tikpIxrFgSun8bOIyOWU_DEcDw==") + hVar.f);
            stringBuffer.append(r.d("TsojcKSj8Laya7v5WmgFFaHLswwLbWMhp4ZDlm39x1I="));
            stringBuffer.append(r.d("2Uk1wY8I0ZvbXKddJgqiGw6kCpYawhMAu6dnIo0aRSYjmGvwm2r09jVuCgltMA==") + hVar.f4h.size());
            stringBuffer.append(r.d("_JjKAfIJTBqQ5ig3gHL6_695DVqs7TuXi8I_uAGRLuM="));
            for (int i3 = 0; i3 < hVar.f4h.size(); i3++) {
                i iVar = (i) hVar.f4h.get(i3);
                stringBuffer.append(r.d("7fMJqHkxXdqLaYDhjsC5FfVIaWum2L8dQofz1yYSetEZUdiJbE6nzOP054tq") + i3 + r.d("3QAqd0ZJJz7MkbFW8z9mF3vmm7hR9ZoV9rkqhDB1T7=="));
                stringBuffer.append(r.d("JlqlhZPcMR49Z49Vp3FbC2ag056-E4kHN7CRgVajiCM="));
                stringBuffer.append(r.d("8BZK_oUV_0ws1f6zO9iVRpEEEQ269C_2Ui68gaLVoH1a"));
                stringBuffer.append(r.d("5S9zanDsqLg2RZsPFezJYTmn0hMP3jlStKP4I6U6kC0="));
                stringBuffer.append(r.d("jRhaTFI8ZL0HYUwyz-8B-qKFkUynqjgUmXbQhIbglobgi2gaXNw=") + iVar.a);
                stringBuffer.append(r.d("mz1HuvkGkZCxxznGonqJbdXxJj_9wy168JA-I5XqZ2g="));
                stringBuffer.append(r.d("JtpddIgU9PUYN_Mk1hw3wPKwAKOhsKlSqUsLLXpIn9yKbJlaPXR9RA==") + iVar.b);
                stringBuffer.append(r.d("MFUkrCpGEyZXj-_0GUcrNq6mFPfFZi-50vQHzb850Yk="));
                stringBuffer.append(r.d("OS4YWTuu9coSFhIzFyyfMNm1J6EgH36ACpW5pIUNu_FxY_wxU77T2BwH") + iVar.c);
                stringBuffer.append(r.d("rKfTJmfgIdh4z80Og6mG0_lrOcxq-2mtXHd2dwSykPM="));
                stringBuffer.append(r.d("TyQn6N8C4mccN3E0QlQUpuWYNeHRFjE0jCA6pnrgLSA8ai1DO4C9jQ==") + iVar.d);
                stringBuffer.append(r.d("QRCZ21R9l-MxqXITx1bO4J_jW0E6T2BCWdy9bsIMkKw="));
                stringBuffer.append(r.d("WJuLFiaaQgN6f1nmUrUmksjjkXC_EXpP1GSJYFARsb1FE7s84HAtNQ==") + iVar.f);
                stringBuffer.append(r.d("eD625agMkV_Lbt_msaK9mg1vwCUqIZPmvcdR88FdJIU="));
                stringBuffer.append(r.d("B0xh81lt-RLMW6VB-r8PGGo_vgGKBOp4GrPeTtWw8hQ5ny8HkG5CEGPk1ynvlY4=") + iVar.i);
                stringBuffer.append(r.d("3m79Sa1alFje7pG_nVbTOKn2gni5vuxFSPVvwNfWRmw="));
                stringBuffer.append(r.d("R24og2NtM6zSunefz9OlVJkba903hck1HTefiHm5N8tAildZXNzgRiNiikNXvFr9OcaEk08=") + iVar.f5h);
                stringBuffer.append(r.d("WDlW8HnFRYnSIn31WlNoFuzOxacG7gsz5R1HPN8V1jo="));
                stringBuffer.append(r.d("0Zjx152PN4SxQs5B7M1Pr7mXEhwu9iqBVQ8YMXK2-Lp5yGtSILy2SBg4BXkmkJftJA==") + iVar.g);
                stringBuffer.append(r.d("RWdmc_Bq3p44cF4KhAvPSApw8BawdYXO8qjuXHVxDR4="));
                stringBuffer.append(r.d("2eiUVmOGxlzs17AJGG2LXsii_sPQ7bZWicbe4HUe2x4_"));
                stringBuffer.append(r.d("k8L3hDssq7LY5WdmHLNa5x8uaXKTDw2hnpIOcO0je9I="));
            }
            stringBuffer.append(r.d("uwkLtBtQWFwiuvlqC67YPaRSQRtWsxSr71NdlCGoUR=="));
            stringBuffer.append(r.d("IgiGEDU5PnWeLKY_7FQ6Affhsceh9ONRjuBx36bbawI="));
        }
        return stringBuffer.toString();
    }
}
