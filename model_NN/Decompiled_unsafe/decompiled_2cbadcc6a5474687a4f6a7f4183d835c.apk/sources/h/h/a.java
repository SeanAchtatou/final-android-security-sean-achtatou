package h.h;

import android.os.AsyncTask;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class a extends AsyncTask {
    int a = 2000;

    public a() {
    }

    public a(int i) {
        this.a = i;
    }

    /* access modifiers changed from: protected */
    public String a(HttpResponse httpResponse) {
        try {
            StatusLine statusLine = httpResponse.getStatusLine();
            if (statusLine.getStatusCode() == 200) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                httpResponse.getEntity().writeTo(byteArrayOutputStream);
                byteArrayOutputStream.close();
                return byteArrayOutputStream.toString();
            }
            httpResponse.getEntity().getContent().close();
            throw new IOException(statusLine.getReasonPhrase());
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, this.a);
        try {
            return a(new DefaultHttpClient(basicHttpParams).execute(new HttpGet(strArr[0])));
        } catch (Exception e) {
            return null;
        }
    }
}
