package org.MobileDb;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

public class MobileDatabase {
    private static boolean useNativeUtf8Decoder = true;
    public int Version = 0;
    private boolean loadAllDataInMemory = true;
    private String path = null;
    private Vector tables = new Vector();

    public MobileDatabase() {
        useNativeUtf8Decoder = isSupportUtf8();
    }

    public void loadFrom(String path2) throws IOException {
        this.loadAllDataInMemory = true;
        this.path = path2;
        loadFrom(getClass().getResourceAsStream(path2));
    }

    public void loadFrom(String path2, boolean loadAllDataInMemory2) throws IOException {
        this.loadAllDataInMemory = loadAllDataInMemory2;
        this.path = path2;
        loadFrom(getClass().getResourceAsStream(path2));
    }

    public void loadFrom(InputStream stream) throws IOException {
        byte[] data = new byte[4];
        readDataFromStream(stream, data);
        this.Version = stream.read();
        long position = 0 + 4 + 1;
        Table table = null;
        while (true) {
            long value = (long) stream.read();
            if (value != -1) {
                position++;
                if (value == 9) {
                    int length = stream.read();
                    data = new byte[length];
                    readDataFromStream(stream, data);
                    position = position + 1 + ((long) length);
                    table = new Table(getUtf8String(data), this.loadAllDataInMemory, this.path);
                    this.tables.addElement(table);
                } else if (value == 10) {
                    int type = stream.read();
                    int length2 = stream.read();
                    data = new byte[length2];
                    readDataFromStream(stream, data);
                    position = position + 1 + 1 + ((long) length2);
                    table.addField(new Field(type, getUtf8String(data)));
                } else if (value == 11) {
                    long rowOffset = position;
                    Row row = table.createRow();
                    for (int i = 0; i < row.fieldsCount(); i++) {
                        int type2 = row.getFieldType(i);
                        if (type2 == Field.SMALL_INT) {
                            position++;
                            row.setValue(i, new Integer(stream.read()));
                        } else if (type2 == Field.SHORT_INT) {
                            byte[] tmp = new byte[2];
                            readDataFromStream(stream, tmp);
                            position += 2;
                            row.setValue(i, new Integer(shortIntFromBytes(tmp)));
                        } else if (type2 == Field.INT) {
                            byte[] tmp2 = new byte[4];
                            readDataFromStream(stream, tmp2);
                            position += 4;
                            row.setValue(i, new Integer(intFromBytes(tmp2)));
                        } else if (type2 == Field.TIME) {
                            byte[] tmp3 = new byte[4];
                            readDataFromStream(stream, tmp3);
                            position += 4;
                            row.setValue(i, new Integer(intFromBytes(tmp3)));
                        } else if (type2 == Field.NAME) {
                            int length3 = stream.read();
                            byte[] tmp4 = new byte[length3];
                            readDataFromStream(stream, tmp4);
                            position = position + 1 + ((long) length3);
                            row.setValue(i, getUtf8String(tmp4));
                        } else if (type2 == Field.TEXT) {
                            byte[] tmp5 = new byte[2];
                            readDataFromStream(stream, tmp5);
                            int length4 = shortIntFromBytes(tmp5);
                            byte[] tmp6 = new byte[length4];
                            readDataFromStream(stream, tmp6);
                            position = position + 2 + ((long) length4);
                            row.setValue(i, getUtf8String(tmp6));
                        } else if (type2 == Field.BINARY) {
                            byte[] tmp7 = new byte[4];
                            readDataFromStream(stream, tmp7);
                            int length5 = intFromBytes(tmp7);
                            readDataFromStream(stream, data);
                            position = position + 4 + ((long) length5);
                            row.setValue(i, new byte[length5]);
                        }
                    }
                    if (this.loadAllDataInMemory) {
                        table.addRow(row);
                    } else {
                        if (table.getOffset() == -1) {
                            table.setOffset(rowOffset);
                        }
                        table.addRow();
                    }
                }
            } else {
                stream.close();
                return;
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Table getTable(int index) {
        if (index < 0 || index >= this.tables.size()) {
            return null;
        }
        return (Table) this.tables.elementAt(index);
    }

    public Table getTableByName(String name) {
        for (int i = 0; i < this.tables.size(); i++) {
            Table table = (Table) this.tables.elementAt(i);
            if (table.name.equals(name)) {
                return table;
            }
        }
        return null;
    }

    public int tablesCount() {
        return this.tables.size();
    }

    public void optimize() {
        for (int i = 0; i < this.tables.size(); i++) {
            ((Table) this.tables.elementAt(i)).optimize();
        }
        System.gc();
    }

    public static int shortIntFromBytes(byte[] arr) {
        return ((arr[1] & 255) << 8) + (arr[0] & 255);
    }

    public static int intFromBytes(byte[] arr) {
        return ((arr[3] & 255) << 24) + ((arr[2] & 255) << 16) + ((arr[1] & 255) << 8) + (arr[0] & 255);
    }

    public static boolean isSupportUtf8() {
        try {
            new String("23".getBytes(), "utf-8");
            return true;
        } catch (UnsupportedEncodingException e) {
            return false;
        }
    }

    public static String getUtf8String(byte[] data) {
        if (useNativeUtf8Decoder) {
            String tmp = "";
            try {
                tmp = new String(data, "utf-8");
            } catch (UnsupportedEncodingException e) {
            }
            return tmp;
        }
        Utf8StringBuffer buffer = new Utf8StringBuffer();
        buffer.append(data, 0, data.length);
        return buffer.toString();
    }

    public static void readDataFromStream(InputStream stream, byte[] data) throws IOException {
        int pos = 0;
        int length = data.length;
        while (true) {
            int read = stream.read(data, pos, length);
            length -= read;
            if (length != 0) {
                pos += read;
            } else {
                return;
            }
        }
    }
}
