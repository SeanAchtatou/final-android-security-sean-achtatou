package bsh;

import java.io.Serializable;
import java.lang.reflect.Array;

class Name implements Serializable {
    private static String FINISHED = null;
    Class asClass;
    private int callstackDepth;
    Class classOfStaticMethod;
    private Object evalBaseObject;
    private String evalName;
    private String lastEvalName;
    public NameSpace namespace;
    String value = null;

    Name(NameSpace nameSpace, String str) {
        this.namespace = nameSpace;
        this.value = str;
    }

    private Object completeRound(String str, String str2, Object obj) {
        if (obj == null) {
            throw new InterpreterError("lastEvalName = " + str);
        }
        this.lastEvalName = str;
        this.evalName = str2;
        this.evalBaseObject = obj;
        return obj;
    }

    private Object consumeNextObjectField(CallStack callStack, Interpreter interpreter, boolean z, boolean z2) {
        Object obj;
        Class cls;
        Object resolveThisFieldReference;
        if (this.evalBaseObject == null && !isCompound(this.evalName) && !z && (resolveThisFieldReference = resolveThisFieldReference(callStack, this.namespace, interpreter, this.evalName, false)) != Primitive.VOID) {
            return completeRound(this.evalName, FINISHED, resolveThisFieldReference);
        }
        String prefix = prefix(this.evalName, 1);
        if ((this.evalBaseObject == null || (this.evalBaseObject instanceof This)) && !z) {
            if (Interpreter.DEBUG) {
                Interpreter.debug("trying to resolve variable: " + prefix);
            }
            Object resolveThisFieldReference2 = this.evalBaseObject == null ? resolveThisFieldReference(callStack, this.namespace, interpreter, prefix, false) : resolveThisFieldReference(callStack, ((This) this.evalBaseObject).namespace, interpreter, prefix, true);
            if (resolveThisFieldReference2 != Primitive.VOID) {
                if (Interpreter.DEBUG) {
                    Interpreter.debug("resolved variable: " + prefix + " in namespace: " + this.namespace);
                }
                return completeRound(prefix, suffix(this.evalName), resolveThisFieldReference2);
            }
        }
        if (this.evalBaseObject == null) {
            if (Interpreter.DEBUG) {
                Interpreter.debug("trying class: " + this.evalName);
            }
            Class cls2 = null;
            int i = 1;
            String str = null;
            while (i <= countParts(this.evalName) && (cls2 = this.namespace.getClass((str = prefix(this.evalName, i)))) == null) {
                i++;
            }
            if (cls2 != null) {
                return completeRound(str, suffix(this.evalName, countParts(this.evalName) - i), new ClassIdentifier(cls2));
            }
            if (Interpreter.DEBUG) {
                Interpreter.debug("not a class, trying var prefix " + this.evalName);
            }
        }
        if ((this.evalBaseObject == null || (this.evalBaseObject instanceof This)) && !z && z2) {
            NameSpace nameSpace = this.evalBaseObject == null ? this.namespace : ((This) this.evalBaseObject).namespace;
            This thisR = new NameSpace(nameSpace, "auto: " + prefix).getThis(interpreter);
            nameSpace.setVariable(prefix, thisR, false);
            return completeRound(prefix, suffix(this.evalName), thisR);
        } else if (this.evalBaseObject == null) {
            if (!isCompound(this.evalName)) {
                return completeRound(this.evalName, FINISHED, Primitive.VOID);
            }
            throw new UtilEvalError("Class or variable not found: " + this.evalName);
        } else if (this.evalBaseObject == Primitive.NULL) {
            throw new UtilTargetError(new NullPointerException("Null Pointer while evaluating: " + this.value));
        } else if (this.evalBaseObject == Primitive.VOID) {
            throw new UtilEvalError("Undefined variable or class name while evaluating: " + this.value);
        } else if (this.evalBaseObject instanceof Primitive) {
            throw new UtilEvalError("Can't treat primitive like an object. Error while evaluating: " + this.value);
        } else if (this.evalBaseObject instanceof ClassIdentifier) {
            Class<?> targetClass = ((ClassIdentifier) this.evalBaseObject).getTargetClass();
            String prefix2 = prefix(this.evalName, 1);
            if (prefix2.equals("this")) {
                for (NameSpace nameSpace2 = this.namespace; nameSpace2 != null; nameSpace2 = nameSpace2.getParent()) {
                    if (nameSpace2.classInstance != null && nameSpace2.classInstance.getClass() == targetClass) {
                        return completeRound(prefix2, suffix(this.evalName), nameSpace2.classInstance);
                    }
                }
                throw new UtilEvalError("Can't find enclosing 'this' instance of class: " + targetClass);
            }
            try {
                if (Interpreter.DEBUG) {
                    Interpreter.debug("Name call to getStaticFieldValue, class: " + targetClass + ", field:" + prefix2);
                }
                obj = Reflect.getStaticFieldValue(targetClass, prefix2);
            } catch (ReflectError e) {
                if (Interpreter.DEBUG) {
                    Interpreter.debug("field reflect error: " + e);
                }
                obj = null;
            }
            if (obj == null && (cls = this.namespace.getClass(targetClass.getName() + "$" + prefix2)) != null) {
                obj = new ClassIdentifier(cls);
            }
            if (obj != null) {
                return completeRound(prefix2, suffix(this.evalName), obj);
            }
            throw new UtilEvalError("No static field or inner class: " + prefix2 + " of " + targetClass);
        } else if (z) {
            throw new UtilEvalError(this.value + " does not resolve to a class name.");
        } else {
            String prefix3 = prefix(this.evalName, 1);
            if (prefix3.equals("length") && this.evalBaseObject.getClass().isArray()) {
                return completeRound(prefix3, suffix(this.evalName), new Primitive(Array.getLength(this.evalBaseObject)));
            }
            try {
                return completeRound(prefix3, suffix(this.evalName), Reflect.getObjectFieldValue(this.evalBaseObject, prefix3));
            } catch (ReflectError e2) {
                throw new UtilEvalError("Cannot access field: " + prefix3 + ", on object: " + this.evalBaseObject);
            }
        }
    }

    static int countParts(String str) {
        if (str == null) {
            return 0;
        }
        int i = 0;
        int i2 = -1;
        while (true) {
            i2 = str.indexOf(46, i2 + 1);
            if (i2 == -1) {
                return i + 1;
            }
            i++;
        }
    }

    static NameSpace getClassNameSpace(NameSpace nameSpace) {
        if (nameSpace.isClass) {
            return nameSpace;
        }
        if (!nameSpace.isMethod || nameSpace.getParent() == null || !nameSpace.getParent().isClass) {
            return null;
        }
        return nameSpace.getParent();
    }

    private Object invokeLocalMethod(Interpreter interpreter, Object[] objArr, CallStack callStack, SimpleNode simpleNode) {
        if (Interpreter.DEBUG) {
            Interpreter.debug("invokeLocalMethod: " + this.value);
        }
        if (interpreter == null) {
            throw new InterpreterError("invokeLocalMethod: interpreter = null");
        }
        String str = this.value;
        Class[] types = Types.getTypes(objArr);
        try {
            BshMethod method = this.namespace.getMethod(str, types);
            if (method != null) {
                return method.invoke(objArr, interpreter, callStack, simpleNode);
            }
            interpreter.getClassManager();
            try {
                Object command = this.namespace.getCommand(str, types, interpreter);
                if (command == null) {
                    try {
                        BshMethod method2 = this.namespace.getMethod("invoke", new Class[]{null, null});
                        if (method2 != null) {
                            return method2.invoke(new Object[]{str, objArr}, interpreter, callStack, simpleNode);
                        }
                        throw new EvalError("Command not found: " + StringUtil.methodString(str, types), simpleNode, callStack);
                    } catch (UtilEvalError e) {
                        throw e.toEvalError("Local method invocation", simpleNode, callStack);
                    }
                } else if (command instanceof BshMethod) {
                    return ((BshMethod) command).invoke(objArr, interpreter, callStack, simpleNode);
                } else {
                    if (command instanceof Class) {
                        try {
                            return Reflect.invokeCompiledCommand((Class) command, objArr, interpreter, callStack);
                        } catch (UtilEvalError e2) {
                            throw e2.toEvalError("Error invoking compiled command: ", simpleNode, callStack);
                        }
                    } else {
                        throw new InterpreterError("invalid command type");
                    }
                }
            } catch (UtilEvalError e3) {
                throw e3.toEvalError("Error loading command: ", simpleNode, callStack);
            }
        } catch (UtilEvalError e4) {
            throw e4.toEvalError("Local method invocation", simpleNode, callStack);
        }
    }

    public static boolean isCompound(String str) {
        return str.indexOf(46) != -1;
    }

    static String prefix(String str) {
        if (!isCompound(str)) {
            return null;
        }
        return prefix(str, countParts(str) - 1);
    }

    static String prefix(String str, int i) {
        if (i <= 0) {
            return null;
        }
        int i2 = -1;
        int i3 = 0;
        do {
            i2 = str.indexOf(46, i2 + 1);
            if (i2 == -1) {
                break;
            }
            i3++;
        } while (i3 < i);
        return i2 != -1 ? str.substring(0, i2) : str;
    }

    private void reset() {
        this.evalName = this.value;
        this.evalBaseObject = null;
        this.callstackDepth = 0;
    }

    static String suffix(String str) {
        if (!isCompound(str)) {
            return null;
        }
        return suffix(str, countParts(str) - 1);
    }

    public static String suffix(String str, int i) {
        if (i <= 0) {
            return null;
        }
        int i2 = 0;
        int length = str.length() + 1;
        do {
            length = str.lastIndexOf(46, length - 1);
            if (length == -1) {
                break;
            }
            i2++;
        } while (i2 < i);
        return length != -1 ? str.substring(length + 1) : str;
    }

    public Object invokeMethod(Interpreter interpreter, Object[] objArr, CallStack callStack, SimpleNode simpleNode) {
        NameSpace classNameSpace;
        String suffix = suffix(this.value, 1);
        BshClassManager classManager = interpreter.getClassManager();
        NameSpace pVar = callStack.top();
        if (this.classOfStaticMethod != null) {
            return Reflect.invokeStaticMethod(classManager, this.classOfStaticMethod, suffix, objArr);
        }
        if (!isCompound(this.value)) {
            return invokeLocalMethod(interpreter, objArr, callStack, simpleNode);
        }
        String prefix = prefix(this.value);
        if (prefix.equals("super") && countParts(this.value) == 2 && (classNameSpace = getClassNameSpace(pVar.getThis(interpreter).getNameSpace())) != null) {
            return ClassGenerator.getClassGenerator().invokeSuperclassMethod(classManager, classNameSpace.getClassInstance(), suffix, objArr);
        }
        Name nameResolver = pVar.getNameResolver(prefix);
        Object object = nameResolver.toObject(callStack, interpreter);
        if (object == Primitive.VOID) {
            throw new UtilEvalError("Attempt to resolve method: " + suffix + "() on undefined variable or class name: " + nameResolver);
        } else if (!(object instanceof ClassIdentifier)) {
            if (object instanceof Primitive) {
                if (object == Primitive.NULL) {
                    throw new UtilTargetError(new NullPointerException("Null Pointer in Method Invocation"));
                } else if (Interpreter.DEBUG) {
                    Interpreter.debug("Attempt to access method on primitive... allowing bsh.Primitive to peek through for debugging");
                }
            }
            return Reflect.invokeObjectMethod(object, suffix, objArr, interpreter, callStack, simpleNode);
        } else {
            if (Interpreter.DEBUG) {
                Interpreter.debug("invokeMethod: trying static - " + nameResolver);
            }
            Class targetClass = ((ClassIdentifier) object).getTargetClass();
            this.classOfStaticMethod = targetClass;
            if (targetClass != null) {
                return Reflect.invokeStaticMethod(classManager, targetClass, suffix, objArr);
            }
            throw new UtilEvalError("invokeMethod: unknown target: " + nameResolver);
        }
    }

    /* access modifiers changed from: package-private */
    public Object resolveThisFieldReference(CallStack callStack, NameSpace nameSpace, Interpreter interpreter, String str, boolean z) {
        if (str.equals("this")) {
            if (z) {
                throw new UtilEvalError("Redundant to call .this on This type");
            }
            This thisR = nameSpace.getThis(interpreter);
            NameSpace classNameSpace = getClassNameSpace(thisR.getNameSpace());
            return classNameSpace != null ? isCompound(this.evalName) ? classNameSpace.getThis(interpreter) : classNameSpace.getClassInstance() : thisR;
        } else if (str.equals("super")) {
            This thisR2 = nameSpace.getSuper(interpreter);
            NameSpace nameSpace2 = thisR2.getNameSpace();
            return (nameSpace2.getParent() == null || !nameSpace2.getParent().isClass) ? thisR2 : nameSpace2.getParent().getThis(interpreter);
        } else {
            Object obj = null;
            if (str.equals("global")) {
                obj = nameSpace.getGlobal(interpreter);
            }
            if (obj == null && z) {
                if (str.equals("namespace")) {
                    obj = nameSpace;
                } else if (str.equals("variables")) {
                    obj = nameSpace.getVariableNames();
                } else if (str.equals("methods")) {
                    obj = nameSpace.getMethodNames();
                } else if (str.equals("interpreter")) {
                    if (this.lastEvalName.equals("this")) {
                        obj = interpreter;
                    } else {
                        throw new UtilEvalError("Can only call .interpreter on literal 'this'");
                    }
                }
            }
            if (obj != null || !z || !str.equals("caller")) {
                if (obj == null && z && str.equals("callstack")) {
                    if (!this.lastEvalName.equals("this")) {
                        throw new UtilEvalError("Can only call .callstack on literal 'this'");
                    } else if (callStack == null) {
                        throw new InterpreterError("no callstack");
                    } else {
                        obj = callStack;
                    }
                }
                if (obj == null) {
                    obj = nameSpace.getVariable(str);
                }
                if (obj != null) {
                    return obj;
                }
                throw new InterpreterError("null this field ref:" + str);
            } else if (!this.lastEvalName.equals("this") && !this.lastEvalName.equals("caller")) {
                throw new UtilEvalError("Can only call .caller on literal 'this' or literal '.caller'");
            } else if (callStack == null) {
                throw new InterpreterError("no callstack");
            } else {
                int i = this.callstackDepth + 1;
                this.callstackDepth = i;
                return callStack.get(i).getThis(interpreter);
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: java.lang.Class} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: java.lang.Class} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v10, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: java.lang.Class} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: java.lang.Class} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v14, resolved type: java.lang.Class} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v15, resolved type: java.lang.Class} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.Class toClass() {
        /*
            r5 = this;
            r0 = 0
            monitor-enter(r5)
            java.lang.Class r1 = r5.asClass     // Catch:{ all -> 0x001b }
            if (r1 == 0) goto L_0x000a
            java.lang.Class r0 = r5.asClass     // Catch:{ all -> 0x001b }
        L_0x0008:
            monitor-exit(r5)
            return r0
        L_0x000a:
            r5.reset()     // Catch:{ all -> 0x001b }
            java.lang.String r1 = r5.evalName     // Catch:{ all -> 0x001b }
            java.lang.String r2 = "var"
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x001b }
            if (r1 == 0) goto L_0x001e
            r1 = 0
            r5.asClass = r1     // Catch:{ all -> 0x001b }
            goto L_0x0008
        L_0x001b:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x001e:
            bsh.NameSpace r1 = r5.namespace     // Catch:{ all -> 0x001b }
            java.lang.String r2 = r5.evalName     // Catch:{ all -> 0x001b }
            java.lang.Class r1 = r1.getClass(r2)     // Catch:{ all -> 0x001b }
            if (r1 != 0) goto L_0x0063
            r2 = 0
            r3 = 0
            r4 = 1
            java.lang.Object r0 = r5.toObject(r2, r3, r4)     // Catch:{ UtilEvalError -> 0x0061 }
        L_0x002f:
            boolean r2 = r0 instanceof bsh.ClassIdentifier     // Catch:{ all -> 0x001b }
            if (r2 == 0) goto L_0x0063
            bsh.ClassIdentifier r0 = (bsh.ClassIdentifier) r0     // Catch:{ all -> 0x001b }
            java.lang.Class r0 = r0.getTargetClass()     // Catch:{ all -> 0x001b }
        L_0x0039:
            if (r0 != 0) goto L_0x005c
            java.lang.ClassNotFoundException r0 = new java.lang.ClassNotFoundException     // Catch:{ all -> 0x001b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x001b }
            r1.<init>()     // Catch:{ all -> 0x001b }
            java.lang.String r2 = "Class: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x001b }
            java.lang.String r2 = r5.value     // Catch:{ all -> 0x001b }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x001b }
            java.lang.String r2 = " not found in namespace"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x001b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x001b }
            r0.<init>(r1)     // Catch:{ all -> 0x001b }
            throw r0     // Catch:{ all -> 0x001b }
        L_0x005c:
            r5.asClass = r0     // Catch:{ all -> 0x001b }
            java.lang.Class r0 = r5.asClass     // Catch:{ all -> 0x001b }
            goto L_0x0008
        L_0x0061:
            r2 = move-exception
            goto L_0x002f
        L_0x0063:
            r0 = r1
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Name.toClass():java.lang.Class");
    }

    public synchronized LHS toLHS(CallStack callStack, Interpreter interpreter) {
        LHS lHSStaticField;
        boolean z = false;
        synchronized (this) {
            reset();
            if (isCompound(this.evalName)) {
                Object obj = null;
                while (this.evalName != null && isCompound(this.evalName)) {
                    try {
                        obj = consumeNextObjectField(callStack, interpreter, false, true);
                    } catch (ReflectError e) {
                        throw new UtilEvalError("Field access: " + e);
                    } catch (UtilEvalError e2) {
                        throw new UtilEvalError("LHS evaluation: " + e2.getMessage());
                    }
                }
                if (this.evalName == null && (obj instanceof ClassIdentifier)) {
                    throw new UtilEvalError("Can't assign to class: " + this.value);
                } else if (obj == null) {
                    throw new UtilEvalError("Error in LHS: " + this.value);
                } else if (obj instanceof This) {
                    if (this.evalName.equals("namespace") || this.evalName.equals("variables") || this.evalName.equals("methods") || this.evalName.equals("caller")) {
                        throw new UtilEvalError("Can't assign to special variable: " + this.evalName);
                    }
                    Interpreter.debug("found This reference evaluating LHS");
                    if (!this.lastEvalName.equals("super")) {
                        z = true;
                    }
                    lHSStaticField = new LHS(((This) obj).namespace, this.evalName, z);
                } else if (this.evalName != null) {
                    lHSStaticField = obj instanceof ClassIdentifier ? Reflect.getLHSStaticField(((ClassIdentifier) obj).getTargetClass(), this.evalName) : Reflect.getLHSObjectField(obj, this.evalName);
                } else {
                    throw new InterpreterError("Internal error in lhs...");
                }
            } else if (this.evalName.equals("this")) {
                throw new UtilEvalError("Can't assign to 'this'.");
            } else {
                lHSStaticField = new LHS(this.namespace, this.evalName, false);
            }
        }
        return lHSStaticField;
    }

    public Object toObject(CallStack callStack, Interpreter interpreter) {
        return toObject(callStack, interpreter, false);
    }

    public synchronized Object toObject(CallStack callStack, Interpreter interpreter, boolean z) {
        Object obj;
        reset();
        obj = null;
        while (this.evalName != null) {
            obj = consumeNextObjectField(callStack, interpreter, z, false);
        }
        if (obj == null) {
            throw new InterpreterError("null value in toObject()");
        }
        return obj;
    }

    public String toString() {
        return this.value;
    }
}
