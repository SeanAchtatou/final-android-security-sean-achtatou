package bsh;

import bsh.BshClassManager;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class NameSpace implements BshClassManager.Listener, Serializable {
    public static final NameSpace JAVACODE;
    SimpleNode callerInfoNode;
    private transient Hashtable classCache;
    Object classInstance;
    private transient BshClassManager classManager;
    Class classStatic;
    protected Hashtable importedClasses;
    private Vector importedCommands;
    private Vector importedObjects;
    private Vector importedPackages;
    private Vector importedStatic;
    boolean isClass;
    boolean isMethod;
    private Hashtable methods;
    private Hashtable names;
    private String nsName;
    private String packageName;
    private NameSpace parent;
    private This thisReference;
    private Hashtable variables;

    static {
        NameSpace nameSpace = new NameSpace((BshClassManager) null, "Called from compiled Java code.");
        JAVACODE = nameSpace;
        nameSpace.isMethod = true;
    }

    public NameSpace(BshClassManager bshClassManager, String str) {
        this(null, bshClassManager, str);
    }

    public NameSpace(NameSpace nameSpace, BshClassManager bshClassManager, String str) {
        setName(str);
        setParent(nameSpace);
        setClassManager(bshClassManager);
        if (bshClassManager != null) {
            bshClassManager.addListener(this);
        }
    }

    public NameSpace(NameSpace nameSpace, String str) {
        this(nameSpace, null, str);
    }

    private Class classForName(String str) {
        return getClassManager().classForName(str);
    }

    private String[] enumerationToStringArray(Enumeration enumeration) {
        Vector vector = new Vector();
        while (enumeration.hasMoreElements()) {
            vector.addElement(enumeration.nextElement());
        }
        String[] strArr = new String[vector.size()];
        vector.copyInto(strArr);
        return strArr;
    }

    private BshMethod[] flattenMethodCollection(Enumeration enumeration) {
        Vector vector = new Vector();
        while (enumeration.hasMoreElements()) {
            Object nextElement = enumeration.nextElement();
            if (nextElement instanceof BshMethod) {
                vector.addElement(nextElement);
            } else {
                Vector vector2 = (Vector) nextElement;
                for (int i = 0; i < vector2.size(); i++) {
                    vector.addElement(vector2.elementAt(i));
                }
            }
        }
        BshMethod[] bshMethodArr = new BshMethod[vector.size()];
        vector.copyInto(bshMethodArr);
        return bshMethodArr;
    }

    private Class getClassImpl(String str) {
        Class cls;
        if (this.classCache != null) {
            cls = (Class) this.classCache.get(str);
            if (cls != null) {
                return cls;
            }
        } else {
            cls = null;
        }
        boolean z = !Name.isCompound(str);
        if (z) {
            if (cls == null) {
                cls = getImportedClassImpl(str);
            }
            if (cls != null) {
                cacheClass(str, cls);
                return cls;
            }
        }
        Class classForName = classForName(str);
        if (classForName == null) {
            if (Interpreter.DEBUG) {
                Interpreter.debug("getClass(): " + str + " not\tfound in " + this);
            }
            return null;
        } else if (!z) {
            return classForName;
        } else {
            cacheClass(str, classForName);
            return classForName;
        }
    }

    private Class getImportedClassImpl(String str) {
        String classNameByUnqName;
        String str2 = this.importedClasses != null ? (String) this.importedClasses.get(str) : null;
        if (str2 != null) {
            Class classForName = classForName(str2);
            if (classForName != null) {
                return classForName;
            }
            if (Name.isCompound(str2)) {
                try {
                    classForName = getNameResolver(str2).toClass();
                } catch (ClassNotFoundException e) {
                }
            } else if (Interpreter.DEBUG) {
                Interpreter.debug("imported unpackaged name not found:" + str2);
            }
            if (classForName == null) {
                return null;
            }
            getClassManager().cacheClassInfo(str2, classForName);
            return classForName;
        }
        if (this.importedPackages != null) {
            for (int size = this.importedPackages.size() - 1; size >= 0; size--) {
                Class classForName2 = classForName(((String) this.importedPackages.elementAt(size)) + "." + str);
                if (classForName2 != null) {
                    return classForName2;
                }
            }
        }
        BshClassManager classManager2 = getClassManager();
        if (!classManager2.hasSuperImport() || (classNameByUnqName = classManager2.getClassNameByUnqName(str)) == null) {
            return null;
        }
        return classForName(classNameByUnqName);
    }

    public static Class identifierToClass(ClassIdentifier classIdentifier) {
        return classIdentifier.getTargetClass();
    }

    private BshMethod loadScriptedCommand(InputStream inputStream, String str, Class[] clsArr, String str2, Interpreter interpreter) {
        try {
            interpreter.eval(new InputStreamReader(inputStream), this, str2);
            return getMethod(str, clsArr);
        } catch (EvalError e) {
            Interpreter.debug(e.toString());
            throw new UtilEvalError("Error loading script: " + e.getMessage());
        }
    }

    private synchronized void writeObject(ObjectOutputStream objectOutputStream) {
        this.names = null;
        objectOutputStream.defaultWriteObject();
    }

    /* access modifiers changed from: package-private */
    public boolean attemptSetPropertyValue(String str, Object obj, Interpreter interpreter) {
        String accessorName = Reflect.accessorName("set", str);
        Class[] clsArr = new Class[1];
        clsArr[0] = obj == null ? null : obj.getClass();
        if (getMethod(accessorName, clsArr) == null) {
            return false;
        }
        try {
            invokeMethod(accessorName, new Object[]{obj}, interpreter);
            return true;
        } catch (EvalError e) {
            throw new UtilEvalError("'This' property accessor threw exception: " + e.getMessage());
        }
    }

    /* access modifiers changed from: package-private */
    public void cacheClass(String str, Class cls) {
        if (this.classCache == null) {
            this.classCache = new Hashtable();
        }
        this.classCache.put(str, cls);
    }

    public void classLoaderChanged() {
        nameSpaceChanged();
    }

    public void clear() {
        this.variables = null;
        this.methods = null;
        this.importedClasses = null;
        this.importedPackages = null;
        this.importedCommands = null;
        this.importedObjects = null;
        if (this.parent == null) {
            loadDefaultImports();
        }
        this.classCache = null;
        this.names = null;
    }

    /* access modifiers changed from: protected */
    public Variable createVariable(String str, Class cls, LHS lhs) {
        return new Variable(str, cls, lhs);
    }

    /* access modifiers changed from: protected */
    public Variable createVariable(String str, Class cls, Object obj, Modifiers modifiers) {
        return new Variable(str, cls, obj, modifiers);
    }

    /* access modifiers changed from: protected */
    public Variable createVariable(String str, Object obj, Modifiers modifiers) {
        return createVariable(str, null, obj, modifiers);
    }

    public void doSuperImport() {
        getClassManager().doSuperImport();
    }

    public Object get(String str, Interpreter interpreter) {
        return getNameResolver(str).toObject(new CallStack(this), interpreter);
    }

    public String[] getAllNames() {
        Vector vector = new Vector();
        getAllNamesAux(vector);
        String[] strArr = new String[vector.size()];
        vector.copyInto(strArr);
        return strArr;
    }

    /* access modifiers changed from: protected */
    public void getAllNamesAux(Vector vector) {
        Enumeration keys = this.variables.keys();
        while (keys.hasMoreElements()) {
            vector.addElement(keys.nextElement());
        }
        Enumeration keys2 = this.methods.keys();
        while (keys2.hasMoreElements()) {
            vector.addElement(keys2.nextElement());
        }
        if (this.parent != null) {
            this.parent.getAllNamesAux(vector);
        }
    }

    public Class getClass(String str) {
        Class classImpl = getClassImpl(str);
        if (classImpl != null) {
            return classImpl;
        }
        if (this.parent != null) {
            return this.parent.getClass(str);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public Object getClassInstance() {
        if (this.classInstance != null) {
            return this.classInstance;
        }
        if (this.classStatic != null) {
            throw new UtilEvalError("Can't refer to class instance from static context.");
        }
        throw new InterpreterError("Can't resolve class instance 'this' in: " + this);
    }

    public BshClassManager getClassManager() {
        if (this.classManager != null) {
            return this.classManager;
        }
        if (this.parent != null && this.parent != JAVACODE) {
            return this.parent.getClassManager();
        }
        this.classManager = BshClassManager.createClassManager(null);
        return this.classManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public Object getCommand(String str, Class[] clsArr, Interpreter interpreter) {
        if (Interpreter.DEBUG) {
            Interpreter.debug("getCommand: " + str);
        }
        BshClassManager classManager2 = interpreter.getClassManager();
        if (this.importedCommands != null) {
            for (int size = this.importedCommands.size() - 1; size >= 0; size--) {
                String str2 = (String) this.importedCommands.elementAt(size);
                String str3 = str2.equals("/") ? str2 + str + ".bsh" : str2 + "/" + str + ".bsh";
                Interpreter.debug("searching for script: " + str3);
                InputStream resourceAsStream = classManager2.getResourceAsStream(str3);
                if (resourceAsStream != null) {
                    return loadScriptedCommand(resourceAsStream, str, clsArr, str3, interpreter);
                }
                String str4 = str2.equals("/") ? str : str2.substring(1).replace('/', '.') + "." + str;
                Interpreter.debug("searching for class: " + str4);
                Class classForName = classManager2.classForName(str4);
                if (classForName != null) {
                    return classForName;
                }
            }
        }
        if (this.parent != null) {
            return this.parent.getCommand(str, clsArr, interpreter);
        }
        return null;
    }

    public Variable[] getDeclaredVariables() {
        if (this.variables == null) {
            return new Variable[0];
        }
        Variable[] variableArr = new Variable[this.variables.size()];
        Enumeration elements = this.variables.elements();
        int i = 0;
        while (elements.hasMoreElements()) {
            variableArr[i] = (Variable) elements.nextElement();
            i++;
        }
        return variableArr;
    }

    public This getGlobal(Interpreter interpreter) {
        return this.parent != null ? this.parent.getGlobal(interpreter) : getThis(interpreter);
    }

    /* access modifiers changed from: protected */
    public BshMethod getImportedMethod(String str, Class[] clsArr) {
        if (this.importedObjects != null) {
            for (int i = 0; i < this.importedObjects.size(); i++) {
                Object elementAt = this.importedObjects.elementAt(i);
                Method resolveJavaMethod = Reflect.resolveJavaMethod(getClassManager(), elementAt.getClass(), str, clsArr, false);
                if (resolveJavaMethod != null) {
                    return new BshMethod(resolveJavaMethod, elementAt);
                }
            }
        }
        if (this.importedStatic != null) {
            for (int i2 = 0; i2 < this.importedStatic.size(); i2++) {
                Method resolveJavaMethod2 = Reflect.resolveJavaMethod(getClassManager(), (Class) this.importedStatic.elementAt(i2), str, clsArr, true);
                if (resolveJavaMethod2 != null) {
                    return new BshMethod(resolveJavaMethod2, null);
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Variable getImportedVar(String str) {
        if (this.importedObjects != null) {
            for (int i = 0; i < this.importedObjects.size(); i++) {
                Object elementAt = this.importedObjects.elementAt(i);
                Field resolveJavaField = Reflect.resolveJavaField(elementAt.getClass(), str, false);
                if (resolveJavaField != null) {
                    return createVariable(str, resolveJavaField.getType(), new LHS(elementAt, resolveJavaField));
                }
            }
        }
        if (this.importedStatic != null) {
            for (int i2 = 0; i2 < this.importedStatic.size(); i2++) {
                Field resolveJavaField2 = Reflect.resolveJavaField((Class) this.importedStatic.elementAt(i2), str, true);
                if (resolveJavaField2 != null) {
                    return createVariable(str, resolveJavaField2.getType(), new LHS(resolveJavaField2));
                }
            }
        }
        return null;
    }

    public int getInvocationLine() {
        SimpleNode node = getNode();
        if (node != null) {
            return node.getLineNumber();
        }
        return -1;
    }

    public String getInvocationText() {
        SimpleNode node = getNode();
        return node != null ? node.getText() : "<invoked from Java code>";
    }

    public BshMethod getMethod(String str, Class[] clsArr) {
        return getMethod(str, clsArr, false);
    }

    public BshMethod getMethod(String str, Class[] clsArr, boolean z) {
        BshMethod bshMethod;
        Object obj;
        BshMethod[] bshMethodArr;
        BshMethod bshMethod2 = null;
        if (this.isClass && !z) {
            bshMethod2 = getImportedMethod(str, clsArr);
        }
        if (!(bshMethod2 != null || this.methods == null || (obj = this.methods.get(str)) == null)) {
            if (obj instanceof Vector) {
                Vector vector = (Vector) obj;
                BshMethod[] bshMethodArr2 = new BshMethod[vector.size()];
                vector.copyInto(bshMethodArr2);
                bshMethodArr = bshMethodArr2;
            } else {
                bshMethodArr = new BshMethod[]{(BshMethod) obj};
            }
            Class[][] clsArr2 = new Class[bshMethodArr.length][];
            for (int i = 0; i < bshMethodArr.length; i++) {
                clsArr2[i] = bshMethodArr[i].getParameterTypes();
            }
            int findMostSpecificSignature = Reflect.findMostSpecificSignature(clsArr, clsArr2);
            if (findMostSpecificSignature != -1) {
                bshMethod = bshMethodArr[findMostSpecificSignature];
                if (bshMethod == null && !this.isClass && !z) {
                    bshMethod = getImportedMethod(str, clsArr);
                }
                return !z ? bshMethod : bshMethod;
            }
        }
        bshMethod = bshMethod2;
        bshMethod = getImportedMethod(str, clsArr);
        return !z ? bshMethod : bshMethod;
    }

    public String[] getMethodNames() {
        return this.methods == null ? new String[0] : enumerationToStringArray(this.methods.keys());
    }

    public BshMethod[] getMethods() {
        return this.methods == null ? new BshMethod[0] : flattenMethodCollection(this.methods.elements());
    }

    public String getName() {
        return this.nsName;
    }

    /* access modifiers changed from: package-private */
    public Name getNameResolver(String str) {
        if (this.names == null) {
            this.names = new Hashtable();
        }
        Name name = (Name) this.names.get(str);
        if (name != null) {
            return name;
        }
        Name name2 = new Name(this, str);
        this.names.put(str, name2);
        return name2;
    }

    /* access modifiers changed from: package-private */
    public SimpleNode getNode() {
        if (this.callerInfoNode != null) {
            return this.callerInfoNode;
        }
        if (this.parent != null) {
            return this.parent.getNode();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String getPackage() {
        if (this.packageName != null) {
            return this.packageName;
        }
        if (this.parent != null) {
            return this.parent.getPackage();
        }
        return null;
    }

    public NameSpace getParent() {
        return this.parent;
    }

    /* access modifiers changed from: package-private */
    public Object getPropertyValue(String str, Interpreter interpreter) {
        Class[] clsArr = new Class[0];
        BshMethod method = getMethod(Reflect.accessorName("get", str), clsArr);
        if (method != null) {
            try {
                return method.invoke(null, interpreter);
            } catch (EvalError e) {
                throw new UtilEvalError("'This' property accessor threw exception: " + e.getMessage());
            }
        } else {
            BshMethod method2 = getMethod(Reflect.accessorName("is", str), clsArr);
            return method2 != null ? method2.invoke(null, interpreter) : Primitive.VOID;
        }
    }

    public This getSuper(Interpreter interpreter) {
        return this.parent != null ? this.parent.getThis(interpreter) : getThis(interpreter);
    }

    public This getThis(Interpreter interpreter) {
        if (this.thisReference == null) {
            this.thisReference = This.getThis(this, interpreter);
        }
        return this.thisReference;
    }

    public Object getVariable(String str) {
        return getVariable(str, true);
    }

    public Object getVariable(String str, boolean z) {
        return unwrapVariable(getVariableImpl(str, z));
    }

    /* access modifiers changed from: protected */
    public Variable getVariableImpl(String str, boolean z) {
        Variable variable = null;
        if (this.isClass) {
            variable = getImportedVar(str);
        }
        if (variable == null && this.variables != null) {
            variable = (Variable) this.variables.get(str);
        }
        if (variable == null && !this.isClass) {
            variable = getImportedVar(str);
        }
        return (!z || variable != null || this.parent == null) ? variable : this.parent.getVariableImpl(str, z);
    }

    public String[] getVariableNames() {
        return this.variables == null ? new String[0] : enumerationToStringArray(this.variables.keys());
    }

    public Object getVariableOrProperty(String str, Interpreter interpreter) {
        Object variable = getVariable(str, true);
        return variable == Primitive.VOID ? getPropertyValue(str, interpreter) : variable;
    }

    public void importClass(String str) {
        if (this.importedClasses == null) {
            this.importedClasses = new Hashtable();
        }
        this.importedClasses.put(Name.suffix(str, 1), str);
        nameSpaceChanged();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public void importCommands(String str) {
        if (this.importedCommands == null) {
            this.importedCommands = new Vector();
        }
        String replace = str.replace('.', '/');
        if (!replace.startsWith("/")) {
            replace = "/" + replace;
        }
        if (replace.length() > 1 && replace.endsWith("/")) {
            replace = replace.substring(0, replace.length() - 1);
        }
        if (this.importedCommands.contains(replace)) {
            this.importedCommands.remove(replace);
        }
        this.importedCommands.addElement(replace);
        nameSpaceChanged();
    }

    public void importObject(Object obj) {
        if (this.importedObjects == null) {
            this.importedObjects = new Vector();
        }
        if (this.importedObjects.contains(obj)) {
            this.importedObjects.remove(obj);
        }
        this.importedObjects.addElement(obj);
        nameSpaceChanged();
    }

    public void importPackage(String str) {
        if (this.importedPackages == null) {
            this.importedPackages = new Vector();
        }
        if (this.importedPackages.contains(str)) {
            this.importedPackages.remove(str);
        }
        this.importedPackages.addElement(str);
        nameSpaceChanged();
    }

    public void importStatic(Class cls) {
        if (this.importedStatic == null) {
            this.importedStatic = new Vector();
        }
        if (this.importedStatic.contains(cls)) {
            this.importedStatic.remove(cls);
        }
        this.importedStatic.addElement(cls);
        nameSpaceChanged();
    }

    public Object invokeMethod(String str, Object[] objArr, Interpreter interpreter) {
        return invokeMethod(str, objArr, interpreter, null, null);
    }

    public Object invokeMethod(String str, Object[] objArr, Interpreter interpreter, CallStack callStack, SimpleNode simpleNode) {
        return getThis(interpreter).invokeMethod(str, objArr, interpreter, callStack, simpleNode, false);
    }

    public void loadDefaultImports() {
        importClass("bsh.EvalError");
        importClass("bsh.Interpreter");
        importPackage("javax.swing.event");
        importPackage("javax.swing");
        importPackage("java.awt.event");
        importPackage("java.awt");
        importPackage("java.net");
        importPackage("java.util");
        importPackage("java.io");
        importPackage("java.lang");
        importCommands("/bsh/commands");
    }

    public void nameSpaceChanged() {
        this.classCache = null;
        this.names = null;
    }

    public void prune() {
        if (this.classManager == null) {
            setClassManager(BshClassManager.createClassManager(null));
        }
        setParent(null);
    }

    /* access modifiers changed from: package-private */
    public void setClassInstance(Object obj) {
        this.classInstance = obj;
        importObject(obj);
    }

    /* access modifiers changed from: package-private */
    public void setClassManager(BshClassManager bshClassManager) {
        this.classManager = bshClassManager;
    }

    /* access modifiers changed from: package-private */
    public void setClassStatic(Class cls) {
        this.classStatic = cls;
        importStatic(cls);
    }

    /* access modifiers changed from: package-private */
    public void setLocalVariable(String str, Object obj, boolean z) {
        setVariable(str, obj, z, false);
    }

    /* access modifiers changed from: package-private */
    public void setLocalVariableOrProperty(String str, Object obj, boolean z) {
        setVariableOrProperty(str, obj, z, false);
    }

    public void setMethod(String str, BshMethod bshMethod) {
        if (this.methods == null) {
            this.methods = new Hashtable();
        }
        Object obj = this.methods.get(str);
        if (obj == null) {
            this.methods.put(str, bshMethod);
        } else if (obj instanceof BshMethod) {
            Vector vector = new Vector();
            vector.addElement(obj);
            vector.addElement(bshMethod);
            this.methods.put(str, vector);
        } else {
            ((Vector) obj).addElement(bshMethod);
        }
    }

    public void setName(String str) {
        this.nsName = str;
    }

    /* access modifiers changed from: package-private */
    public void setNode(SimpleNode simpleNode) {
        this.callerInfoNode = simpleNode;
    }

    /* access modifiers changed from: package-private */
    public void setPackage(String str) {
        this.packageName = str;
    }

    public void setParent(NameSpace nameSpace) {
        this.parent = nameSpace;
        if (nameSpace == null) {
            loadDefaultImports();
        }
    }

    public void setTypedVariable(String str, Class cls, Object obj, Modifiers modifiers) {
        if (this.variables == null) {
            this.variables = new Hashtable();
        }
        Variable variableImpl = getVariableImpl(str, false);
        if (variableImpl == null || variableImpl.getType() == null) {
            this.variables.put(str, createVariable(str, cls, obj, modifiers));
        } else if (variableImpl.getType() != cls) {
            throw new UtilEvalError("Typed variable: " + str + " was previously declared with type: " + variableImpl.getType());
        } else {
            variableImpl.setValue(obj, 0);
        }
    }

    public void setTypedVariable(String str, Class cls, Object obj, boolean z) {
        Modifiers modifiers = new Modifiers();
        if (z) {
            modifiers.addModifier(2, "final");
        }
        setTypedVariable(str, cls, obj, modifiers);
    }

    public void setVariable(String str, Object obj, boolean z) {
        setVariable(str, obj, z, Interpreter.LOCALSCOPING ? z : true);
    }

    /* access modifiers changed from: package-private */
    public void setVariable(String str, Object obj, boolean z, boolean z2) {
        if (this.variables == null) {
            this.variables = new Hashtable();
        }
        if (obj == null) {
            throw new InterpreterError("null variable value");
        }
        Variable variableImpl = getVariableImpl(str, z2);
        if (variableImpl != null) {
            try {
                variableImpl.setValue(obj, 1);
            } catch (UtilEvalError e) {
                throw new UtilEvalError("Variable assignment: " + str + ": " + e.getMessage());
            }
        } else if (z) {
            throw new UtilEvalError("(Strict Java mode) Assignment to undeclared variable: " + str);
        } else {
            this.variables.put(str, createVariable(str, obj, (Modifiers) null));
            nameSpaceChanged();
        }
    }

    public void setVariableOrProperty(String str, Object obj, boolean z) {
        setVariableOrProperty(str, obj, z, Interpreter.LOCALSCOPING ? z : true);
    }

    /* access modifiers changed from: package-private */
    public void setVariableOrProperty(String str, Object obj, boolean z, boolean z2) {
        if (this.variables == null) {
            this.variables = new Hashtable();
        }
        if (obj == null) {
            throw new InterpreterError("null variable value");
        }
        Variable variableImpl = getVariableImpl(str, z2);
        if (variableImpl != null) {
            try {
                variableImpl.setValue(obj, 1);
            } catch (UtilEvalError e) {
                throw new UtilEvalError("Variable assignment: " + str + ": " + e.getMessage());
            }
        } else if (z) {
            throw new UtilEvalError("(Strict Java mode) Assignment to undeclared variable: " + str);
        } else if (!attemptSetPropertyValue(str, obj, null)) {
            this.variables.put(str, createVariable(str, obj, (Modifiers) null));
            nameSpaceChanged();
        }
    }

    public String toString() {
        return "NameSpace: " + (this.nsName == null ? super.toString() : this.nsName + " (" + super.toString() + ")") + (this.isClass ? " (isClass) " : "") + (this.isMethod ? " (method) " : "") + (this.classStatic != null ? " (class static) " : "") + (this.classInstance != null ? " (class instance) " : "");
    }

    public void unsetVariable(String str) {
        if (this.variables != null) {
            this.variables.remove(str);
            nameSpaceChanged();
        }
    }

    /* access modifiers changed from: protected */
    public Object unwrapVariable(Variable variable) {
        return variable == null ? Primitive.VOID : variable.getValue();
    }
}
