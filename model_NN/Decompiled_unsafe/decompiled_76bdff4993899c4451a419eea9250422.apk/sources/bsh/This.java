package bsh;

import java.io.Serializable;

public class This implements Serializable, Runnable {
    transient Interpreter declaringInterpreter;
    NameSpace namespace;

    protected This(NameSpace nameSpace, Interpreter interpreter) {
        this.namespace = nameSpace;
        this.declaringInterpreter = interpreter;
    }

    public static void bind(This thisR, NameSpace nameSpace, Interpreter interpreter) {
        thisR.namespace.setParent(nameSpace);
        thisR.declaringInterpreter = interpreter;
    }

    static This getThis(NameSpace nameSpace, Interpreter interpreter) {
        Class<?> cls;
        try {
            if (Capabilities.canGenerateInterfaces()) {
                cls = Class.forName("bsh.XThis");
            } else if (!Capabilities.haveSwing()) {
                return new This(nameSpace, interpreter);
            } else {
                cls = Class.forName("bsh.JThis");
            }
            return (This) Reflect.constructObject(cls, new Object[]{nameSpace, interpreter});
        } catch (Exception e) {
            throw new InterpreterError("internal error 1 in This: " + e);
        }
    }

    static boolean isExposedThisMethod(String str) {
        return str.equals("getClass") || str.equals("invokeMethod") || str.equals("getInterface") || str.equals("wait") || str.equals("notify") || str.equals("notifyAll");
    }

    public Object getInterface(Class cls) {
        if (cls.isInstance(this)) {
            return this;
        }
        throw new UtilEvalError("Dynamic proxy mechanism not available. Cannot construct interface type: " + cls);
    }

    public Object getInterface(Class[] clsArr) {
        for (int i = 0; i < clsArr.length; i++) {
            if (!clsArr[i].isInstance(this)) {
                throw new UtilEvalError("Dynamic proxy mechanism not available. Cannot construct interface type: " + clsArr[i]);
            }
        }
        return this;
    }

    public NameSpace getNameSpace() {
        return this.namespace;
    }

    public Object invokeMethod(String str, Object[] objArr) {
        return invokeMethod(str, objArr, null, null, null, false);
    }

    public Object invokeMethod(String str, Object[] objArr, Interpreter interpreter, CallStack callStack, SimpleNode simpleNode, boolean z) {
        if (objArr != null) {
            Object[] objArr2 = new Object[objArr.length];
            for (int i = 0; i < objArr.length; i++) {
                objArr2[i] = objArr[i] == null ? Primitive.NULL : objArr[i];
            }
            objArr = objArr2;
        }
        if (interpreter == null) {
            interpreter = this.declaringInterpreter;
        }
        if (callStack == null) {
            callStack = new CallStack(this.namespace);
        }
        if (simpleNode == null) {
            simpleNode = SimpleNode.JAVACODE;
        }
        Class[] types = Types.getTypes(objArr);
        BshMethod bshMethod = null;
        try {
            bshMethod = this.namespace.getMethod(str, types, z);
        } catch (UtilEvalError e) {
        }
        if (bshMethod != null) {
            return bshMethod.invoke(objArr, interpreter, callStack, simpleNode);
        }
        if (str.equals("toString")) {
            return toString();
        }
        if (str.equals("hashCode")) {
            return new Integer(hashCode());
        }
        if (str.equals("equals")) {
            return this == objArr[0] ? Boolean.TRUE : Boolean.FALSE;
        }
        try {
            bshMethod = this.namespace.getMethod("invoke", new Class[]{null, null});
        } catch (UtilEvalError e2) {
        }
        if (bshMethod != null) {
            return bshMethod.invoke(new Object[]{str, objArr}, interpreter, callStack, simpleNode);
        }
        throw new EvalError("Method " + StringUtil.methodString(str, types) + " not found in bsh scripted object: " + this.namespace.getName(), simpleNode, callStack);
    }

    public void run() {
        try {
            invokeMethod("run", new Object[0]);
        } catch (EvalError e) {
            this.declaringInterpreter.error("Exception in runnable:" + e);
        }
    }

    public String toString() {
        return "'this' reference to Bsh object: " + this.namespace;
    }
}
