package bsh;

import java.lang.reflect.Array;

class BSHArrayInitializer extends SimpleNode {
    BSHArrayInitializer(int i) {
        super(i);
    }

    private void throwTypeError(Class cls, Object obj, int i, CallStack callStack) {
        throw new EvalError("Incompatible type: " + (obj instanceof Primitive ? ((Primitive) obj).getType().getName() : Reflect.normalizeClassName(obj.getClass())) + " in initializer of array type: " + cls + " at position: " + i, this, callStack);
    }

    public Object eval(CallStack callStack, Interpreter interpreter) {
        throw new EvalError("Array initializer has no base type.", this, callStack);
    }

    public Object eval(Class cls, int i, CallStack callStack, Interpreter interpreter) {
        Object eval;
        Object obj;
        int jjtGetNumChildren = jjtGetNumChildren();
        int[] iArr = new int[i];
        iArr[0] = jjtGetNumChildren;
        Object newInstance = Array.newInstance(cls, iArr);
        for (int i2 = 0; i2 < jjtGetNumChildren; i2++) {
            SimpleNode simpleNode = (SimpleNode) jjtGetChild(i2);
            if (!(simpleNode instanceof BSHArrayInitializer)) {
                eval = simpleNode.eval(callStack, interpreter);
            } else if (i < 2) {
                throw new EvalError("Invalid Location for Intializer, position: " + i2, this, callStack);
            } else {
                eval = ((BSHArrayInitializer) simpleNode).eval(cls, i - 1, callStack, interpreter);
            }
            if (eval == Primitive.VOID) {
                throw new EvalError("Void in array initializer, position" + i2, this, callStack);
            }
            if (i == 1) {
                try {
                    obj = Primitive.unwrap(Types.castObject(eval, cls, 0));
                } catch (UtilEvalError e) {
                    throw e.toEvalError("Error in array initializer", this, callStack);
                }
            } else {
                obj = eval;
            }
            try {
                Array.set(newInstance, i2, obj);
            } catch (IllegalArgumentException e2) {
                Interpreter.debug("illegal arg" + e2);
                throwTypeError(cls, eval, i2, callStack);
            } catch (ArrayStoreException e3) {
                Interpreter.debug("arraystore" + e3);
                throwTypeError(cls, eval, i2, callStack);
            }
        }
        return newInstance;
    }
}
