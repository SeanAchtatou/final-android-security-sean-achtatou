package bsh;

class BSHMethodDeclaration extends SimpleNode {
    BSHBlock blockNode;
    int firstThrowsClause;
    public Modifiers modifiers;
    public String name;
    int numThrows = 0;
    BSHFormalParameters paramsNode;
    Class returnType;
    BSHReturnType returnTypeNode;

    BSHMethodDeclaration(int i) {
        super(i);
    }

    private void evalNodes(CallStack callStack, Interpreter interpreter) {
        insureNodesParsed();
        int i = this.firstThrowsClause;
        while (true) {
            int i2 = i;
            if (i2 >= this.numThrows + this.firstThrowsClause) {
                break;
            }
            ((BSHAmbiguousName) jjtGetChild(i2)).toClass(callStack, interpreter);
            i = i2 + 1;
        }
        this.paramsNode.eval(callStack, interpreter);
        if (interpreter.getStrictJava()) {
            for (int i3 = 0; i3 < this.paramsNode.paramTypes.length; i3++) {
                if (this.paramsNode.paramTypes[i3] == null) {
                    throw new EvalError("(Strict Java Mode) Undeclared argument type, parameter: " + this.paramsNode.getParamNames()[i3] + " in method: " + this.name, this, null);
                }
            }
            if (this.returnType == null) {
                throw new EvalError("(Strict Java Mode) Undeclared return type for method: " + this.name, this, null);
            }
        }
    }

    public Object eval(CallStack callStack, Interpreter interpreter) {
        this.returnType = evalReturnType(callStack, interpreter);
        evalNodes(callStack, interpreter);
        NameSpace pVar = callStack.top();
        try {
            pVar.setMethod(this.name, new BshMethod(this, pVar, this.modifiers));
            return Primitive.VOID;
        } catch (UtilEvalError e) {
            throw e.toEvalError(this, callStack);
        }
    }

    /* access modifiers changed from: package-private */
    public Class evalReturnType(CallStack callStack, Interpreter interpreter) {
        insureNodesParsed();
        if (this.returnTypeNode != null) {
            return this.returnTypeNode.evalReturnType(callStack, interpreter);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String getReturnTypeDescriptor(CallStack callStack, Interpreter interpreter, String str) {
        insureNodesParsed();
        if (this.returnTypeNode == null) {
            return null;
        }
        return this.returnTypeNode.getTypeDescriptor(callStack, interpreter, str);
    }

    /* access modifiers changed from: package-private */
    public BSHReturnType getReturnTypeNode() {
        insureNodesParsed();
        return this.returnTypeNode;
    }

    /* access modifiers changed from: package-private */
    public synchronized void insureNodesParsed() {
        if (this.paramsNode == null) {
            Node jjtGetChild = jjtGetChild(0);
            this.firstThrowsClause = 1;
            if (jjtGetChild instanceof BSHReturnType) {
                this.returnTypeNode = (BSHReturnType) jjtGetChild;
                this.paramsNode = (BSHFormalParameters) jjtGetChild(1);
                if (jjtGetNumChildren() > this.numThrows + 2) {
                    this.blockNode = (BSHBlock) jjtGetChild(this.numThrows + 2);
                }
                this.firstThrowsClause++;
            } else {
                this.paramsNode = (BSHFormalParameters) jjtGetChild(0);
                this.blockNode = (BSHBlock) jjtGetChild(this.numThrows + 1);
            }
        }
    }

    public String toString() {
        return "MethodDeclaration: " + this.name;
    }
}
