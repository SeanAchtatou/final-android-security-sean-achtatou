package bsh;

import java.util.Stack;

class JJTParserState {
    private Stack marks = new Stack();
    private int mk = 0;
    private boolean node_created;
    private Stack nodes = new Stack();
    private int sp = 0;

    JJTParserState() {
    }

    /* access modifiers changed from: package-private */
    public void clearNodeScope(Node node) {
        while (this.sp > this.mk) {
            popNode();
        }
        this.mk = ((Integer) this.marks.pop()).intValue();
    }

    /* access modifiers changed from: package-private */
    public void closeNodeScope(Node node, int i) {
        this.mk = ((Integer) this.marks.pop()).intValue();
        while (true) {
            int i2 = i - 1;
            if (i > 0) {
                Node popNode = popNode();
                popNode.jjtSetParent(node);
                node.jjtAddChild(popNode, i2);
                i = i2;
            } else {
                node.jjtClose();
                pushNode(node);
                this.node_created = true;
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void closeNodeScope(Node node, boolean z) {
        if (z) {
            int nodeArity = nodeArity();
            this.mk = ((Integer) this.marks.pop()).intValue();
            while (true) {
                int i = nodeArity;
                nodeArity = i - 1;
                if (i > 0) {
                    Node popNode = popNode();
                    popNode.jjtSetParent(node);
                    node.jjtAddChild(popNode, nodeArity);
                } else {
                    node.jjtClose();
                    pushNode(node);
                    this.node_created = true;
                    return;
                }
            }
        } else {
            this.mk = ((Integer) this.marks.pop()).intValue();
            this.node_created = false;
        }
    }

    /* access modifiers changed from: package-private */
    public int nodeArity() {
        return this.sp - this.mk;
    }

    /* access modifiers changed from: package-private */
    public boolean nodeCreated() {
        return this.node_created;
    }

    /* access modifiers changed from: package-private */
    public void openNodeScope(Node node) {
        this.marks.push(new Integer(this.mk));
        this.mk = this.sp;
        node.jjtOpen();
    }

    /* access modifiers changed from: package-private */
    public Node peekNode() {
        return (Node) this.nodes.peek();
    }

    /* access modifiers changed from: package-private */
    public Node popNode() {
        int i = this.sp - 1;
        this.sp = i;
        if (i < this.mk) {
            this.mk = ((Integer) this.marks.pop()).intValue();
        }
        return (Node) this.nodes.pop();
    }

    /* access modifiers changed from: package-private */
    public void pushNode(Node node) {
        this.nodes.push(node);
        this.sp++;
    }

    /* access modifiers changed from: package-private */
    public void reset() {
        this.nodes.removeAllElements();
        this.marks.removeAllElements();
        this.sp = 0;
        this.mk = 0;
    }

    /* access modifiers changed from: package-private */
    public Node rootNode() {
        return (Node) this.nodes.elementAt(0);
    }
}
