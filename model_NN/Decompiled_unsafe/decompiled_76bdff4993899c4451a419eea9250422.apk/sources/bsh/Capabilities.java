package bsh;

import java.util.Hashtable;

public class Capabilities {
    private static boolean accessibility = false;
    private static Hashtable classes = new Hashtable();

    public class Unavailable extends UtilEvalError {
        public Unavailable(String str) {
            super(str);
        }
    }

    public static boolean canGenerateInterfaces() {
        return classExists("java.lang.reflect.Proxy");
    }

    public static boolean classExists(String str) {
        Object obj = classes.get(str);
        if (obj == null) {
            try {
                obj = Class.forName(str);
            } catch (ClassNotFoundException e) {
            }
            if (obj != null) {
                classes.put(obj, "unused");
            }
        }
        return obj != null;
    }

    public static boolean haveAccessibility() {
        return accessibility;
    }

    public static boolean haveSwing() {
        return classExists("javax.swing.JButton");
    }

    public static void setAccessibility(boolean z) {
        if (!z) {
            accessibility = false;
        } else if (!classExists("java.lang.reflect.AccessibleObject") || !classExists("bsh.reflect.ReflectManagerImpl")) {
            throw new Unavailable("Accessibility unavailable");
        } else {
            try {
                String.class.getDeclaredMethods();
                accessibility = true;
            } catch (SecurityException e) {
                throw new Unavailable("Accessibility unavailable: " + e);
            }
        }
    }
}
