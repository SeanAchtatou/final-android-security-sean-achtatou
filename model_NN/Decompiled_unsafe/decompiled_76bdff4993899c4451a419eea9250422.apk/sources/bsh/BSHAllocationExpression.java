package bsh;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;

class BSHAllocationExpression extends SimpleNode {
    private static int innerClassCount = 0;

    BSHAllocationExpression(int i) {
        super(i);
    }

    private Object arrayAllocation(BSHArrayDimensions bSHArrayDimensions, Class cls, CallStack callStack, Interpreter interpreter) {
        Object eval = bSHArrayDimensions.eval(cls, callStack, interpreter);
        return eval != Primitive.VOID ? eval : arrayNewInstance(cls, bSHArrayDimensions, callStack);
    }

    private Object arrayNewInstance(Class cls, BSHArrayDimensions bSHArrayDimensions, CallStack callStack) {
        if (bSHArrayDimensions.numUndefinedDims > 0) {
            cls = Array.newInstance(cls, new int[bSHArrayDimensions.numUndefinedDims]).getClass();
        }
        try {
            return Array.newInstance(cls, bSHArrayDimensions.definedDimensions);
        } catch (NegativeArraySizeException e) {
            throw new TargetError(e, this, callStack);
        } catch (Exception e2) {
            throw new EvalError("Can't construct primitive array: " + e2.getMessage(), this, callStack);
        }
    }

    private Object constructObject(Class cls, Object[] objArr, CallStack callStack) {
        NameSpace classNameSpace;
        try {
            Object constructObject = Reflect.constructObject(cls, objArr);
            String name = cls.getName();
            if (!(name.indexOf("$") == -1 || (classNameSpace = Name.getClassNameSpace(callStack.top().getThis(null).getNameSpace())) == null || !name.startsWith(classNameSpace.getName() + "$"))) {
                try {
                    ClassGenerator.getClassGenerator().setInstanceNameSpaceParent(constructObject, name, classNameSpace);
                } catch (UtilEvalError e) {
                    throw e.toEvalError(this, callStack);
                }
            }
            return constructObject;
        } catch (ReflectError e2) {
            throw new EvalError("Constructor error: " + e2.getMessage(), this, callStack);
        } catch (InvocationTargetException e3) {
            Interpreter.debug("The constructor threw an exception:\n\t" + e3.getTargetException());
            throw new TargetError("Object constructor", e3.getTargetException(), this, callStack, true);
        }
    }

    private Object constructWithClassBody(Class cls, Object[] objArr, BSHBlock bSHBlock, CallStack callStack, Interpreter interpreter) {
        StringBuilder append = new StringBuilder().append(callStack.top().getName()).append("$");
        int i = innerClassCount + 1;
        innerClassCount = i;
        String sb = append.append(i).toString();
        Modifiers modifiers = new Modifiers();
        modifiers.addModifier(0, "public");
        try {
            try {
                return Reflect.constructObject(ClassGenerator.getClassGenerator().generateClass(sb, modifiers, null, cls, bSHBlock, false, callStack, interpreter), objArr);
            } catch (Exception e) {
                e = e;
                if (e instanceof InvocationTargetException) {
                    e = (Exception) ((InvocationTargetException) e).getTargetException();
                }
                if (Interpreter.DEBUG) {
                    e.printStackTrace();
                }
                throw new EvalError("Error constructing inner class instance: " + e, this, callStack);
            }
        } catch (UtilEvalError e2) {
            throw e2.toEvalError(this, callStack);
        }
    }

    private Object constructWithInterfaceBody(Class cls, Object[] objArr, BSHBlock bSHBlock, CallStack callStack, Interpreter interpreter) {
        NameSpace nameSpace = new NameSpace(callStack.top(), "AnonymousBlock");
        callStack.push(nameSpace);
        bSHBlock.eval(callStack, interpreter, true);
        callStack.pop();
        nameSpace.importStatic(cls);
        try {
            return nameSpace.getThis(interpreter).getInterface(cls);
        } catch (UtilEvalError e) {
            throw e.toEvalError(this, callStack);
        }
    }

    private Object objectAllocation(BSHAmbiguousName bSHAmbiguousName, BSHArguments bSHArguments, CallStack callStack, Interpreter interpreter) {
        callStack.top();
        Object[] arguments = bSHArguments.getArguments(callStack, interpreter);
        if (arguments == null) {
            throw new EvalError("Null args in new.", this, callStack);
        }
        bSHAmbiguousName.toObject(callStack, interpreter, false);
        Object object = bSHAmbiguousName.toObject(callStack, interpreter, true);
        if (object instanceof ClassIdentifier) {
            Class targetClass = ((ClassIdentifier) object).getTargetClass();
            if (!(jjtGetNumChildren() > 2)) {
                return constructObject(targetClass, arguments, callStack);
            }
            BSHBlock bSHBlock = (BSHBlock) jjtGetChild(2);
            return targetClass.isInterface() ? constructWithInterfaceBody(targetClass, arguments, bSHBlock, callStack, interpreter) : constructWithClassBody(targetClass, arguments, bSHBlock, callStack, interpreter);
        }
        throw new EvalError("Unknown class: " + bSHAmbiguousName.text, this, callStack);
    }

    private Object objectArrayAllocation(BSHAmbiguousName bSHAmbiguousName, BSHArrayDimensions bSHArrayDimensions, CallStack callStack, Interpreter interpreter) {
        NameSpace pVar = callStack.top();
        Class cls = bSHAmbiguousName.toClass(callStack, interpreter);
        if (cls != null) {
            return arrayAllocation(bSHArrayDimensions, cls, callStack, interpreter);
        }
        throw new EvalError("Class " + bSHAmbiguousName.getName(pVar) + " not found.", this, callStack);
    }

    private Object primitiveArrayAllocation(BSHPrimitiveType bSHPrimitiveType, BSHArrayDimensions bSHArrayDimensions, CallStack callStack, Interpreter interpreter) {
        return arrayAllocation(bSHArrayDimensions, bSHPrimitiveType.getType(), callStack, interpreter);
    }

    public Object eval(CallStack callStack, Interpreter interpreter) {
        SimpleNode simpleNode = (SimpleNode) jjtGetChild(0);
        SimpleNode simpleNode2 = (SimpleNode) jjtGetChild(1);
        if (!(simpleNode instanceof BSHAmbiguousName)) {
            return primitiveArrayAllocation((BSHPrimitiveType) simpleNode, (BSHArrayDimensions) simpleNode2, callStack, interpreter);
        }
        BSHAmbiguousName bSHAmbiguousName = (BSHAmbiguousName) simpleNode;
        return simpleNode2 instanceof BSHArguments ? objectAllocation(bSHAmbiguousName, (BSHArguments) simpleNode2, callStack, interpreter) : objectArrayAllocation(bSHAmbiguousName, (BSHArrayDimensions) simpleNode2, callStack, interpreter);
    }
}
