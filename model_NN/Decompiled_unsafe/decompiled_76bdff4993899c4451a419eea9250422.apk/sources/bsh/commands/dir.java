package bsh.commands;

import bsh.CallStack;
import bsh.Interpreter;
import bsh.StringUtil;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.GregorianCalendar;

public class dir {
    static final String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    public static void invoke(Interpreter interpreter, CallStack callStack) {
        invoke(interpreter, callStack, ".");
    }

    public static void invoke(Interpreter interpreter, CallStack callStack, String str) {
        try {
            String absolutePath = interpreter.pathToFile(str).getAbsolutePath();
            File pathToFile = interpreter.pathToFile(str);
            if (!pathToFile.exists() || !pathToFile.canRead()) {
                interpreter.println("Can't read " + pathToFile);
                return;
            }
            if (!pathToFile.isDirectory()) {
                interpreter.println("'" + str + "' is not a directory");
            }
            String[] bubbleSort = StringUtil.bubbleSort(pathToFile.list());
            for (int i = 0; i < bubbleSort.length; i++) {
                File file = new File(absolutePath + File.separator + bubbleSort[i]);
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(file.canRead() ? "r" : "-");
                stringBuffer.append(file.canWrite() ? "w" : "-");
                stringBuffer.append("_");
                stringBuffer.append(" ");
                Date date = new Date(file.lastModified());
                GregorianCalendar gregorianCalendar = new GregorianCalendar();
                gregorianCalendar.setTime(date);
                int i2 = gregorianCalendar.get(5);
                stringBuffer.append(months[gregorianCalendar.get(2)] + " " + i2);
                if (i2 < 10) {
                    stringBuffer.append(" ");
                }
                stringBuffer.append(" ");
                StringBuffer stringBuffer2 = new StringBuffer();
                for (int i3 = 0; i3 < 8; i3++) {
                    stringBuffer2.append(" ");
                }
                stringBuffer2.insert(0, file.length());
                stringBuffer2.setLength(8);
                int indexOf = stringBuffer2.toString().indexOf(" ");
                if (indexOf != -1) {
                    String substring = stringBuffer2.toString().substring(indexOf);
                    stringBuffer2.setLength(indexOf);
                    stringBuffer2.insert(0, substring);
                }
                stringBuffer.append(stringBuffer2.toString());
                stringBuffer.append(" " + file.getName());
                if (file.isDirectory()) {
                    stringBuffer.append("/");
                }
                interpreter.println(stringBuffer.toString());
            }
        } catch (IOException e) {
            interpreter.println("error reading path: " + e);
        }
    }

    public static String usage() {
        return "usage: dir( String dir )\n       dir()";
    }
}
