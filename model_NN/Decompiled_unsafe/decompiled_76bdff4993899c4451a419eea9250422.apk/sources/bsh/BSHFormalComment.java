package bsh;

public class BSHFormalComment extends SimpleNode {
    public String text;

    public BSHFormalComment(int i) {
        super(i);
    }

    public /* bridge */ /* synthetic */ void dump(String str) {
        super.dump(str);
    }

    public /* bridge */ /* synthetic */ Object eval(CallStack callStack, Interpreter interpreter) {
        return super.eval(callStack, interpreter);
    }

    public /* bridge */ /* synthetic */ SimpleNode getChild(int i) {
        return super.getChild(i);
    }

    public /* bridge */ /* synthetic */ int getLineNumber() {
        return super.getLineNumber();
    }

    public /* bridge */ /* synthetic */ String getSourceFile() {
        return super.getSourceFile();
    }

    public /* bridge */ /* synthetic */ String getText() {
        return super.getText();
    }

    public /* bridge */ /* synthetic */ void jjtAddChild(Node node, int i) {
        super.jjtAddChild(node, i);
    }

    public /* bridge */ /* synthetic */ void jjtClose() {
        super.jjtClose();
    }

    public /* bridge */ /* synthetic */ Node jjtGetChild(int i) {
        return super.jjtGetChild(i);
    }

    public /* bridge */ /* synthetic */ int jjtGetNumChildren() {
        return super.jjtGetNumChildren();
    }

    public /* bridge */ /* synthetic */ Node jjtGetParent() {
        return super.jjtGetParent();
    }

    public /* bridge */ /* synthetic */ void jjtOpen() {
        super.jjtOpen();
    }

    public /* bridge */ /* synthetic */ void jjtSetParent(Node node) {
        super.jjtSetParent(node);
    }

    public /* bridge */ /* synthetic */ void prune() {
        super.prune();
    }

    public /* bridge */ /* synthetic */ void setSourceFile(String str) {
        super.setSourceFile(str);
    }

    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    public /* bridge */ /* synthetic */ String toString(String str) {
        return super.toString(str);
    }
}
