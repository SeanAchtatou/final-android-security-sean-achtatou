package bsh;

class BlockNameSpace extends NameSpace {
    public BlockNameSpace(NameSpace nameSpace) {
        super(nameSpace, nameSpace.getName() + "/BlockNameSpace");
    }

    private NameSpace getNonBlockParent() {
        while (true) {
            NameSpace parent = super.getParent();
            if (!(parent instanceof BlockNameSpace)) {
                return parent;
            }
            this = (BlockNameSpace) parent;
        }
    }

    private boolean weHaveVar(String str) {
        try {
            return super.getVariableImpl(str, false) != null;
        } catch (UtilEvalError e) {
            return false;
        }
    }

    public This getSuper(Interpreter interpreter) {
        return getNonBlockParent().getSuper(interpreter);
    }

    public This getThis(Interpreter interpreter) {
        return getNonBlockParent().getThis(interpreter);
    }

    public void importClass(String str) {
        getParent().importClass(str);
    }

    public void importPackage(String str) {
        getParent().importPackage(str);
    }

    public void setBlockVariable(String str, Object obj) {
        super.setVariable(str, obj, false, false);
    }

    public void setMethod(String str, BshMethod bshMethod) {
        getParent().setMethod(str, bshMethod);
    }

    public void setVariable(String str, Object obj, boolean z, boolean z2) {
        if (weHaveVar(str)) {
            super.setVariable(str, obj, z, false);
        } else {
            getParent().setVariable(str, obj, z, z2);
        }
    }
}
