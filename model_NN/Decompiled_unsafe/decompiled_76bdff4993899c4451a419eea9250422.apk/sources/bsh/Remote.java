package bsh;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLEncoder;

public class Remote {
    static String doBsh(String str, String str2) {
        try {
            String substring = str.substring(6);
            int indexOf = substring.indexOf(":");
            String substring2 = substring.substring(0, indexOf);
            String substring3 = substring.substring(indexOf + 1, substring.length());
            try {
                System.out.println("Connecting to host : " + substring2 + " at port : " + substring3);
                Socket socket = new Socket(substring2, Integer.parseInt(substring3) + 1);
                OutputStream outputStream = socket.getOutputStream();
                InputStream inputStream = socket.getInputStream();
                sendLine(str2, outputStream);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        return "1";
                    }
                    System.out.println(readLine);
                }
            } catch (Exception e) {
                System.err.println("Error communicating with server: " + e);
                return "-1";
            }
        } catch (Exception e2) {
            System.err.println("Bad URL: " + str + ": " + e2);
            return "-1";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void}
     arg types: [java.io.OutputStreamWriter, int]
     candidates:
      ClspMth{java.io.PrintWriter.<init>(java.io.File, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.OutputStream, boolean):void}
      ClspMth{java.io.PrintWriter.<init>(java.lang.String, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void} */
    static String doHttp(String str, String str2) {
        String str3;
        MalformedURLException malformedURLException;
        IOException iOException;
        String str4;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("bsh.client=Remote");
        stringBuffer.append("&bsh.script=");
        stringBuffer.append(URLEncoder.encode(str2));
        String stringBuffer2 = stringBuffer.toString();
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            PrintWriter printWriter = new PrintWriter((Writer) new OutputStreamWriter(httpURLConnection.getOutputStream(), "8859_1"), true);
            printWriter.print(stringBuffer2);
            printWriter.flush();
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode != 200) {
                System.out.println("Error, HTTP response: " + responseCode);
            }
            String headerField = httpURLConnection.getHeaderField("Bsh-Return");
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        System.out.println(readLine);
                    } else {
                        System.out.println("Return Value: " + headerField);
                        return headerField;
                    }
                }
            } catch (MalformedURLException e) {
                MalformedURLException malformedURLException2 = e;
                str3 = headerField;
                malformedURLException = malformedURLException2;
                System.out.println(malformedURLException);
                return str3;
            } catch (IOException e2) {
                IOException iOException2 = e2;
                str4 = headerField;
                iOException = iOException2;
                System.out.println(iOException);
                return str4;
            }
        } catch (MalformedURLException e3) {
            MalformedURLException malformedURLException3 = e3;
            str3 = null;
            malformedURLException = malformedURLException3;
            System.out.println(malformedURLException);
            return str3;
        } catch (IOException e4) {
            IOException iOException3 = e4;
            str4 = null;
            iOException = iOException3;
            System.out.println(iOException);
            return str4;
        }
    }

    public static int eval(String str, String str2) {
        String doBsh;
        if (str.startsWith("http:")) {
            doBsh = doHttp(str, str2);
        } else if (str.startsWith("bsh:")) {
            doBsh = doBsh(str, str2);
        } else {
            throw new IOException("Unrecognized URL type.Scheme must be http:// or bsh://");
        }
        try {
            return Integer.parseInt(doBsh);
        } catch (Exception e) {
            return 0;
        }
    }

    static String getFile(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(str));
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return stringBuffer.toString();
            }
            stringBuffer.append(readLine).append("\n");
        }
    }

    public static void main(String[] strArr) {
        if (strArr.length < 2) {
            System.out.println("usage: Remote URL(http|bsh) file [ file ] ... ");
            System.exit(1);
        }
        System.exit(eval(strArr[0], getFile(strArr[1])));
    }

    private static void sendLine(String str, OutputStream outputStream) {
        outputStream.write(str.getBytes());
        outputStream.flush();
    }
}
