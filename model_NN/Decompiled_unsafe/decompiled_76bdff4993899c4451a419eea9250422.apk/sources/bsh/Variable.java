package bsh;

import java.io.Serializable;

public class Variable implements Serializable {
    static final int ASSIGNMENT = 1;
    static final int DECLARATION = 0;
    LHS lhs;
    Modifiers modifiers;
    String name;
    Class type;
    String typeDescriptor;
    Object value;

    Variable(String str, Class cls, LHS lhs2) {
        this.type = null;
        this.name = str;
        this.lhs = lhs2;
        this.type = cls;
    }

    Variable(String str, Class cls, Object obj, Modifiers modifiers2) {
        this.type = null;
        this.name = str;
        this.type = cls;
        this.modifiers = modifiers2;
        setValue(obj, 0);
    }

    Variable(String str, Object obj, Modifiers modifiers2) {
        this(str, (Class) null, obj, modifiers2);
    }

    Variable(String str, String str2, Object obj, Modifiers modifiers2) {
        this(str, (Class) null, obj, modifiers2);
        this.typeDescriptor = str2;
    }

    public Modifiers getModifiers() {
        return this.modifiers;
    }

    public String getName() {
        return this.name;
    }

    public Class getType() {
        return this.type;
    }

    public String getTypeDescriptor() {
        return this.typeDescriptor;
    }

    /* access modifiers changed from: package-private */
    public Object getValue() {
        return this.lhs != null ? this.type == null ? this.lhs.getValue() : Primitive.wrap(this.lhs.getValue(), this.type) : this.value;
    }

    public boolean hasModifier(String str) {
        return this.modifiers != null && this.modifiers.hasModifier(str);
    }

    public void setValue(Object obj, int i) {
        int i2 = 0;
        if (!hasModifier("final") || this.value == null) {
            if (obj == null) {
                obj = Primitive.getDefaultValue(this.type);
            }
            if (this.lhs != null) {
                this.lhs.assign(Primitive.unwrap(obj), false);
                return;
            }
            if (this.type != null) {
                Class cls = this.type;
                if (i != 0) {
                    i2 = 1;
                }
                obj = Types.castObject(obj, cls, i2);
            }
            this.value = obj;
            return;
        }
        throw new UtilEvalError("Final variable, can't re-assign.");
    }

    public String toString() {
        return "Variable: " + super.toString() + " " + this.name + ", type:" + this.type + ", value:" + this.value + ", lhs = " + this.lhs;
    }
}
