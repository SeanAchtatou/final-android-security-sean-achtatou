package bsh;

class BSHTryStatement extends SimpleNode {
    BSHTryStatement(int i) {
        super(i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a1, code lost:
        r1 = (bsh.BSHBlock) r9.elementAt(r7);
        r5 = r13.top();
        r6 = new bsh.BlockNameSpace(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b4, code lost:
        if (r0.type != bsh.BSHFormalParameter.UNTYPED) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b6, code lost:
        r6.setBlockVariable(r0.name, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00bb, code lost:
        r13.swap(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r0 = r1.eval(r13, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:?, code lost:
        new bsh.Modifiers();
        r6.setTypedVariable(r0.name, r0.type, r4, new bsh.Modifiers());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00e8, code lost:
        throw new bsh.InterpreterError("Unable to set var in catch block namespace.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00e9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ea, code lost:
        r13.swap(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00ed, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object eval(bsh.CallStack r13, bsh.Interpreter r14) {
        /*
            r12 = this;
            r1 = 1
            r7 = 0
            r3 = 0
            bsh.Node r0 = r12.jjtGetChild(r7)
            bsh.BSHBlock r0 = (bsh.BSHBlock) r0
            java.util.Vector r8 = new java.util.Vector
            r8.<init>()
            java.util.Vector r9 = new java.util.Vector
            r9.<init>()
            int r2 = r12.jjtGetNumChildren()
        L_0x0017:
            if (r1 >= r2) goto L_0x0030
            int r4 = r1 + 1
            bsh.Node r1 = r12.jjtGetChild(r1)
            boolean r5 = r1 instanceof bsh.BSHFormalParameter
            if (r5 == 0) goto L_0x0031
            r8.addElement(r1)
            int r1 = r4 + 1
            bsh.Node r4 = r12.jjtGetChild(r4)
            r9.addElement(r4)
            goto L_0x0017
        L_0x0030:
            r1 = r3
        L_0x0031:
            if (r1 == 0) goto L_0x0105
            bsh.BSHBlock r1 = (bsh.BSHBlock) r1
            r2 = r1
        L_0x0036:
            int r1 = r13.depth()
            java.lang.Object r5 = r0.eval(r13, r14)     // Catch:{ TargetError -> 0x0068 }
            r6 = r3
        L_0x003f:
            if (r6 == 0) goto L_0x00ff
            java.lang.Throwable r4 = r6.getTarget()
        L_0x0045:
            if (r4 == 0) goto L_0x00fc
            int r10 = r8.size()
        L_0x004b:
            if (r7 >= r10) goto L_0x00fc
            java.lang.Object r0 = r8.elementAt(r7)
            bsh.BSHFormalParameter r0 = (bsh.BSHFormalParameter) r0
            r0.eval(r13, r14)
            java.lang.Class r1 = r0.type
            if (r1 != 0) goto L_0x0093
            boolean r1 = r14.getStrictJava()
            if (r1 == 0) goto L_0x0093
            bsh.EvalError r0 = new bsh.EvalError
            java.lang.String r1 = "(Strict Java) Untyped catch block"
            r0.<init>(r1, r12, r13)
            throw r0
        L_0x0068:
            r6 = move-exception
            java.lang.String r0 = "Bsh Stack: "
        L_0x006b:
            int r4 = r13.depth()
            if (r4 <= r1) goto L_0x0102
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r4 = "\t"
            java.lang.StringBuilder r0 = r0.append(r4)
            bsh.NameSpace r4 = r13.pop()
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = "\n"
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            goto L_0x006b
        L_0x0093:
            java.lang.Class r1 = r0.type
            if (r1 == 0) goto L_0x00a1
            java.lang.Class r1 = r0.type     // Catch:{ UtilEvalError -> 0x00ee }
            r11 = 1
            java.lang.Object r1 = bsh.Types.castObject(r4, r1, r11)     // Catch:{ UtilEvalError -> 0x00ee }
            java.lang.Throwable r1 = (java.lang.Throwable) r1     // Catch:{ UtilEvalError -> 0x00ee }
            r4 = r1
        L_0x00a1:
            java.lang.Object r1 = r9.elementAt(r7)
            bsh.BSHBlock r1 = (bsh.BSHBlock) r1
            bsh.NameSpace r5 = r13.top()
            bsh.BlockNameSpace r6 = new bsh.BlockNameSpace
            r6.<init>(r5)
            java.lang.Class r7 = r0.type     // Catch:{ UtilEvalError -> 0x00e0 }
            java.lang.Class r8 = bsh.BSHFormalParameter.UNTYPED     // Catch:{ UtilEvalError -> 0x00e0 }
            if (r7 != r8) goto L_0x00ce
            java.lang.String r0 = r0.name     // Catch:{ UtilEvalError -> 0x00e0 }
            r6.setBlockVariable(r0, r4)     // Catch:{ UtilEvalError -> 0x00e0 }
        L_0x00bb:
            r13.swap(r6)
            java.lang.Object r0 = r1.eval(r13, r14)     // Catch:{ all -> 0x00e9 }
            r13.swap(r5)
        L_0x00c5:
            if (r2 == 0) goto L_0x00cb
            java.lang.Object r0 = r2.eval(r13, r14)
        L_0x00cb:
            if (r3 == 0) goto L_0x00f4
            throw r3
        L_0x00ce:
            bsh.Modifiers r7 = new bsh.Modifiers     // Catch:{ UtilEvalError -> 0x00e0 }
            r7.<init>()     // Catch:{ UtilEvalError -> 0x00e0 }
            java.lang.String r7 = r0.name     // Catch:{ UtilEvalError -> 0x00e0 }
            java.lang.Class r0 = r0.type     // Catch:{ UtilEvalError -> 0x00e0 }
            bsh.Modifiers r8 = new bsh.Modifiers     // Catch:{ UtilEvalError -> 0x00e0 }
            r8.<init>()     // Catch:{ UtilEvalError -> 0x00e0 }
            r6.setTypedVariable(r7, r0, r4, r8)     // Catch:{ UtilEvalError -> 0x00e0 }
            goto L_0x00bb
        L_0x00e0:
            r0 = move-exception
            bsh.InterpreterError r0 = new bsh.InterpreterError
            java.lang.String r1 = "Unable to set var in catch block namespace."
            r0.<init>(r1)
            throw r0
        L_0x00e9:
            r0 = move-exception
            r13.swap(r5)
            throw r0
        L_0x00ee:
            r0 = move-exception
            int r0 = r7 + 1
            r7 = r0
            goto L_0x004b
        L_0x00f4:
            boolean r1 = r0 instanceof bsh.ReturnControl
            if (r1 == 0) goto L_0x00f9
        L_0x00f8:
            return r0
        L_0x00f9:
            bsh.Primitive r0 = bsh.Primitive.VOID
            goto L_0x00f8
        L_0x00fc:
            r0 = r5
            r3 = r6
            goto L_0x00c5
        L_0x00ff:
            r4 = r3
            goto L_0x0045
        L_0x0102:
            r5 = r3
            goto L_0x003f
        L_0x0105:
            r2 = r3
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.BSHTryStatement.eval(bsh.CallStack, bsh.Interpreter):java.lang.Object");
    }
}
