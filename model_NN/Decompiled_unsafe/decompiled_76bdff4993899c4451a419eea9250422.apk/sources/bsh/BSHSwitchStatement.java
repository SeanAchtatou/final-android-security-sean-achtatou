package bsh;

class BSHSwitchStatement extends SimpleNode implements ParserConstants {
    public BSHSwitchStatement(int i) {
        super(i);
    }

    private boolean primitiveEquals(Object obj, Object obj2, CallStack callStack, SimpleNode simpleNode) {
        if (!(obj instanceof Primitive) && !(obj2 instanceof Primitive)) {
            return obj.equals(obj2);
        }
        try {
            return Primitive.unwrap(Primitive.binaryOperation(obj, obj2, 90)).equals(Boolean.TRUE);
        } catch (UtilEvalError e) {
            throw e.toEvalError("Switch value: " + simpleNode.getText() + ": ", this, callStack);
        }
    }

    public Object eval(CallStack callStack, Interpreter interpreter) {
        int jjtGetNumChildren = jjtGetNumChildren();
        SimpleNode simpleNode = (SimpleNode) jjtGetChild(0);
        Object eval = simpleNode.eval(callStack, interpreter);
        ReturnControl returnControl = null;
        if (1 >= jjtGetNumChildren) {
            throw new EvalError("Empty switch statement.", this, callStack);
        }
        BSHSwitchLabel bSHSwitchLabel = (BSHSwitchLabel) jjtGetChild(1);
        int i = 2;
        while (i < jjtGetNumChildren && returnControl == null) {
            if (!bSHSwitchLabel.isDefault && !primitiveEquals(eval, bSHSwitchLabel.eval(callStack, interpreter), callStack, simpleNode)) {
                while (true) {
                    if (i >= jjtGetNumChildren) {
                        break;
                    }
                    int i2 = i + 1;
                    Node jjtGetChild = jjtGetChild(i);
                    if (jjtGetChild instanceof BSHSwitchLabel) {
                        bSHSwitchLabel = (BSHSwitchLabel) jjtGetChild;
                        i = i2;
                        break;
                    }
                    i = i2;
                }
            } else {
                while (true) {
                    if (i >= jjtGetNumChildren) {
                        break;
                    }
                    int i3 = i + 1;
                    Node jjtGetChild2 = jjtGetChild(i);
                    if (!(jjtGetChild2 instanceof BSHSwitchLabel)) {
                        Object eval2 = ((SimpleNode) jjtGetChild2).eval(callStack, interpreter);
                        if (eval2 instanceof ReturnControl) {
                            returnControl = (ReturnControl) eval2;
                            i = i3;
                            break;
                        }
                        i = i3;
                    } else {
                        i = i3;
                    }
                }
            }
        }
        return (returnControl == null || returnControl.kind != 46) ? Primitive.VOID : returnControl;
    }
}
