package bsh;

public class UtilTargetError extends UtilEvalError {
    public Throwable t;

    public UtilTargetError(String str, Throwable th) {
        super(str);
        this.t = th;
    }

    public UtilTargetError(Throwable th) {
        this(null, th);
    }

    public EvalError toEvalError(String str, SimpleNode simpleNode, CallStack callStack) {
        return new TargetError(str == null ? getMessage() : str + ": " + getMessage(), this.t, simpleNode, callStack, false);
    }
}
