package bsh;

class BSHLiteral extends SimpleNode {
    public Object value;

    BSHLiteral(int i) {
        super(i);
    }

    private char getEscapeChar(char c) {
        switch (c) {
            case ParserConstants.BOOL_AND /*98*/:
                return 8;
            case ParserConstants.PLUS /*102*/:
                return 12;
            case ParserConstants.XOR /*110*/:
                return 10;
            case ParserConstants.RSIGNEDSHIFT /*114*/:
                return 13;
            case ParserConstants.RUNSIGNEDSHIFT /*116*/:
                return 9;
            default:
                return c;
        }
    }

    public void charSetup(String str) {
        char charAt = str.charAt(0);
        if (charAt == '\\') {
            char charAt2 = str.charAt(1);
            charAt = Character.isDigit(charAt2) ? (char) Integer.parseInt(str.substring(1), 8) : getEscapeChar(charAt2);
        }
        this.value = new Primitive(new Character(charAt).charValue());
    }

    public Object eval(CallStack callStack, Interpreter interpreter) {
        if (this.value != null) {
            return this.value;
        }
        throw new InterpreterError("Null in bsh literal: " + this.value);
    }

    /* access modifiers changed from: package-private */
    public void stringSetup(String str) {
        int i;
        char c;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < str.length(); i2 = i + 1) {
            char charAt = str.charAt(i2);
            if (charAt == '\\') {
                int i3 = i2 + 1;
                char charAt2 = str.charAt(i3);
                if (Character.isDigit(charAt2)) {
                    i = i3;
                    while (i < i3 + 2 && Character.isDigit(str.charAt(i + 1))) {
                        i++;
                    }
                    c = (char) Integer.parseInt(str.substring(i3, i + 1), 8);
                } else {
                    c = getEscapeChar(charAt2);
                    i = i3;
                }
            } else {
                char c2 = charAt;
                i = i2;
                c = c2;
            }
            stringBuffer.append(c);
        }
        this.value = stringBuffer.toString().intern();
    }
}
