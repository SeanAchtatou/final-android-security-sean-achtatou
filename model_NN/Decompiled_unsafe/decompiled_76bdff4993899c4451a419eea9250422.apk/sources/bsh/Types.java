package bsh;

class Types {
    static final int ASSIGNMENT = 1;
    static final int BSH_ASSIGNABLE = 4;
    static final int CAST = 0;
    static final int FIRST_ROUND_ASSIGNABLE = 1;
    static Primitive INVALID_CAST = new Primitive(-1);
    static final int JAVA_BASE_ASSIGNABLE = 1;
    static final int JAVA_BOX_TYPES_ASSIGABLE = 2;
    static final int JAVA_VARARGS_ASSIGNABLE = 3;
    static final int LAST_ROUND_ASSIGNABLE = 4;
    static Primitive VALID_CAST = new Primitive(1);

    Types() {
    }

    static boolean areSignaturesEqual(Class[] clsArr, Class[] clsArr2) {
        if (clsArr.length != clsArr2.length) {
            return false;
        }
        for (int i = 0; i < clsArr.length; i++) {
            if (clsArr[i] != clsArr2[i]) {
                return false;
            }
        }
        return true;
    }

    static UtilEvalError castError(Class cls, Class cls2, int i) {
        return castError(Reflect.normalizeClassName(cls), Reflect.normalizeClassName(cls2), i);
    }

    static UtilEvalError castError(String str, String str2, int i) {
        return i == 1 ? new UtilEvalError("Can't assign " + str2 + " to " + str) : new UtilTargetError(new ClassCastException("Cannot cast " + str2 + " to " + str));
    }

    private static Object castObject(Class cls, Class cls2, Object obj, int i, boolean z) {
        if (z && obj != null) {
            throw new InterpreterError("bad cast params 1");
        } else if (!z && obj == null) {
            throw new InterpreterError("bad cast params 2");
        } else if (cls2 == Primitive.class) {
            throw new InterpreterError("bad from Type, need to unwrap");
        } else if (obj == Primitive.NULL && cls2 != null) {
            throw new InterpreterError("inconsistent args 1");
        } else if (obj == Primitive.VOID && cls2 != Void.TYPE) {
            throw new InterpreterError("inconsistent args 2");
        } else if (cls == Void.TYPE) {
            throw new InterpreterError("loose toType should be null");
        } else if (cls == null || cls == cls2) {
            return z ? VALID_CAST : obj;
        } else {
            if (cls.isPrimitive()) {
                if (cls2 == Void.TYPE || cls2 == null || cls2.isPrimitive()) {
                    return Primitive.castPrimitive(cls, cls2, (Primitive) obj, z, i);
                }
                if (Primitive.isWrapperType(cls2)) {
                    Class unboxType = Primitive.unboxType(cls2);
                    return Primitive.castPrimitive(cls, unboxType, z ? null : (Primitive) Primitive.wrap(obj, unboxType), z, i);
                } else if (z) {
                    return INVALID_CAST;
                } else {
                    throw castError(cls, cls2, i);
                }
            } else if (cls2 == Void.TYPE || cls2 == null || cls2.isPrimitive()) {
                return (!Primitive.isWrapperType(cls) || cls2 == Void.TYPE || cls2 == null) ? (cls != Object.class || cls2 == Void.TYPE || cls2 == null) ? Primitive.castPrimitive(cls, cls2, (Primitive) obj, z, i) : z ? VALID_CAST : ((Primitive) obj).getValue() : z ? VALID_CAST : Primitive.castWrapper(Primitive.unboxType(cls), ((Primitive) obj).getValue());
            } else {
                if (cls.isAssignableFrom(cls2)) {
                    return z ? VALID_CAST : obj;
                }
                if (cls.isInterface() && This.class.isAssignableFrom(cls2) && Capabilities.canGenerateInterfaces()) {
                    return z ? VALID_CAST : ((This) obj).getInterface(cls);
                }
                if (Primitive.isWrapperType(cls) && Primitive.isWrapperType(cls2)) {
                    return z ? VALID_CAST : Primitive.castWrapper(cls, obj);
                }
                if (z) {
                    return INVALID_CAST;
                }
                throw castError(cls, cls2, i);
            }
        }
    }

    public static Object castObject(Object obj, Class cls, int i) {
        if (obj == null) {
            throw new InterpreterError("null fromValue");
        }
        return castObject(cls, obj instanceof Primitive ? ((Primitive) obj).getType() : obj.getClass(), obj, i, false);
    }

    public static Class[] getTypes(Object[] objArr) {
        int i = 0;
        if (objArr == null) {
            return new Class[0];
        }
        Class[] clsArr = new Class[objArr.length];
        while (true) {
            int i2 = i;
            if (i2 >= objArr.length) {
                return clsArr;
            }
            if (objArr[i2] == null) {
                clsArr[i2] = null;
            } else if (objArr[i2] instanceof Primitive) {
                clsArr[i2] = ((Primitive) objArr[i2]).getType();
            } else {
                clsArr[i2] = objArr[i2].getClass();
            }
            i = i2 + 1;
        }
    }

    static boolean isBshAssignable(Class cls, Class cls2) {
        try {
            return castObject(cls, cls2, null, 1, true) == VALID_CAST;
        } catch (UtilEvalError e) {
            throw new InterpreterError("err in cast check: " + e);
        }
    }

    static boolean isJavaAssignable(Class cls, Class cls2) {
        return isJavaBaseAssignable(cls, cls2) || isJavaBoxTypesAssignable(cls, cls2);
    }

    static boolean isJavaBaseAssignable(Class cls, Class cls2) {
        if (cls == null) {
            return false;
        }
        if (cls2 == null) {
            return !cls.isPrimitive();
        }
        if (!cls.isPrimitive() || !cls2.isPrimitive()) {
            return cls.isAssignableFrom(cls2);
        }
        if (cls == cls2) {
            return true;
        }
        if (cls2 == Byte.TYPE && (cls == Short.TYPE || cls == Integer.TYPE || cls == Long.TYPE || cls == Float.TYPE || cls == Double.TYPE)) {
            return true;
        }
        if (cls2 == Short.TYPE && (cls == Integer.TYPE || cls == Long.TYPE || cls == Float.TYPE || cls == Double.TYPE)) {
            return true;
        }
        if (cls2 == Character.TYPE && (cls == Integer.TYPE || cls == Long.TYPE || cls == Float.TYPE || cls == Double.TYPE)) {
            return true;
        }
        if (cls2 == Integer.TYPE && (cls == Long.TYPE || cls == Float.TYPE || cls == Double.TYPE)) {
            return true;
        }
        if (cls2 == Long.TYPE && (cls == Float.TYPE || cls == Double.TYPE)) {
            return true;
        }
        return cls2 == Float.TYPE && cls == Double.TYPE;
    }

    static boolean isJavaBoxTypesAssignable(Class cls, Class cls2) {
        if (cls == null) {
            return false;
        }
        if (cls == Object.class) {
            return true;
        }
        if (cls != Number.class || cls2 == Character.TYPE || cls2 == Boolean.TYPE) {
            return Primitive.wrapperMap.get(cls) == cls2;
        }
        return true;
    }

    static boolean isSignatureAssignable(Class[] clsArr, Class[] clsArr2, int i) {
        if (i != 3 && clsArr.length != clsArr2.length) {
            return false;
        }
        switch (i) {
            case 1:
                for (int i2 = 0; i2 < clsArr.length; i2++) {
                    if (!isJavaBaseAssignable(clsArr2[i2], clsArr[i2])) {
                        return false;
                    }
                }
                return true;
            case 2:
                for (int i3 = 0; i3 < clsArr.length; i3++) {
                    if (!isJavaBoxTypesAssignable(clsArr2[i3], clsArr[i3])) {
                        return false;
                    }
                }
                return true;
            case 3:
                return isSignatureVarargsAssignable(clsArr, clsArr2);
            case ParserTreeConstants.JJTIMPORTDECLARATION:
                for (int i4 = 0; i4 < clsArr.length; i4++) {
                    if (!isBshAssignable(clsArr2[i4], clsArr[i4])) {
                        return false;
                    }
                }
                return true;
            default:
                throw new InterpreterError("bad case");
        }
    }

    private static boolean isSignatureVarargsAssignable(Class[] clsArr, Class[] clsArr2) {
        return false;
    }
}
