package bsh;

import java.io.Serializable;
import java.util.Hashtable;

public final class Primitive implements ParserConstants, Serializable {
    public static Primitive FALSE = new Primitive(false);
    public static final Primitive NULL = new Primitive(Special.NULL_VALUE);
    public static Primitive TRUE = new Primitive(true);
    public static final Primitive VOID = new Primitive(Special.VOID_TYPE);
    static Hashtable wrapperMap;
    private Object value;

    class Special implements Serializable {
        public static final Special NULL_VALUE = new Special();
        public static final Special VOID_TYPE = new Special();

        private Special() {
        }
    }

    static {
        Hashtable hashtable = new Hashtable();
        wrapperMap = hashtable;
        hashtable.put(Boolean.TYPE, Boolean.class);
        wrapperMap.put(Byte.TYPE, Byte.class);
        wrapperMap.put(Short.TYPE, Short.class);
        wrapperMap.put(Character.TYPE, Character.class);
        wrapperMap.put(Integer.TYPE, Integer.class);
        wrapperMap.put(Long.TYPE, Long.class);
        wrapperMap.put(Float.TYPE, Float.class);
        wrapperMap.put(Double.TYPE, Double.class);
        wrapperMap.put(Boolean.class, Boolean.TYPE);
        wrapperMap.put(Byte.class, Byte.TYPE);
        wrapperMap.put(Short.class, Short.TYPE);
        wrapperMap.put(Character.class, Character.TYPE);
        wrapperMap.put(Integer.class, Integer.TYPE);
        wrapperMap.put(Long.class, Long.TYPE);
        wrapperMap.put(Float.class, Float.TYPE);
        wrapperMap.put(Double.class, Double.TYPE);
    }

    public Primitive(byte b) {
        this(new Byte(b));
    }

    public Primitive(char c) {
        this(new Character(c));
    }

    public Primitive(double d) {
        this(new Double(d));
    }

    public Primitive(float f) {
        this(new Float(f));
    }

    public Primitive(int i) {
        this(new Integer(i));
    }

    public Primitive(long j) {
        this(new Long(j));
    }

    public Primitive(Object obj) {
        if (obj == null) {
            throw new InterpreterError("Use Primitve.NULL instead of Primitive(null)");
        } else if (obj == Special.NULL_VALUE || obj == Special.VOID_TYPE || isWrapperType(obj.getClass())) {
            this.value = obj;
        } else {
            throw new InterpreterError("Not a wrapper type: " + obj.getClass());
        }
    }

    public Primitive(short s) {
        this(new Short(s));
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Primitive(boolean z) {
        this(z ? Boolean.TRUE : Boolean.FALSE);
    }

    public static Object binaryOperation(Object obj, Object obj2, int i) {
        if (obj == NULL || obj2 == NULL) {
            throw new UtilEvalError("Null value or 'null' literal in binary operation");
        } else if (obj == VOID || obj2 == VOID) {
            throw new UtilEvalError("Undefined variable, class, or 'void' literal in binary operation");
        } else {
            Class<?> cls = obj.getClass();
            Class<?> cls2 = obj2.getClass();
            if (obj instanceof Primitive) {
                obj = ((Primitive) obj).getValue();
            }
            if (obj2 instanceof Primitive) {
                obj2 = ((Primitive) obj2).getValue();
            }
            Object[] promotePrimitives = promotePrimitives(obj, obj2);
            Object obj3 = promotePrimitives[0];
            Object obj4 = promotePrimitives[1];
            if (obj3.getClass() != obj4.getClass()) {
                throw new UtilEvalError("Type mismatch in operator.  " + obj3.getClass() + " cannot be used with " + obj4.getClass());
            }
            try {
                Object binaryOperationImpl = binaryOperationImpl(obj3, obj4, i);
                return binaryOperationImpl instanceof Boolean ? ((Boolean) binaryOperationImpl).booleanValue() ? TRUE : FALSE : (cls == Primitive.class && cls2 == Primitive.class) ? new Primitive(binaryOperationImpl) : binaryOperationImpl;
            } catch (ArithmeticException e) {
                throw new UtilTargetError("Arithemetic Exception in binary op", e);
            }
        }
    }

    static Object binaryOperationImpl(Object obj, Object obj2, int i) {
        if (obj instanceof Boolean) {
            return booleanBinaryOperation((Boolean) obj, (Boolean) obj2, i);
        }
        if (obj instanceof Integer) {
            return intBinaryOperation((Integer) obj, (Integer) obj2, i);
        }
        if (obj instanceof Long) {
            return longBinaryOperation((Long) obj, (Long) obj2, i);
        }
        if (obj instanceof Float) {
            return floatBinaryOperation((Float) obj, (Float) obj2, i);
        }
        if (obj instanceof Double) {
            return doubleBinaryOperation((Double) obj, (Double) obj2, i);
        }
        throw new UtilEvalError("Invalid types in binary operator");
    }

    static Boolean booleanBinaryOperation(Boolean bool, Boolean bool2, int i) {
        boolean booleanValue = bool.booleanValue();
        boolean booleanValue2 = bool2.booleanValue();
        switch (i) {
            case ParserConstants.EQ:
                return booleanValue == booleanValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.LE:
            case ParserConstants.LEX:
            case ParserConstants.GE:
            case ParserConstants.GEX:
            default:
                throw new InterpreterError("unimplemented binary operator");
            case ParserConstants.NE:
                return booleanValue != booleanValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.BOOL_OR:
            case ParserConstants.BOOL_ORX:
                return (booleanValue || booleanValue2) ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.BOOL_AND:
            case ParserConstants.BOOL_ANDX:
                return (!booleanValue || !booleanValue2) ? Boolean.FALSE : Boolean.TRUE;
        }
    }

    static boolean booleanUnaryOperation(Boolean bool, int i) {
        boolean booleanValue = bool.booleanValue();
        switch (i) {
            case ParserConstants.BANG:
                return !booleanValue;
            default:
                throw new UtilEvalError("Operator inappropriate for boolean");
        }
    }

    public static Class boxType(Class cls) {
        Class cls2 = (Class) wrapperMap.get(cls);
        if (cls2 != null) {
            return cls2;
        }
        throw new InterpreterError("Not a primitive type: " + cls);
    }

    static Primitive castPrimitive(Class cls, Class cls2, Primitive primitive, boolean z, int i) {
        if (z && primitive != null) {
            throw new InterpreterError("bad cast param 1");
        } else if (!z && primitive == null) {
            throw new InterpreterError("bad cast param 2");
        } else if (cls2 != null && !cls2.isPrimitive()) {
            throw new InterpreterError("bad fromType:" + cls2);
        } else if (primitive == NULL && cls2 != null) {
            throw new InterpreterError("inconsistent args 1");
        } else if (primitive == VOID && cls2 != Void.TYPE) {
            throw new InterpreterError("inconsistent args 2");
        } else if (cls2 != Void.TYPE) {
            Object obj = null;
            if (primitive != null) {
                obj = primitive.getValue();
            }
            if (cls.isPrimitive()) {
                if (cls2 == null) {
                    if (z) {
                        return Types.INVALID_CAST;
                    }
                    throw Types.castError("primitive type:" + cls, "Null value", i);
                } else if (cls2 == Boolean.TYPE) {
                    if (cls == Boolean.TYPE) {
                        return z ? Types.VALID_CAST : primitive;
                    }
                    if (z) {
                        return Types.INVALID_CAST;
                    }
                    throw Types.castError(cls, cls2, i);
                } else if (i != 1 || Types.isJavaAssignable(cls, cls2)) {
                    return z ? Types.VALID_CAST : new Primitive(castWrapper(cls, obj));
                } else {
                    if (z) {
                        return Types.INVALID_CAST;
                    }
                    throw Types.castError(cls, cls2, i);
                }
            } else if (cls2 == null) {
                return z ? Types.VALID_CAST : NULL;
            } else {
                if (z) {
                    return Types.INVALID_CAST;
                }
                throw Types.castError("object type:" + cls, "primitive value", i);
            }
        } else if (z) {
            return Types.INVALID_CAST;
        } else {
            throw Types.castError(Reflect.normalizeClassName(cls), "void value", i);
        }
    }

    static Object castWrapper(Class cls, Object obj) {
        if (!cls.isPrimitive()) {
            throw new InterpreterError("invalid type in castWrapper: " + cls);
        } else if (obj == null) {
            throw new InterpreterError("null value in castWrapper, guard");
        } else if (!(obj instanceof Boolean)) {
            Object num = obj instanceof Character ? new Integer(((Character) obj).charValue()) : obj;
            if (!(num instanceof Number)) {
                throw new InterpreterError("bad type in cast");
            }
            Number number = (Number) num;
            if (cls == Byte.TYPE) {
                return new Byte(number.byteValue());
            }
            if (cls == Short.TYPE) {
                return new Short(number.shortValue());
            }
            if (cls == Character.TYPE) {
                return new Character((char) number.intValue());
            }
            if (cls == Integer.TYPE) {
                return new Integer(number.intValue());
            }
            if (cls == Long.TYPE) {
                return new Long(number.longValue());
            }
            if (cls == Float.TYPE) {
                return new Float(number.floatValue());
            }
            if (cls == Double.TYPE) {
                return new Double(number.doubleValue());
            }
            throw new InterpreterError("error in wrapper cast");
        } else if (cls == Boolean.TYPE) {
            return obj;
        } else {
            throw new InterpreterError("bad wrapper cast of boolean");
        }
    }

    static Object doubleBinaryOperation(Double d, Double d2, int i) {
        double doubleValue = d.doubleValue();
        double doubleValue2 = d2.doubleValue();
        switch (i) {
            case ParserConstants.GT:
            case ParserConstants.GTX:
                return doubleValue > doubleValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.LT:
            case ParserConstants.LTX:
                return doubleValue < doubleValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.BANG:
            case ParserConstants.TILDE:
            case ParserConstants.HOOK:
            case ParserConstants.COLON:
            case ParserConstants.BOOL_OR:
            case ParserConstants.BOOL_ORX:
            case ParserConstants.BOOL_AND:
            case ParserConstants.BOOL_ANDX:
            case ParserConstants.INCR:
            case ParserConstants.DECR:
            case ParserConstants.BIT_AND:
            case ParserConstants.BIT_ANDX:
            case ParserConstants.BIT_OR:
            case ParserConstants.BIT_ORX:
            case ParserConstants.XOR:
            default:
                throw new InterpreterError("Unimplemented binary double operator");
            case ParserConstants.EQ:
                return doubleValue == doubleValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.LE:
            case ParserConstants.LEX:
                return doubleValue <= doubleValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.GE:
            case ParserConstants.GEX:
                return doubleValue >= doubleValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.NE:
                return doubleValue != doubleValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.PLUS:
                return new Double(doubleValue + doubleValue2);
            case ParserConstants.MINUS:
                return new Double(doubleValue - doubleValue2);
            case ParserConstants.STAR:
                return new Double(doubleValue * doubleValue2);
            case ParserConstants.SLASH:
                return new Double(doubleValue / doubleValue2);
            case ParserConstants.MOD:
                return new Double(doubleValue % doubleValue2);
            case ParserConstants.LSHIFT:
            case ParserConstants.LSHIFTX:
            case ParserConstants.RSIGNEDSHIFT:
            case ParserConstants.RSIGNEDSHIFTX:
            case ParserConstants.RUNSIGNEDSHIFT:
            case ParserConstants.RUNSIGNEDSHIFTX:
                throw new UtilEvalError("Can't shift doubles");
        }
    }

    static double doubleUnaryOperation(Double d, int i) {
        double doubleValue = d.doubleValue();
        switch (i) {
            case ParserConstants.PLUS:
                return doubleValue;
            case ParserConstants.MINUS:
                return -doubleValue;
            default:
                throw new InterpreterError("bad double unaryOperation");
        }
    }

    static Object floatBinaryOperation(Float f, Float f2, int i) {
        float floatValue = f.floatValue();
        float floatValue2 = f2.floatValue();
        switch (i) {
            case ParserConstants.GT:
            case ParserConstants.GTX:
                return floatValue > floatValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.LT:
            case ParserConstants.LTX:
                return floatValue < floatValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.BANG:
            case ParserConstants.TILDE:
            case ParserConstants.HOOK:
            case ParserConstants.COLON:
            case ParserConstants.BOOL_OR:
            case ParserConstants.BOOL_ORX:
            case ParserConstants.BOOL_AND:
            case ParserConstants.BOOL_ANDX:
            case ParserConstants.INCR:
            case ParserConstants.DECR:
            case ParserConstants.BIT_AND:
            case ParserConstants.BIT_ANDX:
            case ParserConstants.BIT_OR:
            case ParserConstants.BIT_ORX:
            case ParserConstants.XOR:
            default:
                throw new InterpreterError("Unimplemented binary float operator");
            case ParserConstants.EQ:
                return floatValue == floatValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.LE:
            case ParserConstants.LEX:
                return floatValue <= floatValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.GE:
            case ParserConstants.GEX:
                return floatValue >= floatValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.NE:
                return floatValue != floatValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.PLUS:
                return new Float(floatValue + floatValue2);
            case ParserConstants.MINUS:
                return new Float(floatValue - floatValue2);
            case ParserConstants.STAR:
                return new Float(floatValue * floatValue2);
            case ParserConstants.SLASH:
                return new Float(floatValue / floatValue2);
            case ParserConstants.MOD:
                return new Float(floatValue % floatValue2);
            case ParserConstants.LSHIFT:
            case ParserConstants.LSHIFTX:
            case ParserConstants.RSIGNEDSHIFT:
            case ParserConstants.RSIGNEDSHIFTX:
            case ParserConstants.RUNSIGNEDSHIFT:
            case ParserConstants.RUNSIGNEDSHIFTX:
                throw new UtilEvalError("Can't shift floats ");
        }
    }

    static float floatUnaryOperation(Float f, int i) {
        float floatValue = f.floatValue();
        switch (i) {
            case ParserConstants.PLUS:
                return floatValue;
            case ParserConstants.MINUS:
                return -floatValue;
            default:
                throw new InterpreterError("bad float unaryOperation");
        }
    }

    public static Primitive getDefaultValue(Class cls) {
        if (cls == null || !cls.isPrimitive()) {
            return NULL;
        }
        if (cls == Boolean.TYPE) {
            return FALSE;
        }
        try {
            return new Primitive(0).castToType(cls, 0);
        } catch (UtilEvalError e) {
            throw new InterpreterError("bad cast");
        }
    }

    static Object intBinaryOperation(Integer num, Integer num2, int i) {
        int intValue = num.intValue();
        int intValue2 = num2.intValue();
        switch (i) {
            case ParserConstants.GT:
            case ParserConstants.GTX:
                return intValue > intValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.LT:
            case ParserConstants.LTX:
                return intValue < intValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.BANG:
            case ParserConstants.TILDE:
            case ParserConstants.HOOK:
            case ParserConstants.COLON:
            case ParserConstants.BOOL_OR:
            case ParserConstants.BOOL_ORX:
            case ParserConstants.BOOL_AND:
            case ParserConstants.BOOL_ANDX:
            case ParserConstants.INCR:
            case ParserConstants.DECR:
            default:
                throw new InterpreterError("Unimplemented binary integer operator");
            case ParserConstants.EQ:
                return intValue == intValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.LE:
            case ParserConstants.LEX:
                return intValue <= intValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.GE:
            case ParserConstants.GEX:
                return intValue >= intValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.NE:
                return intValue != intValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.PLUS:
                return new Integer(intValue + intValue2);
            case ParserConstants.MINUS:
                return new Integer(intValue - intValue2);
            case ParserConstants.STAR:
                return new Integer(intValue * intValue2);
            case ParserConstants.SLASH:
                return new Integer(intValue / intValue2);
            case ParserConstants.BIT_AND:
            case ParserConstants.BIT_ANDX:
                return new Integer(intValue & intValue2);
            case ParserConstants.BIT_OR:
            case ParserConstants.BIT_ORX:
                return new Integer(intValue | intValue2);
            case ParserConstants.XOR:
                return new Integer(intValue ^ intValue2);
            case ParserConstants.MOD:
                return new Integer(intValue % intValue2);
            case ParserConstants.LSHIFT:
            case ParserConstants.LSHIFTX:
                return new Integer(intValue << intValue2);
            case ParserConstants.RSIGNEDSHIFT:
            case ParserConstants.RSIGNEDSHIFTX:
                return new Integer(intValue >> intValue2);
            case ParserConstants.RUNSIGNEDSHIFT:
            case ParserConstants.RUNSIGNEDSHIFTX:
                return new Integer(intValue >>> intValue2);
        }
    }

    static int intUnaryOperation(Integer num, int i) {
        int intValue = num.intValue();
        switch (i) {
            case ParserConstants.TILDE:
                return intValue ^ -1;
            case ParserConstants.INCR:
                return intValue + 1;
            case ParserConstants.DECR:
                return intValue - 1;
            case ParserConstants.PLUS:
                return intValue;
            case ParserConstants.MINUS:
                return -intValue;
            default:
                throw new InterpreterError("bad integer unaryOperation");
        }
    }

    public static boolean isWrapperType(Class cls) {
        return wrapperMap.get(cls) != null && !cls.isPrimitive();
    }

    static Object longBinaryOperation(Long l, Long l2, int i) {
        long longValue = l.longValue();
        long longValue2 = l2.longValue();
        switch (i) {
            case ParserConstants.GT:
            case ParserConstants.GTX:
                return longValue > longValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.LT:
            case ParserConstants.LTX:
                return longValue < longValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.BANG:
            case ParserConstants.TILDE:
            case ParserConstants.HOOK:
            case ParserConstants.COLON:
            case ParserConstants.BOOL_OR:
            case ParserConstants.BOOL_ORX:
            case ParserConstants.BOOL_AND:
            case ParserConstants.BOOL_ANDX:
            case ParserConstants.INCR:
            case ParserConstants.DECR:
            default:
                throw new InterpreterError("Unimplemented binary long operator");
            case ParserConstants.EQ:
                return longValue == longValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.LE:
            case ParserConstants.LEX:
                return longValue <= longValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.GE:
            case ParserConstants.GEX:
                return longValue >= longValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.NE:
                return longValue != longValue2 ? Boolean.TRUE : Boolean.FALSE;
            case ParserConstants.PLUS:
                return new Long(longValue + longValue2);
            case ParserConstants.MINUS:
                return new Long(longValue - longValue2);
            case ParserConstants.STAR:
                return new Long(longValue * longValue2);
            case ParserConstants.SLASH:
                return new Long(longValue / longValue2);
            case ParserConstants.BIT_AND:
            case ParserConstants.BIT_ANDX:
                return new Long(longValue & longValue2);
            case ParserConstants.BIT_OR:
            case ParserConstants.BIT_ORX:
                return new Long(longValue | longValue2);
            case ParserConstants.XOR:
                return new Long(longValue ^ longValue2);
            case ParserConstants.MOD:
                return new Long(longValue % longValue2);
            case ParserConstants.LSHIFT:
            case ParserConstants.LSHIFTX:
                return new Long(longValue << ((int) longValue2));
            case ParserConstants.RSIGNEDSHIFT:
            case ParserConstants.RSIGNEDSHIFTX:
                return new Long(longValue >> ((int) longValue2));
            case ParserConstants.RUNSIGNEDSHIFT:
            case ParserConstants.RUNSIGNEDSHIFTX:
                return new Long(longValue >>> ((int) longValue2));
        }
    }

    static long longUnaryOperation(Long l, int i) {
        long longValue = l.longValue();
        switch (i) {
            case ParserConstants.TILDE:
                return longValue ^ -1;
            case ParserConstants.INCR:
                return longValue + 1;
            case ParserConstants.DECR:
                return longValue - 1;
            case ParserConstants.PLUS:
                return longValue;
            case ParserConstants.MINUS:
                return -longValue;
            default:
                throw new InterpreterError("bad long unaryOperation");
        }
    }

    static Object[] promotePrimitives(Object obj, Object obj2) {
        Object promoteToInteger = promoteToInteger(obj);
        Object promoteToInteger2 = promoteToInteger(obj2);
        if ((promoteToInteger instanceof Number) && (promoteToInteger2 instanceof Number)) {
            Number number = (Number) promoteToInteger;
            Number number2 = (Number) promoteToInteger2;
            boolean z = number instanceof Double;
            if (!z && !(number2 instanceof Double)) {
                boolean z2 = number instanceof Float;
                if (!z2 && !(number2 instanceof Float)) {
                    boolean z3 = number instanceof Long;
                    if (z3 || (number2 instanceof Long)) {
                        if (z3) {
                            promoteToInteger2 = new Long(number2.longValue());
                        } else {
                            promoteToInteger = new Long(number.longValue());
                        }
                    }
                } else if (z2) {
                    promoteToInteger2 = new Float(number2.floatValue());
                } else {
                    promoteToInteger = new Float(number.floatValue());
                }
            } else if (z) {
                promoteToInteger2 = new Double(number2.doubleValue());
            } else {
                promoteToInteger = new Double(number.doubleValue());
            }
        }
        return new Object[]{promoteToInteger, promoteToInteger2};
    }

    static Object promoteToInteger(Object obj) {
        return obj instanceof Character ? new Integer(((Character) obj).charValue()) : ((obj instanceof Byte) || (obj instanceof Short)) ? new Integer(((Number) obj).intValue()) : obj;
    }

    public static Primitive unaryOperation(Primitive primitive, int i) {
        if (primitive == NULL) {
            throw new UtilEvalError("illegal use of null object or 'null' literal");
        } else if (primitive == VOID) {
            throw new UtilEvalError("illegal use of undefined object or 'void' literal");
        } else {
            Class type = primitive.getType();
            Object promoteToInteger = promoteToInteger(primitive.getValue());
            if (promoteToInteger instanceof Boolean) {
                return booleanUnaryOperation((Boolean) promoteToInteger, i) ? TRUE : FALSE;
            }
            if (promoteToInteger instanceof Integer) {
                int intUnaryOperation = intUnaryOperation((Integer) promoteToInteger, i);
                if (i == 100 || i == 101) {
                    if (type == Byte.TYPE) {
                        return new Primitive((byte) intUnaryOperation);
                    }
                    if (type == Short.TYPE) {
                        return new Primitive((short) intUnaryOperation);
                    }
                    if (type == Character.TYPE) {
                        return new Primitive((char) intUnaryOperation);
                    }
                }
                return new Primitive(intUnaryOperation);
            } else if (promoteToInteger instanceof Long) {
                return new Primitive(longUnaryOperation((Long) promoteToInteger, i));
            } else {
                if (promoteToInteger instanceof Float) {
                    return new Primitive(floatUnaryOperation((Float) promoteToInteger, i));
                }
                if (promoteToInteger instanceof Double) {
                    return new Primitive(doubleUnaryOperation((Double) promoteToInteger, i));
                }
                throw new InterpreterError("An error occurred.  Please call technical support.");
            }
        }
    }

    public static Class unboxType(Class cls) {
        Class cls2 = (Class) wrapperMap.get(cls);
        if (cls2 != null) {
            return cls2;
        }
        throw new InterpreterError("Not a primitive wrapper type: " + cls);
    }

    public static Object unwrap(Object obj) {
        if (obj == VOID) {
            return null;
        }
        return obj instanceof Primitive ? ((Primitive) obj).getValue() : obj;
    }

    public static Object[] unwrap(Object[] objArr) {
        Object[] objArr2 = new Object[objArr.length];
        for (int i = 0; i < objArr.length; i++) {
            objArr2[i] = unwrap(objArr[i]);
        }
        return objArr2;
    }

    public static Object wrap(Object obj, Class cls) {
        return cls == Void.TYPE ? VOID : obj == null ? NULL : obj instanceof Boolean ? ((Boolean) obj).booleanValue() ? TRUE : FALSE : (!cls.isPrimitive() || !isWrapperType(obj.getClass())) ? obj : new Primitive(obj);
    }

    public static Object[] wrap(Object[] objArr, Class[] clsArr) {
        if (objArr == null) {
            return null;
        }
        Object[] objArr2 = new Object[objArr.length];
        for (int i = 0; i < objArr.length; i++) {
            objArr2[i] = wrap(objArr[i], clsArr[i]);
        }
        return objArr2;
    }

    public final boolean booleanValue() {
        if (this.value instanceof Boolean) {
            return ((Boolean) this.value).booleanValue();
        }
        throw new UtilEvalError("Primitive not a boolean");
    }

    public final Primitive castToType(Class cls, int i) {
        return castPrimitive(cls, getType(), this, false, i);
    }

    public final boolean equals(Object obj) {
        if (obj instanceof Primitive) {
            return ((Primitive) obj).value.equals(this.value);
        }
        return false;
    }

    public final Class getType() {
        if (this == VOID) {
            return Void.TYPE;
        }
        if (this == NULL) {
            return null;
        }
        return unboxType(this.value.getClass());
    }

    public final Object getValue() {
        if (this.value == Special.NULL_VALUE) {
            return null;
        }
        if (this.value != Special.VOID_TYPE) {
            return this.value;
        }
        throw new InterpreterError("attempt to unwrap void type");
    }

    public final int hashCode() {
        return this.value.hashCode() * 21;
    }

    public final int intValue() {
        if (this.value instanceof Number) {
            return ((Number) this.value).intValue();
        }
        throw new UtilEvalError("Primitive not a number");
    }

    public final boolean isNumber() {
        return ((this.value instanceof Boolean) || this == NULL || this == VOID) ? false : true;
    }

    public final Number numberValue() {
        Object obj = this.value;
        if (obj instanceof Character) {
            obj = new Integer(((Character) obj).charValue());
        }
        if (obj instanceof Number) {
            return (Number) obj;
        }
        throw new UtilEvalError("Primitive not a number");
    }

    public final String toString() {
        return this.value == Special.NULL_VALUE ? "null" : this.value == Special.VOID_TYPE ? "void" : this.value.toString();
    }
}
