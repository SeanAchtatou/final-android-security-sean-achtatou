package bsh;

class BSHForStatement extends SimpleNode implements ParserConstants {
    private SimpleNode expression;
    private SimpleNode forInit;
    private SimpleNode forUpdate;
    public boolean hasExpression;
    public boolean hasForInit;
    public boolean hasForUpdate;
    private boolean parsed;
    private SimpleNode statement;

    BSHForStatement(int i) {
        super(i);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0087 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object eval(bsh.CallStack r7, bsh.Interpreter r8) {
        /*
            r6 = this;
            r2 = 1
            r3 = 0
            boolean r0 = r6.hasForInit
            if (r0 == 0) goto L_0x008b
            bsh.Node r0 = r6.jjtGetChild(r3)
            bsh.SimpleNode r0 = (bsh.SimpleNode) r0
            r6.forInit = r0
            r0 = r2
        L_0x000f:
            boolean r1 = r6.hasExpression
            if (r1 == 0) goto L_0x001e
            int r1 = r0 + 1
            bsh.Node r0 = r6.jjtGetChild(r0)
            bsh.SimpleNode r0 = (bsh.SimpleNode) r0
            r6.expression = r0
            r0 = r1
        L_0x001e:
            boolean r1 = r6.hasForUpdate
            if (r1 == 0) goto L_0x002d
            int r1 = r0 + 1
            bsh.Node r0 = r6.jjtGetChild(r0)
            bsh.SimpleNode r0 = (bsh.SimpleNode) r0
            r6.forUpdate = r0
            r0 = r1
        L_0x002d:
            int r1 = r6.jjtGetNumChildren()
            if (r0 >= r1) goto L_0x003b
            bsh.Node r0 = r6.jjtGetChild(r0)
            bsh.SimpleNode r0 = (bsh.SimpleNode) r0
            r6.statement = r0
        L_0x003b:
            bsh.NameSpace r5 = r7.top()
            bsh.BlockNameSpace r0 = new bsh.BlockNameSpace
            r0.<init>(r5)
            r7.swap(r0)
            boolean r0 = r6.hasForInit
            if (r0 == 0) goto L_0x0050
            bsh.SimpleNode r0 = r6.forInit
            r0.eval(r7, r8)
        L_0x0050:
            bsh.Primitive r4 = bsh.Primitive.VOID
        L_0x0052:
            boolean r0 = r6.hasExpression
            if (r0 == 0) goto L_0x005e
            bsh.SimpleNode r0 = r6.expression
            boolean r0 = bsh.BSHIfStatement.evaluateCondition(r0, r7, r8)
            if (r0 == 0) goto L_0x0087
        L_0x005e:
            bsh.SimpleNode r0 = r6.statement
            if (r0 == 0) goto L_0x0074
            bsh.SimpleNode r0 = r6.statement
            java.lang.Object r1 = r0.eval(r7, r8)
            boolean r0 = r1 instanceof bsh.ReturnControl
            if (r0 == 0) goto L_0x0074
            r0 = r1
            bsh.ReturnControl r0 = (bsh.ReturnControl) r0
            int r0 = r0.kind
            switch(r0) {
                case 12: goto L_0x0083;
                case 19: goto L_0x0081;
                case 46: goto L_0x0084;
                default: goto L_0x0074;
            }
        L_0x0074:
            r0 = r3
        L_0x0075:
            if (r0 != 0) goto L_0x0087
            boolean r0 = r6.hasForUpdate
            if (r0 == 0) goto L_0x0052
            bsh.SimpleNode r0 = r6.forUpdate
            r0.eval(r7, r8)
            goto L_0x0052
        L_0x0081:
            r0 = r3
            goto L_0x0075
        L_0x0083:
            r1 = r4
        L_0x0084:
            r0 = r2
            r4 = r1
            goto L_0x0075
        L_0x0087:
            r7.swap(r5)
            return r4
        L_0x008b:
            r0 = r3
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.BSHForStatement.eval(bsh.CallStack, bsh.Interpreter):java.lang.Object");
    }
}
