package bsh;

class BSHFormalParameters extends SimpleNode {
    int numArgs;
    private String[] paramNames;
    Class[] paramTypes;
    String[] typeDescriptors;

    BSHFormalParameters(int i) {
        super(i);
    }

    public Object eval(CallStack callStack, Interpreter interpreter) {
        if (this.paramTypes != null) {
            return this.paramTypes;
        }
        insureParsed();
        Class[] clsArr = new Class[this.numArgs];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.numArgs) {
                clsArr[i2] = (Class) ((BSHFormalParameter) jjtGetChild(i2)).eval(callStack, interpreter);
                i = i2 + 1;
            } else {
                this.paramTypes = clsArr;
                return clsArr;
            }
        }
    }

    public String[] getParamNames() {
        insureParsed();
        return this.paramNames;
    }

    public String[] getTypeDescriptors(CallStack callStack, Interpreter interpreter, String str) {
        if (this.typeDescriptors != null) {
            return this.typeDescriptors;
        }
        insureParsed();
        String[] strArr = new String[this.numArgs];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.numArgs) {
                strArr[i2] = ((BSHFormalParameter) jjtGetChild(i2)).getTypeDescriptor(callStack, interpreter, str);
                i = i2 + 1;
            } else {
                this.typeDescriptors = strArr;
                return strArr;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void insureParsed() {
        if (this.paramNames == null) {
            this.numArgs = jjtGetNumChildren();
            String[] strArr = new String[this.numArgs];
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.numArgs) {
                    strArr[i2] = ((BSHFormalParameter) jjtGetChild(i2)).name;
                    i = i2 + 1;
                } else {
                    this.paramNames = strArr;
                    return;
                }
            }
        }
    }
}
