package bsh;

class SimpleNode implements Node {
    public static SimpleNode JAVACODE = new SimpleNode(-1) {
        public final int getLineNumber() {
            return -1;
        }

        public final String getSourceFile() {
            return "<Called from Java Code>";
        }

        public final String getText() {
            return "<Compiled Java Code>";
        }
    };
    protected Node[] children;
    Token firstToken;
    protected int id;
    Token lastToken;
    protected Node parent;
    String sourceFile;

    public SimpleNode(int i) {
        this.id = i;
    }

    public void dump(String str) {
        System.out.println(toString(str));
        if (this.children != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.children.length) {
                    SimpleNode simpleNode = (SimpleNode) this.children[i2];
                    if (simpleNode != null) {
                        simpleNode.dump(str + " ");
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public Object eval(CallStack callStack, Interpreter interpreter) {
        throw new InterpreterError("Unimplemented or inappropriate for " + getClass().getName());
    }

    public SimpleNode getChild(int i) {
        return (SimpleNode) jjtGetChild(i);
    }

    public int getLineNumber() {
        return this.firstToken.beginLine;
    }

    public String getSourceFile() {
        return this.sourceFile == null ? this.parent != null ? ((SimpleNode) this.parent).getSourceFile() : "<unknown file>" : this.sourceFile;
    }

    public String getText() {
        StringBuffer stringBuffer = new StringBuffer();
        for (Token token = this.firstToken; token != null; token = token.next) {
            stringBuffer.append(token.image);
            if (!token.image.equals(".")) {
                stringBuffer.append(" ");
            }
            if (token == this.lastToken || token.image.equals("{") || token.image.equals(";")) {
                break;
            }
        }
        return stringBuffer.toString();
    }

    public void jjtAddChild(Node node, int i) {
        if (this.children == null) {
            this.children = new Node[(i + 1)];
        } else if (i >= this.children.length) {
            Node[] nodeArr = new Node[(i + 1)];
            System.arraycopy(this.children, 0, nodeArr, 0, this.children.length);
            this.children = nodeArr;
        }
        this.children[i] = node;
    }

    public void jjtClose() {
    }

    public Node jjtGetChild(int i) {
        return this.children[i];
    }

    public int jjtGetNumChildren() {
        if (this.children == null) {
            return 0;
        }
        return this.children.length;
    }

    public Node jjtGetParent() {
        return this.parent;
    }

    public void jjtOpen() {
    }

    public void jjtSetParent(Node node) {
        this.parent = node;
    }

    public void prune() {
        jjtSetParent(null);
    }

    public void setSourceFile(String str) {
        this.sourceFile = str;
    }

    public String toString() {
        return ParserTreeConstants.jjtNodeName[this.id];
    }

    public String toString(String str) {
        return str + toString();
    }
}
