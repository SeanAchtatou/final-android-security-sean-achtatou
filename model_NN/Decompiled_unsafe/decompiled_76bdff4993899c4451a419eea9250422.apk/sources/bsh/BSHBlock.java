package bsh;

class BSHBlock extends SimpleNode {
    public boolean isStatic = false;
    public boolean isSynchronized = false;

    public interface NodeFilter {
        boolean isVisible(SimpleNode simpleNode);
    }

    BSHBlock(int i) {
        super(i);
    }

    public Object eval(CallStack callStack, Interpreter interpreter) {
        return eval(callStack, interpreter, false);
    }

    public Object eval(CallStack callStack, Interpreter interpreter, boolean z) {
        Object evalBlock;
        Object eval = this.isSynchronized ? ((SimpleNode) jjtGetChild(0)).eval(callStack, interpreter) : null;
        if (!this.isSynchronized) {
            return evalBlock(callStack, interpreter, z, null);
        }
        synchronized (eval) {
            evalBlock = evalBlock(callStack, interpreter, z, null);
        }
        return evalBlock;
    }

    /* access modifiers changed from: package-private */
    public Object evalBlock(CallStack callStack, Interpreter interpreter, boolean z, NodeFilter nodeFilter) {
        NameSpace nameSpace;
        Object obj;
        Object obj2 = Primitive.VOID;
        if (!z) {
            NameSpace pVar = callStack.top();
            callStack.swap(new BlockNameSpace(pVar));
            nameSpace = pVar;
        } else {
            nameSpace = null;
        }
        int i = this.isSynchronized ? 1 : 0;
        int jjtGetNumChildren = jjtGetNumChildren();
        int i2 = i;
        while (i2 < jjtGetNumChildren) {
            try {
                SimpleNode simpleNode = (SimpleNode) jjtGetChild(i2);
                if ((nodeFilter == null || nodeFilter.isVisible(simpleNode)) && (simpleNode instanceof BSHClassDeclaration)) {
                    simpleNode.eval(callStack, interpreter);
                }
                i2++;
            } catch (Throwable th) {
                if (!z) {
                    callStack.swap(nameSpace);
                }
                throw th;
            }
        }
        int i3 = i;
        Object obj3 = obj2;
        int i4 = i3;
        while (true) {
            if (i4 >= jjtGetNumChildren) {
                obj = obj3;
                break;
            }
            SimpleNode simpleNode2 = (SimpleNode) jjtGetChild(i4);
            if (!(simpleNode2 instanceof BSHClassDeclaration) && (nodeFilter == null || nodeFilter.isVisible(simpleNode2))) {
                obj = simpleNode2.eval(callStack, interpreter);
                if (obj instanceof ReturnControl) {
                    break;
                }
            } else {
                obj = obj3;
            }
            i4++;
            obj3 = obj;
        }
        if (!z) {
            callStack.swap(nameSpace);
        }
        return obj;
    }
}
