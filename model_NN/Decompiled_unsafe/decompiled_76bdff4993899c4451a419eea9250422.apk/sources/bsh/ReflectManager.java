package bsh;

import bsh.Capabilities;

public abstract class ReflectManager {
    private static ReflectManager rfm;

    public static boolean RMSetAccessible(Object obj) {
        return getReflectManager().setAccessible(obj);
    }

    public static ReflectManager getReflectManager() {
        if (rfm == null) {
            try {
                rfm = (ReflectManager) Class.forName("bsh.reflect.ReflectManagerImpl").newInstance();
            } catch (Exception e) {
                throw new Capabilities.Unavailable("Reflect Manager unavailable: " + e);
            }
        }
        return rfm;
    }

    public abstract boolean setAccessible(Object obj);
}
