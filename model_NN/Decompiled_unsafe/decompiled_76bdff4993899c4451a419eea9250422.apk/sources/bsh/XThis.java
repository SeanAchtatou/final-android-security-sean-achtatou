package bsh;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Hashtable;

public class XThis extends This {
    Hashtable interfaces;
    InvocationHandler invocationHandler = new Handler();

    class Handler implements Serializable, InvocationHandler {
        Handler() {
        }

        public Object invoke(Object obj, Method method, Object[] objArr) {
            try {
                return invokeImpl(obj, method, objArr);
            } catch (TargetError e) {
                throw e.getTarget();
            } catch (EvalError e2) {
                if (Interpreter.DEBUG) {
                    Interpreter.debug("EvalError in scripted interface: " + XThis.this.toString() + ": " + e2);
                }
                throw e2;
            }
        }

        public Object invokeImpl(Object obj, Method method, Object[] objArr) {
            BshMethod bshMethod;
            BshMethod bshMethod2 = null;
            String name = method.getName();
            new CallStack(XThis.this.namespace);
            try {
                bshMethod = XThis.this.namespace.getMethod("equals", new Class[]{Object.class});
            } catch (UtilEvalError e) {
                bshMethod = null;
            }
            if (name.equals("equals") && bshMethod == null) {
                return obj == objArr[0] ? Boolean.TRUE : Boolean.FALSE;
            }
            try {
                bshMethod2 = XThis.this.namespace.getMethod("toString", new Class[0]);
            } catch (UtilEvalError e2) {
            }
            if (!name.equals("toString") || bshMethod2 != null) {
                return Primitive.unwrap(XThis.this.invokeMethod(name, Primitive.wrap(objArr, method.getParameterTypes())));
            }
            Class<?>[] interfaces = obj.getClass().getInterfaces();
            StringBuffer stringBuffer = new StringBuffer(XThis.this.toString() + "\nimplements:");
            for (int i = 0; i < interfaces.length; i++) {
                stringBuffer.append(" " + interfaces[i].getName() + (interfaces.length > 1 ? "," : ""));
            }
            return stringBuffer.toString();
        }
    }

    public XThis(NameSpace nameSpace, Interpreter interpreter) {
        super(nameSpace, interpreter);
    }

    public Object getInterface(Class cls) {
        return getInterface(new Class[]{cls});
    }

    public Object getInterface(Class[] clsArr) {
        if (this.interfaces == null) {
            this.interfaces = new Hashtable();
        }
        int i = 21;
        for (Class hashCode : clsArr) {
            i *= hashCode.hashCode() + 3;
        }
        Integer num = new Integer(i);
        Object obj = this.interfaces.get(num);
        if (obj != null) {
            return obj;
        }
        Object newProxyInstance = Proxy.newProxyInstance(clsArr[0].getClassLoader(), clsArr, this.invocationHandler);
        this.interfaces.put(num, newProxyInstance);
        return newProxyInstance;
    }

    public String toString() {
        return "'this' reference (XThis) to Bsh object: " + this.namespace;
    }
}
