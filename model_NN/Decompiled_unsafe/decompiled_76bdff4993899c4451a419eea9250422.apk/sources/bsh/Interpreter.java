package bsh;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Interpreter implements ConsoleInterface, Serializable, Runnable {
    public static boolean DEBUG = false;
    public static boolean LOCALSCOPING = false;
    public static boolean TRACE = false;
    public static final String VERSION = "2.0b5";
    static transient PrintStream debug;
    static This sharedObject;
    static String systemLineSeparator = "\n";
    ConsoleInterface console;
    transient PrintStream err;
    protected boolean evalOnly;
    private boolean exitOnEOF;
    NameSpace globalNameSpace;
    transient Reader in;
    protected boolean interactive;
    transient PrintStream out;
    Interpreter parent;
    transient Parser parser;
    private boolean showResults;
    String sourceFileInfo;
    private boolean strictJava;

    static {
        staticInit();
    }

    public Interpreter() {
        this(new StringReader(""), System.out, System.err, false, null);
        this.evalOnly = true;
        setu("bsh.evalOnly", Primitive.TRUE);
    }

    public Interpreter(ConsoleInterface consoleInterface) {
        this(consoleInterface, null);
    }

    public Interpreter(ConsoleInterface consoleInterface, NameSpace nameSpace) {
        this(consoleInterface.getIn(), consoleInterface.getOut(), consoleInterface.getErr(), true, nameSpace);
        setConsole(consoleInterface);
    }

    public Interpreter(Reader reader, PrintStream printStream, PrintStream printStream2, boolean z) {
        this(reader, printStream, printStream2, z, null);
    }

    public Interpreter(Reader reader, PrintStream printStream, PrintStream printStream2, boolean z, NameSpace nameSpace) {
        this(reader, printStream, printStream2, z, nameSpace, null, null);
    }

    public Interpreter(Reader reader, PrintStream printStream, PrintStream printStream2, boolean z, NameSpace nameSpace, Interpreter interpreter, String str) {
        this.strictJava = false;
        this.exitOnEOF = true;
        this.parser = new Parser(reader);
        long currentTimeMillis = System.currentTimeMillis();
        this.in = reader;
        this.out = printStream;
        this.err = printStream2;
        this.interactive = z;
        debug = printStream2;
        this.parent = interpreter;
        if (interpreter != null) {
            setStrictJava(interpreter.getStrictJava());
        }
        this.sourceFileInfo = str;
        BshClassManager createClassManager = BshClassManager.createClassManager(this);
        if (nameSpace == null) {
            this.globalNameSpace = new NameSpace(createClassManager, "global");
        } else {
            this.globalNameSpace = nameSpace;
        }
        if (!(getu("bsh") instanceof This)) {
            initRootSystemObject();
        }
        if (z) {
            loadRCFiles();
        }
        long currentTimeMillis2 = System.currentTimeMillis();
        if (DEBUG) {
            debug("Time to initialize interpreter: " + (currentTimeMillis2 - currentTimeMillis));
        }
    }

    private boolean Line() {
        return this.parser.Line();
    }

    public static final void debug(String str) {
        if (DEBUG) {
            debug.println("// Debug: " + str);
        }
    }

    private String getBshPrompt() {
        try {
            return (String) eval("getBshPrompt()");
        } catch (Exception e) {
            return "bsh % ";
        }
    }

    public static boolean getSaveClasses() {
        return getSaveClassesDir() != null;
    }

    public static String getSaveClassesDir() {
        return System.getProperty("saveClasses");
    }

    private JavaCharStream get_jj_input_stream() {
        return this.parser.jj_input_stream;
    }

    private JJTParserState get_jjtree() {
        return this.parser.jjtree;
    }

    private void initRootSystemObject() {
        BshClassManager classManager = getClassManager();
        setu("bsh", new NameSpace(classManager, "Bsh Object").getThis(this));
        if (sharedObject == null) {
            sharedObject = new NameSpace(classManager, "Bsh Shared System Object").getThis(this);
        }
        setu("bsh.system", sharedObject);
        setu("bsh.shared", sharedObject);
        setu("bsh.help", new NameSpace(classManager, "Bsh Command Help Text").getThis(this));
        try {
            setu("bsh.cwd", System.getProperty("user.dir"));
        } catch (SecurityException e) {
            setu("bsh.cwd", ".");
        }
        setu("bsh.interactive", this.interactive ? Primitive.TRUE : Primitive.FALSE);
        setu("bsh.evalOnly", this.evalOnly ? Primitive.TRUE : Primitive.FALSE);
    }

    public static void invokeMain(Class cls, String[] strArr) {
        Method resolveJavaMethod = Reflect.resolveJavaMethod(null, cls, "main", new Class[]{String[].class}, true);
        if (resolveJavaMethod != null) {
            resolveJavaMethod.invoke(null, strArr);
        }
    }

    public static void main(String[] strArr) {
        String[] strArr2;
        if (strArr.length > 0) {
            String str = strArr[0];
            if (strArr.length > 1) {
                String[] strArr3 = new String[(strArr.length - 1)];
                System.arraycopy(strArr, 1, strArr3, 0, strArr.length - 1);
                strArr2 = strArr3;
            } else {
                strArr2 = new String[0];
            }
            Interpreter interpreter = new Interpreter();
            interpreter.setu("bsh.args", strArr2);
            try {
                Object source = interpreter.source(str, interpreter.globalNameSpace);
                if (source instanceof Class) {
                    try {
                        invokeMain((Class) source, strArr2);
                    } catch (Exception e) {
                        e = e;
                        if (e instanceof InvocationTargetException) {
                            e = ((InvocationTargetException) e).getTargetException();
                        }
                        System.err.println("Class: " + source + " main method threw exception:" + e);
                    }
                }
            } catch (FileNotFoundException e2) {
                System.out.println("File not found: " + e2);
            } catch (TargetError e3) {
                System.out.println("Script threw exception: " + e3);
                if (e3.inNativeCode()) {
                    e3.printStackTrace(DEBUG, System.err);
                }
            } catch (EvalError e4) {
                System.out.println("Evaluation Error: " + e4);
            } catch (IOException e5) {
                System.out.println("I/O Error: " + e5);
            }
        } else {
            new Interpreter(new CommandLineReader(new InputStreamReader((!System.getProperty("os.name").startsWith("Windows") || !System.getProperty("java.version").startsWith("1.1.")) ? System.in : new FilterInputStream(System.in) {
                public final int available() {
                    return 0;
                }
            })), System.out, System.err, true).run();
        }
    }

    private void readObject(ObjectInputStream objectInputStream) {
        objectInputStream.defaultReadObject();
        if (this.console != null) {
            setOut(this.console.getOut());
            setErr(this.console.getErr());
            return;
        }
        setOut(System.out);
        setErr(System.err);
    }

    public static void redirectOutputToFile(String str) {
        try {
            PrintStream printStream = new PrintStream(new FileOutputStream(str));
            System.setOut(printStream);
            System.setErr(printStream);
        } catch (IOException e) {
            System.err.println("Can't redirect output to file: " + str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private String showEvalString(String str) {
        String replace = str.replace(10, ' ').replace(13, ' ');
        return replace.length() > 80 ? replace.substring(0, 80) + " . . . " : replace;
    }

    static void staticInit() {
        try {
            systemLineSeparator = System.getProperty("line.separator");
            debug = System.err;
            DEBUG = Boolean.getBoolean("debug");
            TRACE = Boolean.getBoolean("trace");
            LOCALSCOPING = Boolean.getBoolean("localscoping");
            String property = System.getProperty("outfile");
            if (property != null) {
                redirectOutputToFile(property);
            }
        } catch (SecurityException e) {
            System.err.println("Could not init static:" + e);
        } catch (Exception e2) {
            System.err.println("Could not init static(2):" + e2);
        } catch (Throwable th) {
            System.err.println("Could not init static(3):" + th);
        }
    }

    public final void error(Object obj) {
        if (this.console != null) {
            this.console.error("// Error: " + obj + "\n");
            return;
        }
        this.err.println("// Error: " + obj);
        this.err.flush();
    }

    public Object eval(Reader reader) {
        return eval(reader, this.globalNameSpace, "eval stream");
    }

    /* JADX WARN: Type inference failed for: r2v35, types: [bsh.Node] */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x023a, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x023c, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x023d, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x023f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0240, code lost:
        r4 = r2;
        r2 = r5;
        r5 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x024b, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x024c, code lost:
        r4 = r2;
        r2 = r5;
        r5 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0257, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0258, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b6, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00b9, code lost:
        if (bsh.Interpreter.DEBUG != false) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00bb, code lost:
        error(r2.getMessage(bsh.Interpreter.DEBUG));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c4, code lost:
        r2.setErrorSourceFile(r15);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00c7, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0138, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0162, code lost:
        throw new bsh.EvalError("Sourced file: " + r15 + " internal Error: " + r2.getMessage(), r4, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0163, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0164, code lost:
        r11 = r2;
        r2 = r5;
        r5 = r4;
        r4 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x016e, code lost:
        r4.setNode(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0194, code lost:
        r6.clear();
        r6.push(r14);
        r5 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x019d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x019e, code lost:
        r11 = r2;
        r2 = r5;
        r5 = r4;
        r4 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01a6, code lost:
        r4.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01af, code lost:
        r4.setNode(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x01d5, code lost:
        r6.clear();
        r6.push(r14);
        r5 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01de, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01e1, code lost:
        if (bsh.Interpreter.DEBUG != false) goto L_0x01e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01e3, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x020c, code lost:
        throw new bsh.EvalError("Sourced file: " + r15 + " unknown error: " + r2.getMessage(), r4, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x020d, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0234, code lost:
        throw new bsh.EvalError("Sourced file: " + r15 + " Token Parsing Error: " + r2.getMessage(), r4, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0239, code lost:
        r2 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x023c A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:5:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0257 A[ExcHandler: InterpreterError (e bsh.InterpreterError), Splitter:B:5:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b6 A[ExcHandler: ParseException (r2v24 'e' bsh.ParseException A[CUSTOM_DECLARE]), Splitter:B:5:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x016e A[Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0257, TargetError -> 0x0251, EvalError -> 0x0245, Exception -> 0x023c, TokenMgrError -> 0x0239, all -> 0x00c8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0194  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01a6 A[Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0257, TargetError -> 0x0251, EvalError -> 0x0245, Exception -> 0x023c, TokenMgrError -> 0x0239, all -> 0x00c8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01af A[Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0257, TargetError -> 0x0251, EvalError -> 0x0245, Exception -> 0x023c, TokenMgrError -> 0x0239, all -> 0x00c8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01d5  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01e3 A[Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0257, TargetError -> 0x0251, EvalError -> 0x0245, Exception -> 0x023c, TokenMgrError -> 0x0239, all -> 0x00c8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0239 A[ExcHandler: TokenMgrError (e bsh.TokenMgrError), Splitter:B:5:0x0032] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object eval(java.io.Reader r13, bsh.NameSpace r14, java.lang.String r15) {
        /*
            r12 = this;
            r9 = 0
            r5 = 0
            r10 = 1
            boolean r1 = bsh.Interpreter.DEBUG
            if (r1 == 0) goto L_0x001d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "eval: nameSpace = "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r14)
            java.lang.String r1 = r1.toString()
            debug(r1)
        L_0x001d:
            bsh.Interpreter r1 = new bsh.Interpreter
            java.io.PrintStream r3 = r12.out
            java.io.PrintStream r4 = r12.err
            r2 = r13
            r6 = r14
            r7 = r12
            r8 = r15
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
            bsh.CallStack r6 = new bsh.CallStack
            r6.<init>(r14)
            r3 = r9
        L_0x0030:
            if (r5 != 0) goto L_0x00fa
            boolean r5 = r1.Line()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0257, TargetError -> 0x024b, EvalError -> 0x023f, Exception -> 0x023c, TokenMgrError -> 0x0239 }
            bsh.JJTParserState r2 = r1.get_jjtree()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0257, TargetError -> 0x0251, EvalError -> 0x0245, Exception -> 0x023c, TokenMgrError -> 0x0239 }
            int r2 = r2.nodeArity()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0257, TargetError -> 0x0251, EvalError -> 0x0245, Exception -> 0x023c, TokenMgrError -> 0x0239 }
            if (r2 <= 0) goto L_0x0123
            bsh.JJTParserState r2 = r1.get_jjtree()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0257, TargetError -> 0x0251, EvalError -> 0x0245, Exception -> 0x023c, TokenMgrError -> 0x0239 }
            bsh.Node r2 = r2.rootNode()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0257, TargetError -> 0x0251, EvalError -> 0x0245, Exception -> 0x023c, TokenMgrError -> 0x0239 }
            r0 = r2
            bsh.SimpleNode r0 = (bsh.SimpleNode) r0     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0257, TargetError -> 0x0251, EvalError -> 0x0245, Exception -> 0x023c, TokenMgrError -> 0x0239 }
            r4 = r0
            boolean r2 = getSaveClasses()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            if (r2 == 0) goto L_0x0072
            boolean r2 = r4 instanceof bsh.BSHClassDeclaration     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            if (r2 != 0) goto L_0x0072
            boolean r2 = r4 instanceof bsh.BSHImportDeclaration     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            if (r2 != 0) goto L_0x0072
            boolean r2 = r4 instanceof bsh.BSHPackageDeclaration     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            if (r2 != 0) goto L_0x0072
            bsh.JJTParserState r2 = r1.get_jjtree()
            r2.reset()
            int r2 = r6.depth()
            if (r2 <= r10) goto L_0x0030
            r6.clear()
            r6.push(r14)
            goto L_0x0030
        L_0x0072:
            r4.setSourceFile(r15)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            boolean r2 = bsh.Interpreter.TRACE     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            if (r2 == 0) goto L_0x0093
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            r2.<init>()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.String r7 = "// "
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.String r7 = r4.getText()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.String r2 = r2.toString()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            r12.println(r2)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
        L_0x0093:
            java.lang.Object r3 = r4.eval(r6, r1)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            int r2 = r6.depth()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            if (r2 <= r10) goto L_0x00dd
            bsh.InterpreterError r2 = new bsh.InterpreterError     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            r7.<init>()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.String r8 = "Callstack growing: "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.StringBuilder r7 = r7.append(r6)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.String r7 = r7.toString()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            r2.<init>(r7)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            throw r2     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
        L_0x00b6:
            r2 = move-exception
            boolean r3 = bsh.Interpreter.DEBUG     // Catch:{ all -> 0x00c8 }
            if (r3 == 0) goto L_0x00c4
            boolean r3 = bsh.Interpreter.DEBUG     // Catch:{ all -> 0x00c8 }
            java.lang.String r3 = r2.getMessage(r3)     // Catch:{ all -> 0x00c8 }
            r12.error(r3)     // Catch:{ all -> 0x00c8 }
        L_0x00c4:
            r2.setErrorSourceFile(r15)     // Catch:{ all -> 0x00c8 }
            throw r2     // Catch:{ all -> 0x00c8 }
        L_0x00c8:
            r2 = move-exception
            bsh.JJTParserState r1 = r1.get_jjtree()
            r1.reset()
            int r1 = r6.depth()
            if (r1 <= r10) goto L_0x00dc
            r6.clear()
            r6.push(r14)
        L_0x00dc:
            throw r2
        L_0x00dd:
            boolean r2 = r3 instanceof bsh.ReturnControl     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            if (r2 == 0) goto L_0x00ff
            r0 = r3
            bsh.ReturnControl r0 = (bsh.ReturnControl) r0     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            r2 = r0
            java.lang.Object r3 = r2.value     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            bsh.JJTParserState r1 = r1.get_jjtree()
            r1.reset()
            int r1 = r6.depth()
            if (r1 <= r10) goto L_0x00fa
            r6.clear()
            r6.push(r14)
        L_0x00fa:
            java.lang.Object r1 = bsh.Primitive.unwrap(r3)
            return r1
        L_0x00ff:
            boolean r2 = r1.showResults     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            if (r2 == 0) goto L_0x0123
            bsh.Primitive r2 = bsh.Primitive.VOID     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            if (r3 == r2) goto L_0x0123
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            r2.<init>()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.String r7 = "<"
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.String r7 = ">"
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            java.lang.String r2 = r2.toString()     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
            r12.println(r2)     // Catch:{ ParseException -> 0x00b6, InterpreterError -> 0x0138, TargetError -> 0x0163, EvalError -> 0x019d, Exception -> 0x01de, TokenMgrError -> 0x020d }
        L_0x0123:
            bsh.JJTParserState r2 = r1.get_jjtree()
            r2.reset()
            int r2 = r6.depth()
            if (r2 <= r10) goto L_0x0235
            r6.clear()
            r6.push(r14)
            goto L_0x0030
        L_0x0138:
            r2 = move-exception
        L_0x0139:
            r2.printStackTrace()     // Catch:{ all -> 0x00c8 }
            bsh.EvalError r3 = new bsh.EvalError     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r5.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r7 = "Sourced file: "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r5 = r5.append(r15)     // Catch:{ all -> 0x00c8 }
            java.lang.String r7 = " internal Error: "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ all -> 0x00c8 }
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ all -> 0x00c8 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00c8 }
            r3.<init>(r2, r4, r6)     // Catch:{ all -> 0x00c8 }
            throw r3     // Catch:{ all -> 0x00c8 }
        L_0x0163:
            r2 = move-exception
            r11 = r2
            r2 = r5
            r5 = r4
            r4 = r11
        L_0x0168:
            bsh.SimpleNode r7 = r4.getNode()     // Catch:{ all -> 0x00c8 }
            if (r7 != 0) goto L_0x0171
            r4.setNode(r5)     // Catch:{ all -> 0x00c8 }
        L_0x0171:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r5.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r7 = "Sourced file: "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r5 = r5.append(r15)     // Catch:{ all -> 0x00c8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00c8 }
            r4.reThrow(r5)     // Catch:{ all -> 0x00c8 }
            bsh.JJTParserState r4 = r1.get_jjtree()
            r4.reset()
            int r4 = r6.depth()
            if (r4 <= r10) goto L_0x0236
            r6.clear()
            r6.push(r14)
            r5 = r2
            goto L_0x0030
        L_0x019d:
            r2 = move-exception
            r11 = r2
            r2 = r5
            r5 = r4
            r4 = r11
        L_0x01a2:
            boolean r7 = bsh.Interpreter.DEBUG     // Catch:{ all -> 0x00c8 }
            if (r7 == 0) goto L_0x01a9
            r4.printStackTrace()     // Catch:{ all -> 0x00c8 }
        L_0x01a9:
            bsh.SimpleNode r7 = r4.getNode()     // Catch:{ all -> 0x00c8 }
            if (r7 != 0) goto L_0x01b2
            r4.setNode(r5)     // Catch:{ all -> 0x00c8 }
        L_0x01b2:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r5.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r7 = "Sourced file: "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r5 = r5.append(r15)     // Catch:{ all -> 0x00c8 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00c8 }
            r4.reThrow(r5)     // Catch:{ all -> 0x00c8 }
            bsh.JJTParserState r4 = r1.get_jjtree()
            r4.reset()
            int r4 = r6.depth()
            if (r4 <= r10) goto L_0x0236
            r6.clear()
            r6.push(r14)
            r5 = r2
            goto L_0x0030
        L_0x01de:
            r2 = move-exception
        L_0x01df:
            boolean r3 = bsh.Interpreter.DEBUG     // Catch:{ all -> 0x00c8 }
            if (r3 == 0) goto L_0x01e6
            r2.printStackTrace()     // Catch:{ all -> 0x00c8 }
        L_0x01e6:
            bsh.EvalError r3 = new bsh.EvalError     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r5.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r7 = "Sourced file: "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r5 = r5.append(r15)     // Catch:{ all -> 0x00c8 }
            java.lang.String r7 = " unknown error: "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ all -> 0x00c8 }
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ all -> 0x00c8 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00c8 }
            r3.<init>(r2, r4, r6)     // Catch:{ all -> 0x00c8 }
            throw r3     // Catch:{ all -> 0x00c8 }
        L_0x020d:
            r2 = move-exception
        L_0x020e:
            bsh.EvalError r3 = new bsh.EvalError     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r5.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r7 = "Sourced file: "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r5 = r5.append(r15)     // Catch:{ all -> 0x00c8 }
            java.lang.String r7 = " Token Parsing Error: "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ all -> 0x00c8 }
            java.lang.String r2 = r2.getMessage()     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ all -> 0x00c8 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00c8 }
            r3.<init>(r2, r4, r6)     // Catch:{ all -> 0x00c8 }
            throw r3     // Catch:{ all -> 0x00c8 }
        L_0x0235:
            r2 = r5
        L_0x0236:
            r5 = r2
            goto L_0x0030
        L_0x0239:
            r2 = move-exception
            r4 = r9
            goto L_0x020e
        L_0x023c:
            r2 = move-exception
            r4 = r9
            goto L_0x01df
        L_0x023f:
            r2 = move-exception
            r4 = r2
            r2 = r5
            r5 = r9
            goto L_0x01a2
        L_0x0245:
            r2 = move-exception
            r4 = r2
            r2 = r5
            r5 = r9
            goto L_0x01a2
        L_0x024b:
            r2 = move-exception
            r4 = r2
            r2 = r5
            r5 = r9
            goto L_0x0168
        L_0x0251:
            r2 = move-exception
            r4 = r2
            r2 = r5
            r5 = r9
            goto L_0x0168
        L_0x0257:
            r2 = move-exception
            r4 = r9
            goto L_0x0139
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Interpreter.eval(java.io.Reader, bsh.NameSpace, java.lang.String):java.lang.Object");
    }

    public Object eval(String str) {
        if (DEBUG) {
            debug("eval(String): " + str);
        }
        return eval(str, this.globalNameSpace);
    }

    public Object eval(String str, NameSpace nameSpace) {
        if (!str.endsWith(";")) {
            str = str + ";";
        }
        return eval(new StringReader(str), nameSpace, "inline evaluation of: ``" + showEvalString(str) + "''");
    }

    public Object get(String str) {
        try {
            return Primitive.unwrap(this.globalNameSpace.get(str, this));
        } catch (UtilEvalError e) {
            throw e.toEvalError(SimpleNode.JAVACODE, new CallStack());
        }
    }

    public BshClassManager getClassManager() {
        return getNameSpace().getClassManager();
    }

    public PrintStream getErr() {
        return this.err;
    }

    public Reader getIn() {
        return this.in;
    }

    public Object getInterface(Class cls) {
        try {
            return this.globalNameSpace.getThis(this).getInterface(cls);
        } catch (UtilEvalError e) {
            throw e.toEvalError(SimpleNode.JAVACODE, new CallStack());
        }
    }

    public NameSpace getNameSpace() {
        return this.globalNameSpace;
    }

    public PrintStream getOut() {
        return this.out;
    }

    public Interpreter getParent() {
        return this.parent;
    }

    public boolean getShowResults() {
        return this.showResults;
    }

    public String getSourceFileInfo() {
        return this.sourceFileInfo != null ? this.sourceFileInfo : "<unknown source>";
    }

    public boolean getStrictJava() {
        return this.strictJava;
    }

    /* access modifiers changed from: package-private */
    public Object getu(String str) {
        try {
            return get(str);
        } catch (EvalError e) {
            throw new InterpreterError("set: " + e);
        }
    }

    /* access modifiers changed from: package-private */
    public void loadRCFiles() {
        try {
            source(System.getProperty("user.home") + File.separator + ".bshrc", this.globalNameSpace);
        } catch (Exception e) {
            if (DEBUG) {
                debug("Could not find rc file: " + e);
            }
        }
    }

    public File pathToFile(String str) {
        File file = new File(str);
        if (!file.isAbsolute()) {
            file = new File(((String) getu("bsh.cwd")) + File.separator + str);
        }
        return new File(file.getCanonicalPath());
    }

    public final void print(Object obj) {
        if (this.console != null) {
            this.console.print(obj);
            return;
        }
        this.out.print(obj);
        this.out.flush();
    }

    public final void println(Object obj) {
        print(String.valueOf(obj) + systemLineSeparator);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0236, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x023d, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x024b, code lost:
        r4.clear();
        r4.push(r8.globalNameSpace);
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0256, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0257, code lost:
        r7 = r0;
        r0 = r2;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x027b, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0289, code lost:
        r4.clear();
        r4.push(r8.globalNameSpace);
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x02c1, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        error("Parser Error: " + r0.getMessage(bsh.Interpreter.DEBUG));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x009c, code lost:
        if (bsh.Interpreter.DEBUG != false) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009e, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a3, code lost:
        if (r8.interactive == false) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00a5, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a6, code lost:
        r8.parser.reInitInput(r8.in);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ad, code lost:
        get_jjtree().reset();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b8, code lost:
        if (r4.depth() > 1) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ba, code lost:
        r4.clear();
        r4.push(r8.globalNameSpace);
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0115, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0116, code lost:
        r7 = r0;
        r0 = r2;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x013a, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0148, code lost:
        r4.clear();
        r4.push(r8.globalNameSpace);
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0153, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        r7 = r0;
        r0 = r2;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0173, code lost:
        r2.printStackTrace(bsh.Interpreter.DEBUG, r8.err);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x017e, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0195, code lost:
        r4.clear();
        r4.push(r8.globalNameSpace);
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01a0, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01a1, code lost:
        r7 = r0;
        r0 = r2;
        r2 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01a8, code lost:
        error("EvalError: " + r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01c6, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01cd, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01db, code lost:
        r4.clear();
        r4.push(r8.globalNameSpace);
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        error("EvalError: " + r2.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0218, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0219, code lost:
        r7 = r0;
        r0 = r2;
        r2 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0236 A[Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4, all -> 0x0201 }] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x023d  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x024b  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x027b  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0289  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007d A[ExcHandler: ParseException (r0v32 'e' bsh.ParseException A[CUSTOM_DECLARE]), Splitter:B:11:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0173 A[Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4, all -> 0x0201 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x017e A[Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4, all -> 0x0201 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0195  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x01a8 A[Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4, all -> 0x0201 }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01c6 A[Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4, all -> 0x0201 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01cd  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01e6 A[SYNTHETIC, Splitter:B:89:0x01e6] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r8 = this;
            r3 = 0
            r1 = 1
            boolean r0 = r8.evalOnly
            if (r0 == 0) goto L_0x000e
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "bsh Interpreter: No stream"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            boolean r0 = r8.interactive
            if (r0 == 0) goto L_0x0017
            java.lang.String r0 = "printBanner();"
            r8.eval(r0)     // Catch:{ EvalError -> 0x00c5 }
        L_0x0017:
            bsh.CallStack r4 = new bsh.CallStack
            bsh.NameSpace r0 = r8.globalNameSpace
            r4.<init>(r0)
            r2 = r3
        L_0x001f:
            if (r2 != 0) goto L_0x0298
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x0115, TargetError -> 0x0153, EvalError -> 0x01a0, Exception -> 0x0218, TokenMgrError -> 0x0256 }
            r0.flush()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x0115, TargetError -> 0x0153, EvalError -> 0x01a0, Exception -> 0x0218, TokenMgrError -> 0x0256 }
            java.io.PrintStream r0 = java.lang.System.err     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x0115, TargetError -> 0x0153, EvalError -> 0x01a0, Exception -> 0x0218, TokenMgrError -> 0x0256 }
            r0.flush()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x0115, TargetError -> 0x0153, EvalError -> 0x01a0, Exception -> 0x0218, TokenMgrError -> 0x0256 }
            java.lang.Thread.yield()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x0115, TargetError -> 0x0153, EvalError -> 0x01a0, Exception -> 0x0218, TokenMgrError -> 0x0256 }
            boolean r0 = r8.interactive     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x0115, TargetError -> 0x0153, EvalError -> 0x01a0, Exception -> 0x0218, TokenMgrError -> 0x0256 }
            if (r0 == 0) goto L_0x0039
            java.lang.String r0 = r8.getBshPrompt()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x0115, TargetError -> 0x0153, EvalError -> 0x01a0, Exception -> 0x0218, TokenMgrError -> 0x0256 }
            r8.print(r0)     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x0115, TargetError -> 0x0153, EvalError -> 0x01a0, Exception -> 0x0218, TokenMgrError -> 0x0256 }
        L_0x0039:
            boolean r2 = r8.Line()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x0115, TargetError -> 0x0153, EvalError -> 0x01a0, Exception -> 0x0218, TokenMgrError -> 0x0256 }
            bsh.JJTParserState r0 = r8.get_jjtree()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            int r0 = r0.nodeArity()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            if (r0 <= 0) goto L_0x00fe
            bsh.JJTParserState r0 = r8.get_jjtree()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            bsh.Node r0 = r0.rootNode()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            bsh.SimpleNode r0 = (bsh.SimpleNode) r0     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            boolean r5 = bsh.Interpreter.DEBUG     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            if (r5 == 0) goto L_0x005a
            java.lang.String r5 = ">"
            r0.dump(r5)     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
        L_0x005a:
            java.lang.Object r0 = r0.eval(r4, r8)     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            int r5 = r4.depth()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            if (r5 <= r1) goto L_0x00cd
            bsh.InterpreterError r0 = new bsh.InterpreterError     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            r5.<init>()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            java.lang.String r6 = "Callstack growing: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            java.lang.StringBuilder r5 = r5.append(r4)     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            java.lang.String r5 = r5.toString()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            r0.<init>(r5)     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            throw r0     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
        L_0x007d:
            r0 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0201 }
            r5.<init>()     // Catch:{ all -> 0x0201 }
            java.lang.String r6 = "Parser Error: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0201 }
            boolean r6 = bsh.Interpreter.DEBUG     // Catch:{ all -> 0x0201 }
            java.lang.String r6 = r0.getMessage(r6)     // Catch:{ all -> 0x0201 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0201 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0201 }
            r8.error(r5)     // Catch:{ all -> 0x0201 }
            boolean r5 = bsh.Interpreter.DEBUG     // Catch:{ all -> 0x0201 }
            if (r5 == 0) goto L_0x00a1
            r0.printStackTrace()     // Catch:{ all -> 0x0201 }
        L_0x00a1:
            boolean r0 = r8.interactive     // Catch:{ all -> 0x0201 }
            if (r0 != 0) goto L_0x02c1
            r0 = r1
        L_0x00a6:
            bsh.Parser r2 = r8.parser     // Catch:{ all -> 0x0201 }
            java.io.Reader r5 = r8.in     // Catch:{ all -> 0x0201 }
            r2.reInitInput(r5)     // Catch:{ all -> 0x0201 }
            bsh.JJTParserState r2 = r8.get_jjtree()
            r2.reset()
            int r2 = r4.depth()
            if (r2 <= r1) goto L_0x0295
            r4.clear()
            bsh.NameSpace r2 = r8.globalNameSpace
            r4.push(r2)
            r2 = r0
            goto L_0x001f
        L_0x00c5:
            r0 = move-exception
            java.lang.String r0 = "BeanShell 2.0b5 - by Pat Niemeyer (pat@pat.net)"
            r8.println(r0)
            goto L_0x0017
        L_0x00cd:
            boolean r5 = r0 instanceof bsh.ReturnControl     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            if (r5 == 0) goto L_0x00d5
            bsh.ReturnControl r0 = (bsh.ReturnControl) r0     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            java.lang.Object r0 = r0.value     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
        L_0x00d5:
            bsh.Primitive r5 = bsh.Primitive.VOID     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            if (r0 == r5) goto L_0x00fe
            java.lang.String r5 = "$_"
            r8.setu(r5, r0)     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            boolean r5 = r8.showResults     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            if (r5 == 0) goto L_0x00fe
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            r5.<init>()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            java.lang.String r6 = "<"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            java.lang.String r5 = ">"
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            java.lang.String r0 = r0.toString()     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
            r8.println(r0)     // Catch:{ ParseException -> 0x007d, InterpreterError -> 0x02bb, TargetError -> 0x02b5, EvalError -> 0x02af, Exception -> 0x02a9, TokenMgrError -> 0x02a4 }
        L_0x00fe:
            bsh.JJTParserState r0 = r8.get_jjtree()
            r0.reset()
            int r0 = r4.depth()
            if (r0 <= r1) goto L_0x0294
            r4.clear()
            bsh.NameSpace r0 = r8.globalNameSpace
            r4.push(r0)
            goto L_0x001f
        L_0x0115:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r7
        L_0x0119:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0201 }
            r5.<init>()     // Catch:{ all -> 0x0201 }
            java.lang.String r6 = "Internal Error: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0201 }
            java.lang.String r6 = r2.getMessage()     // Catch:{ all -> 0x0201 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0201 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0201 }
            r8.error(r5)     // Catch:{ all -> 0x0201 }
            r2.printStackTrace()     // Catch:{ all -> 0x0201 }
            boolean r2 = r8.interactive     // Catch:{ all -> 0x0201 }
            if (r2 != 0) goto L_0x013b
            r0 = r1
        L_0x013b:
            bsh.JJTParserState r2 = r8.get_jjtree()
            r2.reset()
            int r2 = r4.depth()
            if (r2 <= r1) goto L_0x0295
            r4.clear()
            bsh.NameSpace r2 = r8.globalNameSpace
            r4.push(r2)
            r2 = r0
            goto L_0x001f
        L_0x0153:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r7
        L_0x0157:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0201 }
            r5.<init>()     // Catch:{ all -> 0x0201 }
            java.lang.String r6 = "// Uncaught Exception: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0201 }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ all -> 0x0201 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0201 }
            r8.error(r5)     // Catch:{ all -> 0x0201 }
            boolean r5 = r2.inNativeCode()     // Catch:{ all -> 0x0201 }
            if (r5 == 0) goto L_0x017a
            boolean r5 = bsh.Interpreter.DEBUG     // Catch:{ all -> 0x0201 }
            java.io.PrintStream r6 = r8.err     // Catch:{ all -> 0x0201 }
            r2.printStackTrace(r5, r6)     // Catch:{ all -> 0x0201 }
        L_0x017a:
            boolean r5 = r8.interactive     // Catch:{ all -> 0x0201 }
            if (r5 != 0) goto L_0x017f
            r0 = r1
        L_0x017f:
            java.lang.String r5 = "$_e"
            java.lang.Throwable r2 = r2.getTarget()     // Catch:{ all -> 0x0201 }
            r8.setu(r5, r2)     // Catch:{ all -> 0x0201 }
            bsh.JJTParserState r2 = r8.get_jjtree()
            r2.reset()
            int r2 = r4.depth()
            if (r2 <= r1) goto L_0x0295
            r4.clear()
            bsh.NameSpace r2 = r8.globalNameSpace
            r4.push(r2)
            r2 = r0
            goto L_0x001f
        L_0x01a0:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r7
        L_0x01a4:
            boolean r5 = r8.interactive     // Catch:{ all -> 0x0201 }
            if (r5 == 0) goto L_0x01e6
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0201 }
            r5.<init>()     // Catch:{ all -> 0x0201 }
            java.lang.String r6 = "EvalError: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0201 }
            java.lang.String r6 = r2.toString()     // Catch:{ all -> 0x0201 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0201 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0201 }
            r8.error(r5)     // Catch:{ all -> 0x0201 }
        L_0x01c2:
            boolean r5 = bsh.Interpreter.DEBUG     // Catch:{ all -> 0x0201 }
            if (r5 == 0) goto L_0x01c9
            r2.printStackTrace()     // Catch:{ all -> 0x0201 }
        L_0x01c9:
            boolean r2 = r8.interactive     // Catch:{ all -> 0x0201 }
            if (r2 != 0) goto L_0x01ce
            r0 = r1
        L_0x01ce:
            bsh.JJTParserState r2 = r8.get_jjtree()
            r2.reset()
            int r2 = r4.depth()
            if (r2 <= r1) goto L_0x0295
            r4.clear()
            bsh.NameSpace r2 = r8.globalNameSpace
            r4.push(r2)
            r2 = r0
            goto L_0x001f
        L_0x01e6:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0201 }
            r5.<init>()     // Catch:{ all -> 0x0201 }
            java.lang.String r6 = "EvalError: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0201 }
            java.lang.String r6 = r2.getMessage()     // Catch:{ all -> 0x0201 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0201 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0201 }
            r8.error(r5)     // Catch:{ all -> 0x0201 }
            goto L_0x01c2
        L_0x0201:
            r0 = move-exception
            bsh.JJTParserState r2 = r8.get_jjtree()
            r2.reset()
            int r2 = r4.depth()
            if (r2 <= r1) goto L_0x0217
            r4.clear()
            bsh.NameSpace r1 = r8.globalNameSpace
            r4.push(r1)
        L_0x0217:
            throw r0
        L_0x0218:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r7
        L_0x021c:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0201 }
            r5.<init>()     // Catch:{ all -> 0x0201 }
            java.lang.String r6 = "Unknown error: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0201 }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ all -> 0x0201 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0201 }
            r8.error(r5)     // Catch:{ all -> 0x0201 }
            boolean r5 = bsh.Interpreter.DEBUG     // Catch:{ all -> 0x0201 }
            if (r5 == 0) goto L_0x0239
            r2.printStackTrace()     // Catch:{ all -> 0x0201 }
        L_0x0239:
            boolean r2 = r8.interactive     // Catch:{ all -> 0x0201 }
            if (r2 != 0) goto L_0x023e
            r0 = r1
        L_0x023e:
            bsh.JJTParserState r2 = r8.get_jjtree()
            r2.reset()
            int r2 = r4.depth()
            if (r2 <= r1) goto L_0x0295
            r4.clear()
            bsh.NameSpace r2 = r8.globalNameSpace
            r4.push(r2)
            r2 = r0
            goto L_0x001f
        L_0x0256:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r7
        L_0x025a:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0201 }
            r5.<init>()     // Catch:{ all -> 0x0201 }
            java.lang.String r6 = "Error parsing input: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0201 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ all -> 0x0201 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0201 }
            r8.error(r2)     // Catch:{ all -> 0x0201 }
            bsh.Parser r2 = r8.parser     // Catch:{ all -> 0x0201 }
            java.io.Reader r5 = r8.in     // Catch:{ all -> 0x0201 }
            r2.reInitTokenInput(r5)     // Catch:{ all -> 0x0201 }
            boolean r2 = r8.interactive     // Catch:{ all -> 0x0201 }
            if (r2 != 0) goto L_0x027c
            r0 = r1
        L_0x027c:
            bsh.JJTParserState r2 = r8.get_jjtree()
            r2.reset()
            int r2 = r4.depth()
            if (r2 <= r1) goto L_0x0295
            r4.clear()
            bsh.NameSpace r2 = r8.globalNameSpace
            r4.push(r2)
            r2 = r0
            goto L_0x001f
        L_0x0294:
            r0 = r2
        L_0x0295:
            r2 = r0
            goto L_0x001f
        L_0x0298:
            boolean r0 = r8.interactive
            if (r0 == 0) goto L_0x02a3
            boolean r0 = r8.exitOnEOF
            if (r0 == 0) goto L_0x02a3
            java.lang.System.exit(r3)
        L_0x02a3:
            return
        L_0x02a4:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r7
            goto L_0x025a
        L_0x02a9:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r7
            goto L_0x021c
        L_0x02af:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r7
            goto L_0x01a4
        L_0x02b5:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r7
            goto L_0x0157
        L_0x02bb:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r7
            goto L_0x0119
        L_0x02c1:
            r0 = r2
            goto L_0x00a6
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Interpreter.run():void");
    }

    public void set(String str, double d) {
        set(str, new Primitive(d));
    }

    public void set(String str, float f) {
        set(str, new Primitive(f));
    }

    public void set(String str, int i) {
        set(str, new Primitive(i));
    }

    public void set(String str, long j) {
        set(str, new Primitive(j));
    }

    public void set(String str, Object obj) {
        if (obj == null) {
            obj = Primitive.NULL;
        }
        CallStack callStack = new CallStack();
        try {
            if (Name.isCompound(str)) {
                this.globalNameSpace.getNameResolver(str).toLHS(callStack, this).assign(obj, false);
            } else {
                this.globalNameSpace.setVariable(str, obj, false);
            }
        } catch (UtilEvalError e) {
            throw e.toEvalError(SimpleNode.JAVACODE, callStack);
        }
    }

    public void set(String str, boolean z) {
        set(str, z ? Primitive.TRUE : Primitive.FALSE);
    }

    public void setClassLoader(ClassLoader classLoader) {
        getClassManager().setClassLoader(classLoader);
    }

    public void setConsole(ConsoleInterface consoleInterface) {
        this.console = consoleInterface;
        setu("bsh.console", consoleInterface);
        setOut(consoleInterface.getOut());
        setErr(consoleInterface.getErr());
    }

    public void setErr(PrintStream printStream) {
        this.err = printStream;
    }

    public void setExitOnEOF(boolean z) {
        this.exitOnEOF = z;
    }

    public void setNameSpace(NameSpace nameSpace) {
        this.globalNameSpace = nameSpace;
    }

    public void setOut(PrintStream printStream) {
        this.out = printStream;
    }

    public void setShowResults(boolean z) {
        this.showResults = z;
    }

    public void setStrictJava(boolean z) {
        this.strictJava = z;
    }

    /* access modifiers changed from: package-private */
    public void setu(String str, Object obj) {
        try {
            set(str, obj);
        } catch (EvalError e) {
            throw new InterpreterError("set: " + e);
        }
    }

    public Object source(String str) {
        return source(str, this.globalNameSpace);
    }

    public Object source(String str, NameSpace nameSpace) {
        File pathToFile = pathToFile(str);
        if (DEBUG) {
            debug("Sourcing file: " + pathToFile);
        }
        BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToFile));
        try {
            return eval(bufferedReader, nameSpace, str);
        } finally {
            bufferedReader.close();
        }
    }

    public void unset(String str) {
        try {
            LHS lhs = this.globalNameSpace.getNameResolver(str).toLHS(new CallStack(), this);
            if (lhs.type != 0) {
                throw new EvalError("Can't unset, not a variable: " + str, SimpleNode.JAVACODE, new CallStack());
            }
            lhs.nameSpace.unsetVariable(str);
        } catch (UtilEvalError e) {
            throw new EvalError(e.getMessage(), SimpleNode.JAVACODE, new CallStack());
        }
    }
}
