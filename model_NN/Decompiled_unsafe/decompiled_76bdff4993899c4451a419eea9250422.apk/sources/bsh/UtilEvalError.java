package bsh;

public class UtilEvalError extends Exception {
    protected UtilEvalError() {
    }

    public UtilEvalError(String str) {
        super(str);
    }

    public EvalError toEvalError(SimpleNode simpleNode, CallStack callStack) {
        return toEvalError(null, simpleNode, callStack);
    }

    public EvalError toEvalError(String str, SimpleNode simpleNode, CallStack callStack) {
        if (Interpreter.DEBUG) {
            printStackTrace();
        }
        return new EvalError((str == null ? "" : str + ": ") + getMessage(), simpleNode, callStack);
    }
}
