package bsh;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BshMethod implements Serializable {
    private Class[] cparamTypes;
    private Class creturnType;
    NameSpace declaringNameSpace;
    private Method javaMethod;
    private Object javaObject;
    BSHBlock methodBody;
    Modifiers modifiers;
    private String name;
    private int numArgs;
    private String[] paramNames;

    BshMethod(BSHMethodDeclaration bSHMethodDeclaration, NameSpace nameSpace, Modifiers modifiers2) {
        this(bSHMethodDeclaration.name, bSHMethodDeclaration.returnType, bSHMethodDeclaration.paramsNode.getParamNames(), bSHMethodDeclaration.paramsNode.paramTypes, bSHMethodDeclaration.blockNode, nameSpace, modifiers2);
    }

    BshMethod(String str, Class cls, String[] strArr, Class[] clsArr, BSHBlock bSHBlock, NameSpace nameSpace, Modifiers modifiers2) {
        this.name = str;
        this.creturnType = cls;
        this.paramNames = strArr;
        if (strArr != null) {
            this.numArgs = strArr.length;
        }
        this.cparamTypes = clsArr;
        this.methodBody = bSHBlock;
        this.declaringNameSpace = nameSpace;
        this.modifiers = modifiers2;
    }

    BshMethod(Method method, Object obj) {
        this(method.getName(), method.getReturnType(), null, method.getParameterTypes(), null, null, null);
        this.javaMethod = method;
        this.javaObject = obj;
    }

    private Object invokeImpl(Object[] objArr, Interpreter interpreter, CallStack callStack, SimpleNode simpleNode, boolean z) {
        NameSpace nameSpace;
        ReturnControl returnControl;
        Object obj;
        Class returnType = getReturnType();
        Class[] parameterTypes = getParameterTypes();
        if (callStack == null) {
            callStack = new CallStack(this.declaringNameSpace);
        }
        if (objArr == null) {
            objArr = new Object[0];
        }
        if (objArr.length != this.numArgs) {
            throw new EvalError("Wrong number of arguments for local method: " + this.name, simpleNode, callStack);
        }
        if (z) {
            nameSpace = callStack.top();
        } else {
            nameSpace = new NameSpace(this.declaringNameSpace, this.name);
            nameSpace.isMethod = true;
        }
        nameSpace.setNode(simpleNode);
        for (int i = 0; i < this.numArgs; i++) {
            if (parameterTypes[i] != null) {
                try {
                    objArr[i] = Types.castObject(objArr[i], parameterTypes[i], 1);
                    try {
                        nameSpace.setTypedVariable(this.paramNames[i], parameterTypes[i], objArr[i], (Modifiers) null);
                    } catch (UtilEvalError e) {
                        throw e.toEvalError("Typed method parameter assignment", simpleNode, callStack);
                    }
                } catch (UtilEvalError e2) {
                    throw new EvalError("Invalid argument: `" + this.paramNames[i] + "'" + " for method: " + this.name + " : " + e2.getMessage(), simpleNode, callStack);
                }
            } else if (objArr[i] == Primitive.VOID) {
                throw new EvalError("Undefined variable or class name, parameter: " + this.paramNames[i] + " to method: " + this.name, simpleNode, callStack);
            } else {
                try {
                    nameSpace.setLocalVariable(this.paramNames[i], objArr[i], interpreter.getStrictJava());
                } catch (UtilEvalError e3) {
                    throw e3.toEvalError(simpleNode, callStack);
                }
            }
        }
        if (!z) {
            callStack.push(nameSpace);
        }
        Object eval = this.methodBody.eval(callStack, interpreter, true);
        CallStack copy = callStack.copy();
        if (!z) {
            callStack.pop();
        }
        if (eval instanceof ReturnControl) {
            ReturnControl returnControl2 = (ReturnControl) eval;
            if (returnControl2.kind == 46) {
                Object obj2 = ((ReturnControl) eval).value;
                if (returnType != Void.TYPE || obj2 == Primitive.VOID) {
                    Object obj3 = obj2;
                    returnControl = returnControl2;
                    obj = obj3;
                } else {
                    throw new EvalError("Cannot return value from void method", returnControl2.returnPoint, copy);
                }
            } else {
                throw new EvalError("'continue' or 'break' in method body", returnControl2.returnPoint, copy);
            }
        } else {
            Object obj4 = eval;
            returnControl = null;
            obj = obj4;
        }
        if (returnType == null) {
            return obj;
        }
        if (returnType == Void.TYPE) {
            return Primitive.VOID;
        }
        try {
            return Types.castObject(obj, returnType, 1);
        } catch (UtilEvalError e4) {
            if (returnControl != null) {
                simpleNode = returnControl.returnPoint;
            }
            throw e4.toEvalError("Incorrect type returned from method: " + this.name + e4.getMessage(), simpleNode, callStack);
        }
    }

    public Modifiers getModifiers() {
        return this.modifiers;
    }

    public String getName() {
        return this.name;
    }

    public String[] getParameterNames() {
        return this.paramNames;
    }

    public Class[] getParameterTypes() {
        return this.cparamTypes;
    }

    public Class getReturnType() {
        return this.creturnType;
    }

    public boolean hasModifier(String str) {
        return this.modifiers != null && this.modifiers.hasModifier(str);
    }

    public Object invoke(Object[] objArr, Interpreter interpreter) {
        return invoke(objArr, interpreter, null, null, false);
    }

    public Object invoke(Object[] objArr, Interpreter interpreter, CallStack callStack, SimpleNode simpleNode) {
        return invoke(objArr, interpreter, callStack, simpleNode, false);
    }

    /* access modifiers changed from: package-private */
    public Object invoke(Object[] objArr, Interpreter interpreter, CallStack callStack, SimpleNode simpleNode, boolean z) {
        Object obj;
        Object invokeImpl;
        if (objArr != null) {
            for (Object obj2 : objArr) {
                if (obj2 == null) {
                    throw new Error("HERE!");
                }
            }
        }
        if (this.javaMethod != null) {
            try {
                return Reflect.invokeMethod(this.javaMethod, this.javaObject, objArr);
            } catch (ReflectError e) {
                throw new EvalError("Error invoking Java method: " + e, simpleNode, callStack);
            } catch (InvocationTargetException e2) {
                throw new TargetError("Exception invoking imported object method.", e2, simpleNode, callStack, true);
            }
        } else if (this.modifiers == null || !this.modifiers.hasModifier("synchronized")) {
            return invokeImpl(objArr, interpreter, callStack, simpleNode, z);
        } else {
            if (this.declaringNameSpace.isClass) {
                try {
                    obj = this.declaringNameSpace.getClassInstance();
                } catch (UtilEvalError e3) {
                    throw new InterpreterError("Can't get class instance for synchronized method.");
                }
            } else {
                obj = this.declaringNameSpace.getThis(interpreter);
            }
            synchronized (obj) {
                invokeImpl = invokeImpl(objArr, interpreter, callStack, simpleNode, z);
            }
            return invokeImpl;
        }
    }

    public String toString() {
        return "Scripted Method: " + StringUtil.methodString(this.name, getParameterTypes());
    }
}
