package bsh;

class BSHVariableDeclarator extends SimpleNode {
    public String name;

    BSHVariableDeclarator(int i) {
        super(i);
    }

    public Object eval(BSHType bSHType, CallStack callStack, Interpreter interpreter) {
        Object obj = null;
        if (jjtGetNumChildren() > 0) {
            SimpleNode simpleNode = (SimpleNode) jjtGetChild(0);
            obj = (bSHType == null || !(simpleNode instanceof BSHArrayInitializer)) ? simpleNode.eval(callStack, interpreter) : ((BSHArrayInitializer) simpleNode).eval(bSHType.getBaseType(), bSHType.getArrayDims(), callStack, interpreter);
        }
        if (obj != Primitive.VOID) {
            return obj;
        }
        throw new EvalError("Void initializer.", this, callStack);
    }

    public String toString() {
        return "BSHVariableDeclarator " + this.name;
    }
}
