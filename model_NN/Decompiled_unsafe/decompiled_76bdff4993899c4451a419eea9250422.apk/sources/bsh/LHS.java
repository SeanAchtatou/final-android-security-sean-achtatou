package bsh;

import java.io.Serializable;
import java.lang.reflect.Field;

class LHS implements ParserConstants, Serializable {
    static final int FIELD = 1;
    static final int INDEX = 3;
    static final int METHOD_EVAL = 4;
    static final int PROPERTY = 2;
    static final int VARIABLE = 0;
    Field field;
    int index;
    boolean localVar;
    NameSpace nameSpace;
    Object object;
    String propName;
    int type;
    String varName;

    LHS(NameSpace nameSpace2, String str, boolean z) {
        this.type = 0;
        this.localVar = z;
        this.varName = str;
        this.nameSpace = nameSpace2;
    }

    LHS(Object obj, int i) {
        if (obj == null) {
            throw new NullPointerException("constructed empty LHS");
        }
        this.type = 3;
        this.object = obj;
        this.index = i;
    }

    LHS(Object obj, String str) {
        if (obj == null) {
            throw new NullPointerException("constructed empty LHS");
        }
        this.type = 2;
        this.object = obj;
        this.propName = str;
    }

    LHS(Object obj, Field field2) {
        if (obj == null) {
            throw new NullPointerException("constructed empty LHS");
        }
        this.type = 1;
        this.object = obj;
        this.field = field2;
    }

    LHS(Field field2) {
        this.type = 1;
        this.object = null;
        this.field = field2;
    }

    public Object assign(Object obj, boolean z) {
        if (this.type == 0) {
            if (this.localVar) {
                this.nameSpace.setLocalVariableOrProperty(this.varName, obj, z);
            } else {
                this.nameSpace.setVariableOrProperty(this.varName, obj, z);
            }
        } else if (this.type == 1) {
            try {
                ReflectManager.RMSetAccessible(this.field);
                this.field.set(this.object, Primitive.unwrap(obj));
            } catch (NullPointerException e) {
                throw new UtilEvalError("LHS (" + this.field.getName() + ") not a static field.");
            } catch (IllegalAccessException e2) {
                throw new UtilEvalError("LHS (" + this.field.getName() + ") can't access field: " + e2);
            } catch (IllegalArgumentException e3) {
                String name = obj instanceof Primitive ? ((Primitive) obj).getType().getName() : obj.getClass().getName();
                StringBuilder append = new StringBuilder().append("Argument type mismatch. ");
                if (obj == null) {
                    name = "null";
                }
                throw new UtilEvalError(append.append(name).append(" not assignable to field ").append(this.field.getName()).toString());
            }
        } else if (this.type == 2) {
            CollectionManager collectionManager = CollectionManager.getCollectionManager();
            if (collectionManager.isMap(this.object)) {
                collectionManager.putInMap(this.object, this.propName, Primitive.unwrap(obj));
            } else {
                try {
                    Reflect.setObjectProperty(this.object, this.propName, obj);
                } catch (ReflectError e4) {
                    Interpreter.debug("Assignment: " + e4.getMessage());
                    throw new UtilEvalError("No such property: " + this.propName);
                }
            }
        } else if (this.type == 3) {
            try {
                Reflect.setIndex(this.object, this.index, obj);
            } catch (UtilTargetError e5) {
                throw e5;
            } catch (Exception e6) {
                throw new UtilEvalError("Assignment: " + e6.getMessage());
            }
        } else {
            throw new InterpreterError("unknown lhs");
        }
        return obj;
    }

    public Object getValue() {
        if (this.type == 0) {
            return this.nameSpace.getVariableOrProperty(this.varName, null);
        }
        if (this.type == 1) {
            try {
                return Primitive.wrap(this.field.get(this.object), this.field.getType());
            } catch (IllegalAccessException e) {
                throw new UtilEvalError("Can't read field: " + this.field);
            }
        } else if (this.type == 2) {
            CollectionManager collectionManager = CollectionManager.getCollectionManager();
            if (collectionManager.isMap(this.object)) {
                return collectionManager.getFromMap(this.object, this.propName);
            }
            try {
                return Reflect.getObjectProperty(this.object, this.propName);
            } catch (ReflectError e2) {
                Interpreter.debug(e2.getMessage());
                throw new UtilEvalError("No such property: " + this.propName);
            }
        } else if (this.type == 3) {
            try {
                return Reflect.getIndex(this.object, this.index);
            } catch (Exception e3) {
                throw new UtilEvalError("Array access: " + e3);
            }
        } else {
            throw new InterpreterError("LHS type");
        }
    }

    public String toString() {
        return "LHS: " + (this.field != null ? "field = " + this.field.toString() : "") + (this.varName != null ? " varName = " + this.varName : "") + (this.nameSpace != null ? " nameSpace = " + this.nameSpace.toString() : "");
    }
}
