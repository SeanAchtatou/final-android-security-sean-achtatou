package bsh;

import java.io.IOException;
import java.io.PrintStream;

public class ParserTokenManager implements ParserConstants {
    static final long[] jjbitVec0 = {0, 0, -1, -1};
    static final long[] jjbitVec1 = {-2, -1, -1, -1};
    static final long[] jjbitVec3 = {2301339413881290750L, -16384, 4294967295L, 432345564227567616L};
    static final long[] jjbitVec4 = {0, 0, 0, -36028797027352577L};
    static final long[] jjbitVec5 = {0, -1, -1, -1};
    static final long[] jjbitVec6 = {-1, -1, 65535, 0};
    static final long[] jjbitVec7 = {-1, -1, 0, 0};
    static final long[] jjbitVec8 = {70368744177663L, 0, 0, 0};
    static final int[] jjnextStates = {37, 38, 43, 44, 47, 48, 15, 56, 61, 73, 26, 27, 29, 17, 19, 52, 54, 9, 57, 58, 60, 2, 3, 5, 11, 12, 15, 26, 27, 31, 29, 39, 40, 15, 47, 48, 15, 63, 64, 66, 69, 70, 72, 13, 14, 20, 21, 23, 28, 30, 32, 41, 42, 45, 46, 49, 50};
    public static final String[] jjstrLiteralImages = {"", null, null, null, null, null, null, null, null, null, "abstract", "boolean", "break", "class", "byte", "case", "catch", "char", "const", "continue", "default", "do", "double", "else", "enum", "extends", "false", "final", "finally", "float", "for", "goto", "if", "implements", "import", "instanceof", "int", "interface", "long", "native", "new", "null", "package", "private", "protected", "public", "return", "short", "static", "strictfp", "switch", "synchronized", "transient", "throw", "throws", "true", "try", "void", "volatile", "while", null, null, null, null, null, null, null, null, null, null, null, null, "(", ")", "{", "}", "[", "]", ";", ",", ".", "=", ">", "@gt", "<", "@lt", "!", "~", "?", ":", "==", "<=", "@lteq", ">=", "@gteq", "!=", "||", "@or", "&&", "@and", "++", "--", "+", "-", "*", "/", "&", "@bitwise_and", "|", "@bitwise_or", "^", "%", "<<", "@left_shift", ">>", "@right_shift", ">>>", "@right_unsigned_shift", "+=", "-=", "*=", "/=", "&=", "@and_assign", "|=", "@or_assign", "^=", "%=", "<<=", "@left_shift_assign", ">>=", "@right_shift_assign", ">>>=", "@right_unsigned_shift_assign"};
    static final long[] jjtoSkip = {1022, 0, 0};
    static final long[] jjtoSpecial = {896, 0, 0};
    static final long[] jjtoToken = {2305843009213692929L, -195, 63};
    public static final String[] lexStateNames = {"DEFAULT"};
    protected char curChar;
    int curLexState;
    public PrintStream debugStream;
    int defaultLexState;
    protected JavaCharStream input_stream;
    int jjmatchedKind;
    int jjmatchedPos;
    int jjnewStateCnt;
    int jjround;
    private final int[] jjrounds;
    private final int[] jjstateSet;

    public ParserTokenManager(JavaCharStream javaCharStream) {
        this.debugStream = System.out;
        this.jjrounds = new int[74];
        this.jjstateSet = new int[148];
        this.curLexState = 0;
        this.defaultLexState = 0;
        this.input_stream = javaCharStream;
    }

    public ParserTokenManager(JavaCharStream javaCharStream, int i) {
        this(javaCharStream);
        SwitchTo(i);
    }

    private final void ReInitRounds() {
        this.jjround = -2147483647;
        int i = 74;
        while (true) {
            int i2 = i - 1;
            if (i > 0) {
                this.jjrounds[i2] = Integer.MIN_VALUE;
                i = i2;
            } else {
                return;
            }
        }
    }

    private final void jjAddStates(int i, int i2) {
        while (true) {
            int[] iArr = this.jjstateSet;
            int i3 = this.jjnewStateCnt;
            this.jjnewStateCnt = i3 + 1;
            iArr[i3] = jjnextStates[i];
            int i4 = i + 1;
            if (i != i2) {
                i = i4;
            } else {
                return;
            }
        }
    }

    private static final boolean jjCanMove_0(int i, int i2, int i3, long j, long j2) {
        switch (i) {
            case 0:
                return (jjbitVec0[i3] & j2) != 0;
            default:
                return false;
        }
    }

    private static final boolean jjCanMove_1(int i, int i2, int i3, long j, long j2) {
        switch (i) {
            case 0:
                return (jjbitVec0[i3] & j2) != 0;
            default:
                return (jjbitVec1[i2] & j) != 0;
        }
    }

    private static final boolean jjCanMove_2(int i, int i2, int i3, long j, long j2) {
        switch (i) {
            case 0:
                return (jjbitVec4[i3] & j2) != 0;
            case ParserConstants.STATIC:
                return (jjbitVec5[i3] & j2) != 0;
            case ParserConstants.STRICTFP:
                return (jjbitVec6[i3] & j2) != 0;
            case ParserConstants.SYNCHRONIZED:
                return (jjbitVec7[i3] & j2) != 0;
            case ParserConstants.DECIMAL_LITERAL:
                return (jjbitVec8[i3] & j2) != 0;
            default:
                return (jjbitVec3[i2] & j) != 0;
        }
    }

    private final void jjCheckNAdd(int i) {
        if (this.jjrounds[i] != this.jjround) {
            int[] iArr = this.jjstateSet;
            int i2 = this.jjnewStateCnt;
            this.jjnewStateCnt = i2 + 1;
            iArr[i2] = i;
            this.jjrounds[i] = this.jjround;
        }
    }

    private final void jjCheckNAddStates(int i) {
        jjCheckNAdd(jjnextStates[i]);
        jjCheckNAdd(jjnextStates[i + 1]);
    }

    private final void jjCheckNAddStates(int i, int i2) {
        while (true) {
            jjCheckNAdd(jjnextStates[i]);
            int i3 = i + 1;
            if (i != i2) {
                i = i3;
            } else {
                return;
            }
        }
    }

    private final void jjCheckNAddTwoStates(int i, int i2) {
        jjCheckNAdd(i);
        jjCheckNAdd(i2);
    }

    private final int jjMoveNfa_0(int i, int i2) {
        int i3;
        int i4 = 0;
        this.jjnewStateCnt = 74;
        int i5 = 1;
        this.jjstateSet[0] = i;
        int i6 = Integer.MAX_VALUE;
        while (true) {
            int i7 = i5;
            int i8 = i4;
            int i9 = i6;
            int i10 = this.jjround + 1;
            this.jjround = i10;
            if (i10 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long j = 1 << this.curChar;
                do {
                    i7--;
                    switch (this.jjstateSet[i7]) {
                        case 0:
                            if ((8589934591L & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 6) {
                                    i9 = 6;
                                }
                                jjCheckNAdd(0);
                                continue;
                            }
                        case 1:
                            if (this.curChar == '!') {
                                jjCheckNAddStates(21, 23);
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if ((-9217 & j) != 0) {
                                jjCheckNAddStates(21, 23);
                                continue;
                            } else {
                                continue;
                            }
                        case 3:
                            if ((9216 & j) != 0 && i9 > 8) {
                                i9 = 8;
                                continue;
                            }
                        case ParserTreeConstants.JJTIMPORTDECLARATION /*4*/:
                            if (this.curChar == 10 && i9 > 8) {
                                i9 = 8;
                                continue;
                            }
                        case ParserTreeConstants.JJTVARIABLEDECLARATOR /*5*/:
                            if (this.curChar == 13) {
                                int[] iArr = this.jjstateSet;
                                int i11 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i11 + 1;
                                iArr[i11] = 4;
                                continue;
                            } else {
                                continue;
                            }
                        case 6:
                            if ((8589934591L & j) != 0) {
                                if (i9 > 6) {
                                    i9 = 6;
                                }
                                jjCheckNAdd(0);
                                i3 = i9;
                            } else if ((287948901175001088L & j) != 0) {
                                jjCheckNAddStates(0, 6);
                                i3 = i9;
                            } else if (this.curChar == '/') {
                                jjAddStates(7, 9);
                                i3 = i9;
                            } else if (this.curChar == '$') {
                                if (i9 > 69) {
                                    i9 = 69;
                                }
                                jjCheckNAdd(35);
                                i3 = i9;
                            } else if (this.curChar == '\"') {
                                jjCheckNAddStates(10, 12);
                                i3 = i9;
                            } else if (this.curChar == '\'') {
                                jjAddStates(13, 14);
                                i3 = i9;
                            } else if (this.curChar == '.') {
                                jjCheckNAdd(11);
                                i3 = i9;
                            } else {
                                if (this.curChar == '#') {
                                    int[] iArr2 = this.jjstateSet;
                                    int i12 = this.jjnewStateCnt;
                                    this.jjnewStateCnt = i12 + 1;
                                    iArr2[i12] = 1;
                                }
                                i3 = i9;
                            }
                            if ((287667426198290432L & j) != 0) {
                                if (i3 > 60) {
                                    i3 = 60;
                                }
                                jjCheckNAddTwoStates(8, 9);
                                i9 = i3;
                                continue;
                            } else if (this.curChar == '0') {
                                if (i3 > 60) {
                                    i3 = 60;
                                }
                                jjCheckNAddStates(15, 17);
                                i9 = i3;
                                continue;
                            } else {
                                i9 = i3;
                                continue;
                            }
                        case 7:
                            if ((287667426198290432L & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 60) {
                                    i9 = 60;
                                }
                                jjCheckNAddTwoStates(8, 9);
                                continue;
                            }
                        case 8:
                            if ((287948901175001088L & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 60) {
                                    i9 = 60;
                                }
                                jjCheckNAddTwoStates(8, 9);
                                continue;
                            }
                        case 10:
                            if (this.curChar == '.') {
                                jjCheckNAdd(11);
                                continue;
                            } else {
                                continue;
                            }
                        case 11:
                            if ((287948901175001088L & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 64) {
                                    i9 = 64;
                                }
                                jjCheckNAddStates(24, 26);
                                continue;
                            }
                        case 13:
                            if ((43980465111040L & j) != 0) {
                                jjCheckNAdd(14);
                                continue;
                            } else {
                                continue;
                            }
                        case 14:
                            if ((287948901175001088L & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 64) {
                                    i9 = 64;
                                }
                                jjCheckNAddTwoStates(14, 15);
                                continue;
                            }
                        case 16:
                            if (this.curChar == '\'') {
                                jjAddStates(13, 14);
                                continue;
                            } else {
                                continue;
                            }
                        case 17:
                            if ((-549755823105L & j) != 0) {
                                jjCheckNAdd(18);
                                continue;
                            } else {
                                continue;
                            }
                        case 18:
                            if (this.curChar == '\'' && i9 > 66) {
                                i9 = 66;
                                continue;
                            }
                        case 20:
                            if ((566935683072L & j) != 0) {
                                jjCheckNAdd(18);
                                continue;
                            } else {
                                continue;
                            }
                        case 21:
                            if ((71776119061217280L & j) != 0) {
                                jjCheckNAddTwoStates(22, 18);
                                continue;
                            } else {
                                continue;
                            }
                        case 22:
                            if ((71776119061217280L & j) != 0) {
                                jjCheckNAdd(18);
                                continue;
                            } else {
                                continue;
                            }
                        case 23:
                            if ((4222124650659840L & j) != 0) {
                                int[] iArr3 = this.jjstateSet;
                                int i13 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i13 + 1;
                                iArr3[i13] = 24;
                                continue;
                            } else {
                                continue;
                            }
                        case 24:
                            if ((71776119061217280L & j) != 0) {
                                jjCheckNAdd(22);
                                continue;
                            } else {
                                continue;
                            }
                        case 25:
                            if (this.curChar == '\"') {
                                jjCheckNAddStates(10, 12);
                                continue;
                            } else {
                                continue;
                            }
                        case 26:
                            if ((-17179878401L & j) != 0) {
                                jjCheckNAddStates(10, 12);
                                continue;
                            } else {
                                continue;
                            }
                        case 28:
                            if ((566935683072L & j) != 0) {
                                jjCheckNAddStates(10, 12);
                                continue;
                            } else {
                                continue;
                            }
                        case 29:
                            if (this.curChar == '\"' && i9 > 67) {
                                i9 = 67;
                                continue;
                            }
                        case 30:
                            if ((71776119061217280L & j) != 0) {
                                jjCheckNAddStates(27, 30);
                                continue;
                            } else {
                                continue;
                            }
                        case 31:
                            if ((71776119061217280L & j) != 0) {
                                jjCheckNAddStates(10, 12);
                                continue;
                            } else {
                                continue;
                            }
                        case 32:
                            if ((4222124650659840L & j) != 0) {
                                int[] iArr4 = this.jjstateSet;
                                int i14 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i14 + 1;
                                iArr4[i14] = 33;
                                continue;
                            } else {
                                continue;
                            }
                        case 33:
                            if ((71776119061217280L & j) != 0) {
                                jjCheckNAdd(31);
                                continue;
                            } else {
                                continue;
                            }
                        case 34:
                            if (this.curChar != '$') {
                                continue;
                            } else {
                                if (i9 > 69) {
                                    i9 = 69;
                                }
                                jjCheckNAdd(35);
                                continue;
                            }
                        case 35:
                            if ((287948969894477824L & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 69) {
                                    i9 = 69;
                                }
                                jjCheckNAdd(35);
                                continue;
                            }
                        case 36:
                            if ((287948901175001088L & j) != 0) {
                                jjCheckNAddStates(0, 6);
                                continue;
                            } else {
                                continue;
                            }
                        case 37:
                            if ((287948901175001088L & j) != 0) {
                                jjCheckNAddTwoStates(37, 38);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.LONG:
                            if (this.curChar != '.') {
                                continue;
                            } else {
                                if (i9 > 64) {
                                    i9 = 64;
                                }
                                jjCheckNAddStates(31, 33);
                                continue;
                            }
                        case ParserConstants.NATIVE:
                            if ((287948901175001088L & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 64) {
                                    i9 = 64;
                                }
                                jjCheckNAddStates(31, 33);
                                continue;
                            }
                        case ParserConstants.NULL:
                            if ((43980465111040L & j) != 0) {
                                jjCheckNAdd(42);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.PACKAGE:
                            if ((287948901175001088L & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 64) {
                                    i9 = 64;
                                }
                                jjCheckNAddTwoStates(42, 15);
                                continue;
                            }
                        case ParserConstants.PRIVATE:
                            if ((287948901175001088L & j) != 0) {
                                jjCheckNAddTwoStates(43, 44);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.PUBLIC:
                            if ((43980465111040L & j) != 0) {
                                jjCheckNAdd(46);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.RETURN:
                            if ((287948901175001088L & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 64) {
                                    i9 = 64;
                                }
                                jjCheckNAddTwoStates(46, 15);
                                continue;
                            }
                        case ParserConstants.SHORT:
                            if ((287948901175001088L & j) != 0) {
                                jjCheckNAddStates(34, 36);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.STRICTFP:
                            if ((43980465111040L & j) != 0) {
                                jjCheckNAdd(50);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.SWITCH:
                            if ((287948901175001088L & j) != 0) {
                                jjCheckNAddTwoStates(50, 15);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.SYNCHRONIZED:
                            if (this.curChar != '0') {
                                continue;
                            } else {
                                if (i9 > 60) {
                                    i9 = 60;
                                }
                                jjCheckNAddStates(15, 17);
                                continue;
                            }
                        case ParserConstants.THROW:
                            if ((287948901175001088L & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 60) {
                                    i9 = 60;
                                }
                                jjCheckNAddTwoStates(53, 9);
                                continue;
                            }
                        case ParserConstants.THROWS:
                            if ((71776119061217280L & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 60) {
                                    i9 = 60;
                                }
                                jjCheckNAddTwoStates(54, 9);
                                continue;
                            }
                        case ParserConstants.TRUE:
                            if (this.curChar == '/') {
                                jjAddStates(7, 9);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.TRY:
                            if (this.curChar == '*') {
                                int[] iArr5 = this.jjstateSet;
                                int i15 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i15 + 1;
                                iArr5[i15] = 67;
                            } else if (this.curChar == '/') {
                                if (i9 > 7) {
                                    i9 = 7;
                                }
                                jjCheckNAddStates(18, 20);
                            }
                            if (this.curChar == '*') {
                                jjCheckNAdd(62);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.VOID:
                            if ((-9217 & j) == 0) {
                                continue;
                            } else {
                                if (i9 > 7) {
                                    i9 = 7;
                                }
                                jjCheckNAddStates(18, 20);
                                continue;
                            }
                        case ParserConstants.VOLATILE:
                            if ((9216 & j) != 0 && i9 > 7) {
                                i9 = 7;
                                continue;
                            }
                        case ParserConstants.WHILE:
                            if (this.curChar == 10 && i9 > 7) {
                                i9 = 7;
                                continue;
                            }
                        case ParserConstants.INTEGER_LITERAL:
                            if (this.curChar == 13) {
                                int[] iArr6 = this.jjstateSet;
                                int i16 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i16 + 1;
                                iArr6[i16] = 59;
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.DECIMAL_LITERAL:
                            if (this.curChar == '*') {
                                jjCheckNAdd(62);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.HEX_LITERAL:
                            if ((-4398046511105L & j) != 0) {
                                jjCheckNAddTwoStates(62, 63);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.OCTAL_LITERAL:
                            if (this.curChar == '*') {
                                jjCheckNAddStates(37, 39);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.FLOATING_POINT_LITERAL:
                            if ((-145135534866433L & j) != 0) {
                                jjCheckNAddTwoStates(65, 63);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.EXPONENT:
                            if ((-4398046511105L & j) != 0) {
                                jjCheckNAddTwoStates(65, 63);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.CHARACTER_LITERAL:
                            if (this.curChar == '/' && i9 > 9) {
                                i9 = 9;
                                continue;
                            }
                        case ParserConstants.STRING_LITERAL:
                            if (this.curChar == '*') {
                                jjCheckNAddTwoStates(68, 69);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.FORMAL_COMMENT:
                            if ((-4398046511105L & j) != 0) {
                                jjCheckNAddTwoStates(68, 69);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.IDENTIFIER:
                            if (this.curChar == '*') {
                                jjCheckNAddStates(40, 42);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.LETTER:
                            if ((-145135534866433L & j) != 0) {
                                jjCheckNAddTwoStates(71, 69);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.DIGIT:
                            if ((-4398046511105L & j) != 0) {
                                jjCheckNAddTwoStates(71, 69);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.LPAREN:
                            if (this.curChar == '/' && i9 > 68) {
                                i9 = 68;
                                continue;
                            }
                        case ParserConstants.RPAREN:
                            if (this.curChar == '*') {
                                int[] iArr7 = this.jjstateSet;
                                int i17 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i17 + 1;
                                iArr7[i17] = 67;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i7 != i8);
                i6 = i9;
            } else if (this.curChar < 128) {
                long j2 = 1 << (this.curChar & '?');
                do {
                    i7--;
                    switch (this.jjstateSet[i7]) {
                        case 2:
                            jjAddStates(21, 23);
                            continue;
                        case 6:
                        case 35:
                            if ((576460745995190270L & j2) == 0) {
                                continue;
                            } else {
                                if (i9 > 69) {
                                    i9 = 69;
                                }
                                jjCheckNAdd(35);
                                continue;
                            }
                        case 9:
                            if ((17592186048512L & j2) != 0 && i9 > 60) {
                                i9 = 60;
                                continue;
                            }
                        case 12:
                            if ((137438953504L & j2) != 0) {
                                jjAddStates(43, 44);
                                continue;
                            } else {
                                continue;
                            }
                        case 15:
                            if ((343597383760L & j2) != 0 && i9 > 64) {
                                i9 = 64;
                                continue;
                            }
                        case 17:
                            if ((-268435457 & j2) != 0) {
                                jjCheckNAdd(18);
                                continue;
                            } else {
                                continue;
                            }
                        case 19:
                            if (this.curChar == '\\') {
                                jjAddStates(45, 47);
                                continue;
                            } else {
                                continue;
                            }
                        case 20:
                            if ((5700160604602368L & j2) != 0) {
                                jjCheckNAdd(18);
                                continue;
                            } else {
                                continue;
                            }
                        case 26:
                            if ((-268435457 & j2) != 0) {
                                jjCheckNAddStates(10, 12);
                                continue;
                            } else {
                                continue;
                            }
                        case 27:
                            if (this.curChar == '\\') {
                                jjAddStates(48, 50);
                                continue;
                            } else {
                                continue;
                            }
                        case 28:
                            if ((5700160604602368L & j2) != 0) {
                                jjCheckNAddStates(10, 12);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.NEW:
                            if ((137438953504L & j2) != 0) {
                                jjAddStates(51, 52);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.PROTECTED:
                            if ((137438953504L & j2) != 0) {
                                jjAddStates(53, 54);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.STATIC:
                            if ((137438953504L & j2) != 0) {
                                jjAddStates(55, 56);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.TRANSIENT:
                            if ((72057594054705152L & j2) != 0) {
                                jjCheckNAdd(53);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.THROW:
                            if ((541165879422L & j2) == 0) {
                                continue;
                            } else {
                                if (i9 > 60) {
                                    i9 = 60;
                                }
                                jjCheckNAddTwoStates(53, 9);
                                continue;
                            }
                        case ParserConstants.VOID:
                            if (i9 > 7) {
                                i9 = 7;
                            }
                            jjAddStates(18, 20);
                            continue;
                        case ParserConstants.HEX_LITERAL:
                            jjCheckNAddTwoStates(62, 63);
                            continue;
                        case ParserConstants.FLOATING_POINT_LITERAL:
                        case ParserConstants.EXPONENT:
                            jjCheckNAddTwoStates(65, 63);
                            continue;
                        case ParserConstants.FORMAL_COMMENT:
                            jjCheckNAddTwoStates(68, 69);
                            continue;
                        case ParserConstants.LETTER:
                        case ParserConstants.DIGIT:
                            jjCheckNAddTwoStates(71, 69);
                            continue;
                    }
                } while (i7 != i8);
                i6 = i9;
            } else {
                int i18 = this.curChar >> 8;
                int i19 = i18 >> 6;
                long j3 = 1 << (i18 & 63);
                int i20 = (this.curChar & 255) >> 6;
                long j4 = 1 << (this.curChar & '?');
                do {
                    i7--;
                    switch (this.jjstateSet[i7]) {
                        case 0:
                            if (!jjCanMove_0(i18, i19, i20, j3, j4)) {
                                continue;
                            } else {
                                if (i9 > 6) {
                                    i9 = 6;
                                }
                                jjCheckNAdd(0);
                                continue;
                            }
                        case 2:
                            if (jjCanMove_1(i18, i19, i20, j3, j4)) {
                                jjAddStates(21, 23);
                                continue;
                            } else {
                                continue;
                            }
                        case 6:
                            if (jjCanMove_0(i18, i19, i20, j3, j4)) {
                                if (i9 > 6) {
                                    i9 = 6;
                                }
                                jjCheckNAdd(0);
                            }
                            if (!jjCanMove_2(i18, i19, i20, j3, j4)) {
                                continue;
                            } else {
                                if (i9 > 69) {
                                    i9 = 69;
                                }
                                jjCheckNAdd(35);
                                continue;
                            }
                        case 17:
                            if (jjCanMove_1(i18, i19, i20, j3, j4)) {
                                int[] iArr8 = this.jjstateSet;
                                int i21 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i21 + 1;
                                iArr8[i21] = 18;
                                continue;
                            } else {
                                continue;
                            }
                        case 26:
                            if (jjCanMove_1(i18, i19, i20, j3, j4)) {
                                jjAddStates(10, 12);
                                continue;
                            } else {
                                continue;
                            }
                        case 34:
                        case 35:
                            if (!jjCanMove_2(i18, i19, i20, j3, j4)) {
                                continue;
                            } else {
                                if (i9 > 69) {
                                    i9 = 69;
                                }
                                jjCheckNAdd(35);
                                continue;
                            }
                        case ParserConstants.VOID:
                            if (!jjCanMove_1(i18, i19, i20, j3, j4)) {
                                continue;
                            } else {
                                if (i9 > 7) {
                                    i9 = 7;
                                }
                                jjAddStates(18, 20);
                                continue;
                            }
                        case ParserConstants.HEX_LITERAL:
                            if (jjCanMove_1(i18, i19, i20, j3, j4)) {
                                jjCheckNAddTwoStates(62, 63);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.FLOATING_POINT_LITERAL:
                        case ParserConstants.EXPONENT:
                            if (jjCanMove_1(i18, i19, i20, j3, j4)) {
                                jjCheckNAddTwoStates(65, 63);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.FORMAL_COMMENT:
                            if (jjCanMove_1(i18, i19, i20, j3, j4)) {
                                jjCheckNAddTwoStates(68, 69);
                                continue;
                            } else {
                                continue;
                            }
                        case ParserConstants.LETTER:
                        case ParserConstants.DIGIT:
                            if (jjCanMove_1(i18, i19, i20, j3, j4)) {
                                jjCheckNAddTwoStates(71, 69);
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i7 != i8);
                i6 = i9;
            }
            if (i6 != Integer.MAX_VALUE) {
                this.jjmatchedKind = i6;
                this.jjmatchedPos = i2;
                i6 = Integer.MAX_VALUE;
            }
            i2++;
            i5 = this.jjnewStateCnt;
            this.jjnewStateCnt = i8;
            i4 = 74 - i8;
            if (i5 != i4) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return i2;
        }
    }

    private final int jjMoveStringLiteralDfa0_0() {
        switch (this.curChar) {
            case 9:
                return jjStartNfaWithStates_0(0, 2, 0);
            case 10:
                return jjStartNfaWithStates_0(0, 5, 0);
            case 12:
                return jjStartNfaWithStates_0(0, 4, 0);
            case 13:
                return jjStartNfaWithStates_0(0, 3, 0);
            case ' ':
                return jjStartNfaWithStates_0(0, 1, 0);
            case '!':
                this.jjmatchedKind = 86;
                return jjMoveStringLiteralDfa1_0(0, 2147483648L, 0);
            case '%':
                this.jjmatchedKind = ParserConstants.MOD;
                return jjMoveStringLiteralDfa1_0(0, Long.MIN_VALUE, 0);
            case ParserConstants.LONG:
                this.jjmatchedKind = ParserConstants.BIT_AND;
                return jjMoveStringLiteralDfa1_0(0, 288230393331580928L, 0);
            case ParserConstants.NEW:
                return jjStopAtPos(0, 72);
            case ParserConstants.NULL:
                return jjStopAtPos(0, 73);
            case ParserConstants.PACKAGE:
                this.jjmatchedKind = ParserConstants.STAR;
                return jjMoveStringLiteralDfa1_0(0, 72057594037927936L, 0);
            case ParserConstants.PRIVATE:
                this.jjmatchedKind = ParserConstants.PLUS;
                return jjMoveStringLiteralDfa1_0(0, 18014467228958720L, 0);
            case ParserConstants.PROTECTED:
                return jjStopAtPos(0, 79);
            case ParserConstants.PUBLIC:
                this.jjmatchedKind = ParserConstants.MINUS;
                return jjMoveStringLiteralDfa1_0(0, 36028934457917440L, 0);
            case ParserConstants.RETURN:
                return jjStartNfaWithStates_0(0, 80, 11);
            case ParserConstants.SHORT:
                this.jjmatchedKind = ParserConstants.SLASH;
                return jjMoveStringLiteralDfa1_0(0, 144115188075855872L, 0);
            case ParserConstants.VOLATILE:
                return jjStopAtPos(0, 89);
            case ParserConstants.WHILE:
                return jjStopAtPos(0, 78);
            case ParserConstants.INTEGER_LITERAL:
                this.jjmatchedKind = 84;
                return jjMoveStringLiteralDfa1_0(0, 281475110928384L, 1);
            case ParserConstants.DECIMAL_LITERAL:
                this.jjmatchedKind = 81;
                return jjMoveStringLiteralDfa1_0(0, 67108864, 0);
            case ParserConstants.HEX_LITERAL:
                this.jjmatchedKind = 82;
                return jjMoveStringLiteralDfa1_0(0, 5629500071084032L, 20);
            case ParserConstants.OCTAL_LITERAL:
                return jjStopAtPos(0, 88);
            case ParserConstants.FLOATING_POINT_LITERAL:
                return jjMoveStringLiteralDfa1_0(0, 2894169735298547712L, 42);
            case ParserConstants.LE:
                return jjStopAtPos(0, 76);
            case ParserConstants.GE:
                return jjStopAtPos(0, 77);
            case ParserConstants.GEX:
                this.jjmatchedKind = ParserConstants.XOR;
                return jjMoveStringLiteralDfa1_0(0, 4611686018427387904L, 0);
            case ParserConstants.BOOL_ORX:
                return jjMoveStringLiteralDfa1_0(1024, 0, 0);
            case ParserConstants.BOOL_AND:
                return jjMoveStringLiteralDfa1_0(22528, 0, 0);
            case ParserConstants.BOOL_ANDX:
                return jjMoveStringLiteralDfa1_0(1024000, 0, 0);
            case ParserConstants.INCR:
                return jjMoveStringLiteralDfa1_0(7340032, 0, 0);
            case ParserConstants.DECR:
                return jjMoveStringLiteralDfa1_0(58720256, 0, 0);
            case ParserConstants.PLUS:
                return jjMoveStringLiteralDfa1_0(2080374784, 0, 0);
            case ParserConstants.MINUS:
                return jjMoveStringLiteralDfa1_0(2147483648L, 0, 0);
            case ParserConstants.SLASH:
                return jjMoveStringLiteralDfa1_0(270582939648L, 0, 0);
            case ParserConstants.BIT_OR:
                return jjMoveStringLiteralDfa1_0(274877906944L, 0, 0);
            case ParserConstants.XOR:
                return jjMoveStringLiteralDfa1_0(3848290697216L, 0, 0);
            case ParserConstants.LSHIFT:
                return jjMoveStringLiteralDfa1_0(65970697666560L, 0, 0);
            case ParserConstants.RSIGNEDSHIFT:
                return jjMoveStringLiteralDfa1_0(70368744177664L, 0, 0);
            case ParserConstants.RSIGNEDSHIFTX:
                return jjMoveStringLiteralDfa1_0(4362862139015168L, 0, 0);
            case ParserConstants.RUNSIGNEDSHIFT:
                return jjMoveStringLiteralDfa1_0(139611588448485376L, 0, 0);
            case ParserConstants.PLUSASSIGN:
                return jjMoveStringLiteralDfa1_0(432345564227567616L, 0, 0);
            case ParserConstants.MINUSASSIGN:
                return jjMoveStringLiteralDfa1_0(576460752303423488L, 0, 0);
            case ParserConstants.ANDASSIGNX:
                return jjStopAtPos(0, 74);
            case ParserConstants.ORASSIGN:
                this.jjmatchedKind = ParserConstants.BIT_OR;
                return jjMoveStringLiteralDfa1_0(0, 1152921508901814272L, 0);
            case ParserConstants.ORASSIGNX:
                return jjStopAtPos(0, 75);
            case ParserConstants.XORASSIGN:
                return jjStopAtPos(0, 87);
            default:
                return jjMoveNfa_0(6, 0);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private final int jjMoveStringLiteralDfa10_0(long j, long j2, long j3, long j4, long j5, long j6) {
        long j7 = j2 & j;
        long j8 = j4 & j3;
        long j9 = j6 & j5;
        if ((j7 | j8 | j9) == 0) {
            return jjStartNfa_0(8, j, j3, j5);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.DECR:
                    return jjMoveStringLiteralDfa11_0(j7, 2251799813685248L, j8, 0, j9, 0);
                case ParserConstants.PLUS:
                    return jjMoveStringLiteralDfa11_0(j7, 0, j8, 2251799813685248L, j9, 8);
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa11_0(j7, 0, j8, 9007199254740992L, j9, 32);
                case ParserConstants.XOR:
                    return (576460752303423488L & j8) != 0 ? jjStopAtPos(10, ParserConstants.ANDASSIGNX) : jjMoveStringLiteralDfa11_0(j7, 0, j8, 8796093022208L, j9, 0);
                case ParserConstants.RSIGNEDSHIFT:
                    if ((35184372088832L & j8) != 0) {
                        return jjStopAtPos(10, ParserConstants.BIT_ORX);
                    }
                    break;
                case ParserConstants.RUNSIGNEDSHIFT:
                    if ((562949953421312L & j8) != 0) {
                        this.jjmatchedKind = ParserConstants.LSHIFTX;
                        this.jjmatchedPos = 10;
                    }
                    return jjMoveStringLiteralDfa11_0(j7, 0, j8, 0, j9, 2);
            }
            return jjStartNfa_0(9, j7, j8, j9);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(9, j7, j8, j9);
            return 10;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private final int jjMoveStringLiteralDfa11_0(long j, long j2, long j3, long j4, long j5, long j6) {
        long j7 = j2 & j;
        long j8 = j4 & j3;
        long j9 = j6 & j5;
        if ((j7 | j8 | j9) == 0) {
            return jjStartNfa_0(9, j, j3, j5);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.NE:
                    return jjMoveStringLiteralDfa12_0(j7, 0, j8, 0, j9, 2);
                case ParserConstants.INCR:
                    if ((2251799813685248L & j7) != 0) {
                        return jjStartNfaWithStates_0(11, 51, 35);
                    }
                    if ((8796093022208L & j8) != 0) {
                        return jjStopAtPos(11, ParserConstants.BIT_ANDX);
                    }
                    break;
                case ParserConstants.MINUS:
                    return jjMoveStringLiteralDfa12_0(j7, 0, j8, 9007199254740992L, j9, 32);
                case ParserConstants.RUNSIGNEDSHIFT:
                    if ((2251799813685248L & j8) != 0) {
                        this.jjmatchedKind = ParserConstants.RSIGNEDSHIFTX;
                        this.jjmatchedPos = 11;
                    }
                    return jjMoveStringLiteralDfa12_0(j7, 0, j8, 0, j9, 8);
            }
            return jjStartNfa_0(10, j7, j8, j9);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(10, j7, j8, j9);
            return 11;
        }
    }

    private final int jjMoveStringLiteralDfa12_0(long j, long j2, long j3, long j4, long j5, long j6) {
        long j7 = j4 & j3;
        long j8 = j6 & j5;
        if (((j2 & j) | j7 | j8) == 0) {
            return jjStartNfa_0(10, j, j3, j5);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.NE:
                    return jjMoveStringLiteralDfa13_0(j7, 0, j8, 8);
                case ParserConstants.BOOL_ORX:
                    return jjMoveStringLiteralDfa13_0(j7, 0, j8, 2);
                case ParserConstants.XOR:
                    return jjMoveStringLiteralDfa13_0(j7, 9007199254740992L, j8, 32);
                default:
                    return jjStartNfa_0(11, 0, j7, j8);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(11, 0, j7, j8);
            return 12;
        }
    }

    private final int jjMoveStringLiteralDfa13_0(long j, long j2, long j3, long j4) {
        long j5 = j2 & j;
        long j6 = j4 & j3;
        if ((j5 | j6) == 0) {
            return jjStartNfa_0(11, 0, j, j3);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.BOOL_ORX:
                    return jjMoveStringLiteralDfa14_0(j5, 0, j6, 8);
                case ParserConstants.DECR:
                    return jjMoveStringLiteralDfa14_0(j5, 9007199254740992L, j6, 32);
                case ParserConstants.RSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa14_0(j5, 0, j6, 2);
                default:
                    return jjStartNfa_0(12, 0, j5, j6);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(12, 0, j5, j6);
            return 13;
        }
    }

    private final int jjMoveStringLiteralDfa14_0(long j, long j2, long j3, long j4) {
        long j5 = j2 & j;
        long j6 = j4 & j3;
        if ((j5 | j6) == 0) {
            return jjStartNfa_0(12, 0, j, j3);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.INCR:
                    return jjMoveStringLiteralDfa15_0(j5, 9007199254740992L, j6, 32);
                case ParserConstants.RSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa15_0(j5, 0, j6, 10);
                default:
                    return jjStartNfa_0(13, 0, j5, j6);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(13, 0, j5, j6);
            return 14;
        }
    }

    private final int jjMoveStringLiteralDfa15_0(long j, long j2, long j3, long j4) {
        long j5 = j2 & j;
        long j6 = j4 & j3;
        if ((j5 | j6) == 0) {
            return jjStartNfa_0(13, 0, j, j3);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.NE:
                    return jjMoveStringLiteralDfa16_0(j5, 9007199254740992L, j6, 32);
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa16_0(j5, 0, j6, 2);
                case ParserConstants.RSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa16_0(j5, 0, j6, 8);
                default:
                    return jjStartNfa_0(14, 0, j5, j6);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(14, 0, j5, j6);
            return 15;
        }
    }

    private final int jjMoveStringLiteralDfa16_0(long j, long j2, long j3, long j4) {
        long j5 = j2 & j;
        long j6 = j4 & j3;
        if ((j5 | j6) == 0) {
            return jjStartNfa_0(14, 0, j, j3);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.MINUS:
                    return jjMoveStringLiteralDfa17_0(j5, 0, j6, 2);
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa17_0(j5, 0, j6, 8);
                case ParserConstants.RSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa17_0(j5, 9007199254740992L, j6, 32);
                default:
                    return jjStartNfa_0(15, 0, j5, j6);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(15, 0, j5, j6);
            return 16;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private final int jjMoveStringLiteralDfa17_0(long j, long j2, long j3, long j4) {
        long j5 = j2 & j;
        long j6 = j4 & j3;
        if ((j5 | j6) == 0) {
            return jjStartNfa_0(15, 0, j, j3);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.MINUS:
                    return jjMoveStringLiteralDfa18_0(j5, 0, j6, 8);
                case ParserConstants.STAR:
                    return jjMoveStringLiteralDfa18_0(j5, 9007199254740992L, j6, 32);
                case ParserConstants.XOR:
                    if ((2 & j6) != 0) {
                        return jjStopAtPos(17, ParserConstants.LSHIFTASSIGNX);
                    }
                    break;
            }
            return jjStartNfa_0(16, 0, j5, j6);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(16, 0, j5, j6);
            return 17;
        }
    }

    private final int jjMoveStringLiteralDfa18_0(long j, long j2, long j3, long j4) {
        long j5 = j2 & j;
        long j6 = j4 & j3;
        if ((j5 | j6) == 0) {
            return jjStartNfa_0(16, 0, j, j3);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa19_0(j5, 9007199254740992L, j6, 32);
                case ParserConstants.XOR:
                    if ((8 & j6) != 0) {
                        return jjStopAtPos(18, ParserConstants.RSIGNEDSHIFTASSIGNX);
                    }
                    break;
            }
            return jjStartNfa_0(17, 0, j5, j6);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(17, 0, j5, j6);
            return 18;
        }
    }

    private final int jjMoveStringLiteralDfa19_0(long j, long j2, long j3, long j4) {
        long j5 = j2 & j;
        long j6 = j4 & j3;
        if ((j5 | j6) == 0) {
            return jjStartNfa_0(17, 0, j, j3);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.PLUS:
                    return jjMoveStringLiteralDfa20_0(j5, 9007199254740992L, j6, 32);
                default:
                    return jjStartNfa_0(18, 0, j5, j6);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(18, 0, j5, j6);
            return 19;
        }
    }

    private final int jjMoveStringLiteralDfa1_0(long j, long j2, long j3) {
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.LONG:
                    if ((17179869184L & j2) != 0) {
                        return jjStopAtPos(1, 98);
                    }
                    break;
                case ParserConstants.PRIVATE:
                    if ((68719476736L & j2) != 0) {
                        return jjStopAtPos(1, 100);
                    }
                    break;
                case ParserConstants.PUBLIC:
                    if ((137438953472L & j2) != 0) {
                        return jjStopAtPos(1, ParserConstants.DECR);
                    }
                    break;
                case ParserConstants.INTEGER_LITERAL:
                    if ((281474976710656L & j2) != 0) {
                        this.jjmatchedKind = ParserConstants.LSHIFT;
                        this.jjmatchedPos = 1;
                    }
                    return jjMoveStringLiteralDfa2_0(j, 0, j2, 0, j3, 1);
                case ParserConstants.DECIMAL_LITERAL:
                    if ((67108864 & j2) != 0) {
                        return jjStopAtPos(1, 90);
                    }
                    if ((134217728 & j2) != 0) {
                        return jjStopAtPos(1, 91);
                    }
                    if ((536870912 & j2) != 0) {
                        return jjStopAtPos(1, 93);
                    }
                    if ((2147483648L & j2) != 0) {
                        return jjStopAtPos(1, 95);
                    }
                    if ((18014398509481984L & j2) != 0) {
                        return jjStopAtPos(1, ParserConstants.PLUSASSIGN);
                    }
                    if ((36028797018963968L & j2) != 0) {
                        return jjStopAtPos(1, ParserConstants.MINUSASSIGN);
                    }
                    if ((72057594037927936L & j2) != 0) {
                        return jjStopAtPos(1, ParserConstants.STARASSIGN);
                    }
                    if ((144115188075855872L & j2) != 0) {
                        return jjStopAtPos(1, ParserConstants.SLASHASSIGN);
                    }
                    if ((288230376151711744L & j2) != 0) {
                        return jjStopAtPos(1, ParserConstants.ANDASSIGN);
                    }
                    if ((1152921504606846976L & j2) != 0) {
                        return jjStopAtPos(1, ParserConstants.ORASSIGN);
                    }
                    if ((4611686018427387904L & j2) != 0) {
                        return jjStopAtPos(1, ParserConstants.XORASSIGN);
                    }
                    if ((Long.MIN_VALUE & j2) != 0) {
                        return jjStopAtPos(1, ParserConstants.MODASSIGN);
                    }
                    break;
                case ParserConstants.HEX_LITERAL:
                    if ((1125899906842624L & j2) != 0) {
                        this.jjmatchedKind = ParserConstants.RSIGNEDSHIFT;
                        this.jjmatchedPos = 1;
                    }
                    return jjMoveStringLiteralDfa2_0(j, 0, j2, 4503599627370496L, j3, 20);
                case ParserConstants.BOOL_ORX:
                    return jjMoveStringLiteralDfa2_0(j, 4947869532160L, j2, 576460786663161856L, j3, 0);
                case ParserConstants.BOOL_AND:
                    return jjMoveStringLiteralDfa2_0(j, 1024, j2, 43980465111040L, j3, 0);
                case ParserConstants.DECR:
                    return jjMoveStringLiteralDfa2_0(j, 71468256854016L, j2, 0, j3, 0);
                case ParserConstants.PLUS:
                    if ((4294967296L & j) != 0) {
                        return jjStartNfaWithStates_0(1, 32, 35);
                    }
                    break;
                case ParserConstants.MINUS:
                    return jjMoveStringLiteralDfa2_0(j, 0, j2, 1074266112, j3, 0);
                case ParserConstants.STAR:
                    return jjMoveStringLiteralDfa2_0(j, 603623087556132864L, j2, 0, j3, 0);
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa2_0(j, 402653184, j2, 0, j3, 0);
                case ParserConstants.BIT_OR:
                    return jjMoveStringLiteralDfa2_0(j, 545267712, j2, 562950223953920L, j3, 2);
                case ParserConstants.BIT_ORX:
                    return jjMoveStringLiteralDfa2_0(j, 25769803776L, j2, 0, j3, 0);
                case ParserConstants.XOR:
                    return jjMoveStringLiteralDfa2_0(j, 240534945792L, j2, 0, j3, 0);
                case ParserConstants.MOD:
                    if ((2097152 & j) != 0) {
                        this.jjmatchedKind = 21;
                        this.jjmatchedPos = 1;
                    }
                    return jjMoveStringLiteralDfa2_0(j, 432345842331682816L, j2, 2305843017803628544L, j3, 0);
                case ParserConstants.RSIGNEDSHIFT:
                    return jjMoveStringLiteralDfa2_0(j, 112616378963333120L, j2, 11258999068426240L, j3, 40);
                case ParserConstants.RUNSIGNEDSHIFT:
                    return jjMoveStringLiteralDfa2_0(j, 844424930131968L, j2, 0, j3, 0);
                case ParserConstants.RUNSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa2_0(j, 37383395344384L, j2, 0, j3, 0);
                case ParserConstants.MINUSASSIGN:
                    return jjMoveStringLiteralDfa2_0(j, 1125899906842624L, j2, 0, j3, 0);
                case ParserConstants.STARASSIGN:
                    return jjMoveStringLiteralDfa2_0(j, 33554432, j2, 0, j3, 0);
                case ParserConstants.SLASHASSIGN:
                    return jjMoveStringLiteralDfa2_0(j, 2251799813701632L, j2, 0, j3, 0);
                case ParserConstants.ORASSIGN:
                    if ((4294967296L & j2) != 0) {
                        return jjStopAtPos(1, 96);
                    }
                    break;
            }
            return jjStartNfa_0(0, j, j2, j3);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(0, j, j2, j3);
            return 1;
        }
    }

    private final int jjMoveStringLiteralDfa20_0(long j, long j2, long j3, long j4) {
        long j5 = j2 & j;
        long j6 = j4 & j3;
        if ((j5 | j6) == 0) {
            return jjStartNfa_0(18, 0, j, j3);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.RUNSIGNEDSHIFT:
                    if ((9007199254740992L & j5) != 0) {
                        this.jjmatchedKind = ParserConstants.RUNSIGNEDSHIFTX;
                        this.jjmatchedPos = 20;
                    }
                    return jjMoveStringLiteralDfa21_0(j5, 0, j6, 32);
                default:
                    return jjStartNfa_0(19, 0, j5, j6);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(19, 0, j5, j6);
            return 20;
        }
    }

    private final int jjMoveStringLiteralDfa21_0(long j, long j2, long j3, long j4) {
        long j5 = j4 & j3;
        if (((j2 & j) | j5) == 0) {
            return jjStartNfa_0(19, 0, j, j3);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.NE:
                    return jjMoveStringLiteralDfa22_0(j5, 32);
                default:
                    return jjStartNfa_0(20, 0, 0, j5);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(20, 0, 0, j5);
            return 21;
        }
    }

    private final int jjMoveStringLiteralDfa22_0(long j, long j2) {
        long j3 = j2 & j;
        if (j3 == 0) {
            return jjStartNfa_0(20, 0, 0, j);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.BOOL_ORX:
                    return jjMoveStringLiteralDfa23_0(j3, 32);
                default:
                    return jjStartNfa_0(21, 0, 0, j3);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(21, 0, 0, j3);
            return 22;
        }
    }

    private final int jjMoveStringLiteralDfa23_0(long j, long j2) {
        long j3 = j2 & j;
        if (j3 == 0) {
            return jjStartNfa_0(21, 0, 0, j);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.RSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa24_0(j3, 32);
                default:
                    return jjStartNfa_0(22, 0, 0, j3);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(22, 0, 0, j3);
            return 23;
        }
    }

    private final int jjMoveStringLiteralDfa24_0(long j, long j2) {
        long j3 = j2 & j;
        if (j3 == 0) {
            return jjStartNfa_0(22, 0, 0, j);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.RSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa25_0(j3, 32);
                default:
                    return jjStartNfa_0(23, 0, 0, j3);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(23, 0, 0, j3);
            return 24;
        }
    }

    private final int jjMoveStringLiteralDfa25_0(long j, long j2) {
        long j3 = j2 & j;
        if (j3 == 0) {
            return jjStartNfa_0(23, 0, 0, j);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa26_0(j3, 32);
                default:
                    return jjStartNfa_0(24, 0, 0, j3);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(24, 0, 0, j3);
            return 25;
        }
    }

    private final int jjMoveStringLiteralDfa26_0(long j, long j2) {
        long j3 = j2 & j;
        if (j3 == 0) {
            return jjStartNfa_0(24, 0, 0, j);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.MINUS:
                    return jjMoveStringLiteralDfa27_0(j3, 32);
                default:
                    return jjStartNfa_0(25, 0, 0, j3);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(25, 0, 0, j3);
            return 26;
        }
    }

    private final int jjMoveStringLiteralDfa27_0(long j, long j2) {
        long j3 = j2 & j;
        if (j3 == 0) {
            return jjStartNfa_0(25, 0, 0, j);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.XOR:
                    if ((32 & j3) != 0) {
                        return jjStopAtPos(27, ParserConstants.RUNSIGNEDSHIFTASSIGNX);
                    }
                    break;
            }
            return jjStartNfa_0(26, 0, 0, j3);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(26, 0, 0, j3);
            return 27;
        }
    }

    private final int jjMoveStringLiteralDfa2_0(long j, long j2, long j3, long j4, long j5, long j6) {
        long j7 = j2 & j;
        long j8 = j4 & j3;
        long j9 = j6 & j5;
        if ((j7 | j8 | j9) == 0) {
            return jjStartNfa_0(0, j, j3, j5);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.DECIMAL_LITERAL:
                    if ((1 & j9) != 0) {
                        return jjStopAtPos(2, ParserConstants.LSHIFTASSIGN);
                    }
                    if ((4 & j9) != 0) {
                        return jjStopAtPos(2, ParserConstants.RSIGNEDSHIFTASSIGN);
                    }
                    break;
                case ParserConstants.HEX_LITERAL:
                    if ((4503599627370496L & j8) != 0) {
                        this.jjmatchedKind = ParserConstants.RUNSIGNEDSHIFT;
                        this.jjmatchedPos = 2;
                    }
                    return jjMoveStringLiteralDfa3_0(j7, 0, j8, 0, j9, 16);
                case ParserConstants.BOOL_ORX:
                    return jjMoveStringLiteralDfa3_0(j7, 4785074604220416L, j8, 0, j9, 0);
                case ParserConstants.BOOL_AND:
                    return jjMoveStringLiteralDfa3_0(j7, 35184372088832L, j8, 0, j9, 0);
                case ParserConstants.BOOL_ANDX:
                    return jjMoveStringLiteralDfa3_0(j7, 4398046511104L, j8, 0, j9, 0);
                case ParserConstants.DECR:
                    return jjMoveStringLiteralDfa3_0(j7, 4096, j8, 562949953421312L, j9, 2);
                case ParserConstants.PLUS:
                    return jjMoveStringLiteralDfa3_0(j7, 1048576, j8, 0, j9, 0);
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa3_0(j7, 721710636379144192L, j8, 11302979533537280L, j9, 40);
                case ParserConstants.BIT_OR:
                    return jjMoveStringLiteralDfa3_0(j7, 288232575242076160L, j8, 0, j9, 0);
                case ParserConstants.XOR:
                    return jjMoveStringLiteralDfa3_0(j7, 2252075095031808L, j8, 576460786663161856L, j9, 0);
                case ParserConstants.MOD:
                    return jjMoveStringLiteralDfa3_0(j7, 158330211272704L, j8, 0, j9, 0);
                case ParserConstants.LSHIFT:
                    return jjMoveStringLiteralDfa3_0(j7, 25769803776L, j8, 0, j9, 0);
                case ParserConstants.RSIGNEDSHIFT:
                    if ((1073741824 & j7) != 0) {
                        return jjStartNfaWithStates_0(2, 30, 35);
                    }
                    if ((8589934592L & j8) != 0) {
                        this.jjmatchedKind = 97;
                        this.jjmatchedPos = 2;
                    }
                    return jjMoveStringLiteralDfa3_0(j7, 27584547717644288L, j8, 2305843009213693952L, j9, 0);
                case ParserConstants.RSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa3_0(j7, 34368160768L, j8, 0, j9, 0);
                case ParserConstants.RUNSIGNEDSHIFT:
                    if ((68719476736L & j7) != 0) {
                        this.jjmatchedKind = 36;
                        this.jjmatchedPos = 2;
                    } else if ((524288 & j8) != 0) {
                        this.jjmatchedKind = 83;
                        this.jjmatchedPos = 2;
                    } else if ((2097152 & j8) != 0) {
                        this.jjmatchedKind = 85;
                        this.jjmatchedPos = 2;
                    }
                    return jjMoveStringLiteralDfa3_0(j7, 71058120065024L, j8, 1342177280, j9, 0);
                case ParserConstants.RUNSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa3_0(j7, 36028797039935488L, j8, 0, j9, 0);
                case ParserConstants.MINUSASSIGN:
                    if ((1099511627776L & j7) != 0) {
                        return jjStartNfaWithStates_0(2, 40, 35);
                    }
                    break;
                case ParserConstants.SLASHASSIGN:
                    if ((72057594037927936L & j7) != 0) {
                        return jjStartNfaWithStates_0(2, 56, 35);
                    }
                    break;
            }
            return jjStartNfa_0(1, j7, j8, j9);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(1, j7, j8, j9);
            return 2;
        }
    }

    private final int jjMoveStringLiteralDfa3_0(long j, long j2, long j3, long j4, long j5, long j6) {
        long j7 = j2 & j;
        long j8 = j4 & j3;
        long j9 = j6 & j5;
        if ((j7 | j8 | j9) == 0) {
            return jjStartNfa_0(1, j, j3, j5);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.DECIMAL_LITERAL:
                    if ((16 & j9) != 0) {
                        return jjStopAtPos(3, ParserConstants.RUNSIGNEDSHIFTASSIGN);
                    }
                    break;
                case ParserConstants.NE:
                    return jjMoveStringLiteralDfa4_0(j7, 0, j8, 2305843009213693952L, j9, 0);
                case ParserConstants.BOOL_ORX:
                    return jjMoveStringLiteralDfa4_0(j7, 288230377092288512L, j8, 0, j9, 0);
                case ParserConstants.BOOL_AND:
                    return jjMoveStringLiteralDfa4_0(j7, 4194304, j8, 0, j9, 0);
                case ParserConstants.BOOL_ANDX:
                    return jjMoveStringLiteralDfa4_0(j7, 2251799813750784L, j8, 0, j9, 0);
                case ParserConstants.INCR:
                    if ((144115188075855872L & j7) != 0) {
                        return jjStartNfaWithStates_0(3, 57, 35);
                    }
                    if ((34359738368L & j8) != 0) {
                        this.jjmatchedKind = 99;
                        this.jjmatchedPos = 3;
                    }
                    return jjMoveStringLiteralDfa4_0(j7, 0, j8, 576460752303423488L, j9, 0);
                case ParserConstants.DECR:
                    return (16384 & j7) != 0 ? jjStartNfaWithStates_0(3, 14, 35) : (32768 & j7) != 0 ? jjStartNfaWithStates_0(3, 15, 35) : (8388608 & j7) != 0 ? jjStartNfaWithStates_0(3, 23, 35) : (36028797018963968L & j7) != 0 ? jjStartNfaWithStates_0(3, 55, 35) : jjMoveStringLiteralDfa4_0(j7, 137472507904L, j8, 1342177280, j9, 0);
                case ParserConstants.PLUS:
                    return jjMoveStringLiteralDfa4_0(j7, 0, j8, 562949953421312L, j9, 2);
                case ParserConstants.MINUS:
                    return (274877906944L & j7) != 0 ? jjStartNfaWithStates_0(3, 38, 35) : jjMoveStringLiteralDfa4_0(j7, 0, j8, 11258999068426240L, j9, 40);
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa4_0(j7, 563499709235200L, j8, 0, j9, 0);
                case ParserConstants.BIT_ANDX:
                    return jjMoveStringLiteralDfa4_0(j7, 4398046511104L, j8, 0, j9, 0);
                case ParserConstants.BIT_OR:
                    return (2199023255552L & j7) != 0 ? jjStartNfaWithStates_0(3, 41, 35) : jjMoveStringLiteralDfa4_0(j7, 576495945265448960L, j8, 0, j9, 0);
                case ParserConstants.BIT_ORX:
                    if ((16777216 & j7) != 0) {
                        return jjStartNfaWithStates_0(3, 24, 35);
                    }
                    break;
                case ParserConstants.XOR:
                    return jjMoveStringLiteralDfa4_0(j7, 4503599627370496L, j8, 0, j9, 0);
                case ParserConstants.MOD:
                    return (2147483648L & j7) != 0 ? jjStartNfaWithStates_0(3, 31, 35) : jjMoveStringLiteralDfa4_0(j7, 27021614944092160L, j8, 0, j9, 0);
                case ParserConstants.RSIGNEDSHIFT:
                    return (131072 & j7) != 0 ? jjStartNfaWithStates_0(3, 17, 35) : jjMoveStringLiteralDfa4_0(j7, 140737488355328L, j8, 0, j9, 0);
                case ParserConstants.RSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa4_0(j7, 67379200, j8, 0, j9, 0);
                case ParserConstants.RUNSIGNEDSHIFT:
                    return jjMoveStringLiteralDfa4_0(j7, 1425001429861376L, j8, 43980465111040L, j9, 0);
                case ParserConstants.RUNSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa4_0(j7, 70368744177664L, j8, 0, j9, 0);
                case ParserConstants.PLUSASSIGN:
                    return jjMoveStringLiteralDfa4_0(j7, 8796093022208L, j8, 0, j9, 0);
            }
            return jjStartNfa_0(2, j7, j8, j9);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(2, j7, j8, j9);
            return 3;
        }
    }

    private final int jjMoveStringLiteralDfa4_0(long j, long j2, long j3, long j4, long j5, long j6) {
        long j7 = j2 & j;
        long j8 = j4 & j3;
        long j9 = j6 & j5;
        if ((j7 | j8 | j9) == 0) {
            return jjStartNfa_0(2, j, j3, j5);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.NE:
                    return jjMoveStringLiteralDfa5_0(j7, 0, j8, 576460752303423488L, j9, 0);
                case ParserConstants.BOOL_ORX:
                    return jjMoveStringLiteralDfa5_0(j7, 13228499271680L, j8, 2305843009213693952L, j9, 0);
                case ParserConstants.BOOL_ANDX:
                    return jjMoveStringLiteralDfa5_0(j7, 1688849860263936L, j8, 0, j9, 0);
                case ParserConstants.DECR:
                    return (67108864 & j7) != 0 ? jjStartNfaWithStates_0(4, 26, 35) : (576460752303423488L & j7) != 0 ? jjStartNfaWithStates_0(4, 59, 35) : jjMoveStringLiteralDfa5_0(j7, 17600775981056L, j8, 0, j9, 0);
                case ParserConstants.STAR:
                    return (65536 & j7) != 0 ? jjStartNfaWithStates_0(4, 16, 35) : jjMoveStringLiteralDfa5_0(j7, 2251799813685248L, j8, 11258999068426240L, j9, 40);
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa5_0(j7, 316659349323776L, j8, 0, j9, 0);
                case ParserConstants.BIT_ANDX:
                    if ((4096 & j7) != 0) {
                        return jjStartNfaWithStates_0(4, 12, 35);
                    }
                    break;
                case ParserConstants.BIT_OR:
                    if ((134217728 & j7) != 0) {
                        this.jjmatchedKind = 27;
                        this.jjmatchedPos = 4;
                    }
                    return jjMoveStringLiteralDfa5_0(j7, 272629760, j8, 0, j9, 0);
                case ParserConstants.XOR:
                    return jjMoveStringLiteralDfa5_0(j7, 33554432, j8, 0, j9, 0);
                case ParserConstants.LSHIFTX:
                    if ((268435456 & j8) != 0) {
                        return jjStopAtPos(4, 92);
                    }
                    if ((1073741824 & j8) != 0) {
                        return jjStopAtPos(4, 94);
                    }
                    break;
                case ParserConstants.RSIGNEDSHIFT:
                    return jjMoveStringLiteralDfa5_0(j7, 70523363001344L, j8, 0, j9, 0);
                case ParserConstants.RSIGNEDSHIFTX:
                    return (8192 & j7) != 0 ? jjStartNfaWithStates_0(4, 13, 35) : jjMoveStringLiteralDfa5_0(j7, 4503599627370496L, j8, 0, j9, 0);
                case ParserConstants.RUNSIGNEDSHIFT:
                    return (262144 & j7) != 0 ? jjStartNfaWithStates_0(4, 18, 35) : (536870912 & j7) != 0 ? jjStartNfaWithStates_0(4, 29, 35) : (140737488355328L & j7) != 0 ? jjStartNfaWithStates_0(4, 47, 35) : jjMoveStringLiteralDfa5_0(j7, 288230376151711744L, j8, 562949953421312L, j9, 2);
                case ParserConstants.RUNSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa5_0(j7, 1048576, j8, 0, j9, 0);
                case ParserConstants.PLUSASSIGN:
                    return jjMoveStringLiteralDfa5_0(j7, 549755813888L, j8, 0, j9, 0);
                case ParserConstants.MINUSASSIGN:
                    if ((9007199254740992L & j7) != 0) {
                        this.jjmatchedKind = 53;
                        this.jjmatchedPos = 4;
                    }
                    return jjMoveStringLiteralDfa5_0(j7, 18014398509481984L, j8, 43980465111040L, j9, 0);
            }
            return jjStartNfa_0(3, j7, j8, j9);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(3, j7, j8, j9);
            return 4;
        }
    }

    private final int jjMoveStringLiteralDfa5_0(long j, long j2, long j3, long j4, long j5, long j6) {
        long j7 = j2 & j;
        long j8 = j4 & j3;
        long j9 = j6 & j5;
        if ((j7 | j8 | j9) == 0) {
            return jjStartNfa_0(3, j, j3, j5);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.NE:
                    return jjMoveStringLiteralDfa6_0(j7, 0, j8, 562949953421312L, j9, 2);
                case ParserConstants.BOOL_ORX:
                    return jjMoveStringLiteralDfa6_0(j7, 3072, j8, 576460752303423488L, j9, 0);
                case ParserConstants.BOOL_ANDX:
                    return (35184372088832L & j7) != 0 ? jjStartNfaWithStates_0(5, 45, 35) : (281474976710656L & j7) != 0 ? jjStartNfaWithStates_0(5, 48, 35) : jjMoveStringLiteralDfa6_0(j7, 17592186044416L, j8, 0, j9, 0);
                case ParserConstants.INCR:
                    return jjMoveStringLiteralDfa6_0(j7, 33554432, j8, 0, j9, 0);
                case ParserConstants.DECR:
                    if ((4194304 & j7) != 0) {
                        return jjStartNfaWithStates_0(5, 22, 35);
                    }
                    if ((549755813888L & j7) != 0) {
                        return jjStartNfaWithStates_0(5, 39, 35);
                    }
                    break;
                case ParserConstants.PLUS:
                    return jjMoveStringLiteralDfa6_0(j7, 137438953472L, j8, 0, j9, 0);
                case ParserConstants.MINUS:
                    return jjMoveStringLiteralDfa6_0(j7, 4398046511104L, j8, 0, j9, 0);
                case ParserConstants.STAR:
                    if ((1125899906842624L & j7) != 0) {
                        return jjStartNfaWithStates_0(5, 50, 35);
                    }
                    break;
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa6_0(j7, 292733975779082240L, j8, 43980465111040L, j9, 0);
                case ParserConstants.BIT_OR:
                    return jjMoveStringLiteralDfa6_0(j7, 269484032, j8, 0, j9, 0);
                case ParserConstants.BIT_ORX:
                    return jjMoveStringLiteralDfa6_0(j7, 8589934592L, j8, 0, j9, 0);
                case ParserConstants.XOR:
                    return (70368744177664L & j7) != 0 ? jjStartNfaWithStates_0(5, 46, 35) : jjMoveStringLiteralDfa6_0(j7, 34360262656L, j8, 0, j9, 0);
                case ParserConstants.RSIGNEDSHIFT:
                    return jjMoveStringLiteralDfa6_0(j7, 2251799813685248L, j8, 0, j9, 0);
                case ParserConstants.RSIGNEDSHIFTX:
                    return (18014398509481984L & j7) != 0 ? jjStartNfaWithStates_0(5, 54, 35) : jjMoveStringLiteralDfa6_0(j7, 0, j8, 2305843009213693952L, j9, 0);
                case ParserConstants.RUNSIGNEDSHIFT:
                    return (17179869184L & j7) != 0 ? jjStartNfaWithStates_0(5, 34, 35) : jjMoveStringLiteralDfa6_0(j7, 571746046443520L, j8, 11258999068426240L, j9, 40);
            }
            return jjStartNfa_0(4, j7, j8, j9);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(4, j7, j8, j9);
            return 5;
        }
    }

    private final int jjMoveStringLiteralDfa6_0(long j, long j2, long j3, long j4, long j5, long j6) {
        long j7 = j2 & j;
        long j8 = j4 & j3;
        long j9 = j6 & j5;
        if ((j7 | j8 | j9) == 0) {
            return jjStartNfa_0(4, j, j3, j5);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.NE:
                    return jjMoveStringLiteralDfa7_0(j7, 0, j8, 11258999068426240L, j9, 40);
                case ParserConstants.BOOL_ORX:
                    return jjMoveStringLiteralDfa7_0(j7, 137438953472L, j8, 0, j9, 0);
                case ParserConstants.BOOL_ANDX:
                    return jjMoveStringLiteralDfa7_0(j7, 34359739392L, j8, 0, j9, 0);
                case ParserConstants.DECR:
                    return (4398046511104L & j7) != 0 ? jjStartNfaWithStates_0(6, 42, 35) : (8796093022208L & j7) != 0 ? jjStartNfaWithStates_0(6, 43, 35) : jjMoveStringLiteralDfa7_0(j7, 4503608217305088L, j8, 0, j9, 0);
                case ParserConstants.PLUS:
                    return jjMoveStringLiteralDfa7_0(j7, 562949953421312L, j8, 0, j9, 0);
                case ParserConstants.BIT_OR:
                    return jjMoveStringLiteralDfa7_0(j7, 288230376151711744L, j8, 0, j9, 0);
                case ParserConstants.XOR:
                    if ((2048 & j7) != 0) {
                        return jjStartNfaWithStates_0(6, 11, 35);
                    }
                    break;
                case ParserConstants.MOD:
                    return jjMoveStringLiteralDfa7_0(j7, 2251799813685248L, j8, 0, j9, 0);
                case ParserConstants.RSIGNEDSHIFTX:
                    return (33554432 & j7) != 0 ? jjStartNfaWithStates_0(6, 25, 35) : jjMoveStringLiteralDfa7_0(j7, 0, j8, 2882910691935649792L, j9, 2);
                case ParserConstants.RUNSIGNEDSHIFT:
                    return (1048576 & j7) != 0 ? jjStartNfaWithStates_0(6, 20, 35) : jjMoveStringLiteralDfa7_0(j7, 17592186044416L, j8, 0, j9, 0);
                case ParserConstants.RUNSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa7_0(j7, 524288, j8, 0, j9, 0);
                case ParserConstants.SLASHASSIGN:
                    if ((268435456 & j7) != 0) {
                        return jjStartNfaWithStates_0(6, 28, 35);
                    }
                    break;
            }
            return jjStartNfa_0(5, j7, j8, j9);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(5, j7, j8, j9);
            return 6;
        }
    }

    private final int jjMoveStringLiteralDfa7_0(long j, long j2, long j3, long j4, long j5, long j6) {
        long j7 = j2 & j;
        long j8 = j4 & j3;
        long j9 = j6 & j5;
        if ((j7 | j8 | j9) == 0) {
            return jjStartNfa_0(5, j, j3, j5);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.BOOL_ANDX:
                    return jjMoveStringLiteralDfa8_0(j7, 137438953472L, j8, 0, j9, 0);
                case ParserConstants.DECR:
                    return (524288 & j7) != 0 ? jjStartNfaWithStates_0(7, 19, 35) : (288230376151711744L & j7) != 0 ? jjStartNfaWithStates_0(7, 58, 35) : jjMoveStringLiteralDfa8_0(j7, 17626545782784L, j8, 43980465111040L, j9, 0);
                case ParserConstants.STAR:
                    return jjMoveStringLiteralDfa8_0(j7, 0, j8, 562949953421312L, j9, 2);
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa8_0(j7, 0, j8, 2305843009213693952L, j9, 0);
                case ParserConstants.XOR:
                    return jjMoveStringLiteralDfa8_0(j7, 6755408030990336L, j8, 0, j9, 0);
                case ParserConstants.LSHIFT:
                    if ((562949953421312L & j7) != 0) {
                        return jjStartNfaWithStates_0(7, 49, 35);
                    }
                    break;
                case ParserConstants.RSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa8_0(j7, 0, j8, 578712552117108736L, j9, 8);
                case ParserConstants.RUNSIGNEDSHIFT:
                    if ((1024 & j7) != 0) {
                        return jjStartNfaWithStates_0(7, 10, 35);
                    }
                    break;
                case ParserConstants.RUNSIGNEDSHIFTX:
                    return jjMoveStringLiteralDfa8_0(j7, 0, j8, 9007199254740992L, j9, 32);
            }
            return jjStartNfa_0(6, j7, j8, j9);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(6, j7, j8, j9);
            return 7;
        }
    }

    private final int jjMoveStringLiteralDfa8_0(long j, long j2, long j3, long j4, long j5, long j6) {
        long j7 = j2 & j;
        long j8 = j4 & j3;
        long j9 = j6 & j5;
        if ((j7 | j8 | j9) == 0) {
            return jjStartNfa_0(6, j, j3, j5);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.NE:
                    return jjMoveStringLiteralDfa9_0(j7, 0, j8, 43980465111040L, j9, 0);
                case ParserConstants.INCR:
                    if ((17592186044416L & j7) != 0) {
                        return jjStartNfaWithStates_0(8, 44, 35);
                    }
                    break;
                case ParserConstants.DECR:
                    if ((137438953472L & j7) != 0) {
                        return jjStartNfaWithStates_0(8, 37, 35);
                    }
                    break;
                case ParserConstants.MINUS:
                    return jjMoveStringLiteralDfa9_0(j7, 0, j8, 2305843009213693952L, j9, 0);
                case ParserConstants.STAR:
                    return jjMoveStringLiteralDfa9_0(j7, 0, j8, 2251799813685248L, j9, 8);
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa9_0(j7, 2251799813685248L, j8, 577023702256844800L, j9, 2);
                case ParserConstants.XOR:
                    return jjMoveStringLiteralDfa9_0(j7, 0, j8, 9007199254740992L, j9, 32);
                case ParserConstants.MOD:
                    return jjMoveStringLiteralDfa9_0(j7, 34359738368L, j8, 0, j9, 0);
                case ParserConstants.RUNSIGNEDSHIFT:
                    return (4503599627370496L & j7) != 0 ? jjStartNfaWithStates_0(8, 52, 35) : jjMoveStringLiteralDfa9_0(j7, 8589934592L, j8, 0, j9, 0);
            }
            return jjStartNfa_0(7, j7, j8, j9);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(7, j7, j8, j9);
            return 8;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private final int jjMoveStringLiteralDfa9_0(long j, long j2, long j3, long j4, long j5, long j6) {
        long j7 = j2 & j;
        long j8 = j4 & j3;
        long j9 = j6 & j5;
        if ((j7 | j8 | j9) == 0) {
            return jjStartNfa_0(7, j, j3, j5);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ParserConstants.BOOL_ORX:
                    return jjMoveStringLiteralDfa10_0(j7, 0, j8, 8796093022208L, j9, 0);
                case ParserConstants.PLUS:
                    return (34359738368L & j7) != 0 ? jjStartNfaWithStates_0(9, 35, 35) : jjMoveStringLiteralDfa10_0(j7, 0, j8, 562949953421312L, j9, 2);
                case ParserConstants.MINUS:
                    return jjMoveStringLiteralDfa10_0(j7, 0, j8, 576460752303423488L, j9, 0);
                case ParserConstants.SLASH:
                    return jjMoveStringLiteralDfa10_0(j7, 0, j8, 2251799813685248L, j9, 8);
                case ParserConstants.XOR:
                    if ((2305843009213693952L & j8) != 0) {
                        return jjStopAtPos(9, ParserConstants.ORASSIGNX);
                    }
                    break;
                case ParserConstants.MOD:
                    return jjMoveStringLiteralDfa10_0(j7, 0, j8, 35184372088832L, j9, 0);
                case ParserConstants.RSIGNEDSHIFTX:
                    return (8589934592L & j7) != 0 ? jjStartNfaWithStates_0(9, 33, 35) : jjMoveStringLiteralDfa10_0(j7, 0, j8, 9007199254740992L, j9, 32);
                case ParserConstants.ANDASSIGN:
                    return jjMoveStringLiteralDfa10_0(j7, 2251799813685248L, j8, 0, j9, 0);
            }
            return jjStartNfa_0(8, j7, j8, j9);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(8, j7, j8, j9);
            return 9;
        }
    }

    private final int jjStartNfaWithStates_0(int i, int i2, int i3) {
        this.jjmatchedKind = i2;
        this.jjmatchedPos = i;
        try {
            this.curChar = this.input_stream.readChar();
            return jjMoveNfa_0(i3, i + 1);
        } catch (IOException e) {
            return i + 1;
        }
    }

    private final int jjStartNfa_0(int i, long j, long j2, long j3) {
        return jjMoveNfa_0(jjStopStringLiteralDfa_0(i, j, j2, j3), i + 1);
    }

    private final int jjStopAtPos(int i, int i2) {
        this.jjmatchedKind = i2;
        this.jjmatchedPos = i;
        return i + 1;
    }

    private final int jjStopStringLiteralDfa_0(int i, long j, long j2, long j3) {
        switch (i) {
            case 0:
                if ((144117387099111424L & j2) != 0) {
                    return 56;
                }
                if ((62 & j) != 0) {
                    return 0;
                }
                if ((65536 & j2) != 0) {
                    return 11;
                }
                if ((1152921504606845952L & j) == 0) {
                    return -1;
                }
                this.jjmatchedKind = 69;
                return 35;
            case 1:
                if ((4301258752L & j) != 0) {
                    return 35;
                }
                if ((1152921500305587200L & j) == 0) {
                    return -1;
                }
                if (this.jjmatchedPos == 1) {
                    return 35;
                }
                this.jjmatchedKind = 69;
                this.jjmatchedPos = 1;
                return 35;
            case 2:
                if ((1080862599528053760L & j) == 0) {
                    return (72058900781727744L & j) == 0 ? -1 : 35;
                }
                if (this.jjmatchedPos == 2) {
                    return 35;
                }
                this.jjmatchedKind = 69;
                this.jjmatchedPos = 2;
                return 35;
            case 3:
                if ((900716275798195200L & j) == 0) {
                    return (180146461168812032L & j) == 0 ? -1 : 35;
                }
                if (this.jjmatchedPos == 3) {
                    return 35;
                }
                this.jjmatchedKind = 69;
                this.jjmatchedPos = 3;
                return 35;
            case ParserTreeConstants.JJTIMPORTDECLARATION /*4*/:
                if ((603623088562974720L & j) != 0) {
                    return 35;
                }
                if ((297093187235220480L & j) == 0) {
                    return -1;
                }
                if (this.jjmatchedPos == 4) {
                    return 35;
                }
                this.jjmatchedKind = 69;
                this.jjmatchedPos = 4;
                return 35;
            case ParserTreeConstants.JJTVARIABLEDECLARATOR /*5*/:
                if ((295579692563958784L & j) == 0) {
                    return (19527893449179136L & j) == 0 ? -1 : 35;
                }
                this.jjmatchedKind = 69;
                this.jjmatchedPos = 5;
                return 35;
            case 6:
                if ((295566498121384960L & j) == 0) {
                    return (13194442573824L & j) == 0 ? -1 : 35;
                }
                this.jjmatchedKind = 69;
                this.jjmatchedPos = 6;
                return 35;
            case 7:
                if ((288793326105658368L & j) != 0) {
                    return 35;
                }
                if ((6773172015726592L & j) == 0) {
                    return -1;
                }
                this.jjmatchedKind = 69;
                this.jjmatchedPos = 7;
                return 35;
            case 8:
                if ((2251842763358208L & j) == 0) {
                    return (4521329252368384L & j) == 0 ? -1 : 35;
                }
                this.jjmatchedKind = 69;
                this.jjmatchedPos = 8;
                return 35;
            case 9:
                if ((2251799813685248L & j) == 0) {
                    return (42949672960L & j) == 0 ? -1 : 35;
                }
                this.jjmatchedKind = 69;
                this.jjmatchedPos = 9;
                return 35;
            case 10:
                if ((2251799813685248L & j) == 0) {
                    return -1;
                }
                if (this.jjmatchedPos == 10) {
                    return 35;
                }
                this.jjmatchedKind = 69;
                this.jjmatchedPos = 10;
                return 35;
            case 11:
                return (2251799813685248L & j) == 0 ? -1 : 35;
            default:
                return -1;
        }
    }

    public void ReInit(JavaCharStream javaCharStream) {
        this.jjnewStateCnt = 0;
        this.jjmatchedPos = 0;
        this.curLexState = this.defaultLexState;
        this.input_stream = javaCharStream;
        ReInitRounds();
    }

    public void ReInit(JavaCharStream javaCharStream, int i) {
        ReInit(javaCharStream);
        SwitchTo(i);
    }

    public void SwitchTo(int i) {
        if (i > 0 || i < 0) {
            throw new TokenMgrError("Error: Ignoring invalid lexical state : " + i + ". State unchanged.", 2);
        }
        this.curLexState = i;
    }

    public Token getNextToken() {
        boolean z;
        String str;
        Token token = null;
        while (true) {
            try {
                this.curChar = this.input_stream.BeginToken();
                this.jjmatchedKind = Integer.MAX_VALUE;
                this.jjmatchedPos = 0;
                int jjMoveStringLiteralDfa0_0 = jjMoveStringLiteralDfa0_0();
                if (this.jjmatchedKind != Integer.MAX_VALUE) {
                    if (this.jjmatchedPos + 1 < jjMoveStringLiteralDfa0_0) {
                        this.input_stream.backup((jjMoveStringLiteralDfa0_0 - this.jjmatchedPos) - 1);
                    }
                    if ((jjtoToken[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                        Token jjFillToken = jjFillToken();
                        jjFillToken.specialToken = token;
                        return jjFillToken;
                    } else if ((jjtoSpecial[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                        Token jjFillToken2 = jjFillToken();
                        if (token == null) {
                            token = jjFillToken2;
                        } else {
                            jjFillToken2.specialToken = token;
                            token.next = jjFillToken2;
                            token = jjFillToken2;
                        }
                    }
                } else {
                    int endLine = this.input_stream.getEndLine();
                    int endColumn = this.input_stream.getEndColumn();
                    try {
                        this.input_stream.readChar();
                        this.input_stream.backup(1);
                        z = false;
                        str = null;
                    } catch (IOException e) {
                        String GetImage = jjMoveStringLiteralDfa0_0 <= 1 ? "" : this.input_stream.GetImage();
                        if (this.curChar == 10 || this.curChar == 13) {
                            endLine++;
                            z = true;
                            str = GetImage;
                            endColumn = 0;
                        } else {
                            endColumn++;
                            z = true;
                            str = GetImage;
                        }
                    }
                    if (!z) {
                        this.input_stream.backup(1);
                        str = jjMoveStringLiteralDfa0_0 <= 1 ? "" : this.input_stream.GetImage();
                    }
                    throw new TokenMgrError(z, this.curLexState, endLine, endColumn, str, this.curChar, 0);
                }
            } catch (IOException e2) {
                this.jjmatchedKind = 0;
                Token jjFillToken3 = jjFillToken();
                jjFillToken3.specialToken = token;
                return jjFillToken3;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Token jjFillToken() {
        Token newToken = Token.newToken(this.jjmatchedKind);
        newToken.kind = this.jjmatchedKind;
        String str = jjstrLiteralImages[this.jjmatchedKind];
        if (str == null) {
            str = this.input_stream.GetImage();
        }
        newToken.image = str;
        newToken.beginLine = this.input_stream.getBeginLine();
        newToken.beginColumn = this.input_stream.getBeginColumn();
        newToken.endLine = this.input_stream.getEndLine();
        newToken.endColumn = this.input_stream.getEndColumn();
        return newToken;
    }

    public void setDebugStream(PrintStream printStream) {
        this.debugStream = printStream;
    }
}
