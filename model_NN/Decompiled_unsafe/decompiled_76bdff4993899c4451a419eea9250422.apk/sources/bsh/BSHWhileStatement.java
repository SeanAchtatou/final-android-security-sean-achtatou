package bsh;

class BSHWhileStatement extends SimpleNode implements ParserConstants {
    public boolean isDoStatement;

    BSHWhileStatement(int i) {
        super(i);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object eval(bsh.CallStack r8, bsh.Interpreter r9) {
        /*
            r7 = this;
            r4 = 1
            r5 = 0
            int r2 = r7.jjtGetNumChildren()
            r1 = 0
            boolean r0 = r7.isDoStatement
            if (r0 == 0) goto L_0x003b
            bsh.Node r0 = r7.jjtGetChild(r4)
            bsh.SimpleNode r0 = (bsh.SimpleNode) r0
            bsh.Node r1 = r7.jjtGetChild(r5)
            bsh.SimpleNode r1 = (bsh.SimpleNode) r1
            r2 = r1
            r3 = r0
        L_0x0019:
            boolean r0 = r7.isDoStatement
            r6 = r0
        L_0x001c:
            if (r6 != 0) goto L_0x0024
            boolean r0 = bsh.BSHIfStatement.evaluateCondition(r3, r8, r9)
            if (r0 == 0) goto L_0x004e
        L_0x0024:
            if (r2 == 0) goto L_0x001c
            java.lang.Object r1 = r2.eval(r8, r9)
            boolean r0 = r1 instanceof bsh.ReturnControl
            if (r0 == 0) goto L_0x0036
            r0 = r1
            bsh.ReturnControl r0 = (bsh.ReturnControl) r0
            int r0 = r0.kind
            switch(r0) {
                case 12: goto L_0x004c;
                case 19: goto L_0x001c;
                case 46: goto L_0x0050;
                default: goto L_0x0036;
            }
        L_0x0036:
            r0 = r5
        L_0x0037:
            if (r0 != 0) goto L_0x004e
            r6 = r5
            goto L_0x001c
        L_0x003b:
            bsh.Node r0 = r7.jjtGetChild(r5)
            bsh.SimpleNode r0 = (bsh.SimpleNode) r0
            if (r2 <= r4) goto L_0x0051
            bsh.Node r1 = r7.jjtGetChild(r4)
            bsh.SimpleNode r1 = (bsh.SimpleNode) r1
            r2 = r1
            r3 = r0
            goto L_0x0019
        L_0x004c:
            r0 = r4
            goto L_0x0037
        L_0x004e:
            bsh.Primitive r1 = bsh.Primitive.VOID
        L_0x0050:
            return r1
        L_0x0051:
            r2 = r1
            r3 = r0
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.BSHWhileStatement.eval(bsh.CallStack, bsh.Interpreter):java.lang.Object");
    }
}
