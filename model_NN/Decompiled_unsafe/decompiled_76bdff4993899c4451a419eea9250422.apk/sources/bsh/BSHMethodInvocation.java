package bsh;

import java.lang.reflect.InvocationTargetException;

class BSHMethodInvocation extends SimpleNode {
    BSHMethodInvocation(int i) {
        super(i);
    }

    public Object eval(CallStack callStack, Interpreter interpreter) {
        NameSpace pVar = callStack.top();
        BSHAmbiguousName nameNode = getNameNode();
        if (pVar.getParent() != null && pVar.getParent().isClass && (nameNode.text.equals("super") || nameNode.text.equals("this"))) {
            return Primitive.VOID;
        }
        Name name = nameNode.getName(pVar);
        try {
            return name.invokeMethod(interpreter, getArgsNode().getArguments(callStack, interpreter), callStack, this);
        } catch (ReflectError e) {
            throw new EvalError("Error in method invocation: " + e.getMessage(), this, callStack);
        } catch (InvocationTargetException e2) {
            String str = "Method Invocation " + name;
            Throwable targetException = e2.getTargetException();
            boolean z = true;
            if (targetException instanceof EvalError) {
                z = targetException instanceof TargetError ? ((TargetError) targetException).inNativeCode() : false;
            }
            throw new TargetError(str, targetException, this, callStack, z);
        } catch (UtilEvalError e3) {
            throw e3.toEvalError(this, callStack);
        }
    }

    /* access modifiers changed from: package-private */
    public BSHArguments getArgsNode() {
        return (BSHArguments) jjtGetChild(1);
    }

    /* access modifiers changed from: package-private */
    public BSHAmbiguousName getNameNode() {
        return (BSHAmbiguousName) jjtGetChild(0);
    }
}
