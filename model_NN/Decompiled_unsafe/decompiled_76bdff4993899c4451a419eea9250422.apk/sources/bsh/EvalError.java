package bsh;

public class EvalError extends Exception {
    CallStack callstack;
    String message;
    SimpleNode node;

    public EvalError(String str, SimpleNode simpleNode, CallStack callStack) {
        setMessage(str);
        this.node = simpleNode;
        if (callStack != null) {
            this.callstack = callStack.copy();
        }
    }

    public int getErrorLineNumber() {
        if (this.node != null) {
            return this.node.getLineNumber();
        }
        return -1;
    }

    public String getErrorSourceFile() {
        return this.node != null ? this.node.getSourceFile() : "<unknown file>";
    }

    public String getErrorText() {
        return this.node != null ? this.node.getText() : "<unknown error>";
    }

    public String getMessage() {
        return this.message;
    }

    /* access modifiers changed from: package-private */
    public SimpleNode getNode() {
        return this.node;
    }

    public String getScriptStackTrace() {
        if (this.callstack == null) {
            return "<Unknown>";
        }
        String str = "";
        CallStack copy = this.callstack.copy();
        while (copy.depth() > 0) {
            NameSpace pop = copy.pop();
            SimpleNode node2 = pop.getNode();
            if (pop.isMethod) {
                str = str + "\nCalled from method: " + pop.getName();
                if (node2 != null) {
                    str = str + " : at Line: " + node2.getLineNumber() + " : in file: " + node2.getSourceFile() + " : " + node2.getText();
                }
            }
        }
        return str;
    }

    /* access modifiers changed from: protected */
    public void prependMessage(String str) {
        if (str != null) {
            if (this.message == null) {
                this.message = str;
            } else {
                this.message = str + " : " + this.message;
            }
        }
    }

    public void reThrow(String str) {
        prependMessage(str);
        throw this;
    }

    public void setMessage(String str) {
        this.message = str;
    }

    /* access modifiers changed from: package-private */
    public void setNode(SimpleNode simpleNode) {
        this.node = simpleNode;
    }

    public String toString() {
        String str = this.node != null ? " : at Line: " + this.node.getLineNumber() + " : in file: " + this.node.getSourceFile() + " : " + this.node.getText() : ": <at unknown location>";
        if (this.callstack != null) {
            str = str + "\n" + getScriptStackTrace();
        }
        return getMessage() + str;
    }
}
