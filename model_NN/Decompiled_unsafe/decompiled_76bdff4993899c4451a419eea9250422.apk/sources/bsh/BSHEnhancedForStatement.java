package bsh;

class BSHEnhancedForStatement extends SimpleNode implements ParserConstants {
    String varName;

    BSHEnhancedForStatement(int i) {
        super(i);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a6 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object eval(bsh.CallStack r12, bsh.Interpreter r13) {
        /*
            r11 = this;
            r2 = 0
            r4 = 2
            r6 = 0
            r5 = 1
            bsh.NameSpace r7 = r12.top()
            bsh.Node r0 = r11.jjtGetChild(r6)
            bsh.SimpleNode r0 = (bsh.SimpleNode) r0
            int r1 = r11.jjtGetNumChildren()
            boolean r3 = r0 instanceof bsh.BSHType
            if (r3 == 0) goto L_0x0043
            bsh.BSHType r0 = (bsh.BSHType) r0
            java.lang.Class r3 = r0.getType(r12, r13)
            bsh.Node r0 = r11.jjtGetChild(r5)
            bsh.SimpleNode r0 = (bsh.SimpleNode) r0
            if (r1 <= r4) goto L_0x002b
            bsh.Node r1 = r11.jjtGetChild(r4)
            bsh.SimpleNode r1 = (bsh.SimpleNode) r1
            r2 = r1
        L_0x002b:
            bsh.BlockNameSpace r8 = new bsh.BlockNameSpace
            r8.<init>(r7)
            r12.swap(r8)
            java.lang.Object r0 = r0.eval(r12, r13)
            bsh.Primitive r1 = bsh.Primitive.NULL
            if (r0 != r1) goto L_0x004e
            bsh.EvalError r0 = new bsh.EvalError
            java.lang.String r1 = "The collection, array, map, iterator, or enumeration portion of a for statement cannot be null."
            r0.<init>(r1, r11, r12)
            throw r0
        L_0x0043:
            if (r1 <= r5) goto L_0x00d6
            bsh.Node r1 = r11.jjtGetChild(r5)
            bsh.SimpleNode r1 = (bsh.SimpleNode) r1
            r3 = r2
            r2 = r1
            goto L_0x002b
        L_0x004e:
            bsh.CollectionManager r1 = bsh.CollectionManager.getCollectionManager()
            boolean r4 = r1.isBshIterable(r0)
            if (r4 != 0) goto L_0x0075
            bsh.EvalError r1 = new bsh.EvalError
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Can't iterate over type: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.Class r0 = r0.getClass()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0, r11, r12)
            throw r1
        L_0x0075:
            bsh.BshIterator r9 = r1.getBshIterator(r0)
            bsh.Primitive r4 = bsh.Primitive.VOID
        L_0x007b:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x00a6
            if (r3 == 0) goto L_0x00aa
            java.lang.String r0 = r11.varName     // Catch:{ UtilEvalError -> 0x00b5 }
            java.lang.Object r1 = r9.next()     // Catch:{ UtilEvalError -> 0x00b5 }
            bsh.Modifiers r10 = new bsh.Modifiers     // Catch:{ UtilEvalError -> 0x00b5 }
            r10.<init>()     // Catch:{ UtilEvalError -> 0x00b5 }
            r8.setTypedVariable(r0, r3, r1, r10)     // Catch:{ UtilEvalError -> 0x00b5 }
        L_0x0091:
            if (r2 == 0) goto L_0x00a3
            java.lang.Object r1 = r2.eval(r12, r13)
            boolean r0 = r1 instanceof bsh.ReturnControl
            if (r0 == 0) goto L_0x00a3
            r0 = r1
            bsh.ReturnControl r0 = (bsh.ReturnControl) r0
            int r0 = r0.kind
            switch(r0) {
                case 12: goto L_0x00d2;
                case 19: goto L_0x00d0;
                case 46: goto L_0x00d3;
                default: goto L_0x00a3;
            }
        L_0x00a3:
            r0 = r6
        L_0x00a4:
            if (r0 == 0) goto L_0x007b
        L_0x00a6:
            r12.swap(r7)
            return r4
        L_0x00aa:
            java.lang.String r0 = r11.varName     // Catch:{ UtilEvalError -> 0x00b5 }
            java.lang.Object r1 = r9.next()     // Catch:{ UtilEvalError -> 0x00b5 }
            r10 = 0
            r8.setVariable(r0, r1, r10)     // Catch:{ UtilEvalError -> 0x00b5 }
            goto L_0x0091
        L_0x00b5:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "for loop iterator variable:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r11.varName
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            bsh.EvalError r0 = r0.toEvalError(r1, r11, r12)
            throw r0
        L_0x00d0:
            r0 = r6
            goto L_0x00a4
        L_0x00d2:
            r1 = r4
        L_0x00d3:
            r0 = r5
            r4 = r1
            goto L_0x00a4
        L_0x00d6:
            r3 = r2
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.BSHEnhancedForStatement.eval(bsh.CallStack, bsh.Interpreter):java.lang.Object");
    }
}
