package bsh;

import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Vector;

class Reflect {
    Reflect() {
    }

    static String accessorName(String str, String str2) {
        return str + String.valueOf(Character.toUpperCase(str2.charAt(0))) + str2.substring(1);
    }

    private static Vector addCandidates(Method[] methodArr, String str, int i, boolean z, Vector vector) {
        for (Method method : methodArr) {
            if (method.getName().equals(str) && method.getParameterTypes().length == i && (!z || isPublic(method))) {
                vector.add(method);
            }
        }
        return vector;
    }

    private static ReflectError cantFindConstructor(Class cls, Class[] clsArr) {
        return clsArr.length == 0 ? new ReflectError("Can't find default constructor for: " + cls) : new ReflectError("Can't find constructor: " + StringUtil.methodString(cls.getName(), clsArr) + " in class: " + cls.getName());
    }

    private static void checkFoundStaticMethod(Method method, boolean z, Class cls) {
        if (method != null && z && !isStatic(method)) {
            throw new UtilEvalError("Cannot reach instance method: " + StringUtil.methodString(method.getName(), method.getParameterTypes()) + " from static context: " + cls.getName());
        }
    }

    static Object constructObject(Class cls, Object[] objArr) {
        if (cls.isInterface()) {
            throw new ReflectError("Can't create instance of an interface: " + cls);
        }
        Class[] types = Types.getTypes(objArr);
        Constructor<?>[] declaredConstructors = Capabilities.haveAccessibility() ? cls.getDeclaredConstructors() : cls.getConstructors();
        if (Interpreter.DEBUG) {
            Interpreter.debug("Looking for most specific constructor: " + cls);
        }
        Constructor findMostSpecificConstructor = findMostSpecificConstructor(types, declaredConstructors);
        if (findMostSpecificConstructor == null) {
            throw cantFindConstructor(cls, types);
        }
        if (!isPublic(findMostSpecificConstructor)) {
            try {
                ReflectManager.RMSetAccessible(findMostSpecificConstructor);
            } catch (UtilEvalError e) {
            }
        }
        try {
            Object newInstance = findMostSpecificConstructor.newInstance(Primitive.unwrap(objArr));
            if (newInstance != null) {
                return newInstance;
            }
            throw new ReflectError("Couldn't construct the object");
        } catch (InstantiationException e2) {
            throw new ReflectError("The class " + cls + " is abstract ");
        } catch (IllegalAccessException e3) {
            throw new ReflectError("We don't have permission to create an instance.Use setAccessibility(true) to enable access.");
        } catch (IllegalArgumentException e4) {
            throw new ReflectError("The number of arguments was wrong");
        }
    }

    private static Field findAccessibleField(Class cls, String str) {
        try {
            Field field = cls.getField(str);
            ReflectManager.RMSetAccessible(field);
            return field;
        } catch (NoSuchFieldException e) {
            while (cls != null) {
                try {
                    Field declaredField = cls.getDeclaredField(str);
                    ReflectManager.RMSetAccessible(declaredField);
                    return declaredField;
                } catch (NoSuchFieldException e2) {
                    cls = cls.getSuperclass();
                }
            }
            throw new NoSuchFieldException(str);
        }
    }

    static Constructor findMostSpecificConstructor(Class[] clsArr, Constructor[] constructorArr) {
        int findMostSpecificConstructorIndex = findMostSpecificConstructorIndex(clsArr, constructorArr);
        if (findMostSpecificConstructorIndex == -1) {
            return null;
        }
        return constructorArr[findMostSpecificConstructorIndex];
    }

    static int findMostSpecificConstructorIndex(Class[] clsArr, Constructor[] constructorArr) {
        Class[][] clsArr2 = new Class[constructorArr.length][];
        for (int i = 0; i < clsArr2.length; i++) {
            clsArr2[i] = constructorArr[i].getParameterTypes();
        }
        return findMostSpecificSignature(clsArr, clsArr2);
    }

    static Method findMostSpecificMethod(Class[] clsArr, Method[] methodArr) {
        Class[][] clsArr2 = new Class[methodArr.length][];
        for (int i = 0; i < methodArr.length; i++) {
            clsArr2[i] = methodArr[i].getParameterTypes();
        }
        int findMostSpecificSignature = findMostSpecificSignature(clsArr, clsArr2);
        if (findMostSpecificSignature == -1) {
            return null;
        }
        return methodArr[findMostSpecificSignature];
    }

    static int findMostSpecificSignature(Class[] clsArr, Class[][] clsArr2) {
        for (int i = 1; i <= 4; i++) {
            Class[] clsArr3 = null;
            int i2 = -1;
            for (int i3 = 0; i3 < clsArr2.length; i3++) {
                Class[] clsArr4 = clsArr2[i3];
                if (Types.isSignatureAssignable(clsArr, clsArr4, i) && (clsArr3 == null || (Types.isSignatureAssignable(clsArr4, clsArr3, 1) && !Types.areSignaturesEqual(clsArr4, clsArr3)))) {
                    i2 = i3;
                    clsArr3 = clsArr4;
                }
            }
            if (clsArr3 != null) {
                return i2;
            }
        }
        return -1;
    }

    private static Method findOverloadedMethod(Class cls, String str, Class[] clsArr, boolean z) {
        if (Interpreter.DEBUG) {
            Interpreter.debug("Searching for method: " + StringUtil.methodString(str, clsArr) + " in '" + cls.getName() + "'");
        }
        Method[] candidateMethods = getCandidateMethods(cls, str, clsArr.length, z);
        if (Interpreter.DEBUG) {
            Interpreter.debug("Looking for most specific method: " + str);
        }
        return findMostSpecificMethod(clsArr, candidateMethods);
    }

    private static Vector gatherMethodsRecursive(Class cls, String str, int i, boolean z, Vector vector) {
        if (vector == null) {
            vector = new Vector();
        }
        if (!z) {
            addCandidates(cls.getDeclaredMethods(), str, i, z, vector);
        } else if (isPublic(cls)) {
            addCandidates(cls.getMethods(), str, i, z, vector);
        }
        Class<?>[] interfaces = cls.getInterfaces();
        for (Class<?> gatherMethodsRecursive : interfaces) {
            gatherMethodsRecursive(gatherMethodsRecursive, str, i, z, vector);
        }
        Class superclass = cls.getSuperclass();
        if (superclass != null) {
            gatherMethodsRecursive(superclass, str, i, z, vector);
        }
        return vector;
    }

    public static Class getArrayBaseType(Class cls) {
        if (cls.isArray()) {
            return cls.getComponentType();
        }
        throw new ReflectError("The class is not an array.");
    }

    public static int getArrayDimensions(Class cls) {
        if (!cls.isArray()) {
            return 0;
        }
        return cls.getName().lastIndexOf(91) + 1;
    }

    static Method[] getCandidateMethods(Class cls, String str, int i, boolean z) {
        Vector gatherMethodsRecursive = gatherMethodsRecursive(cls, str, i, z, null);
        Method[] methodArr = new Method[gatherMethodsRecursive.size()];
        gatherMethodsRecursive.copyInto(methodArr);
        return methodArr;
    }

    private static Object getFieldValue(Class cls, Object obj, String str, boolean z) {
        try {
            Field resolveExpectedJavaField = resolveExpectedJavaField(cls, str, z);
            return Primitive.wrap(resolveExpectedJavaField.get(obj), resolveExpectedJavaField.getType());
        } catch (NullPointerException e) {
            throw new ReflectError("???" + str + " is not a static field.");
        } catch (IllegalAccessException e2) {
            throw new ReflectError("Can't access field: " + str);
        }
    }

    public static Object getIndex(Object obj, int i) {
        if (Interpreter.DEBUG) {
            Interpreter.debug("getIndex: " + obj + ", index=" + i);
        }
        try {
            return Primitive.wrap(Array.get(obj, i), obj.getClass().getComponentType());
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new UtilTargetError(e);
        } catch (Exception e2) {
            throw new ReflectError("Array access:" + e2);
        }
    }

    static LHS getLHSObjectField(Object obj, String str) {
        if (obj instanceof This) {
            return new LHS(((This) obj).namespace, str, false);
        }
        try {
            return new LHS(obj, resolveExpectedJavaField(obj.getClass(), str, false));
        } catch (ReflectError e) {
            if (hasObjectPropertySetter(obj.getClass(), str)) {
                return new LHS(obj, str);
            }
            throw e;
        }
    }

    static LHS getLHSStaticField(Class cls, String str) {
        return new LHS(resolveExpectedJavaField(cls, str, true));
    }

    public static Object getObjectFieldValue(Object obj, String str) {
        if (obj instanceof This) {
            return ((This) obj).namespace.getVariableOrProperty(str, null);
        }
        try {
            return getFieldValue(obj.getClass(), obj, str, false);
        } catch (ReflectError e) {
            if (hasObjectPropertyGetter(obj.getClass(), str)) {
                return getObjectProperty(obj, str);
            }
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0077 A[SYNTHETIC, Splitter:B:24:0x0077] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object getObjectProperty(java.lang.Object r10, java.lang.String r11) {
        /*
            r0 = 0
            r6 = 0
            java.lang.Object[] r4 = new java.lang.Object[r0]
            java.lang.String r0 = "property access: "
            bsh.Interpreter.debug(r0)
            java.lang.String r0 = "get"
            java.lang.String r3 = accessorName(r0, r11)     // Catch:{ Exception -> 0x006b }
            r0 = 0
            java.lang.Class r1 = r10.getClass()     // Catch:{ Exception -> 0x006b }
            r5 = 0
            r2 = r10
            java.lang.reflect.Method r7 = resolveExpectedJavaMethod(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x006b }
            r8 = r6
        L_0x001b:
            if (r7 != 0) goto L_0x009f
            java.lang.String r0 = "is"
            java.lang.String r3 = accessorName(r0, r11)     // Catch:{ Exception -> 0x006f }
            r0 = 0
            java.lang.Class r1 = r10.getClass()     // Catch:{ Exception -> 0x006f }
            r5 = 0
            r2 = r10
            java.lang.reflect.Method r0 = resolveExpectedJavaMethod(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x006f }
            java.lang.Class r1 = r0.getReturnType()     // Catch:{ Exception -> 0x009a }
            java.lang.Class r2 = java.lang.Boolean.TYPE     // Catch:{ Exception -> 0x009a }
            if (r1 == r2) goto L_0x0037
            r0 = r6
        L_0x0037:
            if (r0 != 0) goto L_0x0077
            bsh.ReflectError r1 = new bsh.ReflectError
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "Error in property getter: "
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r2 = r0.append(r8)
            if (r6 == 0) goto L_0x0074
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = " : "
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r0 = r0.toString()
        L_0x005f:
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x006b:
            r0 = move-exception
            r8 = r0
            r7 = r6
            goto L_0x001b
        L_0x006f:
            r0 = move-exception
            r1 = r7
        L_0x0071:
            r6 = r0
            r0 = r1
            goto L_0x0037
        L_0x0074:
            java.lang.String r0 = ""
            goto L_0x005f
        L_0x0077:
            java.lang.Object r0 = invokeMethod(r0, r10, r4)     // Catch:{ InvocationTargetException -> 0x007c }
            return r0
        L_0x007c:
            r0 = move-exception
            bsh.UtilEvalError r1 = new bsh.UtilEvalError
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Property accessor threw exception: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.Throwable r0 = r0.getTargetException()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x009a:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0071
        L_0x009f:
            r0 = r7
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Reflect.getObjectProperty(java.lang.Object, java.lang.String):java.lang.Object");
    }

    public static Object getStaticFieldValue(Class cls, String str) {
        return getFieldValue(cls, null, str, true);
    }

    public static boolean hasObjectPropertyGetter(Class cls, String str) {
        try {
            cls.getMethod(accessorName("get", str), new Class[0]);
            return true;
        } catch (NoSuchMethodException e) {
            try {
                return cls.getMethod(accessorName("is", str), new Class[0]).getReturnType() == Boolean.TYPE;
            } catch (NoSuchMethodException e2) {
                return false;
            }
        }
    }

    public static boolean hasObjectPropertySetter(Class cls, String str) {
        String accessorName = accessorName("set", str);
        Method[] methods = cls.getMethods();
        for (Method name : methods) {
            if (name.getName().equals(accessorName)) {
                return true;
            }
        }
        return false;
    }

    public static Object invokeCompiledCommand(Class cls, Object[] objArr, Interpreter interpreter, CallStack callStack) {
        Object[] objArr2 = new Object[(objArr.length + 2)];
        objArr2[0] = interpreter;
        objArr2[1] = callStack;
        System.arraycopy(objArr, 0, objArr2, 2, objArr.length);
        try {
            return invokeStaticMethod(interpreter.getClassManager(), cls, "invoke", objArr2);
        } catch (InvocationTargetException e) {
            throw new UtilEvalError("Error in compiled command: " + e.getTargetException());
        } catch (ReflectError e2) {
            throw new UtilEvalError("Error invoking compiled command: " + e2);
        }
    }

    static Object invokeMethod(Method method, Object obj, Object[] objArr) {
        int i = 0;
        if (objArr == null) {
            objArr = new Object[0];
        }
        logInvokeMethod("Invoking method (entry): ", method, objArr);
        Object[] objArr2 = new Object[objArr.length];
        Class<?>[] parameterTypes = method.getParameterTypes();
        while (i < objArr.length) {
            try {
                objArr2[i] = Types.castObject(objArr[i], parameterTypes[i], 1);
                i++;
            } catch (UtilEvalError e) {
                throw new InterpreterError("illegal argument type in method invocation: " + e);
            }
        }
        Object[] unwrap = Primitive.unwrap(objArr2);
        logInvokeMethod("Invoking method (after massaging values): ", method, unwrap);
        try {
            Object invoke = method.invoke(obj, unwrap);
            if (invoke == null) {
                invoke = Primitive.NULL;
            }
            return Primitive.wrap(invoke, method.getReturnType());
        } catch (IllegalAccessException e2) {
            throw new ReflectError("Cannot access method " + StringUtil.methodString(method.getName(), method.getParameterTypes()) + " in '" + method.getDeclaringClass() + "' :" + e2);
        }
    }

    public static Object invokeObjectMethod(Object obj, String str, Object[] objArr, Interpreter interpreter, CallStack callStack, SimpleNode simpleNode) {
        if ((obj instanceof This) && !This.isExposedThisMethod(str)) {
            return ((This) obj).invokeMethod(str, objArr, interpreter, callStack, simpleNode, false);
        }
        try {
            return invokeMethod(resolveExpectedJavaMethod(interpreter == null ? null : interpreter.getClassManager(), obj.getClass(), obj, str, objArr, false), obj, objArr);
        } catch (UtilEvalError e) {
            throw e.toEvalError(simpleNode, callStack);
        }
    }

    public static Object invokeStaticMethod(BshClassManager bshClassManager, Class cls, String str, Object[] objArr) {
        Interpreter.debug("invoke static Method");
        return invokeMethod(resolveExpectedJavaMethod(bshClassManager, cls, null, str, objArr, true), null, objArr);
    }

    private static boolean isPublic(Class cls) {
        return Modifier.isPublic(cls.getModifiers());
    }

    private static boolean isPublic(Constructor constructor) {
        return Modifier.isPublic(constructor.getModifiers());
    }

    private static boolean isPublic(Method method) {
        return Modifier.isPublic(method.getModifiers());
    }

    private static boolean isStatic(Method method) {
        return Modifier.isStatic(method.getModifiers());
    }

    private static void logInvokeMethod(String str, Method method, Object[] objArr) {
        if (Interpreter.DEBUG) {
            Interpreter.debug(str + method + " with args:");
            for (int i = 0; i < objArr.length; i++) {
                Interpreter.debug("args[" + i + "] = " + objArr[i] + " type = " + objArr[i].getClass());
            }
        }
    }

    public static String normalizeClassName(Class cls) {
        if (!cls.isArray()) {
            return cls.getName();
        }
        StringBuffer stringBuffer = new StringBuffer();
        try {
            stringBuffer.append(getArrayBaseType(cls).getName() + " ");
            for (int i = 0; i < getArrayDimensions(cls); i++) {
                stringBuffer.append("[]");
            }
        } catch (ReflectError e) {
        }
        return stringBuffer.toString();
    }

    protected static Field resolveExpectedJavaField(Class cls, String str, boolean z) {
        try {
            Field findAccessibleField = Capabilities.haveAccessibility() ? findAccessibleField(cls, str) : cls.getField(str);
            if (!z || Modifier.isStatic(findAccessibleField.getModifiers())) {
                return findAccessibleField;
            }
            throw new UtilEvalError("Can't reach instance field: " + str + " from static context: " + cls.getName());
        } catch (NoSuchFieldException e) {
            throw new ReflectError("No such field: " + str);
        } catch (SecurityException e2) {
            throw new UtilTargetError("Security Exception while searching fields of: " + cls, e2);
        }
    }

    protected static Method resolveExpectedJavaMethod(BshClassManager bshClassManager, Class cls, Object obj, String str, Object[] objArr, boolean z) {
        if (obj == Primitive.NULL) {
            throw new UtilTargetError(new NullPointerException("Attempt to invoke method " + str + " on null value"));
        }
        Class[] types = Types.getTypes(objArr);
        Method resolveJavaMethod = resolveJavaMethod(bshClassManager, cls, str, types, z);
        if (resolveJavaMethod != null) {
            return resolveJavaMethod;
        }
        throw new ReflectError((z ? "Static method " : "Method ") + StringUtil.methodString(str, types) + " not found in class'" + cls.getName() + "'");
    }

    protected static Field resolveJavaField(Class cls, String str, boolean z) {
        try {
            return resolveExpectedJavaField(cls, str, z);
        } catch (ReflectError e) {
            return null;
        }
    }

    protected static Method resolveJavaMethod(BshClassManager bshClassManager, Class cls, String str, Class[] clsArr, boolean z) {
        if (cls == null) {
            throw new InterpreterError("null class");
        }
        Method method = null;
        if (bshClassManager == null) {
            Interpreter.debug("resolveJavaMethod UNOPTIMIZED lookup");
        } else {
            method = bshClassManager.getResolvedMethod(cls, str, clsArr, z);
        }
        if (method != null) {
            return method;
        }
        boolean z2 = !Capabilities.haveAccessibility();
        try {
            Method findOverloadedMethod = findOverloadedMethod(cls, str, clsArr, z2);
            checkFoundStaticMethod(findOverloadedMethod, z, cls);
            if (findOverloadedMethod != null && !z2) {
                try {
                    ReflectManager.RMSetAccessible(findOverloadedMethod);
                } catch (UtilEvalError e) {
                }
            }
            if (!(findOverloadedMethod == null || bshClassManager == null)) {
                bshClassManager.cacheResolvedMethod(cls, clsArr, findOverloadedMethod);
            }
            return findOverloadedMethod;
        } catch (SecurityException e2) {
            throw new UtilTargetError("Security Exception while searching methods of: " + cls, e2);
        }
    }

    public static void setIndex(Object obj, int i, Object obj2) {
        try {
            Array.set(obj, i, Primitive.unwrap(obj2));
        } catch (ArrayStoreException e) {
            throw new UtilTargetError(e);
        } catch (IllegalArgumentException e2) {
            throw new UtilTargetError(new ArrayStoreException(e2.toString()));
        } catch (Exception e3) {
            throw new ReflectError("Array access:" + e3);
        }
    }

    public static void setObjectProperty(Object obj, String str, Object obj2) {
        String accessorName = accessorName("set", str);
        Object[] objArr = {obj2};
        Interpreter.debug("property access: ");
        try {
            invokeMethod(resolveExpectedJavaMethod(null, obj.getClass(), obj, accessorName, objArr, false), obj, objArr);
        } catch (InvocationTargetException e) {
            throw new UtilEvalError("Property accessor threw exception: " + e.getTargetException());
        }
    }
}
