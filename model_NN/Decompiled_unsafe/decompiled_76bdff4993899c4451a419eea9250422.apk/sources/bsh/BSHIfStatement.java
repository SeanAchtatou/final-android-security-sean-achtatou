package bsh;

class BSHIfStatement extends SimpleNode {
    BSHIfStatement(int i) {
        super(i);
    }

    public static boolean evaluateCondition(SimpleNode simpleNode, CallStack callStack, Interpreter interpreter) {
        Object eval = simpleNode.eval(callStack, interpreter);
        if (eval instanceof Primitive) {
            if (eval == Primitive.VOID) {
                throw new EvalError("Condition evaluates to void type", simpleNode, callStack);
            }
            eval = ((Primitive) eval).getValue();
        }
        if (eval instanceof Boolean) {
            return ((Boolean) eval).booleanValue();
        }
        throw new EvalError("Condition must evaluate to a Boolean or boolean.", simpleNode, callStack);
    }

    public Object eval(CallStack callStack, Interpreter interpreter) {
        Object eval = evaluateCondition((SimpleNode) jjtGetChild(0), callStack, interpreter) ? ((SimpleNode) jjtGetChild(1)).eval(callStack, interpreter) : jjtGetNumChildren() > 2 ? ((SimpleNode) jjtGetChild(2)).eval(callStack, interpreter) : null;
        return eval instanceof ReturnControl ? eval : Primitive.VOID;
    }
}
