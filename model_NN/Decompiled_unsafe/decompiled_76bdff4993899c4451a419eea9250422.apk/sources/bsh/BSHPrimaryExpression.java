package bsh;

class BSHPrimaryExpression extends SimpleNode {
    BSHPrimaryExpression(int i) {
        super(i);
    }

    private Object eval(boolean z, CallStack callStack, Interpreter interpreter) {
        Object obj;
        Object jjtGetChild = jjtGetChild(0);
        int jjtGetNumChildren = jjtGetNumChildren();
        for (int i = 1; i < jjtGetNumChildren; i++) {
            jjtGetChild = ((BSHPrimarySuffix) jjtGetChild(i)).doSuffix(jjtGetChild, z, callStack, interpreter);
        }
        if (!(jjtGetChild instanceof SimpleNode)) {
            obj = jjtGetChild;
        } else if (jjtGetChild instanceof BSHAmbiguousName) {
            obj = z ? ((BSHAmbiguousName) jjtGetChild).toLHS(callStack, interpreter) : ((BSHAmbiguousName) jjtGetChild).toObject(callStack, interpreter);
        } else if (z) {
            throw new EvalError("Can't assign to prefix.", this, callStack);
        } else {
            obj = ((SimpleNode) jjtGetChild).eval(callStack, interpreter);
        }
        if (!(obj instanceof LHS) || z) {
            return obj;
        }
        try {
            return ((LHS) obj).getValue();
        } catch (UtilEvalError e) {
            throw e.toEvalError(this, callStack);
        }
    }

    public Object eval(CallStack callStack, Interpreter interpreter) {
        return eval(false, callStack, interpreter);
    }

    public LHS toLHS(CallStack callStack, Interpreter interpreter) {
        Object eval = eval(true, callStack, interpreter);
        if (eval instanceof LHS) {
            return (LHS) eval;
        }
        throw new EvalError("Can't assign to:", this, callStack);
    }
}
