package bsh;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class JavaCharStream {
    public static final boolean staticFlag = false;
    int available;
    protected int[] bufcolumn;
    protected char[] buffer;
    protected int[] bufline;
    public int bufpos;
    int bufsize;
    protected int column;
    protected int inBuf;
    protected Reader inputStream;
    protected int line;
    protected int maxNextCharInd;
    protected char[] nextCharBuf;
    protected int nextCharInd;
    protected boolean prevCharIsCR;
    protected boolean prevCharIsLF;
    int tokenBegin;

    public JavaCharStream(InputStream inputStream2) {
        this(inputStream2, 1, 1, 4096);
    }

    public JavaCharStream(InputStream inputStream2, int i, int i2) {
        this(inputStream2, i, i2, 4096);
    }

    public JavaCharStream(InputStream inputStream2, int i, int i2, int i3) {
        this(new InputStreamReader(inputStream2), i, i2, 4096);
    }

    public JavaCharStream(Reader reader) {
        this(reader, 1, 1, 4096);
    }

    public JavaCharStream(Reader reader, int i, int i2) {
        this(reader, i, i2, 4096);
    }

    public JavaCharStream(Reader reader, int i, int i2, int i3) {
        this.bufpos = -1;
        this.column = 0;
        this.line = 1;
        this.prevCharIsCR = false;
        this.prevCharIsLF = false;
        this.maxNextCharInd = 0;
        this.nextCharInd = -1;
        this.inBuf = 0;
        this.inputStream = reader;
        this.line = i;
        this.column = i2 - 1;
        this.bufsize = i3;
        this.available = i3;
        this.buffer = new char[i3];
        this.bufline = new int[i3];
        this.bufcolumn = new int[i3];
        this.nextCharBuf = new char[4096];
    }

    static final int hexval(char c) {
        switch (c) {
            case ParserConstants.STATIC /*48*/:
                return 0;
            case ParserConstants.STRICTFP /*49*/:
                return 1;
            case ParserConstants.SWITCH /*50*/:
                return 2;
            case ParserConstants.SYNCHRONIZED /*51*/:
                return 3;
            case ParserConstants.TRANSIENT /*52*/:
                return 4;
            case ParserConstants.THROW /*53*/:
                return 5;
            case ParserConstants.THROWS /*54*/:
                return 6;
            case ParserConstants.TRUE /*55*/:
                return 7;
            case ParserConstants.TRY /*56*/:
                return 8;
            case ParserConstants.VOID /*57*/:
                return 9;
            case ParserConstants.EXPONENT /*65*/:
            case ParserConstants.BOOL_ORX /*97*/:
                return 10;
            case ParserConstants.CHARACTER_LITERAL /*66*/:
            case ParserConstants.BOOL_AND /*98*/:
                return 11;
            case ParserConstants.STRING_LITERAL /*67*/:
            case ParserConstants.BOOL_ANDX /*99*/:
                return 12;
            case ParserConstants.FORMAL_COMMENT /*68*/:
            case ParserConstants.INCR /*100*/:
                return 13;
            case ParserConstants.IDENTIFIER /*69*/:
            case ParserConstants.DECR /*101*/:
                return 14;
            case ParserConstants.LETTER /*70*/:
            case ParserConstants.PLUS /*102*/:
                return 15;
            default:
                throw new IOException();
        }
    }

    /* access modifiers changed from: protected */
    public void AdjustBuffSize() {
        if (this.available == this.bufsize) {
            if (this.tokenBegin > 2048) {
                this.bufpos = 0;
            } else {
                ExpandBuff(false);
                return;
            }
        } else if (this.available > this.tokenBegin) {
            this.available = this.bufsize;
            return;
        } else if (this.tokenBegin - this.available < 2048) {
            ExpandBuff(true);
            return;
        }
        this.available = this.tokenBegin;
    }

    public char BeginToken() {
        if (this.inBuf > 0) {
            this.inBuf--;
            int i = this.bufpos + 1;
            this.bufpos = i;
            if (i == this.bufsize) {
                this.bufpos = 0;
            }
            this.tokenBegin = this.bufpos;
            return this.buffer[this.bufpos];
        }
        this.tokenBegin = 0;
        this.bufpos = -1;
        return readChar();
    }

    public void Done() {
        this.nextCharBuf = null;
        this.buffer = null;
        this.bufline = null;
        this.bufcolumn = null;
    }

    /* access modifiers changed from: protected */
    public void ExpandBuff(boolean z) {
        char[] cArr = new char[(this.bufsize + 2048)];
        int[] iArr = new int[(this.bufsize + 2048)];
        int[] iArr2 = new int[(this.bufsize + 2048)];
        if (z) {
            try {
                System.arraycopy(this.buffer, this.tokenBegin, cArr, 0, this.bufsize - this.tokenBegin);
                System.arraycopy(this.buffer, 0, cArr, this.bufsize - this.tokenBegin, this.bufpos);
                this.buffer = cArr;
                System.arraycopy(this.bufline, this.tokenBegin, iArr, 0, this.bufsize - this.tokenBegin);
                System.arraycopy(this.bufline, 0, iArr, this.bufsize - this.tokenBegin, this.bufpos);
                this.bufline = iArr;
                System.arraycopy(this.bufcolumn, this.tokenBegin, iArr2, 0, this.bufsize - this.tokenBegin);
                System.arraycopy(this.bufcolumn, 0, iArr2, this.bufsize - this.tokenBegin, this.bufpos);
                this.bufcolumn = iArr2;
                this.bufpos += this.bufsize - this.tokenBegin;
            } catch (Throwable th) {
                throw new Error(th.getMessage());
            }
        } else {
            System.arraycopy(this.buffer, this.tokenBegin, cArr, 0, this.bufsize - this.tokenBegin);
            this.buffer = cArr;
            System.arraycopy(this.bufline, this.tokenBegin, iArr, 0, this.bufsize - this.tokenBegin);
            this.bufline = iArr;
            System.arraycopy(this.bufcolumn, this.tokenBegin, iArr2, 0, this.bufsize - this.tokenBegin);
            this.bufcolumn = iArr2;
            this.bufpos -= this.tokenBegin;
        }
        int i = this.bufsize + 2048;
        this.bufsize = i;
        this.available = i;
        this.tokenBegin = 0;
    }

    /* access modifiers changed from: protected */
    public void FillBuff() {
        if (this.maxNextCharInd == 4096) {
            this.nextCharInd = 0;
            this.maxNextCharInd = 0;
        }
        try {
            int read = this.inputStream.read(this.nextCharBuf, this.maxNextCharInd, 4096 - this.maxNextCharInd);
            if (read == -1) {
                this.inputStream.close();
                throw new IOException();
            } else {
                this.maxNextCharInd = read + this.maxNextCharInd;
            }
        } catch (IOException e) {
            if (this.bufpos != 0) {
                this.bufpos--;
                backup(0);
            } else {
                this.bufline[this.bufpos] = this.line;
                this.bufcolumn[this.bufpos] = this.column;
            }
            throw e;
        }
    }

    public String GetImage() {
        return this.bufpos >= this.tokenBegin ? new String(this.buffer, this.tokenBegin, (this.bufpos - this.tokenBegin) + 1) : new String(this.buffer, this.tokenBegin, this.bufsize - this.tokenBegin) + new String(this.buffer, 0, this.bufpos + 1);
    }

    public char[] GetSuffix(int i) {
        char[] cArr = new char[i];
        if (this.bufpos + 1 >= i) {
            System.arraycopy(this.buffer, (this.bufpos - i) + 1, cArr, 0, i);
        } else {
            System.arraycopy(this.buffer, this.bufsize - ((i - this.bufpos) - 1), cArr, 0, (i - this.bufpos) - 1);
            System.arraycopy(this.buffer, 0, cArr, (i - this.bufpos) - 1, this.bufpos + 1);
        }
        return cArr;
    }

    public void ReInit(InputStream inputStream2) {
        ReInit(inputStream2, 1, 1, 4096);
    }

    public void ReInit(InputStream inputStream2, int i, int i2) {
        ReInit(inputStream2, i, i2, 4096);
    }

    public void ReInit(InputStream inputStream2, int i, int i2, int i3) {
        ReInit(new InputStreamReader(inputStream2), i, i2, 4096);
    }

    public void ReInit(Reader reader) {
        ReInit(reader, 1, 1, 4096);
    }

    public void ReInit(Reader reader, int i, int i2) {
        ReInit(reader, i, i2, 4096);
    }

    public void ReInit(Reader reader, int i, int i2, int i3) {
        this.inputStream = reader;
        this.line = i;
        this.column = i2 - 1;
        if (this.buffer == null || i3 != this.buffer.length) {
            this.bufsize = i3;
            this.available = i3;
            this.buffer = new char[i3];
            this.bufline = new int[i3];
            this.bufcolumn = new int[i3];
            this.nextCharBuf = new char[4096];
        }
        this.prevCharIsCR = false;
        this.prevCharIsLF = false;
        this.maxNextCharInd = 0;
        this.inBuf = 0;
        this.tokenBegin = 0;
        this.bufpos = -1;
        this.nextCharInd = -1;
    }

    /* access modifiers changed from: protected */
    public char ReadByte() {
        int i = this.nextCharInd + 1;
        this.nextCharInd = i;
        if (i >= this.maxNextCharInd) {
            FillBuff();
        }
        return this.nextCharBuf[this.nextCharInd];
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void UpdateLineColumn(char r4) {
        /*
            r3 = this;
            r2 = 0
            r1 = 1
            int r0 = r3.column
            int r0 = r0 + 1
            r3.column = r0
            boolean r0 = r3.prevCharIsLF
            if (r0 == 0) goto L_0x002a
            r3.prevCharIsLF = r2
        L_0x000e:
            int r0 = r3.line
            r3.column = r1
            int r0 = r0 + 1
            r3.line = r0
        L_0x0016:
            switch(r4) {
                case 9: goto L_0x003d;
                case 10: goto L_0x003a;
                case 11: goto L_0x0019;
                case 12: goto L_0x0019;
                case 13: goto L_0x0037;
                default: goto L_0x0019;
            }
        L_0x0019:
            int[] r0 = r3.bufline
            int r1 = r3.bufpos
            int r2 = r3.line
            r0[r1] = r2
            int[] r0 = r3.bufcolumn
            int r1 = r3.bufpos
            int r2 = r3.column
            r0[r1] = r2
            return
        L_0x002a:
            boolean r0 = r3.prevCharIsCR
            if (r0 == 0) goto L_0x0016
            r3.prevCharIsCR = r2
            r0 = 10
            if (r4 != r0) goto L_0x000e
            r3.prevCharIsLF = r1
            goto L_0x0016
        L_0x0037:
            r3.prevCharIsCR = r1
            goto L_0x0019
        L_0x003a:
            r3.prevCharIsLF = r1
            goto L_0x0019
        L_0x003d:
            int r0 = r3.column
            int r0 = r0 + -1
            r3.column = r0
            int r0 = r3.column
            int r1 = r3.column
            r1 = r1 & 7
            int r1 = 8 - r1
            int r0 = r0 + r1
            r3.column = r0
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.JavaCharStream.UpdateLineColumn(char):void");
    }

    public void adjustBeginLineColumn(int i, int i2) {
        int i3;
        int i4 = 0;
        int i5 = this.tokenBegin;
        int i6 = this.bufpos >= this.tokenBegin ? (this.bufpos - this.tokenBegin) + this.inBuf + 1 : (this.bufsize - this.tokenBegin) + this.bufpos + 1 + this.inBuf;
        int i7 = 0;
        int i8 = 0;
        while (i3 < i6) {
            int[] iArr = this.bufline;
            i4 = i5 % this.bufsize;
            int i9 = iArr[i4];
            int[] iArr2 = this.bufline;
            i5++;
            int i10 = i5 % this.bufsize;
            if (i9 != iArr2[i10]) {
                break;
            }
            this.bufline[i4] = i;
            int i11 = (this.bufcolumn[i10] + i7) - this.bufcolumn[i4];
            this.bufcolumn[i4] = i7 + i2;
            i8 = i3 + 1;
            i7 = i11;
        }
        if (i3 < i6) {
            this.bufline[i4] = i;
            this.bufcolumn[i4] = i7 + i2;
            int i12 = i + 1;
            int i13 = i5;
            while (true) {
                int i14 = i3 + 1;
                if (i3 >= i6) {
                    break;
                }
                int[] iArr3 = this.bufline;
                i4 = i13 % this.bufsize;
                int i15 = iArr3[i4];
                int i16 = i13 + 1;
                if (i15 != this.bufline[i16 % this.bufsize]) {
                    this.bufline[i4] = i12;
                    i12++;
                    i13 = i16;
                    i3 = i14;
                } else {
                    this.bufline[i4] = i12;
                    i13 = i16;
                    i3 = i14;
                }
            }
        }
        int i17 = i4;
        this.line = this.bufline[i17];
        this.column = this.bufcolumn[i17];
    }

    public void backup(int i) {
        this.inBuf += i;
        int i2 = this.bufpos - i;
        this.bufpos = i2;
        if (i2 < 0) {
            this.bufpos += this.bufsize;
        }
    }

    public int getBeginColumn() {
        return this.bufcolumn[this.tokenBegin];
    }

    public int getBeginLine() {
        return this.bufline[this.tokenBegin];
    }

    public int getColumn() {
        return this.bufcolumn[this.bufpos];
    }

    public int getEndColumn() {
        return this.bufcolumn[this.bufpos];
    }

    public int getEndLine() {
        return this.bufline[this.bufpos];
    }

    public int getLine() {
        return this.bufline[this.bufpos];
    }

    public char readChar() {
        char ReadByte;
        char ReadByte2;
        if (this.inBuf > 0) {
            this.inBuf--;
            int i = this.bufpos + 1;
            this.bufpos = i;
            if (i == this.bufsize) {
                this.bufpos = 0;
            }
            return this.buffer[this.bufpos];
        }
        int i2 = this.bufpos + 1;
        this.bufpos = i2;
        if (i2 == this.available) {
            AdjustBuffSize();
        }
        char[] cArr = this.buffer;
        int i3 = this.bufpos;
        char ReadByte3 = ReadByte();
        cArr[i3] = ReadByte3;
        if (ReadByte3 == '\\') {
            UpdateLineColumn(ReadByte3);
            int i4 = 1;
            while (true) {
                int i5 = this.bufpos + 1;
                this.bufpos = i5;
                if (i5 == this.available) {
                    AdjustBuffSize();
                }
                try {
                    char[] cArr2 = this.buffer;
                    int i6 = this.bufpos;
                    ReadByte = ReadByte();
                    cArr2[i6] = ReadByte;
                    if (ReadByte != '\\') {
                        break;
                    }
                    UpdateLineColumn(ReadByte);
                    i4++;
                } catch (IOException e) {
                    if (i4 > 1) {
                        backup(i4);
                    }
                    return '\\';
                }
            }
            UpdateLineColumn(ReadByte);
            if (ReadByte == 'u' && (i4 & 1) == 1) {
                int i7 = this.bufpos - 1;
                this.bufpos = i7;
                if (i7 < 0) {
                    this.bufpos = this.bufsize - 1;
                }
                while (true) {
                    try {
                        ReadByte2 = ReadByte();
                        if (ReadByte2 != 'u') {
                            break;
                        }
                        this.column++;
                    } catch (IOException e2) {
                        throw new Error("Invalid escape character at line " + this.line + " column " + this.column + ".");
                    }
                }
                char[] cArr3 = this.buffer;
                int i8 = this.bufpos;
                char hexval = (char) ((hexval(ReadByte2) << 12) | (hexval(ReadByte()) << 8) | (hexval(ReadByte()) << 4) | hexval(ReadByte()));
                cArr3[i8] = hexval;
                this.column += 4;
                if (i4 == 1) {
                    return hexval;
                }
                backup(i4 - 1);
                return '\\';
            }
            backup(i4);
            return '\\';
        }
        UpdateLineColumn(ReadByte3);
        return ReadByte3;
    }
}
