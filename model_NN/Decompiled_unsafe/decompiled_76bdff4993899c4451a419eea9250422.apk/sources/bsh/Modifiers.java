package bsh;

import java.io.Serializable;
import java.util.Hashtable;

public class Modifiers implements Serializable {
    public static final int CLASS = 0;
    public static final int FIELD = 2;
    public static final int METHOD = 1;
    Hashtable modifiers;

    private void insureNo(String str, String str2) {
        if (hasModifier(str)) {
            throw new IllegalStateException(str2 + " cannot be declared '" + str + "'");
        }
    }

    private void validateForClass() {
        validateForMethod();
        insureNo("native", "Class");
        insureNo("synchronized", "Class");
    }

    private void validateForField() {
        insureNo("synchronized", "Variable");
        insureNo("native", "Variable");
        insureNo("abstract", "Variable");
    }

    private void validateForMethod() {
        insureNo("volatile", "Method");
        insureNo("transient", "Method");
    }

    public void addModifier(int i, String str) {
        if (this.modifiers == null) {
            this.modifiers = new Hashtable();
        }
        if (this.modifiers.put(str, Void.TYPE) != null) {
            throw new IllegalStateException("Duplicate modifier: " + str);
        }
        int i2 = 0;
        if (hasModifier("private")) {
            i2 = 1;
        }
        if (hasModifier("protected")) {
            i2++;
        }
        if (hasModifier("public")) {
            i2++;
        }
        if (i2 > 1) {
            throw new IllegalStateException("public/private/protected cannot be used in combination.");
        }
        switch (i) {
            case 0:
                validateForClass();
                return;
            case 1:
                validateForMethod();
                return;
            case 2:
                validateForField();
                return;
            default:
                return;
        }
    }

    public boolean hasModifier(String str) {
        if (this.modifiers == null) {
            this.modifiers = new Hashtable();
        }
        return this.modifiers.get(str) != null;
    }

    public String toString() {
        return "Modifiers: " + this.modifiers;
    }
}
