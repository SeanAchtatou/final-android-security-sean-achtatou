package bsh.collection;

import bsh.BshIterator;
import bsh.CollectionManager;
import bsh.InterpreterError;
import java.util.Collection;
import java.util.Iterator;

public class CollectionIterator implements BshIterator {
    private Iterator iterator;

    public CollectionIterator(Object obj) {
        this.iterator = createIterator(obj);
    }

    /* access modifiers changed from: protected */
    public Iterator createIterator(Object obj) {
        if (obj == null) {
            throw new NullPointerException("Object arguments passed to the CollectionIterator constructor cannot be null.");
        } else if (obj instanceof Iterator) {
            return (Iterator) obj;
        } else {
            if (obj instanceof Collection) {
                return ((Collection) obj).iterator();
            }
            Iterator iteratorForIterable = getIteratorForIterable(obj);
            if (iteratorForIterable != null) {
                return iteratorForIterable;
            }
            final CollectionManager.BasicBshIterator basicBshIterator = new CollectionManager.BasicBshIterator(obj);
            return new Iterator() {
                public final boolean hasNext() {
                    return basicBshIterator.hasNext();
                }

                public final Object next() {
                    return basicBshIterator.next();
                }

                public final void remove() {
                    throw new UnsupportedOperationException("remove() is not supported");
                }
            };
        }
    }

    /* access modifiers changed from: package-private */
    public Iterator getIteratorForIterable(Object obj) {
        try {
            Class<?> cls = Class.forName("java.lang.Iterable");
            if (cls.isInstance(obj)) {
                return (Iterator) cls.getMethod("iterator", new Class[0]).invoke(obj, new Object[0]);
            }
            return null;
        } catch (Exception e) {
            throw new InterpreterError("Unexpected problem calling \"iterator()\" on instance of java.lang.Iterable." + e);
        } catch (ClassNotFoundException e2) {
            return null;
        }
    }

    public boolean hasNext() {
        return this.iterator.hasNext();
    }

    public Object next() {
        return this.iterator.next();
    }
}
