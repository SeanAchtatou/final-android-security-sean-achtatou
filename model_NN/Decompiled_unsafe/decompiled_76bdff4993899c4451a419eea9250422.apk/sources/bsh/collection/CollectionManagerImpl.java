package bsh.collection;

import bsh.BshIterator;
import bsh.CollectionManager;
import java.util.Map;

public class CollectionManagerImpl extends CollectionManager {
    public BshIterator getBshIterator(Object obj) {
        return new CollectionIterator(obj);
    }

    public Object getFromMap(Object obj, Object obj2) {
        return ((Map) obj).get(obj2);
    }

    public boolean isMap(Object obj) {
        if (obj instanceof Map) {
            return true;
        }
        return super.isMap(obj);
    }

    public Object putInMap(Object obj, Object obj2, Object obj3) {
        return ((Map) obj).put(obj2, obj3);
    }
}
