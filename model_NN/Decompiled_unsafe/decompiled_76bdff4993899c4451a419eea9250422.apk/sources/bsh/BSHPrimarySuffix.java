package bsh;

import java.lang.reflect.InvocationTargetException;

class BSHPrimarySuffix extends SimpleNode {
    public static final int CLASS = 0;
    public static final int INDEX = 1;
    public static final int NAME = 2;
    public static final int PROPERTY = 3;
    public String field;
    Object index;
    public int operation;

    BSHPrimarySuffix(int i) {
        super(i);
    }

    private Object doIndex(Object obj, boolean z, CallStack callStack, Interpreter interpreter) {
        int indexAux = getIndexAux(obj, callStack, interpreter, this);
        if (z) {
            return new LHS(obj, indexAux);
        }
        try {
            return Reflect.getIndex(obj, indexAux);
        } catch (UtilEvalError e) {
            throw e.toEvalError(this, callStack);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object doName(java.lang.Object r9, boolean r10, bsh.CallStack r11, bsh.Interpreter r12) {
        /*
            r8 = this;
            r7 = 0
            java.lang.String r1 = r8.field     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.String r2 = "length"
            boolean r1 = r1.equals(r2)     // Catch:{ UtilEvalError -> 0x001f }
            if (r1 == 0) goto L_0x002f
            java.lang.Class r1 = r9.getClass()     // Catch:{ UtilEvalError -> 0x001f }
            boolean r1 = r1.isArray()     // Catch:{ UtilEvalError -> 0x001f }
            if (r1 == 0) goto L_0x002f
            if (r10 == 0) goto L_0x0025
            bsh.EvalError r1 = new bsh.EvalError     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.String r2 = "Can't assign array length"
            r1.<init>(r2, r8, r11)     // Catch:{ UtilEvalError -> 0x001f }
            throw r1     // Catch:{ UtilEvalError -> 0x001f }
        L_0x001f:
            r1 = move-exception
            bsh.EvalError r1 = r1.toEvalError(r8, r11)
            throw r1
        L_0x0025:
            bsh.Primitive r1 = new bsh.Primitive     // Catch:{ UtilEvalError -> 0x001f }
            int r2 = java.lang.reflect.Array.getLength(r9)     // Catch:{ UtilEvalError -> 0x001f }
            r1.<init>(r2)     // Catch:{ UtilEvalError -> 0x001f }
        L_0x002e:
            return r1
        L_0x002f:
            int r1 = r8.jjtGetNumChildren()     // Catch:{ UtilEvalError -> 0x001f }
            if (r1 != 0) goto L_0x0045
            if (r10 == 0) goto L_0x003e
            java.lang.String r1 = r8.field     // Catch:{ UtilEvalError -> 0x001f }
            bsh.LHS r1 = bsh.Reflect.getLHSObjectField(r9, r1)     // Catch:{ UtilEvalError -> 0x001f }
            goto L_0x002e
        L_0x003e:
            java.lang.String r1 = r8.field     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.Object r1 = bsh.Reflect.getObjectFieldValue(r9, r1)     // Catch:{ UtilEvalError -> 0x001f }
            goto L_0x002e
        L_0x0045:
            r1 = 0
            bsh.Node r1 = r8.jjtGetChild(r1)     // Catch:{ UtilEvalError -> 0x001f }
            bsh.BSHArguments r1 = (bsh.BSHArguments) r1     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.Object[] r3 = r1.getArguments(r11, r12)     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.String r2 = r8.field     // Catch:{ ReflectError -> 0x005b, InvocationTargetException -> 0x0079 }
            r1 = r9
            r4 = r12
            r5 = r11
            r6 = r8
            java.lang.Object r1 = bsh.Reflect.invokeObjectMethod(r1, r2, r3, r4, r5, r6)     // Catch:{ ReflectError -> 0x005b, InvocationTargetException -> 0x0079 }
            goto L_0x002e
        L_0x005b:
            r1 = move-exception
            bsh.EvalError r2 = new bsh.EvalError     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ UtilEvalError -> 0x001f }
            r3.<init>()     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.String r4 = "Error in method invocation: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.String r1 = r1.getMessage()     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.String r1 = r1.toString()     // Catch:{ UtilEvalError -> 0x001f }
            r2.<init>(r1, r8, r11)     // Catch:{ UtilEvalError -> 0x001f }
            throw r2     // Catch:{ UtilEvalError -> 0x001f }
        L_0x0079:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UtilEvalError -> 0x001f }
            r2.<init>()     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.String r3 = "Method Invocation "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.String r3 = r8.field     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.String r2 = r2.toString()     // Catch:{ UtilEvalError -> 0x001f }
            java.lang.Throwable r3 = r1.getTargetException()     // Catch:{ UtilEvalError -> 0x001f }
            r6 = 1
            boolean r1 = r3 instanceof bsh.EvalError     // Catch:{ UtilEvalError -> 0x001f }
            if (r1 == 0) goto L_0x00a4
            boolean r1 = r3 instanceof bsh.TargetError     // Catch:{ UtilEvalError -> 0x001f }
            if (r1 == 0) goto L_0x00ac
            r0 = r3
            bsh.TargetError r0 = (bsh.TargetError) r0     // Catch:{ UtilEvalError -> 0x001f }
            r1 = r0
            boolean r6 = r1.inNativeCode()     // Catch:{ UtilEvalError -> 0x001f }
        L_0x00a4:
            bsh.TargetError r1 = new bsh.TargetError     // Catch:{ UtilEvalError -> 0x001f }
            r4 = r8
            r5 = r11
            r1.<init>(r2, r3, r4, r5, r6)     // Catch:{ UtilEvalError -> 0x001f }
            throw r1     // Catch:{ UtilEvalError -> 0x001f }
        L_0x00ac:
            r6 = r7
            goto L_0x00a4
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.BSHPrimarySuffix.doName(java.lang.Object, boolean, bsh.CallStack, bsh.Interpreter):java.lang.Object");
    }

    private Object doProperty(boolean z, Object obj, CallStack callStack, Interpreter interpreter) {
        if (obj == Primitive.VOID) {
            throw new EvalError("Attempt to access property on undefined variable or class name", this, callStack);
        } else if (obj instanceof Primitive) {
            throw new EvalError("Attempt to access property on a primitive", this, callStack);
        } else {
            Object eval = ((SimpleNode) jjtGetChild(0)).eval(callStack, interpreter);
            if (!(eval instanceof String)) {
                throw new EvalError("Property expression must be a String or identifier.", this, callStack);
            } else if (z) {
                return new LHS(obj, (String) eval);
            } else {
                CollectionManager collectionManager = CollectionManager.getCollectionManager();
                if (collectionManager.isMap(obj)) {
                    Object fromMap = collectionManager.getFromMap(obj, eval);
                    return fromMap == null ? Primitive.NULL : fromMap;
                }
                try {
                    return Reflect.getObjectProperty(obj, (String) eval);
                } catch (UtilEvalError e) {
                    throw e.toEvalError("Property: " + eval, this, callStack);
                } catch (ReflectError e2) {
                    throw new EvalError("No such property: " + eval, this, callStack);
                }
            }
        }
    }

    static int getIndexAux(Object obj, CallStack callStack, Interpreter interpreter, SimpleNode simpleNode) {
        if (!obj.getClass().isArray()) {
            throw new EvalError("Not an array", simpleNode, callStack);
        }
        try {
            Object eval = ((SimpleNode) simpleNode.jjtGetChild(0)).eval(callStack, interpreter);
            if (!(eval instanceof Primitive)) {
                eval = Types.castObject(eval, Integer.TYPE, 1);
            }
            return ((Primitive) eval).intValue();
        } catch (UtilEvalError e) {
            Interpreter.debug("doIndex: " + e);
            throw e.toEvalError("Arrays may only be indexed by integer types.", simpleNode, callStack);
        }
    }

    public Object doSuffix(Object obj, boolean z, CallStack callStack, Interpreter interpreter) {
        if (this.operation != 0) {
            if (obj instanceof SimpleNode) {
                obj = obj instanceof BSHAmbiguousName ? ((BSHAmbiguousName) obj).toObject(callStack, interpreter) : ((SimpleNode) obj).eval(callStack, interpreter);
            } else if (obj instanceof LHS) {
                try {
                    obj = ((LHS) obj).getValue();
                } catch (UtilEvalError e) {
                    throw e.toEvalError(this, callStack);
                }
            }
            try {
                switch (this.operation) {
                    case 1:
                        return doIndex(obj, z, callStack, interpreter);
                    case 2:
                        return doName(obj, z, callStack, interpreter);
                    case 3:
                        return doProperty(z, obj, callStack, interpreter);
                    default:
                        throw new InterpreterError("Unknown suffix type");
                }
            } catch (ReflectError e2) {
                throw new EvalError("reflection error: " + e2, this, callStack);
            } catch (InvocationTargetException e3) {
                throw new TargetError("target exception", e3.getTargetException(), this, callStack, true);
            }
        } else if (!(obj instanceof BSHType)) {
            throw new EvalError("Attempt to use .class suffix on non class.", this, callStack);
        } else if (z) {
            throw new EvalError("Can't assign .class", this, callStack);
        } else {
            callStack.top();
            return ((BSHType) obj).getType(callStack, interpreter);
        }
    }
}
