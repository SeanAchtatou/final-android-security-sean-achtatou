package bsh;

public class ParseException extends EvalError {
    public Token currentToken;
    protected String eol;
    public int[][] expectedTokenSequences;
    String sourceFile;
    protected boolean specialConstructor;
    public String[] tokenImage;

    public ParseException() {
        this("");
        this.specialConstructor = false;
    }

    public ParseException(Token token, int[][] iArr, String[] strArr) {
        this();
        this.specialConstructor = true;
        this.currentToken = token;
        this.expectedTokenSequences = iArr;
        this.tokenImage = strArr;
    }

    public ParseException(String str) {
        super(str, null, null);
        this.sourceFile = "<unknown>";
        this.eol = System.getProperty("line.separator", "\n");
        this.specialConstructor = false;
    }

    /* access modifiers changed from: protected */
    public String add_escapes(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
                case 0:
                    break;
                case 8:
                    stringBuffer.append("\\b");
                    break;
                case 9:
                    stringBuffer.append("\\t");
                    break;
                case 10:
                    stringBuffer.append("\\n");
                    break;
                case 12:
                    stringBuffer.append("\\f");
                    break;
                case 13:
                    stringBuffer.append("\\r");
                    break;
                case '\"':
                    stringBuffer.append("\\\"");
                    break;
                case ParserConstants.NATIVE /*39*/:
                    stringBuffer.append("\\'");
                    break;
                case ParserConstants.LEX /*92*/:
                    stringBuffer.append("\\\\");
                    break;
                default:
                    char charAt = str.charAt(i);
                    if (charAt >= ' ' && charAt <= '~') {
                        stringBuffer.append(charAt);
                        break;
                    } else {
                        String str2 = "0000" + Integer.toString(charAt, 16);
                        stringBuffer.append("\\u" + str2.substring(str2.length() - 4, str2.length()));
                        break;
                    }
                    break;
            }
        }
        return stringBuffer.toString();
    }

    public int getErrorLineNumber() {
        return this.currentToken.next.beginLine;
    }

    public String getErrorSourceFile() {
        return this.sourceFile;
    }

    public String getErrorText() {
        int i = 0;
        for (int i2 = 0; i2 < this.expectedTokenSequences.length; i2++) {
            if (i < this.expectedTokenSequences[i2].length) {
                i = this.expectedTokenSequences[i2].length;
            }
        }
        Token token = this.currentToken.next;
        String str = "";
        int i3 = 0;
        while (i3 < i) {
            if (i3 != 0) {
                str = str + " ";
            }
            if (token.kind == 0) {
                return str + this.tokenImage[0];
            }
            String str2 = str + add_escapes(token.image);
            token = token.next;
            i3++;
            str = str2;
        }
        return str;
    }

    public String getMessage() {
        return getMessage(false);
    }

    public String getMessage(boolean z) {
        if (!this.specialConstructor) {
            return super.getMessage();
        }
        String str = "";
        int i = 0;
        int i2 = 0;
        while (i < this.expectedTokenSequences.length) {
            int length = i2 < this.expectedTokenSequences[i].length ? this.expectedTokenSequences[i].length : i2;
            String str2 = str;
            for (int i3 = 0; i3 < this.expectedTokenSequences[i].length; i3++) {
                str2 = str2 + this.tokenImage[this.expectedTokenSequences[i][i3]] + " ";
            }
            if (this.expectedTokenSequences[i][this.expectedTokenSequences[i].length - 1] != 0) {
                str2 = str2 + "...";
            }
            i++;
            str = str2 + this.eol + "    ";
            i2 = length;
        }
        Token token = this.currentToken.next;
        String str3 = "In file: " + this.sourceFile + " Encountered \"";
        int i4 = 0;
        while (true) {
            if (i4 >= i2) {
                break;
            }
            if (i4 != 0) {
                str3 = str3 + " ";
            }
            if (token.kind == 0) {
                str3 = str3 + this.tokenImage[0];
                break;
            }
            String str4 = str3 + add_escapes(token.image);
            token = token.next;
            i4++;
            str3 = str4;
        }
        String str5 = str3 + "\" at line " + this.currentToken.next.beginLine + ", column " + this.currentToken.next.beginColumn + "." + this.eol;
        if (!z) {
            return str5;
        }
        return (this.expectedTokenSequences.length == 1 ? str5 + "Was expecting:" + this.eol + "    " : str5 + "Was expecting one of:" + this.eol + "    ") + str;
    }

    public void setErrorSourceFile(String str) {
        this.sourceFile = str;
    }

    public String toString() {
        return getMessage();
    }
}
