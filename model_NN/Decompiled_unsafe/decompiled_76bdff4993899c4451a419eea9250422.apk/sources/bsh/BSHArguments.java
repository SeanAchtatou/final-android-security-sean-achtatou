package bsh;

class BSHArguments extends SimpleNode {
    BSHArguments(int i) {
        super(i);
    }

    public Object[] getArguments(CallStack callStack, Interpreter interpreter) {
        Object[] objArr = new Object[jjtGetNumChildren()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= objArr.length) {
                return objArr;
            }
            objArr[i2] = ((SimpleNode) jjtGetChild(i2)).eval(callStack, interpreter);
            if (objArr[i2] == Primitive.VOID) {
                throw new EvalError("Undefined argument: " + ((SimpleNode) jjtGetChild(i2)).getText(), this, callStack);
            }
            i = i2 + 1;
        }
    }
}
