package bsh;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class ExternalNameSpace extends NameSpace {
    private Map externalMap;

    public ExternalNameSpace() {
        this(null, "External Map Namespace", null);
    }

    public ExternalNameSpace(NameSpace nameSpace, String str, Map map) {
        super(nameSpace, str);
        this.externalMap = map == null ? new HashMap() : map;
    }

    public void clear() {
        super.clear();
        this.externalMap.clear();
    }

    public Variable createVariable(String str, Class cls, Object obj, Modifiers modifiers) {
        LHS lhs = new LHS(this.externalMap, str);
        try {
            lhs.assign(obj, false);
            return new Variable(str, cls, lhs);
        } catch (UtilEvalError e) {
            throw new InterpreterError(e.toString());
        }
    }

    public Map getMap() {
        return this.externalMap;
    }

    /* access modifiers changed from: protected */
    public Variable getVariableImpl(String str, boolean z) {
        Object obj = this.externalMap.get(str);
        if (obj == null) {
            super.unsetVariable(str);
            return super.getVariableImpl(str, z);
        }
        Variable variableImpl = super.getVariableImpl(str, false);
        return variableImpl == null ? createVariable(str, null, obj, null) : variableImpl;
    }

    public String[] getVariableNames() {
        HashSet hashSet = new HashSet();
        hashSet.addAll(Arrays.asList(super.getVariableNames()));
        hashSet.addAll(this.externalMap.keySet());
        return (String[]) hashSet.toArray(new String[0]);
    }

    public void setMap(Map map) {
        this.externalMap = null;
        clear();
        this.externalMap = map;
    }

    public void unsetVariable(String str) {
        super.unsetVariable(str);
        this.externalMap.remove(str);
    }
}
