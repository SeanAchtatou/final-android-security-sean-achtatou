package bsh;

class BSHAssignment extends SimpleNode implements ParserConstants {
    public int operator;

    BSHAssignment(int i) {
        super(i);
    }

    private Object operation(Object obj, Object obj2, int i) {
        if (!(obj instanceof String) || obj2 == Primitive.VOID) {
            if ((obj instanceof Primitive) || (obj2 instanceof Primitive)) {
                if (obj == Primitive.VOID || obj2 == Primitive.VOID) {
                    throw new UtilEvalError("Illegal use of undefined object or 'void' literal");
                } else if (obj == Primitive.NULL || obj2 == Primitive.NULL) {
                    throw new UtilEvalError("Illegal use of null object or 'null' literal");
                }
            }
            if (((obj instanceof Boolean) || (obj instanceof Character) || (obj instanceof Number) || (obj instanceof Primitive)) && ((obj2 instanceof Boolean) || (obj2 instanceof Character) || (obj2 instanceof Number) || (obj2 instanceof Primitive))) {
                return Primitive.binaryOperation(obj, obj2, i);
            }
            throw new UtilEvalError("Non primitive value in operator: " + obj.getClass() + " " + tokenImage[i] + " " + obj2.getClass());
        } else if (i == 102) {
            return ((String) obj) + obj2;
        } else {
            throw new UtilEvalError("Use of non + operator with String LHS");
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public Object eval(CallStack callStack, Interpreter interpreter) {
        Object obj;
        BSHPrimaryExpression bSHPrimaryExpression = (BSHPrimaryExpression) jjtGetChild(0);
        if (bSHPrimaryExpression == null) {
            throw new InterpreterError("Error, null LHSnode");
        }
        boolean strictJava = interpreter.getStrictJava();
        LHS lhs = bSHPrimaryExpression.toLHS(callStack, interpreter);
        if (lhs == null) {
            throw new InterpreterError("Error, null LHS");
        }
        if (this.operator != 81) {
            try {
                obj = lhs.getValue();
            } catch (UtilEvalError e) {
                throw e.toEvalError(this, callStack);
            }
        } else {
            obj = null;
        }
        Object eval = ((SimpleNode) jjtGetChild(1)).eval(callStack, interpreter);
        if (eval == Primitive.VOID) {
            throw new EvalError("Void assignment.", this, callStack);
        }
        try {
            switch (this.operator) {
                case ParserConstants.ASSIGN /*81*/:
                    return lhs.assign(eval, strictJava);
                case ParserConstants.PLUSASSIGN /*118*/:
                    return lhs.assign(operation(obj, eval, ParserConstants.PLUS), strictJava);
                case ParserConstants.MINUSASSIGN /*119*/:
                    return lhs.assign(operation(obj, eval, ParserConstants.MINUS), strictJava);
                case ParserConstants.STARASSIGN /*120*/:
                    return lhs.assign(operation(obj, eval, ParserConstants.STAR), strictJava);
                case ParserConstants.SLASHASSIGN /*121*/:
                    return lhs.assign(operation(obj, eval, ParserConstants.SLASH), strictJava);
                case ParserConstants.ANDASSIGN /*122*/:
                case ParserConstants.ANDASSIGNX /*123*/:
                    return lhs.assign(operation(obj, eval, ParserConstants.BIT_AND), strictJava);
                case ParserConstants.ORASSIGN /*124*/:
                case ParserConstants.ORASSIGNX /*125*/:
                    return lhs.assign(operation(obj, eval, ParserConstants.BIT_OR), strictJava);
                case ParserConstants.XORASSIGN /*126*/:
                    return lhs.assign(operation(obj, eval, ParserConstants.XOR), strictJava);
                case ParserConstants.MODASSIGN /*127*/:
                    return lhs.assign(operation(obj, eval, ParserConstants.MOD), strictJava);
                case ParserConstants.LSHIFTASSIGN /*128*/:
                case ParserConstants.LSHIFTASSIGNX /*129*/:
                    return lhs.assign(operation(obj, eval, ParserConstants.LSHIFT), strictJava);
                case ParserConstants.RSIGNEDSHIFTASSIGN /*130*/:
                case ParserConstants.RSIGNEDSHIFTASSIGNX /*131*/:
                    return lhs.assign(operation(obj, eval, ParserConstants.RSIGNEDSHIFT), strictJava);
                case ParserConstants.RUNSIGNEDSHIFTASSIGN /*132*/:
                case ParserConstants.RUNSIGNEDSHIFTASSIGNX /*133*/:
                    return lhs.assign(operation(obj, eval, ParserConstants.RUNSIGNEDSHIFT), strictJava);
                default:
                    throw new InterpreterError("unimplemented operator in assignment BSH");
            }
        } catch (UtilEvalError e2) {
            throw e2.toEvalError(this, callStack);
        }
        throw e2.toEvalError(this, callStack);
    }
}
