package bsh;

public class DelayedEvalBshMethod extends BshMethod {
    transient CallStack callstack;
    transient Interpreter interpreter;
    String[] paramTypeDescriptors;
    BSHFormalParameters paramTypesNode;
    String returnTypeDescriptor;
    BSHReturnType returnTypeNode;

    DelayedEvalBshMethod(String str, String str2, BSHReturnType bSHReturnType, String[] strArr, String[] strArr2, BSHFormalParameters bSHFormalParameters, BSHBlock bSHBlock, NameSpace nameSpace, Modifiers modifiers, CallStack callStack, Interpreter interpreter2) {
        super(str, null, strArr, null, bSHBlock, nameSpace, modifiers);
        this.returnTypeDescriptor = str2;
        this.returnTypeNode = bSHReturnType;
        this.paramTypeDescriptors = strArr2;
        this.paramTypesNode = bSHFormalParameters;
        this.callstack = callStack;
        this.interpreter = interpreter2;
    }

    public String[] getParamTypeDescriptors() {
        return this.paramTypeDescriptors;
    }

    public Class[] getParameterTypes() {
        try {
            return (Class[]) this.paramTypesNode.eval(this.callstack, this.interpreter);
        } catch (EvalError e) {
            throw new InterpreterError("can't eval param types: " + e);
        }
    }

    public Class getReturnType() {
        if (this.returnTypeNode == null) {
            return null;
        }
        try {
            return this.returnTypeNode.evalReturnType(this.callstack, this.interpreter);
        } catch (EvalError e) {
            throw new InterpreterError("can't eval return type: " + e);
        }
    }

    public String getReturnTypeDescriptor() {
        return this.returnTypeDescriptor;
    }
}
