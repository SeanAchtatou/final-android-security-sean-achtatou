package bsh;

import java.io.FileReader;
import java.io.InputStream;
import java.io.Reader;
import java.util.Enumeration;
import java.util.Vector;

public class Parser implements ParserConstants, ParserTreeConstants {
    private static int[] jj_la1_0;
    private static int[] jj_la1_1;
    private static int[] jj_la1_2;
    private static int[] jj_la1_3;
    private static int[] jj_la1_4;
    private final JJCalls[] jj_2_rtns = new JJCalls[31];
    private int jj_endpos;
    private Vector jj_expentries = new Vector();
    private int[] jj_expentry;
    private int jj_gc = 0;
    private int jj_gen;
    JavaCharStream jj_input_stream;
    private int jj_kind = -1;
    private int jj_la;
    private final int[] jj_la1 = new int[88];
    private Token jj_lastpos;
    private int[] jj_lasttokens = new int[100];
    private final LookaheadSuccess jj_ls = new LookaheadSuccess();
    public Token jj_nt;
    private int jj_ntk;
    private boolean jj_rescan = false;
    private Token jj_scanpos;
    private boolean jj_semLA;
    protected JJTParserState jjtree = new JJTParserState();
    public boolean lookingAhead = false;
    boolean retainComments = false;
    public Token token;
    public ParserTokenManager token_source;

    final class JJCalls {
        int arg;
        Token first;
        int gen;
        JJCalls next;

        JJCalls() {
        }
    }

    final class LookaheadSuccess extends Error {
        private LookaheadSuccess() {
        }
    }

    static {
        jj_la1_0();
        jj_la1_1();
        jj_la1_2();
        jj_la1_3();
        jj_la1_4();
    }

    public Parser(ParserTokenManager parserTokenManager) {
        this.token_source = parserTokenManager;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 88; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public Parser(InputStream inputStream) {
        this.jj_input_stream = new JavaCharStream(inputStream, 1, 1);
        this.token_source = new ParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 88; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public Parser(Reader reader) {
        this.jj_input_stream = new JavaCharStream(reader, 1, 1);
        this.token_source = new ParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 88; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    private final boolean jj_2_1(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_1()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(0, i);
        }
        return z;
    }

    private final boolean jj_2_10(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_10()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(9, i);
        }
        return z;
    }

    private final boolean jj_2_11(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_11()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(10, i);
        }
        return z;
    }

    private final boolean jj_2_12(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_12()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(11, i);
        }
        return z;
    }

    private final boolean jj_2_13(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_13()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(12, i);
        }
        return z;
    }

    private final boolean jj_2_14(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_14()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(13, i);
        }
        return z;
    }

    private final boolean jj_2_15(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_15()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(14, i);
        }
        return z;
    }

    private final boolean jj_2_16(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_16()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(15, i);
        }
        return z;
    }

    private final boolean jj_2_17(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_17()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(16, i);
        }
        return z;
    }

    private final boolean jj_2_18(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_18()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(17, i);
        }
        return z;
    }

    private final boolean jj_2_19(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_19()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(18, i);
        }
        return z;
    }

    /* JADX INFO: finally extract failed */
    private final boolean jj_2_2(int i) {
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            boolean z = !jj_3_2();
            jj_save(1, i);
            return z;
        } catch (LookaheadSuccess e) {
            jj_save(1, i);
            return true;
        } catch (Throwable th) {
            jj_save(1, i);
            throw th;
        }
    }

    private final boolean jj_2_20(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_20()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(19, i);
        }
        return z;
    }

    private final boolean jj_2_21(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_21()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(20, i);
        }
        return z;
    }

    private final boolean jj_2_22(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_22()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(21, i);
        }
        return z;
    }

    private final boolean jj_2_23(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_23()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(22, i);
        }
        return z;
    }

    private final boolean jj_2_24(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_24()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(23, i);
        }
        return z;
    }

    private final boolean jj_2_25(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_25()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(24, i);
        }
        return z;
    }

    private final boolean jj_2_26(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_26()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(25, i);
        }
        return z;
    }

    private final boolean jj_2_27(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_27()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(26, i);
        }
        return z;
    }

    private final boolean jj_2_28(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_28()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(27, i);
        }
        return z;
    }

    private final boolean jj_2_29(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_29()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(28, i);
        }
        return z;
    }

    private final boolean jj_2_3(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_3()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(2, i);
        }
        return z;
    }

    private final boolean jj_2_30(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_30()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(29, i);
        }
        return z;
    }

    private final boolean jj_2_31(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_31()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(30, i);
        }
        return z;
    }

    private final boolean jj_2_4(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_4()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(3, i);
        }
        return z;
    }

    private final boolean jj_2_5(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_5()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(4, i);
        }
        return z;
    }

    private final boolean jj_2_6(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_6()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(5, i);
        }
        return z;
    }

    private final boolean jj_2_7(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_7()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(6, i);
        }
        return z;
    }

    private final boolean jj_2_8(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_8()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(7, i);
        }
        return z;
    }

    private final boolean jj_2_9(int i) {
        boolean z = true;
        this.jj_la = i;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (jj_3_9()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            jj_save(8, i);
        }
        return z;
    }

    private final boolean jj_3R_100() {
        return jj_3R_130();
    }

    private final boolean jj_3R_101() {
        return jj_3R_37();
    }

    private final boolean jj_3R_102() {
        return jj_3R_32();
    }

    private final boolean jj_3R_103() {
        return jj_3R_29();
    }

    private final boolean jj_3R_104() {
        Token token2 = this.jj_scanpos;
        if (jj_3_16()) {
            this.jj_scanpos = token2;
            if (jj_3R_131()) {
                this.jj_scanpos = token2;
                if (jj_3R_132()) {
                    this.jj_scanpos = token2;
                    if (jj_3R_133()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private final boolean jj_3R_105() {
        return jj_3R_129();
    }

    private final boolean jj_3R_106() {
        return jj_3R_134();
    }

    private final boolean jj_3R_107() {
        return jj_3R_33() || jj_3R_34() || jj_3R_39();
    }

    private final boolean jj_3R_108() {
        if (jj_3R_135()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_156()) {
            this.jj_scanpos = token2;
        }
        return false;
    }

    private final boolean jj_3R_109() {
        Token token2 = this.jj_scanpos;
        if (jj_3_5()) {
            this.jj_scanpos = token2;
            if (jj_3R_136()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_110() {
        return jj_scan_token(79) || jj_3R_109();
    }

    private final boolean jj_3R_111() {
        return jj_scan_token(79) || jj_3R_29();
    }

    private final boolean jj_3R_112() {
        return jj_3R_39();
    }

    private final boolean jj_3R_113() {
        Token token2;
        if (jj_scan_token(50) || jj_scan_token(72) || jj_3R_39() || jj_scan_token(73) || jj_scan_token(74)) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_183());
        this.jj_scanpos = token2;
        return jj_scan_token(75);
    }

    private final boolean jj_3R_114() {
        if (jj_scan_token(32) || jj_scan_token(72) || jj_3R_39() || jj_scan_token(73) || jj_3R_45()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_184()) {
            this.jj_scanpos = token2;
        }
        return false;
    }

    private final boolean jj_3R_115() {
        return jj_scan_token(59) || jj_scan_token(72) || jj_3R_39() || jj_scan_token(73) || jj_3R_45();
    }

    private final boolean jj_3R_116() {
        return jj_scan_token(21) || jj_3R_45() || jj_scan_token(59) || jj_scan_token(72) || jj_3R_39() || jj_scan_token(73) || jj_scan_token(78);
    }

    private final boolean jj_3R_117() {
        if (jj_scan_token(30) || jj_scan_token(72)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_185()) {
            this.jj_scanpos = token2;
        }
        if (jj_scan_token(78)) {
            return true;
        }
        Token token3 = this.jj_scanpos;
        if (jj_3R_186()) {
            this.jj_scanpos = token3;
        }
        if (jj_scan_token(78)) {
            return true;
        }
        Token token4 = this.jj_scanpos;
        if (jj_3R_187()) {
            this.jj_scanpos = token4;
        }
        return jj_scan_token(73) || jj_3R_45();
    }

    private final boolean jj_3R_118() {
        Token token2 = this.jj_scanpos;
        if (jj_3_30()) {
            this.jj_scanpos = token2;
            if (jj_3R_137()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_119() {
        if (jj_scan_token(12)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(69)) {
            this.jj_scanpos = token2;
        }
        return jj_scan_token(78);
    }

    private final boolean jj_3R_120() {
        if (jj_scan_token(19)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(69)) {
            this.jj_scanpos = token2;
        }
        return jj_scan_token(78);
    }

    private final boolean jj_3R_121() {
        if (jj_scan_token(46)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_188()) {
            this.jj_scanpos = token2;
        }
        return jj_scan_token(78);
    }

    private final boolean jj_3R_122() {
        return jj_scan_token(51) || jj_scan_token(72) || jj_3R_39() || jj_scan_token(73) || jj_3R_38();
    }

    private final boolean jj_3R_123() {
        return jj_scan_token(53) || jj_3R_39() || jj_scan_token(78);
    }

    private final boolean jj_3R_124() {
        Token token2;
        if (jj_scan_token(56) || jj_3R_38()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_189());
        this.jj_scanpos = token2;
        Token token3 = this.jj_scanpos;
        if (jj_3R_190()) {
            this.jj_scanpos = token3;
        }
        return false;
    }

    private final boolean jj_3R_125() {
        return jj_scan_token(37);
    }

    private final boolean jj_3R_126() {
        return jj_scan_token(69);
    }

    private final boolean jj_3R_127() {
        return jj_3R_42() || jj_scan_token(69);
    }

    private final boolean jj_3R_128() {
        return jj_scan_token(34) || jj_scan_token(ParserConstants.STAR) || jj_scan_token(78);
    }

    private final boolean jj_3R_129() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_138()) {
            this.jj_scanpos = token2;
            if (jj_3R_139()) {
                this.jj_scanpos = token2;
                if (jj_3R_140()) {
                    this.jj_scanpos = token2;
                    if (jj_3R_141()) {
                        this.jj_scanpos = token2;
                        if (jj_3R_142()) {
                            this.jj_scanpos = token2;
                            if (jj_3R_143()) {
                                this.jj_scanpos = token2;
                                if (jj_3R_144()) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private final boolean jj_3R_130() {
        Token token2 = this.jj_scanpos;
        if (jj_3_18()) {
            this.jj_scanpos = token2;
            if (jj_3R_145()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_131() {
        return jj_scan_token(76) || jj_3R_39() || jj_scan_token(77);
    }

    private final boolean jj_3R_132() {
        if (jj_scan_token(80) || jj_scan_token(69)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_146()) {
            this.jj_scanpos = token2;
        }
        return false;
    }

    private final boolean jj_3R_133() {
        return jj_scan_token(74) || jj_3R_39() || jj_scan_token(75);
    }

    private final boolean jj_3R_134() {
        Token token2;
        if (jj_3R_39()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_147());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_135() {
        Token token2;
        if (jj_3R_148()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_159());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_136() {
        return jj_scan_token(69);
    }

    private final boolean jj_3R_137() {
        return jj_scan_token(30) || jj_scan_token(72) || jj_3R_32() || jj_scan_token(69) || jj_scan_token(89) || jj_3R_39() || jj_scan_token(73) || jj_3R_45();
    }

    private final boolean jj_3R_138() {
        return jj_scan_token(60);
    }

    private final boolean jj_3R_139() {
        return jj_scan_token(64);
    }

    private final boolean jj_3R_140() {
        return jj_scan_token(66);
    }

    private final boolean jj_3R_141() {
        return jj_scan_token(67);
    }

    private final boolean jj_3R_142() {
        return jj_3R_149();
    }

    private final boolean jj_3R_143() {
        return jj_scan_token(41);
    }

    private final boolean jj_3R_144() {
        return jj_scan_token(57);
    }

    private final boolean jj_3R_145() {
        if (jj_scan_token(40) || jj_3R_29()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_151()) {
            this.jj_scanpos = token2;
            return jj_3R_152();
        }
    }

    private final boolean jj_3R_146() {
        return jj_3R_69();
    }

    private final boolean jj_3R_147() {
        return jj_scan_token(79) || jj_3R_39();
    }

    private final boolean jj_3R_148() {
        Token token2;
        if (jj_3R_153()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_162());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_149() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_154()) {
            this.jj_scanpos = token2;
            if (jj_3R_155()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_150() {
        Token token2 = this.jj_scanpos;
        if (jj_3_21()) {
            this.jj_scanpos = token2;
            if (jj_3R_157()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_151() {
        return jj_3R_150();
    }

    private final boolean jj_3R_152() {
        if (jj_3R_69()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3_17()) {
            this.jj_scanpos = token2;
        }
        return false;
    }

    private final boolean jj_3R_153() {
        Token token2;
        if (jj_3R_158()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_165());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_154() {
        return jj_scan_token(55);
    }

    private final boolean jj_3R_155() {
        return jj_scan_token(26);
    }

    private final boolean jj_3R_156() {
        return jj_scan_token(88) || jj_3R_39() || jj_scan_token(89) || jj_3R_108();
    }

    private final boolean jj_3R_157() {
        Token token2;
        if (jj_3R_160()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_160());
        this.jj_scanpos = token2;
        return jj_3R_97();
    }

    private final boolean jj_3R_158() {
        Token token2;
        if (jj_3R_161()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_167());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_159() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(96)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(97)) {
                return true;
            }
        }
        return jj_3R_148();
    }

    private final boolean jj_3R_160() {
        return jj_scan_token(76) || jj_scan_token(77);
    }

    private final boolean jj_3R_161() {
        Token token2;
        if (jj_3R_164()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_169());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_162() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(98)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(99)) {
                return true;
            }
        }
        return jj_3R_153();
    }

    private final boolean jj_3R_163() {
        Token token2;
        if (jj_3R_31()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3_4());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_164() {
        Token token2;
        if (jj_3R_166()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_171());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_165() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(ParserConstants.BIT_OR)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(ParserConstants.BIT_ORX)) {
                return true;
            }
        }
        return jj_3R_158();
    }

    private final boolean jj_3R_166() {
        if (jj_3R_168()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_179()) {
            this.jj_scanpos = token2;
        }
        return false;
    }

    private final boolean jj_3R_167() {
        return jj_scan_token(ParserConstants.XOR) || jj_3R_161();
    }

    private final boolean jj_3R_168() {
        Token token2;
        if (jj_3R_170()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_182());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_169() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(ParserConstants.BIT_AND)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(ParserConstants.BIT_ANDX)) {
                return true;
            }
        }
        return jj_3R_164();
    }

    private final boolean jj_3R_170() {
        Token token2;
        if (jj_3R_178()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_192());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_171() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(90)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(95)) {
                return true;
            }
        }
        return jj_3R_166();
    }

    private final boolean jj_3R_172() {
        return jj_scan_token(25) || jj_3R_29();
    }

    private final boolean jj_3R_173() {
        return jj_scan_token(33) || jj_3R_76();
    }

    private final boolean jj_3R_174() {
        return jj_scan_token(54) || jj_3R_76();
    }

    private final boolean jj_3R_175() {
        return jj_3R_38();
    }

    private final boolean jj_3R_176() {
        if (jj_scan_token(69)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_180()) {
            this.jj_scanpos = token2;
        }
        return false;
    }

    private final boolean jj_3R_177() {
        return jj_scan_token(79) || jj_3R_176();
    }

    private final boolean jj_3R_178() {
        Token token2;
        if (jj_3R_181()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_200());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_179() {
        return jj_scan_token(35) || jj_3R_32();
    }

    private final boolean jj_3R_180() {
        return jj_scan_token(81) || jj_3R_31();
    }

    private final boolean jj_3R_181() {
        Token token2;
        if (jj_3R_191()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_209());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_182() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(84)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(85)) {
                this.jj_scanpos = token2;
                if (jj_scan_token(82)) {
                    this.jj_scanpos = token2;
                    if (jj_scan_token(83)) {
                        this.jj_scanpos = token2;
                        if (jj_scan_token(91)) {
                            this.jj_scanpos = token2;
                            if (jj_scan_token(92)) {
                                this.jj_scanpos = token2;
                                if (jj_scan_token(93)) {
                                    this.jj_scanpos = token2;
                                    if (jj_scan_token(94)) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return jj_3R_170();
    }

    private final boolean jj_3R_183() {
        Token token2;
        if (jj_3R_193()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3_29());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_184() {
        return jj_scan_token(23) || jj_3R_45();
    }

    private final boolean jj_3R_185() {
        return jj_3R_194();
    }

    private final boolean jj_3R_186() {
        return jj_3R_39();
    }

    private final boolean jj_3R_187() {
        return jj_3R_195();
    }

    private final boolean jj_3R_188() {
        return jj_3R_39();
    }

    private final boolean jj_3R_189() {
        return jj_scan_token(16) || jj_scan_token(72) || jj_3R_109() || jj_scan_token(73) || jj_3R_38();
    }

    private final boolean jj_3R_190() {
        return jj_scan_token(28) || jj_3R_38();
    }

    private final boolean jj_3R_191() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_196()) {
            this.jj_scanpos = token2;
            if (jj_3R_197()) {
                this.jj_scanpos = token2;
                if (jj_3R_198()) {
                    this.jj_scanpos = token2;
                    if (jj_3R_199()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private final boolean jj_3R_192() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(ParserConstants.LSHIFT)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(ParserConstants.LSHIFTX)) {
                this.jj_scanpos = token2;
                if (jj_scan_token(ParserConstants.RSIGNEDSHIFT)) {
                    this.jj_scanpos = token2;
                    if (jj_scan_token(ParserConstants.RSIGNEDSHIFTX)) {
                        this.jj_scanpos = token2;
                        if (jj_scan_token(ParserConstants.RUNSIGNEDSHIFT)) {
                            this.jj_scanpos = token2;
                            if (jj_scan_token(ParserConstants.RUNSIGNEDSHIFTX)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return jj_3R_178();
    }

    private final boolean jj_3R_193() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_201()) {
            this.jj_scanpos = token2;
            if (jj_3R_202()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_194() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_203()) {
            this.jj_scanpos = token2;
            if (jj_3R_204()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_195() {
        return jj_3R_205();
    }

    private final boolean jj_3R_196() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(ParserConstants.PLUS)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(ParserConstants.MINUS)) {
                return true;
            }
        }
        return jj_3R_191();
    }

    private final boolean jj_3R_197() {
        return jj_3R_206();
    }

    private final boolean jj_3R_198() {
        return jj_3R_207();
    }

    private final boolean jj_3R_199() {
        return jj_3R_208();
    }

    private final boolean jj_3R_200() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(ParserConstants.PLUS)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(ParserConstants.MINUS)) {
                return true;
            }
        }
        return jj_3R_181();
    }

    private final boolean jj_3R_201() {
        return jj_scan_token(15) || jj_3R_39() || jj_scan_token(89);
    }

    private final boolean jj_3R_202() {
        return jj_scan_token(20) || jj_scan_token(89);
    }

    private final boolean jj_3R_203() {
        return jj_3R_93();
    }

    private final boolean jj_3R_204() {
        return jj_3R_205();
    }

    private final boolean jj_3R_205() {
        Token token2;
        if (jj_3R_112()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_210());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_206() {
        return jj_scan_token(100) || jj_3R_33();
    }

    private final boolean jj_3R_207() {
        return jj_scan_token(ParserConstants.DECR) || jj_3R_33();
    }

    private final boolean jj_3R_208() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_211()) {
            this.jj_scanpos = token2;
            if (jj_3R_212()) {
                this.jj_scanpos = token2;
                if (jj_3R_213()) {
                    return true;
                }
            }
        }
        return false;
    }

    private final boolean jj_3R_209() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(ParserConstants.STAR)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(ParserConstants.SLASH)) {
                this.jj_scanpos = token2;
                if (jj_scan_token(ParserConstants.MOD)) {
                    return true;
                }
            }
        }
        return jj_3R_191();
    }

    private final boolean jj_3R_210() {
        return jj_scan_token(79) || jj_3R_112();
    }

    private final boolean jj_3R_211() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(87)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(86)) {
                return true;
            }
        }
        return jj_3R_191();
    }

    private final boolean jj_3R_212() {
        return jj_3R_214();
    }

    private final boolean jj_3R_213() {
        return jj_3R_215();
    }

    private final boolean jj_3R_214() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_216()) {
            this.jj_scanpos = token2;
            if (jj_3R_217()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_215() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_218()) {
            this.jj_scanpos = token2;
            if (jj_3R_219()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_216() {
        return jj_scan_token(72) || jj_3R_32() || jj_scan_token(73) || jj_3R_191();
    }

    private final boolean jj_3R_217() {
        return jj_scan_token(72) || jj_3R_32() || jj_scan_token(73) || jj_3R_208();
    }

    private final boolean jj_3R_218() {
        if (jj_3R_33()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(100)) {
            this.jj_scanpos = token2;
            return jj_scan_token(ParserConstants.DECR);
        }
    }

    private final boolean jj_3R_219() {
        return jj_3R_33();
    }

    private final boolean jj_3R_28() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_46()) {
            this.jj_scanpos = token2;
            if (jj_3R_47()) {
                this.jj_scanpos = token2;
                if (jj_3R_48()) {
                    this.jj_scanpos = token2;
                    if (jj_3R_49()) {
                        this.jj_scanpos = token2;
                        if (jj_3_28()) {
                            this.jj_scanpos = token2;
                            if (jj_3R_50()) {
                                this.jj_scanpos = token2;
                                if (jj_3R_51()) {
                                    this.jj_scanpos = token2;
                                    if (jj_3R_52()) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private final boolean jj_3R_29() {
        Token token2;
        if (jj_scan_token(69)) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3_7());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_30() {
        return jj_scan_token(80) || jj_scan_token(ParserConstants.STAR);
    }

    private final boolean jj_3R_31() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_53()) {
            this.jj_scanpos = token2;
            if (jj_3R_54()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_32() {
        Token token2;
        Token token3 = this.jj_scanpos;
        if (jj_3R_55()) {
            this.jj_scanpos = token3;
            if (jj_3R_56()) {
                return true;
            }
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3_6());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_33() {
        Token token2;
        if (jj_3R_57()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_58());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_34() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(81)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(ParserConstants.STARASSIGN)) {
                this.jj_scanpos = token2;
                if (jj_scan_token(ParserConstants.SLASHASSIGN)) {
                    this.jj_scanpos = token2;
                    if (jj_scan_token(ParserConstants.MODASSIGN)) {
                        this.jj_scanpos = token2;
                        if (jj_scan_token(ParserConstants.PLUSASSIGN)) {
                            this.jj_scanpos = token2;
                            if (jj_scan_token(ParserConstants.MINUSASSIGN)) {
                                this.jj_scanpos = token2;
                                if (jj_scan_token(ParserConstants.ANDASSIGN)) {
                                    this.jj_scanpos = token2;
                                    if (jj_scan_token(ParserConstants.XORASSIGN)) {
                                        this.jj_scanpos = token2;
                                        if (jj_scan_token(ParserConstants.ORASSIGN)) {
                                            this.jj_scanpos = token2;
                                            if (jj_scan_token(ParserConstants.LSHIFTASSIGN)) {
                                                this.jj_scanpos = token2;
                                                if (jj_scan_token(ParserConstants.LSHIFTASSIGNX)) {
                                                    this.jj_scanpos = token2;
                                                    if (jj_scan_token(ParserConstants.RSIGNEDSHIFTASSIGN)) {
                                                        this.jj_scanpos = token2;
                                                        if (jj_scan_token(ParserConstants.RSIGNEDSHIFTASSIGNX)) {
                                                            this.jj_scanpos = token2;
                                                            if (jj_scan_token(ParserConstants.RUNSIGNEDSHIFTASSIGN)) {
                                                                this.jj_scanpos = token2;
                                                                if (jj_scan_token(ParserConstants.RUNSIGNEDSHIFTASSIGNX)) {
                                                                    return true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private final boolean jj_3R_35() {
        Token token2 = this.jj_scanpos;
        if (jj_3_10()) {
            this.jj_scanpos = token2;
            if (jj_3R_59()) {
                this.jj_scanpos = token2;
                if (jj_3R_60()) {
                    return true;
                }
            }
        }
        return false;
    }

    private final boolean jj_3R_36() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_61()) {
            this.jj_scanpos = token2;
            if (jj_3R_62()) {
                this.jj_scanpos = token2;
                if (jj_3R_63()) {
                    this.jj_scanpos = token2;
                    if (jj_3R_64()) {
                        this.jj_scanpos = token2;
                        if (jj_3R_65()) {
                            this.jj_scanpos = token2;
                            if (jj_3R_66()) {
                                this.jj_scanpos = token2;
                                if (jj_3R_67()) {
                                    this.jj_scanpos = token2;
                                    if (jj_3R_68()) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private final boolean jj_3R_37() {
        return jj_3R_29() || jj_3R_69();
    }

    private final boolean jj_3R_38() {
        Token token2;
        Token token3 = this.jj_scanpos;
        if (jj_scan_token(48)) {
            this.jj_scanpos = token3;
        }
        if (jj_scan_token(74)) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3_23());
        this.jj_scanpos = token2;
        return jj_scan_token(75);
    }

    private final boolean jj_3R_39() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_70()) {
            this.jj_scanpos = token2;
            if (jj_3R_71()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_40() {
        return jj_scan_token(69) || jj_scan_token(89) || jj_3R_45();
    }

    private final boolean jj_3R_41() {
        Token token2;
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_72());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_42() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_73()) {
            this.jj_scanpos = token2;
            if (jj_3R_74()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_43() {
        if (jj_scan_token(72)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_75()) {
            this.jj_scanpos = token2;
        }
        return jj_scan_token(73);
    }

    private final boolean jj_3R_44() {
        return jj_scan_token(54) || jj_3R_76();
    }

    private final boolean jj_3R_45() {
        Token token2 = this.jj_scanpos;
        if (jj_3_22()) {
            this.jj_scanpos = token2;
            if (jj_3R_77()) {
                this.jj_scanpos = token2;
                if (jj_scan_token(78)) {
                    this.jj_scanpos = token2;
                    if (jj_3R_78()) {
                        this.jj_scanpos = token2;
                        if (jj_3R_79()) {
                            this.jj_scanpos = token2;
                            if (jj_3R_80()) {
                                this.jj_scanpos = token2;
                                if (jj_3R_81()) {
                                    this.jj_scanpos = token2;
                                    if (jj_3R_82()) {
                                        this.jj_scanpos = token2;
                                        this.lookingAhead = true;
                                        this.jj_semLA = isRegularForStatement();
                                        this.lookingAhead = false;
                                        if (!this.jj_semLA || jj_3R_83()) {
                                            this.jj_scanpos = token2;
                                            if (jj_3R_84()) {
                                                this.jj_scanpos = token2;
                                                if (jj_3R_85()) {
                                                    this.jj_scanpos = token2;
                                                    if (jj_3R_86()) {
                                                        this.jj_scanpos = token2;
                                                        if (jj_3R_87()) {
                                                            this.jj_scanpos = token2;
                                                            if (jj_3R_88()) {
                                                                this.jj_scanpos = token2;
                                                                if (jj_3R_89()) {
                                                                    this.jj_scanpos = token2;
                                                                    if (jj_3R_90()) {
                                                                        return true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private final boolean jj_3R_46() {
        return jj_3R_91();
    }

    private final boolean jj_3R_47() {
        return jj_3R_92();
    }

    private final boolean jj_3R_48() {
        return jj_3R_92();
    }

    private final boolean jj_3R_49() {
        return jj_3R_93() || jj_scan_token(78);
    }

    private final boolean jj_3R_50() {
        return jj_3R_94();
    }

    private final boolean jj_3R_51() {
        return jj_3R_95();
    }

    private final boolean jj_3R_52() {
        return jj_3R_96();
    }

    private final boolean jj_3R_53() {
        return jj_3R_97();
    }

    private final boolean jj_3R_54() {
        return jj_3R_39();
    }

    private final boolean jj_3R_55() {
        return jj_3R_36();
    }

    private final boolean jj_3R_56() {
        return jj_3R_29();
    }

    private final boolean jj_3R_57() {
        Token token2 = this.jj_scanpos;
        if (jj_3R_98()) {
            this.jj_scanpos = token2;
            if (jj_3R_99()) {
                this.jj_scanpos = token2;
                if (jj_3R_100()) {
                    this.jj_scanpos = token2;
                    if (jj_3R_101()) {
                        this.jj_scanpos = token2;
                        if (jj_3R_102()) {
                            this.jj_scanpos = token2;
                            if (jj_3R_103()) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private final boolean jj_3R_58() {
        return jj_3R_104();
    }

    private final boolean jj_3R_59() {
        return jj_scan_token(72) || jj_3R_29() || jj_scan_token(76) || jj_scan_token(77);
    }

    private final boolean jj_3R_60() {
        if (jj_scan_token(72) || jj_3R_29() || jj_scan_token(73)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(87)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(86)) {
                this.jj_scanpos = token2;
                if (jj_scan_token(72)) {
                    this.jj_scanpos = token2;
                    if (jj_scan_token(69)) {
                        this.jj_scanpos = token2;
                        if (jj_scan_token(40)) {
                            this.jj_scanpos = token2;
                            return jj_3R_105();
                        }
                    }
                }
            }
        }
    }

    private final boolean jj_3R_61() {
        return jj_scan_token(11);
    }

    private final boolean jj_3R_62() {
        return jj_scan_token(17);
    }

    private final boolean jj_3R_63() {
        return jj_scan_token(14);
    }

    private final boolean jj_3R_64() {
        return jj_scan_token(47);
    }

    private final boolean jj_3R_65() {
        return jj_scan_token(36);
    }

    private final boolean jj_3R_66() {
        return jj_scan_token(38);
    }

    private final boolean jj_3R_67() {
        return jj_scan_token(29);
    }

    private final boolean jj_3R_68() {
        return jj_scan_token(22);
    }

    private final boolean jj_3R_69() {
        if (jj_scan_token(72)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_106()) {
            this.jj_scanpos = token2;
        }
        return jj_scan_token(73);
    }

    private final boolean jj_3R_70() {
        return jj_3R_107();
    }

    private final boolean jj_3R_71() {
        return jj_3R_108();
    }

    private final boolean jj_3R_72() {
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(43)) {
            this.jj_scanpos = token2;
            if (jj_scan_token(44)) {
                this.jj_scanpos = token2;
                if (jj_scan_token(45)) {
                    this.jj_scanpos = token2;
                    if (jj_scan_token(51)) {
                        this.jj_scanpos = token2;
                        if (jj_scan_token(27)) {
                            this.jj_scanpos = token2;
                            if (jj_scan_token(39)) {
                                this.jj_scanpos = token2;
                                if (jj_scan_token(52)) {
                                    this.jj_scanpos = token2;
                                    if (jj_scan_token(58)) {
                                        this.jj_scanpos = token2;
                                        if (jj_scan_token(10)) {
                                            this.jj_scanpos = token2;
                                            if (jj_scan_token(48)) {
                                                this.jj_scanpos = token2;
                                                if (jj_scan_token(49)) {
                                                    return true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private final boolean jj_3R_73() {
        return jj_scan_token(57);
    }

    private final boolean jj_3R_74() {
        return jj_3R_32();
    }

    private final boolean jj_3R_75() {
        Token token2;
        if (jj_3R_109()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_110());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_76() {
        Token token2;
        if (jj_3R_29()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_111());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_77() {
        return jj_3R_38();
    }

    private final boolean jj_3R_78() {
        return jj_3R_112() || jj_scan_token(78);
    }

    private final boolean jj_3R_79() {
        return jj_3R_113();
    }

    private final boolean jj_3R_80() {
        return jj_3R_114();
    }

    private final boolean jj_3R_81() {
        return jj_3R_115();
    }

    private final boolean jj_3R_82() {
        return jj_3R_116();
    }

    private final boolean jj_3R_83() {
        return jj_3R_117();
    }

    private final boolean jj_3R_84() {
        return jj_3R_118();
    }

    private final boolean jj_3R_85() {
        return jj_3R_119();
    }

    private final boolean jj_3R_86() {
        return jj_3R_120();
    }

    private final boolean jj_3R_87() {
        return jj_3R_121();
    }

    private final boolean jj_3R_88() {
        return jj_3R_122();
    }

    private final boolean jj_3R_89() {
        return jj_3R_123();
    }

    private final boolean jj_3R_90() {
        return jj_3R_124();
    }

    private final boolean jj_3R_91() {
        if (jj_3R_41()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(13)) {
            this.jj_scanpos = token2;
            if (jj_3R_125()) {
                return true;
            }
        }
        if (jj_scan_token(69)) {
            return true;
        }
        Token token3 = this.jj_scanpos;
        if (jj_3R_172()) {
            this.jj_scanpos = token3;
        }
        Token token4 = this.jj_scanpos;
        if (jj_3R_173()) {
            this.jj_scanpos = token4;
        }
        return jj_3R_38();
    }

    private final boolean jj_3R_92() {
        if (jj_3R_41()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_126()) {
            this.jj_scanpos = token2;
            if (jj_3R_127()) {
                return true;
            }
        }
        if (jj_3R_43()) {
            return true;
        }
        Token token3 = this.jj_scanpos;
        if (jj_3R_174()) {
            this.jj_scanpos = token3;
        }
        Token token4 = this.jj_scanpos;
        if (jj_3R_175()) {
            this.jj_scanpos = token4;
            return jj_scan_token(78);
        }
    }

    private final boolean jj_3R_93() {
        Token token2;
        if (jj_3R_41() || jj_3R_32() || jj_3R_176()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3R_177());
        this.jj_scanpos = token2;
        return false;
    }

    private final boolean jj_3R_94() {
        Token token2 = this.jj_scanpos;
        if (jj_3_3()) {
            this.jj_scanpos = token2;
            if (jj_3R_128()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_95() {
        return jj_scan_token(42) || jj_3R_29();
    }

    private final boolean jj_3R_96() {
        return jj_scan_token(68);
    }

    private final boolean jj_3R_97() {
        if (jj_scan_token(74)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_163()) {
            this.jj_scanpos = token2;
        }
        Token token3 = this.jj_scanpos;
        if (jj_scan_token(79)) {
            this.jj_scanpos = token3;
        }
        return jj_scan_token(75);
    }

    private final boolean jj_3R_98() {
        return jj_3R_129();
    }

    private final boolean jj_3R_99() {
        return jj_scan_token(72) || jj_3R_39() || jj_scan_token(73);
    }

    private final boolean jj_3_1() {
        return jj_3R_28();
    }

    private final boolean jj_3_10() {
        return jj_scan_token(72) || jj_3R_36();
    }

    private final boolean jj_3_11() {
        return jj_scan_token(72) || jj_3R_29() || jj_scan_token(76);
    }

    private final boolean jj_3_12() {
        if (jj_3R_33()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(100)) {
            this.jj_scanpos = token2;
            return jj_scan_token(ParserConstants.DECR);
        }
    }

    private final boolean jj_3_13() {
        return jj_scan_token(72) || jj_3R_36();
    }

    private final boolean jj_3_14() {
        return jj_3R_37();
    }

    private final boolean jj_3_15() {
        return jj_3R_32() || jj_scan_token(80) || jj_scan_token(13);
    }

    private final boolean jj_3_16() {
        return jj_scan_token(80) || jj_scan_token(13);
    }

    private final boolean jj_3_17() {
        return jj_3R_38();
    }

    private final boolean jj_3_18() {
        return jj_scan_token(40) || jj_3R_36() || jj_3R_150();
    }

    private final boolean jj_3_19() {
        return jj_scan_token(76) || jj_3R_39() || jj_scan_token(77);
    }

    private final boolean jj_3_2() {
        return jj_scan_token(69) || jj_scan_token(72);
    }

    private final boolean jj_3_20() {
        return jj_scan_token(76) || jj_scan_token(77);
    }

    private final boolean jj_3_21() {
        Token token2;
        Token token3;
        if (jj_3_19()) {
            return true;
        }
        do {
            token2 = this.jj_scanpos;
        } while (!jj_3_19());
        this.jj_scanpos = token2;
        do {
            token3 = this.jj_scanpos;
        } while (!jj_3_20());
        this.jj_scanpos = token3;
        return false;
    }

    private final boolean jj_3_22() {
        return jj_3R_40();
    }

    private final boolean jj_3_23() {
        return jj_3R_28();
    }

    private final boolean jj_3_24() {
        if (jj_3R_41()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(13)) {
            this.jj_scanpos = token2;
            return jj_scan_token(37);
        }
    }

    private final boolean jj_3_25() {
        return jj_3R_41() || jj_3R_42() || jj_scan_token(69) || jj_scan_token(72);
    }

    private final boolean jj_3_26() {
        if (jj_3R_41() || jj_scan_token(69) || jj_3R_43()) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_3R_44()) {
            this.jj_scanpos = token2;
        }
        return jj_scan_token(74);
    }

    private final boolean jj_3_27() {
        return jj_3R_41() || jj_3R_32() || jj_scan_token(69);
    }

    private final boolean jj_3_28() {
        return jj_3R_45();
    }

    private final boolean jj_3_29() {
        return jj_3R_28();
    }

    private final boolean jj_3_3() {
        if (jj_scan_token(34)) {
            return true;
        }
        Token token2 = this.jj_scanpos;
        if (jj_scan_token(48)) {
            this.jj_scanpos = token2;
        }
        if (jj_3R_29()) {
            return true;
        }
        Token token3 = this.jj_scanpos;
        if (jj_3R_30()) {
            this.jj_scanpos = token3;
        }
        return jj_scan_token(78);
    }

    private final boolean jj_3_30() {
        return jj_scan_token(30) || jj_scan_token(72) || jj_scan_token(69) || jj_scan_token(89) || jj_3R_39() || jj_scan_token(73) || jj_3R_45();
    }

    private final boolean jj_3_31() {
        return jj_3R_41() || jj_3R_32() || jj_scan_token(69);
    }

    private final boolean jj_3_4() {
        return jj_scan_token(79) || jj_3R_31();
    }

    private final boolean jj_3_5() {
        return jj_3R_32() || jj_scan_token(69);
    }

    private final boolean jj_3_6() {
        return jj_scan_token(76) || jj_scan_token(77);
    }

    private final boolean jj_3_7() {
        return jj_scan_token(80) || jj_scan_token(69);
    }

    private final boolean jj_3_8() {
        return jj_3R_33() || jj_3R_34();
    }

    private final boolean jj_3_9() {
        return jj_3R_35();
    }

    private void jj_add_error_token(int i, int i2) {
        boolean z;
        if (i2 < 100) {
            if (i2 == this.jj_endpos + 1) {
                int[] iArr = this.jj_lasttokens;
                int i3 = this.jj_endpos;
                this.jj_endpos = i3 + 1;
                iArr[i3] = i;
            } else if (this.jj_endpos != 0) {
                this.jj_expentry = new int[this.jj_endpos];
                for (int i4 = 0; i4 < this.jj_endpos; i4++) {
                    this.jj_expentry[i4] = this.jj_lasttokens[i4];
                }
                Enumeration elements = this.jj_expentries.elements();
                boolean z2 = false;
                while (true) {
                    if (!elements.hasMoreElements()) {
                        z = z2;
                        break;
                    }
                    int[] iArr2 = (int[]) elements.nextElement();
                    if (iArr2.length == this.jj_expentry.length) {
                        int i5 = 0;
                        while (true) {
                            if (i5 >= this.jj_expentry.length) {
                                z = true;
                                break;
                            } else if (iArr2[i5] != this.jj_expentry[i5]) {
                                z = false;
                                break;
                            } else {
                                i5++;
                            }
                        }
                        if (z) {
                            break;
                        }
                    } else {
                        z = z2;
                    }
                    z2 = z;
                }
                if (!z) {
                    this.jj_expentries.addElement(this.jj_expentry);
                }
                if (i2 != 0) {
                    int[] iArr3 = this.jj_lasttokens;
                    this.jj_endpos = i2;
                    iArr3[i2 - 1] = i;
                }
            }
        }
    }

    private final Token jj_consume_token(int i) {
        Token token2 = this.token;
        if (token2.next != null) {
            this.token = this.token.next;
        } else {
            Token token3 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token3.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        if (this.token.kind == i) {
            this.jj_gen++;
            int i2 = this.jj_gc + 1;
            this.jj_gc = i2;
            if (i2 > 100) {
                this.jj_gc = 0;
                for (JJCalls jJCalls : this.jj_2_rtns) {
                    while (jJCalls != null) {
                        if (jJCalls.gen < this.jj_gen) {
                            jJCalls.first = null;
                        }
                        jJCalls = jJCalls.next;
                    }
                }
            }
            return this.token;
        }
        this.token = token2;
        this.jj_kind = i;
        throw generateParseException();
    }

    private static void jj_la1_0() {
        jj_la1_0 = new int[]{1, 134218752, 134218752, 8192, 33554432, 0, 541214720, 0, 0, 0, 0, 0, 0, 608323584, 608323584, 0, 0, 541214720, 0, 541214720, 541214720, 541214720, 0, 608323584, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 608323584, 0, 0, 608323584, 67108864, 0, 0, 608323584, 0, 0, 67108864, 0, 0, 0, 67108864, 67108864, 608323584, 0, 0, 0, 0, 0, 610420736, 1074270208, 0, 0, 1081344, 1081344, 8388608, 742542336, 608323584, 608323584, 1073741824, 608323584, 0, 0, 0, 0, 608323584, 65536, 268435456};
    }

    private static void jj_la1_1() {
        jj_la1_1 = new int[]{0, 68892800, 68892800, 32, 0, 2, 33587280, 4194304, 65536, 65536, 0, 4, 0, 310412112, 310412112, 0, 0, 32848, 0, 32848, 33587280, 32848, 0, 310412112, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 310412112, 0, 0, 310412112, 310379264, 0, 0, 310412112, 0, 0, 310379264, 0, 0, 0, 310379008, 8388608, 310412112, 0, 0, 256, 0, 0, 444957521, 19415040, 65536, 1028, 0, 0, 0, 379304912, 310412112, 310412112, 0, 310412112, 0, 0, 0, 0, 310412112, 0, 0};
    }

    private static void jj_la1_2() {
        jj_la1_2 = new int[]{0, 0, 0, 0, 0, 0, 32, 0, 17408, 0, 65536, 0, 131072, 12584237, 12584237, 32768, 32768, 32, 32, 32, 32, 0, 32768, 12583213, 131072, 16777216, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2080374784, -2080374784, 0, 2017198080, 2017198080, 0, 0, 0, 0, 0, 0, 0, 12583213, 12582912, 12582912, 301, 12583213, 256, 0, 301, 256, 70656, 269, 32, 256, 70656, 13, 0, 12583213, 32768, 4352, 0, 4096, 4096, 12600621, 0, 0, 16, 0, 0, 0, 12583213, 12583213, 12583213, 0, 12583213, 32768, 32768, 32, 32, 12583213, 0, 0};
    }

    private static void jj_la1_3() {
        jj_la1_3 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 240, 240, 0, 0, 0, 0, 0, 0, 0, 0, 240, -675282944, 0, 3, 3, 12, 12, 12288, 12288, 16384, 3072, 3072, 0, 0, 0, 0, 0, 4128768, 4128768, 192, 192, 33536, 33536, 192, 240, 0, 0, 0, 0, 0, 48, 0, 0, 0, 0, 0, 0, 0, 0, 0, 240, 0, 0, 0, 0, 0, 240, 0, 0, 0, 0, 0, 0, 240, 240, 240, 0, 240, 0, 0, 0, 0, 240, 0, 0};
    }

    private static void jj_la1_4() {
        jj_la1_4 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    }

    private final int jj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token3.next = nextToken;
            int i = nextToken.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    private final void jj_rescan_token() {
        this.jj_rescan = true;
        for (int i = 0; i < 31; i++) {
            JJCalls jJCalls = this.jj_2_rtns[i];
            do {
                if (jJCalls.gen > this.jj_gen) {
                    this.jj_la = jJCalls.arg;
                    Token token2 = jJCalls.first;
                    this.jj_scanpos = token2;
                    this.jj_lastpos = token2;
                    switch (i) {
                        case 0:
                            jj_3_1();
                            break;
                        case 1:
                            jj_3_2();
                            break;
                        case 2:
                            jj_3_3();
                            break;
                        case 3:
                            jj_3_4();
                            break;
                        case ParserTreeConstants.JJTIMPORTDECLARATION:
                            jj_3_5();
                            break;
                        case ParserTreeConstants.JJTVARIABLEDECLARATOR:
                            jj_3_6();
                            break;
                        case 6:
                            jj_3_7();
                            break;
                        case 7:
                            jj_3_8();
                            break;
                        case 8:
                            jj_3_9();
                            break;
                        case 9:
                            jj_3_10();
                            break;
                        case 10:
                            jj_3_11();
                            break;
                        case 11:
                            jj_3_12();
                            break;
                        case 12:
                            jj_3_13();
                            break;
                        case 13:
                            jj_3_14();
                            break;
                        case 14:
                            jj_3_15();
                            break;
                        case 15:
                            jj_3_16();
                            break;
                        case 16:
                            jj_3_17();
                            break;
                        case 17:
                            jj_3_18();
                            break;
                        case 18:
                            jj_3_19();
                            break;
                        case 19:
                            jj_3_20();
                            break;
                        case 20:
                            jj_3_21();
                            break;
                        case 21:
                            jj_3_22();
                            break;
                        case 22:
                            jj_3_23();
                            break;
                        case 23:
                            jj_3_24();
                            break;
                        case 24:
                            jj_3_25();
                            break;
                        case 25:
                            jj_3_26();
                            break;
                        case 26:
                            jj_3_27();
                            break;
                        case 27:
                            jj_3_28();
                            break;
                        case 28:
                            jj_3_29();
                            break;
                        case 29:
                            jj_3_30();
                            break;
                        case 30:
                            jj_3_31();
                            break;
                    }
                }
                jJCalls = jJCalls.next;
            } while (jJCalls != null);
        }
        this.jj_rescan = false;
    }

    private final void jj_save(int i, int i2) {
        JJCalls jJCalls = this.jj_2_rtns[i];
        while (true) {
            if (jJCalls.gen <= this.jj_gen) {
                break;
            } else if (jJCalls.next == null) {
                JJCalls jJCalls2 = new JJCalls();
                jJCalls.next = jJCalls2;
                jJCalls = jJCalls2;
                break;
            } else {
                jJCalls = jJCalls.next;
            }
        }
        jJCalls.gen = (this.jj_gen + i2) - this.jj_la;
        jJCalls.first = this.token;
        jJCalls.arg = i2;
    }

    private final boolean jj_scan_token(int i) {
        if (this.jj_scanpos == this.jj_lastpos) {
            this.jj_la--;
            if (this.jj_scanpos.next == null) {
                Token token2 = this.jj_scanpos;
                Token nextToken = this.token_source.getNextToken();
                token2.next = nextToken;
                this.jj_scanpos = nextToken;
                this.jj_lastpos = nextToken;
            } else {
                Token token3 = this.jj_scanpos.next;
                this.jj_scanpos = token3;
                this.jj_lastpos = token3;
            }
        } else {
            this.jj_scanpos = this.jj_scanpos.next;
        }
        if (this.jj_rescan) {
            Token token4 = this.token;
            int i2 = 0;
            while (token4 != null && token4 != this.jj_scanpos) {
                i2++;
                token4 = token4.next;
            }
            if (token4 != null) {
                jj_add_error_token(i, i2);
            }
        }
        if (this.jj_scanpos.kind != i) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_scanpos != this.jj_lastpos) {
            return false;
        }
        throw this.jj_ls;
    }

    public static void main(String[] strArr) {
        boolean z;
        int i = 0;
        if (strArr[0].equals("-p")) {
            i = 1;
            z = true;
        } else {
            z = false;
        }
        while (i < strArr.length) {
            Parser parser = new Parser(new FileReader(strArr[i]));
            parser.setRetainComments(true);
            while (!parser.Line()) {
                if (z) {
                    System.out.println(parser.popNode());
                }
            }
            i++;
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0010 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH] */
    public final void AdditiveExpression() {
        /*
            r7 = this;
            r6 = 2
            r5 = -1
            r7.MultiplicativeExpression()
        L_0x0005:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0019
            int r0 = r7.jj_ntk()
        L_0x000d:
            switch(r0) {
                case 102: goto L_0x001c;
                case 103: goto L_0x001c;
                default: goto L_0x0010;
            }
        L_0x0010:
            int[] r0 = r7.jj_la1
            r1 = 42
            int r2 = r7.jj_gen
            r0[r1] = r2
            return
        L_0x0019:
            int r0 = r7.jj_ntk
            goto L_0x000d
        L_0x001c:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0038
            int r0 = r7.jj_ntk()
        L_0x0024:
            switch(r0) {
                case 102: goto L_0x003b;
                case 103: goto L_0x006f;
                default: goto L_0x0027;
            }
        L_0x0027:
            int[] r0 = r7.jj_la1
            r1 = 43
            int r2 = r7.jj_gen
            r0[r1] = r2
            r7.jj_consume_token(r5)
            bsh.ParseException r0 = new bsh.ParseException
            r0.<init>()
            throw r0
        L_0x0038:
            int r0 = r7.jj_ntk
            goto L_0x0024
        L_0x003b:
            r0 = 102(0x66, float:1.43E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
        L_0x0041:
            r7.MultiplicativeExpression()
            bsh.BSHBinaryExpression r2 = new bsh.BSHBinaryExpression
            r1 = 15
            r2.<init>(r1)
            r1 = 1
            bsh.JJTParserState r3 = r7.jjtree
            r3.openNodeScope(r2)
            r7.jjtreeOpenNodeScope(r2)
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x0063 }
            r4 = 2
            r3.closeNodeScope(r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 0
            r7.jjtreeCloseNodeScope(r2)     // Catch:{ all -> 0x0063 }
            int r0 = r0.kind     // Catch:{ all -> 0x0063 }
            r2.kind = r0     // Catch:{ all -> 0x0063 }
            goto L_0x0005
        L_0x0063:
            r0 = move-exception
            if (r1 == 0) goto L_0x006e
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r2, r6)
            r7.jjtreeCloseNodeScope(r2)
        L_0x006e:
            throw r0
        L_0x006f:
            r0 = 103(0x67, float:1.44E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.AdditiveExpression():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHAllocationExpression, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void AllocationExpression() {
        /*
            r5 = this;
            r2 = 1
            r1 = -1
            bsh.BSHAllocationExpression r3 = new bsh.BSHAllocationExpression
            r0 = 23
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 2
            boolean r0 = r5.jj_2_18(r0)     // Catch:{ Throwable -> 0x0049 }
            if (r0 == 0) goto L_0x002c
            r0 = 40
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0049 }
            r5.PrimitiveType()     // Catch:{ Throwable -> 0x0049 }
            r5.ArrayDimensions()     // Catch:{ Throwable -> 0x0049 }
        L_0x0023:
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x002c:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0049 }
            if (r0 != r1) goto L_0x0063
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x0049 }
        L_0x0034:
            switch(r0) {
                case 40: goto L_0x0066;
                default: goto L_0x0037;
            }     // Catch:{ Throwable -> 0x0049 }
        L_0x0037:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x0049 }
            r1 = 66
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x0049 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x0049 }
            r0 = -1
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0049 }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x0049 }
            r0.<init>()     // Catch:{ Throwable -> 0x0049 }
            throw r0     // Catch:{ Throwable -> 0x0049 }
        L_0x0049:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x008b }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x008b }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0057 }
            if (r4 == 0) goto L_0x00a3
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0057 }
            throw r0     // Catch:{ all -> 0x0057 }
        L_0x0057:
            r0 = move-exception
        L_0x0058:
            if (r1 == 0) goto L_0x0062
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0062:
            throw r0
        L_0x0063:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0049 }
            goto L_0x0034
        L_0x0066:
            r0 = 40
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0049 }
            r5.AmbiguousName()     // Catch:{ Throwable -> 0x0049 }
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0049 }
            if (r0 != r1) goto L_0x008e
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x0049 }
        L_0x0076:
            switch(r0) {
                case 72: goto L_0x0095;
                case 76: goto L_0x0091;
                default: goto L_0x0079;
            }     // Catch:{ Throwable -> 0x0049 }
        L_0x0079:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x0049 }
            r1 = 65
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x0049 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x0049 }
            r0 = -1
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0049 }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x0049 }
            r0.<init>()     // Catch:{ Throwable -> 0x0049 }
            throw r0     // Catch:{ Throwable -> 0x0049 }
        L_0x008b:
            r0 = move-exception
            r1 = r2
            goto L_0x0058
        L_0x008e:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0049 }
            goto L_0x0076
        L_0x0091:
            r5.ArrayDimensions()     // Catch:{ Throwable -> 0x0049 }
            goto L_0x0023
        L_0x0095:
            r5.Arguments()     // Catch:{ Throwable -> 0x0049 }
            r0 = 2
            boolean r0 = r5.jj_2_17(r0)     // Catch:{ Throwable -> 0x0049 }
            if (r0 == 0) goto L_0x0023
            r5.Block()     // Catch:{ Throwable -> 0x0049 }
            goto L_0x0023
        L_0x00a3:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0057 }
            if (r4 == 0) goto L_0x00aa
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0057 }
            throw r0     // Catch:{ all -> 0x0057 }
        L_0x00aa:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0057 }
            throw r0     // Catch:{ all -> 0x0057 }
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.AllocationExpression():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHAmbiguousName, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x004c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void AmbiguousName() {
        /*
            r6 = this;
            r2 = 1
            bsh.BSHAmbiguousName r3 = new bsh.BSHAmbiguousName
            r0 = 12
            r3.<init>(r0)
            bsh.JJTParserState r0 = r6.jjtree
            r0.openNodeScope(r3)
            r6.jjtreeOpenNodeScope(r3)
            r0 = 69
            bsh.Token r0 = r6.jj_consume_token(r0)     // Catch:{ all -> 0x0048 }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ all -> 0x0048 }
            java.lang.String r0 = r0.image     // Catch:{ all -> 0x0048 }
            r4.<init>(r0)     // Catch:{ all -> 0x0048 }
        L_0x001d:
            r0 = 2
            boolean r0 = r6.jj_2_7(r0)     // Catch:{ all -> 0x0048 }
            if (r0 == 0) goto L_0x0055
            r0 = 80
            r6.jj_consume_token(r0)     // Catch:{ all -> 0x0048 }
            r0 = 69
            bsh.Token r0 = r6.jj_consume_token(r0)     // Catch:{ all -> 0x0048 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0048 }
            r1.<init>()     // Catch:{ all -> 0x0048 }
            java.lang.String r5 = "."
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x0048 }
            java.lang.String r0 = r0.image     // Catch:{ all -> 0x0048 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0048 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0048 }
            r4.append(r0)     // Catch:{ all -> 0x0048 }
            goto L_0x001d
        L_0x0048:
            r0 = move-exception
            r1 = r2
        L_0x004a:
            if (r1 == 0) goto L_0x0054
            bsh.JJTParserState r1 = r6.jjtree
            r1.closeNodeScope(r3, r2)
            r6.jjtreeCloseNodeScope(r3)
        L_0x0054:
            throw r0
        L_0x0055:
            bsh.JJTParserState r0 = r6.jjtree     // Catch:{ all -> 0x0048 }
            r1 = 1
            r0.closeNodeScope(r3, r1)     // Catch:{ all -> 0x0048 }
            r1 = 0
            r6.jjtreeCloseNodeScope(r3)     // Catch:{ all -> 0x0066 }
            java.lang.String r0 = r4.toString()     // Catch:{ all -> 0x0066 }
            r3.text = r0     // Catch:{ all -> 0x0066 }
            return
        L_0x0066:
            r0 = move-exception
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.AmbiguousName():void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0010 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH] */
    public final void AndExpression() {
        /*
            r7 = this;
            r6 = 2
            r5 = -1
            r7.EqualityExpression()
        L_0x0005:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0019
            int r0 = r7.jj_ntk()
        L_0x000d:
            switch(r0) {
                case 106: goto L_0x001c;
                case 107: goto L_0x001c;
                default: goto L_0x0010;
            }
        L_0x0010:
            int[] r0 = r7.jj_la1
            r1 = 33
            int r2 = r7.jj_gen
            r0[r1] = r2
            return
        L_0x0019:
            int r0 = r7.jj_ntk
            goto L_0x000d
        L_0x001c:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0038
            int r0 = r7.jj_ntk()
        L_0x0024:
            switch(r0) {
                case 106: goto L_0x003b;
                case 107: goto L_0x006f;
                default: goto L_0x0027;
            }
        L_0x0027:
            int[] r0 = r7.jj_la1
            r1 = 34
            int r2 = r7.jj_gen
            r0[r1] = r2
            r7.jj_consume_token(r5)
            bsh.ParseException r0 = new bsh.ParseException
            r0.<init>()
            throw r0
        L_0x0038:
            int r0 = r7.jj_ntk
            goto L_0x0024
        L_0x003b:
            r0 = 106(0x6a, float:1.49E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
        L_0x0041:
            r7.EqualityExpression()
            bsh.BSHBinaryExpression r2 = new bsh.BSHBinaryExpression
            r1 = 15
            r2.<init>(r1)
            r1 = 1
            bsh.JJTParserState r3 = r7.jjtree
            r3.openNodeScope(r2)
            r7.jjtreeOpenNodeScope(r2)
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x0063 }
            r4 = 2
            r3.closeNodeScope(r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 0
            r7.jjtreeCloseNodeScope(r2)     // Catch:{ all -> 0x0063 }
            int r0 = r0.kind     // Catch:{ all -> 0x0063 }
            r2.kind = r0     // Catch:{ all -> 0x0063 }
            goto L_0x0005
        L_0x0063:
            r0 = move-exception
            if (r1 == 0) goto L_0x006e
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r2, r6)
            r7.jjtreeCloseNodeScope(r2)
        L_0x006e:
            throw r0
        L_0x006f:
            r0 = 107(0x6b, float:1.5E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.AndExpression():void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0008  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0018  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001b A[FALL_THROUGH, LOOP:0: B:1:0x0003->B:8:0x001b, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x000f A[SYNTHETIC] */
    public final void ArgumentList() {
        /*
            r3 = this;
            r3.Expression()
        L_0x0003:
            int r0 = r3.jj_ntk
            r1 = -1
            if (r0 != r1) goto L_0x0018
            int r0 = r3.jj_ntk()
        L_0x000c:
            switch(r0) {
                case 79: goto L_0x001b;
                default: goto L_0x000f;
            }
        L_0x000f:
            int[] r0 = r3.jj_la1
            r1 = 64
            int r2 = r3.jj_gen
            r0[r1] = r2
            return
        L_0x0018:
            int r0 = r3.jj_ntk
            goto L_0x000c
        L_0x001b:
            r0 = 79
            r3.jj_consume_token(r0)
            r3.Expression()
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ArgumentList():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHArguments, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Arguments() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHArguments r3 = new bsh.BSHArguments
            r0 = 22
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 72
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x003e }
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x003e }
            r1 = -1
            if (r0 != r1) goto L_0x0037
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x003e }
        L_0x001e:
            switch(r0) {
                case 11: goto L_0x003a;
                case 14: goto L_0x003a;
                case 17: goto L_0x003a;
                case 22: goto L_0x003a;
                case 26: goto L_0x003a;
                case 29: goto L_0x003a;
                case 36: goto L_0x003a;
                case 38: goto L_0x003a;
                case 40: goto L_0x003a;
                case 41: goto L_0x003a;
                case 47: goto L_0x003a;
                case 55: goto L_0x003a;
                case 57: goto L_0x003a;
                case 60: goto L_0x003a;
                case 64: goto L_0x003a;
                case 66: goto L_0x003a;
                case 67: goto L_0x003a;
                case 69: goto L_0x003a;
                case 72: goto L_0x003a;
                case 86: goto L_0x003a;
                case 87: goto L_0x003a;
                case 100: goto L_0x003a;
                case 101: goto L_0x003a;
                case 102: goto L_0x003a;
                case 103: goto L_0x003a;
                default: goto L_0x0021;
            }     // Catch:{ Throwable -> 0x003e }
        L_0x0021:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x003e }
            r1 = 63
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x003e }
            r0[r1] = r4     // Catch:{ Throwable -> 0x003e }
        L_0x0029:
            r0 = 73
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x003e }
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x0037:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x003e }
            goto L_0x001e
        L_0x003a:
            r5.ArgumentList()     // Catch:{ Throwable -> 0x003e }
            goto L_0x0029
        L_0x003e:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x0062 }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x0062 }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x004c }
            if (r4 == 0) goto L_0x0058
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x004c }
            throw r0     // Catch:{ all -> 0x004c }
        L_0x004c:
            r0 = move-exception
        L_0x004d:
            if (r1 == 0) goto L_0x0057
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0057:
            throw r0
        L_0x0058:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x004c }
            if (r4 == 0) goto L_0x005f
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x004c }
            throw r0     // Catch:{ all -> 0x004c }
        L_0x005f:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x004c }
            throw r0     // Catch:{ all -> 0x004c }
        L_0x0062:
            r0 = move-exception
            r1 = r2
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.Arguments():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHArrayDimensions, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0081 A[Catch:{ Throwable -> 0x0044, all -> 0x007b, all -> 0x0052 }, LOOP_START] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ArrayDimensions() {
        /*
            r5 = this;
            r2 = 1
            r1 = -1
            bsh.BSHArrayDimensions r3 = new bsh.BSHArrayDimensions
            r0 = 24
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 2
            boolean r0 = r5.jj_2_21(r0)     // Catch:{ Throwable -> 0x0044 }
            if (r0 == 0) goto L_0x005e
        L_0x0018:
            r0 = 76
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0044 }
            r5.Expression()     // Catch:{ Throwable -> 0x0044 }
            r0 = 77
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0044 }
            r3.addDefinedDimension()     // Catch:{ Throwable -> 0x0044 }
            r0 = 2
            boolean r0 = r5.jj_2_19(r0)     // Catch:{ Throwable -> 0x0044 }
            if (r0 != 0) goto L_0x0018
        L_0x002f:
            r0 = 2
            boolean r0 = r5.jj_2_20(r0)     // Catch:{ Throwable -> 0x0044 }
            if (r0 == 0) goto L_0x00a4
            r0 = 76
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0044 }
            r0 = 77
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0044 }
            r3.addUndefinedDimension()     // Catch:{ Throwable -> 0x0044 }
            goto L_0x002f
        L_0x0044:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x007b }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x007b }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0052 }
            if (r4 == 0) goto L_0x00b0
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0052 }
            throw r0     // Catch:{ all -> 0x0052 }
        L_0x0052:
            r0 = move-exception
        L_0x0053:
            if (r1 == 0) goto L_0x005d
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x005d:
            throw r0
        L_0x005e:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0044 }
            if (r0 != r1) goto L_0x007e
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x0044 }
        L_0x0066:
            switch(r0) {
                case 76: goto L_0x0081;
                default: goto L_0x0069;
            }     // Catch:{ Throwable -> 0x0044 }
        L_0x0069:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x0044 }
            r1 = 68
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x0044 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x0044 }
            r0 = -1
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0044 }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x0044 }
            r0.<init>()     // Catch:{ Throwable -> 0x0044 }
            throw r0     // Catch:{ Throwable -> 0x0044 }
        L_0x007b:
            r0 = move-exception
            r1 = r2
            goto L_0x0053
        L_0x007e:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0044 }
            goto L_0x0066
        L_0x0081:
            r0 = 76
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0044 }
            r0 = 77
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0044 }
            r3.addUndefinedDimension()     // Catch:{ Throwable -> 0x0044 }
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0044 }
            if (r0 != r1) goto L_0x00ad
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x0044 }
        L_0x0096:
            switch(r0) {
                case 76: goto L_0x0081;
                default: goto L_0x0099;
            }     // Catch:{ Throwable -> 0x0044 }
        L_0x0099:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x0044 }
            r1 = 67
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x0044 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x0044 }
            r5.ArrayInitializer()     // Catch:{ Throwable -> 0x0044 }
        L_0x00a4:
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x00ad:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0044 }
            goto L_0x0096
        L_0x00b0:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0052 }
            if (r4 == 0) goto L_0x00b7
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0052 }
            throw r0     // Catch:{ all -> 0x0052 }
        L_0x00b7:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0052 }
            throw r0     // Catch:{ all -> 0x0052 }
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ArrayDimensions():void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHArrayInitializer, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ArrayInitializer() {
        /*
            r6 = this;
            r5 = -1
            r2 = 1
            bsh.BSHArrayInitializer r3 = new bsh.BSHArrayInitializer
            r0 = 6
            r3.<init>(r0)
            bsh.JJTParserState r0 = r6.jjtree
            r0.openNodeScope(r3)
            r6.jjtreeOpenNodeScope(r3)
            r0 = 74
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005c }
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x005c }
            if (r0 != r5) goto L_0x0049
            int r0 = r6.jj_ntk()     // Catch:{ Throwable -> 0x005c }
        L_0x001d:
            switch(r0) {
                case 11: goto L_0x004c;
                case 14: goto L_0x004c;
                case 17: goto L_0x004c;
                case 22: goto L_0x004c;
                case 26: goto L_0x004c;
                case 29: goto L_0x004c;
                case 36: goto L_0x004c;
                case 38: goto L_0x004c;
                case 40: goto L_0x004c;
                case 41: goto L_0x004c;
                case 47: goto L_0x004c;
                case 55: goto L_0x004c;
                case 57: goto L_0x004c;
                case 60: goto L_0x004c;
                case 64: goto L_0x004c;
                case 66: goto L_0x004c;
                case 67: goto L_0x004c;
                case 69: goto L_0x004c;
                case 72: goto L_0x004c;
                case 74: goto L_0x004c;
                case 86: goto L_0x004c;
                case 87: goto L_0x004c;
                case 100: goto L_0x004c;
                case 101: goto L_0x004c;
                case 102: goto L_0x004c;
                case 103: goto L_0x004c;
                default: goto L_0x0020;
            }     // Catch:{ Throwable -> 0x005c }
        L_0x0020:
            int[] r0 = r6.jj_la1     // Catch:{ Throwable -> 0x005c }
            r1 = 14
            int r4 = r6.jj_gen     // Catch:{ Throwable -> 0x005c }
            r0[r1] = r4     // Catch:{ Throwable -> 0x005c }
        L_0x0028:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x005c }
            if (r0 != r5) goto L_0x0076
            int r0 = r6.jj_ntk()     // Catch:{ Throwable -> 0x005c }
        L_0x0030:
            switch(r0) {
                case 79: goto L_0x0079;
                default: goto L_0x0033;
            }     // Catch:{ Throwable -> 0x005c }
        L_0x0033:
            int[] r0 = r6.jj_la1     // Catch:{ Throwable -> 0x005c }
            r1 = 15
            int r4 = r6.jj_gen     // Catch:{ Throwable -> 0x005c }
            r0[r1] = r4     // Catch:{ Throwable -> 0x005c }
        L_0x003b:
            r0 = 75
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005c }
            bsh.JJTParserState r0 = r6.jjtree
            r0.closeNodeScope(r3, r2)
            r6.jjtreeCloseNodeScope(r3)
            return
        L_0x0049:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x005c }
            goto L_0x001d
        L_0x004c:
            r6.VariableInitializer()     // Catch:{ Throwable -> 0x005c }
            r0 = 2
            boolean r0 = r6.jj_2_4(r0)     // Catch:{ Throwable -> 0x005c }
            if (r0 == 0) goto L_0x0028
            r0 = 79
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005c }
            goto L_0x004c
        L_0x005c:
            r0 = move-exception
            bsh.JJTParserState r1 = r6.jjtree     // Catch:{ all -> 0x007f }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x007f }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x006a }
            if (r4 == 0) goto L_0x0082
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x006a }
            throw r0     // Catch:{ all -> 0x006a }
        L_0x006a:
            r0 = move-exception
        L_0x006b:
            if (r1 == 0) goto L_0x0075
            bsh.JJTParserState r1 = r6.jjtree
            r1.closeNodeScope(r3, r2)
            r6.jjtreeCloseNodeScope(r3)
        L_0x0075:
            throw r0
        L_0x0076:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x005c }
            goto L_0x0030
        L_0x0079:
            r0 = 79
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005c }
            goto L_0x003b
        L_0x007f:
            r0 = move-exception
            r1 = r2
            goto L_0x006b
        L_0x0082:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x006a }
            if (r4 == 0) goto L_0x0089
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x006a }
            throw r0     // Catch:{ all -> 0x006a }
        L_0x0089:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x006a }
            throw r0     // Catch:{ all -> 0x006a }
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ArrayInitializer():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHAssignment, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0036  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Assignment() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHAssignment r3 = new bsh.BSHAssignment
            r0 = 13
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r5.PrimaryExpression()     // Catch:{ Throwable -> 0x0025 }
            int r0 = r5.AssignmentOperator()     // Catch:{ Throwable -> 0x0025 }
            r3.operator = r0     // Catch:{ Throwable -> 0x0025 }
            r5.Expression()     // Catch:{ Throwable -> 0x0025 }
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x0025:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x0049 }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x0049 }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0033 }
            if (r4 == 0) goto L_0x003f
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0033 }
            throw r0     // Catch:{ all -> 0x0033 }
        L_0x0033:
            r0 = move-exception
        L_0x0034:
            if (r1 == 0) goto L_0x003e
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x003e:
            throw r0
        L_0x003f:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0033 }
            if (r4 == 0) goto L_0x0046
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0033 }
            throw r0     // Catch:{ all -> 0x0033 }
        L_0x0046:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0033 }
            throw r0     // Catch:{ all -> 0x0033 }
        L_0x0049:
            r0 = move-exception
            r1 = r2
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.Assignment():void");
    }

    public final int AssignmentOperator() {
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case ParserConstants.ASSIGN:
                jj_consume_token(81);
                break;
            case ParserConstants.PLUSASSIGN:
                jj_consume_token(ParserConstants.PLUSASSIGN);
                break;
            case ParserConstants.MINUSASSIGN:
                jj_consume_token(ParserConstants.MINUSASSIGN);
                break;
            case ParserConstants.STARASSIGN:
                jj_consume_token(ParserConstants.STARASSIGN);
                break;
            case ParserConstants.SLASHASSIGN:
                jj_consume_token(ParserConstants.SLASHASSIGN);
                break;
            case ParserConstants.ANDASSIGN:
                jj_consume_token(ParserConstants.ANDASSIGN);
                break;
            case ParserConstants.ORASSIGN:
                jj_consume_token(ParserConstants.ORASSIGN);
                break;
            case ParserConstants.XORASSIGN:
                jj_consume_token(ParserConstants.XORASSIGN);
                break;
            case ParserConstants.MODASSIGN:
                jj_consume_token(ParserConstants.MODASSIGN);
                break;
            case ParserConstants.LSHIFTASSIGN:
                jj_consume_token(ParserConstants.LSHIFTASSIGN);
                break;
            case ParserConstants.LSHIFTASSIGNX:
                jj_consume_token(ParserConstants.LSHIFTASSIGNX);
                break;
            case ParserConstants.RSIGNEDSHIFTASSIGN:
                jj_consume_token(ParserConstants.RSIGNEDSHIFTASSIGN);
                break;
            case ParserConstants.RSIGNEDSHIFTASSIGNX:
                jj_consume_token(ParserConstants.RSIGNEDSHIFTASSIGNX);
                break;
            case ParserConstants.RUNSIGNEDSHIFTASSIGN:
                jj_consume_token(ParserConstants.RUNSIGNEDSHIFTASSIGN);
                break;
            case ParserConstants.RUNSIGNEDSHIFTASSIGNX:
                jj_consume_token(ParserConstants.RUNSIGNEDSHIFTASSIGNX);
                break;
            default:
                this.jj_la1[24] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
        return getToken(0).kind;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHBlock, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a A[SYNTHETIC, Splitter:B:14:0x003a] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0044 A[Catch:{ all -> 0x0047 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0071 A[SYNTHETIC, Splitter:B:32:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0077 A[Catch:{ all -> 0x0047 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Block() {
        /*
            r7 = this;
            r3 = 0
            r2 = 1
            bsh.BSHBlock r4 = new bsh.BSHBlock
            r0 = 25
            r4.<init>(r0)
            bsh.JJTParserState r0 = r7.jjtree
            r0.openNodeScope(r4)
            r7.jjtreeOpenNodeScope(r4)
            r0 = 0
            int r1 = r7.jj_ntk     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
            r5 = -1
            if (r1 != r5) goto L_0x0053
            int r1 = r7.jj_ntk()     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
        L_0x001b:
            switch(r1) {
                case 48: goto L_0x0056;
                default: goto L_0x001e;
            }     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
        L_0x001e:
            int[] r1 = r7.jj_la1     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
            r5 = 71
            int r6 = r7.jj_gen     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
            r1[r5] = r6     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
        L_0x0026:
            r1 = 74
            r7.jj_consume_token(r1)     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
        L_0x002b:
            r1 = 1
            boolean r1 = r7.jj_2_23(r1)     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
            if (r1 == 0) goto L_0x005d
            r7.BlockStatement()     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
            goto L_0x002b
        L_0x0036:
            r0 = move-exception
            r1 = r2
        L_0x0038:
            if (r1 == 0) goto L_0x0071
            bsh.JJTParserState r5 = r7.jjtree     // Catch:{ all -> 0x0047 }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x0047 }
            r1 = r3
        L_0x0040:
            boolean r3 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0047 }
            if (r3 == 0) goto L_0x0077
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0047 }
            throw r0     // Catch:{ all -> 0x0047 }
        L_0x0047:
            r0 = move-exception
        L_0x0048:
            if (r1 == 0) goto L_0x0052
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r4, r2)
            r7.jjtreeCloseNodeScope(r4)
        L_0x0052:
            throw r0
        L_0x0053:
            int r1 = r7.jj_ntk     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
            goto L_0x001b
        L_0x0056:
            r0 = 48
            bsh.Token r0 = r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
            goto L_0x0026
        L_0x005d:
            r1 = 75
            r7.jj_consume_token(r1)     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
            bsh.JJTParserState r1 = r7.jjtree     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
            r5 = 1
            r1.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x0036, all -> 0x0081 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0087, all -> 0x0084 }
            if (r0 == 0) goto L_0x0070
            r0 = 1
            r4.isStatic = r0     // Catch:{ Throwable -> 0x0087, all -> 0x0084 }
        L_0x0070:
            return
        L_0x0071:
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x0047 }
            r3.popNode()     // Catch:{ all -> 0x0047 }
            goto L_0x0040
        L_0x0077:
            boolean r3 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0047 }
            if (r3 == 0) goto L_0x007e
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0047 }
            throw r0     // Catch:{ all -> 0x0047 }
        L_0x007e:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0047 }
            throw r0     // Catch:{ all -> 0x0047 }
        L_0x0081:
            r0 = move-exception
            r1 = r2
            goto L_0x0048
        L_0x0084:
            r0 = move-exception
            r1 = r3
            goto L_0x0048
        L_0x0087:
            r0 = move-exception
            r1 = r3
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.Block():void");
    }

    public final void BlockStatement() {
        if (jj_2_24(Integer.MAX_VALUE)) {
            ClassDeclaration();
        } else if (jj_2_25(Integer.MAX_VALUE)) {
            MethodDeclaration();
        } else if (jj_2_26(Integer.MAX_VALUE)) {
            MethodDeclaration();
        } else if (jj_2_27(Integer.MAX_VALUE)) {
            TypedVariableDeclaration();
            jj_consume_token(78);
        } else if (jj_2_28(1)) {
            Statement();
        } else {
            switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                case 34:
                    ImportDeclaration();
                    return;
                case ParserConstants.PACKAGE:
                    PackageDeclaration();
                    return;
                case ParserConstants.FORMAL_COMMENT:
                    FormalComment();
                    return;
                default:
                    this.jj_la1[72] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        }
    }

    public final boolean BooleanLiteral() {
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 26:
                jj_consume_token(26);
                return false;
            case ParserConstants.TRUE:
                jj_consume_token(55);
                return true;
            default:
                this.jj_la1[62] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHReturnStatement, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void BreakStatement() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHReturnStatement r3 = new bsh.BSHReturnStatement
            r0 = 35
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 12
            r5.jj_consume_token(r0)     // Catch:{ all -> 0x0046 }
            int r0 = r5.jj_ntk     // Catch:{ all -> 0x0046 }
            r1 = -1
            if (r0 != r1) goto L_0x003d
            int r0 = r5.jj_ntk()     // Catch:{ all -> 0x0046 }
        L_0x001e:
            switch(r0) {
                case 69: goto L_0x0040;
                default: goto L_0x0021;
            }     // Catch:{ all -> 0x0046 }
        L_0x0021:
            int[] r0 = r5.jj_la1     // Catch:{ all -> 0x0046 }
            r1 = 83
            int r4 = r5.jj_gen     // Catch:{ all -> 0x0046 }
            r0[r1] = r4     // Catch:{ all -> 0x0046 }
        L_0x0029:
            r0 = 78
            r5.jj_consume_token(r0)     // Catch:{ all -> 0x0046 }
            bsh.JJTParserState r0 = r5.jjtree     // Catch:{ all -> 0x0046 }
            r1 = 1
            r0.closeNodeScope(r3, r1)     // Catch:{ all -> 0x0046 }
            r1 = 0
            r5.jjtreeCloseNodeScope(r3)     // Catch:{ all -> 0x0053 }
            r0 = 12
            r3.kind = r0     // Catch:{ all -> 0x0053 }
            return
        L_0x003d:
            int r0 = r5.jj_ntk     // Catch:{ all -> 0x0046 }
            goto L_0x001e
        L_0x0040:
            r0 = 69
            r5.jj_consume_token(r0)     // Catch:{ all -> 0x0046 }
            goto L_0x0029
        L_0x0046:
            r0 = move-exception
            r1 = r2
        L_0x0048:
            if (r1 == 0) goto L_0x0052
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0052:
            throw r0
        L_0x0053:
            r0 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.BreakStatement():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHCastExpression, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0061  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void CastExpression() {
        /*
            r5 = this;
            r1 = -1
            r2 = 1
            bsh.BSHCastExpression r3 = new bsh.BSHCastExpression
            r0 = 17
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 2147483647(0x7fffffff, float:NaN)
            boolean r0 = r5.jj_2_13(r0)     // Catch:{ Throwable -> 0x0050 }
            if (r0 == 0) goto L_0x0033
            r0 = 72
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0050 }
            r5.Type()     // Catch:{ Throwable -> 0x0050 }
            r0 = 73
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0050 }
            r5.UnaryExpression()     // Catch:{ Throwable -> 0x0050 }
        L_0x002a:
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x0033:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0050 }
            if (r0 != r1) goto L_0x006a
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x0050 }
        L_0x003b:
            switch(r0) {
                case 72: goto L_0x006d;
                default: goto L_0x003e;
            }     // Catch:{ Throwable -> 0x0050 }
        L_0x003e:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x0050 }
            r1 = 55
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x0050 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x0050 }
            r0 = -1
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0050 }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x0050 }
            r0.<init>()     // Catch:{ Throwable -> 0x0050 }
            throw r0     // Catch:{ Throwable -> 0x0050 }
        L_0x0050:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x007e }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x007e }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x005e }
            if (r4 == 0) goto L_0x0081
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x005e }
            throw r0     // Catch:{ all -> 0x005e }
        L_0x005e:
            r0 = move-exception
        L_0x005f:
            if (r1 == 0) goto L_0x0069
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0069:
            throw r0
        L_0x006a:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0050 }
            goto L_0x003b
        L_0x006d:
            r0 = 72
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0050 }
            r5.Type()     // Catch:{ Throwable -> 0x0050 }
            r0 = 73
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0050 }
            r5.UnaryExpressionNotPlusMinus()     // Catch:{ Throwable -> 0x0050 }
            goto L_0x002a
        L_0x007e:
            r0 = move-exception
            r1 = r2
            goto L_0x005f
        L_0x0081:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x005e }
            if (r4 == 0) goto L_0x0088
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x005e }
            throw r0     // Catch:{ all -> 0x005e }
        L_0x0088:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x005e }
            throw r0     // Catch:{ all -> 0x005e }
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.CastExpression():void");
    }

    public final void CastLookahead() {
        if (jj_2_10(2)) {
            jj_consume_token(72);
            PrimitiveType();
        } else if (jj_2_11(Integer.MAX_VALUE)) {
            jj_consume_token(72);
            AmbiguousName();
            jj_consume_token(76);
            jj_consume_token(77);
        } else {
            switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                case ParserConstants.LPAREN:
                    jj_consume_token(72);
                    AmbiguousName();
                    jj_consume_token(73);
                    switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                        case 26:
                        case ParserConstants.NULL:
                        case ParserConstants.TRUE:
                        case ParserConstants.VOID:
                        case ParserConstants.INTEGER_LITERAL:
                        case ParserConstants.FLOATING_POINT_LITERAL:
                        case ParserConstants.CHARACTER_LITERAL:
                        case ParserConstants.STRING_LITERAL:
                            Literal();
                            return;
                        case ParserConstants.NEW:
                            jj_consume_token(40);
                            return;
                        case ParserConstants.IDENTIFIER:
                            jj_consume_token(69);
                            return;
                        case ParserConstants.LPAREN:
                            jj_consume_token(72);
                            return;
                        case ParserConstants.BANG:
                            jj_consume_token(86);
                            return;
                        case ParserConstants.TILDE:
                            jj_consume_token(87);
                            return;
                        default:
                            this.jj_la1[51] = this.jj_gen;
                            jj_consume_token(-1);
                            throw new ParseException();
                    }
                default:
                    this.jj_la1[52] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHClassDeclaration, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036 A[SYNTHETIC, Splitter:B:11:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003f A[Catch:{ all -> 0x0042 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00bd A[SYNTHETIC, Splitter:B:48:0x00bd] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00c5 A[SYNTHETIC, Splitter:B:51:0x00c5] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ClassDeclaration() {
        /*
            r9 = this;
            r3 = 0
            r8 = -1
            r2 = 1
            bsh.BSHClassDeclaration r4 = new bsh.BSHClassDeclaration
            r4.<init>(r2)
            bsh.JJTParserState r0 = r9.jjtree
            r0.openNodeScope(r4)
            r9.jjtreeOpenNodeScope(r4)
            r0 = 0
            r1 = 0
            bsh.Modifiers r1 = r9.Modifiers(r0, r1)     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            int r0 = r9.jj_ntk     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            if (r0 != r8) goto L_0x004e
            int r0 = r9.jj_ntk()     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
        L_0x001e:
            switch(r0) {
                case 13: goto L_0x0051;
                case 37: goto L_0x0093;
                default: goto L_0x0021;
            }     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
        L_0x0021:
            int[] r0 = r9.jj_la1     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r1 = 3
            int r5 = r9.jj_gen     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r0[r1] = r5     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r0 = -1
            r9.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r0.<init>()     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            throw r0     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
        L_0x0032:
            r0 = move-exception
            r1 = r2
        L_0x0034:
            if (r1 == 0) goto L_0x00bd
            bsh.JJTParserState r5 = r9.jjtree     // Catch:{ all -> 0x00cf }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x00cf }
        L_0x003b:
            boolean r1 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0042 }
            if (r1 == 0) goto L_0x00c5
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0042 }
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x0042:
            r0 = move-exception
        L_0x0043:
            if (r3 == 0) goto L_0x004d
            bsh.JJTParserState r1 = r9.jjtree
            r1.closeNodeScope(r4, r2)
            r9.jjtreeCloseNodeScope(r4)
        L_0x004d:
            throw r0
        L_0x004e:
            int r0 = r9.jj_ntk     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            goto L_0x001e
        L_0x0051:
            r0 = 13
            r9.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
        L_0x0056:
            r0 = 69
            bsh.Token r5 = r9.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            int r0 = r9.jj_ntk     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            if (r0 != r8) goto L_0x009f
            int r0 = r9.jj_ntk()     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
        L_0x0064:
            switch(r0) {
                case 25: goto L_0x00a2;
                default: goto L_0x0067;
            }     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
        L_0x0067:
            int[] r0 = r9.jj_la1     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r6 = 4
            int r7 = r9.jj_gen     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r0[r6] = r7     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
        L_0x006e:
            int r0 = r9.jj_ntk     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            if (r0 != r8) goto L_0x00ae
            int r0 = r9.jj_ntk()     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
        L_0x0076:
            switch(r0) {
                case 33: goto L_0x00b1;
                default: goto L_0x0079;
            }     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
        L_0x0079:
            int[] r0 = r9.jj_la1     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r6 = 5
            int r7 = r9.jj_gen     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r0[r6] = r7     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
        L_0x0080:
            r9.Block()     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            bsh.JJTParserState r0 = r9.jjtree     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r6 = 1
            r0.closeNodeScope(r4, r6)     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r9.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x00d3 }
            r4.modifiers = r1     // Catch:{ Throwable -> 0x00d3 }
            java.lang.String r0 = r5.image     // Catch:{ Throwable -> 0x00d3 }
            r4.name = r0     // Catch:{ Throwable -> 0x00d3 }
            return
        L_0x0093:
            r0 = 37
            r9.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r0 = 1
            r4.isInterface = r0     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            goto L_0x0056
        L_0x009c:
            r0 = move-exception
            r3 = r2
            goto L_0x0043
        L_0x009f:
            int r0 = r9.jj_ntk     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            goto L_0x0064
        L_0x00a2:
            r0 = 25
            r9.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r9.AmbiguousName()     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r0 = 1
            r4.extend = r0     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            goto L_0x006e
        L_0x00ae:
            int r0 = r9.jj_ntk     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            goto L_0x0076
        L_0x00b1:
            r0 = 33
            r9.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            int r0 = r9.NameList()     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            r4.numInterfaces = r0     // Catch:{ Throwable -> 0x0032, all -> 0x009c }
            goto L_0x0080
        L_0x00bd:
            bsh.JJTParserState r3 = r9.jjtree     // Catch:{ all -> 0x00cf }
            r3.popNode()     // Catch:{ all -> 0x00cf }
            r3 = r1
            goto L_0x003b
        L_0x00c5:
            boolean r1 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0042 }
            if (r1 == 0) goto L_0x00cc
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0042 }
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x00cc:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0042 }
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x00cf:
            r0 = move-exception
            r3 = r1
            goto L_0x0043
        L_0x00d3:
            r0 = move-exception
            r1 = r3
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ClassDeclaration():void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0010 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH] */
    public final void ConditionalAndExpression() {
        /*
            r7 = this;
            r6 = 2
            r5 = -1
            r7.InclusiveOrExpression()
        L_0x0005:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0019
            int r0 = r7.jj_ntk()
        L_0x000d:
            switch(r0) {
                case 98: goto L_0x001c;
                case 99: goto L_0x001c;
                default: goto L_0x0010;
            }
        L_0x0010:
            int[] r0 = r7.jj_la1
            r1 = 28
            int r2 = r7.jj_gen
            r0[r1] = r2
            return
        L_0x0019:
            int r0 = r7.jj_ntk
            goto L_0x000d
        L_0x001c:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0038
            int r0 = r7.jj_ntk()
        L_0x0024:
            switch(r0) {
                case 98: goto L_0x003b;
                case 99: goto L_0x006f;
                default: goto L_0x0027;
            }
        L_0x0027:
            int[] r0 = r7.jj_la1
            r1 = 29
            int r2 = r7.jj_gen
            r0[r1] = r2
            r7.jj_consume_token(r5)
            bsh.ParseException r0 = new bsh.ParseException
            r0.<init>()
            throw r0
        L_0x0038:
            int r0 = r7.jj_ntk
            goto L_0x0024
        L_0x003b:
            r0 = 98
            bsh.Token r0 = r7.jj_consume_token(r0)
        L_0x0041:
            r7.InclusiveOrExpression()
            bsh.BSHBinaryExpression r2 = new bsh.BSHBinaryExpression
            r1 = 15
            r2.<init>(r1)
            r1 = 1
            bsh.JJTParserState r3 = r7.jjtree
            r3.openNodeScope(r2)
            r7.jjtreeOpenNodeScope(r2)
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x0063 }
            r4 = 2
            r3.closeNodeScope(r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 0
            r7.jjtreeCloseNodeScope(r2)     // Catch:{ all -> 0x0063 }
            int r0 = r0.kind     // Catch:{ all -> 0x0063 }
            r2.kind = r0     // Catch:{ all -> 0x0063 }
            goto L_0x0005
        L_0x0063:
            r0 = move-exception
            if (r1 == 0) goto L_0x006e
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r2, r6)
            r7.jjtreeCloseNodeScope(r2)
        L_0x006e:
            throw r0
        L_0x006f:
            r0 = 99
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ConditionalAndExpression():void");
    }

    public final void ConditionalExpression() {
        ConditionalOrExpression();
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case ParserConstants.HOOK:
                jj_consume_token(88);
                Expression();
                jj_consume_token(89);
                BSHTernaryExpression bSHTernaryExpression = new BSHTernaryExpression(14);
                boolean z = true;
                this.jjtree.openNodeScope(bSHTernaryExpression);
                jjtreeOpenNodeScope(bSHTernaryExpression);
                try {
                    ConditionalExpression();
                    this.jjtree.closeNodeScope(bSHTernaryExpression, 3);
                    jjtreeCloseNodeScope(bSHTernaryExpression);
                    return;
                } catch (Throwable th) {
                    if (z) {
                        this.jjtree.closeNodeScope(bSHTernaryExpression, 3);
                        jjtreeCloseNodeScope(bSHTernaryExpression);
                    }
                    throw th;
                }
            default:
                this.jj_la1[25] = this.jj_gen;
                return;
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0010 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH] */
    public final void ConditionalOrExpression() {
        /*
            r7 = this;
            r6 = 2
            r5 = -1
            r7.ConditionalAndExpression()
        L_0x0005:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0019
            int r0 = r7.jj_ntk()
        L_0x000d:
            switch(r0) {
                case 96: goto L_0x001c;
                case 97: goto L_0x001c;
                default: goto L_0x0010;
            }
        L_0x0010:
            int[] r0 = r7.jj_la1
            r1 = 26
            int r2 = r7.jj_gen
            r0[r1] = r2
            return
        L_0x0019:
            int r0 = r7.jj_ntk
            goto L_0x000d
        L_0x001c:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0038
            int r0 = r7.jj_ntk()
        L_0x0024:
            switch(r0) {
                case 96: goto L_0x003b;
                case 97: goto L_0x006f;
                default: goto L_0x0027;
            }
        L_0x0027:
            int[] r0 = r7.jj_la1
            r1 = 27
            int r2 = r7.jj_gen
            r0[r1] = r2
            r7.jj_consume_token(r5)
            bsh.ParseException r0 = new bsh.ParseException
            r0.<init>()
            throw r0
        L_0x0038:
            int r0 = r7.jj_ntk
            goto L_0x0024
        L_0x003b:
            r0 = 96
            bsh.Token r0 = r7.jj_consume_token(r0)
        L_0x0041:
            r7.ConditionalAndExpression()
            bsh.BSHBinaryExpression r2 = new bsh.BSHBinaryExpression
            r1 = 15
            r2.<init>(r1)
            r1 = 1
            bsh.JJTParserState r3 = r7.jjtree
            r3.openNodeScope(r2)
            r7.jjtreeOpenNodeScope(r2)
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x0063 }
            r4 = 2
            r3.closeNodeScope(r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 0
            r7.jjtreeCloseNodeScope(r2)     // Catch:{ all -> 0x0063 }
            int r0 = r0.kind     // Catch:{ all -> 0x0063 }
            r2.kind = r0     // Catch:{ all -> 0x0063 }
            goto L_0x0005
        L_0x0063:
            r0 = move-exception
            if (r1 == 0) goto L_0x006e
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r2, r6)
            r7.jjtreeCloseNodeScope(r2)
        L_0x006e:
            throw r0
        L_0x006f:
            r0 = 97
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ConditionalOrExpression():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHReturnStatement, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ContinueStatement() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHReturnStatement r3 = new bsh.BSHReturnStatement
            r0 = 35
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 19
            r5.jj_consume_token(r0)     // Catch:{ all -> 0x0046 }
            int r0 = r5.jj_ntk     // Catch:{ all -> 0x0046 }
            r1 = -1
            if (r0 != r1) goto L_0x003d
            int r0 = r5.jj_ntk()     // Catch:{ all -> 0x0046 }
        L_0x001e:
            switch(r0) {
                case 69: goto L_0x0040;
                default: goto L_0x0021;
            }     // Catch:{ all -> 0x0046 }
        L_0x0021:
            int[] r0 = r5.jj_la1     // Catch:{ all -> 0x0046 }
            r1 = 84
            int r4 = r5.jj_gen     // Catch:{ all -> 0x0046 }
            r0[r1] = r4     // Catch:{ all -> 0x0046 }
        L_0x0029:
            r0 = 78
            r5.jj_consume_token(r0)     // Catch:{ all -> 0x0046 }
            bsh.JJTParserState r0 = r5.jjtree     // Catch:{ all -> 0x0046 }
            r1 = 1
            r0.closeNodeScope(r3, r1)     // Catch:{ all -> 0x0046 }
            r1 = 0
            r5.jjtreeCloseNodeScope(r3)     // Catch:{ all -> 0x0053 }
            r0 = 19
            r3.kind = r0     // Catch:{ all -> 0x0053 }
            return
        L_0x003d:
            int r0 = r5.jj_ntk     // Catch:{ all -> 0x0046 }
            goto L_0x001e
        L_0x0040:
            r0 = 69
            r5.jj_consume_token(r0)     // Catch:{ all -> 0x0046 }
            goto L_0x0029
        L_0x0046:
            r0 = move-exception
            r1 = r2
        L_0x0048:
            if (r1 == 0) goto L_0x0052
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0052:
            throw r0
        L_0x0053:
            r0 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ContinueStatement():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHWhileStatement, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004b A[Catch:{ all -> 0x004e }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005a A[SYNTHETIC, Splitter:B:19:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0060 A[Catch:{ all -> 0x004e }] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0041 A[SYNTHETIC, Splitter:B:9:0x0041] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void DoStatement() {
        /*
            r6 = this;
            r2 = 0
            r3 = 1
            bsh.BSHWhileStatement r4 = new bsh.BSHWhileStatement
            r0 = 30
            r4.<init>(r0)
            bsh.JJTParserState r0 = r6.jjtree
            r0.openNodeScope(r4)
            r6.jjtreeOpenNodeScope(r4)
            r0 = 21
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x003d, all -> 0x006a }
            r6.Statement()     // Catch:{ Throwable -> 0x003d, all -> 0x006a }
            r0 = 59
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x003d, all -> 0x006a }
            r0 = 72
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x003d, all -> 0x006a }
            r6.Expression()     // Catch:{ Throwable -> 0x003d, all -> 0x006a }
            r0 = 73
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x003d, all -> 0x006a }
            r0 = 78
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x003d, all -> 0x006a }
            bsh.JJTParserState r0 = r6.jjtree     // Catch:{ Throwable -> 0x003d, all -> 0x006a }
            r1 = 1
            r0.closeNodeScope(r4, r1)     // Catch:{ Throwable -> 0x003d, all -> 0x006a }
            r6.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0070, all -> 0x006d }
            r0 = 1
            r4.isDoStatement = r0     // Catch:{ Throwable -> 0x0070, all -> 0x006d }
            return
        L_0x003d:
            r0 = move-exception
            r1 = r3
        L_0x003f:
            if (r1 == 0) goto L_0x005a
            bsh.JJTParserState r5 = r6.jjtree     // Catch:{ all -> 0x004e }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x004e }
            r1 = r2
        L_0x0047:
            boolean r2 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x004e }
            if (r2 == 0) goto L_0x0060
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x004e }
            throw r0     // Catch:{ all -> 0x004e }
        L_0x004e:
            r0 = move-exception
        L_0x004f:
            if (r1 == 0) goto L_0x0059
            bsh.JJTParserState r1 = r6.jjtree
            r1.closeNodeScope(r4, r3)
            r6.jjtreeCloseNodeScope(r4)
        L_0x0059:
            throw r0
        L_0x005a:
            bsh.JJTParserState r2 = r6.jjtree     // Catch:{ all -> 0x004e }
            r2.popNode()     // Catch:{ all -> 0x004e }
            goto L_0x0047
        L_0x0060:
            boolean r2 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x004e }
            if (r2 == 0) goto L_0x0067
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x004e }
            throw r0     // Catch:{ all -> 0x004e }
        L_0x0067:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x004e }
            throw r0     // Catch:{ all -> 0x004e }
        L_0x006a:
            r0 = move-exception
            r1 = r3
            goto L_0x004f
        L_0x006d:
            r0 = move-exception
            r1 = r2
            goto L_0x004f
        L_0x0070:
            r0 = move-exception
            r1 = r2
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.DoStatement():void");
    }

    public final void EmptyStatement() {
        jj_consume_token(78);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHEnhancedForStatement, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0077  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void EnhancedForStatement() {
        /*
            r6 = this;
            r1 = -1
            r2 = 0
            r3 = 1
            bsh.BSHEnhancedForStatement r4 = new bsh.BSHEnhancedForStatement
            r0 = 32
            r4.<init>(r0)
            bsh.JJTParserState r0 = r6.jjtree
            r0.openNodeScope(r4)
            r6.jjtreeOpenNodeScope(r4)
            r0 = 4
            boolean r0 = r6.jj_2_30(r0)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            if (r0 == 0) goto L_0x0047
            r0 = 30
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r0 = 72
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r0 = 69
            bsh.Token r0 = r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r1 = 89
            r6.jj_consume_token(r1)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r6.Expression()     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r1 = 73
            r6.jj_consume_token(r1)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r6.Statement()     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            bsh.JJTParserState r1 = r6.jjtree     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r5 = 1
            r1.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r6.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x00b4 }
            java.lang.String r0 = r0.image     // Catch:{ Throwable -> 0x00b4 }
            r4.varName = r0     // Catch:{ Throwable -> 0x00b4 }
        L_0x0046:
            return
        L_0x0047:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            if (r0 != r1) goto L_0x0080
            int r0 = r6.jj_ntk()     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
        L_0x004f:
            switch(r0) {
                case 30: goto L_0x0083;
                default: goto L_0x0052;
            }     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
        L_0x0052:
            int[] r0 = r6.jj_la1     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r1 = 79
            int r5 = r6.jj_gen     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r0[r1] = r5     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r0 = -1
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r0.<init>()     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            throw r0     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
        L_0x0064:
            r0 = move-exception
            r1 = r3
        L_0x0066:
            if (r1 == 0) goto L_0x00b7
            bsh.JJTParserState r5 = r6.jjtree     // Catch:{ all -> 0x00cb }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x00cb }
        L_0x006d:
            boolean r1 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0074 }
            if (r1 == 0) goto L_0x00be
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0074 }
            throw r0     // Catch:{ all -> 0x0074 }
        L_0x0074:
            r0 = move-exception
        L_0x0075:
            if (r2 == 0) goto L_0x007f
            bsh.JJTParserState r1 = r6.jjtree
            r1.closeNodeScope(r4, r3)
            r6.jjtreeCloseNodeScope(r4)
        L_0x007f:
            throw r0
        L_0x0080:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            goto L_0x004f
        L_0x0083:
            r0 = 30
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r0 = 72
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r6.Type()     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r0 = 69
            bsh.Token r0 = r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r1 = 89
            r6.jj_consume_token(r1)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r6.Expression()     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r1 = 73
            r6.jj_consume_token(r1)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r6.Statement()     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            bsh.JJTParserState r1 = r6.jjtree     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r5 = 1
            r1.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x0064, all -> 0x00c8 }
            r6.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x00b4 }
            java.lang.String r0 = r0.image     // Catch:{ Throwable -> 0x00b4 }
            r4.varName = r0     // Catch:{ Throwable -> 0x00b4 }
            goto L_0x0046
        L_0x00b4:
            r0 = move-exception
            r1 = r2
            goto L_0x0066
        L_0x00b7:
            bsh.JJTParserState r2 = r6.jjtree     // Catch:{ all -> 0x00cb }
            r2.popNode()     // Catch:{ all -> 0x00cb }
            r2 = r1
            goto L_0x006d
        L_0x00be:
            boolean r1 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0074 }
            if (r1 == 0) goto L_0x00c5
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0074 }
            throw r0     // Catch:{ all -> 0x0074 }
        L_0x00c5:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0074 }
            throw r0     // Catch:{ all -> 0x0074 }
        L_0x00c8:
            r0 = move-exception
            r2 = r3
            goto L_0x0075
        L_0x00cb:
            r0 = move-exception
            r2 = r1
            goto L_0x0075
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.EnhancedForStatement():void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0010 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH] */
    public final void EqualityExpression() {
        /*
            r7 = this;
            r6 = 2
            r5 = -1
            r7.InstanceOfExpression()
        L_0x0005:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0019
            int r0 = r7.jj_ntk()
        L_0x000d:
            switch(r0) {
                case 90: goto L_0x001c;
                case 95: goto L_0x001c;
                default: goto L_0x0010;
            }
        L_0x0010:
            int[] r0 = r7.jj_la1
            r1 = 35
            int r2 = r7.jj_gen
            r0[r1] = r2
            return
        L_0x0019:
            int r0 = r7.jj_ntk
            goto L_0x000d
        L_0x001c:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0038
            int r0 = r7.jj_ntk()
        L_0x0024:
            switch(r0) {
                case 90: goto L_0x003b;
                case 95: goto L_0x006f;
                default: goto L_0x0027;
            }
        L_0x0027:
            int[] r0 = r7.jj_la1
            r1 = 36
            int r2 = r7.jj_gen
            r0[r1] = r2
            r7.jj_consume_token(r5)
            bsh.ParseException r0 = new bsh.ParseException
            r0.<init>()
            throw r0
        L_0x0038:
            int r0 = r7.jj_ntk
            goto L_0x0024
        L_0x003b:
            r0 = 90
            bsh.Token r0 = r7.jj_consume_token(r0)
        L_0x0041:
            r7.InstanceOfExpression()
            bsh.BSHBinaryExpression r2 = new bsh.BSHBinaryExpression
            r1 = 15
            r2.<init>(r1)
            r1 = 1
            bsh.JJTParserState r3 = r7.jjtree
            r3.openNodeScope(r2)
            r7.jjtreeOpenNodeScope(r2)
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x0063 }
            r4 = 2
            r3.closeNodeScope(r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 0
            r7.jjtreeCloseNodeScope(r2)     // Catch:{ all -> 0x0063 }
            int r0 = r0.kind     // Catch:{ all -> 0x0063 }
            r2.kind = r0     // Catch:{ all -> 0x0063 }
            goto L_0x0005
        L_0x0063:
            r0 = move-exception
            if (r1 == 0) goto L_0x006e
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r2, r6)
            r7.jjtreeCloseNodeScope(r2)
        L_0x006e:
            throw r0
        L_0x006f:
            r0 = 95
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.EqualityExpression():void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0010 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH] */
    public final void ExclusiveOrExpression() {
        /*
            r6 = this;
            r5 = 2
            r6.AndExpression()
        L_0x0004:
            int r0 = r6.jj_ntk
            r1 = -1
            if (r0 != r1) goto L_0x0019
            int r0 = r6.jj_ntk()
        L_0x000d:
            switch(r0) {
                case 110: goto L_0x001c;
                default: goto L_0x0010;
            }
        L_0x0010:
            int[] r0 = r6.jj_la1
            r1 = 32
            int r2 = r6.jj_gen
            r0[r1] = r2
            return
        L_0x0019:
            int r0 = r6.jj_ntk
            goto L_0x000d
        L_0x001c:
            r0 = 110(0x6e, float:1.54E-43)
            bsh.Token r0 = r6.jj_consume_token(r0)
            r6.AndExpression()
            bsh.BSHBinaryExpression r2 = new bsh.BSHBinaryExpression
            r1 = 15
            r2.<init>(r1)
            r1 = 1
            bsh.JJTParserState r3 = r6.jjtree
            r3.openNodeScope(r2)
            r6.jjtreeOpenNodeScope(r2)
            bsh.JJTParserState r3 = r6.jjtree     // Catch:{ all -> 0x0044 }
            r4 = 2
            r3.closeNodeScope(r2, r4)     // Catch:{ all -> 0x0044 }
            r1 = 0
            r6.jjtreeCloseNodeScope(r2)     // Catch:{ all -> 0x0044 }
            int r0 = r0.kind     // Catch:{ all -> 0x0044 }
            r2.kind = r0     // Catch:{ all -> 0x0044 }
            goto L_0x0004
        L_0x0044:
            r0 = move-exception
            if (r1 == 0) goto L_0x004f
            bsh.JJTParserState r1 = r6.jjtree
            r1.closeNodeScope(r2, r5)
            r6.jjtreeCloseNodeScope(r2)
        L_0x004f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ExclusiveOrExpression():void");
    }

    public final void Expression() {
        if (jj_2_8(Integer.MAX_VALUE)) {
            Assignment();
            return;
        }
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 11:
            case 14:
            case 17:
            case 22:
            case 26:
            case 29:
            case 36:
            case ParserConstants.LONG:
            case ParserConstants.NEW:
            case ParserConstants.NULL:
            case ParserConstants.SHORT:
            case ParserConstants.TRUE:
            case ParserConstants.VOID:
            case ParserConstants.INTEGER_LITERAL:
            case ParserConstants.FLOATING_POINT_LITERAL:
            case ParserConstants.CHARACTER_LITERAL:
            case ParserConstants.STRING_LITERAL:
            case ParserConstants.IDENTIFIER:
            case ParserConstants.LPAREN:
            case ParserConstants.BANG:
            case ParserConstants.TILDE:
            case ParserConstants.INCR:
            case ParserConstants.DECR:
            case ParserConstants.PLUS:
            case ParserConstants.MINUS:
                ConditionalExpression();
                return;
            default:
                this.jj_la1[23] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    public final void ForInit() {
        if (jj_2_31(Integer.MAX_VALUE)) {
            TypedVariableDeclaration();
            return;
        }
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 11:
            case 14:
            case 17:
            case 22:
            case 26:
            case 29:
            case 36:
            case ParserConstants.LONG:
            case ParserConstants.NEW:
            case ParserConstants.NULL:
            case ParserConstants.SHORT:
            case ParserConstants.TRUE:
            case ParserConstants.VOID:
            case ParserConstants.INTEGER_LITERAL:
            case ParserConstants.FLOATING_POINT_LITERAL:
            case ParserConstants.CHARACTER_LITERAL:
            case ParserConstants.STRING_LITERAL:
            case ParserConstants.IDENTIFIER:
            case ParserConstants.LPAREN:
            case ParserConstants.BANG:
            case ParserConstants.TILDE:
            case ParserConstants.INCR:
            case ParserConstants.DECR:
            case ParserConstants.PLUS:
            case ParserConstants.MINUS:
                StatementExpressionList();
                return;
            default:
                this.jj_la1[80] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHForStatement, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ForStatement() {
        /*
            r6 = this;
            r5 = -1
            r2 = 1
            bsh.BSHForStatement r3 = new bsh.BSHForStatement
            r0 = 31
            r3.<init>(r0)
            bsh.JJTParserState r0 = r6.jjtree
            r0.openNodeScope(r3)
            r6.jjtreeOpenNodeScope(r3)
            r0 = 30
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0079 }
            r0 = 72
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0079 }
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x0079 }
            if (r0 != r5) goto L_0x006f
            int r0 = r6.jj_ntk()     // Catch:{ Throwable -> 0x0079 }
        L_0x0023:
            switch(r0) {
                case 10: goto L_0x0072;
                case 11: goto L_0x0072;
                case 14: goto L_0x0072;
                case 17: goto L_0x0072;
                case 22: goto L_0x0072;
                case 26: goto L_0x0072;
                case 27: goto L_0x0072;
                case 29: goto L_0x0072;
                case 36: goto L_0x0072;
                case 38: goto L_0x0072;
                case 39: goto L_0x0072;
                case 40: goto L_0x0072;
                case 41: goto L_0x0072;
                case 43: goto L_0x0072;
                case 44: goto L_0x0072;
                case 45: goto L_0x0072;
                case 47: goto L_0x0072;
                case 48: goto L_0x0072;
                case 49: goto L_0x0072;
                case 51: goto L_0x0072;
                case 52: goto L_0x0072;
                case 55: goto L_0x0072;
                case 57: goto L_0x0072;
                case 58: goto L_0x0072;
                case 60: goto L_0x0072;
                case 64: goto L_0x0072;
                case 66: goto L_0x0072;
                case 67: goto L_0x0072;
                case 69: goto L_0x0072;
                case 72: goto L_0x0072;
                case 86: goto L_0x0072;
                case 87: goto L_0x0072;
                case 100: goto L_0x0072;
                case 101: goto L_0x0072;
                case 102: goto L_0x0072;
                case 103: goto L_0x0072;
                default: goto L_0x0026;
            }     // Catch:{ Throwable -> 0x0079 }
        L_0x0026:
            int[] r0 = r6.jj_la1     // Catch:{ Throwable -> 0x0079 }
            r1 = 76
            int r4 = r6.jj_gen     // Catch:{ Throwable -> 0x0079 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x0079 }
        L_0x002e:
            r0 = 78
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0079 }
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x0079 }
            if (r0 != r5) goto L_0x0093
            int r0 = r6.jj_ntk()     // Catch:{ Throwable -> 0x0079 }
        L_0x003b:
            switch(r0) {
                case 11: goto L_0x0096;
                case 14: goto L_0x0096;
                case 17: goto L_0x0096;
                case 22: goto L_0x0096;
                case 26: goto L_0x0096;
                case 29: goto L_0x0096;
                case 36: goto L_0x0096;
                case 38: goto L_0x0096;
                case 40: goto L_0x0096;
                case 41: goto L_0x0096;
                case 47: goto L_0x0096;
                case 55: goto L_0x0096;
                case 57: goto L_0x0096;
                case 60: goto L_0x0096;
                case 64: goto L_0x0096;
                case 66: goto L_0x0096;
                case 67: goto L_0x0096;
                case 69: goto L_0x0096;
                case 72: goto L_0x0096;
                case 86: goto L_0x0096;
                case 87: goto L_0x0096;
                case 100: goto L_0x0096;
                case 101: goto L_0x0096;
                case 102: goto L_0x0096;
                case 103: goto L_0x0096;
                default: goto L_0x003e;
            }     // Catch:{ Throwable -> 0x0079 }
        L_0x003e:
            int[] r0 = r6.jj_la1     // Catch:{ Throwable -> 0x0079 }
            r1 = 77
            int r4 = r6.jj_gen     // Catch:{ Throwable -> 0x0079 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x0079 }
        L_0x0046:
            r0 = 78
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0079 }
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x0079 }
            if (r0 != r5) goto L_0x00a0
            int r0 = r6.jj_ntk()     // Catch:{ Throwable -> 0x0079 }
        L_0x0053:
            switch(r0) {
                case 11: goto L_0x00a3;
                case 14: goto L_0x00a3;
                case 17: goto L_0x00a3;
                case 22: goto L_0x00a3;
                case 26: goto L_0x00a3;
                case 29: goto L_0x00a3;
                case 36: goto L_0x00a3;
                case 38: goto L_0x00a3;
                case 40: goto L_0x00a3;
                case 41: goto L_0x00a3;
                case 47: goto L_0x00a3;
                case 55: goto L_0x00a3;
                case 57: goto L_0x00a3;
                case 60: goto L_0x00a3;
                case 64: goto L_0x00a3;
                case 66: goto L_0x00a3;
                case 67: goto L_0x00a3;
                case 69: goto L_0x00a3;
                case 72: goto L_0x00a3;
                case 86: goto L_0x00a3;
                case 87: goto L_0x00a3;
                case 100: goto L_0x00a3;
                case 101: goto L_0x00a3;
                case 102: goto L_0x00a3;
                case 103: goto L_0x00a3;
                default: goto L_0x0056;
            }     // Catch:{ Throwable -> 0x0079 }
        L_0x0056:
            int[] r0 = r6.jj_la1     // Catch:{ Throwable -> 0x0079 }
            r1 = 78
            int r4 = r6.jj_gen     // Catch:{ Throwable -> 0x0079 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x0079 }
        L_0x005e:
            r0 = 73
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0079 }
            r6.Statement()     // Catch:{ Throwable -> 0x0079 }
            bsh.JJTParserState r0 = r6.jjtree
            r0.closeNodeScope(r3, r2)
            r6.jjtreeCloseNodeScope(r3)
            return
        L_0x006f:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x0079 }
            goto L_0x0023
        L_0x0072:
            r6.ForInit()     // Catch:{ Throwable -> 0x0079 }
            r0 = 1
            r3.hasForInit = r0     // Catch:{ Throwable -> 0x0079 }
            goto L_0x002e
        L_0x0079:
            r0 = move-exception
            bsh.JJTParserState r1 = r6.jjtree     // Catch:{ all -> 0x009d }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x009d }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0087 }
            if (r4 == 0) goto L_0x00aa
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0087 }
            throw r0     // Catch:{ all -> 0x0087 }
        L_0x0087:
            r0 = move-exception
        L_0x0088:
            if (r1 == 0) goto L_0x0092
            bsh.JJTParserState r1 = r6.jjtree
            r1.closeNodeScope(r3, r2)
            r6.jjtreeCloseNodeScope(r3)
        L_0x0092:
            throw r0
        L_0x0093:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x0079 }
            goto L_0x003b
        L_0x0096:
            r6.Expression()     // Catch:{ Throwable -> 0x0079 }
            r0 = 1
            r3.hasExpression = r0     // Catch:{ Throwable -> 0x0079 }
            goto L_0x0046
        L_0x009d:
            r0 = move-exception
            r1 = r2
            goto L_0x0088
        L_0x00a0:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x0079 }
            goto L_0x0053
        L_0x00a3:
            r6.StatementExpressionList()     // Catch:{ Throwable -> 0x0079 }
            r0 = 1
            r3.hasForUpdate = r0     // Catch:{ Throwable -> 0x0079 }
            goto L_0x005e
        L_0x00aa:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0087 }
            if (r4 == 0) goto L_0x00b1
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0087 }
            throw r0     // Catch:{ all -> 0x0087 }
        L_0x00b1:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0087 }
            throw r0     // Catch:{ all -> 0x0087 }
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ForStatement():void");
    }

    public final void ForUpdate() {
        StatementExpressionList();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHFormalComment, boolean]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    public final void FormalComment() {
        BSHFormalComment bSHFormalComment = new BSHFormalComment(26);
        boolean z = true;
        this.jjtree.openNodeScope(bSHFormalComment);
        jjtreeOpenNodeScope(bSHFormalComment);
        try {
            Token jj_consume_token = jj_consume_token(68);
            this.jjtree.closeNodeScope((Node) bSHFormalComment, this.retainComments);
            z = false;
            jjtreeCloseNodeScope(bSHFormalComment);
            bSHFormalComment.text = jj_consume_token.image;
        } catch (Throwable th) {
            if (z) {
                this.jjtree.closeNodeScope((Node) bSHFormalComment, this.retainComments);
                jjtreeCloseNodeScope(bSHFormalComment);
            }
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHFormalParameter, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0060  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void FormalParameter() {
        /*
            r6 = this;
            r1 = -1
            r2 = 0
            r3 = 1
            bsh.BSHFormalParameter r4 = new bsh.BSHFormalParameter
            r0 = 8
            r4.<init>(r0)
            bsh.JJTParserState r0 = r6.jjtree
            r0.openNodeScope(r4)
            r6.jjtreeOpenNodeScope(r4)
            r0 = 2
            boolean r0 = r6.jj_2_5(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            if (r0 == 0) goto L_0x0030
            r6.Type()     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            r0 = 69
            bsh.Token r0 = r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            bsh.JJTParserState r1 = r6.jjtree     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            r5 = 1
            r1.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            r6.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0080 }
            java.lang.String r0 = r0.image     // Catch:{ Throwable -> 0x0080 }
            r4.name = r0     // Catch:{ Throwable -> 0x0080 }
        L_0x002f:
            return
        L_0x0030:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            if (r0 != r1) goto L_0x0069
            int r0 = r6.jj_ntk()     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
        L_0x0038:
            switch(r0) {
                case 69: goto L_0x006c;
                default: goto L_0x003b;
            }     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
        L_0x003b:
            int[] r0 = r6.jj_la1     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            r1 = 18
            int r5 = r6.jj_gen     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            r0[r1] = r5     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            r0 = -1
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            r0.<init>()     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            throw r0     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
        L_0x004d:
            r0 = move-exception
            r1 = r3
        L_0x004f:
            if (r1 == 0) goto L_0x0083
            bsh.JJTParserState r5 = r6.jjtree     // Catch:{ all -> 0x0097 }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x0097 }
        L_0x0056:
            boolean r1 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x005d }
            if (r1 == 0) goto L_0x008a
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x005d }
            throw r0     // Catch:{ all -> 0x005d }
        L_0x005d:
            r0 = move-exception
        L_0x005e:
            if (r2 == 0) goto L_0x0068
            bsh.JJTParserState r1 = r6.jjtree
            r1.closeNodeScope(r4, r3)
            r6.jjtreeCloseNodeScope(r4)
        L_0x0068:
            throw r0
        L_0x0069:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            goto L_0x0038
        L_0x006c:
            r0 = 69
            bsh.Token r0 = r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            bsh.JJTParserState r1 = r6.jjtree     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            r5 = 1
            r1.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x004d, all -> 0x0094 }
            r6.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0080 }
            java.lang.String r0 = r0.image     // Catch:{ Throwable -> 0x0080 }
            r4.name = r0     // Catch:{ Throwable -> 0x0080 }
            goto L_0x002f
        L_0x0080:
            r0 = move-exception
            r1 = r2
            goto L_0x004f
        L_0x0083:
            bsh.JJTParserState r2 = r6.jjtree     // Catch:{ all -> 0x0097 }
            r2.popNode()     // Catch:{ all -> 0x0097 }
            r2 = r1
            goto L_0x0056
        L_0x008a:
            boolean r1 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x005d }
            if (r1 == 0) goto L_0x0091
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x005d }
            throw r0     // Catch:{ all -> 0x005d }
        L_0x0091:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x005d }
            throw r0     // Catch:{ all -> 0x005d }
        L_0x0094:
            r0 = move-exception
            r2 = r3
            goto L_0x005e
        L_0x0097:
            r0 = move-exception
            r2 = r1
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.FormalParameter():void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHFormalParameters, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0066  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void FormalParameters() {
        /*
            r5 = this;
            r1 = -1
            r2 = 1
            bsh.BSHFormalParameters r3 = new bsh.BSHFormalParameters
            r0 = 7
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 72
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0055 }
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0055 }
            if (r0 != r1) goto L_0x0036
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x0055 }
        L_0x001d:
            switch(r0) {
                case 11: goto L_0x003e;
                case 14: goto L_0x003e;
                case 17: goto L_0x003e;
                case 22: goto L_0x003e;
                case 29: goto L_0x003e;
                case 36: goto L_0x003e;
                case 38: goto L_0x003e;
                case 47: goto L_0x003e;
                case 69: goto L_0x003e;
                default: goto L_0x0020;
            }     // Catch:{ Throwable -> 0x0055 }
        L_0x0020:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x0055 }
            r1 = 17
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x0055 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x0055 }
        L_0x0028:
            r0 = 73
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0055 }
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x0036:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0055 }
            goto L_0x001d
        L_0x0039:
            r0 = 79
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0055 }
        L_0x003e:
            r5.FormalParameter()     // Catch:{ Throwable -> 0x0055 }
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0055 }
            if (r0 != r1) goto L_0x006f
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x0055 }
        L_0x0049:
            switch(r0) {
                case 79: goto L_0x0039;
                default: goto L_0x004c;
            }     // Catch:{ Throwable -> 0x0055 }
        L_0x004c:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x0055 }
            r1 = 16
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x0055 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x0055 }
            goto L_0x0028
        L_0x0055:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x007c }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x007c }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0063 }
            if (r4 == 0) goto L_0x0072
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0063 }
            throw r0     // Catch:{ all -> 0x0063 }
        L_0x0063:
            r0 = move-exception
        L_0x0064:
            if (r1 == 0) goto L_0x006e
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x006e:
            throw r0
        L_0x006f:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0055 }
            goto L_0x0049
        L_0x0072:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0063 }
            if (r4 == 0) goto L_0x0079
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0063 }
            throw r0     // Catch:{ all -> 0x0063 }
        L_0x0079:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0063 }
            throw r0     // Catch:{ all -> 0x0063 }
        L_0x007c:
            r0 = move-exception
            r1 = r2
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.FormalParameters():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHIfStatement, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void IfStatement() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHIfStatement r3 = new bsh.BSHIfStatement
            r0 = 29
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 32
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004e }
            r0 = 72
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004e }
            r5.Expression()     // Catch:{ Throwable -> 0x004e }
            r0 = 73
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004e }
            r5.Statement()     // Catch:{ Throwable -> 0x004e }
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x004e }
            r1 = -1
            if (r0 != r1) goto L_0x0042
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x004e }
        L_0x002e:
            switch(r0) {
                case 23: goto L_0x0045;
                default: goto L_0x0031;
            }     // Catch:{ Throwable -> 0x004e }
        L_0x0031:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x004e }
            r1 = 75
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x004e }
            r0[r1] = r4     // Catch:{ Throwable -> 0x004e }
        L_0x0039:
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x0042:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x004e }
            goto L_0x002e
        L_0x0045:
            r0 = 23
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004e }
            r5.Statement()     // Catch:{ Throwable -> 0x004e }
            goto L_0x0039
        L_0x004e:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x0072 }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x0072 }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x005c }
            if (r4 == 0) goto L_0x0068
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x005c }
            throw r0     // Catch:{ all -> 0x005c }
        L_0x005c:
            r0 = move-exception
        L_0x005d:
            if (r1 == 0) goto L_0x0067
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0067:
            throw r0
        L_0x0068:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x005c }
            if (r4 == 0) goto L_0x006f
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x005c }
            throw r0     // Catch:{ all -> 0x005c }
        L_0x006f:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x005c }
            throw r0     // Catch:{ all -> 0x005c }
        L_0x0072:
            r0 = move-exception
            r1 = r2
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.IfStatement():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHImportDeclaration, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ImportDeclaration() {
        /*
            r9 = this;
            r3 = 0
            r8 = -1
            r2 = 1
            bsh.BSHImportDeclaration r5 = new bsh.BSHImportDeclaration
            r0 = 4
            r5.<init>(r0)
            bsh.JJTParserState r0 = r9.jjtree
            r0.openNodeScope(r5)
            r9.jjtreeOpenNodeScope(r5)
            r1 = 0
            r0 = 0
            r4 = 3
            boolean r4 = r9.jj_2_3(r4)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            if (r4 == 0) goto L_0x0098
            r4 = 34
            r9.jj_consume_token(r4)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            int r4 = r9.jj_ntk     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            if (r4 != r8) goto L_0x0062
            int r4 = r9.jj_ntk()     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
        L_0x0027:
            switch(r4) {
                case 48: goto L_0x0065;
                default: goto L_0x002a;
            }     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
        L_0x002a:
            int[] r4 = r9.jj_la1     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r6 = 9
            int r7 = r9.jj_gen     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r4[r6] = r7     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r4 = r1
        L_0x0033:
            r9.AmbiguousName()     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            int r1 = r9.jj_ntk     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            if (r1 != r8) goto L_0x006d
            int r1 = r9.jj_ntk()     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
        L_0x003e:
            switch(r1) {
                case 80: goto L_0x0070;
                default: goto L_0x0041;
            }     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
        L_0x0041:
            int[] r1 = r9.jj_la1     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r6 = 10
            int r7 = r9.jj_gen     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r1[r6] = r7     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
        L_0x0049:
            r1 = 78
            r9.jj_consume_token(r1)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            bsh.JJTParserState r1 = r9.jjtree     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r6 = 1
            r1.closeNodeScope(r5, r6)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r9.jjtreeCloseNodeScope(r5)     // Catch:{ Throwable -> 0x00d7 }
            if (r4 == 0) goto L_0x005c
            r1 = 1
            r5.staticImport = r1     // Catch:{ Throwable -> 0x00d7 }
        L_0x005c:
            if (r0 == 0) goto L_0x0061
            r0 = 1
            r5.importPackage = r0     // Catch:{ Throwable -> 0x00d7 }
        L_0x0061:
            return
        L_0x0062:
            int r4 = r9.jj_ntk     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            goto L_0x0027
        L_0x0065:
            r1 = 48
            bsh.Token r1 = r9.jj_consume_token(r1)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r4 = r1
            goto L_0x0033
        L_0x006d:
            int r1 = r9.jj_ntk     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            goto L_0x003e
        L_0x0070:
            r0 = 80
            bsh.Token r0 = r9.jj_consume_token(r0)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r1 = 104(0x68, float:1.46E-43)
            r9.jj_consume_token(r1)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            goto L_0x0049
        L_0x007c:
            r0 = move-exception
            r1 = r2
        L_0x007e:
            if (r1 == 0) goto L_0x00da
            bsh.JJTParserState r4 = r9.jjtree     // Catch:{ all -> 0x00eb }
            r4.clearNodeScope(r5)     // Catch:{ all -> 0x00eb }
        L_0x0085:
            boolean r1 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x008c }
            if (r1 == 0) goto L_0x00e1
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x008c }
            throw r0     // Catch:{ all -> 0x008c }
        L_0x008c:
            r0 = move-exception
        L_0x008d:
            if (r3 == 0) goto L_0x0097
            bsh.JJTParserState r1 = r9.jjtree
            r1.closeNodeScope(r5, r2)
            r9.jjtreeCloseNodeScope(r5)
        L_0x0097:
            throw r0
        L_0x0098:
            int r0 = r9.jj_ntk     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            if (r0 != r8) goto L_0x00b8
            int r0 = r9.jj_ntk()     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
        L_0x00a0:
            switch(r0) {
                case 34: goto L_0x00bb;
                default: goto L_0x00a3;
            }     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
        L_0x00a3:
            int[] r0 = r9.jj_la1     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r1 = 11
            int r4 = r9.jj_gen     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r0 = -1
            r9.jj_consume_token(r0)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r0.<init>()     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            throw r0     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
        L_0x00b5:
            r0 = move-exception
            r3 = r2
            goto L_0x008d
        L_0x00b8:
            int r0 = r9.jj_ntk     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            goto L_0x00a0
        L_0x00bb:
            r0 = 34
            r9.jj_consume_token(r0)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r0 = 104(0x68, float:1.46E-43)
            r9.jj_consume_token(r0)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r0 = 78
            r9.jj_consume_token(r0)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            bsh.JJTParserState r0 = r9.jjtree     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r1 = 1
            r0.closeNodeScope(r5, r1)     // Catch:{ Throwable -> 0x007c, all -> 0x00b5 }
            r9.jjtreeCloseNodeScope(r5)     // Catch:{ Throwable -> 0x00d7 }
            r0 = 1
            r5.superImport = r0     // Catch:{ Throwable -> 0x00d7 }
            goto L_0x0061
        L_0x00d7:
            r0 = move-exception
            r1 = r3
            goto L_0x007e
        L_0x00da:
            bsh.JJTParserState r3 = r9.jjtree     // Catch:{ all -> 0x00eb }
            r3.popNode()     // Catch:{ all -> 0x00eb }
            r3 = r1
            goto L_0x0085
        L_0x00e1:
            boolean r1 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x008c }
            if (r1 == 0) goto L_0x00e8
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x008c }
            throw r0     // Catch:{ all -> 0x008c }
        L_0x00e8:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x008c }
            throw r0     // Catch:{ all -> 0x008c }
        L_0x00eb:
            r0 = move-exception
            r3 = r1
            goto L_0x008d
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ImportDeclaration():void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0010 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH] */
    public final void InclusiveOrExpression() {
        /*
            r7 = this;
            r6 = 2
            r5 = -1
            r7.ExclusiveOrExpression()
        L_0x0005:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0019
            int r0 = r7.jj_ntk()
        L_0x000d:
            switch(r0) {
                case 108: goto L_0x001c;
                case 109: goto L_0x001c;
                default: goto L_0x0010;
            }
        L_0x0010:
            int[] r0 = r7.jj_la1
            r1 = 30
            int r2 = r7.jj_gen
            r0[r1] = r2
            return
        L_0x0019:
            int r0 = r7.jj_ntk
            goto L_0x000d
        L_0x001c:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0038
            int r0 = r7.jj_ntk()
        L_0x0024:
            switch(r0) {
                case 108: goto L_0x003b;
                case 109: goto L_0x006f;
                default: goto L_0x0027;
            }
        L_0x0027:
            int[] r0 = r7.jj_la1
            r1 = 31
            int r2 = r7.jj_gen
            r0[r1] = r2
            r7.jj_consume_token(r5)
            bsh.ParseException r0 = new bsh.ParseException
            r0.<init>()
            throw r0
        L_0x0038:
            int r0 = r7.jj_ntk
            goto L_0x0024
        L_0x003b:
            r0 = 108(0x6c, float:1.51E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
        L_0x0041:
            r7.ExclusiveOrExpression()
            bsh.BSHBinaryExpression r2 = new bsh.BSHBinaryExpression
            r1 = 15
            r2.<init>(r1)
            r1 = 1
            bsh.JJTParserState r3 = r7.jjtree
            r3.openNodeScope(r2)
            r7.jjtreeOpenNodeScope(r2)
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x0063 }
            r4 = 2
            r3.closeNodeScope(r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 0
            r7.jjtreeCloseNodeScope(r2)     // Catch:{ all -> 0x0063 }
            int r0 = r0.kind     // Catch:{ all -> 0x0063 }
            r2.kind = r0     // Catch:{ all -> 0x0063 }
            goto L_0x0005
        L_0x0063:
            r0 = move-exception
            if (r1 == 0) goto L_0x006e
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r2, r6)
            r7.jjtreeCloseNodeScope(r2)
        L_0x006e:
            throw r0
        L_0x006f:
            r0 = 109(0x6d, float:1.53E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.InclusiveOrExpression():void");
    }

    public final void InstanceOfExpression() {
        RelationalExpression();
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 35:
                Token jj_consume_token = jj_consume_token(35);
                Type();
                BSHBinaryExpression bSHBinaryExpression = new BSHBinaryExpression(15);
                boolean z = true;
                this.jjtree.openNodeScope(bSHBinaryExpression);
                jjtreeOpenNodeScope(bSHBinaryExpression);
                try {
                    this.jjtree.closeNodeScope(bSHBinaryExpression, 2);
                    z = false;
                    jjtreeCloseNodeScope(bSHBinaryExpression);
                    bSHBinaryExpression.kind = jj_consume_token.kind;
                    return;
                } catch (Throwable th) {
                    if (z) {
                        this.jjtree.closeNodeScope(bSHBinaryExpression, 2);
                        jjtreeCloseNodeScope(bSHBinaryExpression);
                    }
                    throw th;
                }
            default:
                this.jj_la1[37] = this.jj_gen;
                return;
        }
    }

    public final void LabeledStatement() {
        jj_consume_token(69);
        jj_consume_token(89);
        Statement();
    }

    public final boolean Line() {
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 0:
                jj_consume_token(0);
                Interpreter.debug("End of File!");
                return true;
            default:
                this.jj_la1[0] = this.jj_gen;
                if (jj_2_1(1)) {
                    BlockStatement();
                    return false;
                }
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHLiteral, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00b4, code lost:
        throw createParseException("Error or number too big for integer type: " + r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b5, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b6, code lost:
        r1 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x015a, code lost:
        throw createParseException("Error parsing character: " + r0.image);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0198, code lost:
        throw createParseException("Error parsing string: " + r0.image);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:25:0x005a, B:31:0x008c, B:59:0x012c, B:70:0x016a] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Literal() {
        /*
            r7 = this;
            r1 = -1
            r2 = 1
            r3 = 0
            bsh.BSHLiteral r4 = new bsh.BSHLiteral
            r0 = 21
            r4.<init>(r0)
            bsh.JJTParserState r0 = r7.jjtree
            r0.openNodeScope(r4)
            r7.jjtreeOpenNodeScope(r4)
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            if (r0 != r1) goto L_0x004b
            int r0 = r7.jj_ntk()     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
        L_0x001a:
            switch(r0) {
                case 26: goto L_0x0199;
                case 41: goto L_0x01b1;
                case 55: goto L_0x0199;
                case 57: goto L_0x01c3;
                case 60: goto L_0x004e;
                case 64: goto L_0x00b9;
                case 66: goto L_0x011d;
                case 67: goto L_0x015b;
                default: goto L_0x001d;
            }     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
        L_0x001d:
            int[] r0 = r7.jj_la1     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r1 = 61
            int r5 = r7.jj_gen     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r0[r1] = r5     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r0 = -1
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r0.<init>()     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            throw r0     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
        L_0x002f:
            r0 = move-exception
            r1 = r2
        L_0x0031:
            if (r1 == 0) goto L_0x01d5
            bsh.JJTParserState r5 = r7.jjtree     // Catch:{ all -> 0x01eb }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x01eb }
        L_0x0038:
            boolean r1 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x01dd
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x003f }
            throw r0     // Catch:{ all -> 0x003f }
        L_0x003f:
            r0 = move-exception
        L_0x0040:
            if (r3 == 0) goto L_0x004a
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r4, r2)
            r7.jjtreeCloseNodeScope(r4)
        L_0x004a:
            throw r0
        L_0x004b:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            goto L_0x001a
        L_0x004e:
            r0 = 60
            bsh.Token r0 = r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            bsh.JJTParserState r1 = r7.jjtree     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r5 = 1
            r1.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r0 = r0.image     // Catch:{ Throwable -> 0x00b5 }
            int r1 = r0.length()     // Catch:{ Throwable -> 0x00b5 }
            int r1 = r1 + -1
            char r1 = r0.charAt(r1)     // Catch:{ Throwable -> 0x00b5 }
            r5 = 108(0x6c, float:1.51E-43)
            if (r1 == r5) goto L_0x0071
            r5 = 76
            if (r1 != r5) goto L_0x008c
        L_0x0071:
            r1 = 0
            int r5 = r0.length()     // Catch:{ Throwable -> 0x00b5 }
            int r5 = r5 + -1
            java.lang.String r0 = r0.substring(r1, r5)     // Catch:{ Throwable -> 0x00b5 }
            bsh.Primitive r1 = new bsh.Primitive     // Catch:{ Throwable -> 0x00b5 }
            java.lang.Long r0 = java.lang.Long.decode(r0)     // Catch:{ Throwable -> 0x00b5 }
            long r5 = r0.longValue()     // Catch:{ Throwable -> 0x00b5 }
            r1.<init>(r5)     // Catch:{ Throwable -> 0x00b5 }
            r4.value = r1     // Catch:{ Throwable -> 0x00b5 }
        L_0x008b:
            return
        L_0x008c:
            bsh.Primitive r1 = new bsh.Primitive     // Catch:{ NumberFormatException -> 0x009c }
            java.lang.Integer r5 = java.lang.Integer.decode(r0)     // Catch:{ NumberFormatException -> 0x009c }
            int r5 = r5.intValue()     // Catch:{ NumberFormatException -> 0x009c }
            r1.<init>(r5)     // Catch:{ NumberFormatException -> 0x009c }
            r4.value = r1     // Catch:{ NumberFormatException -> 0x009c }
            goto L_0x008b
        L_0x009c:
            r1 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00b5 }
            r1.<init>()     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r5 = "Error or number too big for integer type: "
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00b5 }
            bsh.ParseException r0 = r7.createParseException(r0)     // Catch:{ Throwable -> 0x00b5 }
            throw r0     // Catch:{ Throwable -> 0x00b5 }
        L_0x00b5:
            r0 = move-exception
            r1 = r3
            goto L_0x0031
        L_0x00b9:
            r0 = 64
            bsh.Token r0 = r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            bsh.JJTParserState r1 = r7.jjtree     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r5 = 1
            r1.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r0 = r0.image     // Catch:{ Throwable -> 0x00b5 }
            int r1 = r0.length()     // Catch:{ Throwable -> 0x00b5 }
            int r1 = r1 + -1
            char r1 = r0.charAt(r1)     // Catch:{ Throwable -> 0x00b5 }
            r5 = 102(0x66, float:1.43E-43)
            if (r1 == r5) goto L_0x00dc
            r5 = 70
            if (r1 != r5) goto L_0x00f8
        L_0x00dc:
            r1 = 0
            int r5 = r0.length()     // Catch:{ Throwable -> 0x00b5 }
            int r5 = r5 + -1
            java.lang.String r0 = r0.substring(r1, r5)     // Catch:{ Throwable -> 0x00b5 }
            bsh.Primitive r1 = new bsh.Primitive     // Catch:{ Throwable -> 0x00b5 }
            java.lang.Float r5 = new java.lang.Float     // Catch:{ Throwable -> 0x00b5 }
            r5.<init>(r0)     // Catch:{ Throwable -> 0x00b5 }
            float r0 = r5.floatValue()     // Catch:{ Throwable -> 0x00b5 }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x00b5 }
            r4.value = r1     // Catch:{ Throwable -> 0x00b5 }
            goto L_0x008b
        L_0x00f8:
            r5 = 100
            if (r1 == r5) goto L_0x0100
            r5 = 68
            if (r1 != r5) goto L_0x010b
        L_0x0100:
            r1 = 0
            int r5 = r0.length()     // Catch:{ Throwable -> 0x00b5 }
            int r5 = r5 + -1
            java.lang.String r0 = r0.substring(r1, r5)     // Catch:{ Throwable -> 0x00b5 }
        L_0x010b:
            bsh.Primitive r1 = new bsh.Primitive     // Catch:{ Throwable -> 0x00b5 }
            java.lang.Double r5 = new java.lang.Double     // Catch:{ Throwable -> 0x00b5 }
            r5.<init>(r0)     // Catch:{ Throwable -> 0x00b5 }
            double r5 = r5.doubleValue()     // Catch:{ Throwable -> 0x00b5 }
            r1.<init>(r5)     // Catch:{ Throwable -> 0x00b5 }
            r4.value = r1     // Catch:{ Throwable -> 0x00b5 }
            goto L_0x008b
        L_0x011d:
            r0 = 66
            bsh.Token r0 = r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            bsh.JJTParserState r1 = r7.jjtree     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r5 = 1
            r1.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r1 = r0.image     // Catch:{ Exception -> 0x0140 }
            r5 = 1
            java.lang.String r6 = r0.image     // Catch:{ Exception -> 0x0140 }
            int r6 = r6.length()     // Catch:{ Exception -> 0x0140 }
            int r6 = r6 + -1
            java.lang.String r1 = r1.substring(r5, r6)     // Catch:{ Exception -> 0x0140 }
            r4.charSetup(r1)     // Catch:{ Exception -> 0x0140 }
            goto L_0x008b
        L_0x0140:
            r1 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00b5 }
            r1.<init>()     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r5 = "Error parsing character: "
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r0 = r0.image     // Catch:{ Throwable -> 0x00b5 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00b5 }
            bsh.ParseException r0 = r7.createParseException(r0)     // Catch:{ Throwable -> 0x00b5 }
            throw r0     // Catch:{ Throwable -> 0x00b5 }
        L_0x015b:
            r0 = 67
            bsh.Token r0 = r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            bsh.JJTParserState r1 = r7.jjtree     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r5 = 1
            r1.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r1 = r0.image     // Catch:{ Exception -> 0x017e }
            r5 = 1
            java.lang.String r6 = r0.image     // Catch:{ Exception -> 0x017e }
            int r6 = r6.length()     // Catch:{ Exception -> 0x017e }
            int r6 = r6 + -1
            java.lang.String r1 = r1.substring(r5, r6)     // Catch:{ Exception -> 0x017e }
            r4.stringSetup(r1)     // Catch:{ Exception -> 0x017e }
            goto L_0x008b
        L_0x017e:
            r1 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00b5 }
            r1.<init>()     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r5 = "Error parsing string: "
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r0 = r0.image     // Catch:{ Throwable -> 0x00b5 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00b5 }
            bsh.ParseException r0 = r7.createParseException(r0)     // Catch:{ Throwable -> 0x00b5 }
            throw r0     // Catch:{ Throwable -> 0x00b5 }
        L_0x0199:
            boolean r0 = r7.BooleanLiteral()     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            bsh.JJTParserState r1 = r7.jjtree     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r5 = 1
            r1.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x00b5 }
            if (r0 == 0) goto L_0x01ae
            bsh.Primitive r0 = bsh.Primitive.TRUE     // Catch:{ Throwable -> 0x00b5 }
        L_0x01aa:
            r4.value = r0     // Catch:{ Throwable -> 0x00b5 }
            goto L_0x008b
        L_0x01ae:
            bsh.Primitive r0 = bsh.Primitive.FALSE     // Catch:{ Throwable -> 0x00b5 }
            goto L_0x01aa
        L_0x01b1:
            r7.NullLiteral()     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            bsh.JJTParserState r0 = r7.jjtree     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r1 = 1
            r0.closeNodeScope(r4, r1)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x00b5 }
            bsh.Primitive r0 = bsh.Primitive.NULL     // Catch:{ Throwable -> 0x00b5 }
            r4.value = r0     // Catch:{ Throwable -> 0x00b5 }
            goto L_0x008b
        L_0x01c3:
            r7.VoidLiteral()     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            bsh.JJTParserState r0 = r7.jjtree     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r1 = 1
            r0.closeNodeScope(r4, r1)     // Catch:{ Throwable -> 0x002f, all -> 0x01e7 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x00b5 }
            bsh.Primitive r0 = bsh.Primitive.VOID     // Catch:{ Throwable -> 0x00b5 }
            r4.value = r0     // Catch:{ Throwable -> 0x00b5 }
            goto L_0x008b
        L_0x01d5:
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x01eb }
            r3.popNode()     // Catch:{ all -> 0x01eb }
            r3 = r1
            goto L_0x0038
        L_0x01dd:
            boolean r1 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x01e4
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x003f }
            throw r0     // Catch:{ all -> 0x003f }
        L_0x01e4:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x003f }
            throw r0     // Catch:{ all -> 0x003f }
        L_0x01e7:
            r0 = move-exception
            r3 = r2
            goto L_0x0040
        L_0x01eb:
            r0 = move-exception
            r3 = r1
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.Literal():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHMethodDeclaration, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void MethodDeclaration() {
        /*
            r7 = this;
            r1 = 0
            r2 = 1
            r6 = -1
            bsh.BSHMethodDeclaration r3 = new bsh.BSHMethodDeclaration
            r0 = 2
            r3.<init>(r0)
            bsh.JJTParserState r0 = r7.jjtree
            r0.openNodeScope(r3)
            r7.jjtreeOpenNodeScope(r3)
            r0 = 1
            r4 = 0
            bsh.Modifiers r0 = r7.Modifiers(r0, r4)     // Catch:{ Throwable -> 0x005e }
            r3.modifiers = r0     // Catch:{ Throwable -> 0x005e }
            r0 = 2147483647(0x7fffffff, float:NaN)
            boolean r0 = r7.jj_2_2(r0)     // Catch:{ Throwable -> 0x005e }
            if (r0 == 0) goto L_0x0077
            r0 = 69
            bsh.Token r0 = r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005e }
            java.lang.String r0 = r0.image     // Catch:{ Throwable -> 0x005e }
            r3.name = r0     // Catch:{ Throwable -> 0x005e }
        L_0x002c:
            r7.FormalParameters()     // Catch:{ Throwable -> 0x005e }
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x005e }
            if (r0 != r6) goto L_0x00a7
            int r0 = r7.jj_ntk()     // Catch:{ Throwable -> 0x005e }
        L_0x0037:
            switch(r0) {
                case 54: goto L_0x00aa;
                default: goto L_0x003a;
            }     // Catch:{ Throwable -> 0x005e }
        L_0x003a:
            int[] r0 = r7.jj_la1     // Catch:{ Throwable -> 0x005e }
            r4 = 7
            int r5 = r7.jj_gen     // Catch:{ Throwable -> 0x005e }
            r0[r4] = r5     // Catch:{ Throwable -> 0x005e }
        L_0x0041:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x005e }
            if (r0 != r6) goto L_0x00b6
            int r0 = r7.jj_ntk()     // Catch:{ Throwable -> 0x005e }
        L_0x0049:
            switch(r0) {
                case 48: goto L_0x00b9;
                case 74: goto L_0x00b9;
                case 78: goto L_0x00c5;
                default: goto L_0x004c;
            }     // Catch:{ Throwable -> 0x005e }
        L_0x004c:
            int[] r0 = r7.jj_la1     // Catch:{ Throwable -> 0x005e }
            r4 = 8
            int r5 = r7.jj_gen     // Catch:{ Throwable -> 0x005e }
            r0[r4] = r5     // Catch:{ Throwable -> 0x005e }
            r0 = -1
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005e }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x005e }
            r0.<init>()     // Catch:{ Throwable -> 0x005e }
            throw r0     // Catch:{ Throwable -> 0x005e }
        L_0x005e:
            r0 = move-exception
            bsh.JJTParserState r4 = r7.jjtree     // Catch:{ all -> 0x0093 }
            r4.clearNodeScope(r3)     // Catch:{ all -> 0x0093 }
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x006b }
            if (r4 == 0) goto L_0x00cb
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x006b }
            throw r0     // Catch:{ all -> 0x006b }
        L_0x006b:
            r0 = move-exception
        L_0x006c:
            if (r1 == 0) goto L_0x0076
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r3, r2)
            r7.jjtreeCloseNodeScope(r3)
        L_0x0076:
            throw r0
        L_0x0077:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x005e }
            if (r0 != r6) goto L_0x0096
            int r0 = r7.jj_ntk()     // Catch:{ Throwable -> 0x005e }
        L_0x007f:
            switch(r0) {
                case 11: goto L_0x0099;
                case 14: goto L_0x0099;
                case 17: goto L_0x0099;
                case 22: goto L_0x0099;
                case 29: goto L_0x0099;
                case 36: goto L_0x0099;
                case 38: goto L_0x0099;
                case 47: goto L_0x0099;
                case 57: goto L_0x0099;
                case 69: goto L_0x0099;
                default: goto L_0x0082;
            }     // Catch:{ Throwable -> 0x005e }
        L_0x0082:
            int[] r0 = r7.jj_la1     // Catch:{ Throwable -> 0x005e }
            r4 = 6
            int r5 = r7.jj_gen     // Catch:{ Throwable -> 0x005e }
            r0[r4] = r5     // Catch:{ Throwable -> 0x005e }
            r0 = -1
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005e }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x005e }
            r0.<init>()     // Catch:{ Throwable -> 0x005e }
            throw r0     // Catch:{ Throwable -> 0x005e }
        L_0x0093:
            r0 = move-exception
            r1 = r2
            goto L_0x006c
        L_0x0096:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x005e }
            goto L_0x007f
        L_0x0099:
            r7.ReturnType()     // Catch:{ Throwable -> 0x005e }
            r0 = 69
            bsh.Token r0 = r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005e }
            java.lang.String r0 = r0.image     // Catch:{ Throwable -> 0x005e }
            r3.name = r0     // Catch:{ Throwable -> 0x005e }
            goto L_0x002c
        L_0x00a7:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x005e }
            goto L_0x0037
        L_0x00aa:
            r0 = 54
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005e }
            int r0 = r7.NameList()     // Catch:{ Throwable -> 0x005e }
            r3.numThrows = r0     // Catch:{ Throwable -> 0x005e }
            goto L_0x0041
        L_0x00b6:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x005e }
            goto L_0x0049
        L_0x00b9:
            r7.Block()     // Catch:{ Throwable -> 0x005e }
        L_0x00bc:
            bsh.JJTParserState r0 = r7.jjtree
            r0.closeNodeScope(r3, r2)
            r7.jjtreeCloseNodeScope(r3)
            return
        L_0x00c5:
            r0 = 78
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005e }
            goto L_0x00bc
        L_0x00cb:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x006b }
            if (r4 == 0) goto L_0x00d2
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x006b }
            throw r0     // Catch:{ all -> 0x006b }
        L_0x00d2:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x006b }
            throw r0     // Catch:{ all -> 0x006b }
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.MethodDeclaration():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHMethodInvocation, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void MethodInvocation() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHMethodInvocation r3 = new bsh.BSHMethodInvocation
            r0 = 19
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r5.AmbiguousName()     // Catch:{ Throwable -> 0x001f }
            r5.Arguments()     // Catch:{ Throwable -> 0x001f }
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x001f:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x0043 }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x0043 }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x002d }
            if (r4 == 0) goto L_0x0039
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x002d }
            throw r0     // Catch:{ all -> 0x002d }
        L_0x002d:
            r0 = move-exception
        L_0x002e:
            if (r1 == 0) goto L_0x0038
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0038:
            throw r0
        L_0x0039:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x002d }
            if (r4 == 0) goto L_0x0040
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x002d }
            throw r0     // Catch:{ all -> 0x002d }
        L_0x0040:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x002d }
            throw r0     // Catch:{ all -> 0x002d }
        L_0x0043:
            r0 = move-exception
            r1 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.MethodInvocation():void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x000d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0006  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0015  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0018 A[FALL_THROUGH] */
    public final bsh.Modifiers Modifiers(int r5, boolean r6) {
        /*
            r4 = this;
            r3 = -1
            r0 = 0
        L_0x0002:
            int r1 = r4.jj_ntk
            if (r1 != r3) goto L_0x0015
            int r1 = r4.jj_ntk()
        L_0x000a:
            switch(r1) {
                case 10: goto L_0x0018;
                case 27: goto L_0x0018;
                case 39: goto L_0x0018;
                case 43: goto L_0x0018;
                case 44: goto L_0x0018;
                case 45: goto L_0x0018;
                case 48: goto L_0x0018;
                case 49: goto L_0x0018;
                case 51: goto L_0x0018;
                case 52: goto L_0x0018;
                case 58: goto L_0x0018;
                default: goto L_0x000d;
            }
        L_0x000d:
            int[] r1 = r4.jj_la1
            r2 = 1
            int r3 = r4.jj_gen
            r1[r2] = r3
            return r0
        L_0x0015:
            int r1 = r4.jj_ntk
            goto L_0x000a
        L_0x0018:
            int r1 = r4.jj_ntk
            if (r1 != r3) goto L_0x0033
            int r1 = r4.jj_ntk()
        L_0x0020:
            switch(r1) {
                case 10: goto L_0x0083;
                case 27: goto L_0x006b;
                case 39: goto L_0x0071;
                case 43: goto L_0x0036;
                case 44: goto L_0x0059;
                case 45: goto L_0x005f;
                case 48: goto L_0x0089;
                case 49: goto L_0x008f;
                case 51: goto L_0x0065;
                case 52: goto L_0x0077;
                case 58: goto L_0x007d;
                default: goto L_0x0023;
            }
        L_0x0023:
            int[] r0 = r4.jj_la1
            r1 = 2
            int r2 = r4.jj_gen
            r0[r1] = r2
            r4.jj_consume_token(r3)
            bsh.ParseException r0 = new bsh.ParseException
            r0.<init>()
            throw r0
        L_0x0033:
            int r1 = r4.jj_ntk
            goto L_0x0020
        L_0x0036:
            r1 = 43
            r4.jj_consume_token(r1)
        L_0x003b:
            if (r6 != 0) goto L_0x0002
            if (r0 != 0) goto L_0x0044
            bsh.Modifiers r0 = new bsh.Modifiers     // Catch:{ IllegalStateException -> 0x004f }
            r0.<init>()     // Catch:{ IllegalStateException -> 0x004f }
        L_0x0044:
            r1 = 0
            bsh.Token r1 = r4.getToken(r1)     // Catch:{ IllegalStateException -> 0x004f }
            java.lang.String r1 = r1.image     // Catch:{ IllegalStateException -> 0x004f }
            r0.addModifier(r5, r1)     // Catch:{ IllegalStateException -> 0x004f }
            goto L_0x0002
        L_0x004f:
            r0 = move-exception
            java.lang.String r0 = r0.getMessage()
            bsh.ParseException r0 = r4.createParseException(r0)
            throw r0
        L_0x0059:
            r1 = 44
            r4.jj_consume_token(r1)
            goto L_0x003b
        L_0x005f:
            r1 = 45
            r4.jj_consume_token(r1)
            goto L_0x003b
        L_0x0065:
            r1 = 51
            r4.jj_consume_token(r1)
            goto L_0x003b
        L_0x006b:
            r1 = 27
            r4.jj_consume_token(r1)
            goto L_0x003b
        L_0x0071:
            r1 = 39
            r4.jj_consume_token(r1)
            goto L_0x003b
        L_0x0077:
            r1 = 52
            r4.jj_consume_token(r1)
            goto L_0x003b
        L_0x007d:
            r1 = 58
            r4.jj_consume_token(r1)
            goto L_0x003b
        L_0x0083:
            r1 = 10
            r4.jj_consume_token(r1)
            goto L_0x003b
        L_0x0089:
            r1 = 48
            r4.jj_consume_token(r1)
            goto L_0x003b
        L_0x008f:
            r1 = 49
            r4.jj_consume_token(r1)
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.Modifiers(int, boolean):bsh.Modifiers");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0010 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH] */
    public final void MultiplicativeExpression() {
        /*
            r7 = this;
            r6 = 2
            r5 = -1
            r7.UnaryExpression()
        L_0x0005:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0019
            int r0 = r7.jj_ntk()
        L_0x000d:
            switch(r0) {
                case 104: goto L_0x001c;
                case 105: goto L_0x001c;
                case 111: goto L_0x001c;
                default: goto L_0x0010;
            }
        L_0x0010:
            int[] r0 = r7.jj_la1
            r1 = 44
            int r2 = r7.jj_gen
            r0[r1] = r2
            return
        L_0x0019:
            int r0 = r7.jj_ntk
            goto L_0x000d
        L_0x001c:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0038
            int r0 = r7.jj_ntk()
        L_0x0024:
            switch(r0) {
                case 104: goto L_0x003b;
                case 105: goto L_0x006f;
                case 111: goto L_0x0076;
                default: goto L_0x0027;
            }
        L_0x0027:
            int[] r0 = r7.jj_la1
            r1 = 45
            int r2 = r7.jj_gen
            r0[r1] = r2
            r7.jj_consume_token(r5)
            bsh.ParseException r0 = new bsh.ParseException
            r0.<init>()
            throw r0
        L_0x0038:
            int r0 = r7.jj_ntk
            goto L_0x0024
        L_0x003b:
            r0 = 104(0x68, float:1.46E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
        L_0x0041:
            r7.UnaryExpression()
            bsh.BSHBinaryExpression r2 = new bsh.BSHBinaryExpression
            r1 = 15
            r2.<init>(r1)
            r1 = 1
            bsh.JJTParserState r3 = r7.jjtree
            r3.openNodeScope(r2)
            r7.jjtreeOpenNodeScope(r2)
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x0063 }
            r4 = 2
            r3.closeNodeScope(r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 0
            r7.jjtreeCloseNodeScope(r2)     // Catch:{ all -> 0x0063 }
            int r0 = r0.kind     // Catch:{ all -> 0x0063 }
            r2.kind = r0     // Catch:{ all -> 0x0063 }
            goto L_0x0005
        L_0x0063:
            r0 = move-exception
            if (r1 == 0) goto L_0x006e
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r2, r6)
            r7.jjtreeCloseNodeScope(r2)
        L_0x006e:
            throw r0
        L_0x006f:
            r0 = 105(0x69, float:1.47E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        L_0x0076:
            r0 = 111(0x6f, float:1.56E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.MultiplicativeExpression():void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH, LOOP:0: B:1:0x0004->B:8:0x001c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0010 A[SYNTHETIC] */
    public final int NameList() {
        /*
            r4 = this;
            r4.AmbiguousName()
            r0 = 1
        L_0x0004:
            int r1 = r4.jj_ntk
            r2 = -1
            if (r1 != r2) goto L_0x0019
            int r1 = r4.jj_ntk()
        L_0x000d:
            switch(r1) {
                case 79: goto L_0x001c;
                default: goto L_0x0010;
            }
        L_0x0010:
            int[] r1 = r4.jj_la1
            r2 = 22
            int r3 = r4.jj_gen
            r1[r2] = r3
            return r0
        L_0x0019:
            int r1 = r4.jj_ntk
            goto L_0x000d
        L_0x001c:
            r1 = 79
            r4.jj_consume_token(r1)
            r4.AmbiguousName()
            int r0 = r0 + 1
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.NameList():int");
    }

    public final void NullLiteral() {
        jj_consume_token(41);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHPackageDeclaration, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void PackageDeclaration() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHPackageDeclaration r3 = new bsh.BSHPackageDeclaration
            r0 = 3
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 42
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0020 }
            r5.AmbiguousName()     // Catch:{ Throwable -> 0x0020 }
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x0020:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x0044 }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x0044 }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x002e }
            if (r4 == 0) goto L_0x003a
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x002e }
            throw r0     // Catch:{ all -> 0x002e }
        L_0x002e:
            r0 = move-exception
        L_0x002f:
            if (r1 == 0) goto L_0x0039
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0039:
            throw r0
        L_0x003a:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x002e }
            if (r4 == 0) goto L_0x0041
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x002e }
            throw r0     // Catch:{ all -> 0x002e }
        L_0x0041:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x002e }
            throw r0     // Catch:{ all -> 0x002e }
        L_0x0044:
            r0 = move-exception
            r1 = r2
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.PackageDeclaration():void");
    }

    public final void PostfixExpression() {
        Token jj_consume_token;
        boolean z;
        if (jj_2_12(Integer.MAX_VALUE)) {
            PrimaryExpression();
            switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                case ParserConstants.INCR:
                    jj_consume_token = jj_consume_token(100);
                    break;
                case ParserConstants.DECR:
                    jj_consume_token = jj_consume_token(ParserConstants.DECR);
                    break;
                default:
                    this.jj_la1[53] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
            BSHUnaryExpression bSHUnaryExpression = new BSHUnaryExpression(16);
            this.jjtree.openNodeScope(bSHUnaryExpression);
            jjtreeOpenNodeScope(bSHUnaryExpression);
            try {
                this.jjtree.closeNodeScope(bSHUnaryExpression, 1);
                z = false;
                try {
                    jjtreeCloseNodeScope(bSHUnaryExpression);
                    bSHUnaryExpression.kind = jj_consume_token.kind;
                    bSHUnaryExpression.postfix = true;
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Throwable th2) {
                th = th2;
                z = true;
                if (z) {
                    this.jjtree.closeNodeScope(bSHUnaryExpression, 1);
                    jjtreeCloseNodeScope(bSHUnaryExpression);
                }
                throw th;
            }
        } else {
            switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                case 11:
                case 14:
                case 17:
                case 22:
                case 26:
                case 29:
                case 36:
                case ParserConstants.LONG:
                case ParserConstants.NEW:
                case ParserConstants.NULL:
                case ParserConstants.SHORT:
                case ParserConstants.TRUE:
                case ParserConstants.VOID:
                case ParserConstants.INTEGER_LITERAL:
                case ParserConstants.FLOATING_POINT_LITERAL:
                case ParserConstants.CHARACTER_LITERAL:
                case ParserConstants.STRING_LITERAL:
                case ParserConstants.IDENTIFIER:
                case ParserConstants.LPAREN:
                    PrimaryExpression();
                    return;
                default:
                    this.jj_la1[54] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void PreDecrementExpression() {
        /*
            r5 = this;
            r2 = 1
            r0 = 101(0x65, float:1.42E-43)
            bsh.Token r0 = r5.jj_consume_token(r0)
            r5.PrimaryExpression()
            bsh.BSHUnaryExpression r3 = new bsh.BSHUnaryExpression
            r1 = 16
            r3.<init>(r1)
            bsh.JJTParserState r1 = r5.jjtree
            r1.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x0028 }
            r4 = 1
            r1.closeNodeScope(r3, r4)     // Catch:{ all -> 0x0028 }
            r1 = 0
            r5.jjtreeCloseNodeScope(r3)     // Catch:{ all -> 0x0035 }
            int r0 = r0.kind     // Catch:{ all -> 0x0035 }
            r3.kind = r0     // Catch:{ all -> 0x0035 }
            return
        L_0x0028:
            r0 = move-exception
            r1 = r2
        L_0x002a:
            if (r1 == 0) goto L_0x0034
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0034:
            throw r0
        L_0x0035:
            r0 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.PreDecrementExpression():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void PreIncrementExpression() {
        /*
            r5 = this;
            r2 = 1
            r0 = 100
            bsh.Token r0 = r5.jj_consume_token(r0)
            r5.PrimaryExpression()
            bsh.BSHUnaryExpression r3 = new bsh.BSHUnaryExpression
            r1 = 16
            r3.<init>(r1)
            bsh.JJTParserState r1 = r5.jjtree
            r1.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x0028 }
            r4 = 1
            r1.closeNodeScope(r3, r4)     // Catch:{ all -> 0x0028 }
            r1 = 0
            r5.jjtreeCloseNodeScope(r3)     // Catch:{ all -> 0x0035 }
            int r0 = r0.kind     // Catch:{ all -> 0x0035 }
            r3.kind = r0     // Catch:{ all -> 0x0035 }
            return
        L_0x0028:
            r0 = move-exception
            r1 = r2
        L_0x002a:
            if (r1 == 0) goto L_0x0034
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0034:
            throw r0
        L_0x0035:
            r0 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.PreIncrementExpression():void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0030 A[SYNTHETIC, Splitter:B:10:0x0030] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0033 A[Catch:{ Throwable -> 0x0037, all -> 0x005b, all -> 0x0045 }, FALL_THROUGH, LOOP:0: B:3:0x0013->B:12:0x0033, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x001f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0018 A[Catch:{ Throwable -> 0x0037, all -> 0x005b, all -> 0x0045 }] */
    public final void PrimaryExpression() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHPrimaryExpression r3 = new bsh.BSHPrimaryExpression
            r0 = 18
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r5.PrimaryPrefix()     // Catch:{ Throwable -> 0x0037 }
        L_0x0013:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0037 }
            r1 = -1
            if (r0 != r1) goto L_0x0030
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x0037 }
        L_0x001c:
            switch(r0) {
                case 74: goto L_0x0033;
                case 76: goto L_0x0033;
                case 80: goto L_0x0033;
                default: goto L_0x001f;
            }     // Catch:{ Throwable -> 0x0037 }
        L_0x001f:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x0037 }
            r1 = 56
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x0037 }
            r0[r1] = r4     // Catch:{ Throwable -> 0x0037 }
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x0030:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x0037 }
            goto L_0x001c
        L_0x0033:
            r5.PrimarySuffix()     // Catch:{ Throwable -> 0x0037 }
            goto L_0x0013
        L_0x0037:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x005b }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x005b }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0045 }
            if (r4 == 0) goto L_0x0051
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0045 }
            throw r0     // Catch:{ all -> 0x0045 }
        L_0x0045:
            r0 = move-exception
        L_0x0046:
            if (r1 == 0) goto L_0x0050
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0050:
            throw r0
        L_0x0051:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0045 }
            if (r4 == 0) goto L_0x0058
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0045 }
            throw r0     // Catch:{ all -> 0x0045 }
        L_0x0058:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0045 }
            throw r0     // Catch:{ all -> 0x0045 }
        L_0x005b:
            r0 = move-exception
            r1 = r2
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.PrimaryExpression():void");
    }

    public final void PrimaryPrefix() {
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 26:
            case ParserConstants.NULL:
            case ParserConstants.TRUE:
            case ParserConstants.VOID:
            case ParserConstants.INTEGER_LITERAL:
            case ParserConstants.FLOATING_POINT_LITERAL:
            case ParserConstants.CHARACTER_LITERAL:
            case ParserConstants.STRING_LITERAL:
                Literal();
                return;
            case ParserConstants.NEW:
                AllocationExpression();
                return;
            case ParserConstants.LPAREN:
                jj_consume_token(72);
                Expression();
                jj_consume_token(73);
                return;
            default:
                this.jj_la1[57] = this.jj_gen;
                if (jj_2_14(Integer.MAX_VALUE)) {
                    MethodInvocation();
                    return;
                } else if (jj_2_15(Integer.MAX_VALUE)) {
                    Type();
                    return;
                } else {
                    switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                        case ParserConstants.IDENTIFIER:
                            AmbiguousName();
                            return;
                        default:
                            this.jj_la1[58] = this.jj_gen;
                            jj_consume_token(-1);
                            throw new ParseException();
                    }
                }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHPrimarySuffix, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0060  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void PrimarySuffix() {
        /*
            r7 = this;
            r5 = -1
            r2 = 0
            r3 = 1
            bsh.BSHPrimarySuffix r4 = new bsh.BSHPrimarySuffix
            r0 = 20
            r4.<init>(r0)
            bsh.JJTParserState r0 = r7.jjtree
            r0.openNodeScope(r4)
            r7.jjtreeOpenNodeScope(r4)
            r0 = 2
            boolean r0 = r7.jj_2_16(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            if (r0 == 0) goto L_0x0030
            r0 = 80
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r0 = 13
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            bsh.JJTParserState r0 = r7.jjtree     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r1 = 1
            r0.closeNodeScope(r4, r1)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0086 }
            r0 = 0
            r4.operation = r0     // Catch:{ Throwable -> 0x0086 }
        L_0x002f:
            return
        L_0x0030:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            if (r0 != r5) goto L_0x0069
            int r0 = r7.jj_ntk()     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
        L_0x0038:
            switch(r0) {
                case 74: goto L_0x00c3;
                case 76: goto L_0x006c;
                case 80: goto L_0x0089;
                default: goto L_0x003b;
            }     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
        L_0x003b:
            int[] r0 = r7.jj_la1     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r1 = 60
            int r5 = r7.jj_gen     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r0[r1] = r5     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r0 = -1
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r0.<init>()     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            throw r0     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
        L_0x004d:
            r0 = move-exception
            r1 = r3
        L_0x004f:
            if (r1 == 0) goto L_0x00de
            bsh.JJTParserState r5 = r7.jjtree     // Catch:{ all -> 0x00f0 }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x00f0 }
        L_0x0056:
            boolean r1 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x005d }
            if (r1 == 0) goto L_0x00e6
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x005d }
            throw r0     // Catch:{ all -> 0x005d }
        L_0x005d:
            r0 = move-exception
        L_0x005e:
            if (r2 == 0) goto L_0x0068
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r4, r3)
            r7.jjtreeCloseNodeScope(r4)
        L_0x0068:
            throw r0
        L_0x0069:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            goto L_0x0038
        L_0x006c:
            r0 = 76
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r7.Expression()     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r0 = 77
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            bsh.JJTParserState r0 = r7.jjtree     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r1 = 1
            r0.closeNodeScope(r4, r1)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0086 }
            r0 = 1
            r4.operation = r0     // Catch:{ Throwable -> 0x0086 }
            goto L_0x002f
        L_0x0086:
            r0 = move-exception
            r1 = r2
            goto L_0x004f
        L_0x0089:
            r0 = 80
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r0 = 69
            bsh.Token r1 = r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            if (r0 != r5) goto L_0x00b9
            int r0 = r7.jj_ntk()     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
        L_0x009c:
            switch(r0) {
                case 72: goto L_0x00bc;
                default: goto L_0x009f;
            }     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
        L_0x009f:
            int[] r0 = r7.jj_la1     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r5 = 59
            int r6 = r7.jj_gen     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r0[r5] = r6     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
        L_0x00a7:
            bsh.JJTParserState r0 = r7.jjtree     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r5 = 1
            r0.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0086 }
            r0 = 2
            r4.operation = r0     // Catch:{ Throwable -> 0x0086 }
            java.lang.String r0 = r1.image     // Catch:{ Throwable -> 0x0086 }
            r4.field = r0     // Catch:{ Throwable -> 0x0086 }
            goto L_0x002f
        L_0x00b9:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            goto L_0x009c
        L_0x00bc:
            r7.Arguments()     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            goto L_0x00a7
        L_0x00c0:
            r0 = move-exception
            r2 = r3
            goto L_0x005e
        L_0x00c3:
            r0 = 74
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r7.Expression()     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r0 = 75
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            bsh.JJTParserState r0 = r7.jjtree     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r1 = 1
            r0.closeNodeScope(r4, r1)     // Catch:{ Throwable -> 0x004d, all -> 0x00c0 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0086 }
            r0 = 3
            r4.operation = r0     // Catch:{ Throwable -> 0x0086 }
            goto L_0x002f
        L_0x00de:
            bsh.JJTParserState r2 = r7.jjtree     // Catch:{ all -> 0x00f0 }
            r2.popNode()     // Catch:{ all -> 0x00f0 }
            r2 = r1
            goto L_0x0056
        L_0x00e6:
            boolean r1 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x005d }
            if (r1 == 0) goto L_0x00ed
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x005d }
            throw r0     // Catch:{ all -> 0x005d }
        L_0x00ed:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x005d }
            throw r0     // Catch:{ all -> 0x005d }
        L_0x00f0:
            r0 = move-exception
            r2 = r1
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.PrimarySuffix():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHPrimitiveType, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    public final void PrimitiveType() {
        boolean z = false;
        BSHPrimitiveType bSHPrimitiveType = new BSHPrimitiveType(11);
        this.jjtree.openNodeScope(bSHPrimitiveType);
        jjtreeOpenNodeScope(bSHPrimitiveType);
        try {
            switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                case 11:
                    jj_consume_token(11);
                    this.jjtree.closeNodeScope((Node) bSHPrimitiveType, true);
                    try {
                        jjtreeCloseNodeScope(bSHPrimitiveType);
                        bSHPrimitiveType.type = Boolean.TYPE;
                        return;
                    } catch (Throwable th) {
                        th = th;
                        break;
                    }
                case 14:
                    jj_consume_token(14);
                    this.jjtree.closeNodeScope((Node) bSHPrimitiveType, true);
                    jjtreeCloseNodeScope(bSHPrimitiveType);
                    bSHPrimitiveType.type = Byte.TYPE;
                    return;
                case 17:
                    jj_consume_token(17);
                    this.jjtree.closeNodeScope((Node) bSHPrimitiveType, true);
                    jjtreeCloseNodeScope(bSHPrimitiveType);
                    bSHPrimitiveType.type = Character.TYPE;
                    return;
                case 22:
                    jj_consume_token(22);
                    this.jjtree.closeNodeScope((Node) bSHPrimitiveType, true);
                    jjtreeCloseNodeScope(bSHPrimitiveType);
                    bSHPrimitiveType.type = Double.TYPE;
                    return;
                case 29:
                    jj_consume_token(29);
                    this.jjtree.closeNodeScope((Node) bSHPrimitiveType, true);
                    jjtreeCloseNodeScope(bSHPrimitiveType);
                    bSHPrimitiveType.type = Float.TYPE;
                    return;
                case 36:
                    jj_consume_token(36);
                    this.jjtree.closeNodeScope((Node) bSHPrimitiveType, true);
                    jjtreeCloseNodeScope(bSHPrimitiveType);
                    bSHPrimitiveType.type = Integer.TYPE;
                    return;
                case ParserConstants.LONG:
                    jj_consume_token(38);
                    this.jjtree.closeNodeScope((Node) bSHPrimitiveType, true);
                    jjtreeCloseNodeScope(bSHPrimitiveType);
                    bSHPrimitiveType.type = Long.TYPE;
                    return;
                case ParserConstants.SHORT:
                    jj_consume_token(47);
                    this.jjtree.closeNodeScope((Node) bSHPrimitiveType, true);
                    jjtreeCloseNodeScope(bSHPrimitiveType);
                    bSHPrimitiveType.type = Short.TYPE;
                    return;
                default:
                    this.jj_la1[21] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
        } catch (Throwable th2) {
            th = th2;
            z = true;
            if (z) {
                this.jjtree.closeNodeScope((Node) bSHPrimitiveType, true);
                jjtreeCloseNodeScope(bSHPrimitiveType);
            }
            throw th;
        }
    }

    public void ReInit(ParserTokenManager parserTokenManager) {
        this.token_source = parserTokenManager;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jjtree.reset();
        this.jj_gen = 0;
        for (int i = 0; i < 88; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public void ReInit(InputStream inputStream) {
        this.jj_input_stream.ReInit(inputStream, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jjtree.reset();
        this.jj_gen = 0;
        for (int i = 0; i < 88; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public void ReInit(Reader reader) {
        this.jj_input_stream.ReInit(reader, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jjtree.reset();
        this.jj_gen = 0;
        for (int i = 0; i < 88; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0010 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH] */
    public final void RelationalExpression() {
        /*
            r7 = this;
            r6 = 2
            r5 = -1
            r7.ShiftExpression()
        L_0x0005:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0019
            int r0 = r7.jj_ntk()
        L_0x000d:
            switch(r0) {
                case 82: goto L_0x001c;
                case 83: goto L_0x001c;
                case 84: goto L_0x001c;
                case 85: goto L_0x001c;
                case 86: goto L_0x0010;
                case 87: goto L_0x0010;
                case 88: goto L_0x0010;
                case 89: goto L_0x0010;
                case 90: goto L_0x0010;
                case 91: goto L_0x001c;
                case 92: goto L_0x001c;
                case 93: goto L_0x001c;
                case 94: goto L_0x001c;
                default: goto L_0x0010;
            }
        L_0x0010:
            int[] r0 = r7.jj_la1
            r1 = 38
            int r2 = r7.jj_gen
            r0[r1] = r2
            return
        L_0x0019:
            int r0 = r7.jj_ntk
            goto L_0x000d
        L_0x001c:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0038
            int r0 = r7.jj_ntk()
        L_0x0024:
            switch(r0) {
                case 82: goto L_0x0076;
                case 83: goto L_0x007d;
                case 84: goto L_0x003b;
                case 85: goto L_0x006f;
                case 86: goto L_0x0027;
                case 87: goto L_0x0027;
                case 88: goto L_0x0027;
                case 89: goto L_0x0027;
                case 90: goto L_0x0027;
                case 91: goto L_0x0084;
                case 92: goto L_0x008b;
                case 93: goto L_0x0092;
                case 94: goto L_0x0099;
                default: goto L_0x0027;
            }
        L_0x0027:
            int[] r0 = r7.jj_la1
            r1 = 39
            int r2 = r7.jj_gen
            r0[r1] = r2
            r7.jj_consume_token(r5)
            bsh.ParseException r0 = new bsh.ParseException
            r0.<init>()
            throw r0
        L_0x0038:
            int r0 = r7.jj_ntk
            goto L_0x0024
        L_0x003b:
            r0 = 84
            bsh.Token r0 = r7.jj_consume_token(r0)
        L_0x0041:
            r7.ShiftExpression()
            bsh.BSHBinaryExpression r2 = new bsh.BSHBinaryExpression
            r1 = 15
            r2.<init>(r1)
            r1 = 1
            bsh.JJTParserState r3 = r7.jjtree
            r3.openNodeScope(r2)
            r7.jjtreeOpenNodeScope(r2)
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x0063 }
            r4 = 2
            r3.closeNodeScope(r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 0
            r7.jjtreeCloseNodeScope(r2)     // Catch:{ all -> 0x0063 }
            int r0 = r0.kind     // Catch:{ all -> 0x0063 }
            r2.kind = r0     // Catch:{ all -> 0x0063 }
            goto L_0x0005
        L_0x0063:
            r0 = move-exception
            if (r1 == 0) goto L_0x006e
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r2, r6)
            r7.jjtreeCloseNodeScope(r2)
        L_0x006e:
            throw r0
        L_0x006f:
            r0 = 85
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        L_0x0076:
            r0 = 82
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        L_0x007d:
            r0 = 83
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        L_0x0084:
            r0 = 91
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        L_0x008b:
            r0 = 92
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        L_0x0092:
            r0 = 93
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        L_0x0099:
            r0 = 94
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.RelationalExpression():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHReturnStatement, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0048 A[SYNTHETIC, Splitter:B:17:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0052 A[Catch:{ all -> 0x0055 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0061 A[SYNTHETIC, Splitter:B:27:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0067 A[Catch:{ all -> 0x0055 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ReturnStatement() {
        /*
            r6 = this;
            r3 = 0
            r2 = 1
            bsh.BSHReturnStatement r4 = new bsh.BSHReturnStatement
            r0 = 35
            r4.<init>(r0)
            bsh.JJTParserState r0 = r6.jjtree
            r0.openNodeScope(r4)
            r6.jjtreeOpenNodeScope(r4)
            r0 = 46
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            r1 = -1
            if (r0 != r1) goto L_0x003d
            int r0 = r6.jj_ntk()     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
        L_0x001f:
            switch(r0) {
                case 11: goto L_0x0040;
                case 14: goto L_0x0040;
                case 17: goto L_0x0040;
                case 22: goto L_0x0040;
                case 26: goto L_0x0040;
                case 29: goto L_0x0040;
                case 36: goto L_0x0040;
                case 38: goto L_0x0040;
                case 40: goto L_0x0040;
                case 41: goto L_0x0040;
                case 47: goto L_0x0040;
                case 55: goto L_0x0040;
                case 57: goto L_0x0040;
                case 60: goto L_0x0040;
                case 64: goto L_0x0040;
                case 66: goto L_0x0040;
                case 67: goto L_0x0040;
                case 69: goto L_0x0040;
                case 72: goto L_0x0040;
                case 86: goto L_0x0040;
                case 87: goto L_0x0040;
                case 100: goto L_0x0040;
                case 101: goto L_0x0040;
                case 102: goto L_0x0040;
                case 103: goto L_0x0040;
                default: goto L_0x0022;
            }     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
        L_0x0022:
            int[] r0 = r6.jj_la1     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            r1 = 85
            int r5 = r6.jj_gen     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            r0[r1] = r5     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
        L_0x002a:
            r0 = 78
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            bsh.JJTParserState r0 = r6.jjtree     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            r1 = 1
            r0.closeNodeScope(r4, r1)     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            r6.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0077, all -> 0x0074 }
            r0 = 46
            r4.kind = r0     // Catch:{ Throwable -> 0x0077, all -> 0x0074 }
            return
        L_0x003d:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            goto L_0x001f
        L_0x0040:
            r6.Expression()     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            goto L_0x002a
        L_0x0044:
            r0 = move-exception
            r1 = r2
        L_0x0046:
            if (r1 == 0) goto L_0x0061
            bsh.JJTParserState r5 = r6.jjtree     // Catch:{ all -> 0x0055 }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x0055 }
            r1 = r3
        L_0x004e:
            boolean r3 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0055 }
            if (r3 == 0) goto L_0x0067
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0055 }
            throw r0     // Catch:{ all -> 0x0055 }
        L_0x0055:
            r0 = move-exception
        L_0x0056:
            if (r1 == 0) goto L_0x0060
            bsh.JJTParserState r1 = r6.jjtree
            r1.closeNodeScope(r4, r2)
            r6.jjtreeCloseNodeScope(r4)
        L_0x0060:
            throw r0
        L_0x0061:
            bsh.JJTParserState r3 = r6.jjtree     // Catch:{ all -> 0x0055 }
            r3.popNode()     // Catch:{ all -> 0x0055 }
            goto L_0x004e
        L_0x0067:
            boolean r3 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0055 }
            if (r3 == 0) goto L_0x006e
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0055 }
            throw r0     // Catch:{ all -> 0x0055 }
        L_0x006e:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0055 }
            throw r0     // Catch:{ all -> 0x0055 }
        L_0x0071:
            r0 = move-exception
            r1 = r2
            goto L_0x0056
        L_0x0074:
            r0 = move-exception
            r1 = r3
            goto L_0x0056
        L_0x0077:
            r0 = move-exception
            r1 = r3
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ReturnStatement():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHReturnType, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033 A[SYNTHETIC, Splitter:B:11:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003d A[Catch:{ all -> 0x0040 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0070 A[SYNTHETIC, Splitter:B:31:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0076 A[Catch:{ all -> 0x0040 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ReturnType() {
        /*
            r6 = this;
            r3 = 0
            r1 = -1
            r2 = 1
            bsh.BSHReturnType r4 = new bsh.BSHReturnType
            r0 = 10
            r4.<init>(r0)
            bsh.JJTParserState r0 = r6.jjtree
            r0.openNodeScope(r4)
            r6.jjtreeOpenNodeScope(r4)
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            if (r0 != r1) goto L_0x004c
            int r0 = r6.jj_ntk()     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
        L_0x001a:
            switch(r0) {
                case 11: goto L_0x006b;
                case 14: goto L_0x006b;
                case 17: goto L_0x006b;
                case 22: goto L_0x006b;
                case 29: goto L_0x006b;
                case 36: goto L_0x006b;
                case 38: goto L_0x006b;
                case 47: goto L_0x006b;
                case 57: goto L_0x004f;
                case 69: goto L_0x006b;
                default: goto L_0x001d;
            }     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
        L_0x001d:
            int[] r0 = r6.jj_la1     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            r1 = 20
            int r5 = r6.jj_gen     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            r0[r1] = r5     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            r0 = -1
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            r0.<init>()     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            throw r0     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
        L_0x002f:
            r0 = move-exception
            r1 = r2
        L_0x0031:
            if (r1 == 0) goto L_0x0070
            bsh.JJTParserState r5 = r6.jjtree     // Catch:{ all -> 0x0040 }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x0040 }
            r1 = r3
        L_0x0039:
            boolean r3 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0040 }
            if (r3 == 0) goto L_0x0076
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0040 }
            throw r0     // Catch:{ all -> 0x0040 }
        L_0x0040:
            r0 = move-exception
        L_0x0041:
            if (r1 == 0) goto L_0x004b
            bsh.JJTParserState r1 = r6.jjtree
            r1.closeNodeScope(r4, r2)
            r6.jjtreeCloseNodeScope(r4)
        L_0x004b:
            throw r0
        L_0x004c:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            goto L_0x001a
        L_0x004f:
            r0 = 57
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            bsh.JJTParserState r0 = r6.jjtree     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            r1 = 1
            r0.closeNodeScope(r4, r1)     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            r6.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0086, all -> 0x0083 }
            r0 = 1
            r4.isVoid = r0     // Catch:{ Throwable -> 0x0086, all -> 0x0083 }
        L_0x0060:
            if (r3 == 0) goto L_0x006a
            bsh.JJTParserState r0 = r6.jjtree
            r0.closeNodeScope(r4, r2)
            r6.jjtreeCloseNodeScope(r4)
        L_0x006a:
            return
        L_0x006b:
            r6.Type()     // Catch:{ Throwable -> 0x002f, all -> 0x0080 }
            r3 = r2
            goto L_0x0060
        L_0x0070:
            bsh.JJTParserState r3 = r6.jjtree     // Catch:{ all -> 0x0040 }
            r3.popNode()     // Catch:{ all -> 0x0040 }
            goto L_0x0039
        L_0x0076:
            boolean r3 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0040 }
            if (r3 == 0) goto L_0x007d
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0040 }
            throw r0     // Catch:{ all -> 0x0040 }
        L_0x007d:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0040 }
            throw r0     // Catch:{ all -> 0x0040 }
        L_0x0080:
            r0 = move-exception
            r1 = r2
            goto L_0x0041
        L_0x0083:
            r0 = move-exception
            r1 = r3
            goto L_0x0041
        L_0x0086:
            r0 = move-exception
            r1 = r3
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ReturnType():void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0010 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0019  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001c A[FALL_THROUGH] */
    public final void ShiftExpression() {
        /*
            r7 = this;
            r6 = 2
            r5 = -1
            r7.AdditiveExpression()
        L_0x0005:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0019
            int r0 = r7.jj_ntk()
        L_0x000d:
            switch(r0) {
                case 112: goto L_0x001c;
                case 113: goto L_0x001c;
                case 114: goto L_0x001c;
                case 115: goto L_0x001c;
                case 116: goto L_0x001c;
                case 117: goto L_0x001c;
                default: goto L_0x0010;
            }
        L_0x0010:
            int[] r0 = r7.jj_la1
            r1 = 40
            int r2 = r7.jj_gen
            r0[r1] = r2
            return
        L_0x0019:
            int r0 = r7.jj_ntk
            goto L_0x000d
        L_0x001c:
            int r0 = r7.jj_ntk
            if (r0 != r5) goto L_0x0038
            int r0 = r7.jj_ntk()
        L_0x0024:
            switch(r0) {
                case 112: goto L_0x003b;
                case 113: goto L_0x006f;
                case 114: goto L_0x0076;
                case 115: goto L_0x007d;
                case 116: goto L_0x0084;
                case 117: goto L_0x008b;
                default: goto L_0x0027;
            }
        L_0x0027:
            int[] r0 = r7.jj_la1
            r1 = 41
            int r2 = r7.jj_gen
            r0[r1] = r2
            r7.jj_consume_token(r5)
            bsh.ParseException r0 = new bsh.ParseException
            r0.<init>()
            throw r0
        L_0x0038:
            int r0 = r7.jj_ntk
            goto L_0x0024
        L_0x003b:
            r0 = 112(0x70, float:1.57E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
        L_0x0041:
            r7.AdditiveExpression()
            bsh.BSHBinaryExpression r2 = new bsh.BSHBinaryExpression
            r1 = 15
            r2.<init>(r1)
            r1 = 1
            bsh.JJTParserState r3 = r7.jjtree
            r3.openNodeScope(r2)
            r7.jjtreeOpenNodeScope(r2)
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x0063 }
            r4 = 2
            r3.closeNodeScope(r2, r4)     // Catch:{ all -> 0x0063 }
            r1 = 0
            r7.jjtreeCloseNodeScope(r2)     // Catch:{ all -> 0x0063 }
            int r0 = r0.kind     // Catch:{ all -> 0x0063 }
            r2.kind = r0     // Catch:{ all -> 0x0063 }
            goto L_0x0005
        L_0x0063:
            r0 = move-exception
            if (r1 == 0) goto L_0x006e
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r2, r6)
            r7.jjtreeCloseNodeScope(r2)
        L_0x006e:
            throw r0
        L_0x006f:
            r0 = 113(0x71, float:1.58E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        L_0x0076:
            r0 = 114(0x72, float:1.6E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        L_0x007d:
            r0 = 115(0x73, float:1.61E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        L_0x0084:
            r0 = 116(0x74, float:1.63E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        L_0x008b:
            r0 = 117(0x75, float:1.64E-43)
            bsh.Token r0 = r7.jj_consume_token(r0)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ShiftExpression():void");
    }

    public final void Statement() {
        if (jj_2_22(2)) {
            LabeledStatement();
            return;
        }
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 11:
            case 14:
            case 17:
            case 22:
            case 26:
            case 29:
            case 36:
            case ParserConstants.LONG:
            case ParserConstants.NEW:
            case ParserConstants.NULL:
            case ParserConstants.SHORT:
            case ParserConstants.TRUE:
            case ParserConstants.VOID:
            case ParserConstants.INTEGER_LITERAL:
            case ParserConstants.FLOATING_POINT_LITERAL:
            case ParserConstants.CHARACTER_LITERAL:
            case ParserConstants.STRING_LITERAL:
            case ParserConstants.IDENTIFIER:
            case ParserConstants.LPAREN:
            case ParserConstants.BANG:
            case ParserConstants.TILDE:
            case ParserConstants.INCR:
            case ParserConstants.DECR:
            case ParserConstants.PLUS:
            case ParserConstants.MINUS:
                Expression();
                jj_consume_token(78);
                return;
            case 21:
                DoStatement();
                return;
            case 32:
                IfStatement();
                return;
            case ParserConstants.STATIC:
            case ParserConstants.LBRACE:
                Block();
                return;
            case ParserConstants.SWITCH:
                SwitchStatement();
                return;
            case ParserConstants.WHILE:
                WhileStatement();
                return;
            case ParserConstants.SEMICOLON:
                jj_consume_token(78);
                return;
            default:
                this.jj_la1[69] = this.jj_gen;
                if (isRegularForStatement()) {
                    ForStatement();
                    return;
                }
                switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                    case 12:
                        BreakStatement();
                        return;
                    case 19:
                        ContinueStatement();
                        return;
                    case 30:
                        EnhancedForStatement();
                        return;
                    case ParserConstants.RETURN:
                        ReturnStatement();
                        return;
                    case ParserConstants.SYNCHRONIZED:
                        SynchronizedStatement();
                        return;
                    case ParserConstants.THROW:
                        ThrowStatement();
                        return;
                    case ParserConstants.TRY:
                        TryStatement();
                        return;
                    default:
                        this.jj_la1[70] = this.jj_gen;
                        jj_consume_token(-1);
                        throw new ParseException();
                }
        }
    }

    public final void StatementExpression() {
        Expression();
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0030 A[SYNTHETIC, Splitter:B:10:0x0030] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0033 A[Catch:{ Throwable -> 0x003c, all -> 0x0060, all -> 0x004a }, FALL_THROUGH, LOOP:0: B:3:0x0013->B:12:0x0033, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x001f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0018 A[Catch:{ Throwable -> 0x003c, all -> 0x0060, all -> 0x004a }] */
    public final void StatementExpressionList() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHStatementExpressionList r3 = new bsh.BSHStatementExpressionList
            r0 = 34
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r5.Expression()     // Catch:{ Throwable -> 0x003c }
        L_0x0013:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x003c }
            r1 = -1
            if (r0 != r1) goto L_0x0030
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x003c }
        L_0x001c:
            switch(r0) {
                case 79: goto L_0x0033;
                default: goto L_0x001f;
            }     // Catch:{ Throwable -> 0x003c }
        L_0x001f:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x003c }
            r1 = 82
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x003c }
            r0[r1] = r4     // Catch:{ Throwable -> 0x003c }
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x0030:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x003c }
            goto L_0x001c
        L_0x0033:
            r0 = 79
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x003c }
            r5.Expression()     // Catch:{ Throwable -> 0x003c }
            goto L_0x0013
        L_0x003c:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x0060 }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x0060 }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x004a }
            if (r4 == 0) goto L_0x0056
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x004a }
            throw r0     // Catch:{ all -> 0x004a }
        L_0x004a:
            r0 = move-exception
        L_0x004b:
            if (r1 == 0) goto L_0x0055
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0055:
            throw r0
        L_0x0056:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x004a }
            if (r4 == 0) goto L_0x005d
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x004a }
            throw r0     // Catch:{ all -> 0x004a }
        L_0x005d:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x004a }
            throw r0     // Catch:{ all -> 0x004a }
        L_0x0060:
            r0 = move-exception
            r1 = r2
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.StatementExpressionList():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHSwitchLabel, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0043  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void SwitchLabel() {
        /*
            r6 = this;
            r3 = 0
            r1 = -1
            r2 = 1
            bsh.BSHSwitchLabel r4 = new bsh.BSHSwitchLabel
            r0 = 28
            r4.<init>(r0)
            bsh.JJTParserState r0 = r6.jjtree
            r0.openNodeScope(r4)
            r6.jjtreeOpenNodeScope(r4)
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            if (r0 != r1) goto L_0x004c
            int r0 = r6.jj_ntk()     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
        L_0x001a:
            switch(r0) {
                case 15: goto L_0x004f;
                case 20: goto L_0x0068;
                default: goto L_0x001d;
            }     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
        L_0x001d:
            int[] r0 = r6.jj_la1     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            r1 = 74
            int r5 = r6.jj_gen     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            r0[r1] = r5     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            r0 = -1
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            r0.<init>()     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            throw r0     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
        L_0x002f:
            r0 = move-exception
            r1 = r2
        L_0x0031:
            if (r1 == 0) goto L_0x0082
            bsh.JJTParserState r5 = r6.jjtree     // Catch:{ all -> 0x0040 }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x0040 }
            r1 = r3
        L_0x0039:
            boolean r3 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0040 }
            if (r3 == 0) goto L_0x0088
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0040 }
            throw r0     // Catch:{ all -> 0x0040 }
        L_0x0040:
            r0 = move-exception
        L_0x0041:
            if (r1 == 0) goto L_0x004b
            bsh.JJTParserState r1 = r6.jjtree
            r1.closeNodeScope(r4, r2)
            r6.jjtreeCloseNodeScope(r4)
        L_0x004b:
            throw r0
        L_0x004c:
            int r0 = r6.jj_ntk     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            goto L_0x001a
        L_0x004f:
            r0 = 15
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            r6.Expression()     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            r0 = 89
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            r3 = r2
        L_0x005d:
            if (r3 == 0) goto L_0x0067
            bsh.JJTParserState r0 = r6.jjtree
            r0.closeNodeScope(r4, r2)
            r6.jjtreeCloseNodeScope(r4)
        L_0x0067:
            return
        L_0x0068:
            r0 = 20
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            r0 = 89
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            bsh.JJTParserState r0 = r6.jjtree     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            r1 = 1
            r0.closeNodeScope(r4, r1)     // Catch:{ Throwable -> 0x002f, all -> 0x0092 }
            r6.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x007f, all -> 0x0095 }
            r0 = 1
            r4.isDefault = r0     // Catch:{ Throwable -> 0x007f, all -> 0x0095 }
            goto L_0x005d
        L_0x007f:
            r0 = move-exception
            r1 = r3
            goto L_0x0031
        L_0x0082:
            bsh.JJTParserState r3 = r6.jjtree     // Catch:{ all -> 0x0040 }
            r3.popNode()     // Catch:{ all -> 0x0040 }
            goto L_0x0039
        L_0x0088:
            boolean r3 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0040 }
            if (r3 == 0) goto L_0x008f
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0040 }
            throw r0     // Catch:{ all -> 0x0040 }
        L_0x008f:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0040 }
            throw r0     // Catch:{ all -> 0x0040 }
        L_0x0092:
            r0 = move-exception
            r1 = r2
            goto L_0x0041
        L_0x0095:
            r0 = move-exception
            r1 = r3
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.SwitchLabel():void");
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0049 A[SYNTHETIC, Splitter:B:10:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004c A[Catch:{ Throwable -> 0x005a, all -> 0x007e, all -> 0x0068 }, FALL_THROUGH] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0033 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x002c A[Catch:{ Throwable -> 0x005a, all -> 0x007e, all -> 0x0068 }] */
    public final void SwitchStatement() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHSwitchStatement r3 = new bsh.BSHSwitchStatement
            r0 = 27
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 50
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005a }
            r0 = 72
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005a }
            r5.Expression()     // Catch:{ Throwable -> 0x005a }
            r0 = 73
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005a }
            r0 = 74
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005a }
        L_0x0027:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x005a }
            r1 = -1
            if (r0 != r1) goto L_0x0049
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x005a }
        L_0x0030:
            switch(r0) {
                case 15: goto L_0x004c;
                case 20: goto L_0x004c;
                default: goto L_0x0033;
            }     // Catch:{ Throwable -> 0x005a }
        L_0x0033:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x005a }
            r1 = 73
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x005a }
            r0[r1] = r4     // Catch:{ Throwable -> 0x005a }
            r0 = 75
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x005a }
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x0049:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x005a }
            goto L_0x0030
        L_0x004c:
            r5.SwitchLabel()     // Catch:{ Throwable -> 0x005a }
        L_0x004f:
            r0 = 1
            boolean r0 = r5.jj_2_29(r0)     // Catch:{ Throwable -> 0x005a }
            if (r0 == 0) goto L_0x0027
            r5.BlockStatement()     // Catch:{ Throwable -> 0x005a }
            goto L_0x004f
        L_0x005a:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x007e }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x007e }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0068 }
            if (r4 == 0) goto L_0x0074
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0068 }
            throw r0     // Catch:{ all -> 0x0068 }
        L_0x0068:
            r0 = move-exception
        L_0x0069:
            if (r1 == 0) goto L_0x0073
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0073:
            throw r0
        L_0x0074:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0068 }
            if (r4 == 0) goto L_0x007b
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0068 }
            throw r0     // Catch:{ all -> 0x0068 }
        L_0x007b:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0068 }
            throw r0     // Catch:{ all -> 0x0068 }
        L_0x007e:
            r0 = move-exception
            r1 = r2
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.SwitchStatement():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHBlock, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041 A[Catch:{ all -> 0x0044 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0050 A[SYNTHETIC, Splitter:B:19:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0056 A[Catch:{ all -> 0x0044 }] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0037 A[SYNTHETIC, Splitter:B:9:0x0037] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void SynchronizedStatement() {
        /*
            r6 = this;
            r2 = 0
            r3 = 1
            bsh.BSHBlock r4 = new bsh.BSHBlock
            r0 = 25
            r4.<init>(r0)
            bsh.JJTParserState r0 = r6.jjtree
            r0.openNodeScope(r4)
            r6.jjtreeOpenNodeScope(r4)
            r0 = 51
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0033, all -> 0x0060 }
            r0 = 72
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0033, all -> 0x0060 }
            r6.Expression()     // Catch:{ Throwable -> 0x0033, all -> 0x0060 }
            r0 = 73
            r6.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0033, all -> 0x0060 }
            r6.Block()     // Catch:{ Throwable -> 0x0033, all -> 0x0060 }
            bsh.JJTParserState r0 = r6.jjtree     // Catch:{ Throwable -> 0x0033, all -> 0x0060 }
            r1 = 1
            r0.closeNodeScope(r4, r1)     // Catch:{ Throwable -> 0x0033, all -> 0x0060 }
            r6.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            r0 = 1
            r4.isSynchronized = r0     // Catch:{ Throwable -> 0x0066, all -> 0x0063 }
            return
        L_0x0033:
            r0 = move-exception
            r1 = r3
        L_0x0035:
            if (r1 == 0) goto L_0x0050
            bsh.JJTParserState r5 = r6.jjtree     // Catch:{ all -> 0x0044 }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x0044 }
            r1 = r2
        L_0x003d:
            boolean r2 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0044 }
            if (r2 == 0) goto L_0x0056
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0044 }
            throw r0     // Catch:{ all -> 0x0044 }
        L_0x0044:
            r0 = move-exception
        L_0x0045:
            if (r1 == 0) goto L_0x004f
            bsh.JJTParserState r1 = r6.jjtree
            r1.closeNodeScope(r4, r3)
            r6.jjtreeCloseNodeScope(r4)
        L_0x004f:
            throw r0
        L_0x0050:
            bsh.JJTParserState r2 = r6.jjtree     // Catch:{ all -> 0x0044 }
            r2.popNode()     // Catch:{ all -> 0x0044 }
            goto L_0x003d
        L_0x0056:
            boolean r2 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0044 }
            if (r2 == 0) goto L_0x005d
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0044 }
            throw r0     // Catch:{ all -> 0x0044 }
        L_0x005d:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0044 }
            throw r0     // Catch:{ all -> 0x0044 }
        L_0x0060:
            r0 = move-exception
            r1 = r3
            goto L_0x0045
        L_0x0063:
            r0 = move-exception
            r1 = r2
            goto L_0x0045
        L_0x0066:
            r0 = move-exception
            r1 = r2
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.SynchronizedStatement():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHThrowStatement, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ThrowStatement() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHThrowStatement r3 = new bsh.BSHThrowStatement
            r0 = 36
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 53
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0026 }
            r5.Expression()     // Catch:{ Throwable -> 0x0026 }
            r0 = 78
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0026 }
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x0026:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x004a }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x004a }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0034 }
            if (r4 == 0) goto L_0x0040
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0034 }
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x0034:
            r0 = move-exception
        L_0x0035:
            if (r1 == 0) goto L_0x003f
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x003f:
            throw r0
        L_0x0040:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0034 }
            if (r4 == 0) goto L_0x0047
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0034 }
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x0047:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0034 }
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x004a:
            r0 = move-exception
            r1 = r2
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.ThrowStatement():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHTryStatement, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0055 A[SYNTHETIC, Splitter:B:21:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005f A[Catch:{ all -> 0x0062 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0095 A[SYNTHETIC, Splitter:B:37:0x0095] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x009b A[Catch:{ all -> 0x0062 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void TryStatement() {
        /*
            r8 = this;
            r7 = -1
            r3 = 0
            r2 = 1
            bsh.BSHTryStatement r4 = new bsh.BSHTryStatement
            r0 = 37
            r4.<init>(r0)
            bsh.JJTParserState r0 = r8.jjtree
            r0.openNodeScope(r4)
            r8.jjtreeOpenNodeScope(r4)
            r0 = 56
            r8.jj_consume_token(r0)     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r8.Block()     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r0 = r3
        L_0x001b:
            int r1 = r8.jj_ntk     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            if (r1 != r7) goto L_0x006e
            int r1 = r8.jj_ntk()     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
        L_0x0023:
            switch(r1) {
                case 16: goto L_0x0071;
                default: goto L_0x0026;
            }     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
        L_0x0026:
            int[] r1 = r8.jj_la1     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r5 = 86
            int r6 = r8.jj_gen     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r1[r5] = r6     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            int r1 = r8.jj_ntk     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            if (r1 != r7) goto L_0x0088
            int r1 = r8.jj_ntk()     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
        L_0x0036:
            switch(r1) {
                case 28: goto L_0x008b;
                default: goto L_0x0039;
            }     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
        L_0x0039:
            int[] r1 = r8.jj_la1     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r5 = 87
            int r6 = r8.jj_gen     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r1[r5] = r6     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
        L_0x0041:
            bsh.JJTParserState r1 = r8.jjtree     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r5 = 1
            r1.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r8.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0051, all -> 0x00a9 }
            if (r0 != 0) goto L_0x00a5
            bsh.ParseException r0 = r8.generateParseException()     // Catch:{ Throwable -> 0x0051, all -> 0x00a9 }
            throw r0     // Catch:{ Throwable -> 0x0051, all -> 0x00a9 }
        L_0x0051:
            r0 = move-exception
            r1 = r3
        L_0x0053:
            if (r1 == 0) goto L_0x0095
            bsh.JJTParserState r5 = r8.jjtree     // Catch:{ all -> 0x0062 }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x0062 }
            r1 = r3
        L_0x005b:
            boolean r3 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0062 }
            if (r3 == 0) goto L_0x009b
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0062 }
            throw r0     // Catch:{ all -> 0x0062 }
        L_0x0062:
            r0 = move-exception
        L_0x0063:
            if (r1 == 0) goto L_0x006d
            bsh.JJTParserState r1 = r8.jjtree
            r1.closeNodeScope(r4, r2)
            r8.jjtreeCloseNodeScope(r4)
        L_0x006d:
            throw r0
        L_0x006e:
            int r1 = r8.jj_ntk     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            goto L_0x0023
        L_0x0071:
            r0 = 16
            r8.jj_consume_token(r0)     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r0 = 72
            r8.jj_consume_token(r0)     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r8.FormalParameter()     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r0 = 73
            r8.jj_consume_token(r0)     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r8.Block()     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r0 = r2
            goto L_0x001b
        L_0x0088:
            int r1 = r8.jj_ntk     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            goto L_0x0036
        L_0x008b:
            r0 = 28
            r8.jj_consume_token(r0)     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r8.Block()     // Catch:{ Throwable -> 0x00ac, all -> 0x00a6 }
            r0 = r2
            goto L_0x0041
        L_0x0095:
            bsh.JJTParserState r3 = r8.jjtree     // Catch:{ all -> 0x0062 }
            r3.popNode()     // Catch:{ all -> 0x0062 }
            goto L_0x005b
        L_0x009b:
            boolean r3 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0062 }
            if (r3 == 0) goto L_0x00a2
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0062 }
            throw r0     // Catch:{ all -> 0x0062 }
        L_0x00a2:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0062 }
            throw r0     // Catch:{ all -> 0x0062 }
        L_0x00a5:
            return
        L_0x00a6:
            r0 = move-exception
            r1 = r2
            goto L_0x0063
        L_0x00a9:
            r0 = move-exception
            r1 = r3
            goto L_0x0063
        L_0x00ac:
            r0 = move-exception
            r1 = r2
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.TryStatement():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHType, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void Type() {
        /*
            r5 = this;
            r1 = -1
            r2 = 1
            bsh.BSHType r3 = new bsh.BSHType
            r0 = 9
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x002e }
            if (r0 != r1) goto L_0x0048
            int r0 = r5.jj_ntk()     // Catch:{ Throwable -> 0x002e }
        L_0x0019:
            switch(r0) {
                case 11: goto L_0x004b;
                case 14: goto L_0x004b;
                case 17: goto L_0x004b;
                case 22: goto L_0x004b;
                case 29: goto L_0x004b;
                case 36: goto L_0x004b;
                case 38: goto L_0x004b;
                case 47: goto L_0x004b;
                case 69: goto L_0x0066;
                default: goto L_0x001c;
            }     // Catch:{ Throwable -> 0x002e }
        L_0x001c:
            int[] r0 = r5.jj_la1     // Catch:{ Throwable -> 0x002e }
            r1 = 19
            int r4 = r5.jj_gen     // Catch:{ Throwable -> 0x002e }
            r0[r1] = r4     // Catch:{ Throwable -> 0x002e }
            r0 = -1
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002e }
            bsh.ParseException r0 = new bsh.ParseException     // Catch:{ Throwable -> 0x002e }
            r0.<init>()     // Catch:{ Throwable -> 0x002e }
            throw r0     // Catch:{ Throwable -> 0x002e }
        L_0x002e:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x0063 }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x0063 }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x003c }
            if (r4 == 0) goto L_0x0073
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x003c }
            throw r0     // Catch:{ all -> 0x003c }
        L_0x003c:
            r0 = move-exception
        L_0x003d:
            if (r1 == 0) goto L_0x0047
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0047:
            throw r0
        L_0x0048:
            int r0 = r5.jj_ntk     // Catch:{ Throwable -> 0x002e }
            goto L_0x0019
        L_0x004b:
            r5.PrimitiveType()     // Catch:{ Throwable -> 0x002e }
        L_0x004e:
            r0 = 2
            boolean r0 = r5.jj_2_6(r0)     // Catch:{ Throwable -> 0x002e }
            if (r0 == 0) goto L_0x006a
            r0 = 76
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002e }
            r0 = 77
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002e }
            r3.addArrayDimension()     // Catch:{ Throwable -> 0x002e }
            goto L_0x004e
        L_0x0063:
            r0 = move-exception
            r1 = r2
            goto L_0x003d
        L_0x0066:
            r5.AmbiguousName()     // Catch:{ Throwable -> 0x002e }
            goto L_0x004e
        L_0x006a:
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x0073:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x003c }
            if (r4 == 0) goto L_0x007a
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x003c }
            throw r0     // Catch:{ all -> 0x003c }
        L_0x007a:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x003c }
            throw r0     // Catch:{ all -> 0x003c }
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.Type():void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHTypedVariableDeclaration, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d A[SYNTHETIC, Splitter:B:17:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0057 A[Catch:{ all -> 0x005a }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0066 A[SYNTHETIC, Splitter:B:27:0x0066] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006c A[Catch:{ all -> 0x005a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void TypedVariableDeclaration() {
        /*
            r7 = this;
            r2 = 1
            r3 = 0
            bsh.BSHTypedVariableDeclaration r4 = new bsh.BSHTypedVariableDeclaration
            r0 = 33
            r4.<init>(r0)
            bsh.JJTParserState r0 = r7.jjtree
            r0.openNodeScope(r4)
            r7.jjtreeOpenNodeScope(r4)
            r0 = 2
            r1 = 0
            bsh.Modifiers r1 = r7.Modifiers(r0, r1)     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
            r7.Type()     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
            r7.VariableDeclarator()     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
        L_0x001d:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
            r5 = -1
            if (r0 != r5) goto L_0x003d
            int r0 = r7.jj_ntk()     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
        L_0x0026:
            switch(r0) {
                case 79: goto L_0x0040;
                default: goto L_0x0029;
            }     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
        L_0x0029:
            int[] r0 = r7.jj_la1     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
            r5 = 81
            int r6 = r7.jj_gen     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
            r0[r5] = r6     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
            bsh.JJTParserState r0 = r7.jjtree     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
            r5 = 1
            r0.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x007c, all -> 0x0079 }
            r4.modifiers = r1     // Catch:{ Throwable -> 0x007c, all -> 0x0079 }
            return
        L_0x003d:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
            goto L_0x0026
        L_0x0040:
            r0 = 79
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
            r7.VariableDeclarator()     // Catch:{ Throwable -> 0x0049, all -> 0x0076 }
            goto L_0x001d
        L_0x0049:
            r0 = move-exception
            r1 = r2
        L_0x004b:
            if (r1 == 0) goto L_0x0066
            bsh.JJTParserState r5 = r7.jjtree     // Catch:{ all -> 0x005a }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x005a }
            r1 = r3
        L_0x0053:
            boolean r3 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x005a }
            if (r3 == 0) goto L_0x006c
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x005a }
            throw r0     // Catch:{ all -> 0x005a }
        L_0x005a:
            r0 = move-exception
        L_0x005b:
            if (r1 == 0) goto L_0x0065
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r4, r2)
            r7.jjtreeCloseNodeScope(r4)
        L_0x0065:
            throw r0
        L_0x0066:
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x005a }
            r3.popNode()     // Catch:{ all -> 0x005a }
            goto L_0x0053
        L_0x006c:
            boolean r3 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x005a }
            if (r3 == 0) goto L_0x0073
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x005a }
            throw r0     // Catch:{ all -> 0x005a }
        L_0x0073:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x005a }
            throw r0     // Catch:{ all -> 0x005a }
        L_0x0076:
            r0 = move-exception
            r1 = r2
            goto L_0x005b
        L_0x0079:
            r0 = move-exception
            r1 = r3
            goto L_0x005b
        L_0x007c:
            r0 = move-exception
            r1 = r3
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.TypedVariableDeclaration():void");
    }

    public final void UnaryExpression() {
        Token jj_consume_token;
        boolean z;
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 11:
            case 14:
            case 17:
            case 22:
            case 26:
            case 29:
            case 36:
            case ParserConstants.LONG:
            case ParserConstants.NEW:
            case ParserConstants.NULL:
            case ParserConstants.SHORT:
            case ParserConstants.TRUE:
            case ParserConstants.VOID:
            case ParserConstants.INTEGER_LITERAL:
            case ParserConstants.FLOATING_POINT_LITERAL:
            case ParserConstants.CHARACTER_LITERAL:
            case ParserConstants.STRING_LITERAL:
            case ParserConstants.IDENTIFIER:
            case ParserConstants.LPAREN:
            case ParserConstants.BANG:
            case ParserConstants.TILDE:
                UnaryExpressionNotPlusMinus();
                return;
            case ParserConstants.INCR:
                PreIncrementExpression();
                return;
            case ParserConstants.DECR:
                PreDecrementExpression();
                return;
            case ParserConstants.PLUS:
            case ParserConstants.MINUS:
                switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                    case ParserConstants.PLUS:
                        jj_consume_token = jj_consume_token(ParserConstants.PLUS);
                        break;
                    case ParserConstants.MINUS:
                        jj_consume_token = jj_consume_token(ParserConstants.MINUS);
                        break;
                    default:
                        this.jj_la1[46] = this.jj_gen;
                        jj_consume_token(-1);
                        throw new ParseException();
                }
                UnaryExpression();
                BSHUnaryExpression bSHUnaryExpression = new BSHUnaryExpression(16);
                this.jjtree.openNodeScope(bSHUnaryExpression);
                jjtreeOpenNodeScope(bSHUnaryExpression);
                try {
                    this.jjtree.closeNodeScope(bSHUnaryExpression, 1);
                    z = false;
                    try {
                        jjtreeCloseNodeScope(bSHUnaryExpression);
                        bSHUnaryExpression.kind = jj_consume_token.kind;
                        return;
                    } catch (Throwable th) {
                        th = th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    z = true;
                    if (z) {
                        this.jjtree.closeNodeScope(bSHUnaryExpression, 1);
                        jjtreeCloseNodeScope(bSHUnaryExpression);
                    }
                    throw th;
                }
            default:
                this.jj_la1[47] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    public final void UnaryExpressionNotPlusMinus() {
        Token jj_consume_token;
        boolean z;
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case ParserConstants.BANG:
            case ParserConstants.TILDE:
                switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                    case ParserConstants.BANG:
                        jj_consume_token = jj_consume_token(86);
                        break;
                    case ParserConstants.TILDE:
                        jj_consume_token = jj_consume_token(87);
                        break;
                    default:
                        this.jj_la1[48] = this.jj_gen;
                        jj_consume_token(-1);
                        throw new ParseException();
                }
                UnaryExpression();
                BSHUnaryExpression bSHUnaryExpression = new BSHUnaryExpression(16);
                this.jjtree.openNodeScope(bSHUnaryExpression);
                jjtreeOpenNodeScope(bSHUnaryExpression);
                try {
                    this.jjtree.closeNodeScope(bSHUnaryExpression, 1);
                    z = false;
                    try {
                        jjtreeCloseNodeScope(bSHUnaryExpression);
                        bSHUnaryExpression.kind = jj_consume_token.kind;
                        return;
                    } catch (Throwable th) {
                        th = th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    z = true;
                    if (z) {
                        this.jjtree.closeNodeScope(bSHUnaryExpression, 1);
                        jjtreeCloseNodeScope(bSHUnaryExpression);
                    }
                    throw th;
                }
            default:
                this.jj_la1[49] = this.jj_gen;
                if (jj_2_9(Integer.MAX_VALUE)) {
                    CastExpression();
                    return;
                }
                switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                    case 11:
                    case 14:
                    case 17:
                    case 22:
                    case 26:
                    case 29:
                    case 36:
                    case ParserConstants.LONG:
                    case ParserConstants.NEW:
                    case ParserConstants.NULL:
                    case ParserConstants.SHORT:
                    case ParserConstants.TRUE:
                    case ParserConstants.VOID:
                    case ParserConstants.INTEGER_LITERAL:
                    case ParserConstants.FLOATING_POINT_LITERAL:
                    case ParserConstants.CHARACTER_LITERAL:
                    case ParserConstants.STRING_LITERAL:
                    case ParserConstants.IDENTIFIER:
                    case ParserConstants.LPAREN:
                        PostfixExpression();
                        return;
                    default:
                        this.jj_la1[50] = this.jj_gen;
                        jj_consume_token(-1);
                        throw new ParseException();
                }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHVariableDeclarator, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0048 A[SYNTHETIC, Splitter:B:17:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0052 A[Catch:{ all -> 0x0055 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0061 A[SYNTHETIC, Splitter:B:27:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0067 A[Catch:{ all -> 0x0055 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void VariableDeclarator() {
        /*
            r7 = this;
            r3 = 0
            r2 = 1
            bsh.BSHVariableDeclarator r4 = new bsh.BSHVariableDeclarator
            r0 = 5
            r4.<init>(r0)
            bsh.JJTParserState r0 = r7.jjtree
            r0.openNodeScope(r4)
            r7.jjtreeOpenNodeScope(r4)
            r0 = 69
            bsh.Token r1 = r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            r5 = -1
            if (r0 != r5) goto L_0x0038
            int r0 = r7.jj_ntk()     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
        L_0x001f:
            switch(r0) {
                case 81: goto L_0x003b;
                default: goto L_0x0022;
            }     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
        L_0x0022:
            int[] r0 = r7.jj_la1     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            r5 = 12
            int r6 = r7.jj_gen     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            r0[r5] = r6     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
        L_0x002a:
            bsh.JJTParserState r0 = r7.jjtree     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            r5 = 1
            r0.closeNodeScope(r4, r5)     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            r7.jjtreeCloseNodeScope(r4)     // Catch:{ Throwable -> 0x0077, all -> 0x0074 }
            java.lang.String r0 = r1.image     // Catch:{ Throwable -> 0x0077, all -> 0x0074 }
            r4.name = r0     // Catch:{ Throwable -> 0x0077, all -> 0x0074 }
            return
        L_0x0038:
            int r0 = r7.jj_ntk     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            goto L_0x001f
        L_0x003b:
            r0 = 81
            r7.jj_consume_token(r0)     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            r7.VariableInitializer()     // Catch:{ Throwable -> 0x0044, all -> 0x0071 }
            goto L_0x002a
        L_0x0044:
            r0 = move-exception
            r1 = r2
        L_0x0046:
            if (r1 == 0) goto L_0x0061
            bsh.JJTParserState r5 = r7.jjtree     // Catch:{ all -> 0x0055 }
            r5.clearNodeScope(r4)     // Catch:{ all -> 0x0055 }
            r1 = r3
        L_0x004e:
            boolean r3 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0055 }
            if (r3 == 0) goto L_0x0067
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x0055 }
            throw r0     // Catch:{ all -> 0x0055 }
        L_0x0055:
            r0 = move-exception
        L_0x0056:
            if (r1 == 0) goto L_0x0060
            bsh.JJTParserState r1 = r7.jjtree
            r1.closeNodeScope(r4, r2)
            r7.jjtreeCloseNodeScope(r4)
        L_0x0060:
            throw r0
        L_0x0061:
            bsh.JJTParserState r3 = r7.jjtree     // Catch:{ all -> 0x0055 }
            r3.popNode()     // Catch:{ all -> 0x0055 }
            goto L_0x004e
        L_0x0067:
            boolean r3 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x0055 }
            if (r3 == 0) goto L_0x006e
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x0055 }
            throw r0     // Catch:{ all -> 0x0055 }
        L_0x006e:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x0055 }
            throw r0     // Catch:{ all -> 0x0055 }
        L_0x0071:
            r0 = move-exception
            r1 = r2
            goto L_0x0056
        L_0x0074:
            r0 = move-exception
            r1 = r3
            goto L_0x0056
        L_0x0077:
            r0 = move-exception
            r1 = r3
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.VariableDeclarator():void");
    }

    public final void VariableInitializer() {
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 11:
            case 14:
            case 17:
            case 22:
            case 26:
            case 29:
            case 36:
            case ParserConstants.LONG:
            case ParserConstants.NEW:
            case ParserConstants.NULL:
            case ParserConstants.SHORT:
            case ParserConstants.TRUE:
            case ParserConstants.VOID:
            case ParserConstants.INTEGER_LITERAL:
            case ParserConstants.FLOATING_POINT_LITERAL:
            case ParserConstants.CHARACTER_LITERAL:
            case ParserConstants.STRING_LITERAL:
            case ParserConstants.IDENTIFIER:
            case ParserConstants.LPAREN:
            case ParserConstants.BANG:
            case ParserConstants.TILDE:
            case ParserConstants.INCR:
            case ParserConstants.DECR:
            case ParserConstants.PLUS:
            case ParserConstants.MINUS:
                Expression();
                return;
            case ParserConstants.LBRACE:
                ArrayInitializer();
                return;
            default:
                this.jj_la1[13] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    public final void VoidLiteral() {
        jj_consume_token(57);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void
     arg types: [bsh.BSHWhileStatement, int]
     candidates:
      bsh.JJTParserState.closeNodeScope(bsh.Node, int):void
      bsh.JJTParserState.closeNodeScope(bsh.Node, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void WhileStatement() {
        /*
            r5 = this;
            r2 = 1
            bsh.BSHWhileStatement r3 = new bsh.BSHWhileStatement
            r0 = 30
            r3.<init>(r0)
            bsh.JJTParserState r0 = r5.jjtree
            r0.openNodeScope(r3)
            r5.jjtreeOpenNodeScope(r3)
            r0 = 59
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002e }
            r0 = 72
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002e }
            r5.Expression()     // Catch:{ Throwable -> 0x002e }
            r0 = 73
            r5.jj_consume_token(r0)     // Catch:{ Throwable -> 0x002e }
            r5.Statement()     // Catch:{ Throwable -> 0x002e }
            bsh.JJTParserState r0 = r5.jjtree
            r0.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
            return
        L_0x002e:
            r0 = move-exception
            bsh.JJTParserState r1 = r5.jjtree     // Catch:{ all -> 0x0052 }
            r1.clearNodeScope(r3)     // Catch:{ all -> 0x0052 }
            r1 = 0
            boolean r4 = r0 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x003c }
            if (r4 == 0) goto L_0x0048
            java.lang.RuntimeException r0 = (java.lang.RuntimeException) r0     // Catch:{ all -> 0x003c }
            throw r0     // Catch:{ all -> 0x003c }
        L_0x003c:
            r0 = move-exception
        L_0x003d:
            if (r1 == 0) goto L_0x0047
            bsh.JJTParserState r1 = r5.jjtree
            r1.closeNodeScope(r3, r2)
            r5.jjtreeCloseNodeScope(r3)
        L_0x0047:
            throw r0
        L_0x0048:
            boolean r4 = r0 instanceof bsh.ParseException     // Catch:{ all -> 0x003c }
            if (r4 == 0) goto L_0x004f
            bsh.ParseException r0 = (bsh.ParseException) r0     // Catch:{ all -> 0x003c }
            throw r0     // Catch:{ all -> 0x003c }
        L_0x004f:
            java.lang.Error r0 = (java.lang.Error) r0     // Catch:{ all -> 0x003c }
            throw r0     // Catch:{ all -> 0x003c }
        L_0x0052:
            r0 = move-exception
            r1 = r2
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.WhileStatement():void");
    }

    /* access modifiers changed from: package-private */
    public ParseException createParseException(String str) {
        Token token2 = this.token;
        int i = token2.beginLine;
        return new ParseException("Parse error at line " + i + ", column " + token2.beginColumn + " : " + str);
    }

    public final void disable_tracing() {
    }

    public final void enable_tracing() {
    }

    public ParseException generateParseException() {
        int i = 0;
        this.jj_expentries.removeAllElements();
        boolean[] zArr = new boolean[134];
        for (int i2 = 0; i2 < 134; i2++) {
            zArr[i2] = false;
        }
        if (this.jj_kind >= 0) {
            zArr[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i3 = 0; i3 < 88; i3++) {
            if (this.jj_la1[i3] == this.jj_gen) {
                for (int i4 = 0; i4 < 32; i4++) {
                    if ((jj_la1_0[i3] & (1 << i4)) != 0) {
                        zArr[i4] = true;
                    }
                    if ((jj_la1_1[i3] & (1 << i4)) != 0) {
                        zArr[i4 + 32] = true;
                    }
                    if ((jj_la1_2[i3] & (1 << i4)) != 0) {
                        zArr[i4 + 64] = true;
                    }
                    if ((jj_la1_3[i3] & (1 << i4)) != 0) {
                        zArr[i4 + 96] = true;
                    }
                    if ((jj_la1_4[i3] & (1 << i4)) != 0) {
                        zArr[i4 + ParserConstants.LSHIFTASSIGN] = true;
                    }
                }
            }
        }
        for (int i5 = 0; i5 < 134; i5++) {
            if (zArr[i5]) {
                this.jj_expentry = new int[1];
                this.jj_expentry[0] = i5;
                this.jj_expentries.addElement(this.jj_expentry);
            }
        }
        this.jj_endpos = 0;
        jj_rescan_token();
        jj_add_error_token(0, 0);
        int[][] iArr = new int[this.jj_expentries.size()][];
        while (true) {
            int i6 = i;
            if (i6 >= this.jj_expentries.size()) {
                return new ParseException(this.token, iArr, tokenImage);
            }
            iArr[i6] = (int[]) this.jj_expentries.elementAt(i6);
            i = i6 + 1;
        }
    }

    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token2.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    public final Token getToken(int i) {
        Token nextToken;
        int i2 = 0;
        Token token2 = this.lookingAhead ? this.jj_scanpos : this.token;
        while (i2 < i) {
            if (token2.next != null) {
                nextToken = token2.next;
            } else {
                nextToken = this.token_source.getNextToken();
                token2.next = nextToken;
            }
            i2++;
            token2 = nextToken;
        }
        return token2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0029 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027 A[LOOP:0: B:6:0x001c->B:8:0x0027, LOOP_END] */
    boolean isRegularForStatement() {
        /*
            r5 = this;
            r3 = 1
            r2 = 0
            bsh.Token r0 = r5.getToken(r3)
            int r0 = r0.kind
            r1 = 30
            if (r0 == r1) goto L_0x000e
            r0 = r2
        L_0x000d:
            return r0
        L_0x000e:
            r1 = 2
            r0 = 3
            bsh.Token r1 = r5.getToken(r1)
            int r1 = r1.kind
            r4 = 72
            if (r1 == r4) goto L_0x001c
            r0 = r2
            goto L_0x000d
        L_0x001c:
            int r1 = r0 + 1
            bsh.Token r0 = r5.getToken(r0)
            int r0 = r0.kind
            switch(r0) {
                case 0: goto L_0x002d;
                case 78: goto L_0x002b;
                case 89: goto L_0x0029;
                default: goto L_0x0027;
            }
        L_0x0027:
            r0 = r1
            goto L_0x001c
        L_0x0029:
            r0 = r2
            goto L_0x000d
        L_0x002b:
            r0 = r3
            goto L_0x000d
        L_0x002d:
            r0 = r2
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: bsh.Parser.isRegularForStatement():boolean");
    }

    /* access modifiers changed from: package-private */
    public void jjtreeCloseNodeScope(Node node) {
        ((SimpleNode) node).lastToken = getToken(0);
    }

    /* access modifiers changed from: package-private */
    public void jjtreeOpenNodeScope(Node node) {
        ((SimpleNode) node).firstToken = getToken(1);
    }

    public SimpleNode popNode() {
        if (this.jjtree.nodeArity() > 0) {
            return (SimpleNode) this.jjtree.popNode();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void reInitInput(Reader reader) {
        ReInit(reader);
    }

    /* access modifiers changed from: package-private */
    public void reInitTokenInput(Reader reader) {
        this.jj_input_stream.ReInit(reader, this.jj_input_stream.getEndLine(), this.jj_input_stream.getEndColumn());
    }

    public void setRetainComments(boolean z) {
        this.retainComments = z;
    }
}
