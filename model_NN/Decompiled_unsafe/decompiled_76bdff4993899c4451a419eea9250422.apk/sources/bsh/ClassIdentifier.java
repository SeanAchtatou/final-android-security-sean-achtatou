package bsh;

public class ClassIdentifier {
    Class clas;

    public ClassIdentifier(Class cls) {
        this.clas = cls;
    }

    public Class getTargetClass() {
        return this.clas;
    }

    public String toString() {
        return "Class Identifier: " + this.clas.getName();
    }
}
