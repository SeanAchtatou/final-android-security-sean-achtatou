package bsh;

import bsh.Capabilities;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.Hashtable;

public class BshClassManager {
    private static Object NOVALUE = new Object();
    protected transient Hashtable absoluteClassCache = new Hashtable();
    protected transient Hashtable absoluteNonClasses = new Hashtable();
    protected transient Hashtable associatedClasses = new Hashtable();
    private Interpreter declaringInterpreter;
    protected transient Hashtable definingClasses = new Hashtable();
    protected transient Hashtable definingClassesBaseNames = new Hashtable();
    protected ClassLoader externalClassLoader;
    protected transient Hashtable resolvedObjectMethods = new Hashtable();
    protected transient Hashtable resolvedStaticMethods = new Hashtable();

    public interface Listener {
        void classLoaderChanged();
    }

    class SignatureKey {
        Class clas;
        int hashCode = 0;
        String methodName;
        Class[] types;

        SignatureKey(Class cls, String str, Class[] clsArr) {
            this.clas = cls;
            this.methodName = str;
            this.types = clsArr;
        }

        public boolean equals(Object obj) {
            SignatureKey signatureKey = (SignatureKey) obj;
            if (this.types == null) {
                return signatureKey.types == null;
            }
            if (this.clas != signatureKey.clas || !this.methodName.equals(signatureKey.methodName) || this.types.length != signatureKey.types.length) {
                return false;
            }
            for (int i = 0; i < this.types.length; i++) {
                if (this.types[i] == null) {
                    if (signatureKey.types[i] != null) {
                        return false;
                    }
                } else if (!this.types[i].equals(signatureKey.types[i])) {
                    return false;
                }
            }
            return true;
        }

        public int hashCode() {
            if (this.hashCode == 0) {
                this.hashCode = this.clas.hashCode() * this.methodName.hashCode();
                if (this.types == null) {
                    return this.hashCode;
                }
                for (int i = 0; i < this.types.length; i++) {
                    this.hashCode = (this.types[i] == null ? 21 : this.types[i].hashCode()) + (this.hashCode * (i + 1));
                }
            }
            return this.hashCode;
        }
    }

    protected static UtilEvalError cmUnavailable() {
        return new Capabilities.Unavailable("ClassLoading features unavailable.");
    }

    public static BshClassManager createClassManager(Interpreter interpreter) {
        BshClassManager bshClassManager;
        if (!Capabilities.classExists("java.lang.ref.WeakReference") || !Capabilities.classExists("java.util.HashMap") || !Capabilities.classExists("bsh.classpath.ClassManagerImpl")) {
            bshClassManager = new BshClassManager();
        } else {
            try {
                bshClassManager = (BshClassManager) Class.forName("bsh.classpath.ClassManagerImpl").newInstance();
            } catch (Exception e) {
                throw new InterpreterError("Error loading classmanager: " + e);
            }
        }
        if (interpreter == null) {
            interpreter = new Interpreter();
        }
        bshClassManager.declaringInterpreter = interpreter;
        return bshClassManager;
    }

    protected static Error noClassDefFound(String str, Error error) {
        return new NoClassDefFoundError("A class required by class: " + str + " could not be loaded:\n" + error.toString());
    }

    public void addClassPath(URL url) {
    }

    public void addListener(Listener listener) {
    }

    public void associateClass(Class cls) {
        this.associatedClasses.put(cls.getName(), cls);
    }

    public void cacheClassInfo(String str, Class cls) {
        if (cls != null) {
            this.absoluteClassCache.put(str, cls);
        } else {
            this.absoluteNonClasses.put(str, NOVALUE);
        }
    }

    public void cacheResolvedMethod(Class cls, Class[] clsArr, Method method) {
        if (Interpreter.DEBUG) {
            Interpreter.debug("cacheResolvedMethod putting: " + cls + " " + method);
        }
        SignatureKey signatureKey = new SignatureKey(cls, method.getName(), clsArr);
        if (Modifier.isStatic(method.getModifiers())) {
            this.resolvedStaticMethods.put(signatureKey, method);
        } else {
            this.resolvedObjectMethods.put(signatureKey, method);
        }
    }

    public boolean classExists(String str) {
        return classForName(str) != null;
    }

    public Class classForName(String str) {
        if (isClassBeingDefined(str)) {
            throw new InterpreterError("Attempting to load class in the process of being defined: " + str);
        }
        Class cls = null;
        try {
            cls = plainClassForName(str);
        } catch (ClassNotFoundException e) {
        }
        return cls == null ? loadSourceClass(str) : cls;
    }

    /* access modifiers changed from: protected */
    public void classLoaderChanged() {
    }

    /* access modifiers changed from: protected */
    public void clearCaches() {
        this.absoluteNonClasses = new Hashtable();
        this.absoluteClassCache = new Hashtable();
        this.resolvedObjectMethods = new Hashtable();
        this.resolvedStaticMethods = new Hashtable();
    }

    public Class defineClass(String str, byte[] bArr) {
        throw new InterpreterError("Can't create class (" + str + ") without class manager package.");
    }

    /* access modifiers changed from: protected */
    public void definingClass(String str) {
        String suffix = Name.suffix(str, 1);
        int indexOf = suffix.indexOf("$");
        String substring = indexOf != -1 ? suffix.substring(indexOf + 1) : suffix;
        String str2 = (String) this.definingClassesBaseNames.get(substring);
        if (str2 != null) {
            throw new InterpreterError("Defining class problem: " + str + ": BeanShell cannot yet simultaneously define two or more " + "dependant classes of the same name.  Attempt to define: " + str + " while defining: " + str2);
        }
        this.definingClasses.put(str, NOVALUE);
        this.definingClassesBaseNames.put(substring, str);
    }

    /* access modifiers changed from: protected */
    public void doSuperImport() {
        throw cmUnavailable();
    }

    /* access modifiers changed from: protected */
    public void doneDefiningClass(String str) {
        String suffix = Name.suffix(str, 1);
        this.definingClasses.remove(str);
        this.definingClassesBaseNames.remove(suffix);
    }

    public void dump(PrintWriter printWriter) {
        printWriter.println("BshClassManager: no class manager.");
    }

    public Class getAssociatedClass(String str) {
        return (Class) this.associatedClasses.get(str);
    }

    /* access modifiers changed from: protected */
    public String getClassBeingDefined(String str) {
        return (String) this.definingClassesBaseNames.get(Name.suffix(str, 1));
    }

    /* access modifiers changed from: protected */
    public String getClassNameByUnqName(String str) {
        throw cmUnavailable();
    }

    /* access modifiers changed from: protected */
    public Method getResolvedMethod(Class cls, String str, Class[] clsArr, boolean z) {
        SignatureKey signatureKey = new SignatureKey(cls, str, clsArr);
        Method method = (Method) this.resolvedStaticMethods.get(signatureKey);
        if (method == null && !z) {
            method = (Method) this.resolvedObjectMethods.get(signatureKey);
        }
        if (Interpreter.DEBUG) {
            if (method == null) {
                Interpreter.debug("getResolvedMethod cache MISS: " + cls + " - " + str);
            } else {
                Interpreter.debug("getResolvedMethod cache HIT: " + cls + " - " + method);
            }
        }
        return method;
    }

    public URL getResource(String str) {
        URL url = null;
        if (this.externalClassLoader != null) {
            url = this.externalClassLoader.getResource(str.substring(1));
        }
        return url == null ? Interpreter.class.getResource(str) : url;
    }

    public InputStream getResourceAsStream(String str) {
        InputStream inputStream = null;
        if (this.externalClassLoader != null) {
            inputStream = this.externalClassLoader.getResourceAsStream(str.substring(1));
        }
        return inputStream == null ? Interpreter.class.getResourceAsStream(str) : inputStream;
    }

    /* access modifiers changed from: protected */
    public boolean hasSuperImport() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean isClassBeingDefined(String str) {
        return this.definingClasses.get(str) != null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* access modifiers changed from: protected */
    public Class loadSourceClass(String str) {
        String str2 = "/" + str.replace('.', '/') + ".java";
        InputStream resourceAsStream = getResourceAsStream(str2);
        if (resourceAsStream == null) {
            return null;
        }
        try {
            System.out.println("Loading class from source file: " + str2);
            this.declaringInterpreter.eval(new InputStreamReader(resourceAsStream));
        } catch (EvalError e) {
            System.err.println(e);
        }
        try {
            return plainClassForName(str);
        } catch (ClassNotFoundException e2) {
            System.err.println("Class not found in source file: " + str);
            return null;
        }
    }

    public Class plainClassForName(String str) {
        try {
            Class<?> loadClass = this.externalClassLoader != null ? this.externalClassLoader.loadClass(str) : Class.forName(str);
            cacheClassInfo(str, loadClass);
            return loadClass;
        } catch (NoClassDefFoundError e) {
            throw noClassDefFound(str, e);
        }
    }

    public void reloadAllClasses() {
        throw cmUnavailable();
    }

    public void reloadClasses(String[] strArr) {
        throw cmUnavailable();
    }

    public void reloadPackage(String str) {
        throw cmUnavailable();
    }

    public void removeListener(Listener listener) {
    }

    public void reset() {
        clearCaches();
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.externalClassLoader = classLoader;
        classLoaderChanged();
    }

    public void setClassPath(URL[] urlArr) {
        throw cmUnavailable();
    }
}
