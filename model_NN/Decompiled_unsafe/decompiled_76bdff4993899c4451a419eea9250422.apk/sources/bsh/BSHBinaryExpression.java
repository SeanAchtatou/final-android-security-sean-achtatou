package bsh;

class BSHBinaryExpression extends SimpleNode implements ParserConstants {
    public int kind;

    BSHBinaryExpression(int i) {
        super(i);
    }

    private boolean isPrimitiveValue(Object obj) {
        return (!(obj instanceof Primitive) || obj == Primitive.VOID || obj == Primitive.NULL) ? false : true;
    }

    private boolean isWrapper(Object obj) {
        return (obj instanceof Boolean) || (obj instanceof Character) || (obj instanceof Number);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public Object eval(CallStack callStack, Interpreter interpreter) {
        Object eval = ((SimpleNode) jjtGetChild(0)).eval(callStack, interpreter);
        if (this.kind != 35) {
            if (this.kind == 98 || this.kind == 99) {
                Object value = isPrimitiveValue(eval) ? ((Primitive) eval).getValue() : eval;
                if ((value instanceof Boolean) && !((Boolean) value).booleanValue()) {
                    return Primitive.FALSE;
                }
            }
            if (this.kind == 96 || this.kind == 97) {
                Object value2 = isPrimitiveValue(eval) ? ((Primitive) eval).getValue() : eval;
                if ((value2 instanceof Boolean) && ((Boolean) value2).booleanValue()) {
                    return Primitive.TRUE;
                }
            }
            boolean isWrapper = isWrapper(eval);
            Object eval2 = ((SimpleNode) jjtGetChild(1)).eval(callStack, interpreter);
            boolean isWrapper2 = isWrapper(eval2);
            if ((isWrapper || isPrimitiveValue(eval)) && ((isWrapper2 || isPrimitiveValue(eval2)) && (!isWrapper || !isWrapper2 || this.kind != 90))) {
                try {
                    return Primitive.binaryOperation(eval, eval2, this.kind);
                } catch (UtilEvalError e) {
                    throw e.toEvalError(this, callStack);
                }
            } else {
                switch (this.kind) {
                    case ParserConstants.EQ /*90*/:
                        return eval == eval2 ? Primitive.TRUE : Primitive.FALSE;
                    case ParserConstants.NE /*95*/:
                        return eval != eval2 ? Primitive.TRUE : Primitive.FALSE;
                    case ParserConstants.PLUS /*102*/:
                        if ((eval instanceof String) || (eval2 instanceof String)) {
                            return eval.toString() + eval2.toString();
                        }
                }
                if ((eval instanceof Primitive) || (eval2 instanceof Primitive)) {
                    if (eval == Primitive.VOID || eval2 == Primitive.VOID) {
                        throw new EvalError("illegal use of undefined variable, class, or 'void' literal", this, callStack);
                    } else if (eval == Primitive.NULL || eval2 == Primitive.NULL) {
                        throw new EvalError("illegal use of null value or 'null' literal", this, callStack);
                    }
                }
                throw new EvalError("Operator: '" + tokenImage[this.kind] + "' inappropriate for objects", this, callStack);
            }
        } else if (eval == Primitive.NULL) {
            return Primitive.FALSE;
        } else {
            Class<Primitive> type = ((BSHType) jjtGetChild(1)).getType(callStack, interpreter);
            return eval instanceof Primitive ? type == Primitive.class ? Primitive.TRUE : Primitive.FALSE : new Primitive(Types.isJavaBaseAssignable(type, eval.getClass()));
        }
    }
}
