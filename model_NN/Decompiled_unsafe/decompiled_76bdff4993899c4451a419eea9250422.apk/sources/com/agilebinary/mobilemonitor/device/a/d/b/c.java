package com.agilebinary.mobilemonitor.device.a.d.b;

import com.agilebinary.mobilemonitor.device.a.a.a;
import com.agilebinary.mobilemonitor.device.a.b.n;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.c.h;
import com.agilebinary.mobilemonitor.device.a.g.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.j.a.b;

public final class c implements a, d {
    private static String a = f.a();
    private b b;
    private d c;
    private com.agilebinary.mobilemonitor.device.a.g.a.d d = new h(this);
    private com.agilebinary.mobilemonitor.device.a.a.c e;
    private g f;

    public c(n nVar, d dVar, h hVar, com.agilebinary.mobilemonitor.device.a.a.c cVar, b bVar) {
        this.f = hVar;
        this.b = new b(this, nVar, cVar, dVar, hVar, bVar);
        this.c = dVar;
        this.e = cVar;
    }

    private int h() {
        return this.e.o() * 60000;
    }

    public final void a() {
    }

    public final void a(String str, String str2) {
        this.b.a(str, str2);
    }

    public final boolean a(String str) {
        return this.b.a(str);
    }

    public final void b() {
        long h = (long) h();
        this.f.a("CmdP", this.d, null, true, h, h, true);
        this.e.c(this);
    }

    public final void c() {
        this.e.d(this);
        this.f.a("CmdP", this.d);
    }

    public final void d() {
    }

    public final synchronized void e() {
        this.f.a("CmdP", new h(this), null, 0, false);
    }

    public final void f() {
        this.c.a(true, false, false);
        if (this.c.k()) {
            String l = this.e.l();
            if (l == null) {
                l = "";
            }
            com.agilebinary.mobilemonitor.device.a.a.h k_ = this.c.k_();
            if (k_ != null) {
                this.b.a(k_);
            }
            String l2 = this.e.l();
            if (l2 == null) {
                l2 = "";
            }
            if (!l.equals(l2)) {
                f();
                return;
            }
            this.c.l_();
            this.c.m_();
        }
    }

    public final void g() {
        this.d = new h(this);
        this.f.a("CmdP", this.d, null, 0, false);
    }

    public final synchronized void g_() {
        this.f.a("CmdP", this.d);
        long h = (long) h();
        this.d = new h(this);
        this.f.a("CmdP", this.d, null, true, h, h, true);
    }
}
