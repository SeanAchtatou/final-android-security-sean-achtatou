package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.b.a.a.h;
import com.agilebinary.b.a.a.q;
import com.agilebinary.b.a.a.u;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.g.f;

public abstract class b implements d {
    private static final String a = f.a();
    private c b;
    private u c;
    private e d = new e(this);

    public b(c cVar, g gVar) {
        this.b = cVar;
        this.c = new q();
    }

    private synchronized void g() {
        u f = f();
        h.a(f, this.d);
        this.c = f;
    }

    public final void a() {
    }

    public final void b() {
        g();
    }

    public final void c() {
    }

    public final void d() {
    }

    public final synchronized f e() {
        f fVar;
        u f = f();
        h.a(f, this.d);
        if (this.c.b() == 0 || f.b() == 0) {
            fVar = new f(0, a + ": no current and reference cells");
        } else {
            l lVar = (l) f.a(0);
            int c2 = this.c.c(lVar);
            if (c2 == -1) {
                fVar = new f(1, a + ": strongest cell not found in ref list ");
            } else {
                int abs = Math.abs(((l) this.c.a(c2)).a - lVar.a);
                fVar = abs >= this.b.s() ? new f(1, a + ": strongest cell asu change= " + abs) : new f(2, a + ": strongest cell asu change= " + abs);
            }
        }
        if (fVar.a != 2) {
            g();
        }
        "" + fVar;
        return fVar;
    }

    public abstract u f();
}
