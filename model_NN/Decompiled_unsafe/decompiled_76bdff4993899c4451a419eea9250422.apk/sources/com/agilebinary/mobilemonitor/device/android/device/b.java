package com.agilebinary.mobilemonitor.device.android.device;

import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import com.agilebinary.b.a.a.q;
import com.agilebinary.b.a.a.u;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.c.a.g;
import java.util.List;

public final class b extends g {
    private h b;
    private /* synthetic */ j c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(j jVar, c cVar, j jVar2) {
        super(cVar, jVar2);
        this.c = jVar;
    }

    public final void b() {
        this.b = new h(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
        this.c.d.registerReceiver(this.b, intentFilter);
        super.b();
    }

    public final void c() {
        super.c();
        this.c.d.unregisterReceiver(this.b);
    }

    public final u f() {
        if (this.c.e.startScan()) {
            synchronized (this.b) {
                try {
                    this.b.wait(10000);
                } catch (InterruptedException e) {
                }
            }
        }
        List<ScanResult> scanResults = this.c.e.getScanResults();
        q qVar = new q();
        if (scanResults != null) {
            for (ScanResult next : scanResults) {
                qVar.b(new com.agilebinary.mobilemonitor.device.a.c.a.c(next.BSSID, next.level));
            }
        }
        return qVar;
    }
}
