package com.agilebinary.mobilemonitor.device.a.a;

import com.agilebinary.mobilemonitor.device.a.e.a;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class f {
    public static final String a = com.agilebinary.mobilemonitor.device.a.g.f.a();
    protected h b = new h();
    protected i c;
    private boolean d = false;

    protected f(i iVar) {
        this.c = iVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, int, boolean):int
     arg types: [java.lang.String, int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, long, boolean):long
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, java.lang.String, boolean):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, int, boolean):int */
    /* access modifiers changed from: protected */
    public final int a(String str, int i) {
        return a(str, i, false);
    }

    /* access modifiers changed from: protected */
    public final int a(String str, int i, boolean z) {
        if (z && !this.b.containsKey(str)) {
            return i;
        }
        try {
            return e(str);
        } catch (RuntimeException e) {
            "" + i;
            e.getMessage();
            return i;
        }
    }

    /* access modifiers changed from: protected */
    public final long a(String str, long j, boolean z) {
        if (!this.b.containsKey(str)) {
            return -1;
        }
        try {
            return Long.parseLong(this.b.a(str));
        } catch (RuntimeException e) {
            "" + -1L;
            e.getMessage();
            return -1;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, int, boolean):int
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, long, boolean):long
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: protected */
    public final String a(String str, String str2) {
        return a(str, str2, false);
    }

    /* access modifiers changed from: protected */
    public final String a(String str, String str2, boolean z) {
        String f;
        return ((!z || this.b.containsKey(str)) && (f = f(str)) != null) ? f : str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.h.a(com.agilebinary.mobilemonitor.device.a.a.h, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.a.h, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.h.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.a.h.a(java.io.OutputStream, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.g.e.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.a.h.a(com.agilebinary.mobilemonitor.device.a.a.h, boolean):void */
    public final void a(h hVar, boolean z) {
        this.b.a(hVar, true);
    }

    /* access modifiers changed from: protected */
    public final void a(String str, long j) {
        this.b.put(str, String.valueOf(j));
    }

    /* access modifiers changed from: protected */
    public final void ag() {
        try {
            this.b.clear();
            InputStream resourceAsStream = getClass().getResourceAsStream(b());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[512];
            while (true) {
                int read = resourceAsStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    this.b.a(new ByteArrayInputStream(this.c.a(byteArrayOutputStream.toByteArray())));
                    return;
                }
            }
        } catch (IOException e) {
            a.a(e);
            System.exit(0);
        }
    }

    /* access modifiers changed from: package-private */
    public abstract String b();

    /* access modifiers changed from: protected */
    public final void b(String str, String str2) {
        if (str2 == null) {
            this.b.remove(str);
        } else {
            this.b.put(str, str2.trim());
        }
    }

    /* access modifiers changed from: protected */
    public final boolean c(String str, boolean z) {
        try {
            return "true".equalsIgnoreCase(this.b.a(str));
        } catch (RuntimeException e) {
            "" + z;
            e.getMessage();
            return z;
        }
    }

    /* access modifiers changed from: protected */
    public final void d(String str, boolean z) {
        this.b.put(str, String.valueOf(z));
    }

    /* access modifiers changed from: protected */
    public final int e(String str) {
        return Integer.parseInt(this.b.a(str));
    }

    /* access modifiers changed from: protected */
    public final String f(String str) {
        String a2 = this.b.a(str);
        return a2 != null ? a2.trim() : a2;
    }
}
