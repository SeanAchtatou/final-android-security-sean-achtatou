package com.agilebinary.mobilemonitor.device.a.h;

import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.Enumeration;
import java.util.Vector;

public final class b implements c {
    private static final String a = f.a();
    private int b = 0;
    private int c = 0;
    private long d = System.currentTimeMillis();
    private long e = System.currentTimeMillis();
    private boolean f;
    private long g;
    private g h;
    private com.agilebinary.mobilemonitor.device.a.a.b i;
    private e j;
    private Vector k;

    public b(e eVar, g gVar, com.agilebinary.mobilemonitor.device.a.a.b bVar) {
        this.i = bVar;
        this.h = gVar;
        this.j = eVar;
        this.k = new Vector();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        a("ONE_TEST_PASSED");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x016c, code lost:
        r0 = 2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized int a(long r10, int r12, boolean r13, boolean r14) {
        /*
            r9 = this;
            monitor-enter(r9)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
            r0.<init>()     // Catch:{ all -> 0x017e }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = r0.append(r10)     // Catch:{ all -> 0x017e }
            r0.toString()     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
            r0.<init>()     // Catch:{ all -> 0x017e }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = r0.append(r12)     // Catch:{ all -> 0x017e }
            r0.toString()     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
            r0.<init>()     // Catch:{ all -> 0x017e }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = r0.append(r13)     // Catch:{ all -> 0x017e }
            r0.toString()     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
            r0.<init>()     // Catch:{ all -> 0x017e }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = r0.append(r14)     // Catch:{ all -> 0x017e }
            r0.toString()     // Catch:{ all -> 0x017e }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x017e }
            long r4 = r0 - r10
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
            r0.<init>()     // Catch:{ all -> 0x017e }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x017e }
            r0.toString()     // Catch:{ all -> 0x017e }
            com.agilebinary.mobilemonitor.device.a.a.b r0 = r9.i     // Catch:{ all -> 0x017e }
            int r0 = r0.d(r12)     // Catch:{ all -> 0x017e }
            int r0 = r0 * 1000
            long r0 = (long) r0     // Catch:{ all -> 0x017e }
            long r0 = r0 - r4
            int r0 = (int) r0     // Catch:{ all -> 0x017e }
            com.agilebinary.mobilemonitor.device.a.a.b r1 = r9.i     // Catch:{ all -> 0x017e }
            int r1 = r1.b(r12)     // Catch:{ all -> 0x017e }
            com.agilebinary.mobilemonitor.device.a.a.b r2 = r9.i     // Catch:{ all -> 0x017e }
            int r2 = r2.c(r12)     // Catch:{ all -> 0x017e }
            int r2 = r2 * 1000
            com.agilebinary.mobilemonitor.device.a.a.b r3 = r9.i     // Catch:{ all -> 0x017e }
            int r3 = r3.a(r12)     // Catch:{ all -> 0x017e }
            int r3 = r3 * 1000
            int r2 = r2 + r3
            int r1 = r1 * r2
            int r0 = r0 + r1
            if (r0 >= 0) goto L_0x0146
            r0 = 0
        L_0x0089:
            r9.g = r0     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
            r0.<init>()     // Catch:{ all -> 0x017e }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x017e }
            long r1 = r9.g     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x017e }
            r0.toString()     // Catch:{ all -> 0x017e }
            if (r13 == 0) goto L_0x0149
            r0 = 0
            r3 = r0
        L_0x00a3:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
            r0.<init>()     // Catch:{ all -> 0x017e }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x017e }
            r0.toString()     // Catch:{ all -> 0x017e }
            if (r14 == 0) goto L_0x0154
            r0 = 1
        L_0x00b8:
            if (r0 > 0) goto L_0x0181
            r0 = 1
            r2 = r0
        L_0x00bc:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
            r0.<init>()     // Catch:{ all -> 0x017e }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x017e }
            r0.toString()     // Catch:{ all -> 0x017e }
            if (r13 == 0) goto L_0x015c
            r0 = 0
            r1 = r0
        L_0x00d2:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
            r0.<init>()     // Catch:{ all -> 0x017e }
            java.lang.String r6 = ""
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x017e }
            r0.toString()     // Catch:{ all -> 0x017e }
            com.agilebinary.mobilemonitor.device.a.a.b r0 = r9.i     // Catch:{ all -> 0x017e }
            int r0 = r0.a(r12)     // Catch:{ all -> 0x017e }
            int r6 = r0 * 1000
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x017e }
            r0.<init>()     // Catch:{ all -> 0x017e }
            java.lang.String r7 = ""
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ all -> 0x017e }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x017e }
            r0.toString()     // Catch:{ all -> 0x017e }
            long r7 = (long) r3
            long r3 = r7 - r4
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x016f }
            r0.<init>()     // Catch:{ InterruptedException -> 0x016f }
            java.lang.String r5 = ""
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ InterruptedException -> 0x016f }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ InterruptedException -> 0x016f }
            r0.toString()     // Catch:{ InterruptedException -> 0x016f }
            r7 = 0
            int r0 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r0 <= 0) goto L_0x011c
            java.lang.Thread.sleep(r3)     // Catch:{ InterruptedException -> 0x016f }
        L_0x011c:
            r0 = 0
        L_0x011d:
            if (r0 >= r2) goto L_0x0177
            com.agilebinary.mobilemonitor.device.a.d.e r3 = r9.j     // Catch:{ InterruptedException -> 0x016f }
            long r4 = (long) r6     // Catch:{ InterruptedException -> 0x016f }
            boolean r3 = r3.a(r4)     // Catch:{ InterruptedException -> 0x016f }
            if (r3 != 0) goto L_0x0167
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x016f }
            r3.<init>()     // Catch:{ InterruptedException -> 0x016f }
            java.lang.String r4 = ""
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ InterruptedException -> 0x016f }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ InterruptedException -> 0x016f }
            r3.toString()     // Catch:{ InterruptedException -> 0x016f }
            long r3 = (long) r1     // Catch:{ InterruptedException -> 0x016f }
            java.lang.Thread.sleep(r3)     // Catch:{ InterruptedException -> 0x016f }
            java.lang.String r3 = "RETRY"
            r9.a(r3)     // Catch:{ InterruptedException -> 0x016f }
            int r0 = r0 + 1
            goto L_0x011d
        L_0x0146:
            long r0 = (long) r0
            goto L_0x0089
        L_0x0149:
            com.agilebinary.mobilemonitor.device.a.a.b r0 = r9.i     // Catch:{ all -> 0x017e }
            int r0 = r0.d(r12)     // Catch:{ all -> 0x017e }
            int r0 = r0 * 1000
            r3 = r0
            goto L_0x00a3
        L_0x0154:
            com.agilebinary.mobilemonitor.device.a.a.b r0 = r9.i     // Catch:{ all -> 0x017e }
            int r0 = r0.b(r12)     // Catch:{ all -> 0x017e }
            goto L_0x00b8
        L_0x015c:
            com.agilebinary.mobilemonitor.device.a.a.b r0 = r9.i     // Catch:{ all -> 0x017e }
            int r0 = r0.c(r12)     // Catch:{ all -> 0x017e }
            int r0 = r0 * 1000
            r1 = r0
            goto L_0x00d2
        L_0x0167:
            java.lang.String r0 = "ONE_TEST_PASSED"
            r9.a(r0)     // Catch:{ InterruptedException -> 0x016f }
            r0 = 2
        L_0x016d:
            monitor-exit(r9)
            return r0
        L_0x016f:
            r0 = move-exception
            java.lang.String r1 = com.agilebinary.mobilemonitor.device.a.h.b.a     // Catch:{ all -> 0x017e }
            java.lang.String r2 = "InterruptedException"
            com.agilebinary.mobilemonitor.device.a.e.a.e(r1, r2, r0)     // Catch:{ all -> 0x017e }
        L_0x0177:
            java.lang.String r0 = "ONE_TEST_FAILED"
            r9.a(r0)     // Catch:{ all -> 0x017e }
            r0 = 3
            goto L_0x016d
        L_0x017e:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x0181:
            r2 = r0
            goto L_0x00bc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.h.b.a(long, int, boolean, boolean):int");
    }

    private void a(String str) {
        Enumeration elements = this.k.elements();
        while (elements.hasMoreElements()) {
            ((a) elements.nextElement()).a(str, null);
        }
    }

    private synchronized void a(boolean z, boolean z2) {
        "" + z;
        if (this.h.h()) {
            a("START_CHECK_WLAN");
            this.b = a(this.d, 2, z, z2);
            a("END_CHECK_WLAN");
        } else if (this.h.i()) {
            a("START_CHECK_MOBI");
            this.c = a(this.e, 1, z, z2);
            a("END_CHECK_MOBI");
        }
    }

    public final synchronized void a() {
        if (this.f) {
            synchronized (this) {
                try {
                    wait(this.g);
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    public final void a(int i2, boolean z) {
        if (i2 == 1) {
            if (z) {
                this.e = System.currentTimeMillis();
                this.c = 1;
                a("CONNECTIVITY_MOBI_UP");
            } else {
                this.c = 0;
                a("CONNECTIVITY_MOBI_DOWN");
            }
        }
        if (i2 == 2) {
            if (z) {
                this.d = System.currentTimeMillis();
                this.b = 1;
                a("CONNECTIVITY_WLAN_UP");
            } else {
                this.b = 0;
                a("CONNECTIVITY_WLAN_DOWN");
            }
        }
        if (i2 == 0) {
            a("CONNECTIVITY_NO_CONNECTIVITY");
        }
    }

    public final void a(a aVar) {
        this.k.addElement(aVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0040, code lost:
        if (r3.h.h() != false) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(boolean r4, boolean r5, boolean r6) {
        /*
            r3 = this;
            r2 = 2
            monitor-enter(r3)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0058 }
            r0.<init>()     // Catch:{ all -> 0x0058 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0058 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x0058 }
            r0.toString()     // Catch:{ all -> 0x0058 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0058 }
            r0.<init>()     // Catch:{ all -> 0x0058 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0058 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ all -> 0x0058 }
            r0.toString()     // Catch:{ all -> 0x0058 }
            if (r4 != 0) goto L_0x0042
            int r0 = r3.c     // Catch:{ all -> 0x0058 }
            if (r0 != r2) goto L_0x0036
            com.agilebinary.mobilemonitor.device.a.c.g r0 = r3.h     // Catch:{ all -> 0x0058 }
            boolean r0 = r0.i()     // Catch:{ all -> 0x0058 }
            if (r0 == 0) goto L_0x0036
        L_0x0034:
            monitor-exit(r3)
            return
        L_0x0036:
            int r0 = r3.b     // Catch:{ all -> 0x0058 }
            if (r0 != r2) goto L_0x0042
            com.agilebinary.mobilemonitor.device.a.c.g r0 = r3.h     // Catch:{ all -> 0x0058 }
            boolean r0 = r0.h()     // Catch:{ all -> 0x0058 }
            if (r0 != 0) goto L_0x0034
        L_0x0042:
            r0 = 1
            r3.f = r0     // Catch:{ all -> 0x005b }
            r3.a(r5, r6)     // Catch:{ all -> 0x005b }
            monitor-enter(r3)     // Catch:{ all -> 0x0058 }
            r0 = 0
            r3.f = r0     // Catch:{ all -> 0x0055 }
            r0 = 0
            r3.g = r0     // Catch:{ all -> 0x0055 }
            r3.notifyAll()     // Catch:{ all -> 0x0055 }
            monitor-exit(r3)     // Catch:{ all -> 0x0055 }
            goto L_0x0034
        L_0x0055:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0055 }
            throw r0     // Catch:{ all -> 0x0058 }
        L_0x0058:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x005b:
            r0 = move-exception
            monitor-enter(r3)     // Catch:{ all -> 0x0058 }
            r1 = 0
            r3.f = r1     // Catch:{ all -> 0x0069 }
            r1 = 0
            r3.g = r1     // Catch:{ all -> 0x0069 }
            r3.notifyAll()     // Catch:{ all -> 0x0069 }
            monitor-exit(r3)     // Catch:{ all -> 0x0069 }
            throw r0     // Catch:{ all -> 0x0058 }
        L_0x0069:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0069 }
            throw r0     // Catch:{ all -> 0x0058 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.h.b.a(boolean, boolean, boolean):void");
    }

    public final int b() {
        return this.c;
    }

    public final void b(a aVar) {
        this.k.removeElement(aVar);
    }

    public final int c() {
        return this.b;
    }

    public final boolean d() {
        return this.c == 2 || this.b == 2;
    }

    public final int e() {
        if (this.b == 2) {
            return 2;
        }
        return this.c == 2 ? 1 : 0;
    }
}
