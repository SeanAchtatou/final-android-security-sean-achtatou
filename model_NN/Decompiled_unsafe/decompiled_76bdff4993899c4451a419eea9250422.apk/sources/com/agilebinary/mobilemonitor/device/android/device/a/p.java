package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class p extends SQLiteOpenHelper {
    private /* synthetic */ i a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(i iVar, Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
        this.a = iVar;
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        String format = String.format("CREATE TABLE %1$s (%2$s STRING PRIMARY KEY, %3$s INTEGER);", u.a, u.b, u.c);
        String.format("Executing SQL: %1$s", format);
        sQLiteDatabase.execSQL(format);
        boolean unused = this.a.i = true;
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        String.format("Upgrading database from version %1$s to %2$s, destroying all existing data", Integer.valueOf(i), Integer.valueOf(i2));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", u.a));
        onCreate(sQLiteDatabase);
    }
}
