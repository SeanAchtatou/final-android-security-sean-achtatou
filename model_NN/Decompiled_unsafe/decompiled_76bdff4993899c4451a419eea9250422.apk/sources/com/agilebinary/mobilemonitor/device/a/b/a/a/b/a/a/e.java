package com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.b;

public final class e extends b {
    private g a;
    private int b;
    private int c;
    private byte[] d;
    private byte[] e;
    private boolean f;
    private b g;

    public e(b bVar, int i) {
        this(bVar, i, false);
    }

    private e(b bVar, int i, boolean z) {
        this.a = new g();
        this.b = 512;
        this.c = 0;
        this.d = new byte[this.b];
        this.e = new byte[1];
        this.g = bVar;
        g gVar = this.a;
        gVar.j = new l();
        gVar.j.a(gVar, i, 15);
        this.f = true;
    }

    private void b() {
        if (this.a != null) {
            if (this.f) {
                g gVar = this.a;
                if (gVar.j != null) {
                    gVar.j.a();
                    gVar.j = null;
                }
            } else {
                g gVar2 = this.a;
                if (gVar2.k != null) {
                    gVar2.k.a(gVar2);
                    gVar2.k = null;
                }
            }
            g gVar3 = this.a;
            gVar3.a = null;
            gVar3.e = null;
            gVar3.i = null;
            gVar3.m = null;
            this.a = null;
        }
    }

    public final void a() {
        while (true) {
            try {
                this.a.e = this.d;
                this.a.f = 0;
                this.a.g = this.b;
                int b2 = this.f ? this.a.b(4) : this.a.a(4);
                if (b2 == 1 || b2 == 0) {
                    if (this.b - this.a.g > 0) {
                        this.g.a(this.d, 0, this.b - this.a.g);
                    }
                    if (this.a.c <= 0 && this.a.g != 0) {
                        break;
                    }
                } else {
                    throw new h((this.f ? "de" : "in") + "flating: " + this.a.i);
                }
            } catch (com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.e e2) {
            } catch (Throwable th) {
                b();
                this.g.a();
                this.g = null;
                throw th;
            }
        }
        b();
        this.g.a();
        this.g = null;
    }

    public final void a(int i) {
        this.e[0] = (byte) i;
        a(this.e, 0, 1);
    }

    public final void a(byte[] bArr, int i, int i2) {
        if (i2 != 0) {
            this.a.a = bArr;
            this.a.b = i;
            this.a.c = i2;
            while (true) {
                this.a.e = this.d;
                this.a.f = 0;
                this.a.g = this.b;
                if ((this.f ? this.a.b(0) : this.a.a(0)) != 0) {
                    throw new h((this.f ? "de" : "in") + "flating: " + this.a.i);
                }
                this.g.a(this.d, 0, this.b - this.a.g);
                if (this.a.c <= 0 && this.a.g != 0) {
                    return;
                }
            }
        }
    }
}
