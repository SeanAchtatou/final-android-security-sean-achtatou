package com.agilebinary.mobilemonitor.device.android.b;

import com.agilebinary.a.a.a.c.d.f;
import com.agilebinary.a.a.a.e.a;
import com.agilebinary.a.a.a.g.d;
import com.agilebinary.a.a.a.h.b.g;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.j;
import com.agilebinary.mobilemonitor.device.a.d.c;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public final class b extends f implements c {
    private static String a = com.agilebinary.mobilemonitor.device.a.g.f.a();
    private static b b = null;
    private static a c;

    static {
        a aVar = new a();
        c = aVar;
        aVar.b("http.connection.timeout", 10000);
        c.b("http.socket.timeout", 10000);
        c.b("http.socket.linger", 10);
    }

    private b(com.agilebinary.mobilemonitor.device.a.a.b bVar) {
        super(new com.agilebinary.a.a.a.c.b.a.b(new h(), 5, TimeUnit.SECONDS));
        com.agilebinary.a.a.a.c.b.a.b bVar2 = (com.agilebinary.a.a.a.c.b.a.b) v();
        bVar2.b(5);
        bVar2.a(20);
        try {
            v().a().a(new g("https", new e(bVar), 443));
        } catch (Exception e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "error creating SSLSocketFactory", e);
        }
    }

    public static b a() {
        if (b == null) {
            b = new b(com.agilebinary.mobilemonitor.device.a.a.b.a());
        }
        return b;
    }

    public static void b() {
        b = null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:16:? A[Catch:{ Exception -> 0x0026 }, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.mobilemonitor.device.a.d.a a(com.agilebinary.mobilemonitor.device.a.d.b r6, java.lang.String r7, long r8) {
        /*
            r5 = this;
            r0 = 0
            com.agilebinary.mobilemonitor.device.android.c.c.a()
            com.agilebinary.a.a.a.d.c.a r2 = new com.agilebinary.a.a.a.d.c.a     // Catch:{ Exception -> 0x0017 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x0017 }
            com.agilebinary.a.a.a.j r3 = com.agilebinary.mobilemonitor.device.android.b.c.a(r6, r5, r2, r8)     // Catch:{ Exception -> 0x0026 }
            if (r3 != 0) goto L_0x0010
        L_0x000f:
            return r0
        L_0x0010:
            com.agilebinary.mobilemonitor.device.android.b.a r1 = new com.agilebinary.mobilemonitor.device.android.b.a     // Catch:{ Exception -> 0x0026 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0026 }
            r0 = r1
            goto L_0x000f
        L_0x0017:
            r1 = move-exception
            r2 = r0
        L_0x0019:
            java.lang.String r3 = com.agilebinary.mobilemonitor.device.android.b.b.a
            java.lang.String r4 = "Exception"
            com.agilebinary.mobilemonitor.device.a.e.a.e(r3, r4, r1)
            if (r2 == 0) goto L_0x000f
            r2.d()
            goto L_0x000f
        L_0x0026:
            r1 = move-exception
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.b.b.a(com.agilebinary.mobilemonitor.device.a.d.b, java.lang.String, long):com.agilebinary.mobilemonitor.device.a.d.a");
    }

    public final com.agilebinary.mobilemonitor.device.a.d.a a(com.agilebinary.mobilemonitor.device.a.d.b bVar, String str, byte[] bArr, long j) {
        com.agilebinary.mobilemonitor.device.android.c.c.a();
        com.agilebinary.a.a.a.d.c.c cVar = new com.agilebinary.a.a.a.d.c.c(str);
        if (bArr != null) {
            cVar.a(new d(bArr));
        }
        try {
            j a2 = c.a(bVar, this, cVar, j);
            if (a2 == null) {
                return null;
            }
            return new a(a2);
        } catch (com.agilebinary.a.a.a.d.h e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "ClientProtocolException", e);
            cVar.d();
            return null;
        } catch (IllegalStateException e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "IllegalStateException", e2);
            cVar.d();
            return null;
        } catch (IOException e3) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "IOException", e3);
            cVar.d();
            return null;
        }
    }

    public final void c() {
        v().a(0, TimeUnit.MILLISECONDS);
    }

    public final void d() {
        v().a(1000, TimeUnit.MILLISECONDS);
    }
}
