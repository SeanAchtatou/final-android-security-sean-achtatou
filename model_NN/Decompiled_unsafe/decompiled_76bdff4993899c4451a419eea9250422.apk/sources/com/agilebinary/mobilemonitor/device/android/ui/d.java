package com.agilebinary.mobilemonitor.device.android.ui;

import android.net.Uri;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.f.b;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;

final class d extends at {
    private /* synthetic */ MainActivity b;

    d(MainActivity mainActivity) {
        this.b = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, int):boolean
      com.agilebinary.mobilemonitor.device.a.a.f.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object[] objArr) {
        String str = ((String[]) objArr)[0];
        b bVar = new b();
        int a = this.b.C.a(str, this.b.B.u());
        if (!d()) {
            if (a == 0) {
                c.a().b((String) null, true);
                bVar.a = true;
                BackgroundService.a(this.b, "EXTRA_DEACTIVATE_FROM_GUI", (Uri) null);
            } else {
                bVar.a = false;
                bVar.b = b.a("ACTIVATION_ERROR_" + a);
            }
        }
        return bVar;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        try {
            this.b.s.hide();
            this.b.a(b.a("COMMONS_DEACTIVATION_FAILED", b.a("COMMONS_CANCELLED")));
        } catch (Exception e) {
            a.e(a, "onCancelled5", e);
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        b bVar = (b) obj;
        super.a(bVar);
        try {
            this.b.s.hide();
            this.b.d();
            if (bVar == null || !bVar.a || d()) {
                this.b.a(b.a("COMMONS_DEACTIVATION_FAILED", bVar.b));
                return;
            }
            com.agilebinary.mobilemonitor.device.android.device.admin.a.b(this.b);
            this.b.a((String) null);
        } catch (Exception e) {
            a.e(a, "onPostExecute6", e);
        }
    }
}
