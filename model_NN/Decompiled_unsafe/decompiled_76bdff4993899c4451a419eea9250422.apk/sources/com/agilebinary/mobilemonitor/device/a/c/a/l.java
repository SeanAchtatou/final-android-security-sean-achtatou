package com.agilebinary.mobilemonitor.device.a.c.a;

public final class l {
    int a;
    private String b;

    public l(String str, int i) {
        this.b = str;
        this.a = i;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof l)) {
            return false;
        }
        return this.b.equals(((l) obj).b);
    }

    public final int hashCode() {
        return this.b.hashCode();
    }
}
