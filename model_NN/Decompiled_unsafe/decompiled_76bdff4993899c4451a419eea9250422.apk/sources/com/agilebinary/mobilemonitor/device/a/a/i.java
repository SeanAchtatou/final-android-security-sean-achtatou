package com.agilebinary.mobilemonitor.device.a.a;

import com.agilebinary.mobilemonitor.device.a.g.f;

public abstract class i {
    protected byte[] a = new byte[8];

    static {
        f.a();
    }

    public i() {
        this.a[0] = 84;
        this.a[1] = (byte) ((this.a[0] * 2) + 7);
        this.a[2] = (byte) ((this.a[1] - 9) + 13);
        this.a[3] = (byte) ((this.a[2] * 5) + (this.a[1] * 13));
        this.a[4] = (byte) ((this.a[0] * 7) + (this.a[3] * 15));
        this.a[5] = (byte) ((this.a[2] - 9) * 66);
        this.a[6] = 10;
        this.a[7] = (byte) ((this.a[5] * 5) + (this.a[3] * 13));
    }

    public abstract byte[] a(byte[] bArr);

    public abstract byte[] b(byte[] bArr);
}
