package com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a;

import bsh.ParserConstants;

public final class l {
    private static final f[] l;
    private static final String[] m = {"need dictionary", "stream end", "", "file error", "stream error", "data error", "insufficient memory", "buffer error", "incompatible version", ""};
    private int A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private int G;
    private int H;
    private int I;
    private int J;
    private int K;
    private int L;
    private int M;
    private int N;
    private int O;
    private int P;
    private int Q;
    private int R;
    private short[] S = new short[1146];
    private short[] T = new short[ParserConstants.ANDASSIGN];
    private short[] U = new short[78];
    private j V = new j();
    private j W = new j();
    private j X = new j();
    private int Y;
    private int Z;
    byte[] a;
    private int aa;
    private int ab;
    private int ac;
    private int ad;
    private short ae;
    private int af;
    int b;
    int c;
    int d;
    short[] e = new short[16];
    int[] f = new int[573];
    int g;
    int h;
    byte[] i = new byte[573];
    int j;
    int k;
    private g n;
    private int o;
    private int p;
    private byte q;
    private int r;
    private int s;
    private int t;
    private int u;
    private byte[] v;
    private int w;
    private short[] x;
    private short[] y;
    private int z;

    static {
        f[] fVarArr = new f[10];
        l = fVarArr;
        fVarArr[0] = new f(0, 0, 0, 0, 0);
        l[1] = new f(4, 4, 8, 4, 1);
        l[2] = new f(4, 5, 16, 8, 1);
        l[3] = new f(4, 6, 32, 32, 1);
        l[4] = new f(4, 4, 16, 16, 2);
        l[5] = new f(8, 16, 32, 32, 2);
        l[6] = new f(8, 16, ParserConstants.LSHIFTASSIGN, ParserConstants.LSHIFTASSIGN, 2);
        l[7] = new f(8, 32, ParserConstants.LSHIFTASSIGN, 256, 2);
        l[8] = new f(32, ParserConstants.LSHIFTASSIGN, 258, 1024, 2);
        l[9] = new f(32, 258, 258, 4096, 2);
    }

    l() {
    }

    private void a(byte b2) {
        byte[] bArr = this.a;
        int i2 = this.c;
        this.c = i2 + 1;
        bArr[i2] = b2;
    }

    private void a(int i2) {
        a((byte) i2);
        a((byte) (i2 >>> 8));
    }

    private void a(int i2, int i3) {
        if (this.af > 16 - i3) {
            this.ae = (short) (this.ae | ((i2 << this.af) & 65535));
            a(this.ae);
            this.ae = (short) (i2 >>> (16 - this.af));
            this.af += i3 - 16;
            return;
        }
        this.ae = (short) (this.ae | ((i2 << this.af) & 65535));
        this.af += i3;
    }

    private void a(int i2, int i3, boolean z2) {
        a((z2 ? 1 : 0) + 0, 3);
        d();
        this.ad = 8;
        a((short) i3);
        a((short) (i3 ^ -1));
        System.arraycopy(this.v, i2, this.a, this.c, i3);
        this.c += i3;
    }

    private void a(int i2, short[] sArr) {
        int i3 = i2 * 2;
        a(sArr[i3] & 65535, sArr[i3 + 1] & 65535);
    }

    private void a(boolean z2) {
        int i2;
        int i3;
        int i4;
        int i5 = 1;
        int i6 = this.E >= 0 ? this.E : -1;
        int i7 = this.I - this.E;
        if (this.O > 0) {
            if (this.q == 2) {
                int i8 = 0;
                int i9 = 0;
                while (i9 < 7) {
                    i8 += this.S[i9 * 2];
                    i9++;
                }
                int i10 = 0;
                while (i9 < 128) {
                    i10 += this.S[i9 * 2];
                    i9++;
                }
                while (i9 < 256) {
                    i8 += this.S[i9 * 2];
                    i9++;
                }
                this.q = (byte) (i8 > (i10 >>> 2) ? 0 : 1);
            }
            this.V.a(this);
            this.W.a(this);
            b(this.S, this.V.i);
            b(this.T, this.W.i);
            this.X.a(this);
            i4 = 18;
            while (i4 >= 3 && this.U[(j.d[i4] * 2) + 1] == 0) {
                i4--;
            }
            this.j += ((i4 + 1) * 3) + 5 + 5 + 4;
            i3 = ((this.j + 3) + 7) >>> 3;
            i2 = ((this.k + 3) + 7) >>> 3;
            if (i2 <= i3) {
                i3 = i2;
            }
        } else {
            int i11 = i7 + 5;
            i2 = i11;
            i3 = i11;
            i4 = 0;
        }
        if (i7 + 4 <= i3 && i6 != -1) {
            a(i6, i7, z2);
        } else if (i2 == i3) {
            if (!z2) {
                i5 = 0;
            }
            a(i5 + 2, 3);
            a(c.a, c.b);
        } else {
            if (!z2) {
                i5 = 0;
            }
            a(i5 + 4, 3);
            int i12 = this.V.i + 1;
            int i13 = this.W.i + 1;
            int i14 = i4 + 1;
            a(i12 - 257, 5);
            a(i13 - 1, 5);
            a(i14 - 4, 4);
            for (int i15 = 0; i15 < i14; i15++) {
                a(this.U[(j.d[i15] * 2) + 1], 3);
            }
            c(this.S, i12 - 1);
            c(this.T, i13 - 1);
            a(this.S, this.T);
        }
        b();
        if (z2) {
            d();
        }
        this.E = this.I;
        this.n.a();
    }

    private void a(short[] sArr, short[] sArr2) {
        int i2 = 0;
        if (this.aa != 0) {
            do {
                byte b2 = ((this.a[this.ab + (i2 * 2)] << 8) & 65280) | (this.a[this.ab + (i2 * 2) + 1] & 255);
                byte b3 = this.a[this.Y + i2] & 255;
                i2++;
                if (b2 == 0) {
                    a(b3, sArr);
                } else {
                    byte b4 = j.e[b3];
                    a(b4 + 256 + 1, sArr);
                    int i3 = j.a[b4];
                    if (i3 != 0) {
                        a(b3 - j.f[b4], i3);
                    }
                    int i4 = b2 - 1;
                    int a2 = j.a(i4);
                    a(a2, sArr2);
                    int i5 = j.b[a2];
                    if (i5 != 0) {
                        a(i4 - j.g[a2], i5);
                    }
                }
            } while (i2 < this.aa);
        }
        a(256, sArr);
        this.ad = sArr[513];
    }

    private static boolean a(short[] sArr, int i2, int i3, byte[] bArr) {
        short s2 = sArr[i2 * 2];
        short s3 = sArr[i3 * 2];
        return s2 < s3 || (s2 == s3 && bArr[i2] <= bArr[i3]);
    }

    private void b() {
        for (int i2 = 0; i2 < 286; i2++) {
            this.S[i2 * 2] = 0;
        }
        for (int i3 = 0; i3 < 30; i3++) {
            this.T[i3 * 2] = 0;
        }
        for (int i4 = 0; i4 < 19; i4++) {
            this.U[i4 * 2] = 0;
        }
        this.S[512] = 1;
        this.k = 0;
        this.j = 0;
        this.ac = 0;
        this.aa = 0;
    }

    private void b(int i2) {
        a((byte) (i2 >> 8));
        a((byte) i2);
    }

    private void b(short[] sArr, int i2) {
        int i3;
        int i4;
        short s2 = -1;
        short s3 = sArr[1];
        if (s3 == 0) {
            i4 = 138;
            i3 = 3;
        } else {
            i3 = 4;
            i4 = 7;
        }
        sArr[((i2 + 1) * 2) + 1] = -1;
        int i5 = 0;
        int i6 = 0;
        while (i6 <= i2) {
            short s4 = sArr[((i6 + 1) * 2) + 1];
            i5++;
            if (i5 >= i4 || s3 != s4) {
                if (i5 < i3) {
                    short[] sArr2 = this.U;
                    int i7 = s3 * 2;
                    sArr2[i7] = (short) (i5 + sArr2[i7]);
                } else if (s3 != 0) {
                    if (s3 != s2) {
                        short[] sArr3 = this.U;
                        int i8 = s3 * 2;
                        sArr3[i8] = (short) (sArr3[i8] + 1);
                    }
                    short[] sArr4 = this.U;
                    sArr4[32] = (short) (sArr4[32] + 1);
                } else if (i5 <= 10) {
                    short[] sArr5 = this.U;
                    sArr5[34] = (short) (sArr5[34] + 1);
                } else {
                    short[] sArr6 = this.U;
                    sArr6[36] = (short) (sArr6[36] + 1);
                }
                if (s4 == 0) {
                    i4 = 138;
                    i5 = 0;
                    s2 = s3;
                    i3 = 3;
                } else if (s3 == s4) {
                    i4 = 6;
                    i5 = 0;
                    s2 = s3;
                    i3 = 3;
                } else {
                    i3 = 4;
                    i4 = 7;
                    i5 = 0;
                    s2 = s3;
                }
            }
            i6++;
            s3 = s4;
        }
    }

    private boolean b(int i2, int i3) {
        this.a[this.ab + (this.aa * 2)] = (byte) (i2 >>> 8);
        this.a[this.ab + (this.aa * 2) + 1] = (byte) i2;
        this.a[this.Y + this.aa] = (byte) i3;
        this.aa++;
        if (i2 == 0) {
            short[] sArr = this.S;
            int i4 = i3 * 2;
            sArr[i4] = (short) (sArr[i4] + 1);
        } else {
            this.ac++;
            short[] sArr2 = this.S;
            int i5 = (j.e[i3] + 256 + 1) * 2;
            sArr2[i5] = (short) (sArr2[i5] + 1);
            short[] sArr3 = this.T;
            int a2 = j.a(i2 - 1) * 2;
            sArr3[a2] = (short) (sArr3[a2] + 1);
        }
        if ((this.aa & 8191) == 0 && this.O > 2) {
            int i6 = this.I - this.E;
            int i7 = this.aa * 8;
            for (int i8 = 0; i8 < 30; i8++) {
                i7 = (int) (((long) i7) + (((long) this.T[i8 * 2]) * (5 + ((long) j.b[i8]))));
            }
            int i9 = i7 >>> 3;
            if (this.ac < this.aa / 2 && i9 < i6 / 2) {
                return true;
            }
        }
        return this.aa == this.Z + -1;
    }

    private int c(int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        byte b2;
        byte b3;
        int i7 = this.M;
        int i8 = this.I;
        int i9 = this.L;
        int i10 = this.I > this.s + -262 ? this.I - (this.s - 262) : 0;
        int i11 = this.R;
        int i12 = this.u;
        int i13 = this.I + 258;
        byte b4 = this.v[(i8 + i9) - 1];
        byte b5 = this.v[i8 + i9];
        if (this.L >= this.Q) {
            i7 >>= 2;
        }
        if (i11 > this.K) {
            byte b6 = b5;
            b2 = b4;
            i6 = this.K;
            i5 = i9;
            i4 = i8;
            i3 = i7;
            b3 = b6;
        } else {
            byte b7 = b5;
            b2 = b4;
            i6 = i11;
            i5 = i9;
            i4 = i8;
            i3 = i7;
            b3 = b7;
        }
        while (true) {
            if (this.v[i2 + i5] == b3 && this.v[(i2 + i5) - 1] == b2 && this.v[i2] == this.v[i4]) {
                int i14 = i2 + 1;
                if (this.v[i14] == this.v[i4 + 1]) {
                    int i15 = i4 + 2;
                    int i16 = i14 + 1;
                    do {
                        i15++;
                        int i17 = i16 + 1;
                        if (this.v[i15] != this.v[i17]) {
                            break;
                        }
                        i15++;
                        int i18 = i17 + 1;
                        if (this.v[i15] != this.v[i18]) {
                            break;
                        }
                        i15++;
                        int i19 = i18 + 1;
                        if (this.v[i15] != this.v[i19]) {
                            break;
                        }
                        i15++;
                        int i20 = i19 + 1;
                        if (this.v[i15] != this.v[i20]) {
                            break;
                        }
                        i15++;
                        int i21 = i20 + 1;
                        if (this.v[i15] != this.v[i21]) {
                            break;
                        }
                        i15++;
                        int i22 = i21 + 1;
                        if (this.v[i15] != this.v[i22]) {
                            break;
                        }
                        i15++;
                        int i23 = i22 + 1;
                        if (this.v[i15] != this.v[i23]) {
                            break;
                        }
                        i15++;
                        i16 = i23 + 1;
                        if (this.v[i15] != this.v[i16]) {
                            break;
                        }
                    } while (i15 < i13);
                    int i24 = 258 - (i13 - i15);
                    int i25 = i13 - 258;
                    if (i24 > i5) {
                        this.J = i2;
                        if (i24 >= i6) {
                            i5 = i24;
                            break;
                        }
                        b2 = this.v[(i25 + i24) - 1];
                        b3 = this.v[i25 + i24];
                        i5 = i24;
                        i4 = i25;
                    } else {
                        i4 = i25;
                    }
                }
            }
            i2 = this.x[i2 & i12] & 65535;
            if (i2 > i10) {
                i3--;
                if (i3 == 0) {
                    break;
                }
            } else {
                break;
            }
        }
        return i5 <= this.K ? i5 : this.K;
    }

    private void c() {
        if (this.af == 16) {
            a(this.ae);
            this.ae = 0;
            this.af = 0;
        } else if (this.af >= 8) {
            a((byte) this.ae);
            this.ae = (short) (this.ae >>> 8);
            this.af -= 8;
        }
    }

    private void c(short[] sArr, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        short s2 = -1;
        short s3 = sArr[1];
        if (s3 == 0) {
            i3 = 3;
            i4 = 138;
        } else {
            i3 = 4;
            i4 = 7;
        }
        int i8 = i4;
        int i9 = 0;
        int i10 = i3;
        int i11 = 0;
        while (i9 <= i2) {
            short s4 = sArr[((i9 + 1) * 2) + 1];
            int i12 = i11 + 1;
            if (i12 >= i8 || s3 != s4) {
                if (i12 < i10) {
                    do {
                        a(s3, this.U);
                        i12--;
                    } while (i12 != 0);
                } else if (s3 != 0) {
                    if (s3 != s2) {
                        a(s3, this.U);
                        i12--;
                    }
                    a(16, this.U);
                    a(i12 - 3, 2);
                } else if (i12 <= 10) {
                    a(17, this.U);
                    a(i12 - 3, 3);
                } else {
                    a(18, this.U);
                    a(i12 - 11, 7);
                }
                if (s4 == 0) {
                    i7 = 3;
                    i6 = 138;
                    i5 = 0;
                    s2 = s3;
                } else if (s3 == s4) {
                    i6 = 6;
                    i5 = 0;
                    s2 = s3;
                    i7 = 3;
                } else {
                    i7 = 4;
                    i6 = 7;
                    i5 = 0;
                    s2 = s3;
                }
            } else {
                int i13 = i10;
                i6 = i8;
                i5 = i12;
                i7 = i13;
            }
            i9++;
            s3 = s4;
            int i14 = i5;
            i8 = i6;
            i10 = i7;
            i11 = i14;
        }
    }

    private void d() {
        if (this.af > 8) {
            a(this.ae);
        } else if (this.af > 0) {
            a((byte) this.ae);
        }
        this.ae = 0;
        this.af = 0;
    }

    private void e() {
        int i2;
        do {
            int i3 = (this.w - this.K) - this.I;
            if (i3 == 0 && this.I == 0 && this.K == 0) {
                i2 = this.s;
            } else if (i3 == -1) {
                i2 = i3 - 1;
            } else if (this.I >= (this.s + this.s) - 262) {
                System.arraycopy(this.v, this.s, this.v, 0, this.s);
                this.J -= this.s;
                this.I -= this.s;
                this.E -= this.s;
                int i4 = this.A;
                int i5 = i4;
                do {
                    i4--;
                    short s2 = this.y[i4] & 65535;
                    this.y[i4] = s2 >= this.s ? (short) (s2 - this.s) : 0;
                    i5--;
                } while (i5 != 0);
                int i6 = this.s;
                int i7 = i6;
                do {
                    i6--;
                    short s3 = this.x[i6] & 65535;
                    this.x[i6] = s3 >= this.s ? (short) (s3 - this.s) : 0;
                    i7--;
                } while (i7 != 0);
                i2 = this.s + i3;
            } else {
                i2 = i3;
            }
            if (this.n.c != 0) {
                g gVar = this.n;
                byte[] bArr = this.v;
                int i8 = this.K + this.I;
                int i9 = gVar.c;
                if (i9 <= i2) {
                    i2 = i9;
                }
                if (i2 == 0) {
                    i2 = 0;
                } else {
                    gVar.c -= i2;
                    if (gVar.j.d == 0) {
                        gVar.l = i.a(gVar.l, gVar.a, gVar.b, i2);
                    }
                    System.arraycopy(gVar.a, gVar.b, bArr, i8, i2);
                    gVar.b += i2;
                    gVar.d += (long) i2;
                }
                this.K = i2 + this.K;
                if (this.K >= 3) {
                    this.z = this.v[this.I] & 255;
                    this.z = ((this.z << this.D) ^ (this.v[this.I + 1] & 255)) & this.C;
                }
                if (this.K >= 262) {
                    return;
                }
            } else {
                return;
            }
        } while (this.n.c != 0);
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        if (this.o != 42 && this.o != 113 && this.o != 666) {
            return -2;
        }
        this.a = null;
        this.y = null;
        this.x = null;
        this.v = null;
        return this.o == 113 ? -3 : 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.l.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.l.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.g, int, int):int
      com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.l.a(int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final int a(g gVar, int i2) {
        int i3;
        short s2;
        boolean z2;
        short s3;
        int i4;
        if (i2 > 4 || i2 < 0) {
            return -2;
        }
        if (gVar.e == null || ((gVar.a == null && gVar.c != 0) || (this.o == 666 && i2 != 4))) {
            gVar.i = m[4];
            return -2;
        } else if (gVar.g == 0) {
            gVar.i = m[7];
            return -5;
        } else {
            this.n = gVar;
            int i5 = this.r;
            this.r = i2;
            if (this.o == 42) {
                int i6 = (((this.t - 8) << 4) + 8) << 8;
                int i7 = ((this.O - 1) & 255) >> 1;
                if (i7 > 3) {
                    i7 = 3;
                }
                int i8 = (i7 << 6) | i6;
                if (this.I != 0) {
                    i8 |= 32;
                }
                this.o = ParserConstants.LSHIFTX;
                b(i8 + (31 - (i8 % 31)));
                if (this.I != 0) {
                    b((int) (gVar.l >>> 16));
                    b((int) (gVar.l & 65535));
                }
                gVar.l = i.a(0, null, 0, 0);
            }
            if (this.c != 0) {
                gVar.a();
                if (gVar.g == 0) {
                    this.r = -1;
                    return 0;
                }
            } else if (gVar.c == 0 && i2 <= i5 && i2 != 4) {
                gVar.i = m[7];
                return -5;
            }
            if (this.o != 666 || gVar.c == 0) {
                if (!(gVar.c == 0 && this.K == 0 && (i2 == 0 || this.o == 666))) {
                    char c2 = 65535;
                    switch (l[this.O].e) {
                        case 0:
                            int i9 = 65535;
                            if (65535 > this.p - 5) {
                                i9 = this.p - 5;
                            }
                            while (true) {
                                if (this.K <= 1) {
                                    e();
                                    if (this.K != 0 || i2 != 0) {
                                        if (this.K == 0) {
                                            a(i2 == 4);
                                            if (this.n.g != 0) {
                                                if (i2 != 4) {
                                                    c2 = 1;
                                                    break;
                                                } else {
                                                    c2 = 3;
                                                    break;
                                                }
                                            } else if (i2 != 4) {
                                                c2 = 0;
                                                break;
                                            } else {
                                                c2 = 2;
                                                break;
                                            }
                                        }
                                    } else {
                                        c2 = 0;
                                        break;
                                    }
                                }
                                this.I += this.K;
                                this.K = 0;
                                int i10 = this.E + i9;
                                if (this.I == 0 || this.I >= i10) {
                                    this.K = this.I - i10;
                                    this.I = i10;
                                    a(false);
                                    if (this.n.g == 0) {
                                        c2 = 0;
                                        break;
                                    }
                                }
                                if (this.I - this.E >= this.s - 262) {
                                    a(false);
                                    if (this.n.g == 0) {
                                        c2 = 0;
                                        break;
                                    }
                                }
                            }
                        case 1:
                            short s4 = 0;
                            while (true) {
                                if (this.K < 262) {
                                    e();
                                    if (this.K >= 262 || i2 != 0) {
                                        if (this.K == 0) {
                                            a(i2 == 4);
                                            if (this.n.g != 0) {
                                                if (i2 != 4) {
                                                    c2 = 1;
                                                    break;
                                                } else {
                                                    c2 = 3;
                                                    break;
                                                }
                                            } else if (i2 != 4) {
                                                c2 = 0;
                                                break;
                                            } else {
                                                c2 = 2;
                                                break;
                                            }
                                        }
                                    } else {
                                        c2 = 0;
                                        break;
                                    }
                                }
                                if (this.K >= 3) {
                                    this.z = ((this.z << this.D) ^ (this.v[this.I + 2] & 255)) & this.C;
                                    s2 = 65535 & this.y[this.z];
                                    this.x[this.I & this.u] = this.y[this.z];
                                    this.y[this.z] = (short) this.I;
                                } else {
                                    s2 = s4;
                                }
                                if (!(((long) s2) == 0 || ((this.I - s2) & 65535) > this.s - 262 || this.P == 2)) {
                                    this.F = c(s2);
                                }
                                if (this.F >= 3) {
                                    boolean b2 = b(this.I - this.J, this.F - 3);
                                    this.K -= this.F;
                                    if (this.F > this.N || this.K < 3) {
                                        this.I += this.F;
                                        this.F = 0;
                                        this.z = this.v[this.I] & 255;
                                        this.z = ((this.z << this.D) ^ (this.v[this.I + 1] & 255)) & this.C;
                                        boolean z3 = b2;
                                        s4 = s2;
                                        z2 = z3;
                                    } else {
                                        this.F--;
                                        do {
                                            this.I++;
                                            this.z = ((this.z << this.D) ^ (this.v[this.I + 2] & 255)) & this.C;
                                            s3 = this.y[this.z] & 65535;
                                            this.x[this.I & this.u] = this.y[this.z];
                                            this.y[this.z] = (short) this.I;
                                            i4 = this.F - 1;
                                            this.F = i4;
                                        } while (i4 != 0);
                                        this.I++;
                                        boolean z4 = b2;
                                        s4 = s3;
                                        z2 = z4;
                                    }
                                } else {
                                    boolean b3 = b(0, this.v[this.I] & 255);
                                    this.K--;
                                    this.I++;
                                    boolean z5 = b3;
                                    s4 = s2;
                                    z2 = z5;
                                }
                                if (z2) {
                                    a(false);
                                    if (this.n.g == 0) {
                                        c2 = 0;
                                        break;
                                    }
                                }
                            }
                            break;
                        case 2:
                            short s5 = 0;
                            while (true) {
                                if (this.K < 262) {
                                    e();
                                    if (this.K >= 262 || i2 != 0) {
                                        if (this.K == 0) {
                                            if (this.H != 0) {
                                                b(0, this.v[this.I - 1] & 255);
                                                this.H = 0;
                                            }
                                            a(i2 == 4);
                                            if (this.n.g != 0) {
                                                if (i2 != 4) {
                                                    c2 = 1;
                                                    break;
                                                } else {
                                                    c2 = 3;
                                                    break;
                                                }
                                            } else if (i2 != 4) {
                                                c2 = 0;
                                                break;
                                            } else {
                                                c2 = 2;
                                                break;
                                            }
                                        }
                                    } else {
                                        c2 = 0;
                                        break;
                                    }
                                }
                                if (this.K >= 3) {
                                    this.z = ((this.z << this.D) ^ (this.v[this.I + 2] & 255)) & this.C;
                                    s5 = this.y[this.z] & 65535;
                                    this.x[this.I & this.u] = this.y[this.z];
                                    this.y[this.z] = (short) this.I;
                                }
                                this.L = this.F;
                                this.G = this.J;
                                this.F = 2;
                                if (s5 != 0 && this.L < this.N && ((this.I - s5) & 65535) <= this.s - 262) {
                                    if (this.P != 2) {
                                        this.F = c(s5);
                                    }
                                    if (this.F <= 5 && (this.P == 1 || (this.F == 3 && this.I - this.J > 4096))) {
                                        this.F = 2;
                                    }
                                }
                                if (this.L >= 3 && this.F <= this.L) {
                                    int i11 = (this.I + this.K) - 3;
                                    boolean b4 = b((this.I - 1) - this.G, this.L - 3);
                                    this.K -= this.L - 1;
                                    this.L -= 2;
                                    do {
                                        int i12 = this.I + 1;
                                        this.I = i12;
                                        if (i12 <= i11) {
                                            this.z = ((this.z << this.D) ^ (this.v[this.I + 2] & 255)) & this.C;
                                            s5 = this.y[this.z] & 65535;
                                            this.x[this.I & this.u] = this.y[this.z];
                                            this.y[this.z] = (short) this.I;
                                        }
                                        i3 = this.L - 1;
                                        this.L = i3;
                                    } while (i3 != 0);
                                    this.H = 0;
                                    this.F = 2;
                                    this.I++;
                                    if (b4) {
                                        a(false);
                                        if (this.n.g == 0) {
                                            c2 = 0;
                                            break;
                                        }
                                    } else {
                                        continue;
                                    }
                                } else if (this.H != 0) {
                                    if (b(0, this.v[this.I - 1] & 255)) {
                                        a(false);
                                    }
                                    this.I++;
                                    this.K--;
                                    if (this.n.g == 0) {
                                        c2 = 0;
                                        break;
                                    }
                                } else {
                                    this.H = 1;
                                    this.I++;
                                    this.K--;
                                }
                            }
                            break;
                    }
                    if (c2 == 2 || c2 == 3) {
                        this.o = 666;
                    }
                    if (c2 == 0 || c2 == 2) {
                        if (gVar.g != 0) {
                            return 0;
                        }
                        this.r = -1;
                        return 0;
                    } else if (c2 == 1) {
                        if (i2 == 1) {
                            a(2, 3);
                            a(256, c.a);
                            c();
                            if (((this.ad + 1) + 10) - this.af < 9) {
                                a(2, 3);
                                a(256, c.a);
                                c();
                            }
                            this.ad = 7;
                        } else {
                            a(0, 0, false);
                            if (i2 == 3) {
                                for (int i13 = 0; i13 < this.A; i13++) {
                                    this.y[i13] = 0;
                                }
                            }
                        }
                        gVar.a();
                        if (gVar.g == 0) {
                            this.r = -1;
                            return 0;
                        }
                    }
                }
                if (i2 != 4) {
                    return 0;
                }
                if (this.d != 0) {
                    return 1;
                }
                b((int) (gVar.l >>> 16));
                b((int) (gVar.l & 65535));
                gVar.a();
                this.d = -1;
                return this.c == 0 ? 1 : 0;
            }
            gVar.i = m[7];
            return -5;
        }
    }

    /* access modifiers changed from: package-private */
    public final int a(g gVar, int i2, int i3) {
        int i4;
        gVar.i = null;
        if (i2 == -1) {
            i2 = 6;
        }
        if (i3 < 0) {
            i3 = -i3;
            i4 = 1;
        } else {
            i4 = 0;
        }
        if (i3 < 9 || i3 > 15 || i2 < 0 || i2 > 9) {
            return -2;
        }
        gVar.j = this;
        this.d = i4;
        this.t = i3;
        this.s = 1 << this.t;
        this.u = this.s - 1;
        this.B = 15;
        this.A = 1 << this.B;
        this.C = this.A - 1;
        this.D = ((this.B + 3) - 1) / 3;
        this.v = new byte[(this.s * 2)];
        this.x = new short[this.s];
        this.y = new short[this.A];
        this.Z = 16384;
        this.a = new byte[(this.Z * 4)];
        this.p = this.Z * 4;
        this.ab = this.Z / 2;
        this.Y = this.Z * 3;
        this.O = i2;
        this.P = 0;
        gVar.h = 0;
        gVar.d = 0;
        gVar.i = null;
        this.c = 0;
        this.b = 0;
        if (this.d < 0) {
            this.d = 0;
        }
        this.o = this.d != 0 ? ParserConstants.LSHIFTX : 42;
        gVar.l = i.a(0, null, 0, 0);
        this.r = 0;
        this.V.h = this.S;
        this.V.j = c.c;
        this.W.h = this.T;
        this.W.j = c.d;
        this.X.h = this.U;
        this.X.j = c.e;
        this.ae = 0;
        this.af = 0;
        this.ad = 8;
        b();
        this.w = this.s * 2;
        this.y[this.A - 1] = 0;
        for (int i5 = 0; i5 < this.A - 1; i5++) {
            this.y[i5] = 0;
        }
        this.N = l[this.O].b;
        this.Q = l[this.O].a;
        this.R = l[this.O].c;
        this.M = l[this.O].d;
        this.I = 0;
        this.E = 0;
        this.K = 0;
        this.L = 2;
        this.F = 2;
        this.H = 0;
        this.z = 0;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final void a(short[] sArr, int i2) {
        int i3 = this.f[i2];
        int i4 = i2 << 1;
        while (i4 <= this.g) {
            int i5 = (i4 >= this.g || !a(sArr, this.f[i4 + 1], this.f[i4], this.i)) ? i4 : i4 + 1;
            if (a(sArr, i3, this.f[i5], this.i)) {
                break;
            }
            this.f[i2] = this.f[i5];
            i4 = i5 << 1;
            i2 = i5;
        }
        this.f[i2] = i3;
    }
}
