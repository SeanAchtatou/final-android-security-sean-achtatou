package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.b.a.a.h;
import com.agilebinary.b.a.a.q;
import com.agilebinary.b.a.a.u;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.g.f;

public abstract class g extends k {
    private static final String b = f.a();
    private u c = new q();
    private i d = new i(this);

    public g(c cVar, com.agilebinary.mobilemonitor.device.a.c.g gVar) {
        super(cVar, gVar);
    }

    public void b() {
        super.b();
        g();
    }

    public final synchronized f e() {
        f e;
        e = super.e();
        if (e.a != 1) {
            u f = f();
            h.a(f, this.d);
            if (this.c.b() == 0 && f.b() == 0) {
                e = new f(0, b + ": no current and reference APs");
            } else {
                c cVar = (c) f.a(0);
                int c2 = this.c.c(cVar);
                if (c2 == -1) {
                    e = new f(1, b + ": strongest AP not found in ref list ");
                } else {
                    int abs = Math.abs(((c) this.c.a(c2)).a - cVar.a);
                    e = abs >= this.a.t() ? new f(1, b + ": strongest AP rssi change= " + abs) : new f(2, b + ": strongest AP rssi change= " + abs);
                }
            }
        }
        if (e.a != 2) {
            g();
        }
        "" + e;
        return e;
    }

    public abstract u f();

    public final synchronized void g() {
        super.g();
        u f = f();
        h.a(f, this.d);
        this.c = f;
    }
}
