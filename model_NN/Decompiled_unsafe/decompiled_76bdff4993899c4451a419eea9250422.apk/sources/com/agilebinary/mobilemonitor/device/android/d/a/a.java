package com.agilebinary.mobilemonitor.device.android.d.a;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.a.e;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class a extends com.agilebinary.mobilemonitor.device.a.j.a.a {
    public static a a;
    private static final String e = f.a();
    private SQLiteDatabase f;
    private SQLiteOpenHelper g;
    private SQLiteStatement h;
    private SQLiteStatement i;
    private SQLiteStatement j;
    private SQLiteStatement k;
    private SQLiteStatement l;
    private SQLiteStatement m;
    private SQLiteStatement n;
    private SQLiteStatement o;
    private SQLiteStatement p;
    private SQLiteStatement q;
    private long r;
    private SQLiteStatement s;

    public a(Context context, g gVar, c cVar, b bVar) {
        super(gVar, cVar, bVar, new e(context));
        this.g = new c(this, context, "db", null, 1);
        a = this;
    }

    public final synchronized void a(int i2) {
        try {
            this.n.bindLong(1, (long) i2);
            this.n.execute();
            this.r = this.s.simpleQueryForLong();
        } catch (SQLiteException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.j.a(e2);
        }
    }

    public final synchronized void a(long j2) {
        try {
            this.h.bindLong(1, j2);
            this.h.execute();
            this.r = this.s.simpleQueryForLong();
        } catch (SQLException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.j.a(e2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x006f A[SYNTHETIC, Splitter:B:22:0x006f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.agilebinary.mobilemonitor.device.a.b.b r10) {
        /*
            r9 = this;
            r8 = 0
            monitor-enter(r9)
            android.database.sqlite.SQLiteDatabase r0 = r9.f     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            java.lang.String r1 = com.agilebinary.mobilemonitor.device.android.d.a.b.a     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.device.android.d.a.b.j     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.device.android.d.a.b.b     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            java.lang.String r4 = "="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            long r4 = r10.g()     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0079, all -> 0x0076 }
            boolean r0 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0065 }
            if (r0 == 0) goto L_0x0042
            r0 = 0
            byte[] r0 = r1.getBlob(r0)     // Catch:{ SQLiteException -> 0x0065 }
            r10.a(r0)     // Catch:{ SQLiteException -> 0x0065 }
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ all -> 0x0073 }
        L_0x0040:
            monitor-exit(r9)
            return
        L_0x0042:
            com.agilebinary.mobilemonitor.device.a.j.a r0 = new com.agilebinary.mobilemonitor.device.a.j.a     // Catch:{ SQLiteException -> 0x0065 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0065 }
            r2.<init>()     // Catch:{ SQLiteException -> 0x0065 }
            java.lang.String r3 = "record with "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLiteException -> 0x0065 }
            long r3 = r10.g()     // Catch:{ SQLiteException -> 0x0065 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLiteException -> 0x0065 }
            java.lang.String r3 = " not found!"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLiteException -> 0x0065 }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLiteException -> 0x0065 }
            r0.<init>(r2)     // Catch:{ SQLiteException -> 0x0065 }
            throw r0     // Catch:{ SQLiteException -> 0x0065 }
        L_0x0065:
            r0 = move-exception
        L_0x0066:
            com.agilebinary.mobilemonitor.device.a.j.a r2 = new com.agilebinary.mobilemonitor.device.a.j.a     // Catch:{ all -> 0x006c }
            r2.<init>(r0)     // Catch:{ all -> 0x006c }
            throw r2     // Catch:{ all -> 0x006c }
        L_0x006c:
            r0 = move-exception
        L_0x006d:
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ all -> 0x0073 }
        L_0x0072:
            throw r0     // Catch:{ all -> 0x0073 }
        L_0x0073:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x0076:
            r0 = move-exception
            r1 = r8
            goto L_0x006d
        L_0x0079:
            r0 = move-exception
            r1 = r8
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.d.a.a.a(com.agilebinary.mobilemonitor.device.a.b.b):void");
    }

    public final synchronized long b(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        long executeInsert;
        int k2 = this.c.k() * 1024;
        if (k2 <= 0 || this.r + ((long) bVar.k()) <= ((long) k2)) {
            try {
                this.j.bindLong(1, bVar.j());
                this.j.bindLong(2, (long) bVar.h());
                this.j.bindLong(3, (long) bVar.i());
                this.j.bindLong(4, (long) bVar.f());
                this.j.bindBlob(5, bVar.e());
                this.j.bindLong(6, (long) bVar.k());
                executeInsert = this.j.executeInsert();
                this.r += (long) bVar.k();
            } catch (SQLiteFullException e2) {
                throw new com.agilebinary.mobilemonitor.device.a.j.b(e2);
            } catch (SQLiteException e3) {
                throw new com.agilebinary.mobilemonitor.device.a.j.a(e3);
            }
        } else {
            throw new com.agilebinary.mobilemonitor.device.a.j.b("saving event would exceed max allowed db size, currentsize=" + this.r + " eventsize=" + bVar.k() + " maxSize=" + k2);
        }
        return executeInsert;
    }

    public final synchronized void b(long j2) {
        try {
            this.i.bindLong(1, j2);
            this.i.execute();
        } catch (SQLiteFullException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.j.b(e2);
        } catch (SQLiteException e3) {
            throw new com.agilebinary.mobilemonitor.device.a.j.a(e3);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x004e A[SYNTHETIC, Splitter:B:25:0x004e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int c(long r10) {
        /*
            r9 = this;
            r8 = 0
            monitor-enter(r9)
            android.database.sqlite.SQLiteDatabase r0 = r9.f     // Catch:{ SQLiteException -> 0x0042, all -> 0x0055 }
            java.lang.String r1 = com.agilebinary.mobilemonitor.device.android.d.a.b.a     // Catch:{ SQLiteException -> 0x0042, all -> 0x0055 }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.device.android.d.a.b.k     // Catch:{ SQLiteException -> 0x0042, all -> 0x0055 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0042, all -> 0x0055 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0042, all -> 0x0055 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.device.android.d.a.b.b     // Catch:{ SQLiteException -> 0x0042, all -> 0x0055 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0042, all -> 0x0055 }
            java.lang.String r4 = "="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0042, all -> 0x0055 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ SQLiteException -> 0x0042, all -> 0x0055 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0042, all -> 0x0055 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0042, all -> 0x0055 }
            boolean r0 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0057 }
            if (r0 == 0) goto L_0x003b
            r0 = 0
            int r0 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x0057 }
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0039:
            monitor-exit(r9)
            return r0
        L_0x003b:
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0040:
            r0 = -1
            goto L_0x0039
        L_0x0042:
            r0 = move-exception
            r1 = r8
        L_0x0044:
            com.agilebinary.mobilemonitor.device.a.j.a r2 = new com.agilebinary.mobilemonitor.device.a.j.a     // Catch:{ all -> 0x004a }
            r2.<init>(r0)     // Catch:{ all -> 0x004a }
            throw r2     // Catch:{ all -> 0x004a }
        L_0x004a:
            r0 = move-exception
            r8 = r1
        L_0x004c:
            if (r8 == 0) goto L_0x0051
            r8.close()     // Catch:{ all -> 0x0052 }
        L_0x0051:
            throw r0     // Catch:{ all -> 0x0052 }
        L_0x0052:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x0055:
            r0 = move-exception
            goto L_0x004c
        L_0x0057:
            r0 = move-exception
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.d.a.a.c(long):int");
    }

    public final synchronized void c(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        try {
            this.o.bindLong(1, (long) bVar.f());
            this.o.bindLong(2, bVar.j());
            this.o.executeInsert();
        } catch (SQLiteFullException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.j.b(e2);
        } catch (SQLiteException e3) {
            throw new com.agilebinary.mobilemonitor.device.a.j.a(e3);
        }
    }

    public final synchronized void e() {
    }

    public final synchronized void f() {
        try {
            this.f = this.g.getWritableDatabase();
            this.h = this.f.compileStatement(String.format("DELETE FROM %1$s where %2$s=? ", b.a, b.b));
            this.q = this.f.compileStatement(String.format("DELETE FROM %1$s ", b.a));
            this.i = this.f.compileStatement(String.format("UPDATE %1$s set %2$s=%2$s+1 where %3$s=? ", b.a, b.d, b.b));
            this.j = this.f.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s) values(?,?,?,?,?,?) ", b.a, b.c, b.d, b.e, b.f, b.g, b.h));
            this.k = this.f.compileStatement(String.format("DELETE FROM %1$s where %2$s=? and %3$s<=? ", b.a, b.e, b.c));
            this.l = this.f.compileStatement(String.format("SELECT MIN(%2$s) from %1$s ", b.a, b.c));
            this.m = this.f.compileStatement(String.format("DELETE from %1$s where %2$s=?", b.a, b.c));
            this.n = this.f.compileStatement(String.format("DELETE from %1$s where %2$s=?", b.a, b.e));
            this.o = this.f.compileStatement(String.format("UPDATE %1$s set %2$s=? where %3$s=? ", b.a, b.f, b.b));
            this.s = this.f.compileStatement(String.format("SELECT SUM(%2$s) FROM %1$s ", b.a, b.h));
            this.p = this.f.compileStatement(String.format("SELECT count(*) FROM %1$s ", b.a));
            this.r = this.s.simpleQueryForLong();
        } catch (SQLException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.j.a(e2);
        }
    }

    public final synchronized void g() {
        try {
            this.f.close();
        } catch (SQLException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.j.a(e2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0070 A[SYNTHETIC, Splitter:B:31:0x0070] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.agilebinary.mobilemonitor.device.a.b.b[] h() {
        /*
            r12 = this;
            r11 = 0
            r8 = 0
            monitor-enter(r12)
            android.database.sqlite.SQLiteDatabase r0 = r12.f     // Catch:{ SQLException -> 0x0064, all -> 0x0077 }
            java.lang.String r1 = com.agilebinary.mobilemonitor.device.android.d.a.b.a     // Catch:{ SQLException -> 0x0064, all -> 0x0077 }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.device.android.d.a.b.i     // Catch:{ SQLException -> 0x0064, all -> 0x0077 }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = com.agilebinary.mobilemonitor.device.android.d.a.b.c     // Catch:{ SQLException -> 0x0064, all -> 0x0077 }
            android.database.Cursor r10 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLException -> 0x0064, all -> 0x0077 }
            boolean r0 = r10.moveToLast()     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            if (r0 != 0) goto L_0x0022
            com.agilebinary.mobilemonitor.device.a.b.b[] r0 = com.agilebinary.mobilemonitor.device.android.d.a.a.b     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            if (r10 == 0) goto L_0x0020
            r10.close()     // Catch:{ all -> 0x0074 }
        L_0x0020:
            monitor-exit(r12)
            return r0
        L_0x0022:
            int r0 = r10.getPosition()     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            int r0 = r0 + 1
            com.agilebinary.mobilemonitor.device.a.b.b[] r9 = new com.agilebinary.mobilemonitor.device.a.b.b[r0]     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            r0 = -1
            r10.moveToPosition(r0)     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
        L_0x002e:
            boolean r0 = r10.moveToNext()     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            if (r0 == 0) goto L_0x005d
            com.agilebinary.mobilemonitor.device.a.b.b r0 = new com.agilebinary.mobilemonitor.device.a.b.b     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            r1 = 0
            long r1 = r10.getLong(r1)     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            r3 = 3
            int r3 = r10.getInt(r3)     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            r4 = 2
            int r4 = r10.getInt(r4)     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            r5 = 4
            int r5 = r10.getInt(r5)     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            r6 = 1
            long r6 = r10.getLong(r6)     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            r8 = 5
            int r8 = r10.getInt(r8)     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            r0.<init>(r1, r3, r4, r5, r6, r8)     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            r9[r11] = r0     // Catch:{ SQLException -> 0x007c, all -> 0x007a }
            int r0 = r11 + 1
            r11 = r0
            goto L_0x002e
        L_0x005d:
            if (r10 == 0) goto L_0x0062
            r10.close()     // Catch:{ all -> 0x0074 }
        L_0x0062:
            r0 = r9
            goto L_0x0020
        L_0x0064:
            r0 = move-exception
            r1 = r8
        L_0x0066:
            com.agilebinary.mobilemonitor.device.a.j.a r2 = new com.agilebinary.mobilemonitor.device.a.j.a     // Catch:{ all -> 0x006c }
            r2.<init>(r0)     // Catch:{ all -> 0x006c }
            throw r2     // Catch:{ all -> 0x006c }
        L_0x006c:
            r0 = move-exception
            r10 = r1
        L_0x006e:
            if (r10 == 0) goto L_0x0073
            r10.close()     // Catch:{ all -> 0x0074 }
        L_0x0073:
            throw r0     // Catch:{ all -> 0x0074 }
        L_0x0074:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        L_0x0077:
            r0 = move-exception
            r10 = r8
            goto L_0x006e
        L_0x007a:
            r0 = move-exception
            goto L_0x006e
        L_0x007c:
            r0 = move-exception
            r1 = r10
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.d.a.a.h():com.agilebinary.mobilemonitor.device.a.b.b[]");
    }

    public final synchronized boolean i() {
        boolean z = false;
        synchronized (this) {
            try {
                if (this.p.simpleQueryForLong() != 0) {
                    try {
                        this.m.bindLong(1, this.l.simpleQueryForLong());
                        this.m.execute();
                        this.r = this.s.simpleQueryForLong();
                        z = true;
                    } catch (SQLException e2) {
                        throw new com.agilebinary.mobilemonitor.device.a.j.a(e2);
                    }
                }
            } catch (SQLiteDoneException e3) {
            }
        }
        return z;
    }

    public final synchronized void j() {
        try {
            byte[] bArr = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a;
            for (byte b : bArr) {
                this.k.bindLong(1, (long) b);
                this.k.bindLong(2, System.currentTimeMillis() - ((long) (this.c.j(b) * 86400000)));
                this.k.execute();
            }
            this.r = this.s.simpleQueryForLong();
        } catch (SQLiteException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.j.a(e2);
        }
    }

    public final synchronized void k() {
        try {
            this.q.execute();
            this.r = 0;
        } catch (SQLiteException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.j.a(e2);
        }
    }

    public final long l() {
        return this.p.simpleQueryForLong();
    }
}
