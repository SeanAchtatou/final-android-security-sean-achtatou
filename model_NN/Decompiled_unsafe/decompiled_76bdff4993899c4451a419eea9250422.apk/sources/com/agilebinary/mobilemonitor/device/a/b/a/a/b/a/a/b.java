package com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a;

import bsh.ParserConstants;

final class b {
    private static final int[] g = {0, 1, 3, 7, 15, 31, 63, ParserConstants.MODASSIGN, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767, 65535};
    private static int[] h = {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};
    int a;
    int b;
    byte[] c;
    int d;
    int e;
    int f;
    private int i;
    private int j;
    private int k;
    private int l;
    private int[] m;
    private int[] n = new int[1];
    private int[] o = new int[1];
    private a p = new a();
    private int q;
    private int[] r = new int[4320];
    private Object s;
    private long t;
    private d u = new d();

    b(g gVar, Object obj, int i2) {
        this.c = new byte[i2];
        this.d = i2;
        this.s = obj;
        this.i = 0;
        a(gVar, (long[]) null);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x0781, code lost:
        r0.f = r4;
        r3 = b(r19, r20);
        r13 = r0.f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x0795, code lost:
        if (r0.e == r0.f) goto L_0x07c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x0797, code lost:
        r0.b = r8;
        r0.a = r7;
        r19.c = r5;
        r19.d += (long) (r6 - r19.b);
        r19.b = r6;
        r0.f = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x07c5, code lost:
        r0.i = 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x07cb, code lost:
        r0.b = r8;
        r0.a = r7;
        r19.c = r5;
        r19.d += (long) (r6 - r19.b);
        r19.b = r6;
        r0.f = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:?, code lost:
        return b(r19, -3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:?, code lost:
        return b(r19, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:?, code lost:
        return b(r19, 1);
     */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x04de  */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x043d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x0660 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x03cc  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.g r19, int r20) {
        /*
            r18 = this;
            r0 = r19
            int r6 = r0.b
            r0 = r19
            int r5 = r0.c
            r0 = r18
            int r8 = r0.b
            r0 = r18
            int r7 = r0.a
            r0 = r18
            int r4 = r0.f
            r0 = r18
            int r3 = r0.e
            if (r4 >= r3) goto L_0x005a
            r0 = r18
            int r3 = r0.e
            int r3 = r3 - r4
            int r3 = r3 + -1
        L_0x0021:
            r10 = r3
            r13 = r4
            r3 = r5
            r4 = r6
        L_0x0025:
            r0 = r18
            int r5 = r0.i
            switch(r5) {
                case 0: goto L_0x0851;
                case 1: goto L_0x014b;
                case 2: goto L_0x01fa;
                case 3: goto L_0x02fd;
                case 4: goto L_0x03ba;
                case 5: goto L_0x0838;
                case 6: goto L_0x0840;
                case 7: goto L_0x0848;
                case 8: goto L_0x084d;
                case 9: goto L_0x07fa;
                default: goto L_0x002c;
            }
        L_0x002c:
            r0 = r18
            r0.b = r8
            r0 = r18
            r0.a = r7
            r0 = r19
            r0.c = r3
            r0 = r19
            long r5 = r0.d
            r0 = r19
            int r3 = r0.b
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r0 = r19
            r0.d = r5
            r0 = r19
            r0.b = r4
            r0 = r18
            r0.f = r13
            r3 = -2
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r3)
        L_0x0059:
            return r3
        L_0x005a:
            r0 = r18
            int r3 = r0.d
            int r3 = r3 - r4
            goto L_0x0021
        L_0x0060:
            r3 = 3
            if (r14 >= r3) goto L_0x00a2
            if (r11 == 0) goto L_0x0079
            r20 = 0
            int r11 = r11 + -1
            r0 = r19
            byte[] r3 = r0.a
            int r4 = r12 + 1
            byte r3 = r3[r12]
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r3 = r3 << r14
            r15 = r15 | r3
            int r14 = r14 + 8
            r12 = r4
            goto L_0x0060
        L_0x0079:
            r0 = r18
            r0.b = r15
            r0 = r18
            r0.a = r14
            r0 = r19
            r0.c = r11
            r0 = r19
            long r3 = r0.d
            r0 = r19
            int r5 = r0.b
            int r5 = r12 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r0 = r19
            r0.d = r3
            r0 = r19
            r0.b = r12
            r0 = r18
            r0.f = r13
            int r3 = r18.b(r19, r20)
            goto L_0x0059
        L_0x00a2:
            r3 = r15 & 7
            r4 = r3 & 1
            r0 = r18
            r0.q = r4
            int r3 = r3 >>> 1
            switch(r3) {
                case 0: goto L_0x00b5;
                case 1: goto L_0x00ca;
                case 2: goto L_0x00fd;
                case 3: goto L_0x010c;
                default: goto L_0x00af;
            }
        L_0x00af:
            r3 = r11
            r4 = r12
            r7 = r14
            r8 = r15
            goto L_0x0025
        L_0x00b5:
            int r3 = r15 >>> 3
            int r4 = r14 + -3
            r6 = r4 & 7
            int r5 = r3 >>> r6
            int r3 = r4 - r6
            r4 = 1
            r0 = r18
            r0.i = r4
            r4 = r12
            r7 = r3
            r8 = r5
            r3 = r11
            goto L_0x0025
        L_0x00ca:
            r3 = 1
            int[] r4 = new int[r3]
            r3 = 1
            int[] r5 = new int[r3]
            r3 = 1
            int[][] r6 = new int[r3][]
            r3 = 1
            int[][] r8 = new int[r3][]
            com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.d.a(r4, r5, r6, r8)
            r0 = r18
            com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a r3 = r0.p
            r7 = 0
            r4 = r4[r7]
            r7 = 0
            r5 = r5[r7]
            r7 = 0
            r6 = r6[r7]
            r7 = 0
            r9 = 0
            r8 = r8[r9]
            r9 = 0
            r3.a(r4, r5, r6, r7, r8, r9)
            int r5 = r15 >>> 3
            int r3 = r14 + -3
            r4 = 6
            r0 = r18
            r0.i = r4
            r4 = r12
            r7 = r3
            r8 = r5
            r3 = r11
            goto L_0x0025
        L_0x00fd:
            int r5 = r15 >>> 3
            int r3 = r14 + -3
            r4 = 3
            r0 = r18
            r0.i = r4
            r4 = r12
            r7 = r3
            r8 = r5
            r3 = r11
            goto L_0x0025
        L_0x010c:
            int r3 = r15 >>> 3
            int r4 = r14 + -3
            r5 = 9
            r0 = r18
            r0.i = r5
            java.lang.String r5 = "invalid block type"
            r0 = r19
            r0.i = r5
            r0 = r18
            r0.b = r3
            r0 = r18
            r0.a = r4
            r0 = r19
            r0.c = r11
            r0 = r19
            long r3 = r0.d
            r0 = r19
            int r5 = r0.b
            int r5 = r12 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r0 = r19
            r0.d = r3
            r0 = r19
            r0.b = r12
            r0 = r18
            r0.f = r13
            r3 = -3
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r3)
            goto L_0x0059
        L_0x014b:
            r5 = 32
            if (r7 >= r5) goto L_0x018f
            if (r3 == 0) goto L_0x0165
            r20 = 0
            int r3 = r3 + -1
            r0 = r19
            byte[] r6 = r0.a
            int r5 = r4 + 1
            byte r4 = r6[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r7
            r8 = r8 | r4
            int r7 = r7 + 8
            r4 = r5
            goto L_0x014b
        L_0x0165:
            r0 = r18
            r0.b = r8
            r0 = r18
            r0.a = r7
            r0 = r19
            r0.c = r3
            r0 = r19
            long r5 = r0.d
            r0 = r19
            int r3 = r0.b
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r0 = r19
            r0.d = r5
            r0 = r19
            r0.b = r4
            r0 = r18
            r0.f = r13
            int r3 = r18.b(r19, r20)
            goto L_0x0059
        L_0x018f:
            r5 = r8 ^ -1
            int r5 = r5 >>> 16
            r6 = 65535(0xffff, float:9.1834E-41)
            r5 = r5 & r6
            r6 = 65535(0xffff, float:9.1834E-41)
            r6 = r6 & r8
            if (r5 == r6) goto L_0x01d8
            r5 = 9
            r0 = r18
            r0.i = r5
            java.lang.String r5 = "invalid stored block lengths"
            r0 = r19
            r0.i = r5
            r0 = r18
            r0.b = r8
            r0 = r18
            r0.a = r7
            r0 = r19
            r0.c = r3
            r0 = r19
            long r5 = r0.d
            r0 = r19
            int r3 = r0.b
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r0 = r19
            r0.d = r5
            r0 = r19
            r0.b = r4
            r0 = r18
            r0.f = r13
            r3 = -3
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r3)
            goto L_0x0059
        L_0x01d8:
            r5 = 65535(0xffff, float:9.1834E-41)
            r5 = r5 & r8
            r0 = r18
            r0.j = r5
            r5 = 0
            r0 = r18
            int r6 = r0.j
            if (r6 == 0) goto L_0x01f0
            r6 = 2
        L_0x01e8:
            r0 = r18
            r0.i = r6
            r7 = r5
            r8 = r5
            goto L_0x0025
        L_0x01f0:
            r0 = r18
            int r6 = r0.q
            if (r6 == 0) goto L_0x01f8
            r6 = 7
            goto L_0x01e8
        L_0x01f8:
            r6 = 0
            goto L_0x01e8
        L_0x01fa:
            if (r3 != 0) goto L_0x0226
            r0 = r18
            r0.b = r8
            r0 = r18
            r0.a = r7
            r0 = r19
            r0.c = r3
            r0 = r19
            long r5 = r0.d
            r0 = r19
            int r3 = r0.b
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r0 = r19
            r0.d = r5
            r0 = r19
            r0.b = r4
            r0 = r18
            r0.f = r13
            int r3 = r18.b(r19, r20)
            goto L_0x0059
        L_0x0226:
            if (r10 != 0) goto L_0x02bf
            r0 = r18
            int r5 = r0.d
            if (r13 != r5) goto L_0x0244
            r0 = r18
            int r5 = r0.e
            if (r5 == 0) goto L_0x0244
            r13 = 0
            r0 = r18
            int r5 = r0.e
            if (r5 <= 0) goto L_0x02ab
            r0 = r18
            int r5 = r0.e
            int r5 = r5 + 0
            int r5 = r5 + -1
        L_0x0243:
            r10 = r5
        L_0x0244:
            if (r10 != 0) goto L_0x02bf
            r0 = r18
            r0.f = r13
            int r6 = r18.b(r19, r20)
            r0 = r18
            int r13 = r0.f
            r0 = r18
            int r5 = r0.e
            if (r13 >= r5) goto L_0x02b2
            r0 = r18
            int r5 = r0.e
            int r5 = r5 - r13
            int r5 = r5 + -1
        L_0x025f:
            r0 = r18
            int r9 = r0.d
            if (r13 != r9) goto L_0x0835
            r0 = r18
            int r9 = r0.e
            if (r9 == 0) goto L_0x0835
            r13 = 0
            r0 = r18
            int r5 = r0.e
            if (r5 <= 0) goto L_0x02b8
            r0 = r18
            int r5 = r0.e
            int r5 = r5 + 0
            int r5 = r5 + -1
        L_0x027a:
            r10 = r5
        L_0x027b:
            if (r10 != 0) goto L_0x02bf
            r0 = r18
            r0.b = r8
            r0 = r18
            r0.a = r7
            r0 = r19
            r0.c = r3
            r0 = r19
            long r7 = r0.d
            r0 = r19
            int r3 = r0.b
            int r3 = r4 - r3
            long r9 = (long) r3
            long r7 = r7 + r9
            r0 = r19
            r0.d = r7
            r0 = r19
            r0.b = r4
            r0 = r18
            r0.f = r13
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r6)
            goto L_0x0059
        L_0x02ab:
            r0 = r18
            int r5 = r0.d
            int r5 = r5 + 0
            goto L_0x0243
        L_0x02b2:
            r0 = r18
            int r5 = r0.d
            int r5 = r5 - r13
            goto L_0x025f
        L_0x02b8:
            r0 = r18
            int r5 = r0.d
            int r5 = r5 + 0
            goto L_0x027a
        L_0x02bf:
            r20 = 0
            r0 = r18
            int r5 = r0.j
            if (r5 <= r3) goto L_0x02c8
            r5 = r3
        L_0x02c8:
            if (r5 <= r10) goto L_0x0832
            r9 = r10
        L_0x02cb:
            r0 = r19
            byte[] r5 = r0.a
            r0 = r18
            byte[] r6 = r0.c
            java.lang.System.arraycopy(r5, r4, r6, r13, r9)
            int r6 = r4 + r9
            int r5 = r3 - r9
            int r4 = r13 + r9
            int r3 = r10 - r9
            r0 = r18
            int r10 = r0.j
            int r9 = r10 - r9
            r0 = r18
            r0.j = r9
            if (r9 != 0) goto L_0x082c
            r0 = r18
            int r9 = r0.q
            if (r9 == 0) goto L_0x02fb
            r9 = 7
        L_0x02f1:
            r0 = r18
            r0.i = r9
            r10 = r3
            r13 = r4
            r3 = r5
            r4 = r6
            goto L_0x0025
        L_0x02fb:
            r9 = 0
            goto L_0x02f1
        L_0x02fd:
            r5 = 14
            if (r7 >= r5) goto L_0x0341
            if (r3 == 0) goto L_0x0317
            r20 = 0
            int r3 = r3 + -1
            r0 = r19
            byte[] r6 = r0.a
            int r5 = r4 + 1
            byte r4 = r6[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r7
            r8 = r8 | r4
            int r7 = r7 + 8
            r4 = r5
            goto L_0x02fd
        L_0x0317:
            r0 = r18
            r0.b = r8
            r0 = r18
            r0.a = r7
            r0 = r19
            r0.c = r3
            r0 = r19
            long r5 = r0.d
            r0 = r19
            int r3 = r0.b
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r0 = r19
            r0.d = r5
            r0 = r19
            r0.b = r4
            r0 = r18
            r0.f = r13
            int r3 = r18.b(r19, r20)
            goto L_0x0059
        L_0x0341:
            r5 = r8 & 16383(0x3fff, float:2.2957E-41)
            r0 = r18
            r0.k = r5
            r6 = r5 & 31
            r9 = 29
            if (r6 > r9) goto L_0x0355
            int r6 = r5 >> 5
            r6 = r6 & 31
            r9 = 29
            if (r6 <= r9) goto L_0x0390
        L_0x0355:
            r5 = 9
            r0 = r18
            r0.i = r5
            java.lang.String r5 = "too many length or distance symbols"
            r0 = r19
            r0.i = r5
            r0 = r18
            r0.b = r8
            r0 = r18
            r0.a = r7
            r0 = r19
            r0.c = r3
            r0 = r19
            long r5 = r0.d
            r0 = r19
            int r3 = r0.b
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r0 = r19
            r0.d = r5
            r0 = r19
            r0.b = r4
            r0 = r18
            r0.f = r13
            r3 = -3
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r3)
            goto L_0x0059
        L_0x0390:
            r6 = r5 & 31
            int r6 = r6 + 258
            int r5 = r5 >> 5
            r5 = r5 & 31
            int r6 = r6 + r5
            r0 = r18
            int[] r5 = r0.m
            if (r5 == 0) goto L_0x03a6
            r0 = r18
            int[] r5 = r0.m
            int r5 = r5.length
            if (r5 >= r6) goto L_0x03e7
        L_0x03a6:
            int[] r5 = new int[r6]
            r0 = r18
            r0.m = r5
        L_0x03ac:
            int r8 = r8 >>> 14
            int r7 = r7 + -14
            r5 = 0
            r0 = r18
            r0.l = r5
            r5 = 4
            r0 = r18
            r0.i = r5
        L_0x03ba:
            r9 = r3
            r10 = r4
            r11 = r7
            r12 = r8
        L_0x03be:
            r0 = r18
            int r3 = r0.l
            r0 = r18
            int r4 = r0.k
            int r4 = r4 >>> 10
            int r4 = r4 + 4
            if (r3 >= r4) goto L_0x043d
            r3 = r9
            r4 = r10
        L_0x03ce:
            r5 = 3
            if (r11 >= r5) goto L_0x041e
            if (r3 == 0) goto L_0x03f4
            r20 = 0
            int r3 = r3 + -1
            r0 = r19
            byte[] r5 = r0.a
            int r10 = r4 + 1
            byte r4 = r5[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r11
            r12 = r12 | r4
            int r11 = r11 + 8
            r4 = r10
            goto L_0x03ce
        L_0x03e7:
            r5 = 0
        L_0x03e8:
            if (r5 >= r6) goto L_0x03ac
            r0 = r18
            int[] r9 = r0.m
            r10 = 0
            r9[r5] = r10
            int r5 = r5 + 1
            goto L_0x03e8
        L_0x03f4:
            r0 = r18
            r0.b = r12
            r0 = r18
            r0.a = r11
            r0 = r19
            r0.c = r3
            r0 = r19
            long r5 = r0.d
            r0 = r19
            int r3 = r0.b
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r0 = r19
            r0.d = r5
            r0 = r19
            r0.b = r4
            r0 = r18
            r0.f = r13
            int r3 = r18.b(r19, r20)
            goto L_0x0059
        L_0x041e:
            r0 = r18
            int[] r5 = r0.m
            int[] r6 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.b.h
            r0 = r18
            int r7 = r0.l
            int r8 = r7 + 1
            r0 = r18
            r0.l = r8
            r6 = r6[r7]
            r7 = r12 & 7
            r5[r6] = r7
            int r8 = r12 >>> 3
            int r7 = r11 + -3
            r9 = r3
            r10 = r4
            r11 = r7
            r12 = r8
            goto L_0x03be
        L_0x043d:
            r0 = r18
            int r3 = r0.l
            r4 = 19
            if (r3 >= r4) goto L_0x045b
            r0 = r18
            int[] r3 = r0.m
            int[] r4 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.b.h
            r0 = r18
            int r5 = r0.l
            int r6 = r5 + 1
            r0 = r18
            r0.l = r6
            r4 = r4[r5]
            r5 = 0
            r3[r4] = r5
            goto L_0x043d
        L_0x045b:
            r0 = r18
            int[] r3 = r0.n
            r4 = 0
            r5 = 7
            r3[r4] = r5
            r0 = r18
            com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.d r3 = r0.u
            r0 = r18
            int[] r4 = r0.m
            r0 = r18
            int[] r5 = r0.n
            r0 = r18
            int[] r6 = r0.o
            r0 = r18
            int[] r7 = r0.r
            r8 = r19
            int r3 = r3.a(r4, r5, r6, r7, r8)
            if (r3 == 0) goto L_0x04bb
            r4 = -3
            if (r3 != r4) goto L_0x048d
            r4 = 0
            r0 = r18
            r0.m = r4
            r4 = 9
            r0 = r18
            r0.i = r4
        L_0x048d:
            r0 = r18
            r0.b = r12
            r0 = r18
            r0.a = r11
            r0 = r19
            r0.c = r9
            r0 = r19
            long r4 = r0.d
            r0 = r19
            int r6 = r0.b
            int r6 = r10 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r19
            r0.d = r4
            r0 = r19
            r0.b = r10
            r0 = r18
            r0.f = r13
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r3)
            goto L_0x0059
        L_0x04bb:
            r3 = 0
            r0 = r18
            r0.l = r3
            r3 = 5
            r0 = r18
            r0.i = r3
            r14 = r9
            r15 = r10
            r16 = r11
            r17 = r12
        L_0x04cb:
            r0 = r18
            int r3 = r0.k
            r0 = r18
            int r4 = r0.l
            r5 = r3 & 31
            int r5 = r5 + 258
            int r3 = r3 >> 5
            r3 = r3 & 31
            int r3 = r3 + r5
            if (r4 >= r3) goto L_0x0660
            r0 = r18
            int[] r3 = r0.n
            r4 = 0
            r3 = r3[r4]
            r9 = r14
            r10 = r15
            r4 = r16
            r5 = r17
        L_0x04eb:
            if (r4 >= r3) goto L_0x052d
            if (r9 == 0) goto L_0x0503
            r20 = 0
            int r9 = r9 + -1
            r0 = r19
            byte[] r6 = r0.a
            int r15 = r10 + 1
            byte r6 = r6[r10]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << r4
            r5 = r5 | r6
            int r4 = r4 + 8
            r10 = r15
            goto L_0x04eb
        L_0x0503:
            r0 = r18
            r0.b = r5
            r0 = r18
            r0.a = r4
            r0 = r19
            r0.c = r9
            r0 = r19
            long r3 = r0.d
            r0 = r19
            int r5 = r0.b
            int r5 = r10 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r0 = r19
            r0.d = r3
            r0 = r19
            r0.b = r10
            r0 = r18
            r0.f = r13
            int r3 = r18.b(r19, r20)
            goto L_0x0059
        L_0x052d:
            r0 = r18
            int[] r6 = r0.r
            r0 = r18
            int[] r7 = r0.o
            r8 = 0
            r7 = r7[r8]
            int[] r8 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.b.g
            r3 = r8[r3]
            r3 = r3 & r5
            int r3 = r3 + r7
            int r3 = r3 * 3
            int r3 = r3 + 1
            r8 = r6[r3]
            r0 = r18
            int[] r3 = r0.r
            r0 = r18
            int[] r6 = r0.o
            r7 = 0
            r6 = r6[r7]
            int[] r7 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.b.g
            r7 = r7[r8]
            r7 = r7 & r5
            int r6 = r6 + r7
            int r6 = r6 * 3
            int r6 = r6 + 2
            r14 = r3[r6]
            r3 = 16
            if (r14 >= r3) goto L_0x057b
            int r12 = r5 >>> r8
            int r11 = r4 - r8
            r0 = r18
            int[] r3 = r0.m
            r0 = r18
            int r4 = r0.l
            int r5 = r4 + 1
            r0 = r18
            r0.l = r5
            r3[r4] = r14
            r14 = r9
            r15 = r10
            r16 = r11
            r17 = r12
            goto L_0x04cb
        L_0x057b:
            r3 = 18
            if (r14 != r3) goto L_0x05a3
            r3 = 7
            r7 = r3
        L_0x0581:
            r3 = 18
            if (r14 != r3) goto L_0x05a7
            r3 = 11
        L_0x0587:
            r6 = r5
            r5 = r4
        L_0x0589:
            int r4 = r8 + r7
            if (r5 >= r4) goto L_0x05d3
            if (r9 == 0) goto L_0x05a9
            r20 = 0
            int r9 = r9 + -1
            r0 = r19
            byte[] r11 = r0.a
            int r4 = r10 + 1
            byte r10 = r11[r10]
            r10 = r10 & 255(0xff, float:3.57E-43)
            int r10 = r10 << r5
            r6 = r6 | r10
            int r5 = r5 + 8
            r10 = r4
            goto L_0x0589
        L_0x05a3:
            int r3 = r14 + -14
            r7 = r3
            goto L_0x0581
        L_0x05a7:
            r3 = 3
            goto L_0x0587
        L_0x05a9:
            r0 = r18
            r0.b = r6
            r0 = r18
            r0.a = r5
            r0 = r19
            r0.c = r9
            r0 = r19
            long r3 = r0.d
            r0 = r19
            int r5 = r0.b
            int r5 = r10 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r0 = r19
            r0.d = r3
            r0 = r19
            r0.b = r10
            r0 = r18
            r0.f = r13
            int r3 = r18.b(r19, r20)
            goto L_0x0059
        L_0x05d3:
            int r6 = r6 >>> r8
            int r5 = r5 - r8
            int[] r4 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.b.g
            r4 = r4[r7]
            r4 = r4 & r6
            int r4 = r4 + r3
            int r12 = r6 >>> r7
            int r11 = r5 - r7
            r0 = r18
            int r5 = r0.l
            r0 = r18
            int r3 = r0.k
            int r6 = r5 + r4
            r7 = r3 & 31
            int r7 = r7 + 258
            int r3 = r3 >> 5
            r3 = r3 & 31
            int r3 = r3 + r7
            if (r6 > r3) goto L_0x05fa
            r3 = 16
            if (r14 != r3) goto L_0x063a
            if (r5 > 0) goto L_0x063a
        L_0x05fa:
            r3 = 0
            r0 = r18
            r0.m = r3
            r3 = 9
            r0 = r18
            r0.i = r3
            java.lang.String r3 = "invalid bit length repeat"
            r0 = r19
            r0.i = r3
            r0 = r18
            r0.b = r12
            r0 = r18
            r0.a = r11
            r0 = r19
            r0.c = r9
            r0 = r19
            long r3 = r0.d
            r0 = r19
            int r5 = r0.b
            int r5 = r10 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r0 = r19
            r0.d = r3
            r0 = r19
            r0.b = r10
            r0 = r18
            r0.f = r13
            r3 = -3
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r3)
            goto L_0x0059
        L_0x063a:
            r3 = 16
            if (r14 != r3) goto L_0x065e
            r0 = r18
            int[] r3 = r0.m
            int r6 = r5 + -1
            r3 = r3[r6]
        L_0x0646:
            r0 = r18
            int[] r7 = r0.m
            int r6 = r5 + 1
            r7[r5] = r3
            int r4 = r4 + -1
            if (r4 != 0) goto L_0x0829
            r0 = r18
            r0.l = r6
            r14 = r9
            r15 = r10
            r16 = r11
            r17 = r12
            goto L_0x04cb
        L_0x065e:
            r3 = 0
            goto L_0x0646
        L_0x0660:
            r0 = r18
            int[] r3 = r0.o
            r4 = 0
            r5 = -1
            r3[r4] = r5
            r3 = 1
            int[] r7 = new int[r3]
            r3 = 1
            int[] r8 = new int[r3]
            r3 = 1
            int[] r9 = new int[r3]
            r3 = 1
            int[] r10 = new int[r3]
            r3 = 0
            r4 = 9
            r7[r3] = r4
            r3 = 0
            r4 = 6
            r8[r3] = r4
            r0 = r18
            int r5 = r0.k
            r0 = r18
            com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.d r3 = r0.u
            r4 = r5 & 31
            int r4 = r4 + 257
            int r5 = r5 >> 5
            r5 = r5 & 31
            int r5 = r5 + 1
            r0 = r18
            int[] r6 = r0.m
            r0 = r18
            int[] r11 = r0.r
            r12 = r19
            int r3 = r3.a(r4, r5, r6, r7, r8, r9, r10, r11, r12)
            if (r3 == 0) goto L_0x06df
            r4 = -3
            if (r3 != r4) goto L_0x06ad
            r4 = 0
            r0 = r18
            r0.m = r4
            r4 = 9
            r0 = r18
            r0.i = r4
        L_0x06ad:
            r0 = r17
            r1 = r18
            r1.b = r0
            r0 = r16
            r1 = r18
            r1.a = r0
            r0 = r19
            r0.c = r14
            r0 = r19
            long r4 = r0.d
            r0 = r19
            int r6 = r0.b
            int r6 = r15 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r19
            r0.d = r4
            r0 = r19
            r0.b = r15
            r0 = r18
            r0.f = r13
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r3)
            goto L_0x0059
        L_0x06df:
            r0 = r18
            com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a r3 = r0.p
            r4 = 0
            r4 = r7[r4]
            r5 = 0
            r5 = r8[r5]
            r0 = r18
            int[] r6 = r0.r
            r7 = 0
            r7 = r9[r7]
            r0 = r18
            int[] r8 = r0.r
            r9 = 0
            r9 = r10[r9]
            r3.a(r4, r5, r6, r7, r8, r9)
            r3 = 6
            r0 = r18
            r0.i = r3
        L_0x06ff:
            r0 = r17
            r1 = r18
            r1.b = r0
            r0 = r16
            r1 = r18
            r1.a = r0
            r0 = r19
            r0.c = r14
            r0 = r19
            long r3 = r0.d
            r0 = r19
            int r5 = r0.b
            int r5 = r15 - r5
            long r5 = (long) r5
            long r3 = r3 + r5
            r0 = r19
            r0.d = r3
            r0 = r19
            r0.b = r15
            r0 = r18
            r0.f = r13
            r0 = r18
            com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a r3 = r0.p
            r0 = r18
            r1 = r19
            r2 = r20
            int r3 = r3.a(r0, r1, r2)
            r4 = 1
            if (r3 == r4) goto L_0x0742
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r3)
            goto L_0x0059
        L_0x0742:
            r20 = 0
            r0 = r19
            int r6 = r0.b
            r0 = r19
            int r5 = r0.c
            r0 = r18
            int r8 = r0.b
            r0 = r18
            int r7 = r0.a
            r0 = r18
            int r4 = r0.f
            r0 = r18
            int r3 = r0.e
            if (r4 >= r3) goto L_0x0776
            r0 = r18
            int r3 = r0.e
            int r3 = r3 - r4
            int r3 = r3 + -1
        L_0x0765:
            r0 = r18
            int r9 = r0.q
            if (r9 != 0) goto L_0x077c
            r9 = 0
            r0 = r18
            r0.i = r9
            r10 = r3
            r13 = r4
            r3 = r5
            r4 = r6
            goto L_0x0025
        L_0x0776:
            r0 = r18
            int r3 = r0.d
            int r3 = r3 - r4
            goto L_0x0765
        L_0x077c:
            r3 = 7
            r0 = r18
            r0.i = r3
        L_0x0781:
            r0 = r18
            r0.f = r4
            int r3 = r18.b(r19, r20)
            r0 = r18
            int r13 = r0.f
            r0 = r18
            int r4 = r0.e
            r0 = r18
            int r9 = r0.f
            if (r4 == r9) goto L_0x07c5
            r0 = r18
            r0.b = r8
            r0 = r18
            r0.a = r7
            r0 = r19
            r0.c = r5
            r0 = r19
            long r4 = r0.d
            r0 = r19
            int r7 = r0.b
            int r7 = r6 - r7
            long r7 = (long) r7
            long r4 = r4 + r7
            r0 = r19
            r0.d = r4
            r0 = r19
            r0.b = r6
            r0 = r18
            r0.f = r13
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r3)
            goto L_0x0059
        L_0x07c5:
            r3 = 8
            r0 = r18
            r0.i = r3
        L_0x07cb:
            r0 = r18
            r0.b = r8
            r0 = r18
            r0.a = r7
            r0 = r19
            r0.c = r5
            r0 = r19
            long r3 = r0.d
            r0 = r19
            int r5 = r0.b
            int r5 = r6 - r5
            long r7 = (long) r5
            long r3 = r3 + r7
            r0 = r19
            r0.d = r3
            r0 = r19
            r0.b = r6
            r0 = r18
            r0.f = r13
            r3 = 1
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r3)
            goto L_0x0059
        L_0x07fa:
            r0 = r18
            r0.b = r8
            r0 = r18
            r0.a = r7
            r0 = r19
            r0.c = r3
            r0 = r19
            long r5 = r0.d
            r0 = r19
            int r3 = r0.b
            int r3 = r4 - r3
            long r7 = (long) r3
            long r5 = r5 + r7
            r0 = r19
            r0.d = r5
            r0 = r19
            r0.b = r4
            r0 = r18
            r0.f = r13
            r3 = -3
            r0 = r18
            r1 = r19
            int r3 = r0.b(r1, r3)
            goto L_0x0059
        L_0x0829:
            r5 = r6
            goto L_0x0646
        L_0x082c:
            r10 = r3
            r13 = r4
            r3 = r5
            r4 = r6
            goto L_0x0025
        L_0x0832:
            r9 = r5
            goto L_0x02cb
        L_0x0835:
            r10 = r5
            goto L_0x027b
        L_0x0838:
            r14 = r3
            r15 = r4
            r16 = r7
            r17 = r8
            goto L_0x04cb
        L_0x0840:
            r14 = r3
            r15 = r4
            r16 = r7
            r17 = r8
            goto L_0x06ff
        L_0x0848:
            r5 = r3
            r6 = r4
            r4 = r13
            goto L_0x0781
        L_0x084d:
            r5 = r3
            r6 = r4
            goto L_0x07cb
        L_0x0851:
            r11 = r3
            r12 = r4
            r14 = r7
            r15 = r8
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.b.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.g, int):int");
    }

    /* access modifiers changed from: package-private */
    public final void a(g gVar) {
        a(gVar, (long[]) null);
        this.c = null;
        this.r = null;
    }

    /* access modifiers changed from: package-private */
    public final void a(g gVar, long[] jArr) {
        if (jArr != null) {
            jArr[0] = this.t;
        }
        this.i = 0;
        this.a = 0;
        this.b = 0;
        this.f = 0;
        this.e = 0;
        if (this.s != null) {
            long a2 = i.a(0, null, 0, 0);
            this.t = a2;
            gVar.l = a2;
        }
    }

    /* access modifiers changed from: package-private */
    public final int b(g gVar, int i2) {
        int i3;
        int i4 = gVar.f;
        int i5 = this.e;
        int i6 = (i5 <= this.f ? this.f : this.d) - i5;
        if (i6 > gVar.g) {
            i6 = gVar.g;
        }
        if (i6 != 0 && i2 == -5) {
            i2 = 0;
        }
        gVar.g -= i6;
        gVar.h += (long) i6;
        if (this.s != null) {
            long a2 = i.a(this.t, this.c, i5, i6);
            this.t = a2;
            gVar.l = a2;
        }
        System.arraycopy(this.c, i5, gVar.e, i4, i6);
        int i7 = i4 + i6;
        int i8 = i6 + i5;
        if (i8 == this.d) {
            if (this.f == this.d) {
                this.f = 0;
            }
            int i9 = this.f + 0;
            if (i9 > gVar.g) {
                i9 = gVar.g;
            }
            if (i9 != 0 && i2 == -5) {
                i2 = 0;
            }
            gVar.g -= i9;
            gVar.h += (long) i9;
            if (this.s != null) {
                long a3 = i.a(this.t, this.c, 0, i9);
                this.t = a3;
                gVar.l = a3;
            }
            System.arraycopy(this.c, 0, gVar.e, i7, i9);
            i3 = i7 + i9;
            i8 = i9 + 0;
        } else {
            i3 = i7;
        }
        gVar.f = i3;
        this.e = i8;
        return i2;
    }
}
