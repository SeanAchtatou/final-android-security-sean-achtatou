package com.agilebinary.mobilemonitor.device.android.ui;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.f.b;
import com.agilebinary.mobilemonitor.device.android.b.b.a;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;

final class aj extends at {
    private /* synthetic */ MainActivity b;

    aj(MainActivity mainActivity) {
        this.b = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, int):boolean
      com.agilebinary.mobilemonitor.device.a.a.f.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object[] objArr) {
        String str = ((String[]) objArr)[0];
        b bVar = new b();
        a g = this.b.C;
        this.b.B.u();
        int a = g.a(str);
        if (!d()) {
            if (a == 0) {
                c.a().b(str, true);
                bVar.a = true;
                BackgroundService.a(this.b, "EXTRA_START_FROM_GUI", (Uri) null);
            } else {
                bVar.a = false;
                bVar.b = b.a("ACTIVATION_ERROR_" + a);
            }
        }
        return bVar;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        try {
            this.b.s.hide();
            this.b.a(b.a("COMMONS_ACTIVATION_FAILED", b.a("COMMONS_CANCELLED")));
        } catch (Exception e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "onCancelled3", e);
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        b bVar = (b) obj;
        super.a(bVar);
        try {
            this.b.s.hide();
            this.b.d();
            if (bVar == null || !bVar.a || d()) {
                this.b.a(bVar.b);
                return;
            }
            this.b.a((String) null);
            this.b.e();
            if (!com.agilebinary.mobilemonitor.device.android.device.admin.a.c(this.b)) {
                this.b.g();
            } else if (!com.agilebinary.mobilemonitor.device.android.device.admin.a.a((Context) this.b)) {
                com.agilebinary.mobilemonitor.device.android.device.admin.a.a((Activity) this.b);
                au unused = this.b.E = au.b;
                "mActivateState:" + this.b.E;
            } else {
                this.b.g();
            }
        } catch (Exception e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "onPOstExecute4", e);
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
    }
}
