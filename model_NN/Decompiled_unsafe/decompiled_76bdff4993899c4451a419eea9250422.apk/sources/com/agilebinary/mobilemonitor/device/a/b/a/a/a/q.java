package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public abstract class q extends s {
    private boolean a;
    private double b;
    private double c;
    private boolean d;
    private double e;
    private boolean f;
    private double g;
    private double h;
    private boolean i;
    private double j;
    private boolean k;
    private double l;
    private long m;

    public q(a aVar) {
        super(aVar);
        this.a = aVar.b();
        if (this.a) {
            this.b = aVar.g();
            this.c = aVar.g();
            this.d = aVar.b();
            if (this.d) {
                this.e = aVar.g();
            }
            this.f = aVar.b();
            if (this.f) {
                this.g = aVar.g();
                this.h = aVar.g();
            }
            this.i = aVar.b();
            if (this.i) {
                this.j = aVar.g();
            }
            this.k = aVar.b();
            if (this.k) {
                this.l = aVar.g();
            }
            this.m = aVar.f();
        }
    }

    public q(String str, String str2, d dVar) {
        super(str, str2);
        if (dVar != null) {
            this.a = true;
            this.b = dVar.i();
            this.c = dVar.j();
            this.d = dVar.a();
            if (this.d) {
                this.e = dVar.b();
            }
            this.f = dVar.f();
            if (this.f) {
                this.g = dVar.h();
                this.h = dVar.g();
            }
            this.i = dVar.k();
            if (this.i) {
                this.j = dVar.l();
            }
            this.k = dVar.c();
            if (this.k) {
                this.l = dVar.d();
            }
            this.m = dVar.e();
        }
    }

    public String a(c cVar) {
        return super.a(cVar) + (this.a ? "\nLocation_Latitude: " + this.b + "\nLocation_Longitude: " + this.c + "\nLocation_Altitude: " + this.e + "\nLocation_HorizontalAccuracy: " + this.g + "\nLocation_VerticalAccuracy: " + this.h + "\nLocation_Speed: " + this.j + "\nLocation_Bearing: " + this.l + "\nLocation_fixTime: " + cVar.a(this.m) : "\nNo Location Info!");
    }

    public void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        if (this.a) {
            aVar.a(this.b);
            aVar.a(this.c);
            aVar.a(this.d);
            if (this.d) {
                aVar.a(this.e);
            }
            aVar.a(this.f);
            if (this.f) {
                aVar.a(this.g);
                aVar.a(this.h);
            }
            aVar.a(this.i);
            if (this.i) {
                aVar.a(this.j);
            }
            aVar.a(this.k);
            if (this.k) {
                aVar.a(this.l);
            }
            aVar.a(this.m);
        }
    }
}
