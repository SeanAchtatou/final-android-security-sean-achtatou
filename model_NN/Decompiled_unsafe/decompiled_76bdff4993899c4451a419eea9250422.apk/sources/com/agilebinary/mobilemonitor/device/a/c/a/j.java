package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.b.a.a.a;
import com.agilebinary.b.a.a.u;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class j implements d {
    private static final String a = f.a();
    private u b;

    public j(u uVar) {
        this.b = uVar;
        if (this.b.b() == 0) {
            this.b.b(new a());
        }
    }

    public final void a() {
        a a2 = this.b.a();
        while (a2.a()) {
            ((d) a2.b()).a();
        }
    }

    public final void b() {
        a a2 = this.b.a();
        while (a2.a()) {
            ((d) a2.b()).b();
        }
    }

    public final void c() {
        a a2 = this.b.a();
        while (a2.a()) {
            ((d) a2.b()).c();
        }
    }

    public final void d() {
        a a2 = this.b.a();
        while (a2.a()) {
            ((d) a2.b()).d();
        }
        this.b.c();
    }

    public final f e() {
        boolean z;
        boolean z2;
        StringBuffer stringBuffer = new StringBuffer();
        a a2 = this.b.a();
        boolean z3 = true;
        boolean z4 = true;
        while (a2.a()) {
            f e = ((d) a2.b()).e();
            stringBuffer.append(e.b).append(", ");
            if (e.a == 0) {
                z = z3;
                z2 = z4;
            } else if (e.a == 1) {
                z = false;
                z2 = false;
            } else {
                z = false;
                z2 = z4;
            }
            z4 = z2;
            z3 = z;
        }
        return z3 ? new f(0, stringBuffer.toString()) : z4 ? new f(2, stringBuffer.toString()) : new f(1, stringBuffer.toString());
    }
}
