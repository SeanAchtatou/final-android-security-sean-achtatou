package com.agilebinary.mobilemonitor.device.android.device.admin;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.a.a.a.j.f;
import com.agilebinary.phonebeagle.R;

public class a {
    private long a;
    private long b;

    public a() {
    }

    public a(f fVar, f fVar2) {
        this.a = 0;
        this.b = 0;
    }

    public static void a(Activity activity) {
        try {
            Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
            intent.putExtra("android.app.extra.DEVICE_ADMIN", d(activity));
            intent.putExtra("android.app.extra.ADD_EXPLANATION", activity.getString(R.string.device_admin_description_activate));
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e("DPMP", "addDeviceAdmin", e);
            e.printStackTrace();
        }
    }

    public static boolean a(Context context) {
        Object systemService = context.getSystemService("device_policy");
        if (systemService == null) {
            throw new IllegalArgumentException();
        }
        return ((Boolean) systemService.getClass().getMethod("isAdminActive", ComponentName.class).invoke(systemService, d(context))).booleanValue();
    }

    public static void b(Context context) {
        Object systemService = context.getSystemService("device_policy");
        if (systemService == null) {
            throw new IllegalArgumentException();
        }
        systemService.getClass().getMethod("removeActiveAdmin", ComponentName.class).invoke(systemService, d(context));
    }

    public static boolean c(Context context) {
        try {
            return context.getSystemService("device_policy") != null;
        } catch (Exception e) {
            return false;
        }
    }

    private static ComponentName d(Context context) {
        return new ComponentName(context, MyDeviceAdminReceiver.class);
    }

    public void a() {
        this.a++;
    }

    public void b() {
        this.b++;
    }
}
