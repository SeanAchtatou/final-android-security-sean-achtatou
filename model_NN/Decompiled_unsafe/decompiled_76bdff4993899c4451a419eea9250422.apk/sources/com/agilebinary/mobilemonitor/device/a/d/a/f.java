package com.agilebinary.mobilemonitor.device.a.d.a;

import com.agilebinary.mobilemonitor.device.a.b.b;
import com.agilebinary.mobilemonitor.device.a.j.a;
import java.util.Enumeration;
import java.util.Vector;

public final class f {
    private static String a = com.agilebinary.mobilemonitor.device.a.g.f.a();
    private Vector b = new Vector();
    private int c;
    private int d;

    private static int a(byte[] bArr, int i, long j, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            bArr[((i + i2) - 1) - i3] = (byte) ((int) ((j >>> (((i2 - 1) - i3) * 8)) & 255));
        }
        return i2;
    }

    public final Enumeration a() {
        return this.b.elements();
    }

    public final void a(int i) {
        this.c = i;
    }

    public final boolean a(b bVar) {
        if (bVar.f() != 0 || bVar.k() > 65536 || this.d + bVar.k() + 2 > this.c || this.b.size() >= 255) {
            return false;
        }
        this.b.addElement(bVar);
        this.d += bVar.k() + 2;
        return true;
    }

    public final byte[] a(com.agilebinary.mobilemonitor.device.a.j.a.b bVar) {
        byte[] bArr;
        synchronized (bVar) {
            int size = (this.b.size() * 11) + 1;
            Enumeration elements = this.b.elements();
            Vector vector = new Vector();
            while (elements.hasMoreElements()) {
                b bVar2 = (b) elements.nextElement();
                try {
                    if (!bVar2.b()) {
                        bVar.a(bVar2);
                    }
                    size += bVar2.k();
                    vector.addElement(bVar2);
                } catch (a e) {
                    "" + bVar2.j();
                    bVar2.f(2);
                }
            }
            bArr = new byte[size];
            bArr[0] = (byte) this.b.size();
            Enumeration elements2 = vector.elements();
            int i = 1;
            while (elements2.hasMoreElements()) {
                b bVar3 = (b) elements2.nextElement();
                byte[] e2 = bVar3.e();
                int a2 = i + a(bArr, i, bVar3.j(), 8);
                int a3 = a(bArr, a2, (long) bVar3.i(), 1) + a2;
                int a4 = a3 + a(bArr, a3, (long) e2.length, 2);
                System.arraycopy(e2, 0, bArr, a4, e2.length);
                i = a4 + e2.length;
            }
        }
        return bArr;
    }

    public final void b() {
        this.b.setSize(0);
        this.d = 0;
    }

    public final void b(int i) {
        Enumeration elements = this.b.elements();
        while (elements.hasMoreElements()) {
            b bVar = (b) elements.nextElement();
            bVar.f(i);
            if (i == 0) {
                bVar.a(bVar.k());
            }
        }
    }

    public final boolean c() {
        return this.b.isEmpty();
    }
}
