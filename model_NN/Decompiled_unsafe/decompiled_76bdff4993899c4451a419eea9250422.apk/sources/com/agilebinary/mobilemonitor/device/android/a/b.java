package com.agilebinary.mobilemonitor.device.android.a;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteFullException;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.device.a.e.c;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Iterator;
import java.util.Stack;

public class b implements c {
    private static b a;
    private SQLiteDatabase b;
    private SQLiteStatement c;
    private SQLiteStatement d;

    private b() {
    }

    public b(Context context) {
        a = this;
        try {
            this.b = new a(this, context, "db_ex", null, 1).getWritableDatabase();
            this.b.setMaximumSize(262144);
            this.c = this.b.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s) values(?,?,?,?) ", c.a, c.b, c.c, c.d, c.e));
            this.d = this.b.compileStatement(String.format("DELETE FROM %1$s where %2$s = (SELECT MIN(%2$s) from %1$s)", c.a, c.b));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String a(String str) {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(str.length());
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt != '/') {
                sb.append(charAt);
                z = false;
            } else if (!z) {
                sb.append(charAt);
                z = true;
            }
        }
        return sb.toString();
    }

    public static StringBuffer a() {
        Cursor cursor;
        StringBuffer stringBuffer = new StringBuffer();
        try {
            if (a != null) {
                try {
                    cursor = a.b.query(c.a, c.f, null, null, null, null, c.b);
                    while (cursor.moveToNext()) {
                        try {
                            long j = cursor.getLong(0);
                            stringBuffer.append("#").append(j).append(" (").append(new Date(j)).append(")").append("\n");
                            stringBuffer.append("TAG:").append(cursor.getString(1)).append("\n");
                            stringBuffer.append("MSG:").append(cursor.getString(2)).append("\n");
                            stringBuffer.append("STACK:").append(cursor.getString(3)).append("\n");
                        } catch (Throwable th) {
                            th = th;
                            if (cursor != null) {
                                cursor.close();
                            }
                            throw th;
                        }
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuffer;
    }

    public static URI a(String str, String str2, int i, String str3, String str4, String str5) {
        StringBuilder sb = new StringBuilder();
        if (str2 != null) {
            if (str != null) {
                sb.append(str);
                sb.append("://");
            }
            sb.append(str2);
            if (i > 0) {
                sb.append(':');
                sb.append(i);
            }
        }
        if (str3 == null || !str3.startsWith("/")) {
            sb.append('/');
        }
        if (str3 != null) {
            sb.append(str3);
        }
        if (str4 != null) {
            sb.append('?');
            sb.append(str4);
        }
        if (str5 != null) {
            sb.append('#');
            sb.append(str5);
        }
        return new URI(sb.toString());
    }

    private static URI a(URI uri) {
        String path = uri.getPath();
        if (path == null || path.indexOf("/.") == -1) {
            return uri;
        }
        String[] split = path.split("/");
        Stack stack = new Stack();
        for (int i = 0; i < split.length; i++) {
            if (split[i].length() != 0 && !".".equals(split[i])) {
                if (!"..".equals(split[i])) {
                    stack.push(split[i]);
                } else if (!stack.isEmpty()) {
                    stack.pop();
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = stack.iterator();
        while (it.hasNext()) {
            sb.append('/').append((String) it.next());
        }
        try {
            return new URI(uri.getScheme(), uri.getAuthority(), sb.toString(), uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.a.b.a(java.net.URI, com.agilebinary.a.a.a.b, boolean):java.net.URI
     arg types: [java.net.URI, com.agilebinary.a.a.a.b, int]
     candidates:
      com.agilebinary.mobilemonitor.device.android.a.b.a(java.lang.String, java.lang.String, java.lang.Throwable):void
      com.agilebinary.mobilemonitor.device.a.e.c.a(java.lang.String, java.lang.String, java.lang.Throwable):void
      com.agilebinary.mobilemonitor.device.android.a.b.a(java.net.URI, com.agilebinary.a.a.a.b, boolean):java.net.URI */
    public static URI a(URI uri, com.agilebinary.a.a.a.b bVar) {
        return a(uri, bVar, false);
    }

    public static URI a(URI uri, com.agilebinary.a.a.a.b bVar, boolean z) {
        if (uri == null) {
            throw new IllegalArgumentException("URI may nor be null");
        } else if (bVar != null) {
            return a(bVar.c(), bVar.a(), bVar.b(), a(uri.getRawPath()), uri.getRawQuery(), z ? null : uri.getRawFragment());
        } else {
            return a(null, null, -1, a(uri.getRawPath()), uri.getRawQuery(), z ? null : uri.getRawFragment());
        }
    }

    public static URI a(URI uri, URI uri2) {
        if (uri == null) {
            throw new IllegalArgumentException("Base URI may nor be null");
        } else if (uri2 == null) {
            throw new IllegalArgumentException("Reference URI may nor be null");
        } else {
            String uri3 = uri2.toString();
            if (uri3.startsWith("?")) {
                String uri4 = uri.toString();
                if (uri4.indexOf(63) >= 0) {
                    uri4 = uri4.substring(0, uri4.indexOf(63));
                }
                return URI.create(uri4 + uri2.toString());
            }
            boolean z = uri3.length() == 0;
            if (z) {
                uri2 = URI.create("#");
            }
            URI resolve = uri.resolve(uri2);
            if (z) {
                String uri5 = resolve.toString();
                resolve = URI.create(uri5.substring(0, uri5.indexOf(35)));
            }
            return a(resolve);
        }
    }

    private void a(String str, String str2, Throwable th, int i) {
        if (i <= 20 && this.c != null) {
            try {
                long currentTimeMillis = System.currentTimeMillis();
                StringWriter stringWriter = new StringWriter();
                th.printStackTrace(new PrintWriter(stringWriter));
                this.c.bindLong(1, currentTimeMillis);
                this.c.bindString(2, str);
                this.c.bindString(3, str2);
                this.c.bindString(4, stringWriter.toString());
                this.c.executeInsert();
            } catch (SQLiteFullException e) {
                this.d.execute();
                a(str, str2, th, i);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public final void a(String str, String str2, Throwable th) {
        a(str, str2, th, 0);
    }
}
