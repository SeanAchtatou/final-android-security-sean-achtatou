package com.agilebinary.mobilemonitor.device.a.c;

import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.b.m;
import com.agilebinary.mobilemonitor.device.a.b.n;
import com.agilebinary.mobilemonitor.device.a.d.a.h;
import com.agilebinary.mobilemonitor.device.a.d.a.i;
import com.agilebinary.mobilemonitor.device.a.d.b.g;
import com.agilebinary.mobilemonitor.device.a.d.f;
import com.agilebinary.mobilemonitor.device.a.g.d;
import com.agilebinary.mobilemonitor.device.a.j.a.a;

public abstract class e implements f, d {
    private static final String d = com.agilebinary.mobilemonitor.device.a.g.f.a();
    protected c a;
    protected com.agilebinary.mobilemonitor.device.a.a.c b;
    protected boolean c;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.device.a.d.b.c e;
    private n f;
    private a g;
    private h h;
    private com.agilebinary.mobilemonitor.device.a.d.b.d i;
    private i j;
    private b k;
    private boolean l;

    /* access modifiers changed from: protected */
    public abstract h a(b bVar, com.agilebinary.mobilemonitor.device.a.a.c cVar);

    /* access modifiers changed from: protected */
    public abstract g a(g gVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, b bVar2, com.agilebinary.mobilemonitor.device.a.a.c cVar);

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.mobilemonitor.device.a.j.a.b a(g gVar, com.agilebinary.mobilemonitor.device.a.a.c cVar, b bVar);

    public void a() {
        if (!this.l) {
            this.b = com.agilebinary.mobilemonitor.device.a.a.c.a();
            if (this.b == null) {
                throw new com.agilebinary.mobilemonitor.device.a.g.c();
            }
            this.k = b.a();
            if (this.k == null) {
                throw new com.agilebinary.mobilemonitor.device.a.g.c();
            }
            this.h = a(this.k, this.b);
            this.g = (a) a(this.h, this.b, this.k);
            this.i = a(this.h, this.g, this.k, this.b);
            this.j = b(this.h, this.g, this.k, this.b);
            ((g) this.i).a(this.j);
            i iVar = this.j;
            a aVar = this.g;
            com.agilebinary.mobilemonitor.device.a.d.b.d dVar = this.i;
            this.f = new n(this, iVar, aVar, this.h, this.k, this.b);
            this.b.a((com.agilebinary.mobilemonitor.device.a.b.a) this.f);
            this.b.a((m) this.f);
            this.e = new com.agilebinary.mobilemonitor.device.a.d.b.c(this.f, this.i, this.h, this.b, this.g);
            this.a = new c(this.f, this.e, this.b, this.k);
            this.h.a(this.a);
            this.h.a();
            this.i.a();
            this.j.a();
            this.g.a();
            this.e.a();
            this.f.a();
            this.l = true;
        }
    }

    /* access modifiers changed from: protected */
    public abstract h b(g gVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, b bVar2, com.agilebinary.mobilemonitor.device.a.a.c cVar);

    public void b() {
        if (!this.c) {
            this.g.b();
            this.h.b();
            this.i.b();
            this.j.b();
            this.e.b();
            this.f.b();
            this.c = true;
        }
    }

    public void c() {
        if (this.c) {
            this.f.c();
            this.e.c();
            this.i.c();
            this.j.c();
            this.h.c();
            this.g.c();
            this.i.c();
            this.j.c();
            this.c = false;
        }
    }

    public final void d() {
        if (this.l) {
            this.f.d();
            this.e.d();
            this.i.d();
            this.j.d();
            this.h.d();
            com.agilebinary.mobilemonitor.device.a.i.a.b();
            this.g.d();
            this.i.d();
            this.j.d();
            this.l = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, int):boolean
      com.agilebinary.mobilemonitor.device.a.a.f.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void */
    public void e() {
        this.b.b((String) null, true);
        this.b.Z();
        c();
        d();
        try {
            this.g.a();
            this.g.b();
            this.g.k();
            this.g.c();
            this.g.d();
        } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(d, "deactivate", e2);
        }
    }

    public void g() {
        this.h.a("ChkActB", new a(this, "ForceCheckActivationAfterBoot"), null, 120000, true);
        int u = this.b.u();
        if (u > 0) {
            this.h.a("LocXB", new b(this, "ForceGetLocationAndUploadAfterBoot"), null, (long) (u * 1000), true);
        }
        if (this.b.j()) {
            try {
                this.a.a(System.currentTimeMillis(), 2);
            } catch (Exception e2) {
                com.agilebinary.mobilemonitor.device.a.e.a.e(d, "exception in handleBootCompleteEvent: ", e2);
            }
        }
    }

    public final com.agilebinary.mobilemonitor.device.a.d.b.c h() {
        return this.e;
    }

    public final n i() {
        return this.f;
    }

    public final com.agilebinary.mobilemonitor.device.a.j.a.b j() {
        return this.g;
    }

    public final h k() {
        return this.h;
    }

    public final i l() {
        return this.j;
    }
}
