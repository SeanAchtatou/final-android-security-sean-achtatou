package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public final class d extends q {
    private long a;
    private int b;
    private String c;

    public d(a aVar) {
        super(aVar);
        this.a = aVar.f();
        this.b = aVar.e();
        this.c = aVar.i();
    }

    public d(String str, String str2, com.agilebinary.mobilemonitor.device.a.b.a.a.d dVar, long j, int i, String str3) {
        super(str, str2, dVar);
        this.a = j;
        this.b = i;
        this.c = str3 == null ? "" : str3;
    }

    public final String a(c cVar) {
        return super.a(cVar) + "\nTime: " + cVar.a(this.a) + "\nKind: " + this.b + "\nName: " + this.c;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
    }

    public final byte b() {
        return 11;
    }
}
