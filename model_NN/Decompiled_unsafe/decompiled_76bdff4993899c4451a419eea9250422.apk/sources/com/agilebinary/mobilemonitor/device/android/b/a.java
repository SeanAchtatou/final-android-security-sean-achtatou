package com.agilebinary.mobilemonitor.device.android.b;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.t;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public final class a extends com.agilebinary.mobilemonitor.device.a.d.a {
    private static String a = f.a();
    private j b;
    private byte[] c;
    private int d;

    public a(j jVar) {
        this.b = jVar;
        this.d = jVar.a().b();
        try {
            this.c = a(jVar.b());
        } catch (IOException e) {
            com.agilebinary.mobilemonitor.device.a.e.a.d(a, "failed to read body of a response", e);
        }
    }

    private synchronized byte[] a(c cVar) {
        ByteArrayOutputStream byteArrayOutputStream;
        byteArrayOutputStream = new ByteArrayOutputStream();
        cVar.a(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public final String a(String str) {
        t[] b2 = this.b.b(str);
        if (b2.length == 0) {
            return null;
        }
        return b2[0].b();
    }

    public final byte[] a() {
        return this.c;
    }

    public final int b() {
        return this.d;
    }
}
