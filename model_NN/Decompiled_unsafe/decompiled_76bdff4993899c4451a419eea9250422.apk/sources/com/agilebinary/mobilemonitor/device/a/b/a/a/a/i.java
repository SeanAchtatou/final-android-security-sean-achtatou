package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public abstract class i extends q {
    private long a;
    private long b;
    private byte c;
    private String d;

    public i(a aVar) {
        super(aVar);
        this.a = aVar.f();
        this.b = aVar.f();
        this.c = aVar.c();
        this.d = aVar.i();
    }

    public i(String str, String str2, d dVar, long j, long j2, byte b2, String str3) {
        super(str, str2, dVar);
        this.a = j;
        this.b = j2;
        this.c = b2;
        this.d = str3;
    }

    public String a(c cVar) {
        return super.a(cVar) + "\nTimeSent: " + cVar.a(this.a) + "\nTimeReceived: " + cVar.a(this.a) + "\nDirection: " + ((int) this.c) + "\nSMSC: " + this.d;
    }

    public void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
        aVar.a(this.d);
    }
}
