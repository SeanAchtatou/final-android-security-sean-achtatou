package com.agilebinary.mobilemonitor.device.a.d;

public final class d extends Exception {
    private int a;

    public d(int i, f fVar) {
        this.a = i;
        if (fVar != null) {
            fVar.f();
        }
    }

    public final int a() {
        return this.a;
    }
}
