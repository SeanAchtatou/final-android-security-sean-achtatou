package com.agilebinary.mobilemonitor.device.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.a.f.b;

final class al extends BroadcastReceiver {
    private /* synthetic */ MainActivity a;

    al(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.a.s != null && this.a.s.isShowing()) {
            this.a.s.cancel();
        }
        if (!intent.hasExtra("EXTRA_SERVICE_STATUS")) {
            return;
        }
        if (intent.getBooleanExtra("EXTRA_SERVICE_STATUS", false)) {
            this.a.A.add(new ap(b.a("COMMON_SERVICE_STARTED")));
        } else {
            this.a.A.add(new ap(b.a("COMMON_SERVICE_STOPPED")));
        }
    }
}
