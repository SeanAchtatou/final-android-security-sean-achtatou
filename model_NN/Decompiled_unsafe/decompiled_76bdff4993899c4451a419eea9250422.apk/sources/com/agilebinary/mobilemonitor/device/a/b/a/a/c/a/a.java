package com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;

public final class a extends b {
    private byte[] a;
    private int b;

    public a() {
        this(32);
    }

    public a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Negative initial size: " + i);
        }
        this.a = new byte[i];
    }

    public final synchronized void a() {
    }

    public final synchronized void a(int i) {
        int i2 = this.b + 1;
        if (i2 > this.a.length) {
            byte[] bArr = new byte[Math.max(this.a.length << 1, i2)];
            System.arraycopy(this.a, 0, bArr, 0, this.b);
            this.a = bArr;
        }
        this.a[this.b] = (byte) i;
        this.b = i2;
    }

    public final synchronized void a(byte[] bArr, int i, int i2) {
        if (i >= 0) {
            if (i <= bArr.length && i2 >= 0 && i + i2 <= bArr.length && i + i2 >= 0) {
                if (i2 != 0) {
                    int i3 = this.b + i2;
                    if (i3 > this.a.length) {
                        byte[] bArr2 = new byte[Math.max(this.a.length << 1, i3)];
                        System.arraycopy(this.a, 0, bArr2, 0, this.b);
                        this.a = bArr2;
                    }
                    System.arraycopy(bArr, i, this.a, this.b, i2);
                    this.b = i3;
                }
            }
        }
        throw new IndexOutOfBoundsException();
    }

    public final synchronized void b() {
        this.b = 0;
    }

    public final synchronized byte[] c() {
        byte[] bArr;
        bArr = new byte[this.b];
        System.arraycopy(this.a, 0, bArr, 0, this.b);
        return bArr;
    }
}
