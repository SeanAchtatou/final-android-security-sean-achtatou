package com.agilebinary.mobilemonitor.device.android.ui;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class z implements ThreadFactory {
    private final AtomicInteger a = new AtomicInteger(1);

    z() {
    }

    public final Thread newThread(Runnable runnable) {
        return new Thread(runnable, "MinimalAsyncTask #" + this.a.getAndIncrement());
    }
}
