package com.agilebinary.mobilemonitor.device.a.j.a.a;

import bsh.ParserTreeConstants;
import com.agilebinary.mobilemonitor.device.a.b.n;
import com.agilebinary.mobilemonitor.device.a.d.a.i;
import com.agilebinary.mobilemonitor.device.a.j.a.b;
import java.util.Hashtable;

public final class e {
    private static Hashtable a = new Hashtable();

    public static a a(int i, b bVar, i iVar, n nVar) {
        String valueOf = String.valueOf(i);
        a aVar = (a) a.get(valueOf);
        if (aVar == null) {
            switch (i) {
                case 0:
                    aVar = new b(bVar, iVar);
                    break;
                case 1:
                    aVar = new c(3, bVar, iVar, nVar);
                    break;
                case 2:
                    aVar = new c(2, bVar, iVar, nVar);
                    break;
                case 3:
                    aVar = new c(1, bVar, iVar, nVar);
                    break;
                case ParserTreeConstants.JJTIMPORTDECLARATION:
                    aVar = new d(3, bVar, iVar, nVar);
                    break;
                case ParserTreeConstants.JJTVARIABLEDECLARATOR:
                    aVar = new d(2, bVar, iVar, nVar);
                    break;
                case 6:
                    aVar = new d(1, bVar, iVar, nVar);
                    break;
            }
            a.put(valueOf, aVar);
        }
        return aVar;
    }
}
