package com.agilebinary.mobilemonitor.device.android.device;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.p;
import com.agilebinary.mobilemonitor.device.a.c.h;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.device.a.n;
import com.agilebinary.mobilemonitor.device.android.device.a.q;
import com.agilebinary.mobilemonitor.device.android.device.a.r;
import com.agilebinary.mobilemonitor.device.android.device.a.v;
import com.agilebinary.mobilemonitor.device.android.device.a.w;
import com.agilebinary.mobilemonitor.device.android.device.receivers.d;
import com.agilebinary.mobilemonitor.device.android.device.receivers.e;

public final class a extends j implements h {
    private static String g = f.a();
    private r h;
    private e i;
    private com.agilebinary.mobilemonitor.device.android.device.receivers.a j;
    private com.agilebinary.mobilemonitor.device.android.device.a.e k;
    private n l;
    private q m;
    private w n;
    private PendingIntent o;
    private v p;
    private com.agilebinary.mobilemonitor.device.android.device.receivers.f q;
    private d r;

    public a(Context context, b bVar, c cVar, String str) {
        super(context, bVar, cVar, str);
    }

    public final void a() {
        this.n = new w(this.d, this, this.a);
        this.n.a();
        this.i = new e(this.d, this.b);
        this.j = new com.agilebinary.mobilemonitor.device.android.device.receivers.a(this.d, this.b);
        this.h = new r(this.d, this.b, this);
        super.a();
    }

    public final void a(String str) {
        new com.agilebinary.mobilemonitor.device.android.device.b.b(str, this.a.X());
    }

    public final void a(boolean z) {
        if (z) {
            this.l = new n(this.d, this.b, this, this.a);
            this.l.a();
            this.q = new com.agilebinary.mobilemonitor.device.android.device.receivers.f(this.d, this.b, this, this.a);
            this.q.a();
            return;
        }
        if (this.q != null) {
            this.q.b();
            this.q = null;
        }
        if (this.l != null) {
            this.l.b();
            this.l = null;
        }
    }

    public final void a(boolean z, String str, boolean z2, boolean z3) {
        this.n.a(z, str, z2, z3);
    }

    public final void b() {
        super.b();
        this.a.a(this.n);
        this.h.c();
        this.o = PendingIntent.getBroadcast(this.d, 0, new Intent("com.agilebinary.mobilemonitor.WATCHDOG_ACTION"), 0);
        this.f.setInexactRepeating(0, System.currentTimeMillis() + 1800000, 1800000, this.o);
    }

    public final void b(boolean z) {
        if (z) {
            this.n.b(this.a.v());
            this.n.b();
            return;
        }
        this.n.c();
    }

    public final void c() {
        this.f.cancel(this.o);
        this.a.a((p) null);
        this.h.f();
        super.c();
    }

    public final void c(boolean z) {
        this.h.a(z);
        if (z) {
            this.r = new d(this.d, this.b, this, this.a);
            this.r.a();
        } else if (this.r != null) {
            this.r.b();
            this.r = null;
        }
    }

    public final void d() {
        super.d();
        this.n.d();
    }

    public final void d(boolean z) {
        if (z) {
            this.k = new com.agilebinary.mobilemonitor.device.android.device.a.e(this.d, this.b, this, this.a);
            this.k.b();
        } else if (this.k != null) {
            this.k.c();
            this.k = null;
        }
    }

    public final com.agilebinary.mobilemonitor.device.a.b.a.a.d e() {
        return this.n.e();
    }

    public final void e(boolean z) {
    }

    public final void f(boolean z) {
    }

    public final void g(boolean z) {
        if (z) {
            this.m = new q(this.d, this.b, this, this.a);
            this.m.a();
            return;
        }
        if (this.m != null) {
            this.m.b();
        }
        this.m = null;
    }

    public final void h(boolean z) {
        if (z) {
            this.i = new e(this.d, this.b);
            this.i.a();
            return;
        }
        if (this.i != null) {
            this.i.b();
        }
        this.i = null;
    }

    public final void i(boolean z) {
        if (z) {
            this.j = new com.agilebinary.mobilemonitor.device.android.device.receivers.a(this.d, this.b);
            this.j.a();
            return;
        }
        if (this.j != null) {
            this.j.b();
        }
        this.j = null;
    }

    /* access modifiers changed from: protected */
    public final void j(boolean z) {
        if (z) {
            this.p = new v(this.d, this.b, this, this.a);
            this.p.a();
            this.p.b();
            return;
        }
        if (this.p != null) {
            this.p.c();
            this.p.d();
        }
        this.p = null;
    }
}
