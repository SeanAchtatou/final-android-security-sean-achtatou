package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public abstract class u extends s {
    private long a;

    public u(a aVar) {
        super(aVar);
        this.a = aVar.f();
    }

    public u(String str, String str2, long j) {
        super(str, str2);
        this.a = j;
    }

    public String a(c cVar) {
        return super.a(cVar) + "\nTime: " + cVar.a(this.a);
    }

    public void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
    }

    public final long i() {
        return this.a;
    }
}
