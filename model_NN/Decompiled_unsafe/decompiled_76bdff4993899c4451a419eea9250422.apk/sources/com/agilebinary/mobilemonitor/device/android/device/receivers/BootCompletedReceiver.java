package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;

public class BootCompletedReceiver extends BroadcastReceiver {
    private static final String a = f.a();

    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            BackgroundService.a(context, "EXTRA_START_FROM_BOOTCOMPLETERECEIVER", (Uri) null);
        }
    }
}
