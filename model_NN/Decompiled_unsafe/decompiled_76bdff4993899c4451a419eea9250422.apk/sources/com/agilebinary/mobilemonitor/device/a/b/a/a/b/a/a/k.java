package com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.h;

public final class k extends d {
    private g b;
    private int c;
    private int d;
    private byte[] e;
    private byte[] f;
    private boolean g;
    private h h;
    private boolean i;

    public k(h hVar) {
        this(hVar, false);
    }

    private k(h hVar, boolean z) {
        super(hVar);
        this.b = new g();
        this.c = 512;
        this.d = 0;
        this.e = new byte[this.c];
        this.f = new byte[1];
        this.h = null;
        this.i = false;
        this.h = hVar;
        g gVar = this.b;
        gVar.k = new m();
        gVar.k.a(gVar, 15);
        this.g = false;
        this.b.a = this.e;
        this.b.b = 0;
        this.b.c = 0;
    }

    public final int a() {
        if (a(this.f, 0, 1) == -1) {
            return -1;
        }
        return this.f[0] & 255;
    }

    public final int a(byte[] bArr, int i2, int i3) {
        int a;
        if (i3 == 0) {
            return 0;
        }
        this.b.e = bArr;
        this.b.f = i2;
        this.b.g = i3;
        do {
            if (this.b.c == 0 && !this.i) {
                this.b.b = 0;
                this.b.c = this.h.a(this.e, 0, this.c);
                if (this.b.c == -1) {
                    this.b.c = 0;
                    this.i = true;
                }
            }
            a = this.b.a(0);
            if (this.i && a == -5) {
                return -1;
            }
            if (a == 0 || a == 1) {
                if ((!this.i && a != 1) || this.b.g != i3) {
                    if (this.b.g != i3) {
                        break;
                    }
                } else {
                    return -1;
                }
            } else {
                throw new h("in" + "flating: " + this.b.i);
            }
        } while (a == 0);
        return i3 - this.b.g;
    }
}
