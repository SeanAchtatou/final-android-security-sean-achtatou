package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public final class f extends k {
    private double b;
    private double c;

    public f(a aVar) {
        super(aVar);
        this.b = aVar.g();
        this.c = aVar.g();
    }

    public f(String str, String str2, long j, double d, double d2, double d3, double d4, double d5, long j2, boolean z, boolean z2, String str3, d dVar) {
        super(str, str2, j, d, d2, d4, j2, z, z2, str3, dVar);
        this.b = d3;
        this.c = d5;
    }

    public final double a() {
        return this.b;
    }

    public final String a(c cVar) {
        return super.a(cVar) + "\nFixTimeUTC: " + cVar.a(this.a) + "\nAltitude: " + this.b + "\nVerticalAccuracy: " + this.c;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.b);
        aVar.a(this.c);
    }

    public final byte b() {
        return 5;
    }

    public final double c() {
        return this.c;
    }
}
