package com.agilebinary.mobilemonitor.device.android.ui;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.e.a;

final class v implements View.OnClickListener {
    private /* synthetic */ WatcherInstallActivity a;

    v(WatcherInstallActivity watcherInstallActivity) {
        this.a = watcherInstallActivity;
    }

    public final void onClick(View view) {
        try {
            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(b.a().t())));
        } catch (Exception e) {
            a.e(WatcherInstallActivity.b, "okmarket", e);
        }
        this.a.finish();
    }
}
