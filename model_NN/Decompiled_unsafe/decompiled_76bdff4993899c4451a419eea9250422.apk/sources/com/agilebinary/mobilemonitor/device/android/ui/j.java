package com.agilebinary.mobilemonitor.device.android.ui;

import android.net.Uri;
import android.view.View;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;

final class j implements View.OnClickListener {
    private /* synthetic */ MainActivity a;

    j(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void onClick(View view) {
        this.a.p.setEnabled(false);
        try {
            if (c.a().Y()) {
                BackgroundService.a(this.a, "EXTRA_SYNC_NOW", (Uri) null);
            }
        } finally {
            this.a.p.setEnabled(true);
        }
    }
}
