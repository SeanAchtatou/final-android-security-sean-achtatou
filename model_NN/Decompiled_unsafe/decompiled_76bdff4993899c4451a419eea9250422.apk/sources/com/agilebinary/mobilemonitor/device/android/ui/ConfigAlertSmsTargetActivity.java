package com.agilebinary.mobilemonitor.device.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.device.d;
import com.agilebinary.phonebeagle.R;

public class ConfigAlertSmsTargetActivity extends Activity implements View.OnClickListener {
    private static final String a = f.a();
    private EditText b;
    private Button c;

    public static boolean a(Activity activity) {
        c a2 = c.a();
        if (a2 == null) {
            d.a(activity);
            a2 = c.a();
        }
        if (a2.l() != null && a2.l().trim().length() != 0) {
            return false;
        }
        Intent intent = new Intent();
        intent.setClass(activity, ConfigAlertSmsTargetActivity.class);
        intent.setFlags(268435456);
        activity.startActivity(intent);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.c.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.f.a(com.agilebinary.mobilemonitor.device.a.a.h, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, long):void
      com.agilebinary.mobilemonitor.device.a.a.c.a(java.lang.String, boolean):void */
    public void onClick(View view) {
        c.a().a(this.b.getEditableText().toString(), true);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.enter_alert_sms_recipient_dialog);
        this.b = (EditText) findViewById(R.id.smsrec_number);
        this.c = (Button) findViewById(R.id.smsrec_ok);
        this.c.setOnClickListener(this);
    }
}
