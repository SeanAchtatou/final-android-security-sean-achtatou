package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public final class r extends b {
    private String a;

    public r(a aVar) {
        super(aVar);
        this.a = aVar.i();
    }

    public final String a() {
        return this.a;
    }

    public final String a(c cVar) {
        return super.a(cVar) + "\nCellName: " + this.a;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
    }

    public final byte b() {
        return 9;
    }
}
