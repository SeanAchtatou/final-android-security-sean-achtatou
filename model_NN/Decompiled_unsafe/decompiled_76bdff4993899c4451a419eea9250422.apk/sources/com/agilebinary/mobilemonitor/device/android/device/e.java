package com.agilebinary.mobilemonitor.device.android.device;

import android.app.PendingIntent;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.a.d;

final class e {
    private PendingIntent a;
    private d b;

    public e(j jVar, String str, d dVar, PendingIntent pendingIntent) {
        this.a = pendingIntent;
        this.b = dVar;
    }

    public final PendingIntent a() {
        return this.a;
    }

    public final void b() {
        try {
            this.b.a();
        } catch (Exception e) {
            a.e(j.c, "execute1", e);
        }
    }
}
