package com.agilebinary.mobilemonitor.device.android.ui;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
final class au extends Enum {
    public static final au a = new au("NOOP", 0);
    public static final au b = new au("ADD_DEVICE_ADMIN_DIALOG_SHOWN", 1);
    public static final au c = new au("ENTER_ALERT_TARGET_NUMBER_SHOWN", 2);
    public static final au d = new au("WAITING_FOR_WATCHER_INSTALL", 3);
    public static final au e = new au("WAITING_FOR_START_ACTIVITY_AFTER_WATCHER_INSTALL", 4);

    static {
        au[] auVarArr = {a, b, c, d, e};
    }

    private au(String str, int i) {
    }
}
