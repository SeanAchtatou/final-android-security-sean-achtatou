package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class e extends BroadcastReceiver {
    private static final String a = f.a();
    private Context b;
    private c c;
    private boolean d;

    public e(Context context, c cVar) {
        this.b = context;
        this.c = cVar;
    }

    public final void a() {
        if (!this.d) {
            this.d = true;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.ACTION_SHUTDOWN");
            intentFilter.addAction("android.intent.action.REBOOT");
            intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
            intentFilter.addAction("android.intent.action.BATTERY_OKAY");
            intentFilter.addAction("android.intent.action.BATTERY_LOW");
            this.b.registerReceiver(this, intentFilter);
        }
    }

    public final void b() {
        if (this.d) {
            this.d = false;
            this.b.unregisterReceiver(this);
        }
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        long currentTimeMillis = System.currentTimeMillis();
        try {
            if ("android.intent.action.ACTION_SHUTDOWN".equals(action)) {
                this.c.a(currentTimeMillis, 1);
            } else if ("android.intent.action.REBOOT".equals(action)) {
                this.c.a(currentTimeMillis, 3);
            } else if ("android.intent.action.AIRPLANE_MODE".equals(action)) {
                this.c.a(currentTimeMillis, intent.getBooleanExtra("state", false) ? 5 : 4);
            } else if ("android.intent.action.BATTERY_LOW".equals(action)) {
                this.c.a(currentTimeMillis, 6);
            } else if ("android.intent.action.BATTERY_OKAY".equals(action)) {
                this.c.a(currentTimeMillis, 7);
            }
        } catch (Exception e) {
            a.e(a, "exception in handleSystemEvent: ", e);
        }
    }
}
