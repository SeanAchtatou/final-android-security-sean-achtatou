package com.agilebinary.mobilemonitor.device.android.ui;

import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;

public abstract class at {
    public static final String a = f.a();
    private static final ThreadFactory b = new z();
    private static ExecutorService c = f();
    /* access modifiers changed from: private */
    public static final as d = new as();
    private final an e = new aa(this);
    private final FutureTask f = new x(this, this.e);
    private volatile ao g = ao.PENDING;

    static /* synthetic */ void a(at atVar, Object obj) {
        atVar.a(obj);
        atVar.g = ao.FINISHED;
    }

    public static void c() {
        try {
            c.shutdownNow();
        } catch (Exception e2) {
            a.e(a, "RESET", e2);
        }
        c = f();
    }

    private static ExecutorService f() {
        new LinkedBlockingQueue();
        return Executors.newCachedThreadPool(b);
    }

    /* access modifiers changed from: protected */
    public abstract Object a(Object... objArr);

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
    }

    public final boolean a(boolean z) {
        return this.f.cancel(true);
    }

    public final at b(Object... objArr) {
        if (this.g != ao.PENDING) {
            switch (y.a[this.g.ordinal()]) {
                case 1:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                case 2:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.g = ao.RUNNING;
        b();
        this.e.a = objArr;
        try {
            c.execute(this.f);
        } catch (RejectedExecutionException e2) {
            a();
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public final boolean d() {
        return this.f.isCancelled();
    }
}
