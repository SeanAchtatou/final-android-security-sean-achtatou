package com.agilebinary.mobilemonitor.device.android.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.phonebeagle.R;

public class LockActivity extends Activity {
    private TextView a;

    static {
        f.a();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.lock);
        findViewById(R.id.lock_message);
        this.a = (TextView) findViewById(R.id.lock_reason);
        String stringExtra = getIntent().getStringExtra("EXTRA_MSG");
        if (stringExtra == null || stringExtra.trim().length() == 0) {
            this.a.setVisibility(4);
            return;
        }
        this.a.setVisibility(0);
        this.a.setText(getString(R.string.app_lock_reason, new Object[]{stringExtra}));
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }
}
