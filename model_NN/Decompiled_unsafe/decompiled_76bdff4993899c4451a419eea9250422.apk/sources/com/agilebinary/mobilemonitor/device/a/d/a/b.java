package com.agilebinary.mobilemonitor.device.a.d.a;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.n;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.g.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.Hashtable;

public final class b implements d {
    public static final String a = f.a();
    /* access modifiers changed from: private */
    public n b;
    /* access modifiers changed from: private */
    public g c;
    /* access modifiers changed from: private */
    public a d;
    private com.agilebinary.mobilemonitor.device.a.a.b e;
    private c f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public Hashtable h;
    /* access modifiers changed from: private */
    public int i = 0;
    /* access modifiers changed from: private */
    public g j;

    public b(n nVar, g gVar, c cVar, com.agilebinary.mobilemonitor.device.a.a.b bVar) {
        this.b = nVar;
        this.c = gVar;
        this.d = new a(this, this);
        this.h = new Hashtable();
        this.e = bVar;
        this.f = cVar;
    }

    static /* synthetic */ int d(b bVar) {
        int i2 = bVar.i;
        bVar.i = i2 + 1;
        return i2;
    }

    private void h() {
        this.c.e(a);
        synchronized (this.d) {
            if (this.j == null) {
                this.j = new g(this);
                this.j.start();
            }
            this.d.notify();
        }
    }

    public final void a() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.mobilemonitor.device.a.b.b r10) {
        /*
            r9 = this;
            r8 = 2
            r1 = 0
            r0 = 1
            r10.c(r1)
            com.agilebinary.mobilemonitor.device.a.d.a.a r4 = r9.d
            monitor-enter(r4)
            java.util.Hashtable r2 = r9.h     // Catch:{ all -> 0x0033 }
            boolean r2 = r2.contains(r10)     // Catch:{ all -> 0x0033 }
            if (r2 == 0) goto L_0x0029
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0033 }
            r0.<init>()     // Catch:{ all -> 0x0033 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0033 }
            long r1 = r10.j()     // Catch:{ all -> 0x0033 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0033 }
            r0.toString()     // Catch:{ all -> 0x0033 }
            monitor-exit(r4)     // Catch:{ all -> 0x0033 }
        L_0x0028:
            return
        L_0x0029:
            com.agilebinary.mobilemonitor.device.a.d.a.a r2 = r9.d     // Catch:{ all -> 0x0033 }
            int r2 = r2.a(r10)     // Catch:{ all -> 0x0033 }
            if (r2 != 0) goto L_0x0036
            monitor-exit(r4)     // Catch:{ all -> 0x0033 }
            goto L_0x0028
        L_0x0033:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0033 }
            throw r0
        L_0x0036:
            if (r2 != r8) goto L_0x003b
            r9.h()     // Catch:{ all -> 0x0033 }
        L_0x003b:
            if (r2 != r0) goto L_0x0061
            com.agilebinary.mobilemonitor.device.a.d.a.a r2 = r9.d     // Catch:{ all -> 0x0033 }
            r3 = 1
            com.agilebinary.mobilemonitor.device.a.b.b r5 = r2.a(r3)     // Catch:{ all -> 0x0033 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0033 }
            r2.<init>()     // Catch:{ all -> 0x0033 }
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0033 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ all -> 0x0033 }
            r2.toString()     // Catch:{ all -> 0x0033 }
            if (r5 == 0) goto L_0x005e
            boolean r2 = r5.q()     // Catch:{ all -> 0x0033 }
            if (r2 != 0) goto L_0x0063
        L_0x005e:
            r9.h()     // Catch:{ all -> 0x0033 }
        L_0x0061:
            monitor-exit(r4)     // Catch:{ all -> 0x0033 }
            goto L_0x0028
        L_0x0063:
            long r2 = r5.j()     // Catch:{ all -> 0x0033 }
            long r6 = r10.j()     // Catch:{ all -> 0x0033 }
            int r2 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r2 == 0) goto L_0x0061
            int r2 = r10.l()     // Catch:{ all -> 0x0033 }
            if (r2 != r8) goto L_0x0089
            r3 = r0
        L_0x0076:
            int r2 = r5.l()     // Catch:{ all -> 0x0033 }
            if (r2 != r8) goto L_0x008b
            r2 = r0
        L_0x007d:
            if (r3 == 0) goto L_0x008d
            if (r2 != 0) goto L_0x008d
        L_0x0081:
            if (r0 == 0) goto L_0x0061
            com.agilebinary.mobilemonitor.device.a.b.n r0 = r9.b     // Catch:{ all -> 0x0033 }
            r0.b(r5)     // Catch:{ all -> 0x0033 }
            goto L_0x0061
        L_0x0089:
            r3 = r1
            goto L_0x0076
        L_0x008b:
            r2 = r1
            goto L_0x007d
        L_0x008d:
            if (r3 != 0) goto L_0x0091
            if (r2 != 0) goto L_0x009b
        L_0x0091:
            int r2 = r9.c(r10)     // Catch:{ all -> 0x0033 }
            int r3 = r9.c(r5)     // Catch:{ all -> 0x0033 }
            if (r2 < r3) goto L_0x0081
        L_0x009b:
            r0 = r1
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.d.a.b.a(com.agilebinary.mobilemonitor.device.a.b.b):void");
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.b bVar, int i2) {
        synchronized (this.d) {
            this.h.put(bVar, bVar);
            this.c.a("EvUpRtry", new d(this, bVar), null, (long) i2, true);
        }
    }

    public final void b() {
    }

    public final void b(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        "" + bVar.j();
        "" + this.i;
        synchronized (this.d) {
            if (this.d.b(bVar)) {
                this.i--;
            }
        }
        "" + this.i;
    }

    public final int c(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        return bVar.a(this.e);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.c.g.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.g.a.d):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.Thread, int):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.g.e):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, boolean):void */
    public final void c() {
        synchronized (this.d) {
            if (this.j != null) {
                this.j.a();
            }
            this.d.a();
            this.d.notify();
            this.c.a(a, true);
        }
    }

    public final int d(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        return bVar.a(this.f, this.e);
    }

    public final void d() {
    }

    public final boolean e() {
        boolean z;
        synchronized (this.d) {
            z = this.d.b() && this.h.isEmpty();
        }
        return z;
    }

    public final boolean f() {
        if (this.i <= 0) {
            return false;
        }
        this.g = true;
        return true;
    }

    public final void g() {
        if (this.j != null) {
            try {
                this.c.a(this.j, 30000);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
