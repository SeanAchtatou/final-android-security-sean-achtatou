package com.agilebinary.mobilemonitor.device.a.d.a;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.c.e;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.d.a;
import com.agilebinary.mobilemonitor.device.a.d.b;
import com.agilebinary.mobilemonitor.device.a.d.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.Enumeration;
import java.util.Vector;

public abstract class h extends b implements i {
    public static final String f = f.a();
    private com.agilebinary.mobilemonitor.device.a.j.a.b g;
    private long h;
    private long i;
    private int j;
    private Vector k;
    private com.agilebinary.mobilemonitor.device.a.b.b l = null;

    public h(e eVar, g gVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, com.agilebinary.mobilemonitor.device.a.a.b bVar2, c cVar) {
        super(0, eVar, gVar, bVar2, cVar);
        this.g = bVar;
        this.k = new Vector(1);
    }

    private int a(a aVar) {
        if (aVar == null) {
            return 1;
        }
        try {
            if (aVar.a(this.e)) {
                this.i = System.currentTimeMillis();
                this.j = q();
                u();
                return 0;
            }
            "" + aVar.b(this.e);
            return 102 == aVar.b(this.e) ? 2 : 1;
        } catch (d e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(f, "exception in eventProtocolStatusToLocalResultCode", e);
            return 2;
        }
    }

    private void u() {
        if (this.k.size() > 0) {
            Enumeration elements = this.k.elements();
            while (elements.hasMoreElements()) {
                ((e) elements.nextElement()).a(this.i, this.j);
            }
        }
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        int i2;
        this.h = System.currentTimeMillis();
        int i3 = 0;
        if (!bVar.b()) {
            try {
                this.g.a(bVar);
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e) {
                bVar.f(2);
                return;
            }
        }
        try {
            bVar.b(true);
            while (true) {
                int i4 = i3;
                if (q() != 0) {
                    int f2 = bVar.f();
                    int min = Math.min(this.b.e(q()), bVar.e().length - f2);
                    byte[] bArr = new byte[min];
                    System.arraycopy(bVar.e(), f2, bArr, 0, min);
                    "uploading a chunk of event " + bVar.j() + " bytes: " + f2 + " - " + ((f2 + min) - 1) + " (total-size=" + bVar.k() + ")";
                    String n = this.d.n();
                    String u = this.d.u();
                    boolean o = this.b.o();
                    boolean z = this.b.n() > 0;
                    long j2 = bVar.j();
                    int i5 = bVar.i();
                    int k2 = bVar.k();
                    int f3 = bVar.f();
                    StringBuffer l2 = l();
                    l2.append("SE-A");
                    l2.append("?");
                    l2.append("PV=");
                    l2.append(this.d.b("1"));
                    l2.append("&");
                    l2.append("K=");
                    l2.append(this.d.b(n));
                    l2.append("&");
                    l2.append("I=");
                    l2.append(this.d.b(u));
                    l2.append("&");
                    l2.append("E=");
                    l2.append(this.d.b(o ? "true" : "false"));
                    l2.append("&");
                    l2.append("C=");
                    l2.append(this.d.b(z ? "true" : "false"));
                    l2.append("&");
                    l2.append("TS=");
                    l2.append(this.d.b(Long.toString(j2)));
                    l2.append("&");
                    l2.append("ET=");
                    l2.append(this.d.b(Integer.toString(i5)));
                    l2.append("&");
                    l2.append("TL=");
                    l2.append(this.d.b(Integer.toString(k2)));
                    l2.append("&");
                    l2.append("OF=");
                    l2.append(this.d.b(Integer.toString(f3)));
                    i2 = a(a(l2.toString(), this.b.a(q(), 0) * 1000, bArr));
                    "" + i2;
                    bVar.f(i2);
                    if (i2 != 0) {
                        break;
                    }
                    this.i = System.currentTimeMillis();
                    this.j = q();
                    u();
                    bVar.a(f2 + min);
                    if (!bVar.d() && (i3 = i4 + 1) != bVar.m()) {
                        if (this.l != null && this.l.j() == bVar.j()) {
                            bVar.c(true);
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    bVar.f(1);
                    bVar.b(false);
                    this.l = null;
                    return;
                }
            }
        } catch (d e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(f, "exception in uploadChunked", e2);
            i2 = 2;
        } catch (Throwable th) {
            bVar.b(false);
            this.l = null;
            throw th;
        }
        bVar.b(false);
        this.l = null;
    }

    public final void a(e eVar) {
        this.k.addElement(eVar);
    }

    public final void a(f fVar) {
        if (q() != 0) {
            this.h = System.currentTimeMillis();
            byte[] a = fVar.a(this.g);
            try {
                String n = this.d.n();
                String u = this.d.u();
                boolean o = this.b.o();
                StringBuffer l2 = l();
                l2.append("SE-AA");
                l2.append("?");
                l2.append("PV=");
                l2.append(this.d.b("1"));
                l2.append("&");
                l2.append("K=");
                l2.append(this.d.b(n));
                l2.append("&");
                l2.append("I=");
                l2.append(this.d.b(u));
                l2.append("&");
                l2.append("E=");
                l2.append(this.d.b(o ? "true" : "false"));
                fVar.b(a(a(l2.toString(), this.b.a(q(), 0) * 1000, a)));
            } catch (d e) {
                fVar.b(2);
            }
            this.l = null;
        }
    }

    public final void b(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        this.l = bVar;
    }

    public final void b(e eVar) {
        this.k.removeElement(eVar);
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return "1";
    }

    public final String m() {
        return this.c.I();
    }

    public final String n() {
        return this.c.J();
    }

    public final String o() {
        return this.c.L();
    }

    public final int p() {
        String K = this.c.K();
        if (K == null || K.trim().length() == 0) {
            return 0;
        }
        return Integer.parseInt(K);
    }

    public final long r() {
        return this.h;
    }

    public final long s() {
        return this.i;
    }

    public final int t() {
        return this.j;
    }
}
