package com.agilebinary.mobilemonitor.device.a.d;

import com.agilebinary.mobilemonitor.device.a.c.e;
import com.agilebinary.mobilemonitor.device.a.g.f;

public abstract class a {
    private static String a = f.a();

    public abstract String a(String str);

    public final boolean a(e eVar) {
        return b() == 200 && (b(eVar) == 0 || a("SI-S-T") != null);
    }

    public abstract byte[] a();

    public abstract int b();

    public final int b(e eVar) {
        String a2 = a("SI-S");
        if (a2 == null) {
            return -2;
        }
        try {
            int parseInt = Integer.parseInt(a2);
            if (parseInt != 8 && parseInt != 9 && parseInt != 5 && parseInt != 7) {
                return parseInt;
            }
            if (eVar != null) {
                eVar.e();
            }
            throw new d(parseInt, eVar);
        } catch (NumberFormatException e) {
            return -2;
        }
    }

    public final String c() {
        return a("SI-S-T");
    }
}
