package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public final class o extends q {
    private long a;
    private String b;
    private String c;
    private boolean d;
    private int e;

    public o(a aVar) {
        super(aVar);
        this.a = aVar.f();
        this.b = aVar.i();
        this.c = aVar.i();
        this.d = aVar.b();
        this.e = aVar.e();
    }

    public o(String str, String str2, d dVar, long j, String str3, String str4, boolean z, int i) {
        super(str, str2, dVar);
        this.a = j;
        this.b = str3;
        this.c = str4;
        this.d = z;
        this.e = i;
    }

    public final String a(c cVar) {
        return super.a(cVar) + "\nTime: " + cVar.a(this.a) + "\nUrl: " + this.b + "\nTitle: " + this.c + "\nIsBookmark: " + this.d + "\nNumVisits: " + this.e;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
        aVar.a(this.d);
        aVar.a(this.e);
    }

    public final byte b() {
        return 14;
    }
}
