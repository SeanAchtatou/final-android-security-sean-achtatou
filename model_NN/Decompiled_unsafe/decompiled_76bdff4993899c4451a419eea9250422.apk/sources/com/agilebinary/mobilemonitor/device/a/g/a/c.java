package com.agilebinary.mobilemonitor.device.a.g.a;

import com.agilebinary.mobilemonitor.device.a.e.a;

final class c extends Thread {
    private /* synthetic */ b a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(b bVar) {
        super(b.a + "_" + bVar.d);
        this.a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.c.g.a(com.agilebinary.mobilemonitor.device.a.g.a.d, boolean):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.g.a.d):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.Thread, int):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(com.agilebinary.mobilemonitor.device.a.b.a.a.a.u[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.g.e):void
      com.agilebinary.mobilemonitor.device.a.c.g.a(java.lang.String, boolean):void */
    public final void run() {
        d dVar = null;
        this.a.c.I();
        try {
            this.a.c.e(this.a.d);
            synchronized (this.a.b) {
                if (this.a.b.size() >= 0) {
                    dVar = (d) this.a.b.elementAt(0);
                }
            }
            b.b(dVar);
            if (this.a.b != null) {
                synchronized (this.a.b) {
                    if (this.a.b.size() > 0) {
                        this.a.b.removeElementAt(0);
                    }
                }
            }
            try {
                this.a.c.a(this.a.d, true);
            } catch (Exception e) {
                a.e(b.a, "aarrrgh, error in QueuedExecutorThread, release wake lock", e);
            }
        } catch (Throwable th) {
            try {
                a.e(b.a, "aarrrgh, error in QueuedExecutorThread", th);
                try {
                } catch (Exception e2) {
                    a.e(b.a, "aarrrgh, error in QueuedExecutorThread, release wake lock", e2);
                }
            } finally {
                try {
                    this.a.c.a(this.a.d, true);
                } catch (Exception e3) {
                    a.e(b.a, "aarrrgh, error in QueuedExecutorThread, release wake lock", e3);
                }
            }
        }
        if (this.a.b != null) {
            synchronized (this.a.b) {
                c unused = this.a.e = null;
            }
        }
        this.a.f();
    }
}
