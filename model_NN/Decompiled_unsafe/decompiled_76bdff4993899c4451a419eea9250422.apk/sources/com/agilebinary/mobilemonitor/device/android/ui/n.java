package com.agilebinary.mobilemonitor.device.android.ui;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.f.b;

final class n extends at {
    private /* synthetic */ MainActivity b;

    n(MainActivity mainActivity) {
        this.b = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.b.a(boolean, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, long, byte[]):com.agilebinary.mobilemonitor.device.a.d.a
      com.agilebinary.mobilemonitor.device.a.d.b.a(boolean, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object a(Object[] objArr) {
        boolean z = true;
        this.b.C.a(true, true, true);
        if (d()) {
            return null;
        }
        int q = this.b.C.q();
        b bVar = new b();
        if (q == 0) {
            z = false;
        }
        bVar.a = z;
        bVar.b = b.a("CONNECTION_TYPE_" + q);
        return bVar;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        try {
            this.b.t.hide();
        } catch (Exception e) {
            a.e(a, "onCancelled1", e);
        }
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        b bVar = (b) obj;
        super.a(bVar);
        try {
            this.b.t.hide();
            if (bVar == null || !bVar.a || d()) {
                this.b.A.add(new ap(b.a("CONNECTION_TEST_FAILED")));
                return;
            }
            this.b.A.add(new ap(b.a("CONNECTION_TEST_SUCCESS", bVar.b)));
            if (c.a().Y()) {
                new ai(this.b).b(new Void[0]);
            }
        } catch (Exception e) {
            a.e(a, "onPostExecute2", e);
        }
    }
}
