package com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a;

import bsh.ParserConstants;

final class a {
    private static final int[] a = {0, 1, 3, 7, 15, 31, 63, ParserConstants.MODASSIGN, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767, 65535};
    private int b;
    private int c;
    private int[] d;
    private int e = 0;
    private int f;
    private int g;
    private int h;
    private int i;
    private byte j;
    private byte k;
    private int[] l;
    private int m;
    private int[] n;
    private int o;

    a() {
    }

    /* JADX WARN: Type inference failed for: r5v11, types: [int] */
    /* JADX WARN: Type inference failed for: r6v25 */
    /* JADX WARN: Type inference failed for: r5v18, types: [int] */
    /* JADX WARN: Type inference failed for: r5v24, types: [int] */
    /* JADX WARN: Type inference failed for: r5v31, types: [int] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0126, code lost:
        if ((r5 >> 3) >= r2) goto L_0x012a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0128, code lost:
        r2 = r5 >> 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x012a, code lost:
        r4 = r4 - r2;
        r22.b = r6;
        r22.a = r5 - (r2 << 3);
        r23.c = r3 + r2;
        r23.d += (long) (r4 - r23.b);
        r23.b = r4;
        r22.f = r7;
        r24 = 0;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.b r22, com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.g r23, int r24) {
        /*
            r21 = this;
            r0 = r23
            int r5 = r0.b
            r0 = r23
            int r4 = r0.c
            r0 = r22
            int r7 = r0.b
            r0 = r22
            int r6 = r0.a
            r0 = r22
            int r3 = r0.f
            r0 = r22
            int r2 = r0.e
            if (r3 >= r2) goto L_0x0056
            r0 = r22
            int r2 = r0.e
            int r2 = r2 - r3
            int r2 = r2 + -1
        L_0x0021:
            r0 = r21
            int r8 = r0.b
            switch(r8) {
                case 0: goto L_0x005c;
                case 1: goto L_0x03eb;
                case 2: goto L_0x04e9;
                case 3: goto L_0x055f;
                case 4: goto L_0x063d;
                case 5: goto L_0x069b;
                case 6: goto L_0x076d;
                case 7: goto L_0x081d;
                case 8: goto L_0x0870;
                case 9: goto L_0x089f;
                default: goto L_0x0028;
            }
        L_0x0028:
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r6
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r8 = (long) r2
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            r2 = -2
            r0 = r22
            r1 = r23
            int r2 = r0.b(r1, r2)
        L_0x0055:
            return r2
        L_0x0056:
            r0 = r22
            int r2 = r0.d
            int r2 = r2 - r3
            goto L_0x0021
        L_0x005c:
            r8 = 258(0x102, float:3.62E-43)
            if (r2 < r8) goto L_0x03ce
            r8 = 10
            if (r4 < r8) goto L_0x03ce
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r6
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r8 = (long) r2
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            r0 = r21
            byte r8 = r0.j
            r0 = r21
            byte r9 = r0.k
            r0 = r21
            int[] r14 = r0.l
            r0 = r21
            int r15 = r0.m
            r0 = r21
            int[] r0 = r0.n
            r16 = r0
            r0 = r21
            int r0 = r0.o
            r17 = r0
            r0 = r23
            int r4 = r0.b
            r0 = r23
            int r3 = r0.c
            r0 = r22
            int r6 = r0.b
            r0 = r22
            int r5 = r0.a
            r0 = r22
            int r7 = r0.f
            r0 = r22
            int r2 = r0.e
            if (r7 >= r2) goto L_0x00e9
            r0 = r22
            int r2 = r0.e
            int r2 = r2 - r7
            int r2 = r2 + -1
        L_0x00c5:
            int[] r10 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r18 = r10[r8]
            int[] r8 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r19 = r8[r9]
            r8 = r2
            r9 = r3
            r10 = r4
            r3 = r5
            r4 = r6
        L_0x00d2:
            r2 = 20
            if (r3 >= r2) goto L_0x00ef
            int r2 = r9 + -1
            r0 = r23
            byte[] r6 = r0.a
            int r5 = r10 + 1
            byte r6 = r6[r10]
            r6 = r6 & 255(0xff, float:3.57E-43)
            int r6 = r6 << r3
            r4 = r4 | r6
            int r3 = r3 + 8
            r9 = r2
            r10 = r5
            goto L_0x00d2
        L_0x00e9:
            r0 = r22
            int r2 = r0.d
            int r2 = r2 - r7
            goto L_0x00c5
        L_0x00ef:
            r6 = r4 & r18
            int r2 = r15 + r6
            int r2 = r2 * 3
            r5 = r14[r2]
            if (r5 != 0) goto L_0x0185
            int r5 = r2 + 1
            r5 = r14[r5]
            int r6 = r4 >> r5
            int r4 = r2 + 1
            r4 = r14[r4]
            int r5 = r3 - r4
            r0 = r22
            byte[] r3 = r0.c
            int r4 = r7 + 1
            int r2 = r2 + 2
            r2 = r14[r2]
            byte r2 = (byte) r2
            r3[r7] = r2
            int r2 = r8 + -1
            r7 = r4
            r3 = r9
            r4 = r10
        L_0x0117:
            r8 = 258(0x102, float:3.62E-43)
            if (r2 < r8) goto L_0x011f
            r8 = 10
            if (r3 >= r8) goto L_0x08d1
        L_0x011f:
            r0 = r23
            int r2 = r0.c
            int r2 = r2 - r3
            int r8 = r5 >> 3
            if (r8 >= r2) goto L_0x012a
            int r2 = r5 >> 3
        L_0x012a:
            int r3 = r3 + r2
            int r4 = r4 - r2
            int r2 = r2 << 3
            int r2 = r5 - r2
            r0 = r22
            r0.b = r6
            r0 = r22
            r0.a = r2
            r0 = r23
            r0.c = r3
            r0 = r23
            long r2 = r0.d
            r0 = r23
            int r5 = r0.b
            int r5 = r4 - r5
            long r5 = (long) r5
            long r2 = r2 + r5
            r0 = r23
            r0.d = r2
            r0 = r23
            r0.b = r4
            r0 = r22
            r0.f = r7
            r24 = 0
        L_0x0156:
            r0 = r23
            int r5 = r0.b
            r0 = r23
            int r4 = r0.c
            r0 = r22
            int r7 = r0.b
            r0 = r22
            int r6 = r0.a
            r0 = r22
            int r3 = r0.f
            r0 = r22
            int r2 = r0.e
            if (r3 >= r2) goto L_0x03c3
            r0 = r22
            int r2 = r0.e
            int r2 = r2 - r3
            int r2 = r2 + -1
        L_0x0177:
            if (r24 == 0) goto L_0x03ce
            r8 = 1
            r0 = r24
            if (r0 != r8) goto L_0x03ca
            r8 = 7
        L_0x017f:
            r0 = r21
            r0.b = r8
            goto L_0x0021
        L_0x0185:
            int r11 = r2 + 1
            r11 = r14[r11]
            int r4 = r4 >> r11
            int r11 = r2 + 1
            r11 = r14[r11]
            int r3 = r3 - r11
            r11 = r5 & 16
            if (r11 == 0) goto L_0x030b
            r5 = r5 & 15
            int r2 = r2 + 2
            r2 = r14[r2]
            int[] r6 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r6 = r6[r5]
            r6 = r6 & r4
            int r13 = r2 + r6
            int r4 = r4 >> r5
            int r3 = r3 - r5
        L_0x01a2:
            r2 = 15
            if (r3 >= r2) goto L_0x01b8
            int r9 = r9 + -1
            r0 = r23
            byte[] r5 = r0.a
            int r2 = r10 + 1
            byte r5 = r5[r10]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r5 = r5 << r3
            r4 = r4 | r5
            int r3 = r3 + 8
            r10 = r2
            goto L_0x01a2
        L_0x01b8:
            r6 = r4 & r19
            int r2 = r17 + r6
            int r2 = r2 * 3
            r5 = r16[r2]
        L_0x01c0:
            int r11 = r2 + 1
            r11 = r16[r11]
            int r4 = r4 >> r11
            int r11 = r2 + 1
            r11 = r16[r11]
            int r3 = r3 - r11
            r11 = r5 & 16
            if (r11 == 0) goto L_0x02b2
            r6 = r5 & 15
            r5 = r4
            r4 = r3
        L_0x01d2:
            if (r4 >= r6) goto L_0x01e6
            int r9 = r9 + -1
            r0 = r23
            byte[] r11 = r0.a
            int r3 = r10 + 1
            byte r10 = r11[r10]
            r10 = r10 & 255(0xff, float:3.57E-43)
            int r10 = r10 << r4
            r5 = r5 | r10
            int r4 = r4 + 8
            r10 = r3
            goto L_0x01d2
        L_0x01e6:
            int r2 = r2 + 2
            r2 = r16[r2]
            int[] r3 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r3 = r3[r6]
            r3 = r3 & r5
            int r2 = r2 + r3
            int r12 = r5 >> r6
            int r11 = r4 - r6
            int r8 = r8 - r13
            if (r7 < r2) goto L_0x025c
            int r2 = r7 - r2
            int r3 = r7 - r2
            if (r3 <= 0) goto L_0x0249
            r3 = 2
            int r4 = r7 - r2
            if (r3 <= r4) goto L_0x0249
            r0 = r22
            byte[] r3 = r0.c
            int r5 = r7 + 1
            r0 = r22
            byte[] r4 = r0.c
            int r6 = r2 + 1
            byte r2 = r4[r2]
            r3[r7] = r2
            r0 = r22
            byte[] r2 = r0.c
            int r3 = r5 + 1
            r0 = r22
            byte[] r7 = r0.c
            int r4 = r6 + 1
            byte r6 = r7[r6]
            r2[r5] = r6
            int r2 = r13 + -2
        L_0x0224:
            int r5 = r3 - r4
            if (r5 <= 0) goto L_0x029e
            int r5 = r3 - r4
            if (r2 <= r5) goto L_0x029e
            r5 = r4
        L_0x022d:
            r0 = r22
            byte[] r7 = r0.c
            int r4 = r3 + 1
            r0 = r22
            byte[] r13 = r0.c
            int r6 = r5 + 1
            byte r5 = r13[r5]
            r7[r3] = r5
            int r2 = r2 + -1
            if (r2 != 0) goto L_0x08d8
            r2 = r8
            r7 = r4
            r3 = r9
            r5 = r11
            r6 = r12
            r4 = r10
            goto L_0x0117
        L_0x0249:
            r0 = r22
            byte[] r3 = r0.c
            r0 = r22
            byte[] r4 = r0.c
            r5 = 2
            java.lang.System.arraycopy(r3, r2, r4, r7, r5)
            int r3 = r7 + 2
            int r4 = r2 + 2
            int r2 = r13 + -2
            goto L_0x0224
        L_0x025c:
            int r2 = r7 - r2
        L_0x025e:
            r0 = r22
            int r3 = r0.d
            int r2 = r2 + r3
            if (r2 < 0) goto L_0x025e
            r0 = r22
            int r3 = r0.d
            int r3 = r3 - r2
            if (r13 <= r3) goto L_0x08e1
            int r13 = r13 - r3
            int r4 = r7 - r2
            if (r4 <= 0) goto L_0x0291
            int r4 = r7 - r2
            if (r3 <= r4) goto L_0x0291
            r4 = r3
            r5 = r2
            r2 = r7
        L_0x0278:
            r0 = r22
            byte[] r7 = r0.c
            int r3 = r2 + 1
            r0 = r22
            byte[] r0 = r0.c
            r20 = r0
            int r6 = r5 + 1
            byte r5 = r20[r5]
            r7[r2] = r5
            int r2 = r4 + -1
            if (r2 != 0) goto L_0x08dc
        L_0x028e:
            r4 = 0
            r2 = r13
            goto L_0x0224
        L_0x0291:
            r0 = r22
            byte[] r4 = r0.c
            r0 = r22
            byte[] r5 = r0.c
            java.lang.System.arraycopy(r4, r2, r5, r7, r3)
            int r3 = r3 + r7
            goto L_0x028e
        L_0x029e:
            r0 = r22
            byte[] r5 = r0.c
            r0 = r22
            byte[] r6 = r0.c
            java.lang.System.arraycopy(r5, r4, r6, r3, r2)
            int r7 = r3 + r2
            r2 = r8
            r3 = r9
            r4 = r10
            r5 = r11
            r6 = r12
            goto L_0x0117
        L_0x02b2:
            r11 = r5 & 64
            if (r11 != 0) goto L_0x02ca
            int r2 = r2 + 2
            r2 = r16[r2]
            int r2 = r2 + r6
            int[] r6 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r5 = r6[r5]
            r5 = r5 & r4
            int r6 = r2 + r5
            int r2 = r17 + r6
            int r2 = r2 * 3
            r5 = r16[r2]
            goto L_0x01c0
        L_0x02ca:
            java.lang.String r2 = "invalid distance code"
            r0 = r23
            r0.i = r2
            r0 = r23
            int r2 = r0.c
            int r2 = r2 - r9
            int r5 = r3 >> 3
            if (r5 >= r2) goto L_0x02db
            int r2 = r3 >> 3
        L_0x02db:
            int r5 = r9 + r2
            int r6 = r10 - r2
            int r2 = r2 << 3
            int r2 = r3 - r2
            r0 = r22
            r0.b = r4
            r0 = r22
            r0.a = r2
            r0 = r23
            r0.c = r5
            r0 = r23
            long r2 = r0.d
            r0 = r23
            int r4 = r0.b
            int r4 = r6 - r4
            long r4 = (long) r4
            long r2 = r2 + r4
            r0 = r23
            r0.d = r2
            r0 = r23
            r0.b = r6
            r0 = r22
            r0.f = r7
            r24 = -3
            goto L_0x0156
        L_0x030b:
            r11 = r5 & 64
            if (r11 != 0) goto L_0x0343
            int r2 = r2 + 2
            r2 = r14[r2]
            int r2 = r2 + r6
            int[] r6 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r5 = r6[r5]
            r5 = r5 & r4
            int r6 = r2 + r5
            int r2 = r15 + r6
            int r2 = r2 * 3
            r5 = r14[r2]
            if (r5 != 0) goto L_0x0185
            int r5 = r2 + 1
            r5 = r14[r5]
            int r6 = r4 >> r5
            int r4 = r2 + 1
            r4 = r14[r4]
            int r5 = r3 - r4
            r0 = r22
            byte[] r3 = r0.c
            int r4 = r7 + 1
            int r2 = r2 + 2
            r2 = r14[r2]
            byte r2 = (byte) r2
            r3[r7] = r2
            int r2 = r8 + -1
            r7 = r4
            r3 = r9
            r4 = r10
            goto L_0x0117
        L_0x0343:
            r2 = r5 & 32
            if (r2 == 0) goto L_0x0382
            r0 = r23
            int r2 = r0.c
            int r2 = r2 - r9
            int r5 = r3 >> 3
            if (r5 >= r2) goto L_0x0352
            int r2 = r3 >> 3
        L_0x0352:
            int r5 = r9 + r2
            int r6 = r10 - r2
            int r2 = r2 << 3
            int r2 = r3 - r2
            r0 = r22
            r0.b = r4
            r0 = r22
            r0.a = r2
            r0 = r23
            r0.c = r5
            r0 = r23
            long r2 = r0.d
            r0 = r23
            int r4 = r0.b
            int r4 = r6 - r4
            long r4 = (long) r4
            long r2 = r2 + r4
            r0 = r23
            r0.d = r2
            r0 = r23
            r0.b = r6
            r0 = r22
            r0.f = r7
            r24 = 1
            goto L_0x0156
        L_0x0382:
            java.lang.String r2 = "invalid literal/length code"
            r0 = r23
            r0.i = r2
            r0 = r23
            int r2 = r0.c
            int r2 = r2 - r9
            int r5 = r3 >> 3
            if (r5 >= r2) goto L_0x0393
            int r2 = r3 >> 3
        L_0x0393:
            int r5 = r9 + r2
            int r6 = r10 - r2
            int r2 = r2 << 3
            int r2 = r3 - r2
            r0 = r22
            r0.b = r4
            r0 = r22
            r0.a = r2
            r0 = r23
            r0.c = r5
            r0 = r23
            long r2 = r0.d
            r0 = r23
            int r4 = r0.b
            int r4 = r6 - r4
            long r4 = (long) r4
            long r2 = r2 + r4
            r0 = r23
            r0.d = r2
            r0 = r23
            r0.b = r6
            r0 = r22
            r0.f = r7
            r24 = -3
            goto L_0x0156
        L_0x03c3:
            r0 = r22
            int r2 = r0.d
            int r2 = r2 - r3
            goto L_0x0177
        L_0x03ca:
            r8 = 9
            goto L_0x017f
        L_0x03ce:
            r0 = r21
            byte r8 = r0.j
            r0 = r21
            r0.f = r8
            r0 = r21
            int[] r8 = r0.l
            r0 = r21
            r0.d = r8
            r0 = r21
            int r8 = r0.m
            r0 = r21
            r0.e = r8
            r8 = 1
            r0 = r21
            r0.b = r8
        L_0x03eb:
            r0 = r21
            int r9 = r0.f
            r8 = r6
        L_0x03f0:
            if (r8 >= r9) goto L_0x0433
            if (r4 == 0) goto L_0x0409
            r24 = 0
            int r4 = r4 + -1
            r0 = r23
            byte[] r10 = r0.a
            int r6 = r5 + 1
            byte r5 = r10[r5]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r5 = r5 << r8
            r7 = r7 | r5
            int r5 = r8 + 8
            r8 = r5
            r5 = r6
            goto L_0x03f0
        L_0x0409:
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r8
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r8 = (long) r2
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            int r2 = r22.b(r23, r24)
            goto L_0x0055
        L_0x0433:
            r0 = r21
            int r6 = r0.e
            int[] r10 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r9 = r10[r9]
            r9 = r9 & r7
            int r6 = r6 + r9
            int r9 = r6 * 3
            r0 = r21
            int[] r6 = r0.d
            int r10 = r9 + 1
            r6 = r6[r10]
            int r7 = r7 >>> r6
            r0 = r21
            int[] r6 = r0.d
            int r10 = r9 + 1
            r6 = r6[r10]
            int r6 = r8 - r6
            r0 = r21
            int[] r8 = r0.d
            r8 = r8[r9]
            if (r8 != 0) goto L_0x046d
            r0 = r21
            int[] r8 = r0.d
            int r9 = r9 + 2
            r8 = r8[r9]
            r0 = r21
            r0.g = r8
            r8 = 6
            r0 = r21
            r0.b = r8
            goto L_0x0021
        L_0x046d:
            r10 = r8 & 16
            if (r10 == 0) goto L_0x048a
            r8 = r8 & 15
            r0 = r21
            r0.h = r8
            r0 = r21
            int[] r8 = r0.d
            int r9 = r9 + 2
            r8 = r8[r9]
            r0 = r21
            r0.c = r8
            r8 = 2
            r0 = r21
            r0.b = r8
            goto L_0x0021
        L_0x048a:
            r10 = r8 & 64
            if (r10 != 0) goto L_0x04a3
            r0 = r21
            r0.f = r8
            int r8 = r9 / 3
            r0 = r21
            int[] r10 = r0.d
            int r9 = r9 + 2
            r9 = r10[r9]
            int r8 = r8 + r9
            r0 = r21
            r0.e = r8
            goto L_0x0021
        L_0x04a3:
            r8 = r8 & 32
            if (r8 == 0) goto L_0x04ae
            r8 = 7
            r0 = r21
            r0.b = r8
            goto L_0x0021
        L_0x04ae:
            r2 = 9
            r0 = r21
            r0.b = r2
            java.lang.String r2 = "invalid literal/length code"
            r0 = r23
            r0.i = r2
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r6
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r8 = (long) r2
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            r2 = -3
            r0 = r22
            r1 = r23
            int r2 = r0.b(r1, r2)
            goto L_0x0055
        L_0x04e9:
            r0 = r21
            int r9 = r0.h
            r8 = r6
        L_0x04ee:
            if (r8 >= r9) goto L_0x0531
            if (r4 == 0) goto L_0x0507
            r24 = 0
            int r4 = r4 + -1
            r0 = r23
            byte[] r10 = r0.a
            int r6 = r5 + 1
            byte r5 = r10[r5]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r5 = r5 << r8
            r7 = r7 | r5
            int r5 = r8 + 8
            r8 = r5
            r5 = r6
            goto L_0x04ee
        L_0x0507:
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r8
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r8 = (long) r2
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            int r2 = r22.b(r23, r24)
            goto L_0x0055
        L_0x0531:
            r0 = r21
            int r6 = r0.c
            int[] r10 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r10 = r10[r9]
            r10 = r10 & r7
            int r6 = r6 + r10
            r0 = r21
            r0.c = r6
            int r7 = r7 >> r9
            int r6 = r8 - r9
            r0 = r21
            byte r8 = r0.k
            r0 = r21
            r0.f = r8
            r0 = r21
            int[] r8 = r0.n
            r0 = r21
            r0.d = r8
            r0 = r21
            int r8 = r0.o
            r0 = r21
            r0.e = r8
            r8 = 3
            r0 = r21
            r0.b = r8
        L_0x055f:
            r0 = r21
            int r9 = r0.f
            r8 = r6
        L_0x0564:
            if (r8 >= r9) goto L_0x05a7
            if (r4 == 0) goto L_0x057d
            r24 = 0
            int r4 = r4 + -1
            r0 = r23
            byte[] r10 = r0.a
            int r6 = r5 + 1
            byte r5 = r10[r5]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r5 = r5 << r8
            r7 = r7 | r5
            int r5 = r8 + 8
            r8 = r5
            r5 = r6
            goto L_0x0564
        L_0x057d:
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r8
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r8 = (long) r2
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            int r2 = r22.b(r23, r24)
            goto L_0x0055
        L_0x05a7:
            r0 = r21
            int r6 = r0.e
            int[] r10 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r9 = r10[r9]
            r9 = r9 & r7
            int r6 = r6 + r9
            int r9 = r6 * 3
            r0 = r21
            int[] r6 = r0.d
            int r10 = r9 + 1
            r6 = r6[r10]
            int r7 = r7 >> r6
            r0 = r21
            int[] r6 = r0.d
            int r10 = r9 + 1
            r6 = r6[r10]
            int r6 = r8 - r6
            r0 = r21
            int[] r8 = r0.d
            r8 = r8[r9]
            r10 = r8 & 16
            if (r10 == 0) goto L_0x05e9
            r8 = r8 & 15
            r0 = r21
            r0.h = r8
            r0 = r21
            int[] r8 = r0.d
            int r9 = r9 + 2
            r8 = r8[r9]
            r0 = r21
            r0.i = r8
            r8 = 4
            r0 = r21
            r0.b = r8
            goto L_0x0021
        L_0x05e9:
            r10 = r8 & 64
            if (r10 != 0) goto L_0x0602
            r0 = r21
            r0.f = r8
            int r8 = r9 / 3
            r0 = r21
            int[] r10 = r0.d
            int r9 = r9 + 2
            r9 = r10[r9]
            int r8 = r8 + r9
            r0 = r21
            r0.e = r8
            goto L_0x0021
        L_0x0602:
            r2 = 9
            r0 = r21
            r0.b = r2
            java.lang.String r2 = "invalid distance code"
            r0 = r23
            r0.i = r2
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r6
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r8 = (long) r2
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            r2 = -3
            r0 = r22
            r1 = r23
            int r2 = r0.b(r1, r2)
            goto L_0x0055
        L_0x063d:
            r0 = r21
            int r9 = r0.h
            r8 = r6
        L_0x0642:
            if (r8 >= r9) goto L_0x0685
            if (r4 == 0) goto L_0x065b
            r24 = 0
            int r4 = r4 + -1
            r0 = r23
            byte[] r10 = r0.a
            int r6 = r5 + 1
            byte r5 = r10[r5]
            r5 = r5 & 255(0xff, float:3.57E-43)
            int r5 = r5 << r8
            r7 = r7 | r5
            int r5 = r8 + 8
            r8 = r5
            r5 = r6
            goto L_0x0642
        L_0x065b:
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r8
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r8 = (long) r2
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            int r2 = r22.b(r23, r24)
            goto L_0x0055
        L_0x0685:
            r0 = r21
            int r6 = r0.i
            int[] r10 = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a
            r10 = r10[r9]
            r10 = r10 & r7
            int r6 = r6 + r10
            r0 = r21
            r0.i = r6
            int r7 = r7 >> r9
            int r6 = r8 - r9
            r8 = 5
            r0 = r21
            r0.b = r8
        L_0x069b:
            r0 = r21
            int r8 = r0.i
            int r8 = r3 - r8
        L_0x06a1:
            if (r8 >= 0) goto L_0x06cd
            r0 = r22
            int r9 = r0.d
            int r8 = r8 + r9
            goto L_0x06a1
        L_0x06a9:
            r9 = r3
            r0 = r22
            byte[] r11 = r0.c
            int r3 = r9 + 1
            r0 = r22
            byte[] r12 = r0.c
            int r10 = r8 + 1
            byte r8 = r12[r8]
            r11[r9] = r8
            int r2 = r2 + -1
            r0 = r22
            int r8 = r0.d
            if (r10 != r8) goto L_0x08ce
            r8 = 0
        L_0x06c3:
            r0 = r21
            int r9 = r0.c
            int r9 = r9 + -1
            r0 = r21
            r0.c = r9
        L_0x06cd:
            r0 = r21
            int r9 = r0.c
            if (r9 == 0) goto L_0x0766
            if (r2 != 0) goto L_0x06a9
            r0 = r22
            int r9 = r0.d
            if (r3 != r9) goto L_0x06f0
            r0 = r22
            int r9 = r0.e
            if (r9 == 0) goto L_0x06f0
            r3 = 0
            r0 = r22
            int r2 = r0.e
            if (r2 <= 0) goto L_0x0752
            r0 = r22
            int r2 = r0.e
            int r2 = r2 + 0
            int r2 = r2 + -1
        L_0x06f0:
            if (r2 != 0) goto L_0x06a9
            r0 = r22
            r0.f = r3
            int r24 = r22.b(r23, r24)
            r0 = r22
            int r3 = r0.f
            r0 = r22
            int r2 = r0.e
            if (r3 >= r2) goto L_0x0759
            r0 = r22
            int r2 = r0.e
            int r2 = r2 - r3
            int r2 = r2 + -1
        L_0x070b:
            r0 = r22
            int r9 = r0.d
            if (r3 != r9) goto L_0x0726
            r0 = r22
            int r9 = r0.e
            if (r9 == 0) goto L_0x0726
            r3 = 0
            r0 = r22
            int r2 = r0.e
            if (r2 <= 0) goto L_0x075f
            r0 = r22
            int r2 = r0.e
            int r2 = r2 + 0
            int r2 = r2 + -1
        L_0x0726:
            if (r2 != 0) goto L_0x06a9
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r6
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r8 = (long) r2
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            int r2 = r22.b(r23, r24)
            goto L_0x0055
        L_0x0752:
            r0 = r22
            int r2 = r0.d
            int r2 = r2 + 0
            goto L_0x06f0
        L_0x0759:
            r0 = r22
            int r2 = r0.d
            int r2 = r2 - r3
            goto L_0x070b
        L_0x075f:
            r0 = r22
            int r2 = r0.d
            int r2 = r2 + 0
            goto L_0x0726
        L_0x0766:
            r8 = 0
            r0 = r21
            r0.b = r8
            goto L_0x0021
        L_0x076d:
            if (r2 != 0) goto L_0x0804
            r0 = r22
            int r8 = r0.d
            if (r3 != r8) goto L_0x078a
            r0 = r22
            int r8 = r0.e
            if (r8 == 0) goto L_0x078a
            r3 = 0
            r0 = r22
            int r2 = r0.e
            if (r2 <= 0) goto L_0x07f0
            r0 = r22
            int r2 = r0.e
            int r2 = r2 + 0
            int r2 = r2 + -1
        L_0x078a:
            if (r2 != 0) goto L_0x0804
            r0 = r22
            r0.f = r3
            int r8 = r22.b(r23, r24)
            r0 = r22
            int r3 = r0.f
            r0 = r22
            int r2 = r0.e
            if (r3 >= r2) goto L_0x07f7
            r0 = r22
            int r2 = r0.e
            int r2 = r2 - r3
            int r2 = r2 + -1
        L_0x07a5:
            r0 = r22
            int r9 = r0.d
            if (r3 != r9) goto L_0x07c0
            r0 = r22
            int r9 = r0.e
            if (r9 == 0) goto L_0x07c0
            r3 = 0
            r0 = r22
            int r2 = r0.e
            if (r2 <= 0) goto L_0x07fd
            r0 = r22
            int r2 = r0.e
            int r2 = r2 + 0
            int r2 = r2 + -1
        L_0x07c0:
            if (r2 != 0) goto L_0x0804
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r6
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r9 = (long) r2
            long r6 = r6 + r9
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            r0 = r22
            r1 = r23
            int r2 = r0.b(r1, r8)
            goto L_0x0055
        L_0x07f0:
            r0 = r22
            int r2 = r0.d
            int r2 = r2 + 0
            goto L_0x078a
        L_0x07f7:
            r0 = r22
            int r2 = r0.d
            int r2 = r2 - r3
            goto L_0x07a5
        L_0x07fd:
            r0 = r22
            int r2 = r0.d
            int r2 = r2 + 0
            goto L_0x07c0
        L_0x0804:
            r8 = r3
            r24 = 0
            r0 = r22
            byte[] r9 = r0.c
            int r3 = r8 + 1
            r0 = r21
            int r10 = r0.g
            byte r10 = (byte) r10
            r9[r8] = r10
            int r2 = r2 + -1
            r8 = 0
            r0 = r21
            r0.b = r8
            goto L_0x0021
        L_0x081d:
            r2 = 7
            if (r6 <= r2) goto L_0x0826
            int r6 = r6 + -8
            int r4 = r4 + 1
            int r5 = r5 + -1
        L_0x0826:
            r0 = r22
            r0.f = r3
            int r2 = r22.b(r23, r24)
            r0 = r22
            int r3 = r0.f
            r0 = r22
            int r8 = r0.e
            r0 = r22
            int r9 = r0.f
            if (r8 == r9) goto L_0x086a
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r6
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r4 = r0.b
            int r4 = r5 - r4
            long r8 = (long) r4
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            r0 = r22
            r1 = r23
            int r2 = r0.b(r1, r2)
            goto L_0x0055
        L_0x086a:
            r2 = 8
            r0 = r21
            r0.b = r2
        L_0x0870:
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r6
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r8 = (long) r2
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            r2 = 1
            r0 = r22
            r1 = r23
            int r2 = r0.b(r1, r2)
            goto L_0x0055
        L_0x089f:
            r0 = r22
            r0.b = r7
            r0 = r22
            r0.a = r6
            r0 = r23
            r0.c = r4
            r0 = r23
            long r6 = r0.d
            r0 = r23
            int r2 = r0.b
            int r2 = r5 - r2
            long r8 = (long) r2
            long r6 = r6 + r8
            r0 = r23
            r0.d = r6
            r0 = r23
            r0.b = r5
            r0 = r22
            r0.f = r3
            r2 = -3
            r0 = r22
            r1 = r23
            int r2 = r0.b(r1, r2)
            goto L_0x0055
        L_0x08ce:
            r8 = r10
            goto L_0x06c3
        L_0x08d1:
            r8 = r2
            r9 = r3
            r10 = r4
            r3 = r5
            r4 = r6
            goto L_0x00d2
        L_0x08d8:
            r3 = r4
            r5 = r6
            goto L_0x022d
        L_0x08dc:
            r4 = r2
            r5 = r6
            r2 = r3
            goto L_0x0278
        L_0x08e1:
            r3 = r7
            r4 = r2
            r2 = r13
            goto L_0x0224
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.a.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.b, com.agilebinary.mobilemonitor.device.a.b.a.a.b.a.a.g, int):int");
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2, int i3, int[] iArr, int i4, int[] iArr2, int i5) {
        this.b = 0;
        this.j = (byte) i2;
        this.k = (byte) i3;
        this.l = iArr;
        this.m = i4;
        this.n = iArr2;
        this.o = i5;
        this.d = null;
    }
}
