package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public final class h extends b {
    private int a = -1;
    private int b = -1;
    private int c = -1;
    private double d;
    private double e;

    public h(a aVar) {
        super(aVar);
        this.a = aVar.e();
        this.b = aVar.e();
        this.c = aVar.e();
        this.d = aVar.g();
        this.e = aVar.g();
    }

    public h(String str, String str2, long j, int i, int i2, int i3, double d2, double d3, String str3) {
        super(str, str2, j, str3);
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = d2;
        this.e = d3;
    }

    public final int a() {
        return this.a;
    }

    public final String a(c cVar) {
        return super.a(cVar) + "\nBaseStationId: " + this.a + "\nNetworkId: " + this.b + "\nSystemId: " + this.c + "\nBaseStationLatitude: " + this.d + "\nBaseStationLongitude: " + this.e;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
        aVar.a(this.d);
        aVar.a(this.e);
    }

    public final byte b() {
        return 8;
    }

    public final int c() {
        return this.b;
    }

    public final int d() {
        return this.c;
    }

    public final double e() {
        return this.d;
    }

    public final double f() {
        return this.e;
    }
}
