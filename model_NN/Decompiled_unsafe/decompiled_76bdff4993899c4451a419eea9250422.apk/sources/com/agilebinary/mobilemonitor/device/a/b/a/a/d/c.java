package com.agilebinary.mobilemonitor.device.a.b.a.a.d;

import bsh.ParserConstants;
import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.b;
import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.g;

public final class c extends g {
    public c(b bVar) {
        super(bVar);
    }

    public final void a(double d) {
        char[] charArray = String.valueOf(d).toCharArray();
        this.a.a(charArray.length);
        int i = -1;
        for (int i2 = 0; i2 < charArray.length; i2++) {
            if (charArray[i2] == 'E') {
                charArray[i2] = '/';
            }
            int i3 = charArray[i2] - '-';
            if ((i2 & 1) == 0) {
                i = i3 << 4;
            } else {
                this.a.a(i + i3);
                i = -1;
            }
        }
        if (i >= 0) {
            this.a.a(i);
        }
    }

    public final void a(long j) {
        this.a.a(((int) (j >>> 56)) & 255);
        this.a.a(((int) (j >>> 48)) & 255);
        this.a.a(((int) (j >>> 40)) & 255);
        this.a.a(((int) (j >>> 32)) & 255);
        this.a.a(((int) (j >>> 24)) & 255);
        this.a.a(((int) (j >>> 16)) & 255);
        this.a.a(((int) (j >>> 8)) & 255);
        this.a.a(((int) (j >>> 0)) & 255);
    }

    public final void a(String str) {
        int i;
        if (str == null) {
            this.a.a(1);
            return;
        }
        this.a.a(0);
        int length = str.length();
        char[] cArr = new char[length];
        str.getChars(0, length, cArr, 0);
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            char c = cArr[i2];
            i2++;
            i3 = (c <= 0 || c > 127) ? c > 2047 ? i3 + 3 : i3 + 2 : i3 + 1;
        }
        if (i3 > 65535) {
            throw new IllegalArgumentException();
        }
        byte[] bArr = new byte[(i3 + 2)];
        bArr[0] = (byte) ((i3 >>> 8) & 255);
        bArr[1] = (byte) ((i3 >>> 0) & 255);
        int i4 = 0;
        int i5 = 2;
        while (i4 < length) {
            char c2 = cArr[i4];
            if (c2 > 0 && c2 <= 127) {
                i = i5 + 1;
                bArr[i5] = (byte) c2;
            } else if (c2 > 2047) {
                int i6 = i5 + 1;
                bArr[i5] = (byte) (((c2 >> 12) & 15) | 224);
                int i7 = i6 + 1;
                bArr[i6] = (byte) (((c2 >> 6) & 63) | ParserConstants.LSHIFTASSIGN);
                i = i7 + 1;
                bArr[i7] = (byte) (((c2 >> 0) & 63) | ParserConstants.LSHIFTASSIGN);
            } else {
                int i8 = i5 + 1;
                bArr[i5] = (byte) (((c2 >> 6) & 31) | 192);
                i = i8 + 1;
                bArr[i8] = (byte) (((c2 >> 0) & 63) | ParserConstants.LSHIFTASSIGN);
            }
            i4++;
            i5 = i;
        }
        this.a.a(bArr);
    }

    public final void a(boolean z) {
        this.a.a(z ? 1 : 0);
    }

    public final void a(byte[] bArr) {
        this.a.a(bArr, 0, bArr.length);
    }

    public final void a(byte[] bArr, int i, int i2) {
        this.a.a(bArr, i, i2);
    }

    public final void b(int i) {
        this.a.a(i);
    }

    public final void c(int i) {
        this.a.a((i >>> 24) & 255);
        this.a.a((i >>> 16) & 255);
        this.a.a((i >>> 8) & 255);
        this.a.a((i >>> 0) & 255);
    }
}
