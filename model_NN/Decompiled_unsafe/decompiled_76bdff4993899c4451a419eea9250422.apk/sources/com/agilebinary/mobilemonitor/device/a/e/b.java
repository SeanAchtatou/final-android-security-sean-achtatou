package com.agilebinary.mobilemonitor.device.a.e;

import com.agilebinary.a.a.a.c.b.p;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.j.f;

public class b implements e, c {
    private final e a;
    private final p b;
    private final String c;

    public b() {
    }

    public b(e eVar, p pVar, String str) {
        this.a = eVar;
        this.b = pVar;
        this.c = str == null ? "ASCII" : str;
    }

    public void a() {
        this.a.a();
    }

    public void a(int i) {
        this.a.a(i);
        if (this.b.a()) {
            this.b.a(new byte[]{(byte) i});
        }
    }

    public void a(com.agilebinary.a.a.a.i.b bVar) {
        this.a.a(bVar);
        if (this.b.a()) {
            this.b.a((new String(bVar.b(), 0, bVar.c()) + "\r\n").getBytes(this.c));
        }
    }

    public void a(String str) {
        this.a.a(str);
        if (this.b.a()) {
            this.b.a((str + "\r\n").getBytes(this.c));
        }
    }

    public final void a(String str, String str2, Throwable th) {
    }

    public void a(byte[] bArr, int i, int i2) {
        this.a.a(bArr, i, i2);
        if (this.b.a()) {
            this.b.a(bArr, i, i2);
        }
    }

    public f b() {
        return this.a.b();
    }
}
