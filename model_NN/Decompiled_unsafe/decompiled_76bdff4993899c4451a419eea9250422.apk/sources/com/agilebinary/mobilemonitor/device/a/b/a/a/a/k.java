package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.b;

public abstract class k extends u {
    protected long a;
    private double b;
    private double c;
    private double d;
    private boolean e;
    private boolean f;
    private String g;
    private boolean h;
    private double i;
    private boolean j;
    private double k;

    public k(a aVar) {
        super(aVar);
        this.b = aVar.g();
        this.c = aVar.g();
        this.d = aVar.g();
        this.a = aVar.f();
        this.e = aVar.b();
        this.f = aVar.b();
        this.g = aVar.i();
        this.h = aVar.b();
        if (this.h) {
            this.i = aVar.g();
        }
        this.j = aVar.b();
        if (this.j) {
            this.k = aVar.g();
        }
    }

    public k(String str, String str2, long j2, double d2, double d3, double d4, long j3, boolean z, boolean z2, String str3, d dVar) {
        super(str, str2, j2);
        this.b = d2;
        this.c = d3;
        this.d = d4;
        this.a = j3;
        this.e = z;
        this.f = z2;
        this.g = str3;
        if (dVar != null) {
            this.h = dVar.k();
            if (this.h) {
                this.i = dVar.l();
            }
            this.j = dVar.c();
            if (this.j) {
                this.k = dVar.d();
            }
        }
    }

    public String a(c cVar) {
        return super.a(cVar) + "\nLatitude: " + this.b + "\nLongitude: " + this.c + "\nHorizontalAccuracy: " + this.d + "\nCurrent: " + this.e + "\nPowersave: " + this.f + "\nPowersavereason: " + this.g;
    }

    public void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.b);
        aVar.a(this.c);
        aVar.a(this.d);
        aVar.a(this.a);
        aVar.a(this.e);
        aVar.a(this.f);
        aVar.a(b.a ? this.g : "");
        aVar.a(this.h);
        if (this.h) {
            aVar.a(this.i);
        }
        aVar.a(this.j);
        if (this.j) {
            aVar.a(this.k);
        }
    }

    public final double d() {
        return this.b;
    }

    public final double e() {
        return this.c;
    }

    public final double f() {
        return this.d;
    }

    public final long g() {
        return this.a;
    }

    public final boolean h() {
        return this.e;
    }
}
