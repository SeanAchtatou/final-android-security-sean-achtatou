package com.agilebinary.mobilemonitor.device.android.device.a;

import android.database.ContentObserver;
import android.os.Handler;

final class a extends ContentObserver {
    private /* synthetic */ e a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(e eVar, Handler handler) {
        super(null);
        this.a = eVar;
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        try {
            this.a.a.a(this.a);
        } catch (Exception e) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(e.b, "onChange", e);
        }
    }
}
