package com.agilebinary.mobilemonitor.device.a.j.a;

import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.a.e;
import com.agilebinary.mobilemonitor.device.a.b.b;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.util.Enumeration;
import java.util.Vector;

public abstract class a implements b {
    private static final String a = f.a();
    public static final b[] b = new b[0];
    protected com.agilebinary.mobilemonitor.device.a.a.b c;
    protected c d;
    private g e;
    private d f = new d(this);
    private Vector g = new Vector(1);
    private e h;

    protected a(g gVar, c cVar, com.agilebinary.mobilemonitor.device.a.a.b bVar, e eVar) {
        this.e = gVar;
        this.c = bVar;
        this.d = cVar;
        this.h = eVar;
    }

    private void a(Exception exc) {
        c();
        d();
        this.h.a();
        a();
        b();
        throw new com.agilebinary.mobilemonitor.device.a.j.a(exc);
    }

    private void o() {
        if (this.g.size() > 0) {
            long l = l();
            Enumeration elements = this.g.elements();
            while (elements.hasMoreElements()) {
                ((c) elements.nextElement()).a(l);
            }
        }
    }

    public final synchronized void a() {
        try {
            e();
        } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "exception in initIt", e2);
        }
        return;
    }

    /* access modifiers changed from: protected */
    public abstract void a(long j);

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        if (r0.a(r4) != false) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0020, code lost:
        com.agilebinary.mobilemonitor.device.a.e.a.e(com.agilebinary.mobilemonitor.device.a.j.a.a.a, "failed to persist Event", r0);
        a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        o();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.agilebinary.mobilemonitor.device.a.b.b r4, com.agilebinary.mobilemonitor.device.a.d.a.i r5, com.agilebinary.mobilemonitor.device.a.b.n r6) {
        /*
            r3 = this;
            monitor-enter(r3)
            com.agilebinary.mobilemonitor.device.a.a.b r0 = r3.c     // Catch:{ all -> 0x002b }
            int r1 = r4.i()     // Catch:{ all -> 0x002b }
            int r0 = r0.k(r1)     // Catch:{ all -> 0x002b }
            com.agilebinary.mobilemonitor.device.a.j.a.a.a r0 = com.agilebinary.mobilemonitor.device.a.j.a.a.e.a(r0, r3, r5, r6)     // Catch:{ all -> 0x002b }
        L_0x000f:
            r3.b(r4)     // Catch:{ b -> 0x0017, Exception -> 0x001f }
        L_0x0012:
            r3.o()     // Catch:{ all -> 0x002b }
            monitor-exit(r3)
            return
        L_0x0017:
            r1 = move-exception
            boolean r1 = r0.a(r4)     // Catch:{ all -> 0x002b }
            if (r1 == 0) goto L_0x0012
            goto L_0x000f
        L_0x001f:
            r0 = move-exception
            java.lang.String r1 = com.agilebinary.mobilemonitor.device.a.j.a.a.a     // Catch:{ all -> 0x002b }
            java.lang.String r2 = "failed to persist Event"
            com.agilebinary.mobilemonitor.device.a.e.a.e(r1, r2, r0)     // Catch:{ all -> 0x002b }
            r3.a(r0)     // Catch:{ all -> 0x002b }
            goto L_0x0012
        L_0x002b:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.j.a.a.a(com.agilebinary.mobilemonitor.device.a.b.b, com.agilebinary.mobilemonitor.device.a.d.a.i, com.agilebinary.mobilemonitor.device.a.b.n):void");
    }

    public final void a(c cVar) {
        this.g.addElement(cVar);
    }

    public final synchronized boolean a(long j, int i) {
        boolean z = false;
        synchronized (this) {
            try {
                if (c((long) ((int) j)) >= this.c.i(i)) {
                    a((long) ((int) j));
                    o();
                    z = true;
                } else {
                    b(j);
                }
            } catch (Exception e2) {
                com.agilebinary.mobilemonitor.device.a.e.a.e(a, "failure in incRetryCountAndDeleteIfTooOften", e2);
                a(e2);
            }
        }
        return z;
    }

    public abstract long b(b bVar);

    public final synchronized void b() {
        try {
            f();
        } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "exception in startIt", e2);
        }
        return;
    }

    /* access modifiers changed from: protected */
    public abstract void b(long j);

    public final void b(c cVar) {
        this.g.removeElement(cVar);
    }

    /* access modifiers changed from: protected */
    public abstract int c(long j);

    public final synchronized void c() {
        try {
            g();
        } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "exception in stopIt", e2);
        }
        return;
    }

    public final synchronized void d() {
    }

    public final synchronized void d(long j) {
        try {
            a(j);
            o();
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "failure in incRetryCountAndDeleteIfTooOften", e2);
            a(e2);
        }
        return;
    }

    public abstract void e();

    public abstract void f();

    public abstract void g();

    /* access modifiers changed from: protected */
    public abstract void j();

    public final synchronized b[] m() {
        b[] bVarArr;
        try {
            j();
            bVarArr = h();
            this.e.a(bVarArr, this.f);
            o();
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "failed to delete expired Events", e2);
            a(e2);
            bVarArr = null;
        }
        return bVarArr;
    }

    public final synchronized b[] n() {
        b[] bVarArr;
        try {
            bVarArr = h();
            this.e.a(bVarArr, this.f);
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(a, "failure in getAllEventsWithoutBody_sorted", e2);
            a(e2);
            bVarArr = null;
        }
        return bVarArr;
    }
}
