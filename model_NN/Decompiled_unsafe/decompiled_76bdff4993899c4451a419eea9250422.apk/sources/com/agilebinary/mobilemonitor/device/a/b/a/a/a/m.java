package com.agilebinary.mobilemonitor.device.a.b.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a;

public final class m extends b {
    private int a = -1;
    private int b = -1;
    private int c = -1;
    private int d = -1;

    public m(a aVar) {
        super(aVar);
        this.a = aVar.e();
        this.b = aVar.e();
        this.c = aVar.e();
        this.d = aVar.e();
    }

    public m(String str, String str2, long j, int i, int i2, int i3, int i4, String str3) {
        super(str, str2, j, str3);
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }

    public final int a() {
        return this.a;
    }

    public final String a(c cVar) {
        return super.a(cVar) + "\nCid: " + this.a + "\nLac: " + this.b + "\nMCC: " + this.c + "\nMNC: " + this.d;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
        aVar.a(this.d);
    }

    public final byte b() {
        return 7;
    }

    public final int c() {
        return this.b;
    }

    public final int d() {
        return this.c;
    }

    public final int e() {
        return this.d;
    }
}
