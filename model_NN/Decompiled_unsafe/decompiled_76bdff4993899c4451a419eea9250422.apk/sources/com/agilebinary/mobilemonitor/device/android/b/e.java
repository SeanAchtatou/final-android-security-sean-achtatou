package com.agilebinary.mobilemonitor.device.android.b;

import com.agilebinary.a.a.a.c.e.a;
import com.agilebinary.a.a.a.h.b.j;
import com.agilebinary.a.a.a.h.d.b;
import com.agilebinary.a.a.a.h.h;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

public final class e implements j {
    /* access modifiers changed from: private */
    public static final String a = f.a();
    private static b b = new com.agilebinary.a.a.a.h.d.e();
    private final SSLContext c;
    private final SSLSocketFactory d;
    private final com.agilebinary.a.a.a.h.b.b e;
    private volatile b f;

    static {
        new e();
    }

    private e() {
        this.f = b;
        this.c = null;
        this.d = HttpsURLConnection.getDefaultSSLSocketFactory();
        this.e = null;
    }

    public e(com.agilebinary.mobilemonitor.device.a.a.b bVar) {
        this.f = b;
        this.c = SSLContext.getInstance("TLS");
        this.c.init(null, new TrustManager[]{new d(this, bVar)}, null);
        this.d = this.c.getSocketFactory();
        this.e = null;
    }

    public final Socket a() {
        return (SSLSocket) this.d.createSocket();
    }

    public final Socket a(Socket socket, String str, int i, InetAddress inetAddress, int i2, com.agilebinary.a.a.a.e.b bVar) {
        if (str == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Parameters may not be null.");
        } else {
            SSLSocket sSLSocket = (SSLSocket) (socket != null ? socket : a());
            if (inetAddress != null || i2 > 0) {
                if (i2 < 0) {
                    i2 = 0;
                }
                sSLSocket.bind(new InetSocketAddress(inetAddress, i2));
            }
            int c2 = a.c(bVar);
            int a2 = a.a(bVar);
            InetSocketAddress inetSocketAddress = new InetSocketAddress(str, i);
            try {
                sSLSocket.connect(inetSocketAddress, c2);
                sSLSocket.setSoTimeout(a2);
                try {
                    this.f.a(str, sSLSocket);
                    return sSLSocket;
                } catch (IOException e2) {
                    try {
                        sSLSocket.close();
                    } catch (Exception e3) {
                    }
                    throw e2;
                }
            } catch (SocketTimeoutException e4) {
                throw new h("Connect to " + inetSocketAddress + " timed out");
            }
        }
    }

    public final boolean a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null.");
        } else if (!(socket instanceof SSLSocket)) {
            throw new IllegalArgumentException("Socket not created by this factory.");
        } else if (!socket.isClosed()) {
            return true;
        } else {
            throw new IllegalArgumentException("Socket is closed.");
        }
    }

    public final Socket a_(Socket socket, String str, int i, boolean z) {
        SSLSocket sSLSocket = (SSLSocket) this.d.createSocket(socket, str, i, z);
        this.f.a(str, sSLSocket);
        return sSLSocket;
    }
}
