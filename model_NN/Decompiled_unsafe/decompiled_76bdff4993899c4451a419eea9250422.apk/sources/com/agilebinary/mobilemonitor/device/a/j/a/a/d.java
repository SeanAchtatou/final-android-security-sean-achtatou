package com.agilebinary.mobilemonitor.device.a.j.a.a;

import com.agilebinary.mobilemonitor.device.a.b.n;
import com.agilebinary.mobilemonitor.device.a.d.a.i;
import com.agilebinary.mobilemonitor.device.a.j.a.b;

public final class d extends f {
    private int c;
    private n d;

    public d(int i, b bVar, i iVar, n nVar) {
        super(bVar, iVar);
        this.c = i;
        this.d = nVar;
    }

    public final boolean a(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        if (bVar.p() || (this.c != 3 && this.c != this.b.q())) {
            return super.a(bVar);
        }
        this.d.a(bVar, this.c);
        return false;
    }
}
