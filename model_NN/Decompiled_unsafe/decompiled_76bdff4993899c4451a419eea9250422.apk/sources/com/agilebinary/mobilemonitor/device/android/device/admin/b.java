package com.agilebinary.mobilemonitor.device.android.device.admin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class b extends BroadcastReceiver {
    protected static String a = "MyBaseDeviceAdminReceiver";

    public CharSequence a(Context context) {
        return null;
    }

    public void b(Context context) {
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (!"android.app.action.ACTION_PASSWORD_CHANGED".equals(action) && !"android.app.action.ACTION_PASSWORD_FAILED".equals(action) && !"android.app.action.ACTION_PASSWORD_SUCCEEDED".equals(action) && !"android.app.action.DEVICE_ADMIN_ENABLED".equals(action)) {
            if ("android.app.action.DEVICE_ADMIN_DISABLE_REQUESTED".equals(action)) {
                CharSequence a2 = a(context);
                if (a2 != null) {
                    getResultExtras(true).putCharSequence("android.app.extra.DISABLE_WARNING", a2);
                }
            } else if ("android.app.action.DEVICE_ADMIN_DISABLED".equals(action)) {
                b(context);
            }
        }
    }
}
