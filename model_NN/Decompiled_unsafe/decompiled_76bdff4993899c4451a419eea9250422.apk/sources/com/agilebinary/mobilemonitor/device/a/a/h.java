package com.agilebinary.mobilemonitor.device.a.a;

import bsh.ParserConstants;
import com.agilebinary.b.a.a.a;
import com.agilebinary.b.a.a.q;
import com.agilebinary.mobilemonitor.device.a.g.e;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Hashtable;

public final class h extends Hashtable implements e {
    private static char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    private static char a(int i) {
        return a[i & 15];
    }

    public final int a(Object obj, Object obj2) {
        return ((String) obj).compareTo((String) obj2);
    }

    public final String a(String str) {
        return (String) super.get(str);
    }

    public final void a(h hVar, boolean z) {
        Enumeration keys = keys();
        while (keys.hasMoreElements()) {
            String str = (String) keys.nextElement();
            if (z || !str.endsWith("_cmd")) {
                hVar.put(str, get(str));
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x00c6 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x012a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.io.InputStream r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            r3 = 80
            r0 = 80
            char[] r2 = new char[r0]     // Catch:{ all -> 0x0125 }
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
        L_0x000b:
            switch(r1) {
                case -1: goto L_0x0128;
                case 9: goto L_0x0051;
                case 10: goto L_0x0051;
                case 13: goto L_0x0051;
                case 32: goto L_0x0051;
                case 33: goto L_0x0042;
                case 35: goto L_0x0042;
                default: goto L_0x000e;
            }     // Catch:{ all -> 0x0125 }
        L_0x000e:
            r0 = 0
            r4 = r0
        L_0x0010:
            if (r1 < 0) goto L_0x0056
            r0 = 61
            if (r1 == r0) goto L_0x0056
            r0 = 58
            if (r1 == r0) goto L_0x0056
            r0 = 32
            if (r1 == r0) goto L_0x0056
            r0 = 9
            if (r1 == r0) goto L_0x0056
            r0 = 10
            if (r1 == r0) goto L_0x0056
            r0 = 13
            if (r1 == r0) goto L_0x0056
            if (r4 < r3) goto L_0x0037
            int r3 = r3 * 2
            char[] r0 = new char[r3]     // Catch:{ all -> 0x0125 }
            r5 = 0
            r6 = 0
            int r7 = r2.length     // Catch:{ all -> 0x0125 }
            java.lang.System.arraycopy(r2, r5, r0, r6, r7)     // Catch:{ all -> 0x0125 }
            r2 = r0
        L_0x0037:
            int r0 = r4 + 1
            char r1 = (char) r1     // Catch:{ all -> 0x0125 }
            r2[r4] = r1     // Catch:{ all -> 0x0125 }
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
            r4 = r0
            goto L_0x0010
        L_0x0042:
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
            if (r1 < 0) goto L_0x000b
            r0 = 10
            if (r1 == r0) goto L_0x000b
            r0 = 13
            if (r1 != r0) goto L_0x0042
            goto L_0x000b
        L_0x0051:
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
            goto L_0x000b
        L_0x0056:
            r0 = 32
            if (r1 == r0) goto L_0x005e
            r0 = 9
            if (r1 != r0) goto L_0x0063
        L_0x005e:
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
            goto L_0x0056
        L_0x0063:
            r0 = 61
            if (r1 == r0) goto L_0x006b
            r0 = 58
            if (r1 != r0) goto L_0x006f
        L_0x006b:
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
        L_0x006f:
            r0 = 32
            if (r1 == r0) goto L_0x0077
            r0 = 9
            if (r1 != r0) goto L_0x007c
        L_0x0077:
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
            goto L_0x006f
        L_0x007c:
            java.lang.String r8 = new java.lang.String     // Catch:{ all -> 0x0125 }
            r0 = 0
            r8.<init>(r2, r0, r4)     // Catch:{ all -> 0x0125 }
            r0 = 0
            r7 = r0
        L_0x0084:
            if (r1 < 0) goto L_0x011a
            r0 = 10
            if (r1 == r0) goto L_0x011a
            r0 = 13
            if (r1 == r0) goto L_0x011a
            r5 = 0
            r0 = 92
            if (r1 != r0) goto L_0x00d5
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
            switch(r1) {
                case 10: goto L_0x00c6;
                case 13: goto L_0x00b6;
                case 110: goto L_0x00dc;
                case 114: goto L_0x00df;
                case 116: goto L_0x00d3;
                case 117: goto L_0x00e2;
                default: goto L_0x009a;
            }     // Catch:{ all -> 0x0125 }
        L_0x009a:
            int r0 = r12.read()     // Catch:{ all -> 0x0125 }
            r4 = r0
            r5 = r1
        L_0x00a0:
            if (r7 < r3) goto L_0x012a
            int r1 = r3 * 2
            char[] r0 = new char[r1]     // Catch:{ all -> 0x0125 }
            r3 = 0
            r6 = 0
            int r9 = r2.length     // Catch:{ all -> 0x0125 }
            java.lang.System.arraycopy(r2, r3, r0, r6, r9)     // Catch:{ all -> 0x0125 }
        L_0x00ac:
            int r2 = r7 + 1
            char r3 = (char) r5     // Catch:{ all -> 0x0125 }
            r0[r7] = r3     // Catch:{ all -> 0x0125 }
            r7 = r2
            r3 = r1
            r2 = r0
            r1 = r4
            goto L_0x0084
        L_0x00b6:
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
            r0 = 10
            if (r1 == r0) goto L_0x00c6
            r0 = 32
            if (r1 == r0) goto L_0x00c6
            r0 = 9
            if (r1 != r0) goto L_0x0084
        L_0x00c6:
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
            r0 = 32
            if (r1 == r0) goto L_0x00c6
            r0 = 9
            if (r1 != r0) goto L_0x0084
            goto L_0x00c6
        L_0x00d3:
            r1 = 9
        L_0x00d5:
            int r0 = r12.read()     // Catch:{ all -> 0x0125 }
            r4 = r0
            r5 = r1
            goto L_0x00a0
        L_0x00dc:
            r1 = 10
            goto L_0x00d5
        L_0x00df:
            r1 = 13
            goto L_0x00d5
        L_0x00e2:
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
            r0 = 117(0x75, float:1.64E-43)
            if (r1 == r0) goto L_0x00e2
            r4 = 0
            r0 = 0
            r6 = r1
            r10 = r5
            r5 = r0
            r0 = r10
        L_0x00f0:
            r1 = 4
            if (r5 >= r1) goto L_0x00fb
            int r1 = r12.read()     // Catch:{ all -> 0x0125 }
            switch(r6) {
                case 48: goto L_0x00fe;
                case 49: goto L_0x00fe;
                case 50: goto L_0x00fe;
                case 51: goto L_0x00fe;
                case 52: goto L_0x00fe;
                case 53: goto L_0x00fe;
                case 54: goto L_0x00fe;
                case 55: goto L_0x00fe;
                case 56: goto L_0x00fe;
                case 57: goto L_0x00fe;
                case 65: goto L_0x0112;
                case 66: goto L_0x0112;
                case 67: goto L_0x0112;
                case 68: goto L_0x0112;
                case 69: goto L_0x0112;
                case 70: goto L_0x0112;
                case 97: goto L_0x010a;
                case 98: goto L_0x010a;
                case 99: goto L_0x010a;
                case 100: goto L_0x010a;
                case 101: goto L_0x010a;
                case 102: goto L_0x010a;
                default: goto L_0x00fa;
            }     // Catch:{ all -> 0x0125 }
        L_0x00fa:
            r0 = r1
        L_0x00fb:
            r5 = r4
            r4 = r0
            goto L_0x00a0
        L_0x00fe:
            int r0 = r4 << 4
            int r0 = r0 + r6
            int r0 = r0 + -48
        L_0x0103:
            int r4 = r5 + 1
            r5 = r4
            r6 = r1
            r4 = r0
            r0 = r1
            goto L_0x00f0
        L_0x010a:
            int r0 = r4 << 4
            int r0 = r0 + 10
            int r0 = r0 + r6
            int r0 = r0 + -97
            goto L_0x0103
        L_0x0112:
            int r0 = r4 << 4
            int r0 = r0 + 10
            int r0 = r0 + r6
            int r0 = r0 + -65
            goto L_0x0103
        L_0x011a:
            java.lang.String r0 = new java.lang.String     // Catch:{ all -> 0x0125 }
            r4 = 0
            r0.<init>(r2, r4, r7)     // Catch:{ all -> 0x0125 }
            r11.put(r8, r0)     // Catch:{ all -> 0x0125 }
            goto L_0x000b
        L_0x0125:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        L_0x0128:
            monitor-exit(r11)
            return
        L_0x012a:
            r0 = r2
            r1 = r3
            goto L_0x00ac
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.a.h.a(java.io.InputStream):void");
    }

    public final synchronized void a(OutputStream outputStream, String str) {
        PrintStream printStream = new PrintStream(outputStream);
        printStream.write(35);
        printStream.println(System.currentTimeMillis());
        q qVar = new q(size());
        Enumeration keys = keys();
        while (keys.hasMoreElements()) {
            qVar.b((String) keys.nextElement());
        }
        com.agilebinary.b.a.a.h.a(qVar, this);
        a a2 = qVar.a();
        while (a2.a()) {
            String str2 = (String) a2.b();
            printStream.print(str2);
            printStream.write(61);
            String str3 = (String) get(str2);
            int length = str3.length();
            for (int i = 0; i < length; i++) {
                char charAt = str3.charAt(i);
                switch (charAt) {
                    case 9:
                        printStream.write(92);
                        printStream.write((int) ParserConstants.RUNSIGNEDSHIFT);
                        break;
                    case 10:
                        printStream.write(92);
                        printStream.write((int) ParserConstants.XOR);
                        break;
                    case 13:
                        printStream.write(92);
                        printStream.write((int) ParserConstants.RSIGNEDSHIFT);
                        break;
                    case ParserConstants.LEX:
                        printStream.write(92);
                        printStream.write(92);
                        break;
                    default:
                        if (charAt >= ' ' && charAt < 127) {
                            printStream.write(charAt);
                            break;
                        } else {
                            printStream.write(92);
                            printStream.write((int) ParserConstants.RUNSIGNEDSHIFTX);
                            printStream.write(a((charAt >> 12) & 15));
                            printStream.write(a((charAt >> 8) & 15));
                            printStream.write(a((charAt >> 4) & 15));
                            printStream.write(a((charAt >> 0) & 15));
                            break;
                        }
                        break;
                }
            }
            printStream.write(10);
        }
    }
}
