package com.agilebinary.mobilemonitor.device.android.ui;

import com.agilebinary.mobilemonitor.device.a.e.a;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

final class x extends FutureTask {
    private /* synthetic */ at a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    x(at atVar, Callable callable) {
        super(callable);
        this.a = atVar;
    }

    /* access modifiers changed from: protected */
    public final void done() {
        Object obj = null;
        try {
            obj = get();
        } catch (InterruptedException e) {
            a.d(at.a, "", e);
        } catch (ExecutionException e2) {
            throw new RuntimeException("An error occured while executing doInBackground()", e2.getCause());
        } catch (CancellationException e3) {
            at.d.obtainMessage(3, new ar(this.a, null)).sendToTarget();
            return;
        } catch (Throwable th) {
            throw new RuntimeException("An error occured while executing doInBackground()", th);
        }
        at.d.obtainMessage(1, new ar(this.a, obj)).sendToTarget();
    }
}
