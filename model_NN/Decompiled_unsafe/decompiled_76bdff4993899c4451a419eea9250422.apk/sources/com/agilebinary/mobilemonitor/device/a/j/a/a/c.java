package com.agilebinary.mobilemonitor.device.a.j.a.a;

import com.agilebinary.mobilemonitor.device.a.b.n;
import com.agilebinary.mobilemonitor.device.a.d.a.i;
import com.agilebinary.mobilemonitor.device.a.j.a.b;

public final class c extends b {
    private int c;
    private n d;

    public c(int i, b bVar, i iVar, n nVar) {
        super(bVar, iVar);
        this.c = i;
        this.d = nVar;
    }

    public final boolean a(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        if (bVar.p()) {
            return super.a(bVar);
        }
        this.b.a(true, false, false);
        if (this.c != 3 && this.c != this.b.q()) {
            return false;
        }
        this.d.a(bVar, this.c);
        return false;
    }
}
