package com.agilebinary.b.a.a;

final class e extends l {
    private /* synthetic */ c a;

    e(c cVar) {
        this.a = cVar;
    }

    public final a a() {
        return c.a(this.a, 2);
    }

    public final boolean a(Object obj) {
        if (!(obj instanceof y)) {
            return false;
        }
        y yVar = (y) obj;
        Object a2 = yVar.a();
        z[] b = this.a.a;
        int hashCode = a2 == null ? 0 : a2.hashCode();
        for (z zVar = b[(Integer.MAX_VALUE & hashCode) % b.length]; zVar != null; zVar = zVar.d) {
            if (zVar.a == hashCode && zVar.equals(yVar)) {
                return true;
            }
        }
        return false;
    }

    public final int b() {
        return this.a.b;
    }

    public final void c() {
        this.a.b();
    }
}
