package com.agilebinary.b.a.a;

public abstract class o implements aa {
    protected o() {
    }

    public abstract a a();

    public boolean a(Object obj) {
        a a = a();
        if (obj == null) {
            while (a.a()) {
                if (a.b() == null) {
                    return true;
                }
            }
        } else {
            while (a.a()) {
                if (obj.equals(a.b())) {
                    return true;
                }
            }
        }
        return false;
    }

    public abstract int b();

    public boolean b(Object obj) {
        throw new UnsupportedOperationException();
    }

    public void c() {
        a a = a();
        while (a.a()) {
            a.b();
            a.c();
        }
    }

    public Object[] d() {
        Object[] objArr = new Object[b()];
        a a = a();
        int i = 0;
        while (a.a()) {
            objArr[i] = a.b();
            i++;
        }
        return objArr;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        a a = a();
        stringBuffer.append("[");
        int b = b() - 1;
        for (int i = 0; i <= b; i++) {
            stringBuffer.append(String.valueOf(a.b()));
            if (i < b) {
                stringBuffer.append(", ");
            }
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }
}
