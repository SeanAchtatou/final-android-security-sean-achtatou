package com.agilebinary.b.a.a;

public final class c extends i implements m {
    private static r h = new r();
    /* access modifiers changed from: private */
    public transient z[] a;
    /* access modifiers changed from: private */
    public transient int b;
    private int c;
    private float d;
    /* access modifiers changed from: private */
    public transient int e;
    private transient n f;
    private transient n g;

    public c() {
        this(11, 0.75f);
    }

    private c(int i, float f2) {
        this.e = 0;
        this.f = null;
        this.g = null;
        if (Float.isNaN(0.75f)) {
            throw new IllegalArgumentException("Illegal Load factor: " + 0.75f);
        }
        this.d = 0.75f;
        this.a = new z[11];
        this.c = 8;
    }

    static /* synthetic */ a a(c cVar, int i) {
        return cVar.b == 0 ? h : new k(cVar, i);
    }

    static /* synthetic */ int c(c cVar) {
        int i = cVar.e;
        cVar.e = i + 1;
        return i;
    }

    static /* synthetic */ int d(c cVar) {
        int i = cVar.b;
        cVar.b = i - 1;
        return i;
    }

    public final int a() {
        return this.b;
    }

    public final Object a(Object obj, Object obj2) {
        int i;
        int i2 = 0;
        z[] zVarArr = this.a;
        if (obj != null) {
            i = obj.hashCode();
            i2 = (i & Integer.MAX_VALUE) % zVarArr.length;
            z zVar = zVarArr[i2];
            while (zVar != null) {
                if (zVar.a != i || !obj.equals(zVar.b)) {
                    zVar = zVar.d;
                } else {
                    Object obj3 = zVar.c;
                    zVar.c = obj2;
                    return obj3;
                }
            }
        } else {
            for (z zVar2 = zVarArr[0]; zVar2 != null; zVar2 = zVar2.d) {
                if (zVar2.b == null) {
                    Object obj4 = zVar2.c;
                    zVar2.c = obj2;
                    return obj4;
                }
            }
            i = 0;
        }
        this.e++;
        if (this.b >= this.c) {
            int length = this.a.length;
            z[] zVarArr2 = this.a;
            int i3 = (length * 2) + 1;
            z[] zVarArr3 = new z[i3];
            this.e++;
            this.c = (int) (((float) i3) * this.d);
            this.a = zVarArr3;
            while (true) {
                int i4 = length - 1;
                if (length <= 0) {
                    break;
                }
                z zVar3 = zVarArr2[i4];
                while (zVar3 != null) {
                    z zVar4 = zVar3.d;
                    int i5 = (zVar3.a & Integer.MAX_VALUE) % i3;
                    zVar3.d = zVarArr3[i5];
                    zVarArr3[i5] = zVar3;
                    zVar3 = zVar4;
                }
                length = i4;
            }
            zVarArr = this.a;
            i2 = (i & Integer.MAX_VALUE) % zVarArr.length;
        }
        zVarArr[i2] = new z(i, obj, obj2, zVarArr[i2]);
        this.b++;
        return null;
    }

    public final boolean a(Object obj) {
        z[] zVarArr = this.a;
        if (obj != null) {
            int hashCode = obj.hashCode();
            for (z zVar = zVarArr[(Integer.MAX_VALUE & hashCode) % zVarArr.length]; zVar != null; zVar = zVar.d) {
                if (zVar.a == hashCode && obj.equals(zVar.b)) {
                    return true;
                }
            }
        } else {
            for (z zVar2 = zVarArr[0]; zVar2 != null; zVar2 = zVar2.d) {
                if (zVar2.b == null) {
                    return true;
                }
            }
        }
        return false;
    }

    public final Object b(Object obj) {
        z[] zVarArr = this.a;
        if (obj != null) {
            int hashCode = obj.hashCode();
            for (z zVar = zVarArr[(Integer.MAX_VALUE & hashCode) % zVarArr.length]; zVar != null; zVar = zVar.d) {
                if (zVar.a == hashCode && obj.equals(zVar.b)) {
                    return zVar.c;
                }
            }
        } else {
            for (z zVar2 = zVarArr[0]; zVar2 != null; zVar2 = zVar2.d) {
                if (zVar2.b == null) {
                    return zVar2.c;
                }
            }
        }
        return null;
    }

    public final void b() {
        z[] zVarArr = this.a;
        this.e++;
        int length = zVarArr.length;
        while (true) {
            length--;
            if (length >= 0) {
                zVarArr[length] = null;
            } else {
                this.b = 0;
                return;
            }
        }
    }

    public final n c() {
        if (this.f == null) {
            this.f = new d(this);
        }
        return this.f;
    }

    public final Object clone() {
        c cVar = new c();
        cVar.a = new z[this.a.length];
        int length = this.a.length;
        while (true) {
            int i = length - 1;
            if (length > 0) {
                cVar.a[i] = this.a[i] != null ? (z) this.a[i].clone() : null;
                length = i;
            } else {
                cVar.f = null;
                cVar.g = null;
                cVar.e = 0;
                return cVar;
            }
        }
    }

    public final n d() {
        if (this.g == null) {
            this.g = new e(this);
        }
        return this.g;
    }
}
