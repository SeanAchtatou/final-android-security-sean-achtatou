package com.agilebinary.b.a.a;

public final class w extends l implements n {
    private static final Object b = new Object();
    private transient c a = new c();

    public final a a() {
        return this.a.c().a();
    }

    public final boolean a(Object obj) {
        return this.a.a(obj);
    }

    public final int b() {
        return this.a.a();
    }

    public final boolean b(Object obj) {
        return this.a.a(obj, b) == null;
    }

    public final void c() {
        this.a.b();
    }

    public final Object clone() {
        w wVar = new w();
        wVar.a = (c) this.a.clone();
        return wVar;
    }
}
