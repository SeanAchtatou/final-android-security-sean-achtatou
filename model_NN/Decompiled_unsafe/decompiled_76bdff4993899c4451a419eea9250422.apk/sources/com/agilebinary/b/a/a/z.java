package com.agilebinary.b.a.a;

final class z implements y {
    int a;
    Object b;
    Object c;
    z d;

    z(int i, Object obj, Object obj2, z zVar) {
        this.a = i;
        this.b = obj;
        this.c = obj2;
        this.d = zVar;
    }

    public final Object a() {
        return this.b;
    }

    public final Object b() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public final Object clone() {
        return new z(this.a, this.b, this.c, this.d == null ? null : (z) this.d.clone());
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001c A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(java.lang.Object r4) {
        /*
            r3 = this;
            r0 = 0
            boolean r1 = r4 instanceof com.agilebinary.b.a.a.y
            if (r1 != 0) goto L_0x0006
        L_0x0005:
            return r0
        L_0x0006:
            com.agilebinary.b.a.a.y r4 = (com.agilebinary.b.a.a.y) r4
            java.lang.Object r1 = r3.b
            if (r1 != 0) goto L_0x001e
            java.lang.Object r1 = r4.a()
            if (r1 != 0) goto L_0x0005
        L_0x0012:
            java.lang.Object r1 = r3.c
            if (r1 != 0) goto L_0x002b
            java.lang.Object r1 = r4.b()
            if (r1 != 0) goto L_0x0005
        L_0x001c:
            r0 = 1
            goto L_0x0005
        L_0x001e:
            java.lang.Object r1 = r3.b
            java.lang.Object r2 = r4.a()
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0005
            goto L_0x0012
        L_0x002b:
            java.lang.Object r1 = r3.c
            java.lang.Object r2 = r4.b()
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0005
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.b.a.a.z.equals(java.lang.Object):boolean");
    }

    public final int hashCode() {
        return (this.c == null ? 0 : this.c.hashCode()) ^ this.a;
    }

    public final String toString() {
        return this.b + "=" + this.c;
    }
}
