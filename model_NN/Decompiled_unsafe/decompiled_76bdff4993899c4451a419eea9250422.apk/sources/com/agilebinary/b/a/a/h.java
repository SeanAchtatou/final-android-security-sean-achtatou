package com.agilebinary.b.a.a;

import com.agilebinary.mobilemonitor.device.a.g.e;
import java.util.Random;

public final class h {
    public static final n a = new ac();

    static {
        new Random();
        new ab();
        new v();
        new p();
    }

    private h() {
    }

    public static void a(u uVar, e eVar) {
        Object[] d = uVar.d();
        b.a(d, eVar);
        s e = uVar.e();
        for (Object a2 : d) {
            e.b();
            e.a(a2);
        }
    }
}
