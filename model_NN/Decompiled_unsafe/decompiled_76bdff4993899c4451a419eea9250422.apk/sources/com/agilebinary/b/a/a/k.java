package com.agilebinary.b.a.a;

import java.util.NoSuchElementException;

final class k implements a {
    private z[] a = this.g.a;
    private int b = this.a.length;
    private z c = null;
    private z d = null;
    private int e;
    private int f = this.g.e;
    private /* synthetic */ c g;

    k(c cVar, int i) {
        this.g = cVar;
        this.e = i;
    }

    public final boolean a() {
        z zVar = this.c;
        int i = this.b;
        z[] zVarArr = this.a;
        while (zVar == null && i > 0) {
            i--;
            zVar = zVarArr[i];
        }
        this.c = zVar;
        this.b = i;
        return zVar != null;
    }

    public final Object b() {
        if (this.g.e != this.f) {
            throw new g();
        }
        z zVar = this.c;
        int i = this.b;
        z[] zVarArr = this.a;
        while (zVar == null && i > 0) {
            i--;
            zVar = zVarArr[i];
        }
        this.c = zVar;
        this.b = i;
        if (zVar != null) {
            z zVar2 = this.c;
            this.d = zVar2;
            this.c = zVar2.d;
            return this.e == 0 ? zVar2.b : this.e == 1 ? zVar2.c : zVar2;
        }
        throw new NoSuchElementException();
    }

    public final void c() {
        if (this.d == null) {
            throw new IllegalStateException();
        } else if (this.g.e != this.f) {
            throw new g();
        } else {
            z[] b2 = this.g.a;
            int length = (this.d.a & Integer.MAX_VALUE) % b2.length;
            z zVar = b2[length];
            z zVar2 = null;
            while (zVar != null) {
                if (zVar == this.d) {
                    c.c(this.g);
                    this.f++;
                    if (zVar2 == null) {
                        b2[length] = zVar.d;
                    } else {
                        zVar2.d = zVar.d;
                    }
                    c.d(this.g);
                    this.d = null;
                    return;
                }
                z zVar3 = zVar;
                zVar = zVar.d;
                zVar2 = zVar3;
            }
            throw new g();
        }
    }
}
