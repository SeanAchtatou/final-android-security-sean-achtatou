package com.agilebinary.b.a.a;

public abstract class i implements m {
    private transient n a = null;

    protected i() {
    }

    public int a() {
        return d().b();
    }

    public Object a(Object obj, Object obj2) {
        throw new UnsupportedOperationException();
    }

    public boolean a(Object obj) {
        a a2 = d().a();
        if (obj == null) {
            while (a2.a()) {
                if (((y) a2.b()).a() == null) {
                    return true;
                }
            }
        } else {
            while (a2.a()) {
                if (obj.equals(((y) a2.b()).a())) {
                    return true;
                }
            }
        }
        return false;
    }

    public Object b(Object obj) {
        a a2 = d().a();
        if (obj == null) {
            while (a2.a()) {
                y yVar = (y) a2.b();
                if (yVar.a() == null) {
                    return yVar.b();
                }
            }
        } else {
            while (a2.a()) {
                y yVar2 = (y) a2.b();
                if (obj.equals(yVar2.a())) {
                    return yVar2.b();
                }
            }
        }
        return null;
    }

    public void b() {
        d().c();
    }

    public n c() {
        if (this.a == null) {
            this.a = new x(this);
        }
        return this.a;
    }

    public abstract n d();

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof m)) {
            return false;
        }
        m mVar = (m) obj;
        if (mVar.a() != a()) {
            return false;
        }
        a a2 = d().a();
        while (a2.a()) {
            y yVar = (y) a2.b();
            Object a3 = yVar.a();
            Object b = yVar.b();
            if (b == null) {
                if (mVar.b(a3) != null || !mVar.a(a3)) {
                    return false;
                }
            } else if (!b.equals(mVar.b(a3))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        a a2 = d().a();
        while (a2.a()) {
            i += a2.b().hashCode();
        }
        return i;
    }

    public String toString() {
        int a2 = a() - 1;
        StringBuffer stringBuffer = new StringBuffer();
        a a3 = d().a();
        stringBuffer.append("{");
        for (int i = 0; i <= a2; i++) {
            y yVar = (y) a3.b();
            stringBuffer.append(yVar.a() + "=" + yVar.b());
            if (i < a2) {
                stringBuffer.append(", ");
            }
        }
        stringBuffer.append("}");
        return stringBuffer.toString();
    }
}
