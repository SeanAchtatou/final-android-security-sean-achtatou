package com.agilebinary.b.a.a;

public abstract class l extends o implements n {
    protected l() {
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof n)) {
            return false;
        }
        aa aaVar = (aa) obj;
        if (aaVar.b() != b()) {
            return false;
        }
        a a = aaVar.a();
        while (a.a()) {
            if (!a(a.b())) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        a a = a();
        while (a.a()) {
            Object b = a.b();
            if (b != null) {
                i += b.hashCode();
            }
        }
        return i;
    }
}
