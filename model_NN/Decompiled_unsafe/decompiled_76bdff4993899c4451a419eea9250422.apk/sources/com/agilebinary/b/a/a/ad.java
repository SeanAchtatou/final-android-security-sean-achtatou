package com.agilebinary.b.a.a;

public abstract class ad extends o implements u {
    protected transient int a = 0;

    protected ad() {
    }

    private s c(int i) {
        if (i >= 0 && i <= b()) {
            return new t(this, i);
        }
        throw new IndexOutOfBoundsException("Index: " + i);
    }

    public final a a() {
        return new b(this);
    }

    public abstract Object a(int i);

    public Object a(int i, Object obj) {
        throw new UnsupportedOperationException();
    }

    /* access modifiers changed from: protected */
    public void a(int i, int i2) {
        s c = c(0);
        int i3 = i2 + 0;
        for (int i4 = 0; i4 < i3; i4++) {
            c.b();
            c.c();
        }
    }

    public Object b(int i) {
        throw new UnsupportedOperationException();
    }

    public void b(int i, Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean b(Object obj) {
        b(b(), obj);
        return true;
    }

    public int c(Object obj) {
        s c = c(0);
        if (obj == null) {
            while (c.a()) {
                if (c.b() == null) {
                    return c.j_();
                }
            }
        } else {
            while (c.a()) {
                if (obj.equals(c.b())) {
                    return c.j_();
                }
            }
        }
        return -1;
    }

    public void c() {
        a(0, b());
    }

    public final s e() {
        return c(0);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof u)) {
            return false;
        }
        s c = c(0);
        s e = ((u) obj).e();
        while (c.a() && e.a()) {
            Object b = c.b();
            Object b2 = e.b();
            if (b == null) {
                if (b2 != null) {
                }
            } else if (!b.equals(b2)) {
            }
            return false;
        }
        return !c.a() && !e.a();
    }

    public int hashCode() {
        int i = 1;
        a a2 = a();
        while (a2.a()) {
            Object b = a2.b();
            i = (b == null ? 0 : b.hashCode()) + (i * 31);
        }
        return i;
    }
}
