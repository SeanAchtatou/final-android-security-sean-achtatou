package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.z;
import java.io.IOException;

public final class b {
    public static void a(f fVar, ab abVar, i iVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (abVar == null) {
            throw new IllegalArgumentException("HTTP processor may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            iVar.a("http.request", fVar);
            abVar.a(fVar, iVar);
        }
    }

    public static void a(j jVar, ab abVar, i iVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        } else if (abVar == null) {
            throw new IllegalArgumentException("HTTP processor may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            iVar.a("http.response", jVar);
            abVar.a(jVar, iVar);
        }
    }

    private static final void a(z zVar) {
        try {
            zVar.k();
        } catch (IOException e) {
        }
    }

    private static boolean a(f fVar, j jVar) {
        int b;
        return ("HEAD".equalsIgnoreCase(fVar.a().a()) || (b = jVar.a().b()) < 200 || b == 204 || b == 304 || b == 205) ? false : true;
    }

    private j b(f fVar, z zVar, i iVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (zVar == null) {
            throw new IllegalArgumentException("HTTP connection may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            j jVar = null;
            int i = 0;
            while (true) {
                if (jVar != null && i >= 200) {
                    return jVar;
                }
                jVar = zVar.d();
                if (a(fVar, jVar)) {
                    zVar.a(jVar);
                }
                i = jVar.a().b();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x00cc A[Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.a.a.a.j a(com.agilebinary.a.a.a.f r7, com.agilebinary.a.a.a.z r8, com.agilebinary.a.a.a.f.i r9) {
        /*
            r6 = this;
            r2 = 0
            if (r7 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "HTTP request may not be null"
            r1.<init>(r2)
            throw r1
        L_0x000b:
            if (r8 != 0) goto L_0x0015
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Client connection may not be null"
            r1.<init>(r2)
            throw r1
        L_0x0015:
            if (r9 != 0) goto L_0x001f
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "HTTP context may not be null"
            r1.<init>(r2)
            throw r1
        L_0x001f:
            if (r7 != 0) goto L_0x002e
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r2 = "HTTP request may not be null"
            r1.<init>(r2)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            throw r1     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x0029:
            r1 = move-exception
            a(r8)
            throw r1
        L_0x002e:
            if (r8 != 0) goto L_0x003d
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r2 = "HTTP connection may not be null"
            r1.<init>(r2)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            throw r1     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x0038:
            r1 = move-exception
            a(r8)
            throw r1
        L_0x003d:
            if (r9 != 0) goto L_0x004c
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r2 = "HTTP context may not be null"
            r1.<init>(r2)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            throw r1     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x0047:
            r1 = move-exception
            a(r8)
            throw r1
        L_0x004c:
            java.lang.String r1 = "http.connection"
            r9.a(r1, r8)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r1 = "http.request_sent"
            java.lang.Boolean r3 = java.lang.Boolean.FALSE     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r9.a(r1, r3)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r8.a(r7)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            boolean r1 = r7 instanceof com.agilebinary.a.a.a.p     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r1 == 0) goto L_0x00ea
            r3 = 1
            com.agilebinary.a.a.a.d r1 = r7.a()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            com.agilebinary.a.a.a.a r4 = r1.b()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r0 = r7
            com.agilebinary.a.a.a.p r0 = (com.agilebinary.a.a.a.p) r0     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r1 = r0
            boolean r1 = r1.f_()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r1 == 0) goto L_0x00e8
            com.agilebinary.a.a.a.aa r1 = com.agilebinary.a.a.a.aa.c     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            boolean r1 = r4.a(r1)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r1 != 0) goto L_0x00e8
            r8.c()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            com.agilebinary.a.a.a.e.b r1 = r7.g()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r4 = "http.protocol.wait-for-continue"
            r5 = 2000(0x7d0, float:2.803E-42)
            int r1 = r1.a(r4, r5)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            boolean r1 = r8.a(r1)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r1 == 0) goto L_0x00e8
            com.agilebinary.a.a.a.j r4 = r8.d()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            boolean r1 = a(r7, r4)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r1 == 0) goto L_0x009c
            r8.a(r4)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x009c:
            com.agilebinary.a.a.a.h r1 = r4.a()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            int r1 = r1.b()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r1 >= r5) goto L_0x00e5
            r5 = 100
            if (r1 == r5) goto L_0x00c9
            java.net.ProtocolException r1 = new java.net.ProtocolException     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r2.<init>()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r3 = "Unexpected response: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            com.agilebinary.a.a.a.h r3 = r4.a()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            throw r1     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x00c9:
            r1 = r3
        L_0x00ca:
            if (r1 == 0) goto L_0x00d3
            r0 = r7
            com.agilebinary.a.a.a.p r0 = (com.agilebinary.a.a.a.p) r0     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r1 = r0
            r8.a(r1)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x00d3:
            r1 = r2
        L_0x00d4:
            r8.c()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r2 = "http.request_sent"
            java.lang.Boolean r3 = java.lang.Boolean.TRUE     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r9.a(r2, r3)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r1 != 0) goto L_0x00e4
            com.agilebinary.a.a.a.j r1 = r6.b(r7, r8, r9)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x00e4:
            return r1
        L_0x00e5:
            r1 = 0
            r2 = r4
            goto L_0x00ca
        L_0x00e8:
            r1 = r3
            goto L_0x00ca
        L_0x00ea:
            r1 = r2
            goto L_0x00d4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.f.b.a(com.agilebinary.a.a.a.f, com.agilebinary.a.a.a.z, com.agilebinary.a.a.a.f.i):com.agilebinary.a.a.a.j");
    }
}
