package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.d.e;
import com.agilebinary.a.a.a.d.f;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.f.h;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.h.j;
import com.agilebinary.a.a.a.h.m;
import com.agilebinary.a.a.a.k;
import com.agilebinary.a.a.a.k.d;
import com.agilebinary.a.a.a.s;
import com.agilebinary.a.a.b.a.a;
import java.net.URI;
import org.apache.commons.logging.Log;

public abstract class c {
    private final Log a = a.a(getClass());
    private b b;
    private com.agilebinary.a.a.a.f.b c;
    private j d;
    private s e;
    private m f;
    private d g;
    private com.agilebinary.a.a.a.a.a h;
    private h i;
    private a j;
    private com.agilebinary.a.a.a.d.j k;
    private com.agilebinary.a.a.a.d.c l;
    private com.agilebinary.a.a.a.d.b m;
    private com.agilebinary.a.a.a.d.b n;
    private com.agilebinary.a.a.a.d.d o;
    private e p;
    private com.agilebinary.a.a.a.h.c.h q;
    private f r;

    protected c(j jVar, b bVar) {
        this.b = bVar;
        this.d = jVar;
    }

    private synchronized com.agilebinary.a.a.a.d.c A() {
        if (this.l == null) {
            this.l = new m();
        }
        return this.l;
    }

    private synchronized com.agilebinary.a.a.a.d.b B() {
        if (this.m == null) {
            this.m = o();
        }
        return this.m;
    }

    private synchronized com.agilebinary.a.a.a.d.b C() {
        if (this.n == null) {
            this.n = p();
        }
        return this.n;
    }

    private synchronized com.agilebinary.a.a.a.h.c.h D() {
        if (this.q == null) {
            this.q = s();
        }
        return this.q;
    }

    private synchronized f E() {
        if (this.r == null) {
            this.r = t();
        }
        return this.r;
    }

    private synchronized h F() {
        if (this.i == null) {
            this.i = m();
        }
        return this.i;
    }

    private final synchronized ab G() {
        a aVar;
        synchronized (this) {
            if (this.j == null) {
                h F = F();
                int a2 = F.a();
                ab[] abVarArr = new ab[a2];
                for (int i2 = 0; i2 < a2; i2++) {
                    abVarArr[i2] = F.a(i2);
                }
                int b2 = F.b();
                ad[] adVarArr = new ad[b2];
                for (int i3 = 0; i3 < b2; i3++) {
                    adVarArr[i3] = F.b(i3);
                }
                this.j = new a(abVarArr, adVarArr);
            }
            aVar = this.j;
        }
        return aVar;
    }

    private synchronized com.agilebinary.a.a.a.f.b a() {
        if (this.c == null) {
            this.c = g();
        }
        return this.c;
    }

    private com.agilebinary.a.a.a.j a(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar, i iVar) {
        com.agilebinary.a.a.a.f.e eVar;
        p pVar;
        if (fVar == null) {
            throw new IllegalArgumentException("Request must not be null.");
        }
        synchronized (this) {
            i f2 = f();
            eVar = iVar == null ? f2 : new com.agilebinary.a.a.a.f.e(iVar, f2);
            pVar = new p(this.a, a(), v(), b(), c(), D(), G(), d(), A(), B(), C(), E(), new n(null, u(), fVar.g(), null));
        }
        try {
            return pVar.a(bVar, fVar, eVar);
        } catch (k e2) {
            throw new com.agilebinary.a.a.a.d.h(e2);
        }
    }

    private synchronized s b() {
        if (this.e == null) {
            this.e = k();
        }
        return this.e;
    }

    private synchronized m c() {
        if (this.f == null) {
            this.f = l();
        }
        return this.f;
    }

    private synchronized com.agilebinary.a.a.a.d.j d() {
        if (this.k == null) {
            this.k = n();
        }
        return this.k;
    }

    public final com.agilebinary.a.a.a.j a(com.agilebinary.a.a.a.d.c.b bVar) {
        com.agilebinary.a.a.a.b bVar2;
        if (bVar == null) {
            throw new IllegalArgumentException("Request must not be null.");
        }
        URI c_ = bVar.c_();
        if (c_.isAbsolute()) {
            String schemeSpecificPart = c_.getSchemeSpecificPart();
            String substring = schemeSpecificPart.substring(2, schemeSpecificPart.length());
            String substring2 = substring.substring(0, substring.indexOf(58) > 0 ? substring.indexOf(58) : substring.indexOf(47) > 0 ? substring.indexOf(47) : substring.indexOf(63) > 0 ? substring.indexOf(63) : substring.length());
            int port = c_.getPort();
            String scheme = c_.getScheme();
            if (substring2 == null || "".equals(substring2)) {
                throw new com.agilebinary.a.a.a.d.h("URI does not specify a valid host name: " + c_);
            }
            bVar2 = new com.agilebinary.a.a.a.b(substring2, port, scheme);
        } else {
            bVar2 = null;
        }
        return a(bVar2, bVar, null);
    }

    /* access modifiers changed from: protected */
    public abstract b e();

    /* access modifiers changed from: protected */
    public abstract i f();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.f.b g();

    /* access modifiers changed from: protected */
    public abstract j h();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.a.a i();

    /* access modifiers changed from: protected */
    public abstract d j();

    /* access modifiers changed from: protected */
    public abstract s k();

    /* access modifiers changed from: protected */
    public abstract m l();

    /* access modifiers changed from: protected */
    public abstract h m();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.j n();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.b o();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.b p();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.d q();

    /* access modifiers changed from: protected */
    public abstract e r();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.h.c.h s();

    /* access modifiers changed from: protected */
    public abstract f t();

    public final synchronized b u() {
        if (this.b == null) {
            this.b = e();
        }
        return this.b;
    }

    public final synchronized j v() {
        if (this.d == null) {
            this.d = h();
        }
        return this.d;
    }

    public final synchronized com.agilebinary.a.a.a.a.a w() {
        if (this.h == null) {
            this.h = i();
        }
        return this.h;
    }

    public final synchronized d x() {
        if (this.g == null) {
            this.g = j();
        }
        return this.g;
    }

    public final synchronized com.agilebinary.a.a.a.d.d y() {
        if (this.o == null) {
            this.o = q();
        }
        return this.o;
    }

    public final synchronized e z() {
        if (this.p == null) {
            this.p = r();
        }
        return this.p;
    }
}
