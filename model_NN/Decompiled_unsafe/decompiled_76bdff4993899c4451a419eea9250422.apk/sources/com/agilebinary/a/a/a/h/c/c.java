package com.agilebinary.a.a.a.h.c;

import com.agilebinary.a.a.a.b;
import java.net.InetAddress;

public final class c implements b, Cloneable {
    private static final b[] a = new b[0];
    private final b b;
    private final InetAddress c;
    private final b[] d;
    private final g e;
    private final e f;
    private final boolean g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.h.c.c.<init>(java.net.InetAddress, com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void
     arg types: [?[OBJECT, ARRAY], com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], int, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e]
     candidates:
      com.agilebinary.a.a.a.h.c.c.<init>(com.agilebinary.a.a.a.b, java.net.InetAddress, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void
      com.agilebinary.a.a.a.h.c.c.<init>(java.net.InetAddress, com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void */
    public c(b bVar) {
        this((InetAddress) null, bVar, a, false, g.a, e.a);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public c(b bVar, InetAddress inetAddress, b bVar2, boolean z) {
        this(inetAddress, bVar, bVar2 == null ? a : new b[]{bVar2}, z, z ? g.b : g.a, z ? e.b : e.a);
        if (bVar2 == null) {
            throw new IllegalArgumentException("Proxy host may not be null.");
        }
    }

    public c(b bVar, InetAddress inetAddress, boolean z) {
        this(inetAddress, bVar, a, z, g.a, e.a);
    }

    public c(b bVar, InetAddress inetAddress, b[] bVarArr, boolean z, g gVar, e eVar) {
        this(inetAddress, bVar, a(bVarArr), z, gVar, eVar);
    }

    private c(InetAddress inetAddress, b bVar, b[] bVarArr, boolean z, g gVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        } else if (bVarArr == null) {
            throw new IllegalArgumentException("Proxies may not be null.");
        } else if (gVar == g.b && bVarArr.length == 0) {
            throw new IllegalArgumentException("Proxy required if tunnelled.");
        } else {
            gVar = gVar == null ? g.a : gVar;
            eVar = eVar == null ? e.a : eVar;
            this.b = bVar;
            this.c = inetAddress;
            this.d = bVarArr;
            this.g = z;
            this.e = gVar;
            this.f = eVar;
        }
    }

    private static b[] a(b[] bVarArr) {
        if (bVarArr == null || bVarArr.length <= 0) {
            return a;
        }
        for (b bVar : bVarArr) {
            if (bVar == null) {
                throw new IllegalArgumentException("Proxy chain may not contain null elements.");
            }
        }
        b[] bVarArr2 = new b[bVarArr.length];
        System.arraycopy(bVarArr, 0, bVarArr2, 0, bVarArr.length);
        return bVarArr2;
    }

    public final b a() {
        return this.b;
    }

    public final b a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Hop index must not be negative: " + i);
        }
        int length = this.d.length + 1;
        if (i < length) {
            return i < length + -1 ? this.d[i] : this.b;
        }
        throw new IllegalArgumentException("Hop index " + i + " exceeds route length " + length);
    }

    public final InetAddress b() {
        return this.c;
    }

    public final int c() {
        return this.d.length + 1;
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean d() {
        return this.e == g.b;
    }

    public final boolean e() {
        return this.f == e.b;
    }

    public final boolean equals(Object obj) {
        boolean z = true;
        int i = 0;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        boolean equals = (this.d == cVar.d || this.d.length == cVar.d.length) & this.b.equals(cVar.b) & (this.c == cVar.c || (this.c != null && this.c.equals(cVar.c)));
        if (!(this.g == cVar.g && this.e == cVar.e && this.f == cVar.f)) {
            z = false;
        }
        boolean z2 = equals & z;
        if (z2 && this.d != null) {
            while (z2 && i < this.d.length) {
                z2 = this.d[i].equals(cVar.d[i]);
                i++;
            }
        }
        return z2;
    }

    public final boolean f() {
        return this.g;
    }

    public final b g() {
        if (this.d.length == 0) {
            return null;
        }
        return this.d[0];
    }

    public final int hashCode() {
        int hashCode = this.b.hashCode();
        if (this.c != null) {
            hashCode ^= this.c.hashCode();
        }
        b[] bVarArr = this.d;
        int length = bVarArr.length;
        int length2 = this.d.length ^ hashCode;
        int i = 0;
        while (i < length) {
            int hashCode2 = bVarArr[i].hashCode() ^ length2;
            i++;
            length2 = hashCode2;
        }
        if (this.g) {
            length2 ^= 286331153;
        }
        return (length2 ^ this.e.hashCode()) ^ this.f.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(((this.d.length + 1) * 30) + 50);
        sb.append("HttpRoute[");
        if (this.c != null) {
            sb.append(this.c);
            sb.append("->");
        }
        sb.append('{');
        if (this.e == g.b) {
            sb.append('t');
        }
        if (this.f == e.b) {
            sb.append('l');
        }
        if (this.g) {
            sb.append('s');
        }
        sb.append("}->");
        for (b append : this.d) {
            sb.append(append);
            sb.append("->");
        }
        sb.append(this.b);
        sb.append(']');
        return sb.toString();
    }
}
