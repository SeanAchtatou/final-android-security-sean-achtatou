package com.agilebinary.a.a.b.a;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.j;
import org.apache.commons.logging.Log;

public class a implements ab {
    private final ab[] a;
    private final ad[] b;

    public a() {
    }

    public a(ab[] abVarArr, ad[] adVarArr) {
        if (abVarArr != null) {
            int length = abVarArr.length;
            this.a = new ab[length];
            for (int i = 0; i < length; i++) {
                this.a[i] = abVarArr[i];
            }
        } else {
            this.a = new ab[0];
        }
        if (adVarArr != null) {
            int length2 = adVarArr.length;
            this.b = new ad[length2];
            for (int i2 = 0; i2 < length2; i2++) {
                this.b[i2] = adVarArr[i2];
            }
            return;
        }
        this.b = new ad[0];
    }

    public static Log a(Class cls) {
        return new b(cls.getName());
    }

    public static Log a(String str) {
        return new b(str);
    }

    public void a(f fVar, i iVar) {
        for (ab a2 : this.a) {
            a2.a(fVar, iVar);
        }
    }

    public void a(j jVar, i iVar) {
        for (ad a2 : this.b) {
            a2.a(jVar, iVar);
        }
    }
}
