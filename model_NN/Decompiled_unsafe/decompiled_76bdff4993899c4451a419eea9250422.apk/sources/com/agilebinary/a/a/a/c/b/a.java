package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.c.f;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.l;
import java.io.IOException;

public abstract class a {
    protected final g a;
    protected volatile c b;
    protected volatile f c;
    private l d;
    private volatile Object e;

    protected a(l lVar, c cVar) {
        if (lVar == null) {
            throw new IllegalArgumentException("Connection operator may not be null");
        }
        this.d = lVar;
        this.a = lVar.a();
        this.b = cVar;
        this.c = null;
    }

    public final Object a() {
        return this.e;
    }

    public final void a(i iVar, b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else if (this.c == null || !this.c.g()) {
            throw new IllegalStateException("Connection not open.");
        } else if (!this.c.d()) {
            throw new IllegalStateException("Protocol layering without a tunnel not supported.");
        } else if (this.c.e()) {
            throw new IllegalStateException("Multiple protocol layering not supported.");
        } else {
            this.d.a(this.a, this.c.a(), iVar, bVar);
            this.c.c(this.a.h_());
        }
    }

    public final void a(c cVar, i iVar, b bVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Route must not be null.");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else if (this.c == null || !this.c.g()) {
            this.c = new f(cVar);
            com.agilebinary.a.a.a.b g = cVar.g();
            this.d.a(this.a, g != null ? g : cVar.a(), cVar.b(), iVar, bVar);
            f fVar = this.c;
            if (fVar == null) {
                throw new IOException("Request aborted");
            } else if (g == null) {
                fVar.a(this.a.h_());
            } else {
                fVar.a(g, this.a.h_());
            }
        } else {
            throw new IllegalStateException("Connection already open.");
        }
    }

    public final void a(Object obj) {
        this.e = obj;
    }

    public final void a(boolean z, b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else if (this.c == null || !this.c.g()) {
            throw new IllegalStateException("Connection not open.");
        } else if (this.c.d()) {
            throw new IllegalStateException("Connection is already tunnelled.");
        } else {
            this.a.a(null, this.c.a(), z, bVar);
            this.c.b(z);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.c = null;
        this.e = null;
    }
}
