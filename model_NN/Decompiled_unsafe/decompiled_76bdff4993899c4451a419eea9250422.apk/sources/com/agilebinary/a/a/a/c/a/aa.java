package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.a;
import com.agilebinary.a.a.a.b.f;
import com.agilebinary.a.a.a.b.i;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import java.util.ArrayList;

public final class aa {
    public static final aa a = new aa();
    private static final char[] b = {';'};
    private final f c = f.a;

    public final m a(b bVar, i iVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            o a2 = this.c.a(bVar, iVar, b);
            ArrayList arrayList = new ArrayList();
            while (!iVar.c()) {
                arrayList.add(this.c.a(bVar, iVar, b));
            }
            return new a(a2.a(), a2.b(), (o[]) arrayList.toArray(new o[arrayList.size()]));
        }
    }
}
