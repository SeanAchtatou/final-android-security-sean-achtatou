package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.d.c;
import com.agilebinary.a.a.a.d.c.d;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.b.a.a;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.commons.logging.Log;

public final class m implements c {
    private final Log a = a.a(getClass());

    private static URI a(String str) {
        try {
            return new URI(str);
        } catch (URISyntaxException e) {
            throw new l("Invalid redirect URI: " + str, e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.a.b.a(java.net.URI, com.agilebinary.a.a.a.b, boolean):java.net.URI
     arg types: [java.net.URI, com.agilebinary.a.a.a.b, int]
     candidates:
      com.agilebinary.mobilemonitor.device.android.a.b.a(java.lang.String, java.lang.String, java.lang.Throwable):void
      com.agilebinary.mobilemonitor.device.a.e.c.a(java.lang.String, java.lang.String, java.lang.Throwable):void
      com.agilebinary.mobilemonitor.device.android.a.b.a(java.net.URI, com.agilebinary.a.a.a.b, boolean):java.net.URI */
    private URI b(f fVar, j jVar, i iVar) {
        URI uri;
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        t c = jVar.c("location");
        if (c == null) {
            throw new l("Received redirect response " + jVar.a() + " but no location header");
        }
        String b = c.b();
        if (this.a.isDebugEnabled()) {
            this.a.debug("Redirect requested to location '" + b + "'");
        }
        URI a2 = a(b);
        b g = jVar.g();
        if (!a2.isAbsolute()) {
            if (g.b("http.protocol.reject-relative-redirect")) {
                throw new l("Relative redirect location '" + a2 + "' not allowed");
            }
            com.agilebinary.a.a.a.b bVar = (com.agilebinary.a.a.a.b) iVar.a("http.target_host");
            if (bVar == null) {
                throw new IllegalStateException("Target host not available in the HTTP context");
            }
            try {
                a2 = com.agilebinary.mobilemonitor.device.android.a.b.a(com.agilebinary.mobilemonitor.device.android.a.b.a(new URI(fVar.a().c()), bVar, true), a2);
            } catch (URISyntaxException e) {
                throw new l(e.getMessage(), e);
            }
        }
        if (g.c("http.protocol.allow-circular-redirects")) {
            o oVar = (o) iVar.a("http.protocol.redirect-locations");
            if (oVar == null) {
                oVar = new o();
                iVar.a("http.protocol.redirect-locations", oVar);
            }
            if (a2.getFragment() != null) {
                try {
                    uri = com.agilebinary.mobilemonitor.device.android.a.b.a(a2, new com.agilebinary.a.a.a.b(a2.getHost(), a2.getPort(), a2.getScheme()), true);
                } catch (URISyntaxException e2) {
                    throw new l(e2.getMessage(), e2);
                }
            } else {
                uri = a2;
            }
            if (oVar.a(uri)) {
                throw new com.agilebinary.a.a.a.d.l("Circular redirect to '" + uri + "'");
            }
            oVar.b(uri);
        }
        return a2;
    }

    public final com.agilebinary.a.a.a.d.c.b a(f fVar, j jVar, i iVar) {
        URI b = b(fVar, jVar, iVar);
        return fVar.a().a().equalsIgnoreCase("HEAD") ? new d(b) : new com.agilebinary.a.a.a.d.c.a(b);
    }

    public final boolean a(f fVar, j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        int b = jVar.a().b();
        String a2 = fVar.a().a();
        t c = jVar.c("location");
        switch (b) {
            case 301:
            case 307:
                return a2.equalsIgnoreCase("GET") || a2.equalsIgnoreCase("HEAD");
            case 302:
                return (a2.equalsIgnoreCase("GET") || a2.equalsIgnoreCase("HEAD")) && c != null;
            case 303:
                return true;
            case 304:
            case 305:
            case 306:
            default:
                return false;
        }
    }
}
