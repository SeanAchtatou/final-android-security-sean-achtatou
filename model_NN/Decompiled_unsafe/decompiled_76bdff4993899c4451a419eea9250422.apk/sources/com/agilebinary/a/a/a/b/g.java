package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import java.util.List;
import java.util.NoSuchElementException;

public final class g implements w {
    private List a;
    private int b;
    private int c;
    private String d;

    public g(List list, String str) {
        if (list == null) {
            throw new IllegalArgumentException("Header list must not be null.");
        }
        this.a = list;
        this.d = str;
        this.b = a(-1);
        this.c = -1;
    }

    private int a(int i) {
        boolean equalsIgnoreCase;
        if (i < -1) {
            return -1;
        }
        int size = this.a.size() - 1;
        boolean z = false;
        int i2 = i;
        while (!z && i2 < size) {
            int i3 = i2 + 1;
            if (this.d == null) {
                equalsIgnoreCase = true;
            } else {
                equalsIgnoreCase = this.d.equalsIgnoreCase(((t) this.a.get(i3)).a());
            }
            z = equalsIgnoreCase;
            i2 = i3;
        }
        if (!z) {
            return -1;
        }
        return i2;
    }

    public final t a() {
        int i = this.b;
        if (i < 0) {
            throw new NoSuchElementException("Iteration already finished.");
        }
        this.c = i;
        this.b = a(i);
        return (t) this.a.get(i);
    }

    public final boolean hasNext() {
        return this.b >= 0;
    }

    public final Object next() {
        return a();
    }

    public final void remove() {
        if (this.c < 0) {
            throw new IllegalStateException("No header to remove.");
        }
        this.a.remove(this.c);
        this.c = -1;
        this.b--;
    }
}
