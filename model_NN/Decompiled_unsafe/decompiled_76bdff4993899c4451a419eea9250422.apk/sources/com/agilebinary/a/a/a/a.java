package com.agilebinary.a.a.a;

import com.agilebinary.a.a.a.i.b;
import java.io.Serializable;

public class a implements Serializable, Cloneable {
    protected final int a;
    protected final int b;
    private String c;

    public a(String str, int i, int i2) {
        if (str == null) {
            throw new IllegalArgumentException("Protocol name must not be null.");
        } else if (i < 0) {
            throw new IllegalArgumentException("Protocol major version number must not be negative.");
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Protocol minor version number may not be negative");
        } else {
            this.c = str;
            this.a = i;
            this.b = i2;
        }
    }

    public a a(int i, int i2) {
        return (i == this.a && i2 == this.b) ? this : new a(this.c, i, i2);
    }

    public final String a() {
        return this.c;
    }

    public final boolean a(a aVar) {
        if (aVar != null && this.c.equals(aVar.c)) {
            if (aVar == null) {
                throw new IllegalArgumentException("Protocol version must not be null.");
            } else if (!this.c.equals(aVar.c)) {
                throw new IllegalArgumentException("Versions for different protocols cannot be compared. " + this + " " + aVar);
            } else {
                int i = this.a - aVar.a;
                if (i == 0) {
                    i = this.b - aVar.b;
                }
                if (i <= 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public final int b() {
        return this.a;
    }

    public final int c() {
        return this.b;
    }

    public Object clone() {
        return super.clone();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return this.c.equals(aVar.c) && this.a == aVar.a && this.b == aVar.b;
    }

    public final int hashCode() {
        return (this.c.hashCode() ^ (this.a * 100000)) ^ this.b;
    }

    public String toString() {
        b bVar = new b(16);
        bVar.a(this.c);
        bVar.a('/');
        bVar.a(Integer.toString(this.a));
        bVar.a('.');
        bVar.a(Integer.toString(this.b));
        return bVar.toString();
    }
}
