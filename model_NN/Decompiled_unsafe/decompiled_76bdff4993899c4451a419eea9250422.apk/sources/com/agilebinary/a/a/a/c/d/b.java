package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.j;
import java.util.List;
import java.util.Map;

public final class b extends q {
    /* access modifiers changed from: protected */
    public final List a(j jVar, i iVar) {
        List list = (List) jVar.g().a("http.auth.target-scheme-pref");
        return list != null ? list : super.a(jVar, iVar);
    }

    public final boolean a(j jVar) {
        if (jVar != null) {
            return jVar.a().b() == 401;
        }
        throw new IllegalArgumentException("HTTP response may not be null");
    }

    public final Map b(j jVar) {
        if (jVar != null) {
            return a(jVar.b("WWW-Authenticate"));
        }
        throw new IllegalArgumentException("HTTP response may not be null");
    }
}
