package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.d;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.a.t;

public final class q implements c {
    public static final q a = new q();

    private static int a(a aVar) {
        return aVar.a().length() + 4;
    }

    private static b a(b bVar) {
        if (bVar == null) {
            return new b(64);
        }
        bVar.a();
        return bVar;
    }

    private b a(b bVar, a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Protocol version may not be null");
        }
        int a2 = a(aVar);
        if (bVar == null) {
            bVar = new b(a2);
        } else {
            bVar.b(a2);
        }
        bVar.a(aVar.a());
        bVar.a('/');
        bVar.a(Integer.toString(aVar.b()));
        bVar.a('.');
        bVar.a(Integer.toString(aVar.c()));
        return bVar;
    }

    public final b a(b bVar, d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("Request line may not be null");
        }
        b a2 = a(bVar);
        String a3 = dVar.a();
        String c = dVar.c();
        a2.b(a3.length() + 1 + c.length() + 1 + a(dVar.b()));
        a2.a(a3);
        a2.a(' ');
        a2.a(c);
        a2.a(' ');
        a(a2, dVar.b());
        return a2;
    }

    public final b a(b bVar, h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException("Status line may not be null");
        }
        b a2 = a((b) null);
        int a3 = a(hVar.a()) + 1 + 3 + 1;
        String c = hVar.c();
        if (c != null) {
            a3 += c.length();
        }
        a2.b(a3);
        a(a2, hVar.a());
        a2.a(' ');
        a2.a(Integer.toString(hVar.b()));
        a2.a(' ');
        if (c != null) {
            a2.a(c);
        }
        return a2;
    }

    public final b a(b bVar, t tVar) {
        if (tVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (tVar instanceof r) {
            return ((r) tVar).e();
        } else {
            b a2 = a(bVar);
            String a3 = tVar.a();
            String b = tVar.b();
            int length = a3.length() + 2;
            if (b != null) {
                length += b.length();
            }
            a2.b(length);
            a2.a(a3);
            a2.a(": ");
            if (b != null) {
                a2.a(b);
            }
            return a2;
        }
    }
}
