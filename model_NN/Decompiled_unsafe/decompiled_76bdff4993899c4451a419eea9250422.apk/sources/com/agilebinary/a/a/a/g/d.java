package com.agilebinary.a.a.a.g;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class d extends f implements Cloneable {
    private byte[] a;

    public d(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("Source byte array may not be null");
        }
        this.a = bArr;
    }

    public final void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        outputStream.write(this.a);
        outputStream.flush();
    }

    public final boolean a() {
        return true;
    }

    public final long c() {
        return (long) this.a.length;
    }

    public final Object clone() {
        return super.clone();
    }

    public final InputStream f() {
        return new ByteArrayInputStream(this.a);
    }

    public final boolean g() {
        return false;
    }
}
