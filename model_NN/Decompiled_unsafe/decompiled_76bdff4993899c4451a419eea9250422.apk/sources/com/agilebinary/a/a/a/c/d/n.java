package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.e.b;

public final class n extends b {
    private b a = null;
    private b b;
    private b c;
    private b d;

    public n(b bVar, b bVar2, b bVar3, b bVar4) {
        this.b = bVar2;
        this.c = bVar3;
        this.d = null;
    }

    public final b a(String str, Object obj) {
        throw new UnsupportedOperationException("Setting parameters in a stack is not supported.");
    }

    public final Object a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Parameter name must not be null.");
        }
        Object obj = null;
        if (this.d != null) {
            obj = this.d.a(str);
        }
        if (obj == null && this.c != null) {
            obj = this.c.a(str);
        }
        if (obj == null && this.b != null) {
            obj = this.b.a(str);
        }
        return (obj != null || this.a == null) ? obj : this.a.a(str);
    }
}
