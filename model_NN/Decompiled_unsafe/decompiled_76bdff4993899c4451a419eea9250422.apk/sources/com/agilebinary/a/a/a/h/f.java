package com.agilebinary.a.a.a.h;

import com.agilebinary.a.a.a.b;
import java.net.ConnectException;

public final class f extends ConnectException {
    public f(b bVar, ConnectException connectException) {
        super("Connection to " + bVar + " refused");
        initCause(connectException);
    }
}
