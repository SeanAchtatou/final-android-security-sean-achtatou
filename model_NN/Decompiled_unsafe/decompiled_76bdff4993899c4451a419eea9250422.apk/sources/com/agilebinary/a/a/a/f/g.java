package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.ae;
import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.q;
import java.net.InetAddress;

public final class g implements ab {
    public final void a(f fVar, i iVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            a b = fVar.a().b();
            if ((!fVar.a().a().equalsIgnoreCase("CONNECT") || !b.a(aa.c)) && !fVar.a("Host")) {
                b bVar = (b) iVar.a("http.target_host");
                if (bVar == null) {
                    q qVar = (q) iVar.a("http.connection");
                    if (qVar instanceof ae) {
                        InetAddress p = ((ae) qVar).p();
                        int q = ((ae) qVar).q();
                        if (p != null) {
                            bVar = new b(p.getHostName(), q);
                        }
                    }
                    if (bVar == null) {
                        if (!b.a(aa.c)) {
                            throw new l("Target host missing");
                        }
                        return;
                    }
                }
                fVar.a("Host", bVar.d());
            }
        }
    }
}
