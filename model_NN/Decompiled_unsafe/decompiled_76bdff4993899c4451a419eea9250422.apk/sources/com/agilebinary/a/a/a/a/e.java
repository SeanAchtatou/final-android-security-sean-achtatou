package com.agilebinary.a.a.a.a;

import com.agilebinary.a.a.a.h.b.b;

public final class e {
    private f a;
    private c b;
    private b c;

    public final void a() {
        this.a = null;
        this.b = null;
        this.c = null;
    }

    public final void a(c cVar) {
        this.b = cVar;
    }

    public final void a(f fVar) {
        if (fVar == null) {
            a();
        } else {
            this.a = fVar;
        }
    }

    public final void a(b bVar) {
        this.c = bVar;
    }

    public final boolean b() {
        return this.a != null;
    }

    public final f c() {
        return this.a;
    }

    public final b d() {
        return this.c;
    }

    public final c e() {
        return this.b;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("auth scope [");
        sb.append(this.b);
        sb.append("]; credentials set [");
        sb.append(this.c != null ? "true" : "false");
        sb.append("]");
        return sb.toString();
    }
}
