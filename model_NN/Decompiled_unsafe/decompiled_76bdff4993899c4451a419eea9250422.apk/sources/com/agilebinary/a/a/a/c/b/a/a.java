package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.a.b;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.l;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import org.apache.commons.logging.Log;

public final class a extends i {
    private final Log c = com.agilebinary.a.a.b.a.a.a(getClass());
    /* access modifiers changed from: private */
    public final Lock d;
    private l e;
    private b f;
    private Set g;
    private Queue h;
    private Queue i;
    private Map j;
    private final long k;
    private final TimeUnit l;
    private volatile boolean m;
    private volatile int n;
    private volatile int o;

    public a(l lVar, b bVar, int i2, long j2, TimeUnit timeUnit) {
        if (lVar == null) {
            throw new IllegalArgumentException("Connection operator may not be null");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Connections per route may not be null");
        } else {
            this.d = this.a;
            this.g = this.b;
            this.e = lVar;
            this.f = bVar;
            this.n = 20;
            this.h = new LinkedList();
            this.i = new LinkedList();
            this.j = new HashMap();
            this.k = j2;
            this.l = timeUnit;
        }
    }

    private c a(c cVar, boolean z) {
        this.d.lock();
        try {
            c cVar2 = (c) this.j.get(cVar);
            if (cVar2 == null) {
                cVar2 = new c(cVar, this.f);
                this.j.put(cVar, cVar2);
            }
            return cVar2;
        } finally {
            this.d.unlock();
        }
    }

    private g a(c cVar, l lVar) {
        if (this.c.isDebugEnabled()) {
            this.c.debug("Creating new connection [" + cVar.a() + "]");
        }
        g gVar = new g(lVar, cVar.a(), this.k, this.l);
        this.d.lock();
        try {
            cVar.b(gVar);
            this.o++;
            this.g.add(gVar);
            return gVar;
        } finally {
            this.d.unlock();
        }
    }

    private g a(c cVar, Object obj) {
        g gVar = null;
        this.d.lock();
        boolean z = false;
        while (!z) {
            try {
                gVar = cVar.a(obj);
                if (gVar != null) {
                    if (this.c.isDebugEnabled()) {
                        this.c.debug("Getting free connection [" + cVar.a() + "][" + obj + "]");
                    }
                    this.h.remove(gVar);
                    if (gVar.a(System.currentTimeMillis())) {
                        if (this.c.isDebugEnabled()) {
                            this.c.debug("Closing expired free connection [" + cVar.a() + "][" + obj + "]");
                        }
                        a(gVar);
                        cVar.e();
                        this.o--;
                    } else {
                        this.g.add(gVar);
                        z = true;
                    }
                } else if (this.c.isDebugEnabled()) {
                    this.c.debug("No free connections [" + cVar.a() + "][" + obj + "]");
                    z = true;
                } else {
                    z = true;
                }
            } catch (Throwable th) {
                this.d.unlock();
                throw th;
            }
        }
        this.d.unlock();
        return gVar;
    }

    private void a(g gVar) {
        g c2 = gVar.c();
        if (c2 != null) {
            try {
                c2.k();
            } catch (IOException e2) {
                this.c.debug("I/O error closing connection", e2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.h.c.c, boolean):com.agilebinary.a.a.a.c.b.a.c
     arg types: [com.agilebinary.a.a.a.h.c.c, int]
     candidates:
      com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.c.b.a.c, com.agilebinary.a.a.a.h.l):com.agilebinary.a.a.a.c.b.a.g
      com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.c.b.a.c, java.lang.Object):com.agilebinary.a.a.a.c.b.a.g
      com.agilebinary.a.a.a.c.b.a.a.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.a.c.b.a.i.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.h.c.c, boolean):com.agilebinary.a.a.a.c.b.a.c */
    private void b(g gVar) {
        c d2 = gVar.d();
        if (this.c.isDebugEnabled()) {
            this.c.debug("Deleting connection [" + d2 + "][" + gVar.a() + "]");
        }
        this.d.lock();
        try {
            a(gVar);
            c a = a(d2, true);
            a.c(gVar);
            this.o--;
            if (a.c()) {
                this.j.remove(d2);
            }
        } finally {
            this.d.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.h.c.c, boolean):com.agilebinary.a.a.a.c.b.a.c
     arg types: [com.agilebinary.a.a.a.h.c.c, int]
     candidates:
      com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.c.b.a.c, com.agilebinary.a.a.a.h.l):com.agilebinary.a.a.a.c.b.a.g
      com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.c.b.a.c, java.lang.Object):com.agilebinary.a.a.a.c.b.a.g
      com.agilebinary.a.a.a.c.b.a.a.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.a.c.b.a.i.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.h.c.c, boolean):com.agilebinary.a.a.a.c.b.a.c */
    /* access modifiers changed from: protected */
    public final g a(c cVar, Object obj, long j2, TimeUnit timeUnit, f fVar) {
        j jVar;
        Date date = j2 > 0 ? new Date(System.currentTimeMillis() + timeUnit.toMillis(j2)) : null;
        this.d.lock();
        c a = a(cVar, true);
        g gVar = null;
        j jVar2 = null;
        while (gVar == null) {
            if (!this.m) {
                if (this.c.isDebugEnabled()) {
                    this.c.debug("[" + cVar + "] total kept alive: " + this.h.size() + ", total issued: " + this.g.size() + ", total allocated: " + this.o + " out of " + this.n);
                }
                gVar = a(a, obj);
                if (gVar != null) {
                    break;
                }
                boolean z = a.d() > 0;
                if (this.c.isDebugEnabled()) {
                    this.c.debug("Available capacity: " + a.d() + " out of " + a.b() + " [" + cVar + "][" + obj + "]");
                }
                if (z && this.o < this.n) {
                    gVar = a(a, this.e);
                } else if (!z || this.h.isEmpty()) {
                    if (this.c.isDebugEnabled()) {
                        this.c.debug("Need to wait for connection [" + cVar + "][" + obj + "]");
                    }
                    if (jVar2 == null) {
                        jVar = new j(this.d.newCondition(), a);
                        fVar.a(jVar);
                    } else {
                        jVar = jVar2;
                    }
                    a.a(jVar);
                    this.i.add(jVar);
                    boolean a2 = jVar.a(date);
                    a.b(jVar);
                    this.i.remove(jVar);
                    if (a2 || date == null || date.getTime() > System.currentTimeMillis()) {
                        jVar2 = jVar;
                    } else {
                        throw new k("Timeout waiting for connection");
                    }
                } else {
                    this.d.lock();
                    try {
                        g gVar2 = (g) this.h.remove();
                        if (gVar2 != null) {
                            b(gVar2);
                        } else if (this.c.isDebugEnabled()) {
                            this.c.debug("No free connection to delete");
                        }
                        this.d.unlock();
                        c a3 = a(cVar, true);
                        a = a3;
                        gVar = a(a3, this.e);
                    } catch (Throwable th) {
                        this.d.unlock();
                        throw th;
                    }
                }
            } else {
                throw new IllegalStateException("Connection pool shut down");
            }
        }
        this.d.unlock();
        return gVar;
    }

    public final void a() {
        this.d.lock();
        try {
            if (!this.m) {
                this.m = true;
                Iterator it = this.g.iterator();
                while (it.hasNext()) {
                    it.remove();
                    a((g) it.next());
                }
                Iterator it2 = this.h.iterator();
                while (it2.hasNext()) {
                    g gVar = (g) it2.next();
                    it2.remove();
                    if (this.c.isDebugEnabled()) {
                        this.c.debug("Closing connection [" + gVar.d() + "][" + gVar.a() + "]");
                    }
                    a(gVar);
                }
                Iterator it3 = this.i.iterator();
                while (it3.hasNext()) {
                    it3.remove();
                    ((j) it3.next()).a();
                }
                this.j.clear();
                this.d.unlock();
            }
        } finally {
            this.d.unlock();
        }
    }

    public final void a(int i2) {
        this.d.lock();
        try {
            this.n = i2;
        } finally {
            this.d.unlock();
        }
    }

    public final void a(long j2, TimeUnit timeUnit) {
        if (timeUnit == null) {
            throw new IllegalArgumentException("Time unit must not be null.");
        }
        if (j2 < 0) {
            j2 = 0;
        }
        if (this.c.isDebugEnabled()) {
            this.c.debug("Closing connections idle longer than " + j2 + " " + timeUnit);
        }
        long currentTimeMillis = System.currentTimeMillis() - timeUnit.toMillis(j2);
        this.d.lock();
        try {
            Iterator it = this.h.iterator();
            while (it.hasNext()) {
                g gVar = (g) it.next();
                if (gVar.e() <= currentTimeMillis) {
                    if (this.c.isDebugEnabled()) {
                        this.c.debug("Closing connection last used @ " + new Date(gVar.e()));
                    }
                    it.remove();
                    b(gVar);
                }
            }
        } finally {
            this.d.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.h.c.c, boolean):com.agilebinary.a.a.a.c.b.a.c
     arg types: [com.agilebinary.a.a.a.h.c.c, int]
     candidates:
      com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.c.b.a.c, com.agilebinary.a.a.a.h.l):com.agilebinary.a.a.a.c.b.a.g
      com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.c.b.a.c, java.lang.Object):com.agilebinary.a.a.a.c.b.a.g
      com.agilebinary.a.a.a.c.b.a.a.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.a.c.b.a.i.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.h.c.c, boolean):com.agilebinary.a.a.a.c.b.a.c */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00f4 A[Catch:{ all -> 0x0148, all -> 0x0111 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.c.b.a.g r7, boolean r8, long r9, java.util.concurrent.TimeUnit r11) {
        /*
            r6 = this;
            com.agilebinary.a.a.a.h.c.c r1 = r7.d()
            org.apache.commons.logging.Log r0 = r6.c
            boolean r0 = r0.isDebugEnabled()
            if (r0 == 0) goto L_0x0038
            org.apache.commons.logging.Log r0 = r6.c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Releasing connection ["
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r3 = "]["
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.Object r3 = r7.a()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "]"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.debug(r2)
        L_0x0038:
            java.util.concurrent.locks.Lock r0 = r6.d
            r0.lock()
            boolean r0 = r6.m     // Catch:{ all -> 0x0111 }
            if (r0 == 0) goto L_0x004a
            r6.a(r7)     // Catch:{ all -> 0x0111 }
            java.util.concurrent.locks.Lock r0 = r6.d
            r0.unlock()
        L_0x0049:
            return
        L_0x004a:
            java.util.Set r0 = r6.g     // Catch:{ all -> 0x0111 }
            r0.remove(r7)     // Catch:{ all -> 0x0111 }
            r0 = 1
            com.agilebinary.a.a.a.c.b.a.c r2 = r6.a(r1, r0)     // Catch:{ all -> 0x0111 }
            if (r8 == 0) goto L_0x0107
            org.apache.commons.logging.Log r0 = r6.c     // Catch:{ all -> 0x0111 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0111 }
            if (r0 == 0) goto L_0x00ab
            r3 = 0
            int r0 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            if (r0 < 0) goto L_0x0103
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0111 }
            r0.<init>()     // Catch:{ all -> 0x0111 }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ all -> 0x0111 }
            java.lang.String r3 = " "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x0111 }
            java.lang.StringBuilder r0 = r0.append(r11)     // Catch:{ all -> 0x0111 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0111 }
        L_0x007b:
            org.apache.commons.logging.Log r3 = r6.c     // Catch:{ all -> 0x0111 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0111 }
            r4.<init>()     // Catch:{ all -> 0x0111 }
            java.lang.String r5 = "Pooling connection ["
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0111 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ all -> 0x0111 }
            java.lang.String r4 = "]["
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x0111 }
            java.lang.Object r4 = r7.a()     // Catch:{ all -> 0x0111 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x0111 }
            java.lang.String r4 = "]; keep alive for "
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x0111 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0111 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0111 }
            r3.debug(r0)     // Catch:{ all -> 0x0111 }
        L_0x00ab:
            r2.a(r7)     // Catch:{ all -> 0x0111 }
            r7.a(r9, r11)     // Catch:{ all -> 0x0111 }
            java.util.Queue r0 = r6.h     // Catch:{ all -> 0x0111 }
            r0.add(r7)     // Catch:{ all -> 0x0111 }
        L_0x00b6:
            r0 = 0
            java.util.concurrent.locks.Lock r1 = r6.d     // Catch:{ all -> 0x0111 }
            r1.lock()     // Catch:{ all -> 0x0111 }
            if (r2 == 0) goto L_0x0118
            boolean r1 = r2.f()     // Catch:{ all -> 0x0148 }
            if (r1 == 0) goto L_0x0118
            org.apache.commons.logging.Log r0 = r6.c     // Catch:{ all -> 0x0148 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0148 }
            if (r0 == 0) goto L_0x00ee
            org.apache.commons.logging.Log r0 = r6.c     // Catch:{ all -> 0x0148 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0148 }
            r1.<init>()     // Catch:{ all -> 0x0148 }
            java.lang.String r3 = "Notifying thread waiting on pool ["
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0148 }
            com.agilebinary.a.a.a.h.c.c r3 = r2.a()     // Catch:{ all -> 0x0148 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0148 }
            java.lang.String r3 = "]"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ all -> 0x0148 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0148 }
            r0.debug(r1)     // Catch:{ all -> 0x0148 }
        L_0x00ee:
            com.agilebinary.a.a.a.c.b.a.j r0 = r2.g()     // Catch:{ all -> 0x0148 }
        L_0x00f2:
            if (r0 == 0) goto L_0x00f7
            r0.a()     // Catch:{ all -> 0x0148 }
        L_0x00f7:
            java.util.concurrent.locks.Lock r0 = r6.d     // Catch:{ all -> 0x0111 }
            r0.unlock()     // Catch:{ all -> 0x0111 }
            java.util.concurrent.locks.Lock r0 = r6.d
            r0.unlock()
            goto L_0x0049
        L_0x0103:
            java.lang.String r0 = "ever"
            goto L_0x007b
        L_0x0107:
            r2.e()     // Catch:{ all -> 0x0111 }
            int r0 = r6.o     // Catch:{ all -> 0x0111 }
            int r0 = r0 + -1
            r6.o = r0     // Catch:{ all -> 0x0111 }
            goto L_0x00b6
        L_0x0111:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r6.d
            r1.unlock()
            throw r0
        L_0x0118:
            java.util.Queue r1 = r6.i     // Catch:{ all -> 0x0148 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0148 }
            if (r1 != 0) goto L_0x0138
            org.apache.commons.logging.Log r0 = r6.c     // Catch:{ all -> 0x0148 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0148 }
            if (r0 == 0) goto L_0x012f
            org.apache.commons.logging.Log r0 = r6.c     // Catch:{ all -> 0x0148 }
            java.lang.String r1 = "Notifying thread waiting on any pool"
            r0.debug(r1)     // Catch:{ all -> 0x0148 }
        L_0x012f:
            java.util.Queue r0 = r6.i     // Catch:{ all -> 0x0148 }
            java.lang.Object r0 = r0.remove()     // Catch:{ all -> 0x0148 }
            com.agilebinary.a.a.a.c.b.a.j r0 = (com.agilebinary.a.a.a.c.b.a.j) r0     // Catch:{ all -> 0x0148 }
            goto L_0x00f2
        L_0x0138:
            org.apache.commons.logging.Log r1 = r6.c     // Catch:{ all -> 0x0148 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x0148 }
            if (r1 == 0) goto L_0x00f2
            org.apache.commons.logging.Log r1 = r6.c     // Catch:{ all -> 0x0148 }
            java.lang.String r2 = "Notifying no-one, there are no waiting threads"
            r1.debug(r2)     // Catch:{ all -> 0x0148 }
            goto L_0x00f2
        L_0x0148:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r6.d     // Catch:{ all -> 0x0111 }
            r1.unlock()     // Catch:{ all -> 0x0111 }
            throw r0     // Catch:{ all -> 0x0111 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.a.a.a(com.agilebinary.a.a.a.c.b.a.g, boolean, long, java.util.concurrent.TimeUnit):void");
    }
}
