package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.h.c.c;
import java.util.concurrent.TimeUnit;

public class e {
    final /* synthetic */ f a;
    final /* synthetic */ c b;
    final /* synthetic */ Object c;
    final /* synthetic */ a d;

    private e() {
    }

    e(a aVar, f fVar, c cVar, Object obj) {
        this.d = aVar;
        this.a = fVar;
        this.b = cVar;
        this.c = obj;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.b.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.b.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.b
      com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean */
    public static boolean a(b bVar) {
        if (bVar != null) {
            return bVar.a("http.protocol.handle-authentication", true);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    public g a(long j, TimeUnit timeUnit) {
        return this.d.a(this.b, this.c, j, timeUnit, this.a);
    }

    public void a() {
        this.d.d.lock();
        try {
            this.a.a();
        } finally {
            this.d.d.unlock();
        }
    }
}
