package com.agilebinary.a.a.a.d;

import com.agilebinary.a.a.a.a.e;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.h.a;
import com.agilebinary.a.a.a.h.b.b;
import java.security.Principal;
import javax.net.ssl.SSLSession;

public class f {
    private static Principal a(e eVar) {
        b d;
        com.agilebinary.a.a.a.a.f c = eVar.c();
        if (c == null || !c.e() || !c.d() || (d = eVar.d()) == null) {
            return null;
        }
        return d.a();
    }

    public Object a(i iVar) {
        SSLSession f;
        Principal principal = null;
        e eVar = (e) iVar.a("http.auth.target-scope");
        if (eVar != null && (principal = a(eVar)) == null) {
            principal = a((e) iVar.a("http.auth.proxy-scope"));
        }
        if (principal == null) {
            a aVar = (a) iVar.a("http.connection");
            if (aVar.l() && (f = aVar.f()) != null) {
                return f.getLocalPrincipal();
            }
        }
        return principal;
    }
}
