package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.a.b;
import com.agilebinary.a.a.a.a.d;
import com.agilebinary.a.a.a.a.e;
import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.b.a.a;
import org.apache.commons.logging.Log;

public final class f implements ab {
    private final Log a = a.a(getClass());

    public final void a(com.agilebinary.a.a.a.f fVar, i iVar) {
        e eVar;
        com.agilebinary.a.a.a.a.f c;
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else if (!fVar.a("Proxy-Authorization") && (eVar = (e) iVar.a("http.auth.proxy-scope")) != null && (c = eVar.c()) != null) {
            if (eVar.d() == null) {
                this.a.debug("User credentials not available");
            } else if (eVar.e() != null || !c.d()) {
                try {
                    fVar.a(c instanceof d ? ((d) c).a() : c.f());
                } catch (b e) {
                    if (this.a.isErrorEnabled()) {
                        this.a.error("Proxy authentication error: " + e.getMessage());
                    }
                }
            }
        }
    }
}
