package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.h;
import com.agilebinary.a.a.a.k.j;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.t;
import java.util.Iterator;
import java.util.List;

public final class p implements j {
    private final String[] a;
    private final boolean b;
    private ae c;
    private ag d;
    private w e;
    private q f;

    public p() {
        this(null, false);
    }

    public p(String[] strArr, boolean z) {
        this.a = strArr == null ? null : (String[]) strArr.clone();
        this.b = z;
    }

    private ae c() {
        if (this.c == null) {
            this.c = new ae(this.a, this.b);
        }
        return this.c;
    }

    private ag d() {
        if (this.d == null) {
            this.d = new ag(this.a, this.b);
        }
        return this.d;
    }

    private w e() {
        if (this.e == null) {
            this.e = new w(this.a);
        }
        return this.e;
    }

    public final int a() {
        return c().a();
    }

    public final List a(t tVar, f fVar) {
        boolean z = false;
        if (tVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            m[] c2 = tVar.c();
            boolean z2 = false;
            for (m mVar : c2) {
                if (mVar.a("version") != null) {
                    z = true;
                }
                if (mVar.a("expires") != null) {
                    z2 = true;
                }
            }
            if (z) {
                return "Set-Cookie2".equals(tVar.a()) ? c().a(c2, fVar) : d().a(c2, fVar);
            }
            if (!z2) {
                return e().a(c2, fVar);
            }
            if (this.f == null) {
                this.f = new q(this.a);
            }
            return this.f.a(tVar, fVar);
        }
    }

    public final List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException("List of cookie may not be null");
        }
        Iterator it = list.iterator();
        int i = Integer.MAX_VALUE;
        boolean z = true;
        while (it.hasNext()) {
            c cVar = (c) it.next();
            if (!(cVar instanceof h)) {
                z = false;
            }
            i = cVar.g() < i ? cVar.g() : i;
        }
        return i > 0 ? z ? c().a(list) : d().a(list) : e().a(list);
    }

    public final void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (cVar.g() <= 0) {
            e().a(cVar, fVar);
        } else if (cVar instanceof h) {
            c().a(cVar, fVar);
        } else {
            d().a(cVar, fVar);
        }
    }

    public final t b() {
        return c().b();
    }

    public final boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar != null) {
            return cVar.g() > 0 ? cVar instanceof h ? c().b(cVar, fVar) : d().b(cVar, fVar) : e().b(cVar, fVar);
        } else {
            throw new IllegalArgumentException("Cookie origin may not be null");
        }
    }

    public final String toString() {
        return "best-match";
    }
}
