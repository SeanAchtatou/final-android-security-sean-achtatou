package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.a.b;
import com.agilebinary.a.a.a.a.d;
import com.agilebinary.a.a.a.a.e;
import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.i;
import org.apache.commons.logging.Log;

public final class a implements ab {
    private final Log a = com.agilebinary.a.a.b.a.a.a(getClass());

    public final void a(f fVar, i iVar) {
        e eVar;
        com.agilebinary.a.a.a.a.f c;
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else if (fVar.a().a().equalsIgnoreCase("CONNECT") || fVar.a("Authorization") || (eVar = (e) iVar.a("http.auth.target-scope")) == null || (c = eVar.c()) == null) {
        } else {
            if (eVar.d() == null) {
                this.a.debug("User credentials not available");
            } else if (eVar.e() != null || !c.d()) {
                try {
                    fVar.a(c instanceof d ? ((d) c).a() : c.f());
                } catch (b e) {
                    if (this.a.isErrorEnabled()) {
                        this.a.error("Authentication error: " + e.getMessage());
                    }
                }
            }
        }
    }
}
