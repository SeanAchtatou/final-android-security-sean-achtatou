package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.a.u;
import java.io.Serializable;

public final class e implements r, Serializable, Cloneable {
    private final String a;
    private final b b;
    private final int c;

    public e(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        }
        int c2 = bVar.c(58);
        if (c2 == -1) {
            throw new u("Invalid header: " + bVar.toString());
        }
        String b2 = bVar.b(0, c2);
        if (b2.length() == 0) {
            throw new u("Invalid header: " + bVar.toString());
        }
        this.b = bVar;
        this.a = b2;
        this.c = c2 + 1;
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b.b(this.c, this.b.c());
    }

    public final m[] c() {
        i iVar = new i(0, this.b.c());
        iVar.a(this.c);
        return f.a.a(this.b, iVar);
    }

    public final Object clone() {
        return super.clone();
    }

    public final int d() {
        return this.c;
    }

    public final b e() {
        return this.b;
    }

    public final String toString() {
        return this.b.toString();
    }
}
