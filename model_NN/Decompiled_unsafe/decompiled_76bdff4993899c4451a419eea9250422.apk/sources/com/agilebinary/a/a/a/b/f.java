package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.c.b.a.c;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import java.util.ArrayList;

public final class f implements j {
    public static final f a = new f();
    private static final char[] b = {';', ','};

    private static o a(String str, String str2) {
        return new o(str, str2);
    }

    private static boolean a(char c, char[] cArr) {
        if (cArr == null) {
            return false;
        }
        for (char c2 : cArr) {
            if (c == c2) {
                return true;
            }
        }
        return false;
    }

    public static final m[] a(String str, j jVar) {
        if (str == null) {
            throw new IllegalArgumentException("Value to parse may not be null");
        }
        f fVar = a;
        b bVar = new b(str.length());
        bVar.a(str);
        return fVar.a(bVar, new i(0, str.length()));
    }

    private o[] c(b bVar, i iVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            int a2 = iVar.a();
            while (b2 < a2 && c.a(bVar.a(b2))) {
                b2++;
            }
            iVar.a(b2);
            if (iVar.c()) {
                return new o[0];
            }
            ArrayList arrayList = new ArrayList();
            while (!iVar.c()) {
                arrayList.add(d(bVar, iVar));
                if (bVar.a(iVar.b() - 1) == ',') {
                    break;
                }
            }
            return (o[]) arrayList.toArray(new o[arrayList.size()]);
        }
    }

    private o d(b bVar, i iVar) {
        return a(bVar, iVar, b);
    }

    public final o a(b bVar, i iVar, char[] cArr) {
        boolean z;
        String str;
        boolean z2;
        boolean z3 = true;
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            int b3 = iVar.b();
            int a2 = iVar.a();
            while (true) {
                if (b2 < a2) {
                    char a3 = bVar.a(b2);
                    if (a3 == '=') {
                        break;
                    } else if (a(a3, cArr)) {
                        z = true;
                        break;
                    } else {
                        b2++;
                    }
                } else {
                    break;
                }
            }
            z = false;
            if (b2 == a2) {
                str = bVar.b(b3, a2);
                z = true;
            } else {
                String b4 = bVar.b(b3, b2);
                b2++;
                str = b4;
            }
            if (z) {
                iVar.a(b2);
                return a(str, (String) null);
            }
            boolean z4 = false;
            boolean z5 = false;
            int i = b2;
            while (true) {
                if (i < a2) {
                    char a4 = bVar.a(i);
                    if (a4 != '\"' || z4) {
                        z2 = z5;
                    } else {
                        z2 = !z5;
                    }
                    if (!z2 && !z4 && a(a4, cArr)) {
                        break;
                    }
                    i++;
                    z4 = !z4 && z2 && a4 == '\\';
                    z5 = z2;
                } else {
                    z3 = z;
                    break;
                }
            }
            int i2 = b2;
            while (i2 < i && c.a(bVar.a(i2))) {
                i2++;
            }
            int i3 = i;
            while (i3 > i2 && c.a(bVar.a(i3 - 1))) {
                i3--;
            }
            if (i3 - i2 >= 2 && bVar.a(i2) == '\"' && bVar.a(i3 - 1) == '\"') {
                i2++;
                i3--;
            }
            String a5 = bVar.a(i2, i3);
            iVar.a(z3 ? i + 1 : i);
            return a(str, a5);
        }
    }

    public final m[] a(b bVar, i iVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            ArrayList arrayList = new ArrayList();
            while (!iVar.c()) {
                m b2 = b(bVar, iVar);
                if (b2.a().length() != 0 || b2.b() != null) {
                    arrayList.add(b2);
                }
            }
            return (m[]) arrayList.toArray(new m[arrayList.size()]);
        }
    }

    public final m b(b bVar, i iVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            o d = d(bVar, iVar);
            o[] oVarArr = null;
            if (!iVar.c() && bVar.a(iVar.b() - 1) != ',') {
                oVarArr = c(bVar, iVar);
            }
            return new a(d.a(), d.b(), oVarArr);
        }
    }
}
