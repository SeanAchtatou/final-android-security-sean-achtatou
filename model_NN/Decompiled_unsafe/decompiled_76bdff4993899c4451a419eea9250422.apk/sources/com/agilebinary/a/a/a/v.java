package com.agilebinary.a.a.a;

import com.agilebinary.a.a.a.b.f;
import com.agilebinary.a.a.a.b.i;
import com.agilebinary.a.a.a.b.j;
import com.agilebinary.a.a.a.i.b;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class v implements Iterator {
    private final w a;
    private final j b;
    private m c;
    private b d;
    private i e;

    public v(w wVar) {
        this(wVar, f.a);
    }

    public v(w wVar, j jVar) {
        this.c = null;
        this.d = null;
        this.e = null;
        if (wVar == null) {
            throw new IllegalArgumentException("Header iterator may not be null");
        } else if (jVar == null) {
            throw new IllegalArgumentException("Parser may not be null");
        } else {
            this.a = wVar;
            this.b = jVar;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x005d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b() {
        /*
            r5 = this;
            r4 = 0
            r3 = 0
        L_0x0002:
            com.agilebinary.a.a.a.w r0 = r5.a
            boolean r0 = r0.hasNext()
            if (r0 != 0) goto L_0x000e
            com.agilebinary.a.a.a.b.i r0 = r5.e
            if (r0 == 0) goto L_0x0079
        L_0x000e:
            com.agilebinary.a.a.a.b.i r0 = r5.e
            if (r0 == 0) goto L_0x001a
            com.agilebinary.a.a.a.b.i r0 = r5.e
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x0051
        L_0x001a:
            r5.e = r3
            r5.d = r3
        L_0x001e:
            com.agilebinary.a.a.a.w r0 = r5.a
            boolean r0 = r0.hasNext()
            if (r0 == 0) goto L_0x0051
            com.agilebinary.a.a.a.w r0 = r5.a
            com.agilebinary.a.a.a.t r1 = r0.a()
            boolean r0 = r1 instanceof com.agilebinary.a.a.a.r
            if (r0 == 0) goto L_0x007a
            r0 = r1
            com.agilebinary.a.a.a.r r0 = (com.agilebinary.a.a.a.r) r0
            com.agilebinary.a.a.a.i.b r0 = r0.e()
            r5.d = r0
            com.agilebinary.a.a.a.b.i r0 = new com.agilebinary.a.a.a.b.i
            com.agilebinary.a.a.a.i.b r2 = r5.d
            int r2 = r2.c()
            r0.<init>(r4, r2)
            r5.e = r0
            com.agilebinary.a.a.a.b.i r0 = r5.e
            com.agilebinary.a.a.a.r r1 = (com.agilebinary.a.a.a.r) r1
            int r1 = r1.d()
            r0.a(r1)
        L_0x0051:
            com.agilebinary.a.a.a.b.i r0 = r5.e
            if (r0 == 0) goto L_0x0002
        L_0x0055:
            com.agilebinary.a.a.a.b.i r0 = r5.e
            boolean r0 = r0.c()
            if (r0 != 0) goto L_0x009e
            com.agilebinary.a.a.a.b.j r0 = r5.b
            com.agilebinary.a.a.a.i.b r1 = r5.d
            com.agilebinary.a.a.a.b.i r2 = r5.e
            com.agilebinary.a.a.a.m r0 = r0.b(r1, r2)
            java.lang.String r1 = r0.a()
            int r1 = r1.length()
            if (r1 != 0) goto L_0x0077
            java.lang.String r1 = r0.b()
            if (r1 == 0) goto L_0x0055
        L_0x0077:
            r5.c = r0
        L_0x0079:
            return
        L_0x007a:
            java.lang.String r0 = r1.b()
            if (r0 == 0) goto L_0x001e
            com.agilebinary.a.a.a.i.b r1 = new com.agilebinary.a.a.a.i.b
            int r2 = r0.length()
            r1.<init>(r2)
            r5.d = r1
            com.agilebinary.a.a.a.i.b r1 = r5.d
            r1.a(r0)
            com.agilebinary.a.a.a.b.i r0 = new com.agilebinary.a.a.a.b.i
            com.agilebinary.a.a.a.i.b r1 = r5.d
            int r1 = r1.c()
            r0.<init>(r4, r1)
            r5.e = r0
            goto L_0x0051
        L_0x009e:
            com.agilebinary.a.a.a.b.i r0 = r5.e
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x0002
            r5.e = r3
            r5.d = r3
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.v.b():void");
    }

    public m a() {
        if (this.c == null) {
            b();
        }
        if (this.c == null) {
            throw new NoSuchElementException("No more header elements available");
        }
        m mVar = this.c;
        this.c = null;
        return mVar;
    }

    public boolean hasNext() {
        if (this.c == null) {
            b();
        }
        return this.c != null;
    }

    public Object next() {
        return a();
    }

    public void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
