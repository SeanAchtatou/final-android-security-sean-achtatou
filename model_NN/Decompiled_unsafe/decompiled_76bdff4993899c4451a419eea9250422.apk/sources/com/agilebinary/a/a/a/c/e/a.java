package com.agilebinary.a.a.a.c.e;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.c.c.e;
import com.agilebinary.a.a.a.c.c.l;
import com.agilebinary.a.a.a.c.c.n;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.x;

public class a {
    private final com.agilebinary.a.a.a.g.a a;

    private a() {
    }

    public a(com.agilebinary.a.a.a.g.a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Content length strategy may not be null");
        }
        this.a = aVar;
    }

    public static int a(b bVar) {
        if (bVar != null) {
            return bVar.a("http.socket.timeout", 0);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.b.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.b.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.b
      com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean */
    public static boolean b(b bVar) {
        if (bVar != null) {
            return bVar.a("http.socket.reuseaddr", false);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    public static int c(b bVar) {
        if (bVar != null) {
            return bVar.a("http.connection.timeout", 0);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    public final c a(com.agilebinary.a.a.a.j.a aVar, x xVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (xVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        } else {
            com.agilebinary.a.a.a.g.c cVar = new com.agilebinary.a.a.a.g.c();
            long a2 = this.a.a(xVar);
            if (a2 == -2) {
                cVar.a(true);
                cVar.a(-1);
                cVar.a(new e(aVar));
            } else if (a2 == -1) {
                cVar.a(false);
                cVar.a(-1);
                cVar.a(new n(aVar));
            } else {
                cVar.a(false);
                cVar.a(a2);
                cVar.a(new l(aVar, a2));
            }
            t c = xVar.c("Content-Type");
            if (c != null) {
                cVar.a(c);
            }
            t c2 = xVar.c("Content-Encoding");
            if (c2 != null) {
                cVar.b(c2);
            }
            return cVar;
        }
    }
}
