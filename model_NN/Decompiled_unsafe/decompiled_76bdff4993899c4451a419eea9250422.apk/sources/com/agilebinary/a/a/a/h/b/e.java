package com.agilebinary.a.a.a.h.b;

import com.agilebinary.a.a.a.c.e.a;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.h.h;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

public final class e implements a, i {
    private static final e a = new e();
    private final b b = null;

    public static e b() {
        return a;
    }

    public final Socket a() {
        return new Socket();
    }

    public final Socket a(Socket socket, String str, int i, InetAddress inetAddress, int i2, b bVar) {
        InetSocketAddress inetSocketAddress = null;
        if (inetAddress != null || i2 > 0) {
            if (i2 < 0) {
                i2 = 0;
            }
            inetSocketAddress = new InetSocketAddress(inetAddress, i2);
        }
        return a(socket, new InetSocketAddress(InetAddress.getByName(str), i), inetSocketAddress, bVar);
    }

    public final Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, b bVar) {
        if (inetSocketAddress == null) {
            throw new IllegalArgumentException("Remote address may not be null");
        } else if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            if (socket == null) {
                socket = new Socket();
            }
            if (inetSocketAddress2 != null) {
                socket.setReuseAddress(a.b(bVar));
                socket.bind(inetSocketAddress2);
            }
            try {
                socket.connect(inetSocketAddress, a.c(bVar));
                return socket;
            } catch (SocketTimeoutException e) {
                throw new h("Connect to " + inetSocketAddress.getHostName() + "/" + inetSocketAddress.getAddress() + " timed out");
            }
        }
    }

    public final boolean a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null.");
        } else if (!socket.isClosed()) {
            return false;
        } else {
            throw new IllegalArgumentException("Socket is closed.");
        }
    }

    public final Socket e_() {
        return new Socket();
    }
}
