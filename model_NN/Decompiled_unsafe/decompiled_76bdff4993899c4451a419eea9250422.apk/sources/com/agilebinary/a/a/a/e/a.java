package com.agilebinary.a.a.a.e;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class a extends b implements Serializable, Cloneable {
    private final HashMap a = new HashMap();

    public b a(String str, Object obj) {
        this.a.put(str, obj);
        return this;
    }

    public Object a(String str) {
        return this.a.get(str);
    }

    public Object clone() {
        a aVar = (a) super.clone();
        for (Map.Entry entry : this.a.entrySet()) {
            if (entry.getKey() instanceof String) {
                aVar.a((String) entry.getKey(), entry.getValue());
            }
        }
        return aVar;
    }
}
