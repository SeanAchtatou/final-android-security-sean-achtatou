package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.b.r;
import com.agilebinary.a.a.a.g;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.k;
import com.agilebinary.a.a.a.y;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public final class e extends InputStream {
    private final a a;
    private final b b;
    private int c;
    private int d;
    private int e;
    private boolean f = false;
    private boolean g = false;

    public e(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        }
        this.a = aVar;
        this.e = 0;
        this.b = new b(16);
        this.c = 1;
    }

    private void a() {
        this.d = b();
        if (this.d < 0) {
            throw new g("Negative chunk size");
        }
        this.c = 2;
        this.e = 0;
        if (this.d == 0) {
            this.f = true;
            try {
                f.a(this.a, -1, -1, r.a, new ArrayList());
            } catch (k e2) {
                g gVar = new g("Invalid footer: " + e2.getMessage());
                com.agilebinary.a.a.a.i.a.a(gVar, e2);
                throw gVar;
            }
        }
    }

    private int b() {
        switch (this.c) {
            case 1:
                break;
            case 2:
            default:
                throw new IllegalStateException("Inconsistent codec state");
            case 3:
                this.b.a();
                if (this.a.a(this.b) != -1) {
                    if (this.b.d()) {
                        this.c = 1;
                        break;
                    } else {
                        throw new g("Unexpected content at the end of chunk");
                    }
                } else {
                    return 0;
                }
        }
        this.b.a();
        if (this.a.a(this.b) == -1) {
            return 0;
        }
        int c2 = this.b.c(59);
        if (c2 < 0) {
            c2 = this.b.c();
        }
        try {
            return Integer.parseInt(this.b.b(0, c2), 16);
        } catch (NumberFormatException e2) {
            throw new g("Bad chunk header");
        }
    }

    public final int available() {
        if (this.a instanceof com.agilebinary.a.a.a.j.g) {
            return Math.min(((com.agilebinary.a.a.a.j.g) this.a).d_(), this.d - this.e);
        }
        return 0;
    }

    public final void close() {
        if (!this.g) {
            try {
                if (!this.f) {
                    do {
                    } while (read(new byte[2048]) >= 0);
                }
            } finally {
                this.f = true;
                this.g = true;
            }
        }
    }

    public final int read() {
        if (this.g) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.f) {
            return -1;
        } else {
            if (this.c != 2) {
                a();
                if (this.f) {
                    return -1;
                }
            }
            int d2 = this.a.d();
            if (d2 != -1) {
                this.e++;
                if (this.e >= this.d) {
                    this.c = 3;
                }
            }
            return d2;
        }
    }

    public final int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (this.g) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.f) {
            return -1;
        } else {
            if (this.c != 2) {
                a();
                if (this.f) {
                    return -1;
                }
            }
            int a2 = this.a.a(bArr, i, Math.min(i2, this.d - this.e));
            if (a2 != -1) {
                this.e += a2;
                if (this.e >= this.d) {
                    this.c = 3;
                }
                return a2;
            }
            this.f = true;
            throw new y("Truncated chunk ( expected size: " + this.d + "; actual size: " + this.e + ")");
        }
    }
}
