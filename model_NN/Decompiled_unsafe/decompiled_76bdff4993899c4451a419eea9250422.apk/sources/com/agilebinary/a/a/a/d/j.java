package com.agilebinary.a.a.a.d;

import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.p;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import javax.net.ssl.SSLException;

public class j {
    private final int a;
    private final boolean b;

    public j() {
        this(3, false);
    }

    public j(int i, boolean z) {
        this.a = 3;
        this.b = false;
    }

    public boolean a(IOException iOException, int i, i iVar) {
        if (iOException == null) {
            throw new IllegalArgumentException("Exception parameter may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else if (i > this.a) {
            return false;
        } else {
            if (iOException instanceof InterruptedIOException) {
                return false;
            }
            if (iOException instanceof UnknownHostException) {
                return false;
            }
            if (iOException instanceof ConnectException) {
                return false;
            }
            if (iOException instanceof SSLException) {
                return false;
            }
            if (!(((f) iVar.a("http.request")) instanceof p)) {
                return true;
            }
            Boolean bool = (Boolean) iVar.a("http.request_sent");
            return !(bool != null && bool.booleanValue()) || this.b;
        }
    }
}
