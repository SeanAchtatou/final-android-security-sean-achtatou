package com.agilebinary.a.a.a;

import com.agilebinary.a.a.a.c.e.c;
import java.io.Serializable;
import java.util.Locale;

public final class b implements Serializable, Cloneable {
    private String a;
    private String b;
    private int c;
    private String d;

    public b(String str, int i) {
        this(str, i, null);
    }

    public b(String str, int i, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Host name may not be null");
        }
        this.a = str;
        this.b = str.toLowerCase(Locale.ENGLISH);
        if (str2 != null) {
            this.d = str2.toLowerCase(Locale.ENGLISH);
        } else {
            this.d = "http";
        }
        this.c = i;
    }

    public final String a() {
        return this.a;
    }

    public final int b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    public final Object clone() {
        return super.clone();
    }

    public final String d() {
        if (this.c == -1) {
            return this.a;
        }
        com.agilebinary.a.a.a.i.b bVar = new com.agilebinary.a.a.a.i.b(this.a.length() + 6);
        bVar.a(this.a);
        bVar.a(":");
        bVar.a(Integer.toString(this.c));
        return bVar.toString();
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return this.b.equals(bVar.b) && this.c == bVar.c && this.d.equals(bVar.d);
    }

    public final int hashCode() {
        return c.a((c.a(17, this.b) * 37) + this.c, this.d);
    }

    public final String toString() {
        com.agilebinary.a.a.a.i.b bVar = new com.agilebinary.a.a.a.i.b(32);
        bVar.a(this.d);
        bVar.a("://");
        bVar.a(this.a);
        if (this.c != -1) {
            bVar.a(':');
            bVar.a(Integer.toString(this.c));
        }
        return bVar.toString();
    }
}
