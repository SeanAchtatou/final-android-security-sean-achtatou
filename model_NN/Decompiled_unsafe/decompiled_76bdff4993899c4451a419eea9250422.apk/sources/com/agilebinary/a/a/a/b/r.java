package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.c.b.a.c;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.u;

public final class r implements s {
    public static final r a = new r();
    private a b;

    public r() {
        this(null);
    }

    private r(a aVar) {
        this.b = aa.d;
    }

    private a c(b bVar, i iVar) {
        boolean z = true;
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            String a2 = this.b.a();
            int length = a2.length();
            int b2 = iVar.b();
            int a3 = iVar.a();
            d(bVar, iVar);
            int b3 = iVar.b();
            if (b3 + length + 4 > a3) {
                throw new u("Not a valid protocol version: " + bVar.a(b2, a3));
            }
            int i = 0;
            boolean z2 = true;
            while (z2 && i < length) {
                z2 = bVar.a(b3 + i) == a2.charAt(i);
                i++;
            }
            if (!z2) {
                z = z2;
            } else if (bVar.a(b3 + length) != '/') {
                z = false;
            }
            if (!z) {
                throw new u("Not a valid protocol version: " + bVar.a(b2, a3));
            }
            int i2 = length + 1 + b3;
            int a4 = bVar.a(46, i2, a3);
            if (a4 == -1) {
                throw new u("Invalid protocol version number: " + bVar.a(b2, a3));
            }
            try {
                int parseInt = Integer.parseInt(bVar.b(i2, a4));
                int i3 = a4 + 1;
                int a5 = bVar.a(32, i3, a3);
                if (a5 == -1) {
                    a5 = a3;
                }
                try {
                    int parseInt2 = Integer.parseInt(bVar.b(i3, a5));
                    iVar.a(a5);
                    return this.b.a(parseInt, parseInt2);
                } catch (NumberFormatException e) {
                    throw new u("Invalid protocol minor version number: " + bVar.a(b2, a3));
                }
            } catch (NumberFormatException e2) {
                throw new u("Invalid protocol major version number: " + bVar.a(b2, a3));
            }
        }
    }

    private static void d(b bVar, i iVar) {
        int b2 = iVar.b();
        int a2 = iVar.a();
        while (b2 < a2 && c.a(bVar.a(b2))) {
            b2++;
        }
        iVar.a(b2);
    }

    public final t a(b bVar) {
        return new e(bVar);
    }

    public final boolean a(b bVar, i iVar) {
        boolean z = true;
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            String a2 = this.b.a();
            int length = a2.length();
            if (bVar.c() < length + 4) {
                return false;
            }
            if (b2 < 0) {
                b2 = (bVar.c() - 4) - length;
            } else if (b2 == 0) {
                while (b2 < bVar.c() && c.a(bVar.a(b2))) {
                    b2++;
                }
            }
            if (b2 + length + 4 > bVar.c()) {
                return false;
            }
            int i = 0;
            boolean z2 = true;
            while (z2 && i < length) {
                z2 = bVar.a(b2 + i) == a2.charAt(i);
                i++;
            }
            if (!z2) {
                z = z2;
            } else if (bVar.a(b2 + length) != '/') {
                z = false;
            }
            return z;
        }
    }

    public final h b(b bVar, i iVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            int a2 = iVar.a();
            try {
                a c = c(bVar, iVar);
                d(bVar, iVar);
                int b3 = iVar.b();
                int a3 = bVar.a(32, b3, a2);
                int i = a3 < 0 ? a2 : a3;
                String b4 = bVar.b(b3, i);
                for (int i2 = 0; i2 < b4.length(); i2++) {
                    if (!Character.isDigit(b4.charAt(i2))) {
                        throw new u("Status line contains invalid status code: " + bVar.a(b2, a2));
                    }
                }
                return new m(c, Integer.parseInt(b4), i < a2 ? bVar.b(i, a2) : "");
            } catch (NumberFormatException e) {
                throw new u("Status line contains invalid status code: " + bVar.a(b2, a2));
            } catch (IndexOutOfBoundsException e2) {
                throw new u("Invalid status line: " + bVar.a(b2, a2));
            }
        }
    }
}
