package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.j;

public abstract class l extends k {
    protected volatile a a;

    protected l(j jVar, a aVar) {
        super(jVar, aVar.a);
        this.a = aVar;
    }

    private void a(a aVar) {
        if (o() || aVar == null) {
            throw new m();
        }
    }

    public final void a(i iVar, b bVar) {
        a i = i();
        a(i);
        i.a(iVar, bVar);
    }

    public final void a(c cVar, i iVar, b bVar) {
        a i = i();
        a(i);
        i.a(cVar, iVar, bVar);
    }

    public final void a(Object obj) {
        a i = i();
        a(i);
        i.a(obj);
    }

    public final void a(boolean z, b bVar) {
        a i = i();
        a(i);
        i.a(z, bVar);
    }

    public final c a_() {
        a i = i();
        a(i);
        if (i.c == null) {
            return null;
        }
        return i.c.h();
    }

    /* access modifiers changed from: protected */
    public a i() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public synchronized void j() {
        this.a = null;
        super.j();
    }

    public final void k() {
        a i = i();
        if (i != null) {
            i.b();
        }
        g n = n();
        if (n != null) {
            n.k();
        }
    }

    public final void m() {
        a i = i();
        if (i != null) {
            i.b();
        }
        g n = n();
        if (n != null) {
            n.m();
        }
    }
}
