package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.a;
import com.agilebinary.a.a.a.h.b;
import com.agilebinary.a.a.a.h.c.c;
import java.util.concurrent.TimeUnit;

final class h implements b {
    private /* synthetic */ e a;
    private /* synthetic */ c b;
    private /* synthetic */ b c;

    h(b bVar, e eVar, c cVar) {
        this.c = bVar;
        this.a = eVar;
        this.b = cVar;
    }

    public final a a(long j, TimeUnit timeUnit) {
        if (this.b == null) {
            throw new IllegalArgumentException("Route may not be null.");
        }
        if (this.c.a.isDebugEnabled()) {
            this.c.a.debug("Get connection: " + this.b + ", timeout = " + j);
        }
        return new d(this.c, this.a.a(j, timeUnit));
    }

    public final void a() {
        this.a.a();
    }
}
