package com.agilebinary.a.a.a.g;

import java.io.InputStream;
import java.io.OutputStream;

public final class c extends f {
    private InputStream a;
    private long b = -1;

    public final void a(long j) {
        this.b = j;
    }

    public final void a(InputStream inputStream) {
        this.a = inputStream;
    }

    public final void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        InputStream f = f();
        try {
            byte[] bArr = new byte[2048];
            while (true) {
                int read = f.read(bArr);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        } finally {
            f.close();
        }
    }

    public final boolean a() {
        return false;
    }

    public final long c() {
        return this.b;
    }

    public final InputStream f() {
        if (this.a != null) {
            return this.a;
        }
        throw new IllegalStateException("Content has not been provided");
    }

    public final boolean g() {
        return this.a != null;
    }
}
