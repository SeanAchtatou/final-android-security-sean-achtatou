package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.c.e.c;
import com.agilebinary.a.a.a.i.b;
import java.io.Serializable;

public final class o implements com.agilebinary.a.a.a.o, Serializable, Cloneable {
    private final String a;
    private final String b;

    public o(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.a = str;
        this.b = str2;
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof com.agilebinary.a.a.a.o)) {
            return false;
        }
        o oVar = (o) obj;
        return this.a.equals(oVar.a) && c.a(this.b, oVar.b);
    }

    public final int hashCode() {
        return c.a(c.a(17, this.a), this.b);
    }

    public final String toString() {
        if (this.b == null) {
            return this.a;
        }
        b bVar = new b(this.a.length() + 1 + this.b.length());
        bVar.a(this.a);
        bVar.a("=");
        bVar.a(this.b);
        return bVar.toString();
    }
}
