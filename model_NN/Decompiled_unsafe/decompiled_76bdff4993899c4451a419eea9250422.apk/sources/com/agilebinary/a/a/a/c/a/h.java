package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.f;

public final class h extends x {
    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        }
        bVar.a(true);
    }

    public final boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar != null) {
            return !cVar.e() || fVar.d();
        } else {
            throw new IllegalArgumentException("Cookie origin may not be null");
        }
    }
}
