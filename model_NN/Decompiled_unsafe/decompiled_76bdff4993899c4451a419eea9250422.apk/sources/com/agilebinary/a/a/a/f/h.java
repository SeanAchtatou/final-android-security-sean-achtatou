package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.j;
import java.util.ArrayList;
import java.util.List;

public final class h implements ab, Cloneable {
    private List a = new ArrayList();
    private List b = new ArrayList();

    public final int a() {
        return this.a.size();
    }

    public final ab a(int i) {
        if (i < 0 || i >= this.a.size()) {
            return null;
        }
        return (ab) this.a.get(i);
    }

    public final void a(ab abVar) {
        if (abVar != null) {
            this.a.add(abVar);
        }
    }

    public final void a(ad adVar) {
        if (adVar != null) {
            this.b.add(adVar);
        }
    }

    public final void a(f fVar, i iVar) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.a.size()) {
                ((ab) this.a.get(i2)).a(fVar, iVar);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public final void a(j jVar, i iVar) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.b.size()) {
                ((ad) this.b.get(i2)).a(jVar, iVar);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public final int b() {
        return this.b.size();
    }

    public final ad b(int i) {
        if (i < 0 || i >= this.b.size()) {
            return null;
        }
        return (ad) this.b.get(i);
    }

    public final Object clone() {
        h hVar = (h) super.clone();
        hVar.a.clear();
        hVar.a.addAll(this.a);
        hVar.b.clear();
        hVar.b.addAll(this.b);
        return hVar;
    }
}
