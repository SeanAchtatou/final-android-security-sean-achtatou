package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.f;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.h;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.l;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.m;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.r;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.t;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.u;
import com.agilebinary.mobilemonitor.device.a.c.g;
import com.agilebinary.mobilemonitor.device.a.f.a;
import com.agilebinary.mobilemonitor.device.a.f.b;

public class j {
    private i a;
    private c b;

    public j() {
    }

    public j(i iVar, c cVar) {
        this.a = iVar;
        this.b = cVar;
    }

    public static String a(u[] uVarArr, g gVar) {
        String str;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < uVarArr.length; i++) {
            u uVar = uVarArr[i];
            if (uVar instanceof r) {
                str = b.a("LOCATIONSMS.CELL_BROADCASTNAME_LINE", ((r) uVar).a());
            } else if (uVar instanceof h) {
                h hVar = (h) uVar;
                str = b.a("LOCATIONSMS.CELL_CDMA_LINE", new String[]{new Integer(hVar.a()).toString(), new Integer(hVar.c()).toString(), new Integer(hVar.d()).toString(), new a(hVar.e(), 4).toString(), new a(hVar.f(), 4).toString()});
            } else if (uVar instanceof m) {
                m mVar = (m) uVar;
                str = b.a("LOCATIONSMS.CELL_GSM_LINE", new String[]{new Integer(mVar.a()).toString(), new Integer(mVar.c()).toString(), new Integer(mVar.d()).toString(), new Integer(mVar.e()).toString()});
            } else if (uVar instanceof com.agilebinary.mobilemonitor.device.a.b.a.a.a.a) {
                str = b.a("LOCATIONSMS.CELL_IDEN_LINE", new String[]{new Integer(((com.agilebinary.mobilemonitor.device.a.b.a.a.a.a) uVar).a()).toString()});
            } else if (uVar instanceof l) {
                l lVar = (l) uVar;
                if (!lVar.h()) {
                    str = b.a("LOCATIONSMS.COMBINED_LINE_NOFIX");
                } else {
                    str = b.a("LOCATIONSMS.COMBINED_LINE", new String[]{new a(lVar.d(), 4).toString(), new a(lVar.e(), 4).toString(), new a(lVar.f(), 0).toString(), gVar.a(lVar.g())});
                }
            } else if (uVar instanceof f) {
                f fVar = (f) uVar;
                if (!fVar.h()) {
                    str = b.a("LOCATIONSMS.GPS_LINE_NOFIX");
                } else {
                    str = b.a("LOCATIONSMS.GPS_LINE", new String[]{new a(fVar.d(), 4).toString(), new a(fVar.e(), 4).toString(), new a(fVar.a(), 0).toString(), new a(fVar.f(), 0).toString(), new a(fVar.c(), 0).toString(), gVar.a(fVar.g())});
                }
            } else if (uVar instanceof t) {
                t tVar = (t) uVar;
                if (!tVar.h()) {
                    str = b.a("LOCATIONSMS.TOWERTRIANGULATION_LINE_NOFIX");
                } else {
                    str = b.a("LOCATIONSMS.TOWERTRIANGULATION_LINE", new String[]{new a(tVar.d(), 4).toString(), new a(tVar.e(), 4).toString(), new a(tVar.f(), 0).toString(), gVar.a(tVar.g())});
                }
            } else {
                uVar.getClass().getName();
                str = null;
            }
            if (str != null) {
                stringBuffer.append(str);
                if (i < uVarArr.length - 1) {
                    stringBuffer.append("\n");
                }
            }
        }
        return stringBuffer.toString();
    }

    public final i a() {
        return this.a;
    }

    public final c b() {
        return this.b;
    }
}
