package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.e;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.w;
import java.util.NoSuchElementException;

public final class d implements e {
    private w a;
    private String b;
    private String c;
    private int d;

    public d(w wVar) {
        if (wVar == null) {
            throw new IllegalArgumentException("Header iterator must not be null.");
        }
        this.a = wVar;
        this.d = a(-1);
    }

    private int a(int i) {
        int i2 = 0;
        if (i < 0) {
            if (!this.a.hasNext()) {
                return -1;
            }
            this.b = this.a.a().b();
        } else if (i < 0) {
            throw new IllegalArgumentException("Search position must not be negative: " + i);
        } else {
            int length = this.b.length();
            boolean z = false;
            i2 = i;
            while (!z && i2 < length) {
                char charAt = this.b.charAt(i2);
                if (a(charAt)) {
                    z = true;
                } else if (b(charAt)) {
                    i2++;
                } else if (c(charAt)) {
                    throw new u("Tokens without separator (pos " + i2 + "): " + this.b);
                } else {
                    throw new u("Invalid character after token (pos " + i2 + "): " + this.b);
                }
            }
        }
        int b2 = b(i2);
        if (b2 < 0) {
            this.c = null;
            return -1;
        }
        int c2 = c(b2);
        this.c = this.b.substring(b2, c2);
        return c2;
    }

    private static boolean a(char c2) {
        return c2 == ',';
    }

    private int b(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Search position must not be negative: " + i);
        }
        boolean z = false;
        int i2 = i;
        while (!z && this.b != null) {
            int length = this.b.length();
            boolean z2 = z;
            int i3 = i2;
            boolean z3 = z2;
            while (!z3 && i3 < length) {
                char charAt = this.b.charAt(i3);
                if (a(charAt) || b(charAt)) {
                    i3++;
                } else if (c(this.b.charAt(i3))) {
                    z3 = true;
                } else {
                    throw new u("Invalid character before token (pos " + i3 + "): " + this.b);
                }
            }
            if (!z3) {
                if (this.a.hasNext()) {
                    this.b = this.a.a().b();
                    z = z3;
                    i2 = 0;
                } else {
                    this.b = null;
                }
            }
            boolean z4 = z3;
            i2 = i3;
            z = z4;
        }
        if (z) {
            return i2;
        }
        return -1;
    }

    private static boolean b(char c2) {
        return c2 == 9 || Character.isSpaceChar(c2);
    }

    private int c(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Token start position must not be negative: " + i);
        }
        int length = this.b.length();
        int i2 = i + 1;
        while (i2 < length && c(this.b.charAt(i2))) {
            i2++;
        }
        return i2;
    }

    private boolean c(char c2) {
        if (Character.isLetterOrDigit(c2)) {
            return true;
        }
        if (Character.isISOControl(c2)) {
            return false;
        }
        return !(" ,;=()<>@:\\\"/[]?{}\t".indexOf(c2) >= 0);
    }

    public final String a() {
        if (this.c == null) {
            throw new NoSuchElementException("Iteration already finished.");
        }
        String str = this.c;
        this.d = a(this.d);
        return str;
    }

    public final boolean hasNext() {
        return this.c != null;
    }

    public final Object next() {
        return a();
    }

    public final void remove() {
        throw new UnsupportedOperationException("Removing tokens is not supported.");
    }
}
