package com.agilebinary.a.a.a.h.a;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.h.c.c;
import java.net.InetAddress;

public final class a {
    private static b a = new b("127.0.0.255", 0, "no-host");
    private static c b = new c(a);

    private a() {
    }

    public static b a(com.agilebinary.a.a.a.e.b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        }
        b bVar2 = (b) bVar.a("http.route.default-proxy");
        if (bVar2 == null || !a.equals(bVar2)) {
            return bVar2;
        }
        return null;
    }

    public static c b(com.agilebinary.a.a.a.e.b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        }
        c cVar = (c) bVar.a("http.route.forced-route");
        if (cVar == null || !b.equals(cVar)) {
            return cVar;
        }
        return null;
    }

    public static InetAddress c(com.agilebinary.a.a.a.e.b bVar) {
        if (bVar != null) {
            return (InetAddress) bVar.a("http.route.local-address");
        }
        throw new IllegalArgumentException("Parameters must not be null.");
    }
}
