package com.agilebinary.a.a.a.i;

import java.lang.reflect.Method;

public final class a {
    private static final Method a = a();

    private a() {
    }

    private static Method a() {
        try {
            return Throwable.class.getMethod("initCause", Throwable.class);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    public static void a(Throwable th, Throwable th2) {
        if (a != null) {
            try {
                a.invoke(th, th2);
            } catch (Exception e) {
            }
        }
    }
}
