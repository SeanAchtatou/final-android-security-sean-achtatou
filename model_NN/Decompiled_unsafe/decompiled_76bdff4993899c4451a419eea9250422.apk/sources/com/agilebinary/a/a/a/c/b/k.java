package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.h.a;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.j;
import com.agilebinary.a.a.a.p;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

public abstract class k implements i, a {
    private volatile j a;
    private volatile g b;
    private volatile boolean c = false;
    private volatile boolean d = false;
    private volatile long e = Long.MAX_VALUE;

    protected k(j jVar, g gVar) {
        this.a = jVar;
        this.b = gVar;
    }

    private void a(g gVar) {
        if (this.d || gVar == null) {
            throw new m();
        }
    }

    public final synchronized Object a(String str) {
        g gVar;
        gVar = this.b;
        a(gVar);
        return gVar instanceof i ? ((i) gVar).a(str) : null;
    }

    public final void a(long j, TimeUnit timeUnit) {
        if (j > 0) {
            this.e = timeUnit.toMillis(j);
        } else {
            this.e = -1;
        }
    }

    public final void a(f fVar) {
        g gVar = this.b;
        a(gVar);
        this.c = false;
        gVar.a(fVar);
    }

    public final void a(com.agilebinary.a.a.a.j jVar) {
        g gVar = this.b;
        a(gVar);
        this.c = false;
        gVar.a(jVar);
    }

    public final void a(p pVar) {
        g gVar = this.b;
        a(gVar);
        this.c = false;
        gVar.a(pVar);
    }

    public final synchronized void a(String str, Object obj) {
        g gVar = this.b;
        a(gVar);
        if (gVar instanceof i) {
            ((i) gVar).a(str, obj);
        }
    }

    public final boolean a() {
        g gVar = this.b;
        a(gVar);
        return gVar.h_();
    }

    public final boolean a(int i) {
        g gVar = this.b;
        a(gVar);
        return gVar.a(i);
    }

    public final synchronized void b() {
        if (!this.d) {
            this.d = true;
            this.c = false;
            try {
                m();
            } catch (IOException e2) {
            }
            if (this.a != null) {
                this.a.a(this, this.e, TimeUnit.MILLISECONDS);
            }
        }
    }

    public final void b(int i) {
        g gVar = this.b;
        a(gVar);
        gVar.b(i);
    }

    public final synchronized void b_() {
        if (!this.d) {
            this.d = true;
            if (this.a != null) {
                this.a.a(this, this.e, TimeUnit.MILLISECONDS);
            }
        }
    }

    public final void c() {
        g gVar = this.b;
        a(gVar);
        gVar.c();
    }

    public final com.agilebinary.a.a.a.j d() {
        g gVar = this.b;
        a(gVar);
        this.c = false;
        return gVar.d();
    }

    public final boolean e() {
        g gVar;
        if (!this.d && (gVar = this.b) != null) {
            return gVar.e();
        }
        return true;
    }

    public final SSLSession f() {
        g gVar = this.b;
        a(gVar);
        if (!l()) {
            return null;
        }
        Socket i_ = gVar.i_();
        return i_ instanceof SSLSocket ? ((SSLSocket) i_).getSession() : null;
    }

    public final void g() {
        this.c = true;
    }

    /* access modifiers changed from: protected */
    public j h() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public synchronized void j() {
        this.b = null;
        this.a = null;
        this.e = Long.MAX_VALUE;
    }

    public final boolean l() {
        g gVar = this.b;
        if (gVar == null) {
            return false;
        }
        return gVar.l();
    }

    /* access modifiers changed from: protected */
    public final g n() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final boolean o() {
        return this.d;
    }

    public final InetAddress p() {
        g gVar = this.b;
        a(gVar);
        return gVar.p();
    }

    public final int q() {
        g gVar = this.b;
        a(gVar);
        return gVar.q();
    }

    public final boolean r() {
        return this.c;
    }
}
