package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.e.b;

public final class f implements ab {
    public final void a(com.agilebinary.a.a.a.f fVar, i iVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (!fVar.a("User-Agent")) {
            b g = fVar.g();
            if (g == null) {
                throw new IllegalArgumentException("HTTP parameters may not be null");
            }
            String str = (String) g.a("http.useragent");
            if (str != null) {
                fVar.a("User-Agent", str);
            }
        }
    }
}
