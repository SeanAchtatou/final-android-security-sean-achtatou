package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.e.a;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import com.agilebinary.a.a.a.x;

public abstract class l implements x {
    protected b a;
    protected b b;

    protected l() {
        this(null);
    }

    private l(b bVar) {
        this.a = new b();
        this.b = null;
    }

    public final void a(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        this.b = bVar;
    }

    public final void a(t tVar) {
        this.a.a(tVar);
    }

    public final void a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Header name may not be null");
        }
        this.a.a(new h(str, str2));
    }

    public final void a(t[] tVarArr) {
        this.a.a(tVarArr);
    }

    public final boolean a(String str) {
        return this.a.c(str);
    }

    public final void b(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Header name may not be null");
        }
        this.a.b(new h(str, str2));
    }

    public final t[] b(String str) {
        return this.a.a(str);
    }

    public final t c(String str) {
        return this.a.b(str);
    }

    public final w d(String str) {
        return this.a.d(str);
    }

    public final t[] e() {
        return this.a.b();
    }

    public final w f() {
        return this.a.c();
    }

    public final b g() {
        if (this.b == null) {
            this.b = new a();
        }
        return this.b;
    }
}
