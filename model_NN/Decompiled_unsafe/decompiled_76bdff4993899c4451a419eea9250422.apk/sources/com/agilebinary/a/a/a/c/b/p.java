package com.agilebinary.a.a.a.c.b;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.apache.commons.logging.Log;

public final class p {
    private final Log a;

    public p(Log log) {
        this.a = log;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.CharSequence):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.Object):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, float):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.String):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, long):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char[]):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, int):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, boolean):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, double):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder} */
    private void a(String str, InputStream inputStream) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                break;
            } else if (read == 13) {
                sb.append("[\\r]");
            } else if (read == 10) {
                sb.append("[\\n]\"");
                sb.insert(0, "\"");
                sb.insert(0, str);
                this.a.debug(sb.toString());
                sb.setLength(0);
            } else if (read < 32 || read > 127) {
                sb.append("[0x");
                sb.append(Integer.toHexString(read));
                sb.append("]");
            } else {
                sb.append((char) read);
            }
        }
        if (sb.length() > 0) {
            sb.append('\"');
            sb.insert(0, '\"');
            sb.insert(0, str);
            this.a.debug(sb.toString());
        }
    }

    public final void a(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("Output may not be null");
        }
        a(">> ", new ByteArrayInputStream(bArr));
    }

    public final void a(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("Output may not be null");
        }
        a(">> ", new ByteArrayInputStream(bArr, i, i2));
    }

    public final boolean a() {
        return this.a.isDebugEnabled();
    }

    public final void b(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("Input may not be null");
        }
        a("<< ", new ByteArrayInputStream(bArr));
    }

    public final void b(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("Input may not be null");
        }
        a("<< ", new ByteArrayInputStream(bArr, i, i2));
    }
}
