package com.agilebinary.a.a.a.a;

import com.agilebinary.a.a.a.e.b;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

public final class a {
    private final ConcurrentHashMap a = new ConcurrentHashMap();

    public final f a(String str, b bVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        com.agilebinary.a.a.a.h.b.b bVar2 = (com.agilebinary.a.a.a.h.b.b) this.a.get(str.toLowerCase(Locale.ENGLISH));
        if (bVar2 != null) {
            return bVar2.d();
        }
        throw new IllegalStateException("Unsupported authentication scheme: " + str);
    }
}
