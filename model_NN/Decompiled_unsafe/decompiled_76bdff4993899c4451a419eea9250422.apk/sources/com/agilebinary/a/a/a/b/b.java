package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class b implements Serializable, Cloneable {
    private final List a = new ArrayList(16);

    public final void a() {
        this.a.clear();
    }

    public final void a(t tVar) {
        if (tVar != null) {
            this.a.add(tVar);
        }
    }

    public final void a(t[] tVarArr) {
        a();
        if (tVarArr != null) {
            for (t add : tVarArr) {
                this.a.add(add);
            }
        }
    }

    public final t[] a(String str) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.a.size()) {
                return (t[]) arrayList.toArray(new t[arrayList.size()]);
            }
            t tVar = (t) this.a.get(i2);
            if (tVar.a().equalsIgnoreCase(str)) {
                arrayList.add(tVar);
            }
            i = i2 + 1;
        }
    }

    public final t b(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.a.size()) {
                return null;
            }
            t tVar = (t) this.a.get(i2);
            if (tVar.a().equalsIgnoreCase(str)) {
                return tVar;
            }
            i = i2 + 1;
        }
    }

    public final void b(t tVar) {
        if (tVar != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.a.size()) {
                    this.a.add(tVar);
                    return;
                } else if (((t) this.a.get(i2)).a().equalsIgnoreCase(tVar.a())) {
                    this.a.set(i2, tVar);
                    return;
                } else {
                    i = i2 + 1;
                }
            }
        }
    }

    public final t[] b() {
        return (t[]) this.a.toArray(new t[this.a.size()]);
    }

    public final w c() {
        return new g(this.a, null);
    }

    public final boolean c(String str) {
        for (int i = 0; i < this.a.size(); i++) {
            if (((t) this.a.get(i)).a().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    public final Object clone() {
        b bVar = (b) super.clone();
        bVar.a.clear();
        bVar.a.addAll(this.a);
        return bVar;
    }

    public final w d(String str) {
        return new g(this.a, str);
    }

    public final String toString() {
        return this.a.toString();
    }
}
