package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.c.e.c;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;

public final class a implements m, Cloneable {
    private final String a;
    private final String b;
    private final o[] c;

    public a(String str, String str2, o[] oVarArr) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.a = str;
        this.b = str2;
        if (oVarArr != null) {
            this.c = oVarArr;
        } else {
            this.c = new o[0];
        }
    }

    public final o a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        for (o oVar : this.c) {
            if (oVar.a().equalsIgnoreCase(str)) {
                return oVar;
            }
        }
        return null;
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }

    public final o[] c() {
        return (o[]) this.c.clone();
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean equals(Object obj) {
        boolean z;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof m)) {
            return false;
        }
        a aVar = (a) obj;
        if (!this.a.equals(aVar.a) || !c.a(this.b, aVar.b)) {
            return false;
        }
        o[] oVarArr = this.c;
        o[] oVarArr2 = aVar.c;
        if (oVarArr == null) {
            z = oVarArr2 == null;
        } else {
            if (oVarArr2 != null && oVarArr.length == oVarArr2.length) {
                int i = 0;
                while (true) {
                    if (i < oVarArr.length) {
                        if (!c.a(oVarArr[i], oVarArr2[i])) {
                            break;
                        }
                        i++;
                    } else {
                        z = true;
                        break;
                    }
                }
            }
            z = false;
        }
        return z;
    }

    public final int hashCode() {
        int a2 = c.a(c.a(17, this.a), this.b);
        for (o a3 : this.c) {
            a2 = c.a(a2, a3);
        }
        return a2;
    }

    public final String toString() {
        b bVar = new b(64);
        bVar.a(this.a);
        if (this.b != null) {
            bVar.a("=");
            bVar.a(this.b);
        }
        for (o valueOf : this.c) {
            bVar.a("; ");
            bVar.a(String.valueOf(valueOf));
        }
        return bVar.toString();
    }
}
