package com.agilebinary.a.a.a.h.b;

import com.agilebinary.a.a.a.c.e.c;
import java.util.Locale;

public final class g {
    private final String a;
    private final i b;
    private final int c;
    private final boolean d;
    private String e;

    public g(String str, int i, i iVar) {
        if (str == null) {
            throw new IllegalArgumentException("Scheme name may not be null");
        } else if (i <= 0 || i > 65535) {
            throw new IllegalArgumentException("Port is invalid: " + i);
        } else if (iVar == null) {
            throw new IllegalArgumentException("Socket factory may not be null");
        } else {
            this.a = str.toLowerCase(Locale.ENGLISH);
            this.b = iVar;
            this.c = i;
            this.d = iVar instanceof f;
        }
    }

    public g(String str, a aVar, int i) {
        if (str == null) {
            throw new IllegalArgumentException("Scheme name may not be null");
        } else if (aVar == null) {
            throw new IllegalArgumentException("Socket factory may not be null");
        } else {
            this.a = str.toLowerCase(Locale.ENGLISH);
            if (aVar instanceof j) {
                this.b = new c((j) aVar);
                this.d = true;
            } else {
                this.b = new d(aVar);
                this.d = false;
            }
            this.c = 443;
        }
    }

    public final int a() {
        return this.c;
    }

    public final int a(int i) {
        return i <= 0 ? this.c : i;
    }

    public final i b() {
        return this.b;
    }

    public final String c() {
        return this.a;
    }

    public final boolean d() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        return this.a.equals(gVar.a) && this.c == gVar.c && this.d == gVar.d && this.b.equals(gVar.b);
    }

    public final int hashCode() {
        return c.a((this.d ? 1 : 0) + (c.a(this.c + 629, this.a) * 37), this.b);
    }

    public final String toString() {
        if (this.e == null) {
            this.e = this.a + ':' + Integer.toString(this.c);
        }
        return this.e;
    }
}
