package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.h.b;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.c.f;
import com.agilebinary.a.a.a.h.j;
import com.agilebinary.a.a.a.h.l;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public final class i implements j {
    protected final l a;
    private final Log b = a.a(getClass());
    private h c;
    private boolean d;
    private f e;
    private c f;
    private long g;
    private long h;
    private volatile boolean i;

    public i(h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException("Scheme registry must not be null.");
        }
        this.c = hVar;
        this.a = new b(hVar);
        this.e = new f(this);
        this.f = null;
        this.g = -1;
        this.d = false;
        this.i = false;
    }

    private void b() {
        if (this.i) {
            throw new IllegalStateException("Manager is shut down.");
        }
    }

    private synchronized void c() {
        if (System.currentTimeMillis() >= this.h) {
            a(0, TimeUnit.MILLISECONDS);
        }
    }

    private synchronized void d() {
        this.i = true;
        if (this.f != null) {
            this.f.j();
        }
        try {
            if (this.e != null) {
                this.e.d();
            }
            this.e = null;
        } catch (IOException e2) {
            this.b.debug("Problem while shutting down manager.", e2);
            this.e = null;
        } catch (Throwable th) {
            this.e = null;
            throw th;
        }
        return;
    }

    public final synchronized com.agilebinary.a.a.a.h.a a(c cVar) {
        boolean z;
        c cVar2;
        boolean z2 = true;
        boolean z3 = false;
        synchronized (this) {
            if (cVar == null) {
                throw new IllegalArgumentException("Route may not be null.");
            }
            b();
            if (this.b.isDebugEnabled()) {
                this.b.debug("Get connection for route " + cVar);
            }
            if (this.f != null) {
                throw new IllegalStateException("Invalid use of SingleClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.");
            }
            c();
            if (this.e.a.l()) {
                f fVar = this.e.c;
                boolean z4 = fVar == null || !fVar.h().equals(cVar);
                z = false;
                z3 = z4;
            } else {
                z = true;
            }
            if (z3) {
                try {
                    this.e.d();
                } catch (IOException e2) {
                    this.b.debug("Problem shutting down connection.", e2);
                }
            } else {
                z2 = z;
            }
            if (z2) {
                this.e = new f(this);
            }
            this.f = new c(this, this.e, cVar);
            cVar2 = this.f;
        }
        return cVar2;
    }

    public final h a() {
        return this.c;
    }

    public final b a(c cVar, Object obj) {
        return new d(this, cVar, obj);
    }

    public final synchronized void a(long j, TimeUnit timeUnit) {
        b();
        if (timeUnit == null) {
            throw new IllegalArgumentException("Time unit must not be null.");
        } else if (this.f == null && this.e.a.l()) {
            if (this.g <= System.currentTimeMillis() - timeUnit.toMillis(j)) {
                try {
                    this.e.c();
                } catch (IOException e2) {
                    this.b.debug("Problem closing idle connection.", e2);
                }
            }
        }
        return;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:33:0x006b=Splitter:B:33:0x006b, B:43:0x009d=Splitter:B:43:0x009d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.agilebinary.a.a.a.h.a r6, long r7, java.util.concurrent.TimeUnit r9) {
        /*
            r5 = this;
            r3 = 0
            monitor-enter(r5)
            r5.b()     // Catch:{ all -> 0x0012 }
            boolean r0 = r6 instanceof com.agilebinary.a.a.a.c.b.c     // Catch:{ all -> 0x0012 }
            if (r0 != 0) goto L_0x0015
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0012 }
            java.lang.String r1 = "Connection class mismatch, connection not obtained from this manager."
            r0.<init>(r1)     // Catch:{ all -> 0x0012 }
            throw r0     // Catch:{ all -> 0x0012 }
        L_0x0012:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0015:
            org.apache.commons.logging.Log r0 = r5.b     // Catch:{ all -> 0x0012 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0012 }
            if (r0 == 0) goto L_0x0035
            org.apache.commons.logging.Log r0 = r5.b     // Catch:{ all -> 0x0012 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0012 }
            r1.<init>()     // Catch:{ all -> 0x0012 }
            java.lang.String r2 = "Releasing connection "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0012 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ all -> 0x0012 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0012 }
            r0.debug(r1)     // Catch:{ all -> 0x0012 }
        L_0x0035:
            com.agilebinary.a.a.a.c.b.c r6 = (com.agilebinary.a.a.a.c.b.c) r6     // Catch:{ all -> 0x0012 }
            com.agilebinary.a.a.a.c.b.a r0 = r6.a     // Catch:{ all -> 0x0012 }
            if (r0 != 0) goto L_0x003d
        L_0x003b:
            monitor-exit(r5)
            return
        L_0x003d:
            com.agilebinary.a.a.a.h.j r0 = r6.h()     // Catch:{ all -> 0x0012 }
            if (r0 == 0) goto L_0x004d
            if (r0 == r5) goto L_0x004d
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0012 }
            java.lang.String r1 = "Connection not obtained from this manager."
            r0.<init>(r1)     // Catch:{ all -> 0x0012 }
            throw r0     // Catch:{ all -> 0x0012 }
        L_0x004d:
            boolean r0 = r6.l()     // Catch:{ IOException -> 0x008d }
            if (r0 == 0) goto L_0x006b
            boolean r0 = r6.r()     // Catch:{ IOException -> 0x008d }
            if (r0 != 0) goto L_0x006b
            org.apache.commons.logging.Log r0 = r5.b     // Catch:{ IOException -> 0x008d }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ IOException -> 0x008d }
            if (r0 == 0) goto L_0x0068
            org.apache.commons.logging.Log r0 = r5.b     // Catch:{ IOException -> 0x008d }
            java.lang.String r1 = "Released connection open but not reusable."
            r0.debug(r1)     // Catch:{ IOException -> 0x008d }
        L_0x0068:
            r6.m()     // Catch:{ IOException -> 0x008d }
        L_0x006b:
            r6.j()     // Catch:{ all -> 0x0012 }
            r0 = 0
            r5.f = r0     // Catch:{ all -> 0x0012 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r5.g = r0     // Catch:{ all -> 0x0012 }
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0085
            long r0 = r9.toMillis(r7)     // Catch:{ all -> 0x0012 }
            long r2 = r5.g     // Catch:{ all -> 0x0012 }
            long r0 = r0 + r2
            r5.h = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003b
        L_0x0085:
            r0 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r5.h = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003b
        L_0x008d:
            r0 = move-exception
            org.apache.commons.logging.Log r1 = r5.b     // Catch:{ all -> 0x00c0 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x00c0 }
            if (r1 == 0) goto L_0x009d
            org.apache.commons.logging.Log r1 = r5.b     // Catch:{ all -> 0x00c0 }
            java.lang.String r2 = "Exception shutting down released connection."
            r1.debug(r2, r0)     // Catch:{ all -> 0x00c0 }
        L_0x009d:
            r6.j()     // Catch:{ all -> 0x0012 }
            r0 = 0
            r5.f = r0     // Catch:{ all -> 0x0012 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r5.g = r0     // Catch:{ all -> 0x0012 }
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x00b7
            long r0 = r9.toMillis(r7)     // Catch:{ all -> 0x0012 }
            long r2 = r5.g     // Catch:{ all -> 0x0012 }
            long r0 = r0 + r2
            r5.h = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003b
        L_0x00b7:
            r0 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r5.h = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003b
        L_0x00c0:
            r0 = move-exception
            r6.j()     // Catch:{ all -> 0x0012 }
            r1 = 0
            r5.f = r1     // Catch:{ all -> 0x0012 }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r5.g = r1     // Catch:{ all -> 0x0012 }
            int r1 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x00db
            long r1 = r9.toMillis(r7)     // Catch:{ all -> 0x0012 }
            long r3 = r5.g     // Catch:{ all -> 0x0012 }
            long r1 = r1 + r3
            r5.h = r1     // Catch:{ all -> 0x0012 }
        L_0x00da:
            throw r0     // Catch:{ all -> 0x0012 }
        L_0x00db:
            r1 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r5.h = r1     // Catch:{ all -> 0x0012 }
            goto L_0x00da
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.i.a(com.agilebinary.a.a.a.h.a, long, java.util.concurrent.TimeUnit):void");
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        try {
            d();
        } finally {
            super.finalize();
        }
    }
}
