package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.e.b;
import java.net.Socket;

public final class h extends d {
    public h(Socket socket, int i, b bVar) {
        int i2 = 1024;
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        }
        int sendBufferSize = i < 0 ? socket.getSendBufferSize() : i;
        a(socket.getOutputStream(), sendBufferSize >= 1024 ? sendBufferSize : i2, bVar);
    }
}
