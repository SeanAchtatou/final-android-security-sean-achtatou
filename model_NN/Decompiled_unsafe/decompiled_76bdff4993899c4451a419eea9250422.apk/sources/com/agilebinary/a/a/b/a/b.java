package com.agilebinary.a.a.b.a;

import com.agilebinary.mobilemonitor.device.a.e.a;
import org.apache.commons.logging.Log;

public final class b implements Log {
    private String a;

    public b(String str) {
        this.a = str;
    }

    public final void debug(Object obj) {
        obj.toString();
    }

    public final void debug(Object obj, Throwable th) {
        a.b(this.a, obj.toString(), th);
    }

    public final void error(Object obj) {
        obj.toString();
    }

    public final void error(Object obj, Throwable th) {
        a.e(this.a, obj.toString(), th);
    }

    public final void fatal(Object obj) {
        obj.toString();
    }

    public final void fatal(Object obj, Throwable th) {
        a.e(this.a, obj.toString(), th);
    }

    public final void info(Object obj) {
        obj.toString();
    }

    public final void info(Object obj, Throwable th) {
        a.c(this.a, obj.toString(), th);
    }

    public final boolean isDebugEnabled() {
        return false;
    }

    public final boolean isErrorEnabled() {
        return false;
    }

    public final boolean isFatalEnabled() {
        return false;
    }

    public final boolean isInfoEnabled() {
        return false;
    }

    public final boolean isTraceEnabled() {
        return false;
    }

    public final boolean isWarnEnabled() {
        return false;
    }

    public final void trace(Object obj) {
        obj.toString();
    }

    public final void trace(Object obj, Throwable th) {
        a.a(this.a, obj.toString(), th);
    }

    public final void warn(Object obj) {
        obj.toString();
    }

    public final void warn(Object obj, Throwable th) {
        a.d(this.a, obj.toString(), th);
    }
}
