package com.agilebinary.a.a.a.f;

public final class e implements i {
    private final i a;
    private final i b;

    public e(i iVar, i iVar2) {
        if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        }
        this.a = iVar;
        this.b = iVar2;
    }

    public final Object a(String str) {
        Object a2 = this.a.a(str);
        return a2 == null ? this.b.a(str) : a2;
    }

    public final void a(String str, Object obj) {
        this.a.a(str, obj);
    }
}
