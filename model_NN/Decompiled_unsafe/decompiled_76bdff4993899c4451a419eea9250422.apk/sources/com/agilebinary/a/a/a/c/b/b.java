package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.h.b.f;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.l;
import com.agilebinary.a.a.b.a.a;
import java.net.ConnectException;
import java.net.Socket;
import org.apache.commons.logging.Log;

public class b implements l {
    private final Log a;
    private h b;

    public b() {
    }

    public b(h hVar) {
        this.a = a.a(getClass());
        if (hVar == null) {
            throw new IllegalArgumentException("Scheme registry amy not be null");
        }
        this.b = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.b.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.b.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.b
      com.agilebinary.a.a.a.e.b.a(java.lang.String, boolean):boolean */
    private static void a(Socket socket, com.agilebinary.a.a.a.e.b bVar) {
        boolean z = true;
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        socket.setTcpNoDelay(bVar.a("http.tcp.nodelay", true));
        socket.setSoTimeout(com.agilebinary.a.a.a.c.e.a.a(bVar));
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        int a2 = bVar.a("http.socket.linger", -1);
        if (a2 >= 0) {
            if (a2 <= 0) {
                z = false;
            }
            socket.setSoLinger(z, a2);
        }
    }

    public final g a() {
        return new h();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.h.b.f.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      com.agilebinary.a.a.a.h.b.i.a(java.net.Socket, java.net.InetSocketAddress, java.net.InetSocketAddress, com.agilebinary.a.a.a.e.b):java.net.Socket
      com.agilebinary.a.a.a.h.b.f.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket */
    public final void a(g gVar, com.agilebinary.a.a.a.b bVar, i iVar, com.agilebinary.a.a.a.e.b bVar2) {
        if (gVar == null) {
            throw new IllegalArgumentException("Connection may not be null");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Target host may not be null");
        } else if (bVar2 == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        } else if (!gVar.l()) {
            throw new IllegalStateException("Connection must be open");
        } else {
            com.agilebinary.a.a.a.h.b.g a2 = this.b.a(bVar.c());
            if (!(a2.b() instanceof f)) {
                throw new IllegalArgumentException("Target scheme (" + a2.c() + ") must have layered socket factory.");
            }
            f fVar = (f) a2.b();
            try {
                Socket a3 = fVar.a(gVar.i_(), bVar.a(), bVar.b(), true);
                a(a3, bVar2);
                gVar.a(a3, bVar, fVar.a(a3), bVar2);
            } catch (ConnectException e) {
                throw new com.agilebinary.a.a.a.h.f(bVar, e);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00e0 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.h.g r13, com.agilebinary.a.a.a.b r14, java.net.InetAddress r15, com.agilebinary.a.a.a.f.i r16, com.agilebinary.a.a.a.e.b r17) {
        /*
            r12 = this;
            if (r13 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Connection may not be null"
            r1.<init>(r2)
            throw r1
        L_0x000a:
            if (r14 != 0) goto L_0x0014
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Target host may not be null"
            r1.<init>(r2)
            throw r1
        L_0x0014:
            if (r17 != 0) goto L_0x001e
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Parameters may not be null"
            r1.<init>(r2)
            throw r1
        L_0x001e:
            boolean r1 = r13.l()
            if (r1 == 0) goto L_0x002c
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "Connection must not be open"
            r1.<init>(r2)
            throw r1
        L_0x002c:
            com.agilebinary.a.a.a.h.b.h r1 = r12.b
            java.lang.String r2 = r14.c()
            com.agilebinary.a.a.a.h.b.g r1 = r1.a(r2)
            com.agilebinary.a.a.a.h.b.i r5 = r1.b()
            java.lang.String r2 = r14.a()
            java.net.InetAddress[] r6 = java.net.InetAddress.getAllByName(r2)
            int r2 = r14.b()
            int r7 = r1.a(r2)
            r1 = 0
        L_0x004b:
            int r2 = r6.length
            if (r1 >= r2) goto L_0x00a4
            r3 = r6[r1]
            int r2 = r6.length
            int r2 = r2 + -1
            if (r1 != r2) goto L_0x00a5
            r2 = 1
        L_0x0056:
            java.net.Socket r4 = r5.e_()
            r13.a(r4, r14)
            java.net.InetSocketAddress r8 = new java.net.InetSocketAddress
            r8.<init>(r3, r7)
            r3 = 0
            if (r15 == 0) goto L_0x006b
            java.net.InetSocketAddress r3 = new java.net.InetSocketAddress
            r9 = 0
            r3.<init>(r15, r9)
        L_0x006b:
            org.apache.commons.logging.Log r9 = r12.a
            boolean r9 = r9.isDebugEnabled()
            if (r9 == 0) goto L_0x008b
            org.apache.commons.logging.Log r9 = r12.a
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "Connecting to "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r8)
            java.lang.String r10 = r10.toString()
            r9.debug(r10)
        L_0x008b:
            r0 = r17
            java.net.Socket r3 = r5.a(r4, r8, r3, r0)     // Catch:{ ConnectException -> 0x00a7, h -> 0x00b0 }
            if (r4 == r3) goto L_0x00e4
            r13.a(r3, r14)     // Catch:{ ConnectException -> 0x00a7, h -> 0x00b0 }
        L_0x0096:
            r0 = r17
            a(r3, r0)     // Catch:{ ConnectException -> 0x00a7, h -> 0x00b0 }
            boolean r3 = r5.a(r3)     // Catch:{ ConnectException -> 0x00a7, h -> 0x00b0 }
            r0 = r17
            r13.a(r3, r0)     // Catch:{ ConnectException -> 0x00a7, h -> 0x00b0 }
        L_0x00a4:
            return
        L_0x00a5:
            r2 = 0
            goto L_0x0056
        L_0x00a7:
            r3 = move-exception
            if (r2 == 0) goto L_0x00b4
            com.agilebinary.a.a.a.h.f r1 = new com.agilebinary.a.a.a.h.f
            r1.<init>(r14, r3)
            throw r1
        L_0x00b0:
            r3 = move-exception
            if (r2 == 0) goto L_0x00b4
            throw r3
        L_0x00b4:
            org.apache.commons.logging.Log r2 = r12.a
            boolean r2 = r2.isDebugEnabled()
            if (r2 == 0) goto L_0x00e0
            org.apache.commons.logging.Log r2 = r12.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Connect to "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r8)
            java.lang.String r4 = " timed out. "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "Connection will be retried using another IP address"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.debug(r3)
        L_0x00e0:
            int r1 = r1 + 1
            goto L_0x004b
        L_0x00e4:
            r3 = r4
            goto L_0x0096
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.b.a(com.agilebinary.a.a.a.h.g, com.agilebinary.a.a.a.b, java.net.InetAddress, com.agilebinary.a.a.a.f.i, com.agilebinary.a.a.a.e.b):void");
    }
}
