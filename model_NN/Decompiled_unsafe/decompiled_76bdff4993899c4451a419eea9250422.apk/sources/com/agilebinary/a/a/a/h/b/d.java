package com.agilebinary.a.a.a.h.b;

import com.agilebinary.a.a.a.e.b;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

class d implements i {
    private final a a;

    d(a aVar) {
        this.a = aVar;
    }

    public final Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, b bVar) {
        String hostName = inetSocketAddress.getHostName();
        int port = inetSocketAddress.getPort();
        InetAddress inetAddress = null;
        int i = 0;
        if (inetSocketAddress2 != null) {
            inetAddress = inetSocketAddress2.getAddress();
            i = inetSocketAddress2.getPort();
        }
        return this.a.a(socket, hostName, port, inetAddress, i, bVar);
    }

    public final boolean a(Socket socket) {
        return this.a.a(socket);
    }

    public final Socket e_() {
        return this.a.a();
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return obj instanceof d ? this.a.equals(((d) obj).a) : this.a.equals(obj);
    }

    public int hashCode() {
        return this.a.hashCode();
    }
}
