package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.f;
import com.agilebinary.a.a.a.a.g;
import com.agilebinary.a.a.a.c.b.a.c;
import com.agilebinary.a.a.a.d.b;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.b.a.a;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.logging.Log;

public abstract class q implements b {
    private static final List b = Collections.unmodifiableList(Arrays.asList("negotiate", "NTLM", "Digest", "Basic"));
    private final Log a = a.a(getClass());

    protected static Map a(t[] tVarArr) {
        com.agilebinary.a.a.a.i.b bVar;
        int i;
        HashMap hashMap = new HashMap(tVarArr.length);
        for (t tVar : tVarArr) {
            if (tVar instanceof r) {
                bVar = ((r) tVar).e();
                i = ((r) tVar).d();
            } else {
                String b2 = tVar.b();
                if (b2 == null) {
                    throw new g("Header value is null");
                }
                com.agilebinary.a.a.a.i.b bVar2 = new com.agilebinary.a.a.a.i.b(b2.length());
                bVar2.a(b2);
                bVar = bVar2;
                i = 0;
            }
            while (i < bVar.c() && c.a(bVar.a(i))) {
                i++;
            }
            int i2 = i;
            while (i2 < bVar.c() && !c.a(bVar.a(i2))) {
                i2++;
            }
            hashMap.put(bVar.a(i, i2).toLowerCase(Locale.ENGLISH), tVar);
        }
        return hashMap;
    }

    public final f a(Map map, j jVar, i iVar) {
        f fVar;
        com.agilebinary.a.a.a.a.a aVar = (com.agilebinary.a.a.a.a.a) iVar.a("http.authscheme-registry");
        if (aVar == null) {
            throw new IllegalStateException("AuthScheme registry not set in HTTP context");
        }
        List a2 = a(jVar, iVar);
        if (a2 == null) {
            a2 = b;
        }
        if (this.a.isDebugEnabled()) {
            this.a.debug("Authentication schemes in the order of preference: " + a2);
        }
        Iterator it = a2.iterator();
        while (true) {
            if (!it.hasNext()) {
                fVar = null;
                break;
            }
            String str = (String) it.next();
            if (((t) map.get(str.toLowerCase(Locale.ENGLISH))) != null) {
                if (this.a.isDebugEnabled()) {
                    this.a.debug(str + " authentication scheme selected");
                }
                try {
                    fVar = aVar.a(str, jVar.g());
                    break;
                } catch (IllegalStateException e) {
                    if (this.a.isWarnEnabled()) {
                        this.a.warn("Authentication scheme " + str + " not supported");
                    }
                }
            } else if (this.a.isDebugEnabled()) {
                this.a.debug("Challenge for " + str + " authentication scheme not available");
            }
        }
        if (fVar != null) {
            return fVar;
        }
        throw new com.agilebinary.a.a.a.a.b("Unable to respond to any of these challenges: " + map);
    }

    /* access modifiers changed from: protected */
    public List a(j jVar, i iVar) {
        return b;
    }
}
