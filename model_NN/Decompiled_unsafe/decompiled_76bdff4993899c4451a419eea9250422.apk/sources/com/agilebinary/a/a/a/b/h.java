package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.t;
import java.io.Serializable;

public final class h implements t, Serializable, Cloneable {
    private final String a;
    private final String b;

    public h(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.a = str;
        this.b = str2;
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }

    public final m[] c() {
        return this.b != null ? f.a(this.b, (j) null) : new m[0];
    }

    public final Object clone() {
        return super.clone();
    }

    public final String toString() {
        return q.a.a((b) null, this).toString();
    }
}
