package com.agilebinary.a.a.a.c;

import com.agilebinary.a.a.a.ac;
import com.agilebinary.a.a.a.b.k;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.j;
import java.util.Locale;

public final class b implements i {
    private ac a;

    public b() {
        this(d.a);
    }

    private b(ac acVar) {
        if (acVar == null) {
            throw new IllegalArgumentException("Reason phrase catalog must not be null.");
        }
        this.a = acVar;
    }

    public final j a(h hVar, com.agilebinary.a.a.a.f.i iVar) {
        if (hVar == null) {
            throw new IllegalArgumentException("Status line may not be null");
        }
        return new k(hVar, this.a, Locale.getDefault());
    }
}
