package com.agilebinary.a.a.a.i;

public final class c {
    private final String a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;

    private c(String str, String str2, String str3, String str4, String str5) {
        if (str == null) {
            throw new IllegalArgumentException("Package identifier must not be null.");
        }
        this.a = str;
        this.b = str2 == null ? "UNAVAILABLE" : str2;
        this.c = str3 == null ? "UNAVAILABLE" : str3;
        this.d = str4 == null ? "UNAVAILABLE" : str4;
        this.e = str5 == null ? "UNAVAILABLE" : str5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00c1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final com.agilebinary.a.a.a.i.c a(java.lang.String r6, java.lang.ClassLoader r7) {
        /*
            r5 = 0
            if (r6 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Package identifier must not be null."
            r0.<init>(r1)
            throw r0
        L_0x000b:
            if (r7 != 0) goto L_0x0015
            java.lang.Thread r0 = java.lang.Thread.currentThread()
            java.lang.ClassLoader r7 = r0.getContextClassLoader()
        L_0x0015:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0059 }
            r0.<init>()     // Catch:{ IOException -> 0x0059 }
            r1 = 46
            r2 = 47
            java.lang.String r1 = r6.replace(r1, r2)     // Catch:{ IOException -> 0x0059 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ IOException -> 0x0059 }
            java.lang.String r1 = "/"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ IOException -> 0x0059 }
            java.lang.String r1 = "version.properties"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ IOException -> 0x0059 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0059 }
            java.io.InputStream r1 = r7.getResourceAsStream(r0)     // Catch:{ IOException -> 0x0059 }
            if (r1 == 0) goto L_0x005d
            java.util.Properties r0 = new java.util.Properties     // Catch:{ all -> 0x0054 }
            r0.<init>()     // Catch:{ all -> 0x0054 }
            r0.load(r1)     // Catch:{ all -> 0x0054 }
            r1.close()     // Catch:{ IOException -> 0x00b3 }
            r3 = r0
        L_0x0048:
            if (r3 == 0) goto L_0x00c1
            if (r6 != 0) goto L_0x005f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Package identifier must not be null."
            r0.<init>(r1)
            throw r0
        L_0x0054:
            r0 = move-exception
            r1.close()     // Catch:{ IOException -> 0x0059 }
            throw r0     // Catch:{ IOException -> 0x0059 }
        L_0x0059:
            r0 = move-exception
            r0 = r5
        L_0x005b:
            r3 = r0
            goto L_0x0048
        L_0x005d:
            r3 = r5
            goto L_0x0048
        L_0x005f:
            if (r3 == 0) goto L_0x00bd
            java.lang.String r0 = "info.module"
            java.lang.Object r0 = r3.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00bb
            int r1 = r0.length()
            if (r1 > 0) goto L_0x00bb
            r1 = r5
        L_0x0072:
            java.lang.String r0 = "info.release"
            java.lang.Object r0 = r3.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00b9
            int r2 = r0.length()
            if (r2 <= 0) goto L_0x008a
            java.lang.String r2 = "${pom.version}"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x00b9
        L_0x008a:
            r2 = r5
        L_0x008b:
            java.lang.String r0 = "info.timestamp"
            java.lang.Object r0 = r3.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x00b5
            int r3 = r0.length()
            if (r3 <= 0) goto L_0x00a3
            java.lang.String r3 = "${mvn.timestamp}"
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x00b5
        L_0x00a3:
            r4 = r5
            r3 = r2
            r2 = r1
        L_0x00a6:
            if (r7 == 0) goto L_0x00ac
            java.lang.String r5 = r7.toString()
        L_0x00ac:
            com.agilebinary.a.a.a.i.c r0 = new com.agilebinary.a.a.a.i.c
            r1 = r6
            r0.<init>(r1, r2, r3, r4, r5)
        L_0x00b2:
            return r0
        L_0x00b3:
            r1 = move-exception
            goto L_0x005b
        L_0x00b5:
            r4 = r0
            r3 = r2
            r2 = r1
            goto L_0x00a6
        L_0x00b9:
            r2 = r0
            goto L_0x008b
        L_0x00bb:
            r1 = r0
            goto L_0x0072
        L_0x00bd:
            r4 = r5
            r3 = r5
            r2 = r5
            goto L_0x00a6
        L_0x00c1:
            r0 = r5
            goto L_0x00b2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.i.c.a(java.lang.String, java.lang.ClassLoader):com.agilebinary.a.a.a.i.c");
    }

    public final String a() {
        return this.c;
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer(this.a.length() + 20 + this.b.length() + this.c.length() + this.d.length() + this.e.length());
        stringBuffer.append("VersionInfo(").append(this.a).append(':').append(this.b);
        if (!"UNAVAILABLE".equals(this.c)) {
            stringBuffer.append(':').append(this.c);
        }
        if (!"UNAVAILABLE".equals(this.d)) {
            stringBuffer.append(':').append(this.d);
        }
        stringBuffer.append(')');
        if (!"UNAVAILABLE".equals(this.e)) {
            stringBuffer.append('@').append(this.e);
        }
        return stringBuffer.toString();
    }
}
