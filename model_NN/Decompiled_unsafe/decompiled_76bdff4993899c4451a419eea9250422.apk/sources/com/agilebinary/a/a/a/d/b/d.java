package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.i;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.j;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.b.a.a;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import org.apache.commons.logging.Log;

public final class d implements ab {
    private final Log a = a.a(getClass());

    public final void a(f fVar, i iVar) {
        URI uri;
        int i;
        t b;
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else if (!fVar.a().a().equalsIgnoreCase("CONNECT")) {
            com.agilebinary.a.a.a.d.d dVar = (com.agilebinary.a.a.a.d.d) iVar.a("http.cookie-store");
            if (dVar == null) {
                this.a.info("Cookie store not available in HTTP context");
                return;
            }
            com.agilebinary.a.a.a.k.d dVar2 = (com.agilebinary.a.a.a.k.d) iVar.a("http.cookiespec-registry");
            if (dVar2 == null) {
                this.a.info("CookieSpec registry not available in HTTP context");
                return;
            }
            b bVar = (b) iVar.a("http.target_host");
            if (bVar == null) {
                throw new IllegalStateException("Target host not specified in HTTP context");
            }
            com.agilebinary.a.a.a.h.a aVar = (com.agilebinary.a.a.a.h.a) iVar.a("http.connection");
            if (aVar == null) {
                throw new IllegalStateException("Client connection not specified in HTTP context");
            }
            com.agilebinary.a.a.a.e.b g = fVar.g();
            if (g == null) {
                throw new IllegalArgumentException("HTTP parameters may not be null");
            }
            String str = (String) g.a("http.protocol.cookie-policy");
            String str2 = str == null ? "best-match" : str;
            if (this.a.isDebugEnabled()) {
                this.a.debug("CookieSpec selected: " + str2);
            }
            if (fVar instanceof com.agilebinary.a.a.a.d.c.b) {
                uri = ((com.agilebinary.a.a.a.d.c.b) fVar).c_();
            } else {
                try {
                    uri = new URI(fVar.a().c());
                } catch (URISyntaxException e) {
                    throw new l("Invalid request URI: " + fVar.a().c(), e);
                }
            }
            String a2 = bVar.a();
            int b2 = bVar.b();
            if (b2 < 0) {
                h hVar = (h) iVar.a("http.scheme-registry");
                i = hVar != null ? hVar.b(bVar.c()).a(b2) : aVar.q();
            } else {
                i = b2;
            }
            com.agilebinary.a.a.a.k.f fVar2 = new com.agilebinary.a.a.a.k.f(a2, i, uri.getPath(), aVar.a());
            j a3 = dVar2.a(str2, fVar.g());
            ArrayList<c> arrayList = new ArrayList<>(dVar.a());
            ArrayList<c> arrayList2 = new ArrayList<>();
            Date date = new Date();
            for (c cVar : arrayList) {
                if (!cVar.b(date)) {
                    if (a3.b(cVar, fVar2)) {
                        if (this.a.isDebugEnabled()) {
                            this.a.debug("Cookie " + cVar + " match " + fVar2);
                        }
                        arrayList2.add(cVar);
                    }
                } else if (this.a.isDebugEnabled()) {
                    this.a.debug("Cookie " + cVar + " expired");
                }
            }
            if (!arrayList2.isEmpty()) {
                for (t a4 : a3.a(arrayList2)) {
                    fVar.a(a4);
                }
            }
            int a5 = a3.a();
            if (a5 > 0) {
                boolean z = false;
                for (c cVar2 : arrayList2) {
                    z = (a5 != cVar2.g() || !(cVar2 instanceof com.agilebinary.a.a.a.k.h)) ? true : z;
                }
                if (z && (b = a3.b()) != null) {
                    fVar.a(b);
                }
            }
            iVar.a("http.cookie-spec", a3);
            iVar.a("http.cookie-origin", fVar2);
        }
    }
}
