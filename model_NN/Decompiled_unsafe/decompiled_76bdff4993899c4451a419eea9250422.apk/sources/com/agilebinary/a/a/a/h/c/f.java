package com.agilebinary.a.a.a.h.c;

import com.agilebinary.a.a.a.b;
import java.net.InetAddress;

public final class f implements b, Cloneable {
    private final b a;
    private final InetAddress b;
    private boolean c;
    private b[] d;
    private g e;
    private e f;
    private boolean g;

    private f(b bVar, InetAddress inetAddress) {
        if (bVar == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        }
        this.a = bVar;
        this.b = inetAddress;
        this.e = g.a;
        this.f = e.a;
    }

    public f(c cVar) {
        this(cVar.a(), cVar.b());
    }

    public final b a() {
        return this.a;
    }

    public final b a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Hop index must not be negative: " + i);
        }
        int c2 = c();
        if (i < c2) {
            return i < c2 + -1 ? this.d[i] : this.a;
        }
        throw new IllegalArgumentException("Hop index " + i + " exceeds tracked route length " + c2 + ".");
    }

    public final void a(b bVar, boolean z) {
        if (bVar == null) {
            throw new IllegalArgumentException("Proxy host may not be null.");
        } else if (this.c) {
            throw new IllegalStateException("Already connected.");
        } else {
            this.c = true;
            this.d = new b[]{bVar};
            this.g = z;
        }
    }

    public final void a(boolean z) {
        if (this.c) {
            throw new IllegalStateException("Already connected.");
        }
        this.c = true;
        this.g = z;
    }

    public final InetAddress b() {
        return this.b;
    }

    public final void b(boolean z) {
        if (!this.c) {
            throw new IllegalStateException("No tunnel unless connected.");
        } else if (this.d == null) {
            throw new IllegalStateException("No tunnel without proxy.");
        } else {
            this.e = g.b;
            this.g = z;
        }
    }

    public final int c() {
        if (!this.c) {
            return 0;
        }
        if (this.d == null) {
            return 1;
        }
        return this.d.length + 1;
    }

    public final void c(boolean z) {
        if (!this.c) {
            throw new IllegalStateException("No layered protocol unless connected.");
        }
        this.f = e.b;
        this.g = z;
    }

    public final Object clone() {
        return super.clone();
    }

    public final boolean d() {
        return this.e == g.b;
    }

    public final boolean e() {
        return this.f == e.b;
    }

    public final boolean equals(Object obj) {
        boolean z = true;
        int i = 0;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        boolean equals = (this.d == fVar.d || !(this.d == null || fVar.d == null || this.d.length != fVar.d.length)) & this.a.equals(fVar.a) & (this.b == fVar.b || (this.b != null && this.b.equals(fVar.b)));
        if (!(this.c == fVar.c && this.g == fVar.g && this.e == fVar.e && this.f == fVar.f)) {
            z = false;
        }
        boolean z2 = equals & z;
        if (z2 && this.d != null) {
            while (z2 && i < this.d.length) {
                z2 = this.d[i].equals(fVar.d[i]);
                i++;
            }
        }
        return z2;
    }

    public final boolean f() {
        return this.g;
    }

    public final boolean g() {
        return this.c;
    }

    public final c h() {
        if (!this.c) {
            return null;
        }
        return new c(this.a, this.b, this.d, this.g, this.e, this.f);
    }

    public final int hashCode() {
        int i;
        int hashCode = this.a.hashCode();
        if (this.b != null) {
            hashCode ^= this.b.hashCode();
        }
        if (this.d != null) {
            i = this.d.length ^ hashCode;
            for (b hashCode2 : this.d) {
                i ^= hashCode2.hashCode();
            }
        } else {
            i = hashCode;
        }
        if (this.c) {
            i ^= 286331153;
        }
        if (this.g) {
            i ^= 572662306;
        }
        return (this.e.hashCode() ^ i) ^ this.f.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((c() * 30) + 50);
        sb.append("RouteTracker[");
        if (this.b != null) {
            sb.append(this.b);
            sb.append("->");
        }
        sb.append('{');
        if (this.c) {
            sb.append('c');
        }
        if (this.e == g.b) {
            sb.append('t');
        }
        if (this.f == e.b) {
            sb.append('l');
        }
        if (this.g) {
            sb.append('s');
        }
        sb.append("}->");
        if (this.d != null) {
            for (b append : this.d) {
                sb.append(append);
                sb.append("->");
            }
        }
        sb.append(this.a);
        sb.append(']');
        return sb.toString();
    }
}
