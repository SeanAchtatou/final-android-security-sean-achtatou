package com.agilebinary.a.a.a.i;

import java.io.Serializable;

public final class d implements Serializable {
    private byte[] a;
    private int b;

    public d(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Buffer capacity may not be negative");
        }
        this.a = new byte[i];
    }

    private void d(int i) {
        byte[] bArr = new byte[Math.max(this.a.length << 1, i)];
        System.arraycopy(this.a, 0, bArr, 0, this.b);
        this.a = bArr;
    }

    public final void a() {
        this.b = 0;
    }

    public final void a(int i) {
        int i2 = this.b + 1;
        if (i2 > this.a.length) {
            d(i2);
        }
        this.a[this.b] = (byte) i;
        this.b = i2;
    }

    public final void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i < 0 || i > bArr.length || i2 < 0 || i + i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + bArr.length);
            } else if (i2 != 0) {
                int i3 = this.b + i2;
                if (i3 > this.a.length) {
                    d(i3);
                }
                System.arraycopy(bArr, i, this.a, this.b, i2);
                this.b = i3;
            }
        }
    }

    public final void a(char[] cArr, int i, int i2) {
        if (cArr != null) {
            if (i < 0 || i > cArr.length || i2 < 0 || i + i2 < 0 || i + i2 > cArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + cArr.length);
            } else if (i2 != 0) {
                int i3 = this.b;
                int i4 = i3 + i2;
                if (i4 > this.a.length) {
                    d(i4);
                }
                while (i3 < i4) {
                    this.a[i3] = (byte) cArr[i];
                    i++;
                    i3++;
                }
                this.b = i4;
            }
        }
    }

    public final int b(int i) {
        return this.a[i];
    }

    public final byte[] b() {
        byte[] bArr = new byte[this.b];
        if (this.b > 0) {
            System.arraycopy(this.a, 0, bArr, 0, this.b);
        }
        return bArr;
    }

    public final int c() {
        return this.a.length;
    }

    public final void c(int i) {
        if (i < 0 || i > this.a.length) {
            throw new IndexOutOfBoundsException("len: " + i + " < 0 or > buffer len: " + this.a.length);
        }
        this.b = i;
    }

    public final int d() {
        return this.b;
    }

    public final byte[] e() {
        return this.a;
    }

    public final boolean f() {
        return this.b == 0;
    }

    public final boolean g() {
        return this.b == this.a.length;
    }
}
