package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.c;
import com.agilebinary.a.a.a.d.e;
import com.agilebinary.a.a.a.h.b.b;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class k implements e {
    private final ConcurrentHashMap a = new ConcurrentHashMap();

    private static b a(Map map, c cVar) {
        int i;
        b bVar = (b) map.get(cVar);
        if (bVar != null) {
            return bVar;
        }
        int i2 = -1;
        c cVar2 = null;
        for (c cVar3 : map.keySet()) {
            int a2 = cVar.a(cVar3);
            if (a2 > i2) {
                i = a2;
            } else {
                cVar3 = cVar2;
                i = i2;
            }
            i2 = i;
            cVar2 = cVar3;
        }
        return cVar2 != null ? (b) map.get(cVar2) : bVar;
    }

    public final b a(c cVar) {
        if (cVar != null) {
            return a(this.a, cVar);
        }
        throw new IllegalArgumentException("Authentication scope may not be null");
    }

    public final String toString() {
        return this.a.toString();
    }
}
