package com.agilebinary.a.a.a.c.a;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public final class l {
    private static final String[] a = {"EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE, dd MMM yyyy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy"};
    private static final Date b;
    private static TimeZone c = TimeZone.getTimeZone("GMT");

    static {
        Calendar instance = Calendar.getInstance();
        instance.setTimeZone(c);
        instance.set(2000, 0, 1, 0, 0, 0);
        instance.set(14, 0);
        b = instance.getTime();
    }

    private l() {
    }

    public static Date a(String str, String[] strArr) {
        return a(str, strArr, null);
    }

    private static Date a(String str, String[] strArr, Date date) {
        if (str == null) {
            throw new IllegalArgumentException("dateValue is null");
        }
        if (strArr == null) {
            strArr = a;
        }
        Date date2 = b;
        if (str.length() > 1 && str.startsWith("'") && str.endsWith("'")) {
            str = str.substring(1, str.length() - 1);
        }
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            SimpleDateFormat a2 = ad.a(strArr[i]);
            a2.set2DigitYearStart(date2);
            try {
                return a2.parse(str);
            } catch (ParseException e) {
                i++;
            }
        }
        throw new k("Unable to parse the date " + str);
    }
}
