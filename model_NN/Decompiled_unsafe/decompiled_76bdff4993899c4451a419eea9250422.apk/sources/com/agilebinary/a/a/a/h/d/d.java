package com.agilebinary.a.a.a.h.d;

import com.agilebinary.a.a.a.h.e.a;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

public abstract class d implements b {
    private static final String[] a;

    static {
        String[] strArr = {"ac", "co", "com", "ed", "edu", "go", "gouv", "gov", "info", "lg", "ne", "net", "or", "org"};
        a = strArr;
        Arrays.sort(strArr);
    }

    private static int a(String str) {
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (str.charAt(i2) == '.') {
                i++;
            }
        }
        return i;
    }

    private void a(String str, X509Certificate x509Certificate) {
        String[] strArr;
        LinkedList linkedList = new LinkedList();
        StringTokenizer stringTokenizer = new StringTokenizer(x509Certificate.getSubjectX500Principal().toString(), ",");
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken();
            int indexOf = nextToken.indexOf("CN=");
            if (indexOf >= 0) {
                linkedList.add(nextToken.substring(indexOf + 3));
            }
        }
        if (!linkedList.isEmpty()) {
            strArr = new String[linkedList.size()];
            linkedList.toArray(strArr);
        } else {
            strArr = null;
        }
        a(str, strArr, a(x509Certificate, str));
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00df A[EDGE_INSN: B:61:0x00df->B:49:0x00df ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r9, java.lang.String[] r10, java.lang.String[] r11, boolean r12) {
        /*
            r8 = 46
            r1 = 1
            r2 = 0
            java.util.LinkedList r3 = new java.util.LinkedList
            r3.<init>()
            if (r10 == 0) goto L_0x0017
            int r0 = r10.length
            if (r0 <= 0) goto L_0x0017
            r0 = r10[r2]
            if (r0 == 0) goto L_0x0017
            r0 = r10[r2]
            r3.add(r0)
        L_0x0017:
            if (r11 == 0) goto L_0x0027
            int r4 = r11.length
            r0 = r2
        L_0x001b:
            if (r0 >= r4) goto L_0x0027
            r5 = r11[r0]
            if (r5 == 0) goto L_0x0024
            r3.add(r5)
        L_0x0024:
            int r0 = r0 + 1
            goto L_0x001b
        L_0x0027:
            boolean r0 = r3.isEmpty()
            if (r0 == 0) goto L_0x004c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Certificate for <"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r1 = "> doesn't contain CN or DNS subjectAlt"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            javax.net.ssl.SSLException r1 = new javax.net.ssl.SSLException
            r1.<init>(r0)
            throw r1
        L_0x004c:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r0 = r9.trim()
            java.util.Locale r5 = java.util.Locale.ENGLISH
            java.lang.String r5 = r0.toLowerCase(r5)
            java.util.Iterator r3 = r3.iterator()
            r0 = r2
        L_0x0060:
            boolean r6 = r3.hasNext()
            if (r6 == 0) goto L_0x00df
            java.lang.Object r0 = r3.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.Locale r6 = java.util.Locale.ENGLISH
            java.lang.String r6 = r0.toLowerCase(r6)
            java.lang.String r0 = " <"
            r4.append(r0)
            r4.append(r6)
            r0 = 62
            r4.append(r0)
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x008a
            java.lang.String r0 = " OR"
            r4.append(r0)
        L_0x008a:
            java.lang.String r0 = "*."
            boolean r0 = r6.startsWith(r0)
            if (r0 == 0) goto L_0x0108
            int r0 = r6.lastIndexOf(r8)
            if (r0 < 0) goto L_0x0108
            int r0 = r6.length()
            r7 = 7
            if (r0 < r7) goto L_0x0106
            r7 = 9
            if (r0 > r7) goto L_0x0106
            int r7 = r0 + -3
            char r7 = r6.charAt(r7)
            if (r7 != r8) goto L_0x0106
            r7 = 2
            int r0 = r0 + -3
            java.lang.String r0 = r6.substring(r7, r0)
            java.lang.String[] r7 = com.agilebinary.a.a.a.h.d.d.a
            int r0 = java.util.Arrays.binarySearch(r7, r0)
            if (r0 >= 0) goto L_0x0104
            r0 = r1
        L_0x00bb:
            if (r0 == 0) goto L_0x0108
            boolean r0 = b(r9)
            if (r0 != 0) goto L_0x0108
            r0 = r1
        L_0x00c4:
            if (r0 == 0) goto L_0x010c
            java.lang.String r0 = r6.substring(r1)
            boolean r0 = r5.endsWith(r0)
            if (r0 == 0) goto L_0x00dd
            if (r12 == 0) goto L_0x00dd
            int r0 = a(r5)
            int r6 = a(r6)
            if (r0 != r6) goto L_0x010a
            r0 = r1
        L_0x00dd:
            if (r0 == 0) goto L_0x0060
        L_0x00df:
            if (r0 != 0) goto L_0x0111
            javax.net.ssl.SSLException r0 = new javax.net.ssl.SSLException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "hostname in certificate didn't match: <"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.String r2 = "> !="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0104:
            r0 = r2
            goto L_0x00bb
        L_0x0106:
            r0 = r1
            goto L_0x00bb
        L_0x0108:
            r0 = r2
            goto L_0x00c4
        L_0x010a:
            r0 = r2
            goto L_0x00dd
        L_0x010c:
            boolean r0 = r5.equals(r6)
            goto L_0x00dd
        L_0x0111:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.h.d.d.a(java.lang.String, java.lang.String[], java.lang.String[], boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.security.cert.CertificateParsingException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static String[] a(X509Certificate x509Certificate, String str) {
        Collection<List<?>> collection;
        int i = b(str) ? 7 : 2;
        LinkedList linkedList = new LinkedList();
        try {
            collection = x509Certificate.getSubjectAlternativeNames();
        } catch (CertificateParsingException e) {
            Logger.getLogger(d.class.getName()).log(Level.FINE, "Error parsing certificate.", (Throwable) e);
            collection = null;
        }
        if (collection != null) {
            for (List next : collection) {
                if (((Integer) next.get(0)).intValue() == i) {
                    linkedList.add((String) next.get(1));
                }
            }
        }
        if (linkedList.isEmpty()) {
            return null;
        }
        String[] strArr = new String[linkedList.size()];
        linkedList.toArray(strArr);
        return strArr;
    }

    private static boolean b(String str) {
        return str != null && (a.a(str) || a.b(str));
    }

    public final void a(String str, SSLSocket sSLSocket) {
        if (str == null) {
            throw new NullPointerException("host to verify is null");
        }
        SSLSession session = sSLSocket.getSession();
        if (session == null) {
            sSLSocket.getInputStream().available();
            session = sSLSocket.getSession();
            if (session == null) {
                sSLSocket.startHandshake();
                session = sSLSocket.getSession();
            }
        }
        a(str, (X509Certificate) session.getPeerCertificates()[0]);
    }

    public final boolean verify(String str, SSLSession sSLSession) {
        try {
            a(str, (X509Certificate) sSLSession.getPeerCertificates()[0]);
            return true;
        } catch (SSLException e) {
            return false;
        }
    }
}
