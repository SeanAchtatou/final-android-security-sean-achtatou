package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.f;
import com.agilebinary.a.a.a.b;
import java.util.HashMap;

public final class g implements com.agilebinary.a.a.a.d.g {
    private final HashMap a = new HashMap();

    public final f a(b bVar) {
        if (bVar != null) {
            return (f) this.a.get(bVar);
        }
        throw new IllegalArgumentException("HTTP host may not be null");
    }

    public final void a(b bVar, f fVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP host may not be null");
        }
        this.a.put(bVar, fVar);
    }

    public final void b(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP host may not be null");
        }
        this.a.remove(bVar);
    }

    public final String toString() {
        return this.a.toString();
    }
}
