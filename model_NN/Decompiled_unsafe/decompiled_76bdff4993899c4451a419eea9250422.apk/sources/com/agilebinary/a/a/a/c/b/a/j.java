package com.agilebinary.a.a.a.c.b.a;

import java.util.Date;
import java.util.concurrent.locks.Condition;

public final class j {
    private final Condition a;
    private Thread b;
    private boolean c;

    public j(Condition condition, c cVar) {
        if (condition == null) {
            throw new IllegalArgumentException("Condition must not be null.");
        }
        this.a = condition;
    }

    public final void a() {
        if (this.b == null) {
            throw new IllegalStateException("Nobody waiting on this object.");
        }
        this.a.signalAll();
    }

    public final boolean a(Date date) {
        boolean z;
        if (this.b != null) {
            throw new IllegalStateException("A thread is already waiting on this object.\ncaller: " + Thread.currentThread() + "\nwaiter: " + this.b);
        } else if (this.c) {
            throw new InterruptedException("Operation interrupted");
        } else {
            this.b = Thread.currentThread();
            if (date != null) {
                try {
                    z = this.a.awaitUntil(date);
                } catch (Throwable th) {
                    this.b = null;
                    throw th;
                }
            } else {
                this.a.await();
                z = true;
            }
            if (this.c) {
                throw new InterruptedException("Operation interrupted");
            }
            this.b = null;
            return z;
        }
    }

    public final void b() {
        this.c = true;
        this.a.signalAll();
    }
}
