package com.agilebinary.a.a.a.c.c;

import bsh.ParserConstants;
import com.agilebinary.a.a.a.b.s;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.n;
import com.agilebinary.a.a.a.x;

public final class m extends f {
    private final i b;
    private final b c;

    public m(a aVar, s sVar, i iVar, com.agilebinary.a.a.a.e.b bVar) {
        super(aVar, null, bVar);
        if (iVar == null) {
            throw new IllegalArgumentException("Response factory may not be null");
        }
        this.b = iVar;
        this.c = new b(ParserConstants.LSHIFTASSIGN);
    }

    /* access modifiers changed from: protected */
    public final x a(a aVar) {
        this.c.a();
        if (aVar.a(this.c) == -1) {
            throw new n("The target server failed to respond");
        }
        return this.b.a(this.a.b(this.c, new com.agilebinary.a.a.a.b.i(0, this.c.c())), null);
    }
}
