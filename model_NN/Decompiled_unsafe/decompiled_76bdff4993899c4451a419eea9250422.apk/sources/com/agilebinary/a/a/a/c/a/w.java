package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.e;
import com.agilebinary.a.a.a.i.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.List;

public final class w extends ah {
    private static final String[] a = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"};
    private final String[] b;

    static {
        String[] strArr = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"};
    }

    public w() {
        this(null);
    }

    public w(String[] strArr) {
        if (strArr != null) {
            this.b = (String[]) strArr.clone();
        } else {
            this.b = a;
        }
        a("path", new m());
        a("domain", new r());
        a("max-age", new ac());
        a("secure", new h());
        a("comment", new o());
        a("expires", new j(this.b));
    }

    public final int a() {
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List a(com.agilebinary.a.a.a.t r8, com.agilebinary.a.a.a.k.f r9) {
        /*
            r7 = this;
            r2 = 1
            r5 = -1
            r3 = 0
            if (r8 != 0) goto L_0x000d
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Header may not be null"
            r0.<init>(r1)
            throw r0
        L_0x000d:
            if (r9 != 0) goto L_0x0017
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Cookie origin may not be null"
            r0.<init>(r1)
            throw r0
        L_0x0017:
            java.lang.String r0 = r8.a()
            java.lang.String r1 = r8.b()
            java.lang.String r4 = "Set-Cookie"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x004a
            com.agilebinary.a.a.a.k.e r0 = new com.agilebinary.a.a.a.k.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unrecognized cookie header '"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r8.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "'"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x004a:
            java.util.Locale r0 = java.util.Locale.ENGLISH
            java.lang.String r0 = r1.toLowerCase(r0)
            java.lang.String r4 = "expires="
            int r0 = r0.indexOf(r4)
            if (r0 == r5) goto L_0x00a2
            java.lang.String r4 = "expires="
            int r4 = r4.length()
            int r4 = r4 + r0
            r0 = 59
            int r0 = r1.indexOf(r0, r4)
            if (r0 != r5) goto L_0x006b
            int r0 = r1.length()
        L_0x006b:
            java.lang.String r0 = r1.substring(r4, r0)     // Catch:{ k -> 0x00a1 }
            java.lang.String[] r1 = r7.b     // Catch:{ k -> 0x00a1 }
            com.agilebinary.a.a.a.c.a.l.a(r0, r1)     // Catch:{ k -> 0x00a1 }
            r0 = r2
        L_0x0075:
            if (r0 == 0) goto L_0x00c8
            com.agilebinary.a.a.a.c.a.aa r4 = com.agilebinary.a.a.a.c.a.aa.a
            boolean r0 = r8 instanceof com.agilebinary.a.a.a.r
            if (r0 == 0) goto L_0x00a4
            r0 = r8
            com.agilebinary.a.a.a.r r0 = (com.agilebinary.a.a.a.r) r0
            com.agilebinary.a.a.a.i.b r0 = r0.e()
            com.agilebinary.a.a.a.b.i r1 = new com.agilebinary.a.a.a.b.i
            com.agilebinary.a.a.a.r r8 = (com.agilebinary.a.a.a.r) r8
            int r5 = r8.d()
            int r6 = r0.c()
            r1.<init>(r5, r6)
        L_0x0093:
            com.agilebinary.a.a.a.m[] r2 = new com.agilebinary.a.a.a.m[r2]
            com.agilebinary.a.a.a.m r0 = r4.a(r0, r1)
            r2[r3] = r0
            r0 = r2
        L_0x009c:
            java.util.List r0 = r7.a(r0, r9)
            return r0
        L_0x00a1:
            r0 = move-exception
        L_0x00a2:
            r0 = r3
            goto L_0x0075
        L_0x00a4:
            java.lang.String r1 = r8.b()
            if (r1 != 0) goto L_0x00b2
            com.agilebinary.a.a.a.k.e r0 = new com.agilebinary.a.a.a.k.e
            java.lang.String r1 = "Header value is null"
            r0.<init>(r1)
            throw r0
        L_0x00b2:
            com.agilebinary.a.a.a.i.b r0 = new com.agilebinary.a.a.a.i.b
            int r5 = r1.length()
            r0.<init>(r5)
            r0.a(r1)
            com.agilebinary.a.a.a.b.i r1 = new com.agilebinary.a.a.a.b.i
            int r5 = r0.c()
            r1.<init>(r3, r5)
            goto L_0x0093
        L_0x00c8:
            com.agilebinary.a.a.a.m[] r0 = r8.c()
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.a.w.a(com.agilebinary.a.a.a.t, com.agilebinary.a.a.a.k.f):java.util.List");
    }

    public final List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException("List of cookies may not be null");
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException("List of cookies may not be empty");
        } else {
            b bVar = new b(list.size() * 20);
            bVar.a("Cookie");
            bVar.a(": ");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    c cVar = (c) list.get(i2);
                    if (i2 > 0) {
                        bVar.a("; ");
                    }
                    bVar.a(cVar.a());
                    bVar.a("=");
                    String b2 = cVar.b();
                    if (b2 != null) {
                        bVar.a(b2);
                    }
                    i = i2 + 1;
                } else {
                    ArrayList arrayList = new ArrayList(1);
                    arrayList.add(new e(bVar));
                    return arrayList;
                }
            }
        }
    }

    public final t b() {
        return null;
    }

    public final String toString() {
        return "compatibility";
    }
}
