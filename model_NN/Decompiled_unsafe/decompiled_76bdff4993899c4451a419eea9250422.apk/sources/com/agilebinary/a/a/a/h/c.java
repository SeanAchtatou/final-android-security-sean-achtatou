package com.agilebinary.a.a.a.h;

import java.io.IOException;
import java.io.InputStream;

public final class c extends InputStream implements d {
    private InputStream a;
    private boolean b;
    private final e c;

    public c(InputStream inputStream, e eVar) {
        if (inputStream == null) {
            throw new IllegalArgumentException("Wrapped stream may not be null.");
        }
        this.a = inputStream;
        this.b = false;
        this.c = eVar;
    }

    private void a(int i) {
        if (this.a != null && i < 0) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.a(this.a);
                }
                if (z) {
                    this.a.close();
                }
            } finally {
                this.a = null;
            }
        }
    }

    private boolean c() {
        if (!this.b) {
            return this.a != null;
        }
        throw new IOException("Attempted read on closed stream.");
    }

    private void d() {
        if (this.a != null) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.o_();
                }
                if (z) {
                    this.a.close();
                }
            } finally {
                this.a = null;
            }
        }
    }

    public final int available() {
        if (!c()) {
            return 0;
        }
        try {
            return this.a.available();
        } catch (IOException e) {
            d();
            throw e;
        }
    }

    public final void b() {
        this.b = true;
        d();
    }

    public final void b_() {
        close();
    }

    public final void close() {
        boolean z = true;
        this.b = true;
        if (this.a != null) {
            try {
                if (this.c != null) {
                    z = this.c.b(this.a);
                }
                if (z) {
                    this.a.close();
                }
            } finally {
                this.a = null;
            }
        }
    }

    public final int read() {
        if (!c()) {
            return -1;
        }
        try {
            int read = this.a.read();
            a(read);
            return read;
        } catch (IOException e) {
            d();
            throw e;
        }
    }

    public final int read(byte[] bArr) {
        if (!c()) {
            return -1;
        }
        try {
            int read = this.a.read(bArr);
            a(read);
            return read;
        } catch (IOException e) {
            d();
            throw e;
        }
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (!c()) {
            return -1;
        }
        try {
            int read = this.a.read(bArr, i, i2);
            a(read);
            return read;
        } catch (IOException e) {
            d();
            throw e;
        }
    }
}
