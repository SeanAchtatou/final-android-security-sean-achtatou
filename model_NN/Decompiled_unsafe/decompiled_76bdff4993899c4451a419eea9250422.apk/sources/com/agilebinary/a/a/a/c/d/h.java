package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.p;
import com.agilebinary.a.a.a.t;

public final class h extends i implements p {
    private c c;
    /* access modifiers changed from: private */
    public boolean d;

    public h(p pVar) {
        super(pVar);
        c h = pVar.h();
        this.c = h != null ? new e(this, h) : null;
        this.d = false;
    }

    public final boolean f_() {
        t c2 = c("Expect");
        return c2 != null && "100-continue".equalsIgnoreCase(c2.b());
    }

    public final c h() {
        return this.c;
    }

    public final boolean i() {
        return this.c == null || this.c.a() || !this.d;
    }
}
