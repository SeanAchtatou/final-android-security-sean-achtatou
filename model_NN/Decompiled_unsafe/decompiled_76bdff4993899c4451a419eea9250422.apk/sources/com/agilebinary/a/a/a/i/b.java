package com.agilebinary.a.a.a.i;

import com.agilebinary.a.a.a.c.b.a.c;
import java.io.Serializable;

public final class b implements Serializable {
    private char[] a;
    private int b;

    public b(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Buffer capacity may not be negative");
        }
        this.a = new char[i];
    }

    private void d(int i) {
        char[] cArr = new char[Math.max(this.a.length << 1, i)];
        System.arraycopy(this.a, 0, cArr, 0, this.b);
        this.a = cArr;
    }

    public final char a(int i) {
        return this.a[i];
    }

    public final int a(int i, int i2, int i3) {
        if (i2 < 0) {
            i2 = 0;
        }
        if (i3 > this.b) {
            i3 = this.b;
        }
        if (i2 > i3) {
            return -1;
        }
        for (int i4 = i2; i4 < i3; i4++) {
            if (this.a[i4] == i) {
                return i4;
            }
        }
        return -1;
    }

    public final String a(int i, int i2) {
        return new String(this.a, i, i2 - i);
    }

    public final void a() {
        this.b = 0;
    }

    public final void a(char c) {
        int i = this.b + 1;
        if (i > this.a.length) {
            d(i);
        }
        this.a[this.b] = c;
        this.b = i;
    }

    public final void a(b bVar, int i, int i2) {
        char[] cArr;
        if (bVar != null && (cArr = bVar.a) != null) {
            if (i < 0 || i > cArr.length || i2 < 0 || i + i2 < 0 || i + i2 > cArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + cArr.length);
            } else if (i2 != 0) {
                int i3 = this.b + i2;
                if (i3 > this.a.length) {
                    d(i3);
                }
                System.arraycopy(cArr, i, this.a, this.b, i2);
                this.b = i3;
            }
        }
    }

    public final void a(String str) {
        if (str == null) {
            str = "null";
        }
        int length = str.length();
        int i = this.b + length;
        if (i > this.a.length) {
            d(i);
        }
        str.getChars(0, length, this.a, this.b);
        this.b = i;
    }

    public final void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i < 0 || i > bArr.length || i2 < 0 || i + i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + bArr.length);
            } else if (i2 != 0) {
                int i3 = this.b;
                int i4 = i3 + i2;
                if (i4 > this.a.length) {
                    d(i4);
                }
                while (i3 < i4) {
                    this.a[i3] = (char) (bArr[i] & 255);
                    i++;
                    i3++;
                }
                this.b = i4;
            }
        }
    }

    public final String b(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Negative beginIndex: " + i);
        } else if (i2 > this.b) {
            throw new IndexOutOfBoundsException("endIndex: " + i2 + " > length: " + this.b);
        } else if (i > i2) {
            throw new IndexOutOfBoundsException("beginIndex: " + i + " > endIndex: " + i2);
        } else {
            while (i < i2 && c.a(this.a[i])) {
                i++;
            }
            while (i2 > i && c.a(this.a[i2 - 1])) {
                i2--;
            }
            return new String(this.a, i, i2 - i);
        }
    }

    public final void b(int i) {
        if (i > 0 && i > this.a.length - this.b) {
            d(this.b + i);
        }
    }

    public final char[] b() {
        return this.a;
    }

    public final int c() {
        return this.b;
    }

    public final int c(int i) {
        return a(i, 0, this.b);
    }

    public final boolean d() {
        return this.b == 0;
    }

    public final String toString() {
        return new String(this.a, 0, this.b);
    }
}
