package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.k.g;
import com.agilebinary.a.a.a.k.j;
import java.util.Collection;

public final class s implements g {
    public final j a(b bVar) {
        if (bVar == null) {
            return new q();
        }
        Collection collection = (Collection) bVar.a("http.protocol.cookie-datepatterns");
        return new q(collection != null ? (String[]) collection.toArray(new String[collection.size()]) : null);
    }
}
