package com.agilebinary.a.a.a.c;

import com.agilebinary.a.a.a.ae;
import com.agilebinary.a.a.a.c.c.c;
import com.agilebinary.a.a.a.c.c.h;
import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.j.a;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

public class e extends a implements ae {
    private volatile boolean a;
    private volatile Socket b = null;

    /* access modifiers changed from: protected */
    public a a(Socket socket, int i, b bVar) {
        return new c(socket, i, bVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (!this.a) {
            throw new IllegalStateException("Connection is not open");
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Socket socket, b bVar) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        } else if (bVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.b = socket;
            if (bVar == null) {
                throw new IllegalArgumentException("HTTP parameters may not be null");
            }
            int a2 = bVar.a("http.socket.buffer-size", -1);
            a(a(socket, a2, bVar), b(socket, a2, bVar), bVar);
            this.a = true;
        }
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.a.j.e b(Socket socket, int i, b bVar) {
        return new h(socket, i, bVar);
    }

    public final void b(int i) {
        a();
        if (this.b != null) {
            try {
                this.b.setSoTimeout(i);
            } catch (SocketException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (this.a) {
            throw new IllegalStateException("Connection is already open");
        }
    }

    /* access modifiers changed from: protected */
    public Socket i_() {
        return this.b;
    }

    public void k() {
        if (this.a) {
            this.a = false;
            Socket socket = this.b;
            try {
                b();
                try {
                    socket.shutdownOutput();
                } catch (IOException e) {
                }
                try {
                    socket.shutdownInput();
                } catch (IOException | UnsupportedOperationException e2) {
                }
            } finally {
                socket.close();
            }
        }
    }

    public final boolean l() {
        return this.a;
    }

    public void m() {
        this.a = false;
        Socket socket = this.b;
        if (socket != null) {
            socket.close();
        }
    }

    public final InetAddress p() {
        if (this.b != null) {
            return this.b.getInetAddress();
        }
        return null;
    }

    public final int q() {
        if (this.b != null) {
            return this.b.getPort();
        }
        return -1;
    }
}
