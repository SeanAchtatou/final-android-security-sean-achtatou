package com.a.a;

final class f {
    f() {
    }

    static long a(long j, byte[] bArr, int i, int i2) {
        int i3;
        if (bArr == null) {
            return 1;
        }
        long j2 = j & 65535;
        long j3 = (j >> 16) & 65535;
        int i4 = i2;
        while (i4 > 0) {
            int i5 = i4 < 5552 ? i4 : 5552;
            int i6 = i4 - i5;
            long j4 = j3;
            long j5 = j2;
            int i7 = i;
            int i8 = i5;
            long j6 = j4;
            while (i8 >= 16) {
                int i9 = i7 + 1;
                long j7 = j5 + ((long) (bArr[i7] & 255));
                long j8 = j6 + j7;
                int i10 = i9 + 1;
                long j9 = j7 + ((long) (bArr[i9] & 255));
                long j10 = j8 + j9;
                int i11 = i10 + 1;
                long j11 = j9 + ((long) (bArr[i10] & 255));
                long j12 = j10 + j11;
                int i12 = i11 + 1;
                long j13 = j11 + ((long) (bArr[i11] & 255));
                long j14 = j12 + j13;
                int i13 = i12 + 1;
                long j15 = j13 + ((long) (bArr[i12] & 255));
                long j16 = j14 + j15;
                int i14 = i13 + 1;
                long j17 = j15 + ((long) (bArr[i13] & 255));
                long j18 = j16 + j17;
                int i15 = i14 + 1;
                long j19 = j17 + ((long) (bArr[i14] & 255));
                long j20 = j18 + j19;
                int i16 = i15 + 1;
                long j21 = j19 + ((long) (bArr[i15] & 255));
                long j22 = j20 + j21;
                int i17 = i16 + 1;
                long j23 = j21 + ((long) (bArr[i16] & 255));
                long j24 = j22 + j23;
                int i18 = i17 + 1;
                long j25 = j23 + ((long) (bArr[i17] & 255));
                long j26 = j24 + j25;
                int i19 = i18 + 1;
                long j27 = j25 + ((long) (bArr[i18] & 255));
                long j28 = j26 + j27;
                int i20 = i19 + 1;
                long j29 = j27 + ((long) (bArr[i19] & 255));
                long j30 = j28 + j29;
                int i21 = i20 + 1;
                long j31 = j29 + ((long) (bArr[i20] & 255));
                long j32 = j30 + j31;
                int i22 = i21 + 1;
                long j33 = j31 + ((long) (bArr[i21] & 255));
                long j34 = j32 + j33;
                int i23 = i22 + 1;
                long j35 = j33 + ((long) (bArr[i22] & 255));
                long j36 = j34 + j35;
                i7 = i23 + 1;
                j5 = j35 + ((long) (bArr[i23] & 255));
                j6 = j36 + j5;
                i8 -= 16;
            }
            if (i8 != 0) {
                int i24 = i8;
                while (true) {
                    i3 = i7 + 1;
                    j5 += (long) (bArr[i7] & 255);
                    j6 += j5;
                    int i25 = i24 - 1;
                    if (i25 == 0) {
                        break;
                    }
                    i24 = i25;
                    i7 = i3;
                }
            } else {
                i3 = i7;
            }
            i = i3;
            j2 = j5 % 65521;
            j3 = j6 % 65521;
            i4 = i6;
        }
        return (j3 << 16) | j2;
    }
}
