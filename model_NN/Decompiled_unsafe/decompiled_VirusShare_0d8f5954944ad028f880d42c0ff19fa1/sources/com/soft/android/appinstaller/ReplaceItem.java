package com.soft.android.appinstaller;

/* compiled from: GlobalConfig */
class ReplaceItem {
    private String from;
    private String to;

    public ReplaceItem(String from2, String to2) {
        setFrom(from2);
        setTo(to2);
    }

    public String getFrom() {
        return this.from;
    }

    public void setFrom(String from2) {
        this.from = from2;
    }

    public String getTo() {
        return this.to;
    }

    public void setTo(String to2) {
        this.to = to2;
    }
}
