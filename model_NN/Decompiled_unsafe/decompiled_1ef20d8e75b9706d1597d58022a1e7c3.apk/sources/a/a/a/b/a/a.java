package a.a.a.b.a;

import a.a.a.n;
import java.net.InetAddress;
import java.util.Collection;

public class a implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    public static final a f12a = new b().a();
    private final boolean b;
    private final n c;
    private final InetAddress d;
    private final boolean e;
    private final String f;
    private final boolean g;
    private final boolean h;
    private final boolean i;
    private final int j;
    private final boolean k;
    private final Collection l;
    private final Collection m;
    private final int n;
    private final int o;
    private final int p;
    private final boolean q;

    a(boolean z, n nVar, InetAddress inetAddress, boolean z2, String str, boolean z3, boolean z4, boolean z5, int i2, boolean z6, Collection collection, Collection collection2, int i3, int i4, int i5, boolean z7) {
        this.b = z;
        this.c = nVar;
        this.d = inetAddress;
        this.e = z2;
        this.f = str;
        this.g = z3;
        this.h = z4;
        this.i = z5;
        this.j = i2;
        this.k = z6;
        this.l = collection;
        this.m = collection2;
        this.n = i3;
        this.o = i4;
        this.p = i5;
        this.q = z7;
    }

    public static b g() {
        return new b();
    }

    public String a() {
        return this.f;
    }

    public boolean b() {
        return this.h;
    }

    public boolean c() {
        return this.i;
    }

    public Collection d() {
        return this.l;
    }

    public Collection e() {
        return this.m;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public a clone() {
        return (a) super.clone();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append("expectContinueEnabled=").append(this.b);
        sb.append(", proxy=").append(this.c);
        sb.append(", localAddress=").append(this.d);
        sb.append(", cookieSpec=").append(this.f);
        sb.append(", redirectsEnabled=").append(this.g);
        sb.append(", relativeRedirectsAllowed=").append(this.h);
        sb.append(", maxRedirects=").append(this.j);
        sb.append(", circularRedirectsAllowed=").append(this.i);
        sb.append(", authenticationEnabled=").append(this.k);
        sb.append(", targetPreferredAuthSchemes=").append(this.l);
        sb.append(", proxyPreferredAuthSchemes=").append(this.m);
        sb.append(", connectionRequestTimeout=").append(this.n);
        sb.append(", connectTimeout=").append(this.o);
        sb.append(", socketTimeout=").append(this.p);
        sb.append(", decompressionEnabled=").append(this.q);
        sb.append("]");
        return sb.toString();
    }
}
