package a.a.a.b.e;

import a.a.a.m.e;
import a.a.a.n.a;
import a.a.a.q;
import a.a.a.r;
import java.util.Collection;

public class g implements r {

    /* renamed from: a  reason: collision with root package name */
    private final Collection f22a;

    public g() {
        this(null);
    }

    public g(Collection collection) {
        this.f22a = collection;
    }

    public void a(q qVar, e eVar) {
        a.a(qVar, "HTTP request");
        if (!qVar.g().a().equalsIgnoreCase("CONNECT")) {
            Collection<a.a.a.e> collection = (Collection) qVar.f().a("http.default-headers");
            if (collection == null) {
                collection = this.f22a;
            }
            if (collection != null) {
                for (a.a.a.e a2 : collection) {
                    qVar.a(a2);
                }
            }
        }
    }
}
