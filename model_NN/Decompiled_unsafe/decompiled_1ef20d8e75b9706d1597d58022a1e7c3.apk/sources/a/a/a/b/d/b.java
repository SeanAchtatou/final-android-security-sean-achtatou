package a.a.a.b.d;

import a.a.a.k.c;
import a.a.a.k.e;
import a.a.a.n.a;

@Deprecated
public class b {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.k.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      a.a.a.k.e.a(java.lang.String, int):int
      a.a.a.k.e.a(java.lang.String, long):long
      a.a.a.k.e.a(java.lang.String, java.lang.Object):a.a.a.k.e
      a.a.a.k.e.a(java.lang.String, boolean):boolean */
    public static boolean a(e eVar) {
        a.a(eVar, "HTTP parameters");
        return eVar.a("http.protocol.handle-redirects", true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.k.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      a.a.a.k.e.a(java.lang.String, int):int
      a.a.a.k.e.a(java.lang.String, long):long
      a.a.a.k.e.a(java.lang.String, java.lang.Object):a.a.a.k.e
      a.a.a.k.e.a(java.lang.String, boolean):boolean */
    public static boolean b(e eVar) {
        a.a(eVar, "HTTP parameters");
        return eVar.a("http.protocol.handle-authentication", true);
    }

    public static long c(e eVar) {
        a.a(eVar, "HTTP parameters");
        Long l = (Long) eVar.a("http.conn-manager.timeout");
        return l != null ? l.longValue() : (long) c.e(eVar);
    }
}
