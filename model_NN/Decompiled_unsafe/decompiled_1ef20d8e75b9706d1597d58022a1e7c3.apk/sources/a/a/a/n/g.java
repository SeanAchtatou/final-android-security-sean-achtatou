package a.a.a.n;

import a.a.a.k;
import java.io.InputStream;

public final class g {
    /* JADX WARN: Failed to insert an additional move for type inference into block B:15:0x0032 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:17:0x0035 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.nio.charset.Charset} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: java.nio.charset.Charset} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v8, resolved type: java.nio.charset.Charset} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: java.nio.charset.Charset} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v10, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v12, resolved type: java.nio.charset.Charset} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(a.a.a.k r8, java.nio.charset.Charset r9) {
        /*
            r1 = 0
            r0 = 0
            java.lang.String r2 = "Entity"
            a.a.a.n.a.a(r8, r2)
            java.io.InputStream r3 = r8.f()
            if (r3 != 0) goto L_0x000e
        L_0x000d:
            return r1
        L_0x000e:
            long r4 = r8.c()     // Catch:{ all -> 0x0053 }
            r6 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r2 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r2 > 0) goto L_0x001a
            r0 = 1
        L_0x001a:
            java.lang.String r2 = "HTTP entity too large to be buffered in memory"
            a.a.a.n.a.a(r0, r2)     // Catch:{ all -> 0x0053 }
            long r4 = r8.c()     // Catch:{ all -> 0x0053 }
            int r0 = (int) r4
            if (r0 >= 0) goto L_0x0028
            r0 = 4096(0x1000, float:5.74E-42)
        L_0x0028:
            a.a.a.g.e r2 = a.a.a.g.e.a(r8)     // Catch:{ UnsupportedCharsetException -> 0x0058 }
            if (r2 == 0) goto L_0x0032
            java.nio.charset.Charset r1 = r2.b()     // Catch:{ UnsupportedCharsetException -> 0x0058 }
        L_0x0032:
            if (r1 != 0) goto L_0x0035
            r1 = r9
        L_0x0035:
            if (r1 != 0) goto L_0x0039
            java.nio.charset.Charset r1 = a.a.a.m.d.f185a     // Catch:{ all -> 0x0053 }
        L_0x0039:
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ all -> 0x0053 }
            r2.<init>(r3, r1)     // Catch:{ all -> 0x0053 }
            a.a.a.n.d r1 = new a.a.a.n.d     // Catch:{ all -> 0x0053 }
            r1.<init>(r0)     // Catch:{ all -> 0x0053 }
            r0 = 1024(0x400, float:1.435E-42)
            char[] r0 = new char[r0]     // Catch:{ all -> 0x0053 }
        L_0x0047:
            int r4 = r2.read(r0)     // Catch:{ all -> 0x0053 }
            r5 = -1
            if (r4 == r5) goto L_0x0065
            r5 = 0
            r1.a(r0, r5, r4)     // Catch:{ all -> 0x0053 }
            goto L_0x0047
        L_0x0053:
            r0 = move-exception
            r3.close()
            throw r0
        L_0x0058:
            r2 = move-exception
            if (r9 != 0) goto L_0x0032
            java.io.UnsupportedEncodingException r0 = new java.io.UnsupportedEncodingException     // Catch:{ all -> 0x0053 }
            java.lang.String r1 = r2.getMessage()     // Catch:{ all -> 0x0053 }
            r0.<init>(r1)     // Catch:{ all -> 0x0053 }
            throw r0     // Catch:{ all -> 0x0053 }
        L_0x0065:
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0053 }
            r3.close()
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.n.g.a(a.a.a.k, java.nio.charset.Charset):java.lang.String");
    }

    public static void a(k kVar) {
        InputStream f;
        if (kVar != null && kVar.g() && (f = kVar.f()) != null) {
            f.close();
        }
    }

    public static byte[] b(k kVar) {
        int i = 4096;
        boolean z = false;
        a.a(kVar, "Entity");
        InputStream f = kVar.f();
        if (f == null) {
            return null;
        }
        try {
            if (kVar.c() <= 2147483647L) {
                z = true;
            }
            a.a(z, "HTTP entity too large to be buffered in memory");
            int c = (int) kVar.c();
            if (c >= 0) {
                i = c;
            }
            c cVar = new c(i);
            byte[] bArr = new byte[4096];
            while (true) {
                int read = f.read(bArr);
                if (read == -1) {
                    return cVar.b();
                }
                cVar.a(bArr, 0, read);
            }
        } finally {
            f.close();
        }
    }

    public static String c(k kVar) {
        return a(kVar, null);
    }
}
