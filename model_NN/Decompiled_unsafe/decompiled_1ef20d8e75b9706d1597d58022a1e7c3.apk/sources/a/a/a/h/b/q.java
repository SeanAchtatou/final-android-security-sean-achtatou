package a.a.a.h.b;

import a.a.a.e;
import a.a.a.k;
import a.a.a.l;

@Deprecated
public class q extends v implements l {
    private k c;
    /* access modifiers changed from: private */
    public boolean d;

    public q(l lVar) {
        super(lVar);
        a(lVar.b());
    }

    public void a(k kVar) {
        this.c = kVar != null ? new r(this, kVar) : null;
        this.d = false;
    }

    public boolean a() {
        e c2 = c("Expect");
        return c2 != null && "100-continue".equalsIgnoreCase(c2.d());
    }

    public k b() {
        return this.c;
    }

    public boolean j() {
        return this.c == null || this.c.a() || !this.d;
    }
}
