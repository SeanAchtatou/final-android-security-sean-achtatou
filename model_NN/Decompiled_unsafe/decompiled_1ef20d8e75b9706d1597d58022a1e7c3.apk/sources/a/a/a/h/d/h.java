package a.a.a.h.d;

import a.a.a.f.b;
import a.a.a.f.n;
import a.a.a.f.o;
import a.a.a.n.a;
import java.util.Date;

public class h extends a implements b {
    public String a() {
        return "max-age";
    }

    public void a(o oVar, String str) {
        a.a(oVar, "Cookie");
        if (str == null) {
            throw new n("Missing value for 'max-age' attribute");
        }
        try {
            int parseInt = Integer.parseInt(str);
            if (parseInt < 0) {
                throw new n("Negative 'max-age' attribute: " + str);
            }
            oVar.b(new Date(System.currentTimeMillis() + (((long) parseInt) * 1000)));
        } catch (NumberFormatException e) {
            throw new n("Invalid 'max-age' attribute: " + str);
        }
    }
}
