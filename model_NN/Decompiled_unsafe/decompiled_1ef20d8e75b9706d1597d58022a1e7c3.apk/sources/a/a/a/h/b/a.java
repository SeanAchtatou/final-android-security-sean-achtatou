package a.a.a.h.b;

import a.a.a.a.f;
import a.a.a.b.c;
import a.a.a.b.k;
import a.a.a.b.o;
import a.a.a.b.p;
import a.a.a.b.q;
import a.a.a.e.b;
import a.a.a.e.b.d;
import a.a.a.e.g;
import a.a.a.f.l;
import a.a.a.h.a.ac;
import a.a.a.h.a.z;
import a.a.a.h.d.ab;
import a.a.a.h.d.ai;
import a.a.a.h.d.y;
import a.a.a.k.e;
import a.a.a.m;
import a.a.a.m.h;
import a.a.a.m.i;
import a.a.a.n;
import a.a.a.r;
import a.a.a.u;
import java.io.IOException;
import java.lang.reflect.UndeclaredThrowableException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Deprecated
public abstract class a extends h {

    /* renamed from: a  reason: collision with root package name */
    private final Log f95a = LogFactory.getLog(getClass());
    private e b;
    private h c;
    private b d;
    private a.a.a.b e;
    private g f;
    private l g;
    private f h;
    private a.a.a.m.b i;
    private i j;
    private k k;
    private o l;
    private c m;
    private c n;
    private a.a.a.b.h o;
    private a.a.a.b.i p;
    private d q;
    private q r;
    private a.a.a.b.g s;
    private a.a.a.b.d t;

    protected a(b bVar, e eVar) {
        this.b = eVar;
        this.d = bVar;
    }

    private synchronized a.a.a.m.g I() {
        i iVar;
        synchronized (this) {
            if (this.j == null) {
                a.a.a.m.b H = H();
                int a2 = H.a();
                r[] rVarArr = new r[a2];
                for (int i2 = 0; i2 < a2; i2++) {
                    rVarArr[i2] = H.a(i2);
                }
                int b2 = H.b();
                u[] uVarArr = new u[b2];
                for (int i3 = 0; i3 < b2; i3++) {
                    uVarArr[i3] = H.b(i3);
                }
                this.j = new i(rVarArr, uVarArr);
            }
            iVar = this.j;
        }
        return iVar;
    }

    public final synchronized o A() {
        if (this.l == null) {
            this.l = new m();
        }
        return this.l;
    }

    public final synchronized c B() {
        if (this.m == null) {
            this.m = l();
        }
        return this.m;
    }

    public final synchronized c C() {
        if (this.n == null) {
            this.n = m();
        }
        return this.n;
    }

    public final synchronized a.a.a.b.h D() {
        if (this.o == null) {
            this.o = n();
        }
        return this.o;
    }

    public final synchronized a.a.a.b.i E() {
        if (this.p == null) {
            this.p = o();
        }
        return this.p;
    }

    public final synchronized d F() {
        if (this.q == null) {
            this.q = p();
        }
        return this.q;
    }

    public final synchronized q G() {
        if (this.r == null) {
            this.r = q();
        }
        return this.r;
    }

    /* access modifiers changed from: protected */
    public final synchronized a.a.a.m.b H() {
        if (this.i == null) {
            this.i = c();
        }
        return this.i;
    }

    /* access modifiers changed from: protected */
    public final a.a.a.b.c.e a(n nVar, a.a.a.q qVar, a.a.a.m.e eVar) {
        a.a.a.m.c cVar;
        p a2;
        d F;
        a.a.a.b.g u;
        a.a.a.b.d w;
        a.a.a.e.b.b a3;
        a.a.a.n.a.a(qVar, "HTTP request");
        synchronized (this) {
            a.a.a.m.e d2 = d();
            cVar = eVar == null ? d2 : new a.a.a.m.c(eVar, d2);
            e a4 = a(qVar);
            cVar.a("http.request-config", a.a.a.b.d.a.a(a4));
            a2 = a(s(), r(), x(), y(), F(), I(), z(), A(), B(), C(), G(), a4);
            F = F();
            u = u();
            w = w();
        }
        if (u == null || w == null) {
            return i.a(a2.a(nVar, qVar, cVar));
        }
        try {
            a3 = F.a(nVar != null ? nVar : (n) a(qVar).a("http.default-host"), qVar, cVar);
            a.a.a.b.c.e a5 = i.a(a2.a(nVar, qVar, cVar));
            if (u.a(a5)) {
                w.a(a3);
                return a5;
            }
            w.b(a3);
            return a5;
        } catch (RuntimeException e2) {
            if (u.a(e2)) {
                w.a(a3);
            }
            throw e2;
        } catch (Exception e3) {
            if (u.a(e3)) {
                w.a(a3);
            }
            if (e3 instanceof m) {
                throw ((m) e3);
            } else if (e3 instanceof IOException) {
                throw ((IOException) e3);
            } else {
                throw new UndeclaredThrowableException(e3);
            }
        } catch (m e4) {
            throw new a.a.a.b.f(e4);
        }
    }

    /* access modifiers changed from: protected */
    public p a(h hVar, b bVar, a.a.a.b bVar2, g gVar, d dVar, a.a.a.m.g gVar2, k kVar, o oVar, c cVar, c cVar2, q qVar, e eVar) {
        return new o(this.f95a, hVar, bVar, bVar2, gVar, dVar, gVar2, kVar, oVar, cVar, cVar2, qVar, eVar);
    }

    public final synchronized e a() {
        if (this.b == null) {
            this.b = b();
        }
        return this.b;
    }

    /* access modifiers changed from: protected */
    public e a(a.a.a.q qVar) {
        return new g(null, a(), qVar.f(), null);
    }

    /* access modifiers changed from: protected */
    public abstract e b();

    /* access modifiers changed from: protected */
    public abstract a.a.a.m.b c();

    public void close() {
        r().b();
    }

    /* access modifiers changed from: protected */
    public a.a.a.m.e d() {
        a.a.a.m.a aVar = new a.a.a.m.a();
        aVar.a("http.scheme-registry", r().a());
        aVar.a("http.authscheme-registry", t());
        aVar.a("http.cookiespec-registry", v());
        aVar.a("http.cookie-store", D());
        aVar.a("http.auth.credentials-provider", E());
        return aVar;
    }

    /* JADX WARN: Type inference failed for: r2v6, types: [java.lang.Object] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a.a.a.e.b e() {
        /*
            r5 = this;
            a.a.a.e.c.g r3 = a.a.a.h.c.m.a()
            a.a.a.k.e r4 = r5.a()
            r2 = 0
            java.lang.String r1 = "http.connection-manager.factory-class-name"
            java.lang.Object r1 = r4.a(r1)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x005c
            java.lang.Class r2 = java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException -> 0x0026, IllegalAccessException -> 0x0040, InstantiationException -> 0x004b }
            java.lang.Object r2 = r2.newInstance()     // Catch:{ ClassNotFoundException -> 0x0026, IllegalAccessException -> 0x0040, InstantiationException -> 0x004b }
            r0 = r2
            a.a.a.e.c r0 = (a.a.a.e.c) r0     // Catch:{ ClassNotFoundException -> 0x0026, IllegalAccessException -> 0x0040, InstantiationException -> 0x004b }
            r1 = r0
        L_0x001f:
            if (r1 == 0) goto L_0x0056
            a.a.a.e.b r1 = r1.a(r4, r3)
        L_0x0025:
            return r1
        L_0x0026:
            r2 = move-exception
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Invalid class name: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.<init>(r1)
            throw r2
        L_0x0040:
            r1 = move-exception
            java.lang.IllegalAccessError r2 = new java.lang.IllegalAccessError
            java.lang.String r1 = r1.getMessage()
            r2.<init>(r1)
            throw r2
        L_0x004b:
            r1 = move-exception
            java.lang.InstantiationError r2 = new java.lang.InstantiationError
            java.lang.String r1 = r1.getMessage()
            r2.<init>(r1)
            throw r2
        L_0x0056:
            a.a.a.h.c.a r1 = new a.a.a.h.c.a
            r1.<init>(r3)
            goto L_0x0025
        L_0x005c:
            r1 = r2
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.h.b.a.e():a.a.a.e.b");
    }

    /* access modifiers changed from: protected */
    public f f() {
        f fVar = new f();
        fVar.a("Basic", new a.a.a.h.a.c());
        fVar.a("Digest", new a.a.a.h.a.e());
        fVar.a("NTLM", new z());
        fVar.a("Negotiate", new ac());
        fVar.a("Kerberos", new a.a.a.h.a.m());
        return fVar;
    }

    /* access modifiers changed from: protected */
    public l g() {
        l lVar = new l();
        lVar.a("default", new a.a.a.h.d.l());
        lVar.a("best-match", new a.a.a.h.d.l());
        lVar.a("compatibility", new a.a.a.h.d.o());
        lVar.a("netscape", new y());
        lVar.a("rfc2109", new ab());
        lVar.a("rfc2965", new ai());
        lVar.a("ignoreCookies", new a.a.a.h.d.u());
        return lVar;
    }

    /* access modifiers changed from: protected */
    public h h() {
        return new h();
    }

    /* access modifiers changed from: protected */
    public a.a.a.b i() {
        return new a.a.a.h.b();
    }

    /* access modifiers changed from: protected */
    public g j() {
        return new j();
    }

    /* access modifiers changed from: protected */
    public k k() {
        return new l();
    }

    /* access modifiers changed from: protected */
    public c l() {
        return new x();
    }

    /* access modifiers changed from: protected */
    public c m() {
        return new t();
    }

    /* access modifiers changed from: protected */
    public a.a.a.b.h n() {
        return new e();
    }

    /* access modifiers changed from: protected */
    public a.a.a.b.i o() {
        return new f();
    }

    /* access modifiers changed from: protected */
    public d p() {
        return new a.a.a.h.c.g(r().a());
    }

    /* access modifiers changed from: protected */
    public q q() {
        return new p();
    }

    public final synchronized b r() {
        if (this.d == null) {
            this.d = e();
        }
        return this.d;
    }

    public final synchronized h s() {
        if (this.c == null) {
            this.c = h();
        }
        return this.c;
    }

    public final synchronized f t() {
        if (this.h == null) {
            this.h = f();
        }
        return this.h;
    }

    public final synchronized a.a.a.b.g u() {
        return this.s;
    }

    public final synchronized l v() {
        if (this.g == null) {
            this.g = g();
        }
        return this.g;
    }

    public final synchronized a.a.a.b.d w() {
        return this.t;
    }

    public final synchronized a.a.a.b x() {
        if (this.e == null) {
            this.e = i();
        }
        return this.e;
    }

    public final synchronized g y() {
        if (this.f == null) {
            this.f = j();
        }
        return this.f;
    }

    public final synchronized k z() {
        if (this.k == null) {
            this.k = k();
        }
        return this.k;
    }
}
