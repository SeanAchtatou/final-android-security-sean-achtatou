package a.a.a.h.a;

import java.security.Key;
import java.util.Arrays;
import javax.crypto.Cipher;

public class q {

    /* renamed from: a  reason: collision with root package name */
    protected final String f85a;
    protected final String b;
    protected final String c;
    protected final byte[] d;
    protected final String e;
    protected final byte[] f;
    protected byte[] g;
    protected byte[] h;
    protected byte[] i;
    protected byte[] j;
    protected byte[] k;
    protected byte[] l;
    protected byte[] m;
    protected byte[] n;
    protected byte[] o;
    protected byte[] p;
    protected byte[] q;
    protected byte[] r;
    protected byte[] s;
    protected byte[] t;
    protected byte[] u;
    protected byte[] v;
    protected byte[] w;
    protected byte[] x;
    protected byte[] y;
    protected byte[] z;

    public q(String str, String str2, String str3, byte[] bArr, String str4, byte[] bArr2) {
        this(str, str2, str3, bArr, str4, bArr2, null, null, null, null);
    }

    public q(String str, String str2, String str3, byte[] bArr, String str4, byte[] bArr2, byte[] bArr3, byte[] bArr4, byte[] bArr5, byte[] bArr6) {
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = null;
        this.r = null;
        this.s = null;
        this.t = null;
        this.u = null;
        this.v = null;
        this.w = null;
        this.x = null;
        this.y = null;
        this.z = null;
        this.f85a = str;
        this.e = str4;
        this.b = str2;
        this.c = str3;
        this.d = bArr;
        this.f = bArr2;
        this.g = bArr3;
        this.h = bArr4;
        this.i = bArr5;
        this.j = bArr6;
    }

    public byte[] a() {
        if (this.g == null) {
            this.g = p.f();
        }
        return this.g;
    }

    public byte[] b() {
        if (this.h == null) {
            this.h = p.f();
        }
        return this.h;
    }

    public byte[] c() {
        if (this.i == null) {
            this.i = p.g();
        }
        return this.i;
    }

    public byte[] d() {
        if (this.k == null) {
            this.k = p.h(this.c);
        }
        return this.k;
    }

    public byte[] e() {
        if (this.l == null) {
            this.l = p.d(d(), this.d);
        }
        return this.l;
    }

    public byte[] f() {
        if (this.m == null) {
            this.m = p.i(this.c);
        }
        return this.m;
    }

    public byte[] g() {
        if (this.n == null) {
            this.n = p.d(f(), this.d);
        }
        return this.n;
    }

    public byte[] h() {
        if (this.p == null) {
            this.p = p.c(this.f85a, this.b, f());
        }
        return this.p;
    }

    public byte[] i() {
        if (this.o == null) {
            this.o = p.d(this.f85a, this.b, f());
        }
        return this.o;
    }

    public byte[] j() {
        if (this.j == null) {
            long currentTimeMillis = 10000 * (System.currentTimeMillis() + 11644473600000L);
            this.j = new byte[8];
            for (int i2 = 0; i2 < 8; i2++) {
                this.j[i2] = (byte) ((int) currentTimeMillis);
                currentTimeMillis >>>= 8;
            }
        }
        return this.j;
    }

    public byte[] k() {
        if (this.r == null) {
            this.r = p.e(b(), this.f, j());
        }
        return this.r;
    }

    public byte[] l() {
        if (this.s == null) {
            this.s = p.d(i(), this.d, k());
        }
        return this.s;
    }

    public byte[] m() {
        if (this.q == null) {
            this.q = p.d(h(), this.d, a());
        }
        return this.q;
    }

    public byte[] n() {
        if (this.t == null) {
            this.t = p.a(f(), this.d, a());
        }
        return this.t;
    }

    public byte[] o() {
        if (this.u == null) {
            byte[] a2 = a();
            this.u = new byte[24];
            System.arraycopy(a2, 0, this.u, 0, a2.length);
            Arrays.fill(this.u, a2.length, this.u.length, (byte) 0);
        }
        return this.u;
    }

    public byte[] p() {
        if (this.v == null) {
            this.v = new byte[16];
            System.arraycopy(d(), 0, this.v, 0, 8);
            Arrays.fill(this.v, 8, 16, (byte) 0);
        }
        return this.v;
    }

    public byte[] q() {
        if (this.w == null) {
            s sVar = new s();
            sVar.a(f());
            this.w = sVar.a();
        }
        return this.w;
    }

    public byte[] r() {
        if (this.x == null) {
            byte[] i2 = i();
            byte[] bArr = new byte[16];
            System.arraycopy(l(), 0, bArr, 0, 16);
            this.x = p.a(bArr, i2);
        }
        return this.x;
    }

    public byte[] s() {
        if (this.y == null) {
            byte[] o2 = o();
            byte[] bArr = new byte[(this.d.length + o2.length)];
            System.arraycopy(this.d, 0, bArr, 0, this.d.length);
            System.arraycopy(o2, 0, bArr, this.d.length, o2.length);
            this.y = p.a(bArr, q());
        }
        return this.y;
    }

    public byte[] t() {
        if (this.z == null) {
            try {
                byte[] bArr = new byte[14];
                System.arraycopy(d(), 0, bArr, 0, 8);
                Arrays.fill(bArr, 8, bArr.length, (byte) -67);
                Key a2 = p.g(bArr, 0);
                Key a3 = p.g(bArr, 7);
                byte[] bArr2 = new byte[8];
                System.arraycopy(e(), 0, bArr2, 0, bArr2.length);
                Cipher instance = Cipher.getInstance("DES/ECB/NoPadding");
                instance.init(1, a2);
                byte[] doFinal = instance.doFinal(bArr2);
                Cipher instance2 = Cipher.getInstance("DES/ECB/NoPadding");
                instance2.init(1, a3);
                byte[] doFinal2 = instance2.doFinal(bArr2);
                this.z = new byte[16];
                System.arraycopy(doFinal, 0, this.z, 0, doFinal.length);
                System.arraycopy(doFinal2, 0, this.z, doFinal.length, doFinal2.length);
            } catch (Exception e2) {
                throw new o(e2.getMessage(), e2);
            }
        }
        return this.z;
    }
}
