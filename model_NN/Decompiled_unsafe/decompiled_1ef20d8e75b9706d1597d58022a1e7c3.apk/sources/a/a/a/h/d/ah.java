package a.a.a.h.d;

import a.a.a.e;
import a.a.a.f;
import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.d;
import a.a.a.f.n;
import a.a.a.j.p;
import a.a.a.n.a;
import a.a.a.y;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ah extends aa {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.h.d.ah.<init>(java.lang.String[], boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      a.a.a.h.d.ah.<init>(boolean, a.a.a.f.b[]):void
      a.a.a.h.d.ah.<init>(java.lang.String[], boolean):void */
    public ah() {
        this((String[]) null, false);
    }

    ah(boolean z, b... bVarArr) {
        super(z, bVarArr);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ah(java.lang.String[] r5, boolean r6) {
        /*
            r4 = this;
            r0 = 10
            a.a.a.f.b[] r1 = new a.a.a.f.b[r0]
            r0 = 0
            a.a.a.h.d.aj r2 = new a.a.a.h.d.aj
            r2.<init>()
            r1[r0] = r2
            r0 = 1
            a.a.a.h.d.i r2 = new a.a.a.h.d.i
            r2.<init>()
            r1[r0] = r2
            r0 = 2
            a.a.a.h.d.af r2 = new a.a.a.h.d.af
            r2.<init>()
            r1[r0] = r2
            r0 = 3
            a.a.a.h.d.ag r2 = new a.a.a.h.d.ag
            r2.<init>()
            r1[r0] = r2
            r0 = 4
            a.a.a.h.d.h r2 = new a.a.a.h.d.h
            r2.<init>()
            r1[r0] = r2
            r0 = 5
            a.a.a.h.d.j r2 = new a.a.a.h.d.j
            r2.<init>()
            r1[r0] = r2
            r0 = 6
            a.a.a.h.d.e r2 = new a.a.a.h.d.e
            r2.<init>()
            r1[r0] = r2
            r2 = 7
            a.a.a.h.d.g r3 = new a.a.a.h.d.g
            if (r5 == 0) goto L_0x0062
            java.lang.Object r0 = r5.clone()
            java.lang.String[] r0 = (java.lang.String[]) r0
        L_0x0047:
            r3.<init>(r0)
            r1[r2] = r3
            r0 = 8
            a.a.a.h.d.ad r2 = new a.a.a.h.d.ad
            r2.<init>()
            r1[r0] = r2
            r0 = 9
            a.a.a.h.d.ae r2 = new a.a.a.h.d.ae
            r2.<init>()
            r1[r0] = r2
            r4.<init>(r6, r1)
            return
        L_0x0062:
            java.lang.String[] r0 = a.a.a.h.d.ah.f130a
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.h.d.ah.<init>(java.lang.String[], boolean):void");
    }

    private List b(f[] fVarArr, a.a.a.f.f fVar) {
        ArrayList arrayList = new ArrayList(fVarArr.length);
        for (f fVar2 : fVarArr) {
            String a2 = fVar2.a();
            String b = fVar2.b();
            if (a2 == null || a2.isEmpty()) {
                throw new n("Cookie name may not be empty");
            }
            d dVar = new d(a2, b);
            dVar.e(a(fVar));
            dVar.d(b(fVar));
            dVar.a(new int[]{fVar.c()});
            y[] c = fVar2.c();
            HashMap hashMap = new HashMap(c.length);
            for (int length = c.length - 1; length >= 0; length--) {
                y yVar = c[length];
                hashMap.put(yVar.a().toLowerCase(Locale.ROOT), yVar);
            }
            for (Map.Entry value : hashMap.entrySet()) {
                y yVar2 = (y) value.getValue();
                String lowerCase = yVar2.a().toLowerCase(Locale.ROOT);
                dVar.a(lowerCase, yVar2.b());
                d a3 = a(lowerCase);
                if (a3 != null) {
                    a3.a(dVar, yVar2.b());
                }
            }
            arrayList.add(dVar);
        }
        return arrayList;
    }

    private static a.a.a.f.f c(a.a.a.f.f fVar) {
        boolean z = false;
        String a2 = fVar.a();
        int i = 0;
        while (true) {
            if (i >= a2.length()) {
                z = true;
                break;
            }
            char charAt = a2.charAt(i);
            if (charAt == '.' || charAt == ':') {
                break;
            }
            i++;
        }
        return z ? new a.a.a.f.f(a2 + ".local", fVar.c(), fVar.b(), fVar.d()) : fVar;
    }

    public int a() {
        return 1;
    }

    public List a(e eVar, a.a.a.f.f fVar) {
        a.a(eVar, "Header");
        a.a(fVar, "Cookie origin");
        if (eVar.c().equalsIgnoreCase("Set-Cookie2")) {
            return b(eVar.e(), c(fVar));
        }
        throw new n("Unrecognized cookie header '" + eVar.toString() + "'");
    }

    /* access modifiers changed from: protected */
    public List a(f[] fVarArr, a.a.a.f.f fVar) {
        return b(fVarArr, c(fVar));
    }

    public void a(c cVar, a.a.a.f.f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        super.a(cVar, c(fVar));
    }

    /* access modifiers changed from: protected */
    public void a(a.a.a.n.d dVar, c cVar, int i) {
        String a2;
        int[] f;
        super.a(dVar, cVar, i);
        if ((cVar instanceof a.a.a.f.a) && (a2 = ((a.a.a.f.a) cVar).a("port")) != null) {
            dVar.a("; $Port");
            dVar.a("=\"");
            if (!a2.trim().isEmpty() && (f = cVar.f()) != null) {
                int length = f.length;
                for (int i2 = 0; i2 < length; i2++) {
                    if (i2 > 0) {
                        dVar.a(",");
                    }
                    dVar.a(Integer.toString(f[i2]));
                }
            }
            dVar.a("\"");
        }
    }

    public e b() {
        a.a.a.n.d dVar = new a.a.a.n.d(40);
        dVar.a("Cookie2");
        dVar.a(": ");
        dVar.a("$Version=");
        dVar.a(Integer.toString(a()));
        return new p(dVar);
    }

    public boolean b(c cVar, a.a.a.f.f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        return super.b(cVar, c(fVar));
    }

    public String toString() {
        return "rfc2965";
    }
}
