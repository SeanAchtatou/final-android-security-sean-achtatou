package a.a.a.h.d;

import a.a.a.f.b;
import a.a.a.f.n;
import a.a.a.f.o;
import a.a.a.n.a;
import java.util.Date;

public class g extends a implements b {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f136a;

    public g(String[] strArr) {
        a.a(strArr, "Array of date patterns");
        this.f136a = strArr;
    }

    public String a() {
        return "expires";
    }

    public void a(o oVar, String str) {
        a.a(oVar, "Cookie");
        if (str == null) {
            throw new n("Missing value for 'expires' attribute");
        }
        Date a2 = a.a.a.b.f.b.a(str, this.f136a);
        if (a2 == null) {
            throw new n("Invalid 'expires' attribute: " + str);
        }
        oVar.b(a2);
    }
}
