package a.a.a.h.a;

import a.a.a.a.n;
import a.a.a.e;
import a.a.a.q;
import org.ietf.jgss.Oid;

public class ab extends f {
    public ab() {
    }

    public ab(boolean z, boolean z2) {
        super(z, z2);
    }

    public e a(n nVar, q qVar, a.a.a.m.e eVar) {
        return super.a(nVar, qVar, eVar);
    }

    public String a() {
        return "Negotiate";
    }

    /* access modifiers changed from: protected */
    public byte[] a(byte[] bArr, String str) {
        return super.a(bArr, str);
    }

    /* access modifiers changed from: protected */
    public byte[] a(byte[] bArr, String str, n nVar) {
        return a(bArr, new Oid("1.3.6.1.5.5.2"), str, nVar);
    }

    public String b() {
        return null;
    }

    public boolean c() {
        return true;
    }
}
