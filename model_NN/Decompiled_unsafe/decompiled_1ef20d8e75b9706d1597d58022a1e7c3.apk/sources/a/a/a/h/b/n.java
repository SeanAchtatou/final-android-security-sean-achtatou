package a.a.a.h.b;

import a.a.a.b.c.h;
import a.a.a.b.c.i;
import a.a.a.b.c.l;
import a.a.a.b.o;
import a.a.a.m.e;
import a.a.a.q;
import a.a.a.s;
import java.net.URI;

@Deprecated
class n implements o {

    /* renamed from: a  reason: collision with root package name */
    private final a.a.a.b.n f107a;

    public a.a.a.b.n a() {
        return this.f107a;
    }

    public boolean a(q qVar, s sVar, e eVar) {
        return this.f107a.a(sVar, eVar);
    }

    public l b(q qVar, s sVar, e eVar) {
        URI b = this.f107a.b(sVar, eVar);
        return qVar.g().a().equalsIgnoreCase("HEAD") ? new i(b) : new h(b);
    }
}
