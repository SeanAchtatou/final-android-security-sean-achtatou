package a.a.a.h.c;

import a.a.a.e.p;
import a.a.a.e.q;
import a.a.a.h.f;
import a.a.a.i.c;
import a.a.a.i.g;
import a.a.a.m.e;
import a.a.a.n;
import a.a.a.n.a;
import a.a.a.s;
import a.a.a.t;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Deprecated
public class d extends f implements p, q, e {

    /* renamed from: a  reason: collision with root package name */
    private final Log f119a = LogFactory.getLog(getClass());
    private final Log b = LogFactory.getLog("org.apache.http.headers");
    private final Log c = LogFactory.getLog("org.apache.http.wire");
    private volatile Socket d;
    private n e;
    private boolean f;
    private volatile boolean g;
    private final Map h = new HashMap();

    /* access modifiers changed from: protected */
    public c a(a.a.a.i.f fVar, t tVar, a.a.a.k.e eVar) {
        return new f(fVar, null, tVar, eVar);
    }

    /* access modifiers changed from: protected */
    public a.a.a.i.f a(Socket socket, int i, a.a.a.k.e eVar) {
        if (i <= 0) {
            i = 8192;
        }
        a.a.a.i.f a2 = super.a(socket, i, eVar);
        return this.c.isDebugEnabled() ? new j(a2, new o(this.c), a.a.a.k.f.a(eVar)) : a2;
    }

    public s a() {
        s a2 = super.a();
        if (this.f119a.isDebugEnabled()) {
            this.f119a.debug("Receiving response: " + a2.a());
        }
        if (this.b.isDebugEnabled()) {
            this.b.debug("<< " + a2.a().toString());
            a.a.a.e[] d2 = a2.d();
            int length = d2.length;
            for (int i = 0; i < length; i++) {
                this.b.debug("<< " + d2[i].toString());
            }
        }
        return a2;
    }

    public Object a(String str) {
        return this.h.get(str);
    }

    public void a(a.a.a.q qVar) {
        if (this.f119a.isDebugEnabled()) {
            this.f119a.debug("Sending request: " + qVar.g());
        }
        super.a(qVar);
        if (this.b.isDebugEnabled()) {
            this.b.debug(">> " + qVar.g().toString());
            a.a.a.e[] d2 = qVar.d();
            int length = d2.length;
            for (int i = 0; i < length; i++) {
                this.b.debug(">> " + d2[i].toString());
            }
        }
    }

    public void a(String str, Object obj) {
        this.h.put(str, obj);
    }

    public void a(Socket socket, n nVar) {
        q();
        this.d = socket;
        this.e = nVar;
        if (this.g) {
            socket.close();
            throw new InterruptedIOException("Connection already shutdown");
        }
    }

    public void a(Socket socket, n nVar, boolean z, a.a.a.k.e eVar) {
        j();
        a.a(nVar, "Target host");
        a.a(eVar, "Parameters");
        if (socket != null) {
            this.d = socket;
            a(socket, eVar);
        }
        this.e = nVar;
        this.f = z;
    }

    public void a(boolean z, a.a.a.k.e eVar) {
        a.a(eVar, "Parameters");
        q();
        this.f = z;
        a(this.d, eVar);
    }

    /* access modifiers changed from: protected */
    public g b(Socket socket, int i, a.a.a.k.e eVar) {
        if (i <= 0) {
            i = 8192;
        }
        g b2 = super.b(socket, i, eVar);
        return this.c.isDebugEnabled() ? new k(b2, new o(this.c), a.a.a.k.f.a(eVar)) : b2;
    }

    public void close() {
        try {
            super.close();
            if (this.f119a.isDebugEnabled()) {
                this.f119a.debug("Connection " + this + " closed");
            }
        } catch (IOException e2) {
            this.f119a.debug("I/O error closing connection", e2);
        }
    }

    public void e() {
        this.g = true;
        try {
            super.e();
            if (this.f119a.isDebugEnabled()) {
                this.f119a.debug("Connection " + this + " shut down");
            }
            Socket socket = this.d;
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e2) {
            this.f119a.debug("I/O error shutting down connection", e2);
        }
    }

    public final boolean h() {
        return this.f;
    }

    public final Socket i() {
        return this.d;
    }

    public SSLSession m() {
        if (this.d instanceof SSLSocket) {
            return ((SSLSocket) this.d).getSession();
        }
        return null;
    }
}
