package a.a.a.h.f;

import a.a.a.h;
import a.a.a.i.d;
import a.a.a.i.g;
import a.a.a.j.i;
import a.a.a.j.s;
import a.a.a.k.e;
import a.a.a.n.a;
import a.a.a.p;

public abstract class b implements d {

    /* renamed from: a  reason: collision with root package name */
    protected final g f151a;
    protected final a.a.a.n.d b = new a.a.a.n.d(128);
    protected final s c;

    @Deprecated
    public b(g gVar, s sVar, e eVar) {
        a.a(gVar, "Session input buffer");
        this.f151a = gVar;
        this.c = sVar == null ? i.b : sVar;
    }

    /* access modifiers changed from: protected */
    public abstract void a(p pVar);

    public void b(p pVar) {
        a.a(pVar, "HTTP message");
        a(pVar);
        h e = pVar.e();
        while (e.hasNext()) {
            this.f151a.a(this.c.a(this.b, e.a()));
        }
        this.b.a();
        this.f151a.a(this.b);
    }
}
