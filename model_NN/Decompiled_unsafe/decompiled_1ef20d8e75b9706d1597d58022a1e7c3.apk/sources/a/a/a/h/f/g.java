package a.a.a.h.f;

import a.a.a.i.f;
import a.a.a.n.a;
import java.io.IOException;
import java.io.InputStream;

public class g extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final long f156a;
    private long b = 0;
    private boolean c = false;
    private f d = null;

    public g(f fVar, long j) {
        this.d = (f) a.a(fVar, "Session input buffer");
        this.f156a = a.a(j, "Content length");
    }

    public int available() {
        if (this.d instanceof a.a.a.i.a) {
            return Math.min(((a.a.a.i.a) this.d).e(), (int) (this.f156a - this.b));
        }
        return 0;
    }

    public void close() {
        if (!this.c) {
            try {
                if (this.b < this.f156a) {
                    do {
                    } while (read(new byte[2048]) >= 0);
                }
            } finally {
                this.c = true;
            }
        }
    }

    public int read() {
        if (this.c) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.b >= this.f156a) {
            return -1;
        } else {
            int a2 = this.d.a();
            if (a2 != -1) {
                this.b++;
            } else if (this.b < this.f156a) {
                throw new a.a.a.a("Premature end of Content-Length delimited message body (expected: " + this.f156a + "; received: " + this.b);
            }
            return a2;
        }
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.c) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.b >= this.f156a) {
            return -1;
        } else {
            if (this.b + ((long) i2) > this.f156a) {
                i2 = (int) (this.f156a - this.b);
            }
            int a2 = this.d.a(bArr, i, i2);
            if (a2 != -1 || this.b >= this.f156a) {
                if (a2 > 0) {
                    this.b += (long) a2;
                }
                return a2;
            }
            throw new a.a.a.a("Premature end of Content-Length delimited message body (expected: " + this.f156a + "; received: " + this.b);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public long skip(long j) {
        int read;
        if (j <= 0) {
            return 0;
        }
        byte[] bArr = new byte[2048];
        long min = Math.min(j, this.f156a - this.b);
        long j2 = 0;
        while (min > 0 && (read = read(bArr, 0, (int) Math.min(2048L, min))) != -1) {
            j2 += (long) read;
            min -= (long) read;
        }
        return j2;
    }
}
