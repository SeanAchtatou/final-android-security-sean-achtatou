package a.a.a.h.a;

import a.a.a.a.c;
import a.a.a.a.d;
import a.a.a.a.e;

public class ac implements d, e {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f72a;
    private final boolean b;

    public ac() {
        this(true, true);
    }

    public ac(boolean z, boolean z2) {
        this.f72a = z;
        this.b = z2;
    }

    public c a(a.a.a.k.e eVar) {
        return new ab(this.f72a, this.b);
    }

    public c a(a.a.a.m.e eVar) {
        return new ab(this.f72a, this.b);
    }
}
