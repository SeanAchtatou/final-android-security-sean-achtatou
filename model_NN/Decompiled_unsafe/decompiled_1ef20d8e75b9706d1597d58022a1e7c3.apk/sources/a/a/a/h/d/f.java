package a.a.a.h.d;

import a.a.a.e.e.a;
import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.h;
import a.a.a.f.n;
import a.a.a.f.o;
import a.a.a.n.i;
import java.util.Locale;

public class f implements b {
    static boolean a(String str, String str2) {
        if (a.a(str2) || a.d(str2)) {
            return false;
        }
        if (str.startsWith(".")) {
            str = str.substring(1);
        }
        if (str2.endsWith(str)) {
            int length = str2.length() - str.length();
            if (length == 0) {
                return true;
            }
            if (length > 1 && str2.charAt(length - 1) == '.') {
                return true;
            }
        }
        return false;
    }

    public String a() {
        return "domain";
    }

    public void a(c cVar, a.a.a.f.f fVar) {
        a.a.a.n.a.a(cVar, "Cookie");
        a.a.a.n.a.a(fVar, "Cookie origin");
        String a2 = fVar.a();
        String d = cVar.d();
        if (d == null) {
            throw new h("Cookie 'domain' may not be null");
        } else if (!a2.equals(d) && !a(d, a2)) {
            throw new h("Illegal 'domain' attribute \"" + d + "\". Domain of origin: \"" + a2 + "\"");
        }
    }

    public void a(o oVar, String str) {
        a.a.a.n.a.a(oVar, "Cookie");
        if (i.b(str)) {
            throw new n("Blank or null value for domain attribute");
        } else if (!str.endsWith(".")) {
            if (str.startsWith(".")) {
                str = str.substring(1);
            }
            oVar.d(str.toLowerCase(Locale.ROOT));
        }
    }

    public boolean b(c cVar, a.a.a.f.f fVar) {
        a.a.a.n.a.a(cVar, "Cookie");
        a.a.a.n.a.a(fVar, "Cookie origin");
        String a2 = fVar.a();
        String d = cVar.d();
        if (d == null) {
            return false;
        }
        if (d.startsWith(".")) {
            d = d.substring(1);
        }
        String lowerCase = d.toLowerCase(Locale.ROOT);
        if (a2.equals(lowerCase)) {
            return true;
        }
        if (!(cVar instanceof a.a.a.f.a) || !((a.a.a.f.a) cVar).b("domain")) {
            return false;
        }
        return a(lowerCase, a2);
    }
}
