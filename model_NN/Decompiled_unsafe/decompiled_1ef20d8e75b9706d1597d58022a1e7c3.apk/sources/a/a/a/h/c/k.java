package a.a.a.h.c;

import a.a.a.c;
import a.a.a.i.e;
import a.a.a.i.g;
import a.a.a.n.d;

@Deprecated
public class k implements g {

    /* renamed from: a  reason: collision with root package name */
    private final g f125a;
    private final o b;
    private final String c;

    public k(g gVar, o oVar, String str) {
        this.f125a = gVar;
        this.b = oVar;
        this.c = str == null ? c.b.name() : str;
    }

    public void a() {
        this.f125a.a();
    }

    public void a(int i) {
        this.f125a.a(i);
        if (this.b.a()) {
            this.b.a(i);
        }
    }

    public void a(d dVar) {
        this.f125a.a(dVar);
        if (this.b.a()) {
            this.b.a((new String(dVar.b(), 0, dVar.length()) + "\r\n").getBytes(this.c));
        }
    }

    public void a(String str) {
        this.f125a.a(str);
        if (this.b.a()) {
            this.b.a((str + "\r\n").getBytes(this.c));
        }
    }

    public void a(byte[] bArr, int i, int i2) {
        this.f125a.a(bArr, i, i2);
        if (this.b.a()) {
            this.b.a(bArr, i, i2);
        }
    }

    public e b() {
        return this.f125a.b();
    }
}
