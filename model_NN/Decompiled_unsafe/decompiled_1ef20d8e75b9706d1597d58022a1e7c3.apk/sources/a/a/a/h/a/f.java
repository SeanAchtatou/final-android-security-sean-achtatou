package a.a.a.h.a;

import a.a.a.a.j;
import a.a.a.a.n;
import a.a.a.a.o;
import a.a.a.e;
import a.a.a.e.b.b;
import a.a.a.j.p;
import a.a.a.n.a;
import a.a.a.n.d;
import a.a.a.q;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ietf.jgss.GSSContext;
import org.ietf.jgss.GSSException;
import org.ietf.jgss.GSSManager;
import org.ietf.jgss.GSSName;
import org.ietf.jgss.Oid;

public abstract class f extends a {

    /* renamed from: a  reason: collision with root package name */
    private final Log f77a;
    private final Base64 b;
    private final boolean c;
    private final boolean d;
    private h e;
    private byte[] f;

    f() {
        this(true, true);
    }

    f(boolean z, boolean z2) {
        this.f77a = LogFactory.getLog(getClass());
        this.b = new Base64(0);
        this.c = z;
        this.d = z2;
        this.e = h.UNINITIATED;
    }

    private String a(String str) {
        InetAddress byName = InetAddress.getByName(str);
        String canonicalHostName = byName.getCanonicalHostName();
        return byName.getHostAddress().contentEquals(canonicalHostName) ? str : canonicalHostName;
    }

    @Deprecated
    public e a(n nVar, q qVar) {
        return a(nVar, qVar, (a.a.a.m.e) null);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public e a(n nVar, q qVar, a.a.a.m.e eVar) {
        a.a.a.n a2;
        a.a(qVar, "HTTP request");
        switch (g.f78a[this.e.ordinal()]) {
            case 1:
                throw new j(a() + " authentication has not been initiated");
            case 2:
                throw new j(a() + " authentication has failed");
            case 3:
                try {
                    b bVar = (b) eVar.a("http.route");
                    if (bVar != null) {
                        if (e()) {
                            a2 = bVar.d();
                            if (a2 == null) {
                                a2 = bVar.a();
                            }
                        } else {
                            a2 = bVar.a();
                        }
                        String a3 = a2.a();
                        if (this.d) {
                            try {
                                a3 = a(a3);
                            } catch (UnknownHostException e2) {
                            }
                        }
                        if (!this.c) {
                            a3 = a3 + ":" + a2.b();
                        }
                        if (this.f77a.isDebugEnabled()) {
                            this.f77a.debug("init " + a3);
                        }
                        this.f = a(this.f, a3, nVar);
                        this.e = h.TOKEN_GENERATED;
                        break;
                    } else {
                        throw new j("Connection route is not available");
                    }
                } catch (GSSException e3) {
                    this.e = h.FAILED;
                    if (e3.getMajor() == 9 || e3.getMajor() == 8) {
                        throw new o(e3.getMessage(), e3);
                    } else if (e3.getMajor() == 13) {
                        throw new o(e3.getMessage(), e3);
                    } else if (e3.getMajor() == 10 || e3.getMajor() == 19 || e3.getMajor() == 20) {
                        throw new j(e3.getMessage(), e3);
                    } else {
                        throw new j(e3.getMessage());
                    }
                }
                break;
            case 4:
                break;
            default:
                throw new IllegalStateException("Illegal state: " + this.e);
        }
        String str = new String(this.b.encode(this.f));
        if (this.f77a.isDebugEnabled()) {
            this.f77a.debug("Sending response '" + str + "' back to the auth server");
        }
        d dVar = new d(32);
        if (e()) {
            dVar.a("Proxy-Authorization");
        } else {
            dVar.a("Authorization");
        }
        dVar.a(": Negotiate ");
        dVar.a(str);
        return new p(dVar);
    }

    /* access modifiers changed from: protected */
    public void a(d dVar, int i, int i2) {
        String b2 = dVar.b(i, i2);
        if (this.f77a.isDebugEnabled()) {
            this.f77a.debug("Received challenge '" + b2 + "' from the auth server");
        }
        if (this.e == h.UNINITIATED) {
            this.f = Base64.decodeBase64(b2.getBytes());
            this.e = h.CHALLENGE_RECEIVED;
            return;
        }
        this.f77a.debug("Authentication already attempted");
        this.e = h.FAILED;
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public byte[] a(byte[] bArr, String str) {
        return null;
    }

    /* access modifiers changed from: protected */
    public byte[] a(byte[] bArr, String str, n nVar) {
        return a(bArr, str);
    }

    /* access modifiers changed from: protected */
    public byte[] a(byte[] bArr, Oid oid, String str, n nVar) {
        if (bArr == null) {
            bArr = new byte[0];
        }
        GSSManager f2 = f();
        GSSContext createContext = f2.createContext(f2.createName("HTTP@" + str, GSSName.NT_HOSTBASED_SERVICE).canonicalize(oid), oid, nVar instanceof a.a.a.a.p ? ((a.a.a.a.p) nVar).c() : null, 0);
        createContext.requestMutualAuth(true);
        createContext.requestCredDeleg(true);
        return createContext.initSecContext(bArr, 0, bArr.length);
    }

    public boolean d() {
        return this.e == h.TOKEN_GENERATED || this.e == h.FAILED;
    }

    /* access modifiers changed from: protected */
    public GSSManager f() {
        return GSSManager.getInstance();
    }
}
