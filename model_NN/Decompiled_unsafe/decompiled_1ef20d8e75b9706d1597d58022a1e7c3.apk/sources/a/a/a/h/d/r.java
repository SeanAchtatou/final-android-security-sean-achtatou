package a.a.a.h.d;

import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.d;
import a.a.a.f.f;
import a.a.a.f.n;
import a.a.a.n.a;
import a.a.a.y;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class r extends b {
    public r() {
    }

    protected r(b... bVarArr) {
        super(bVarArr);
    }

    protected static String a(f fVar) {
        String b = fVar.b();
        int lastIndexOf = b.lastIndexOf(47);
        if (lastIndexOf < 0) {
            return b;
        }
        if (lastIndexOf == 0) {
            lastIndexOf = 1;
        }
        return b.substring(0, lastIndexOf);
    }

    protected static String b(f fVar) {
        return fVar.a();
    }

    /* access modifiers changed from: protected */
    public List a(a.a.a.f[] fVarArr, f fVar) {
        ArrayList arrayList = new ArrayList(fVarArr.length);
        for (a.a.a.f fVar2 : fVarArr) {
            String a2 = fVar2.a();
            String b = fVar2.b();
            if (a2 == null || a2.isEmpty()) {
                throw new n("Cookie name may not be empty");
            }
            c cVar = new c(a2, b);
            cVar.e(a(fVar));
            cVar.d(b(fVar));
            y[] c = fVar2.c();
            for (int length = c.length - 1; length >= 0; length--) {
                y yVar = c[length];
                String lowerCase = yVar.a().toLowerCase(Locale.ROOT);
                cVar.a(lowerCase, yVar.b());
                d a3 = a(lowerCase);
                if (a3 != null) {
                    a3.a(cVar, yVar.b());
                }
            }
            arrayList.add(cVar);
        }
        return arrayList;
    }

    public void a(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        for (d a2 : c()) {
            a2.a(cVar, fVar);
        }
    }

    public boolean b(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        for (d b : c()) {
            if (!b.b(cVar, fVar)) {
                return false;
            }
        }
        return true;
    }
}
