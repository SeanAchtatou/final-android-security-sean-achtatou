package a.a.a.h.a;

import a.a.a.a.c;
import a.a.a.a.d;
import java.nio.charset.Charset;

public class e implements d, a.a.a.a.e {

    /* renamed from: a  reason: collision with root package name */
    private final Charset f76a;

    public e() {
        this(null);
    }

    public e(Charset charset) {
        this.f76a = charset;
    }

    public c a(a.a.a.k.e eVar) {
        return new d();
    }

    public c a(a.a.a.m.e eVar) {
        return new d(this.f76a);
    }
}
