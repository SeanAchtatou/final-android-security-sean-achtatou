package a.a.a.h.e;

import a.a.a.g.b;
import a.a.a.g.d;
import a.a.a.h.f.e;
import a.a.a.h.f.g;
import a.a.a.h.f.m;
import a.a.a.i.f;
import a.a.a.k;
import a.a.a.p;

@Deprecated
public class a {

    /* renamed from: a  reason: collision with root package name */
    private final d f145a;

    public a(d dVar) {
        this.f145a = (d) a.a.a.n.a.a(dVar, "Content length strategy");
    }

    /* access modifiers changed from: protected */
    public b a(f fVar, p pVar) {
        b bVar = new b();
        long a2 = this.f145a.a(pVar);
        if (a2 == -2) {
            bVar.a(true);
            bVar.a(-1);
            bVar.a(new e(fVar));
        } else if (a2 == -1) {
            bVar.a(false);
            bVar.a(-1);
            bVar.a(new m(fVar));
        } else {
            bVar.a(false);
            bVar.a(a2);
            bVar.a(new g(fVar, a2));
        }
        a.a.a.e c = pVar.c("Content-Type");
        if (c != null) {
            bVar.a(c);
        }
        a.a.a.e c2 = pVar.c("Content-Encoding");
        if (c2 != null) {
            bVar.b(c2);
        }
        return bVar;
    }

    public k b(f fVar, p pVar) {
        a.a.a.n.a.a(fVar, "Session input buffer");
        a.a.a.n.a.a(pVar, "HTTP message");
        return a(fVar, pVar);
    }
}
