package a.a.a.h.c;

import a.a.a.c;
import a.a.a.i.b;
import a.a.a.i.e;
import a.a.a.i.f;
import a.a.a.n.d;

@Deprecated
public class j implements b, f {

    /* renamed from: a  reason: collision with root package name */
    private final f f124a;
    private final b b;
    private final o c;
    private final String d;

    public j(f fVar, o oVar, String str) {
        this.f124a = fVar;
        this.b = fVar instanceof b ? (b) fVar : null;
        this.c = oVar;
        this.d = str == null ? c.b.name() : str;
    }

    public int a() {
        int a2 = this.f124a.a();
        if (this.c.a() && a2 != -1) {
            this.c.b(a2);
        }
        return a2;
    }

    public int a(d dVar) {
        int a2 = this.f124a.a(dVar);
        if (this.c.a() && a2 >= 0) {
            this.c.b((new String(dVar.b(), dVar.length() - a2, a2) + "\r\n").getBytes(this.d));
        }
        return a2;
    }

    public int a(byte[] bArr, int i, int i2) {
        int a2 = this.f124a.a(bArr, i, i2);
        if (this.c.a() && a2 > 0) {
            this.c.b(bArr, i, a2);
        }
        return a2;
    }

    public boolean a(int i) {
        return this.f124a.a(i);
    }

    public e b() {
        return this.f124a.b();
    }

    public boolean c() {
        if (this.b != null) {
            return this.b.c();
        }
        return false;
    }
}
