package a.a.a.f;

import java.io.Serializable;
import java.util.Comparator;

public class e implements Serializable, Comparator {
    /* renamed from: a */
    public int compare(c cVar, c cVar2) {
        int compareTo = cVar.a().compareTo(cVar2.a());
        if (compareTo == 0) {
            String d = cVar.d();
            if (d == null) {
                d = "";
            } else if (d.indexOf(46) == -1) {
                d = d + ".local";
            }
            String d2 = cVar2.d();
            if (d2 == null) {
                d2 = "";
            } else if (d2.indexOf(46) == -1) {
                d2 = d2 + ".local";
            }
            compareTo = d.compareToIgnoreCase(d2);
        }
        if (compareTo != 0) {
            return compareTo;
        }
        String e = cVar.e();
        if (e == null) {
            e = "/";
        }
        String e2 = cVar2.e();
        if (e2 == null) {
            e2 = "/";
        }
        return e.compareTo(e2);
    }
}
