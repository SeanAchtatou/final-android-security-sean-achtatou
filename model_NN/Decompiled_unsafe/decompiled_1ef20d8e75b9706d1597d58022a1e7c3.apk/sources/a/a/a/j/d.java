package a.a.a.j;

import a.a.a.e;
import a.a.a.f;
import a.a.a.g;
import a.a.a.h;
import a.a.a.n.a;
import java.util.NoSuchElementException;

public class d implements g {

    /* renamed from: a  reason: collision with root package name */
    private final h f166a;
    private final r b;
    private f c;
    private a.a.a.n.d d;
    private u e;

    public d(h hVar) {
        this(hVar, f.b);
    }

    public d(h hVar, r rVar) {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f166a = (h) a.a(hVar, "Header iterator");
        this.b = (r) a.a(rVar, "Parser");
    }

    private void b() {
        this.e = null;
        this.d = null;
        while (this.f166a.hasNext()) {
            e a2 = this.f166a.a();
            if (a2 instanceof a.a.a.d) {
                this.d = ((a.a.a.d) a2).a();
                this.e = new u(0, this.d.length());
                this.e.a(((a.a.a.d) a2).b());
                return;
            }
            String d2 = a2.d();
            if (d2 != null) {
                this.d = new a.a.a.n.d(d2.length());
                this.d.a(d2);
                this.e = new u(0, this.d.length());
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c() {
        /*
            r4 = this;
            r3 = 0
        L_0x0001:
            a.a.a.h r0 = r4.f166a
            boolean r0 = r0.hasNext()
            if (r0 != 0) goto L_0x000d
            a.a.a.j.u r0 = r4.e
            if (r0 == 0) goto L_0x0044
        L_0x000d:
            a.a.a.j.u r0 = r4.e
            if (r0 == 0) goto L_0x0019
            a.a.a.j.u r0 = r4.e
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x001c
        L_0x0019:
            r4.b()
        L_0x001c:
            a.a.a.j.u r0 = r4.e
            if (r0 == 0) goto L_0x0001
        L_0x0020:
            a.a.a.j.u r0 = r4.e
            boolean r0 = r0.c()
            if (r0 != 0) goto L_0x0045
            a.a.a.j.r r0 = r4.b
            a.a.a.n.d r1 = r4.d
            a.a.a.j.u r2 = r4.e
            a.a.a.f r0 = r0.b(r1, r2)
            java.lang.String r1 = r0.a()
            int r1 = r1.length()
            if (r1 != 0) goto L_0x0042
            java.lang.String r1 = r0.b()
            if (r1 == 0) goto L_0x0020
        L_0x0042:
            r4.c = r0
        L_0x0044:
            return
        L_0x0045:
            a.a.a.j.u r0 = r4.e
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x0001
            r4.e = r3
            r4.d = r3
            goto L_0x0001
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.j.d.c():void");
    }

    public f a() {
        if (this.c == null) {
            c();
        }
        if (this.c == null) {
            throw new NoSuchElementException("No more header elements available");
        }
        f fVar = this.c;
        this.c = null;
        return fVar;
    }

    public boolean hasNext() {
        if (this.c == null) {
            c();
        }
        return this.c != null;
    }

    public final Object next() {
        return a();
    }

    public void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
