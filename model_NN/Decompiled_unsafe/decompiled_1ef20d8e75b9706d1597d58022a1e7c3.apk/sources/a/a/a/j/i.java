package a.a.a.j;

import a.a.a.ac;
import a.a.a.ae;
import a.a.a.af;
import a.a.a.e;
import a.a.a.n.a;
import a.a.a.n.d;

public class i implements s {
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    public static final i f169a = new i();
    public static final i b = new i();

    /* access modifiers changed from: protected */
    public int a(ac acVar) {
        return acVar.a().length() + 4;
    }

    /* access modifiers changed from: protected */
    public d a(d dVar) {
        if (dVar == null) {
            return new d(64);
        }
        dVar.a();
        return dVar;
    }

    public d a(d dVar, ac acVar) {
        a.a(acVar, "Protocol version");
        int a2 = a(acVar);
        if (dVar == null) {
            dVar = new d(a2);
        } else {
            dVar.a(a2);
        }
        dVar.a(acVar.a());
        dVar.a('/');
        dVar.a(Integer.toString(acVar.b()));
        dVar.a('.');
        dVar.a(Integer.toString(acVar.c()));
        return dVar;
    }

    public d a(d dVar, ae aeVar) {
        a.a(aeVar, "Request line");
        d a2 = a(dVar);
        b(a2, aeVar);
        return a2;
    }

    public d a(d dVar, af afVar) {
        a.a(afVar, "Status line");
        d a2 = a(dVar);
        b(a2, afVar);
        return a2;
    }

    public d a(d dVar, e eVar) {
        a.a(eVar, "Header");
        if (eVar instanceof a.a.a.d) {
            return ((a.a.a.d) eVar).a();
        }
        d a2 = a(dVar);
        b(a2, eVar);
        return a2;
    }

    /* access modifiers changed from: protected */
    public void b(d dVar, ae aeVar) {
        String a2 = aeVar.a();
        String c = aeVar.c();
        dVar.a(a2.length() + 1 + c.length() + 1 + a(aeVar.b()));
        dVar.a(a2);
        dVar.a(' ');
        dVar.a(c);
        dVar.a(' ');
        a(dVar, aeVar.b());
    }

    /* access modifiers changed from: protected */
    public void b(d dVar, af afVar) {
        int a2 = a(afVar.a()) + 1 + 3 + 1;
        String c = afVar.c();
        if (c != null) {
            a2 += c.length();
        }
        dVar.a(a2);
        a(dVar, afVar.a());
        dVar.a(' ');
        dVar.a(Integer.toString(afVar.b()));
        dVar.a(' ');
        if (c != null) {
            dVar.a(c);
        }
    }

    /* access modifiers changed from: protected */
    public void b(d dVar, e eVar) {
        String c = eVar.c();
        String d = eVar.d();
        int length = c.length() + 2;
        if (d != null) {
            length += d.length();
        }
        dVar.a(length);
        dVar.a(c);
        dVar.a(": ");
        if (d != null) {
            dVar.a(d);
        }
    }
}
