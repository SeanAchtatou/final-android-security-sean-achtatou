package a.a.a.j;

import a.a.a.aa;
import a.a.a.d;
import a.a.a.f;
import a.a.a.n.a;
import java.io.Serializable;

public class p implements d, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f176a;
    private final a.a.a.n.d b;
    private final int c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [a.a.a.n.d, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public p(a.a.a.n.d dVar) {
        a.a((Object) dVar, "Char array buffer");
        int b2 = dVar.b(58);
        if (b2 == -1) {
            throw new aa("Invalid header: " + dVar.toString());
        }
        String b3 = dVar.b(0, b2);
        if (b3.length() == 0) {
            throw new aa("Invalid header: " + dVar.toString());
        }
        this.b = dVar;
        this.f176a = b3;
        this.c = b2 + 1;
    }

    public a.a.a.n.d a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public String c() {
        return this.f176a;
    }

    public Object clone() {
        return super.clone();
    }

    public String d() {
        return this.b.b(this.c, this.b.length());
    }

    public f[] e() {
        u uVar = new u(0, this.b.length());
        uVar.a(this.c);
        return f.b.a(this.b, uVar);
    }

    public String toString() {
        return this.b.toString();
    }
}
