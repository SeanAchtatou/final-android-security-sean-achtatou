package a.a.a.j;

import a.a.a.f;
import a.a.a.n.a;
import a.a.a.n.h;
import a.a.a.y;

public class c implements f, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f165a;
    private final String b;
    private final y[] c;

    public c(String str, String str2) {
        this(str, str2, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public c(String str, String str2, y[] yVarArr) {
        this.f165a = (String) a.a((Object) str, "Name");
        this.b = str2;
        if (yVarArr != null) {
            this.c = yVarArr;
        } else {
            this.c = new y[0];
        }
    }

    public y a(int i) {
        return this.c[i];
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public y a(String str) {
        a.a((Object) str, "Name");
        for (y yVar : this.c) {
            if (yVar.a().equalsIgnoreCase(str)) {
                return yVar;
            }
        }
        return null;
    }

    public String a() {
        return this.f165a;
    }

    public String b() {
        return this.b;
    }

    public y[] c() {
        return (y[]) this.c.clone();
    }

    public Object clone() {
        return super.clone();
    }

    public int d() {
        return this.c.length;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        c cVar = (c) obj;
        return this.f165a.equals(cVar.f165a) && h.a(this.b, cVar.b) && h.a(this.c, cVar.c);
    }

    public int hashCode() {
        int a2 = h.a(h.a(17, this.f165a), this.b);
        for (y a3 : this.c) {
            a2 = h.a(a2, a3);
        }
        return a2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f165a);
        if (this.b != null) {
            sb.append("=");
            sb.append(this.b);
        }
        for (y append : this.c) {
            sb.append("; ");
            sb.append(append);
        }
        return sb.toString();
    }
}
