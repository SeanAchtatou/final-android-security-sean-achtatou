package a.a.a.j;

import a.a.a.n.a;
import a.a.a.n.d;
import a.a.a.y;
import java.util.ArrayList;
import java.util.BitSet;

public class f implements r {
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    public static final f f168a = new f();
    public static final f b = new f();
    private static final BitSet c = v.a(61, 59, 44);
    private static final BitSet d = v.a(59, 44);
    private final v e = v.f179a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public static a.a.a.f[] a(String str, r rVar) {
        a.a((Object) str, "Value");
        d dVar = new d(str.length());
        dVar.a(str);
        u uVar = new u(0, str.length());
        if (rVar == null) {
            rVar = b;
        }
        return rVar.a(dVar, uVar);
    }

    /* access modifiers changed from: protected */
    public a.a.a.f a(String str, String str2, y[] yVarArr) {
        return new c(str, str2, yVarArr);
    }

    /* access modifiers changed from: protected */
    public y a(String str, String str2) {
        return new l(str, str2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [a.a.a.n.d, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public a.a.a.f[] a(d dVar, u uVar) {
        a.a((Object) dVar, "Char array buffer");
        a.a(uVar, "Parser cursor");
        ArrayList arrayList = new ArrayList();
        while (!uVar.c()) {
            a.a.a.f b2 = b(dVar, uVar);
            if (b2.a().length() != 0 || b2.b() != null) {
                arrayList.add(b2);
            }
        }
        return (a.a.a.f[]) arrayList.toArray(new a.a.a.f[arrayList.size()]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [a.a.a.n.d, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public a.a.a.f b(d dVar, u uVar) {
        a.a((Object) dVar, "Char array buffer");
        a.a(uVar, "Parser cursor");
        y d2 = d(dVar, uVar);
        y[] yVarArr = null;
        if (!uVar.c() && dVar.charAt(uVar.b() - 1) != ',') {
            yVarArr = c(dVar, uVar);
        }
        return a(d2.a(), d2.b(), yVarArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [a.a.a.n.d, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public y[] c(d dVar, u uVar) {
        a.a((Object) dVar, "Char array buffer");
        a.a(uVar, "Parser cursor");
        this.e.a(dVar, uVar);
        ArrayList arrayList = new ArrayList();
        while (!uVar.c()) {
            arrayList.add(d(dVar, uVar));
            if (dVar.charAt(uVar.b() - 1) == ',') {
                break;
            }
        }
        return (y[]) arrayList.toArray(new y[arrayList.size()]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [a.a.a.n.d, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public y d(d dVar, u uVar) {
        a.a((Object) dVar, "Char array buffer");
        a.a(uVar, "Parser cursor");
        String a2 = this.e.a(dVar, uVar, c);
        if (uVar.c()) {
            return new l(a2, null);
        }
        char charAt = dVar.charAt(uVar.b());
        uVar.a(uVar.b() + 1);
        if (charAt != '=') {
            return a(a2, (String) null);
        }
        String b2 = this.e.b(dVar, uVar, d);
        if (!uVar.c()) {
            uVar.a(uVar.b() + 1);
        }
        return a(a2, b2);
    }
}
