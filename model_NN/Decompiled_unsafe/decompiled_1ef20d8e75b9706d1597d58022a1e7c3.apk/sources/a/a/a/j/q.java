package a.a.a.j;

import a.a.a.e;
import a.a.a.h;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class q implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final List f177a = new ArrayList(16);

    public void a() {
        this.f177a.clear();
    }

    public void a(e eVar) {
        if (eVar != null) {
            this.f177a.add(eVar);
        }
    }

    public void a(e[] eVarArr) {
        a();
        if (eVarArr != null) {
            Collections.addAll(this.f177a, eVarArr);
        }
    }

    public e[] a(String str) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f177a.size()) {
                return (e[]) arrayList.toArray(new e[arrayList.size()]);
            }
            e eVar = (e) this.f177a.get(i2);
            if (eVar.c().equalsIgnoreCase(str)) {
                arrayList.add(eVar);
            }
            i = i2 + 1;
        }
    }

    public e b(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f177a.size()) {
                return null;
            }
            e eVar = (e) this.f177a.get(i2);
            if (eVar.c().equalsIgnoreCase(str)) {
                return eVar;
            }
            i = i2 + 1;
        }
    }

    public void b(e eVar) {
        if (eVar != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f177a.size()) {
                    this.f177a.add(eVar);
                    return;
                } else if (((e) this.f177a.get(i2)).c().equalsIgnoreCase(eVar.c())) {
                    this.f177a.set(i2, eVar);
                    return;
                } else {
                    i = i2 + 1;
                }
            }
        }
    }

    public e[] b() {
        return (e[]) this.f177a.toArray(new e[this.f177a.size()]);
    }

    public h c() {
        return new k(this.f177a, null);
    }

    public boolean c(String str) {
        for (int i = 0; i < this.f177a.size(); i++) {
            if (((e) this.f177a.get(i)).c().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    public Object clone() {
        return super.clone();
    }

    public h d(String str) {
        return new k(this.f177a, str);
    }

    public String toString() {
        return this.f177a.toString();
    }
}
