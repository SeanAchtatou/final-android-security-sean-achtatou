package a.a.a.m;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class a implements e {

    /* renamed from: a  reason: collision with root package name */
    private final e f182a;
    private final Map b;

    public a() {
        this(null);
    }

    public a(e eVar) {
        this.b = new ConcurrentHashMap();
        this.f182a = eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public Object a(String str) {
        a.a.a.n.a.a((Object) str, "Id");
        Object obj = this.b.get(str);
        return (obj != null || this.f182a == null) ? obj : this.f182a.a(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public void a(String str, Object obj) {
        a.a.a.n.a.a((Object) str, "Id");
        if (obj != null) {
            this.b.put(str, obj);
        } else {
            this.b.remove(str);
        }
    }

    public String toString() {
        return this.b.toString();
    }
}
