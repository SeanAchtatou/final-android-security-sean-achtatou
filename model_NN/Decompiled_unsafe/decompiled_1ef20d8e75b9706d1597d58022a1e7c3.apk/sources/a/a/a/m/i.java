package a.a.a.m;

import a.a.a.q;
import a.a.a.r;
import a.a.a.s;
import a.a.a.u;

public final class i implements g {

    /* renamed from: a  reason: collision with root package name */
    private final r[] f188a;
    private final u[] b;

    public i(r[] rVarArr, u[] uVarArr) {
        if (rVarArr != null) {
            int length = rVarArr.length;
            this.f188a = new r[length];
            System.arraycopy(rVarArr, 0, this.f188a, 0, length);
        } else {
            this.f188a = new r[0];
        }
        if (uVarArr != null) {
            int length2 = uVarArr.length;
            this.b = new u[length2];
            System.arraycopy(uVarArr, 0, this.b, 0, length2);
            return;
        }
        this.b = new u[0];
    }

    public void a(q qVar, e eVar) {
        for (r a2 : this.f188a) {
            a2.a(qVar, eVar);
        }
    }

    public void a(s sVar, e eVar) {
        for (u a2 : this.b) {
            a2.a(sVar, eVar);
        }
    }
}
