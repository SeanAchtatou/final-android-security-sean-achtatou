package a.a.a.g.a;

import a.a.a.n.c;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.List;

abstract class a {
    private static final c c = a(i.f62a, ": ");
    private static final c d = a(i.f62a, "\r\n");
    private static final c e = a(i.f62a, "--");

    /* renamed from: a  reason: collision with root package name */
    final Charset f54a;
    final String b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public a(Charset charset, String str) {
        a.a.a.n.a.a((Object) str, "Multipart boundary");
        this.f54a = charset == null ? i.f62a : charset;
        this.b = str;
    }

    private static c a(Charset charset, String str) {
        ByteBuffer encode = charset.encode(CharBuffer.wrap(str));
        c cVar = new c(encode.remaining());
        cVar.a(encode.array(), encode.position(), encode.remaining());
        return cVar;
    }

    protected static void a(j jVar, OutputStream outputStream) {
        a(jVar.a(), outputStream);
        a(c, outputStream);
        a(jVar.b(), outputStream);
        a(d, outputStream);
    }

    protected static void a(j jVar, Charset charset, OutputStream outputStream) {
        a(jVar.a(), charset, outputStream);
        a(c, outputStream);
        a(jVar.b(), charset, outputStream);
        a(d, outputStream);
    }

    private static void a(c cVar, OutputStream outputStream) {
        outputStream.write(cVar.e(), 0, cVar.d());
    }

    private static void a(String str, OutputStream outputStream) {
        a(a(i.f62a, str), outputStream);
    }

    private static void a(String str, Charset charset, OutputStream outputStream) {
        a(a(charset, str), outputStream);
    }

    public abstract List a();

    /* access modifiers changed from: protected */
    public abstract void a(b bVar, OutputStream outputStream);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.g.a.a.a(java.io.OutputStream, boolean):void
     arg types: [java.io.OutputStream, int]
     candidates:
      a.a.a.g.a.a.a(java.nio.charset.Charset, java.lang.String):a.a.a.n.c
      a.a.a.g.a.a.a(a.a.a.g.a.j, java.io.OutputStream):void
      a.a.a.g.a.a.a(a.a.a.n.c, java.io.OutputStream):void
      a.a.a.g.a.a.a(java.lang.String, java.io.OutputStream):void
      a.a.a.g.a.a.a(a.a.a.g.a.b, java.io.OutputStream):void
      a.a.a.g.a.a.a(java.io.OutputStream, boolean):void */
    public void a(OutputStream outputStream) {
        a(outputStream, true);
    }

    /* access modifiers changed from: package-private */
    public void a(OutputStream outputStream, boolean z) {
        c a2 = a(this.f54a, this.b);
        for (b bVar : a()) {
            a(e, outputStream);
            a(a2, outputStream);
            a(d, outputStream);
            a(bVar, outputStream);
            a(d, outputStream);
            if (z) {
                bVar.a().a(outputStream);
            }
            a(d, outputStream);
        }
        a(e, outputStream);
        a(a2, outputStream);
        a(e, outputStream);
        a(d, outputStream);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.g.a.a.a(java.io.OutputStream, boolean):void
     arg types: [java.io.ByteArrayOutputStream, int]
     candidates:
      a.a.a.g.a.a.a(java.nio.charset.Charset, java.lang.String):a.a.a.n.c
      a.a.a.g.a.a.a(a.a.a.g.a.j, java.io.OutputStream):void
      a.a.a.g.a.a.a(a.a.a.n.c, java.io.OutputStream):void
      a.a.a.g.a.a.a(java.lang.String, java.io.OutputStream):void
      a.a.a.g.a.a.a(a.a.a.g.a.b, java.io.OutputStream):void
      a.a.a.g.a.a.a(java.io.OutputStream, boolean):void */
    public long b() {
        long j = 0;
        for (b a2 : a()) {
            long f = a2.a().f();
            if (f < 0) {
                return -1;
            }
            j = f + j;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a((OutputStream) byteArrayOutputStream, false);
            return ((long) byteArrayOutputStream.toByteArray().length) + j;
        } catch (IOException e2) {
            return -1;
        }
    }
}
