package a.a.a.g.a;

import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.List;

class e extends a {
    private final List c;

    public e(Charset charset, String str, List list) {
        super(charset, str);
        this.c = list;
    }

    public List a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void a(b bVar, OutputStream outputStream) {
        d b = bVar.b();
        a(b.a("Content-Disposition"), this.f54a, outputStream);
        if (bVar.a().d() != null) {
            a(b.a("Content-Type"), this.f54a, outputStream);
        }
    }
}
