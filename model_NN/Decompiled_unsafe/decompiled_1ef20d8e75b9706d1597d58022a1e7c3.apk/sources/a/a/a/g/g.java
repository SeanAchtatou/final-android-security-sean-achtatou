package a.a.a.g;

import a.a.a.m.d;
import a.a.a.n.a;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

public class g extends a implements Cloneable {
    protected final byte[] d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public g(String str, e eVar) {
        a.a((Object) str, "Source string");
        Charset b = eVar != null ? eVar.b() : null;
        this.d = str.getBytes(b == null ? d.f185a : b);
        if (eVar != null) {
            a(eVar.toString());
        }
    }

    public void a(OutputStream outputStream) {
        a.a(outputStream, "Output stream");
        outputStream.write(this.d);
        outputStream.flush();
    }

    public boolean a() {
        return true;
    }

    public long c() {
        return (long) this.d.length;
    }

    public Object clone() {
        return super.clone();
    }

    public InputStream f() {
        return new ByteArrayInputStream(this.d);
    }

    public boolean g() {
        return false;
    }
}
