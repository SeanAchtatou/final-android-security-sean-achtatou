package a.a.a.g;

import a.a.a.e;
import a.a.a.k;
import a.a.a.n.a;
import java.io.InputStream;
import java.io.OutputStream;

public class f implements k {
    protected k c;

    public f(k kVar) {
        this.c = (k) a.a(kVar, "Wrapped entity");
    }

    public void a(OutputStream outputStream) {
        this.c.a(outputStream);
    }

    public boolean a() {
        return this.c.a();
    }

    public boolean b() {
        return this.c.b();
    }

    public long c() {
        return this.c.c();
    }

    public e d() {
        return this.c.d();
    }

    public e e() {
        return this.c.e();
    }

    public InputStream f() {
        return this.c.f();
    }

    public boolean g() {
        return this.c.g();
    }
}
