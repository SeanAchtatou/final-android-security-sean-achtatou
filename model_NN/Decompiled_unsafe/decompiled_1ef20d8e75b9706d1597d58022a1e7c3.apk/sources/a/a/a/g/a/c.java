package a.a.a.g.a;

import a.a.a.g.a.a.a;
import a.a.a.g.e;
import a.a.a.n.b;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private String f59a;
    private a.a.a.g.a.a.c b;
    private final d c;

    c() {
        this.c = new d();
    }

    c(String str, a.a.a.g.a.a.c cVar) {
        this();
        this.f59a = str;
        this.b = cVar;
    }

    public static c a(String str, a.a.a.g.a.a.c cVar) {
        return new c(str, cVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.b.a(java.lang.CharSequence, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.b.a(java.lang.Object, java.lang.String):void
      a.a.a.n.b.a(boolean, java.lang.String):void
      a.a.a.n.b.a(java.lang.CharSequence, java.lang.String):void */
    public b a() {
        b.a((CharSequence) this.f59a, "Name");
        b.a(this.b, "Content body");
        d dVar = new d();
        for (j a2 : this.c.a()) {
            dVar.a(a2);
        }
        if (dVar.a("Content-Disposition") == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("form-data; name=\"");
            sb.append(this.f59a);
            sb.append("\"");
            if (this.b.d() != null) {
                sb.append("; filename=\"");
                sb.append(this.b.d());
                sb.append("\"");
            }
            dVar.a(new j("Content-Disposition", sb.toString()));
        }
        if (dVar.a("Content-Type") == null) {
            e a3 = this.b instanceof a ? ((a) this.b).a() : null;
            if (a3 != null) {
                dVar.a(new j("Content-Type", a3.toString()));
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.b.b());
                if (this.b.c() != null) {
                    sb2.append("; charset=");
                    sb2.append(this.b.c());
                }
                dVar.a(new j("Content-Type", sb2.toString()));
            }
        }
        if (dVar.a("Content-Transfer-Encoding") == null) {
            dVar.a(new j("Content-Transfer-Encoding", this.b.e()));
        }
        return new b(this.f59a, this.b, dVar);
    }
}
