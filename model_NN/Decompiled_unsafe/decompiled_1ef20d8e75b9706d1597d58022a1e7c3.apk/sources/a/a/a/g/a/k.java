package a.a.a.g.a;

import a.a.a.g.a.a.b;
import a.a.a.g.a.a.c;
import a.a.a.g.e;
import a.a.a.j.l;
import a.a.a.n.a;
import a.a.a.y;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f64a = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private e b;
    private f c = f.STRICT;
    private String d = null;
    private Charset e = null;
    private List f = null;

    k() {
    }

    public static k a() {
        return new k();
    }

    private String d() {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int nextInt = random.nextInt(11) + 30;
        for (int i = 0; i < nextInt; i++) {
            sb.append(f64a[random.nextInt(f64a.length)]);
        }
        return sb.toString();
    }

    public k a(b bVar) {
        if (bVar != null) {
            if (this.f == null) {
                this.f = new ArrayList();
            }
            this.f.add(bVar);
        }
        return this;
    }

    public k a(f fVar) {
        this.c = fVar;
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public k a(String str, c cVar) {
        a.a((Object) str, "Name");
        a.a(cVar, "Content body");
        return a(c.a(str, cVar).a());
    }

    public k a(String str, String str2, e eVar) {
        return a(str, new a.a.a.g.a.a.e(str2, eVar));
    }

    public k a(String str, byte[] bArr, e eVar, String str2) {
        return a(str, new b(bArr, eVar, str2));
    }

    public k a(Charset charset) {
        this.e = charset;
        return this;
    }

    /* access modifiers changed from: package-private */
    public m b() {
        a gVar;
        String str = this.d;
        if (str == null && this.b != null) {
            str = this.b.a("boundary");
        }
        String d2 = str == null ? d() : str;
        Charset charset = this.e;
        Charset b2 = (charset != null || this.b == null) ? charset : this.b.b();
        ArrayList arrayList = new ArrayList(2);
        arrayList.add(new l("boundary", d2));
        if (b2 != null) {
            arrayList.add(new l("charset", b2.name()));
        }
        y[] yVarArr = (y[]) arrayList.toArray(new y[arrayList.size()]);
        e a2 = this.b != null ? this.b.a(yVarArr) : e.a("multipart/form-data", yVarArr);
        List arrayList2 = this.f != null ? new ArrayList(this.f) : Collections.emptyList();
        switch (l.f65a[(this.c != null ? this.c : f.STRICT).ordinal()]) {
            case 1:
                gVar = new e(b2, d2, arrayList2);
                break;
            case 2:
                gVar = new g(b2, d2, arrayList2);
                break;
            default:
                gVar = new h(b2, d2, arrayList2);
                break;
        }
        return new m(gVar, a2, gVar.b());
    }

    public a.a.a.k c() {
        return b();
    }
}
