package a.a.a.e.c;

import a.a.a.e.f;
import a.a.a.k.e;
import a.a.a.n.a;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

@Deprecated
public class c implements h {

    /* renamed from: a  reason: collision with root package name */
    private final a f37a = null;

    public static c a() {
        return new c();
    }

    public Socket a(e eVar) {
        return new Socket();
    }

    public Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, e eVar) {
        a.a(inetSocketAddress, "Remote address");
        a.a(eVar, "HTTP parameters");
        if (socket == null) {
            socket = b();
        }
        if (inetSocketAddress2 != null) {
            socket.setReuseAddress(a.a.a.k.c.b(eVar));
            socket.bind(inetSocketAddress2);
        }
        int e = a.a.a.k.c.e(eVar);
        try {
            socket.setSoTimeout(a.a.a.k.c.a(eVar));
            socket.connect(inetSocketAddress, e);
            return socket;
        } catch (SocketTimeoutException e2) {
            throw new f("Connect to " + inetSocketAddress + " timed out");
        }
    }

    public final boolean a(Socket socket) {
        return false;
    }

    public Socket b() {
        return new Socket();
    }
}
