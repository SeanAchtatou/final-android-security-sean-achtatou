package a.a.a.e;

import a.a.a.n;
import a.a.a.n.a;
import java.net.InetAddress;
import java.net.InetSocketAddress;

@Deprecated
public class m extends InetSocketAddress {

    /* renamed from: a  reason: collision with root package name */
    private final n f48a;

    public m(n nVar, InetAddress inetAddress, int i) {
        super(inetAddress, i);
        a.a(nVar, "HTTP host");
        this.f48a = nVar;
    }

    public n a() {
        return this.f48a;
    }

    public String toString() {
        return this.f48a.a() + ":" + getPort();
    }
}
