package a.a.a.e.a;

import a.a.a.e.b.b;
import a.a.a.k.e;
import a.a.a.n;
import java.net.InetAddress;

@Deprecated
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final n f32a = new n("127.0.0.255", 0, "no-host");
    public static final b b = new b(f32a);

    public static n a(e eVar) {
        a.a.a.n.a.a(eVar, "Parameters");
        n nVar = (n) eVar.a("http.route.default-proxy");
        if (nVar == null || !f32a.equals(nVar)) {
            return nVar;
        }
        return null;
    }

    public static b b(e eVar) {
        a.a.a.n.a.a(eVar, "Parameters");
        b bVar = (b) eVar.a("http.route.forced-route");
        if (bVar == null || !b.equals(bVar)) {
            return bVar;
        }
        return null;
    }

    public static InetAddress c(e eVar) {
        a.a.a.n.a.a(eVar, "Parameters");
        return (InetAddress) eVar.a("http.route.local-address");
    }
}
