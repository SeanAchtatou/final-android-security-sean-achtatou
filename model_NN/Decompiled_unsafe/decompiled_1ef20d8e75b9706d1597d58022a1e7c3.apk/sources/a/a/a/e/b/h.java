package a.a.a.e.b;

import a.a.a.n;
import a.a.a.n.a;
import a.a.a.n.b;
import java.net.InetAddress;

public final class h implements e, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final n f36a;
    private final InetAddress b;
    private boolean c;
    private n[] d;
    private g e;
    private f f;
    private boolean g;

    public h(b bVar) {
        this(bVar.a(), bVar.b());
    }

    public h(n nVar, InetAddress inetAddress) {
        a.a(nVar, "Target host");
        this.f36a = nVar;
        this.b = inetAddress;
        this.e = g.PLAIN;
        this.f = f.PLAIN;
    }

    public final n a() {
        return this.f36a;
    }

    public final n a(int i) {
        a.b(i, "Hop index");
        int c2 = c();
        a.a(i < c2, "Hop index exceeds tracked route length");
        return i < c2 + -1 ? this.d[i] : this.f36a;
    }

    public final void a(n nVar, boolean z) {
        a.a(nVar, "Proxy host");
        b.a(!this.c, "Already connected");
        this.c = true;
        this.d = new n[]{nVar};
        this.g = z;
    }

    public final void a(boolean z) {
        b.a(!this.c, "Already connected");
        this.c = true;
        this.g = z;
    }

    public final InetAddress b() {
        return this.b;
    }

    public final void b(n nVar, boolean z) {
        a.a(nVar, "Proxy host");
        b.a(this.c, "No tunnel unless connected");
        b.a(this.d, "No tunnel without proxy");
        n[] nVarArr = new n[(this.d.length + 1)];
        System.arraycopy(this.d, 0, nVarArr, 0, this.d.length);
        nVarArr[nVarArr.length - 1] = nVar;
        this.d = nVarArr;
        this.g = z;
    }

    public final void b(boolean z) {
        b.a(this.c, "No tunnel unless connected");
        b.a(this.d, "No tunnel without proxy");
        this.e = g.TUNNELLED;
        this.g = z;
    }

    public final int c() {
        if (!this.c) {
            return 0;
        }
        if (this.d == null) {
            return 1;
        }
        return this.d.length + 1;
    }

    public final void c(boolean z) {
        b.a(this.c, "No layered protocol unless connected");
        this.f = f.LAYERED;
        this.g = z;
    }

    public Object clone() {
        return super.clone();
    }

    public final n d() {
        if (this.d == null) {
            return null;
        }
        return this.d[0];
    }

    public final boolean e() {
        return this.e == g.TUNNELLED;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        return this.c == hVar.c && this.g == hVar.g && this.e == hVar.e && this.f == hVar.f && a.a.a.n.h.a(this.f36a, hVar.f36a) && a.a.a.n.h.a(this.b, hVar.b) && a.a.a.n.h.a(this.d, hVar.d);
    }

    public final boolean f() {
        return this.f == f.LAYERED;
    }

    public final boolean g() {
        return this.g;
    }

    public void h() {
        this.c = false;
        this.d = null;
        this.e = g.PLAIN;
        this.f = f.PLAIN;
        this.g = false;
    }

    public final int hashCode() {
        int a2 = a.a.a.n.h.a(a.a.a.n.h.a(17, this.f36a), this.b);
        if (this.d != null) {
            n[] nVarArr = this.d;
            int length = nVarArr.length;
            int i = 0;
            while (i < length) {
                int a3 = a.a.a.n.h.a(a2, nVarArr[i]);
                i++;
                a2 = a3;
            }
        }
        return a.a.a.n.h.a(a.a.a.n.h.a(a.a.a.n.h.a(a.a.a.n.h.a(a2, this.c), this.g), this.e), this.f);
    }

    public final boolean i() {
        return this.c;
    }

    public final b j() {
        if (!this.c) {
            return null;
        }
        return new b(this.f36a, this.b, this.d, this.g, this.e, this.f);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((c() * 30) + 50);
        sb.append("RouteTracker[");
        if (this.b != null) {
            sb.append(this.b);
            sb.append("->");
        }
        sb.append('{');
        if (this.c) {
            sb.append('c');
        }
        if (this.e == g.TUNNELLED) {
            sb.append('t');
        }
        if (this.f == f.LAYERED) {
            sb.append('l');
        }
        if (this.g) {
            sb.append('s');
        }
        sb.append("}->");
        if (this.d != null) {
            for (n append : this.d) {
                sb.append(append);
                sb.append("->");
            }
        }
        sb.append(this.f36a);
        sb.append(']');
        return sb.toString();
    }
}
