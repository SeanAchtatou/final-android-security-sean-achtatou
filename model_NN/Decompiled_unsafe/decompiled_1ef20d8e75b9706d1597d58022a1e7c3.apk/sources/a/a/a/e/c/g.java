package a.a.a.e.c;

import a.a.a.n;
import a.a.a.n.a;
import java.util.concurrent.ConcurrentHashMap;

@Deprecated
public final class g {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f40a = new ConcurrentHashMap();

    public final d a(d dVar) {
        a.a(dVar, "Scheme");
        return (d) this.f40a.put(dVar.c(), dVar);
    }

    public final d a(n nVar) {
        a.a(nVar, "Host");
        return a(nVar.c());
    }

    public final d a(String str) {
        d b = b(str);
        if (b != null) {
            return b;
        }
        throw new IllegalStateException("Scheme '" + str + "' not registered.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public final d b(String str) {
        a.a((Object) str, "Scheme name");
        return (d) this.f40a.get(str);
    }
}
