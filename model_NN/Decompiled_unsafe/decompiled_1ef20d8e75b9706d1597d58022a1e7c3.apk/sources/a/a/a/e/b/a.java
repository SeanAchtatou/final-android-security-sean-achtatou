package a.a.a.e.b;

public class a implements c {
    /* access modifiers changed from: protected */
    public int a(e eVar) {
        return eVar.c() > 1 ? 2 : 1;
    }

    public int a(e eVar, e eVar2) {
        a.a.a.n.a.a(eVar, "Planned route");
        return (eVar2 == null || eVar2.c() < 1) ? a(eVar) : eVar.c() > 1 ? c(eVar, eVar2) : b(eVar, eVar2);
    }

    /* access modifiers changed from: protected */
    public int b(e eVar, e eVar2) {
        if (eVar2.c() <= 1 && eVar.a().equals(eVar2.a()) && eVar.g() == eVar2.g()) {
            return (eVar.b() == null || eVar.b().equals(eVar2.b())) ? 0 : -1;
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public int c(e eVar, e eVar2) {
        int c;
        int c2;
        if (eVar2.c() <= 1 || !eVar.a().equals(eVar2.a()) || (c = eVar.c()) < (c2 = eVar2.c())) {
            return -1;
        }
        for (int i = 0; i < c2 - 1; i++) {
            if (!eVar.a(i).equals(eVar2.a(i))) {
                return -1;
            }
        }
        if (c > c2) {
            return 4;
        }
        if (eVar2.e() && !eVar.e()) {
            return -1;
        }
        if (eVar2.f() && !eVar.f()) {
            return -1;
        }
        if (eVar.e() && !eVar2.e()) {
            return 3;
        }
        if (!eVar.f() || eVar2.f()) {
            return eVar.g() == eVar2.g() ? 0 : -1;
        }
        return 5;
    }
}
