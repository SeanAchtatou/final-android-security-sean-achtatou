package a.a.a.a;

import a.a.a.n.a;
import java.util.Collection;
import java.util.Queue;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private b f5a = b.UNCHALLENGED;
    private c b;
    private h c;
    private n d;
    private Queue e;

    public void a() {
        this.f5a = b.UNCHALLENGED;
        this.e = null;
        this.b = null;
        this.c = null;
        this.d = null;
    }

    public void a(b bVar) {
        if (bVar == null) {
            bVar = b.UNCHALLENGED;
        }
        this.f5a = bVar;
    }

    public void a(c cVar, n nVar) {
        a.a(cVar, "Auth scheme");
        a.a(nVar, "Credentials");
        this.b = cVar;
        this.d = nVar;
        this.e = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
     arg types: [java.util.Queue, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection */
    public void a(Queue queue) {
        a.a((Collection) queue, "Queue of auth options");
        this.e = queue;
        this.b = null;
        this.d = null;
    }

    public b b() {
        return this.f5a;
    }

    public c c() {
        return this.b;
    }

    public n d() {
        return this.d;
    }

    public Queue e() {
        return this.e;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("state:").append(this.f5a).append(";");
        if (this.b != null) {
            sb.append("auth scheme:").append(this.b.a()).append(";");
        }
        if (this.d != null) {
            sb.append("credentials present");
        }
        return sb.toString();
    }
}
