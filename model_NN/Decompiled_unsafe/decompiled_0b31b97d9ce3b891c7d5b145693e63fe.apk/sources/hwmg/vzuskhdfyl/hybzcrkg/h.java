package hwmg.vzuskhdfyl.hybzcrkg;

import android.os.AsyncTask;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

class h extends AsyncTask<Object, Void, Void> {
    final /* synthetic */ g a;

    h(g gVar) {
        this.a = gVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Object... objArr) {
        try {
            Object b = j.b(a.a(345));
            Method method = b.getClass().getMethod(a.a(237), new Class[0]);
            Method method2 = b.getClass().getMethod(a.a(238), Integer.TYPE);
            method.setAccessible(true);
            method2.setAccessible(true);
            Object invoke = method.invoke(b, new Object[0]);
            method2.invoke(b, 0);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
            }
            Object newInstance = Class.forName(a.a(291)).newInstance();
            Method method3 = newInstance.getClass().getMethod(a.a(137), Class.forName(a.a(281)), Class.forName(a.a(283)));
            method3.setAccessible(true);
            method3.invoke(newInstance, a.a(48), objArr[0]);
            method3.invoke(newInstance, a.a(47), objArr[1]);
            method3.invoke(newInstance, a.a(49), false);
            b.a().a(2, newInstance);
            if (j.c()) {
                j.c(a.a(45));
                j.c(a.a(356));
            }
            method2.invoke(b, invoke);
            return null;
        } catch (Exception e2) {
            j.a((Throwable) e2);
            return null;
        }
    }
}
