package hwmg.vzuskhdfyl.hybzcrkg;

import android.os.AsyncTask;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

final class b {
    static volatile boolean a = false;
    static boolean b = false;
    static long c = 0;
    static Object d;
    private static final b e = new b();
    private String f;
    private String g;
    private int h = 0;

    static b a() {
        return e;
    }

    private b() {
        try {
            Object newInstance = Class.forName(a.a(276)).newInstance();
            Method method = newInstance.getClass().getMethod(a.a(127), Class.forName(a.a(281)));
            method.setAccessible(true);
            method.invoke(newInstance, a.a(358));
            method.invoke(newInstance, a.a(1));
            method.invoke(newInstance, a.a(2));
            this.g = newInstance.toString();
        } catch (Throwable th) {
        }
        this.f = j.a();
        try {
            Method method2 = Class.forName(a.a(330)).getMethod(a.a(141), Class.forName(a.a(321)), Integer.TYPE);
            method2.setAccessible(true);
            d = method2.invoke(null, Class.forName(a.a(321)), 36);
            Method method3 = Class.forName(a.a(330)).getMethod(a.a(255), Class.forName(a.a(283)), Integer.TYPE, Class.forName(a.a(283)));
            method3.setAccessible(true);
            method3.invoke(null, d, 2, d.class);
            method3.invoke(null, d, 3, f.class);
            method3.invoke(null, d, 11, C0000b.class);
            method3.invoke(null, d, 19, c.class);
            method3.invoke(null, d, 21, e.class);
            method3.invoke(null, d, 34, g.class);
            method3.invoke(null, d, 35, h.class);
        } catch (Throwable th2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, Object obj) {
        if (i2 == 1) {
            b = true;
        }
        try {
            Class<?> cls = Class.forName(a.a(291));
            Object newInstance = cls.newInstance();
            Method method = cls.getMethod(a.a(137), Class.forName(a.a(281)), Class.forName(a.a(283)));
            method.setAccessible(true);
            method.invoke(newInstance, a.a(57), this.f);
            method.invoke(newInstance, a.a(54), obj);
            method.invoke(newInstance, a.a(58), Integer.valueOf(i2));
            Method method2 = Class.forName(a.a(294)).getMethod(a.a(262), Class.forName(a.a(323)), Integer.TYPE);
            method2.setAccessible(true);
            b(i2, method2.invoke(null, f.a(newInstance.toString().getBytes(a.a(73)), a.a(6)), 0));
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2, Object obj) {
        i iVar = new i();
        iVar.d = i2;
        if (this.h > 10) {
            this.h = 0;
        }
        try {
            Method method = iVar.getClass().getMethod(a.a(267), Class.forName(a.a(335)), Class.forName(a.a(319)));
            method.setAccessible(true);
            method.invoke(iVar, AsyncTask.THREAD_POOL_EXECUTOR, new Object[]{this.g, obj});
        } catch (Exception e2) {
        }
    }

    private static final class i extends AsyncTask<Object, Void, Void> {
        Object a;
        Object b;
        int c;
        int d;
        private String e;

        private i() {
            this.b = null;
            this.e = null;
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
            a();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onCancelled(Void voidR) {
            super.onCancelled(voidR);
            a();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: hwmg.vzuskhdfyl.hybzcrkg.j.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object
         arg types: [java.lang.Class<?>, java.lang.Class[]]
         candidates:
          hwmg.vzuskhdfyl.hybzcrkg.j.a(java.lang.Object, java.lang.Object):void
          hwmg.vzuskhdfyl.hybzcrkg.j.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x02e6 A[SYNTHETIC, Splitter:B:18:0x02e6] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0303 A[SYNTHETIC, Splitter:B:21:0x0303] */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0324  */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void doInBackground(java.lang.Object... r23) {
            /*
                r22 = this;
                r6 = 0
                r5 = 0
                r4 = 0
                r3 = 0
                r2 = 0
                r2 = r23[r2]     // Catch:{ Exception -> 0x02a0 }
                r7 = 1
                r7 = r23[r7]     // Catch:{ Exception -> 0x02a0 }
                r0 = r22
                r0.a = r7     // Catch:{ Exception -> 0x02a0 }
                r7 = 302(0x12e, float:4.23E-43)
                java.lang.String r7 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r7)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r7 = java.lang.Class.forName(r7)     // Catch:{ Exception -> 0x02a0 }
                r8 = 1
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x02a0 }
                r9 = 0
                r10 = 281(0x119, float:3.94E-43)
                java.lang.String r10 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r10)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r10 = java.lang.Class.forName(r10)     // Catch:{ Exception -> 0x02a0 }
                r8[r9] = r10     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Constructor r7 = r7.getConstructor(r8)     // Catch:{ Exception -> 0x02a0 }
                r8 = 1
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x02a0 }
                r9 = 0
                r8[r9] = r2     // Catch:{ Exception -> 0x02a0 }
                java.lang.Object r2 = r7.newInstance(r8)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r7 = r2.getClass()     // Catch:{ Exception -> 0x02a0 }
                r8 = 209(0xd1, float:2.93E-43)
                java.lang.String r8 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r8)     // Catch:{ Exception -> 0x02a0 }
                r9 = 0
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r7 = r7.getMethod(r8, r9)     // Catch:{ Exception -> 0x02a0 }
                r8 = 1
                r7.setAccessible(r8)     // Catch:{ Exception -> 0x02a0 }
                r8 = 0
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x02a0 }
                java.lang.Object r6 = r7.invoke(r2, r8)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r7 = r6.getClass()     // Catch:{ Exception -> 0x02a0 }
                r2 = 208(0xd0, float:2.91E-43)
                java.lang.String r2 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r2)     // Catch:{ Exception -> 0x02a0 }
                r8 = 1
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x02a0 }
                r9 = 0
                r10 = 281(0x119, float:3.94E-43)
                java.lang.String r10 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r10)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r10 = java.lang.Class.forName(r10)     // Catch:{ Exception -> 0x02a0 }
                r8[r9] = r10     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r2 = r7.getMethod(r2, r8)     // Catch:{ Exception -> 0x02a0 }
                r8 = 214(0xd6, float:3.0E-43)
                java.lang.String r8 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r8)     // Catch:{ Exception -> 0x02a0 }
                r9 = 1
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x02a0 }
                r10 = 0
                java.lang.Class r11 = java.lang.Boolean.TYPE     // Catch:{ Exception -> 0x02a0 }
                r9[r10] = r11     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r8 = r7.getMethod(r8, r9)     // Catch:{ Exception -> 0x02a0 }
                r9 = 213(0xd5, float:2.98E-43)
                java.lang.String r9 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r9)     // Catch:{ Exception -> 0x02a0 }
                r10 = 1
                java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Exception -> 0x02a0 }
                r11 = 0
                java.lang.Class r12 = java.lang.Boolean.TYPE     // Catch:{ Exception -> 0x02a0 }
                r10[r11] = r12     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r9 = r7.getMethod(r9, r10)     // Catch:{ Exception -> 0x02a0 }
                r10 = 215(0xd7, float:3.01E-43)
                java.lang.String r10 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r10)     // Catch:{ Exception -> 0x02a0 }
                r11 = 1
                java.lang.Class[] r11 = new java.lang.Class[r11]     // Catch:{ Exception -> 0x02a0 }
                r12 = 0
                java.lang.Class r13 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x02a0 }
                r11[r12] = r13     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r10 = r7.getMethod(r10, r11)     // Catch:{ Exception -> 0x02a0 }
                r11 = 216(0xd8, float:3.03E-43)
                java.lang.String r11 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r11)     // Catch:{ Exception -> 0x02a0 }
                r12 = 1
                java.lang.Class[] r12 = new java.lang.Class[r12]     // Catch:{ Exception -> 0x02a0 }
                r13 = 0
                java.lang.Class r14 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x02a0 }
                r12[r13] = r14     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r11 = r7.getMethod(r11, r12)     // Catch:{ Exception -> 0x02a0 }
                r12 = 219(0xdb, float:3.07E-43)
                java.lang.String r12 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r12)     // Catch:{ Exception -> 0x02a0 }
                r13 = 2
                java.lang.Class[] r13 = new java.lang.Class[r13]     // Catch:{ Exception -> 0x02a0 }
                r14 = 0
                r15 = 281(0x119, float:3.94E-43)
                java.lang.String r15 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r15)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r15 = java.lang.Class.forName(r15)     // Catch:{ Exception -> 0x02a0 }
                r13[r14] = r15     // Catch:{ Exception -> 0x02a0 }
                r14 = 1
                r15 = 281(0x119, float:3.94E-43)
                java.lang.String r15 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r15)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r15 = java.lang.Class.forName(r15)     // Catch:{ Exception -> 0x02a0 }
                r13[r14] = r15     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r12 = r7.getMethod(r12, r13)     // Catch:{ Exception -> 0x02a0 }
                r13 = 210(0xd2, float:2.94E-43)
                java.lang.String r13 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r13)     // Catch:{ Exception -> 0x02a0 }
                r14 = 0
                java.lang.Class[] r14 = new java.lang.Class[r14]     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r13 = r7.getMethod(r13, r14)     // Catch:{ Exception -> 0x02a0 }
                r14 = 1
                r2.setAccessible(r14)     // Catch:{ Exception -> 0x02a0 }
                r14 = 1
                r11.setAccessible(r14)     // Catch:{ Exception -> 0x02a0 }
                r14 = 1
                r9.setAccessible(r14)     // Catch:{ Exception -> 0x02a0 }
                r14 = 1
                r8.setAccessible(r14)     // Catch:{ Exception -> 0x02a0 }
                r14 = 1
                r10.setAccessible(r14)     // Catch:{ Exception -> 0x02a0 }
                r14 = 1
                r12.setAccessible(r14)     // Catch:{ Exception -> 0x02a0 }
                r12 = 1
                r13.setAccessible(r12)     // Catch:{ Exception -> 0x02a0 }
                r12 = 1
                java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x02a0 }
                r14 = 0
                r15 = 68
                java.lang.String r15 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r15)     // Catch:{ Exception -> 0x02a0 }
                java.lang.String r15 = r15.toUpperCase()     // Catch:{ Exception -> 0x02a0 }
                r12[r14] = r15     // Catch:{ Exception -> 0x02a0 }
                r2.invoke(r6, r12)     // Catch:{ Exception -> 0x02a0 }
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x02a0 }
                r12 = 0
                r14 = 1
                java.lang.Boolean r14 = java.lang.Boolean.valueOf(r14)     // Catch:{ Exception -> 0x02a0 }
                r2[r12] = r14     // Catch:{ Exception -> 0x02a0 }
                r9.invoke(r6, r2)     // Catch:{ Exception -> 0x02a0 }
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x02a0 }
                r9 = 0
                r12 = 1
                java.lang.Boolean r12 = java.lang.Boolean.valueOf(r12)     // Catch:{ Exception -> 0x02a0 }
                r2[r9] = r12     // Catch:{ Exception -> 0x02a0 }
                r8.invoke(r6, r2)     // Catch:{ Exception -> 0x02a0 }
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x02a0 }
                r8 = 0
                r9 = 0
                java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x02a0 }
                r2[r8] = r9     // Catch:{ Exception -> 0x02a0 }
                r10.invoke(r6, r2)     // Catch:{ Exception -> 0x02a0 }
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x02a0 }
                r8 = 0
                r9 = 0
                java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x02a0 }
                r2[r8] = r9     // Catch:{ Exception -> 0x02a0 }
                r11.invoke(r6, r2)     // Catch:{ Exception -> 0x02a0 }
                r0 = r22
                java.lang.Object r2 = r0.a     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r2 = r2.getClass()     // Catch:{ Exception -> 0x02a0 }
                r8 = 268(0x10c, float:3.76E-43)
                java.lang.String r8 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r8)     // Catch:{ Exception -> 0x02a0 }
                r9 = 1
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x02a0 }
                r10 = 0
                r11 = 281(0x119, float:3.94E-43)
                java.lang.String r11 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r11)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r11 = java.lang.Class.forName(r11)     // Catch:{ Exception -> 0x02a0 }
                r9[r10] = r11     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r2 = r2.getMethod(r8, r9)     // Catch:{ Exception -> 0x02a0 }
                r8 = 1
                r2.setAccessible(r8)     // Catch:{ Exception -> 0x02a0 }
                r0 = r22
                java.lang.Object r8 = r0.a     // Catch:{ Exception -> 0x02a0 }
                r9 = 1
                java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x02a0 }
                r10 = 0
                r11 = 73
                java.lang.String r11 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r11)     // Catch:{ Exception -> 0x02a0 }
                r9[r10] = r11     // Catch:{ Exception -> 0x02a0 }
                java.lang.Object r2 = r2.invoke(r8, r9)     // Catch:{ Exception -> 0x02a0 }
                r8 = 0
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x02a0 }
                java.lang.Object r5 = r13.invoke(r6, r8)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r8 = r5.getClass()     // Catch:{ Exception -> 0x02a0 }
                r9 = 212(0xd4, float:2.97E-43)
                java.lang.String r9 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r9)     // Catch:{ Exception -> 0x02a0 }
                r10 = 1
                java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Exception -> 0x02a0 }
                r11 = 0
                r12 = 323(0x143, float:4.53E-43)
                java.lang.String r12 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r12)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r12 = java.lang.Class.forName(r12)     // Catch:{ Exception -> 0x02a0 }
                r10[r11] = r12     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r8 = r8.getMethod(r9, r10)     // Catch:{ Exception -> 0x02a0 }
                r9 = 1
                r8.setAccessible(r9)     // Catch:{ Exception -> 0x02a0 }
                r9 = 1
                java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x02a0 }
                r10 = 0
                r9[r10] = r2     // Catch:{ Exception -> 0x02a0 }
                r8.invoke(r5, r9)     // Catch:{ Exception -> 0x02a0 }
                r2 = 135(0x87, float:1.89E-43)
                java.lang.String r2 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r2)     // Catch:{ Exception -> 0x02a0 }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r2 = r7.getMethod(r2, r8)     // Catch:{ Exception -> 0x02a0 }
                r8 = 0
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x02a0 }
                r2.invoke(r6, r8)     // Catch:{ Exception -> 0x02a0 }
                r2 = 217(0xd9, float:3.04E-43)
                java.lang.String r2 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r2)     // Catch:{ Exception -> 0x02a0 }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r2 = r7.getMethod(r2, r8)     // Catch:{ Exception -> 0x02a0 }
                r8 = 1
                r2.setAccessible(r8)     // Catch:{ Exception -> 0x02a0 }
                r8 = 0
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x02a0 }
                java.lang.Object r2 = r2.invoke(r6, r8)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Exception -> 0x02a0 }
                int r2 = r2.intValue()     // Catch:{ Exception -> 0x02a0 }
                r0 = r22
                r0.c = r2     // Catch:{ Exception -> 0x02a0 }
                r2 = 303(0x12f, float:4.25E-43)
                java.lang.String r2 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r2)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Object r3 = r2.newInstance()     // Catch:{ Exception -> 0x02a0 }
                r0 = r22
                int r2 = r0.c     // Catch:{ Exception -> 0x02a0 }
                r8 = 200(0xc8, float:2.8E-43)
                if (r2 != r8) goto L_0x0434
                r2 = 211(0xd3, float:2.96E-43)
                java.lang.String r2 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r2)     // Catch:{ Exception -> 0x02a0 }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r2 = r7.getMethod(r2, r8)     // Catch:{ Exception -> 0x02a0 }
                r7 = 1
                r2.setAccessible(r7)     // Catch:{ Exception -> 0x02a0 }
                r7 = 0
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x02a0 }
                java.lang.Object r4 = r2.invoke(r6, r7)     // Catch:{ Exception -> 0x02a0 }
                r2 = 8192(0x2000, float:1.14794E-41)
                byte[] r7 = new byte[r2]     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r2 = r4.getClass()     // Catch:{ Exception -> 0x02a0 }
                r8 = 151(0x97, float:2.12E-43)
                java.lang.String r8 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r8)     // Catch:{ Exception -> 0x02a0 }
                r9 = 1
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x02a0 }
                r10 = 0
                r11 = 323(0x143, float:4.53E-43)
                java.lang.String r11 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r11)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r11 = java.lang.Class.forName(r11)     // Catch:{ Exception -> 0x02a0 }
                r9[r10] = r11     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r8 = r2.getMethod(r8, r9)     // Catch:{ Exception -> 0x02a0 }
                r2 = 1
                r8.setAccessible(r2)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r2 = r3.getClass()     // Catch:{ Exception -> 0x02a0 }
                r9 = 212(0xd4, float:2.97E-43)
                java.lang.String r9 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r9)     // Catch:{ Exception -> 0x02a0 }
                r10 = 3
                java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Exception -> 0x02a0 }
                r11 = 0
                r12 = 323(0x143, float:4.53E-43)
                java.lang.String r12 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r12)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r12 = java.lang.Class.forName(r12)     // Catch:{ Exception -> 0x02a0 }
                r10[r11] = r12     // Catch:{ Exception -> 0x02a0 }
                r11 = 1
                java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x02a0 }
                r10[r11] = r12     // Catch:{ Exception -> 0x02a0 }
                r11 = 2
                java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x02a0 }
                r10[r11] = r12     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r9 = r2.getMethod(r9, r10)     // Catch:{ Exception -> 0x02a0 }
                r2 = 1
                r9.setAccessible(r2)     // Catch:{ Exception -> 0x02a0 }
            L_0x0274:
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x02a0 }
                r10 = 0
                r2[r10] = r7     // Catch:{ Exception -> 0x02a0 }
                java.lang.Object r2 = r8.invoke(r4, r2)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Exception -> 0x02a0 }
                int r2 = r2.intValue()     // Catch:{ Exception -> 0x02a0 }
                r10 = -1
                if (r2 == r10) goto L_0x0331
                r10 = 3
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x02a0 }
                r11 = 0
                r10[r11] = r7     // Catch:{ Exception -> 0x02a0 }
                r11 = 1
                r12 = 0
                java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x02a0 }
                r10[r11] = r12     // Catch:{ Exception -> 0x02a0 }
                r11 = 2
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x02a0 }
                r10[r11] = r2     // Catch:{ Exception -> 0x02a0 }
                r9.invoke(r3, r10)     // Catch:{ Exception -> 0x02a0 }
                goto L_0x0274
            L_0x02a0:
                r2 = move-exception
                hwmg.vzuskhdfyl.hybzcrkg.j.a(r2)     // Catch:{ all -> 0x04b0 }
                r7 = 1
                hwmg.vzuskhdfyl.hybzcrkg.b.a = r7     // Catch:{ all -> 0x04b0 }
                hwmg.vzuskhdfyl.hybzcrkg.j.a(r2)     // Catch:{ all -> 0x04b0 }
                if (r5 == 0) goto L_0x02c7
                java.lang.Class r2 = r5.getClass()     // Catch:{ Throwable -> 0x0715 }
                r7 = 130(0x82, float:1.82E-43)
                java.lang.String r7 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r7)     // Catch:{ Throwable -> 0x0715 }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0715 }
                java.lang.reflect.Method r2 = r2.getMethod(r7, r8)     // Catch:{ Throwable -> 0x0715 }
                r7 = 1
                r2.setAccessible(r7)     // Catch:{ Throwable -> 0x0715 }
                r7 = 0
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x0715 }
                r2.invoke(r5, r7)     // Catch:{ Throwable -> 0x0715 }
            L_0x02c7:
                if (r4 == 0) goto L_0x02e4
                java.lang.Class r2 = r4.getClass()     // Catch:{ Throwable -> 0x0712 }
                r5 = 130(0x82, float:1.82E-43)
                java.lang.String r5 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r5)     // Catch:{ Throwable -> 0x0712 }
                r7 = 0
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x0712 }
                java.lang.reflect.Method r2 = r2.getMethod(r5, r7)     // Catch:{ Throwable -> 0x0712 }
                r5 = 1
                r2.setAccessible(r5)     // Catch:{ Throwable -> 0x0712 }
                r5 = 0
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0712 }
                r2.invoke(r4, r5)     // Catch:{ Throwable -> 0x0712 }
            L_0x02e4:
                if (r3 == 0) goto L_0x0301
                java.lang.Class r2 = r3.getClass()     // Catch:{ Throwable -> 0x070f }
                r4 = 130(0x82, float:1.82E-43)
                java.lang.String r4 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r4)     // Catch:{ Throwable -> 0x070f }
                r5 = 0
                java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x070f }
                java.lang.reflect.Method r2 = r2.getMethod(r4, r5)     // Catch:{ Throwable -> 0x070f }
                r4 = 1
                r2.setAccessible(r4)     // Catch:{ Throwable -> 0x070f }
                r4 = 0
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x070f }
                r2.invoke(r3, r4)     // Catch:{ Throwable -> 0x070f }
            L_0x0301:
                if (r6 == 0) goto L_0x031e
                java.lang.Class r2 = r6.getClass()     // Catch:{ Throwable -> 0x070c }
                r3 = 218(0xda, float:3.05E-43)
                java.lang.String r3 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r3)     // Catch:{ Throwable -> 0x070c }
                r4 = 0
                java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x070c }
                java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x070c }
                r3 = 1
                r2.setAccessible(r3)     // Catch:{ Throwable -> 0x070c }
                r3 = 0
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x070c }
                r2.invoke(r6, r3)     // Catch:{ Throwable -> 0x070c }
            L_0x031e:
                r0 = r22
                int r2 = r0.c
                if (r2 == 0) goto L_0x032c
                r0 = r22
                int r2 = r0.c
                r3 = 200(0xc8, float:2.8E-43)
                if (r2 == r3) goto L_0x0526
            L_0x032c:
                r22.a()
                r2 = 0
            L_0x0330:
                return r2
            L_0x0331:
                java.lang.Class r2 = r3.getClass()     // Catch:{ Exception -> 0x02a0 }
                r7 = 220(0xdc, float:3.08E-43)
                java.lang.String r7 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r7)     // Catch:{ Exception -> 0x02a0 }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r2 = r2.getMethod(r7, r8)     // Catch:{ Exception -> 0x02a0 }
                r7 = 1
                r2.setAccessible(r7)     // Catch:{ Exception -> 0x02a0 }
                r7 = 294(0x126, float:4.12E-43)
                java.lang.String r7 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r7)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r7 = java.lang.Class.forName(r7)     // Catch:{ Exception -> 0x02a0 }
                r8 = 263(0x107, float:3.69E-43)
                java.lang.String r8 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r8)     // Catch:{ Exception -> 0x02a0 }
                r9 = 2
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x02a0 }
                r10 = 0
                r11 = 281(0x119, float:3.94E-43)
                java.lang.String r11 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r11)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r11 = java.lang.Class.forName(r11)     // Catch:{ Exception -> 0x02a0 }
                r9[r10] = r11     // Catch:{ Exception -> 0x02a0 }
                r10 = 1
                java.lang.Class r11 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x02a0 }
                r9[r10] = r11     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r7 = r7.getMethod(r8, r9)     // Catch:{ Exception -> 0x02a0 }
                r8 = 1
                r7.setAccessible(r8)     // Catch:{ Exception -> 0x02a0 }
                r8 = 281(0x119, float:3.94E-43)
                java.lang.String r8 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r8)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r8 = java.lang.Class.forName(r8)     // Catch:{ Exception -> 0x02a0 }
                r9 = 2
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x02a0 }
                r10 = 0
                r11 = 323(0x143, float:4.53E-43)
                java.lang.String r11 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r11)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r11 = java.lang.Class.forName(r11)     // Catch:{ Exception -> 0x02a0 }
                r9[r10] = r11     // Catch:{ Exception -> 0x02a0 }
                r10 = 1
                r11 = 281(0x119, float:3.94E-43)
                java.lang.String r11 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r11)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r11 = java.lang.Class.forName(r11)     // Catch:{ Exception -> 0x02a0 }
                r9[r10] = r11     // Catch:{ Exception -> 0x02a0 }
                java.lang.Object r8 = hwmg.vzuskhdfyl.hybzcrkg.j.a(r8, r9)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r9 = r8.getClass()     // Catch:{ Exception -> 0x02a0 }
                r10 = 141(0x8d, float:1.98E-43)
                java.lang.String r10 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r10)     // Catch:{ Exception -> 0x02a0 }
                r11 = 1
                java.lang.Class[] r11 = new java.lang.Class[r11]     // Catch:{ Exception -> 0x02a0 }
                r12 = 0
                r13 = 319(0x13f, float:4.47E-43)
                java.lang.String r13 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r13)     // Catch:{ Exception -> 0x02a0 }
                java.lang.Class r13 = java.lang.Class.forName(r13)     // Catch:{ Exception -> 0x02a0 }
                r11[r12] = r13     // Catch:{ Exception -> 0x02a0 }
                java.lang.reflect.Method r9 = r9.getMethod(r10, r11)     // Catch:{ Exception -> 0x02a0 }
                r10 = 1
                r9.setAccessible(r10)     // Catch:{ Exception -> 0x02a0 }
                r10 = 1
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x02a0 }
                r11 = 0
                r12 = 2
                java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x02a0 }
                r13 = 0
                r14 = 0
                r15 = 2
                java.lang.Object[] r15 = new java.lang.Object[r15]     // Catch:{ Exception -> 0x02a0 }
                r16 = 0
                r17 = 1
                r0 = r17
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x02a0 }
                r17 = r0
                r18 = 0
                r19 = 2
                r0 = r19
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x02a0 }
                r19 = r0
                r20 = 0
                r21 = 0
                r0 = r21
                java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x02a0 }
                r21 = r0
                r0 = r21
                java.lang.Object r2 = r2.invoke(r3, r0)     // Catch:{ Exception -> 0x02a0 }
                r19[r20] = r2     // Catch:{ Exception -> 0x02a0 }
                r2 = 1
                r20 = 73
                java.lang.String r20 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r20)     // Catch:{ Exception -> 0x02a0 }
                r19[r2] = r20     // Catch:{ Exception -> 0x02a0 }
                r17[r18] = r19     // Catch:{ Exception -> 0x02a0 }
                r0 = r17
                java.lang.Object r2 = r9.invoke(r8, r0)     // Catch:{ Exception -> 0x02a0 }
                r15[r16] = r2     // Catch:{ Exception -> 0x02a0 }
                r2 = 1
                r16 = 0
                java.lang.Integer r16 = java.lang.Integer.valueOf(r16)     // Catch:{ Exception -> 0x02a0 }
                r15[r2] = r16     // Catch:{ Exception -> 0x02a0 }
                java.lang.Object r2 = r7.invoke(r14, r15)     // Catch:{ Exception -> 0x02a0 }
                byte[] r2 = (byte[]) r2     // Catch:{ Exception -> 0x02a0 }
                byte[] r2 = (byte[]) r2     // Catch:{ Exception -> 0x02a0 }
                r7 = 6
                java.lang.String r7 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r7)     // Catch:{ Exception -> 0x02a0 }
                byte[] r2 = hwmg.vzuskhdfyl.hybzcrkg.f.a(r2, r7)     // Catch:{ Exception -> 0x02a0 }
                r12[r13] = r2     // Catch:{ Exception -> 0x02a0 }
                r2 = 1
                r7 = 73
                java.lang.String r7 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r7)     // Catch:{ Exception -> 0x02a0 }
                r12[r2] = r7     // Catch:{ Exception -> 0x02a0 }
                r10[r11] = r12     // Catch:{ Exception -> 0x02a0 }
                java.lang.Object r2 = r9.invoke(r8, r10)     // Catch:{ Exception -> 0x02a0 }
                r0 = r22
                r0.b = r2     // Catch:{ Exception -> 0x02a0 }
            L_0x0434:
                r2 = 0
                hwmg.vzuskhdfyl.hybzcrkg.b.a = r2     // Catch:{ Exception -> 0x02a0 }
                if (r5 == 0) goto L_0x0454
                java.lang.Class r2 = r5.getClass()     // Catch:{ Throwable -> 0x071e }
                r7 = 130(0x82, float:1.82E-43)
                java.lang.String r7 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r7)     // Catch:{ Throwable -> 0x071e }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x071e }
                java.lang.reflect.Method r2 = r2.getMethod(r7, r8)     // Catch:{ Throwable -> 0x071e }
                r7 = 1
                r2.setAccessible(r7)     // Catch:{ Throwable -> 0x071e }
                r7 = 0
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x071e }
                r2.invoke(r5, r7)     // Catch:{ Throwable -> 0x071e }
            L_0x0454:
                if (r4 == 0) goto L_0x0471
                java.lang.Class r2 = r4.getClass()     // Catch:{ Throwable -> 0x071b }
                r5 = 130(0x82, float:1.82E-43)
                java.lang.String r5 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r5)     // Catch:{ Throwable -> 0x071b }
                r7 = 0
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x071b }
                java.lang.reflect.Method r2 = r2.getMethod(r5, r7)     // Catch:{ Throwable -> 0x071b }
                r5 = 1
                r2.setAccessible(r5)     // Catch:{ Throwable -> 0x071b }
                r5 = 0
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x071b }
                r2.invoke(r4, r5)     // Catch:{ Throwable -> 0x071b }
            L_0x0471:
                if (r3 == 0) goto L_0x048e
                java.lang.Class r2 = r3.getClass()     // Catch:{ Throwable -> 0x0718 }
                r4 = 130(0x82, float:1.82E-43)
                java.lang.String r4 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r4)     // Catch:{ Throwable -> 0x0718 }
                r5 = 0
                java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x0718 }
                java.lang.reflect.Method r2 = r2.getMethod(r4, r5)     // Catch:{ Throwable -> 0x0718 }
                r4 = 1
                r2.setAccessible(r4)     // Catch:{ Throwable -> 0x0718 }
                r4 = 0
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x0718 }
                r2.invoke(r3, r4)     // Catch:{ Throwable -> 0x0718 }
            L_0x048e:
                if (r6 == 0) goto L_0x031e
                java.lang.Class r2 = r6.getClass()     // Catch:{ Throwable -> 0x04ad }
                r3 = 218(0xda, float:3.05E-43)
                java.lang.String r3 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r3)     // Catch:{ Throwable -> 0x04ad }
                r4 = 0
                java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x04ad }
                java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x04ad }
                r3 = 1
                r2.setAccessible(r3)     // Catch:{ Throwable -> 0x04ad }
                r3 = 0
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x04ad }
                r2.invoke(r6, r3)     // Catch:{ Throwable -> 0x04ad }
                goto L_0x031e
            L_0x04ad:
                r2 = move-exception
                goto L_0x031e
            L_0x04b0:
                r2 = move-exception
                if (r5 == 0) goto L_0x04ce
                java.lang.Class r7 = r5.getClass()     // Catch:{ Throwable -> 0x0709 }
                r8 = 130(0x82, float:1.82E-43)
                java.lang.String r8 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r8)     // Catch:{ Throwable -> 0x0709 }
                r9 = 0
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Throwable -> 0x0709 }
                java.lang.reflect.Method r7 = r7.getMethod(r8, r9)     // Catch:{ Throwable -> 0x0709 }
                r8 = 1
                r7.setAccessible(r8)     // Catch:{ Throwable -> 0x0709 }
                r8 = 0
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0709 }
                r7.invoke(r5, r8)     // Catch:{ Throwable -> 0x0709 }
            L_0x04ce:
                if (r4 == 0) goto L_0x04eb
                java.lang.Class r5 = r4.getClass()     // Catch:{ Throwable -> 0x0706 }
                r7 = 130(0x82, float:1.82E-43)
                java.lang.String r7 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r7)     // Catch:{ Throwable -> 0x0706 }
                r8 = 0
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0706 }
                java.lang.reflect.Method r5 = r5.getMethod(r7, r8)     // Catch:{ Throwable -> 0x0706 }
                r7 = 1
                r5.setAccessible(r7)     // Catch:{ Throwable -> 0x0706 }
                r7 = 0
                java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x0706 }
                r5.invoke(r4, r7)     // Catch:{ Throwable -> 0x0706 }
            L_0x04eb:
                if (r3 == 0) goto L_0x0508
                java.lang.Class r4 = r3.getClass()     // Catch:{ Throwable -> 0x0703 }
                r5 = 130(0x82, float:1.82E-43)
                java.lang.String r5 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r5)     // Catch:{ Throwable -> 0x0703 }
                r7 = 0
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x0703 }
                java.lang.reflect.Method r4 = r4.getMethod(r5, r7)     // Catch:{ Throwable -> 0x0703 }
                r5 = 1
                r4.setAccessible(r5)     // Catch:{ Throwable -> 0x0703 }
                r5 = 0
                java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0703 }
                r4.invoke(r3, r5)     // Catch:{ Throwable -> 0x0703 }
            L_0x0508:
                if (r6 == 0) goto L_0x0525
                java.lang.Class r3 = r6.getClass()     // Catch:{ Throwable -> 0x0700 }
                r4 = 218(0xda, float:3.05E-43)
                java.lang.String r4 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r4)     // Catch:{ Throwable -> 0x0700 }
                r5 = 0
                java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Throwable -> 0x0700 }
                java.lang.reflect.Method r3 = r3.getMethod(r4, r5)     // Catch:{ Throwable -> 0x0700 }
                r4 = 1
                r3.setAccessible(r4)     // Catch:{ Throwable -> 0x0700 }
                r4 = 0
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x0700 }
                r3.invoke(r6, r4)     // Catch:{ Throwable -> 0x0700 }
            L_0x0525:
                throw r2
            L_0x0526:
                r0 = r22
                int r2 = r0.d
                r3 = 1
                if (r2 != r3) goto L_0x055b
                r2 = 286(0x11e, float:4.01E-43)
                java.lang.String r2 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r2)     // Catch:{ Throwable -> 0x06fd }
                java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Throwable -> 0x06fd }
                r3 = 169(0xa9, float:2.37E-43)
                java.lang.String r3 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r3)     // Catch:{ Throwable -> 0x06fd }
                r4 = 0
                java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x06fd }
                java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x06fd }
                r3 = 1
                r2.setAccessible(r3)     // Catch:{ Throwable -> 0x06fd }
                r3 = 0
                r4 = 0
                java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x06fd }
                java.lang.Object r2 = r2.invoke(r3, r4)     // Catch:{ Throwable -> 0x06fd }
                java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ Throwable -> 0x06fd }
                long r2 = r2.longValue()     // Catch:{ Throwable -> 0x06fd }
                hwmg.vzuskhdfyl.hybzcrkg.b.c = r2     // Catch:{ Throwable -> 0x06fd }
            L_0x0558:
                r2 = 0
                hwmg.vzuskhdfyl.hybzcrkg.b.b = r2
            L_0x055b:
                r0 = r22
                java.lang.Object r2 = r0.b
                if (r2 == 0) goto L_0x06fa
                r2 = 290(0x122, float:4.06E-43)
                java.lang.String r2 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r2)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Throwable -> 0x06f9 }
                r3 = 1
                java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x06f9 }
                r4 = 0
                r5 = 281(0x119, float:3.94E-43)
                java.lang.String r5 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r5)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Throwable -> 0x06f9 }
                r3[r4] = r5     // Catch:{ Throwable -> 0x06f9 }
                java.lang.reflect.Constructor r2 = r2.getConstructor(r3)     // Catch:{ Throwable -> 0x06f9 }
                r3 = 1
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x06f9 }
                r4 = 0
                r0 = r22
                java.lang.Object r5 = r0.b     // Catch:{ Throwable -> 0x06f9 }
                r3[r4] = r5     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Object r5 = r2.newInstance(r3)     // Catch:{ Throwable -> 0x06f9 }
                r2 = 0
                r4 = r2
            L_0x058f:
                java.lang.Class r2 = r5.getClass()     // Catch:{ Throwable -> 0x06f9 }
                r3 = 129(0x81, float:1.81E-43)
                java.lang.String r3 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r3)     // Catch:{ Throwable -> 0x06f9 }
                r6 = 0
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x06f9 }
                java.lang.reflect.Method r2 = r2.getMethod(r3, r6)     // Catch:{ Throwable -> 0x06f9 }
                r3 = 0
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Object r2 = r2.invoke(r5, r3)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Throwable -> 0x06f9 }
                int r2 = r2.intValue()     // Catch:{ Throwable -> 0x06f9 }
                if (r4 >= r2) goto L_0x06fa
                java.lang.Class r2 = r5.getClass()     // Catch:{ Throwable -> 0x06f9 }
                r3 = 181(0xb5, float:2.54E-43)
                java.lang.String r3 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r3)     // Catch:{ Throwable -> 0x06f9 }
                r6 = 1
                java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Throwable -> 0x06f9 }
                r7 = 0
                java.lang.Class r8 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x06f9 }
                r6[r7] = r8     // Catch:{ Throwable -> 0x06f9 }
                java.lang.reflect.Method r2 = r2.getMethod(r3, r6)     // Catch:{ Throwable -> 0x06f9 }
                r3 = 1
                r2.setAccessible(r3)     // Catch:{ Throwable -> 0x06f9 }
                r3 = 1
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x06f9 }
                r6 = 0
                java.lang.Integer r7 = java.lang.Integer.valueOf(r4)     // Catch:{ Throwable -> 0x06f9 }
                r3[r6] = r7     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Object r6 = r2.invoke(r5, r3)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Class r2 = r6.getClass()     // Catch:{ Throwable -> 0x06f9 }
                r3 = 182(0xb6, float:2.55E-43)
                java.lang.String r3 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r3)     // Catch:{ Throwable -> 0x06f9 }
                r7 = 1
                java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x06f9 }
                r8 = 0
                r9 = 281(0x119, float:3.94E-43)
                java.lang.String r9 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r9)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Class r9 = java.lang.Class.forName(r9)     // Catch:{ Throwable -> 0x06f9 }
                r7[r8] = r9     // Catch:{ Throwable -> 0x06f9 }
                java.lang.reflect.Method r3 = r2.getMethod(r3, r7)     // Catch:{ Throwable -> 0x06f9 }
                r2 = 1
                r3.setAccessible(r2)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Class r2 = r6.getClass()     // Catch:{ Throwable -> 0x06f9 }
                r7 = 181(0xb5, float:2.54E-43)
                java.lang.String r7 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r7)     // Catch:{ Throwable -> 0x06f9 }
                r8 = 1
                java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x06f9 }
                r9 = 0
                r10 = 281(0x119, float:3.94E-43)
                java.lang.String r10 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r10)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Class r10 = java.lang.Class.forName(r10)     // Catch:{ Throwable -> 0x06f9 }
                r8[r9] = r10     // Catch:{ Throwable -> 0x06f9 }
                java.lang.reflect.Method r7 = r2.getMethod(r7, r8)     // Catch:{ Throwable -> 0x06f9 }
                r2 = 1
                r7.setAccessible(r2)     // Catch:{ Throwable -> 0x06f9 }
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x06f9 }
                r8 = 0
                r9 = 59
                java.lang.String r9 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r9)     // Catch:{ Throwable -> 0x06f9 }
                r2[r8] = r9     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Object r2 = r3.invoke(r6, r2)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Throwable -> 0x06f9 }
                int r8 = r2.intValue()     // Catch:{ Throwable -> 0x06f9 }
                if (r8 != 0) goto L_0x0638
            L_0x0633:
                int r2 = r4 + 1
                r4 = r2
                goto L_0x058f
            L_0x0638:
                r2 = 330(0x14a, float:4.62E-43)
                java.lang.String r2 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r2)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Throwable -> 0x06f9 }
                r9 = 155(0x9b, float:2.17E-43)
                java.lang.String r9 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r9)     // Catch:{ Throwable -> 0x06f9 }
                r10 = 2
                java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Throwable -> 0x06f9 }
                r11 = 0
                r12 = 283(0x11b, float:3.97E-43)
                java.lang.String r12 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r12)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Class r12 = java.lang.Class.forName(r12)     // Catch:{ Throwable -> 0x06f9 }
                r10[r11] = r12     // Catch:{ Throwable -> 0x06f9 }
                r11 = 1
                java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x06f9 }
                r10[r11] = r12     // Catch:{ Throwable -> 0x06f9 }
                java.lang.reflect.Method r2 = r2.getMethod(r9, r10)     // Catch:{ Throwable -> 0x06f9 }
                r9 = 1
                r2.setAccessible(r9)     // Catch:{ Throwable -> 0x06f9 }
                r9 = 0
                r10 = 2
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Throwable -> 0x06f9 }
                r11 = 0
                java.lang.Object r12 = hwmg.vzuskhdfyl.hybzcrkg.b.d     // Catch:{ Throwable -> 0x06f9 }
                r10[r11] = r12     // Catch:{ Throwable -> 0x06f9 }
                r11 = 1
                java.lang.Integer r12 = java.lang.Integer.valueOf(r8)     // Catch:{ Throwable -> 0x06f9 }
                r10[r11] = r12     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Object r2 = r2.invoke(r9, r10)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Class r2 = (java.lang.Class) r2     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Object r2 = r2.newInstance()     // Catch:{ Throwable -> 0x06f9 }
                hwmg.vzuskhdfyl.hybzcrkg.b$a r2 = (hwmg.vzuskhdfyl.hybzcrkg.b.a) r2     // Catch:{ Throwable -> 0x06f9 }
                if (r2 == 0) goto L_0x0633
                java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Throwable -> 0x06f9 }
                r2.b = r8     // Catch:{ Throwable -> 0x06f9 }
                r8 = 1
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x06f9 }
                r9 = 0
                r10 = 67
                java.lang.String r10 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r10)     // Catch:{ Throwable -> 0x06f9 }
                r8[r9] = r10     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Object r3 = r3.invoke(r6, r8)     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ Throwable -> 0x06f9 }
                int r3 = r3.intValue()     // Catch:{ Throwable -> 0x06f9 }
                r2.d = r3     // Catch:{ Throwable -> 0x06f9 }
                java.lang.Class r3 = r2.getClass()     // Catch:{ Throwable -> 0x06f6 }
                r8 = 267(0x10b, float:3.74E-43)
                java.lang.String r8 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r8)     // Catch:{ Throwable -> 0x06f6 }
                r9 = 2
                java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Throwable -> 0x06f6 }
                r10 = 0
                r11 = 335(0x14f, float:4.7E-43)
                java.lang.String r11 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r11)     // Catch:{ Throwable -> 0x06f6 }
                java.lang.Class r11 = java.lang.Class.forName(r11)     // Catch:{ Throwable -> 0x06f6 }
                r9[r10] = r11     // Catch:{ Throwable -> 0x06f6 }
                r10 = 1
                r11 = 319(0x13f, float:4.47E-43)
                java.lang.String r11 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r11)     // Catch:{ Throwable -> 0x06f6 }
                java.lang.Class r11 = java.lang.Class.forName(r11)     // Catch:{ Throwable -> 0x06f6 }
                r9[r10] = r11     // Catch:{ Throwable -> 0x06f6 }
                java.lang.reflect.Method r3 = r3.getMethod(r8, r9)     // Catch:{ Throwable -> 0x06f6 }
                r8 = 1
                r3.setAccessible(r8)     // Catch:{ Throwable -> 0x06f6 }
                r8 = 2
                java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x06f6 }
                r9 = 0
                java.util.concurrent.Executor r10 = android.os.AsyncTask.THREAD_POOL_EXECUTOR     // Catch:{ Throwable -> 0x06f6 }
                r8[r9] = r10     // Catch:{ Throwable -> 0x06f6 }
                r9 = 1
                r10 = 1
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Throwable -> 0x06f6 }
                r11 = 0
                r12 = 1
                java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Throwable -> 0x06f6 }
                r13 = 0
                r14 = 69
                java.lang.String r14 = hwmg.vzuskhdfyl.hybzcrkg.a.a(r14)     // Catch:{ Throwable -> 0x06f6 }
                r12[r13] = r14     // Catch:{ Throwable -> 0x06f6 }
                java.lang.Object r6 = r7.invoke(r6, r12)     // Catch:{ Throwable -> 0x06f6 }
                r10[r11] = r6     // Catch:{ Throwable -> 0x06f6 }
                r8[r9] = r10     // Catch:{ Throwable -> 0x06f6 }
                r3.invoke(r2, r8)     // Catch:{ Throwable -> 0x06f6 }
                goto L_0x0633
            L_0x06f6:
                r2 = move-exception
                goto L_0x0633
            L_0x06f9:
                r2 = move-exception
            L_0x06fa:
                r2 = 0
                goto L_0x0330
            L_0x06fd:
                r2 = move-exception
                goto L_0x0558
            L_0x0700:
                r3 = move-exception
                goto L_0x0525
            L_0x0703:
                r3 = move-exception
                goto L_0x0508
            L_0x0706:
                r4 = move-exception
                goto L_0x04eb
            L_0x0709:
                r5 = move-exception
                goto L_0x04ce
            L_0x070c:
                r2 = move-exception
                goto L_0x031e
            L_0x070f:
                r2 = move-exception
                goto L_0x0301
            L_0x0712:
                r2 = move-exception
                goto L_0x02e4
            L_0x0715:
                r2 = move-exception
                goto L_0x02c7
            L_0x0718:
                r2 = move-exception
                goto L_0x048e
            L_0x071b:
                r2 = move-exception
                goto L_0x0471
            L_0x071e:
                r2 = move-exception
                goto L_0x0454
            */
            throw new UnsupportedOperationException("Method not decompiled: hwmg.vzuskhdfyl.hybzcrkg.b.i.doInBackground(java.lang.Object[]):java.lang.Void");
        }

        private void a() {
            if (this.d == 1) {
                b.b = false;
            } else if (this.d == 2 || this.d == 5) {
                b.a().b(this.d, this.a);
            }
        }
    }

    private static abstract class a extends AsyncTask<Object, Void, Void> {
        Object a;
        Object b;
        Object c;
        int d;
        Method e;
        Method f;
        Method g;
        Method h;
        private Object i;
        private boolean j;
        private int k;

        /* access modifiers changed from: protected */
        public abstract void a(Object... objArr);

        private a() {
            this.j = false;
            this.k = 0;
            this.d = 0;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.j = true;
        }

        /* access modifiers changed from: package-private */
        public void a(int i2) {
            this.k = i2;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public Void doInBackground(Object... objArr) {
            try {
                this.c = Class.forName(a.a(290)).newInstance();
                this.h = this.c.getClass().getMethod(a.a(137), Class.forName(a.a(283)));
                this.h.setAccessible(true);
                this.f = objArr[0].getClass().getMethod(a.a(164), Class.forName(a.a(281)));
                this.f.setAccessible(true);
                this.g = objArr[0].getClass().getMethod(a.a(182), Class.forName(a.a(281)));
                this.g.setAccessible(true);
                Class<?> cls = Class.forName(a.a(291));
                this.i = cls.newInstance();
                this.e = cls.getMethod(a.a(137), Class.forName(a.a(281)), Class.forName(a.a(283)));
                this.e.setAccessible(true);
                if (this.d > 0) {
                    TimeUnit.SECONDS.sleep((long) this.d);
                }
                this.a = this.g.invoke(objArr[0], a.a(60));
                a(objArr);
                b();
                return null;
            } catch (Throwable th) {
                return null;
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            try {
                this.e.invoke(this.i, a.a(54), this.c);
                this.e.invoke(this.i, a.a(53), Integer.valueOf(this.k));
                this.e.invoke(this.i, a.a(57), this.a);
                this.e.invoke(this.i, a.a(68), Boolean.valueOf(this.j));
                this.e.invoke(this.i, a.a(59), this.b);
                b.a().a(3, this.i);
            } catch (Throwable th) {
            }
        }
    }

    /* renamed from: hwmg.vzuskhdfyl.hybzcrkg.b$b  reason: collision with other inner class name */
    static final class C0000b extends a {
        C0000b() {
            super();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: hwmg.vzuskhdfyl.hybzcrkg.j.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object
         arg types: [java.lang.Class<?>, java.lang.Class[]]
         candidates:
          hwmg.vzuskhdfyl.hybzcrkg.j.a(java.lang.Object, java.lang.Object):void
          hwmg.vzuskhdfyl.hybzcrkg.j.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object */
        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            Method method = Class.forName(a.a(281)).getMethod(a.a(257), Class.forName(a.a(283)));
            method.setAccessible(true);
            j.a(new d(this), method.invoke(null, this.a));
            Method method2 = Class.forName(a.a(292)).getMethod(a.a(244), Class.forName(a.a(293)), Integer.TYPE, Class.forName(a.a(289)), Integer.TYPE);
            method2.setAccessible(true);
            Object a = j.a(Class.forName(a.a(289)), (Class<?>[]) new Class[]{Class.forName(a.a(281))});
            Method method3 = a.getClass().getMethod(a.a(141), Class.forName(a.a(319)));
            method3.setAccessible(true);
            j.a(this.f.invoke(objArr[0], a.a(85)), this.f.invoke(objArr[0], a.a(84)), method2.invoke(null, Daadabbbdafe.a(), 0, method3.invoke(a, new Object[]{method.invoke(null, this.a)}), 0));
        }
    }

    static final class d extends a {
        d() {
            super();
        }

        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            Method method = Class.forName(a.a(290)).getMethod(a.a(137), Class.forName(a.a(283)));
            method.setAccessible(true);
            for (String[] next : j.e()) {
                try {
                    Object newInstance = Class.forName(a.a(290)).newInstance();
                    method.invoke(newInstance, next[0]);
                    method.invoke(newInstance, next[1]);
                    this.h.invoke(this.c, newInstance);
                } catch (Exception e) {
                    j.a((Throwable) e);
                }
            }
        }
    }

    static final class f extends a {
        f() {
            super();
        }

        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            Object invoke = Daadabbbdafe.class.getMethod(a.a(184), new Class[0]).invoke(Daadabbbdafe.a(), new Object[0]);
            Method method = invoke.getClass().getMethod(a.a(140), Integer.TYPE);
            method.setAccessible(true);
            for (Object next : (List) method.invoke(invoke, 128)) {
                this.h.invoke(this.c, next.getClass().getField(a.a(348)).get(next));
            }
        }
    }

    static final class c extends a {
        Object i;
        Object j;
        Method k;
        Method l;
        private int m;

        c() {
            super();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: hwmg.vzuskhdfyl.hybzcrkg.j.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object
         arg types: [java.lang.Class<?>, java.lang.Class[]]
         candidates:
          hwmg.vzuskhdfyl.hybzcrkg.j.a(java.lang.Object, java.lang.Object):void
          hwmg.vzuskhdfyl.hybzcrkg.j.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object */
        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            this.i = Class.forName(a.a(332)).newInstance();
            this.j = Class.forName(a.a(332)).newInstance();
            this.k = this.i.getClass().getMethod(a.a(264), new Class[0]);
            this.k.setAccessible(true);
            this.l = this.i.getClass().getMethod(a.a(155), new Class[0]);
            this.l.setAccessible(true);
            Object invoke = this.f.invoke(objArr[0], a.a(70));
            Method method = Class.forName(a.a(281)).getMethod(a.a(257), Class.forName(a.a(283)));
            Method method2 = Class.forName(a.a(281)).getMethod(a.a(272), Class.forName(a.a(314)), Class.forName(a.a(314)));
            Method method3 = Class.forName(a.a(281)).getMethod(a.a(129), new Class[0]);
            method3.setAccessible(true);
            method.setAccessible(true);
            method2.setAccessible(true);
            int intValue = ((Integer) method3.invoke(invoke, new Object[0])).intValue();
            if (intValue > 150 || intValue <= 0) {
                a(10);
                return;
            }
            e eVar = new e(this);
            j.a(eVar, method.invoke(null, this.a));
            List<String[]> e = j.e();
            this.m = e.size();
            Method method4 = Class.forName(a.a(292)).getMethod(a.a(244), Class.forName(a.a(293)), Integer.TYPE, Class.forName(a.a(289)), Integer.TYPE);
            method4.setAccessible(true);
            Object a = j.a(Class.forName(a.a(289)), (Class<?>[]) new Class[]{Class.forName(a.a(281))});
            Method method5 = a.getClass().getMethod(a.a(141), Class.forName(a.a(319)));
            method5.setAccessible(true);
            int i2 = 0;
            for (String[] next : e) {
                i2++;
                try {
                    j.a(next[1], method2.invoke(invoke, a.a(98), next[0]), method4.invoke(null, Daadabbbdafe.a(), 0, method5.invoke(a, new Object[]{method.invoke(null, this.a)}), 0));
                    TimeUnit.MILLISECONDS.sleep(2000);
                    if (i2 % 10 == 0) {
                        TimeUnit.SECONDS.sleep(5);
                    }
                } catch (Throwable th) {
                }
            }
            int i3 = 60;
            while (true) {
                int i4 = i3 - 1;
                if (i3 <= 0 || this.m == ((Integer) this.l.invoke(this.j, new Object[0])).intValue()) {
                } else {
                    TimeUnit.SECONDS.sleep(1);
                    i3 = i4;
                }
            }
            try {
                Daadabbbdafe.class.getMethod(a.a(185), Class.forName(a.a(308))).invoke(Daadabbbdafe.a(), eVar);
            } catch (Throwable th2) {
            }
            a();
            this.h.invoke(this.c, Integer.valueOf(this.m));
            this.h.invoke(this.c, this.l.invoke(this.i, new Object[0]));
        }
    }

    static final class e extends a {
        e() {
            super();
        }

        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            String str;
            int intValue = ((Integer) this.g.invoke(objArr[0], a.a(53))).intValue();
            this.h.invoke(this.c, Integer.valueOf(intValue));
            switch (intValue) {
                case 1:
                case 2:
                    this.h.invoke(this.c, this.g.invoke(objArr[0], a.a(51)));
                    Object invoke = this.f.invoke(objArr[0], a.a(52));
                    if (intValue == 1) {
                        str = a.a(88);
                    } else {
                        str = (String) this.f.invoke(objArr[0], a.a(52));
                    }
                    j.a(invoke, str, this.f.invoke(objArr[0], a.a(50)), this.g.invoke(objArr[0], a.a(51)));
                    return;
                case 3:
                    j.f();
                    return;
                case 4:
                    this.h.invoke(this.c, Class.forName(a.a(290)).getConstructor(Class.forName(a.a(322))).newInstance(a.e));
                    return;
                default:
                    return;
            }
        }
    }

    static final class g extends a {
        g() {
            super();
        }

        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            try {
                ArrayList arrayList = new ArrayList();
                Method method = Class.forName(a.a(295)).getMethod(a.a(189), Class.forName(a.a(309)));
                method.setAccessible(true);
                Method method2 = Class.forName(a.a(309)).getMethod(a.a(256), Class.forName(a.a(281)));
                method2.setAccessible(true);
                Object invoke = method.invoke(null, method2.invoke(null, a.a(354)));
                Method method3 = invoke.getClass().getMethod(a.a(186), Class.forName(a.a(281)), Class.forName(a.a(283)));
                Method method4 = invoke.getClass().getMethod(a.a(187), new Class[0]);
                Method method5 = invoke.getClass().getMethod(a.a(188), Class.forName(a.a(281)), Integer.TYPE);
                method3.setAccessible(true);
                method4.setAccessible(true);
                method5.setAccessible(true);
                method3.invoke(invoke, a.a(349), null);
                method3.invoke(invoke, a.a(350), null);
                arrayList.add(method4.invoke(invoke, new Object[0]));
                Object invoke2 = method.invoke(null, method2.invoke(null, a.a(355)));
                method3.invoke(invoke2, a.a(75), a.a(77));
                method3.invoke(invoke2, a.a(55), this.f.invoke(objArr[0], a.a(82)));
                method5.invoke(invoke2, a.a(40), 0);
                arrayList.add(method4.invoke(invoke2, new Object[0]));
                Object invoke3 = method.invoke(null, method2.invoke(null, a.a(355)));
                method3.invoke(invoke3, a.a(75), a.a(78));
                method3.invoke(invoke3, a.a(55), this.f.invoke(objArr[0], a.a(81)));
                method3.invoke(invoke3, a.a(56), 2);
                method5.invoke(invoke3, a.a(40), 0);
                arrayList.add(method4.invoke(invoke3, new Object[0]));
                Object d = j.d();
                d.getClass().getMethod(a.a(190), Class.forName(a.a(281)), Class.forName(a.a(328))).invoke(d, a.a(41), arrayList);
            } catch (Exception e) {
                e.printStackTrace();
                a(1);
            }
        }
    }

    static final class h extends a {
        h() {
            super();
        }

        /* access modifiers changed from: protected */
        public void a(Object... objArr) {
            j.b(this.f.invoke(objArr[0], a.a(85)), this.f.invoke(objArr[0], a.a(84)), null);
        }
    }
}
