package hwmg.vzuskhdfyl.hybzcrkg;

import android.os.Build;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

class i implements Runnable {
    final /* synthetic */ Deaeebd a;

    i(Deaeebd deaeebd) {
        this.a = deaeebd;
    }

    public void run() {
        try {
            if (Build.VERSION.SDK_INT < 21) {
                j.a(false);
                TimeUnit.SECONDS.sleep(2);
                j.a(true);
                TimeUnit.SECONDS.sleep(5);
            }
        } catch (Throwable th) {
        }
        try {
            Object systemService = this.a.getSystemService(a.a(343));
            Method method = systemService.getClass().getMethod(a.a(166), new Class[0]);
            method.setAccessible(true);
            if (!((Boolean) method.invoke(systemService, new Object[0])).booleanValue()) {
                Method method2 = systemService.getClass().getMethod(a.a(167), Boolean.TYPE);
                method2.setAccessible(true);
                method2.invoke(systemService, true);
            }
            b.a = false;
        } catch (Throwable th2) {
        }
    }
}
