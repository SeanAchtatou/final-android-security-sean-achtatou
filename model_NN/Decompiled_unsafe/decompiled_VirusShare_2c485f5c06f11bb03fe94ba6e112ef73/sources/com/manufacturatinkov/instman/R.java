package com.manufacturatinkov.instman;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int ic_launcher = 2130837504;
        public static final int icon = 2130837505;
        public static final int iconnews = 2130837506;
    }

    public static final class id {
        public static final int Button = 2131165199;
        public static final int InnerRelativeLayout = 2131165185;
        public static final int checkBox1 = 2131165188;
        public static final int checkBox10 = 2131165198;
        public static final int checkBox2 = 2131165189;
        public static final int checkBox3 = 2131165190;
        public static final int checkBox4 = 2131165191;
        public static final int checkBox5 = 2131165192;
        public static final int checkBox7 = 2131165195;
        public static final int checkBox8 = 2131165196;
        public static final int checkBox9 = 2131165197;
        public static final int ivImage = 2131165201;
        public static final int linearLayout2 = 2131165186;
        public static final int llTitle = 2131165202;
        public static final int rlChannel = 2131165200;
        public static final int scrollView1 = 2131165184;
        public static final int textView2 = 2131165187;
        public static final int textView6 = 2131165194;
        public static final int textView7 = 2131165193;
        public static final int tvDesc = 2131165204;
        public static final int tvTitle = 2131165203;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int news_notification = 2130903041;
    }

    public static final class raw {
        public static final int conf = 2130968576;
        public static final int result = 2130968577;
        public static final int resulturl = 2130968578;
        public static final int rool = 2130968579;
        public static final int roolurl = 2130968580;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }

    public static final class style {
        public static final int NotificationText = 2131099648;
        public static final int NotificationTitle = 2131099649;
    }
}
