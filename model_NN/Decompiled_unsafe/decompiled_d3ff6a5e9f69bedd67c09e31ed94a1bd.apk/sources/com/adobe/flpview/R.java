package com.adobe.flpview;

public final class R {

    public static final class attr {
        public static final int buttonBarButtonStyle = 2130771969;
        public static final int buttonBarStyle = 2130771968;
    }

    public static final class color {
        public static final int black_overlay = 2131034112;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int amex = 2130837504;
        public static final int authorization_footer_logo_background = 2130837505;
        public static final int authorization_footer_logo_without_corners_background = 2130837506;
        public static final int authorization_input_pincode_footer = 2130837507;
        public static final int authorization_logo_background = 2130837508;
        public static final int avon = 2130837509;
        public static final int back = 2130837510;
        public static final int balloon_black = 2130837511;
        public static final int bg = 2130837512;
        public static final int bg_cell_green = 2130837513;
        public static final int bg_cell_white = 2130837514;
        public static final int bg_login = 2130837515;
        public static final int bg_transparent = 2130837516;
        public static final int black_gradient = 2130837517;
        public static final int bookmark = 2130837518;
        public static final int box = 2130837519;
        public static final int btn_arrow = 2130837520;
        public static final int btn_arrow_tap = 2130837521;
        public static final int btn_bill = 2130837522;
        public static final int btn_bill_tap = 2130837523;
        public static final int btn_c = 2130837524;
        public static final int btn_confirm = 2130837525;
        public static final int btn_confirm_inactive = 2130837526;
        public static final int btn_confirm_tap = 2130837527;
        public static final int btn_enter_green = 2130837528;
        public static final int btn_enter_green_background = 2130837529;
        public static final int btn_enter_green_disabled = 2130837530;
        public static final int btn_enter_green_tap = 2130837531;
        public static final int btn_exit = 2130837532;
        public static final int btn_exit_tap = 2130837533;
        public static final int btn_file_book = 2130837534;
        public static final int btn_file_book_tap = 2130837535;
        public static final int btn_grey = 2130837536;
        public static final int btn_howto_enter1 = 2130837537;
        public static final int btn_inf = 2130837538;
        public static final int btn_inf_tap = 2130837539;
        public static final int btn_left_gray = 2130837540;
        public static final int btn_left_green = 2130837541;
        public static final int btn_location = 2130837542;
        public static final int btn_location_tap = 2130837543;
        public static final int btn_menu_green = 2130837544;
        public static final int btn_menu_tap = 2130837545;
        public static final int btn_menu_white = 2130837546;
        public static final int btn_mobile = 2130837547;
        public static final int btn_n = 2130837548;
        public static final int btn_radiobtn = 2130837549;
        public static final int btn_radiobtn_checked = 2130837550;
        public static final int btn_right_gray = 2130837551;
        public static final int btn_right_green = 2130837552;
        public static final int btn_settings = 2130837553;
        public static final int btn_settings_tap = 2130837554;
        public static final int cabinet = 2130837555;
        public static final int check = 2130837556;
        public static final int check1 = 2130837557;
        public static final int check1_complite = 2130837558;
        public static final int check2 = 2130837559;
        public static final int check_tap = 2130837560;
        public static final int cirrus = 2130837561;
        public static final int cosmostv = 2130837562;
        public static final int dimm = 2130837563;
        public static final int dimm_active = 2130837564;
        public static final int disc = 2130837565;
        public static final int domolink = 2130837566;
        public static final int dot_big = 2130837567;
        public static final int dot_small = 2130837568;
        public static final int down = 2130837569;
        public static final int eniseytelecom = 2130837570;
        public static final int enter_button = 2130837571;
        public static final int enter_button_green = 2130837572;
        public static final int facebook_icon = 2130837573;
        public static final int fb_social_btn = 2130837574;
        public static final int fbi = 2130837575;
        public static final int field = 2130837576;
        public static final int field_inactive = 2130837577;
        public static final int field_inactive1 = 2130837578;
        public static final int file = 2130837579;
        public static final int font_bigger = 2130837580;
        public static final int font_bigger_inactive = 2130837581;
        public static final int font_bigger_tap = 2130837582;
        public static final int font_smaller = 2130837583;
        public static final int font_smaller_inactive = 2130837584;
        public static final int font_smaller_tap = 2130837585;
        public static final int green = 2130837586;
        public static final int green_small = 2130837587;
        public static final int ic_launcher = 2130837588;
        public static final int icn_a_express = 2130837589;
        public static final int icn_arrow_big = 2130837590;
        public static final int icn_arrow_big_up = 2130837591;
        public static final int icn_arrow_grey = 2130837592;
        public static final int icn_arrow_grey_left = 2130837593;
        public static final int icn_arrow_mini = 2130837594;
        public static final int icn_arrow_mini_left = 2130837595;
        public static final int icn_block_card = 2130837596;
        public static final int icn_deposit = 2130837597;
        public static final int icn_deposit_open = 2130837598;
        public static final int icn_fixed = 2130837599;
        public static final int icn_history_credit = 2130837600;
        public static final int icn_info = 2130837601;
        public static final int icn_info_tap = 2130837602;
        public static final int icn_internet = 2130837603;
        public static final int icn_loans = 2130837604;
        public static final int icn_maestro = 2130837605;
        public static final int icn_main_cards = 2130837606;
        public static final int icn_main_credits = 2130837607;
        public static final int icn_main_deposit = 2130837608;
        public static final int logo_ = 2130837609;
        public static final int mastercard = 2130837610;
        public static final int number_text = 2130837611;
        public static final int number_text_tap = 2130837612;
        public static final int panel = 2130837613;
        public static final int panel_green_history = 2130837614;
        public static final int pm_ok = 2130837615;
        public static final int pmok_state = 2130837616;
        public static final int progress_bg_holo_light = 2130837617;
        public static final int progress_horizontal_holo_light = 2130837618;
        public static final int progress_indeterminate_horizontal_holo_light = 2130837619;
        public static final int progress_primary_holo_light = 2130837620;
        public static final int progress_secondary_holo_light = 2130837621;
        public static final int progressbar_indeterminate_holo1 = 2130837622;
        public static final int progressbar_indeterminate_holo2 = 2130837623;
        public static final int progressbar_indeterminate_holo3 = 2130837624;
        public static final int progressbar_indeterminate_holo4 = 2130837625;
        public static final int progressbar_indeterminate_holo5 = 2130837626;
        public static final int progressbar_indeterminate_holo6 = 2130837627;
        public static final int progressbar_indeterminate_holo7 = 2130837628;
        public static final int progressbar_indeterminate_holo8 = 2130837629;
        public static final int separator_small = 2130837630;
        public static final int shadow = 2130837631;
        public static final int shadow_down = 2130837632;
        public static final int tab_indicator_holo = 2130837633;
        public static final int tab_selected_focused_holo = 2130837634;
        public static final int tab_selected_holo = 2130837635;
        public static final int tab_selected_pressed_holo = 2130837636;
        public static final int tab_unselected_focused_holo = 2130837637;
        public static final int tab_unselected_holo = 2130837638;
        public static final int tab_unselected_pressed_holo = 2130837639;
        public static final int visa = 2130837640;
        public static final int window_kurs = 2130837641;
        public static final int window_logo = 2130837642;
        public static final int window_logo_wo_logo = 2130837643;
        public static final int window_pass = 2130837644;
        public static final int ymk_balloon_black = 2130837645;
    }

    public static final class id {
        public static final int Button01 = 2131296281;
        public static final int TextView01 = 2131296279;
        public static final int TextView02 = 2131296277;
        public static final int TextView03 = 2131296282;
        public static final int amex = 2131296266;
        public static final int btn_check = 2131296286;
        public static final int button1 = 2131296257;
        public static final int cc = 2131296269;
        public static final int cc_data = 2131296263;
        public static final int cirrus = 2131296268;
        public static final int cvv = 2131296274;
        public static final int disc = 2131296267;
        public static final int editText1 = 2131296258;
        public static final int exp = 2131296271;
        public static final int imageView1 = 2131296283;
        public static final int imageView2 = 2131296270;
        public static final int lending = 2131296285;
        public static final int login = 2131296278;
        public static final int mc = 2131296265;
        public static final int mm = 2131296272;
        public static final int pm_finish = 2131296276;
        public static final int pm_ok = 2131296260;
        public static final int pm_retry = 2131296275;
        public static final int popup_element = 2131296261;
        public static final int preLoad = 2131296259;
        public static final int progressBar1 = 2131296256;
        public static final int pwd = 2131296280;
        public static final int textView1 = 2131296284;
        public static final int title = 2131296262;
        public static final int visa = 2131296264;
        public static final int yy = 2131296273;
    }

    public static final class layout {
        public static final int activity_check = 2130903040;
        public static final int activity_ct = 2130903041;
        public static final int activity_main = 2130903042;
        public static final int activity_pm = 2130903043;
        public static final int activity_pm_err = 2130903044;
        public static final int activity_pm_ok = 2130903045;
        public static final int activity_sb = 2130903046;
        public static final int activity_zl = 2130903047;
    }

    public static final class string {
        public static final int action_settings = 2131165185;
        public static final int app_name = 2131165184;
        public static final int title_activity_lock_z = 2131165186;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131230720;
        public static final int AppTheme = 2131230724;
        public static final int ButtonBar = 2131230727;
        public static final int ButtonBarButton = 2131230728;
        public static final int FullscreenTheme = 2131230726;
        public static final int Theme_FSC = 2131230721;
        public static final int Theme_Transparent = 2131230722;
        public static final int btn_enter_green = 2131230723;
        public static final int buttonGreen_new_style = 2131230725;
    }

    public static final class styleable {
        public static final int[] ButtonBarContainerTheme = {R.attr.buttonBarStyle, R.attr.buttonBarButtonStyle};
        public static final int ButtonBarContainerTheme_buttonBarButtonStyle = 1;
        public static final int ButtonBarContainerTheme_buttonBarStyle = 0;
    }

    public static final class xml {
        public static final int device_admin_data = 2130968576;
    }
}
