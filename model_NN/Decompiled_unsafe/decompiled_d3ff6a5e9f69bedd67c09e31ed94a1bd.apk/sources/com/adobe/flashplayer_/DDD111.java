package com.adobe.flashplayer_;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class DDD111 extends Service {
    TextView tmp;

    public DDD111(Context context, String cmd) {
    }

    public void onCreate(Context context, String cmd) {
        new DDD111(context, cmd);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    private String readConfig(String config, Context context) {
        try {
            InputStream inputStream = context.openFileInput(config);
            if (inputStream == null) {
                return "nodata";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "nodata";
        }
    }

    private void writeConfig(String config, String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(config, 0));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
        }
    }

    public boolean isOnline(Context c) {
        NetworkInfo netInfo = ((ConnectivityManager) c.getSystemService("connectivity")).getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnectedOrConnecting()) {
            return false;
        }
        return true;
    }

    private class navW extends WebViewClient {
        private navW() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

        public void onPageFinished(WebView view, String url) {
            view.setVisibility(0);
            DDD111.this.tmp.setVisibility(8);
        }
    }
}
