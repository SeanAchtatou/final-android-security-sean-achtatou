package com.paypal.android.a;

final class g extends Thread {
    private /* synthetic */ f a;

    g(f fVar) {
        this.a = fVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.paypal.android.a.f.a(com.paypal.android.a.f, int):int
      com.paypal.android.a.f.a(java.lang.String, java.lang.String):boolean
      com.paypal.android.a.f.a(int, java.lang.Object):void
      com.paypal.android.MEP.b.a(int, java.lang.Object):void
      com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0099  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r10 = this;
            r8 = 4
            r7 = 0
            r6 = 0
            r5 = -1
        L_0x0004:
            com.paypal.android.a.f r0 = r10.a
            java.util.Hashtable r0 = r0.g
            java.lang.String r1 = "delegate"
            java.lang.Object r0 = r0.get(r1)
            com.paypal.android.MEP.b r0 = (com.paypal.android.MEP.b) r0
            com.paypal.android.a.f r1 = r10.a
            int r1 = r1.f
            switch(r1) {
                case 0: goto L_0x0026;
                case 1: goto L_0x001b;
                case 2: goto L_0x001b;
                case 3: goto L_0x00cd;
                case 4: goto L_0x0158;
                case 5: goto L_0x01be;
                case 6: goto L_0x00a4;
                case 7: goto L_0x01e1;
                case 8: goto L_0x0234;
                case 9: goto L_0x011e;
                case 10: goto L_0x006e;
                case 11: goto L_0x020a;
                case 12: goto L_0x0240;
                case 13: goto L_0x024c;
                case 14: goto L_0x0270;
                case 15: goto L_0x0294;
                default: goto L_0x001b;
            }
        L_0x001b:
            r0 = 100
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x0021 }
            goto L_0x0004
        L_0x0021:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0004
        L_0x0026:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.f = -1
            com.paypal.android.a.f r3 = r10.a     // Catch:{ Exception -> 0x005a }
            com.paypal.android.a.f r1 = r10.a     // Catch:{ Exception -> 0x005a }
            java.util.Hashtable r1 = r1.g     // Catch:{ Exception -> 0x005a }
            java.lang.String r2 = "usernameOrPhone"
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x005a }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x005a }
            com.paypal.android.a.f r2 = r10.a     // Catch:{ Exception -> 0x005a }
            java.util.Hashtable r2 = r2.g     // Catch:{ Exception -> 0x005a }
            java.lang.String r4 = "passwordOrPin"
            java.lang.Object r2 = r2.get(r4)     // Catch:{ Exception -> 0x005a }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x005a }
            boolean r1 = r3.a(r1, r2)     // Catch:{ Exception -> 0x005a }
        L_0x004d:
            if (r1 != 0) goto L_0x0054
            com.paypal.android.a.f r2 = r10.a
            int unused = r2.d = -1
        L_0x0054:
            if (r1 == 0) goto L_0x0064
            r0.a(r7, r6)
            goto L_0x001b
        L_0x005a:
            r1 = move-exception
            java.lang.String r2 = "Login"
            java.lang.String r3 = "Error during call to log in."
            android.util.Log.e(r2, r3, r1)
            r1 = r7
            goto L_0x004d
        L_0x0064:
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x001b
        L_0x006e:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.f = -1
            com.paypal.android.a.f r1 = r10.a     // Catch:{ Exception -> 0x008e }
            boolean r1 = r1.v()     // Catch:{ Exception -> 0x008e }
            com.paypal.android.a.f r2 = r10.a     // Catch:{ Exception -> 0x02b3 }
            boolean r1 = r2.r()     // Catch:{ Exception -> 0x02b3 }
        L_0x007f:
            if (r1 != 0) goto L_0x0086
            com.paypal.android.a.f r2 = r10.a
            int unused = r2.d = -1
        L_0x0086:
            if (r1 == 0) goto L_0x0099
            r1 = 10
            r0.a(r1, r6)
            goto L_0x001b
        L_0x008e:
            r1 = move-exception
            r2 = r7
        L_0x0090:
            java.lang.String r3 = "Login"
            java.lang.String r4 = "Error during call to log in."
            android.util.Log.e(r3, r4, r1)
            r1 = r2
            goto L_0x007f
        L_0x0099:
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x001b
        L_0x00a4:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.f = -1
            com.paypal.android.a.f r1 = r10.a
            boolean r1 = r1.k()
            if (r1 != 0) goto L_0x00c1
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.d = -1
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x001b
        L_0x00c1:
            r1 = 6
            com.paypal.android.a.f r2 = r10.a
            java.util.Hashtable r2 = r2.g
            r0.a(r1, r2)
            goto L_0x001b
        L_0x00cd:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.f = -1
            com.paypal.android.a.f r1 = com.paypal.android.MEP.PayPalActivity.a
            java.lang.String r2 = "FundingPlanId"
            java.lang.String r3 = "0"
            r1.a(r2, r3)
            com.paypal.android.a.f r1 = com.paypal.android.MEP.PayPalActivity.a
            java.lang.String r2 = "FundingPlans"
            r1.a(r2, r6)
            com.paypal.android.a.f r1 = com.paypal.android.MEP.PayPalActivity.a
            java.lang.String r2 = "DefaultFundingPlan"
            r1.a(r2, r6)
            com.paypal.android.a.f r1 = r10.a
            java.util.Hashtable r2 = r1.s()
            if (r2 != 0) goto L_0x0101
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.d = -1
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x001b
        L_0x0101:
            java.lang.String r1 = "ActionType"
            java.lang.Object r1 = r2.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r3 = "PAY"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x0118
            java.lang.String r1 = "-1"
            r0.a(r8, r1)
            goto L_0x001b
        L_0x0118:
            r1 = 3
            r0.a(r1, r2)
            goto L_0x001b
        L_0x011e:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.f = -1
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = com.paypal.android.a.f.g(r1)
            if (r1 == 0) goto L_0x0147
            int r2 = r1.length()
            if (r2 <= 0) goto L_0x0147
            com.paypal.android.a.f r2 = r10.a
            java.lang.String r3 = "mpl-success"
            r2.b(r3)
        L_0x0138:
            if (r1 == 0) goto L_0x014d
            int r2 = r1.length()
            if (r2 <= 0) goto L_0x014d
            r2 = 9
            r0.a(r2, r1)
            goto L_0x001b
        L_0x0147:
            com.paypal.android.a.f r2 = r10.a
            int unused = r2.d = -1
            goto L_0x0138
        L_0x014d:
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x001b
        L_0x0158:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.f = -1
            com.paypal.android.a.f r1 = r10.a
            boolean r1 = r1.l()
            if (r1 != 0) goto L_0x017e
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            com.paypal.android.MEP.PayPalActivity r0 = com.paypal.android.MEP.PayPalActivity.a()
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "CHANGE_STRING"
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x001b
        L_0x017e:
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = com.paypal.android.a.f.i(r1)
            if (r1 == 0) goto L_0x01ae
            int r2 = r1.length()
            if (r2 <= 0) goto L_0x01ae
            com.paypal.android.a.f r2 = r10.a
            java.lang.String r3 = "mpl-success"
            r2.b(r3)
        L_0x0193:
            if (r1 == 0) goto L_0x01b4
            int r2 = r1.length()
            if (r2 <= 0) goto L_0x01b4
            r0.a(r8, r1)
        L_0x019e:
            com.paypal.android.MEP.PayPalActivity r0 = com.paypal.android.MEP.PayPalActivity.a()
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "CHANGE_STRING"
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x001b
        L_0x01ae:
            com.paypal.android.a.f r2 = r10.a
            int unused = r2.d = -1
            goto L_0x0193
        L_0x01b4:
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x019e
        L_0x01be:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.f = -1
            com.paypal.android.a.f r1 = r10.a
            java.util.Hashtable r1 = r1.t()
            if (r1 != 0) goto L_0x01db
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.d = -1
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x001b
        L_0x01db:
            r2 = 5
            r0.a(r2, r1)
            goto L_0x001b
        L_0x01e1:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.f = -1
            com.paypal.android.a.f r1 = r10.a
            boolean r1 = r1.m()
            if (r1 == 0) goto L_0x01fa
            r1 = 7
            com.paypal.android.a.f r2 = r10.a
            java.util.Hashtable r2 = r2.g
            r0.a(r1, r2)
            goto L_0x001b
        L_0x01fa:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.d = -1
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x001b
        L_0x020a:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.f = -1
            com.paypal.android.a.f r1 = r10.a
            boolean r1 = r1.w()
            if (r1 == 0) goto L_0x0224
            r1 = 11
            com.paypal.android.a.f r2 = r10.a
            java.util.Hashtable r2 = r2.g
            r0.a(r1, r2)
            goto L_0x001b
        L_0x0224:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.d = -1
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x001b
        L_0x0234:
            com.paypal.android.a.f r0 = r10.a
            int unused = r0.f = -1
            com.paypal.android.a.f r0 = r10.a
            boolean unused = r0.v()
            goto L_0x001b
        L_0x0240:
            com.paypal.android.a.f r0 = r10.a
            int unused = r0.f = -1
            com.paypal.android.a.f r0 = r10.a
            boolean unused = r0.x()
            goto L_0x001b
        L_0x024c:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.f = -1
            com.paypal.android.a.f r1 = r10.a
            boolean r1 = r1.o()
            if (r1 != 0) goto L_0x0269
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.d = -1
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x001b
        L_0x0269:
            r1 = 13
            r0.a(r1, r6)
            goto L_0x001b
        L_0x0270:
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.f = -1
            com.paypal.android.a.f r1 = r10.a
            boolean r1 = r1.p()
            if (r1 != 0) goto L_0x028d
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.d = -1
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x001b
        L_0x028d:
            r1 = 14
            r0.a(r1, r6)
            goto L_0x001b
        L_0x0294:
            com.paypal.android.a.f r1 = r10.a
            boolean r1 = r1.n()
            if (r1 != 0) goto L_0x02ac
            com.paypal.android.a.f r1 = r10.a
            int unused = r1.d = -1
            com.paypal.android.a.f r1 = r10.a
            java.lang.String r1 = r1.d()
            r0.a(r1)
            goto L_0x001b
        L_0x02ac:
            r1 = 15
            r0.a(r1, r6)
            goto L_0x001b
        L_0x02b3:
            r2 = move-exception
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x0090
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.a.g.run():void");
    }
}
