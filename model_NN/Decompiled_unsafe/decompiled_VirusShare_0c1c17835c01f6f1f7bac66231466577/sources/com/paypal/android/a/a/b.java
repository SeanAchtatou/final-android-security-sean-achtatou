package com.paypal.android.a.a;

import com.paypal.android.a.c;
import com.paypal.android.a.d;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class b {
    private String a;
    private String b;

    public static b a(Element element) {
        if (element == null) {
            return null;
        }
        b bVar = new b();
        NodeList elementsByTagName = element.getElementsByTagName("type");
        if (elementsByTagName.getLength() != 1) {
            return null;
        }
        bVar.b = c.a(((Element) elementsByTagName.item(0)).getChildNodes());
        NodeList elementsByTagName2 = element.getElementsByTagName("lastFourOfAccountNumber");
        if (elementsByTagName2.getLength() == 1) {
            bVar.a = c.a(((Element) elementsByTagName2.item(0)).getChildNodes());
        }
        NodeList elementsByTagName3 = element.getElementsByTagName("displayName");
        if (elementsByTagName3.getLength() == 1) {
            c.a(((Element) elementsByTagName3.item(0)).getChildNodes());
        }
        return bVar;
    }

    public final String a() {
        return this.b != null ? this.b.equals("BALANCE") ? d.a("ANDROID_balance") : (this.b.equals("BANK_DELAYED") || this.b.equals("BANK_INSTANT")) ? d.a("ANDROID_bank") : (this.b.equals("CREDITCARD") || this.b.equals("DEBITCARD")) ? d.a("ANDROID_card") : this.b : "";
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.a;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final String c() {
        return this.b;
    }
}
