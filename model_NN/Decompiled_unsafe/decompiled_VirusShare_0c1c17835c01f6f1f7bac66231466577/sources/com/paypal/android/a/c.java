package com.paypal.android.a;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.paypal.android.MEP.o;
import com.paypal.android.a.a.g;
import com.paypal.android.a.a.i;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class c {
    public static int a(String str) {
        NodeList nodeList;
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            NodeList elementsByTagName = parse.getElementsByTagName("ErrorCode");
            if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
                nodeList = parse.getElementsByTagName("errorId");
                if (nodeList == null || nodeList.getLength() == 0) {
                    return 200;
                }
            } else {
                nodeList = elementsByTagName;
            }
            return Integer.parseInt(nodeList.item(0).getChildNodes().item(0).getNodeValue());
        } catch (Exception e) {
            e.printStackTrace();
            return 10004;
        }
    }

    public static String a() {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        String str10;
        String str11;
        String str12;
        String str13;
        Exception e;
        String str14;
        String str15;
        String str16;
        String str17;
        String str18;
        String str19;
        String str20;
        String str21;
        String str22;
        String str23;
        String str24;
        String str25;
        String str26;
        String str27;
        String str28;
        TelephonyManager telephonyManager;
        String str29;
        String str30;
        String str31;
        String str32;
        String str33;
        String replaceAll;
        String packageName;
        String str34;
        String str35 = "";
        String str36 = "false";
        String str37 = "";
        String str38 = "";
        String str39 = "";
        String str40 = "";
        try {
            str = URLEncoder.encode(o.a().f(), "utf-8");
            try {
                telephonyManager = (TelephonyManager) o.a().e().getSystemService("phone");
                if (telephonyManager.getPhoneType() == 1) {
                    str29 = "IMEI";
                    str30 = "AndroidGSM";
                } else {
                    str29 = "MEID";
                    str30 = "AndroidCDMA";
                }
            } catch (Exception e2) {
                e = e2;
                str2 = str35;
                str13 = "";
                str4 = "";
                str11 = "";
                str6 = str39;
                str9 = "";
                String str41 = str40;
                str8 = "";
                str7 = str41;
                String str42 = str38;
                str10 = "";
                str5 = str42;
                String str43 = str37;
                str12 = "";
                str3 = str43;
                e.printStackTrace();
                str14 = str13;
                str15 = str12;
                str16 = str10;
                str17 = str8;
                str18 = str6;
                str19 = str4;
                str20 = str36;
                str21 = str11;
                str22 = str9;
                str23 = str7;
                str24 = str5;
                str25 = str3;
                str26 = str2;
                str27 = str;
                String str44 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:cc=\"urn:ebay:apis:CoreComponentTypes\" xmlns:ed=\"urn:ebay:apis:EnhancedDataTypes\" xmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xmlns:ebl=\"urn:ebay:apis:eBLBaseComponents\" xmlns:ns=\"urn:ebay:api:PayPalAPI\"><SOAP-ENV:Header><Security xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xsi:type=\"wsse:SecurityType\"></Security><RequesterCredentials xmlns=\"urn:ebay:api:PayPalAPI\" xsi:type=\"ebl:CustomSecurityHeaderType\"><Credentials xmlns=\"urn:ebay:apis:eBLBaseComponents\" xsi:type=\"ebl:UserIdPasswordType\"><Username xsi:type=\"xs:string\">gmapi_client</Username><Password xsi:type=\"xs:string\">11111111</Password></Credentials></RequesterCredentials></SOAP-ENV:Header><SOAP-ENV:Body id=\"_0\"><MEPDeviceInterrogationReq xmlns=\"urn:ebay:api:PayPalAPI\"><Request><Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">1.0</Version><PayPalAppID xsi:type=\"xs:string\">" + str27 + "</PayPalAppID>" + "<DeviceDetails xsi:type=\"ns:MEPDeviceDetailsType\">" + "<deviceID xsi:type=\"ns:MEPDeviceIDType\">" + "<deviceIdentifier xsi:type=\"xs:string\">" + str25 + "</deviceIdentifier>" + "<deviceIdType xsi:type=\"ns:MEPDeviceIdentifierType\">" + str22 + "</deviceIdType>" + "</deviceID>" + "<deviceName xsi:type=\"xs:string\">" + str24 + "</deviceName>" + "<deviceModel xsi:type=\"xs:string\">" + str19 + "</deviceModel>" + "<systemName xsi:type=\"xs:string\">" + str18 + "</systemName>" + "<systemVersion xsi:type=\"xs:string\">" + str23 + "</systemVersion>" + "<deviceCategory xsi:type=\"ns:MEPDeviceCategoryType\">" + str17 + "</deviceCategory>" + "<isDeviceSimulator xsi:type=\"xs:boolean\">" + str20 + "</isDeviceSimulator>" + "</DeviceDetails>" + "<ApplicationDetails xsi:type=\"ns:MEPApplicationDetailsType\">" + "<appID xsi:type=\"ns:MEPAppIDType\">" + "<deviceAppID xsi:type=\"xs:string\">" + str16 + "</deviceAppID>" + "</appID>" + "<appName xsi:type=\"xs:string\">" + str21 + "</appName>" + "<appDisplayName xsi:type=\"xs:string\">" + str15 + "</appDisplayName>" + "<clientPlatform xsi:type=\"xs:string\">" + str14 + "</clientPlatform>" + "</ApplicationDetails>" + "<MEPVersion xsi:type=\"xs:string\">" + str26 + "</MEPVersion>";
                FileInputStream openFileInput = o.a().e().openFileInput("DeviceReferenceToken");
                byte[] bArr = new byte[openFileInput.available()];
                openFileInput.read(bArr);
                openFileInput.close();
                str28 = new String(bArr);
                return (str44 + "<DeviceReferenceToken xsi:type=\"xs:string\">" + str28 + "</DeviceReferenceToken>") + "</Request></MEPDeviceInterrogationReq></SOAP-ENV:Body></SOAP-ENV:Envelope>";
            }
            try {
                String deviceId = telephonyManager.getDeviceId();
                String encode = URLEncoder.encode("Phone", "utf-8");
                if (deviceId == null) {
                    deviceId = ((WifiManager) o.a().e().getSystemService("wifi")).getConnectionInfo().getMacAddress();
                    str31 = URLEncoder.encode("MAC", "utf-8");
                    str32 = URLEncoder.encode("Tablet", "utf-8");
                    str33 = "AndroidGSM";
                } else {
                    String str45 = str30;
                    str31 = str29;
                    str32 = encode;
                    str33 = str45;
                }
                try {
                    str37 = URLEncoder.encode(deviceId, "utf-8");
                    str38 = URLEncoder.encode(Build.DEVICE);
                    String str46 = Build.MODEL;
                    try {
                        replaceAll = str46.replaceAll(" ", "%20");
                        str39 = URLEncoder.encode("Android", "utf-8");
                        str40 = URLEncoder.encode(Build.VERSION.SDK, "utf-8");
                        str35 = URLEncoder.encode(o.x(), "utf-8");
                        packageName = o.a().e().getPackageName();
                        try {
                            PackageManager packageManager = o.a().e().getPackageManager();
                            List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(0);
                            Collections.sort(installedApplications, new ApplicationInfo.DisplayNameComparator(packageManager));
                            int i = 0;
                            while (true) {
                                int i2 = i;
                                if (i2 >= installedApplications.size()) {
                                    str34 = null;
                                    break;
                                }
                                ApplicationInfo applicationInfo = installedApplications.get(i2);
                                if (applicationInfo.packageName.equals(packageName)) {
                                    str34 = applicationInfo.loadLabel(packageManager).toString();
                                    break;
                                }
                                i = i2 + 1;
                            }
                            if (str34 == null) {
                                str34 = packageName;
                            }
                        } catch (Exception e3) {
                            e = e3;
                            str4 = replaceAll;
                            str11 = packageName;
                            str3 = str37;
                            str12 = "";
                            String str47 = str31;
                            str6 = str39;
                            str9 = str47;
                            String str48 = str40;
                            str8 = str32;
                            str2 = str35;
                            str13 = str33;
                            str7 = str48;
                            String str49 = packageName;
                            str5 = str38;
                            str10 = str49;
                            e.printStackTrace();
                            str14 = str13;
                            str15 = str12;
                            str16 = str10;
                            str17 = str8;
                            str18 = str6;
                            str19 = str4;
                            str20 = str36;
                            str21 = str11;
                            str22 = str9;
                            str23 = str7;
                            str24 = str5;
                            str25 = str3;
                            str26 = str2;
                            str27 = str;
                            String str442 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:cc=\"urn:ebay:apis:CoreComponentTypes\" xmlns:ed=\"urn:ebay:apis:EnhancedDataTypes\" xmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xmlns:ebl=\"urn:ebay:apis:eBLBaseComponents\" xmlns:ns=\"urn:ebay:api:PayPalAPI\"><SOAP-ENV:Header><Security xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xsi:type=\"wsse:SecurityType\"></Security><RequesterCredentials xmlns=\"urn:ebay:api:PayPalAPI\" xsi:type=\"ebl:CustomSecurityHeaderType\"><Credentials xmlns=\"urn:ebay:apis:eBLBaseComponents\" xsi:type=\"ebl:UserIdPasswordType\"><Username xsi:type=\"xs:string\">gmapi_client</Username><Password xsi:type=\"xs:string\">11111111</Password></Credentials></RequesterCredentials></SOAP-ENV:Header><SOAP-ENV:Body id=\"_0\"><MEPDeviceInterrogationReq xmlns=\"urn:ebay:api:PayPalAPI\"><Request><Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">1.0</Version><PayPalAppID xsi:type=\"xs:string\">" + str27 + "</PayPalAppID>" + "<DeviceDetails xsi:type=\"ns:MEPDeviceDetailsType\">" + "<deviceID xsi:type=\"ns:MEPDeviceIDType\">" + "<deviceIdentifier xsi:type=\"xs:string\">" + str25 + "</deviceIdentifier>" + "<deviceIdType xsi:type=\"ns:MEPDeviceIdentifierType\">" + str22 + "</deviceIdType>" + "</deviceID>" + "<deviceName xsi:type=\"xs:string\">" + str24 + "</deviceName>" + "<deviceModel xsi:type=\"xs:string\">" + str19 + "</deviceModel>" + "<systemName xsi:type=\"xs:string\">" + str18 + "</systemName>" + "<systemVersion xsi:type=\"xs:string\">" + str23 + "</systemVersion>" + "<deviceCategory xsi:type=\"ns:MEPDeviceCategoryType\">" + str17 + "</deviceCategory>" + "<isDeviceSimulator xsi:type=\"xs:boolean\">" + str20 + "</isDeviceSimulator>" + "</DeviceDetails>" + "<ApplicationDetails xsi:type=\"ns:MEPApplicationDetailsType\">" + "<appID xsi:type=\"ns:MEPAppIDType\">" + "<deviceAppID xsi:type=\"xs:string\">" + str16 + "</deviceAppID>" + "</appID>" + "<appName xsi:type=\"xs:string\">" + str21 + "</appName>" + "<appDisplayName xsi:type=\"xs:string\">" + str15 + "</appDisplayName>" + "<clientPlatform xsi:type=\"xs:string\">" + str14 + "</clientPlatform>" + "</ApplicationDetails>" + "<MEPVersion xsi:type=\"xs:string\">" + str26 + "</MEPVersion>";
                            FileInputStream openFileInput2 = o.a().e().openFileInput("DeviceReferenceToken");
                            byte[] bArr2 = new byte[openFileInput2.available()];
                            openFileInput2.read(bArr2);
                            openFileInput2.close();
                            str28 = new String(bArr2);
                            return (str442 + "<DeviceReferenceToken xsi:type=\"xs:string\">" + str28 + "</DeviceReferenceToken>") + "</Request></MEPDeviceInterrogationReq></SOAP-ENV:Body></SOAP-ENV:Envelope>";
                        }
                    } catch (Exception e4) {
                        Exception exc = e4;
                        str11 = "";
                        str4 = str46;
                        e = exc;
                        str3 = str37;
                        str12 = "";
                        String str50 = str31;
                        str6 = str39;
                        str9 = str50;
                        String str51 = str40;
                        str8 = str32;
                        str2 = str35;
                        str13 = str33;
                        str7 = str51;
                        str5 = str38;
                        str10 = "";
                        e.printStackTrace();
                        str14 = str13;
                        str15 = str12;
                        str16 = str10;
                        str17 = str8;
                        str18 = str6;
                        str19 = str4;
                        str20 = str36;
                        str21 = str11;
                        str22 = str9;
                        str23 = str7;
                        str24 = str5;
                        str25 = str3;
                        str26 = str2;
                        str27 = str;
                        String str4422 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:cc=\"urn:ebay:apis:CoreComponentTypes\" xmlns:ed=\"urn:ebay:apis:EnhancedDataTypes\" xmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xmlns:ebl=\"urn:ebay:apis:eBLBaseComponents\" xmlns:ns=\"urn:ebay:api:PayPalAPI\"><SOAP-ENV:Header><Security xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xsi:type=\"wsse:SecurityType\"></Security><RequesterCredentials xmlns=\"urn:ebay:api:PayPalAPI\" xsi:type=\"ebl:CustomSecurityHeaderType\"><Credentials xmlns=\"urn:ebay:apis:eBLBaseComponents\" xsi:type=\"ebl:UserIdPasswordType\"><Username xsi:type=\"xs:string\">gmapi_client</Username><Password xsi:type=\"xs:string\">11111111</Password></Credentials></RequesterCredentials></SOAP-ENV:Header><SOAP-ENV:Body id=\"_0\"><MEPDeviceInterrogationReq xmlns=\"urn:ebay:api:PayPalAPI\"><Request><Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">1.0</Version><PayPalAppID xsi:type=\"xs:string\">" + str27 + "</PayPalAppID>" + "<DeviceDetails xsi:type=\"ns:MEPDeviceDetailsType\">" + "<deviceID xsi:type=\"ns:MEPDeviceIDType\">" + "<deviceIdentifier xsi:type=\"xs:string\">" + str25 + "</deviceIdentifier>" + "<deviceIdType xsi:type=\"ns:MEPDeviceIdentifierType\">" + str22 + "</deviceIdType>" + "</deviceID>" + "<deviceName xsi:type=\"xs:string\">" + str24 + "</deviceName>" + "<deviceModel xsi:type=\"xs:string\">" + str19 + "</deviceModel>" + "<systemName xsi:type=\"xs:string\">" + str18 + "</systemName>" + "<systemVersion xsi:type=\"xs:string\">" + str23 + "</systemVersion>" + "<deviceCategory xsi:type=\"ns:MEPDeviceCategoryType\">" + str17 + "</deviceCategory>" + "<isDeviceSimulator xsi:type=\"xs:boolean\">" + str20 + "</isDeviceSimulator>" + "</DeviceDetails>" + "<ApplicationDetails xsi:type=\"ns:MEPApplicationDetailsType\">" + "<appID xsi:type=\"ns:MEPAppIDType\">" + "<deviceAppID xsi:type=\"xs:string\">" + str16 + "</deviceAppID>" + "</appID>" + "<appName xsi:type=\"xs:string\">" + str21 + "</appName>" + "<appDisplayName xsi:type=\"xs:string\">" + str15 + "</appDisplayName>" + "<clientPlatform xsi:type=\"xs:string\">" + str14 + "</clientPlatform>" + "</ApplicationDetails>" + "<MEPVersion xsi:type=\"xs:string\">" + str26 + "</MEPVersion>";
                        FileInputStream openFileInput22 = o.a().e().openFileInput("DeviceReferenceToken");
                        byte[] bArr22 = new byte[openFileInput22.available()];
                        openFileInput22.read(bArr22);
                        openFileInput22.close();
                        str28 = new String(bArr22);
                        return (str4422 + "<DeviceReferenceToken xsi:type=\"xs:string\">" + str28 + "</DeviceReferenceToken>") + "</Request></MEPDeviceInterrogationReq></SOAP-ENV:Body></SOAP-ENV:Envelope>";
                    }
                    try {
                        if (str37.equals("000000000000000")) {
                            str36 = "true";
                        }
                        str20 = str36;
                        str26 = str35;
                        str21 = packageName;
                        str15 = str34;
                        str14 = str33;
                        str24 = str38;
                        str17 = str32;
                        str27 = str;
                        String str52 = str40;
                        str18 = str39;
                        str23 = str52;
                        String str53 = str37;
                        str16 = packageName;
                        str25 = str53;
                        String str54 = str31;
                        str19 = replaceAll;
                        str22 = str54;
                    } catch (Exception e5) {
                        str3 = str37;
                        str12 = str34;
                        e = e5;
                        str4 = replaceAll;
                        str11 = packageName;
                        String str55 = str31;
                        str6 = str39;
                        str9 = str55;
                        String str56 = str40;
                        str8 = str32;
                        str2 = str35;
                        str13 = str33;
                        str7 = str56;
                        String str57 = str38;
                        str10 = packageName;
                        str5 = str57;
                        e.printStackTrace();
                        str14 = str13;
                        str15 = str12;
                        str16 = str10;
                        str17 = str8;
                        str18 = str6;
                        str19 = str4;
                        str20 = str36;
                        str21 = str11;
                        str22 = str9;
                        str23 = str7;
                        str24 = str5;
                        str25 = str3;
                        str26 = str2;
                        str27 = str;
                        String str44222 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:cc=\"urn:ebay:apis:CoreComponentTypes\" xmlns:ed=\"urn:ebay:apis:EnhancedDataTypes\" xmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xmlns:ebl=\"urn:ebay:apis:eBLBaseComponents\" xmlns:ns=\"urn:ebay:api:PayPalAPI\"><SOAP-ENV:Header><Security xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xsi:type=\"wsse:SecurityType\"></Security><RequesterCredentials xmlns=\"urn:ebay:api:PayPalAPI\" xsi:type=\"ebl:CustomSecurityHeaderType\"><Credentials xmlns=\"urn:ebay:apis:eBLBaseComponents\" xsi:type=\"ebl:UserIdPasswordType\"><Username xsi:type=\"xs:string\">gmapi_client</Username><Password xsi:type=\"xs:string\">11111111</Password></Credentials></RequesterCredentials></SOAP-ENV:Header><SOAP-ENV:Body id=\"_0\"><MEPDeviceInterrogationReq xmlns=\"urn:ebay:api:PayPalAPI\"><Request><Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">1.0</Version><PayPalAppID xsi:type=\"xs:string\">" + str27 + "</PayPalAppID>" + "<DeviceDetails xsi:type=\"ns:MEPDeviceDetailsType\">" + "<deviceID xsi:type=\"ns:MEPDeviceIDType\">" + "<deviceIdentifier xsi:type=\"xs:string\">" + str25 + "</deviceIdentifier>" + "<deviceIdType xsi:type=\"ns:MEPDeviceIdentifierType\">" + str22 + "</deviceIdType>" + "</deviceID>" + "<deviceName xsi:type=\"xs:string\">" + str24 + "</deviceName>" + "<deviceModel xsi:type=\"xs:string\">" + str19 + "</deviceModel>" + "<systemName xsi:type=\"xs:string\">" + str18 + "</systemName>" + "<systemVersion xsi:type=\"xs:string\">" + str23 + "</systemVersion>" + "<deviceCategory xsi:type=\"ns:MEPDeviceCategoryType\">" + str17 + "</deviceCategory>" + "<isDeviceSimulator xsi:type=\"xs:boolean\">" + str20 + "</isDeviceSimulator>" + "</DeviceDetails>" + "<ApplicationDetails xsi:type=\"ns:MEPApplicationDetailsType\">" + "<appID xsi:type=\"ns:MEPAppIDType\">" + "<deviceAppID xsi:type=\"xs:string\">" + str16 + "</deviceAppID>" + "</appID>" + "<appName xsi:type=\"xs:string\">" + str21 + "</appName>" + "<appDisplayName xsi:type=\"xs:string\">" + str15 + "</appDisplayName>" + "<clientPlatform xsi:type=\"xs:string\">" + str14 + "</clientPlatform>" + "</ApplicationDetails>" + "<MEPVersion xsi:type=\"xs:string\">" + str26 + "</MEPVersion>";
                        FileInputStream openFileInput222 = o.a().e().openFileInput("DeviceReferenceToken");
                        byte[] bArr222 = new byte[openFileInput222.available()];
                        openFileInput222.read(bArr222);
                        openFileInput222.close();
                        str28 = new String(bArr222);
                        return (str44222 + "<DeviceReferenceToken xsi:type=\"xs:string\">" + str28 + "</DeviceReferenceToken>") + "</Request></MEPDeviceInterrogationReq></SOAP-ENV:Body></SOAP-ENV:Envelope>";
                    }
                } catch (Exception e6) {
                    e = e6;
                    String str58 = str33;
                    str7 = str40;
                    str8 = str32;
                    str2 = str35;
                    str13 = str58;
                    String str59 = str31;
                    str6 = str39;
                    str9 = str59;
                    str5 = str38;
                    str10 = "";
                    str3 = str37;
                    str12 = "";
                    str11 = "";
                    str4 = "";
                }
            } catch (Exception e7) {
                e = e7;
                String str60 = str30;
                str6 = str39;
                str9 = str29;
                str2 = str35;
                str13 = str60;
                str3 = str37;
                str12 = "";
                String str61 = str40;
                str8 = "";
                str7 = str61;
                String str62 = str38;
                str10 = "";
                str5 = str62;
                str11 = "";
                str4 = "";
                e.printStackTrace();
                str14 = str13;
                str15 = str12;
                str16 = str10;
                str17 = str8;
                str18 = str6;
                str19 = str4;
                str20 = str36;
                str21 = str11;
                str22 = str9;
                str23 = str7;
                str24 = str5;
                str25 = str3;
                str26 = str2;
                str27 = str;
                String str442222 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:cc=\"urn:ebay:apis:CoreComponentTypes\" xmlns:ed=\"urn:ebay:apis:EnhancedDataTypes\" xmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xmlns:ebl=\"urn:ebay:apis:eBLBaseComponents\" xmlns:ns=\"urn:ebay:api:PayPalAPI\"><SOAP-ENV:Header><Security xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xsi:type=\"wsse:SecurityType\"></Security><RequesterCredentials xmlns=\"urn:ebay:api:PayPalAPI\" xsi:type=\"ebl:CustomSecurityHeaderType\"><Credentials xmlns=\"urn:ebay:apis:eBLBaseComponents\" xsi:type=\"ebl:UserIdPasswordType\"><Username xsi:type=\"xs:string\">gmapi_client</Username><Password xsi:type=\"xs:string\">11111111</Password></Credentials></RequesterCredentials></SOAP-ENV:Header><SOAP-ENV:Body id=\"_0\"><MEPDeviceInterrogationReq xmlns=\"urn:ebay:api:PayPalAPI\"><Request><Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">1.0</Version><PayPalAppID xsi:type=\"xs:string\">" + str27 + "</PayPalAppID>" + "<DeviceDetails xsi:type=\"ns:MEPDeviceDetailsType\">" + "<deviceID xsi:type=\"ns:MEPDeviceIDType\">" + "<deviceIdentifier xsi:type=\"xs:string\">" + str25 + "</deviceIdentifier>" + "<deviceIdType xsi:type=\"ns:MEPDeviceIdentifierType\">" + str22 + "</deviceIdType>" + "</deviceID>" + "<deviceName xsi:type=\"xs:string\">" + str24 + "</deviceName>" + "<deviceModel xsi:type=\"xs:string\">" + str19 + "</deviceModel>" + "<systemName xsi:type=\"xs:string\">" + str18 + "</systemName>" + "<systemVersion xsi:type=\"xs:string\">" + str23 + "</systemVersion>" + "<deviceCategory xsi:type=\"ns:MEPDeviceCategoryType\">" + str17 + "</deviceCategory>" + "<isDeviceSimulator xsi:type=\"xs:boolean\">" + str20 + "</isDeviceSimulator>" + "</DeviceDetails>" + "<ApplicationDetails xsi:type=\"ns:MEPApplicationDetailsType\">" + "<appID xsi:type=\"ns:MEPAppIDType\">" + "<deviceAppID xsi:type=\"xs:string\">" + str16 + "</deviceAppID>" + "</appID>" + "<appName xsi:type=\"xs:string\">" + str21 + "</appName>" + "<appDisplayName xsi:type=\"xs:string\">" + str15 + "</appDisplayName>" + "<clientPlatform xsi:type=\"xs:string\">" + str14 + "</clientPlatform>" + "</ApplicationDetails>" + "<MEPVersion xsi:type=\"xs:string\">" + str26 + "</MEPVersion>";
                FileInputStream openFileInput2222 = o.a().e().openFileInput("DeviceReferenceToken");
                byte[] bArr2222 = new byte[openFileInput2222.available()];
                openFileInput2222.read(bArr2222);
                openFileInput2222.close();
                str28 = new String(bArr2222);
                return (str442222 + "<DeviceReferenceToken xsi:type=\"xs:string\">" + str28 + "</DeviceReferenceToken>") + "</Request></MEPDeviceInterrogationReq></SOAP-ENV:Body></SOAP-ENV:Envelope>";
            }
        } catch (Exception e8) {
            Exception exc2 = e8;
            str = "";
            e = exc2;
            str3 = str37;
            str12 = "";
            str5 = str38;
            str10 = "";
            str7 = str40;
            str8 = "";
            String str63 = str39;
            str9 = "";
            str6 = str63;
            str11 = "";
            str4 = "";
            String str64 = str35;
            str13 = "";
            str2 = str64;
        }
        String str4422222 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:cc=\"urn:ebay:apis:CoreComponentTypes\" xmlns:ed=\"urn:ebay:apis:EnhancedDataTypes\" xmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xmlns:ebl=\"urn:ebay:apis:eBLBaseComponents\" xmlns:ns=\"urn:ebay:api:PayPalAPI\"><SOAP-ENV:Header><Security xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xsi:type=\"wsse:SecurityType\"></Security><RequesterCredentials xmlns=\"urn:ebay:api:PayPalAPI\" xsi:type=\"ebl:CustomSecurityHeaderType\"><Credentials xmlns=\"urn:ebay:apis:eBLBaseComponents\" xsi:type=\"ebl:UserIdPasswordType\"><Username xsi:type=\"xs:string\">gmapi_client</Username><Password xsi:type=\"xs:string\">11111111</Password></Credentials></RequesterCredentials></SOAP-ENV:Header><SOAP-ENV:Body id=\"_0\"><MEPDeviceInterrogationReq xmlns=\"urn:ebay:api:PayPalAPI\"><Request><Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">1.0</Version><PayPalAppID xsi:type=\"xs:string\">" + str27 + "</PayPalAppID>" + "<DeviceDetails xsi:type=\"ns:MEPDeviceDetailsType\">" + "<deviceID xsi:type=\"ns:MEPDeviceIDType\">" + "<deviceIdentifier xsi:type=\"xs:string\">" + str25 + "</deviceIdentifier>" + "<deviceIdType xsi:type=\"ns:MEPDeviceIdentifierType\">" + str22 + "</deviceIdType>" + "</deviceID>" + "<deviceName xsi:type=\"xs:string\">" + str24 + "</deviceName>" + "<deviceModel xsi:type=\"xs:string\">" + str19 + "</deviceModel>" + "<systemName xsi:type=\"xs:string\">" + str18 + "</systemName>" + "<systemVersion xsi:type=\"xs:string\">" + str23 + "</systemVersion>" + "<deviceCategory xsi:type=\"ns:MEPDeviceCategoryType\">" + str17 + "</deviceCategory>" + "<isDeviceSimulator xsi:type=\"xs:boolean\">" + str20 + "</isDeviceSimulator>" + "</DeviceDetails>" + "<ApplicationDetails xsi:type=\"ns:MEPApplicationDetailsType\">" + "<appID xsi:type=\"ns:MEPAppIDType\">" + "<deviceAppID xsi:type=\"xs:string\">" + str16 + "</deviceAppID>" + "</appID>" + "<appName xsi:type=\"xs:string\">" + str21 + "</appName>" + "<appDisplayName xsi:type=\"xs:string\">" + str15 + "</appDisplayName>" + "<clientPlatform xsi:type=\"xs:string\">" + str14 + "</clientPlatform>" + "</ApplicationDetails>" + "<MEPVersion xsi:type=\"xs:string\">" + str26 + "</MEPVersion>";
        try {
            FileInputStream openFileInput22222 = o.a().e().openFileInput("DeviceReferenceToken");
            byte[] bArr22222 = new byte[openFileInput22222.available()];
            openFileInput22222.read(bArr22222);
            openFileInput22222.close();
            str28 = new String(bArr22222);
        } catch (Exception e9) {
            str28 = "";
        }
        return (str4422222 + "<DeviceReferenceToken xsi:type=\"xs:string\">" + str28 + "</DeviceReferenceToken>") + "</Request></MEPDeviceInterrogationReq></SOAP-ENV:Body></SOAP-ENV:Envelope>";
    }

    public static String a(String str, String str2) {
        if (str == null) {
            return null;
        }
        if (str.indexOf("<") == -1 || str.indexOf(">") == -1) {
            return str;
        }
        try {
            return URLEncoder.encode(str, str2);
        } catch (Throwable th) {
            return null;
        }
    }

    public static String a(NodeList nodeList) {
        String str = "";
        for (int i = 0; i < nodeList.getLength(); i++) {
            str = str + nodeList.item(i).getNodeValue();
        }
        return str;
    }

    public static boolean a(String str, Hashtable hashtable) {
        try {
            NodeList elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8"))).getElementsByTagName("MEPDeviceInterrogationResponseType");
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NodeList childNodes = elementsByTagName.item(i).getChildNodes();
                for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                    Node item = childNodes.item(i2);
                    String nodeName = item.getNodeName();
                    if (nodeName.compareTo("PayButtonEnable") == 0) {
                        String nodeValue = item.getChildNodes().item(0).getNodeValue();
                        hashtable.put(nodeName, nodeValue);
                        if (nodeValue.compareToIgnoreCase("true") != 0) {
                            Log.e("Error", "Authentication failed, button not enabled.");
                        }
                    } else if (nodeName.compareTo("EncryptionDetails") == 0) {
                        NodeList childNodes2 = item.getChildNodes();
                        for (int i3 = 0; i3 < childNodes2.getLength(); i3++) {
                            Node item2 = childNodes2.item(i3);
                            String nodeName2 = item2.getNodeName();
                            if (item2.getChildNodes().getLength() > 0 && nodeName2.compareTo("Type") == 0) {
                                hashtable.put(nodeName, item2.getChildNodes().item(0).getNodeValue());
                            }
                        }
                    } else if (nodeName.compareTo("DeviceReferenceToken") == 0) {
                        String nodeValue2 = item.getChildNodes().item(0).getNodeValue();
                        hashtable.put(nodeName, nodeValue2);
                        try {
                            FileOutputStream openFileOutput = o.a().e().openFileOutput("DeviceReferenceToken", 2);
                            openFileOutput.write(nodeValue2.getBytes());
                            openFileOutput.flush();
                            openFileOutput.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e2) {
                        }
                    } else if (nodeName.compareTo("DeviceAuthDetails") == 0) {
                        NodeList childNodes3 = item.getChildNodes();
                        for (int i4 = 0; i4 < childNodes3.getLength(); i4++) {
                            Node item3 = childNodes3.item(i4);
                            String nodeName3 = item3.getNodeName();
                            if (item3.getChildNodes().getLength() > 0) {
                                String nodeValue3 = item3.getChildNodes().item(0).getNodeValue();
                                if (nodeName3.compareTo("UserName") == 0) {
                                    o.a().d(nodeValue3);
                                } else if (nodeName3.compareTo("Email") == 0) {
                                    o.a().e(nodeValue3);
                                } else if (nodeName3.compareTo("Phone") == 0) {
                                    String str2 = "";
                                    NodeList childNodes4 = item3.getChildNodes();
                                    for (int i5 = 0; i5 < childNodes4.getLength(); i5++) {
                                        if (childNodes4.item(i5).getChildNodes().getLength() > 0) {
                                            str2 = str2 + childNodes4.item(i5).getChildNodes().item(0).getNodeValue();
                                        }
                                    }
                                    if (str2.length() != 0) {
                                        o.a().f(str2);
                                    }
                                } else if (nodeName3.compareTo("AuthMethod") == 0) {
                                    o.a().b(Integer.parseInt(nodeValue3));
                                } else if (nodeName3.compareTo("AuthSettings") == 0) {
                                    o.a().c(Integer.parseInt(nodeValue3));
                                }
                            }
                        }
                        if (o.a().t() == 0 && o.a().s() == 2) {
                            o.a().b(0);
                        }
                    }
                }
            }
            return true;
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public static String b() {
        String str = "";
        try {
            FileInputStream openFileInput = o.a().e().openFileInput("DeviceReferenceToken");
            byte[] bArr = new byte[openFileInput.available()];
            openFileInput.read(bArr);
            openFileInput.close();
            str = new String(bArr);
        } catch (Exception e) {
        }
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:cc=\"urn:ebay:apis:CoreComponentTypes\" xmlns:ed=\"urn:ebay:apis:EnhancedDataTypes\" xmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xmlns:ebl=\"urn:ebay:apis:eBLBaseComponents\" xmlns:ns=\"urn:ebay:api:PayPalAPI\"><SOAP-ENV:Header><Security xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xsi:type=\"wsse:SecurityType\"></Security><RequesterCredentials xmlns=\"urn:ebay:api:PayPalAPI\" xsi:type=\"ebl:CustomSecurityHeaderType\"><Credentials xmlns=\"urn:ebay:apis:eBLBaseComponents\" xsi:type=\"ebl:UserIdPasswordType\"><Username xsi:type=\"xs:string\">gmapi_client</Username><Password xsi:type=\"xs:string\">11111111</Password></Credentials></RequesterCredentials></SOAP-ENV:Header><SOAP-ENV:Body id=\"_0\"><MEPRemoveDeviceAuthorizationReq xmlns=\"urn:ebay:api:PayPalAPI\"><Request><Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">1</Version><SessionToken xsi:type=\"ns:MEPSessionToken\">" + "" + "</SessionToken>" + "<DeviceReferenceToken xsi:type=\"xs:string\">" + str + "</DeviceReferenceToken>" + "</Request>" + "</MEPRemoveDeviceAuthorizationReq>" + "</SOAP-ENV:Body>" + "</SOAP-ENV:Envelope>";
    }

    public static boolean b(String str, Hashtable hashtable) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            NodeList elementsByTagName = parse.getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            NodeList elementsByTagName6 = parse.getElementsByTagName("payKey");
            if (elementsByTagName6.getLength() == 0) {
                return false;
            }
            hashtable.put("PayKey", a(((Element) elementsByTagName6.item(0)).getChildNodes()));
            NodeList elementsByTagName7 = parse.getElementsByTagName("paymentExecStatus");
            if (elementsByTagName7.getLength() == 0) {
                return false;
            }
            hashtable.put("PaymentExecStatus", a(((Element) elementsByTagName7.item(0)).getChildNodes()));
            NodeList elementsByTagName8 = parse.getElementsByTagName("defaultFundingPlan");
            if (elementsByTagName8.getLength() != 0) {
                hashtable.put("DefaultFundingPlan", g.a((Element) elementsByTagName8.item(0)));
            }
            NodeList elementsByTagName9 = parse.getElementsByTagName("payErrorList");
            return elementsByTagName9.getLength() <= 0 || ((Element) elementsByTagName9.item(0)).getElementsByTagName("payError").getLength() != 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String[] b(String str) {
        Vector vector = new Vector();
        try {
            NodeList elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8"))).getElementsByTagName("parameter");
            if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
                return null;
            }
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                vector.add(elementsByTagName.item(i).getChildNodes().item(0).getNodeValue());
            }
            String[] strArr = new String[vector.size()];
            for (int i2 = 0; i2 < vector.size(); i2++) {
                strArr[i2] = (String) vector.get(i2);
            }
            return strArr;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String c(String str) {
        try {
            NodeList elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8"))).getElementsByTagName("TransactionID");
            if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
                return null;
            }
            return elementsByTagName.item(0).getChildNodes().item(0).getNodeValue();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean c(String str, Hashtable hashtable) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            NodeList elementsByTagName = parse.getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            NodeList elementsByTagName6 = parse.getElementsByTagName("preapprovalKey");
            if (elementsByTagName6.getLength() == 0) {
                return false;
            }
            hashtable.put("PreapprovalKey", a(((Element) elementsByTagName6.item(0)).getChildNodes()));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String d(String str) {
        String[] strArr = {"+1", "+44", "+61", "+54", "+43", "+32", "+55", "+41", "+56", "+86", "+506", "+357", "+420", "+49", "+45", "+593", "+372", "+34", "+358", "+33", "+30", "+852", "+36", "+353", "+972", "+91", "+354", "+39", "+81", "+82", "+370", "+352", "+371", "+377", "+356", "+52", "+60", "+31", "+47", "+64", "+48", "+351", "+46", "+65", "+386", "+421", "+66", "+90", "+886", "+598", "+58", "+27"};
        if (str.indexOf("+") != 0) {
            return str;
        }
        String str2 = str;
        for (int i = 0; i < strArr.length; i++) {
            if (str.indexOf(strArr[i]) >= 0) {
                str2 = str.replace(strArr[i], "");
            }
        }
        return str2;
    }

    public static boolean d(String str, Hashtable hashtable) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            o a = o.a();
            com.paypal.android.MEP.c d = a.d();
            NodeList elementsByTagName = parse.getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            NodeList elementsByTagName6 = parse.getElementsByTagName("approved");
            if (elementsByTagName6.getLength() == 0) {
                return false;
            }
            String a2 = a(((Element) elementsByTagName6.item(0)).getChildNodes());
            d.a(a2.equals("true"));
            hashtable.put("Approved", a2);
            NodeList elementsByTagName7 = parse.getElementsByTagName("cancelUrl");
            if (elementsByTagName7.getLength() == 0) {
                return false;
            }
            String a3 = a(((Element) elementsByTagName7.item(0)).getChildNodes());
            a.b(a3);
            hashtable.put("CancelUrl", a3);
            NodeList elementsByTagName8 = parse.getElementsByTagName("currencyCode");
            if (elementsByTagName8.getLength() == 0) {
                return false;
            }
            String a4 = a(((Element) elementsByTagName8.item(0)).getChildNodes());
            d.a(a4);
            hashtable.put("CurrencyCode", a4);
            NodeList elementsByTagName9 = parse.getElementsByTagName("dateOfMonth");
            if (elementsByTagName9.getLength() > 0) {
                String a5 = a(((Element) elementsByTagName9.item(0)).getChildNodes());
                d.d(Integer.parseInt(a5));
                hashtable.put("DateOfMonth", a5);
            }
            NodeList elementsByTagName10 = parse.getElementsByTagName("dayOfWeek");
            if (elementsByTagName10.getLength() > 0) {
                String a6 = a(((Element) elementsByTagName10.item(0)).getChildNodes());
                d.c(d.g(a6));
                hashtable.put("DayOfWeek", a6);
            }
            NodeList elementsByTagName11 = parse.getElementsByTagName("endingDate");
            if (elementsByTagName11.getLength() > 0) {
                String a7 = a(((Element) elementsByTagName11.item(0)).getChildNodes());
                d.c(a7);
                hashtable.put("EndingDate", a7);
            }
            NodeList elementsByTagName12 = parse.getElementsByTagName("ipnNotificationUrl");
            if (elementsByTagName12.getLength() > 0) {
                String a8 = a(((Element) elementsByTagName12.item(0)).getChildNodes());
                d.d(a8);
                hashtable.put("IpnNotificationUrl", a8);
            }
            NodeList elementsByTagName13 = parse.getElementsByTagName("maxAmountPerPayment");
            if (elementsByTagName13.getLength() > 0) {
                String a9 = a(((Element) elementsByTagName13.item(0)).getChildNodes());
                d.a(new BigDecimal(a9));
                hashtable.put("MaxAmountPerPayment", a9);
            }
            NodeList elementsByTagName14 = parse.getElementsByTagName("maxNumberOfPayments");
            if (elementsByTagName14.getLength() > 0) {
                String a10 = a(((Element) elementsByTagName14.item(0)).getChildNodes());
                d.a(Integer.parseInt(a10));
                hashtable.put("MaxNumberOfPayments", a10);
            }
            NodeList elementsByTagName15 = parse.getElementsByTagName("maxNumberOfPaymentsPerPeriod");
            if (elementsByTagName15.getLength() > 0) {
                String a11 = a(((Element) elementsByTagName15.item(0)).getChildNodes());
                d.e(Integer.parseInt(a11));
                hashtable.put("MaxNumberOfPaymentsPerPeriod", a11);
            }
            NodeList elementsByTagName16 = parse.getElementsByTagName("maxTotalAmountOfAllPayments");
            if (elementsByTagName16.getLength() == 0) {
                return false;
            }
            String a12 = a(((Element) elementsByTagName16.item(0)).getChildNodes());
            d.b(new BigDecimal(a12));
            hashtable.put("MaxTotalAmountOfAllPayments", a12);
            NodeList elementsByTagName17 = parse.getElementsByTagName("memo");
            if (elementsByTagName17.getLength() > 0) {
                String a13 = a(((Element) elementsByTagName17.item(0)).getChildNodes());
                d.e(a13);
                hashtable.put("Memo", a13);
            }
            NodeList elementsByTagName18 = parse.getElementsByTagName("paymentPeriod");
            if (elementsByTagName18.getLength() > 0) {
                String a14 = a(((Element) elementsByTagName18.item(0)).getChildNodes());
                d.b(d.f(a14));
                hashtable.put("PaymentPeriod", a14);
            }
            NodeList elementsByTagName19 = parse.getElementsByTagName("pinType");
            if (elementsByTagName19.getLength() > 0) {
                String a15 = a(((Element) elementsByTagName19.item(0)).getChildNodes());
                d.b(a15.equals("REQUIRED"));
                hashtable.put("PinType", a15);
            }
            NodeList elementsByTagName20 = parse.getElementsByTagName("returnUrl");
            if (elementsByTagName20.getLength() == 0) {
                return false;
            }
            String a16 = a(((Element) elementsByTagName20.item(0)).getChildNodes());
            a.c(a16);
            hashtable.put("ReturnUrl", a16);
            NodeList elementsByTagName21 = parse.getElementsByTagName("senderEmail");
            if (elementsByTagName21.getLength() > 0) {
                hashtable.put("SenderEmail", a(((Element) elementsByTagName21.item(0)).getChildNodes()));
            }
            NodeList elementsByTagName22 = parse.getElementsByTagName("startingDate");
            if (elementsByTagName22.getLength() == 0) {
                return false;
            }
            String a17 = a(((Element) elementsByTagName22.item(0)).getChildNodes());
            d.b(a17);
            hashtable.put("StartingDate", a17);
            hashtable.put("PreapprovalDetails", d);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String e(String str) {
        String[] strArr = {"+1", "+44", "+61", "+54", "+43", "+32", "+55", "+41", "+56", "+86", "+506", "+357", "+420", "+49", "+45", "+593", "+372", "+34", "+358", "+33", "+30", "+852", "+36", "+353", "+972", "+91", "+354", "+39", "+81", "+82", "+370", "+352", "+371", "+377", "+356", "+52", "+60", "+31", "+47", "+64", "+48", "+351", "+46", "+65", "+386", "+421", "+66", "+90", "+886", "+598", "+58", "+27"};
        if (str.indexOf("+") != 0) {
            return "";
        }
        String str2 = "";
        for (int i = 0; i < strArr.length; i++) {
            if (str.indexOf(strArr[i]) >= 0) {
                str2 = strArr[i].substring(1);
            }
        }
        return str2;
    }

    public static boolean e(String str, Hashtable hashtable) {
        try {
            NodeList elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8"))).getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean f(String str, Hashtable hashtable) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            NodeList elementsByTagName = parse.getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            Vector vector = new Vector();
            NodeList elementsByTagName6 = parse.getElementsByTagName("fundingPlan");
            for (int i = 0; i < elementsByTagName6.getLength(); i++) {
                vector.add(g.a((Element) elementsByTagName6.item(i)));
            }
            hashtable.put("FundingPlans", vector);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean g(String str, Hashtable hashtable) {
        try {
            NodeList elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8"))).getElementsByTagName("MEPCreateDevicePinResponseType");
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NodeList childNodes = elementsByTagName.item(i).getChildNodes();
                for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                    Node item = childNodes.item(i2);
                    String nodeName = item.getNodeName();
                    if (nodeName.compareTo("SessionToken") == 0) {
                        hashtable.put(nodeName, item.getChildNodes().item(0).getNodeValue());
                    }
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean h(String str, Hashtable hashtable) {
        try {
            NodeList elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8"))).getElementsByTagName("MEPRemoveDeviceAuthorizationResponseType");
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NodeList childNodes = elementsByTagName.item(i).getChildNodes();
                for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                    Node item = childNodes.item(i2);
                    String nodeName = item.getNodeName();
                    if (nodeName.compareTo("DeviceReferenceToken") == 0) {
                        String nodeValue = item.getChildNodes().item(0).getNodeValue();
                        hashtable.put(nodeName, nodeValue);
                        try {
                            FileOutputStream openFileOutput = o.a().e().openFileOutput("DeviceReferenceToken", 2);
                            openFileOutput.write(nodeValue.getBytes());
                            openFileOutput.flush();
                            openFileOutput.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e2) {
                        }
                    }
                }
            }
            return true;
        } catch (Exception e3) {
            return false;
        }
    }

    public static boolean i(String str, Hashtable hashtable) {
        boolean z;
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            NodeList elementsByTagName = parse.getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            NodeList elementsByTagName6 = parse.getElementsByTagName("availableAddress");
            if (elementsByTagName6.getLength() == 0) {
                return false;
            }
            Vector vector = new Vector();
            for (int i = 0; i < elementsByTagName6.getLength(); i++) {
                i iVar = new i();
                Element element2 = (Element) elementsByTagName6.item(i);
                NodeList elementsByTagName7 = element2.getElementsByTagName("baseAddress");
                if (elementsByTagName7.getLength() == 0) {
                    z = false;
                } else {
                    Element element3 = (Element) elementsByTagName7.item(0);
                    NodeList elementsByTagName8 = element3.getElementsByTagName("line1");
                    if (elementsByTagName8.getLength() == 0) {
                        z = false;
                    } else {
                        iVar.d(a(((Element) elementsByTagName8.item(0)).getChildNodes()));
                        NodeList elementsByTagName9 = element3.getElementsByTagName("line2");
                        if (elementsByTagName9.getLength() > 0) {
                            iVar.e(a(((Element) elementsByTagName9.item(0)).getChildNodes()));
                        }
                        NodeList elementsByTagName10 = element3.getElementsByTagName("city");
                        if (elementsByTagName10.getLength() == 0) {
                            z = false;
                        } else {
                            iVar.b(a(((Element) elementsByTagName10.item(0)).getChildNodes()));
                            NodeList elementsByTagName11 = element3.getElementsByTagName("state");
                            if (elementsByTagName11.getLength() > 0) {
                                iVar.g(a(((Element) elementsByTagName11.item(0)).getChildNodes()));
                            }
                            NodeList elementsByTagName12 = element3.getElementsByTagName("postalCode");
                            if (elementsByTagName12.getLength() > 0) {
                                iVar.f(a(((Element) elementsByTagName12.item(0)).getChildNodes()));
                            }
                            NodeList elementsByTagName13 = element3.getElementsByTagName("countryCode");
                            if (elementsByTagName13.getLength() == 0) {
                                z = false;
                            } else {
                                iVar.c(a(((Element) elementsByTagName13.item(0)).getChildNodes()));
                                NodeList elementsByTagName14 = element3.getElementsByTagName("type");
                                if (elementsByTagName14.getLength() > 0) {
                                    a(((Element) elementsByTagName14.item(0)).getChildNodes());
                                }
                                NodeList elementsByTagName15 = element2.getElementsByTagName("addressId");
                                if (elementsByTagName15.getLength() == 0) {
                                    z = false;
                                } else {
                                    iVar.h(a(((Element) elementsByTagName15.item(0)).getChildNodes()));
                                    NodeList elementsByTagName16 = element2.getElementsByTagName("addresseeName");
                                    if (elementsByTagName16.getLength() > 0) {
                                        iVar.a(a(((Element) elementsByTagName16.item(0)).getChildNodes()));
                                    }
                                    z = true;
                                }
                            }
                        }
                    }
                }
                if (z) {
                    vector.add(iVar);
                }
            }
            hashtable.put("AvailableAddresses", vector);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
