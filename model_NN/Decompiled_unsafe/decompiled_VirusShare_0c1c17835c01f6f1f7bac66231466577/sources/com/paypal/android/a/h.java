package com.paypal.android.a;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.paypal.android.MEP.o;

public final class h {
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003c A[SYNTHETIC, Splitter:B:16:0x003c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.drawable.Drawable a(int r4, int r5) {
        /*
            r3 = 0
            byte[] r0 = com.paypal.android.a.b.a(r4, r5)     // Catch:{ Throwable -> 0x002f, all -> 0x0038 }
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Throwable -> 0x002f, all -> 0x0038 }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x002f, all -> 0x0038 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0048, all -> 0x0046 }
            r0.<init>()     // Catch:{ Throwable -> 0x0048, all -> 0x0046 }
            java.lang.String r2 = ""
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0048, all -> 0x0046 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Throwable -> 0x0048, all -> 0x0046 }
            java.lang.String r2 = "."
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0048, all -> 0x0046 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Throwable -> 0x0048, all -> 0x0046 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0048, all -> 0x0046 }
            android.graphics.drawable.Drawable r0 = android.graphics.drawable.Drawable.createFromStream(r1, r0)     // Catch:{ Throwable -> 0x0048, all -> 0x0046 }
            r1.close()     // Catch:{ Throwable -> 0x0040 }
        L_0x002e:
            return r0
        L_0x002f:
            r0 = move-exception
            r0 = r3
        L_0x0031:
            if (r0 == 0) goto L_0x0036
            r0.close()     // Catch:{ Throwable -> 0x0042 }
        L_0x0036:
            r0 = r3
            goto L_0x002e
        L_0x0038:
            r0 = move-exception
            r1 = r3
        L_0x003a:
            if (r1 == 0) goto L_0x003f
            r1.close()     // Catch:{ Throwable -> 0x0044 }
        L_0x003f:
            throw r0
        L_0x0040:
            r1 = move-exception
            goto L_0x002e
        L_0x0042:
            r0 = move-exception
            goto L_0x0036
        L_0x0044:
            r1 = move-exception
            goto L_0x003f
        L_0x0046:
            r0 = move-exception
            goto L_0x003a
        L_0x0048:
            r0 = move-exception
            r0 = r1
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.a.h.a(int, int):android.graphics.drawable.Drawable");
    }

    public static StateListDrawable a() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-6733, -22016});
        GradientDrawable gradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-211356, -1937101});
        GradientDrawable gradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-803493, -3845098});
        GradientDrawable gradientDrawable4 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{2147272292, 2145546547});
        gradientDrawable.setCornerRadius(5.0f);
        gradientDrawable.setStroke(1, -3637191);
        gradientDrawable2.setCornerRadius(5.0f);
        gradientDrawable2.setStroke(1, -1858224);
        gradientDrawable3.setCornerRadius(5.0f);
        gradientDrawable3.setStroke(1, -3042498);
        gradientDrawable4.setCornerRadius(5.0f);
        gradientDrawable4.setStroke(1, 2145625424);
        stateListDrawable.addState(new int[]{16842910, -16842919, -16842908}, gradientDrawable);
        stateListDrawable.addState(new int[]{16842910, -16842919, 16842908}, gradientDrawable2);
        stateListDrawable.addState(new int[]{16842910, 16842919, -16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{16842910, 16842919, 16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{-16842910, -16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, -16842919, 16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, 16842908}, gradientDrawable4);
        return stateListDrawable;
    }

    public static ImageView a(Context context, String str) {
        ImageView imageView = new ImageView(context);
        Drawable a = a(((Integer) com.paypal.android.a.a.h.a.get(str)).intValue(), ((Integer) com.paypal.android.a.a.h.b.get(str)).intValue());
        imageView.setLayoutParams(new LinearLayout.LayoutParams((int) (((double) a.getIntrinsicWidth()) * Math.pow((double) o.a().A(), 2.0d)), (int) (((double) a.getIntrinsicHeight()) * Math.pow((double) o.a().A(), 2.0d))));
        imageView.setImageDrawable(a);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        return imageView;
    }

    public static StateListDrawable b() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-197380, -3355444});
        GradientDrawable gradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3487030, -6645094});
        GradientDrawable gradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-7302507, -10395036});
        GradientDrawable gradientDrawable4 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{2143996618, 2140838554});
        gradientDrawable.setCornerRadius(5.0f);
        gradientDrawable.setStroke(1, -10066330);
        gradientDrawable2.setCornerRadius(5.0f);
        gradientDrawable2.setStroke(1, -12303292);
        gradientDrawable3.setCornerRadius(5.0f);
        gradientDrawable3.setStroke(1, -13421773);
        gradientDrawable4.setCornerRadius(5.0f);
        gradientDrawable4.setStroke(1, 2135180356);
        stateListDrawable.addState(new int[]{16842910, -16842919, -16842908}, gradientDrawable);
        stateListDrawable.addState(new int[]{16842910, -16842919, 16842908}, gradientDrawable2);
        stateListDrawable.addState(new int[]{16842910, 16842919, -16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{16842910, 16842919, 16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{-16842910, -16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, -16842919, 16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, 16842908}, gradientDrawable4);
        return stateListDrawable;
    }
}
