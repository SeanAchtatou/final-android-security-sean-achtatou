package com.paypal.android.a;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.MEP.f;
import com.paypal.android.MEP.i;
import com.paypal.android.MEP.o;

public final class e {
    public static TextView a(j jVar, Context context) {
        Rect rect = new Rect(5, 2, 5, 3);
        Rect rect2 = new Rect(0, 0, 0, 0);
        TextView textView = new TextView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2, 0.5f);
        layoutParams.setMargins(rect2.left, rect2.top, rect2.right, rect2.bottom);
        textView.setLayoutParams(layoutParams);
        textView.setBackgroundColor(0);
        textView.setTextColor(-16777216);
        textView.setGravity(3);
        textView.setPadding(rect.left, rect.top, rect.right, rect.bottom);
        if (jVar == j.HELVETICA_16_BOLD || jVar == j.HELVETICA_14_BOLD || jVar == j.HELVETICA_12_BOLD || jVar == j.HELVETICA_10_BOLD) {
            textView.setTypeface(Typeface.create("Helvetica", 1));
        } else if (jVar != j.HELVETICA_16_NORMAL && jVar != j.HELVETICA_14_NORMAL && jVar != j.HELVETICA_12_NORMAL && jVar != j.HELVETICA_10_NORMAL) {
            return null;
        } else {
            textView.setTypeface(Typeface.create("Helvetica", 0));
        }
        if (jVar == j.HELVETICA_16_BOLD || jVar == j.HELVETICA_16_NORMAL) {
            textView.setTextSize(16.0f);
        } else if (jVar == j.HELVETICA_14_BOLD || jVar == j.HELVETICA_14_NORMAL) {
            textView.setTextSize(14.0f);
        } else if (jVar == j.HELVETICA_12_BOLD || jVar == j.HELVETICA_12_NORMAL) {
            textView.setTextSize(12.0f);
        } else if (jVar != j.HELVETICA_10_BOLD && jVar != j.HELVETICA_10_NORMAL) {
            return null;
        } else {
            textView.setTextSize(10.0f);
        }
        return textView;
    }

    public static TextView b(j jVar, Context context) {
        TextView a = a(jVar, context);
        o a2 = o.a();
        i c = a2.c();
        if (a2.z() == 3) {
            a.setText(a2.d().a());
        } else if (a2.z() == 0) {
            if (a2.B()) {
                a.setText(d.a("ANDROID_send_to") + ": " + ((f) c.b().get(0)).a());
            } else {
                String k = o.a().c().k();
                if (k == null || k.length() <= 0) {
                    String h = ((f) c.b().get(0)).h();
                    if (h == null || h.length() <= 0) {
                        a.setText(d.a("ANDROID_send_to") + ": " + ((f) c.b().get(0)).a());
                    } else {
                        a.setText(h);
                    }
                } else {
                    a.setText(k);
                }
            }
        } else if (a2.z() == 2) {
            String k2 = o.a().c().k();
            if (k2 == null || k2.length() <= 0) {
                String h2 = c.g().h();
                if (h2 == null || h2.length() <= 0) {
                    a.setText(d.a("ANDROID_send_to") + ": " + c.g().a());
                } else {
                    a.setText(h2);
                }
            } else {
                a.setText(k2);
            }
        } else {
            String k3 = o.a().c().k();
            if (k3 == null || k3.length() <= 0) {
                int i = 0;
                while (true) {
                    if (i < c.b().size()) {
                        String h3 = ((f) c.b().get(i)).h();
                        if (h3 != null && h3.length() > 0) {
                            a.setText(h3);
                            break;
                        }
                        i++;
                    } else {
                        break;
                    }
                }
            } else {
                a.setText(k3);
            }
            if (a.getText() == null || a.getText().length() <= 0) {
                int i2 = 0;
                while (true) {
                    if (i2 < c.b().size()) {
                        String a3 = ((f) c.b().get(i2)).a();
                        if (a3 != null && a3.length() > 0) {
                            a.setText(d.a("ANDROID_send_to") + ": " + a3);
                            break;
                        }
                        i2++;
                    } else {
                        break;
                    }
                }
            }
        }
        a.setGravity(17);
        a.setPadding(5, 2, 5, 3);
        return a;
    }
}
