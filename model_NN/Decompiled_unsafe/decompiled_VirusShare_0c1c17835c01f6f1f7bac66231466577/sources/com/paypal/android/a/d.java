package com.paypal.android.a;

import com.paypal.android.MEP.o;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Hashtable;
import java.util.Locale;

public final class d {
    private static String[] a = {"de_at", "de_ch", "de_de", "en_at", "en_au", "en_be", "en_ca", "en_ch", "en_de", "en_es", "en_fr", "en_gb", "en_hk", "en_in", "en_jp", "en_mx", "en_nl", "en_pl", "en_sg", "en_tw", "en_us", "es_ar", "es_es", "es_mx", "fr_be", "fr_ca", "fr_ch", "fr_fr", "it_it", "ja_jp", "nl_be", "nl_nl", "pl_pl", "pt_br", "zh_hk", "zh_tw"};
    private static Hashtable b;
    private static NumberFormat c;

    public static String a(String str) {
        String str2 = (String) b.get(str);
        return str2 == null ? str : str2;
    }

    public static String a(String str, int i) {
        String[] split = str.substring(0, str.indexOf("T")).split("-");
        if (split.length < 3) {
            return "Error with timestamp.";
        }
        int parseInt = Integer.parseInt(split[0]);
        int parseInt2 = Integer.parseInt(split[1]);
        int parseInt3 = Integer.parseInt(split[2]);
        String h = o.a().h();
        switch ((h.contains("AU") || h.contains("DE") || h.contains("BR") || h.contains("FR") || h.contains("IN") || h.contains("NL") || h.contains("PL") || h.contains("ES") || h.contains("GB") || h.contains("AT") || h.contains("CH") || h.contains("MX") || h.contains("SG") || h.contains("IT") || h.contains("AR")) ? 0 : h.contains("US") ? 2 : (h.contains("BE") || h.contains("CA") || h.contains("JP") || h.contains("HK") || h.contains("TW")) ? 5 : i) {
            case 0:
                return parseInt3 + "/" + parseInt2 + "/" + parseInt;
            case 5:
                return parseInt + "/" + parseInt2 + "/" + parseInt3;
            default:
                return parseInt2 + "/" + parseInt3 + "/" + parseInt;
        }
    }

    public static String a(BigDecimal bigDecimal, String str) {
        c.setCurrency(Currency.getInstance(str));
        return c.format(bigDecimal) + " (" + str + ")";
    }

    public static boolean b(String str) {
        String lowerCase = str.toLowerCase();
        if (lowerCase.indexOf(95) == -1 || lowerCase.length() != 5 || !g(lowerCase.substring(0, 1)) || !g(lowerCase.substring(3, 5))) {
            return false;
        }
        for (String equals : a) {
            if (equals.equals(lowerCase)) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static void c(String str) {
        int i;
        int i2;
        String lowerCase = (str == null ? "en_US" : str).toLowerCase();
        if (!b(lowerCase)) {
            lowerCase = "en_US";
        }
        if (b != null) {
            b = null;
        }
        c = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.getDefault());
        if (lowerCase.equals("de_at")) {
            i2 = 363737;
            i = 20099;
        } else if (lowerCase.equals("de_ch")) {
            i = 20177;
            i2 = 18146;
        } else if (lowerCase.equals("de_de")) {
            i2 = 323192;
            i = 20099;
        } else if (lowerCase.equals("en_at")) {
            i2 = 133660;
            i = 17945;
        } else if (lowerCase.equals("en_au")) {
            i2 = 491995;
            i = 18403;
        } else if (lowerCase.equals("en_be")) {
            i2 = 665352;
            i = 18115;
        } else if (lowerCase.equals("en_ca")) {
            i2 = 611171;
            i = 18051;
        } else if (lowerCase.equals("en_ch")) {
            i2 = 473960;
            i = 18035;
        } else if (lowerCase.equals("en_de")) {
            i2 = 77331;
            i = 17945;
        } else if (lowerCase.equals("en_es")) {
            i2 = 629222;
            i = 18055;
        } else if (lowerCase.equals("en_fr")) {
            i2 = 266712;
            i = 18059;
        } else if (lowerCase.equals("en_gb")) {
            i2 = 437661;
            i = 18150;
        } else if (lowerCase.equals("en_hk")) {
            i2 = 455811;
            i = 18149;
        } else if (lowerCase.equals("en_in")) {
            i2 = 284771;
            i = 18094;
        } else if (lowerCase.equals("en_jp")) {
            i2 = 383836;
            i = 17924;
        } else if (lowerCase.equals("en_mx")) {
            i2 = 115616;
            i = 18044;
        } else if (lowerCase.equals("en_nl")) {
            i2 = 419549;
            i = 18112;
        } else if (lowerCase.equals("en_pl")) {
            i2 = 510398;
            i = 18421;
        } else if (lowerCase.equals("en_sg")) {
            i2 = 151605;
            i = 18109;
        } else if (lowerCase.equals("en_tw")) {
            i = 18146;
            i2 = 0;
        } else if (lowerCase.equals("en_us")) {
            i2 = 647277;
            i = 18075;
        } else if (lowerCase.equals("es_ar")) {
            i2 = 208662;
            i = 19443;
        } else if (lowerCase.equals("es_es")) {
            i2 = 57998;
            i = 19333;
        } else if (lowerCase.equals("es_mx")) {
            i2 = 228105;
            i = 19412;
        } else if (lowerCase.equals("fr_be")) {
            i2 = 302865;
            i = 20327;
        } else if (lowerCase.equals("fr_ca")) {
            i2 = 187048;
            i = 21614;
        } else if (lowerCase.equals("fr_ch")) {
            i2 = 95276;
            i = 20340;
        } else if (lowerCase.equals("fr_fr")) {
            i2 = 590900;
            i = 20271;
        } else if (lowerCase.equals("it_it")) {
            i2 = 38323;
            i = 19675;
        } else if (lowerCase.equals("ja_jp")) {
            i2 = 567734;
            i = 23166;
        } else if (lowerCase.equals("nl_be")) {
            i2 = 528819;
            i = 19209;
        } else if (lowerCase.equals("nl_nl")) {
            i2 = 247517;
            i = 19195;
        } else if (lowerCase.equals("pl_pl")) {
            i2 = 343291;
            i = 20446;
        } else if (lowerCase.equals("pt_br")) {
            i2 = 548028;
            i = 19706;
        } else if (lowerCase.equals("zh_hk")) {
            i2 = 169714;
            i = 17334;
        } else if (lowerCase.equals("zh_tw")) {
            i2 = 401760;
            i = 17789;
        } else {
            i = 0;
            i2 = 0;
        }
        String[] split = new String(b.a(i2, i, b.a("com/paypal/android/utils/data/locale.bin"))).split("\n");
        b = new Hashtable();
        for (String str2 : split) {
            if (str2.contains("\" = \"")) {
                String[] split2 = str2.split("\" = \"");
                b.put(split2[0].replace('\"', ' ').trim(), split2[1].replace("\";", " ").trim());
            }
        }
    }

    public static boolean d(String str) {
        int indexOf = str.indexOf(64);
        int lastIndexOf = str.lastIndexOf(46);
        return (indexOf == -1 || lastIndexOf == -1 || lastIndexOf - 1 <= indexOf || lastIndexOf == str.length() - 1) ? false : true;
    }

    public static boolean e(String str) {
        if (str.length() > 0) {
            StringBuffer stringBuffer = new StringBuffer(str.length());
            for (int i = 0; i < str.length(); i++) {
                char charAt = str.charAt(i);
                if ((charAt >= '0' && charAt <= '9') || charAt == '+') {
                    stringBuffer.append(charAt);
                }
            }
            if (str.equals(stringBuffer.toString())) {
                return true;
            }
        }
        return false;
    }

    public static boolean f(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt < '0' || charAt > '9') {
                return false;
            }
        }
        return true;
    }

    private static boolean g(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && ((charAt < 'A' || charAt > 'Z') && (charAt < '0' || charAt > '9'))) {
                return false;
            }
        }
        return true;
    }
}
