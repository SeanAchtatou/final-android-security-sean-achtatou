package com.paypal.android.a;

import java.math.BigDecimal;

public final class a {
    public static String a(String str, String str2) {
        BigDecimal bigDecimal;
        try {
            bigDecimal = new BigDecimal(str);
        } catch (NumberFormatException e) {
            bigDecimal = new BigDecimal("0.0");
        }
        return d.a(bigDecimal, str2);
    }

    public static String a(BigDecimal bigDecimal, String str) {
        return d.a(bigDecimal, str);
    }
}
