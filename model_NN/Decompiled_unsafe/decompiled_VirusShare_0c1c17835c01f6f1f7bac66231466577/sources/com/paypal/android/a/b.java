package com.paypal.android.a;

public final class b {
    private static byte[] a;
    private static b b = null;

    public static void a() {
        if (b == null) {
            b = new b();
        }
        a = b.b("com/paypal/android/utils/data/data.bin");
    }

    public static byte[] a(int i, int i2) {
        return a(i, i2, a);
    }

    public static byte[] a(int i, int i2, byte[] bArr) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }

    public static byte[] a(String str) {
        return b.b(str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0031 A[SYNTHETIC, Splitter:B:17:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0036 A[SYNTHETIC, Splitter:B:20:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0057 A[SYNTHETIC, Splitter:B:39:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x005c A[SYNTHETIC, Splitter:B:42:0x005c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] b(java.lang.String r8) {
        /*
            r7 = this;
            r5 = 0
            java.lang.Class r0 = r7.getClass()     // Catch:{ IOException -> 0x0076, all -> 0x0052 }
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ IOException -> 0x0076, all -> 0x0052 }
            if (r0 == 0) goto L_0x0049
            java.io.InputStream r0 = r0.getResourceAsStream(r8)     // Catch:{ IOException -> 0x0076, all -> 0x0052 }
            if (r0 == 0) goto L_0x004a
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x007a, all -> 0x006c }
            r1.<init>()     // Catch:{ IOException -> 0x007a, all -> 0x006c }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x002b, all -> 0x0071 }
        L_0x001a:
            r3 = 0
            int r4 = r2.length     // Catch:{ IOException -> 0x002b, all -> 0x0071 }
            int r3 = r0.read(r2, r3, r4)     // Catch:{ IOException -> 0x002b, all -> 0x0071 }
            r4 = -1
            if (r3 == r4) goto L_0x003b
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ IOException -> 0x002b, all -> 0x0071 }
            java.lang.Thread.yield()     // Catch:{ IOException -> 0x002b, all -> 0x0071 }
            goto L_0x001a
        L_0x002b:
            r2 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x002f:
            if (r0 == 0) goto L_0x0034
            r0.close()     // Catch:{ Throwable -> 0x0064 }
        L_0x0034:
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ Throwable -> 0x0066 }
        L_0x0039:
            r0 = r5
        L_0x003a:
            return r0
        L_0x003b:
            byte[] r2 = r1.toByteArray()     // Catch:{ IOException -> 0x002b, all -> 0x0071 }
            r1.close()     // Catch:{ Throwable -> 0x0060 }
        L_0x0042:
            if (r0 == 0) goto L_0x0047
            r0.close()     // Catch:{ Throwable -> 0x0062 }
        L_0x0047:
            r0 = r2
            goto L_0x003a
        L_0x0049:
            r0 = r5
        L_0x004a:
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ Throwable -> 0x0050 }
            goto L_0x0039
        L_0x0050:
            r0 = move-exception
            goto L_0x0039
        L_0x0052:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x0055:
            if (r1 == 0) goto L_0x005a
            r1.close()     // Catch:{ Throwable -> 0x0068 }
        L_0x005a:
            if (r2 == 0) goto L_0x005f
            r2.close()     // Catch:{ Throwable -> 0x006a }
        L_0x005f:
            throw r0
        L_0x0060:
            r1 = move-exception
            goto L_0x0042
        L_0x0062:
            r0 = move-exception
            goto L_0x0047
        L_0x0064:
            r0 = move-exception
            goto L_0x0034
        L_0x0066:
            r0 = move-exception
            goto L_0x0039
        L_0x0068:
            r1 = move-exception
            goto L_0x005a
        L_0x006a:
            r1 = move-exception
            goto L_0x005f
        L_0x006c:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
            goto L_0x0055
        L_0x0071:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x0055
        L_0x0076:
            r0 = move-exception
            r0 = r5
            r1 = r5
            goto L_0x002f
        L_0x007a:
            r1 = move-exception
            r1 = r0
            r0 = r5
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.a.b.b(java.lang.String):byte[]");
    }
}
