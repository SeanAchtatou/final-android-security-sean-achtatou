package com.paypal.android.a;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.a.m;
import com.paypal.android.MEP.b;
import com.paypal.android.MEP.e;
import com.paypal.android.MEP.i;
import com.paypal.android.MEP.o;
import com.paypal.android.MEP.p;
import com.paypal.android.MEP.r;
import com.paypal.android.a.a.a;
import com.paypal.android.a.a.c;
import com.paypal.android.a.a.d;
import com.paypal.android.a.a.k;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public final class f implements b {
    private HttpPost a;
    private HttpGet b;
    private DefaultHttpClient c = null;
    /* access modifiers changed from: private */
    public int d = -1;
    private int e = -1;
    /* access modifiers changed from: private */
    public int f = -1;
    /* access modifiers changed from: private */
    public Hashtable g;
    private Thread h = new g(this);

    private String a(ArrayList arrayList) {
        String str;
        String str2 = "";
        try {
            if (this.c == null) {
                this.c = new DefaultHttpClient();
            }
            this.g.put("SessionToken", "");
            StringBuilder append = new StringBuilder().append(h());
            switch (o.a().k()) {
                case 2:
                    str = "";
                    break;
                case 3:
                    str = "MEPAuthenticate?";
                    break;
                default:
                    str = "MEPAuthenticateUser?";
                    break;
            }
            this.a = new HttpPost(append.append(str).toString());
            ArrayList arrayList2 = new ArrayList();
            arrayList2.addAll(arrayList);
            this.a.setEntity(new UrlEncodedFormEntity(arrayList2, "UTF-8"));
            k(arrayList2.toString());
            HttpEntity entity = this.c.execute(this.a).getEntity();
            InputStream content = entity.getContent();
            StringBuffer stringBuffer = new StringBuffer();
            for (int read = content.read(); read != -1; read = content.read()) {
                stringBuffer.append((char) read);
            }
            str2 = stringBuffer.toString();
            k(str2);
            if (entity != null) {
                entity.consumeContent();
            }
            content.close();
            this.a = null;
            this.c = null;
        } catch (Exception e2) {
        }
        return str2;
    }

    /* access modifiers changed from: private */
    public boolean a(String str, String str2) {
        if (!q()) {
            return false;
        }
        try {
            ArrayList arrayList = new ArrayList();
            if (str.indexOf("@") > 0) {
                arrayList.add(new BasicNameValuePair("Email", str));
                arrayList.add(new BasicNameValuePair("Password", str2));
            } else {
                PayPalActivity.a().getSystemService("phone");
                String country = Locale.getDefault().getCountry();
                arrayList.add(new BasicNameValuePair("Phone", str.indexOf("+") != -1 ? str.substring(1) : !str.startsWith(c(country)) ? c(country) + str : str));
                arrayList.add(new BasicNameValuePair("PIN", str2));
            }
            arrayList.add(new BasicNameValuePair("APIVersion", "3.0"));
            arrayList.add(new BasicNameValuePair("PayPalApplicationID", URLEncoder.encode(o.a().f(), "utf-8")));
            String str3 = "";
            try {
                FileInputStream openFileInput = o.a().e().openFileInput("DeviceReferenceToken");
                byte[] bArr = new byte[openFileInput.available()];
                openFileInput.read(bArr);
                openFileInput.close();
                String str4 = new String(bArr);
                if (str4.length() > 0) {
                    str3 = str4;
                }
            } catch (Exception e2) {
            }
            arrayList.add(new BasicNameValuePair("DeviceReferenceToken", str3));
            arrayList.add(new BasicNameValuePair("AuthorizeDevice", "" + o.a().v()));
            return h(a(arrayList));
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    private static byte[] a(InputStream inputStream) {
        byte[] byteArray;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        byte[] bArr = null;
        while (true) {
            try {
                int read = inputStream.read();
                if (read == -1) {
                    break;
                }
                if (bArr == null) {
                    bArr = new byte[1024];
                }
                if (i == 1024) {
                    byteArrayOutputStream.write(bArr);
                    try {
                        bArr = new byte[1024];
                        i = 0;
                    } catch (IOException e2) {
                        return null;
                    }
                }
                bArr[i] = (byte) read;
                i++;
            } catch (IOException e3) {
                return bArr;
            }
        }
        if (i != 0) {
            for (int i2 = 0; i2 < i; i2++) {
                byteArrayOutputStream.write(bArr[i2]);
            }
        }
        byte[] bArr2 = new byte[byteArrayOutputStream.size()];
        try {
            byteArray = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
            return byteArray;
        } catch (IOException e4) {
            return byteArray;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0146 A[SYNTHETIC, Splitter:B:39:0x0146] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x014b A[SYNTHETIC, Splitter:B:42:0x014b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String b(java.lang.String r9, java.lang.String r10) {
        /*
            r8 = this;
            r7 = 0
            r6 = 0
            j(r9)
            org.apache.http.impl.client.DefaultHttpClient r0 = r8.c     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            if (r0 != 0) goto L_0x0010
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r0.<init>()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r8.c = r0     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
        L_0x0010:
            org.apache.http.client.methods.HttpPost r0 = new org.apache.http.client.methods.HttpPost     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r0.<init>(r10)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r8.a = r0     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r0 = h()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            boolean r0 = r10.contains(r0)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            if (r0 == 0) goto L_0x006e
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r0.<init>()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r2 = "xml"
            r1.<init>(r2, r9)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r0.add(r1)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r1 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.entity.UrlEncodedFormEntity r2 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r3 = "UTF-8"
            r2.<init>(r0, r3)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r1.setEntity(r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
        L_0x003c:
            org.apache.http.impl.client.DefaultHttpClient r0 = r8.c     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r1 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.HttpResponse r0 = r0.execute(r1)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.HttpEntity r1 = r0.getEntity()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.Header[] r0 = r0.getAllHeaders()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            if (r0 == 0) goto L_0x01f7
            r2 = r7
            r3 = r7
        L_0x0050:
            int r4 = r0.length     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            if (r2 >= r4) goto L_0x015a
            r4 = r0[r2]     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r4 = r4.getName()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r5 = "Content-Length"
            int r4 = r4.compareTo(r5)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            if (r4 != 0) goto L_0x006b
            r3 = r0[r2]     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r3 = r3.getValue()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
        L_0x006b:
            int r2 = r2 + 1
            goto L_0x0050
        L_0x006e:
            java.lang.String r0 = i()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            boolean r0 = r10.contains(r0)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            if (r0 == 0) goto L_0x0153
            com.paypal.android.MEP.o r0 = com.paypal.android.MEP.o.a()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            int r0 = r0.k()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r1 = 3
            if (r0 != r1) goto L_0x0124
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "CLIENT-AUTH"
            java.lang.String r2 = "No cert"
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
        L_0x008c:
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "X-PAYPAL-MESSAGE-PROTOCOL"
            java.lang.String r2 = "SOAP11"
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "X-PAYPAL-DEVICE-IPADDRESS"
            java.lang.String r2 = u()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "X-PAYPAL-APPLICATION-ID"
            com.paypal.android.MEP.o r2 = com.paypal.android.MEP.PayPalActivity.b     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r2 = r2.f()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r1 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r2 = "X-PAYPAL-DEVICE-AUTH-TOKEN"
            java.util.Hashtable r0 = r8.g     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r3 = "SessionToken"
            java.lang.Object r0 = r0.get(r3)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r1.setHeader(r2, r0)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "X-PAYPAL-REQUEST-SOURCE"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r2.<init>()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r3 = "MPL_Android_"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r3 = com.paypal.android.MEP.o.x()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "X-PAYPAL-REQUEST-DATA-FORMAT"
            java.lang.String r2 = "XML"
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "X-PAYPAL-RESPONSE-DATA-FORMAT"
            java.lang.String r2 = "XML"
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "x-paypal-service-version"
            java.lang.String r2 = "1.0.0"
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "x-paypal-element-ordering-preserve"
            java.lang.String r2 = "false"
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.entity.StringEntity r1 = new org.apache.http.entity.StringEntity     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r1.<init>(r9)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            r0.setEntity(r1)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            goto L_0x003c
        L_0x010c:
            r0 = move-exception
            r1 = r6
            r2 = r6
        L_0x010f:
            r0.printStackTrace()     // Catch:{ all -> 0x01f0 }
            if (r1 == 0) goto L_0x0117
            r1.close()     // Catch:{ Exception -> 0x01dc }
        L_0x0117:
            if (r2 == 0) goto L_0x011c
            r2.close()     // Catch:{ Exception -> 0x01df }
        L_0x011c:
            r8.a = r6
            r8.c = r6
            r8.e = r7
            r0 = r6
        L_0x0123:
            return r0
        L_0x0124:
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "X-PAYPAL-SECURITY-PASSWORD"
            java.lang.String r2 = "MPL"
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "X-PAYPAL-SECURITY-USERID"
            java.lang.String r2 = "MPL"
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            org.apache.http.client.methods.HttpPost r0 = r8.a     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.lang.String r1 = "X-PAYPAL-SECURITY-SIGNATURE"
            java.lang.String r2 = "MPL"
            r0.setHeader(r1, r2)     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            goto L_0x008c
        L_0x0141:
            r0 = move-exception
            r1 = r6
            r2 = r6
        L_0x0144:
            if (r1 == 0) goto L_0x0149
            r1.close()     // Catch:{ Exception -> 0x01e2 }
        L_0x0149:
            if (r2 == 0) goto L_0x014e
            r2.close()     // Catch:{ Exception -> 0x01e5 }
        L_0x014e:
            r8.a = r6
            r8.c = r6
            throw r0
        L_0x0153:
            java.lang.String r0 = "ErrorId=-1"
            r8.a = r6
            r8.c = r6
            goto L_0x0123
        L_0x015a:
            r0 = r3
        L_0x015b:
            java.io.InputStream r2 = r1.getContent()     // Catch:{ Throwable -> 0x010c, all -> 0x0141 }
            java.io.DataInputStream r3 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x01f3, all -> 0x01e8 }
            r3.<init>(r2)     // Catch:{ Throwable -> 0x01f3, all -> 0x01e8 }
            int r4 = r2.available()     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            if (r4 <= r0) goto L_0x016e
            int r0 = r2.available()     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
        L_0x016e:
            int r4 = r3.available()     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            if (r4 <= r0) goto L_0x0178
            int r0 = r3.available()     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
        L_0x0178:
            if (r0 == 0) goto L_0x01c8
            byte[] r0 = new byte[r0]     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            r3.readFully(r0)     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            java.lang.String r4 = new java.lang.String     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r0, r5)     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            r0 = r4
        L_0x0187:
            if (r1 == 0) goto L_0x018c
            r1.consumeContent()     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
        L_0x018c:
            r3.close()     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            r2.close()     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            r1 = 0
            r8.a = r1     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            r1 = 0
            r8.c = r1     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            j(r0)     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            java.lang.String r1 = "% "
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            r4.<init>()     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            java.lang.String r5 = "%"
            java.lang.String r5 = java.net.URLEncoder.encode(r5)     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            java.lang.String r5 = " "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            java.lang.String r0 = r0.replace(r1, r4)     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            r3.close()     // Catch:{ Exception -> 0x01d8 }
        L_0x01bd:
            if (r2 == 0) goto L_0x01c2
            r2.close()     // Catch:{ Exception -> 0x01da }
        L_0x01c2:
            r8.a = r6
            r8.c = r6
            goto L_0x0123
        L_0x01c8:
            java.lang.String r0 = new java.lang.String     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            byte[] r4 = a(r3)     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            java.lang.String r5 = "UTF-8"
            r0.<init>(r4, r5)     // Catch:{ Throwable -> 0x01d4, all -> 0x01ec }
            goto L_0x0187
        L_0x01d4:
            r0 = move-exception
            r1 = r3
            goto L_0x010f
        L_0x01d8:
            r1 = move-exception
            goto L_0x01bd
        L_0x01da:
            r1 = move-exception
            goto L_0x01c2
        L_0x01dc:
            r0 = move-exception
            goto L_0x0117
        L_0x01df:
            r0 = move-exception
            goto L_0x011c
        L_0x01e2:
            r1 = move-exception
            goto L_0x0149
        L_0x01e5:
            r1 = move-exception
            goto L_0x014e
        L_0x01e8:
            r0 = move-exception
            r1 = r6
            goto L_0x0144
        L_0x01ec:
            r0 = move-exception
            r1 = r3
            goto L_0x0144
        L_0x01f0:
            r0 = move-exception
            goto L_0x0144
        L_0x01f3:
            r0 = move-exception
            r1 = r6
            goto L_0x010f
        L_0x01f7:
            r0 = r7
            goto L_0x015b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.a.f.b(java.lang.String, java.lang.String):java.lang.String");
    }

    public static final boolean b() {
        try {
            return ((String) PayPalActivity.a.g.get("PayButtonEnable")).compareToIgnoreCase("true") == 0;
        } catch (Exception e2) {
        }
    }

    public static String c(String str) {
        if (str == null) {
            return "1";
        }
        String upperCase = str.toUpperCase();
        return (upperCase.compareTo("US") == 0 || upperCase.compareTo("CA") == 0) ? "1" : upperCase.compareTo("GB") == 0 ? "44" : upperCase.compareTo("AU") == 0 ? "61" : upperCase.compareTo("FR") == 0 ? "33" : upperCase.compareTo("ES") == 0 ? "34" : upperCase.compareTo("IT") == 0 ? "39" : "1";
    }

    public static void c() {
        if (PayPalActivity.a == null) {
            PayPalActivity.a = new f();
        }
        if (PayPalActivity.a.g == null) {
            PayPalActivity.a.g = new Hashtable();
        }
        if (o.a().k() == 2) {
            PayPalActivity.a.g.put("PayButtonEnable", "true");
            o.a().a(true);
        }
        if (!PayPalActivity.a.h.isAlive()) {
            PayPalActivity.a.h.start();
        }
    }

    private static boolean e(String str) {
        String[] strArr = {"560027", "580022", "580023"};
        for (String equals : strArr) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    private static boolean f(String str) {
        String[] strArr = {"10818", "10897", "10898", "10899", "520003"};
        for (String equals : strArr) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    static /* synthetic */ String g(f fVar) {
        String str;
        Hashtable hashtable = fVar.g;
        String a2 = c.a((String) hashtable.get("SessionToken"), "utf-8");
        String a3 = c.a((String) hashtable.get("RecipientType"), "utf-8");
        String a4 = c.a((String) hashtable.get("PaymentCurrencyID"), "utf-8");
        String a5 = c.a((String) hashtable.get("PaymentAmount"), "utf-8");
        String a6 = c.a((String) hashtable.get("Tax"), "utf-8");
        String a7 = c.a((String) hashtable.get("Shipping"), "utf-8");
        String a8 = c.a((String) hashtable.get("ItemName"), "utf-8");
        int indexOf = a5.indexOf(".");
        if (a5.length() > indexOf + 3) {
            a5 = a5.substring(0, indexOf + 3);
        }
        int indexOf2 = a6.indexOf(".");
        if (a6.length() > indexOf2 + 3) {
            a6 = a6.substring(0, indexOf2 + 3);
        }
        int indexOf3 = a7.indexOf(".");
        if (a7.length() > indexOf3 + 3) {
            a7 = a7.substring(0, indexOf3 + 3);
        }
        String str2 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"\nxmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"\nxmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\nxmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\nxmlns:xs=\"http://www.w3.org/2001/XMLSchema\"\nxmlns:cc=\"urn:ebay:apis:CoreComponentTypes\"\nxmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\"\nxmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\"\nxmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\"\nxmlns:wsse=\"http://schemas.xmlsoap.org/ws/2002/12/secext\"\nxmlns:ebl=\"urn:ebay:apis:eBLBaseComponents\"\nxmlns:ns=\"urn:ebay:api:PayPalAPI\">\n<SOAP-ENV:Header>\n<Security xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xsi:type=\"wsse:SecurityType\"></Security>\n<RequesterCredentials xmlns=\"urn:ebay:api:PayPalAPI\" xsi:type=\"ebl:CustomSecurityHeaderType\">\n<Credentials xmlns=\"urn:ebay:apis:eBLBaseComponents\" xsi:type=\"ebl:UserIdPasswordType\">\n<Username xsi:type=\"xs:string\">mfoundry_client</Username>\n<Password xsi:type=\"xs:string\">11111111</Password>\n</Credentials>\n</RequesterCredentials>\n</SOAP-ENV:Header>\n<SOAP-ENV:Body id=\"_0\">\n<MEPQuickPaymentReq xmlns=\"urn:ebay:api:PayPalAPI\">\n<Request>\n<Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">1</Version>\n<RequestorID xsi:type=\"ns:IVRRequestorID\">eding-1</RequestorID>\n" + "<SessionToken xsi:type=\"ns:MEPSessionToken\">" + a2 + "</SessionToken>\n";
        if (a3 != null) {
            str2 = str2 + "<RecipientType xsi:type=\"ns:IVRRecipientTypeType\">" + a3 + "</RecipientType>\n";
        }
        if (a3.equals("Email")) {
            str = str2 + "<RecipientEmail xsi:type=\"xs:string\">" + c.a((String) hashtable.get("RecipientAddress"), "utf-8") + "</RecipientEmail>\n";
        } else {
            String str3 = str2 + "<RecipientPhone xsi:type=\"ns:IVRPhoneNumber\">\n";
            str = (((c.e((String) hashtable.get("RecipientAddress")) == null || c.e((String) hashtable.get("RecipientAddress")).length() <= 0) ? str3 + "<CountryCode xsi:type=\"xs:string\">" + c.a((String) hashtable.get("RecipientCountryCode"), "utf-8") + "</CountryCode>\n" : str3 + "<CountryCode xsi:type=\"xs:string\">" + c.a(c.e((String) hashtable.get("RecipientAddress")), "utf-8") + "</CountryCode>\n") + "<PhoneNumber xsi:type=\"xs:string\">" + c.a(c.d((String) hashtable.get("RecipientAddress")), "utf-8") + "</PhoneNumber>\n") + "</RecipientPhone>\n";
        }
        String str4 = str + "<PaymentAmount xsi:type=\"ns:IVRAmountType\" currencyID=\"" + a4 + "\">" + a5 + "</PaymentAmount>\n";
        if (!a6.equals("0.00") && !a6.equals("0")) {
            str4 = str4 + "<Tax xsi:type=\"cc:BasicAmountType\" currencyID=\"" + a4 + "\">" + a6 + "</Tax>\n";
        }
        if (!a7.equals("0.00") && !a7.equals("0")) {
            str4 = str4 + "<Shipping xsi:type=\"cc:BasicAmountType\" currencyID=\"" + a4 + "\">" + a7 + "</Shipping>\n";
        }
        String str5 = (a8 == null || a8.length() <= 0) ? str4 : str4 + "<ItemName xsi:type=\"xs:string\">" + a8 + "</ItemName>\n";
        String b2 = fVar.b((((String) hashtable.get("ShareHomeAddress")).equals("1") ? str5 + "<ShareHomeAddress xsi:type=\"xs:int\">1</ShareHomeAddress>\n" : str5) + "</Request>\n</MEPQuickPaymentReq>\n</SOAP-ENV:Body>\n</SOAP-ENV:Envelope>", i());
        if (fVar.i(b2)) {
            return null;
        }
        String c2 = c.c(b2);
        return c2 == null ? "-1" : c2;
    }

    private static boolean g(String str) {
        String[] strArr = {"10001", "10004", "10800", "10801", "10802", "10804", "10805", "10806", "10808", "10809", "10810", "10811", "10812", "10813", "10815", "10819", "10820", "10821", "10822", "10823", "10824", "10825", "10849", "10850", "10858", "10859", "10860", "10861", "10862", "10863", "10864", "10867", "99999", "520002", "520009", "539041", "540031", "550001", "550006", "559044", "560027", "569000", "569042", "569056", "569060", "579007", "579017", "579033", "579040", "579045", "579047", "579048", "580001", "580022", "580023", "580028", "580030", "580031", "580032", "580033", "580034", "589009", "589019", "500000"};
        for (String equals : strArr) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    private static String h() {
        switch (o.a().k()) {
            case 0:
                return "https://mobileclient.sandbox.paypal.com/mepadapter/";
            case 1:
            default:
                return "https://mobileclient.paypal.com/mepadapter/";
            case 2:
                return "";
            case 3:
                return "https://www.stage2mb101.paypal.com:10521/GMAdapter/";
        }
    }

    private boolean h(String str) {
        o a2 = o.a();
        String[] split = str.split("&");
        for (int i = 0; i < split.length; i++) {
            if (i(split[i])) {
                return false;
            }
            if (split[i] != null && split[i].contains("SessionToken=")) {
                this.g.put("SessionToken", split[i].substring(split[i].indexOf("SessionToken=") + "SessionToken=".length()));
            } else if (split[i] != null && split[i].contains("PinEstablished=")) {
                String substring = split[i].substring(split[i].indexOf("PinEstablished=") + "PinEstablished=".length());
                this.g.put("PinEstablished", substring);
                a2.d(Boolean.parseBoolean(substring));
            } else if (split[i] != null && split[i].contains("AuthorizedDevice=")) {
                String substring2 = split[i].substring(split[i].indexOf("AuthorizedDevice=") + "AuthorizedDevice=".length());
                this.g.put("AuthorizedDevice", substring2);
                a2.e(Boolean.parseBoolean(substring2));
            }
        }
        if (a2.v()) {
            if (a2.p() == null || a2.p().length() == 0) {
                v();
            } else {
                a2.b(2);
            }
        }
        return ((String) this.g.get("SessionToken")).length() > 0;
    }

    private static String i() {
        switch (o.a().k()) {
            case 0:
                return "https://svcs.sandbox.paypal.com/AdaptivePayments/";
            case 1:
            default:
                return "https://svcs.paypal.com/AdaptivePayments/";
            case 2:
                return "";
            case 3:
                return "https://www.stage2mb101.paypal.com:10279/AdaptivePayments/";
        }
    }

    static /* synthetic */ String i(f fVar) {
        Hashtable hashtable = fVar.g;
        String a2 = c.a(PayPalActivity.b.h(), "utf-8");
        String a3 = c.a((String) hashtable.get("PayKey"), "utf-8");
        String a4 = c.a((String) hashtable.get("ActionType"), "utf-8");
        String a5 = c.a((String) hashtable.get("FundingPlanId"), "utf-8");
        String str = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:ExecutePaymentRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<payKey>" + a3 + "</payKey>\n" + "<requestEnvelope>\n" + "<errorLanguage>" + a2 + "</errorLanguage>\n" + "</requestEnvelope>\n";
        if (a4 != null) {
            str = str + "<actionType>" + a4 + "</actionType>\n";
        }
        String b2 = fVar.b((a5 != null ? str + "<fundingPlanId>" + a5 + "</fundingPlanId>\n" : str) + "</ns2:ExecutePaymentRequest></soapenv:Body>\n</soapenv:Envelope>", i() + "ExecutePayment/");
        if (fVar.i(b2)) {
            return null;
        }
        String c2 = c.c(b2);
        return c2 == null ? "-1" : c2;
    }

    private boolean i(String str) {
        Intent intent;
        String str2;
        String[] b2;
        this.e = -1;
        if (str == null || str.length() <= 0) {
            this.e = 408;
            return true;
        }
        int a2 = str.startsWith("<?xml") ? c.a(str) : 200;
        if (str == null) {
            this.e = 408;
            return true;
        }
        if (str.contains("ErrorId=")) {
            this.e = Integer.parseInt(str.substring(str.indexOf("ErrorId=") + "ErrorId=".length()));
        } else if (a2 != 200) {
            this.e = a2;
        } else if (str.contains("<SOAP-ENV:Body") && !str.contains("</SOAP-ENV:Body")) {
            this.e = 0;
            return true;
        }
        if (this.e == -1) {
            return false;
        }
        String str3 = "" + this.e;
        String a3 = d.a("ANDROID_" + this.e);
        if (e("" + this.e) && (b2 = c.b(str)) != null) {
            String str4 = a3;
            for (int i = 0; i < b2.length; i++) {
                str4 = str4 + " " + b2[i] + (i + 1 == b2.length ? "." : ",");
            }
            a3 = str4;
        }
        if (g(str3)) {
            Intent putExtra = new Intent(PayPalActivity.i).putExtra("FATAL_ERROR_ID", str3);
            if (str3.equals("569060")) {
                str2 = d.a("ANDROID_no_personal_payments");
            } else if (str3.equals("500000")) {
                str2 = d.a("ANDROID_10001");
            } else if (str3.equals("580001")) {
                try {
                    if (DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes())).getElementsByTagName("parameter").item(0).getChildNodes().item(0).getNodeValue().length() == 3) {
                        a3 = d.a("ANDROID_580001_4");
                    }
                    str2 = a3;
                } catch (Throwable th) {
                    str2 = a3;
                }
            } else {
                if (str3.equals("520009")) {
                    try {
                        if (DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes())).getElementsByTagName("parameter").item(0).getChildNodes().item(0).getNodeValue().length() > 0) {
                            a3 = d.a("ANDROID_520009_2");
                        }
                        str2 = a3;
                    } catch (Throwable th2) {
                    }
                }
                str2 = a3;
            }
            if (this.d == 8 && str3.equals("10001")) {
                return true;
            }
            if (this.d != 11) {
                PayPalActivity.a().a((String) PayPalActivity.a.d("CorrelationId"), (String) PayPalActivity.a.d("PayKey"), str3, str2, false, true);
            }
            intent = putExtra.putExtra("FATAL_ERROR_MESSAGE", str2);
        } else if (!f(str3) || this.d == 11) {
            intent = (!f(str3) || this.d != 11) ? new Intent(PayPalActivity.h) : new Intent(PayPalActivity.i).putExtra("FATAL_ERROR_ID", str3).putExtra("FATAL_ERROR_MESSAGE", d.a("ANDROID_pin_creation_timeout")).putExtra("ERROR_TIMEOUT", a3);
        } else {
            o.a().b(0);
            intent = new Intent(PayPalActivity.f).putExtra("FATAL_ERROR_ID", str3).putExtra("ERROR_TIMEOUT", a3);
        }
        try {
            PayPalActivity.a().sendBroadcast(intent);
        } catch (Exception e2) {
        }
        return true;
    }

    private static String j() {
        switch (o.a().k()) {
            case 2:
                return "";
            case 3:
                return "GMAdapter";
            default:
                return "MEPAdapter";
        }
    }

    private static void j(String str) {
        str.replaceAll("<", "\n<").replaceAll(">", ">\n").replaceAll("\n\n", "\n");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private static void k(String str) {
        str.replace('[', 10).replaceAll(", ", "\n").replaceAll("&", "\n").replace(']', 10);
    }

    /* access modifiers changed from: private */
    public boolean k() {
        String trim;
        Hashtable hashtable = this.g;
        String a2 = c.a((String) hashtable.get("SessionToken"), "utf-8");
        String a3 = c.a((String) hashtable.get("FlowContext"), "utf-8");
        String a4 = c.a((String) hashtable.get("ItemName"), "utf-8");
        String a5 = c.a((String) hashtable.get("PrimaryFSID"), "utf-8");
        String a6 = c.a((String) hashtable.get("BackupFSID"), "utf-8");
        String a7 = c.a((String) hashtable.get("AddressID"), "utf-8");
        String str = (("<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:cc=\"urn:ebay:apis:CoreComponentTypes\" xmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xmlns:ebl=\"urn:ebay:apis:eBLBaseComponents\" xmlns:ns=\"urn:ebay:api:PayPalAPI\"><SOAP-ENV:Header><Security xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xsi:type=\"wsse:SecurityType\"></Security><RequesterCredentials xmlns=\"urn:ebay:api:PayPalAPI\" xsi:type=\"ebl:CustomSecurityHeaderType\"><Credentials xmlns=\"urn:ebay:apis:eBLBaseComponents\" xsi:type=\"ebl:UserIdPasswordType\"><Username xsi:type=\"xs:string\">mfoundry_client</Username><Password xsi:type=\"xs:string\">11111111</Password></Credentials></RequesterCredentials></SOAP-ENV:Header><SOAP-ENV:Body id=\"_0\"><MEPUpdatePaymentReq xmlns=\"urn:ebay:api:PayPalAPI\"><Request><Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">1</Version><RequestorID xsi:type=\"ns:IVRRequestorID\">eding-1</RequestorID><SessionToken xsi:type=\"ns:MEPSessionToken\">" + a2 + "</SessionToken>" + "<FlowContext xsi:type=\"ns:IVRFlowContext\">" + a3 + "</FlowContext>") + "<PrimaryFSID xsi:type=\"ns:IVRFundingSourceID\">" + (a5 == null ? "" : a5) + "</PrimaryFSID>") + "<BackupFSID xsi:type=\"ns:IVRFundingSourceID\">" + (a6 == null ? "" : a6) + "</BackupFSID>";
        if (a7 == null) {
            a7 = "";
        }
        String str2 = str + "<AddressID xsi:type=\"xs:string\">" + a7 + "</AddressID>";
        if (a4 == null) {
            trim = " ";
        } else {
            trim = a4.trim();
            if (trim.length() == 0) {
                trim = " ";
            }
        }
        String b2 = b(((str2 + "<ItemName xsi:type=\"ns:string\">" + trim + "</ItemName>") + "<FeeBearer>" + c.a((String) hashtable.get("FeeBearer"), "utf-8") + "</FeeBearer>\n") + "</Request></MEPUpdatePaymentReq></SOAP-ENV:Body></SOAP-ENV:Envelope>", i());
        if (i(b2)) {
            try {
                c.b(b2, this.g);
            } catch (Throwable th) {
            }
            return false;
        }
        c.b(b2, this.g);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean l() {
        String str;
        String str2;
        String str3;
        Hashtable hashtable = this.g;
        String a2 = c.a((String) hashtable.get("PayKey"), "utf-8");
        String a3 = c.a(PayPalActivity.b.h(), "utf-8");
        d dVar = (d) hashtable.get("DisplayOptions");
        c cVar = (c) hashtable.get("InstitutionCustomer");
        String a4 = c.a((String) hashtable.get("ShippingAddressId"), "utf-8");
        a aVar = (a) hashtable.get("SenderOptions");
        ArrayList arrayList = (ArrayList) hashtable.get("Receivers");
        String str4 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:SetPaymentOptionsRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<payKey>" + a2 + "</payKey>\n" + "<requestEnvelope>\n" + "<errorLanguage>" + a3 + "</errorLanguage>\n" + "</requestEnvelope>\n";
        if (dVar != null) {
        }
        if (cVar != null) {
        }
        String str5 = a4 != null ? str4 + "<shippingAddressId>" + a4 + "</shippingAddressId>\n" : str4;
        if (aVar != null) {
            str5 = str5 + "<senderOptions>\n<sharePhoneNumber>" + false + "</sharePhoneNumber>\n" + "<shareAddress>" + false + "</shareAddress>\n" + "<requireShippingAddressSelection>" + false + "</requireShippingAddressSelection>\n" + "</senderOptions>\n";
        }
        if (arrayList != null) {
            int i = 0;
            while (true) {
                str = str5;
                if (i >= arrayList.size()) {
                    break;
                }
                com.paypal.android.MEP.f fVar = (com.paypal.android.MEP.f) arrayList.get(i);
                String str6 = str + "<receiverOptions>\n";
                if (fVar.f() != null && fVar.f().length() > 0) {
                    str6 = str6 + "<description>" + c.a(fVar.f(), "utf-8") + "</description>\n";
                }
                if (fVar.g() != null && fVar.g().length() > 0) {
                    str6 = str6 + "<customId>" + c.a(fVar.g(), "utf-8") + "</customId>\n";
                }
                if (fVar.c() != null) {
                    String str7 = str6 + "<invoiceData>\n";
                    int i2 = 0;
                    while (true) {
                        str3 = str7;
                        if (i2 >= fVar.c().c().size()) {
                            break;
                        }
                        e eVar = (e) fVar.c().c().get(i2);
                        String str8 = str3 + "<item>\n";
                        if (eVar.a() != null && eVar.a().length() > 0) {
                            str8 = str8 + "<name>" + c.a(eVar.a(), "utf-8") + "</name>\n";
                        }
                        if (eVar.b() != null && eVar.b().length() > 0) {
                            str8 = str8 + "<identifier>" + c.a(eVar.b(), "utf-8") + "</identifier>\n";
                        }
                        if (eVar.c() != null && eVar.c().toString().length() > 0) {
                            str8 = str8 + "<price>" + c.a(eVar.c().toString(), "utf-8") + "</price>\n";
                        }
                        if (eVar.d() != null && eVar.d().toString().length() > 0) {
                            str8 = str8 + "<itemPrice>" + c.a(eVar.d().toString(), "utf-8") + "</itemPrice>\n";
                        }
                        str7 = (eVar.e() > 0 ? str8 + "<itemCount>" + c.a("" + eVar.e(), "utf-8") + "</itemCount>\n" : str8) + "</item>\n";
                        i2++;
                    }
                    String str9 = (fVar.c().a() == null || fVar.c().a().compareTo(BigDecimal.ZERO) <= 0) ? str3 : str3 + "<totalTax>" + fVar.c().a().toString() + "</totalTax>\n";
                    if (fVar.c().b() != null && fVar.c().b().compareTo(BigDecimal.ZERO) > 0) {
                        str9 = str9 + "<totalShipping>" + fVar.c().b().toString() + "</totalShipping>\n";
                    }
                    str6 = str9 + "</invoiceData>\n";
                }
                String str10 = str6 + "<receiver>\n";
                if (fVar.j()) {
                    str2 = str10 + "<email>" + c.a(fVar.a(), "utf-8") + "</email>\n";
                } else if (((com.paypal.android.MEP.f) arrayList.get(i)).k()) {
                    String str11 = str10 + "<phone>\n";
                    String e2 = c.e(fVar.a());
                    if (e2 == null || e2.length() == 0) {
                        e2 = c(Locale.getDefault().getCountry());
                    }
                    str2 = ((str11 + "<countryCode>" + c.a(e2, "utf-8") + "</countryCode>") + "<phoneNumber>" + c.a(c.d(fVar.a()), "utf-8") + "</phoneNumber>") + "</phone>\n";
                } else {
                    str2 = str10;
                }
                str5 = str2 + "</receiver>\n</receiverOptions>\n";
                i++;
            }
            str5 = str;
        }
        String b2 = b(str5 + "</ns2:SetPaymentOptionsRequest></soapenv:Body>\n</soapenv:Envelope>", i() + "SetPaymentOptions/");
        if (i(b2)) {
            try {
                c.b(b2, this.g);
            } catch (Throwable th) {
            }
            return false;
        }
        c.b(b2, this.g);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean m() {
        String a2 = c.a((String) this.g.get("PayKey"), "utf-8");
        String b2 = b("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:GetAvailableShippingAddressesRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<key>" + a2 + "</key>\n" + "<requestEnvelope>\n" + "<errorLanguage>" + c.a(PayPalActivity.b.h(), "utf-8") + "</errorLanguage>\n" + "</requestEnvelope>\n" + "</ns2:GetAvailableShippingAddressesRequest>" + "</soapenv:Body>\n" + "</soapenv:Envelope>", i() + "GetAvailableShippingAddresses/");
        if (i(b2)) {
            try {
                c.i(b2, this.g);
            } catch (Throwable th) {
            }
            return false;
        }
        c.i(b2, this.g);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean n() {
        Hashtable hashtable = this.g;
        String a2 = c.a(PayPalActivity.b.h(), "utf-8");
        k kVar = (k) hashtable.get("ClientDetails");
        String a3 = c.a((String) hashtable.get("CurrencyCode"), "utf-8");
        String a4 = c.a((String) hashtable.get("DateOfMonth"), "utf-8");
        String a5 = c.a((String) hashtable.get("DayOfWeek"), "utf-8");
        String a6 = c.a((String) hashtable.get("EndingDate"), "utf-8");
        String a7 = c.a((String) hashtable.get("IpnNotificationUrl"), "utf-8");
        String a8 = c.a((String) hashtable.get("MaxAmountPerPayment"), "utf-8");
        String a9 = c.a((String) hashtable.get("MaxNumberOfPayments"), "utf-8");
        String a10 = c.a((String) hashtable.get("MaxNumberOfPaymentsPerPeriod"), "utf-8");
        String a11 = c.a((String) hashtable.get("MaxTotalAmountOfAllPayments"), "utf-8");
        String a12 = c.a((String) hashtable.get("Memo"), "utf-8");
        String a13 = c.a((String) hashtable.get("PaymentPeriod"), "utf-8");
        String a14 = c.a((String) hashtable.get("PinType"), "utf-8");
        String a15 = c.a((String) hashtable.get("SenderEmail"), "utf-8");
        String str = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:PreapprovalRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<requestEnvelope>\n<errorLanguage>" + a2 + "</errorLanguage>\n" + "</requestEnvelope>\n" + "<cancelUrl>" + "https://www.paypal.com" + "</cancelUrl>\n" + "<currencyCode>" + a3 + "</currencyCode>\n" + "<endingDate>" + a6 + "</endingDate>\n" + "<maxTotalAmountOfAllPayments>" + a11 + "</maxTotalAmountOfAllPayments>\n" + "<returnUrl>" + "https://www.paypal.com" + "</returnUrl>\n" + "<startingDate>" + c.a((String) hashtable.get("StartingDate"), "utf-8") + "</startingDate>\n";
        if (kVar != null) {
        }
        String str2 = (a4 == null || a4.length() <= 0) ? str : str + "<dateOfMonth>" + a4 + "</dateOfMonth>\n";
        if (a5 != null && a5.length() > 0) {
            str2 = str2 + "<dayOfWeek>" + a5 + "</dayOfWeek>\n";
        }
        if (a7 != null && a7.length() > 0) {
            str2 = str2 + "<ipnNotificationUrl>" + a7 + "</ipnNotificationUrl>\n";
        }
        if (a8 != null && a8.length() > 0) {
            str2 = str2 + "<maxAmountPerPayment>" + a8 + "</maxAmountPerPayment>\n";
        }
        if (a9 != null && a9.length() > 0) {
            str2 = str2 + "<maxNumberOfPayments>" + a9 + "</maxNumberOfPayments>\n";
        }
        if (a10 != null && a10.length() > 0) {
            str2 = str2 + "<maxNumberOfPaymentsPerPeriod>" + a10 + "</maxNumberOfPaymentsPerPeriod>\n";
        }
        if (a12 != null && a12.length() > 0) {
            str2 = str2 + "<memo>" + a12 + "</memo>\n";
        }
        if (a13 != null && a13.length() > 0) {
            str2 = str2 + "<paymentPeriod>" + a13 + "</paymentPeriod>\n";
        }
        if (a14 != null && a14.length() > 0) {
            str2 = str2 + "<pinType>" + a14 + "</pinType>\n";
        }
        if (a15 != null && a15.length() > 0) {
            str2 = str2 + "<senderEmail>" + a15 + "</senderEmail>\n";
        }
        String b2 = b(str2 + "</ns2:PreapprovalRequest></soapenv:Body>\n</soapenv:Envelope>", i() + "Preapproval/");
        if (i(b2)) {
            try {
                c.c(b2, this.g);
            } catch (Throwable th) {
            }
            return false;
        }
        c.c(b2, this.g);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean o() {
        Hashtable hashtable = this.g;
        String a2 = c.a(PayPalActivity.b.h(), "utf-8");
        String a3 = c.a((String) hashtable.get("PreapprovalKey"), "utf-8");
        String a4 = c.a((String) hashtable.get("GetBillingAddress"), "utf-8");
        String str = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:PreapprovalDetailsRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<requestEnvelope>\n<errorLanguage>" + a2 + "</errorLanguage>\n" + "</requestEnvelope>\n" + "<preapprovalKey>" + a3 + "</preapprovalKey>\n";
        String b2 = b((a4 != null ? str + "<getBillingAddress>" + a4 + "</getBillingAddress>\n" : str) + "</ns2:PreapprovalDetailsRequest></soapenv:Body>\n</soapenv:Envelope>", i() + "PreapprovalDetails/");
        if (i(b2)) {
            try {
                c.d(b2, this.g);
            } catch (Throwable th) {
            }
            return false;
        }
        c.d(b2, this.g);
        if (!this.g.get("Approved").equals("true")) {
            return true;
        }
        Intent putExtra = new Intent(PayPalActivity.i).putExtra("FATAL_ERROR_ID", "-1").putExtra("FATAL_ERROR_MESSAGE", d.a("ANDROID_preapproval_already_approved"));
        PayPalActivity.a().a((String) PayPalActivity.a.d("CorrelationId"), (String) PayPalActivity.a.d("PayKey"), "-1", d.a("ANDROID_preapproval_already_approved"), false, true);
        PayPalActivity.a().sendBroadcast(putExtra);
        return false;
    }

    /* access modifiers changed from: private */
    public boolean p() {
        Hashtable hashtable = this.g;
        String a2 = c.a(PayPalActivity.b.h(), "utf-8");
        String a3 = c.a((String) hashtable.get("PreapprovalKey"), "utf-8");
        String a4 = c.a((String) hashtable.get("Pin"), "utf-8");
        String str = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:ConfirmPreapprovalRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<requestEnvelope>\n<errorLanguage>" + a2 + "</errorLanguage>\n" + "</requestEnvelope>\n" + "<preapprovalKey>" + a3 + "</preapprovalKey>\n";
        String b2 = b((a4 != null ? str + "<pin>" + a4 + "</pin>\n" : str) + "</ns2:ConfirmPreapprovalRequest></soapenv:Body>\n</soapenv:Envelope>", i() + "ConfirmPreapproval/");
        if (i(b2)) {
            try {
                c.e(b2, this.g);
            } catch (Throwable th) {
            }
            return false;
        }
        c.e(b2, this.g);
        return true;
    }

    private static boolean q() {
        String deviceId = ((TelephonyManager) o.a().e().getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            deviceId = ((WifiManager) o.a().e().getSystemService("wifi")).getConnectionInfo().getMacAddress();
        }
        if (o.a().k() != 1 || !deviceId.equals("000000000000000")) {
            return true;
        }
        Intent putExtra = new Intent(PayPalActivity.i).putExtra("FATAL_ERROR_ID", "-1").putExtra("FATAL_ERROR_MESSAGE", d.a("ANDROID_simulator_payment_block"));
        PayPalActivity.a().a((String) PayPalActivity.a.d("CorrelationId"), (String) PayPalActivity.a.d("PayKey"), "-1", d.a("ANDROID_simulator_payment_block"), false, true);
        PayPalActivity.a().sendBroadcast(putExtra);
        return false;
    }

    /* access modifiers changed from: private */
    public boolean r() {
        if (!q()) {
            return false;
        }
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("APIVersion", "3.0"));
            arrayList.add(new BasicNameValuePair("PayPalApplicationID", URLEncoder.encode(o.a().f(), "utf-8")));
            String str = "";
            try {
                FileInputStream openFileInput = o.a().e().openFileInput("DeviceReferenceToken");
                byte[] bArr = new byte[openFileInput.available()];
                openFileInput.read(bArr);
                openFileInput.close();
                String str2 = new String(bArr);
                if (str2.length() > 0) {
                    str = str2;
                }
            } catch (Exception e2) {
            }
            arrayList.add(new BasicNameValuePair("DeviceReferenceToken", str));
            arrayList.add(new BasicNameValuePair("AuthorizeDevice", "true"));
            return h(a(arrayList));
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public Hashtable s() {
        String str;
        BigDecimal bigDecimal;
        String str2;
        Hashtable hashtable = this.g;
        String a2 = c.a((String) hashtable.get("PaymentCurrencyID"), "utf-8");
        String a3 = c.a(PayPalActivity.b.h(), "utf-8");
        String a4 = c.a((String) hashtable.get("TrackingId"), "utf-8");
        String a5 = c.a((String) hashtable.get("ReverseAllParallelPaymentsOnError"), "utf-8");
        String a6 = c.a((String) hashtable.get("Pin"), "utf-8");
        String a7 = c.a((String) hashtable.get("IpnNotificationUrl"), "utf-8");
        String a8 = c.a((String) hashtable.get("FeesPayer"), "utf-8");
        String a9 = c.a((String) hashtable.get("FundingType"), "utf-8");
        String a10 = c.a((String) hashtable.get("Memo"), "utf-8");
        k kVar = (k) hashtable.get("ClientDetails");
        ArrayList arrayList = (ArrayList) hashtable.get("Receivers");
        String str3 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Body>\n<ns2:Pay xmlns:ns2=\"http://svcs.paypal.com/services\">\n<requestEnvelope>\n<errorLanguage>" + a3 + "</errorLanguage>\n" + "</requestEnvelope>\n" + "<sender>\n" + "<useCredentials>true</useCredentials>\n" + "</sender>\n" + "<actionType>" + ((String) hashtable.get("ActionType")) + "</actionType>\n" + "<cancelUrl>" + "https://www.paypal.com" + "</cancelUrl>\n" + "<returnUrl>" + "https://www.paypal.com" + "</returnUrl>\n" + "<currencyCode>" + a2 + "</currencyCode>\n" + "<receiverList>\n";
        if (arrayList == null) {
            str = null;
        } else {
            int i = 0;
            while (true) {
                String str4 = str3;
                if (i < arrayList.size()) {
                    BigDecimal b2 = ((com.paypal.android.MEP.f) arrayList.get(i)).b();
                    if (((com.paypal.android.MEP.f) arrayList.get(i)).c() != null) {
                        BigDecimal b3 = ((com.paypal.android.MEP.f) arrayList.get(i)).c().b();
                        BigDecimal a11 = ((com.paypal.android.MEP.f) arrayList.get(i)).c().a();
                        bigDecimal = b3 != null ? b2.add(b3) : b2;
                        if (a11 != null) {
                            bigDecimal = bigDecimal.add(a11);
                        }
                    } else {
                        bigDecimal = b2;
                    }
                    bigDecimal.setScale(2, 4);
                    b2.setScale(2, 4);
                    int d2 = ((com.paypal.android.MEP.f) arrayList.get(i)).d();
                    int e2 = ((com.paypal.android.MEP.f) arrayList.get(i)).e();
                    boolean i2 = ((com.paypal.android.MEP.f) arrayList.get(i)).i();
                    String str5 = (str4 + "<receiver>\n") + "<amount>" + bigDecimal.toString() + "</amount>\n";
                    String a12 = c.a(((com.paypal.android.MEP.f) arrayList.get(i)).a(), "utf-8");
                    if (a12 == null || a12.length() <= 0) {
                        str = null;
                    } else {
                        if (!((com.paypal.android.MEP.f) arrayList.get(i)).j()) {
                            if (!((com.paypal.android.MEP.f) arrayList.get(i)).k()) {
                                str = null;
                                break;
                            }
                            String str6 = str5 + "<phone>\n";
                            String e3 = c.e(a12);
                            if (e3 == null || e3.length() == 0) {
                                e3 = c(Locale.getDefault().getCountry());
                            }
                            str2 = ((str6 + "<countryCode>" + e3 + "</countryCode>") + "<phoneNumber>" + c.d(a12) + "</phoneNumber>") + "</phone>\n";
                        } else {
                            str2 = str5 + "<email>" + a12 + "</email>\n";
                        }
                        if (d2 != 3) {
                            str2 = str2 + "<paymentType>" + o.d(d2) + "</paymentType>\n";
                        }
                        if (e2 != 22) {
                            str2 = str2 + "<paymentSubtype>" + o.e(e2) + "</paymentSubtype>\n";
                        }
                        if (i2) {
                            str2 = str2 + "<primary>" + i2 + "</primary>\n";
                        }
                        str3 = str2 + "</receiver>\n";
                        i++;
                    }
                } else {
                    String str7 = str4 + "</receiverList>\n";
                    if (a4 != null && a4.length() > 0) {
                        str7 = str7 + "<trackingId>" + a4 + "</trackingId>\n";
                    }
                    if (a5 != null && a5.length() > 0) {
                        str7 = str7 + "<reverseAllParallelPaymentsOnError>" + a5 + "</reverseAllParallelPaymentsOnError>\n";
                    }
                    if (a6 != null && a6.length() > 0) {
                        str7 = str7 + "<pin>" + a6 + "</pin>\n";
                    }
                    if (a7 != null && a7.length() > 0) {
                        str7 = str7 + "<ipnNotificationUrl>" + a7 + "</ipnNotificationUrl>\n";
                    }
                    if (a8 != null && a8.length() > 0) {
                        str7 = str7 + "<feesPayer>" + a8 + "</feesPayer>\n";
                    }
                    if (a9 != null && a9.length() > 0) {
                        str7 = str7 + "<fundingConstraint>\n<allowedFundingType>\n<fundingTypeInfo>\n<fundingType>" + a9 + "</fundingType>\n" + "</fundingTypeInfo>\n" + "</allowedFundingType>\n" + "</fundingConstraint>\n";
                    }
                    if (kVar != null) {
                    }
                    str = ((a10 == null || a10.length() <= 0) ? str7 : str7 + "<memo>" + a10 + "</memo>\n") + "</ns2:Pay></soapenv:Body>\n</soapenv:Envelope>";
                }
            }
        }
        if (str == null) {
            return null;
        }
        String b4 = b(str, i() + "Pay/");
        if (i(b4)) {
            try {
                c.b(b4, this.g);
            } catch (Throwable th) {
            }
            return null;
        } else if (!c.b(b4, this.g)) {
            this.e = -1;
            return null;
        } else if (b4.contains("defaultFundingPlan") || !((String) this.g.get("ActionType")).equals("CREATE")) {
            return this.g;
        } else {
            Intent putExtra = new Intent(PayPalActivity.i).putExtra("FATAL_ERROR_ID", "589009").putExtra("FATAL_ERROR_MESSAGE", d.a("ANDROID_589009"));
            PayPalActivity.a().a((String) PayPalActivity.a.d("CorrelationId"), (String) PayPalActivity.a.d("PayKey"), "589009", d.a("ANDROID_589009"), false, true);
            try {
                PayPalActivity.a().sendBroadcast(putExtra);
            } catch (Exception e4) {
            }
            return null;
        }
    }

    /* access modifiers changed from: private */
    public Hashtable t() {
        String a2 = c.a((String) this.g.get("PayKey"), "utf-8");
        String b2 = b("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:GetFundingPlansRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<payKey>" + a2 + "</payKey>\n" + "<requestEnvelope>\n" + "<errorLanguage>" + c.a(PayPalActivity.b.h(), "utf-8") + "</errorLanguage>\n" + "</requestEnvelope>\n" + "</ns2:GetFundingPlansRequest>" + "</soapenv:Body>\n" + "</soapenv:Envelope>", i() + "GetFundingPlans/");
        if (i(b2)) {
            try {
                c.f(b2, this.g);
            } catch (Throwable th) {
            }
            return null;
        } else if (!c.f(b2, this.g)) {
            this.e = -1;
            return null;
        } else if (((Vector) this.g.get("FundingPlans")).size() != 0) {
            return this.g;
        } else {
            try {
                PayPalActivity.a().sendBroadcast(new Intent(PayPalActivity.i).putExtra("FATAL_ERROR_ID", "589009").putExtra("FATAL_ERROR_MESSAGE", d.a("ANDROID_589009")));
            } catch (Exception e2) {
            }
            return null;
        }
    }

    private static String u() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e2) {
            Log.e("Exception", e2.toString());
        }
        return null;
    }

    /* access modifiers changed from: private */
    public boolean v() {
        String b2 = b(c.a(), h() + j());
        if (i(b2)) {
            try {
                c.a(b2, this.g);
            } catch (Throwable th) {
            }
            return false;
        } else if (c.a(b2, this.g)) {
            o.a().a(true);
            return true;
        } else {
            this.e = -1;
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean w() {
        Hashtable hashtable = this.g;
        String a2 = c.a((String) hashtable.get("SessionToken"), "utf-8");
        String a3 = c.a((String) hashtable.get("NewPhone"), "utf-8");
        String a4 = c.a(c.e(a3), "utf-8");
        String b2 = b("<?xml version=\"1.0\" encoding=\"UTF-8\"?><SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:cc=\"urn:ebay:apis:CoreComponentTypes\" xmlns:ed=\"urn:ebay:apis:EnhancedDataTypes\" xmlns:wsu=\"http://schemas.xmlsoap.org/ws/2002/07/utility\" xmlns:saml=\"urn:oasis:names:tc:SAML:1.0:assertion\" xmlns:ds=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:wsse=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xmlns:ebl=\"urn:ebay:apis:eBLBaseComponents\" xmlns:ns=\"urn:ebay:api:PayPalAPI\"><SOAP-ENV:Header><Security xmlns=\"http://schemas.xmlsoap.org/ws/2002/12/secext\" xsi:type=\"wsse:SecurityType\"></Security><RequesterCredentials xmlns=\"urn:ebay:api:PayPalAPI\" xsi:type=\"ebl:CustomSecurityHeaderType\"><Credentials xmlns=\"urn:ebay:apis:eBLBaseComponents\" xsi:type=\"ebl:UserIdPasswordType\"><Username xsi:type=\"xs:string\">gmapi_client</Username><Password xsi:type=\"xs:string\">11111111</Password></Credentials></RequesterCredentials></SOAP-ENV:Header><SOAP-ENV:Body id=\"_0\"><MEPCreateDevicePinReq xmlns=\"urn:ebay:api:PayPalAPI\"><Request><Version xmlns=\"urn:ebay:apis:eBLBaseComponents\">1</Version><RequestorID xsi:type=\"ns:IVRRequestorID\">eding-1</RequestorID><SessionToken xsi:type=\"ns:MEPSessionToken\">" + a2 + "</SessionToken>" + "<Phone xsi:type=\"ns:IVRPhoneNumber\">" + "<CountryCode xsi:type=\"xs:string\">" + ((a4 == null || a4.equals("")) ? c(((TelephonyManager) o.a().e().getSystemService("phone")).getSimCountryIso()) : a4) + "</CountryCode>" + "<PhoneNumber xsi:type=\"xs:string\">" + c.d(a3) + "</PhoneNumber>" + "<Extension xsi:type=\"xs:string\">" + "" + "</Extension>" + "</Phone>" + "<Pin xsi:type=\"xs:string\">" + c.a((String) hashtable.get("NewPin"), "utf-8") + "</Pin>" + "</Request>" + "</MEPCreateDevicePinReq>" + "</SOAP-ENV:Body>" + "</SOAP-ENV:Envelope>", h() + j());
        if (i(b2)) {
            try {
                c.g(b2, this.g);
            } catch (Throwable th) {
            }
            return false;
        } else if (c.g(b2, this.g)) {
            o.a().f((String) this.g.get("NewPhone"));
            o.a().b(1);
            return true;
        } else {
            this.e = -1;
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean x() {
        String b2 = b(c.b(), h() + j());
        if (i(b2)) {
            try {
                c.h(b2, this.g);
            } catch (Throwable th) {
            }
            return false;
        } else if (c.h(b2, this.g)) {
            return true;
        } else {
            this.e = -1;
            return false;
        }
    }

    public final void a() {
    }

    public final void a(int i) {
        this.f = i;
        this.d = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.PayPalActivity.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.MEP.PayPalActivity.a(com.paypal.android.MEP.k, java.lang.String, java.util.Vector):java.util.Vector
      com.paypal.android.MEP.PayPalActivity.a(java.lang.String, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.paypal.android.a.f.a(com.paypal.android.a.f, int):int
      com.paypal.android.a.f.a(java.lang.String, java.lang.String):boolean
      com.paypal.android.a.f.a(int, java.lang.Object):void
      com.paypal.android.MEP.b.a(int, java.lang.Object):void
      com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r7, java.lang.Object r8) {
        /*
            r6 = this;
            r2 = 4
            r5 = 1
            r4 = 7
            r3 = 0
            switch(r7) {
                case 3: goto L_0x0008;
                case 4: goto L_0x003b;
                case 5: goto L_0x00a1;
                case 6: goto L_0x0007;
                case 7: goto L_0x00c7;
                case 8: goto L_0x0007;
                case 9: goto L_0x0098;
                case 10: goto L_0x0007;
                case 11: goto L_0x0007;
                case 12: goto L_0x0007;
                case 13: goto L_0x011c;
                default: goto L_0x0007;
            }
        L_0x0007:
            return
        L_0x0008:
            java.util.Hashtable r0 = r6.g
            java.lang.String r1 = "quickPay"
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "true"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x001e
            r6.a(r2)
            goto L_0x0007
        L_0x001e:
            com.paypal.android.MEP.o r0 = com.paypal.android.MEP.o.a()
            boolean r0 = r0.n()
            if (r0 == 0) goto L_0x002c
            r6.a(r4)
            goto L_0x0007
        L_0x002c:
            com.paypal.android.MEP.PayPalActivity r0 = com.paypal.android.MEP.PayPalActivity.a()
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = com.paypal.android.MEP.PayPalActivity.g
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x0007
        L_0x003b:
            com.paypal.android.MEP.o r0 = com.paypal.android.MEP.o.a()
            boolean r1 = r0.v()
            if (r1 != 0) goto L_0x0051
            boolean r1 = r0.u()
            if (r1 != 0) goto L_0x0051
            boolean r0 = r0.D()
            if (r0 == 0) goto L_0x006d
        L_0x0051:
            com.paypal.android.MEP.PayPalActivity r1 = com.paypal.android.MEP.PayPalActivity.a()
            com.paypal.android.a.f r0 = com.paypal.android.MEP.PayPalActivity.a
            java.lang.String r2 = "PayKey"
            java.lang.Object r6 = r0.d(r2)
            java.lang.String r6 = (java.lang.String) r6
            com.paypal.android.a.f r0 = com.paypal.android.MEP.PayPalActivity.a
            java.lang.String r2 = "PaymentExecStatus"
            java.lang.Object r0 = r0.d(r2)
            java.lang.String r0 = (java.lang.String) r0
            r1.a(r6, r0, r5)
            goto L_0x0007
        L_0x006d:
            com.paypal.android.MEP.PayPalActivity r0 = com.paypal.android.MEP.PayPalActivity.a()
            r0.a(r5)
            com.paypal.android.MEP.PayPalActivity r1 = com.paypal.android.MEP.PayPalActivity.a()
            com.paypal.android.a.f r0 = com.paypal.android.MEP.PayPalActivity.a
            java.lang.String r2 = "PayKey"
            java.lang.Object r6 = r0.d(r2)
            java.lang.String r6 = (java.lang.String) r6
            com.paypal.android.a.f r0 = com.paypal.android.MEP.PayPalActivity.a
            java.lang.String r2 = "PaymentExecStatus"
            java.lang.Object r0 = r0.d(r2)
            java.lang.String r0 = (java.lang.String) r0
            r1.a(r6, r0, r3)
            java.lang.String r8 = (java.lang.String) r8
            com.paypal.android.MEP.a.k.a = r8
            com.paypal.android.MEP.a.q.b(r4)
            goto L_0x0007
        L_0x0098:
            java.lang.String r8 = (java.lang.String) r8
            com.paypal.android.MEP.a.l.a = r8
            com.paypal.android.MEP.a.q.a(r2)
            goto L_0x0007
        L_0x00a1:
            java.lang.String r0 = "FundingPlanId"
            java.lang.String r1 = "0"
            r6.a(r0, r1)
            com.paypal.android.MEP.o r0 = com.paypal.android.MEP.o.a()
            boolean r0 = r0.n()
            if (r0 == 0) goto L_0x00b7
            r6.a(r4)
            goto L_0x0007
        L_0x00b7:
            com.paypal.android.MEP.PayPalActivity r0 = com.paypal.android.MEP.PayPalActivity.a()
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = com.paypal.android.MEP.PayPalActivity.g
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x0007
        L_0x00c7:
            com.paypal.android.MEP.o r0 = com.paypal.android.MEP.o.a()
            int r0 = r0.k()
            r1 = 2
            if (r0 != r1) goto L_0x0113
            java.util.Hashtable r0 = com.paypal.android.MEP.a.m.a
        L_0x00d4:
            java.lang.String r1 = "AvailableAddresses"
            java.lang.Object r0 = r0.get(r1)
            java.util.Vector r0 = (java.util.Vector) r0
            if (r0 == 0) goto L_0x012c
            int r1 = r0.size()
            if (r1 <= 0) goto L_0x012c
            java.lang.Object r0 = r0.get(r3)
            com.paypal.android.a.a.i r0 = (com.paypal.android.a.a.i) r0
            java.lang.String r1 = "ShippingAddressId"
            java.lang.Object r1 = r6.d(r1)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 != 0) goto L_0x012c
            java.lang.String r1 = "ShippingAddressId"
            java.lang.String r0 = r0.h()
            r6.a(r1, r0)
            boolean r0 = r6.g()
        L_0x0101:
            if (r0 != 0) goto L_0x0007
            com.paypal.android.MEP.PayPalActivity r0 = com.paypal.android.MEP.PayPalActivity.a()
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = com.paypal.android.MEP.PayPalActivity.g
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x0007
        L_0x0113:
            java.util.Hashtable r8 = (java.util.Hashtable) r8
            com.paypal.android.MEP.a.m.a = r8
            com.paypal.android.a.f r0 = com.paypal.android.MEP.PayPalActivity.a
            java.util.Hashtable r0 = r0.g
            goto L_0x00d4
        L_0x011c:
            com.paypal.android.MEP.PayPalActivity r0 = com.paypal.android.MEP.PayPalActivity.a()
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = com.paypal.android.MEP.PayPalActivity.g
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x0007
        L_0x012c:
            r0 = r3
            goto L_0x0101
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.a.f.a(int, java.lang.Object):void");
    }

    public final void a(String str) {
    }

    public final void a(String str, Object obj) {
        if (obj != null) {
            this.g.put(str, obj);
        } else {
            this.g.remove(str);
        }
    }

    public final void b(String str) {
        if (o.a().k() != 2) {
            o a2 = o.a();
            try {
                if (this.c == null) {
                    this.c = new DefaultHttpClient();
                }
                String upperCase = Locale.getDefault().getCountry().toUpperCase();
                String x = o.x();
                String packageName = a2.e().getPackageName();
                String str2 = a2.s() == 0 ? "Password" : a2.s() == 1 ? "PIN" : "Device";
                this.b = new HttpGet(((((((("https://sstats.paypal-metrics.com/b/ss/paypalwireless/5/H.5--WAP/12345?pageName=android/" + "mpl-" + str) + "&c1=" + upperCase) + "&c4=ver" + x) + "&c5=" + "Android") + "&c6=" + packageName) + "&c7=" + str2) + "&c9=" + (a2.z() == 3 ? "Preapproval" : a2.z() == 2 ? "Chained" : a2.z() == 1 ? "Parallel" : "Simple")) + "&c10=" + (a2.n() ? "Enabled" : "Disabled"));
                this.c.execute(this.b).getStatusLine().toString();
                this.b = null;
                this.c = null;
            } catch (SocketException e2) {
                e2.printStackTrace();
            } catch (UnknownHostException e3) {
                e3.printStackTrace();
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        }
    }

    public final Object d(String str) {
        return this.g.get(str);
    }

    public final String d() {
        return d.a("ANDROID_" + this.e);
    }

    public final Hashtable e() {
        return this.g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.paypal.android.a.f.a(com.paypal.android.a.f, int):int
      com.paypal.android.a.f.a(java.lang.String, java.lang.String):boolean
      com.paypal.android.a.f.a(int, java.lang.Object):void
      com.paypal.android.MEP.b.a(int, java.lang.Object):void
      com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void */
    public final void f() {
        String str;
        o a2 = o.a();
        i c2 = a2.c();
        this.g.remove("ShippingAddressId");
        a("PaymentCurrencyID", (Object) c2.a());
        a("CancelUrl", (Object) a2.i());
        a("ReturnUrl", (Object) a2.j());
        switch (a2.m()) {
            case 1:
                str = "SENDER";
                break;
            case 2:
                str = "PRIMARYRECEIVER";
                break;
            case 3:
                str = "SECONDARYONLY";
                break;
            default:
                str = "EACHRECEIVER";
                break;
        }
        a("FeesPayer", (Object) str);
        boolean z = true;
        int i = 0;
        while (true) {
            boolean z2 = z;
            if (i < c2.b().size()) {
                z = ((com.paypal.android.MEP.f) c2.b().get(i)).c() == null ? false : z2;
                i++;
            } else {
                if (a2.n() || !z2 || !d("quickPay").equals("true")) {
                    a("ActionType", (Object) "CREATE");
                } else {
                    a("ActionType", (Object) "PAY");
                }
                a("Receivers", c2.b());
                if (c2.c() != null && !c2.c().equals("")) {
                    a("IpnNotificationUrl", (Object) c2.c());
                }
                if (c2.d() != null && !c2.d().equals("")) {
                    a("Memo", (Object) c2.d());
                }
                a("delegate", this);
                PayPalActivity.a.a(3);
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.paypal.android.a.f.a(com.paypal.android.a.f, int):int
      com.paypal.android.a.f.a(java.lang.String, java.lang.String):boolean
      com.paypal.android.a.f.a(int, java.lang.Object):void
      com.paypal.android.MEP.b.a(int, java.lang.Object):void
      com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void */
    public final boolean g() {
        com.paypal.android.a.a.i iVar;
        o a2 = o.a();
        if (a2.o() && a2.n()) {
            String str = (String) this.g.get("ShippingAddressId");
            Vector vector = (Vector) (o.a().k() == 2 ? m.a : this.g).get("AvailableAddresses");
            int i = 0;
            while (true) {
                if (i >= vector.size()) {
                    iVar = null;
                    break;
                }
                com.paypal.android.a.a.i iVar2 = (com.paypal.android.a.a.i) vector.elementAt(i);
                if (iVar2.h().equals(str)) {
                    iVar = iVar2;
                    break;
                }
                i++;
            }
            com.paypal.android.MEP.k kVar = new com.paypal.android.MEP.k();
            String d2 = iVar.d();
            String e2 = iVar.e();
            String b2 = iVar.b();
            String g2 = iVar.g();
            String f2 = iVar.f();
            String c2 = iVar.c();
            kVar.a(d2);
            kVar.b(e2);
            kVar.c(b2);
            kVar.d(g2);
            kVar.e(f2);
            kVar.f(c2);
            i c3 = o.a().c();
            ArrayList b3 = c3.b();
            Vector vector2 = new Vector();
            for (int i2 = 0; i2 < c3.b().size(); i2++) {
                com.paypal.android.MEP.f fVar = (com.paypal.android.MEP.f) c3.b().get(i2);
                r rVar = new r();
                rVar.a = new p();
                rVar.b = fVar.a();
                if (fVar.c().a() != null) {
                    rVar.a.b(fVar.c().a());
                }
                if (fVar.c().b() != null) {
                    rVar.a.c(fVar.c().b());
                }
                if (c3.a() != null) {
                    rVar.a.a(c3.a());
                }
                if (fVar.b() != null) {
                    rVar.a.a(fVar.b());
                }
                vector2.add(rVar);
            }
            Vector a3 = PayPalActivity.a().a(kVar, c3.a(), vector2);
            if (a3.size() == b3.size()) {
                int i3 = 0;
                boolean z = false;
                while (i3 < a3.size()) {
                    com.paypal.android.MEP.f fVar2 = (com.paypal.android.MEP.f) b3.get(i3);
                    r rVar2 = (r) a3.elementAt(i3);
                    BigDecimal a4 = fVar2.c().a() != null ? fVar2.c().a() : new BigDecimal("0.0");
                    BigDecimal b4 = fVar2.c().b() != null ? fVar2.c().b() : new BigDecimal("0.0");
                    BigDecimal b5 = fVar2.b() != null ? fVar2.b() : new BigDecimal("0.0");
                    String a5 = fVar2.a();
                    BigDecimal b6 = rVar2.a.b();
                    BigDecimal c4 = rVar2.a.c();
                    BigDecimal a6 = rVar2.a.a();
                    String str2 = rVar2.b;
                    if (a6 != b5 || !a5.equals(str2)) {
                        throw new IllegalArgumentException();
                    }
                    i3++;
                    z = (b6 == a4 && c4 == b4) ? z : true;
                }
                if (z) {
                    for (int i4 = 0; i4 < a3.size(); i4++) {
                        com.paypal.android.MEP.f fVar3 = (com.paypal.android.MEP.f) b3.get(i4);
                        r rVar3 = (r) a3.elementAt(i4);
                        fVar3.c().b(rVar3.a.c());
                        fVar3.c().a(rVar3.a.b());
                    }
                    a("PaymentCurrencyID", (Object) c3.a());
                    a("Receivers", c3.b());
                    if (c3.c() != null && !c3.c().equals("")) {
                        a("IpnNotificationUrl", (Object) c3.c());
                    }
                    if (c3.d() != null && !c3.d().equals("")) {
                        a("Memo", (Object) c3.d());
                    }
                    a("ActionType", (Object) "CREATE");
                    a("delegate", this);
                    a(3);
                    return true;
                }
            } else {
                throw new IllegalArgumentException();
            }
        }
        return false;
    }
}
