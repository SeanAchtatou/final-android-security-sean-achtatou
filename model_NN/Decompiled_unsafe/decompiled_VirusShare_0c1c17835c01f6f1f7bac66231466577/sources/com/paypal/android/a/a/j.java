package com.paypal.android.a.a;

import com.paypal.android.a.c;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class j {
    public e a;
    public e b;
    private String c;

    public static j a(Element element) {
        if (element == null) {
            return null;
        }
        j jVar = new j();
        NodeList elementsByTagName = element.getElementsByTagName("from");
        if (elementsByTagName.getLength() == 1) {
            jVar.a = e.a((Element) elementsByTagName.item(0));
        }
        NodeList elementsByTagName2 = element.getElementsByTagName("to");
        if (elementsByTagName2.getLength() == 1) {
            jVar.b = e.a((Element) elementsByTagName2.item(0));
        }
        NodeList elementsByTagName3 = element.getElementsByTagName("exchangeRate");
        if (elementsByTagName3.getLength() != 1) {
            return null;
        }
        jVar.c = c.a(((Element) elementsByTagName3.item(0)).getChildNodes());
        return jVar;
    }

    public final String a() {
        return this.c;
    }
}
