package com.paypal.android.a.a;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class f {
    public e a = null;
    public b b = null;

    public static f a(Element element) {
        b a2;
        if (element == null) {
            return null;
        }
        f fVar = new f();
        fVar.a = null;
        fVar.b = null;
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        for (int i = 0; i < length; i++) {
            Element element2 = (Element) childNodes.item(i);
            String tagName = element2.getTagName();
            if (tagName.equals("charge")) {
                e a3 = e.a(element2);
                if (a3 != null) {
                    fVar.a = a3;
                }
            } else if (tagName.equals("fundingSource") && (a2 = b.a(element2)) != null) {
                fVar.b = a2;
            }
        }
        if (fVar.a == null || fVar.b == null) {
            return null;
        }
        return fVar;
    }
}
