package com.paypal.android.a.a;

import com.paypal.android.a.c;
import java.math.BigDecimal;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class e {
    private BigDecimal a;
    private String b;

    public static e a(Element element) {
        if (element == null) {
            return null;
        }
        e eVar = new e();
        NodeList elementsByTagName = element.getElementsByTagName("code");
        if (elementsByTagName.getLength() != 1) {
            return null;
        }
        eVar.b = c.a(((Element) elementsByTagName.item(0)).getChildNodes());
        NodeList elementsByTagName2 = element.getElementsByTagName("amount");
        if (elementsByTagName2.getLength() != 1) {
            return null;
        }
        eVar.a(c.a(((Element) elementsByTagName2.item(0)).getChildNodes()));
        return eVar;
    }

    public final BigDecimal a() {
        return this.a;
    }

    public final void a(String str) {
        try {
            this.a = new BigDecimal(str);
        } catch (NumberFormatException e) {
            this.a = new BigDecimal("0.0");
        }
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }
}
