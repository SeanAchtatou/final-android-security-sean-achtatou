package com.paypal.android.a.a;

import com.paypal.android.a.c;
import java.util.Vector;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class g {
    public e a;
    public e b;
    public j c;
    public Vector d;
    private String e;

    public static g a(Element element) {
        if (element == null) {
            return null;
        }
        g gVar = new g();
        NodeList elementsByTagName = element.getElementsByTagName("fundingPlanId");
        if (elementsByTagName.getLength() != 1) {
            return null;
        }
        gVar.e = c.a(((Element) elementsByTagName.item(0)).getChildNodes());
        NodeList elementsByTagName2 = element.getElementsByTagName("fundingAmount");
        if (elementsByTagName2.getLength() != 1) {
            return null;
        }
        gVar.a = e.a((Element) elementsByTagName2.item(0));
        NodeList elementsByTagName3 = element.getElementsByTagName("backupFundingSource");
        if (elementsByTagName3.getLength() == 1) {
            b.a((Element) elementsByTagName3.item(0));
        }
        NodeList elementsByTagName4 = element.getElementsByTagName("senderFees");
        if (elementsByTagName4.getLength() == 1) {
            gVar.b = e.a((Element) elementsByTagName4.item(0));
        }
        NodeList elementsByTagName5 = element.getElementsByTagName("currencyConversion");
        if (elementsByTagName5.getLength() == 1) {
            gVar.c = j.a((Element) elementsByTagName5.item(0));
        }
        gVar.d = new Vector();
        NodeList elementsByTagName6 = element.getElementsByTagName("charge");
        for (int i = 0; i < elementsByTagName6.getLength(); i++) {
            f a2 = f.a((Element) elementsByTagName6.item(i));
            if (a2 != null) {
                gVar.d.add(a2);
            }
        }
        return gVar;
    }

    public final String a() {
        return this.e;
    }

    public final void a(String str) {
        this.e = str;
    }
}
