package com.paypal.android.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import com.paypal.android.MEP.o;
import com.paypal.android.a.h;

public final class i extends CheckBox {
    private StateListDrawable a = new StateListDrawable();

    public i(Context context) {
        super(context);
        Drawable a2 = h.a(92148, 842);
        Drawable a3 = h.a(162620, 1154);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setVisible(false, false);
        this.a.addState(new int[]{-16842912}, a2);
        this.a.addState(new int[]{16842912}, a3);
        setLayoutParams(new LinearLayout.LayoutParams((int) (((double) a2.getIntrinsicWidth()) * Math.pow((double) o.a().A(), 2.0d)), (int) (((double) a2.getIntrinsicHeight()) * Math.pow((double) o.a().A(), 2.0d))));
        setBackgroundDrawable(this.a);
        setButtonDrawable(gradientDrawable);
    }
}
