package com.paypal.android.b;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.a.e;
import com.paypal.android.a.j;

public final class c extends LinearLayout {
    private TextView a;
    private TextView b;

    public c(Context context, j jVar, j jVar2) {
        super(context);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.5f));
        setBackgroundColor(0);
        this.a = e.a(jVar, context);
        this.a.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.4f));
        this.a.setPadding(2, 2, 2, 2);
        this.a.setBackgroundColor(0);
        this.a.setVisibility(8);
        this.b = e.a(jVar2, context);
        this.b.setGravity(5);
        this.b.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.6f));
        this.b.setPadding(2, 2, 2, 2);
        this.b.setBackgroundColor(0);
        this.b.setVisibility(8);
        addView(this.a);
        addView(this.b);
    }

    public c(Context context, j jVar, j jVar2, float f, float f2) {
        super(context);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.5f));
        setBackgroundColor(0);
        this.a = e.a(jVar, context);
        this.a.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.5f));
        this.a.setPadding(2, 2, 2, 2);
        this.a.setBackgroundColor(0);
        this.a.setVisibility(8);
        this.b = e.a(jVar2, context);
        this.b.setGravity(5);
        this.b.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.5f));
        this.b.setPadding(2, 2, 2, 2);
        this.b.setBackgroundColor(0);
        this.b.setVisibility(8);
        addView(this.a);
        addView(this.b);
    }

    public final void a(int i) {
        this.a.setTextColor(i);
    }

    public final void a(String str) {
        this.a.setVisibility(8);
        if (str != null && str.length() > 0) {
            this.a.setText(str);
            this.a.setVisibility(0);
        }
    }

    public final void b(int i) {
        this.b.setTextColor(i);
    }

    public final void b(String str) {
        this.b.setVisibility(8);
        if (str != null && str.length() > 0) {
            this.b.setText(str);
            this.b.setVisibility(0);
        }
    }
}
