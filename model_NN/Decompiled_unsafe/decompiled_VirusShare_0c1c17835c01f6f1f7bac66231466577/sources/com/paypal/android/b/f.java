package com.paypal.android.b;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

public class f extends LinearLayout implements View.OnFocusChangeListener {
    private LinearLayout.LayoutParams[] a;
    private p b;
    private int c = 2;
    protected int d = 0;

    public f(Context context) {
        super(context);
        setOnFocusChangeListener(this);
        setFocusable(true);
        setOrientation(1);
    }

    public void a(int i) {
        if (i != this.d) {
            if (i < 0 || i >= this.c) {
                throw new IllegalArgumentException("State " + i + " is outside the acceptable range 0-" + (this.c - 1));
            }
            this.d = i;
            setLayoutParams(this.a[this.d]);
            if (this.b != null) {
                this.b.a(this, i);
            }
        }
    }

    public final void a(LinearLayout.LayoutParams layoutParams, int i) {
        if (this.a == null) {
            this.a = new LinearLayout.LayoutParams[this.c];
        }
        this.a[i] = layoutParams;
        if (i == this.d) {
            setLayoutParams(layoutParams);
        }
    }

    public final void a(p pVar) {
        this.b = pVar;
    }

    public void b(boolean z) {
    }

    public void onFocusChange(View view, boolean z) {
        b(z);
    }
}
