package com.paypal.android.b;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.a.e;
import com.paypal.android.a.h;
import com.paypal.android.a.j;

public final class m extends LinearLayout {
    private Context a;
    private GradientDrawable b;
    private ImageView c;
    private TextView d;

    public m(Context context, o oVar) {
        super(context);
        this.a = context;
        setOrientation(0);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        setGravity(16);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(17);
        linearLayout.setPadding(5, 10, 5, 10);
        switch (n.a[oVar.ordinal()]) {
            case 1:
                this.b = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3172, -52});
                this.b.setStroke(1, -12529);
                this.b.setCornerRadius(3.0f);
                setBackgroundDrawable(this.b);
                this.c = h.a(this.a, "system-icon-error.png");
                this.d = e.a(j.HELVETICA_12_NORMAL, context);
                this.d.setTextColor(-65536);
                break;
            case 2:
                this.b = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3172, -52});
                this.b.setStroke(1, -12529);
                this.b.setCornerRadius(3.0f);
                setBackgroundDrawable(this.b);
                this.c = h.a(this.a, "system-icon-alert.png");
                this.d = e.a(j.HELVETICA_12_NORMAL, context);
                this.d.setTextColor(-65536);
                break;
            case 3:
                this.b = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3219987, -1510918});
                this.b.setStroke(1, -8280890);
                this.b.setCornerRadius(3.0f);
                setBackgroundDrawable(this.b);
                this.c = h.a(this.a, "system-icon-confirmation.png");
                this.d = e.a(j.HELVETICA_12_NORMAL, context);
                this.d.setTextColor(-13408768);
                break;
            case 4:
                this.b = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3219987, -1510918});
                this.b.setStroke(1, -8280890);
                this.b.setCornerRadius(3.0f);
                setBackgroundDrawable(this.b);
                this.c = h.a(this.a, "system-icon-notification.png");
                this.d = e.a(j.HELVETICA_12_NORMAL, context);
                this.d.setTextColor(-13408615);
                break;
        }
        linearLayout.addView(this.c);
        addView(linearLayout);
        addView(this.d);
    }

    public final void a(String str) {
        this.d.setText(str);
    }
}
