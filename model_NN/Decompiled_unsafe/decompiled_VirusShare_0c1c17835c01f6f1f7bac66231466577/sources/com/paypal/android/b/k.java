package com.paypal.android.b;

import android.view.animation.Animation;

public final class k extends Thread {
    private j a;
    private Animation b;

    public k(j jVar, Animation animation) {
        this.a = jVar;
        this.b = animation;
    }

    public final void run() {
        this.a.startAnimation(this.b);
    }
}
