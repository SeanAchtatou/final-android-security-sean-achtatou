package com.paypal.android.b;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.paypal.android.a.h;

public final class d extends LinearLayout {
    private AnimationDrawable a;

    public d(Context context) {
        super(context);
        setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        setPadding(10, 10, 10, 10);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(52, 29));
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        AnimationDrawable animationDrawable = new AnimationDrawable();
        animationDrawable.addFrame(h.a(38862, 5362), 50);
        animationDrawable.addFrame(h.a(171068, 5447), 50);
        animationDrawable.addFrame(h.a(106633, 5446), 50);
        animationDrawable.addFrame(h.a(70013, 5475), 50);
        animationDrawable.addFrame(h.a(20096, 5400), 50);
        animationDrawable.addFrame(h.a(149433, 5270), 50);
        animationDrawable.addFrame(h.a(92990, 5423), 50);
        animationDrawable.addFrame(h.a(55698, 5485), 50);
        animationDrawable.addFrame(h.a(7623, 5470), 50);
        animationDrawable.addFrame(h.a(127767, 5428), 50);
        animationDrawable.addFrame(h.a(119431, 5415), 50);
        animationDrawable.addFrame(h.a(80891, 5367), 50);
        animationDrawable.addFrame(h.a(33070, 5362), 50);
        animationDrawable.addFrame(h.a(163774, 5399), 50);
        animationDrawable.addFrame(h.a(100757, 5412), 50);
        animationDrawable.addFrame(h.a(64110, 5470), 50);
        animationDrawable.addFrame(h.a(14681, 5415), 50);
        animationDrawable.addFrame(h.a(134029, 5357), 50);
        animationDrawable.addFrame(h.a(86845, 5303), 50);
        animationDrawable.addFrame(h.a(50173, 5525), 50);
        animationDrawable.addFrame(h.a(44224, 5524), 50);
        animationDrawable.addFrame(h.a(0, 5487), 50);
        animationDrawable.addFrame(h.a(113975, 5456), 50);
        animationDrawable.addFrame(h.a(75488, 5403), 50);
        animationDrawable.addFrame(h.a(27818, 5252), 50);
        animationDrawable.setOneShot(false);
        animationDrawable.setVisible(true, true);
        this.a = animationDrawable;
        imageView.setBackgroundDrawable(this.a);
        linearLayout.addView(imageView);
        addView(linearLayout);
    }

    public final void a() {
        this.a.stop();
        this.a.start();
    }

    public final void b() {
        this.a.stop();
    }
}
