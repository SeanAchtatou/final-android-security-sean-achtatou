package com.paypal.android.MEP;

public final class m {
    private static m a = null;

    private m() {
    }

    public static m a() {
        if (a == null) {
            if (a != null) {
                throw new IllegalStateException("Attempted to initialize PPMobileAPIInterface more than once.");
            }
            a = new m();
        }
        return a;
    }

    public final void a(b bVar) {
        new a(this, bVar).start();
    }

    public final void a(b bVar, String str, String str2) {
        a aVar = new a(this, bVar);
        bVar.a("usernameOrPhone", str);
        bVar.a("passwordOrPin", str2);
        aVar.start();
    }

    public final void b(b bVar, String str, String str2) {
        a aVar = new a(this, bVar);
        bVar.a("mobileNumber", str);
        bVar.a("newPIN", str2);
        aVar.start();
    }
}
