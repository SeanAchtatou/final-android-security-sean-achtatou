package com.paypal.android.MEP;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class i implements Serializable {
    private String a = null;
    private ArrayList b = new ArrayList();
    private String c = null;
    private String d = null;
    private String e = null;

    public String a() {
        return this.a;
    }

    public void a(String str) {
        this.a = str;
    }

    public ArrayList b() {
        return this.b;
    }

    public void b(String str) {
        this.c = str;
    }

    public String c() {
        return this.c;
    }

    public void c(String str) {
        this.d = str;
    }

    public String d() {
        return this.d;
    }

    public boolean e() {
        if (this.b.size() <= 0) {
            return false;
        }
        for (int i = 0; i < this.b.size(); i++) {
            if (((f) this.b.get(i)).a() == null || ((f) this.b.get(i)).a().length() <= 0 || ((f) this.b.get(i)).b().compareTo(BigDecimal.ZERO) <= 0) {
                return false;
            }
        }
        return (!this.a.equals("AUD") || h().compareTo(new BigDecimal("11000")) < 0) ? (!this.a.equals("CAD") || h().compareTo(new BigDecimal("11000")) < 0) ? (!this.a.equals("CHF") || h().compareTo(new BigDecimal("11000")) < 0) ? (!this.a.equals("CZK") || h().compareTo(new BigDecimal("200000")) < 0) ? (!this.a.equals("DKK") || h().compareTo(new BigDecimal("55000")) < 0) ? (!this.a.equals("EUR") || h().compareTo(new BigDecimal("7500")) < 0) ? (!this.a.equals("GBP") || h().compareTo(new BigDecimal("7500")) < 0) ? (!this.a.equals("HKD") || h().compareTo(new BigDecimal("80000")) < 0) ? (!this.a.equals("HUF") || h().compareTo(new BigDecimal("2000000")) < 0) ? (!this.a.equals("ILS") || h().compareTo(new BigDecimal("40000")) < 0) ? (!this.a.equals("JPY") || h().compareTo(new BigDecimal("1000000")) < 0) ? (!this.a.equals("MXN") || h().compareTo(new BigDecimal("130000")) < 0) ? (!this.a.equals("MYR") || h().compareTo(new BigDecimal("35000")) < 0) ? (!this.a.equals("NOK") || h().compareTo(new BigDecimal("60000")) < 0) ? (!this.a.equals("NZD") || h().compareTo(new BigDecimal("15000")) < 0) ? (!this.a.equals("PHP") || h().compareTo(new BigDecimal("500000")) < 0) ? (!this.a.equals("PLN") || h().compareTo(new BigDecimal("30000")) < 0) ? (!this.a.equals("SEK") || h().compareTo(new BigDecimal("75000")) < 0) ? (!this.a.equals("SGD") || h().compareTo(new BigDecimal("15000")) < 0) ? (!this.a.equals("THB") || h().compareTo(new BigDecimal("350000")) < 0) ? (!this.a.equals("TWD") || h().compareTo(new BigDecimal("350000")) < 0) ? !this.a.equals("USD") || h().compareTo(new BigDecimal("10000")) < 0 : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false;
    }

    public boolean f() {
        for (int i = 0; i < this.b.size(); i++) {
            if (((f) this.b.get(i)).i()) {
                return true;
            }
        }
        return false;
    }

    public f g() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.b.size()) {
                return null;
            }
            if (((f) this.b.get(i2)).i()) {
                return (f) this.b.get(i2);
            }
            i = i2 + 1;
        }
    }

    public BigDecimal h() {
        BigDecimal bigDecimal = BigDecimal.ZERO;
        int i = 0;
        while (true) {
            BigDecimal bigDecimal2 = bigDecimal;
            if (i >= this.b.size()) {
                return bigDecimal2;
            }
            bigDecimal = bigDecimal2.add(((f) this.b.get(i)).l());
            i++;
        }
    }

    public BigDecimal i() {
        BigDecimal bigDecimal = BigDecimal.ZERO;
        int i = 0;
        while (true) {
            BigDecimal bigDecimal2 = bigDecimal;
            if (i >= this.b.size()) {
                return bigDecimal2;
            }
            bigDecimal = (((f) this.b.get(i)).c() == null || ((f) this.b.get(i)).c().a() == null) ? bigDecimal2 : bigDecimal2.add(((f) this.b.get(i)).c().a());
            i++;
        }
    }

    public BigDecimal j() {
        BigDecimal bigDecimal = BigDecimal.ZERO;
        int i = 0;
        while (true) {
            BigDecimal bigDecimal2 = bigDecimal;
            if (i >= this.b.size()) {
                return bigDecimal2;
            }
            bigDecimal = (((f) this.b.get(i)).c() == null || ((f) this.b.get(i)).c().b() == null) ? bigDecimal2 : bigDecimal2.add(((f) this.b.get(i)).c().b());
            i++;
        }
    }

    public String k() {
        return this.e;
    }
}
