package com.paypal.android.MEP.b;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.MEP.a.h;
import com.paypal.android.MEP.a.k;
import com.paypal.android.MEP.a.l;
import com.paypal.android.MEP.f;
import com.paypal.android.MEP.i;
import com.paypal.android.MEP.o;
import com.paypal.android.a.d;
import com.paypal.android.b.c;
import com.paypal.android.b.e;
import com.paypal.android.b.j;
import java.math.BigDecimal;
import java.util.ArrayList;

public final class a extends e implements View.OnTouchListener {
    private c e;
    private boolean f;
    private GradientDrawable g;
    private GradientDrawable h;

    public a(Context context, j jVar) {
        super(context);
        o a = o.a();
        i c = a.c();
        com.paypal.android.MEP.c d = a.d();
        boolean z = jVar instanceof h;
        boolean z2 = (jVar instanceof l) || (jVar instanceof k);
        a(new LinearLayout.LayoutParams(-1, -2), 0);
        a(new LinearLayout.LayoutParams(-1, -2), 1);
        this.g = com.paypal.android.a.i.a(-1, -1510918, -7829368);
        this.h = com.paypal.android.a.i.a(-1, 14209956, -7829368);
        setBackgroundDrawable(this.g);
        setPadding(5, 5, 5, 0);
        a(com.paypal.android.a.h.a(14218, 463));
        b(com.paypal.android.a.h.a(38432, 430));
        setOnTouchListener(this);
        if (a.z() != 3) {
            this.f = c.j().compareTo(BigDecimal.ZERO) > 0 || c.i().compareTo(BigDecimal.ZERO) > 0 || a.o() || c.f() || c.b().size() > 1;
            if (!(((f) c.b().get(0)).c() == null || ((f) c.b().get(0)).c().c() == null)) {
                for (int i = 0; i < ((f) c.b().get(0)).c().c().size(); i++) {
                    if (((com.paypal.android.MEP.e) ((f) c.b().get(0)).c().c().get(i)).f()) {
                        this.f = true;
                    }
                }
            }
            this.a.setGravity(16);
            this.a.addView(z2 ? com.paypal.android.a.h.a(context, "system-icon-confirmation.png") : (a.B() || a.l() == 1) ? com.paypal.android.a.h.a(context, "shopping-list-enabled.png") : com.paypal.android.a.h.a(context, "shopping-cart-enabled.png"));
            this.e = new c(context, com.paypal.android.a.j.HELVETICA_14_NORMAL, com.paypal.android.a.j.HELVETICA_14_BOLD, 0.5f, 0.5f);
            if (z2) {
                if (a.B() || a.l() == 1) {
                    this.e.a(d.a("ANDROID_total_paid") + ":");
                } else {
                    this.e.a(d.a("ANDROID_receipt") + ":");
                }
            } else if (this.f) {
                this.e.a(d.a("ANDROID_my_total") + ":");
            } else {
                String f2 = ((f) c.b().get(0)).f();
                if (f2 == null || f2.length() <= 0) {
                    this.e.a(d.a("ANDROID_my_total") + ":");
                } else {
                    this.e.a(f2 + ":");
                }
            }
            this.e.a(-16777216);
            if (c.f()) {
                this.e.b(com.paypal.android.a.a.a(c.g().l(), c.a()));
            } else {
                this.e.b(com.paypal.android.a.a.a(c.h(), c.a()));
            }
            this.e.b(-13408768);
            this.e.setPadding(5, 0, 5, 0);
            this.a.addView(this.e);
            this.a.addView(this.c);
            this.c.setVisibility(0);
            if (this.f) {
                this.b.setOrientation(1);
                View view = new View(context);
                view.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
                view.setBackgroundColor(-7829368);
                this.b.addView(view);
                if (c.f()) {
                    this.b.addView(b.a(context, c.g(), c.a()));
                } else {
                    ArrayList b = c.b();
                    if (b.size() == 1) {
                        this.b.addView(b.a(context, (f) b.get(0), c.a()));
                    } else {
                        for (int i2 = 0; i2 < b.size(); i2++) {
                            this.b.addView(new b(context, (f) b.get(i2), c.a()));
                        }
                    }
                }
                View view2 = new View(context);
                view2.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
                view2.setBackgroundColor(-7829368);
                this.b.addView(view2);
                TextView a2 = com.paypal.android.a.e.a(com.paypal.android.a.j.HELVETICA_14_BOLD, context);
                a2.setGravity(5);
                String a3 = c.f() ? com.paypal.android.a.a.a(c.g().l(), c.a()) : com.paypal.android.a.a.a(c.h().toString(), c.a());
                if (a3.contains(c.a())) {
                    a2.setText(d.a("ANDROID_total") + ": " + a3);
                } else {
                    a2.setText(d.a("ANDROID_total") + " (" + c.a() + "): " + a3);
                }
                this.b.addView(a2);
                TextView a4 = com.paypal.android.a.e.a(com.paypal.android.a.j.HELVETICA_12_NORMAL, context);
                a4.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                a4.setGravity(5);
                a4.setText(d.a("ANDROID_shipping_tax_estimated_note"));
                if (a.o() && z) {
                    this.b.addView(a4);
                    return;
                }
                return;
            }
            setClickable(false);
            return;
        }
        this.f = true;
        this.a.setGravity(16);
        this.a.addView(z2 ? com.paypal.android.a.h.a(context, "system-icon-confirmation.png") : com.paypal.android.a.h.a(context, "shopping-list-enabled.png"));
        this.e = new c(context, com.paypal.android.a.j.HELVETICA_14_NORMAL, com.paypal.android.a.j.HELVETICA_14_BOLD, 0.5f, 0.5f);
        if (z2) {
            this.e.a(d.a("ANDROID_receipt") + ":");
            this.e.b("");
        } else {
            this.e.a(d.a("ANDROID_billing_summary"));
            this.e.b("");
        }
        this.e.a(-16777216);
        this.e.b(-13408768);
        this.a.addView(this.e);
        this.a.addView(this.c);
        this.c.setVisibility(0);
        com.paypal.android.MEP.c d2 = a.d();
        if (!z) {
            this.b.setOrientation(1);
            View view3 = new View(context);
            view3.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
            view3.setBackgroundColor(-7829368);
            this.b.addView(view3);
            c cVar = new c(context, com.paypal.android.a.j.HELVETICA_12_BOLD, com.paypal.android.a.j.HELVETICA_12_NORMAL);
            cVar.a(d.a("ANDROID_name") + ":");
            cVar.b(d.a());
            this.b.addView(cVar);
            if (d2.b() != null && d2.b().length() > 0) {
                c cVar2 = new c(context, com.paypal.android.a.j.HELVETICA_12_BOLD, com.paypal.android.a.j.HELVETICA_12_NORMAL);
                cVar2.a(d.a("ANDROID_start_date") + ":");
                cVar2.b(d.a(d2.b(), 2));
                this.b.addView(cVar2);
            }
            if (d2.c() != null && d2.c().length() > 0) {
                c cVar3 = new c(context, com.paypal.android.a.j.HELVETICA_12_BOLD, com.paypal.android.a.j.HELVETICA_12_NORMAL);
                cVar3.a(d.a("ANDROID_end_date") + ":");
                cVar3.b(d.a(d2.c(), 2));
                this.b.addView(cVar3);
            }
        }
    }

    public final void a(boolean z, boolean z2) {
        if (!z || !this.f) {
            setFocusable(false);
            setClickable(false);
            this.b.setVisibility(8);
            this.c.setVisibility(8);
            if (z2) {
                this.e.a(-7829368);
                this.e.b(-7829368);
                return;
            }
            return;
        }
        setFocusable(true);
        setClickable(true);
        this.c.setVisibility(0);
        this.e.a(-16777216);
        this.e.b(-13408768);
    }

    public final void b(boolean z) {
        if (z) {
            setBackgroundDrawable(this.h);
        } else {
            setBackgroundDrawable(this.g);
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (!isFocusable()) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                setBackgroundDrawable(this.h);
                break;
            default:
                setBackgroundDrawable(this.g);
                break;
        }
        return false;
    }
}
