package com.paypal.android.MEP;

import java.io.Serializable;
import java.math.BigDecimal;

public class c implements Serializable {
    private String a = null;
    private String b = null;
    private int c = 0;
    private BigDecimal d = null;
    private BigDecimal e = null;
    private String f = null;
    private String g = null;
    private boolean h = false;
    private boolean i = false;
    private int j = 6;
    private int k = 7;
    private int l = 0;
    private int m = 0;
    private String n = null;
    private String o = null;
    private int p = 1;

    public String a() {
        return this.b;
    }

    public void a(int i2) {
        this.c = i2;
    }

    public void a(String str) {
        this.a = str;
    }

    public void a(BigDecimal bigDecimal) {
        this.d = bigDecimal;
    }

    public void a(boolean z) {
        this.h = z;
    }

    public String b() {
        return this.f;
    }

    public void b(int i2) {
        this.j = i2;
    }

    public void b(String str) {
        this.f = str;
    }

    public void b(BigDecimal bigDecimal) {
        this.e = bigDecimal;
    }

    public void b(boolean z) {
        this.i = z;
    }

    public String c() {
        return this.g;
    }

    public void c(int i2) {
        this.k = i2;
    }

    public void c(String str) {
        this.g = str;
    }

    public void d(int i2) {
        this.l = i2;
    }

    public void d(String str) {
        this.n = str;
    }

    public boolean d() {
        return this.i;
    }

    public void e(int i2) {
        this.m = i2;
    }

    public void e(String str) {
        this.o = str;
    }

    public boolean e() {
        return this.b != null && this.b.length() > 0;
    }

    public int f() {
        return this.p;
    }

    public int f(String str) {
        if (str.equals("DAILY")) {
            return 0;
        }
        if (str.equals("WEEKLY")) {
            return 1;
        }
        if (str.equals("BIWEEKLY")) {
            return 2;
        }
        if (str.equals("SEMIMONTHLY")) {
            return 3;
        }
        if (str.equals("MONTHLY")) {
            return 4;
        }
        return str.equals("ANNUALLY") ? 5 : 6;
    }

    public int g(String str) {
        if (str.equals("SUNDAY")) {
            return 0;
        }
        if (str.equals("MONDAY")) {
            return 1;
        }
        if (str.equals("TUESDAY")) {
            return 2;
        }
        if (str.equals("WEDNESDAY")) {
            return 3;
        }
        if (str.equals("THURSDAY")) {
            return 4;
        }
        if (str.equals("FRIDAY")) {
            return 5;
        }
        return str.equals("SATURDAY") ? 6 : 7;
    }
}
