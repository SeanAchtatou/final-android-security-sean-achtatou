package com.paypal.android.MEP;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class d implements Serializable {
    private BigDecimal a = null;
    private BigDecimal b = null;
    private ArrayList c = new ArrayList();

    public BigDecimal a() {
        return this.a;
    }

    public void a(BigDecimal bigDecimal) {
        this.a = bigDecimal.setScale(2, 4);
    }

    public BigDecimal b() {
        return this.b;
    }

    public void b(BigDecimal bigDecimal) {
        this.b = bigDecimal.setScale(2, 4);
    }

    public ArrayList c() {
        return this.c;
    }
}
