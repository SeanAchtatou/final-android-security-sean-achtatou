package com.paypal.android.MEP.a;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.b;
import com.paypal.android.MEP.b.a;
import com.paypal.android.MEP.b.f;
import com.paypal.android.MEP.c;
import com.paypal.android.MEP.o;
import com.paypal.android.a.a.g;
import com.paypal.android.a.e;
import com.paypal.android.b.d;
import com.paypal.android.b.i;
import com.paypal.android.b.j;
import com.paypal.android.b.m;
import com.paypal.android.b.p;
import java.util.Hashtable;
import java.util.Vector;

public final class h extends j implements TextWatcher, View.OnClickListener, b, p {
    public static boolean a = false;
    private static d l = null;
    private p b;
    private Button c;
    private Button d;
    private Button e;
    private TextView f;
    private a g;
    private m h;
    private m i;
    private LinearLayout j;
    private RelativeLayout k;
    private TextView m;
    private f n;
    private i o;
    /* access modifiers changed from: private */
    public WebView p;
    private String q;
    private Hashtable r = new Hashtable();

    public h(Context context) {
        super(context);
    }

    private void e() {
        String a2 = this.n.a();
        String b2 = this.n.b();
        if (a2.contains("@")) {
            o.a().e(a2);
            o.a().b(0);
        } else {
            o.a().f(a2);
            o.a().b(1);
        }
        boolean z = com.paypal.android.a.d.d(a2) || com.paypal.android.a.d.e(a2);
        boolean z2 = b2 != null && b2.length() > 0;
        if (z && z2) {
            this.n.d().setText("");
            a(p.STATE_LOGGING_IN);
            com.paypal.android.MEP.m.a().a(this, a2, b2);
        }
    }

    private boolean f() {
        String a2 = this.n.a();
        String b2 = this.n.b();
        return (com.paypal.android.a.d.d(a2) || com.paypal.android.a.d.e(a2)) && (b2 != null && b2.length() > 0);
    }

    public final void a() {
        PayPalActivity.a.a("usernameOrPhone", this.r.get("usernameOrPhone"));
        PayPalActivity.a.a("passwordOrPin", this.r.get("passwordOrPin"));
        PayPalActivity.a.a("delegate", this);
        PayPalActivity.a.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.paypal.android.a.f.a(com.paypal.android.a.f, int):int
      com.paypal.android.a.f.a(java.lang.String, java.lang.String):boolean
      com.paypal.android.a.f.a(int, java.lang.Object):void
      com.paypal.android.MEP.b.a(int, java.lang.Object):void
      com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void */
    public final void a(int i2, Object obj) {
        com.paypal.android.a.f fVar = PayPalActivity.a;
        if (this.b == p.STATE_LOGGING_IN) {
            switch (i2) {
                case 0:
                case 10:
                    fVar.a("currentUser", (Object) this.n.a());
                    if (o.a().z() == 3) {
                        fVar.a("PreapprovalKey", (Object) o.a().g());
                        fVar.a("delegate", fVar);
                        PayPalActivity.a.a(13);
                        return;
                    }
                    fVar.f();
                    return;
                default:
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.b.a.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.paypal.android.b.f.a(android.widget.LinearLayout$LayoutParams, int):void
      com.paypal.android.MEP.b.a.a(boolean, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.paypal.android.a.f.a(com.paypal.android.a.f, int):int
      com.paypal.android.a.f.a(java.lang.String, java.lang.String):boolean
      com.paypal.android.a.f.a(int, java.lang.Object):void
      com.paypal.android.MEP.b.a(int, java.lang.Object):void
      com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void */
    /* access modifiers changed from: protected */
    public final void a(Context context) {
        o a2 = o.a();
        super.a(context);
        this.b = p.STATE_NORMAL;
        LinearLayout a3 = com.paypal.android.a.i.a(context, -1, -2);
        a3.setOrientation(1);
        a3.setPadding(5, 5, 5, 15);
        a3.addView(e.b(com.paypal.android.a.j.HELVETICA_16_BOLD, context));
        this.g = new a(context, this);
        this.g.a(this);
        a3.addView(this.g);
        if (a2.z() == 3) {
            this.g.a(false, true);
        } else if (!a2.E()) {
            this.g.a(false, false);
        }
        addView(a3);
        this.j = new LinearLayout(context);
        this.j.setOrientation(1);
        this.j.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.j.setBackgroundDrawable(com.paypal.android.a.i.a());
        this.j.setPadding(10, 5, 10, 5);
        this.j.addView(new com.paypal.android.b.h(com.paypal.android.a.d.a("ANDROID_login"), context));
        this.h = new m(context, com.paypal.android.b.o.RED_ALERT);
        this.h.a("Placeholder");
        this.h.setVisibility(8);
        this.h.setPadding(0, 5, 0, 5);
        this.j.addView(this.h);
        this.i = new m(context, com.paypal.android.b.o.BLUE_ALERT);
        this.i.a(com.paypal.android.a.d.a("ANDROID_not_you_message"));
        this.i.setVisibility(8);
        this.i.setPadding(0, 5, 0, 5);
        this.j.addView(this.i);
        this.n = new f(context);
        this.n.c().addTextChangedListener(this);
        this.n.d().addTextChangedListener(this);
        this.j.addView(this.n);
        LinearLayout a4 = com.paypal.android.a.i.a(context, -1, -2);
        a4.setOrientation(0);
        a4.setGravity(16);
        a4.setPadding(5, 5, 5, 0);
        if (o.a().t() == 0 && o.a().s() == 2) {
            o.a().b(0);
        }
        this.o = new i(context);
        this.o.setChecked(o.a().v());
        this.o.setOnClickListener(this);
        if (o.a().t() == 1) {
            a4.addView(this.o);
        }
        this.p = new WebView(context);
        this.p.setWebViewClient(new o(this));
        this.p.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.p.setBackgroundColor(0);
        this.p.loadData("<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><head><style type=\"text/css\">b {color:#1B3664; font-family:Helvetica; font-size:12;}a {color:#686868; font-family:Helvetica; font-size:12;}</style></head><body><b>" + com.paypal.android.a.d.a("ANDROID_checkbox_opt_in") + "</b>" + "  " + "<a href=\"About.Quick.Pay\">" + com.paypal.android.a.d.a("ANDROID_checkbox_whats_this") + "</a>" + "</body>" + "</html>", "text/html", "utf-8");
        if (o.a().t() == 1) {
            a4.addView(this.p);
        }
        this.j.addView(a4);
        LinearLayout a5 = com.paypal.android.a.i.a(context, -1, -2);
        a5.setPadding(5, 5, 5, 5);
        a5.setOrientation(0);
        a5.setGravity(1);
        if (a2.z() == 3 || a2.n() || a2.B()) {
            this.d = null;
            this.e = new Button(context);
            this.e.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.i.b()));
            this.e.setId(184424834);
            this.e.setText(com.paypal.android.a.d.a("ANDROID_login"));
            this.e.setTextColor(-16777216);
            this.e.setBackgroundDrawable(com.paypal.android.a.h.a());
            this.e.setOnClickListener(this);
            this.e.setEnabled(false);
            a5.addView(this.e);
        } else {
            LinearLayout a6 = com.paypal.android.a.i.a(context, -1, -2);
            a6.setOrientation(1);
            a6.setPadding(5, 0, 5, 5);
            TextView a7 = e.a(com.paypal.android.a.j.HELVETICA_10_NORMAL, context);
            a7.setTextColor(-16777216);
            a7.setText(com.paypal.android.a.d.a("ANDROID_review_text"));
            a7.setGravity(3);
            a6.addView(a7);
            this.j.addView(a6);
            this.d = new Button(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, com.paypal.android.a.i.b(), 0.5f);
            layoutParams.setMargins(0, 0, 5, 0);
            this.d.setLayoutParams(layoutParams);
            if (a2.l() == 1) {
                this.d.setText(com.paypal.android.a.d.a("ANDROID_donate"));
            } else {
                this.d.setText(com.paypal.android.a.d.a("ANDROID_pay"));
            }
            this.d.setTextColor(-16777216);
            this.d.setBackgroundDrawable(com.paypal.android.a.h.a());
            this.d.setOnClickListener(this);
            this.d.setEnabled(false);
            a5.addView(this.d);
            this.e = new Button(context);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, com.paypal.android.a.i.b(), 0.5f);
            layoutParams2.setMargins(5, 0, 0, 0);
            this.e.setLayoutParams(layoutParams2);
            this.e.setId(184424834);
            this.e.setText(com.paypal.android.a.d.a("ANDROID_review"));
            this.e.setTextColor(-16777216);
            this.e.setBackgroundDrawable(com.paypal.android.a.h.b());
            this.e.setOnClickListener(this);
            this.e.setEnabled(false);
            a5.addView(this.e);
        }
        this.j.addView(a5);
        LinearLayout a8 = com.paypal.android.a.i.a(context, -1, -2);
        a8.setPadding(5, 5, 5, 5);
        a8.setOrientation(0);
        a8.setGravity(1);
        this.c = new Button(context);
        this.c.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.i.b()));
        this.c.setText(com.paypal.android.a.d.a("ANDROID_cancel"));
        this.c.setTextColor(-16777216);
        this.c.setBackgroundDrawable(com.paypal.android.a.h.b());
        this.c.setOnClickListener(this);
        a8.addView(this.c);
        this.j.addView(a8);
        this.f = e.a(com.paypal.android.a.j.HELVETICA_12_NORMAL, context);
        this.f.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.f.setPadding(10, 10, 10, 10);
        this.f.setTextColor(-16776961);
        this.f.setGravity(17);
        this.f.setOnClickListener(this);
        SpannableString spannableString = new SpannableString(com.paypal.android.a.d.a("ANDROID_help"));
        spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), 0);
        this.f.setText(spannableString);
        this.f.setFocusable(true);
        this.j.addView(this.f);
        this.j.invalidate();
        addView(this.j);
        this.k = new RelativeLayout(context);
        this.k.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.k.setBackgroundDrawable(com.paypal.android.a.i.a());
        LinearLayout a9 = com.paypal.android.a.i.a(context, -1, -2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(a9.getLayoutParams());
        layoutParams3.addRule(13);
        a9.setLayoutParams(layoutParams3);
        a9.setOrientation(1);
        a9.setGravity(1);
        if (l == null) {
            l = new d(context);
        } else {
            ((LinearLayout) l.getParent()).removeAllViews();
        }
        this.m = e.a(com.paypal.android.a.j.HELVETICA_16_NORMAL, context);
        this.m.setGravity(1);
        this.m.setTextColor(-13408615);
        this.m.setText(com.paypal.android.a.d.a("ANDROID_logging_in_message"));
        a9.addView(l);
        a9.addView(this.m);
        this.k.addView(a9);
        this.k.setVisibility(8);
        addView(this.k);
        if (o.a().t() == 0 && o.a().s() == 2) {
            o.a().b(0);
        }
        if (a2.s() == 2) {
            a(p.STATE_LOGGING_IN);
            this.n.d().setText("");
            PayPalActivity.a.a("delegate", this);
            PayPalActivity.a.a("quickPay", (Object) "false");
            PayPalActivity.a.a(10);
        }
        if (a) {
            a(p.STATE_ERROR);
        }
    }

    public final void a(p pVar) {
        this.b = pVar;
        q.b();
    }

    public final void a(com.paypal.android.b.f fVar, int i2) {
    }

    public final void a(String str) {
        a = false;
        this.q = str;
        if (this.b == p.STATE_LOGGING_IN) {
            a(p.STATE_ERROR);
            o.a().b(0);
        } else if (this.b == p.STATE_LOGGING_OUT) {
            a(p.STATE_ERROR);
        }
    }

    public final void a(String str, Object obj) {
        this.r.put(str, obj);
    }

    public final void afterTextChanged(Editable editable) {
        if (this.n.c().getText().length() <= 0 || this.n.d().getText().length() <= 0) {
            if (this.e != null) {
                this.e.setEnabled(false);
            }
            if (this.d != null) {
                this.d.setEnabled(false);
            }
        } else {
            if (this.e != null) {
                this.e.setEnabled(true);
            }
            if (this.d != null) {
                this.d.setEnabled(true);
            }
        }
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.b.a.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.paypal.android.b.f.a(android.widget.LinearLayout$LayoutParams, int):void
      com.paypal.android.MEP.b.a.a(boolean, boolean):void */
    public final void b() {
        if (this.b == p.STATE_LOGGING_IN || this.b == p.STATE_LOGGING_OUT) {
            this.g.a(false, true);
            this.j.setVisibility(8);
            this.k.setVisibility(0);
            l.a();
        } else if (this.b == p.STATE_NORMAL || this.b == p.STATE_ERROR) {
            if (o.a().E()) {
                this.g.a(true, false);
            }
            l.b();
            this.k.setVisibility(8);
            this.j.setVisibility(0);
            if (this.b != p.STATE_ERROR) {
                return;
            }
            if (a) {
                this.i.setVisibility(0);
                this.h.setVisibility(8);
                return;
            }
            this.i.setVisibility(8);
            this.h.setVisibility(0);
            this.h.a(this.q);
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    public final void c() {
        b();
    }

    public final p d() {
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.PayPalActivity.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.MEP.PayPalActivity.a(com.paypal.android.MEP.k, java.lang.String, java.util.Vector):java.util.Vector
      com.paypal.android.MEP.PayPalActivity.a(java.lang.String, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.paypal.android.a.f.a(com.paypal.android.a.f, int):int
      com.paypal.android.a.f.a(java.lang.String, java.lang.String):boolean
      com.paypal.android.a.f.a(int, java.lang.Object):void
      com.paypal.android.MEP.b.a(int, java.lang.Object):void
      com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void */
    public final void onClick(View view) {
        if (view == this.o) {
            o.a().e(this.o.isChecked());
        } else if (view == this.p) {
            q.a(1);
        } else if (view == this.d) {
            if (f()) {
                a(p.STATE_NORMAL);
                this.n.e();
                if (o.a().k() == 2) {
                    PayPalActivity.a().a("27892", (String) PayPalActivity.a.d("PaymentExecStatus"), true);
                    return;
                }
                PayPalActivity.a.a("quickPay", (Object) "true");
                e();
            }
        } else if (view == this.e) {
            if (f()) {
                a(p.STATE_NORMAL);
                this.n.e();
                if (o.a().k() == 2) {
                    if (o.a().z() == 3) {
                        c d2 = o.a().d();
                        d2.b("2011-07-06T23:59:49.000-07:00");
                        d2.c("2011-08-07T23:59:49.000-07:00");
                        d2.b(true);
                    }
                    m.a = new Hashtable();
                    Vector vector = new Vector();
                    Vector vector2 = new Vector();
                    g gVar = new g();
                    gVar.a("0");
                    gVar.a = new com.paypal.android.a.a.e();
                    gVar.a.b("USD");
                    gVar.a.a("2.00");
                    gVar.d = new Vector();
                    com.paypal.android.a.a.f fVar = new com.paypal.android.a.a.f();
                    fVar.a = new com.paypal.android.a.a.e();
                    fVar.a.b("USD");
                    fVar.a.a("2.00");
                    fVar.b = new com.paypal.android.a.a.b();
                    fVar.b.a("2093");
                    fVar.b.b("BANK_INSTANT");
                    gVar.d.add(fVar);
                    vector.add(gVar);
                    g gVar2 = new g();
                    gVar2.a("1");
                    gVar2.a = new com.paypal.android.a.a.e();
                    gVar2.a.b("USD");
                    gVar2.a.a("2.00");
                    gVar2.d = new Vector();
                    com.paypal.android.a.a.f fVar2 = new com.paypal.android.a.a.f();
                    fVar2.a = new com.paypal.android.a.a.e();
                    fVar2.a.b("USD");
                    fVar2.a.a("2.00");
                    fVar2.b = new com.paypal.android.a.a.b();
                    fVar2.b.a("9853");
                    fVar2.b.b("CREDITCARD");
                    gVar2.d.add(fVar2);
                    vector.add(gVar2);
                    g gVar3 = new g();
                    gVar3.a("2");
                    gVar3.a = new com.paypal.android.a.a.e();
                    gVar3.a.b("USD");
                    gVar3.a.a("2.00");
                    gVar3.d = new Vector();
                    com.paypal.android.a.a.f fVar3 = new com.paypal.android.a.a.f();
                    fVar3.a = new com.paypal.android.a.a.e();
                    fVar3.a.b("USD");
                    fVar3.a.a("2.00");
                    fVar3.b = new com.paypal.android.a.a.b();
                    fVar3.b.a("9691");
                    fVar3.b.b("CREDITCARD");
                    gVar3.d.add(fVar3);
                    vector.add(gVar3);
                    m.a.put("FundingPlanId", "0");
                    m.a.put("FundingPlans", vector);
                    com.paypal.android.a.a.i iVar = new com.paypal.android.a.a.i();
                    iVar.b("Trenton");
                    iVar.d("123 Home St");
                    iVar.e("Apt B");
                    iVar.f("08601");
                    iVar.g("NJ");
                    iVar.h("1");
                    iVar.c("US");
                    vector2.add(iVar);
                    com.paypal.android.a.a.i iVar2 = new com.paypal.android.a.a.i();
                    iVar2.b("Hamlin");
                    iVar2.d("3012 Church Rd");
                    iVar2.e("");
                    iVar2.f("14464");
                    iVar2.g("NY");
                    iVar2.h("2");
                    iVar2.c("US");
                    vector2.add(iVar2);
                    m.a.put("ShippingAddressId", "1");
                    m.a.put("AvailableAddresses", vector2);
                    PayPalActivity.a().sendBroadcast(new Intent(PayPalActivity.d));
                    return;
                }
                PayPalActivity.a.a("quickPay", (Object) "false");
                e();
            }
        } else if (view == this.c) {
            new com.paypal.android.MEP.b.e(PayPalActivity.a()).show();
        } else if (view == this.f) {
            q.a(2);
        }
    }

    public final void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }
}
