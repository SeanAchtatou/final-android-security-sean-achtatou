package com.paypal.android.MEP;

import java.io.Serializable;
import java.math.BigDecimal;

public class q implements Serializable {
    private String a = null;
    private String b = null;
    private BigDecimal c = null;
    private d d = null;
    private int e = 3;
    private int f = 22;
    private String g = null;
    private String h = null;
    private String i = null;
    private String j = null;
    private String k = null;

    public String a() {
        return this.a;
    }

    public void a(int i2) {
        this.e = i2;
    }

    public void a(d dVar) {
        this.d = dVar;
    }

    public void a(String str) {
        this.a = str;
    }

    public void a(BigDecimal bigDecimal) {
        this.c = bigDecimal.setScale(2, 4);
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.b = str;
    }

    public BigDecimal c() {
        return this.c;
    }

    public void c(String str) {
        this.g = str;
    }

    public d d() {
        return this.d;
    }

    public void d(String str) {
        this.h = str;
    }

    public int e() {
        return this.e;
    }

    public void e(String str) {
        this.i = str;
    }

    public int f() {
        return this.f;
    }

    public void f(String str) {
        this.k = str;
    }

    public String g() {
        return this.g;
    }

    public String h() {
        return this.h;
    }

    public String i() {
        return this.i;
    }

    public String j() {
        return this.j;
    }

    public String k() {
        return this.k;
    }
}
