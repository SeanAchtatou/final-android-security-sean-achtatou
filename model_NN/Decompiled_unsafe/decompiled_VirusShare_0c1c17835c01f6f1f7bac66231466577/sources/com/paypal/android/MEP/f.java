package com.paypal.android.MEP;

import com.paypal.android.a.d;
import java.io.Serializable;
import java.math.BigDecimal;

public class f implements Serializable {
    private String a = null;
    private BigDecimal b = null;
    private d c = null;
    private int d = 3;
    private int e = 22;
    private String f = null;
    private String g = null;
    private String h = null;
    private boolean i = false;

    public String a() {
        return this.a;
    }

    public void a(int i2) {
        this.d = i2;
    }

    public void a(d dVar) {
        this.c = dVar;
    }

    public void a(String str) {
        this.a = str;
    }

    public void a(BigDecimal bigDecimal) {
        this.b = bigDecimal.setScale(2, 4);
    }

    public void a(boolean z) {
        this.i = z;
    }

    public BigDecimal b() {
        return this.b;
    }

    public void b(int i2) {
        this.e = i2;
    }

    public void b(String str) {
        this.f = str;
    }

    public d c() {
        return this.c;
    }

    public void c(String str) {
        this.g = str;
    }

    public int d() {
        return this.d;
    }

    public void d(String str) {
        this.h = str;
    }

    public int e() {
        return this.e;
    }

    public String f() {
        return this.f;
    }

    public String g() {
        return this.g;
    }

    public String h() {
        return this.h;
    }

    public boolean i() {
        return this.i;
    }

    public boolean j() {
        return d.d(this.a);
    }

    public boolean k() {
        return d.e(this.a);
    }

    public BigDecimal l() {
        BigDecimal add = BigDecimal.ZERO.add(this.b);
        if (this.c == null) {
            return add;
        }
        if (this.c.a() != null) {
            add = add.add(this.c.a());
        }
        return this.c.b() != null ? add.add(this.c.b()) : add;
    }
}
