package com.paypal.android.MEP;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.EditText;
import com.paypal.android.MEP.a.g;
import com.paypal.android.MEP.a.h;
import com.paypal.android.MEP.a.i;
import com.paypal.android.MEP.a.k;
import com.paypal.android.MEP.a.l;
import com.paypal.android.MEP.a.m;
import com.paypal.android.MEP.a.n;
import com.paypal.android.MEP.a.p;
import com.paypal.android.MEP.a.q;
import com.paypal.android.MEP.b.e;
import com.paypal.android.a.d;
import com.paypal.android.a.f;
import com.paypal.android.b.j;
import java.util.Hashtable;
import java.util.Vector;

public final class PayPalActivity extends Activity implements Animation.AnimationListener {
    public static f a = null;
    public static o b;
    public static String d;
    public static String e;
    public static String f;
    public static String g;
    public static String h;
    public static String i;
    public static String j;
    public static String k;
    public static String l;
    public static String m;
    /* access modifiers changed from: private */
    public static PayPalActivity p = null;
    /* access modifiers changed from: private */
    public static String u = null;
    public boolean c = false;
    private g n;
    private j o;
    private Vector q;
    private Animation r;
    /* access modifiers changed from: private */
    public Intent s = null;
    private boolean t = false;
    private BroadcastReceiver v = new n(this);
    private BroadcastReceiver w = new l(this);

    public static PayPalActivity a() {
        return p;
    }

    /* access modifiers changed from: private */
    public boolean a(int i2) {
        j kVar;
        if (i2 == 0) {
            a.b("mpl-login");
            kVar = new h(this);
        } else if (i2 == 1) {
            a.b("mpl-help-binding");
            kVar = new i(this);
        } else if (i2 == 2) {
            a.b("mpl-help");
            kVar = new g(this);
        } else if (i2 == 3) {
            kVar = new m(this);
        } else if (i2 == 4) {
            kVar = new l(this);
        } else if (i2 == 5) {
            kVar = new com.paypal.android.MEP.a.j(this, this.s);
        } else if (i2 == 6) {
            kVar = new n(this);
        } else if (i2 != 7) {
            return false;
        } else {
            a.b("mpl-create-PIN");
            kVar = new k(this);
        }
        j jVar = this.q.size() > 0 ? (j) this.q.lastElement() : null;
        setContentView(kVar);
        this.q.add(kVar);
        if (jVar != null) {
            jVar.c();
        }
        if (i2 == 0) {
            ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 2, 0.5f, 2, 0.5f);
            scaleAnimation.setDuration(500);
            scaleAnimation.setRepeatCount(0);
            scaleAnimation.setAnimationListener(this);
            kVar.setAnimation(scaleAnimation);
            if (u != null) {
                ((h) kVar).a(p.STATE_LOGGING_OUT);
                ((h) kVar).a(u);
                u = null;
            }
        }
        return true;
    }

    static /* synthetic */ boolean a(PayPalActivity payPalActivity) {
        j jVar = (j) p.q.lastElement();
        if (payPalActivity.q.size() != 1) {
            payPalActivity.q.remove(jVar);
            j jVar2 = (j) payPalActivity.q.lastElement();
            if (jVar2 != null) {
                payPalActivity.setContentView(jVar2);
            }
        }
        jVar.c();
        return true;
    }

    static /* synthetic */ boolean b(PayPalActivity payPalActivity, int i2) {
        j jVar = (j) payPalActivity.q.lastElement();
        if (!payPalActivity.a(i2)) {
            return false;
        }
        payPalActivity.q.remove(jVar);
        return true;
    }

    private void d() {
        String f2 = o.a().f();
        j = f2 + "PUSH_DIALOG_";
        k = f2 + "POP_DIALOG";
        l = f2 + "REPLACE_DIALOG_";
        m = f2 + "UPDATE_VIEW";
        d = f2 + "LOGIN_SUCCESS";
        f = f2 + "LOGIN_FAIL";
        e = f2 + "AUTH_SUCCESS";
        g = f2 + "CREATE_PAYMENT_SUCCESS";
        h = f2 + "CREATE_PAYMENT_FAIL";
        i = f2 + "FATAL_ERROR";
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(k);
        for (int i2 = 0; i2 < 9; i2++) {
            intentFilter.addAction(j + i2);
            intentFilter.addAction(l + i2);
        }
        intentFilter.addAction(m);
        registerReceiver(this.w, intentFilter);
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction(d);
        intentFilter2.addAction(f);
        intentFilter2.addAction(g);
        intentFilter2.addAction(h);
        intentFilter2.addAction(i);
        registerReceiver(this.v, intentFilter2);
        f.c();
    }

    private void e() {
        if (this.q == null || this.q.size() <= 0) {
            finish();
            p = null;
            return;
        }
        j jVar = (j) this.q.lastElement();
        this.r = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 2, 0.5f, 2, 0.5f);
        this.r.setDuration(500);
        this.r.setRepeatCount(0);
        this.r.setAnimationListener(this);
        jVar.setAnimation(this.r);
        runOnUiThread(new com.paypal.android.b.k(jVar, this.r));
    }

    public final Vector a(k kVar, String str, Vector vector) {
        if (this.n != null) {
            return this.n.a(kVar, str, vector);
        }
        return null;
    }

    public final void a(String str, String str2) {
        Intent intent = new Intent();
        intent.putExtra("com.paypal.android.PAY_KEY", str);
        intent.putExtra("com.paypal.android.PAYMENT_STATUS", str2);
        setResult(-1, intent);
        e();
    }

    public final void a(String str, String str2, String str3, String str4, boolean z, boolean z2) {
        if (this.c) {
            a((String) a.d("PayKey"), (String) a.d("PaymentExecStatus"));
            return;
        }
        if (this.o != null && z2) {
            this.o.a("OTHER", str, str2, str3, str4);
        }
        if (z) {
            Intent intent = new Intent();
            intent.putExtra("com.paypal.android.CORRELATION_ID", str);
            intent.putExtra("com.paypal.android.PAY_KEY", str2);
            intent.putExtra("com.paypal.android.ERROR_ID", str3);
            intent.putExtra("com.paypal.android.ERROR_MESSAGE", str4);
            intent.putExtra("com.paypal.android.PAYMENT_STATUS", "OTHER");
            setResult(2, intent);
            e();
        }
    }

    public final void a(String str, String str2, boolean z) {
        if (this.o != null) {
            this.o.a(str, str2);
        }
        if (z) {
            a(str, str2);
        }
    }

    public final void a(boolean z) {
        this.c = z;
    }

    public final void b() {
        if (this.c) {
            a((String) a.d("PayKey"), (String) a.d("PaymentExecStatus"));
            return;
        }
        if (this.o != null) {
            this.o.a("OTHER");
        }
        Intent intent = new Intent();
        intent.putExtra("com.paypal.android.PAYMENT_STATUS", "OTHER");
        setResult(0, intent);
        e();
    }

    public final void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 3) {
            setResult(i3, intent);
            e();
        }
    }

    public final void onAnimationEnd(Animation animation) {
        if (animation == this.r) {
            p.finish();
            p = null;
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        int i2 = 0;
        super.onCreate(bundle);
        this.t = false;
        Hashtable hashtable = (Hashtable) getLastNonConfigurationInstance();
        if (hashtable != null) {
            b = (o) hashtable.get("PayPal");
            Vector vector = (Vector) hashtable.get("ViewStack");
            this.q = new Vector(0);
            a = (f) hashtable.get("NetworkHandler");
            if (hashtable.get("ReviewViewInfo") != null) {
                m.a = (Hashtable) hashtable.get("ReviewViewInfo");
            }
            Object obj = null;
            d();
            while (true) {
                Object obj2 = obj;
                if (i2 >= vector.size()) {
                    break;
                }
                int intValue = ((Integer) vector.elementAt(i2)).intValue();
                obj = intValue == 0 ? new h(this) : intValue == 1 ? new i(this) : intValue == 2 ? new g(this) : intValue == 3 ? new m(this) : intValue == 4 ? new l(this) : intValue == 5 ? new com.paypal.android.MEP.a.j(this) : intValue == 6 ? new n(this) : intValue == 7 ? new k(this) : obj2;
                this.q.add(obj);
                i2++;
            }
            Editable editable = (Editable) hashtable.get("UserString");
            EditText editText = (EditText) findViewById(5004);
            if (!(editText == null || editable == null || editable.length() <= 0)) {
                editText.setText(editable);
            }
            Editable editable2 = (Editable) hashtable.get("PasswordString");
            EditText editText2 = (EditText) findViewById(5005);
            if (!(editText2 == null || editable2 == null || editable2.length() <= 0)) {
                editText2.setText(editable2);
            }
            setContentView((View) this.q.lastElement());
            this.q.lastElement();
            p = this;
            return;
        }
        Intent intent = getIntent();
        if (intent.hasExtra("com.paypal.android.PAYPAL_PAYMENT") || intent.hasExtra("com.paypal.android.PAYPAL_PREAPPROVAL")) {
            if (intent.hasExtra("com.paypal.android.PAYMENT_ADJUSTER")) {
                this.n = (g) intent.getSerializableExtra("com.paypal.android.PAYMENT_ADJUSTER");
            }
            if (intent.hasExtra("com.paypal.android.RESULT_DELEGATE")) {
                this.o = (j) intent.getSerializableExtra("com.paypal.android.RESULT_DELEGATE");
            }
            d();
            o a2 = o.a();
            if (a2.z() == 3) {
                c cVar = (c) intent.getSerializableExtra("com.paypal.android.PAYPAL_PREAPPROVAL");
                if (cVar == null) {
                    a((String) a.d("CorrelationId"), (String) a.d("PayKey"), "-1", d.a("ANDROID_no_payment"), true, true);
                    return;
                } else if (!cVar.e() || a2.g() == null || a2.g().length() <= 0) {
                    a((String) a.d("CorrelationId"), (String) a.d("PayKey"), "-1", d.a("ANDROID_invalid_payment"), true, true);
                    return;
                } else if (!f.b()) {
                    a((String) a.d("CorrelationId"), (String) a.d("PayKey"), "-1", d.a("ANDROID_application_not_authorized"), true, true);
                    return;
                }
            } else {
                i iVar = (i) intent.getSerializableExtra("com.paypal.android.PAYPAL_PAYMENT");
                if (iVar == null) {
                    a((String) a.d("CorrelationId"), (String) a.d("PayKey"), "-1", d.a("ANDROID_no_payment"), true, true);
                    return;
                } else if (!iVar.e()) {
                    a((String) a.d("CorrelationId"), (String) a.d("PayKey"), "-1", d.a("ANDROID_invalid_payment"), true, true);
                    return;
                } else if (!f.b()) {
                    a((String) a.d("CorrelationId"), (String) a.d("PayKey"), "-1", d.a("ANDROID_application_not_authorized"), true, true);
                    return;
                }
            }
            p = this;
            if (this.q != null) {
                this.q.setSize(0);
            } else {
                this.q = new Vector(0);
            }
            q.a(0);
            return;
        }
        throw new NullPointerException("PayPalPayment/Preapproval object does not exist in intent");
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(this.w);
            unregisterReceiver(this.v);
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.PayPalActivity.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.MEP.PayPalActivity.a(com.paypal.android.MEP.k, java.lang.String, java.util.Vector):java.util.Vector
      com.paypal.android.MEP.PayPalActivity.a(java.lang.String, java.lang.String, boolean):void */
    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (this.q.lastElement() instanceof h) {
            if (((h) this.q.lastElement()).d() != p.STATE_LOGGING_IN) {
                new e(this).show();
            }
            return true;
        } else if (this.q.lastElement() instanceof m) {
            if (!(((m) this.q.lastElement()).d() == com.paypal.android.MEP.a.d.STATE_SENDING_PAYMENT || ((m) this.q.lastElement()).d() == com.paypal.android.MEP.a.d.STATE_UPDATING)) {
                new e(this).show();
            }
            return true;
        } else if (this.q.lastElement() instanceof n) {
            if (((n) this.q.lastElement()).d() != com.paypal.android.MEP.a.e.STATE_CONFIRM_PREAPPROVAL) {
                new e(this).show();
            }
            return true;
        } else if ((this.q.lastElement() instanceof g) || (this.q.lastElement() instanceof i)) {
            q.a();
            return true;
        } else if (!(this.q.lastElement() instanceof l) && !(this.q.lastElement() instanceof k)) {
            return super.onKeyDown(i2, keyEvent);
        } else {
            if (!this.t) {
                this.t = true;
                a((String) a.d("PayKey"), (String) a.d("PaymentExecStatus"), true);
            }
            return true;
        }
    }

    public final Object onRetainNonConfigurationInstance() {
        Editable text;
        Editable text2;
        Hashtable hashtable = new Hashtable();
        hashtable.put("PayPal", b);
        Vector vector = new Vector();
        for (int i2 = 0; i2 < this.q.size(); i2++) {
            j jVar = (j) this.q.elementAt(i2);
            vector.add(new Integer(jVar instanceof h ? 0 : jVar instanceof i ? 1 : jVar instanceof g ? 2 : jVar instanceof m ? 3 : jVar instanceof l ? 4 : jVar instanceof com.paypal.android.MEP.a.j ? 5 : jVar instanceof n ? 6 : jVar instanceof k ? 7 : 0));
        }
        hashtable.put("ViewStack", vector);
        hashtable.put("NetworkHandler", a);
        if (m.a != null) {
            hashtable.put("ReviewViewInfo", m.a);
        }
        EditText editText = (EditText) findViewById(5004);
        if (!(editText == null || (text2 = editText.getText()) == null || text2.length() <= 0)) {
            hashtable.put("UserString", text2);
        }
        EditText editText2 = (EditText) findViewById(5005);
        if (!(editText2 == null || (text = editText2.getText()) == null || text.length() <= 0)) {
            hashtable.put("PasswordString", text);
        }
        return hashtable;
    }
}
