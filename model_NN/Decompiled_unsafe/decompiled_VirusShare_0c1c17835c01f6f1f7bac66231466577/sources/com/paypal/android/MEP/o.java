package com.paypal.android.MEP;

import android.content.Context;
import android.content.Intent;
import com.paypal.android.a.b;
import com.paypal.android.a.d;
import com.paypal.android.a.f;
import java.io.Serializable;
import java.util.Locale;

public final class o {
    private static final String[] a = {"GOODS", "SERVICE", "PERSONAL", "NONE"};
    private static final String[] b = {"AFFILIATE_PAYMENTS", "B2B", "PAYROLL", "REBATES", "REFUNDS", "REIMBURSEMENTS", "DONATIONS", "UTILITIES", "TUITION", "GOVERNMENT", "INSURANCE", "REMITTANCES", "RENT", "MORTGAGE", "MEDICAL", "CHILD_CARE", "EVENT_PLANNING", "GENERAL_CONTRACTORS", "ENTERTAINMENT", "TOURISM", "INVOICE", "TRANSFER", "NONE"};
    private t c = new t(this);
    private s d = new s(this);
    private boolean e = false;

    private o() {
    }

    public static o a() {
        return PayPalActivity.b;
    }

    public static o a(Context context, String str, int i) {
        if (PayPalActivity.b != null && PayPalActivity.b.b()) {
            throw new IllegalStateException("You have attempted to initialize PayPal more than once.  Use getInstance() after initialization.");
        } else if (i < 0 || i > 2) {
            throw new IllegalStateException("You have attempted to initialize PayPal with an invalid server.");
        } else {
            o oVar = new o();
            PayPalActivity.b = oVar;
            oVar.c.d = context;
            PayPalActivity.b.c.e = str;
            PayPalActivity.b.c.j = i;
            PayPalActivity.b.c.n = false;
            PayPalActivity.b.c.m = true;
            o oVar2 = PayPalActivity.b;
            b.a();
            Locale locale = Locale.getDefault();
            String str2 = locale.getLanguage() + '_' + locale.getCountry();
            oVar2.a(str2);
            d.c(str2);
            f.c();
            PayPalActivity.a.a(8);
            PayPalActivity.b.a(27.0f);
            return PayPalActivity.b;
        }
    }

    private boolean a(float f) {
        int i = 0;
        while (!this.e) {
            i++;
            if (i >= 270) {
                return false;
            }
            try {
                Thread.sleep(100);
            } catch (Exception e2) {
            }
        }
        return true;
    }

    public static String d(int i) {
        return a[i];
    }

    public static String e(int i) {
        return b[i];
    }

    public static String x() {
        return "1.5";
    }

    public static String y() {
        return "11.05.18.001";
    }

    public final float A() {
        return e().getResources().getDisplayMetrics().density;
    }

    public final boolean B() {
        if (this.c.a == null) {
            return false;
        }
        for (int i = 0; i < this.c.a.b().size(); i++) {
            if (((f) this.c.a.b().get(i)).d() != 2) {
                return false;
            }
        }
        return true;
    }

    public final boolean C() {
        String iSO3Country = Locale.getDefault().getISO3Country();
        return !(Locale.GERMANY.getISO3Country().compareTo(iSO3Country) == 0 || Locale.ITALY.getISO3Country().compareTo(iSO3Country) == 0) && B();
    }

    public final boolean D() {
        String iSO3Country = Locale.getDefault().getISO3Country();
        return !iSO3Country.equals("USA") && !iSO3Country.equals("GBR") && !iSO3Country.equals("CAN") && !iSO3Country.equals("AUS") && !iSO3Country.equals("ESP") && !iSO3Country.equals("ITA") && !iSO3Country.equals("FRA") && !iSO3Country.equals("SGP") && !iSO3Country.equals("MYS");
    }

    public final boolean E() {
        if (z() == 3) {
            return false;
        }
        if (c().b().size() == 1) {
            f fVar = (f) c().b().get(0);
            if (fVar.c() == null) {
                return false;
            }
            d c2 = fVar.c();
            if (c2.a() == null && c2.b() == null && c2.c().size() == 0) {
                return false;
            }
        }
        return true;
    }

    public final Intent a(i iVar, Context context, g gVar, j jVar) {
        this.c.a = iVar;
        this.c.b = null;
        Intent intent = new Intent(context, PayPalActivity.class);
        intent.putExtra("com.paypal.android.PAYPAL_PAYMENT", iVar);
        if (gVar != null) {
            intent.putExtra("com.paypal.android.PAYMENT_ADJUSTER", (Serializable) gVar);
        }
        if (jVar != null) {
            intent.putExtra("com.paypal.android.RESULT_DELEGATE", (Serializable) jVar);
        }
        return intent;
    }

    public final Intent a(q qVar, Context context, g gVar, j jVar) {
        i iVar = new i();
        iVar.a(qVar.a());
        iVar.b(qVar.j());
        iVar.c(qVar.k());
        f fVar = new f();
        fVar.a(qVar.b());
        fVar.a(qVar.c());
        fVar.a(qVar.d());
        fVar.a(qVar.e());
        fVar.b(qVar.f());
        fVar.b(qVar.g());
        fVar.c(qVar.h());
        fVar.d(qVar.i());
        fVar.a(false);
        iVar.b().add(fVar);
        return a(iVar, context, gVar, jVar);
    }

    public final Intent a(q qVar, Context context, j jVar) {
        return a(qVar, context, (g) null, jVar);
    }

    public final void a(int i) {
        this.c.l = i;
    }

    public final void a(String str) {
        this.c.g = !d.b(str) ? "en_US" : str;
        d.c(this.c.g);
    }

    public final void a(boolean z) {
        this.e = z;
    }

    public final void b(int i) {
        this.d.d = i;
    }

    public final void b(String str) {
        this.c.h = str;
    }

    public final void b(boolean z) {
        this.c.m = z;
    }

    public final boolean b() {
        if (PayPalActivity.b == null) {
            return false;
        }
        return this.e;
    }

    public final i c() {
        return this.c.a;
    }

    public final void c(int i) {
        this.d.e = i;
    }

    public final void c(String str) {
        this.c.i = str;
    }

    public final void c(boolean z) {
        this.c.n = z;
    }

    public final c d() {
        return this.c.b;
    }

    public final void d(String str) {
        this.d.a = str;
    }

    public final void d(boolean z) {
        this.d.f = z;
    }

    public final Context e() {
        return this.c.d;
    }

    public final void e(String str) {
        this.d.b = str;
    }

    public final void e(boolean z) {
        this.d.g = z;
    }

    public final String f() {
        return this.c.e;
    }

    public final void f(String str) {
        this.d.c = str;
    }

    public final String g() {
        return this.c.f;
    }

    public final String h() {
        return this.c.g;
    }

    public final String i() {
        return this.c.h;
    }

    public final String j() {
        return this.c.i;
    }

    public final int k() {
        return this.c.j;
    }

    public final int l() {
        return this.c.k;
    }

    public final int m() {
        return this.c.l;
    }

    public final boolean n() {
        return this.c.m;
    }

    public final boolean o() {
        return this.c.n;
    }

    public final String p() {
        return this.d.a;
    }

    public final String q() {
        return this.d.b;
    }

    public final String r() {
        return this.d.c;
    }

    public final int s() {
        return this.d.d;
    }

    public final int t() {
        return this.d.e;
    }

    public final boolean u() {
        return this.d.f;
    }

    public final boolean v() {
        if (a().t() == 0) {
            return false;
        }
        return this.d.g;
    }

    public final void w() {
        this.d.a();
    }

    public final int z() {
        if (this.c.a == null && this.c.b != null) {
            return 3;
        }
        if (this.c.a.b().size() == 1) {
            return 0;
        }
        return this.c.a.f() ? 2 : 1;
    }
}
