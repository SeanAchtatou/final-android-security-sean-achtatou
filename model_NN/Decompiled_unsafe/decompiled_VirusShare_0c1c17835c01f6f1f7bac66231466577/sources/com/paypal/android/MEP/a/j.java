package com.paypal.android.MEP.a;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.paypal.android.MEP.b.a;
import com.paypal.android.a.d;
import com.paypal.android.a.e;
import com.paypal.android.a.i;
import com.paypal.android.b.f;
import com.paypal.android.b.h;
import com.paypal.android.b.m;
import com.paypal.android.b.o;
import com.paypal.android.b.p;

public final class j extends com.paypal.android.b.j implements View.OnClickListener, p {
    String a = "";
    String b = "";
    private Intent c = null;
    private m d;
    private a e;

    public j(Context context) {
        super(context);
    }

    public j(Context context, Intent intent) {
        super(context);
        this.c = intent;
        try {
            this.a = this.c.getStringExtra("FATAL_ERROR_ID");
            this.b = this.c.getStringExtra("FATAL_ERROR_MESSAGE");
            this.d.a(this.b);
        } catch (Exception e2) {
            this.a = "10001";
            this.b = d.a("ANDROID_10001");
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Context context) {
        super.a(context);
        LinearLayout a2 = i.a(context, -1, -2);
        a2.setOrientation(1);
        a2.setPadding(5, 5, 5, 15);
        a2.addView(e.b(com.paypal.android.a.j.HELVETICA_16_BOLD, context));
        this.e = new a(context, this);
        this.e.a(this);
        a2.addView(this.e);
        addView(a2);
        LinearLayout a3 = i.a(context, -1, -1);
        a3.setBackgroundDrawable(i.a());
        a3.setPadding(10, 5, 10, 5);
        a3.setOrientation(1);
        a3.addView(new h(d.a("ANDROID_error_heading"), context));
        LinearLayout a4 = i.a(context, -1, -2);
        a4.setOrientation(1);
        a4.setPadding(5, 10, 5, 10);
        this.d = new m(context, o.RED_ALERT);
        this.d.a(d.a("ANDROID_10001"));
        this.d.setPadding(0, 5, 0, 5);
        this.d.setVisibility(0);
        a4.addView(this.d);
        a3.addView(a4);
        Button button = new Button(context);
        button.setText(d.a("ANDROID_ok"));
        button.setLayoutParams(new RelativeLayout.LayoutParams(-1, i.b()));
        button.setGravity(1);
        button.setBackgroundDrawable(com.paypal.android.a.h.a());
        button.setTextColor(-16777216);
        button.setOnClickListener(new f(this));
        a3.addView(button);
        addView(a3);
    }

    public final void a(f fVar, int i) {
    }

    public final void b() {
    }

    public final void c() {
    }

    public final void onClick(View view) {
    }
}
