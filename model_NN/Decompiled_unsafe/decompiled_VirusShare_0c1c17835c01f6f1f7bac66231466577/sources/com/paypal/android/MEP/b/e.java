package com.paypal.android.MEP.b;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.f;
import com.paypal.android.MEP.o;
import com.paypal.android.a.d;
import com.paypal.android.a.h;
import com.paypal.android.a.i;
import com.paypal.android.a.j;

public final class e extends Dialog implements View.OnClickListener {
    private Button a;
    private Button b;

    public e(Context context) {
        super(context);
        String a2;
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        requestWindowFeature(1);
        LinearLayout a3 = i.a(context, -1, -1);
        a3.setOrientation(1);
        a3.setGravity(1);
        a3.setPadding(10, 5, 10, 5);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1510918, -4336918});
        gradientDrawable.setCornerRadius(3.0f);
        gradientDrawable.setStroke(0, 0);
        a3.setBackgroundDrawable(gradientDrawable);
        TextView a4 = com.paypal.android.a.e.a(j.HELVETICA_16_BOLD, context);
        a4.setText(d.a("ANDROID_cancel_transaction"));
        a4.setGravity(17);
        a4.setPadding(0, 0, 0, 10);
        a3.addView(a4);
        String a5 = d.a("ANDROID_go_back_to_merchant");
        o a6 = o.a();
        com.paypal.android.MEP.i c = a6.c();
        if (a6.z() == 3) {
            a2 = a6.d().a();
        } else if (a6.z() == 0) {
            if (a6.B()) {
                a2 = ((f) c.b().get(0)).a();
            } else {
                a2 = ((f) c.b().get(0)).h();
                if (a2 == null || a2.length() <= 0) {
                    a2 = ((f) c.b().get(0)).a();
                }
            }
        } else if (a6.z() == 2) {
            a2 = c.g().h();
            if (a2 == null || a2.length() <= 0) {
                a2 = c.g().a();
            }
        } else {
            a2 = d.a("ANDROID_the_merchant");
        }
        String replace = a5.replace("%m", a2);
        TextView a7 = com.paypal.android.a.e.a(j.HELVETICA_14_NORMAL, context);
        a7.setText(replace);
        a7.setGravity(17);
        a3.addView(a7);
        TextView a8 = com.paypal.android.a.e.a(j.HELVETICA_14_NORMAL, context);
        a8.setText(d.a("ANDROID_lose_all_information"));
        a8.setGravity(17);
        a3.addView(a8);
        LinearLayout a9 = i.a(context, -1, -2);
        a9.setOrientation(0);
        a9.setGravity(16);
        a9.setPadding(0, 10, 0, 10);
        LinearLayout a10 = i.a(context, -1, -2, 0.5f);
        a10.setOrientation(1);
        a10.setGravity(1);
        a10.setPadding(0, 0, 3, 0);
        this.a = new Button(context);
        this.a.setText(d.a("ANDROID_ok"));
        this.a.setTextColor(-16777216);
        this.a.setLayoutParams(new LinearLayout.LayoutParams(-1, i.b()));
        this.a.setGravity(17);
        this.a.setBackgroundDrawable(h.a());
        this.a.setOnClickListener(this);
        a10.addView(this.a);
        a9.addView(a10);
        LinearLayout a11 = i.a(context, -1, -2, 0.5f);
        a11.setOrientation(1);
        a11.setGravity(1);
        a11.setPadding(3, 0, 0, 0);
        this.b = new Button(context);
        this.b.setText(d.a("ANDROID_cancel"));
        this.b.setTextColor(-16777216);
        this.b.setLayoutParams(new LinearLayout.LayoutParams(-1, i.b()));
        this.b.setGravity(17);
        this.b.setBackgroundDrawable(h.b());
        this.b.setOnClickListener(this);
        a11.addView(this.b);
        a9.addView(a11);
        a3.addView(a9);
        setContentView(a3);
    }

    public final void onClick(View view) {
        if (view == this.a) {
            o.a().e(false);
            dismiss();
            PayPalActivity.a().b();
        } else if (view == this.b) {
            dismiss();
        }
    }
}
