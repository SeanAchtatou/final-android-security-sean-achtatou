package com.paypal.android.MEP.a;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.b;
import com.paypal.android.MEP.b.a;
import com.paypal.android.MEP.b.i;
import com.paypal.android.MEP.o;
import com.paypal.android.a.e;
import com.paypal.android.b.d;
import com.paypal.android.b.f;
import com.paypal.android.b.h;
import com.paypal.android.b.j;
import com.paypal.android.b.p;
import java.util.Hashtable;

public final class m extends j implements View.OnClickListener, b, i, p {
    public static Hashtable a = null;
    private static d n = null;
    private d b;
    private a c;
    private Button d;
    private Button e;
    private com.paypal.android.MEP.b.d f;
    private com.paypal.android.MEP.b.d g;
    private com.paypal.android.MEP.b.d h;
    private com.paypal.android.b.m i;
    private String j;
    private LinearLayout k;
    private RelativeLayout l;
    private TextView m = null;
    private Context o;

    public m(Context context) {
        super(context);
        this.o = context;
    }

    private void b(String str) {
        LinearLayout a2 = com.paypal.android.a.i.a(this.o, -1, -2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2.getLayoutParams());
        layoutParams.addRule(13);
        a2.setLayoutParams(layoutParams);
        a2.setOrientation(1);
        a2.setGravity(1);
        if (n == null) {
            n = new d(this.o);
        } else {
            ((LinearLayout) n.getParent()).removeAllViews();
        }
        this.m = e.a(com.paypal.android.a.j.HELVETICA_16_NORMAL, this.o);
        this.m.setGravity(1);
        this.m.setTextColor(-13408615);
        this.m.setText(str);
        a2.addView(n);
        a2.addView(this.m);
        this.l.removeAllViews();
        this.l.addView(a2);
    }

    public final void a() {
        PayPalActivity.a.a("delegate", this);
        PayPalActivity.a.a(4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.PayPalActivity.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.MEP.PayPalActivity.a(com.paypal.android.MEP.k, java.lang.String, java.util.Vector):java.util.Vector
      com.paypal.android.MEP.PayPalActivity.a(java.lang.String, java.lang.String, boolean):void */
    public final void a(int i2, Object obj) {
        if (this.b == d.STATE_SENDING_PAYMENT) {
            o a2 = o.a();
            if (o.a().k() == 2 || (!a2.v() && !a2.u() && !a2.D() && a2.z() != 3)) {
                PayPalActivity.a().a(true);
                PayPalActivity.a().a((String) PayPalActivity.a.d("PayKey"), (String) PayPalActivity.a.d("PaymentExecStatus"), false);
                k.a = (String) obj;
                q.b(7);
                return;
            }
            PayPalActivity.a().a((String) PayPalActivity.a.d("PayKey"), (String) PayPalActivity.a.d("PaymentExecStatus"), true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.b.a.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.paypal.android.b.f.a(android.widget.LinearLayout$LayoutParams, int):void
      com.paypal.android.MEP.b.a.a(boolean, boolean):void */
    public final void a(Context context) {
        o a2 = o.a();
        super.a(context);
        this.o = context;
        this.b = d.STATE_NORMAL;
        LinearLayout a3 = com.paypal.android.a.i.a(context, -1, -2);
        a3.setOrientation(1);
        a3.setPadding(5, 5, 5, 15);
        a3.addView(e.b(com.paypal.android.a.j.HELVETICA_16_BOLD, context));
        this.c = new a(context, this);
        this.c.a(this);
        a3.addView(this.c);
        addView(a3);
        this.k = new LinearLayout(context);
        this.k.setOrientation(1);
        this.k.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.k.setPadding(5, 5, 5, 5);
        this.k.setBackgroundDrawable(com.paypal.android.a.i.a());
        this.k.addView(new h(com.paypal.android.a.d.a("ANDROID_review"), context));
        LinearLayout a4 = com.paypal.android.a.i.a(context, -1, -2);
        a4.setOrientation(1);
        a4.setPadding(5, 10, 5, 10);
        this.i = new com.paypal.android.b.m(context, com.paypal.android.b.o.YELLOW_ALERT);
        this.i.a("This page is currently being used to test components.");
        this.i.setPadding(0, 5, 0, 5);
        this.i.setVisibility(8);
        a4.addView(this.i);
        this.k.addView(a4);
        this.g = new com.paypal.android.MEP.b.d(context, com.paypal.android.MEP.b.h.PAYMENT_DETAILS_FEES, this);
        this.g.a((p) this);
        this.g.setPadding(0, 5, 0, 5);
        this.g.a((i) this);
        if (o.a().C()) {
            this.k.addView(this.g);
        }
        this.f = new com.paypal.android.MEP.b.d(context, com.paypal.android.MEP.b.h.PAYMENT_DETAILS_FUNDING, this);
        this.f.a((p) this);
        this.f.setPadding(0, 5, 0, 5);
        this.f.a((i) this);
        this.k.addView(this.f);
        this.h = new com.paypal.android.MEP.b.d(context, com.paypal.android.MEP.b.h.PAYMENT_DETAILS_SHIPPING, this);
        this.h.a((p) this);
        this.h.setPadding(0, 5, 0, 5);
        this.h.a((i) this);
        if (a2.n()) {
            this.k.addView(this.h);
        }
        LinearLayout a5 = com.paypal.android.a.i.a(context, -1, -2);
        a5.setGravity(1);
        a5.setPadding(5, 10, 5, 5);
        this.e = new Button(context);
        if (a2.l() == 1) {
            this.e.setText(com.paypal.android.a.d.a("ANDROID_donate"));
        } else {
            this.e.setText(com.paypal.android.a.d.a("ANDROID_pay"));
        }
        this.e.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.i.b(), 0.5f));
        this.e.setGravity(17);
        this.e.setBackgroundDrawable(com.paypal.android.a.h.a());
        this.e.setTextColor(-16777216);
        this.e.setOnClickListener(this);
        a5.addView(this.e);
        this.k.addView(a5);
        LinearLayout a6 = com.paypal.android.a.i.a(context, -1, -2);
        a6.setGravity(1);
        a6.setPadding(5, 5, 5, 10);
        this.d = new Button(context);
        this.d.setText(com.paypal.android.a.d.a("ANDROID_cancel"));
        this.d.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.i.b(), 0.5f));
        this.d.setGravity(17);
        this.d.setBackgroundDrawable(com.paypal.android.a.h.b());
        this.d.setTextColor(-16777216);
        this.d.setOnClickListener(this);
        a6.addView(this.d);
        this.k.addView(a6);
        addView(this.k);
        this.l = new RelativeLayout(context);
        this.l.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.l.setBackgroundDrawable(com.paypal.android.a.i.a());
        LinearLayout a7 = com.paypal.android.a.i.a(context, -1, -2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a7.getLayoutParams());
        layoutParams.addRule(13);
        a7.setLayoutParams(layoutParams);
        a7.setOrientation(1);
        a7.setGravity(1);
        if (n == null) {
            n = new d(context);
        } else {
            ((LinearLayout) n.getParent()).removeAllViews();
        }
        this.m = e.a(com.paypal.android.a.j.HELVETICA_16_NORMAL, context);
        this.m.setGravity(1);
        this.m.setTextColor(-13408615);
        this.m.setText(com.paypal.android.a.d.a("ANDROID_processing_transaction_message"));
        a7.addView(n);
        a7.addView(this.m);
        this.l.addView(a7);
        this.l.setVisibility(8);
        addView(this.l);
        if (!o.a().E()) {
            this.c.a(false, false);
        }
    }

    public final void a(d dVar) {
        this.b = dVar;
        q.b();
    }

    public final void a(com.paypal.android.MEP.b.d dVar, int i2) {
        switch (c.a[dVar.e().ordinal()]) {
            case 1:
                if (this.f != null) {
                    this.f.setNextFocusUpId(i2);
                    return;
                }
                return;
            case 2:
                if (this.h != null) {
                    this.h.setNextFocusUpId(i2);
                    return;
                }
                return;
            case 3:
                if (this.e != null) {
                    this.e.setNextFocusUpId(i2);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void a(f fVar, int i2) {
        if (i2 == 1) {
            if (!(this.c == null || fVar == this.c)) {
                this.c.a(0);
            }
            if (!(this.f == null || fVar == this.f)) {
                this.f.a(0);
            }
            if (!(this.g == null || fVar == this.g)) {
                this.g.a(0);
            }
            if (this.h != null && fVar != this.h) {
                this.h.a(0);
            }
        }
    }

    public final void a(String str) {
        if (this.b == d.STATE_SENDING_PAYMENT) {
            this.j = str;
            a(d.STATE_ERROR);
        }
    }

    public final void a(String str, Object obj) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.b.a.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.paypal.android.b.f.a(android.widget.LinearLayout$LayoutParams, int):void
      com.paypal.android.MEP.b.a.a(boolean, boolean):void */
    public final void b() {
        if (this.f != null) {
            this.f.d();
        }
        if (this.g != null) {
            this.g.d();
        }
        if (this.h != null) {
            this.h.d();
        }
        if (this.b == d.STATE_SENDING_PAYMENT) {
            b(com.paypal.android.a.d.a("ANDROID_processing_transaction_message"));
            this.c.a(false, true);
            this.k.setVisibility(8);
            this.l.setVisibility(0);
            n.a();
        } else if (this.b == d.STATE_UPDATING) {
            b(com.paypal.android.a.d.a("ANDROID_getting_information"));
            this.c.a(false, true);
            this.k.setVisibility(8);
            this.l.setVisibility(0);
            n.a();
        } else if (this.b == d.STATE_NORMAL || this.b == d.STATE_ERROR) {
            if (o.a().E()) {
                this.c.a(true, false);
            } else {
                this.c.a(false, false);
            }
            n.b();
            this.l.setVisibility(8);
            this.k.setVisibility(0);
            if (this.b == d.STATE_ERROR) {
                this.i.a(this.j);
                this.i.setVisibility(0);
            }
        }
    }

    public final void c() {
    }

    public final d d() {
        return this.b;
    }

    public final void e() {
        if (this.c != null) {
            this.c.a(0);
        }
        if (this.f != null) {
            this.f.a(0);
        }
        if (this.g != null) {
            this.g.a(0);
        }
        if (this.h != null) {
            this.h.a(0);
        }
    }

    public final void onClick(View view) {
        if (this.d == view) {
            new com.paypal.android.MEP.b.e(PayPalActivity.a()).show();
        } else if (this.e == view) {
            a(d.STATE_SENDING_PAYMENT);
            if (o.a().k() == 2) {
                a(4, "10088342");
            } else {
                com.paypal.android.MEP.m.a().a(this);
            }
        } else if ((view.getId() & 2130706432) == 2130706432) {
            this.f.a(0);
        } else if ((view.getId() & 2113929216) == 2113929216) {
            this.g.a(0);
        } else if ((view.getId() & 2097152000) == 2097152000) {
            this.h.a(0);
        }
    }
}
