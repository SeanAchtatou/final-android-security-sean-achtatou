package com.paypal.android.MEP.b;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.o;
import com.paypal.android.a.d;
import com.paypal.android.a.h;

public final class f extends RelativeLayout implements View.OnClickListener {
    private boolean a;
    private int b;
    private LinearLayout c;
    private LinearLayout d;
    private ImageView e;
    private ImageView f;
    private ImageView g;
    private ImageView h;
    private TextView i;
    private EditText j;
    private EditText k;
    private GradientDrawable l;

    public f(Context context) {
        super(context);
        int i2;
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        setPadding(5, 5, 5, 5);
        this.c = new LinearLayout(context);
        this.c.setId(5001);
        this.c.setBackgroundColor(0);
        this.c.setOrientation(0);
        this.c.setGravity(3);
        this.c.setPadding(5, 0, 5, 0);
        if (!o.a().D()) {
            this.e = h.a(context, "tab-selected-email.png");
            this.e.setPadding(5, 0, 5, 0);
            this.e.setFocusable(false);
            this.c.addView(this.e);
            this.f = h.a(context, "tab-unselected-email.png");
            this.f.setPadding(5, 0, 5, 0);
            this.f.setOnClickListener(this);
            this.f.setFocusable(false);
            this.c.addView(this.f);
            this.g = h.a(context, "tab-selected-phone.png");
            this.g.setPadding(5, 0, 5, 0);
            this.g.setFocusable(false);
            this.c.addView(this.g);
            this.h = h.a(context, "tab-unselected-phone.png");
            this.h.setPadding(5, 0, 5, 0);
            this.h.setOnClickListener(this);
            this.h.setFocusable(false);
            this.c.addView(this.h);
            i2 = -1;
        } else {
            i2 = 0;
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(9);
        this.c.setLayoutParams(layoutParams);
        this.d = new LinearLayout(context);
        this.d.setId(5002);
        this.l = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-789517, -3355444});
        this.l.setCornerRadius(5.0f);
        this.l.setStroke(1, -5197648);
        this.d.setBackgroundDrawable(this.l);
        this.d.setOrientation(1);
        this.d.setGravity(3);
        this.d.setPadding(10, 5, 10, 5);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(5, this.c.getId());
        layoutParams2.addRule(3, this.c.getId());
        layoutParams2.setMargins(0, i2, 0, 0);
        this.d.setLayoutParams(layoutParams2);
        this.i = new TextView(context);
        this.i.setId(5003);
        this.i.setTypeface(Typeface.create("Helvetica", 1));
        this.i.setTextSize(16.0f);
        this.i.setTextColor(-16777216);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.setMargins(0, 2, 0, 2);
        this.i.setLayoutParams(layoutParams3);
        this.d.addView(this.i);
        this.j = new EditText(context);
        this.j.setId(5004);
        this.j.setLayoutParams(layoutParams3);
        this.j.setSingleLine(true);
        this.d.addView(this.j);
        this.k = new EditText(context);
        this.k.setId(5005);
        this.k.setLayoutParams(layoutParams3);
        this.k.setSingleLine(true);
        this.d.addView(this.k);
        addView(this.c);
        addView(this.d);
        if (!Build.VERSION.SDK.equals("3")) {
            bringChildToFront(this.c);
        }
        if (o.a().s() == 1) {
            this.a = false;
            g();
            return;
        }
        f();
    }

    private void f() {
        if (!o.a().D()) {
            this.e.setVisibility(0);
            this.f.setVisibility(8);
            this.g.setVisibility(8);
            this.h.setVisibility(0);
        }
        this.a = true;
        this.i.setText(d.a("ANDROID_login_with_email_and_password") + ":");
        this.j.setVisibility(0);
        this.j.setHint(d.a("ANDROID_email_field"));
        this.j.setInputType(32);
        if (o.a().q() != null) {
            this.j.setText(o.a().q());
        } else {
            this.j.setText("");
        }
        this.k.setText("");
        this.k.setHint(d.a("ANDROID_password_field"));
        this.k.setInputType(128);
        this.k.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.b = 0;
    }

    private void g() {
        if (!o.a().D()) {
            this.e.setVisibility(8);
            this.f.setVisibility(0);
            this.g.setVisibility(0);
            this.h.setVisibility(8);
        }
        if (this.a) {
            this.i.setText(d.a("ANDROID_login_with_phone_and_pin") + ":");
        } else {
            this.i.setText(d.a("ANDROID_login_with_pin") + ":");
        }
        this.j.setVisibility(this.a ? 0 : 8);
        this.j.setHint(d.a("ANDROID_phone_field"));
        this.j.setInputType(3);
        if (o.a().r() != null) {
            this.j.setText(o.a().r());
        } else {
            this.j.setText("");
        }
        this.k.setText("");
        this.k.setHint(d.a("ANDROID_pin_field"));
        this.k.setInputType(3);
        this.k.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.b = 1;
    }

    public final String a() {
        return this.j.getText().toString();
    }

    public final String b() {
        return this.k.getText().toString();
    }

    public final EditText c() {
        return this.j;
    }

    public final EditText d() {
        return this.k;
    }

    public final void e() {
        try {
            ((InputMethodManager) PayPalActivity.a().getSystemService("input_method")).hideSoftInputFromWindow(this.j.getWindowToken(), 0);
        } catch (Exception e2) {
        }
        try {
            ((InputMethodManager) PayPalActivity.a().getSystemService("input_method")).hideSoftInputFromWindow(this.k.getWindowToken(), 0);
        } catch (Exception e3) {
        }
    }

    public final void onClick(View view) {
        if (view == this.f) {
            if (this.b != 0) {
                f();
            }
        } else if (view == this.h && this.b != 1) {
            g();
        }
    }
}
