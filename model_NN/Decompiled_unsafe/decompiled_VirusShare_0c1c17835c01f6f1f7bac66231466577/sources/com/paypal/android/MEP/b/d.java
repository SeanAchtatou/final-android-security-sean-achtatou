package com.paypal.android.MEP.b;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.a.m;
import com.paypal.android.MEP.a.q;
import com.paypal.android.MEP.b;
import com.paypal.android.MEP.o;
import com.paypal.android.a.a.f;
import com.paypal.android.a.a.g;
import com.paypal.android.a.e;
import com.paypal.android.a.h;
import com.paypal.android.a.i;
import com.paypal.android.a.j;
import com.paypal.android.b.l;
import java.util.Hashtable;
import java.util.Vector;

public final class d extends l implements View.OnTouchListener, b {
    private static com.paypal.android.b.d l = null;
    private static boolean q = false;
    boolean e = false;
    Vector f = null;
    private GradientDrawable g;
    private GradientDrawable h;
    private h i;
    private LinearLayout j;
    private LinearLayout k = null;
    private TextView m = null;
    /* access modifiers changed from: private */
    public m n = null;
    /* access modifiers changed from: private */
    public Vector o = new Vector(3);
    /* access modifiers changed from: private */
    public Vector p = new Vector(3);
    private int r = 0;
    private i s = null;
    private View.OnClickListener t = new c(this);
    private String u = null;

    public d(Context context, h hVar, m mVar) {
        super(context);
        setOnTouchListener(this);
        this.n = mVar;
        this.i = hVar;
        setBackgroundColor(-16711681);
        a(new LinearLayout.LayoutParams(-1, -2), 0);
        a(new LinearLayout.LayoutParams(-1, -2), 1);
        this.g = i.a(-1, -1510918, -3154193);
        this.h = i.a(-1, -4336918, -3154193);
        setBackgroundDrawable(this.g);
        setGravity(16);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-10066330, -3487030});
        gradientDrawable.setCornerRadii(new float[]{0.0f, 0.0f, 0.0f, 0.0f, 5.0f, 5.0f, 5.0f, 5.0f});
        gradientDrawable.setStroke(2, -10066330);
        c(gradientDrawable);
        a(h.a(106169, 464));
        b(h.a(113545, 430));
        this.a.setOrientation(1);
        LinearLayout a = i.a(context, -1, -2);
        a.setOrientation(0);
        a.setGravity(16);
        TextView a2 = e.a(j.HELVETICA_14_BOLD, context);
        a2.setText(this.i == h.PAYMENT_DETAILS_FUNDING ? com.paypal.android.a.d.a("ANDROID_funding") + ":" : this.i == h.PAYMENT_DETAILS_FEES ? com.paypal.android.a.d.a("ANDROID_fee") + ":" : o.a().l() == 1 ? com.paypal.android.a.d.a("ANDROID_mailing_address") + ":" : com.paypal.android.a.d.a("ANDROID_ship_to") + ":");
        a2.setGravity(3);
        a.addView(a2);
        a.addView(this.c);
        this.c.setVisibility(0);
        this.a.addView(a);
        this.k = i.a(context, -1, -2);
        this.k.setOrientation(1);
        a(context, this.k);
        this.a.addView(this.k);
        this.j = i.a(context, -1, -2);
        this.j.setOrientation(0);
        this.j.setGravity(16);
        LinearLayout a3 = i.a(context, -1, -2);
        a3.setOrientation(1);
        a3.setGravity(1);
        this.m = e.a(j.HELVETICA_12_NORMAL, context);
        this.m.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.m.setTextColor(-13408615);
        this.m.setText(com.paypal.android.a.d.a("ANDROID_getting_information"));
        this.m.setGravity(1);
        this.m.setSingleLine(false);
        a3.addView(this.m);
        if (this.i == h.PAYMENT_DETAILS_FUNDING) {
            if (l == null) {
                l = new com.paypal.android.b.d(context);
            } else {
                ((LinearLayout) l.getParent()).removeAllViews();
            }
            l.setGravity(1);
            this.j.addView(l);
            this.j.addView(a3);
            this.j.setVisibility(8);
        }
        this.a.addView(this.j);
        if (this.i == h.PAYMENT_DETAILS_FEES) {
            a(false);
        }
        q = false;
    }

    private Button a(Context context, String str, int i2) {
        Button button = new Button(context);
        button.setText(str);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, i.b());
        layoutParams.setMargins(0, 3, 0, 2);
        button.setLayoutParams(layoutParams);
        button.setGravity(17);
        StateListDrawable stateListDrawable = new StateListDrawable();
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-328451, -4336918});
        GradientDrawable gradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-6702886, -11966331});
        GradientDrawable gradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-13605994, -16764058});
        GradientDrawable gradientDrawable4 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{2140780762, 2135517317});
        gradientDrawable.setCornerRadius(5.0f);
        gradientDrawable.setStroke(1, -6307088);
        gradientDrawable2.setCornerRadius(5.0f);
        gradientDrawable2.setStroke(1, -10650469);
        gradientDrawable3.setCornerRadius(5.0f);
        gradientDrawable3.setStroke(1, -16764058);
        gradientDrawable4.setCornerRadius(5.0f);
        gradientDrawable4.setStroke(1, 2136833179);
        stateListDrawable.addState(new int[]{16842910, -16842919, -16842908}, gradientDrawable);
        stateListDrawable.addState(new int[]{16842910, -16842919, 16842908}, gradientDrawable2);
        stateListDrawable.addState(new int[]{16842910, 16842919, -16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{16842910, 16842919, 16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{-16842910, -16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, -16842919, 16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, 16842908}, gradientDrawable4);
        button.setBackgroundDrawable(stateListDrawable);
        button.setTextColor(-16777216);
        button.setFocusable(true);
        button.setOnClickListener(this.t);
        if (this.i == h.PAYMENT_DETAILS_FUNDING) {
            button.setId(2130706432 | i2);
        } else if (this.i == h.PAYMENT_DETAILS_FEES) {
            button.setId(2113929216 | i2);
        } else if (this.i == h.PAYMENT_DETAILS_SHIPPING) {
            button.setId(2097152000 | i2);
        }
        return button;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v37, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v26, resolved type: com.paypal.android.a.a.g} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r18, android.widget.LinearLayout r19) {
        /*
            r17 = this;
            com.paypal.android.MEP.o r4 = com.paypal.android.MEP.o.a()
            int r4 = r4.k()
            r5 = 2
            if (r4 != r5) goto L_0x0181
            java.util.Hashtable r4 = com.paypal.android.MEP.a.m.a
            r6 = r4
        L_0x000e:
            r0 = r17
            com.paypal.android.MEP.b.h r0 = r0.i
            r4 = r0
            com.paypal.android.MEP.b.h r5 = com.paypal.android.MEP.b.h.PAYMENT_DETAILS_FUNDING
            if (r4 != r5) goto L_0x0471
            java.lang.String r4 = "FundingPlans"
            java.lang.Object r4 = r6.get(r4)
            java.util.Vector r4 = (java.util.Vector) r4
            if (r4 == 0) goto L_0x0027
            int r5 = r4.size()
            if (r5 != 0) goto L_0x018a
        L_0x0027:
            java.lang.String r5 = "DefaultFundingPlan"
            java.lang.Object r5 = r6.get(r5)
            com.paypal.android.a.a.g r5 = (com.paypal.android.a.a.g) r5
            r6 = r5
        L_0x0030:
            r5 = 0
            r7 = r5
        L_0x0032:
            java.util.Vector r5 = r6.d
            int r5 = r5.size()
            if (r7 >= r5) goto L_0x03d8
            java.util.Vector r5 = r6.d
            java.lang.Object r5 = r5.get(r7)
            com.paypal.android.a.a.f r5 = (com.paypal.android.a.a.f) r5
            com.paypal.android.a.a.e r8 = r5.a
            java.lang.String r8 = r8.b()
            com.paypal.android.a.a.e r9 = r5.a
            java.math.BigDecimal r9 = r9.a()
            com.paypal.android.a.a.b r10 = r5.b
            java.lang.String r10 = r10.c()
            com.paypal.android.a.a.b r11 = r5.b
            java.lang.String r11 = r11.b()
            if (r11 != 0) goto L_0x005e
            java.lang.String r11 = ""
        L_0x005e:
            com.paypal.android.b.c r12 = new com.paypal.android.b.c
            com.paypal.android.a.j r13 = com.paypal.android.a.j.HELVETICA_14_NORMAL
            com.paypal.android.a.j r14 = com.paypal.android.a.j.HELVETICA_14_NORMAL
            r0 = r12
            r1 = r18
            r2 = r13
            r3 = r14
            r0.<init>(r1, r2, r3)
            com.paypal.android.MEP.o r13 = com.paypal.android.MEP.o.a()
            java.lang.String r13 = r13.h()
            java.lang.String r14 = "fr_"
            boolean r13 = r13.contains(r14)
            if (r13 != 0) goto L_0x01ac
            r13 = 1
        L_0x007d:
            java.lang.String r14 = "BALANCE"
            int r14 = r10.indexOf(r14)
            r15 = -1
            if (r14 == r15) goto L_0x01d7
            if (r13 == 0) goto L_0x01af
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            com.paypal.android.a.a.b r5 = r5.b
            java.lang.String r5 = r5.a()
            java.lang.StringBuilder r5 = r11.append(r5)
            java.lang.String r11 = " ("
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.StringBuilder r5 = r5.append(r8)
            java.lang.String r11 = "):"
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.a(r5)
        L_0x00ae:
            java.lang.String r5 = com.paypal.android.a.d.a(r9, r8)
            r12.b(r5)
            r0 = r19
            r1 = r12
            r0.addView(r1)
            java.lang.String r5 = "BANK_DELAYED"
            boolean r5 = r10.equals(r5)
            if (r5 == 0) goto L_0x00f2
            com.paypal.android.a.j r5 = com.paypal.android.a.j.HELVETICA_12_NORMAL
            r0 = r5
            r1 = r18
            android.widget.TextView r5 = com.paypal.android.a.e.a(r0, r1)
            android.widget.LinearLayout$LayoutParams r10 = new android.widget.LinearLayout$LayoutParams
            r11 = -1
            r12 = -2
            r13 = 1056964608(0x3f000000, float:0.5)
            r10.<init>(r11, r12, r13)
            r5.setLayoutParams(r10)
            r10 = 2
            r11 = 2
            r12 = 2
            r13 = 2
            r5.setPadding(r10, r11, r12, r13)
            r10 = 0
            r5.setBackgroundColor(r10)
            java.lang.String r10 = "ANDROID_echeck_note"
            java.lang.String r10 = com.paypal.android.a.d.a(r10)
            r5.setText(r10)
            r0 = r19
            r1 = r5
            r0.addView(r1)
        L_0x00f2:
            r5 = 0
            java.math.BigDecimal r10 = new java.math.BigDecimal
            java.lang.String r11 = "0"
            r10.<init>(r11)
            com.paypal.android.a.a.e r11 = r6.b
            if (r11 == 0) goto L_0x010c
            com.paypal.android.a.a.e r11 = r6.b
            java.math.BigDecimal r11 = r11.a()
            if (r11 == 0) goto L_0x010c
            com.paypal.android.a.a.e r10 = r6.b
            java.math.BigDecimal r10 = r10.a()
        L_0x010c:
            com.paypal.android.MEP.o r11 = com.paypal.android.MEP.o.a()
            com.paypal.android.MEP.i r11 = r11.c()
            java.math.BigDecimal r11 = r11.h()
            com.paypal.android.MEP.o r12 = com.paypal.android.MEP.o.a()
            com.paypal.android.MEP.i r12 = r12.c()
            java.lang.String r12 = r12.a()
            boolean r8 = r8.equals(r12)
            if (r8 != 0) goto L_0x03cc
            com.paypal.android.a.a.e r5 = r6.a
            java.math.BigDecimal r5 = r5.a()
            com.paypal.android.a.a.j r8 = r6.c
            com.paypal.android.a.a.e r8 = r8.b
            java.math.BigDecimal r8 = r8.a()
            java.math.BigDecimal r5 = r5.subtract(r8)
            com.paypal.android.a.a.j r8 = r6.c
            com.paypal.android.a.a.e r8 = r8.a
            java.math.BigDecimal r8 = r8.a()
            r9 = 0
            r10 = r5
            r16 = r8
            r8 = r9
            r9 = r16
        L_0x014b:
            java.util.Vector r5 = r6.d
            int r5 = r5.size()
            if (r8 >= r5) goto L_0x0383
            java.util.Vector r5 = r6.d
            java.lang.Object r5 = r5.elementAt(r8)
            com.paypal.android.a.a.f r5 = (com.paypal.android.a.a.f) r5
            com.paypal.android.a.a.e r11 = r6.a
            java.lang.String r11 = r11.b()
            com.paypal.android.a.a.e r12 = r5.a
            java.lang.String r12 = r12.b()
            boolean r11 = r11.equals(r12)
            if (r11 == 0) goto L_0x0376
            com.paypal.android.a.a.e r5 = r5.a
            java.math.BigDecimal r5 = r5.a()
            java.math.BigDecimal r5 = r10.subtract(r5)
            r16 = r9
            r9 = r5
            r5 = r16
        L_0x017c:
            int r8 = r8 + 1
            r10 = r9
            r9 = r5
            goto L_0x014b
        L_0x0181:
            com.paypal.android.a.f r4 = com.paypal.android.MEP.PayPalActivity.a
            java.util.Hashtable r4 = r4.e()
            r6 = r4
            goto L_0x000e
        L_0x018a:
            r5 = 0
            java.lang.Object r5 = r4.elementAt(r5)
            com.paypal.android.a.a.g r5 = (com.paypal.android.a.a.g) r5
            java.lang.String r7 = "FundingPlanId"
            java.lang.Object r6 = r6.get(r7)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ Exception -> 0x01a8 }
            int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ Exception -> 0x01a8 }
            java.lang.Object r6 = r4.elementAt(r6)     // Catch:{ Exception -> 0x01a8 }
            r0 = r6
            com.paypal.android.a.a.g r0 = (com.paypal.android.a.a.g) r0     // Catch:{ Exception -> 0x01a8 }
            r5 = r0
            r6 = r5
            goto L_0x0030
        L_0x01a8:
            r6 = move-exception
            r6 = r5
            goto L_0x0030
        L_0x01ac:
            r13 = 0
            goto L_0x007d
        L_0x01af:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            com.paypal.android.a.a.b r5 = r5.b
            java.lang.String r5 = r5.a()
            java.lang.StringBuilder r5 = r11.append(r5)
            java.lang.String r11 = " ("
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.StringBuilder r5 = r5.append(r8)
            java.lang.String r11 = ") :"
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.a(r5)
            goto L_0x00ae
        L_0x01d7:
            java.lang.String r14 = "BANK_DELAYED"
            boolean r14 = r10.equals(r14)
            if (r14 == 0) goto L_0x02b7
            if (r11 == 0) goto L_0x01e7
            int r14 = r11.length()
            if (r14 > 0) goto L_0x0245
        L_0x01e7:
            if (r13 == 0) goto L_0x0217
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            com.paypal.android.a.a.b r5 = r5.b
            java.lang.String r5 = r5.a()
            java.lang.StringBuilder r5 = r11.append(r5)
            java.lang.String r11 = " ("
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = "ANDROID_echeck"
            java.lang.String r11 = com.paypal.android.a.d.a(r11)
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = "):"
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.a(r5)
            goto L_0x00ae
        L_0x0217:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            com.paypal.android.a.a.b r5 = r5.b
            java.lang.String r5 = r5.a()
            java.lang.StringBuilder r5 = r11.append(r5)
            java.lang.String r11 = " ("
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = "ANDROID_echeck"
            java.lang.String r11 = com.paypal.android.a.d.a(r11)
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = ") :"
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.a(r5)
            goto L_0x00ae
        L_0x0245:
            if (r13 == 0) goto L_0x027f
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            com.paypal.android.a.a.b r5 = r5.b
            java.lang.String r5 = r5.a()
            java.lang.StringBuilder r5 = r13.append(r5)
            java.lang.String r13 = " x"
            java.lang.StringBuilder r5 = r5.append(r13)
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = " ("
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = "ANDROID_echeck"
            java.lang.String r11 = com.paypal.android.a.d.a(r11)
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = "):"
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.a(r5)
            goto L_0x00ae
        L_0x027f:
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            com.paypal.android.a.a.b r5 = r5.b
            java.lang.String r5 = r5.a()
            java.lang.StringBuilder r5 = r13.append(r5)
            java.lang.String r13 = " x"
            java.lang.StringBuilder r5 = r5.append(r13)
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = " ("
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = "ANDROID_echeck"
            java.lang.String r11 = com.paypal.android.a.d.a(r11)
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = ") :"
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.a(r5)
            goto L_0x00ae
        L_0x02b7:
            java.lang.String r13 = "BANK_INSTANT"
            boolean r13 = r10.equals(r13)
            if (r13 == 0) goto L_0x030d
            if (r11 == 0) goto L_0x02c7
            int r13 = r11.length()
            if (r13 > 0) goto L_0x02e5
        L_0x02c7:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            com.paypal.android.a.a.b r5 = r5.b
            java.lang.String r5 = r5.a()
            java.lang.StringBuilder r5 = r11.append(r5)
            java.lang.String r11 = ":"
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.a(r5)
            goto L_0x00ae
        L_0x02e5:
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            com.paypal.android.a.a.b r5 = r5.b
            java.lang.String r5 = r5.a()
            java.lang.StringBuilder r5 = r13.append(r5)
            java.lang.String r13 = " x"
            java.lang.StringBuilder r5 = r5.append(r13)
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = ":"
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.a(r5)
            goto L_0x00ae
        L_0x030d:
            java.lang.String r13 = "CREDITCARD"
            boolean r13 = r10.equals(r13)
            if (r13 != 0) goto L_0x031d
            java.lang.String r13 = "DEBITCARD"
            boolean r13 = r10.equals(r13)
            if (r13 == 0) goto L_0x036b
        L_0x031d:
            if (r11 == 0) goto L_0x0325
            int r13 = r11.length()
            if (r13 > 0) goto L_0x0343
        L_0x0325:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            com.paypal.android.a.a.b r5 = r5.b
            java.lang.String r5 = r5.a()
            java.lang.StringBuilder r5 = r11.append(r5)
            java.lang.String r11 = ":"
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.a(r5)
            goto L_0x00ae
        L_0x0343:
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            com.paypal.android.a.a.b r5 = r5.b
            java.lang.String r5 = r5.a()
            java.lang.StringBuilder r5 = r13.append(r5)
            java.lang.String r13 = " x"
            java.lang.StringBuilder r5 = r5.append(r13)
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r11 = ":"
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r5 = r5.toString()
            r12.a(r5)
            goto L_0x00ae
        L_0x036b:
            com.paypal.android.a.a.b r5 = r5.b
            java.lang.String r5 = r5.a()
            r12.a(r5)
            goto L_0x00ae
        L_0x0376:
            com.paypal.android.a.a.e r5 = r5.a
            java.math.BigDecimal r5 = r5.a()
            java.math.BigDecimal r5 = r9.subtract(r5)
            r9 = r10
            goto L_0x017c
        L_0x0383:
            java.math.BigDecimal r5 = java.math.BigDecimal.ZERO
            int r5 = r10.compareTo(r5)
            if (r5 != 0) goto L_0x0393
            java.math.BigDecimal r5 = java.math.BigDecimal.ZERO
            int r5 = r9.compareTo(r5)
            if (r5 == 0) goto L_0x03ca
        L_0x0393:
            r5 = 1
        L_0x0394:
            if (r5 == 0) goto L_0x03c5
            com.paypal.android.a.j r5 = com.paypal.android.a.j.HELVETICA_12_NORMAL
            r0 = r5
            r1 = r18
            android.widget.TextView r5 = com.paypal.android.a.e.a(r0, r1)
            android.widget.LinearLayout$LayoutParams r8 = new android.widget.LinearLayout$LayoutParams
            r9 = -1
            r10 = -2
            r11 = 1056964608(0x3f000000, float:0.5)
            r8.<init>(r9, r10, r11)
            r5.setLayoutParams(r8)
            r8 = 2
            r9 = 2
            r10 = 2
            r11 = 2
            r5.setPadding(r8, r9, r10, r11)
            r8 = 0
            r5.setBackgroundColor(r8)
            java.lang.String r8 = "ANDROID_negative_balance"
            java.lang.String r8 = com.paypal.android.a.d.a(r8)
            r5.setText(r8)
            r0 = r19
            r1 = r5
            r0.addView(r1)
        L_0x03c5:
            int r5 = r7 + 1
            r7 = r5
            goto L_0x0032
        L_0x03ca:
            r5 = 0
            goto L_0x0394
        L_0x03cc:
            java.math.BigDecimal r8 = r11.add(r10)
            int r8 = r9.compareTo(r8)
            if (r8 <= 0) goto L_0x0394
            r5 = 1
            goto L_0x0394
        L_0x03d8:
            com.paypal.android.a.a.j r5 = r6.c
            if (r5 == 0) goto L_0x0433
            java.lang.String r7 = r5.a()
            float r7 = java.lang.Float.parseFloat(r7)
            com.paypal.android.b.c r8 = new com.paypal.android.b.c
            com.paypal.android.a.j r9 = com.paypal.android.a.j.HELVETICA_12_NORMAL
            com.paypal.android.a.j r10 = com.paypal.android.a.j.HELVETICA_12_NORMAL
            r0 = r8
            r1 = r18
            r2 = r9
            r3 = r10
            r0.<init>(r1, r2, r3)
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "1 "
            java.lang.StringBuilder r9 = r9.append(r10)
            com.paypal.android.a.a.e r10 = r5.a
            java.lang.String r10 = r10.b()
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = " = "
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r7 = r9.append(r7)
            java.lang.String r9 = " "
            java.lang.StringBuilder r7 = r7.append(r9)
            com.paypal.android.a.a.e r5 = r5.b
            java.lang.String r5 = r5.b()
            java.lang.StringBuilder r5 = r7.append(r5)
            java.lang.String r5 = r5.toString()
            r8.a(r5)
            java.lang.String r5 = ""
            r8.b(r5)
            r0 = r19
            r1 = r8
            r0.addView(r1)
        L_0x0433:
            r5 = 1
            if (r4 == 0) goto L_0x0643
            int r7 = r4.size()
            if (r7 <= 0) goto L_0x0643
            int r4 = r4.size()
            r5 = 1
            if (r4 <= r5) goto L_0x046f
            r4 = 1
        L_0x0444:
            java.util.Vector r5 = r6.d
            r7 = 0
            java.lang.Object r18 = r5.elementAt(r7)
            com.paypal.android.a.a.f r18 = (com.paypal.android.a.a.f) r18
            r0 = r18
            com.paypal.android.a.a.b r0 = r0.b
            r5 = r0
            java.lang.String r5 = r5.c()
            java.lang.String r7 = "BALANCE"
            boolean r5 = r5.equals(r7)
            if (r5 == 0) goto L_0x0468
            java.util.Vector r5 = r6.d
            int r5 = r5.size()
            r6 = 1
            if (r5 != r6) goto L_0x0468
            r4 = 0
        L_0x0468:
            r0 = r17
            r1 = r4
            r0.a(r1)
        L_0x046e:
            return
        L_0x046f:
            r4 = 0
            goto L_0x0444
        L_0x0471:
            r0 = r17
            com.paypal.android.MEP.b.h r0 = r0.i
            r4 = r0
            com.paypal.android.MEP.b.h r5 = com.paypal.android.MEP.b.h.PAYMENT_DETAILS_FEES
            if (r4 != r5) goto L_0x051e
            java.lang.String r4 = "FundingPlanId"
            java.lang.Object r4 = r6.get(r4)     // Catch:{ Exception -> 0x0640 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Exception -> 0x0640 }
            int r5 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x0640 }
            java.lang.String r4 = "FundingPlans"
            java.lang.Object r4 = r6.get(r4)     // Catch:{ Exception -> 0x0640 }
            java.util.Vector r4 = (java.util.Vector) r4     // Catch:{ Exception -> 0x0640 }
            if (r4 == 0) goto L_0x0496
            int r7 = r4.size()     // Catch:{ Exception -> 0x0640 }
            if (r7 != 0) goto L_0x04ef
        L_0x0496:
            java.lang.String r4 = "DefaultFundingPlan"
            java.lang.Object r4 = r6.get(r4)     // Catch:{ Exception -> 0x0640 }
            com.paypal.android.a.a.g r4 = (com.paypal.android.a.a.g) r4     // Catch:{ Exception -> 0x0640 }
        L_0x049e:
            if (r4 == 0) goto L_0x046e
            com.paypal.android.a.a.e r4 = r4.b
            if (r4 == 0) goto L_0x0500
            java.math.BigDecimal r5 = r4.a()
            java.lang.String r4 = r4.b()
            com.paypal.android.b.c r6 = new com.paypal.android.b.c
            com.paypal.android.a.j r7 = com.paypal.android.a.j.HELVETICA_14_NORMAL
            com.paypal.android.a.j r8 = com.paypal.android.a.j.HELVETICA_14_NORMAL
            r0 = r6
            r1 = r18
            r2 = r7
            r3 = r8
            r0.<init>(r1, r2, r3)
            java.math.BigDecimal r7 = java.math.BigDecimal.ZERO
            int r7 = r5.compareTo(r7)
            if (r7 <= 0) goto L_0x04f6
            java.lang.String r7 = "ANDROID_i_pay"
            java.lang.String r7 = com.paypal.android.a.d.a(r7)
            r6.a(r7)
        L_0x04cb:
            java.lang.String r4 = com.paypal.android.a.d.a(r5, r4)
            r6.b(r4)
            r0 = r19
            r1 = r6
            r0.addView(r1)
            java.math.BigDecimal r4 = java.math.BigDecimal.ZERO
            int r4 = r5.compareTo(r4)
            if (r4 > 0) goto L_0x04e7
            r4 = 0
            r0 = r17
            r1 = r4
            r0.a(r1)
        L_0x04e7:
            r4 = 0
            r0 = r17
            r1 = r4
            r0.a(r1)
            goto L_0x046e
        L_0x04ef:
            java.lang.Object r4 = r4.elementAt(r5)     // Catch:{ Exception -> 0x0640 }
            com.paypal.android.a.a.g r4 = (com.paypal.android.a.a.g) r4     // Catch:{ Exception -> 0x0640 }
            goto L_0x049e
        L_0x04f6:
            java.lang.String r7 = "ANDROID_free"
            java.lang.String r7 = com.paypal.android.a.d.a(r7)
            r6.a(r7)
            goto L_0x04cb
        L_0x0500:
            com.paypal.android.b.c r4 = new com.paypal.android.b.c
            com.paypal.android.a.j r5 = com.paypal.android.a.j.HELVETICA_14_NORMAL
            com.paypal.android.a.j r6 = com.paypal.android.a.j.HELVETICA_14_NORMAL
            r0 = r4
            r1 = r18
            r2 = r5
            r3 = r6
            r0.<init>(r1, r2, r3)
            java.lang.String r5 = "ANDROID_free"
            java.lang.String r5 = com.paypal.android.a.d.a(r5)
            r4.a(r5)
            r0 = r19
            r1 = r4
            r0.addView(r1)
            goto L_0x04e7
        L_0x051e:
            r0 = r17
            com.paypal.android.MEP.b.h r0 = r0.i
            r4 = r0
            com.paypal.android.MEP.b.h r5 = com.paypal.android.MEP.b.h.PAYMENT_DETAILS_SHIPPING
            if (r4 != r5) goto L_0x046e
            java.lang.String r4 = "AvailableAddresses"
            java.lang.Object r4 = r6.get(r4)
            java.util.Vector r4 = (java.util.Vector) r4
            java.lang.String r5 = "ShippingAddressId"
            java.lang.Object r5 = r6.get(r5)
            java.lang.String r5 = (java.lang.String) r5
            if (r4 == 0) goto L_0x046e
            int r6 = r4.size()
            if (r6 <= 0) goto L_0x046e
            r6 = 0
            r7 = 0
        L_0x0541:
            int r8 = r4.size()
            if (r7 >= r8) goto L_0x055a
            java.lang.Object r6 = r4.elementAt(r7)
            com.paypal.android.a.a.i r6 = (com.paypal.android.a.a.i) r6
            java.lang.String r8 = r6.h()
            boolean r8 = r8.equals(r5)
            if (r8 != 0) goto L_0x055a
            int r7 = r7 + 1
            goto L_0x0541
        L_0x055a:
            r5 = r6
            if (r5 == 0) goto L_0x046e
            java.lang.String r6 = r5.a()
            java.lang.String r7 = r5.d()
            java.lang.String r8 = r5.e()
            java.lang.String r9 = r5.b()
            java.lang.String r10 = r5.g()
            java.lang.String r5 = r5.f()
            com.paypal.android.b.c r11 = new com.paypal.android.b.c
            com.paypal.android.a.j r12 = com.paypal.android.a.j.HELVETICA_14_NORMAL
            com.paypal.android.a.j r13 = com.paypal.android.a.j.HELVETICA_14_NORMAL
            r0 = r11
            r1 = r18
            r2 = r12
            r3 = r13
            r0.<init>(r1, r2, r3)
            java.lang.StringBuffer r12 = new java.lang.StringBuffer
            r12.<init>()
            if (r6 == 0) goto L_0x05a6
            int r13 = r6.length()
            if (r13 <= 0) goto L_0x05a6
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.StringBuilder r6 = r13.append(r6)
            java.lang.String r13 = "\n"
            java.lang.StringBuilder r6 = r6.append(r13)
            java.lang.String r6 = r6.toString()
            r12.append(r6)
        L_0x05a6:
            if (r7 == 0) goto L_0x05be
            int r6 = r7.length()
            if (r6 <= 0) goto L_0x05be
            r12.append(r7)
            if (r8 == 0) goto L_0x05be
            int r6 = r8.length()
            if (r6 <= 0) goto L_0x05be
            java.lang.String r6 = ", "
            r12.append(r6)
        L_0x05be:
            if (r8 == 0) goto L_0x05c9
            int r6 = r8.length()
            if (r6 <= 0) goto L_0x05c9
            r12.append(r8)
        L_0x05c9:
            if (r7 == 0) goto L_0x05d1
            int r6 = r7.length()
            if (r6 > 0) goto L_0x05d9
        L_0x05d1:
            if (r8 == 0) goto L_0x05de
            int r6 = r8.length()
            if (r6 <= 0) goto L_0x05de
        L_0x05d9:
            java.lang.String r6 = "\n"
            r12.append(r6)
        L_0x05de:
            if (r9 == 0) goto L_0x05fe
            int r6 = r9.length()
            if (r6 <= 0) goto L_0x05fe
            r12.append(r9)
            if (r10 == 0) goto L_0x05f1
            int r6 = r10.length()
            if (r6 > 0) goto L_0x05f9
        L_0x05f1:
            if (r5 == 0) goto L_0x05fe
            int r6 = r5.length()
            if (r6 <= 0) goto L_0x05fe
        L_0x05f9:
            java.lang.String r6 = ", "
            r12.append(r6)
        L_0x05fe:
            if (r10 == 0) goto L_0x0616
            int r6 = r10.length()
            if (r6 <= 0) goto L_0x0616
            r12.append(r10)
            if (r5 == 0) goto L_0x0616
            int r6 = r5.length()
            if (r6 <= 0) goto L_0x0616
            java.lang.String r6 = " "
            r12.append(r6)
        L_0x0616:
            if (r5 == 0) goto L_0x0621
            int r6 = r5.length()
            if (r6 <= 0) goto L_0x0621
            r12.append(r5)
        L_0x0621:
            java.lang.String r5 = r12.toString()
            r11.a(r5)
            int r4 = r4.size()
            r5 = 1
            if (r4 <= r5) goto L_0x063e
            r4 = 1
        L_0x0630:
            r0 = r17
            r1 = r4
            r0.a(r1)
            r0 = r19
            r1 = r11
            r0.addView(r1)
            goto L_0x046e
        L_0x063e:
            r4 = 0
            goto L_0x0630
        L_0x0640:
            r4 = move-exception
            goto L_0x046e
        L_0x0643:
            r4 = r5
            goto L_0x0444
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.MEP.b.d.a(android.content.Context, android.widget.LinearLayout):void");
    }

    private void c(boolean z) {
        if (z) {
            this.k.setVisibility(8);
            this.j.setVisibility(0);
            l.a();
            return;
        }
        l.b();
        this.j.setVisibility(8);
        this.k.setVisibility(0);
    }

    private void f() {
        this.f = new Vector();
        Vector vector = (Vector) (o.a().k() == 2 ? m.a : PayPalActivity.a.e()).get("FundingPlans");
        for (int i2 = 0; i2 < vector.size(); i2++) {
            g gVar = (g) vector.get(i2);
            f fVar = (f) gVar.d.get(0);
            com.paypal.android.a.a.b bVar = fVar.b;
            String c = bVar.c();
            String a = bVar.a();
            String str = c.equals("BALANCE") ? a + "(" + fVar.a.b() + ")" : ((c.equals("BANK_DELAYED") || c.equals("BANK_INSTANT") || c.equals("CREDITCARD") || c.equals("DEBITCARD")) && bVar.b() != null && bVar.b().length() > 0) ? a + " x" + bVar.b() : a;
            if (c.equals("BANK_DELAYED")) {
                str = str + " (" + com.paypal.android.a.d.a("ANDROID_echeck") + ")";
            }
            Hashtable hashtable = new Hashtable();
            hashtable.put("label", str);
            hashtable.put("plan", gVar);
            this.f.add(hashtable);
        }
        q.b();
    }

    public final void a() {
        switch (g.a[this.i.ordinal()]) {
            case 2:
                if (this.e) {
                    this.e = false;
                    PayPalActivity.a.a("delegate", this);
                    return;
                }
                return;
            case 3:
                PayPalActivity.a.a("delegate", this);
                if (this.e) {
                    this.e = false;
                    return;
                } else {
                    PayPalActivity.a.a(7);
                    return;
                }
            default:
                return;
        }
    }

    public final void a(int i2) {
        int i3;
        this.r = i2;
        if (i2 == 1) {
            switch (g.a[this.i.ordinal()]) {
                case 1:
                    Vector vector = (Vector) (o.a().k() == 2 ? m.a : PayPalActivity.a.e()).get("FundingPlans");
                    if (vector == null || vector.size() == 0) {
                        c(true);
                        PayPalActivity.a.a("delegate", this);
                        PayPalActivity.a.a(5);
                        return;
                    }
                    f();
                    super.a(i2);
                    return;
                case 2:
                    c();
                    TextView a = e.a(j.HELVETICA_14_NORMAL, getContext());
                    a.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                    a.setBackgroundColor(0);
                    a.setText(com.paypal.android.a.d.a("ANDROID_choose_who_pays_the_fee") + ":");
                    a.setTextColor(-1);
                    a(a);
                    if (((String) ((Hashtable) ((Vector) PayPalActivity.a.e().get("PricingDetails")).get(0)).get("FeeBearer")).compareTo("ApplyFeeToReceiver") == 0) {
                        Button a2 = a(getContext(), com.paypal.android.a.d.a("ANDROID_i_pay"), 0);
                        int id = a2.getId();
                        a(a2);
                        a2.setNextFocusUpId(getId());
                        i3 = id;
                    } else {
                        Button a3 = a(getContext(), com.paypal.android.a.d.a("NDROID_recipient_pays"), 1);
                        int id2 = a3.getId();
                        a(a3);
                        a3.setNextFocusUpId(getId());
                        i3 = id2;
                    }
                    setNextFocusDownId(i3);
                    if (this.s != null) {
                        this.s.a(this, i3);
                    }
                    super.a(i2);
                    return;
                case 3:
                    if (o.a().k() != 2) {
                        if (((Vector) PayPalActivity.a.e().get("AvailableAddresses")) == null) {
                            this.f = null;
                        } else {
                            this.f = new Vector();
                        }
                    } else if (((Vector) m.a.get("AvailableAddresses")) == null) {
                        this.f = null;
                    } else {
                        this.f = new Vector();
                    }
                    q.b();
                    super.a(i2);
                    return;
                default:
                    return;
            }
        } else {
            c();
            this.f = null;
            super.a(i2);
        }
    }

    public final void a(int i2, Object obj) {
        q = false;
        switch (i2) {
            case 3:
                PayPalActivity.a.a(6);
                return;
            case 4:
            default:
                return;
            case 5:
                f();
                if (((Vector) PayPalActivity.a.d("FundingPlans")).size() == 1) {
                    a(false);
                    PayPalActivity.a().sendBroadcast(new Intent(PayPalActivity.g));
                    return;
                }
                return;
            case 6:
                try {
                    if (o.a().k() != 2) {
                        m.a = PayPalActivity.a.e();
                    }
                    PayPalActivity.a().sendBroadcast(new Intent(PayPalActivity.g));
                    return;
                } catch (Throwable th) {
                    return;
                }
        }
    }

    public final void a(i iVar) {
        this.s = iVar;
    }

    public final void a(String str) {
        this.u = str;
        q = false;
        q.b();
    }

    public final void a(String str, Object obj) {
    }

    public final void b(boolean z) {
        if (z) {
            setBackgroundDrawable(this.h);
        } else {
            setBackgroundDrawable(this.g);
        }
    }

    public final void d() {
        int i2;
        int i3;
        int i4;
        boolean z;
        int i5;
        boolean z2;
        int i6 = 0;
        if (this.u != null) {
            this.n.a(this.u);
            this.u = null;
            c(false);
        }
        if (this.f != null) {
            c(false);
            super.a(1);
            c();
            switch (g.a[this.i.ordinal()]) {
                case 1:
                    TextView a = e.a(j.HELVETICA_14_NORMAL, getContext());
                    a.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                    a.setBackgroundColor(0);
                    a.setText(com.paypal.android.a.d.a("ANDROID_change_funding") + ":");
                    a.setTextColor(-1);
                    if (this.f.size() > 0) {
                        a(a);
                    }
                    int i7 = 0;
                    boolean z3 = false;
                    int i8 = 0;
                    while (i7 < this.f.size()) {
                        try {
                            Hashtable hashtable = (Hashtable) this.f.get(i7);
                            g gVar = (g) hashtable.get("plan");
                            if (!((String) (o.a().k() == 2 ? m.a : PayPalActivity.a.e()).get("FundingPlanId")).equals(gVar.a())) {
                                Button a2 = a(getContext(), (String) hashtable.get("label"), i8);
                                int id = a2.getId();
                                this.p.add(gVar.a());
                                a(a2);
                                if (!z3) {
                                    setNextFocusDownId(id);
                                    z2 = true;
                                } else {
                                    z2 = z3;
                                }
                                int i9 = id;
                                i5 = i8 + 1;
                                z = z2;
                                i4 = i9;
                            } else {
                                i4 = i6;
                                z = z3;
                                i5 = i8;
                            }
                        } catch (Throwable th) {
                            i4 = i6;
                            z = z3;
                            i5 = i8;
                        }
                        i7++;
                        i6 = i4;
                        z3 = z;
                        i8 = i5;
                    }
                    if (!(i6 == 0 || this.s == null)) {
                        this.s.a(this, i6);
                    }
                    if (i8 == 0) {
                        a.setText(" ");
                        return;
                    }
                    return;
                case 2:
                default:
                    return;
                case 3:
                    Hashtable e2 = o.a().k() == 2 ? m.a : PayPalActivity.a.e();
                    Vector vector = (Vector) e2.get("AvailableAddresses");
                    String str = (String) e2.get("ShippingAddressId");
                    if (vector != null && vector.size() > 1) {
                        TextView a3 = e.a(j.HELVETICA_14_NORMAL, getContext());
                        a3.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                        a3.setBackgroundColor(0);
                        if (o.a().l() == 1) {
                            a3.setText(com.paypal.android.a.d.a("ANDROID_change_mailing_address_to") + ":");
                        } else {
                            a3.setText(com.paypal.android.a.d.a("ANDROID_change_shipping_to") + ":");
                        }
                        a3.setTextColor(-1);
                        a(a3);
                        this.o.removeAllElements();
                        int i10 = 0;
                        int i11 = 0;
                        while (i10 < vector.size()) {
                            com.paypal.android.a.a.i iVar = (com.paypal.android.a.a.i) vector.elementAt(i10);
                            try {
                                String d = iVar.d();
                                String e3 = iVar.e();
                                String b = iVar.b();
                                String g2 = iVar.g();
                                String f2 = iVar.f();
                                String str2 = d + (d.length() > 0 ? "\n" : "");
                                if (e3 != null) {
                                    str2 = str2 + e3 + (e3.length() > 0 ? "\n" : "");
                                }
                                String str3 = str2 + b;
                                if (g2 != null) {
                                    str3 = str3 + ", " + g2;
                                }
                                if (f2 != null) {
                                    str3 = str3 + " " + f2;
                                }
                                if (str == null || !str.equals(iVar.h())) {
                                    Button a4 = a(getContext(), str3, i11);
                                    int id2 = a4.getId();
                                    this.o.add(iVar.h());
                                    a(a4);
                                    if (i6 == 0) {
                                        setNextFocusDownId(id2);
                                        i2 = 1;
                                    } else {
                                        i2 = i6;
                                    }
                                    i3 = i11 + 1;
                                    i10++;
                                    i11 = i3;
                                    i6 = i2;
                                } else {
                                    i2 = i6;
                                    i3 = i11;
                                    i10++;
                                    i11 = i3;
                                    i6 = i2;
                                }
                            } catch (Throwable th2) {
                                i2 = i6;
                                i3 = i11;
                            }
                        }
                        if (this.f.size() == 0) {
                            View view = new View(getContext());
                            view.setMinimumWidth(10);
                            view.setMinimumHeight(10);
                            a(view);
                        }
                        if (i11 == 0) {
                            a3.setText(" ");
                            return;
                        }
                        return;
                    }
                    return;
            }
        }
    }

    public final h e() {
        return this.i;
    }

    public final void onClick(View view) {
        if (this.r == 0) {
            this.n.e();
        }
        super.onClick(view);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (!isClickable()) {
                    return false;
                }
                setBackgroundDrawable(this.h);
                return false;
            default:
                setBackgroundDrawable(this.g);
                return false;
        }
    }
}
