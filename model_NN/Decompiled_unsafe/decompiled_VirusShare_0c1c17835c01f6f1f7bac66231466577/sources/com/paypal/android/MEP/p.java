package com.paypal.android.MEP;

import java.math.BigDecimal;

public class p {
    private String a;
    private BigDecimal b;
    private BigDecimal c;
    private BigDecimal d;

    public BigDecimal a() {
        return this.b;
    }

    public void a(String str) {
        this.a = str;
    }

    public void a(BigDecimal bigDecimal) {
        this.b = bigDecimal;
    }

    public BigDecimal b() {
        return this.c;
    }

    public void b(BigDecimal bigDecimal) {
        this.c = bigDecimal;
    }

    public BigDecimal c() {
        return this.d;
    }

    public void c(BigDecimal bigDecimal) {
        this.d = bigDecimal;
    }
}
