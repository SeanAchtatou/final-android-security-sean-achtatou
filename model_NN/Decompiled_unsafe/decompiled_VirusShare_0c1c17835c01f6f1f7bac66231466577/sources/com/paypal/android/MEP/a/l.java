package com.paypal.android.MEP.a;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.b.a;
import com.paypal.android.MEP.o;
import com.paypal.android.a.d;
import com.paypal.android.a.i;
import com.paypal.android.b.c;
import com.paypal.android.b.f;
import com.paypal.android.b.h;
import com.paypal.android.b.j;
import com.paypal.android.b.p;

public final class l extends j implements View.OnClickListener, p {
    public static String a = null;
    private a b;
    private Button c;

    public l(Context context) {
        super(context);
    }

    public final void a(Context context) {
        o a2 = o.a();
        super.a(context);
        LinearLayout a3 = i.a(context, -1, -2);
        a3.setOrientation(1);
        a3.setPadding(5, 5, 5, 15);
        this.b = new a(context, this);
        this.b.a(this);
        a3.addView(this.b);
        addView(a3);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        linearLayout.setPadding(5, 5, 5, 5);
        linearLayout.setBackgroundDrawable(i.a());
        if (a2.l() == 1) {
            linearLayout.addView(new h(d.a("ANDROID_donation_made"), context));
        } else {
            linearLayout.addView(new h(d.a("ANDROID_payment_made"), context));
        }
        c cVar = new c(context, com.paypal.android.a.j.HELVETICA_16_NORMAL, com.paypal.android.a.j.HELVETICA_16_NORMAL);
        cVar.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        String a4 = d.a("ANDROID_successfully_paid_amount_to_recipient");
        if (a2.l() == 1) {
            a4 = d.a("ANDROID_successfully_donated_amount_to_recipient");
        }
        cVar.a(a4.replace("{1}", a2.c().h().toString()) + ".");
        linearLayout.addView(cVar);
        LinearLayout a5 = i.a(context, -1, -2);
        a5.setOrientation(1);
        a5.setGravity(1);
        this.c = new Button(context);
        this.c.setText(d.a("ANDROID_done"));
        this.c.setLayoutParams(new RelativeLayout.LayoutParams(-2, i.b()));
        this.c.setBackgroundDrawable(com.paypal.android.a.h.a());
        this.c.setTextColor(-16777216);
        this.c.setOnClickListener(this);
        a5.addView(this.c);
        linearLayout.addView(a5);
        addView(linearLayout);
    }

    public final void a(f fVar, int i) {
    }

    public final void b() {
    }

    public final void c() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.PayPalActivity.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.MEP.PayPalActivity.a(com.paypal.android.MEP.k, java.lang.String, java.util.Vector):java.util.Vector
      com.paypal.android.MEP.PayPalActivity.a(java.lang.String, java.lang.String, boolean):void */
    public final void onClick(View view) {
        if (view == this.c) {
            if (a == null || a.length() == 0) {
                a = "1111111";
            }
            PayPalActivity.a().a((String) PayPalActivity.a.d("PayKey"), (String) PayPalActivity.a.d("PaymentExecStatus"), true);
        }
    }
}
