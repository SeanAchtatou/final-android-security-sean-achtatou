package com.paypal.android.MEP.a;

import android.webkit.WebView;
import android.webkit.WebViewClient;

final class o extends WebViewClient {
    private /* synthetic */ h a;

    /* synthetic */ o(h hVar) {
        this(hVar, (byte) 0);
    }

    private o(h hVar, byte b) {
        this.a = hVar;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (!str.equals("About.Quick.Pay")) {
            return true;
        }
        this.a.onClick(this.a.p);
        return true;
    }
}
