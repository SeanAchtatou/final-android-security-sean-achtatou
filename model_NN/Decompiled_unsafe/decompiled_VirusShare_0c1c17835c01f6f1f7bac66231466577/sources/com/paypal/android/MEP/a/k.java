package com.paypal.android.MEP.a;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.b;
import com.paypal.android.MEP.b.a;
import com.paypal.android.a.e;
import com.paypal.android.a.i;
import com.paypal.android.b.d;
import com.paypal.android.b.f;
import com.paypal.android.b.h;
import com.paypal.android.b.j;
import com.paypal.android.b.m;
import com.paypal.android.b.o;
import com.paypal.android.b.p;
import java.util.Hashtable;

public final class k extends j implements TextWatcher, View.OnClickListener, b, p {
    public static String a = null;
    /* access modifiers changed from: private */
    public static d l = null;
    private b b;
    private String c;
    private LinearLayout d;
    private RelativeLayout e;
    private a f;
    private EditText g;
    private EditText h;
    private EditText i;
    private Button j;
    private Button k;
    private TextView m;
    private Hashtable n;
    private m o;
    private LinearLayout p;
    private Context q;

    public k(Context context) {
        super(context);
    }

    private void a(String str, boolean z) {
        LinearLayout a2 = i.a(this.q, -1, -2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2.getLayoutParams());
        layoutParams.addRule(13);
        a2.setLayoutParams(layoutParams);
        a2.setOrientation(1);
        a2.setGravity(1);
        if (l == null) {
            l = new d(this.q);
        } else {
            ((LinearLayout) l.getParent()).removeAllViews();
        }
        this.m = e.a(com.paypal.android.a.j.HELVETICA_16_NORMAL, this.q);
        this.m.setGravity(1);
        this.m.setTextColor(-13408615);
        this.m.setText(str);
        if (z) {
            LinearLayout a3 = i.a(this.q, -2, -2);
            a3.setOrientation(1);
            a3.setPadding(5, 10, 5, 10);
            m mVar = new m(this.q, o.GREEN_ALERT);
            mVar.a(com.paypal.android.a.d.a("ANDROID_pin_success"));
            mVar.setPadding(5, 5, 5, 5);
            a3.addView(mVar);
            a2.addView(a3);
        }
        a2.addView(l);
        a2.addView(this.m);
        this.e.removeAllViews();
        this.e.addView(a2);
    }

    private boolean e() {
        if (this.g.getText().toString().length() < 9) {
            return false;
        }
        return com.paypal.android.a.d.e(this.g.getText().toString());
    }

    private boolean f() {
        String obj = this.h.getText().toString();
        String obj2 = this.i.getText().toString();
        if (obj.length() < 4 || obj.length() > 8 || obj2.length() < 4 || obj2.length() > 8 || !obj.equals(obj2)) {
            return false;
        }
        return com.paypal.android.a.d.f(obj);
    }

    private void g() {
        try {
            ((InputMethodManager) PayPalActivity.a().getSystemService("input_method")).hideSoftInputFromWindow(this.g.getWindowToken(), 0);
        } catch (Exception e2) {
        }
        try {
            ((InputMethodManager) PayPalActivity.a().getSystemService("input_method")).hideSoftInputFromWindow(this.h.getWindowToken(), 0);
        } catch (Exception e3) {
        }
        try {
            ((InputMethodManager) PayPalActivity.a().getSystemService("input_method")).hideSoftInputFromWindow(this.i.getWindowToken(), 0);
        } catch (Exception e4) {
        }
    }

    public final void a() {
        PayPalActivity.a.a("NewPhone", this.n.get("mobileNumber"));
        PayPalActivity.a.a("NewPin", this.n.get("newPIN"));
        PayPalActivity.a.a("delegate", this);
        PayPalActivity.a.a(11);
    }

    public final void a(int i2, Object obj) {
        this.b = b.STATE_PIN_SUCCESS;
        q.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.a.k.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.paypal.android.MEP.a.k.a(int, java.lang.Object):void
      com.paypal.android.MEP.a.k.a(com.paypal.android.b.f, int):void
      com.paypal.android.MEP.a.k.a(java.lang.String, java.lang.Object):void
      com.paypal.android.MEP.b.a(int, java.lang.Object):void
      com.paypal.android.MEP.b.a(java.lang.String, java.lang.Object):void
      com.paypal.android.b.p.a(com.paypal.android.b.f, int):void
      com.paypal.android.MEP.a.k.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.b.a.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.paypal.android.b.f.a(android.widget.LinearLayout$LayoutParams, int):void
      com.paypal.android.MEP.b.a.a(boolean, boolean):void */
    public final void a(Context context) {
        super.a(context);
        this.q = context;
        this.b = b.STATE_NORMAL;
        LinearLayout a2 = i.a(context, -1, -2);
        a2.setOrientation(1);
        a2.setPadding(5, 5, 5, 15);
        a2.addView(e.b(com.paypal.android.a.j.HELVETICA_16_BOLD, context));
        this.f = new a(context, this);
        this.f.a(this);
        a2.addView(this.f);
        addView(a2);
        this.d = i.a(context, -1, -1);
        this.d.setOrientation(1);
        this.d.setPadding(10, 5, 10, 5);
        this.d.setBackgroundDrawable(i.a());
        this.d.addView(new h(com.paypal.android.a.d.a("ANDROID_payment_made"), context));
        this.p = i.a(context, -1, -2);
        this.p.setOrientation(1);
        this.p.setPadding(5, 10, 5, 10);
        this.o = new m(context, o.YELLOW_ALERT);
        this.o.a("This page is currently being used to test components.");
        this.o.setPadding(0, 5, 0, 5);
        this.p.setVisibility(8);
        this.p.addView(this.o);
        this.d.addView(this.p);
        LinearLayout a3 = i.a(context, -1, -2);
        a3.setOrientation(1);
        a3.setPadding(5, 0, 5, 5);
        TextView a4 = e.a(com.paypal.android.a.j.HELVETICA_16_BOLD, context);
        a4.setTextColor(-14993820);
        a4.setText(com.paypal.android.a.d.a("ANDROID_create_a_pin"));
        a4.setGravity(3);
        a3.addView(a4);
        this.d.addView(a3);
        LinearLayout a5 = i.a(context, -1, -2);
        a5.setOrientation(1);
        a5.setPadding(10, 10, 10, 5);
        a5.setBackgroundDrawable(i.a());
        this.g = new EditText(context);
        this.g.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.g.setInputType(3);
        this.g.setHint(com.paypal.android.a.d.a("ANDROID_enter_mobile"));
        this.g.setSingleLine(true);
        this.g.addTextChangedListener(this);
        a5.addView(this.g);
        this.h = new EditText(context);
        this.h.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.h.setInputType(3);
        this.h.setHint(com.paypal.android.a.d.a("ANDROID_enter_pin"));
        this.h.setSingleLine(true);
        this.h.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.h.addTextChangedListener(this);
        a5.addView(this.h);
        this.i = new EditText(context);
        this.i.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.i.setInputType(3);
        this.i.setHint(com.paypal.android.a.d.a("ANDROID_reenter_pin"));
        this.i.setSingleLine(true);
        this.i.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.i.addTextChangedListener(this);
        a5.addView(this.i);
        this.d.addView(a5);
        LinearLayout a6 = i.a(context, -1, -2);
        a6.setGravity(1);
        a6.setOrientation(1);
        a6.setPadding(5, 10, 5, 5);
        this.j = new Button(context);
        this.j.setLayoutParams(new RelativeLayout.LayoutParams(-1, i.b()));
        this.j.setText(com.paypal.android.a.d.a("ANDROID_create_pin"));
        this.j.setTextColor(-16777216);
        this.j.setBackgroundDrawable(com.paypal.android.a.h.a());
        this.j.setOnClickListener(this);
        this.j.setEnabled(false);
        a6.addView(this.j);
        this.d.addView(a6);
        LinearLayout a7 = i.a(context, -1, -2);
        a7.setGravity(1);
        a7.setOrientation(1);
        a7.setPadding(5, 5, 5, 10);
        this.k = new Button(context);
        this.k.setLayoutParams(new RelativeLayout.LayoutParams(-1, i.b()));
        this.k.setText(com.paypal.android.a.d.a("ANDROID_skip"));
        this.k.setTextColor(-16777216);
        this.k.setBackgroundDrawable(com.paypal.android.a.h.b());
        this.k.setOnClickListener(this);
        a7.addView(this.k);
        this.d.addView(a7);
        addView(this.d);
        this.e = new RelativeLayout(context);
        this.e.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.e.setBackgroundDrawable(i.a());
        a(com.paypal.android.a.d.a("ANDROID_creating_pin"), false);
        this.e.setVisibility(8);
        addView(this.e);
        this.n = new Hashtable();
        if (!com.paypal.android.MEP.o.a().E()) {
            this.f.a(false, false);
        }
    }

    public final void a(f fVar, int i2) {
    }

    public final void a(String str) {
        l.b();
        this.c = str;
        this.b = b.STATE_ERROR;
        this.h.setText("");
        this.i.setText("");
        q.b();
    }

    public final void a(String str, Object obj) {
        this.n.put(str, obj);
    }

    public final void afterTextChanged(Editable editable) {
        if (!e() || !f()) {
            this.j.setEnabled(false);
        } else {
            this.j.setEnabled(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.a.k.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.paypal.android.MEP.a.k.a(int, java.lang.Object):void
      com.paypal.android.MEP.a.k.a(com.paypal.android.b.f, int):void
      com.paypal.android.MEP.a.k.a(java.lang.String, java.lang.Object):void
      com.paypal.android.MEP.b.a(int, java.lang.Object):void
      com.paypal.android.MEP.b.a(java.lang.String, java.lang.Object):void
      com.paypal.android.b.p.a(com.paypal.android.b.f, int):void
      com.paypal.android.MEP.a.k.a(java.lang.String, boolean):void */
    public final void b() {
        if (this.b == b.STATE_ERROR) {
            this.d.setVisibility(0);
            this.e.setVisibility(8);
            this.o.a(this.c);
            this.p.setVisibility(0);
        } else if (this.b == b.STATE_PIN_SUCCESS) {
            l.b();
            a(com.paypal.android.a.d.a("ANDROID_returning_to_merchant"), true);
            l.a();
            new Thread(new a(this)).start();
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    public final void c() {
    }

    public final void onClick(View view) {
        if (view == this.j) {
            if (e() && f()) {
                g();
                if (com.paypal.android.MEP.o.a().k() != 2) {
                    com.paypal.android.MEP.m.a().b(this, this.g.getText().toString(), this.h.getText().toString());
                }
                this.d.setVisibility(8);
                this.e.setVisibility(0);
                l.a();
                if (com.paypal.android.MEP.o.a().k() == 2) {
                    this.b = b.STATE_PIN_SUCCESS;
                    q.b();
                }
            }
        } else if (view == this.k) {
            g();
            if (a == null || a.length() == 0) {
                a = "11111111";
            }
            PayPalActivity.a().a((String) PayPalActivity.a.d("PayKey"), (String) PayPalActivity.a.d("PaymentExecStatus"));
        }
    }

    public final void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }
}
