package com.paypal.android.MEP.a;

import android.content.Intent;
import com.paypal.android.MEP.PayPalActivity;

public class q {
    public static void a() {
        if (PayPalActivity.a() != null) {
            PayPalActivity.a().sendBroadcast(new Intent(PayPalActivity.k));
        }
    }

    public static void a(int i) {
        if (i < 0 || i >= 9) {
            throw new IllegalArgumentException("Attempted to push an unknown dialog.");
        } else if (PayPalActivity.a() != null) {
            PayPalActivity.a().sendBroadcast(new Intent(PayPalActivity.j + i));
        }
    }

    public static void b() {
        if (PayPalActivity.a() != null) {
            PayPalActivity.a().sendBroadcast(new Intent(PayPalActivity.m));
        }
    }

    public static void b(int i) {
        if (PayPalActivity.a() != null) {
            PayPalActivity.a().sendBroadcast(new Intent(PayPalActivity.l + i));
        }
    }
}
