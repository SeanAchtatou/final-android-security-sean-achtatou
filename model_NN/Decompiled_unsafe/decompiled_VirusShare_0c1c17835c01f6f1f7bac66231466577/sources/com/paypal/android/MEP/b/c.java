package com.paypal.android.MEP.b;

import android.content.Intent;
import android.view.View;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.a.d;
import com.paypal.android.MEP.a.m;
import com.paypal.android.MEP.a.q;
import com.paypal.android.MEP.o;

final class c implements View.OnClickListener {
    private /* synthetic */ d a;

    c(d dVar) {
        this.a = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.paypal.android.a.f.a(com.paypal.android.a.f, int):int
      com.paypal.android.a.f.a(java.lang.String, java.lang.String):boolean
      com.paypal.android.a.f.a(int, java.lang.Object):void
      com.paypal.android.MEP.b.a(int, java.lang.Object):void
      com.paypal.android.a.f.a(java.lang.String, java.lang.Object):void */
    public final void onClick(View view) {
        int id = view.getId();
        if (this.a.f != null || (id & 2113929216) == 2113929216) {
            if ((id & 2130706432) == 2130706432) {
                int i = id - 2130706432;
                if (this.a.f != null) {
                    (o.a().k() == 2 ? m.a : PayPalActivity.a.e()).put("FundingPlanId", this.a.p.get(i));
                    PayPalActivity.a().sendBroadcast(new Intent(PayPalActivity.g));
                }
            } else if ((id & 2113929216) == 2113929216) {
                PayPalActivity.a.a("FeeBearer", (Object) (id - 2113929216 == 0 ? "ApplyFeeToSender" : "ApplyFeeToReceiver"));
            } else {
                (o.a().k() == 2 ? m.a : PayPalActivity.a.e()).put("ShippingAddressId", (String) this.a.o.get(id - 2097152000));
                if (!(o.a().k() != 2 ? PayPalActivity.a.g() : false)) {
                    PayPalActivity.a().sendBroadcast(new Intent(PayPalActivity.g));
                } else {
                    this.a.n.a(d.STATE_UPDATING);
                }
            }
            this.a.e = true;
            this.a.a(0);
            if (o.a().k() == 2) {
                this.a.a(6, (Object) null);
            }
            q.b();
        }
    }
}
