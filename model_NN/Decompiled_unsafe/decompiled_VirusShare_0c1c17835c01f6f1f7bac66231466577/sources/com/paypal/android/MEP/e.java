package com.paypal.android.MEP;

import java.io.Serializable;
import java.math.BigDecimal;

public class e implements Serializable {
    private String a = null;
    private String b = null;
    private BigDecimal c = null;
    private BigDecimal d = null;
    private int e = 0;

    public String a() {
        return this.a;
    }

    public void a(int i) {
        this.e = i;
    }

    public void a(String str) {
        this.a = str;
    }

    public void a(BigDecimal bigDecimal) {
        this.c = bigDecimal.setScale(2, 4);
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.b = str;
    }

    public void b(BigDecimal bigDecimal) {
        this.d = bigDecimal.setScale(2, 4);
    }

    public BigDecimal c() {
        return this.c;
    }

    public BigDecimal d() {
        return this.d;
    }

    public int e() {
        return this.e;
    }

    public boolean f() {
        if (this.a != null && this.a.length() > 0) {
            return true;
        }
        if (this.b != null && this.b.length() > 0) {
            return true;
        }
        if (this.c != null && this.c.compareTo(BigDecimal.ZERO) > 0) {
            return true;
        }
        if (this.d == null || this.d.compareTo(BigDecimal.ZERO) <= 0) {
            return this.e > 0;
        }
        return true;
    }
}
