package com.paypal.android.MEP;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public final class h extends LinearLayout implements View.OnClickListener {
    private static boolean f = false;
    private boolean a;
    private TextView b;
    private TextView c;
    private LinearLayout d;
    private Context e;

    public h(Context context) {
        super(context);
        this.e = context;
    }

    public final void a(boolean z) {
        setClickable(z);
        setFocusable(z);
    }

    public final void onClick(View view) {
        if (!f && view != this.b) {
            if (view == this.c) {
                f = true;
                int t = o.a().t();
                o.a().w();
                o.a().c(t);
                com.paypal.android.MEP.a.h.a = true;
                PayPalActivity.a.a(12);
                performClick();
            } else if (view == this.d) {
                f = true;
                com.paypal.android.MEP.a.h.a = false;
                if (this.a) {
                    performClick();
                } else {
                    performClick();
                }
            } else {
                return;
            }
            a(false);
        }
    }
}
