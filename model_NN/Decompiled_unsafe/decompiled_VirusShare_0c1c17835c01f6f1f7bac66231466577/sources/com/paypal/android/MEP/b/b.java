package com.paypal.android.MEP.b;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.MEP.f;
import com.paypal.android.a.a;
import com.paypal.android.a.d;
import com.paypal.android.a.h;
import com.paypal.android.a.i;
import com.paypal.android.a.j;
import com.paypal.android.b.c;
import com.paypal.android.b.e;
import java.math.BigDecimal;

public final class b extends e implements View.OnTouchListener {
    public b(Context context, f fVar, String str) {
        super(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(3, 3, 3, 3);
        a(layoutParams, 0);
        a(layoutParams, 1);
        setBackgroundColor(0);
        a(h.a(124846, 2921));
        b(h.a(61183, 2927));
        setOnTouchListener(this);
        this.a.setPadding(5, 0, 5, 0);
        this.a.setBackgroundColor(-2763307);
        this.a.setGravity(16);
        this.a.addView(this.c);
        this.c.setVisibility(0);
        c cVar = new c(context, j.HELVETICA_14_BOLD, j.HELVETICA_14_BOLD);
        cVar.setPadding(3, 0, 0, 0);
        String h = fVar.h();
        h = (h == null || h.length() == 0) ? fVar.a() : h;
        String a = a.a(fVar.l(), str);
        cVar.a(h);
        cVar.b(a);
        this.a.addView(cVar);
        this.b.setPadding(10, 0, 0, 0);
        this.b.setOrientation(1);
        if (fVar.c() == null || fVar.c().c() == null || fVar.c().c().size() <= 0) {
            BigDecimal b = fVar.b();
            if (b != null) {
                c cVar2 = new c(context, j.HELVETICA_14_NORMAL, j.HELVETICA_14_NORMAL);
                cVar2.a(d.a("ANDROID_total"));
                cVar2.b(a.a(b, str));
                this.b.addView(cVar2);
            }
        } else {
            for (int i = 0; i < fVar.c().c().size(); i++) {
                com.paypal.android.MEP.e eVar = (com.paypal.android.MEP.e) fVar.c().c().get(i);
                String a2 = eVar.a();
                String b2 = eVar.b();
                BigDecimal c = eVar.c();
                BigDecimal d = eVar.d();
                int e = eVar.e();
                if (eVar.f()) {
                    if (a2 != null && a2.length() > 0) {
                        c cVar3 = new c(context, j.HELVETICA_14_NORMAL, j.HELVETICA_14_NORMAL);
                        cVar3.a(d.a("ANDROID_item") + ": " + eVar.a());
                        if (c == null || c.toString().length() <= 0) {
                            cVar3.b("");
                        } else {
                            cVar3.b(a.a(c, str));
                        }
                        this.b.addView(cVar3);
                    } else if (c != null && c.compareTo(BigDecimal.ZERO) > 0) {
                        c cVar4 = new c(context, j.HELVETICA_14_NORMAL, j.HELVETICA_14_NORMAL);
                        cVar4.a(d.a("ANDROID_item") + ": " + d.a("ANDROID_item") + " " + (i + 1));
                        cVar4.b(c.toString());
                        this.b.addView(cVar4);
                    }
                    if (b2 != null && b2.length() > 0) {
                        TextView a3 = com.paypal.android.a.e.a(j.HELVETICA_12_NORMAL, context);
                        a3.setText(d.a("ANDROID_item_num") + ": " + b2);
                        this.b.addView(a3);
                    }
                    if (d != null && d.compareTo(BigDecimal.ZERO) > 0) {
                        TextView a4 = com.paypal.android.a.e.a(j.HELVETICA_12_NORMAL, context);
                        a4.setText(d.a("ANDROID_item_price") + ": " + a.a(d, str));
                        this.b.addView(a4);
                    }
                    if (e > 0) {
                        TextView a5 = com.paypal.android.a.e.a(j.HELVETICA_12_NORMAL, context);
                        a5.setText(d.a("ANDROID_quantity") + ": " + e);
                        this.b.addView(a5);
                    }
                    if (i != fVar.c().c().size() - 1) {
                        LinearLayout a6 = i.a(context, 5, 5);
                        a6.setVisibility(4);
                        this.b.addView(a6);
                    }
                }
            }
        }
        BigDecimal a7 = fVar.c().a();
        if (a7 != null && a7.compareTo(BigDecimal.ZERO) > 0) {
            c cVar5 = new c(context, j.HELVETICA_14_NORMAL, j.HELVETICA_14_NORMAL);
            cVar5.a(d.a("ANDROID_tax"));
            cVar5.b(a.a(a7, str));
            this.b.addView(cVar5);
        }
        BigDecimal b3 = fVar.c().b();
        if (b3 != null && b3.compareTo(BigDecimal.ZERO) > 0) {
            c cVar6 = new c(context, j.HELVETICA_14_NORMAL, j.HELVETICA_14_NORMAL);
            cVar6.a(d.a("ANDROID_shipping"));
            cVar6.b(a.a(b3, str));
            this.b.addView(cVar6);
        }
    }

    public static LinearLayout a(Context context, f fVar, String str) {
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.gravity = 1;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setPadding(5, 0, 5, 5);
        linearLayout.setPadding(10, 0, 0, 0);
        linearLayout.setOrientation(1);
        if (fVar.c() == null || fVar.c().c() == null || fVar.c().c().size() <= 0) {
            BigDecimal b = fVar.b();
            if (b != null) {
                c cVar = new c(context, j.HELVETICA_14_NORMAL, j.HELVETICA_14_NORMAL);
                cVar.a(d.a("ANDROID_total"));
                cVar.b(a.a(b, str));
                linearLayout.addView(cVar);
            }
        } else {
            for (int i = 0; i < fVar.c().c().size(); i++) {
                com.paypal.android.MEP.e eVar = (com.paypal.android.MEP.e) fVar.c().c().get(i);
                String a = eVar.a();
                String b2 = eVar.b();
                BigDecimal c = eVar.c();
                BigDecimal d = eVar.d();
                int e = eVar.e();
                if (eVar.f()) {
                    if (a != null && a.length() > 0) {
                        c cVar2 = new c(context, j.HELVETICA_14_NORMAL, j.HELVETICA_14_NORMAL);
                        cVar2.a(d.a("ANDROID_item") + ": " + eVar.a());
                        if (c == null || c.toString().length() <= 0) {
                            cVar2.b("");
                        } else {
                            cVar2.b(a.a(c, str));
                        }
                        linearLayout.addView(cVar2);
                    } else if (c != null && c.compareTo(BigDecimal.ZERO) > 0) {
                        c cVar3 = new c(context, j.HELVETICA_14_NORMAL, j.HELVETICA_14_NORMAL);
                        cVar3.a(d.a("ANDROID_item") + ": " + d.a("ANDROID_item") + " " + (i + 1));
                        cVar3.b(c.toString());
                        linearLayout.addView(cVar3);
                    }
                    if (b2 != null && b2.length() > 0) {
                        TextView a2 = com.paypal.android.a.e.a(j.HELVETICA_12_NORMAL, context);
                        a2.setText(d.a("ANDROID_item_num") + ": " + b2);
                        linearLayout.addView(a2);
                    }
                    if (d != null && d.compareTo(BigDecimal.ZERO) > 0) {
                        TextView a3 = com.paypal.android.a.e.a(j.HELVETICA_12_NORMAL, context);
                        a3.setText(d.a("ANDROID_item_price") + ": " + a.a(d, str));
                        linearLayout.addView(a3);
                    }
                    if (e > 0) {
                        TextView a4 = com.paypal.android.a.e.a(j.HELVETICA_12_NORMAL, context);
                        a4.setText(d.a("ANDROID_quantity") + ": " + e);
                        linearLayout.addView(a4);
                    }
                    if (i != fVar.c().c().size() - 1) {
                        LinearLayout a5 = i.a(context, 5, 5);
                        a5.setVisibility(4);
                        linearLayout.addView(a5);
                    }
                }
            }
        }
        BigDecimal a6 = fVar.c().a();
        if (a6 != null && a6.compareTo(BigDecimal.ZERO) > 0) {
            c cVar4 = new c(context, j.HELVETICA_14_NORMAL, j.HELVETICA_14_NORMAL);
            cVar4.a(d.a("ANDROID_tax"));
            cVar4.b(a.a(a6, str));
            linearLayout.addView(cVar4);
        }
        BigDecimal b3 = fVar.c().b();
        if (b3 != null && b3.compareTo(BigDecimal.ZERO) > 0) {
            c cVar5 = new c(context, j.HELVETICA_14_NORMAL, j.HELVETICA_14_NORMAL);
            cVar5.a(d.a("ANDROID_shipping"));
            cVar5.b(a.a(b3, str));
            linearLayout.addView(cVar5);
        }
        return linearLayout;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}
