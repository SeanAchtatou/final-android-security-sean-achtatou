package com.google.android.apps.analytics;

import java.util.Locale;

class q {
    q() {
    }

    public static String a(w wVar) {
        StringBuilder sb = new StringBuilder();
        i a = wVar.a();
        if (a == null || !a.b()) {
            return "";
        }
        h[] a2 = a.a();
        a(a2, sb, 8);
        a(a2, sb, 9);
        a(a2, sb, 11);
        return sb.toString();
    }

    public static String a(w wVar, String str) {
        String str2 = "";
        if (wVar.j != null) {
            str2 = wVar.j;
        }
        if (!str2.startsWith("/")) {
            str2 = "/" + str2;
        }
        String b = b(str2);
        String a = a(wVar);
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(wVar.d);
        if (a.length() > 0) {
            sb.append("&utme=").append(a);
        }
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(wVar.m), Integer.valueOf(wVar.n)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmp=").append(b);
        sb.append("&utmac=").append(wVar.c);
        sb.append("&utmcc=").append(e(wVar, str));
        return sb.toString();
    }

    private static String a(String str) {
        return str.replace("'", "'0").replace(")", "'1").replace("*", "'2").replace("!", "'3");
    }

    static void a(StringBuilder sb, String str, double d) {
        sb.append(str).append("=");
        double floor = Math.floor((d * 1000000.0d) + 0.5d) / 1000000.0d;
        if (floor != 0.0d) {
            sb.append(Double.toString(floor));
        }
    }

    private static void a(StringBuilder sb, String str, String str2) {
        sb.append(str).append("=");
        if (str2 != null && str2.trim().length() > 0) {
            sb.append(c.a(str2));
        }
    }

    private static void a(h[] hVarArr, StringBuilder sb, int i) {
        sb.append(i).append("(");
        boolean z = true;
        for (int i2 = 0; i2 < hVarArr.length; i2++) {
            if (hVarArr[i2] != null) {
                h hVar = hVarArr[i2];
                if (!z) {
                    sb.append("*");
                } else {
                    z = false;
                }
                sb.append(hVar.d()).append("!");
                switch (i) {
                    case 8:
                        sb.append(a(b(hVar.b())));
                        continue;
                    case 9:
                        sb.append(a(b(hVar.c())));
                        continue;
                    case 11:
                        sb.append(hVar.a());
                        continue;
                }
            }
        }
        sb.append(")");
    }

    public static String b(w wVar, String str) {
        Locale locale = Locale.getDefault();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(String.format("5(%s*%s", b(wVar.i), b(wVar.j)));
        if (wVar.k != null) {
            sb2.append("*").append(b(wVar.k));
        }
        sb2.append(")");
        if (wVar.l > -1) {
            sb2.append(String.format("(%d)", Integer.valueOf(wVar.l)));
        }
        sb2.append(a(wVar));
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(wVar.d);
        sb.append("&utmt=event");
        sb.append("&utme=").append(sb2.toString());
        sb.append("&utmcs=UTF-8");
        sb.append(String.format("&utmsr=%dx%d", Integer.valueOf(wVar.m), Integer.valueOf(wVar.n)));
        sb.append(String.format("&utmul=%s-%s", locale.getLanguage(), locale.getCountry()));
        sb.append("&utmac=").append(wVar.c);
        sb.append("&utmcc=").append(e(wVar, str));
        return sb.toString();
    }

    private static String b(String str) {
        return c.a(str);
    }

    public static String c(w wVar, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(wVar.d);
        sb.append("&utmt=tran");
        o b = wVar.b();
        if (b != null) {
            a(sb, "&utmtid", b.a());
            a(sb, "&utmtst", b.b());
            a(sb, "&utmtto", b.c());
            a(sb, "&utmttx", b.d());
            a(sb, "&utmtsp", b.e());
            a(sb, "&utmtci", "");
            a(sb, "&utmtrg", "");
            a(sb, "&utmtco", "");
        }
        sb.append("&utmac=").append(wVar.c);
        sb.append("&utmcc=").append(e(wVar, str));
        return sb.toString();
    }

    public static String d(w wVar, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("/__utm.gif");
        sb.append("?utmwv=4.6ma");
        sb.append("&utmn=").append(wVar.d);
        sb.append("&utmt=item");
        v c = wVar.c();
        if (c != null) {
            a(sb, "&utmtid", c.a());
            a(sb, "&utmipc", c.b());
            a(sb, "&utmipn", c.c());
            a(sb, "&utmiva", c.d());
            a(sb, "&utmipr", c.e());
            sb.append("&utmiqt=");
            if (c.f() != 0) {
                sb.append(c.f());
            }
        }
        sb.append("&utmac=").append(wVar.c);
        sb.append("&utmcc=").append(e(wVar, str));
        return sb.toString();
    }

    public static String e(w wVar, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("__utma=");
        sb.append("999").append(".");
        sb.append(wVar.b).append(".");
        sb.append(wVar.e).append(".");
        sb.append(wVar.f).append(".");
        sb.append(wVar.g).append(".");
        sb.append(wVar.h);
        if (str != null) {
            sb.append("+__utmz=");
            sb.append("999").append(".");
            sb.append(wVar.e).append(".");
            sb.append("1.1.");
            sb.append(str);
        }
        return b(sb.toString());
    }
}
