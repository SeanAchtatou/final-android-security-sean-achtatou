package com.google.android.apps.analytics;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;

public class j {
    private static final j a = new j();
    private boolean b = false;
    private boolean c = false;
    private String d;
    private Context e;
    private ConnectivityManager f;
    private String g = "GoogleAnalytics";
    private String h = "1.2";
    private int i;
    /* access modifiers changed from: private */
    public t j;
    private a k;
    private boolean l;
    private boolean m;
    private i n;
    private Map o = new HashMap();
    private Map p = new HashMap();
    /* access modifiers changed from: private */
    public Handler q;
    private Runnable r = new m(this);

    private j() {
    }

    public static j a() {
        return a;
    }

    private void a(String str, String str2, String str3, String str4, int i2) {
        w wVar = new w(this.j.d(), str, str2, str3, str4, i2, this.e.getResources().getDisplayMetrics().widthPixels, this.e.getResources().getDisplayMetrics().heightPixels);
        wVar.a(this.n);
        this.n = new i();
        this.j.a(wVar);
        h();
    }

    private void f() {
        if (this.i >= 0 && this.q.postDelayed(this.r, (long) (this.i * 1000)) && this.b) {
            Log.v("GoogleAnalyticsTracker", "Scheduled next dispatch");
        }
    }

    private void g() {
        this.q.removeCallbacks(this.r);
    }

    private void h() {
        if (this.l) {
            this.l = false;
            f();
        }
    }

    public void a(int i2) {
        int i3 = this.i;
        this.i = i2;
        if (i3 <= 0) {
            f();
        } else if (i3 > 0) {
            g();
            f();
        }
    }

    public void a(String str) {
        a(this.d, "__##GOOGLEPAGEVIEW##__", str, (String) null, -1);
    }

    public void a(String str, int i2, Context context) {
        u uVar;
        e eVar = this.j == null ? new e(new l(context)) : this.j;
        if (this.k == null) {
            u uVar2 = new u(this.g, this.h);
            uVar2.a(this.c);
            uVar = uVar2;
        } else {
            uVar = this.k;
        }
        a(str, i2, context, eVar, uVar);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, int i2, Context context, t tVar, a aVar) {
        a(str, i2, context, tVar, aVar, new d(this));
    }

    /* access modifiers changed from: package-private */
    public void a(String str, int i2, Context context, t tVar, a aVar, aa aaVar) {
        this.d = str;
        this.e = context;
        this.j = tVar;
        this.j.e();
        this.k = aVar;
        this.k.a(aaVar, this.j.g());
        this.m = false;
        if (this.f == null) {
            this.f = (ConnectivityManager) this.e.getSystemService("connectivity");
        }
        if (this.q == null) {
            this.q = new Handler(context.getMainLooper());
        } else {
            g();
        }
        a(i2);
    }

    public void a(String str, String str2, String str3, int i2) {
        a(this.d, str, str2, str3, i2);
    }

    public boolean b() {
        if (this.b) {
            Log.v("GoogleAnalyticsTracker", "Called dispatch");
        }
        if (this.m) {
            if (this.b) {
                Log.v("GoogleAnalyticsTracker", "...but dispatcher was busy");
            }
            f();
            return false;
        }
        NetworkInfo activeNetworkInfo = this.f.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
            if (this.b) {
                Log.v("GoogleAnalyticsTracker", "...but there was no network available");
            }
            f();
            return false;
        } else if (this.j.c() != 0) {
            w[] a2 = this.j.a();
            this.k.a(a2);
            this.m = true;
            f();
            if (this.b) {
                Log.v("GoogleAnalyticsTracker", "Sending " + a2.length + " to dispatcher");
            }
            return true;
        } else {
            this.l = true;
            if (this.b) {
                Log.v("GoogleAnalyticsTracker", "...but there was nothing to dispatch");
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.m = false;
    }

    public void d() {
        this.k.a();
        g();
    }

    public boolean e() {
        return this.b;
    }
}
