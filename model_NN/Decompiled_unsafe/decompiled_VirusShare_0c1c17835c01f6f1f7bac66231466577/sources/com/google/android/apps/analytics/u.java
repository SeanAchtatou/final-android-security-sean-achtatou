package com.google.android.apps.analytics;

import android.os.Build;
import java.util.Locale;
import org.apache.http.HttpHost;

class u implements a {
    /* access modifiers changed from: private */
    public static final HttpHost a = new HttpHost("www.google-analytics.com", 80);
    private final String b;
    private g c;
    private boolean d;

    public u() {
        this("GoogleAnalytics", "1.2");
    }

    public u(String str, String str2) {
        this.d = false;
        Locale locale = Locale.getDefault();
        Object[] objArr = new Object[7];
        objArr[0] = str;
        objArr[1] = str2;
        objArr[2] = Build.VERSION.RELEASE;
        objArr[3] = locale.getLanguage() != null ? locale.getLanguage().toLowerCase() : "en";
        objArr[4] = locale.getCountry() != null ? locale.getCountry().toLowerCase() : "";
        objArr[5] = Build.MODEL;
        objArr[6] = Build.ID;
        this.b = String.format("%s/%s (Linux; U; Android %s; %s-%s; %s Build/%s)", objArr);
    }

    public void a() {
        if (this.c != null && this.c.getLooper() != null) {
            this.c.getLooper().quit();
            this.c = null;
        }
    }

    public void a(aa aaVar, String str) {
        a();
        this.c = new g(aaVar, str, this.b, this);
        this.c.start();
    }

    public void a(boolean z) {
        this.d = z;
    }

    public void a(w[] wVarArr) {
        if (this.c != null) {
            this.c.a(wVarArr);
        }
    }

    public boolean b() {
        return this.d;
    }
}
