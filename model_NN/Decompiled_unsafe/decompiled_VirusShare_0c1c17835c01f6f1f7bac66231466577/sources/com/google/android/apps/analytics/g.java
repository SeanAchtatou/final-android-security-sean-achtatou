package com.google.android.apps.analytics;

import android.os.Handler;
import android.os.HandlerThread;

class g extends HandlerThread {
    private Handler a;
    /* access modifiers changed from: private */
    public final k b;
    /* access modifiers changed from: private */
    public final String c;
    /* access modifiers changed from: private */
    public final String d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public long g;
    /* access modifiers changed from: private */
    public f h;
    /* access modifiers changed from: private */
    public final aa i;
    /* access modifiers changed from: private */
    public final x j;
    /* access modifiers changed from: private */
    public final u k;

    private g(aa aaVar, k kVar, String str, String str2, u uVar) {
        super("DispatcherThread");
        this.f = 30;
        this.h = null;
        this.i = aaVar;
        this.c = str;
        this.d = str2;
        this.b = kVar;
        this.j = new x(this);
        this.b.a(this.j);
        this.k = uVar;
    }

    private g(aa aaVar, String str, String str2, u uVar) {
        this(aaVar, new k(u.a), str, str2, uVar);
    }

    static /* synthetic */ long a(g gVar, long j2) {
        long j3 = gVar.g * j2;
        gVar.g = j3;
        return j3;
    }

    public void a(w[] wVarArr) {
        if (this.a != null) {
            this.a.post(new f(this, wVarArr));
        }
    }

    /* access modifiers changed from: protected */
    public void onLooperPrepared() {
        this.a = new Handler();
    }
}
