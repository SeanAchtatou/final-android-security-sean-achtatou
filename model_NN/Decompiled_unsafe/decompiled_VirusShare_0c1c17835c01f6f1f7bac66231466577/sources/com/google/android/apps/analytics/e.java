package com.google.android.apps.analytics;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

class e implements t {
    /* access modifiers changed from: private */
    public static final String k = ("CREATE TABLE events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", "event_id") + String.format(" '%s' INTEGER NOT NULL,", "user_id") + String.format(" '%s' CHAR(256) NOT NULL,", "account_id") + String.format(" '%s' INTEGER NOT NULL,", "random_val") + String.format(" '%s' INTEGER NOT NULL,", "timestamp_first") + String.format(" '%s' INTEGER NOT NULL,", "timestamp_previous") + String.format(" '%s' INTEGER NOT NULL,", "timestamp_current") + String.format(" '%s' INTEGER NOT NULL,", "visits") + String.format(" '%s' CHAR(256) NOT NULL,", "category") + String.format(" '%s' CHAR(256) NOT NULL,", "action") + String.format(" '%s' CHAR(256), ", "label") + String.format(" '%s' INTEGER,", "value") + String.format(" '%s' INTEGER,", "screen_width") + String.format(" '%s' INTEGER);", "screen_height"));
    /* access modifiers changed from: private */
    public static final String l = ("CREATE TABLE session (" + String.format(" '%s' INTEGER PRIMARY KEY,", "timestamp_first") + String.format(" '%s' INTEGER NOT NULL,", "timestamp_previous") + String.format(" '%s' INTEGER NOT NULL,", "timestamp_current") + String.format(" '%s' INTEGER NOT NULL,", "visits") + String.format(" '%s' INTEGER NOT NULL);", "store_id"));
    /* access modifiers changed from: private */
    public static final String m = ("CREATE TABLE custom_variables (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", "cv_id") + String.format(" '%s' INTEGER NOT NULL,", "event_id") + String.format(" '%s' INTEGER NOT NULL,", "cv_index") + String.format(" '%s' CHAR(64) NOT NULL,", "cv_name") + String.format(" '%s' CHAR(64) NOT NULL,", "cv_value") + String.format(" '%s' INTEGER NOT NULL);", "cv_scope"));
    /* access modifiers changed from: private */
    public static final String n = ("CREATE TABLE custom_var_cache (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", "cv_id") + String.format(" '%s' INTEGER NOT NULL,", "event_id") + String.format(" '%s' INTEGER NOT NULL,", "cv_index") + String.format(" '%s' CHAR(64) NOT NULL,", "cv_name") + String.format(" '%s' CHAR(64) NOT NULL,", "cv_value") + String.format(" '%s' INTEGER NOT NULL);", "cv_scope"));
    /* access modifiers changed from: private */
    public static final String o = ("CREATE TABLE transaction_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", "tran_id") + String.format(" '%s' INTEGER NOT NULL,", "event_id") + String.format(" '%s' TEXT NOT NULL,", "order_id") + String.format(" '%s' TEXT,", "tran_storename") + String.format(" '%s' TEXT NOT NULL,", "tran_totalcost") + String.format(" '%s' TEXT,", "tran_totaltax") + String.format(" '%s' TEXT);", "tran_shippingcost"));
    /* access modifiers changed from: private */
    public static final String p = ("CREATE TABLE item_events (" + String.format(" '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,", "item_id") + String.format(" '%s' INTEGER NOT NULL,", "event_id") + String.format(" '%s' TEXT NOT NULL,", "order_id") + String.format(" '%s' TEXT NOT NULL,", "item_sku") + String.format(" '%s' TEXT,", "item_name") + String.format(" '%s' TEXT,", "item_category") + String.format(" '%s' TEXT NOT NULL,", "item_price") + String.format(" '%s' TEXT NOT NULL);", "item_count"));
    private l a;
    private int b;
    private long c;
    private long d;
    private long e;
    private int f;
    private int g;
    private boolean h;
    private boolean i;
    private SQLiteStatement j = null;

    e(l lVar) {
        this.a = lVar;
        try {
            lVar.getWritableDatabase().close();
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
    }

    public void a(long j2) {
        String str = "event_id=" + j2;
        try {
            SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
            if (writableDatabase.delete("events", str, null) != 0) {
                this.g--;
                writableDatabase.delete("custom_variables", str, null);
                writableDatabase.delete("transaction_events", str, null);
                writableDatabase.delete("item_events", str, null);
            }
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0124  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.google.android.apps.analytics.w r11) {
        /*
            r10 = this;
            r2 = 0
            int r0 = r10.g
            r1 = 1000(0x3e8, float:1.401E-42)
            if (r0 < r1) goto L_0x000f
            java.lang.String r0 = "GoogleAnalyticsTracker"
            java.lang.String r1 = "Store full. Not storing last event."
            android.util.Log.w(r0, r1)
        L_0x000e:
            return
        L_0x000f:
            boolean r0 = r10.h
            if (r0 != 0) goto L_0x0016
            r10.f()
        L_0x0016:
            com.google.android.apps.analytics.l r0 = r10.a     // Catch:{ SQLiteException -> 0x0135, all -> 0x0130 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ SQLiteException -> 0x0135, all -> 0x0130 }
            r0.beginTransaction()     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.<init>()     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "user_id"
            int r3 = r11.b     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "account_id"
            java.lang.String r3 = r11.c     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "random_val"
            double r3 = java.lang.Math.random()     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r5 = 4746794007244308480(0x41dfffffffc00000, double:2.147483647E9)
            double r3 = r3 * r5
            int r3 = (int) r3     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "timestamp_first"
            long r3 = r10.c     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "timestamp_previous"
            long r3 = r10.d     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "timestamp_current"
            long r3 = r10.e     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "visits"
            int r3 = r10.f     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "category"
            java.lang.String r3 = r11.i     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "action"
            java.lang.String r3 = r11.j     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "label"
            java.lang.String r3 = r11.k     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "value"
            int r3 = r11.l     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "screen_width"
            int r3 = r11.m     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "screen_height"
            int r3 = r11.n     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r2 = "events"
            java.lang.String r3 = "event_id"
            long r1 = r0.insert(r2, r3, r1)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r3 = -1
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 == 0) goto L_0x0128
            int r1 = r10.g     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            int r1 = r1 + 1
            r10.g = r1     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r1 = "events"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r3 = 0
            java.lang.String r4 = "event_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "event_id DESC"
            r8 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r2 = 0
            r1.moveToPosition(r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r2 = 0
            long r2 = r1.getLong(r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            r1.close()     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r1 = r11.i     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r4 = "__##GOOGLETRANSACTION##__"
            boolean r1 = r1.equals(r4)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            if (r1 == 0) goto L_0x00f8
            r10.a(r11, r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
        L_0x00ee:
            r0.setTransactionSuccessful()     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
        L_0x00f1:
            if (r0 == 0) goto L_0x000e
            r0.endTransaction()
            goto L_0x000e
        L_0x00f8:
            java.lang.String r1 = r11.i     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            java.lang.String r4 = "__##GOOGLEITEM##__"
            boolean r1 = r1.equals(r4)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            if (r1 == 0) goto L_0x011a
            r10.b(r11, r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            goto L_0x00ee
        L_0x0106:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x010a:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0133 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0133 }
            if (r1 == 0) goto L_0x000e
            r1.endTransaction()
            goto L_0x000e
        L_0x011a:
            r10.c(r11, r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            goto L_0x00ee
        L_0x011e:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0122:
            if (r1 == 0) goto L_0x0127
            r1.endTransaction()
        L_0x0127:
            throw r0
        L_0x0128:
            java.lang.String r1 = "GoogleAnalyticsTracker"
            java.lang.String r2 = "Error when attempting to add event to database."
            android.util.Log.e(r1, r2)     // Catch:{ SQLiteException -> 0x0106, all -> 0x011e }
            goto L_0x00f1
        L_0x0130:
            r0 = move-exception
            r1 = r2
            goto L_0x0122
        L_0x0133:
            r0 = move-exception
            goto L_0x0122
        L_0x0135:
            r0 = move-exception
            r1 = r2
            goto L_0x010a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.e.a(com.google.android.apps.analytics.w):void");
    }

    /* access modifiers changed from: package-private */
    public void a(w wVar, long j2) {
        o b2 = wVar.b();
        if (b2 == null) {
            Log.w("GoogleAnalyticsTracker", "missing transaction details for event " + j2);
            return;
        }
        try {
            SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("event_id", Long.valueOf(j2));
            contentValues.put("order_id", b2.a());
            contentValues.put("tran_storename", b2.b());
            contentValues.put("tran_totalcost", b2.c() + "");
            contentValues.put("tran_totaltax", b2.d() + "");
            contentValues.put("tran_shippingcost", b2.e() + "");
            writableDatabase.insert("transaction_events", "event_id", contentValues);
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
    }

    public void a(String str) {
        try {
            SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("referrer", str);
            writableDatabase.insert("install_referrer", null, contentValues);
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
    }

    public w[] a() {
        return a(1000);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0133  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.w[] a(int r22) {
        /*
            r21 = this;
            java.util.ArrayList r19 = new java.util.ArrayList
            r19.<init>()
            r12 = 0
            r0 = r21
            com.google.android.apps.analytics.l r0 = r0.a     // Catch:{ SQLiteException -> 0x015e, all -> 0x0159 }
            r3 = r0
            android.database.sqlite.SQLiteDatabase r3 = r3.getReadableDatabase()     // Catch:{ SQLiteException -> 0x015e, all -> 0x0159 }
            java.lang.String r4 = "events"
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            java.lang.String r10 = "event_id"
            java.lang.String r11 = java.lang.Integer.toString(r22)     // Catch:{ SQLiteException -> 0x015e, all -> 0x0159 }
            android.database.Cursor r20 = r3.query(r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ SQLiteException -> 0x015e, all -> 0x0159 }
        L_0x0020:
            boolean r3 = r20.moveToNext()     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            if (r3 == 0) goto L_0x0142
            com.google.android.apps.analytics.w r3 = new com.google.android.apps.analytics.w     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r4 = 0
            r0 = r20
            r1 = r4
            long r4 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r6 = 1
            r0 = r20
            r1 = r6
            int r6 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r7 = 2
            r0 = r20
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r8 = 3
            r0 = r20
            r1 = r8
            int r8 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r9 = 4
            r0 = r20
            r1 = r9
            int r9 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r10 = 5
            r0 = r20
            r1 = r10
            int r10 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r11 = 6
            r0 = r20
            r1 = r11
            int r11 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r12 = 7
            r0 = r20
            r1 = r12
            int r12 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r13 = 8
            r0 = r20
            r1 = r13
            java.lang.String r13 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r14 = 9
            r0 = r20
            r1 = r14
            java.lang.String r14 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r15 = 10
            r0 = r20
            r1 = r15
            java.lang.String r15 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r16 = 11
            r0 = r20
            r1 = r16
            int r16 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r17 = 12
            r0 = r20
            r1 = r17
            int r17 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r18 = 13
            r0 = r20
            r1 = r18
            int r18 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r3.<init>(r4, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r4 = "event_id"
            r0 = r20
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r0 = r20
            r1 = r4
            long r4 = r0.getLong(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r6 = "__##GOOGLETRANSACTION##__"
            java.lang.String r7 = r3.i     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            boolean r6 = r6.equals(r7)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            if (r6 == 0) goto L_0x00ff
            r0 = r21
            r1 = r4
            com.google.android.apps.analytics.o r6 = r0.b(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            if (r6 != 0) goto L_0x00df
            java.lang.String r7 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r8.<init>()     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r9 = "missing expected transaction for event "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            android.util.Log.w(r7, r4)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
        L_0x00df:
            r3.a(r6)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
        L_0x00e2:
            r0 = r19
            r1 = r3
            r0.add(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            goto L_0x0020
        L_0x00ea:
            r3 = move-exception
            r4 = r20
        L_0x00ed:
            java.lang.String r5 = "GoogleAnalyticsTracker"
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x015c }
            android.util.Log.e(r5, r3)     // Catch:{ all -> 0x015c }
            r3 = 0
            com.google.android.apps.analytics.w[] r3 = new com.google.android.apps.analytics.w[r3]     // Catch:{ all -> 0x015c }
            if (r4 == 0) goto L_0x00fe
            r4.close()
        L_0x00fe:
            return r3
        L_0x00ff:
            java.lang.String r6 = "__##GOOGLEITEM##__"
            java.lang.String r7 = r3.i     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            boolean r6 = r6.equals(r7)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            if (r6 == 0) goto L_0x0137
            r0 = r21
            r1 = r4
            com.google.android.apps.analytics.v r6 = r0.c(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            if (r6 != 0) goto L_0x012a
            java.lang.String r7 = "GoogleAnalyticsTracker"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r8.<init>()     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r9 = "missing expected item for event "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            java.lang.String r4 = r4.toString()     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            android.util.Log.w(r7, r4)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
        L_0x012a:
            r3.a(r6)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            goto L_0x00e2
        L_0x012e:
            r3 = move-exception
            r4 = r20
        L_0x0131:
            if (r4 == 0) goto L_0x0136
            r4.close()
        L_0x0136:
            throw r3
        L_0x0137:
            r0 = r21
            r1 = r4
            com.google.android.apps.analytics.i r4 = r0.d(r1)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            r3.a(r4)     // Catch:{ SQLiteException -> 0x00ea, all -> 0x012e }
            goto L_0x00e2
        L_0x0142:
            if (r20 == 0) goto L_0x0147
            r20.close()
        L_0x0147:
            int r3 = r19.size()
            com.google.android.apps.analytics.w[] r3 = new com.google.android.apps.analytics.w[r3]
            r0 = r19
            r1 = r3
            java.lang.Object[] r21 = r0.toArray(r1)
            com.google.android.apps.analytics.w[] r21 = (com.google.android.apps.analytics.w[]) r21
            r3 = r21
            goto L_0x00fe
        L_0x0159:
            r3 = move-exception
            r4 = r12
            goto L_0x0131
        L_0x015c:
            r3 = move-exception
            goto L_0x0131
        L_0x015e:
            r3 = move-exception
            r4 = r12
            goto L_0x00ed
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.e.a(int):com.google.android.apps.analytics.w[]");
    }

    /* access modifiers changed from: package-private */
    public i b() {
        i iVar = new i();
        try {
            Cursor query = this.a.getReadableDatabase().query("custom_var_cache", null, "cv_scope=1", null, null, null, null);
            while (query.moveToNext()) {
                iVar.a(new h(query.getInt(query.getColumnIndex("cv_index")), query.getString(query.getColumnIndex("cv_name")), query.getString(query.getColumnIndex("cv_value")), query.getInt(query.getColumnIndex("cv_scope"))));
            }
            query.close();
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
        return iVar;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0095  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.o b(long r11) {
        /*
            r10 = this;
            r8 = 0
            com.google.android.apps.analytics.l r0 = r10.a     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            java.lang.String r1 = "transaction_events"
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            java.lang.String r4 = "event_id="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            java.lang.StringBuilder r3 = r3.append(r11)     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0080, all -> 0x0091 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            if (r1 == 0) goto L_0x0079
            com.google.android.apps.analytics.z r1 = new com.google.android.apps.analytics.z     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = "order_id"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r3 = "tran_totalcost"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            double r3 = r0.getDouble(r3)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            r1.<init>(r2, r3)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = "tran_storename"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            com.google.android.apps.analytics.z r1 = r1.a(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = "tran_totaltax"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            double r2 = r0.getDouble(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            com.google.android.apps.analytics.z r1 = r1.a(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            java.lang.String r2 = "tran_shippingcost"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            double r2 = r0.getDouble(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            com.google.android.apps.analytics.z r1 = r1.b(r2)     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            com.google.android.apps.analytics.o r1 = r1.a()     // Catch:{ SQLiteException -> 0x00a0, all -> 0x0099 }
            if (r0 == 0) goto L_0x0077
            r0.close()
        L_0x0077:
            r0 = r1
        L_0x0078:
            return r0
        L_0x0079:
            if (r0 == 0) goto L_0x007e
            r0.close()
        L_0x007e:
            r0 = r8
            goto L_0x0078
        L_0x0080:
            r0 = move-exception
            r1 = r8
        L_0x0082:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x009e }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x009e }
            if (r1 == 0) goto L_0x007e
            r1.close()
            goto L_0x007e
        L_0x0091:
            r0 = move-exception
            r1 = r8
        L_0x0093:
            if (r1 == 0) goto L_0x0098
            r1.close()
        L_0x0098:
            throw r0
        L_0x0099:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0093
        L_0x009e:
            r0 = move-exception
            goto L_0x0093
        L_0x00a0:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.e.b(long):com.google.android.apps.analytics.o");
    }

    /* access modifiers changed from: package-private */
    public void b(w wVar, long j2) {
        v c2 = wVar.c();
        if (c2 == null) {
            Log.w("GoogleAnalyticsTracker", "missing item details for event " + j2);
            return;
        }
        try {
            SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("event_id", Long.valueOf(j2));
            contentValues.put("order_id", c2.a());
            contentValues.put("item_sku", c2.b());
            contentValues.put("item_name", c2.c());
            contentValues.put("item_category", c2.d());
            contentValues.put("item_price", c2.e() + "");
            contentValues.put("item_count", c2.f() + "");
            writableDatabase.insert("item_events", "event_id", contentValues);
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
    }

    public int c() {
        try {
            if (this.j == null) {
                this.j = this.a.getReadableDatabase().compileStatement("SELECT COUNT(*) from events");
            }
            return (int) this.j.simpleQueryForLong();
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.v c(long r10) {
        /*
            r9 = this;
            r8 = 0
            com.google.android.apps.analytics.l r0 = r9.a     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            java.lang.String r1 = "item_events"
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            java.lang.String r4 = "event_id="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0085, all -> 0x0096 }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            if (r0 == 0) goto L_0x007e
            com.google.android.apps.analytics.s r0 = new com.google.android.apps.analytics.s     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = "order_id"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r2 = "item_sku"
            int r2 = r7.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r2 = r7.getString(r2)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r3 = "item_price"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            double r3 = r7.getDouble(r3)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r5 = "item_count"
            int r5 = r7.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            long r5 = r7.getLong(r5)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            r0.<init>(r1, r2, r3, r5)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = "item_name"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            com.google.android.apps.analytics.s r0 = r0.a(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = "item_category"
            int r1 = r7.getColumnIndex(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            com.google.android.apps.analytics.s r0 = r0.b(r1)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            com.google.android.apps.analytics.v r0 = r0.a()     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009e }
            if (r7 == 0) goto L_0x007d
            r7.close()
        L_0x007d:
            return r0
        L_0x007e:
            if (r7 == 0) goto L_0x0083
            r7.close()
        L_0x0083:
            r0 = r8
            goto L_0x007d
        L_0x0085:
            r0 = move-exception
            r1 = r8
        L_0x0087:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a1 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00a1 }
            if (r1 == 0) goto L_0x0083
            r1.close()
            goto L_0x0083
        L_0x0096:
            r0 = move-exception
            r1 = r8
        L_0x0098:
            if (r1 == 0) goto L_0x009d
            r1.close()
        L_0x009d:
            throw r0
        L_0x009e:
            r0 = move-exception
            r1 = r7
            goto L_0x0098
        L_0x00a1:
            r0 = move-exception
            goto L_0x0098
        L_0x00a3:
            r0 = move-exception
            r1 = r7
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.e.c(long):com.google.android.apps.analytics.v");
    }

    /* access modifiers changed from: package-private */
    public void c(w wVar, long j2) {
        try {
            SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
            i a2 = wVar.a();
            if (this.i) {
                if (a2 == null) {
                    a2 = new i();
                    wVar.a(a2);
                }
                i b2 = b();
                for (int i2 = 1; i2 <= 5; i2++) {
                    h b3 = b2.b(i2);
                    h b4 = a2.b(i2);
                    if (b3 != null && b4 == null) {
                        a2.a(b3);
                    }
                }
                this.i = false;
            }
            if (a2 != null) {
                for (int i3 = 1; i3 <= 5; i3++) {
                    if (!a2.a(i3)) {
                        h b5 = a2.b(i3);
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("event_id", Long.valueOf(j2));
                        contentValues.put("cv_index", Integer.valueOf(b5.d()));
                        contentValues.put("cv_name", b5.b());
                        contentValues.put("cv_scope", Integer.valueOf(b5.a()));
                        contentValues.put("cv_value", b5.c());
                        writableDatabase.insert("custom_variables", "event_id", contentValues);
                        writableDatabase.update("custom_var_cache", contentValues, "cv_index=" + b5.d(), null);
                    }
                }
            }
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
    }

    public int d() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.android.apps.analytics.i d(long r12) {
        /*
            r11 = this;
            r9 = 0
            com.google.android.apps.analytics.i r8 = new com.google.android.apps.analytics.i
            r8.<init>()
            com.google.android.apps.analytics.l r0 = r11.a     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.String r1 = "custom_variables"
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.String r4 = "event_id="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.StringBuilder r3 = r3.append(r12)     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0089, all -> 0x007a }
        L_0x002a:
            boolean r1 = r0.moveToNext()     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            if (r1 == 0) goto L_0x0074
            com.google.android.apps.analytics.h r1 = new com.google.android.apps.analytics.h     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r2 = "cv_index"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            int r2 = r0.getInt(r2)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r3 = "cv_name"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r4 = "cv_value"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            java.lang.String r5 = "cv_scope"
            int r5 = r0.getColumnIndex(r5)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            int r5 = r0.getInt(r5)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            r1.<init>(r2, r3, r4, r5)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            r8.a(r1)     // Catch:{ SQLiteException -> 0x0061, all -> 0x0082 }
            goto L_0x002a
        L_0x0061:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x0065:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0087 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x0087 }
            if (r1 == 0) goto L_0x0073
            r1.close()
        L_0x0073:
            return r8
        L_0x0074:
            if (r0 == 0) goto L_0x0073
            r0.close()
            goto L_0x0073
        L_0x007a:
            r0 = move-exception
            r1 = r9
        L_0x007c:
            if (r1 == 0) goto L_0x0081
            r1.close()
        L_0x0081:
            throw r0
        L_0x0082:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x007c
        L_0x0087:
            r0 = move-exception
            goto L_0x007c
        L_0x0089:
            r0 = move-exception
            r1 = r9
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.e.d(long):com.google.android.apps.analytics.i");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x00c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void e() {
        /*
            r9 = this;
            r1 = 1
            r0 = 0
            r8 = 0
            r9.h = r0
            r9.i = r1
            int r0 = r9.c()
            r9.g = r0
            com.google.android.apps.analytics.l r0 = r9.a     // Catch:{ SQLiteException -> 0x00cf, all -> 0x00c5 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ SQLiteException -> 0x00cf, all -> 0x00c5 }
            java.lang.String r1 = "session"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00cf, all -> 0x00c5 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x00b5 }
            if (r2 != 0) goto L_0x008d
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ SQLiteException -> 0x00b5 }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r4
            r9.c = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r9.d = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r9.e = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r2 = 1
            r9.f = r2     // Catch:{ SQLiteException -> 0x00b5 }
            java.security.SecureRandom r2 = new java.security.SecureRandom     // Catch:{ SQLiteException -> 0x00b5 }
            r2.<init>()     // Catch:{ SQLiteException -> 0x00b5 }
            int r2 = r2.nextInt()     // Catch:{ SQLiteException -> 0x00b5 }
            r3 = 2147483647(0x7fffffff, float:NaN)
            r2 = r2 & r3
            r9.b = r2     // Catch:{ SQLiteException -> 0x00b5 }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ SQLiteException -> 0x00b5 }
            r2.<init>()     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "timestamp_first"
            long r4 = r9.c     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "timestamp_previous"
            long r4 = r9.d     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "timestamp_current"
            long r4 = r9.e     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "visits"
            int r4 = r9.f     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "store_id"
            int r4 = r9.b     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ SQLiteException -> 0x00b5 }
            r2.put(r3, r4)     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r3 = "session"
            java.lang.String r4 = "timestamp_first"
            r0.insert(r3, r4, r2)     // Catch:{ SQLiteException -> 0x00b5 }
        L_0x0087:
            if (r1 == 0) goto L_0x008c
            r1.close()
        L_0x008c:
            return
        L_0x008d:
            r0 = 0
            long r2 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            r9.c = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r0 = 2
            long r2 = r1.getLong(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            r9.d = r2     // Catch:{ SQLiteException -> 0x00b5 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ SQLiteException -> 0x00b5 }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 / r4
            r9.e = r2     // Catch:{ SQLiteException -> 0x00b5 }
            r0 = 3
            int r0 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            int r0 = r0 + 1
            r9.f = r0     // Catch:{ SQLiteException -> 0x00b5 }
            r0 = 4
            int r0 = r1.getInt(r0)     // Catch:{ SQLiteException -> 0x00b5 }
            r9.b = r0     // Catch:{ SQLiteException -> 0x00b5 }
            goto L_0x0087
        L_0x00b5:
            r0 = move-exception
        L_0x00b6:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00cd }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00cd }
            if (r1 == 0) goto L_0x008c
            r1.close()
            goto L_0x008c
        L_0x00c5:
            r0 = move-exception
            r1 = r8
        L_0x00c7:
            if (r1 == 0) goto L_0x00cc
            r1.close()
        L_0x00cc:
            throw r0
        L_0x00cd:
            r0 = move-exception
            goto L_0x00c7
        L_0x00cf:
            r0 = move-exception
            r1 = r8
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.e.e():void");
    }

    /* access modifiers changed from: package-private */
    public void f() {
        try {
            SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("timestamp_previous", Long.valueOf(this.d));
            contentValues.put("timestamp_current", Long.valueOf(this.e));
            contentValues.put("visits", Integer.valueOf(this.f));
            writableDatabase.update("session", contentValues, "timestamp_first=?", new String[]{Long.toString(this.c)});
            this.h = true;
        } catch (SQLiteException e2) {
            Log.e("GoogleAnalyticsTracker", e2.toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String g() {
        /*
            r10 = this;
            r8 = 0
            com.google.android.apps.analytics.l r0 = r10.a     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            android.database.sqlite.SQLiteDatabase r0 = r0.getReadableDatabase()     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            java.lang.String r1 = "install_referrer"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            r3 = 0
            java.lang.String r4 = "referrer"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x002c, all -> 0x003e }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x004d, all -> 0x0046 }
            if (r1 == 0) goto L_0x0052
            r1 = 0
            java.lang.String r1 = r0.getString(r1)     // Catch:{ SQLiteException -> 0x004d, all -> 0x0046 }
        L_0x0025:
            if (r0 == 0) goto L_0x002a
            r0.close()
        L_0x002a:
            r0 = r1
        L_0x002b:
            return r0
        L_0x002c:
            r0 = move-exception
            r1 = r8
        L_0x002e:
            java.lang.String r2 = "GoogleAnalyticsTracker"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x004b }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x004b }
            if (r1 == 0) goto L_0x003c
            r1.close()
        L_0x003c:
            r0 = r8
            goto L_0x002b
        L_0x003e:
            r0 = move-exception
            r1 = r8
        L_0x0040:
            if (r1 == 0) goto L_0x0045
            r1.close()
        L_0x0045:
            throw r0
        L_0x0046:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0040
        L_0x004b:
            r0 = move-exception
            goto L_0x0040
        L_0x004d:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x002e
        L_0x0052:
            r1 = r8
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.e.g():java.lang.String");
    }
}
