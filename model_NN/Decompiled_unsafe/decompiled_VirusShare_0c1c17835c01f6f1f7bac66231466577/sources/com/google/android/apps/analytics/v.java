package com.google.android.apps.analytics;

public class v {
    private final String a;
    private final String b;
    private final String c;
    private final String d;
    private final double e;
    private final long f;

    private v(s sVar) {
        this.a = sVar.a;
        this.b = sVar.b;
        this.e = sVar.c;
        this.f = sVar.d;
        this.c = sVar.e;
        this.d = sVar.f;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public double e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public long f() {
        return this.f;
    }
}
