package com.google.android.apps.analytics;

class i {
    private h[] a = new h[5];

    private void c(int i) {
        if (i < 1 || i > 5) {
            throw new IllegalArgumentException("Index must be between 1 and 5 inclusive.");
        }
    }

    public void a(h hVar) {
        c(hVar.d());
        this.a[hVar.d() - 1] = hVar;
    }

    public boolean a(int i) {
        c(i);
        return this.a[i - 1] == null;
    }

    public h[] a() {
        return (h[]) this.a.clone();
    }

    public h b(int i) {
        c(i);
        return this.a[i - 1];
    }

    public boolean b() {
        for (h hVar : this.a) {
            if (hVar != null) {
                return true;
            }
        }
        return false;
    }
}
