package com.google.android.apps.analytics;

public class z {
    /* access modifiers changed from: private */
    public final String a;
    /* access modifiers changed from: private */
    public String b = null;
    /* access modifiers changed from: private */
    public final double c;
    /* access modifiers changed from: private */
    public double d = 0.0d;
    /* access modifiers changed from: private */
    public double e = 0.0d;

    public z(String str, double d2) {
        if (str == null || str.trim().length() == 0) {
            throw new IllegalArgumentException("orderId must not be empty or null");
        }
        this.a = str;
        this.c = d2;
    }

    public o a() {
        return new o(this);
    }

    public z a(double d2) {
        this.d = d2;
        return this;
    }

    public z a(String str) {
        this.b = str;
        return this;
    }

    public z b(double d2) {
        this.e = d2;
        return this;
    }
}
