package com.google.android.apps.analytics;

public class o {
    private final String a;
    private final String b;
    private final double c;
    private final double d;
    private final double e;

    private o(z zVar) {
        this.a = zVar.a;
        this.c = zVar.c;
        this.b = zVar.b;
        this.d = zVar.d;
        this.e = zVar.e;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public double c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public double d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public double e() {
        return this.e;
    }
}
