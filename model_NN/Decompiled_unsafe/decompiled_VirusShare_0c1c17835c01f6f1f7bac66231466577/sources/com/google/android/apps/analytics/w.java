package com.google.android.apps.analytics;

class w {
    final long a;
    final int b;
    final String c;
    final int d;
    final int e;
    final int f;
    final int g;
    final int h;
    final String i;
    final String j;
    final String k;
    final int l;
    final int m;
    final int n;
    i o;
    private o p;
    private v q;

    w(int i2, String str, String str2, String str3, String str4, int i3, int i4, int i5) {
        this(-1, i2, str, -1, -1, -1, -1, -1, str2, str3, str4, i3, i4, i5);
    }

    w(long j2, int i2, String str, int i3, int i4, int i5, int i6, int i7, String str2, String str3, String str4, int i8, int i9, int i10) {
        this.a = j2;
        this.b = i2;
        this.c = str;
        this.d = i3;
        this.e = i4;
        this.f = i5;
        this.g = i6;
        this.h = i7;
        this.i = str2;
        this.j = str3;
        this.k = str4;
        this.l = i8;
        this.n = i10;
        this.m = i9;
    }

    public i a() {
        return this.o;
    }

    public void a(i iVar) {
        this.o = iVar;
    }

    public void a(o oVar) {
        if (!this.i.equals("__##GOOGLETRANSACTION##__")) {
            throw new IllegalStateException("Attempted to add a transction to an event of type " + this.i);
        }
        this.p = oVar;
    }

    public void a(v vVar) {
        if (!this.i.equals("__##GOOGLEITEM##__")) {
            throw new IllegalStateException("Attempted to add an item to an event of type " + this.i);
        }
        this.q = vVar;
    }

    public o b() {
        return this.p;
    }

    public v c() {
        return this.q;
    }

    public String toString() {
        return "id:" + this.a + " " + "random:" + this.d + " " + "timestampCurrent:" + this.g + " " + "timestampPrevious:" + this.f + " " + "timestampFirst:" + this.e + " " + "visits:" + this.h + " " + "value:" + this.l + " " + "category:" + this.i + " " + "action:" + this.j + " " + "label:" + this.k + " " + "width:" + this.m + " " + "height:" + this.n;
    }
}
