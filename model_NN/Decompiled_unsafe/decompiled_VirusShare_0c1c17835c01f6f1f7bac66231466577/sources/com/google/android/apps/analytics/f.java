package com.google.android.apps.analytics;

import android.util.Log;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import org.apache.http.HttpException;

class f implements Runnable {
    final /* synthetic */ g a;
    private final LinkedList b = new LinkedList();

    public f(g gVar, w[] wVarArr) {
        this.a = gVar;
        Collections.addAll(this.b, wVarArr);
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r5) {
        /*
            r4 = this;
            com.google.android.apps.analytics.j r0 = com.google.android.apps.analytics.j.a()
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0013
            if (r5 == 0) goto L_0x0013
            java.lang.String r0 = "GoogleAnalyticsTracker"
            java.lang.String r1 = "dispatching events in dry run mode"
            android.util.Log.v(r0, r1)
        L_0x0013:
            r0 = 0
            r1 = r0
        L_0x0015:
            java.util.LinkedList r0 = r4.b
            int r0 = r0.size()
            if (r1 >= r0) goto L_0x00c5
            com.google.android.apps.analytics.g r0 = r4.a
            int r0 = r0.f
            if (r1 >= r0) goto L_0x00c5
            java.util.LinkedList r0 = r4.b
            java.lang.Object r0 = r0.get(r1)
            com.google.android.apps.analytics.w r0 = (com.google.android.apps.analytics.w) r0
            java.lang.String r2 = "__##GOOGLEPAGEVIEW##__"
            java.lang.String r3 = r0.i
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0086
            com.google.android.apps.analytics.g r2 = r4.a
            java.lang.String r2 = r2.c
            java.lang.String r0 = com.google.android.apps.analytics.q.a(r0, r2)
        L_0x0041:
            org.apache.http.message.BasicHttpRequest r2 = new org.apache.http.message.BasicHttpRequest
            java.lang.String r3 = "GET"
            r2.<init>(r3, r0)
            java.lang.String r0 = "Host"
            org.apache.http.HttpHost r3 = com.google.android.apps.analytics.u.a
            java.lang.String r3 = r3.getHostName()
            r2.addHeader(r0, r3)
            java.lang.String r0 = "User-Agent"
            com.google.android.apps.analytics.g r3 = r4.a
            java.lang.String r3 = r3.d
            r2.addHeader(r0, r3)
            com.google.android.apps.analytics.j r0 = com.google.android.apps.analytics.j.a()
            boolean r0 = r0.e()
            if (r0 == 0) goto L_0x0077
            java.lang.String r0 = "GoogleAnalyticsTracker"
            org.apache.http.RequestLine r3 = r2.getRequestLine()
            java.lang.String r3 = r3.toString()
            android.util.Log.i(r0, r3)
        L_0x0077:
            if (r5 == 0) goto L_0x00bb
            com.google.android.apps.analytics.g r0 = r4.a
            com.google.android.apps.analytics.x r0 = r0.j
            r0.a()
        L_0x0082:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0015
        L_0x0086:
            java.lang.String r2 = "__##GOOGLETRANSACTION##__"
            java.lang.String r3 = r0.i
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x009b
            com.google.android.apps.analytics.g r2 = r4.a
            java.lang.String r2 = r2.c
            java.lang.String r0 = com.google.android.apps.analytics.q.c(r0, r2)
            goto L_0x0041
        L_0x009b:
            java.lang.String r2 = "__##GOOGLEITEM##__"
            java.lang.String r3 = r0.i
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x00b0
            com.google.android.apps.analytics.g r2 = r4.a
            java.lang.String r2 = r2.c
            java.lang.String r0 = com.google.android.apps.analytics.q.d(r0, r2)
            goto L_0x0041
        L_0x00b0:
            com.google.android.apps.analytics.g r2 = r4.a
            java.lang.String r2 = r2.c
            java.lang.String r0 = com.google.android.apps.analytics.q.b(r0, r2)
            goto L_0x0041
        L_0x00bb:
            com.google.android.apps.analytics.g r0 = r4.a
            com.google.android.apps.analytics.k r0 = r0.b
            r0.a(r2)
            goto L_0x0082
        L_0x00c5:
            if (r5 != 0) goto L_0x00d0
            com.google.android.apps.analytics.g r0 = r4.a
            com.google.android.apps.analytics.k r0 = r0.b
            r0.a()
        L_0x00d0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.apps.analytics.f.a(boolean):void");
    }

    public w a() {
        return (w) this.b.poll();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.apps.analytics.g.a(com.google.android.apps.analytics.g, long):long
     arg types: [com.google.android.apps.analytics.g, int]
     candidates:
      com.google.android.apps.analytics.g.a(com.google.android.apps.analytics.g, int):int
      com.google.android.apps.analytics.g.a(com.google.android.apps.analytics.g, com.google.android.apps.analytics.f):com.google.android.apps.analytics.f
      com.google.android.apps.analytics.g.a(com.google.android.apps.analytics.g, long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.apps.analytics.g.b(com.google.android.apps.analytics.g, long):long
     arg types: [com.google.android.apps.analytics.g, int]
     candidates:
      com.google.android.apps.analytics.g.b(com.google.android.apps.analytics.g, int):int
      com.google.android.apps.analytics.g.b(com.google.android.apps.analytics.g, long):long */
    public void run() {
        f unused = this.a.h = this;
        int i = 0;
        while (i < 5 && this.b.size() > 0) {
            long j = 0;
            try {
                if (this.a.e == 500 || this.a.e == 503) {
                    j = (long) (Math.random() * ((double) this.a.g));
                    if (this.a.g < 256) {
                        g.a(this.a, 2L);
                    }
                } else {
                    long unused2 = this.a.g = 2L;
                }
                Thread.sleep(j * 1000);
                a(this.a.k.b());
                i++;
            } catch (InterruptedException e) {
                Log.w("GoogleAnalyticsTracker", "Couldn't sleep.", e);
            } catch (IOException e2) {
                Log.w("GoogleAnalyticsTracker", "Problem with socket or streams.", e2);
            } catch (HttpException e3) {
                Log.w("GoogleAnalyticsTracker", "Problem with http streams.", e3);
            }
        }
        this.a.b.b();
        this.a.i.a();
        f unused3 = this.a.h = (f) null;
    }
}
