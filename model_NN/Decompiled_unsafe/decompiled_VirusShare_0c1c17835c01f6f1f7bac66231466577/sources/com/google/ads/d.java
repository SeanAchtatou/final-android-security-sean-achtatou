package com.google.ads;

import android.content.Context;
import android.location.Location;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class d {
    public static final String a = AdUtil.a("emulator");
    private a b = null;
    private String c = null;
    private Set d = null;
    private Map e = null;
    private Location f = null;
    private boolean g = false;
    private boolean h = false;
    private Set i = null;

    public Map a(Context context) {
        HashMap hashMap = new HashMap();
        if (this.d != null) {
            hashMap.put("kw", this.d);
        }
        if (this.b != null) {
            hashMap.put("cust_gender", this.b.toString());
        }
        if (this.c != null) {
            hashMap.put("cust_age", this.c);
        }
        if (this.f != null) {
            hashMap.put("uule", AdUtil.a(this.f));
        }
        if (this.g) {
            hashMap.put("testing", 1);
        }
        if (b(context)) {
            hashMap.put("adtest", "on");
        } else if (!this.h) {
            com.google.ads.util.d.c("To get test ads on this device, call adRequest.addTestDevice(" + (AdUtil.c() ? "AdRequest.TEST_EMULATOR" : "\"" + AdUtil.a(context) + "\"") + ");");
            this.h = true;
        }
        if (this.e != null) {
            hashMap.put("extras", this.e);
        }
        return hashMap;
    }

    public void a(a aVar) {
        this.b = aVar;
    }

    public void a(String str) {
        if (this.d == null) {
            this.d = new HashSet();
        }
        this.d.add(str);
    }

    public void a(boolean z) {
        this.g = z;
    }

    public boolean b(Context context) {
        if (this.i != null) {
            String a2 = AdUtil.a(context);
            if (a2 == null) {
                return false;
            }
            if (this.i.contains(a2)) {
                return true;
            }
        }
        return false;
    }
}
