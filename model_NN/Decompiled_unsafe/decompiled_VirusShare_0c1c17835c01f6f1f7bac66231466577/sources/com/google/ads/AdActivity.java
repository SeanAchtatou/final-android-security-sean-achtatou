package com.google.ads;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.google.ads.util.d;

public class AdActivity extends Activity implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, View.OnClickListener {
    private static final Object a = new Object();
    private static AdActivity b = null;
    private static c c = null;
    private static AdActivity d = null;
    private static AdActivity e = null;
    private b f;
    private long g;
    private RelativeLayout h;
    private AdActivity i = null;
    private boolean j;
    private VideoView k;

    private void a(b bVar, boolean z, int i2) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        if (bVar.getParent() != null) {
            a("Interstitial created with an AdWebView that has a parent.");
        } else if (bVar.b() != null) {
            a("Interstitial created with an AdWebView that is already in use by another AdActivity.");
        } else {
            setRequestedOrientation(i2);
            bVar.a(this);
            ImageButton imageButton = new ImageButton(getApplicationContext());
            imageButton.setImageResource(17301527);
            imageButton.setBackgroundColor(0);
            imageButton.setOnClickListener(this);
            imageButton.setPadding(0, 0, 0, 0);
            int applyDimension = (int) TypedValue.applyDimension(1, 32.0f, getResources().getDisplayMetrics());
            FrameLayout frameLayout = new FrameLayout(getApplicationContext());
            frameLayout.addView(imageButton, applyDimension, applyDimension);
            this.h.addView(bVar, new ViewGroup.LayoutParams(-1, -1));
            this.h.addView(frameLayout);
            this.h.setKeepScreenOn(true);
            setContentView(this.h);
            if (z) {
                g.a(bVar);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0024, code lost:
        r1 = new android.content.Intent(r0.getApplicationContext(), com.google.ads.AdActivity.class);
        r1.putExtra("com.google.ads.AdOpener", r5.a());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.google.ads.util.d.a("Launching AdActivity.");
        r0.startActivity(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
        com.google.ads.util.d.a(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r0 = r4.d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r0 != null) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        com.google.ads.util.d.e("activity was null while launching an AdActivity.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(defpackage.c r4, defpackage.d r5) {
        /*
            java.lang.Object r0 = com.google.ads.AdActivity.a
            monitor-enter(r0)
            c r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x0016
            com.google.ads.AdActivity.c = r4     // Catch:{ all -> 0x0021 }
        L_0x0009:
            monitor-exit(r0)
            android.app.Activity r0 = r4.d()
            if (r0 != 0) goto L_0x0024
            java.lang.String r0 = "activity was null while launching an AdActivity."
            com.google.ads.util.d.e(r0)
        L_0x0015:
            return
        L_0x0016:
            c r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 == r4) goto L_0x0009
            java.lang.String r1 = "Tried to launch a new AdActivity with a different AdManager."
            com.google.ads.util.d.b(r1)     // Catch:{ all -> 0x0021 }
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            goto L_0x0015
        L_0x0021:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0024:
            android.content.Intent r1 = new android.content.Intent
            android.content.Context r2 = r0.getApplicationContext()
            java.lang.Class<com.google.ads.AdActivity> r3 = com.google.ads.AdActivity.class
            r1.<init>(r2, r3)
            java.lang.String r2 = "com.google.ads.AdOpener"
            android.os.Bundle r3 = r5.a()
            r1.putExtra(r2, r3)
            java.lang.String r2 = "Launching AdActivity."
            com.google.ads.util.d.a(r2)     // Catch:{ ActivityNotFoundException -> 0x0041 }
            r0.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x0041 }
            goto L_0x0015
        L_0x0041:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.google.ads.util.d.a(r1, r0)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.a(c, d):void");
    }

    private void a(String str) {
        d.b(str);
        finish();
    }

    public static boolean c() {
        boolean z;
        synchronized (a) {
            z = d != null;
        }
        return z;
    }

    public VideoView a() {
        return this.k;
    }

    public void a(VideoView videoView) {
        this.k = videoView;
        if (this.f == null) {
            a("Couldn't get adWebView to show the video.");
            return;
        }
        this.f.setBackgroundColor(0);
        videoView.setOnCompletionListener(this);
        videoView.setOnPreparedListener(this);
        videoView.setOnErrorListener(this);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        LinearLayout linearLayout = new LinearLayout(getApplicationContext());
        linearLayout.setGravity(17);
        linearLayout.addView(videoView, layoutParams);
        this.h.addView(linearLayout, 0, layoutParams);
    }

    public b b() {
        if (this.i != null) {
            return this.i.f;
        }
        synchronized (a) {
            if (c == null) {
                d.e("currentAdManager was null while trying to get the opening AdWebView.");
                return null;
            }
            b h2 = c.h();
            if (h2 != this.f) {
                return h2;
            }
            return null;
        }
    }

    public void onClick(View view) {
        finish();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        d.d("Video finished playing.");
        if (this.k != null) {
            this.k.setVisibility(8);
        }
        g.a(this.f, "onVideoEvent", "{'event': 'finish'}");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0040, code lost:
        r9.h = null;
        r9.j = false;
        r9.k = null;
        r0 = getIntent().getBundleExtra("com.google.ads.AdOpener");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0050, code lost:
        if (r0 != null) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        a("Could not get the Bundle used to create AdActivity.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0062, code lost:
        r1 = new defpackage.d(r0);
        r0 = r1.b();
        r7 = r1.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0075, code lost:
        if (r0.equals("intent") == false) goto L_0x00fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0077, code lost:
        r9.f = null;
        r9.g = android.os.SystemClock.elapsedRealtime();
        r9.j = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0081, code lost:
        if (r7 != null) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0083, code lost:
        a("Could not get the paramMap in launchIntent()");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0089, code lost:
        r0 = (java.lang.String) r7.get("u");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0091, code lost:
        if (r0 != null) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0093, code lost:
        a("Could not get the URL parameter in launchIntent().");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0099, code lost:
        r1 = (java.lang.String) r7.get("i");
        r2 = (java.lang.String) r7.get("m");
        r0 = android.net.Uri.parse(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ad, code lost:
        if (r1 != null) goto L_0x00e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00af, code lost:
        r0 = new android.content.Intent("android.intent.action.VIEW", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b7, code lost:
        r1 = com.google.ads.AdActivity.a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00b9, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00bc, code lost:
        if (com.google.ads.AdActivity.b != null) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00be, code lost:
        com.google.ads.AdActivity.b = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00c2, code lost:
        if (com.google.ads.AdActivity.c == null) goto L_0x00f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c4, code lost:
        com.google.ads.AdActivity.c.r();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00c9, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        com.google.ads.util.d.a("Launching an intent from AdActivity.");
        startActivity(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d3, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00d4, code lost:
        com.google.ads.util.d.a(r0.getMessage(), r0);
        finish();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00e0, code lost:
        r3 = new android.content.Intent(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00e5, code lost:
        if (r2 == null) goto L_0x00ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e7, code lost:
        r3.setDataAndType(r0, r2);
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00ec, code lost:
        r3.setData(r0);
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        com.google.ads.util.d.e("currentAdManager is null while trying to call onLeaveApplication().");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00fa, code lost:
        r9.h = new android.widget.RelativeLayout(getApplicationContext());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x010b, code lost:
        if (r0.equals("webapp") == false) goto L_0x018c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x010d, code lost:
        r9.f = new defpackage.b(getApplicationContext(), null);
        r0 = new defpackage.n(r6, defpackage.g.b, true, true);
        r0.b();
        r9.f.setWebViewClient(r0);
        r0 = (java.lang.String) r7.get("u");
        r1 = (java.lang.String) r7.get("baseurl");
        r2 = (java.lang.String) r7.get("html");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x013f, code lost:
        if (r0 == null) goto L_0x0161;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0141, code lost:
        r9.f.loadUrl(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0146, code lost:
        r0 = (java.lang.String) r7.get("o");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0154, code lost:
        if ("p".equals(r0) == false) goto L_0x0174;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0156, code lost:
        r0 = com.google.ads.util.AdUtil.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x015a, code lost:
        a(r9.f, false, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0161, code lost:
        if (r2 == null) goto L_0x016d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0163, code lost:
        r9.f.loadDataWithBaseURL(r1, r2, "text/html", "utf-8", null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x016d, code lost:
        a("Could not get the URL or HTML parameter to show a web app.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x017a, code lost:
        if ("l".equals(r0) == false) goto L_0x0181;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x017c, code lost:
        r0 = com.google.ads.util.AdUtil.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0183, code lost:
        if (r9 != com.google.ads.AdActivity.d) goto L_0x018a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0185, code lost:
        r0 = r6.l();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x018a, code lost:
        r0 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0192, code lost:
        if (r0.equals("interstitial") == false) goto L_0x01a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0194, code lost:
        r9.f = r6.h();
        a(r9.f, true, r6.l());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01a5, code lost:
        a("Unknown AdOpener, <action: " + r0 + ">");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            r8 = 0
            r3 = 1
            r5 = 0
            super.onCreate(r10)
            java.lang.Object r0 = com.google.ads.AdActivity.a
            monitor-enter(r0)
            c r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x0058
            c r6 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x005f }
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x005f }
            if (r1 != 0) goto L_0x0018
            com.google.ads.AdActivity.d = r9     // Catch:{ all -> 0x005f }
            r6.q()     // Catch:{ all -> 0x005f }
        L_0x0018:
            com.google.ads.AdActivity r1 = r9.i     // Catch:{ all -> 0x005f }
            if (r1 != 0) goto L_0x0024
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.e     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x0024
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.e     // Catch:{ all -> 0x005f }
            r9.i = r1     // Catch:{ all -> 0x005f }
        L_0x0024:
            com.google.ads.AdActivity.e = r9     // Catch:{ all -> 0x005f }
            com.google.ads.b r1 = r6.e()     // Catch:{ all -> 0x005f }
            boolean r2 = r1 instanceof com.google.ads.AdView     // Catch:{ all -> 0x005f }
            if (r2 == 0) goto L_0x0032
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x005f }
            if (r2 == r9) goto L_0x003c
        L_0x0032:
            boolean r1 = r1 instanceof com.google.ads.f     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x003f
            com.google.ads.AdActivity r1 = r9.i     // Catch:{ all -> 0x005f }
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x005f }
            if (r1 != r2) goto L_0x003f
        L_0x003c:
            r6.s()     // Catch:{ all -> 0x005f }
        L_0x003f:
            monitor-exit(r0)     // Catch:{ all -> 0x005f }
            r9.h = r5
            r9.j = r8
            r9.k = r5
            android.content.Intent r0 = r9.getIntent()
            java.lang.String r1 = "com.google.ads.AdOpener"
            android.os.Bundle r0 = r0.getBundleExtra(r1)
            if (r0 != 0) goto L_0x0062
            java.lang.String r0 = "Could not get the Bundle used to create AdActivity."
            r9.a(r0)
        L_0x0057:
            return
        L_0x0058:
            java.lang.String r1 = "Could not get currentAdManager."
            r9.a(r1)     // Catch:{ all -> 0x005f }
            monitor-exit(r0)     // Catch:{ all -> 0x005f }
            goto L_0x0057
        L_0x005f:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x0062:
            d r1 = new d
            r1.<init>(r0)
            java.lang.String r0 = r1.b()
            java.util.HashMap r7 = r1.c()
            java.lang.String r1 = "intent"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00fa
            r9.f = r5
            long r0 = android.os.SystemClock.elapsedRealtime()
            r9.g = r0
            r9.j = r3
            if (r7 != 0) goto L_0x0089
            java.lang.String r0 = "Could not get the paramMap in launchIntent()"
            r9.a(r0)
            goto L_0x0057
        L_0x0089:
            java.lang.String r0 = "u"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 != 0) goto L_0x0099
            java.lang.String r0 = "Could not get the URL parameter in launchIntent()."
            r9.a(r0)
            goto L_0x0057
        L_0x0099:
            java.lang.String r1 = "i"
            java.lang.Object r1 = r7.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "m"
            java.lang.Object r2 = r7.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            android.net.Uri r0 = android.net.Uri.parse(r0)
            if (r1 != 0) goto L_0x00e0
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.<init>(r2, r0)
            r0 = r1
        L_0x00b7:
            java.lang.Object r1 = com.google.ads.AdActivity.a
            monitor-enter(r1)
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.b     // Catch:{ all -> 0x00f7 }
            if (r2 != 0) goto L_0x00c9
            com.google.ads.AdActivity.b = r9     // Catch:{ all -> 0x00f7 }
            c r2 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x00f7 }
            if (r2 == 0) goto L_0x00f1
            c r2 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x00f7 }
            r2.r()     // Catch:{ all -> 0x00f7 }
        L_0x00c9:
            monitor-exit(r1)     // Catch:{ all -> 0x00f7 }
            java.lang.String r1 = "Launching an intent from AdActivity."
            com.google.ads.util.d.a(r1)     // Catch:{ ActivityNotFoundException -> 0x00d3 }
            r9.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x00d3 }
            goto L_0x0057
        L_0x00d3:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.google.ads.util.d.a(r1, r0)
            r9.finish()
            goto L_0x0057
        L_0x00e0:
            android.content.Intent r3 = new android.content.Intent
            r3.<init>(r1)
            if (r2 == 0) goto L_0x00ec
            r3.setDataAndType(r0, r2)
            r0 = r3
            goto L_0x00b7
        L_0x00ec:
            r3.setData(r0)
            r0 = r3
            goto L_0x00b7
        L_0x00f1:
            java.lang.String r2 = "currentAdManager is null while trying to call onLeaveApplication()."
            com.google.ads.util.d.e(r2)     // Catch:{ all -> 0x00f7 }
            goto L_0x00c9
        L_0x00f7:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x00fa:
            android.widget.RelativeLayout r1 = new android.widget.RelativeLayout
            android.content.Context r2 = r9.getApplicationContext()
            r1.<init>(r2)
            r9.h = r1
            java.lang.String r1 = "webapp"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x018c
            b r0 = new b
            android.content.Context r1 = r9.getApplicationContext()
            r0.<init>(r1, r5)
            r9.f = r0
            n r0 = new n
            java.util.Map r1 = defpackage.g.b
            r0.<init>(r6, r1, r3, r3)
            r0.b()
            b r1 = r9.f
            r1.setWebViewClient(r0)
            java.lang.String r0 = "u"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "baseurl"
            java.lang.Object r1 = r7.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "html"
            java.lang.Object r2 = r7.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            if (r0 == 0) goto L_0x0161
            b r1 = r9.f
            r1.loadUrl(r0)
        L_0x0146:
            java.lang.String r0 = "o"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "p"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x0174
            int r0 = com.google.ads.util.AdUtil.b()
        L_0x015a:
            b r1 = r9.f
            r9.a(r1, r8, r0)
            goto L_0x0057
        L_0x0161:
            if (r2 == 0) goto L_0x016d
            b r0 = r9.f
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)
            goto L_0x0146
        L_0x016d:
            java.lang.String r0 = "Could not get the URL or HTML parameter to show a web app."
            r9.a(r0)
            goto L_0x0057
        L_0x0174:
            java.lang.String r1 = "l"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x0181
            int r0 = com.google.ads.util.AdUtil.a()
            goto L_0x015a
        L_0x0181:
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.d
            if (r9 != r0) goto L_0x018a
            int r0 = r6.l()
            goto L_0x015a
        L_0x018a:
            r0 = -1
            goto L_0x015a
        L_0x018c:
            java.lang.String r1 = "interstitial"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x01a5
            b r0 = r6.h()
            r9.f = r0
            int r0 = r6.l()
            b r1 = r9.f
            r9.a(r1, r3, r0)
            goto L_0x0057
        L_0x01a5:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown AdOpener, <action: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = ">"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r9.a(r0)
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.onCreate(android.os.Bundle):void");
    }

    public void onDestroy() {
        if (this.h != null) {
            this.h.removeAllViews();
        }
        if (this.f != null) {
            g.b(this.f);
            this.f.a(null);
        }
        if (isFinishing()) {
            if (this.k != null) {
                this.k.stopPlayback();
                this.k = null;
            }
            synchronized (a) {
                if (!(c == null || this.f == null)) {
                    if (this.f == c.h()) {
                        c.a();
                    }
                    this.f.stopLoading();
                    this.f.destroy();
                }
                if (this == d) {
                    if (c != null) {
                        c.p();
                        c = null;
                    } else {
                        d.e("currentAdManager is null while trying to destroy AdActivity.");
                    }
                    d = null;
                }
            }
            if (this == b) {
                b = null;
            }
            e = this.i;
        }
        d.a("AdActivity is closing.");
        super.onDestroy();
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        d.e("Video threw error! <what:" + i2 + ", extra:" + i3 + ">");
        finish();
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        d.d("Video is ready to play.");
        g.a(this.f, "onVideoEvent", "{'event': 'load'}");
    }

    public void onWindowFocusChanged(boolean z) {
        if (this.j && z && SystemClock.elapsedRealtime() - this.g > 250) {
            d.d("Launcher AdActivity got focus and is closing.");
            finish();
        }
        super.onWindowFocusChanged(z);
    }
}
