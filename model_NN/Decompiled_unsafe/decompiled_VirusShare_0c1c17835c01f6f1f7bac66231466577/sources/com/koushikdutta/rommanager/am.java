package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

class am implements View.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ bv a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;
    private final /* synthetic */ TextView d;
    private final /* synthetic */ ListView e;
    private final /* synthetic */ ArrayAdapter f;

    am(bv bvVar, String str, String str2, TextView textView, ListView listView, ArrayAdapter arrayAdapter) {
        this.a = bvVar;
        this.b = str;
        this.c = str2;
        this.d = textView;
        this.e = listView;
        this.f = arrayAdapter;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomList, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void onClick(View view) {
        if (!gd.a(this.a.b, (dw) null)) {
            gd.a((Context) this.a.b, (int) C0000R.string.premium_only);
        } else if (this.b == null || (this.a.a != null && this.a.a.optString("nickname", null) == null)) {
            gd.a((Context) this.a.b, (int) C0000R.string.login_required);
        } else if (this.a.a == null || this.a.a.optInt("rating") == 0) {
            gd.a((Context) this.a.b, (int) C0000R.string.must_rate);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a.b);
            EditText editText = new EditText(this.a.b);
            if (this.a.a != null) {
                String optString = this.a.a.optString("comment");
                if (!gd.b(optString)) {
                    editText.setText(optString);
                }
            }
            editText.setSingleLine(false);
            editText.setGravity(48);
            editText.setLines(4);
            editText.setMaxLines(4);
            editText.selectAll();
            builder.setView(editText);
            builder.setPositiveButton((int) C0000R.string.post_comment, new bl(this, this.c, editText, this.b, this.d, this.e, this.f));
            builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
            builder.setCancelable(true);
            builder.setTitle((int) C0000R.string.post_comment);
            builder.create().show();
        }
    }
}
