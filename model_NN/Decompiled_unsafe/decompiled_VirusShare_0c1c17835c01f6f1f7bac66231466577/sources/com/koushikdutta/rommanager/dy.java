package com.koushikdutta.rommanager;

import android.content.Context;
import java.text.SimpleDateFormat;
import java.util.Date;

class dy extends Thread {
    final /* synthetic */ BackupReceiver a;
    private final /* synthetic */ Context b;

    dy(BackupReceiver backupReceiver, Context context) {
        this.a = backupReceiver;
        this.b = context;
    }

    public void run() {
        gd.a(this.b, (dw) null);
        gd.b(this.b);
        gd.e(this.b, "ScheduledBackup-" + new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss").format(new Date()));
    }
}
