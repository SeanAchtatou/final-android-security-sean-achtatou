package com.koushikdutta.rommanager;

import android.app.AlertDialog;

class a implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ gv a;

    a(gv gvVar) {
        this.a = gvVar;
    }

    public void run() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.a.a);
        builder.setTitle((int) C0000R.string.confirm_version);
        builder.setItems(this.a.a.a.getResources().getStringArray(C0000R.array.clockworkmod_versions), new aw(this));
        builder.create().show();
    }
}
