package com.koushikdutta.rommanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.google.android.apps.analytics.j;

public class CheckinReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        try {
            if ("android.intent.action.DATE_CHANGED".equals(intent.getAction())) {
                j a = j.a();
                a.a("checkin", h.a(context).a("detected_device"), String.valueOf(gd.a(context, (dw) null)), 0);
                a.d();
            }
        } catch (Exception e) {
        }
    }
}
