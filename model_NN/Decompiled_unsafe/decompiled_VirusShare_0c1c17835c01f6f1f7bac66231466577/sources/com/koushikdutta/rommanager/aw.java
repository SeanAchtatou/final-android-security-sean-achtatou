package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;

class aw implements DialogInterface.OnClickListener {
    final /* synthetic */ a a;

    aw(a aVar) {
        this.a = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.a(java.lang.String, int):void
      com.koushikdutta.rommanager.h.a(java.lang.String, long):void
      com.koushikdutta.rommanager.h.a(java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.a.a.c.a("is_clockworkmod", true);
        this.a.a.a.a.c.a("current_recovery_version", String.format("%d.X.X.X", Integer.valueOf(i + 2)));
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.a.a.a);
        builder.setMessage((int) C0000R.string.overriden);
        builder.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
        builder.create().show();
        this.a.a.a.a.h();
        this.a.a.a.a.i();
    }
}
