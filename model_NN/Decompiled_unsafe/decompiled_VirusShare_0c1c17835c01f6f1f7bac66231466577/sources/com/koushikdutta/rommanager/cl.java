package com.koushikdutta.rommanager;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

class cl extends ArrayAdapter {
    final /* synthetic */ ActivityBase a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cl(ActivityBase activityBase, Context context) {
        super(context, 0);
        this.a = activityBase;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        return ((ck) getItem(i)).a(this.a, view);
    }

    public boolean isEnabled(int i) {
        return ((ck) getItem(i)).f;
    }
}
