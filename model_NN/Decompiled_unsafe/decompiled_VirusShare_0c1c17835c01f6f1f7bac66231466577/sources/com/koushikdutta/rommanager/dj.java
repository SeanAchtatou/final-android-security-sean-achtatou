package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.util.Log;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class dj implements DialogInterface.OnClickListener {
    final /* synthetic */ RomList a;
    private final /* synthetic */ ArrayList b;
    private final /* synthetic */ JSONArray c;
    private final /* synthetic */ int[] d;
    private final /* synthetic */ int e;
    private final /* synthetic */ JSONObject f;
    private final /* synthetic */ JSONObject g;
    private final /* synthetic */ JSONArray h;
    private final /* synthetic */ JSONArray i;
    private final /* synthetic */ boolean[] j;

    dj(RomList romList, ArrayList arrayList, JSONArray jSONArray, int[] iArr, int i2, JSONObject jSONObject, JSONObject jSONObject2, JSONArray jSONArray2, JSONArray jSONArray3, boolean[] zArr) {
        this.a = romList;
        this.b = arrayList;
        this.c = jSONArray;
        this.d = iArr;
        this.e = i2;
        this.f = jSONObject;
        this.g = jSONObject2;
        this.h = jSONArray2;
        this.i = jSONArray3;
        this.j = zArr;
    }

    public void onClick(DialogInterface dialogInterface, int i2) {
        try {
            this.b.add(this.c.getJSONObject(this.d[this.e]));
        } catch (JSONException e2) {
            Log.e("Romlist", e2.getLocalizedMessage(), e2);
        }
        this.a.a(this.f, this.g, this.h, this.i, this.e + 1, this.d, this.j, this.b);
    }
}
