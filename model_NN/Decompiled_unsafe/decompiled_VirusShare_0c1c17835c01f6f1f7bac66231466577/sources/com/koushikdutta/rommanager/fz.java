package com.koushikdutta.rommanager;

import android.content.Context;
import android.content.ServiceConnection;
import com.koushikdutta.rommanager.b.b;

class fz extends b {
    final /* synthetic */ ax a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ dw c;
    private final /* synthetic */ ServiceConnection d;

    fz(ax axVar, Context context, dw dwVar, ServiceConnection serviceConnection) {
        this.a = axVar;
        this.b = context;
        this.c = dwVar;
        this.d = serviceConnection;
    }

    public void a(boolean z, String str, String str2) {
        gd.b(this.b, this.c, z, str, str2);
        this.b.unbindService(this.d);
    }
}
