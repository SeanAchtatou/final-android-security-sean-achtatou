package com.koushikdutta.rommanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.koushikdutta.a.a;

class bk extends ArrayAdapter {
    final /* synthetic */ bv a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bk(bv bvVar, Context context, int i) {
        super(context, i);
        this.a = bvVar;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView;
        ImageView imageView2;
        if (view == null) {
            imageView = (ImageView) LayoutInflater.from(this.a.b).inflate((int) C0000R.layout.screenshot, (ViewGroup) null);
            imageView2 = imageView;
        } else {
            imageView = (ImageView) view;
            imageView2 = view;
        }
        a.a(imageView, (String) getItem(i), C0000R.drawable.loading);
        return imageView2;
    }
}
