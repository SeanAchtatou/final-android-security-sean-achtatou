package com.koushikdutta.rommanager;

import android.content.DialogInterface;

class bt implements DialogInterface.OnMultiChoiceClickListener {
    final /* synthetic */ RomList a;
    private final /* synthetic */ boolean[] b;

    bt(RomList romList, boolean[] zArr) {
        this.a = romList;
        this.b = zArr;
    }

    public void onClick(DialogInterface dialogInterface, int i, boolean z) {
        this.b[i] = !this.b[i];
    }
}
