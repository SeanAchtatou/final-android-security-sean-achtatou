package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;

class fn extends ck {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    fn(RomManager romManager, ActivityBase activityBase, int i, int i2, int i3) {
        super(activityBase, i, i2, i3);
        this.a = romManager;
    }

    public void a() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        builder.setCancelable(true);
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.setMessage((int) C0000R.string.flash_alternate_warning);
        builder.setTitle((int) C0000R.string.flash_alternate);
        builder.setPositiveButton(17039370, new dl(this));
        builder.create().show();
    }
}
