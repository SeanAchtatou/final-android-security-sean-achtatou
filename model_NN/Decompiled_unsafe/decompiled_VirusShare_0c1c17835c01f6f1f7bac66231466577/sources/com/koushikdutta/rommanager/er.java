package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.widget.Toast;

class er extends ck {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    er(RomManager romManager, ActivityBase activityBase, int i, int i2, int i3) {
        super(activityBase, i, i2, i3);
        this.a = romManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    public void a() {
        String a2 = this.a.c.a("current_recovery_version");
        String a3 = this.a.c.a("detected_device");
        if (!this.a.c.b("is_clockworkmod", false) || a2 == null || a3 == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
            if (!gd.a(this.a, (dw) null)) {
                gd.a(this.a, (int) C0000R.string.flash_recovery, (int) C0000R.string.flash_clockwork_or_get_premium);
                return;
            }
            builder.setTitle((int) C0000R.string.recovery);
            builder.setMessage((int) C0000R.string.verify_custom_flashed);
            builder.setPositiveButton(17039379, new ea(this));
            builder.setNegativeButton(17039369, (DialogInterface.OnClickListener) null);
            builder.create().show();
            return;
        }
        this.a.startActivity(new Intent(this.a, InstallRom.class));
    }

    public boolean b() {
        PackageManager packageManager = this.a.getPackageManager();
        if (packageManager.getComponentEnabledSetting(new ComponentName(this.a, "com.koushikdutta.rommanager.BurritoManager")) == 1) {
            packageManager.setComponentEnabledSetting(new ComponentName(this.a, "com.koushikdutta.rommanager.BurritoManager"), 2, 1);
        } else {
            packageManager.setComponentEnabledSetting(new ComponentName(this.a, "com.koushikdutta.rommanager.BurritoManager"), 1, 1);
        }
        Toast.makeText(this.a, (int) C0000R.string.toggled_burrito, 1).show();
        return true;
    }
}
