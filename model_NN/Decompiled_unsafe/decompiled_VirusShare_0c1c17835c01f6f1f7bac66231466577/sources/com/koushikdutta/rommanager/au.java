package com.koushikdutta.rommanager;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import com.koushikdutta.rommanager.a.c;
import java.util.ArrayList;

class au implements DialogInterface.OnClickListener {
    private final /* synthetic */ ArrayList a;
    private final /* synthetic */ h b;
    private final /* synthetic */ Activity c;

    au(ArrayList arrayList, h hVar, Activity activity) {
        this.a = arrayList;
        this.b = hVar;
        this.c = activity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [android.app.Activity, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            StringBuilder sb = new StringBuilder();
            String d = c.d((String) this.a.get(0));
            sb.append(" mkdir -p /cache/recovery ; ");
            sb.append(String.format(" echo '--update_package=%s' > /cache/recovery/command ;", d));
            sb.append(" sync ;");
            String a2 = this.b.a("reboot_recovery");
            if (!gd.b(a2)) {
                sb.append(a2);
            } else {
                sb.append(" reboot recovery ; " + this.c.getFilesDir() + "/reboot recovery ; ");
            }
            gd.c(this.c, sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            gd.a((Context) this.c, (int) C0000R.string.script_error);
        }
    }
}
