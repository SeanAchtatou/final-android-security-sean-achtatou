package com.koushikdutta.rommanager;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import com.a.a.b.d;
import com.a.a.b.e;
import com.a.a.b.f;
import java.lang.reflect.Method;
import org.json.JSONObject;

class eu implements ServiceConnection {
    final /* synthetic */ RomManager a;
    private final /* synthetic */ ProgressDialog b;
    private final /* synthetic */ String c;

    eu(RomManager romManager, ProgressDialog progressDialog, String str) {
        this.a = romManager;
        this.b = progressDialog;
        this.c = str;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.b.dismiss();
        Bundle a2 = gd.a(this.a, "CHECK_BILLING_SUPPORTED");
        d a3 = f.a(iBinder);
        try {
            if (e.a(a3.a(a2).getInt("RESPONSE_CODE")) != e.RESULT_OK) {
                throw new Exception();
            }
            Bundle a4 = gd.a(this.a, "REQUEST_PURCHASE");
            a4.putString("ITEM_ID", this.c);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("deviceId", gd.f(this.a));
            jSONObject.put("accounts", p.a(this.a));
            jSONObject.put("detected_device", this.a.c.a("detected_device"));
            a4.putString("DEVELOPER_PAYLOAD", jSONObject.toString());
            Bundle a5 = a3.a(a4);
            if (e.a(a5.getInt("RESPONSE_CODE")) != e.RESULT_OK) {
                throw new Exception();
            }
            PendingIntent pendingIntent = (PendingIntent) a5.getParcelable("PURCHASE_INTENT");
            Method method = null;
            Class<RomManager> cls = RomManager.class;
            try {
                method = cls.getMethod("startIntentSender", IntentSender.class, Intent.class, Integer.TYPE, Integer.TYPE, Integer.TYPE);
            } catch (Exception e) {
            }
            if (method != null) {
                method.invoke(this.a, pendingIntent.getIntentSender(), null, 0, 0, 0);
            } else {
                pendingIntent.send(this.a, 0, new Intent());
            }
            try {
                this.a.unbindService(this);
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            this.a.b(this.c);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
    }
}
