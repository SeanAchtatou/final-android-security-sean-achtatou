package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.content.Intent;
import java.util.ArrayList;

class dm implements DialogInterface.OnClickListener {
    final /* synthetic */ RomList a;
    private final /* synthetic */ RomPackage b;

    dm(RomList romList, RomPackage romPackage) {
        this.a = romList;
        this.b = romPackage;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this.a, InstallRom.class);
        intent.putExtra("rompackage", this.b);
        intent.putStringArrayListExtra("additionalfiles", new ArrayList());
        this.a.startActivity(intent);
    }
}
