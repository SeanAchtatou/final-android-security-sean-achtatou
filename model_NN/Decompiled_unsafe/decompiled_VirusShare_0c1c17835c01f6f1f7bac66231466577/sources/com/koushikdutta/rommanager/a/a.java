package com.koushikdutta.rommanager.a;

public class a extends b {
    public void a(String str) {
        a("backup_rom(\"%s\");", str);
    }

    public void a(String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        Object[] objArr = new Object[6];
        objArr[0] = str;
        objArr[1] = z ? "boot" : "noboot";
        objArr[2] = z2 ? "system" : "nosystem";
        objArr[3] = z3 ? "data" : "nodata";
        objArr[4] = z4 ? "cache" : "nocache";
        objArr[5] = z5 ? "sd-ext" : "nosd-ext";
        a("restore_rom(\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\");", objArr);
    }

    public void a(String str, String... strArr) {
        a("ui_print(\"%s\");", String.format(str, strArr));
    }

    public void b(String str) {
        a("install_zip(\"%s\");", a(str, "/mnt/sdcard", "/sdcard"));
    }

    public void b(String str, String... strArr) {
        String str2 = "";
        for (int i = 0; i < strArr.length; i++) {
            str2 = String.valueOf(str2) + ", \"" + strArr[i] + "\"";
        }
        a("run_program(\"%s\"%s);", str, str2);
    }

    public void c(String str) {
        a("format(\"%s\");", str);
    }
}
