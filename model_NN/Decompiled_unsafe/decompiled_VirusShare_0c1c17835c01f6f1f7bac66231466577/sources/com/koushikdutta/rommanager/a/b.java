package com.koushikdutta.rommanager.a;

import android.content.Context;
import com.koushikdutta.rommanager.bg;
import com.koushikdutta.rommanager.h;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class b {
    public StringBuilder a = new StringBuilder();

    public static b a(Context context) {
        h a2 = h.a(context);
        String a3 = a2.a("current_recovery_version");
        return (!a2.b("is_clockworkmod", false) || a3 == null || "3.0.0.0".compareTo(a3) > 0) ? new c() : new a();
    }

    protected static String a(String str, String str2, String str3) {
        return str.startsWith(str2) ? String.valueOf(str3) + str.substring(str2.length()) : str;
    }

    public void a() {
        a(String.format("%s/%s", "/sdcard/clockworkmod/backup", new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss").format(new Date())));
    }

    public void a(Context context, StringBuilder sb) {
        try {
            String absolutePath = context.getFileStreamPath("extendedcommand").getAbsolutePath();
            String sb2 = this.a.toString();
            bg.a(absolutePath, sb2);
            String bigInteger = new BigInteger(1, MessageDigest.getInstance("MD5").digest(("ROM Manager and its distribution of ClockworkMod recovery are copyright ClockworkMod, LLC. If you are trying to integrate with ClockworkMod recovery, please build and distribute your own version of ClockworkMod recovery. Unauthorized reverse engineering and usage of ClockworkMod, LLC's ROM Manager and distributed ClockworkMod Recovery are in violation of the DMCA, and will be prosecuted." + sb2).getBytes())).toString(16);
            Runtime.getRuntime().exec(new String[]{"sh", "-c", String.format("echo %s > %s", bigInteger, "/sdcard/clockworkmod/.salted_hash")}).waitFor();
            sb.append(String.format("mkdir -p %s ; ", "/cache/recovery"));
            sb.append(String.format("cat %s > %s ; ", absolutePath, "/cache/recovery/extendedcommand"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public abstract void a(String str);

    public abstract void a(String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5);

    /* access modifiers changed from: protected */
    public void a(String str, Object... objArr) {
        this.a.append(String.valueOf(String.format(str, objArr)) + "\n");
    }

    public abstract void a(String str, String... strArr);

    public abstract void b(String str);

    public abstract void b(String str, String... strArr);

    public abstract void c(String str);

    public String toString() {
        return this.a.toString();
    }
}
