package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;

class et extends ck {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    et(RomManager romManager, ActivityBase activityBase, int i, int i2, int i3) {
        super(activityBase, i, i2, i3);
        this.a = romManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    public void a() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        builder.setTitle((int) C0000R.string.reboot_recovery);
        if (this.a.c.b("is_clockworkmod", false)) {
            builder.setMessage((int) C0000R.string.confirm_reboot);
        } else {
            String a2 = this.a.c.a("alternate_recovery_name");
            if (a2 == null) {
                a2 = "Recovery";
            }
            builder.setMessage(this.a.getString(C0000R.string.confirm_reboot_alternate, new Object[]{a2}));
        }
        builder.setCancelable(true);
        builder.setIcon(0);
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton(17039370, new bb(this));
        builder.create().show();
    }
}
