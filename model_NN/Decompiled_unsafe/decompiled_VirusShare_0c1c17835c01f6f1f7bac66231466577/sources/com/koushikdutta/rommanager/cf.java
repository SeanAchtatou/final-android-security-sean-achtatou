package com.koushikdutta.rommanager;

import android.content.Intent;
import android.preference.Preference;
import java.io.File;

class cf implements Preference.OnPreferenceClickListener {
    final /* synthetic */ InstallRom a;
    private final /* synthetic */ File b;

    cf(InstallRom installRom, File file) {
        this.a = installRom;
        this.b = file;
    }

    public boolean onPreferenceClick(Preference preference) {
        Intent intent = new Intent(this.a, InstallRom.class);
        intent.putExtra("path", this.b.getAbsolutePath());
        intent.putStringArrayListExtra("zips", this.a.f);
        this.a.startActivity(intent);
        return true;
    }
}
