package com.koushikdutta.rommanager;

import android.content.Intent;
import android.view.MenuItem;

class ey implements MenuItem.OnMenuItemClickListener {
    final /* synthetic */ RomManager a;

    ey(RomManager romManager) {
        this.a = romManager;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        this.a.startActivityForResult(new Intent(this.a, SettingsScreen.class), 0);
        return true;
    }
}
