package com.koushikdutta.rommanager;

import android.content.DialogInterface;

class du implements DialogInterface.OnClickListener {
    final /* synthetic */ fl a;

    du(fl flVar) {
        this.a = flVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.RomManager.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.RomManager.a(com.koushikdutta.rommanager.RomManager, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.ActivityBase.a(java.lang.String, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.RomManager.a(java.lang.String, java.lang.String, boolean):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        String a2 = this.a.b.c.a("detected_device");
        boolean b = this.a.b.c.b("readonly_recovery", false);
        String str = this.a.b.j[this.a.a];
        if (!b) {
            this.a.b.a(str, String.format(this.a.b.k, str, a2), true);
            return;
        }
        this.a.b.a(str, String.format(this.a.b.l, str, a2));
    }
}
