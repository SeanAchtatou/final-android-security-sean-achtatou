package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.content.Intent;

class e implements DialogInterface.OnClickListener {
    final /* synthetic */ fc a;

    e(fc fcVar) {
        this.a = fcVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this.a.a, ScriptRunner.class);
        intent.putExtra("script", String.valueOf(this.a.a.getFilesDir().getAbsolutePath()) + "/fix_permissions");
        intent.putExtra("title", this.a.a.getString(C0000R.string.fixing_permissions));
        this.a.a.startActivity(intent);
    }
}
