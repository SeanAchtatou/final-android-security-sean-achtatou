package com.koushikdutta.rommanager;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.google.ads.AdView;
import com.google.ads.a;
import com.google.ads.d;
import com.google.ads.g;
import com.google.android.apps.analytics.j;

public class AnalyticsActivity extends PreferenceActivity implements dw {
    protected j a;
    boolean b = true;
    protected h c;
    boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public View f = null;

    private void c() {
        try {
            LayoutInflater from = LayoutInflater.from(this);
            Log.e("RomManager", "Displaying ads...");
            ListView listView = getListView();
            View inflate = from.inflate((int) C0000R.layout.admob, (ViewGroup) null);
            FrameLayout frameLayout = (FrameLayout) inflate.findViewById(C0000R.id.adview);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            AdView adView = new AdView(this, g.a, "a14da1301aa3d95");
            adView.setLayoutParams(layoutParams);
            frameLayout.addView(adView);
            addContentView(inflate, new ViewGroup.LayoutParams(-1, -1));
            d dVar = new d();
            dVar.a(false);
            dVar.a(a.MALE);
            gd.a(this, dVar);
            adView.a(dVar);
            this.f = frameLayout;
            listView.setOnScrollListener(new fw(this));
        } catch (Exception e2) {
        }
    }

    public void a() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* access modifiers changed from: package-private */
    public void a(CharSequence charSequence) {
        if (this.b) {
            try {
                this.a.a("/" + charSequence.toString().replace(' ', '_'));
                this.a.b();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void a(String str) {
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, String str3) {
        if (this.b) {
            try {
                this.a.a(str, str2, str3, 0);
                this.a.b();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void b() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = h.a(this);
        this.b = this.c.b("analytics", true);
        if (gd.e(this)) {
            c();
        }
        if (this.b) {
            try {
                this.a = j.a();
                this.a.a("UA-4956323-3", 10, this);
                a(getTitle());
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.d = true;
        if (this.b) {
            this.a.d();
        }
    }
}
