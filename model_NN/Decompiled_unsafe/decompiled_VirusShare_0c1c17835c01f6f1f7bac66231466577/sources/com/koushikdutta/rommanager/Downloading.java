package com.koushikdutta.rommanager;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Downloading extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.downloading);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 1;
        layoutParams.topMargin = 20;
        layoutParams.bottomMargin = 20;
        ImageView imageView = new ImageView(this);
        imageView.setImageResource(C0000R.drawable.clockwork512);
        ((LinearLayout) findViewById(C0000R.id.layout)).addView(imageView);
        if (imageView != null) {
            imageView.setLayoutParams(layoutParams);
        }
    }
}
