package com.koushikdutta.rommanager;

import android.content.Context;
import android.content.DialogInterface;
import com.koushikdutta.rommanager.a.b;

class bi implements DialogInterface.OnClickListener {
    final /* synthetic */ gj a;
    private final /* synthetic */ String b;
    private final /* synthetic */ boolean[] c;

    bi(gj gjVar, String str, boolean[] zArr) {
        this.a = gjVar;
        this.b = str;
        this.c = zArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.ManageBackups, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        StringBuilder sb = new StringBuilder();
        b a2 = b.a(this.a.a.b.a);
        gd.a(this.a.a.b.a, a2);
        if (this.a.a.a != null) {
            String replace = this.a.a.a.replace(' ', '_');
            gd.a(this.a.a.b.a, a2);
            a2.a(String.format("%s/%s", "/sdcard/clockworkmod/backup", replace));
        }
        a2.a(String.format("%s/%s", "/sdcard/clockworkmod/backup", this.b), this.c[0], this.c[1], this.c[2], this.c[3], this.c[4]);
        a2.a(this.a.a.b.a, sb);
        gd.a(this.a.a.b.a, sb);
        try {
            gd.c(this.a.a.b.a, sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            gd.a((Context) this.a.a.b.a, (int) C0000R.string.script_error);
        }
    }
}
