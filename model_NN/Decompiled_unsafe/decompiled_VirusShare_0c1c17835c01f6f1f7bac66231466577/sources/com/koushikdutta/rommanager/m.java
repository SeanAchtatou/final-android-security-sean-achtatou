package com.koushikdutta.rommanager;

import android.widget.ScrollView;

class m implements Runnable {
    final /* synthetic */ bc a;
    private final /* synthetic */ String b;
    private final /* synthetic */ ScrollView c;

    m(bc bcVar, String str, ScrollView scrollView) {
        this.a = bcVar;
        this.b = str;
        this.c = scrollView;
    }

    public void run() {
        this.a.a.a.setText(this.b);
        this.c.scrollTo(0, this.a.a.a.getHeight());
    }
}
