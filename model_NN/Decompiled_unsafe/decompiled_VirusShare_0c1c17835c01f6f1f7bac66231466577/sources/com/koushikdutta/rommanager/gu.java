package com.koushikdutta.rommanager;

import android.content.Context;

class gu implements cq {
    final /* synthetic */ RomManager a;
    private final /* synthetic */ boolean b;
    private final /* synthetic */ String c;

    gu(RomManager romManager, boolean z, String str) {
        this.a = romManager;
        this.b = z;
        this.c = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void a(String str) {
        String str2;
        try {
            String str3 = this.a.getFilesDir() + "/flash_image";
            String str4 = "echo 3 > /proc/sys/vm/drop_caches ; ";
            if (this.a.c.b("erase_recovery", false)) {
                str4 = String.valueOf(str4) + (this.a.getFilesDir() + "/erase_image") + " recovery ; ";
            }
            String a2 = this.a.c.a("flash_recovery");
            if (gd.b(a2)) {
                str2 = String.valueOf(str4) + str3 + " recovery " + str + " ;";
            } else {
                str2 = String.valueOf(str4) + String.format(a2, str);
            }
            if (gd.c(this.a, String.valueOf(str2) + " sync ;") != 0) {
                throw new Exception();
            }
            this.a.c.a("is_clockworkmod", this.b);
            this.a.c.a("current_recovery_version", this.c);
            this.a.k();
            if (this.b) {
                gd.a((Context) this.a, (int) C0000R.string.flash_recovery_success);
            } else {
                gd.a((Context) this.a, (int) C0000R.string.flash_alternate_recovery_success);
            }
            this.a.h();
            this.a.e.setEnabled(true);
            this.a.i();
        } catch (Exception e) {
            e.printStackTrace();
            gd.a((Context) this.a, (int) C0000R.string.script_error);
        }
    }

    public boolean a() {
        return this.a.i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void b() {
        this.a.e.setEnabled(true);
        this.a.i();
        gd.a((Context) this.a, (int) C0000R.string.flash_recovery_error);
    }
}
