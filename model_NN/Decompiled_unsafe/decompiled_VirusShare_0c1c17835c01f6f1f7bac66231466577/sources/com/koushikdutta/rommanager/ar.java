package com.koushikdutta.rommanager;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import java.io.File;

class ar implements DialogInterface.OnClickListener {
    final /* synthetic */ fb a;
    private final /* synthetic */ EditText b;

    ar(fb fbVar, EditText editText) {
        this.a = fbVar;
        this.b = editText;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.a("Nandroid", "Backup", "Backup");
        if (new File("/sdcard/clockworkmod/backup", this.b.getText().toString().replace(' ', '_')).exists()) {
            gd.a((Context) this.a.a, (int) C0000R.string.backup_error);
        } else {
            gd.e(this.a.a, this.b.getText().toString());
        }
    }
}
