package com.koushikdutta.rommanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import org.json.JSONObject;

class ag extends ArrayAdapter {
    final /* synthetic */ bv a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ag(bv bvVar, Context context, int i) {
        super(context, i);
        this.a = bvVar;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? LayoutInflater.from(this.a.b).inflate((int) C0000R.layout.comment, (ViewGroup) null) : view;
        JSONObject jSONObject = (JSONObject) getItem(i);
        ((TextView) inflate.findViewById(C0000R.id.comment)).setText(jSONObject.optString("comment"));
        ((TextView) inflate.findViewById(C0000R.id.nickname)).setText(jSONObject.optString("nickname"));
        ((RatingBar) inflate.findViewById(C0000R.id.rating)).setRating((float) jSONObject.optInt("rating", 0));
        return inflate;
    }
}
