package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

class fl extends ck {
    int a = 0;
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    fl(RomManager romManager, ActivityBase activityBase, int i, int i2, int i3) {
        super(activityBase, i, i2, i3);
        this.b = romManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void a() {
        if (this.b.c.a("detected_device") == null && !this.b.j()) {
            return;
        }
        if (this.b.j == null || this.b.k == null) {
            gd.a((Context) this.b, (int) C0000R.string.flash_legacy_unavailable2);
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this.b);
        builder.setSingleChoiceItems(this.b.j, this.a, new dv(this));
        builder.setTitle((int) C0000R.string.flash_legacy_and_experimental);
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton(17039370, new du(this));
        builder.create().show();
    }
}
