package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

class fj implements DialogInterface.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ ew a;

    fj(ew ewVar) {
        this.a = ewVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            if (gd.f(this.a.a) == null) {
                gd.a((Context) this.a.a, (int) C0000R.string.no_device_id);
            } else {
                this.a.a.n();
            }
        } else if (i == 1) {
            this.a.a.c("donation");
        } else if (i == 2) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a.a);
            View inflate = LayoutInflater.from(this.a.a).inflate((int) C0000R.layout.paypal_information, (ViewGroup) null);
            builder.setView(inflate);
            builder.setTitle((int) C0000R.string.enter_purchase_information);
            builder.setPositiveButton(17039370, new eb(this, (EditText) inflate.findViewById(C0000R.id.paypal_email_id), (EditText) inflate.findViewById(C0000R.id.rommanager_transaction_number)));
            builder.setNegativeButton((CharSequence) null, (DialogInterface.OnClickListener) null);
            builder.create().show();
        }
    }
}
