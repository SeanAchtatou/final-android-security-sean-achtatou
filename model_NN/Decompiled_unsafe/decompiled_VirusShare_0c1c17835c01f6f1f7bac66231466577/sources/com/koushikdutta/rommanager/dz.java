package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import java.io.File;

class dz implements DialogInterface.OnClickListener {
    String a;
    /* access modifiers changed from: package-private */
    public final /* synthetic */ cr b;
    private final /* synthetic */ File c;
    private final /* synthetic */ String d;

    dz(cr crVar, File file, String str) {
        this.b = crVar;
        this.c = file;
        this.d = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case 0:
                if (this.c.getAbsolutePath().contains(" ")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this.b.a);
                    builder.setTitle((int) C0000R.string.restore);
                    builder.setMessage((int) C0000R.string.no_spaces);
                    builder.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
                    builder.create().show();
                    return;
                } else if (!new File(this.c, "nandroid.md5").exists()) {
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(this.b.a);
                    builder2.setTitle((int) C0000R.string.restore);
                    builder2.setMessage((int) C0000R.string.no_md5);
                    builder2.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
                    builder2.create().show();
                    return;
                } else {
                    boolean[] zArr = {true, true, true, true, true};
                    gj gjVar = new gj(this, this.d, zArr);
                    if (!this.b.a.e.b("developer_mode", false)) {
                        gjVar.run();
                        return;
                    }
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(this.b.a);
                    builder3.setIcon(17301543);
                    builder3.setTitle((int) C0000R.string.restore_options);
                    builder3.setMultiChoiceItems((int) C0000R.array.restore_partitions, zArr, new gi(this));
                    builder3.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
                    builder3.setCancelable(true);
                    builder3.setPositiveButton(17039370, new gh(this, gjVar));
                    builder3.create().show();
                    return;
                }
            case 1:
                AlertDialog.Builder builder4 = new AlertDialog.Builder(this.b.a);
                View inflate = View.inflate(this.b.a, C0000R.layout.romname, null);
                EditText editText = (EditText) inflate.findViewById(C0000R.id.romname);
                editText.setText(this.b.getTitle());
                editText.selectAll();
                builder4.setIcon(0);
                builder4.setTitle((int) C0000R.string.edit_backup_name);
                builder4.setCancelable(true);
                builder4.setPositiveButton(17039370, new gg(this, editText, this.d, this.c));
                builder4.setView(inflate);
                builder4.create().show();
                return;
            case 2:
                AlertDialog.Builder builder5 = new AlertDialog.Builder(this.b.a);
                builder5.setTitle((int) C0000R.string.confirm_delete_backup);
                builder5.setCancelable(true);
                builder5.setIcon(0);
                builder5.setPositiveButton(17039370, new gk(this, this.c));
                builder5.create().show();
                return;
            default:
                return;
        }
    }
}
