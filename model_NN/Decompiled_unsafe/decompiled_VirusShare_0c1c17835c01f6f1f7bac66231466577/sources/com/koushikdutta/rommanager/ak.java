package com.koushikdutta.rommanager;

import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONObject;

class ak implements cq {
    final /* synthetic */ bv a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ ListView c;
    private final /* synthetic */ JSONObject d;
    private final /* synthetic */ ArrayAdapter e;
    private final /* synthetic */ RatingBar f;

    ak(bv bvVar, TextView textView, ListView listView, JSONObject jSONObject, ArrayAdapter arrayAdapter, RatingBar ratingBar) {
        this.a = bvVar;
        this.b = textView;
        this.c = listView;
        this.d = jSONObject;
        this.e = arrayAdapter;
        this.f = ratingBar;
    }

    public void a(String str) {
        try {
            this.a.a = new JSONObject(bg.a(str)).getJSONObject("result");
            JSONArray optJSONArray = this.a.a.optJSONArray("comments");
            if (optJSONArray == null || optJSONArray.length() == 0) {
                this.b.setText((int) C0000R.string.no_comments);
                this.c.setVisibility(8);
            } else {
                this.b.setVisibility(8);
                this.c.setVisibility(0);
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject jSONObject = optJSONArray.getJSONObject(i);
                    if (!this.d.optBoolean(jSONObject.optString("user", "nobody"))) {
                        this.e.add(jSONObject);
                    }
                }
            }
            this.f.setRating((float) this.a.a.optInt("rating", 0));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean a() {
        return this.a.b.h;
    }

    public void b() {
        this.b.setText((int) C0000R.string.ratings_server_error);
        this.c.setVisibility(8);
    }
}
