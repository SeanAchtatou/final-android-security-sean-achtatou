package com.koushikdutta.rommanager;

import android.os.Bundle;
import android.preference.PreferenceScreen;
import java.io.File;

public class ManageBackups extends AnalyticsActivity {
    h e;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.e = h.a(this);
        File[] listFiles = new File("/sdcard/clockworkmod/backup").listFiles();
        if (listFiles != null) {
            PreferenceScreen createPreferenceScreen = getPreferenceManager().createPreferenceScreen(this);
            setPreferenceScreen(createPreferenceScreen);
            for (File file : listFiles) {
                if (file.isDirectory()) {
                    String name = file.getName();
                    cr crVar = new cr(this, this);
                    crVar.setTitle(name);
                    createPreferenceScreen.addPreference(crVar);
                }
            }
        }
    }
}
