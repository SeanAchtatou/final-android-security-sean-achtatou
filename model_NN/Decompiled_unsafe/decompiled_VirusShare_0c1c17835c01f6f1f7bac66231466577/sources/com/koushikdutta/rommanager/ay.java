package com.koushikdutta.rommanager;

import android.content.ComponentName;
import android.content.Context;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import com.a.a.b.d;
import com.a.a.b.f;

class ay implements ServiceConnection {
    private final /* synthetic */ Context a;
    private final /* synthetic */ dw b;

    ay(Context context, dw dwVar) {
        this.a = context;
        this.b = dwVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            d a2 = f.a(iBinder);
            r rVar = new r(this, this.b);
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(String.valueOf(this.a.getPackageName()) + ".PURCHASE_STATE_CHANGED");
            this.a.registerReceiver(rVar, intentFilter);
            Bundle a3 = gd.a(this.a, "RESTORE_TRANSACTIONS");
            a3.putLong("NONCE", (long) gd.l(this.a));
            a2.a(a3);
            this.a.unbindService(this);
        } catch (Exception e) {
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
    }
}
