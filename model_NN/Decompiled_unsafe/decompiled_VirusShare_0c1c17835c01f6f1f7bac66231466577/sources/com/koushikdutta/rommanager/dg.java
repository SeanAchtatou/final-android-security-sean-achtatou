package com.koushikdutta.rommanager;

import java.io.File;
import java.io.FilenameFilter;

class dg implements FilenameFilter {
    final /* synthetic */ UploadService a;

    dg(UploadService uploadService) {
        this.a = uploadService;
    }

    public boolean accept(File file, String str) {
        return str.endsWith(".dup");
    }
}
