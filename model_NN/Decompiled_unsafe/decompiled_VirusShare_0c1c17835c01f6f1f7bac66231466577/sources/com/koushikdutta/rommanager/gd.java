package com.koushikdutta.rommanager;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Log;
import com.a.a.a.a.a;
import com.google.ads.d;
import com.koushikdutta.rommanager.a.b;
import com.paypal.android.MEP.o;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.Signature;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipFile;
import org.json.JSONArray;
import org.json.JSONObject;

public class gd {
    public static int a = 1;
    public static final String b = (a == 1 ? "https://licenseservice.appspot.com/licenseservice" : "http://192.168.1.155:8080/licenseservice");
    public static Object c = new Object();
    private static final byte[] d;
    private static boolean e = false;
    private static long f = 604800000;

    static {
        byte[] bArr = new byte[162];
        bArr[0] = 48;
        bArr[1] = -127;
        bArr[2] = -97;
        bArr[3] = 48;
        bArr[4] = 13;
        bArr[5] = 6;
        bArr[6] = 9;
        bArr[7] = 42;
        bArr[8] = -122;
        bArr[9] = 72;
        bArr[10] = -122;
        bArr[11] = -9;
        bArr[12] = 13;
        bArr[13] = 1;
        bArr[14] = 1;
        bArr[15] = 1;
        bArr[16] = 5;
        bArr[18] = 3;
        bArr[19] = -127;
        bArr[20] = -115;
        bArr[22] = 48;
        bArr[23] = -127;
        bArr[24] = -119;
        bArr[25] = 2;
        bArr[26] = -127;
        bArr[27] = -127;
        bArr[29] = -86;
        bArr[30] = -14;
        bArr[31] = -74;
        bArr[32] = -71;
        bArr[33] = 118;
        bArr[34] = 20;
        bArr[35] = -123;
        bArr[36] = -83;
        bArr[37] = 6;
        bArr[38] = 1;
        bArr[39] = 70;
        bArr[40] = -38;
        bArr[41] = 55;
        bArr[42] = 107;
        bArr[43] = 103;
        bArr[44] = -6;
        bArr[45] = -50;
        bArr[46] = -17;
        bArr[47] = -41;
        bArr[48] = -127;
        bArr[49] = -67;
        bArr[50] = -33;
        bArr[51] = 75;
        bArr[52] = -87;
        bArr[53] = 43;
        bArr[54] = 14;
        bArr[55] = -34;
        bArr[56] = 45;
        bArr[57] = 115;
        bArr[58] = -19;
        bArr[59] = -15;
        bArr[60] = -59;
        bArr[61] = -112;
        bArr[62] = 17;
        bArr[63] = -28;
        bArr[64] = 99;
        bArr[65] = 122;
        bArr[66] = -109;
        bArr[67] = 17;
        bArr[68] = -78;
        bArr[69] = -63;
        bArr[70] = 100;
        bArr[71] = 9;
        bArr[72] = 39;
        bArr[73] = -72;
        bArr[74] = 14;
        bArr[75] = -107;
        bArr[76] = -18;
        bArr[77] = 65;
        bArr[78] = 84;
        bArr[79] = -36;
        bArr[80] = -3;
        bArr[81] = 47;
        bArr[82] = -122;
        bArr[83] = -1;
        bArr[84] = -114;
        bArr[85] = -76;
        bArr[86] = -94;
        bArr[87] = -107;
        bArr[88] = 83;
        bArr[89] = 77;
        bArr[90] = 41;
        bArr[91] = -52;
        bArr[92] = -76;
        bArr[93] = -11;
        bArr[94] = 93;
        bArr[95] = 126;
        bArr[96] = 126;
        bArr[97] = 105;
        bArr[98] = -21;
        bArr[99] = 32;
        bArr[100] = 17;
        bArr[101] = 95;
        bArr[102] = -35;
        bArr[103] = -62;
        bArr[104] = -19;
        bArr[105] = -76;
        bArr[106] = 34;
        bArr[107] = -25;
        bArr[108] = 81;
        bArr[109] = 96;
        bArr[110] = -58;
        bArr[111] = -77;
        bArr[112] = 51;
        bArr[113] = -94;
        bArr[114] = -50;
        bArr[115] = 5;
        bArr[116] = -61;
        bArr[117] = 91;
        bArr[118] = 103;
        bArr[119] = 9;
        bArr[120] = 122;
        bArr[121] = -79;
        bArr[122] = 76;
        bArr[123] = -96;
        bArr[124] = -1;
        bArr[125] = 47;
        bArr[126] = 99;
        bArr[127] = -14;
        bArr[128] = -20;
        bArr[129] = -42;
        bArr[130] = 77;
        bArr[131] = -97;
        bArr[132] = 78;
        bArr[133] = 66;
        bArr[134] = 23;
        bArr[135] = -89;
        bArr[136] = 57;
        bArr[137] = -49;
        bArr[138] = -86;
        bArr[139] = 4;
        bArr[140] = 19;
        bArr[141] = -8;
        bArr[142] = 43;
        bArr[143] = Byte.MIN_VALUE;
        bArr[144] = 46;
        bArr[145] = 117;
        bArr[146] = -103;
        bArr[147] = 45;
        bArr[148] = 100;
        bArr[149] = -102;
        bArr[150] = 21;
        bArr[151] = 124;
        bArr[152] = 85;
        bArr[153] = -4;
        bArr[154] = 107;
        bArr[155] = 58;
        bArr[156] = 107;
        bArr[157] = 2;
        bArr[158] = 3;
        bArr[159] = 1;
        bArr[161] = 1;
        d = bArr;
    }

    static Bundle a(Context context, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("BILLING_REQUEST", str);
        bundle.putInt("API_VERSION", 1);
        bundle.putString("PACKAGE_NAME", context.getPackageName());
        return bundle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static File a(URL url) {
        String query = url.getQuery();
        String str = "/sdcard/clockworkmod/download/" + url.getHost() + url.getPath();
        return new File(((query == null || query.equals("")) ? str : String.valueOf(str) + "/" + query).replace('?', '/').replace('=', '/').replace('&', '/'));
    }

    public static InputStream a(URLConnection uRLConnection) {
        uRLConnection.setRequestProperty("User-Agent", "ROM Manager, gzip");
        uRLConnection.setRequestProperty("Accept-Encoding", "gzip");
        String contentEncoding = uRLConnection.getContentEncoding();
        InputStream inputStream = uRLConnection.getInputStream();
        return "gzip".equalsIgnoreCase(contentEncoding) ? new GZIPInputStream(inputStream) : inputStream;
    }

    public static String a(Context context) {
        String a2 = h.a(context).a("manifest");
        try {
            if (!b(a2, h.a(context).a("manifest_signature"))) {
                throw new Exception();
            }
        } catch (Exception e2) {
            a2 = "http://gh-pages.clockworkmod.com/ROMManagerManifest/manifest";
        }
        return String.format("%s/%s.js", a2, h.a(context).a("detected_device"));
    }

    public static void a(Activity activity, int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(i);
        builder.setPositiveButton(17039370, new ba(activity));
        builder.create().show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    public static void a(Activity activity, ArrayList arrayList, boolean z) {
        h a2 = h.a(activity);
        if (!a2.b("is_clockworkmod", false)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage((int) C0000R.string.confirm_reboot_and_install);
            builder.setCancelable(true);
            builder.setPositiveButton(17039370, new au(arrayList, a2, activity));
            builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
            builder.setTitle((int) C0000R.string.restart_and_install);
            builder.create().show();
            return;
        }
        new at(activity, activity, z, arrayList).create().show();
    }

    public static void a(Context context, int i) {
        d(context, context.getString(i));
    }

    public static void a(Context context, int i, int i2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(i);
        builder.setMessage(i2);
        builder.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
        builder.create().show();
    }

    public static void a(Context context, d dVar) {
        try {
            for (String a2 : h.a(context).a("keywords").split(",")) {
                dVar.a(a2);
            }
        } catch (Exception e2) {
        }
    }

    public static void a(Context context, b bVar) {
        bVar.a("ROM Manager Version %s", c(context));
        bVar.a(DateFormat.getLongDateFormat(context).format(new Date()), new String[0]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, java.lang.String, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int, java.lang.String, com.koushikdutta.rommanager.av]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, boolean, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, java.lang.String, com.koushikdutta.rommanager.cq):void */
    public static void a(Context context, dw dwVar, String str, String str2) {
        String a2 = h.a(context).a("paypal_transaction_id");
        String format = String.format("%s?operation=licenseRequest&deviceId=%s&seller=koushik_dutta@yahoo.com&productId=ROM%%20Manager", b, f(context));
        String str3 = a2 != null ? String.valueOf(format) + "&paypalTransactionId=" + a2 : format;
        if (!(str == null || str2 == null)) {
            str3 = String.valueOf(str3) + "&buyer=" + Uri.encode(str) + "&purchaseId=" + Uri.encode(str2);
        }
        String h = h(context);
        a(str3, (Activity) null, false, h, (cq) new av(context, dwVar, h));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder, boolean):void
     arg types: [android.content.Context, java.lang.StringBuilder, int]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.app.Activity, java.util.ArrayList, boolean):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, boolean):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder, boolean):void */
    public static void a(Context context, StringBuilder sb) {
        a(context, sb, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    public static void a(Context context, StringBuilder sb, boolean z) {
        h a2 = h.a(context);
        boolean b2 = a2.b("readonly_recovery", false);
        String a3 = a2.a("reboot_recovery");
        if (b2) {
            sb.append(" cat /sdcard/clockworkmod/recovery-update.zip > /cache/update.zip ; ");
            sb.append(" mkdir -p /cache/recovery ; ");
            sb.append(" echo '--update_package=CACHE:update.zip' > /cache/recovery/command ;");
            sb.append(" sync ;");
        } else {
            sb.append(" rm /cache/recovery/command ; ");
        }
        if (z) {
            sb.append("mkdir -p /sdcard/clockworkmod ; ");
            sb.append("echo 1 > /sdcard/clockworkmod/.recoverycheckpoint ; ");
        }
        if (!b(a3)) {
            sb.append(a3);
        } else {
            sb.append(" reboot recovery ; " + context.getFilesDir() + "/reboot recovery ; ");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    public static void a(h hVar) {
        boolean z;
        JSONObject jSONObject;
        try {
            GZIPInputStream gZIPInputStream = new GZIPInputStream(new FileInputStream("/sdcard/clockworkmod/.settings2"));
            jSONObject = new JSONObject(new String(bg.b(gZIPInputStream)));
            gZIPInputStream.close();
            z = false;
        } catch (Exception e2) {
            z = true;
            jSONObject = new JSONObject();
        }
        try {
            long b2 = hVar.b("recovery_timestamp", 0L);
            Long valueOf = Long.valueOf(jSONObject.optLong("recovery_timestamp", 0));
            if (b2 < valueOf.longValue()) {
                hVar.a("recovery_timestamp", jSONObject.optLong("recovery_timestamp"));
                hVar.a("is_clockworkmod", jSONObject.optBoolean("is_clockworkmod", false));
                hVar.a("current_recovery_version", jSONObject.optString("current_recovery_version"));
                hVar.a("detected_device", jSONObject.optString("detected_device"));
                hVar.a("readonly_recovery", jSONObject.optBoolean("readonly_recovery", false));
                hVar.a("reboot_recovery", jSONObject.optString("reboot_recovery"));
                hVar.a("flash_recovery", jSONObject.optString("flash_recovery"));
                hVar.a("paypal_transaction_id", jSONObject.optString("paypal_transaction_id"));
            } else if (b2 > valueOf.longValue() || z) {
                jSONObject.put("recovery_timestamp", hVar.b("recovery_timestamp", 0L));
                jSONObject.put("is_clockworkmod", hVar.b("is_clockworkmod", false));
                jSONObject.put("current_recovery_version", hVar.a("current_recovery_version"));
                jSONObject.put("detected_device", hVar.a("detected_device"));
                jSONObject.put("readonly_recovery", hVar.b("readonly_recovery", false));
                jSONObject.put("reboot_recovery", hVar.a("reboot_recovery"));
                jSONObject.put("flash_recovery", hVar.a("flash_recovery"));
                jSONObject.put("paypal_transaction_id", hVar.a("paypal_transaction_id"));
                jSONObject.put("warning", "ROM Manager and its distribution of ClockworkMod recovery are copyright ClockworkMod, LLC. If you are trying to integrate with ClockworkMod recovery, please build and distribute your own version of ClockworkMod recovery. Unauthorized reverse engineering and usage of ClockworkMod, LLC's ROM Manager and distributed ClockworkMod Recovery are in violation of the DMCA, and will be prosecuted.");
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(new FileOutputStream("/sdcard/clockworkmod/.settings2"));
                gZIPOutputStream.write(jSONObject.toString().getBytes());
                gZIPOutputStream.close();
                bg.a("/sdcard/clockworkmod/.settings", "ROM Manager and its distribution of ClockworkMod recovery are copyright ClockworkMod, LLC. If you are trying to integrate with ClockworkMod recovery, please build and distribute your own version of ClockworkMod recovery. Unauthorized reverse engineering and usage of ClockworkMod, LLC's ROM Manager and distributed ClockworkMod Recovery are in violation of the DMCA, and will be prosecuted.");
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public static void a(String str, Activity activity, boolean z, cq cqVar) {
        a(new String[]{str}, activity, z, null, false, cqVar);
    }

    public static void a(String str, Activity activity, boolean z, String str2, cq cqVar) {
        a(new String[]{str}, activity, z, str2, false, cqVar);
    }

    public static void a(String[] strArr, Activity activity, boolean z, String str, boolean z2, cq cqVar) {
        if (a(activity)) {
            new az(strArr, new Handler(), str, z, z2, activity, cqVar).start();
        }
    }

    public static boolean a() {
        return !a(SystemProperties.get("ro.modversion")) && !a(SystemProperties.get("ro.rommanager.developerid"));
    }

    public static boolean a(Activity activity) {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            return true;
        }
        if (activity != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setCancelable(false);
            builder.setTitle((int) C0000R.string.sdcard_not_mounted);
            builder.setMessage((int) C0000R.string.sdcard_needs_to_be_mounted);
            builder.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
            builder.create().show();
        }
        return false;
    }

    private static boolean a(Context context, int i, boolean z) {
        int shortValue = (new BigInteger(f(context).getBytes()).shortValue() << 16) & -65536;
        int i2 = i & -65536;
        if (!z || ((((long) (65535 & i)) << 16) * 1000) + f >= System.currentTimeMillis()) {
            return i2 == shortValue;
        }
        return false;
    }

    static boolean a(Context context, dw dwVar) {
        return true;
    }

    private static boolean a(Context context, File file) {
        try {
            e = a(file.getAbsolutePath(), context);
            if (e) {
                return true;
            }
        } catch (Exception e2) {
        }
        file.delete();
        return false;
    }

    public static boolean a(File file) {
        File[] listFiles;
        if (file.exists() && (listFiles = file.listFiles()) != null) {
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].isDirectory()) {
                    a(listFiles[i]);
                } else {
                    listFiles[i].delete();
                }
            }
        }
        return file.delete();
    }

    public static boolean a(String str) {
        if (str == null) {
            return true;
        }
        return "".equals(str);
    }

    public static boolean a(String str, Context context) {
        JSONObject jSONObject = new JSONObject(bg.a(str)).getJSONObject("license");
        String string = jSONObject.getString("rights");
        JSONObject jSONObject2 = new JSONObject(string);
        String string2 = jSONObject2.getString("device_id");
        if (!b(string, jSONObject.getString("signature"))) {
            return false;
        }
        if (!f(context).equals(string2) || !"koushik_dutta@yahoo.com".equals(jSONObject2.getString("seller"))) {
            return false;
        }
        if (jSONObject2.optLong("expiration_date", System.currentTimeMillis() + 1000) < System.currentTimeMillis()) {
            return false;
        }
        e = true;
        return true;
    }

    static boolean a(String str, String str2) {
        try {
            Signature instance = Signature.getInstance("SHA1withRSA");
            instance.initVerify(KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(a.a("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtBKmQb/Czwai4WdFJ+ZHWbYXF2pxAPcRXy9cs2QHsd4pZY7AZjimETh+BKfLH77+sgwJdc5eRYjv9w3Dd3ofwLXmPcJHURfiWZbTN/WaQXH6ypQVgm4WZcAwmQcpg1+sWdeNjTLJp6V9VHqos9NTbmwbSYS6gJJSSyVgPMKCRu3j1v/57Qk3C0Wuxr8mRKdgqLEEN/tPZ4fLsfiY3o6Ie6TbdTvhR7uItNxxAXSWkzYorM2gVSKA78yIgmZ5KamgxEUfl/7psyHtSqQY9ukB5U1ERNbrLVWszPL7AWLaA8iy92O/YU9LmK5FSjnb/Qzfd7+41/ngD+znNtxg0aZaOwIDAQAB"))));
            instance.update(str.getBytes());
            return instance.verify(a.a(str2));
        } catch (Exception e2) {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long */
    public static long b(Context context) {
        long j;
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 1254, new Intent("com.koushikdutta.rommanager.backup"), 268435456);
        h a2 = h.a(context);
        int b2 = a2.b("backup_frequency", 0);
        long b3 = a2.b("next_backup", 0L);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        if (b2 == 0 || b3 == 0) {
            alarmManager.cancel(broadcast);
            return 0;
        }
        if (b3 > ((long) (b2 * 24 * 60 * 60 * 1000)) + System.currentTimeMillis() || b3 < System.currentTimeMillis()) {
            Date date = new Date(b3);
            Date date2 = new Date();
            date2.setHours(date.getHours());
            date2.setMinutes(date.getMinutes());
            date2.setSeconds(0);
            j = date2.getTime();
            if (j < System.currentTimeMillis()) {
                j += 86400000;
            }
        } else {
            j = b3;
        }
        alarmManager.set(0, j, broadcast);
        return j;
    }

    public static Process b(Context context, String str) {
        DataOutputStream dataOutputStream = new DataOutputStream(context.openFileOutput("rommanager.sh", 0));
        dataOutputStream.writeBytes(str);
        dataOutputStream.close();
        return Runtime.getRuntime().exec(new String[]{"su", "-c", ". " + context.getFilesDir().getAbsolutePath() + "/" + "rommanager.sh"});
    }

    static void b() {
        e = true;
    }

    public static void b(Context context, dw dwVar) {
        a(context, dwVar, (String) null, (String) null);
    }

    /* access modifiers changed from: private */
    public static void b(Context context, dw dwVar, boolean z, String str, String str2) {
        if (z) {
            if (!(str == null || str2 == null)) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject.put("license", jSONObject2);
                    jSONObject2.put("license_response", str);
                    jSONObject2.put("license_signature", str2);
                    bg.a("/sdcard/clockworkmod/.rommanager_market_license", jSONObject.toString());
                } catch (Exception e2) {
                }
            }
            if (!e) {
                e = true;
                if (dwVar != null) {
                    dwVar.a();
                }
            }
        } else if (e) {
            e = false;
            if (dwVar != null) {
                dwVar.a();
                dwVar.b();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int, boolean):boolean
     arg types: [android.content.Context, int, int]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.app.Activity, java.util.ArrayList, boolean):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder, boolean):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, boolean):boolean */
    private static boolean b(Context context, int i) {
        return a(context, i, false);
    }

    public static boolean b(String str) {
        return str == null || str.equals("") || str.equals("null");
    }

    public static boolean b(String str, String str2) {
        Signature instance = Signature.getInstance("SHA1withRSA");
        instance.initVerify((RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(d)));
        instance.update(str.getBytes());
        try {
            if (instance.verify(new BigInteger(str2, 16).toByteArray())) {
                return true;
            }
        } catch (Exception e2) {
        }
        return instance.verify(a.a(str2));
    }

    public static int c(Context context, String str) {
        Process b2 = b(context, str);
        do {
        } while (new DataInputStream(b2.getInputStream()).readLine() != null);
        return b2.waitFor();
    }

    public static String c(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            return "Unknown";
        }
    }

    /* access modifiers changed from: private */
    public static void c(Context context, dw dwVar, String str, String str2) {
        try {
            JSONObject jSONObject = new JSONObject();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject.put("license", jSONObject2);
            jSONObject2.put("license_response", str);
            jSONObject2.put("license_signature", str2);
            bg.a("/sdcard/clockworkmod/.rommanager_inapp_license", jSONObject.toString());
            if (k(context)) {
                e = true;
                dwVar.a();
            }
        } catch (Exception e2) {
        }
    }

    static boolean c() {
        return e;
    }

    private static boolean c(Context context, dw dwVar) {
        if (k(context)) {
            return true;
        }
        context.bindService(new Intent("com.android.vending.billing.MarketBillingService.BIND"), new ay(context, dwVar), 1);
        return false;
    }

    public static boolean c(String str) {
        try {
            ZipFile zipFile = new ZipFile(str);
            return (zipFile.getEntry("META-INF/com/google/android/update-script") == null && zipFile.getEntry("META-INF/com/google/android/updater-script") == null) ? false : true;
        } catch (Exception e2) {
            return false;
        }
    }

    public static String d(String str) {
        try {
            return new BigInteger(1, MessageDigest.getInstance("MD5").digest(str.getBytes())).toString(16).toUpperCase();
        } catch (Exception e2) {
            return null;
        }
    }

    public static void d(Context context, String str) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(str);
            builder.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
            builder.create().show();
        } catch (Exception e2) {
        }
    }

    public static boolean d(Context context) {
        String str = SystemProperties.get("ro.modversion");
        return (str != null && str.contains("CyanogenMod")) || a(context, null);
    }

    private static boolean d(Context context, dw dwVar) {
        Context applicationContext = context.getApplicationContext();
        String packageName = applicationContext.getPackageName();
        String str = String.valueOf(packageName) + ".license";
        try {
            if (applicationContext.getPackageManager().checkSignatures(packageName, str) < 0) {
                return false;
            }
            if (!g(applicationContext, packageName)) {
                return false;
            }
            if (!g(applicationContext, str)) {
                return false;
            }
            if (f(applicationContext, str)) {
                return true;
            }
            try {
                applicationContext.bindService(new Intent("com.koushikdutta.rommanager.LicenseCheckerService"), new ax(applicationContext, dwVar), 1);
            } catch (Exception e2) {
                b(applicationContext, dwVar, false, null, null);
            }
            return false;
        } catch (Exception e3) {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static void e(Context context, String str) {
        try {
            String replace = str.replace(' ', '_');
            StringBuilder sb = new StringBuilder();
            b a2 = b.a(context);
            a(context, a2);
            a2.a(String.format("%s/%s", "/sdcard/clockworkmod/backup", replace));
            a2.a(context, sb);
            a(context, sb);
            c(context, sb.toString());
        } catch (Exception e2) {
            a(context, (int) C0000R.string.script_error);
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    public static boolean e(Context context) {
        if (!d(context)) {
            return true;
        }
        return h.a(context).b("showads", false);
    }

    public static String f(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            deviceId = "000000000000";
        }
        return d(deviceId);
    }

    private static boolean f(Context context, String str) {
        try {
            JSONObject jSONObject = new JSONObject(bg.a("/sdcard/clockworkmod/.rommanager_market_license")).getJSONObject("license");
            String string = jSONObject.getString("license_response");
            String string2 = jSONObject.getString("license_signature");
            if (!(string == null || string2 == null)) {
                com.a.a.a.a a2 = com.a.a.a.a.a(string, string2);
                if (a(string, string2) && str.equals(a2.c) && a2.a == 0 && b(context, a2.b) && a2.f + f > System.currentTimeMillis()) {
                    return true;
                }
            }
            new File("/sdcard/clockworkmod/.rommanager_market_license").delete();
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public static void g(Context context) {
        if (h.a(context).a("registration_id") == null) {
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.putExtra("app", PendingIntent.getBroadcast(context, 0, new Intent(), 0));
            intent.putExtra("sender", "koushd@gmail.com");
            context.startService(intent);
        }
    }

    private static boolean g(Context context, String str) {
        try {
            return context.getPackageManager().getPackageInfo(str, 64).signatures[0].toCharsString().equals("3082025f308201c8a0030201020204491395d0300d06092a864886f70d01010505003074310b3009060355040613025553310b30090603550408130257413110300e0603550407130753656174746c6531193017060355040a13106b6f757368696b64757474612e636f6d31133011060355040b130a436c6f636b776f726b73311630140603550403130d4b6f757368696b204475747461301e170d3038313130373031313134345a170d3336303332353031313134345a3074310b3009060355040613025553310b30090603550408130257413110300e0603550407130753656174746c6531193017060355040a13106b6f757368696b64757474612e636f6d31133011060355040b130a436c6f636b776f726b73311630140603550403130d4b6f757368696b20447574746130819f300d06092a864886f70d010101050003818d0030818902818100aaf2b6b9761485ad060146da376b67faceefd781bddf4ba92b0ede2d73edf1c59011e4637a9311b2c1640927b80e95ee4154dcfd2f86ff8eb4a295534d29ccb4f55d7e7e69eb20115fddc2edb422e75160c6b333a2ce05c35b67097ab14ca0ff2f63f2ecd64d9f4e4217a739cfaa0413f82b802e75992d649a157c55fc6b3a6b0203010001300d06092a864886f70d010105050003818100aaef343a4c39cabb750c1fc3c7121ac35be1023d6f80a71a48760c5345c1d9e0c8d2c20dffe1b3ed7089717964e1c6fcc122ef444ecf96b3cc448a43733807b48e409a9ca792458704d44984aab356833cfff51bab702bc371c14bf5cd43003cde3a91708364fc397535558c0009b7f033091119440129e64a31a8f15997d52d");
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        }
    }

    public static String h(Context context) {
        return context.getFileStreamPath(".rommanager_license").getAbsolutePath();
    }

    public static void i(Context context) {
        synchronized (c) {
            if (o.a() == null) {
                o.a(context, a == 1 ? "APP-7H157964T5671530X" : "APP-80W284485P519543T", a);
                o a2 = o.a();
                a2.a("en_US");
                a2.b(false);
                a2.a(0);
                a2.c(false);
                Log.i("RomNexus", "Done initializing PayPal.");
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int, boolean):boolean
     arg types: [android.content.Context, int, int]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.app.Activity, java.util.ArrayList, boolean):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder, boolean):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, boolean):boolean */
    private static boolean k(Context context) {
        try {
            JSONObject jSONObject = new JSONObject(bg.a("/sdcard/clockworkmod/.rommanager_inapp_license")).getJSONObject("license");
            String string = jSONObject.getString("license_response");
            JSONObject jSONObject2 = new JSONObject(string);
            if (a(string, jSONObject.getString("license_signature")) && a(context, jSONObject2.getInt("nonce"), true)) {
                JSONArray jSONArray = jSONObject2.getJSONArray("orders");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject3 = jSONArray.getJSONObject(i);
                    if (context.getPackageName().equals(jSONObject3.optString("packageName")) && com.a.a.b.b.a(jSONObject3.optInt("purchaseState", -1)) == com.a.a.b.b.PURCHASED && "donation".equals(jSONObject3.optString("productId"))) {
                        return true;
                    }
                }
            }
            new File("/sdcard/clockworkmod/.rommanager_inapp_license").delete();
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public static int l(Context context) {
        return ((int) ((System.currentTimeMillis() / 1000) >>> 16)) | ((new BigInteger(f(context).getBytes()).shortValue() << 16) & -65536);
    }
}
