package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.widget.EditText;

class be implements DialogInterface.OnClickListener {
    final /* synthetic */ cm a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ Runnable c;

    be(cm cmVar, EditText editText, Runnable runnable) {
        this.a = cmVar;
        this.b = editText;
        this.c = runnable;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.a.a = this.b.getText().toString();
        this.c.run();
    }
}
