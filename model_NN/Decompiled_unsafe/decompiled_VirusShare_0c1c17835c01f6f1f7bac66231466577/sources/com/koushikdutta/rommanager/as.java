package com.koushikdutta.rommanager;

import android.os.Parcel;
import android.os.Parcelable;

class as implements Parcelable.Creator {
    as() {
    }

    /* renamed from: a */
    public RomPart createFromParcel(Parcel parcel) {
        return new RomPart(parcel, null);
    }

    /* renamed from: a */
    public RomPart[] newArray(int i) {
        return new RomPart[i];
    }
}
