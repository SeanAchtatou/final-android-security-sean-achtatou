package com.koushikdutta.rommanager;

import android.preference.CheckBoxPreference;
import android.preference.Preference;

class ab implements Preference.OnPreferenceClickListener {
    final /* synthetic */ SettingsScreen a;
    private final /* synthetic */ CheckBoxPreference b;

    ab(SettingsScreen settingsScreen, CheckBoxPreference checkBoxPreference) {
        this.a = settingsScreen;
        this.b = checkBoxPreference;
    }

    public boolean onPreferenceClick(Preference preference) {
        this.a.c.a("showads", this.b.isChecked());
        return true;
    }
}
