package com.koushikdutta.rommanager;

import android.content.Intent;
import android.net.Uri;
import android.view.MenuItem;

class dt implements MenuItem.OnMenuItemClickListener {
    final /* synthetic */ RomList a;

    dt(RomList romList) {
        this.a = romList;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(Uri.parse(this.a.n));
        this.a.startActivity(intent);
        return true;
    }
}
