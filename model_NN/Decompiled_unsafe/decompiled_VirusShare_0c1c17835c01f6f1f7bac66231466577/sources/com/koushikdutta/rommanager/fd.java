package com.koushikdutta.rommanager;

class fd implements Runnable {
    final /* synthetic */ RomManager a;
    private final /* synthetic */ boolean b;

    fd(RomManager romManager, boolean z) {
        this.a = romManager;
        this.b = z;
    }

    public void run() {
        try {
            String str = this.a.getPackageManager().getPackageInfo(this.a.getPackageName(), 0).versionName;
            ck b2 = this.a.b((int) C0000R.string.download_rom);
            if (this.b) {
                this.a.setTitle(String.valueOf(this.a.getString(C0000R.string.app_name_premium)) + " v" + str);
                b2.a((String) null);
                return;
            }
            this.a.setTitle(String.valueOf(this.a.getString(C0000R.string.app_name)) + " v" + str);
            b2.a((int) C0000R.string.download_rom_premium);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
