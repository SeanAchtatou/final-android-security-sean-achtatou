package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.widget.EditText;

class eb implements DialogInterface.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ fj a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ EditText c;

    eb(fj fjVar, EditText editText, EditText editText2) {
        this.a = fjVar;
        this.b = editText;
        this.c = editText2;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        gd.a(this.a.a.a, new f(this), this.b.getText().toString(), this.c.getText().toString());
    }
}
