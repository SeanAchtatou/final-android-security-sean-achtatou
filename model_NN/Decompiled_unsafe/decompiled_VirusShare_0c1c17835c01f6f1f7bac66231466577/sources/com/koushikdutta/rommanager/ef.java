package com.koushikdutta.rommanager;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

class ef extends Thread {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ DownloadService a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;
    private final /* synthetic */ Intent d;

    ef(DownloadService downloadService, String str, String str2, Intent intent) {
        this.a = downloadService;
        this.b = str;
        this.c = str2;
        this.d = intent;
    }

    public void run() {
        try {
            JSONArray jSONArray = new JSONObject(bg.b(gd.a(this.a))).getJSONArray("manifests");
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    String string = jSONObject.getString("manifest");
                    String string2 = jSONObject.getString("id");
                    String string3 = jSONObject.getString("developer");
                    if (string2.equals(this.b)) {
                        JSONObject a2 = this.a.b(jSONObject, this.c);
                        if (a2 == null) {
                            if (this.d.getBooleanExtra("interactive", false)) {
                                this.a.e.post(new ec(this));
                            }
                            this.a.a();
                            return;
                        }
                        String string4 = a2.getString("name");
                        Intent intent = new Intent(this.a, RomList.class);
                        intent.putExtra("manifest_url", string);
                        intent.putExtra("manifest", jSONObject.toString());
                        intent.putExtra("rom", a2.toString());
                        intent.putExtra("name", string3);
                        intent.putExtra("id", string2);
                        intent.setFlags(335544320);
                        if (this.d.getBooleanExtra("interactive", false)) {
                            this.a.e.post(new ed(this, intent));
                        }
                        PendingIntent activity = PendingIntent.getActivity(this.a, 2323, intent, 268435456);
                        Notification notification = new Notification(C0000R.drawable.progress, string4, 0);
                        String string5 = this.a.getString(C0000R.string.update_available);
                        notification.setLatestEventInfo(this.a, string5, string4, activity);
                        notification.tickerText = string5;
                        this.a.a.notify(2323, notification);
                        return;
                    }
                    continue;
                    i++;
                } catch (Exception e) {
                    Log.e("DownloadService", e.getLocalizedMessage(), e);
                }
            }
            this.a.a();
        } catch (Exception e2) {
            Log.e("DownloadService", e2.getLocalizedMessage(), e2);
        } finally {
            this.a.a();
        }
    }
}
