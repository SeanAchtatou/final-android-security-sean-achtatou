package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

class fh implements DialogInterface.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager a;
    private final /* synthetic */ String[] b;
    private final /* synthetic */ String c;

    fh(RomManager romManager, String[] strArr, String str) {
        this.a = romManager;
        this.b = strArr;
        this.c = str;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        String str = this.b[i];
        Log.i("RomNexus", str);
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        View inflate = View.inflate(this.a, C0000R.layout.nickname, null);
        EditText editText = (EditText) inflate.findViewById(C0000R.id.nickname);
        editText.setText(this.a.c.a("nickname"));
        editText.selectAll();
        builder.setIcon(0);
        builder.setTitle((int) C0000R.string.nickname);
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new ae(this, editText, str, this.c));
        builder.setView(inflate);
        builder.create().show();
    }
}
