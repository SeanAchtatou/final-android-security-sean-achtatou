package com.koushikdutta.rommanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ck {
    public String c;
    public String d;
    public ActivityBase e;
    public boolean f;
    public int g;

    public ck(ActivityBase activityBase, int i, int i2) {
        this.f = true;
        if (i != 0) {
            this.c = activityBase.getString(i);
        }
        if (i2 != 0) {
            this.d = activityBase.getString(i2);
        }
        this.e = activityBase;
    }

    public ck(ActivityBase activityBase, int i, int i2, int i3) {
        this(activityBase, i, i2);
        this.g = i3;
    }

    public ck(ActivityBase activityBase, String str, String str2) {
        this.f = true;
        this.c = str;
        this.d = str2;
        this.e = activityBase;
    }

    public ck(ActivityBase activityBase, String str, String str2, int i) {
        this(activityBase, str, str2);
        this.g = i;
    }

    public View a(Context context, View view) {
        View inflate = view == null ? LayoutInflater.from(context).inflate(this.e.c(), (ViewGroup) null) : view;
        TextView textView = (TextView) inflate.findViewById(C0000R.id.title);
        TextView textView2 = (TextView) inflate.findViewById(C0000R.id.summary);
        textView.setEnabled(this.f);
        textView2.setEnabled(this.f);
        textView.setText(this.c);
        if (this.d != null) {
            textView2.setVisibility(0);
            textView2.setText(this.d);
        } else {
            textView2.setVisibility(8);
        }
        ImageView imageView = (ImageView) inflate.findViewById(C0000R.id.image);
        if (imageView != null) {
            if (this.g != 0) {
                imageView.setVisibility(0);
                imageView.setImageResource(this.g);
            } else {
                imageView.setVisibility(8);
            }
        }
        return inflate;
    }

    public void a() {
    }

    public void a(int i) {
        if (i == 0) {
            a((String) null);
        } else {
            a(this.e.getString(i));
        }
    }

    public void a(String str) {
        this.d = str;
        this.e.f.notifyDataSetChanged();
    }

    public void a(boolean z) {
        this.f = z;
        this.e.f.notifyDataSetChanged();
    }

    public boolean b() {
        return false;
    }
}
