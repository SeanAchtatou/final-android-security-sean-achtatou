package com.koushikdutta.rommanager;

import android.net.Uri;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;

class dh extends Thread {
    final /* synthetic */ UploadService a;
    private final /* synthetic */ File[] b;
    private final /* synthetic */ File c;

    dh(UploadService uploadService, File[] fileArr, File file) {
        this.a = uploadService;
        this.b = fileArr;
        this.c = file;
    }

    public void run() {
        try {
            h a2 = h.a(this.a);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(String.format("%s/backup/%s/diff", "http://rombackup.deployfu.com", Uri.encode(a2.a("account")))).openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("registration_id", a2.a("registration_id"));
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            for (File file : this.b) {
                dataOutputStream.writeBytes(String.format("manifest[%s]", file.getName()));
                dataOutputStream.writeChar(61);
                byte[] bArr = new byte[9168];
                FileInputStream fileInputStream = new FileInputStream(file);
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    dataOutputStream.writeBytes(Uri.encode(new String(bArr, 0, read)));
                }
                fileInputStream.close();
                if (file != this.b[this.b.length - 1]) {
                    dataOutputStream.writeChar(38);
                }
            }
            dataOutputStream.close();
            JSONArray jSONArray = new JSONArray(bg.a(httpURLConnection.getInputStream()));
            for (int i = 0; i < jSONArray.length(); i++) {
                String string = jSONArray.getString(i);
                File file2 = new File(String.format("%s/%s", "/sdcard/clockworkmod/blobs", string));
                File file3 = new File(String.format("%s/%s", "/sdcard/clockworkmod/cloud", string));
                try {
                    file2.renameTo(file3);
                } catch (Exception e) {
                }
                if (!file3.exists()) {
                    throw new Exception("File " + file3.toString() + " does not exist.");
                }
            }
            File file4 = new File("/sdcard/clockworkmod/tar");
            gd.a(file4);
            file4.mkdirs();
            gd.c(this.a, String.format("cd %s ; %s tar czvf %s . ; ", "/sdcard/clockworkmod/cloud", String.format("%s/busybox", this.a.getFilesDir().getAbsolutePath()), this.c.getAbsolutePath()));
            if (!this.c.exists()) {
                throw new Exception("Error compressing upload.tgz.");
            }
            this.a.a();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
