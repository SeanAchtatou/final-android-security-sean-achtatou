package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import org.json.JSONObject;

/* renamed from: com.koushikdutta.rommanager.do  reason: invalid class name */
class Cdo implements DialogInterface.OnClickListener {
    final /* synthetic */ RomList a;
    private final /* synthetic */ JSONObject b;
    private final /* synthetic */ JSONObject c;

    Cdo(RomList romList, JSONObject jSONObject, JSONObject jSONObject2) {
        this.a = romList;
        this.b = jSONObject;
        this.c = jSONObject2;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a(this.b, this.c);
    }
}
