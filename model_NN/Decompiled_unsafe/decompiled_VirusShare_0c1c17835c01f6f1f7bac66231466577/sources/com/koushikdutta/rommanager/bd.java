package com.koushikdutta.rommanager;

import android.content.Context;
import android.os.PowerManager;

class bd {
    private static PowerManager.WakeLock a;

    bd() {
    }

    static void a() {
        if (a != null) {
            a.release();
            a = null;
        }
    }

    static void a(Context context) {
        a();
        a = ((PowerManager) context.getSystemService("power")).newWakeLock(805306369, "RomManager");
        a.acquire();
    }
}
