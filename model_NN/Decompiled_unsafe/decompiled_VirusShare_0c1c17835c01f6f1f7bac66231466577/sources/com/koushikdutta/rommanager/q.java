package com.koushikdutta.rommanager;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import com.koushikdutta.rommanager.a.b;
import java.io.File;

class q implements DialogInterface.OnClickListener {
    final /* synthetic */ ek a;
    private final /* synthetic */ EditText b;

    q(ek ekVar, EditText editText) {
        this.a = ekVar;
        this.b = editText;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.CloudBackups, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.a("Cloud", "Backup", "Backup");
        if (new File("/sdcard/clockworkmod/backup", this.b.getText().toString().replace(' ', '_')).exists()) {
            gd.a((Context) this.a.a, (int) C0000R.string.backup_error);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(". %s/%s ; ", this.a.a.getFilesDir().getAbsolutePath(), "preparecloudbackup.sh"));
        b a2 = b.a(this.a.a);
        gd.a(this.a.a, a2);
        String format = String.format("%s/%s", "/sdcard/clockworkmod/backup", this.b.getText().toString().replace(' ', '_'));
        a2.a(format);
        a2.a("Packing the backup for upload...", new String[0]);
        a2.b("/cache/docloudbackup.sh", format);
        a2.a(this.a.a, sb);
        sb.append("sync ; ");
        gd.a(this.a.a, sb);
        try {
            gd.c(this.a.a, sb.toString());
        } catch (Exception e) {
            gd.a((Context) this.a.a, (int) C0000R.string.script_error);
            e.printStackTrace();
        }
    }
}
