package com.koushikdutta.rommanager;

import android.view.MenuItem;

class cg implements MenuItem.OnMenuItemClickListener {
    final /* synthetic */ DeveloperList a;

    cg(DeveloperList developerList) {
        this.a = developerList;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        h.a(this.a).a("developer_sort", "downloads");
        this.a.d();
        this.a.f();
        this.a.f.notifyDataSetChanged();
        return true;
    }
}
