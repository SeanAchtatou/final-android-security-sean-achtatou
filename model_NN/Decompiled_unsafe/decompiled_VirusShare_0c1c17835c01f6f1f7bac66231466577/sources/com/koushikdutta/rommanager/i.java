package com.koushikdutta.rommanager;

import android.content.Context;

class i extends j {
    final /* synthetic */ ActivityBase a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(ActivityBase activityBase, Context context) {
        super(context);
        this.a = activityBase;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public boolean isEnabled(int i) {
        if (!super.isEnabled(i)) {
            return false;
        }
        return ((ck) getItem(i)).f;
    }
}
