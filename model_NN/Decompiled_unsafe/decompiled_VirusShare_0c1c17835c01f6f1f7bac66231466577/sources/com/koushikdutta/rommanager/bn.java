package com.koushikdutta.rommanager;

import java.util.Comparator;

class bn implements Comparator {
    final /* synthetic */ DeveloperList a;

    bn(DeveloperList developerList) {
        this.a = developerList;
    }

    /* renamed from: a */
    public int compare(ck ckVar, ck ckVar2) {
        if (((dk) ckVar).j == ((dk) ckVar2).j) {
            return 0;
        }
        return ((dk) ckVar).j > ((dk) ckVar2).j ? -1 : 1;
    }
}
