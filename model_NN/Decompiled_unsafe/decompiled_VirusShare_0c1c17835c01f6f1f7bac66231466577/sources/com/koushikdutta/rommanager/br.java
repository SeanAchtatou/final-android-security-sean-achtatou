package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import java.util.Date;

class br implements DialogInterface.OnClickListener {
    final /* synthetic */ gr a;
    private final /* synthetic */ int b;
    private final /* synthetic */ int c;

    br(gr grVar, int i, int i2) {
        this.a = grVar;
        this.b = i;
        this.c = i2;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.b.c.a("backup_frequency", this.a.a.a);
        Date date = new Date();
        date.setHours(this.b);
        date.setMinutes(this.c);
        date.setSeconds(0);
        long time = date.getTime();
        if (time < System.currentTimeMillis()) {
            time += 86400000;
        }
        this.a.a.b.c.a("next_backup", time);
        this.a.a.b.c();
    }
}
