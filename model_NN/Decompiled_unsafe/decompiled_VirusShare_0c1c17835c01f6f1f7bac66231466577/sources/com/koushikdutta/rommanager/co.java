package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;

class co implements DialogInterface.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ fo a;

    co(fo foVar) {
        this.a = foVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        String replace = this.a.h.getResources().getStringArray(C0000R.array.ext_size_options)[this.a.a].replace("MB", "M");
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.h);
        builder.setCancelable(true);
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.setTitle((int) C0000R.string.partition_swap_size);
        builder.setSingleChoiceItems((int) C0000R.array.swap_size_options, this.a.b, new cw(this));
        builder.setPositiveButton(17039370, new cv(this, replace));
        builder.create().show();
    }
}
