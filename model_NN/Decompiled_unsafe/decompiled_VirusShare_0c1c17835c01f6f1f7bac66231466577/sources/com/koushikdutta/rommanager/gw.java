package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

class gw implements cq {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager a;
    private final /* synthetic */ boolean b;

    gw(RomManager romManager, boolean z) {
        this.a = romManager;
        this.b = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    public void a(String str) {
        ArrayList arrayList;
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            String a2 = bg.a(fileInputStream);
            fileInputStream.close();
            JSONObject jSONObject = new JSONObject(a2);
            String string = jSONObject.getString("version");
            this.a.k = jSONObject.getString("recovery_url");
            this.a.l = jSONObject.getString("recovery_zip_url");
            JSONArray jSONArray = jSONObject.getJSONArray("devices");
            HashMap hashMap = new HashMap();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= jSONArray.length()) {
                    break;
                }
                try {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    String string2 = jSONObject2.getString("init");
                    ArrayList arrayList2 = (ArrayList) hashMap.get(string2);
                    if (arrayList2 == null) {
                        ArrayList arrayList3 = new ArrayList();
                        hashMap.put(string2, arrayList3);
                        arrayList = arrayList3;
                    } else {
                        arrayList = arrayList2;
                    }
                    String optString = jSONObject2.optString("version");
                    arrayList.add(new dx(jSONObject2.getString("key"), jSONObject2.getString("name"), (optString == null || optString.equals("") || optString.compareTo(string) < 0) ? string : optString, jSONObject2.optBoolean("readonly_recovery", false), jSONObject2.optString("reboot_recovery"), jSONObject2.optString("flash_recovery"), jSONObject2.optBoolean("officially_supported", true)));
                } catch (Exception e) {
                }
                i = i2 + 1;
            }
            ArrayList arrayList4 = new ArrayList();
            for (String str2 : hashMap.keySet()) {
                if (new File(str2).exists()) {
                    Iterator it = ((ArrayList) hashMap.get(str2)).iterator();
                    while (it.hasNext()) {
                        arrayList4.add((dx) it.next());
                    }
                }
            }
            if (this.a.c.b("superuser", false)) {
                arrayList4.clear();
            }
            if (arrayList4.size() == 0) {
                for (ArrayList it2 : hashMap.values()) {
                    Iterator it3 = it2.iterator();
                    while (it3.hasNext()) {
                        arrayList4.add((dx) it3.next());
                    }
                }
            }
            String[] strArr = new String[arrayList4.size()];
            for (int i3 = 0; i3 < arrayList4.size(); i3++) {
                strArr[i3] = ((dx) arrayList4.get(i3)).a;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
            builder.setTitle((int) C0000R.string.confirm_device);
            builder.setItems(strArr, new gv(this, arrayList4, this.b));
            builder.create().show();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean a() {
        return this.a.i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void b() {
        gd.a((Context) this.a, (int) C0000R.string.no_connection);
    }
}
