package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.preference.CheckBoxPreference;

class cy implements DialogInterface.OnClickListener {
    final /* synthetic */ u a;
    private final /* synthetic */ CheckBoxPreference b;

    cy(u uVar, CheckBoxPreference checkBoxPreference) {
        this.a = uVar;
        this.b = checkBoxPreference;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.a(java.lang.String, int):void
      com.koushikdutta.rommanager.h.a(java.lang.String, long):void
      com.koushikdutta.rommanager.h.a(java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.a.c.a("analytics", true);
        this.b.setSummary((int) C0000R.string.analytics_on);
    }
}
