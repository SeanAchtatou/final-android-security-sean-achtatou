package com.koushikdutta.rommanager;

import java.util.Comparator;

class ca implements Comparator {
    final /* synthetic */ DeveloperList a;

    ca(DeveloperList developerList) {
        this.a = developerList;
    }

    /* renamed from: a */
    public int compare(ck ckVar, ck ckVar2) {
        if (((dk) ckVar).k == ((dk) ckVar2).k) {
            return 0;
        }
        return ((dk) ckVar).k > ((dk) ckVar2).k ? -1 : 1;
    }
}
