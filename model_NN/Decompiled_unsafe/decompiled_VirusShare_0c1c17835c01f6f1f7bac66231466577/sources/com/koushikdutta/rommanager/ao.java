package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.widget.ArrayAdapter;
import org.json.JSONObject;

class ao implements DialogInterface.OnClickListener {
    final /* synthetic */ ah a;
    private final /* synthetic */ ArrayAdapter b;
    private final /* synthetic */ int c;
    private final /* synthetic */ JSONObject d;

    ao(ah ahVar, ArrayAdapter arrayAdapter, int i, JSONObject jSONObject) {
        this.a = ahVar;
        this.b = arrayAdapter;
        this.c = i;
        this.d = jSONObject;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public void onClick(DialogInterface dialogInterface, int i) {
        JSONObject jSONObject = (JSONObject) this.b.getItem(this.c);
        try {
            this.d.put(jSONObject.getString("user"), true);
            h.a(this.a.a.b).a("blocked_users", this.d.toString());
            this.b.remove(jSONObject);
            this.b.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
