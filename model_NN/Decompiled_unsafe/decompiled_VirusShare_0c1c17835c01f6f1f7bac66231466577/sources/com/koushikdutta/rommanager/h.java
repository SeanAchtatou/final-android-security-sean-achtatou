package com.koushikdutta.rommanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class h {
    private static h c;
    SQLiteDatabase a;
    Context b;

    private h(Context context) {
        this.b = context;
        cs csVar = new cs(this, this.b, "settings.db", null, 1);
        try {
            this.a = csVar.getWritableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                gd.c(context, "rm -f " + context.getDatabasePath("settings.db").getAbsolutePath());
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            this.a = csVar.getWritableDatabase();
        }
    }

    public static h a(Context context) {
        if (c == null) {
            c = new h(context.getApplicationContext());
        }
        return c;
    }

    public String a(String str) {
        return b(str, (String) null);
    }

    public void a(String str, int i) {
        a(str, Integer.valueOf(i).toString());
    }

    public void a(String str, long j) {
        a(str, Long.valueOf(j).toString());
    }

    public void a(String str, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("key", str);
        contentValues.put("value", str2);
        this.a.delete("settings", "key='" + str + "'", null);
        this.a.insert("settings", null, contentValues);
    }

    public void a(String str, boolean z) {
        a(str, Boolean.valueOf(z).toString());
    }

    public int b(String str, int i) {
        try {
            return Integer.parseInt(b(str, (String) null));
        } catch (Exception e) {
            return i;
        }
    }

    public long b(String str, long j) {
        try {
            return Long.parseLong(b(str, (String) null));
        } catch (Exception e) {
            return j;
        }
    }

    /* JADX INFO: finally extract failed */
    public String b(String str, String str2) {
        Cursor query = this.a.query("settings", new String[]{"value"}, "key='" + str + "'", null, null, null, null);
        try {
            if (query.moveToNext()) {
                String string = query.getString(0);
                query.close();
                return string;
            }
            query.close();
            return str2;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    public boolean b(String str, boolean z) {
        try {
            return Boolean.parseBoolean(b(str, Boolean.valueOf(z).toString()));
        } catch (Exception e) {
            e.printStackTrace();
            return z;
        }
    }
}
