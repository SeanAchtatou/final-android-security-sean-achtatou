package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.preference.CheckBoxPreference;

class gq implements DialogInterface.OnDismissListener {
    final /* synthetic */ ac a;
    private final /* synthetic */ CheckBoxPreference b;

    gq(ac acVar, CheckBoxPreference checkBoxPreference) {
        this.a = acVar;
        this.b = checkBoxPreference;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.b.setChecked(false);
    }
}
