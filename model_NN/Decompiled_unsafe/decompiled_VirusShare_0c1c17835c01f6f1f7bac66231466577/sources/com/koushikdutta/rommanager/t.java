package com.koushikdutta.rommanager;

import android.preference.CheckBoxPreference;
import android.preference.Preference;
import java.io.File;

class t implements Preference.OnPreferenceClickListener {
    final /* synthetic */ SettingsScreen a;
    private final /* synthetic */ CheckBoxPreference b;

    t(SettingsScreen settingsScreen, CheckBoxPreference checkBoxPreference) {
        this.a = settingsScreen;
        this.b = checkBoxPreference;
    }

    public boolean onPreferenceClick(Preference preference) {
        try {
            if (this.b.isChecked()) {
                new File("/sdcard/clockworkmod/.hidenandroidprogress").createNewFile();
            } else {
                new File("/sdcard/clockworkmod/.hidenandroidprogress").delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.b.setSummary(this.b.isChecked() ? C0000R.string.fast_nandroid_on : C0000R.string.fast_nandroid_off);
        return true;
    }
}
