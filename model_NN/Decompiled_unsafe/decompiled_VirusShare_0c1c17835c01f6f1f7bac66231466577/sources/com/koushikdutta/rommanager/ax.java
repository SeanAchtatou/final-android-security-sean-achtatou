package com.koushikdutta.rommanager;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.koushikdutta.rommanager.b.e;

class ax implements ServiceConnection {
    private final /* synthetic */ Context a;
    private final /* synthetic */ dw b;

    ax(Context context, dw dwVar) {
        this.a = context;
        this.b = dwVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, boolean, java.lang.String, java.lang.String):void
     arg types: [android.content.Context, com.koushikdutta.rommanager.dw, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, java.lang.String, com.koushikdutta.rommanager.cq):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, boolean, java.lang.String, java.lang.String):void */
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            e.a(iBinder).a(new fz(this, this.a, this.b, this));
        } catch (Exception e) {
            gd.b(this.a, this.b, false, (String) null, (String) null);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
    }
}
