package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

class fu implements DialogInterface.OnClickListener {
    final /* synthetic */ RomManager a;

    fu(RomManager romManager) {
        this.a = romManager;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            if (this.a.getPackageManager().getPackageInfo("com.koushikdutta.rommanager.license", 0) != null) {
                Intent intent = new Intent();
                intent.setData(Uri.parse("market://details?id=com.koushikdutta.rommanager.license"));
                try {
                    this.a.startActivity(intent);
                } catch (Exception e) {
                }
            } else {
                throw new Exception();
            }
        } catch (Exception e2) {
            Intent intent2 = new Intent();
            intent2.setData(Uri.parse("market://details?id=com.koushikdutta.rommanager"));
            try {
                this.a.startActivity(intent2);
            } catch (Exception e3) {
            }
        }
    }
}
