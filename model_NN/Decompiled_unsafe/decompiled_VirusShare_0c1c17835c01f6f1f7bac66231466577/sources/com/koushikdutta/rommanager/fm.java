package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

class fm extends ck {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    fm(RomManager romManager, ActivityBase activityBase, int i, int i2, int i3) {
        super(activityBase, i, i2, i3);
        this.a = romManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void a() {
        if (this.a.j()) {
            if (!gd.a(this.a, (dw) null)) {
                gd.a((Context) this.a, (int) C0000R.string.qrcode_premium_required);
                return;
            }
            try {
                Intent intent = new Intent("com.google.zxing.client.android.SCAN");
                intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
                this.a.startActivityForResult(intent, 1000);
                this.a.a("Install", "QR Code", "QR Code");
            } catch (Exception e) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
                builder.setTitle((int) C0000R.string.qrcode_not_installed);
                builder.setMessage((int) C0000R.string.qrcode_install);
                builder.setCancelable(true);
                builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
                builder.setPositiveButton(17039370, new fk(this));
                builder.create().show();
            }
        }
    }
}
