package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;

class fo extends ck {
    int a = 1;
    int b = 1;
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager h;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    fo(RomManager romManager, ActivityBase activityBase, int i, int i2, int i3) {
        super(activityBase, i, i2, i3);
        this.h = romManager;
    }

    public void a() {
        if (this.h.j()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.h);
            builder.setCancelable(true);
            builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
            builder.setTitle((int) C0000R.string.partition_ext_size);
            builder.setSingleChoiceItems((int) C0000R.array.ext_size_options, this.a, new cp(this));
            builder.setPositiveButton(17039370, new co(this));
            builder.create().show();
        }
    }
}
