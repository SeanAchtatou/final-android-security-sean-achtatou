package com.koushikdutta.rommanager;

import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.RelativeLayout;

class fx implements AbsListView.OnScrollListener {
    final /* synthetic */ ActivityBase a;

    fx(ActivityBase activityBase) {
        this.a = activityBase;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.ActivityBase.a(com.koushikdutta.rommanager.ActivityBase, boolean):void
     arg types: [com.koushikdutta.rommanager.ActivityBase, int]
     candidates:
      com.koushikdutta.rommanager.ActivityBase.a(int, com.koushikdutta.rommanager.ck):com.koushikdutta.rommanager.ck
      com.koushikdutta.rommanager.ActivityBase.a(com.koushikdutta.rommanager.ActivityBase, boolean):void */
    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (i == 0) {
            try {
                if (this.a.h) {
                    this.a.h = false;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(new ViewGroup.LayoutParams(-2, -2));
                    layoutParams.addRule(12);
                    layoutParams.addRule(13);
                    this.a.i.setLayoutParams(layoutParams);
                }
            } catch (Exception e) {
            }
        } else if (i + i2 >= i3 && !this.a.h) {
            this.a.h = true;
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(new ViewGroup.LayoutParams(-2, -2));
            layoutParams2.addRule(10);
            layoutParams2.addRule(13);
            this.a.i.setLayoutParams(layoutParams2);
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
    }
}
