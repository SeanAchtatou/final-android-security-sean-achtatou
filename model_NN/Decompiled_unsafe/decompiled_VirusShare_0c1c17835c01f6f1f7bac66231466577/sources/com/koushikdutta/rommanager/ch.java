package com.koushikdutta.rommanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.preference.Preference;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

class ch implements Preference.OnPreferenceClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ InstallRom a;
    private final /* synthetic */ File b;

    ch(InstallRom installRom, File file) {
        this.a = installRom;
        this.b = file;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.app.Activity, java.util.ArrayList, boolean):void
     arg types: [com.koushikdutta.rommanager.InstallRom, java.util.ArrayList, int]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder, boolean):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, boolean):boolean
      com.koushikdutta.rommanager.gd.a(android.app.Activity, java.util.ArrayList, boolean):void */
    public boolean onPreferenceClick(Preference preference) {
        String absolutePath = this.b.getAbsolutePath();
        if (absolutePath.contains(" ")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
            builder.setTitle((int) C0000R.string.install_rom);
            builder.setMessage((int) C0000R.string.no_spaces);
            builder.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
            builder.create().show();
            return true;
        }
        ArrayList arrayList = new ArrayList(this.a.f);
        arrayList.add(absolutePath);
        if (!this.a.e.b("developer_mode", false) || !this.a.e.b("is_clockworkmod", false)) {
            gd.a((Activity) this.a, arrayList, false);
            this.a.a("Install", "SD Card", "SD Card");
            return true;
        }
        AlertDialog.Builder builder2 = new AlertDialog.Builder(this.a);
        builder2.setTitle((int) C0000R.string.install_queue);
        RomPackage romPackage = (RomPackage) this.a.getIntent().getParcelableExtra("rompackage");
        builder2.setPositiveButton(17039370, new aq(this, romPackage, arrayList));
        builder2.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder2.setNeutralButton((int) C0000R.string.add_another_zip, new ap(this, arrayList, romPackage));
        StringBuilder sb = new StringBuilder();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            sb.append(String.valueOf(new File((String) it.next()).getName()) + "\n");
        }
        builder2.setMessage(sb.toString());
        builder2.create().show();
        return true;
    }
}
