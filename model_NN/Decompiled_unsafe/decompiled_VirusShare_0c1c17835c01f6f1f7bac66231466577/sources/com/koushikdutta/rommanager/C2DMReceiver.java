package com.koushikdutta.rommanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class C2DMReceiver extends BroadcastReceiver {
    private void a(Context context, Intent intent) {
        Log.i("C2DMReceiver", "Tickle received!");
        if (gd.a(context, (dw) null)) {
            for (String next : intent.getExtras().keySet()) {
                Log.i("C2DMReceiver", String.valueOf(next) + ": " + intent.getStringExtra(next));
                String stringExtra = intent.getStringExtra("url");
                if (stringExtra == null) {
                    Log.i("C2DMReceiver", "No url in ROM!");
                    return;
                }
                String stringExtra2 = intent.getStringExtra("name");
                RomPackage romPackage = new RomPackage();
                if (stringExtra2 == null) {
                    stringExtra2 = "Browser to Phone ROM";
                }
                romPackage.a = stringExtra2;
                romPackage.d = 0;
                romPackage.b = new RomPart[1];
                RomPart[] romPartArr = romPackage.b;
                RomPart romPart = new RomPart();
                romPartArr[0] = romPart;
                romPart.a = stringExtra2;
                romPart.c.add(stringExtra);
                Intent intent2 = new Intent(context, DownloadService.class);
                intent2.putExtra("rompackage", romPackage);
                context.startService(intent2);
                Intent intent3 = new Intent(context, Downloading.class);
                intent3.setFlags(268435456);
                context.startActivity(intent3);
            }
        }
    }

    private void b(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra("registration_id");
        if (intent.getStringExtra("error") != null) {
            Log.i("C2DMReceiver", intent.getStringExtra("error"));
        } else if (intent.getStringExtra("unregistered") == null && stringExtra != null) {
            Log.i("C2DMReceiver", stringExtra);
            h.a(context).a("registration_id", stringExtra);
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("com.google.android.c2dm.intent.REGISTRATION")) {
            b(context, intent);
        } else if (intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {
            a(context, intent);
        }
    }
}
