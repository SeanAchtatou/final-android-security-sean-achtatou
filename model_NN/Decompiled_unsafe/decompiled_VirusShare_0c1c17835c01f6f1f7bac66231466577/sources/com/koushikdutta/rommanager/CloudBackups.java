package com.koushikdutta.rommanager;

import android.os.Bundle;

public class CloudBackups extends ActivityBase {
    boolean h = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((int) C0000R.string.backups_for_current_device, new ek(this, this, C0000R.string.new_cloud_backup, 0, 0));
        String a = h.a(this).a("detected_device");
        gd.a(new String[]{String.format("%s/backup/%s", "http://rombackup.deployfu.com", h.a(this).a("account"))}, this, false, null, true, new ej(this, a));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.h = true;
    }
}
