package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.content.Intent;

class eh implements DialogInterface.OnClickListener {
    final /* synthetic */ SettingsScreen a;
    private final /* synthetic */ String b;

    eh(SettingsScreen settingsScreen, String str) {
        this.a = settingsScreen;
        this.b = str;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{"rommanager@clockworkmod.com"});
        intent.putExtra("android.intent.extra.SUBJECT", "Device Id: " + gd.f(this.a));
        intent.putExtra("android.intent.extra.TEXT", this.b);
        intent.setType("text/plain");
        this.a.startActivity(Intent.createChooser(intent, "Email:"));
    }
}
