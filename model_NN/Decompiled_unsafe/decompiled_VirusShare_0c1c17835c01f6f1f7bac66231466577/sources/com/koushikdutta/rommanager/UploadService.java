package com.koushikdutta.rommanager;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UploadService extends Service {
    NotificationManager a;
    final String b = "UploadService";
    boolean c = false;
    Thread d = null;
    Handler e = new Handler();
    h f;
    String g;

    public void a() {
        File file = new File(String.format("%s/upload.tgz", "/sdcard/clockworkmod/tar"));
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(String.format("%s/backup/%s/%s", "http://rombackup.deployfu.com", Uri.encode(this.f.a("account")), Uri.encode(this.f.a("detected_device")))).openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setChunkedStreamingMode(65536);
        httpURLConnection.setRequestProperty("Content-Type", "application/binary");
        httpURLConnection.setRequestProperty("registration_id", this.f.a("registration_id"));
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        FileInputStream fileInputStream = new FileInputStream(file);
        OutputStream outputStream = httpURLConnection.getOutputStream();
        bg.a(fileInputStream, outputStream);
        outputStream.close();
        fileInputStream.close();
        InputStream inputStream = httpURLConnection.getInputStream();
        String a2 = bg.a(inputStream);
        inputStream.close();
        System.out.println(a2);
    }

    /* access modifiers changed from: package-private */
    public void b() {
        try {
            File file = new File(String.format("%s/upload.tgz", "/sdcard/clockworkmod/tar"));
            if (file.exists()) {
                new df(this).start();
                return;
            }
            File[] listFiles = new File("/sdcard/clockworkmod/cloud").listFiles(new dg(this));
            if (listFiles == null || listFiles.length == 0) {
                stopSelf();
            } else {
                new dh(this, listFiles, file).start();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.f = h.a(this);
        this.g = this.f.a("detected_device");
        this.a = (NotificationManager) getSystemService("notification");
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        this.e.post(new di(this));
    }
}
