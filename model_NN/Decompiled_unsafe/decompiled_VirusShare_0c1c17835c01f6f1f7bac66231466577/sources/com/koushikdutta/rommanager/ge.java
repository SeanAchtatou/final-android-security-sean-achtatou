package com.koushikdutta.rommanager;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

class ge implements fs {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ ae a;
    private final /* synthetic */ ProgressDialog b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;

    ge(ae aeVar, ProgressDialog progressDialog, String str, String str2) {
        this.a = aeVar;
        this.b = progressDialog;
        this.c = str;
        this.d = str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void a(Bundle bundle) {
        if (bundle == null) {
            try {
                throw new Exception();
            } catch (Exception e) {
                this.b.dismiss();
                e.printStackTrace();
                gd.a((Context) this.a.a.a, (int) C0000R.string.web_connect_error);
            }
        } else if (bundle.getString("authtoken") == null) {
            throw new Exception();
        } else {
            new af(this, this.c, this.d, this.b).start();
        }
    }
}
