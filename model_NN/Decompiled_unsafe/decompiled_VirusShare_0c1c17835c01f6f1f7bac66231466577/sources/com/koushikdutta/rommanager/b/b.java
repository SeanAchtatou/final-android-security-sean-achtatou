package com.koushikdutta.rommanager.b;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

public abstract class b extends Binder implements f {
    public b() {
        attachInterface(this, "com.koushikdutta.rommanager.license.ILicenseCheckerCallback");
    }

    public static f a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.koushikdutta.rommanager.license.ILicenseCheckerCallback");
        return (queryLocalInterface == null || !(queryLocalInterface instanceof f)) ? new d(iBinder) : (f) queryLocalInterface;
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.koushikdutta.rommanager.license.ILicenseCheckerCallback");
                a(parcel.readInt() != 0, parcel.readString(), parcel.readString());
                return true;
            case 1598968902:
                parcel2.writeString("com.koushikdutta.rommanager.license.ILicenseCheckerCallback");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
