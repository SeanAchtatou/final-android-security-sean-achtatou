package com.koushikdutta.rommanager.b;

import android.os.IBinder;
import android.os.Parcel;

class d implements f {
    private IBinder a;

    d(IBinder iBinder) {
        this.a = iBinder;
    }

    public void a(boolean z, String str, String str2) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.koushikdutta.rommanager.license.ILicenseCheckerCallback");
            obtain.writeInt(z ? 1 : 0);
            obtain.writeString(str);
            obtain.writeString(str2);
            this.a.transact(1, obtain, null, 1);
        } finally {
            obtain.recycle();
        }
    }

    public IBinder asBinder() {
        return this.a;
    }
}
