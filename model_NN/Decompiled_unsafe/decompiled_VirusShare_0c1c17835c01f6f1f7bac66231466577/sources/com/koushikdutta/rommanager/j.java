package com.koushikdutta.rommanager;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import java.util.LinkedHashMap;
import java.util.Map;

public class j extends BaseAdapter {
    public final Map b = new LinkedHashMap();
    public final ArrayAdapter c;

    public j(Context context) {
        this.c = new ArrayAdapter(context, C0000R.layout.list_header);
    }

    public void a(String str, Adapter adapter) {
        this.c.add(str);
        this.b.put(str, adapter);
    }

    public int getCount() {
        int i = 0;
        for (Adapter count : this.b.values()) {
            i += count.getCount() + 1;
        }
        return i;
    }

    public Object getItem(int i) {
        int i2 = i;
        for (Object next : this.b.keySet()) {
            Adapter adapter = (Adapter) this.b.get(next);
            int count = adapter.getCount() + 1;
            if (i2 == 0) {
                return next;
            }
            if (i2 < count) {
                return adapter.getItem(i2 - 1);
            }
            i2 -= count;
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public int getItemViewType(int i) {
        int i2 = 1;
        int i3 = i;
        for (Object obj : this.b.keySet()) {
            Adapter adapter = (Adapter) this.b.get(obj);
            int count = adapter.getCount() + 1;
            if (i3 == 0) {
                return 0;
            }
            if (i3 < count) {
                return adapter.getItemViewType(i3 - 1) + i2;
            }
            i3 -= count;
            i2 = adapter.getViewTypeCount() + i2;
        }
        return -1;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        int i2 = 0;
        int i3 = i;
        for (Object obj : this.b.keySet()) {
            Adapter adapter = (Adapter) this.b.get(obj);
            int count = adapter.getCount() + 1;
            if (i3 == 0) {
                return this.c.getView(i2, view, viewGroup);
            }
            if (i3 < count) {
                return adapter.getView(i3 - 1, view, viewGroup);
            }
            i2++;
            i3 -= count;
        }
        return null;
    }

    public int getViewTypeCount() {
        int i = 1;
        for (Adapter viewTypeCount : this.b.values()) {
            i += viewTypeCount.getViewTypeCount();
        }
        return i;
    }

    public boolean isEnabled(int i) {
        return getItemViewType(i) != 0;
    }
}
