package com.koushikdutta.rommanager;

import android.app.Activity;
import android.content.DialogInterface;
import android.net.Uri;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

class bl implements DialogInterface.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ am a;
    private final /* synthetic */ String b;
    private final /* synthetic */ EditText c;
    private final /* synthetic */ String d;
    private final /* synthetic */ TextView e;
    private final /* synthetic */ ListView f;
    private final /* synthetic */ ArrayAdapter g;

    bl(am amVar, String str, EditText editText, String str2, TextView textView, ListView listView, ArrayAdapter arrayAdapter) {
        this.a = amVar;
        this.b = str;
        this.c = editText;
        this.d = str2;
        this.e = textView;
        this.f = listView;
        this.g = arrayAdapter;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.RomList, int, com.koushikdutta.rommanager.bh]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        gd.a("https://rommanager.deployfu.com/ratings/" + Uri.encode(this.a.a.b.l) + "/" + Uri.encode(this.b) + "?comment=" + Uri.encode(this.c.getText().toString()) + "&registrationId=" + Uri.encode(this.d), (Activity) this.a.a.b, false, (cq) new bh(this, this.e, this.f, this.g));
    }
}
