package com.koushikdutta.rommanager;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.koushikdutta.a.a;

class el implements View.OnClickListener {
    final /* synthetic */ bj a;
    private final /* synthetic */ ArrayAdapter b;
    private final /* synthetic */ ImageView c;

    el(bj bjVar, ArrayAdapter arrayAdapter, ImageView imageView) {
        this.a = bjVar;
        this.b = arrayAdapter;
        this.c = imageView;
    }

    public void onClick(View view) {
        this.a.a++;
        if (this.a.a >= this.b.getCount()) {
            this.a.a = 0;
        }
        a.a(this.c, (String) this.b.getItem(this.a.a));
    }
}
