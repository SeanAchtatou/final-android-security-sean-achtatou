package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.MenuItem;

class ff implements MenuItem.OnMenuItemClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager a;

    ff(RomManager romManager) {
        this.a = romManager;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        builder.setIcon(17301543);
        builder.setTitle((int) C0000R.string.recovery);
        builder.setCancelable(true);
        builder.setMessage((int) C0000R.string.flash_recovery_override);
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton(17039370, new gf(this));
        builder.create().show();
        return true;
    }
}
