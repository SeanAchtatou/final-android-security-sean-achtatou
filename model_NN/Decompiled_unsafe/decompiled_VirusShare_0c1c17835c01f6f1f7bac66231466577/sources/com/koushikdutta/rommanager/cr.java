package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.Context;
import android.preference.Preference;
import java.io.File;

class cr extends Preference {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ ManageBackups a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cr(ManageBackups manageBackups, Context context) {
        super(context);
        this.a = manageBackups;
    }

    /* access modifiers changed from: protected */
    public void onClick() {
        super.onClick();
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        String charSequence = getTitle().toString();
        File file = new File("/sdcard/clockworkmod/backup", charSequence);
        builder.setTitle(charSequence);
        builder.setItems((int) C0000R.array.manage_options, new dz(this, file, charSequence));
        builder.create().show();
    }
}
