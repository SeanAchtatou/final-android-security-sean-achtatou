package com.koushikdutta.rommanager;

import java.util.Comparator;

class bz implements Comparator {
    final /* synthetic */ DeveloperList a;

    bz(DeveloperList developerList) {
        this.a = developerList;
    }

    /* renamed from: a */
    public int compare(ck ckVar, ck ckVar2) {
        if (((dk) ckVar).b == ((dk) ckVar2).b) {
            return 0;
        }
        return ((dk) ckVar).b < ((dk) ckVar2).b ? -1 : 1;
    }
}
