package com.koushikdutta.rommanager;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class InstallRom extends AnalyticsActivity {
    h e;
    ArrayList f;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.e = h.a(this);
        ArrayList arrayList = new ArrayList();
        PreferenceScreen createPreferenceScreen = getPreferenceManager().createPreferenceScreen(this);
        setPreferenceScreen(createPreferenceScreen);
        this.f = getIntent().getStringArrayListExtra("zips");
        if (this.f == null) {
            this.f = new ArrayList();
        }
        String stringExtra = getIntent().getStringExtra("path");
        if (stringExtra == null) {
            stringExtra = "/sdcard";
        }
        File file = new File(stringExtra);
        if (file != null) {
            for (File file2 : file.listFiles()) {
                if (file2.isDirectory() && !file2.getName().startsWith(".")) {
                    arrayList.add(file2);
                }
            }
        }
        Collections.sort(arrayList, new cb(this));
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            File file3 = (File) it.next();
            Preference preference = new Preference(this);
            preference.setTitle(String.valueOf(file3.getName()) + "/");
            createPreferenceScreen.addPreference(preference);
            preference.setOnPreferenceClickListener(new cf(this, file3));
        }
        arrayList.clear();
        for (File file4 : file.listFiles()) {
            if (!file4.isDirectory() && !file4.getName().startsWith(".") && file4.getName().endsWith(".zip")) {
                arrayList.add(file4);
            }
        }
        Collections.sort(arrayList, new ce(this));
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            File file5 = (File) it2.next();
            Preference preference2 = new Preference(this);
            preference2.setTitle(file5.getName());
            createPreferenceScreen.addPreference(preference2);
            preference2.setOnPreferenceClickListener(new ch(this, file5));
        }
    }
}
