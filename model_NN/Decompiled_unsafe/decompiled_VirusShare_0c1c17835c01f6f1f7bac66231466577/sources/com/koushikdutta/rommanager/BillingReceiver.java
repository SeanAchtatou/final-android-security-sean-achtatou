package com.koushikdutta.rommanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.a.a.b.e;

public class BillingReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("com.android.vending.billing.PURCHASE_STATE_CHANGED".equals(action)) {
            String stringExtra = intent.getStringExtra("inapp_signed_data");
            String stringExtra2 = intent.getStringExtra("inapp_signature");
            Intent intent2 = new Intent(String.valueOf(context.getPackageName()) + ".PURCHASE_STATE_CHANGED");
            intent2.putExtra("inapp_signed_data", stringExtra);
            intent2.putExtra("inapp_signature", stringExtra2);
            context.sendBroadcast(intent2);
        } else if ("com.android.vending.billing.IN_APP_NOTIFY".equals(action)) {
            intent.getStringExtra("notification_id");
        } else if ("com.android.vending.billing.RESPONSE_CODE".equals(action)) {
            intent.getLongExtra("request_id", -1);
            Log.i("BillingReceiver", String.valueOf(intent.getIntExtra("response_code", e.RESULT_ERROR.ordinal())));
        } else {
            Log.w("BillingReceiver", "unexpected action: " + action);
        }
    }
}
