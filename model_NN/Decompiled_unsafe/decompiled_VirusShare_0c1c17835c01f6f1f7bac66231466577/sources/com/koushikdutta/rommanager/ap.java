package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.content.Intent;
import java.util.ArrayList;

class ap implements DialogInterface.OnClickListener {
    final /* synthetic */ ch a;
    private final /* synthetic */ ArrayList b;
    private final /* synthetic */ RomPackage c;

    ap(ch chVar, ArrayList arrayList, RomPackage romPackage) {
        this.a = chVar;
        this.b = arrayList;
        this.c = romPackage;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this.a.a, InstallRom.class);
        intent.putStringArrayListExtra("zips", this.b);
        intent.putExtra("rompackage", this.c);
        this.a.a.startActivity(intent);
    }
}
