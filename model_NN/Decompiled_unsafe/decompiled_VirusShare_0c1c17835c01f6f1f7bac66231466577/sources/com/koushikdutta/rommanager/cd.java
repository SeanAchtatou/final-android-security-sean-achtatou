package com.koushikdutta.rommanager;

import android.view.MenuItem;

class cd implements MenuItem.OnMenuItemClickListener {
    final /* synthetic */ DeveloperList a;

    cd(DeveloperList developerList) {
        this.a = developerList;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        h.a(this.a).a("developer_sort", "date");
        this.a.d();
        this.a.f();
        this.a.f.notifyDataSetChanged();
        return true;
    }
}
