package com.koushikdutta.rommanager;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

class em implements DialogInterface.OnClickListener {
    final /* synthetic */ gc a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ StringBuilder c;

    em(gc gcVar, Activity activity, StringBuilder sb) {
        this.a = gcVar;
        this.b = activity;
        this.c = sb;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [android.app.Activity, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            gd.c(this.b, this.c.toString());
        } catch (Exception e) {
            e.printStackTrace();
            gd.a((Context) this.b, (int) C0000R.string.script_error);
        }
    }
}
