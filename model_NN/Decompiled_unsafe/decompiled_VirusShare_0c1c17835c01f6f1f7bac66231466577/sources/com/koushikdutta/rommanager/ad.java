package com.koushikdutta.rommanager;

import android.content.ComponentName;
import android.preference.CheckBoxPreference;
import android.preference.Preference;

class ad implements Preference.OnPreferenceClickListener {
    final /* synthetic */ SettingsScreen a;
    private final /* synthetic */ CheckBoxPreference b;

    ad(SettingsScreen settingsScreen, CheckBoxPreference checkBoxPreference) {
        this.a = settingsScreen;
        this.b = checkBoxPreference;
    }

    public boolean onPreferenceClick(Preference preference) {
        this.a.getPackageManager().setComponentEnabledSetting(new ComponentName(this.a, UpdateReceiver.class), this.b.isChecked() ? 1 : 2, 1);
        this.b.setSummary(this.b.isChecked() ? C0000R.string.update_notifications_enabled : C0000R.string.update_notifications_disabled);
        return true;
    }
}
