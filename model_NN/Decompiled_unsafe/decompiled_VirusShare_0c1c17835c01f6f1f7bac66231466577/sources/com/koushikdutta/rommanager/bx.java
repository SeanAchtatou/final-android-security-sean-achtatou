package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.util.Log;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class bx implements DialogInterface.OnClickListener {
    final /* synthetic */ RomList a;
    private final /* synthetic */ JSONArray b;
    private final /* synthetic */ boolean[] c;
    private final /* synthetic */ ArrayList d;
    private final /* synthetic */ String e;
    private final /* synthetic */ JSONObject f;

    bx(RomList romList, JSONArray jSONArray, boolean[] zArr, ArrayList arrayList, String str, JSONObject jSONObject) {
        this.a = romList;
        this.b = jSONArray;
        this.c = zArr;
        this.d = arrayList;
        this.e = str;
        this.f = jSONObject;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        for (int i2 = 0; i2 < this.b.length(); i2++) {
            try {
                if (this.c[i2]) {
                    this.d.add(this.b.getJSONObject(i2));
                }
            } catch (JSONException e2) {
                Log.e("Romlist", e2.getLocalizedMessage(), e2);
            }
        }
        this.a.b(this.e);
        this.a.a(this.f, this.d);
    }
}
