package com.koushikdutta.rommanager;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Gallery;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.google.ads.AdView;
import com.google.ads.a;
import com.google.ads.d;
import com.google.ads.g;
import org.json.JSONArray;
import org.json.JSONObject;

class bv extends ck {
    JSONObject a;
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomList b;
    private final /* synthetic */ String h;
    private final /* synthetic */ JSONArray i;
    private final /* synthetic */ String j;
    private final /* synthetic */ JSONObject k;
    private final /* synthetic */ JSONObject l;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    bv(RomList romList, ActivityBase activityBase, String str, String str2, String str3, JSONArray jSONArray, String str4, JSONObject jSONObject, JSONObject jSONObject2) {
        super(activityBase, str, str2);
        this.b = romList;
        this.h = str3;
        this.i = jSONArray;
        this.j = str4;
        this.k = jSONObject;
        this.l = jSONObject2;
    }

    public View a(Context context, View view) {
        int i2;
        double d;
        JSONObject optJSONObject;
        View a2 = super.a(context, view);
        RatingBar ratingBar = (RatingBar) a2.findViewById(C0000R.id.rating);
        TextView textView = (TextView) a2.findViewById(C0000R.id.downloads);
        if (gd.b(this.h)) {
            ratingBar.setVisibility(8);
            textView.setVisibility(8);
        } else {
            ratingBar.setVisibility(0);
            textView.setVisibility(0);
            if (this.b.k == null || (optJSONObject = this.b.k.optJSONObject(this.h)) == null) {
                i2 = 0;
                d = 0.0d;
            } else {
                d = optJSONObject.optDouble("rating", 0.0d);
                i2 = optJSONObject.optInt("downloads");
            }
            textView.setText(this.b.getString(C0000R.string.downloads, new Object[]{Integer.valueOf(i2)}));
            ratingBar.setRating((float) d);
        }
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.RomList, int, com.koushikdutta.rommanager.ak]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    public void a() {
        JSONObject jSONObject;
        this.a = null;
        Dialog dialog = new Dialog(this.b, 16973829);
        String a2 = h.a(this.b).a("registration_id");
        String a3 = h.a(this.b).a("nickname");
        dialog.setContentView(C0000R.layout.rom);
        Gallery gallery = (Gallery) dialog.findViewById(C0000R.id.gallery);
        bk bkVar = new bk(this, this.b, 0);
        gallery.setAdapter((SpinnerAdapter) bkVar);
        if (this.i != null) {
            for (int i2 = 0; i2 < this.i.length(); i2++) {
                String optString = this.i.optString(i2);
                if (!gd.b(optString)) {
                    bkVar.add(optString);
                }
            }
        }
        if (bkVar.getCount() == 0) {
            gallery.setVisibility(8);
        }
        gallery.setOnItemClickListener(new bj(this, bkVar));
        dialog.setTitle(this.j);
        ((Button) dialog.findViewById(C0000R.id.cancel)).setOnClickListener(new ai(this, dialog));
        ((Button) dialog.findViewById(C0000R.id.download)).setOnClickListener(new aj(this, this.k, this.l, this.j));
        Button button = (Button) dialog.findViewById(C0000R.id.comment);
        try {
            jSONObject = new JSONObject(h.a(this.b).a("blocked_users"));
        } catch (Exception e) {
            jSONObject = new JSONObject();
        }
        ag agVar = new ag(this, this.b, 0);
        TextView textView = (TextView) dialog.findViewById(C0000R.id.comments_info);
        ListView listView = (ListView) dialog.findViewById(C0000R.id.comments);
        listView.setOnItemLongClickListener(new ah(this, agVar, jSONObject));
        button.setOnClickListener(new am(this, a2, this.h, textView, listView, agVar));
        listView.setAdapter((ListAdapter) agVar);
        RatingBar ratingBar = (RatingBar) dialog.findViewById(C0000R.id.rating);
        ratingBar.setOnRatingBarChangeListener(new an(this, a2, this.h));
        if (gd.b(this.h)) {
            button.setVisibility(8);
            ratingBar.setVisibility(8);
            listView.setVisibility(8);
            dialog.getWindow().setLayout(-1, -2);
        } else {
            dialog.getWindow().setLayout(-1, -1);
            gd.a((gd.b(a2) || gd.b(a3)) ? "http://rommanager.deployfu.com/ratings/" + Uri.encode(this.b.l) + "/" + Uri.encode(this.h) : "https://rommanager.deployfu.com/ratings/" + Uri.encode(this.b.l) + "/" + Uri.encode(this.h) + "?registrationId=" + Uri.encode(a2), (Activity) this.b, false, (cq) new ak(this, textView, listView, jSONObject, agVar, ratingBar));
        }
        if (gd.e(this.b)) {
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            AdView adView = new AdView(this.b, g.a, "a14da1301aa3d95");
            adView.setLayoutParams(layoutParams);
            ((FrameLayout) dialog.findViewById(C0000R.id.adroot)).addView(adView);
            d dVar = new d();
            dVar.a(false);
            dVar.a(a.MALE);
            gd.a(this.b, dVar);
            adView.a(dVar);
        }
        dialog.show();
    }
}
