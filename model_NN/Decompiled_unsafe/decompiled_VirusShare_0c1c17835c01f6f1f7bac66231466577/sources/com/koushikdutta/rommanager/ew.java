package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.view.MenuItem;

class ew implements MenuItem.OnMenuItemClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager a;

    ew(RomManager romManager) {
        this.a = romManager;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        if (gd.a(this.a, (dw) null)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
            builder.setTitle((int) C0000R.string.donate);
            builder.setItems(new String[]{"PayPal", "Android Market"}, new fi(this));
            builder.create().show();
        } else {
            AlertDialog.Builder builder2 = new AlertDialog.Builder(this.a);
            builder2.setTitle((int) C0000R.string.buy_premium);
            builder2.setItems(new String[]{"PayPal", "Android Market", "Recover PayPal License"}, new fj(this));
            builder2.create().show();
        }
        return true;
    }
}
