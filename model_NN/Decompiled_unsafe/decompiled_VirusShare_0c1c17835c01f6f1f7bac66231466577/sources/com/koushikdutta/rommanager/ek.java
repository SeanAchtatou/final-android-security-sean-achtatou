package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import java.text.SimpleDateFormat;
import java.util.Date;

class ek extends ck {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ CloudBackups a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ek(CloudBackups cloudBackups, ActivityBase activityBase, int i, int i2, int i3) {
        super(activityBase, i, i2, i3);
        this.a = cloudBackups;
    }

    public void a() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        View inflate = View.inflate(this.a, C0000R.layout.romname, null);
        EditText editText = (EditText) inflate.findViewById(C0000R.id.romname);
        editText.setText(new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss").format(new Date()));
        editText.selectAll();
        builder.setIcon(0);
        builder.setTitle((int) C0000R.string.edit_backup_name);
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new q(this, editText));
        builder.setView(inflate);
        builder.create().show();
    }
}
