package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.preference.CheckBoxPreference;
import android.widget.TimePicker;

class gr implements TimePickerDialog.OnTimeSetListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ ac a;
    private final /* synthetic */ CheckBoxPreference b;

    gr(ac acVar, CheckBoxPreference checkBoxPreference) {
        this.a = acVar;
        this.b = checkBoxPreference;
    }

    public void onTimeSet(TimePicker timePicker, int i, int i2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.b);
        builder.setCancelable(true);
        builder.setTitle((int) C0000R.string.automatic_backup_frequency);
        builder.setNegativeButton(17039360, new bs(this, this.b));
        builder.setPositiveButton(17039370, new br(this, i, i2));
        builder.setSingleChoiceItems((int) C0000R.array.automatic_backup_frequencies, this.a.a, new bu(this));
        builder.create().show();
    }
}
