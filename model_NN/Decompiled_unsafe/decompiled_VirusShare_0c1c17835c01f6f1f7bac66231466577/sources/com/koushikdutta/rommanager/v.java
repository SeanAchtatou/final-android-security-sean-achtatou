package com.koushikdutta.rommanager;

import android.preference.CheckBoxPreference;
import android.preference.Preference;

class v implements Preference.OnPreferenceClickListener {
    final /* synthetic */ SettingsScreen a;
    private final /* synthetic */ CheckBoxPreference b;

    v(SettingsScreen settingsScreen, CheckBoxPreference checkBoxPreference) {
        this.a = settingsScreen;
        this.b = checkBoxPreference;
    }

    public boolean onPreferenceClick(Preference preference) {
        this.a.c.a("erase_recovery", this.b.isChecked());
        this.b.setSummary(this.b.isChecked() ? C0000R.string.erase_recovery_on : C0000R.string.erase_recovery_off);
        return true;
    }
}
