package com.koushikdutta.rommanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BackupReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        new dy(this, context).start();
    }
}
