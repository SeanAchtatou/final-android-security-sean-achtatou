package com.koushikdutta.rommanager;

import java.io.File;
import java.util.Comparator;

class cb implements Comparator {
    final /* synthetic */ InstallRom a;

    cb(InstallRom installRom) {
        this.a = installRom;
    }

    /* renamed from: a */
    public int compare(File file, File file2) {
        return file.getName().compareToIgnoreCase(file2.getName());
    }
}
