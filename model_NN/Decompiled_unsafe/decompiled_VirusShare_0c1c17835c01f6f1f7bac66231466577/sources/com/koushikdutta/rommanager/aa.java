package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

class aa implements DialogInterface.OnClickListener {
    final /* synthetic */ DeveloperList a;

    aa(DeveloperList developerList) {
        this.a = developerList;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent();
        intent.setData(Uri.parse("market://details?id=com.koushikdutta.rommanager"));
        this.a.startActivity(intent);
        this.a.finish();
    }
}
