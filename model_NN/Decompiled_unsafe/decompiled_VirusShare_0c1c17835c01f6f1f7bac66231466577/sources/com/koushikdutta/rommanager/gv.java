package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import java.util.ArrayList;

class gv implements DialogInterface.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ gw a;
    private final /* synthetic */ ArrayList b;
    private final /* synthetic */ boolean c;

    gv(gw gwVar, ArrayList arrayList, boolean z) {
        this.a = gwVar;
        this.b = arrayList;
        this.c = z;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        dx dxVar = (dx) this.b.get(i);
        this.a.a.c.a("detected_device", dxVar.b);
        this.a.a.c.a("readonly_recovery", dxVar.d);
        this.a.a.c.a("reboot_recovery", dxVar.e);
        this.a.a.c.a("flash_recovery", dxVar.f);
        gd.a(this.a.a.c);
        this.a.a.b(false);
        b bVar = new b(this, dxVar);
        if (!this.c || !dxVar.g) {
            a aVar = new a(this);
            if (!this.c) {
                aVar.run();
                return;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a.a);
            builder.setMessage(this.a.a.getString(C0000R.string.not_officially_supported, new Object[]{dxVar.a}));
            builder.setPositiveButton((int) C0000R.string.yes, new d(this, aVar));
            builder.setNegativeButton((int) C0000R.string.no, new c(this, dxVar));
            builder.create().show();
            return;
        }
        bVar.run();
    }
}
