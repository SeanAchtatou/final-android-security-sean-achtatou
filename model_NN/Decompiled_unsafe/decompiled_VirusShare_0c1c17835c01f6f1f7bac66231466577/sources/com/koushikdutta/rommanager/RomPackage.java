package com.koushikdutta.rommanager;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class RomPackage implements Parcelable {
    public static final Parcelable.Creator CREATOR = new bw();
    String a;
    RomPart[] b;
    ArrayList c;
    int d;
    ArrayList e;

    public RomPackage() {
        this.c = new ArrayList();
        this.d = 1;
        this.e = new ArrayList();
    }

    private RomPackage(Parcel parcel) {
        this.c = new ArrayList();
        this.d = 1;
        this.e = new ArrayList();
        this.a = parcel.readString();
        this.d = parcel.readInt();
        Parcelable[] readParcelableArray = parcel.readParcelableArray(RomPart.class.getClassLoader());
        parcel.readStringList(this.c);
        parcel.readStringList(this.e);
        this.b = new RomPart[readParcelableArray.length];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < readParcelableArray.length) {
                this.b[i2] = (RomPart) readParcelableArray[i2];
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* synthetic */ RomPackage(Parcel parcel, RomPackage romPackage) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeInt(this.d);
        parcel.writeParcelableArray(this.b, i);
        parcel.writeStringList(this.c);
        parcel.writeStringList(this.e);
    }
}
