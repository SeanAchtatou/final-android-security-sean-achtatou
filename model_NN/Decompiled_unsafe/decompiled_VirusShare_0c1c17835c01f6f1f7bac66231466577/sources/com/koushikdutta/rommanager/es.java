package com.koushikdutta.rommanager;

import android.content.Context;
import com.paypal.android.MEP.d;
import com.paypal.android.MEP.e;
import com.paypal.android.MEP.o;
import com.paypal.android.MEP.q;
import java.math.BigDecimal;
import org.json.JSONObject;

class es implements cq {
    final /* synthetic */ RomManager a;

    es(RomManager romManager) {
        this.a = romManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void a(String str) {
        try {
            synchronized (RomManagerApplication.a) {
                gd.i(this.a);
                JSONObject jSONObject = new JSONObject(bg.a(str)).getJSONObject("purchase_request");
                double d = jSONObject.getDouble("price");
                String string = jSONObject.getString("id");
                if (gd.b(string)) {
                    throw new Exception("Null ID purchase ID was returned.");
                }
                String str2 = "ROM Manager Premium - Transaction #" + string;
                q qVar = new q();
                qVar.a("USD");
                qVar.a(new BigDecimal(d));
                qVar.a(0);
                if (gd.a == 0) {
                    qVar.b("seller_1279241043_biz@hotmail.com");
                } else {
                    qVar.b("koushik_dutta@yahoo.com");
                }
                d dVar = new d();
                dVar.a(new BigDecimal("0"));
                dVar.b(new BigDecimal("0"));
                e eVar = new e();
                eVar.a(str2);
                eVar.b(str2);
                eVar.a(new BigDecimal(d));
                eVar.b(new BigDecimal(d));
                eVar.a(1);
                dVar.c().add(eVar);
                qVar.a(dVar);
                qVar.e("ClockworkMod");
                qVar.c(str2);
                qVar.d(str2);
                qVar.f(str2);
                this.a.startActivityForResult(o.a().a(qVar, this.a, new g()), 1001);
            }
        } catch (Exception e) {
            e.printStackTrace();
            gd.a((Context) this.a, (int) C0000R.string.no_connection);
        }
    }

    public boolean a() {
        return this.a.i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void b() {
        gd.a((Context) this.a, (int) C0000R.string.no_connection);
    }
}
