package com.koushikdutta.rommanager;

import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import org.json.JSONObject;

class bh implements cq {
    final /* synthetic */ bl a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ ListView c;
    private final /* synthetic */ ArrayAdapter d;

    bh(bl blVar, TextView textView, ListView listView, ArrayAdapter arrayAdapter) {
        this.a = blVar;
        this.b = textView;
        this.c = listView;
        this.d = arrayAdapter;
    }

    public void a(String str) {
        try {
            this.a.a.a.a = new JSONObject(bg.a(str)).getJSONObject("result");
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("comment", this.a.a.a.a.optString("comment", "Woot!"));
            jSONObject.put("nickname", this.a.a.a.a.optString("nickname", this.a.a.a.b.getString(C0000R.string.anonymous)));
            jSONObject.put("rating", this.a.a.a.a.optInt("rating", 1));
            this.b.setVisibility(8);
            this.c.setVisibility(0);
            this.d.insert(jSONObject, 0);
        } catch (Exception e) {
        }
    }

    public boolean a() {
        return this.a.a.a.b.h;
    }

    public void b() {
    }
}
