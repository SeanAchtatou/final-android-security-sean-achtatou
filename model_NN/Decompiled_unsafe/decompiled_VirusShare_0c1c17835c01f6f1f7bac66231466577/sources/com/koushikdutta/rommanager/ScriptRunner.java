package com.koushikdutta.rommanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ScrollView;
import android.widget.TextView;

public class ScriptRunner extends Activity {
    TextView a;
    Handler b = new Handler();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        setTitle(intent.getStringExtra("title"));
        String stringExtra = intent.getStringExtra("script");
        ScrollView scrollView = new ScrollView(this);
        this.a = new TextView(this);
        scrollView.addView(this.a);
        setContentView(scrollView);
        new bc(this, stringExtra, scrollView).start();
    }
}
