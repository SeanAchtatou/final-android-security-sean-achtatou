package com.koushikdutta.rommanager;

import android.widget.ScrollView;
import java.io.DataInputStream;

class bc extends Thread {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ ScriptRunner a;
    private final /* synthetic */ String b;
    private final /* synthetic */ ScrollView c;

    bc(ScriptRunner scriptRunner, String str, ScrollView scrollView) {
        this.a = scriptRunner;
        this.b = str;
        this.c = scrollView;
    }

    public void run() {
        try {
            Process b2 = gd.b(this.a, this.b);
            DataInputStream dataInputStream = new DataInputStream(b2.getInputStream());
            String str = "";
            while (true) {
                String readLine = dataInputStream.readLine();
                if (readLine == null) {
                    break;
                }
                if (str.length() > 5000) {
                    str = "";
                }
                str = String.valueOf(str) + "\n" + readLine;
                this.a.b.post(new m(this, str, this.c));
            }
            if (b2.waitFor() != 0) {
                throw new Exception();
            }
            this.a.b.post(new l(this));
        } catch (Exception e) {
            this.a.b.post(new o(this));
        }
    }
}
