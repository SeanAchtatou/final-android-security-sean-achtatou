package com.koushikdutta.rommanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import com.koushikdutta.rommanager.a.b;
import java.util.ArrayList;
import java.util.Iterator;

class gc implements DialogInterface.OnClickListener {
    final /* synthetic */ at a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ ArrayList d;

    gc(at atVar, Activity activity, boolean z, ArrayList arrayList) {
        this.a = atVar;
        this.b = activity;
        this.c = z;
        this.d = arrayList;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        StringBuilder sb = new StringBuilder();
        b a2 = b.a(this.b);
        gd.a(this.b, a2);
        a2.a("Preparing to install ROM...", new String[0]);
        if (this.c) {
            a2.b("/sbin/touch", "/tmp/.installscript");
        }
        if (this.a.a[0]) {
            a2.a();
        }
        if (this.a.a[2]) {
            sb.append(String.format("%s/%s ; ", this.b.getFilesDir().getAbsolutePath(), "preparewipedalvikcache.sh"));
            a2.b("/cache/dowipedalvikcache.sh", new String[0]);
        }
        if (this.a.a[1]) {
            a2.c("/cache");
            a2.c("/data");
            a2.c("/sd-ext");
        }
        Iterator it = this.d.iterator();
        while (it.hasNext()) {
            a2.b((String) it.next());
        }
        a2.a(this.b, sb);
        gd.a(this.b, sb);
        AlertDialog.Builder builder = new AlertDialog.Builder(this.b);
        builder.setMessage((int) C0000R.string.confirm_reboot_and_install);
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new em(this, this.b, sb));
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.setTitle((int) C0000R.string.restart_and_install);
        builder.create().show();
    }
}
