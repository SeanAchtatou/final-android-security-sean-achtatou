package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SettingsScreen extends AnalyticsActivity {
    int e = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.SettingsScreen, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void a() {
        gd.a((Context) this, (int) C0000R.string.license_retrieved);
    }

    public void a(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(str);
        builder.setTitle((int) C0000R.string.license_retrieval_error);
        builder.setPositiveButton(17039370, new eh(this, str));
        builder.create().show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.a(java.lang.String, int):void
      com.koushikdutta.rommanager.h.a(java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void
      com.koushikdutta.rommanager.h.a(java.lang.String, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long */
    /* access modifiers changed from: package-private */
    public void c() {
        CheckBoxPreference checkBoxPreference = (CheckBoxPreference) findPreference("automatic_backup");
        if (!gd.a(this, (dw) null)) {
            this.c.a("backup_frequency", 0);
            this.c.a("next_backup", 0L);
            checkBoxPreference.setEnabled(false);
            checkBoxPreference.setChecked(false);
            checkBoxPreference.setSummary((int) C0000R.string.premium_only);
            gd.b(this);
            return;
        }
        int b = this.c.b("backup_frequency", 0);
        long b2 = this.c.b("next_backup", 0L);
        if (b == 0 || b2 == 0) {
            checkBoxPreference.setSummary((int) C0000R.string.automatic_backup_disabled);
            checkBoxPreference.setChecked(false);
            return;
        }
        checkBoxPreference.setChecked(true);
        checkBoxPreference.setSummary(getString(C0000R.string.automatic_backup_summary, new Object[]{SimpleDateFormat.getDateTimeInstance().format(new Date(gd.b(this)))}));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.a(java.lang.String, int):void
      com.koushikdutta.rommanager.h.a(java.lang.String, long):void
      com.koushikdutta.rommanager.h.a(java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z;
        super.onCreate(bundle);
        addPreferencesFromResource(C0000R.xml.settings);
        int componentEnabledSetting = getPackageManager().getComponentEnabledSetting(new ComponentName(this, UpdateReceiver.class));
        boolean z2 = componentEnabledSetting == 0 || componentEnabledSetting == 1;
        CheckBoxPreference checkBoxPreference = (CheckBoxPreference) findPreference("update_notifications");
        checkBoxPreference.setSummary(z2 ? C0000R.string.update_notifications_enabled : C0000R.string.update_notifications_disabled);
        checkBoxPreference.setChecked(z2);
        checkBoxPreference.setOnPreferenceClickListener(new ad(this, checkBoxPreference));
        if (!gd.a(this, (dw) null)) {
            checkBoxPreference.setEnabled(false);
            checkBoxPreference.setChecked(false);
            checkBoxPreference.setSummary((int) C0000R.string.premium_only);
        }
        CheckBoxPreference checkBoxPreference2 = (CheckBoxPreference) findPreference("automatic_backup");
        c();
        checkBoxPreference2.setOnPreferenceClickListener(new ac(this, checkBoxPreference2));
        CheckBoxPreference checkBoxPreference3 = (CheckBoxPreference) findPreference("showads");
        checkBoxPreference3.setChecked(gd.e(this));
        checkBoxPreference3.setOnPreferenceClickListener(new ab(this, checkBoxPreference3));
        if (!gd.d(this)) {
            checkBoxPreference3.setSummary((int) C0000R.string.premium_only);
            checkBoxPreference3.setEnabled(false);
        }
        CheckBoxPreference checkBoxPreference4 = (CheckBoxPreference) findPreference("erase_recovery");
        if (this.c.b("readonly_recovery", false)) {
            getPreferenceScreen().removePreference(checkBoxPreference4);
        }
        checkBoxPreference4.setSummary(this.c.b("erase_recovery", false) ? C0000R.string.erase_recovery_on : C0000R.string.erase_recovery_off);
        checkBoxPreference4.setChecked(this.c.b("erase_recovery", false));
        checkBoxPreference4.setOnPreferenceClickListener(new v(this, checkBoxPreference4));
        CheckBoxPreference checkBoxPreference5 = (CheckBoxPreference) findPreference("analytics");
        checkBoxPreference5.setSummary(this.c.b("analytics", true) ? C0000R.string.analytics_on : C0000R.string.analytics_off);
        checkBoxPreference5.setChecked(this.c.b("analytics", true));
        checkBoxPreference5.setOnPreferenceClickListener(new u(this, checkBoxPreference5));
        CheckBoxPreference checkBoxPreference6 = (CheckBoxPreference) findPreference("fast_nandroid");
        try {
            z = new File("/sdcard/clockworkmod/.hidenandroidprogress").exists();
        } catch (Exception e2) {
            e2.printStackTrace();
            z = false;
        }
        checkBoxPreference6.setSummary(z ? C0000R.string.fast_nandroid_on : C0000R.string.fast_nandroid_off);
        checkBoxPreference6.setChecked(z);
        checkBoxPreference6.setOnPreferenceClickListener(new t(this, checkBoxPreference6));
        CheckBoxPreference checkBoxPreference7 = (CheckBoxPreference) findPreference("developer_mode");
        checkBoxPreference7.setSummary(this.c.b("developer_mode", false) ? C0000R.string.developer_mode_on : C0000R.string.developer_mode_off);
        checkBoxPreference7.setChecked(this.c.b("developer_mode", false));
        checkBoxPreference7.setOnPreferenceClickListener(new s(this, checkBoxPreference7));
        if (!gd.a(this, (dw) null)) {
            this.c.a("developer_mode", false);
            checkBoxPreference7.setEnabled(false);
            checkBoxPreference7.setChecked(false);
            checkBoxPreference7.setSummary((int) C0000R.string.premium_only);
        }
        findPreference("credits").setOnPreferenceClickListener(new x(this));
        Preference findPreference = findPreference("device_id");
        findPreference.setSummary(gd.f(this));
        findPreference.setOnPreferenceClickListener(new w(this));
    }
}
