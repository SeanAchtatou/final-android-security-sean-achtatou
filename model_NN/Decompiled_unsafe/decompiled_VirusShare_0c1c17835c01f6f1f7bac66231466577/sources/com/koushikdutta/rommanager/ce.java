package com.koushikdutta.rommanager;

import java.io.File;
import java.util.Comparator;

class ce implements Comparator {
    final /* synthetic */ InstallRom a;

    ce(InstallRom installRom) {
        this.a = installRom;
    }

    /* renamed from: a */
    public int compare(File file, File file2) {
        return file.getName().compareToIgnoreCase(file2.getName());
    }
}
