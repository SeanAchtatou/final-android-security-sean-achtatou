package com.koushikdutta.rommanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemProperties;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;

public class RomManager extends ActivityBase implements dw, k {
    String h = null;
    boolean i = false;
    String[] j;
    String k;
    String l;
    boolean m = false;
    Handler n = new Handler();
    fs o;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    /* access modifiers changed from: private */
    public void b(String str) {
        if ("!donation".equals(str)) {
            gd.a((Context) this, (int) C0000R.string.inapp_error);
            return;
        }
        Intent intent = new Intent();
        intent.setData(Uri.parse("market://details?id=com.koushikdutta.rommanager.license"));
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void b(String str, String str2) {
        String a = this.c.a("web_connect_auth_token");
        if (a != null) {
            Log.i("RomNexus", a);
            Log.i("RomNexus", "getting cookie");
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(new URI("https://tickleservice.appspot.com/_ah/login?continue=" + URLEncoder.encode("https://tickleservice.appspot.com", "UTF-8") + "&auth=" + a));
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpClientParams.setRedirecting(basicHttpParams, false);
            httpGet.setParams(basicHttpParams);
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            Header[] headers = execute.getHeaders("Set-Cookie");
            if (execute.getStatusLine().getStatusCode() != 302 || headers.length == 0) {
                throw new Exception("failure getting cookie: " + execute.getStatusLine().getStatusCode() + " " + execute.getStatusLine().getReasonPhrase());
            }
            String str3 = null;
            for (Header header : headers) {
                if (header.getValue().indexOf("ACSID=") >= 0) {
                    str3 = header.getValue().split(";")[0];
                }
            }
            HttpPost httpPost = new HttpPost(new URI("https://tickleservice.appspot.com/register"));
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("applicationId", "ROM Manager"));
            arrayList.add(new BasicNameValuePair("clientId", gd.f(this)));
            arrayList.add(new BasicNameValuePair("registrationId", str));
            arrayList.add(new BasicNameValuePair("nickname", str2));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            httpPost.setHeader("Cookie", str3);
            this.c.a("Cookie", str3);
            httpPost.setHeader("X-Same-Domain", "1");
            Log.i("RomNexus", "Status code from register: " + defaultHttpClient.execute(httpPost).getStatusLine().getStatusCode());
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);
        if (!((this.m || "realdonation".equals(str)) && bindService(new Intent("com.android.vending.billing.MarketBillingService.BIND"), new eu(this, progressDialog, str), 1))) {
            b(str);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    /* access modifiers changed from: private */
    public void m() {
        String a = this.c.a("registration_id");
        if (a == null) {
            gd.a((Context) this, (int) C0000R.string.web_connect_error);
            return;
        }
        Log.i("RomNexus", "registration found");
        String[] a2 = p.a(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(a2, new fh(this, a2, a));
        builder.setCancelable(true);
        builder.create().show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.RomManager, int, com.koushikdutta.rommanager.es]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    /* access modifiers changed from: private */
    public void n() {
        gd.a(String.format("%s?operation=purchaseRequest&deviceId=%s&seller=koushik_dutta@yahoo.com&productId=ROM%%20Manager", gd.b, gd.f(this)), (Activity) this, false, (cq) new es(this));
    }

    public Vector a(ZipFile zipFile) {
        Vector vector = new Vector();
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) entries.nextElement();
            if (zipEntry.getName().startsWith("assets")) {
                vector.add(zipEntry);
            }
        }
        return vector;
    }

    public void a() {
        if (!this.i) {
            a(gd.c());
        }
    }

    public void a(fs fsVar) {
        this.o = fsVar;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005c A[SYNTHETIC, Splitter:B:27:0x005c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.io.InputStream r7, java.io.FileOutputStream r8) {
        /*
            r6 = this;
            r0 = 0
            r1 = 5192(0x1448, float:7.276E-42)
            byte[] r1 = new byte[r1]     // Catch:{ IOException -> 0x0091, all -> 0x0056 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x0091, all -> 0x0056 }
            r3 = 5192(0x1448, float:7.276E-42)
            r2.<init>(r8, r3)     // Catch:{ IOException -> 0x0091, all -> 0x0056 }
        L_0x000c:
            r0 = 0
            r3 = 5192(0x1448, float:7.276E-42)
            int r0 = r7.read(r1, r0, r3)     // Catch:{ IOException -> 0x0024, all -> 0x008c }
            r3 = -1
            if (r0 != r3) goto L_0x001f
            r2.flush()     // Catch:{ IOException -> 0x0024, all -> 0x008c }
            if (r2 == 0) goto L_0x001e
            r2.close()     // Catch:{ IOException -> 0x0076 }
        L_0x001e:
            return
        L_0x001f:
            r3 = 0
            r2.write(r1, r3, r0)     // Catch:{ IOException -> 0x0024, all -> 0x008c }
            goto L_0x000c
        L_0x0024:
            r0 = move-exception
            r1 = r2
        L_0x0026:
            java.lang.String r2 = "RomNexus"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x008f }
            java.lang.String r4 = "Exception while copying: "
            r3.<init>(r4)     // Catch:{ all -> 0x008f }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x008f }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008f }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x008f }
            if (r1 == 0) goto L_0x001e
            r1.close()     // Catch:{ IOException -> 0x0040 }
            goto L_0x001e
        L_0x0040:
            r0 = move-exception
            java.lang.String r1 = "RomNexus"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exception while closing the stream: "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r1, r0)
            goto L_0x001e
        L_0x0056:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x005a:
            if (r1 == 0) goto L_0x005f
            r1.close()     // Catch:{ IOException -> 0x0060 }
        L_0x005f:
            throw r0
        L_0x0060:
            r1 = move-exception
            java.lang.String r2 = "RomNexus"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Exception while closing the stream: "
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r2, r1)
            goto L_0x005f
        L_0x0076:
            r0 = move-exception
            java.lang.String r1 = "RomNexus"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Exception while closing the stream: "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r1, r0)
            goto L_0x001e
        L_0x008c:
            r0 = move-exception
            r1 = r2
            goto L_0x005a
        L_0x008f:
            r0 = move-exception
            goto L_0x005a
        L_0x0091:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.koushikdutta.rommanager.RomManager.a(java.io.InputStream, java.io.FileOutputStream):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.RomManager, int, com.koushikdutta.rommanager.gt]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        try {
            this.e.setEnabled(false);
            gd.a(str2, (Activity) this, true, (cq) new gt(this, str));
        } catch (Exception e) {
            this.e.setEnabled(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.RomManager, int, com.koushikdutta.rommanager.gu]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    /* access modifiers changed from: package-private */
    public void a(String str, String str2, boolean z) {
        try {
            this.e.setEnabled(false);
            gd.a(str2, (Activity) this, true, (cq) new gu(this, z, str));
        } catch (Exception e) {
            this.e.setEnabled(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.n.post(new fd(this, z));
    }

    public void b() {
        runOnUiThread(new gs(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        boolean b = this.c.b("is_clockworkmod", false);
        String a = this.c.a("detected_device");
        if (a != null) {
            new go(this, a, z, b, this.c.a("current_recovery_version")).start();
        }
    }

    public int c() {
        return C0000R.layout.list_item_small;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.RomManager, int, com.koushikdutta.rommanager.gw]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    /* access modifiers changed from: package-private */
    public void c(boolean z) {
        gd.a("http://gh-pages.clockworkmod.com/ROMManagerManifest/devices.js", (Activity) this, false, (cq) new gw(this, z));
    }

    /* access modifiers changed from: package-private */
    public void d() {
        String packageCodePath = getPackageCodePath();
        String file = getFilesDir().toString();
        try {
            long lastModified = new File(packageCodePath).lastModified();
            ZipFile zipFile = new ZipFile(packageCodePath);
            Vector a = a(zipFile);
            int length = "assets".length();
            Enumeration elements = a.elements();
            while (elements.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) elements.nextElement();
                File file2 = new File(file, zipEntry.getName().substring(length));
                file2.getParentFile().mkdirs();
                if (!file2.exists() || zipEntry.getSize() != file2.length() || lastModified >= file2.lastModified()) {
                    a(zipFile.getInputStream(zipEntry), new FileOutputStream(file2));
                    Log.e("RomNexus", "Extracted: " + file2.getAbsolutePath());
                    Runtime.getRuntime().exec("chmod 755 " + file2.getAbsolutePath());
                }
            }
        } catch (IOException e) {
            Log.e("RomNexus", "Error: " + e.getMessage());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void e() {
        File file = new File("/sdcard/clockworkmod/recovery.log");
        File file2 = new File("/sdcard/clockworkmod/.recoverycheckpoint");
        if (file.exists()) {
            File file3 = new File("/sdcard/clockworkmod/report.log");
            file3.delete();
            file.renameTo(file3);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) C0000R.string.report_error);
            builder.setMessage((int) C0000R.string.report_error_message);
            builder.setCancelable(false);
            builder.setPositiveButton(17039379, new fq(this, file3));
            builder.setNegativeButton(17039369, new fp(this));
            builder.create().show();
        } else if (!file2.exists() || !this.c.b("is_clockworkmod", false)) {
            gd.a((Activity) this);
        } else {
            file2.delete();
            AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
            builder2.setTitle((int) C0000R.string.recovery_failure);
            builder2.setCancelable(false);
            builder2.setMessage((int) C0000R.string.recovery_try_fix);
            builder2.setPositiveButton(17039370, new fv(this));
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (!"Welcome to ROM Manager 4.0!\n\nThe newest features brings ROM Ratings, ROM Share, and comments for Premium users!\n\nWe'd love to hear your feature requests! Hit us up on twitter or email!\n\nYou can now block annoying users in the ROM comments. :)".equals(this.c.a("whats_new"))) {
            this.c.a("whats_new", "Welcome to ROM Manager 4.0!\n\nThe newest features brings ROM Ratings, ROM Share, and comments for Premium users!\n\nWe'd love to hear your feature requests! Hit us up on twitter or email!\n\nYou can now block annoying users in the ROM comments. :)");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) C0000R.string.new_features);
            builder.setMessage("Welcome to ROM Manager 4.0!\n\nThe newest features brings ROM Ratings, ROM Share, and comments for Premium users!\n\nWe'd love to hear your feature requests! Hit us up on twitter or email!\n\nYou can now block annoying users in the ROM comments. :)");
            builder.setCancelable(false);
            PackageManager packageManager = getPackageManager();
            builder.setPositiveButton((int) C0000R.string.rate, new fu(this));
            builder.setNegativeButton(17039370, new gp(this));
            try {
                packageManager.getPackageInfo("com.twitter.android", 0);
                builder.setNeutralButton((int) C0000R.string.follow_koush, new gm(this));
            } catch (Exception e) {
                e.printStackTrace();
            }
            builder.create().show();
            return;
        }
        e();
    }

    /* access modifiers changed from: package-private */
    public void g() {
        if (new File("/system/bin/su").exists() || new File("/system/xbin/su").exists()) {
            f();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) C0000R.string.no_su);
        builder.setCancelable(false);
        builder.setPositiveButton(17039370, new gl(this));
        builder.create().show();
    }

    /* access modifiers changed from: package-private */
    public void h() {
        String a = this.c.a("current_recovery_version");
        ck b = b((int) C0000R.string.flash_recovery);
        if (a == null) {
            b.a((int) C0000R.string.do_this_first);
            return;
        }
        String format = String.format(this.c.b("is_clockworkmod", false) ? getString(C0000R.string.recovery_clockworkmod_yours) : getString(C0000R.string.recovery_alternate_yours), a);
        b.a(format);
        if (this.h != null) {
            b.a(String.valueOf(format) + "\n" + String.format(getString(C0000R.string.recovery_latest), this.h));
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        ck b = b((int) C0000R.string.flash_alternate);
        String a = this.c.a("alternate_recovery_name");
        if (this.c.a("alternate_recovery_url") == null || a == null) {
            b.a(false);
        } else {
            b.a(a);
        }
        ck b2 = b((int) C0000R.string.check_updates);
        if (!gd.a()) {
            b2.a((int) C0000R.string.ota_not_supported);
            return;
        }
        String str = SystemProperties.get("ro.modversion");
        b2.a(String.format(getString(C0000R.string.current_rom), str));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean j() {
        String a = this.c.a("current_recovery_version");
        String a2 = this.c.a("detected_device");
        if (this.c.b("is_clockworkmod", false) && a != null && a2 != null) {
            return true;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) C0000R.string.flash_recovery);
        builder.setMessage((int) C0000R.string.flash_recovery_require);
        builder.setCancelable(false);
        builder.setPositiveButton((int) C0000R.string.install, new gn(this));
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.create().show();
        return false;
    }

    public void k() {
        this.c.a("recovery_timestamp", System.currentTimeMillis());
        gd.a(this.c);
    }

    /* access modifiers changed from: package-private */
    public void l() {
        c(true);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        i();
        if (i2 == 1000 && i3 == -1) {
            String stringExtra = intent.getStringExtra("SCAN_RESULT");
            RomPackage romPackage = new RomPackage();
            romPackage.d = 0;
            romPackage.a = getString(C0000R.string.qrcode_download);
            RomPart romPart = new RomPart();
            romPart.c.add(stringExtra);
            romPart.a = romPackage.a;
            romPackage.b = new RomPart[]{romPart};
            Intent intent2 = new Intent(this, DownloadService.class);
            intent2.putExtra("rompackage", romPackage);
            startService(intent2);
        } else if (i2 == 1001 && i3 == -1) {
            gd.b();
            a();
            gd.b(this, this);
            gd.a(this, (int) C0000R.string.app_name_premium, (int) C0000R.string.premium_activated);
        }
        if (this.o != null) {
            this.o.a(new bq(Integer.valueOf(i2), intent));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.a(java.lang.String, int):void
      com.koushikdutta.rommanager.h.a(java.lang.String, long):void
      com.koushikdutta.rommanager.h.a(java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.a(java.lang.String, int):void
      com.koushikdutta.rommanager.h.a(java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void
      com.koushikdutta.rommanager.h.a(java.lang.String, long):void */
    public void onCreate(Bundle bundle) {
        new Thread(new eo(this)).start();
        requestWindowFeature(2);
        requestWindowFeature(5);
        super.onCreate(bundle);
        gd.b(this);
        gd.a(this.c);
        new ep(this).start();
        boolean a = gd.a(this, this);
        a(a);
        a((int) C0000R.string.recovery, new eq(this, this, C0000R.string.flash_recovery, C0000R.string.do_this_first, C0000R.drawable.flash_cwmr));
        a((int) C0000R.string.recovery, new et(this, this, C0000R.string.reboot_recovery, C0000R.string.reboot_into_recovery, C0000R.drawable.reboot_rec));
        a((int) C0000R.string.rom_management, new er(this, this, C0000R.string.install_rom, 0, C0000R.drawable.install_sd));
        a((int) C0000R.string.rom_management, new ex(this, this, C0000R.string.download_rom, 0, C0000R.drawable.download_rom));
        a((int) C0000R.string.backup_and_restore, new ev(this, this, C0000R.string.manage_and_restore_backups, 0, C0000R.drawable.manage_restore));
        a((int) C0000R.string.backup_and_restore, new fb(this, this, C0000R.string.backup_current_rom, 0, C0000R.drawable.rom_backup));
        a((int) C0000R.string.rom_management, new fa(this, this, C0000R.string.check_updates, 0, C0000R.drawable.rom_update));
        a((int) C0000R.string.utilities, new fc(this, this, C0000R.string.fix_permissions, C0000R.string.fix_permissions_detail, C0000R.drawable.fix_perm));
        a((int) C0000R.string.more_recoveries, new fn(this, this, C0000R.string.flash_alternate, C0000R.string.flash_alternate_unavailable, C0000R.drawable.ra_rec));
        a((int) C0000R.string.utilities, new fo(this, this, C0000R.string.partition_sdcard, C0000R.string.partition_sdcard_summary, C0000R.drawable.sd_part));
        a((int) C0000R.string.more_recoveries, new fl(this, this, C0000R.string.flash_legacy_and_experimental, C0000R.string.flash_legacy_and_experimental_summary, C0000R.drawable.older_cwmr));
        a((int) C0000R.string.rom_management, new fm(this, this, C0000R.string.install_from_qrcode, C0000R.string.qrcode_summary, C0000R.drawable.qr_scan));
        b(true);
        h();
        i();
        if (this.c.b("is_first_run", true)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage((int) C0000R.string.brick_warning);
            builder.setCancelable(false);
            builder.setNegativeButton(17039360, new ft(this));
            builder.setPositiveButton(17039370, new fr(this));
            builder.create().show();
        } else {
            g();
        }
        String a2 = this.c.a("detected_device");
        if (a2 == null) {
            a2 = "none";
        }
        String str = SystemProperties.get("ro.modversion");
        if (str == null) {
            str = "Unknown";
        }
        String str2 = SystemProperties.get("ro.rommanager.developerid");
        if (str2 == null) {
            str2 = "Unknown";
        }
        a("Device", a2, a ? "Premium" : "Free");
        a("ROM", str, a ? "Premium" : "Free");
        a("ROMDeveloper", str2, a ? "Premium" : "Free");
        a("Developer", str2, str);
        if (!a && Integer.parseInt(Build.VERSION.SDK) > 4) {
            this.c.a("developer_mode", false);
            this.c.a("backup_frequency", 0);
            this.c.a("next_backup", 0L);
            gd.b(this, this);
        }
        gd.g(this);
        Log.i("RomNexus", "Safe Device ID: " + gd.f(this));
        PackageManager packageManager = getPackageManager();
        try {
            if (packageManager.getPackageInfo("com.koushikdutta.rommanager.license", 0) != null) {
                String installerPackageName = packageManager.getInstallerPackageName("com.koushikdutta.rommanager.license");
                if (installerPackageName == null) {
                    installerPackageName = "none";
                }
                a("License", installerPackageName, (String) null);
            }
        } catch (Exception e) {
        }
        Log.i("RomNexus", "onCreate finished");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(C0000R.menu.rommanagermenu, menu);
        menu.findItem(C0000R.id.web_connect).setOnMenuItemClickListener(new fg(this));
        menu.findItem(C0000R.id.flash_override).setOnMenuItemClickListener(new ff(this));
        menu.findItem(C0000R.id.clear_cache).setOnMenuItemClickListener(new fe(this));
        menu.findItem(C0000R.id.settings).setOnMenuItemClickListener(new ey(this));
        MenuItem findItem = menu.findItem(C0000R.id.donate);
        if (!gd.a(this, (dw) null)) {
            findItem.setTitle((int) C0000R.string.buy_premium);
        }
        findItem.setOnMenuItemClickListener(new ew(this));
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.app.Activity, java.util.ArrayList, boolean):void
     arg types: [com.koushikdutta.rommanager.RomManager, java.util.ArrayList, int]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder, boolean):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, boolean):boolean
      com.koushikdutta.rommanager.gd.a(android.app.Activity, java.util.ArrayList, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Intent intent = getIntent();
        Uri data = intent.getData();
        if (data != null) {
            intent.setData(null);
            ArrayList arrayList = new ArrayList();
            arrayList.add(data.getPath());
            gd.a((Activity) this, arrayList, false);
        }
        if (intent.hasExtra("rompackage")) {
            RomPackage romPackage = (RomPackage) intent.getParcelableExtra("rompackage");
            ArrayList arrayList2 = new ArrayList(romPackage.c);
            arrayList2.addAll(romPackage.e);
            gd.a(this, arrayList2, romPackage.d != 0);
            intent.removeExtra("rompackage");
        } else if (intent.hasExtra("noupdate")) {
            intent.removeExtra("noupdate");
            gd.a((Context) this, (int) C0000R.string.no_update);
        } else if (intent.getBooleanExtra("cancel_download_prompt", false)) {
            intent.removeExtra("cancel_download_prompt");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) C0000R.string.download_rom);
            builder.setMessage((int) C0000R.string.download_in_progress);
            builder.setPositiveButton((int) C0000R.string.yes, new gx(this));
            builder.setNegativeButton((int) C0000R.string.no, (DialogInterface.OnClickListener) null);
            builder.create().show();
        }
    }
}
