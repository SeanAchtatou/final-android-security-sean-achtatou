package com.koushikdutta.rommanager;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.koushikdutta.a.a;
import java.text.DateFormat;
import java.util.Date;
import org.json.JSONObject;

class y extends dk {
    final /* synthetic */ DeveloperList a;
    private final /* synthetic */ String n;
    private final /* synthetic */ boolean o;
    private final /* synthetic */ String p;
    private final /* synthetic */ String q;
    private final /* synthetic */ String r;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    y(DeveloperList developerList, ActivityBase activityBase, String str, String str2, int i, String str3, String str4, boolean z, String str5, String str6) {
        super(activityBase, str, str2);
        this.a = developerList;
        this.q = str3;
        this.n = str4;
        this.o = z;
        this.p = str5;
        this.r = str6;
        this.b = i;
        this.h = str3;
    }

    public View a(Context context, View view) {
        View a2 = super.a(context, view);
        ImageView imageView = (ImageView) a2.findViewById(C0000R.id.image);
        imageView.setVisibility(0);
        a.a(imageView, this.r, C0000R.drawable.no_icon);
        if (this.a.k != null) {
            JSONObject optJSONObject = this.a.k.optJSONObject(this.q);
            RatingBar ratingBar = (RatingBar) a2.findViewById(C0000R.id.rating);
            TextView textView = (TextView) a2.findViewById(C0000R.id.downloads);
            TextView textView2 = (TextView) a2.findViewById(C0000R.id.date);
            if (optJSONObject != null) {
                ratingBar.setRating(this.i);
                DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(this.a);
                Date date = new Date(this.j);
                textView.setText(this.a.getString(C0000R.string.downloads, new Object[]{Integer.valueOf(this.k)}));
                textView2.setText(dateFormat.format(date));
            } else {
                ratingBar.setRating(0.0f);
                textView.setText((CharSequence) null);
                textView2.setText((CharSequence) null);
            }
        }
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.DeveloperList, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void a() {
        try {
            Intent intent = new Intent(this.a, RomList.class);
            intent.putExtra("manifest_url", this.n);
            intent.putExtra("free", this.o);
            intent.putExtra("name", this.p);
            intent.putExtra("id", this.q);
            this.a.startActivity(intent);
        } catch (Exception e) {
            gd.a((Context) this.a, (int) C0000R.string.manifests_error);
            Log.e("DeveloperList", e.getLocalizedMessage(), e);
        }
    }
}
