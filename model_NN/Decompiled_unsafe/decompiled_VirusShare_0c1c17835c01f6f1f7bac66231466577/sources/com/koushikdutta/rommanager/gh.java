package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;

class gh implements DialogInterface.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ dz a;
    private final /* synthetic */ Runnable b;

    gh(dz dzVar, Runnable runnable) {
        this.a = dzVar;
        this.b = runnable;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.b.a);
        builder.setMessage((int) C0000R.string.backup_current_rom_question);
        builder.setPositiveButton((int) C0000R.string.yes, new cm(this, this.b));
        builder.setNegativeButton((int) C0000R.string.no, new cn(this, this.b));
        builder.create().show();
    }
}
