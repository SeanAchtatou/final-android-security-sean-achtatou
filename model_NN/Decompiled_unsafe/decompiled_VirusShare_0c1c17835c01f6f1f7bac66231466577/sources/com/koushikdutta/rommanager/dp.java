package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.content.Intent;

class dp implements DialogInterface.OnClickListener {
    final /* synthetic */ RomList a;
    private final /* synthetic */ RomPackage b;

    dp(RomList romList, RomPackage romPackage) {
        this.a = romList;
        this.b = romPackage;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this.a, DownloadService.class);
        intent.putExtra("rompackage", this.b);
        this.a.startService(intent);
        this.a.startActivity(new Intent(this.a, Downloading.class));
    }
}
