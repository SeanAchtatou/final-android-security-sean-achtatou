package com.koushikdutta.rommanager;

import android.app.Activity;
import android.os.Handler;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;

class az extends Thread {
    private final /* synthetic */ String[] a;
    private final /* synthetic */ Handler b;
    private final /* synthetic */ String c;
    private final /* synthetic */ boolean d;
    private final /* synthetic */ boolean e;
    private final /* synthetic */ Activity f;
    private final /* synthetic */ cq g;

    az(String[] strArr, Handler handler, String str, boolean z, boolean z2, Activity activity, cq cqVar) {
        this.a = strArr;
        this.b = handler;
        this.c = str;
        this.d = z;
        this.e = z2;
        this.f = activity;
        this.g = cqVar;
    }

    public void run() {
        int length = 100 / this.a.length;
        for (int i = 0; i < this.a.length; i++) {
            int i2 = length * i;
            int i3 = i2 + length;
            try {
                URL url = new URL(this.a[i]);
                File file = this.c != null ? new File(this.c) : !this.d ? new File("/sdcard/clockworkmod/download/" + url.hashCode()) : gd.a(url);
                if (!file.exists() || !this.d) {
                    gd.a(file);
                    this.b.post(new db(this, this.f, i2));
                    URLConnection openConnection = url.openConnection();
                    openConnection.setRequestProperty("Cache-Control", "max-age=30");
                    if (this.e) {
                        openConnection.setRequestProperty("registration_id", h.a(this.f).a("registration_id"));
                    }
                    DataInputStream dataInputStream = new DataInputStream(gd.a(openConnection));
                    int contentLength = openConnection.getContentLength();
                    this.b.post(new de(this, this.f, contentLength));
                    File file2 = new File(String.valueOf(file.toString()) + ".tmp");
                    file.getParentFile().mkdirs();
                    FileOutputStream fileOutputStream = new FileOutputStream(file2);
                    byte[] bArr = new byte[200000];
                    int i4 = 0;
                    int i5 = 0;
                    while (true) {
                        int read = dataInputStream.read(bArr);
                        if (read == -1) {
                            fileOutputStream.close();
                            dataInputStream.close();
                            file2.renameTo(file);
                            break;
                        }
                        i4 += read;
                        int i6 = (int) ((((((double) i4) / ((double) contentLength)) * ((double) (i3 - i2))) + ((double) i2)) * 100.0d);
                        if (contentLength != -1 && i4 > 50000 + i5) {
                            if (this.g == null || !this.g.a()) {
                                this.b.post(new dd(this, this.f, i6));
                                i5 = i4;
                            } else {
                                return;
                            }
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                }
                this.b.post(new da(this, this.g, file));
                this.b.post(new dc(this, this.f));
            } catch (Exception e2) {
                e2.printStackTrace();
                this.b.post(new cz(this, this.g));
            } finally {
                this.b.post(new dc(this, this.f));
            }
        }
    }
}
