package com.koushikdutta.rommanager;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import java.util.ArrayList;

class aq implements DialogInterface.OnClickListener {
    final /* synthetic */ ch a;
    private final /* synthetic */ RomPackage b;
    private final /* synthetic */ ArrayList c;

    aq(ch chVar, RomPackage romPackage, ArrayList arrayList) {
        this.a = chVar;
        this.b = romPackage;
        this.c = arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.app.Activity, java.util.ArrayList, boolean):void
     arg types: [com.koushikdutta.rommanager.InstallRom, java.util.ArrayList, int]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder, boolean):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, int, boolean):boolean
      com.koushikdutta.rommanager.gd.a(android.app.Activity, java.util.ArrayList, boolean):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        if (this.b != null) {
            this.b.e = this.c;
            Intent intent = new Intent(this.a.a, DownloadService.class);
            intent.putExtra("rompackage", this.b);
            this.a.a.startService(intent);
            this.a.a.startActivity(new Intent(this.a.a, Downloading.class));
            return;
        }
        gd.a((Activity) this.a.a, this.c, false);
        this.a.a.a("Install", "SD Card", "SD Card");
    }
}
