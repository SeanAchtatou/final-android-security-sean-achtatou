package com.koushikdutta.rommanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import java.util.Comparator;
import java.util.GregorianCalendar;
import org.json.JSONArray;
import org.json.JSONObject;

public class DeveloperList extends ActivityBase {
    boolean h = false;
    h i;
    String j;
    JSONObject k;
    Comparator l = null;

    /* access modifiers changed from: package-private */
    public void a(cl clVar) {
        if (this.k != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= clVar.getCount()) {
                    break;
                }
                dk dkVar = (dk) clVar.getItem(i3);
                JSONObject optJSONObject = this.k.optJSONObject(dkVar.h);
                if (optJSONObject != null) {
                    int optInt = optJSONObject.optInt("totalRating");
                    int optInt2 = optJSONObject.optInt("ratingCount");
                    if (optInt2 != 0) {
                        dkVar.i = ((float) optInt) / ((float) optInt2);
                    }
                    dkVar.l = optJSONObject.optInt("downloadCount");
                    dkVar.m = optJSONObject.optInt("anonymousDownloadCount");
                    dkVar.k = dkVar.l + dkVar.m;
                    dkVar.j = optJSONObject.optLong("lastModified") * 1000;
                    if (dkVar.j == 0) {
                        dkVar.j = new GregorianCalendar(2011, 1, 1).getTimeInMillis();
                    }
                }
                i2 = i3 + 1;
            }
            if (this.l != null) {
                clVar.sort(this.l);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(cl clVar) {
        if (this.l != null) {
            clVar.sort(this.l);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
     arg types: [com.koushikdutta.rommanager.DeveloperList, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.DeveloperList, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    /* access modifiers changed from: package-private */
    public void b(String str) {
        try {
            a((int) C0000R.string.premium_header);
            a((int) C0000R.string.free_header);
            JSONObject jSONObject = new JSONObject(bg.a(str));
            if (jSONObject.getInt("minversion") > getPackageManager().getPackageInfo(getPackageName(), 0).versionCode) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage((int) C0000R.string.upgrade);
                builder.setPositiveButton(17039370, new aa(this));
                builder.create().show();
                return;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("manifests");
            int i2 = 0;
            int i3 = 0;
            while (i3 < jSONArray.length()) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i3);
                String string = jSONObject2.getString("manifest");
                int i4 = i2 + 1;
                String string2 = jSONObject2.getString("developer");
                String optString = jSONObject2.optString("summary");
                boolean optBoolean = jSONObject2.optBoolean("free");
                a(optBoolean ? C0000R.string.free_header : C0000R.string.premium_header, new y(this, this, string2, optString, i3, jSONObject2.getString("id"), string, optBoolean, string2, jSONObject2.optString("icon")));
                i3++;
                i2 = i4;
            }
            if (i2 == 0) {
                gd.a((Activity) this, (int) C0000R.string.manifests_no_roms);
            }
        } catch (Exception e) {
            gd.a((Context) this, (int) C0000R.string.roms_error);
            Log.e("DeveloperList", e.getLocalizedMessage(), e);
        }
    }

    public int c() {
        return C0000R.layout.developer_list_item;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        String a = h.a(this).a("developer_sort");
        if ("rating".equals(a)) {
            this.l = new bm(this);
        } else if ("date".equals(a)) {
            this.l = new bn(this);
        } else if ("downloads".equals(a)) {
            this.l = new ca(this);
        } else {
            this.l = new bz(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        a(a((int) C0000R.string.premium_header));
        a(a((int) C0000R.string.free_header));
    }

    /* access modifiers changed from: package-private */
    public void f() {
        b(a((int) C0000R.string.premium_header));
        b(a((int) C0000R.string.free_header));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.DeveloperList, int, com.koushikdutta.rommanager.bo]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.DeveloperList, int, com.koushikdutta.rommanager.bp]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(2);
        requestWindowFeature(5);
        super.onCreate(bundle);
        this.i = h.a(this);
        this.j = this.i.a("detected_device");
        gd.a(gd.a((Context) this), (Activity) this, false, (cq) new bo(this));
        gd.a("http://rommanager.deployfu.com/v2/ratings", (Activity) this, false, (cq) new bp(this));
        d();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add((int) C0000R.string.sort_by_date).setOnMenuItemClickListener(new cd(this));
        menu.add((int) C0000R.string.sort_by_rating).setOnMenuItemClickListener(new cc(this));
        menu.add((int) C0000R.string.sort_by_downloads).setOnMenuItemClickListener(new cg(this));
        menu.add((int) C0000R.string.no_sort).setOnMenuItemClickListener(new z(this));
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.h = true;
        super.onDestroy();
    }
}
