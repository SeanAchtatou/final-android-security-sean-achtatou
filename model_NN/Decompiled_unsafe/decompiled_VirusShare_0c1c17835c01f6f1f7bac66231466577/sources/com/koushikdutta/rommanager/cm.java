package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import java.text.SimpleDateFormat;
import java.util.Date;

class cm implements DialogInterface.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ gh a;
    private final /* synthetic */ Runnable b;

    cm(gh ghVar, Runnable runnable) {
        this.a = ghVar;
        this.b = runnable;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.a.b.a);
        View inflate = View.inflate(this.a.a.b.a, C0000R.layout.romname, null);
        EditText editText = (EditText) inflate.findViewById(C0000R.id.romname);
        editText.setText(new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss").format(new Date()));
        editText.selectAll();
        builder.setIcon(0);
        builder.setTitle((int) C0000R.string.edit_backup_name);
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new be(this, editText, this.b));
        builder.setView(inflate);
        builder.create().show();
    }
}
