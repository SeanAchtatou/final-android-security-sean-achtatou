package com.koushikdutta.rommanager;

import android.app.Activity;

class db implements Runnable {
    final /* synthetic */ az a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ int c;

    db(az azVar, Activity activity, int i) {
        this.a = azVar;
        this.b = activity;
        this.c = i;
    }

    public void run() {
        if (this.b != null) {
            this.b.setProgressBarVisibility(true);
            this.b.setProgress(this.c);
        }
    }
}
