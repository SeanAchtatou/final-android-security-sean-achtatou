package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.preference.CheckBoxPreference;

class cx implements DialogInterface.OnClickListener {
    final /* synthetic */ u a;
    private final /* synthetic */ CheckBoxPreference b;

    cx(u uVar, CheckBoxPreference checkBoxPreference) {
        this.a = uVar;
        this.b = checkBoxPreference;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.b.setChecked(false);
        this.b.setSummary((int) C0000R.string.analytics_off);
    }
}
