package com.koushikdutta.rommanager;

import android.app.ProgressDialog;

class af extends Thread {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ ge a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;
    private final /* synthetic */ ProgressDialog d;

    af(ge geVar, String str, String str2, ProgressDialog progressDialog) {
        this.a = geVar;
        this.b = str;
        this.c = str2;
        this.d = progressDialog;
    }

    public void run() {
        try {
            this.a.a.a.a.b(this.b, this.c);
            this.a.a.a.a.runOnUiThread(new cj(this, this.d));
        } catch (Exception e) {
            e.printStackTrace();
            this.a.a.a.a.runOnUiThread(new ci(this, this.d));
        }
    }
}
