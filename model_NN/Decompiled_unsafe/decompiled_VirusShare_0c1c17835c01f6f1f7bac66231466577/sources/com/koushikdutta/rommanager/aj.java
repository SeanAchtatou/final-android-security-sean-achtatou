package com.koushikdutta.rommanager;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import org.json.JSONObject;

class aj implements View.OnClickListener {
    final /* synthetic */ bv a;
    private final /* synthetic */ JSONObject b;
    private final /* synthetic */ JSONObject c;
    private final /* synthetic */ String d;

    aj(bv bvVar, JSONObject jSONObject, JSONObject jSONObject2, String str) {
        this.a = bvVar;
        this.b = jSONObject;
        this.c = jSONObject2;
        this.d = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomList, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void onClick(View view) {
        if (this.a.b.getIntent().getBooleanExtra("free", false) || gd.a(this.a.b, (dw) null)) {
            try {
                this.a.b.a(this.b, this.c);
            } catch (Exception e) {
                gd.a((Context) this.a.b, (int) C0000R.string.rom_error);
                Log.e("Romlist", e.getLocalizedMessage(), e);
            }
            this.a.b.a("Install", this.a.b.getTitle().toString(), this.d);
            return;
        }
        Toast.makeText(this.a.b, (int) C0000R.string.premium_required_to_download, 1).show();
    }
}
