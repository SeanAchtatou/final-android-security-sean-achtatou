package com.koushikdutta.rommanager;

import android.os.Parcel;
import android.os.Parcelable;

class bw implements Parcelable.Creator {
    bw() {
    }

    /* renamed from: a */
    public RomPackage createFromParcel(Parcel parcel) {
        return new RomPackage(parcel, null);
    }

    /* renamed from: a */
    public RomPackage[] newArray(int i) {
        return new RomPackage[i];
    }
}
