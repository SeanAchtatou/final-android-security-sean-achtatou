package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;

class fc extends ck {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    fc(RomManager romManager, ActivityBase activityBase, int i, int i2, int i3) {
        super(activityBase, i, i2, i3);
        this.a = romManager;
    }

    public void a() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        builder.setMessage((int) C0000R.string.confirm_fix_permissions);
        builder.setNegativeButton((int) C0000R.string.no, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton((int) C0000R.string.yes, new e(this));
        builder.create().show();
    }
}
