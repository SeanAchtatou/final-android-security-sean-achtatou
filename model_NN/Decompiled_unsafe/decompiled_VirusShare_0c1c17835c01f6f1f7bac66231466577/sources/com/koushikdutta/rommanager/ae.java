package com.koushikdutta.rommanager;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.widget.EditText;

class ae implements DialogInterface.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ fh a;
    private final /* synthetic */ EditText b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;

    ae(fh fhVar, EditText editText, String str, String str2) {
        this.a = fhVar;
        this.b = editText;
        this.c = str;
        this.d = str2;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        String editable = !gd.b(this.b.getText().toString()) ? this.b.getText().toString() : this.a.a.getString(C0000R.string.anonymous);
        this.a.a.c.a("nickname", editable);
        ProgressDialog progressDialog = new ProgressDialog(this.a.a);
        p.a(this.a.a, this.c, new ge(this, progressDialog, this.d, editable), this.a.a);
        progressDialog.show();
    }
}
