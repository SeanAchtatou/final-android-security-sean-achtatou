package com.koushikdutta.rommanager;

import android.content.Context;
import android.content.DialogInterface;
import java.io.File;

class gk implements DialogInterface.OnClickListener {
    final /* synthetic */ dz a;
    private final /* synthetic */ File b;

    gk(dz dzVar, File file) {
        this.a = dzVar;
        this.b = file;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.ManageBackups, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        if (!gd.a(this.b)) {
            gd.a((Context) this.a.b.a, (int) C0000R.string.remove_backup_error);
        } else {
            this.a.b.a.getPreferenceScreen().removePreference(this.a.b);
        }
    }
}
