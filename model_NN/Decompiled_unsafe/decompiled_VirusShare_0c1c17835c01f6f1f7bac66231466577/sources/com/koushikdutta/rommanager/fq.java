package com.koushikdutta.rommanager;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;

class fq implements DialogInterface.OnClickListener {
    final /* synthetic */ RomManager a;
    private final /* synthetic */ File b;

    fq(RomManager romManager, File file) {
        this.a = romManager;
        this.b = file;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(this.b));
            byte[] bArr = new byte[dataInputStream.available()];
            dataInputStream.readFully(bArr);
            String str = new String(bArr);
            gd.a((Activity) this.a);
            Intent intent = new Intent("android.intent.action.SEND");
            intent.putExtra("android.intent.extra.EMAIL", new String[]{"rommanager@clockworkmod.com"});
            intent.putExtra("android.intent.extra.SUBJECT", "Recovery Log");
            intent.putExtra("android.intent.extra.TEXT", str);
            intent.setType("text/plain");
            this.a.startActivity(Intent.createChooser(intent, "Email:"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
