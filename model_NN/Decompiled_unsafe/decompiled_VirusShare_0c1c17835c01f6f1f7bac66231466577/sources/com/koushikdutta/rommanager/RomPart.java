package com.koushikdutta.rommanager;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public class RomPart implements Parcelable {
    public static final Parcelable.Creator CREATOR = new as();
    String a;
    String b;
    ArrayList c;

    public RomPart() {
        this.c = new ArrayList();
    }

    private RomPart(Parcel parcel) {
        this.c = new ArrayList();
        this.a = parcel.readString();
        this.b = parcel.readString();
        parcel.readStringList(this.c);
    }

    /* synthetic */ RomPart(Parcel parcel, RomPart romPart) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeStringList(this.c);
    }
}
