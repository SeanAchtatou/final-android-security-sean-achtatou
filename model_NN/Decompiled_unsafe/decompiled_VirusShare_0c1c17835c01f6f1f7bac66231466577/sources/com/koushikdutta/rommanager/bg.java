package com.koushikdutta.rommanager;

import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class bg {
    public static int a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[65536];
        int i = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return i;
            }
            outputStream.write(bArr, 0, read);
            i += read;
            System.out.println(i);
        }
    }

    public static String a(File file) {
        byte[] bArr = new byte[((int) file.length())];
        new DataInputStream(new FileInputStream(file)).readFully(bArr);
        return new String(bArr);
    }

    public static String a(InputStream inputStream) {
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        byte[] bArr = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        while (true) {
            int read = dataInputStream.read(bArr);
            if (read == -1) {
                return new String(byteArrayOutputStream.toByteArray());
            }
            byteArrayOutputStream.write(bArr, 0, read);
            i += read;
        }
    }

    public static String a(String str) {
        return a(new File(str));
    }

    public static void a(String str, String str2) {
        File file = new File(str);
        file.getParentFile().mkdirs();
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));
        dataOutputStream.write(str2.getBytes());
        dataOutputStream.close();
    }

    public static String b(String str) {
        URL url = new URL(str);
        Log.i("ROMManager", "Downloading: " + url);
        DataInputStream dataInputStream = new DataInputStream(gd.a(url.openConnection()));
        String a = a(dataInputStream);
        dataInputStream.close();
        return a;
    }

    public static byte[] b(InputStream inputStream) {
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        byte[] bArr = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        while (true) {
            int read = dataInputStream.read(bArr);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
            i += read;
        }
    }
}
