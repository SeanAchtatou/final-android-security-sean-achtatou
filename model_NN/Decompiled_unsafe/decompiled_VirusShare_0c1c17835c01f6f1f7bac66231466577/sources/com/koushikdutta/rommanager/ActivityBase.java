package com.koushikdutta.rommanager;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.google.ads.AdView;
import com.google.ads.a;
import com.google.ads.d;
import com.google.ads.g;
import com.google.android.apps.analytics.j;
import java.util.HashMap;

public class ActivityBase extends Activity implements dw {
    protected j a;
    boolean b = true;
    protected h c;
    boolean d = false;
    ListView e;
    i f;
    HashMap g = new HashMap();
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public View i = null;

    private void d() {
        try {
            View inflate = LayoutInflater.from(this).inflate((int) C0000R.layout.admob, (ViewGroup) null);
            FrameLayout frameLayout = (FrameLayout) inflate.findViewById(C0000R.id.adview);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
            AdView adView = new AdView(this, g.a, "a14da1301aa3d95");
            adView.setLayoutParams(layoutParams);
            frameLayout.addView(adView);
            addContentView(inflate, new ViewGroup.LayoutParams(-1, -1));
            d dVar = new d();
            dVar.a(false);
            dVar.a(a.MALE);
            gd.a(this, dVar);
            adView.a(dVar);
            this.i = frameLayout;
            this.e.setOnScrollListener(new fx(this));
        } catch (Exception e2) {
            Log.e("RomManager", "Error", e2);
        }
    }

    /* access modifiers changed from: protected */
    public ck a(int i2, ck ckVar) {
        cl clVar = (cl) this.g.get(Integer.valueOf(i2));
        if (clVar == null) {
            clVar = new cl(this, this);
            this.g.put(Integer.valueOf(i2), clVar);
            this.f.a(getString(i2), clVar);
            this.e.setAdapter((ListAdapter) null);
            this.e.setAdapter((ListAdapter) this.f);
        }
        clVar.add(ckVar);
        return ckVar;
    }

    /* access modifiers changed from: protected */
    public cl a(int i2) {
        cl clVar = (cl) this.g.get(Integer.valueOf(i2));
        if (clVar != null) {
            return clVar;
        }
        cl clVar2 = new cl(this, this);
        this.g.put(Integer.valueOf(i2), clVar2);
        this.f.a(getString(i2), clVar2);
        this.e.setAdapter((ListAdapter) null);
        this.e.setAdapter((ListAdapter) this.f);
        return clVar2;
    }

    public void a() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* access modifiers changed from: package-private */
    public void a(CharSequence charSequence) {
        if (this.b) {
            try {
                this.a.a("/" + charSequence.toString().replace(' ', '_'));
                this.a.b();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void a(String str) {
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, String str3) {
        if (this.b) {
            try {
                this.a.a(str, str2, str3, 0);
                this.a.b();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public ck b(int i2) {
        String string = getString(i2);
        for (Adapter adapter : this.f.b.values()) {
            cl clVar = (cl) adapter;
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 < clVar.getCount()) {
                    ck ckVar = (ck) clVar.getItem(i4);
                    if (string.equals(ckVar.c)) {
                        return ckVar;
                    }
                    i3 = i4 + 1;
                }
            }
        }
        return null;
    }

    public void b() {
    }

    public int c() {
        return C0000R.layout.list_item;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) C0000R.layout.activity_base);
        this.e = (ListView) findViewById(C0000R.id.listview);
        this.e.setOnItemClickListener(new fy(this));
        this.e.setOnItemLongClickListener(new ga(this));
        this.f = new i(this, this);
        this.e.setAdapter((ListAdapter) this.f);
        this.c = h.a(this);
        this.b = this.c.b("analytics", true);
        if (gd.e(this)) {
            d();
        }
        if (this.b) {
            try {
                this.a = j.a();
                this.a.a("UA-4956323-3", 10, this);
                a(getTitle());
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.d = true;
        if (this.b) {
            this.a.d();
        }
    }
}
