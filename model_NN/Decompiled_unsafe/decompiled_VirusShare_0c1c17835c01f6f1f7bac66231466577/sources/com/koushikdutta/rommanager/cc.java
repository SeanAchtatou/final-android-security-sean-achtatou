package com.koushikdutta.rommanager;

import android.view.MenuItem;

class cc implements MenuItem.OnMenuItemClickListener {
    final /* synthetic */ DeveloperList a;

    cc(DeveloperList developerList) {
        this.a = developerList;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        h.a(this.a).a("developer_sort", "rating");
        this.a.d();
        this.a.f();
        this.a.f.notifyDataSetChanged();
        return true;
    }
}
