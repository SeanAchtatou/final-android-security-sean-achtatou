package com.koushikdutta.rommanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONObject;

public class RomList extends ActivityBase {
    boolean h = false;
    h i;
    String j;
    JSONObject k;
    String l;
    String m;
    String n;
    MenuItem o;
    MenuItem p;

    private static JSONArray a(JSONObject jSONObject, String str) {
        if (!jSONObject.has(String.valueOf(str) + "s") && !jSONObject.has(str)) {
            return null;
        }
        JSONArray optJSONArray = jSONObject.optJSONArray(String.valueOf(str) + "s");
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        String optString = jSONObject.optString(str);
        if (gd.b(optString)) {
            return optJSONArray;
        }
        optJSONArray.put(optString);
        return optJSONArray;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject, ArrayList arrayList) {
        RomPackage b = b(jSONObject, arrayList);
        if (this.i.b("developer_mode", false)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage((int) C0000R.string.install_additional_zips);
            builder.setNegativeButton((int) C0000R.string.no, new dp(this, b));
            builder.setPositiveButton((int) C0000R.string.yes, new dm(this, b));
            builder.create().show();
            return;
        }
        Intent intent = new Intent(this, DownloadService.class);
        intent.putExtra("rompackage", b);
        startService(intent);
        startActivity(new Intent(this, Downloading.class));
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject, JSONObject jSONObject2) {
        JSONArray optJSONArray = jSONObject2.optJSONArray("choices");
        JSONArray optJSONArray2 = jSONObject2.optJSONArray("addons");
        int[] iArr = optJSONArray != null ? new int[optJSONArray.length()] : new int[0];
        boolean[] zArr = optJSONArray2 != null ? new boolean[optJSONArray2.length()] : new boolean[0];
        ArrayList arrayList = new ArrayList();
        arrayList.add(jSONObject2);
        JSONArray optJSONArray3 = jSONObject2.optJSONArray("extendedurls");
        if (optJSONArray3 != null) {
            for (int i2 = 0; i2 < optJSONArray3.length(); i2++) {
                arrayList.add(optJSONArray3.optJSONObject(i2));
            }
        }
        a(jSONObject, jSONObject2, optJSONArray, optJSONArray2, 0, iArr, zArr, arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomList, int]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject, JSONObject jSONObject2, JSONArray jSONArray, JSONArray jSONArray2, int i2, int[] iArr, boolean[] zArr, ArrayList arrayList) {
        JSONArray a;
        String optString = jSONObject2.optString("modversion", null);
        String d = (!gd.b(optString) || (a = a(jSONObject2, "url")) == null || a.length() <= 0 || (optString = a.optString(0)) == null) ? optString : gd.d(optString);
        if (i2 < iArr.length) {
            try {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i2);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(jSONObject3.getString("name"));
                builder.setCancelable(true);
                JSONArray jSONArray3 = jSONObject3.getJSONArray("options");
                if (jSONArray3.length() == 0) {
                    throw new Exception();
                }
                String[] strArr = new String[jSONArray3.length()];
                for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                    strArr[i3] = jSONArray3.getJSONObject(i3).getString("name");
                }
                builder.setPositiveButton(17039370, new dj(this, arrayList, jSONArray3, iArr, i2, jSONObject, jSONObject2, jSONArray, jSONArray2, zArr));
                builder.setSingleChoiceItems(strArr, 0, new by(this, iArr, i2));
                builder.create().show();
            } catch (Exception e) {
                Log.e("Romlist", e.getLocalizedMessage(), e);
                gd.a((Context) this, C0000R.string.rom_error);
            }
        } else if (zArr.length > 0) {
            try {
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setTitle((int) C0000R.string.rom_addons);
                builder2.setCancelable(true);
                String[] strArr2 = new String[jSONArray2.length()];
                for (int i4 = 0; i4 < jSONArray2.length(); i4++) {
                    strArr2[i4] = jSONArray2.getJSONObject(i4).getString("name");
                }
                builder2.setPositiveButton(17039370, new bx(this, jSONArray2, zArr, arrayList, d, jSONObject));
                builder2.setMultiChoiceItems(strArr2, (boolean[]) null, new bt(this, zArr));
                builder2.create().show();
            } catch (Exception e2) {
                Log.e("Romlist", e2.getLocalizedMessage(), e2);
                gd.a((Context) this, C0000R.string.rom_error);
            }
        } else {
            b(d);
            a(jSONObject, arrayList);
        }
    }

    /* access modifiers changed from: package-private */
    public RomPackage b(JSONObject jSONObject, ArrayList arrayList) {
        JSONArray optJSONArray = jSONObject.optJSONArray("mirrors");
        RomPackage romPackage = new RomPackage();
        ArrayList arrayList2 = new ArrayList();
        romPackage.a = ((JSONObject) arrayList.get(0)).optString("name");
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            JSONObject jSONObject2 = (JSONObject) arrayList.get(i2);
            if (jSONObject2 != null) {
                RomPart romPart = new RomPart();
                romPart.a = jSONObject2.optString("name");
                JSONArray a = a(jSONObject2, "url");
                if (a == null) {
                    String optString = jSONObject2.optString("file");
                    if (optJSONArray != null && !gd.b(optString)) {
                        for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                            String str = String.valueOf(optJSONArray.optString(i3)) + "/" + optString;
                            if (!gd.b(str)) {
                                romPart.c.add(str);
                            }
                        }
                    }
                } else {
                    for (int i4 = 0; i4 < a.length(); i4++) {
                        String optString2 = a.optString(i4);
                        if (!gd.b(optString2)) {
                            romPart.c.add(optString2);
                        }
                    }
                }
                if (romPart.c.size() == 0) {
                    Log.i("Romlist", "Skipping empty download part: " + romPart.a);
                }
                romPart.b = jSONObject2.optString("referer");
                arrayList2.add(romPart);
            }
        }
        romPackage.b = new RomPart[arrayList2.size()];
        arrayList2.toArray(romPackage.b);
        return romPackage;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.RomList, int, com.koushikdutta.rommanager.dn]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (!gd.b(str)) {
            String stringExtra = getIntent().getStringExtra("id");
            String a = h.a(this).a("registration_id");
            String a2 = h.a(this).a("nickname");
            if (!gd.b(stringExtra) && !gd.b(str)) {
                gd.a((gd.b(a) || gd.b(a2)) ? "http://rommanager.deployfu.com/download/" + Uri.encode(stringExtra) + "/" + Uri.encode(str) : "https://rommanager.deployfu.com/download/" + Uri.encode(stringExtra) + "/" + Uri.encode(str) + "?registrationId=" + Uri.encode(a), (Activity) this, false, (cq) new dn(this));
            }
        }
    }

    public int c() {
        return C0000R.layout.rom_list_item;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.b(java.lang.String, int):int
      com.koushikdutta.rommanager.h.b(java.lang.String, long):long
      com.koushikdutta.rommanager.h.b(java.lang.String, java.lang.String):java.lang.String
      com.koushikdutta.rommanager.h.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
     arg types: [com.koushikdutta.rommanager.RomList, int]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void */
    /* access modifiers changed from: package-private */
    public void c(String str) {
        JSONObject optJSONObject;
        JSONArray a;
        boolean b = this.i.b("developer_mode", false);
        boolean b2 = this.i.b("superuser", false);
        String f = gd.f(this);
        try {
            byte[] bArr = new byte[((int) new File(str).length())];
            new DataInputStream(new FileInputStream(str)).readFully(bArr);
            JSONObject jSONObject = new JSONObject(new String(bArr));
            this.m = jSONObject.optString("donate");
            this.n = jSONObject.optString("homepage");
            d();
            JSONArray jSONArray = jSONObject.getJSONArray("roms");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                JSONArray a2 = a(jSONObject2, "device");
                if (a2 != null) {
                    HashSet hashSet = new HashSet();
                    for (int i3 = 0; i3 < a2.length(); i3++) {
                        hashSet.add(a2.getString(i3));
                    }
                    if ((hashSet.contains(this.j) || hashSet.contains("all")) && ((jSONObject2.optBoolean("visible", true) || b2) && ((optJSONObject = jSONObject2.optJSONObject("whitelist")) == null || b2 || optJSONObject.optBoolean(f, false)))) {
                        String string = jSONObject2.getString("name");
                        String optString = jSONObject2.optString("summary");
                        String optString2 = jSONObject2.optString("modversion", null);
                        bv bvVar = new bv(this, this, string, optString, (!gd.b(optString2) || (a = a(jSONObject2, "url")) == null || a.length() <= 0) ? optString2 : gd.d(a.getString(0)), a(jSONObject2, "screenshot"), string, jSONObject, jSONObject2);
                        if (!b && jSONObject2.optBoolean("developer")) {
                            bvVar.a(false);
                        }
                        a(C0000R.string.roms, bvVar);
                    }
                }
            }
        } catch (Exception e) {
            gd.a((Activity) this, C0000R.string.roms_error);
            Log.e("Romlist", e.getLocalizedMessage(), e);
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.p != null && this.o != null) {
            this.p.setVisible(!gd.b(this.n) && this.n.startsWith("http"));
            this.o.setVisible(!gd.b(this.m) && this.m.startsWith("http"));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.RomList, int, com.koushikdutta.rommanager.dq]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.RomList, int, com.koushikdutta.rommanager.dr]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(2);
        requestWindowFeature(5);
        super.onCreate(bundle);
        this.i = h.a(this);
        this.j = this.i.a("detected_device");
        Intent intent = getIntent();
        setTitle(intent.getCharSequenceExtra("name"));
        this.l = intent.getStringExtra("id");
        String stringExtra = getIntent().getStringExtra("manifest_url");
        if (!getIntent().getBooleanExtra("free", false) && !gd.a(this, (dw) null)) {
            Toast.makeText(this, (int) C0000R.string.premium_required_to_download, 1).show();
        }
        gd.a(stringExtra, (Activity) this, false, (cq) new dq(this));
        gd.a("http://rommanager.deployfu.com/v2/ratings/" + Uri.encode(this.l), (Activity) this, false, (cq) new dr(this));
        if (intent.hasExtra("rom")) {
            try {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                JSONObject jSONObject = new JSONObject(intent.getStringExtra("rom"));
                JSONObject jSONObject2 = new JSONObject(intent.getStringExtra("manifest"));
                builder.setCancelable(true);
                builder.setTitle((int) C0000R.string.update_available);
                builder.setMessage(String.format(getString(C0000R.string.updated_rom), jSONObject.getString("name")));
                builder.setPositiveButton(17039370, new Cdo(this, jSONObject2, jSONObject));
                builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
                builder.create().show();
            } catch (Exception e) {
                Log.e("Romlist", e.getLocalizedMessage(), e);
                finish();
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(C0000R.menu.romlistmenu, menu);
        this.o = menu.findItem(C0000R.id.donate);
        this.o.setVisible(true);
        this.o.setOnMenuItemClickListener(new ds(this));
        this.p = menu.findItem(C0000R.id.developer_homepage);
        this.p.setOnMenuItemClickListener(new dt(this));
        d();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.h = true;
        super.onDestroy();
    }
}
