package com.koushikdutta.rommanager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

class r extends BroadcastReceiver {
    final /* synthetic */ ay a;
    private final /* synthetic */ dw b;

    r(ay ayVar, dw dwVar) {
        this.a = ayVar;
        this.b = dwVar;
    }

    public void onReceive(Context context, Intent intent) {
        try {
            context.unregisterReceiver(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        gd.c(context, this.b, intent.getStringExtra("inapp_signed_data"), intent.getStringExtra("inapp_signature"));
    }
}
