package com.koushikdutta.rommanager;

import android.content.Intent;
import android.net.Uri;
import android.view.MenuItem;

class ds implements MenuItem.OnMenuItemClickListener {
    final /* synthetic */ RomList a;

    ds(RomList romList) {
        this.a = romList;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(Uri.parse(this.a.m));
        this.a.startActivity(intent);
        return true;
    }
}
