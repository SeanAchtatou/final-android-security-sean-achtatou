package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import org.json.JSONObject;

class ah implements AdapterView.OnItemLongClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ bv a;
    private final /* synthetic */ ArrayAdapter b;
    private final /* synthetic */ JSONObject c;

    ah(bv bvVar, ArrayAdapter arrayAdapter, JSONObject jSONObject) {
        this.a = bvVar;
        this.b = arrayAdapter;
        this.c = jSONObject;
    }

    public boolean onItemLongClick(AdapterView adapterView, View view, int i, long j) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.b);
        builder.setItems(new String[]{this.a.b.getString(C0000R.string.block_user)}, new ao(this, this.b, i, this.c));
        builder.create().show();
        return true;
    }
}
