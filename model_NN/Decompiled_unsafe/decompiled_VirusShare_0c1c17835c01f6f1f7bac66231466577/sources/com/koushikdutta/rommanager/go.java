package com.koushikdutta.rommanager;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

class go extends Thread {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager a;
    private final /* synthetic */ String b;
    private final /* synthetic */ boolean c;
    private final /* synthetic */ boolean d;
    private final /* synthetic */ String e;

    go(RomManager romManager, String str, boolean z, boolean z2, String str2) {
        this.a = romManager;
        this.b = str;
        this.c = z;
        this.d = z2;
        this.e = str2;
    }

    public void run() {
        try {
            JSONObject jSONObject = new JSONObject(bg.b("http://gh-pages.clockworkmod.com/ROMManagerManifest/devices.js"));
            this.a.k = jSONObject.getString("recovery_url");
            this.a.l = jSONObject.getString("recovery_zip_url");
            this.a.m = jSONObject.optBoolean("use_in_app", false);
            this.a.c.a("ad_network", jSONObject.optString("ad_network", "adsense"));
            this.a.c.a("manifest", jSONObject.optString("manifest"));
            this.a.c.a("manifest_signature", jSONObject.optString("manifest_signature"));
            this.a.c.a("keywords", jSONObject.optString("keywords"));
            String string = jSONObject.getString("version");
            JSONArray jSONArray = jSONObject.getJSONArray("devices");
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    if (!jSONObject2.getString("key").equals(this.b)) {
                        continue;
                        i++;
                    } else {
                        JSONArray jSONArray2 = jSONObject2.getJSONArray("legacy_versions");
                        this.a.j = new String[jSONArray2.length()];
                        for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                            this.a.j[i2] = jSONArray2.getString(i2);
                        }
                        String optString = jSONObject2.optString("version");
                        if (optString == null || optString.equals("") || optString.compareTo(string) < 0) {
                            optString = string;
                        }
                        this.a.h = optString;
                        boolean z = this.c && this.d && this.e != null && optString.compareTo(this.e) > 0;
                        JSONObject optJSONObject = jSONObject2.optJSONObject("alternate_recovery");
                        if (optJSONObject != null) {
                            String optString2 = optJSONObject.optString("name");
                            String optString3 = optJSONObject.optString("url");
                            boolean optBoolean = optJSONObject.optBoolean("clockwork");
                            if (!gd.b(optString2) && !gd.b(optString3)) {
                                this.a.c.a("alternate_recovery_name", optString2);
                                this.a.c.a("alternate_recovery_url", optString3);
                                this.a.c.a("alternate_recovery_is_clockwork", optBoolean);
                            }
                        }
                        this.a.n.post(new gy(this, z));
                        return;
                    }
                } catch (Exception e2) {
                    Log.e("RomNexus", "Exception:", e2);
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
