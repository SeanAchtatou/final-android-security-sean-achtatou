package com.koushikdutta.rommanager;

import android.view.MenuItem;

class z implements MenuItem.OnMenuItemClickListener {
    final /* synthetic */ DeveloperList a;

    z(DeveloperList developerList) {
        this.a = developerList;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        h.a(this.a).a("developer_sort", "none");
        this.a.d();
        this.a.f();
        this.a.f.notifyDataSetChanged();
        return true;
    }
}
