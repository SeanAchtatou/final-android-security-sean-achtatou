package com.koushikdutta.rommanager;

import android.content.Context;

class av implements cq {
    private final /* synthetic */ Context a;
    private final /* synthetic */ dw b;
    private final /* synthetic */ String c;

    av(Context context, dw dwVar, String str) {
        this.a = context;
        this.b = dwVar;
        this.c = str;
    }

    public void a(String str) {
        try {
            if (!gd.a(str, this.a)) {
                this.b.a("License verification failed.");
                return;
            }
            gd.b();
            this.b.a();
            try {
                String a2 = bg.a(this.c);
                bg.a(gd.h(this.a), a2);
                bg.a("/sdcard/clockworkmod/.rommanager_license", a2);
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            this.b.a(e2.getMessage());
        }
    }

    public boolean a() {
        return false;
    }

    public void b() {
    }
}
