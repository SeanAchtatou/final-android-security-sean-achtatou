package com.koushikdutta.rommanager;

import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.RelativeLayout;

class fw implements AbsListView.OnScrollListener {
    final /* synthetic */ AnalyticsActivity a;

    fw(AnalyticsActivity analyticsActivity) {
        this.a = analyticsActivity;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (i == 0) {
            try {
                if (this.a.e) {
                    this.a.e = false;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(new ViewGroup.LayoutParams(-2, -2));
                    layoutParams.addRule(12);
                    layoutParams.addRule(13);
                    this.a.f.setLayoutParams(layoutParams);
                }
            } catch (Exception e) {
            }
        } else if (i + i2 >= i3 && !this.a.e) {
            this.a.e = true;
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(new ViewGroup.LayoutParams(-2, -2));
            layoutParams2.addRule(10);
            layoutParams2.addRule(13);
            this.a.f.setLayoutParams(layoutParams2);
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
    }
}
