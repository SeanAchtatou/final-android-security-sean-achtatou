package com.koushikdutta.rommanager;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.widget.RatingBar;

class an implements RatingBar.OnRatingBarChangeListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ bv a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    an(bv bvVar, String str, String str2) {
        this.a = bvVar;
        this.b = str;
        this.c = str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomList, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void
     arg types: [java.lang.String, com.koushikdutta.rommanager.RomList, int, com.koushikdutta.rommanager.al]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw, java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.app.Activity, boolean, com.koushikdutta.rommanager.cq):void */
    public void onRatingChanged(RatingBar ratingBar, float f, boolean z) {
        if (z) {
            if (this.a.a != null && f == ((float) this.a.a.optInt("rating", 0))) {
                return;
            }
            if (!gd.a(this.a.b, (dw) null)) {
                gd.a((Context) this.a.b, (int) C0000R.string.premium_only);
            } else if (this.b == null || (this.a.a != null && this.a.a.optString("nickname", null) == null)) {
                gd.a((Context) this.a.b, (int) C0000R.string.login_required);
            } else {
                gd.a("https://rommanager.deployfu.com/ratings/" + Uri.encode(this.a.b.l) + "/" + Uri.encode(this.c) + "?rating=" + ((int) f) + "&registrationId=" + Uri.encode(this.b), (Activity) this.a.b, false, (cq) new al(this));
            }
        }
    }
}
