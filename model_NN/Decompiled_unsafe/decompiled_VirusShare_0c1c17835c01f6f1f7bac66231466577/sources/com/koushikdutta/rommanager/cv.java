package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;

class cv implements DialogInterface.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ co a;
    private final /* synthetic */ String b;

    cv(co coVar, String str) {
        this.a = coVar;
        this.b = str;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        String replace = this.a.a.h.getResources().getStringArray(C0000R.array.swap_size_options)[this.a.a.b].replace("MB", "M");
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.a.h);
        builder.setCancelable(true);
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.setTitle((int) C0000R.string.partition_sdcard);
        builder.setMessage((int) C0000R.string.partition_sdcard_warning);
        builder.setPositiveButton(17039370, new bf(this, this.b, replace));
        builder.create().show();
    }
}
