package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.MenuItem;

class fe implements MenuItem.OnMenuItemClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ RomManager a;

    fe(RomManager romManager) {
        this.a = romManager;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        builder.setIcon(17301543);
        builder.setTitle((int) C0000R.string.clear_cache);
        builder.setCancelable(true);
        builder.setMessage((int) C0000R.string.clear_cache_confirm);
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton(17039370, new en(this));
        builder.create().show();
        return true;
    }
}
