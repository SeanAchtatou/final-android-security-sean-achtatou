package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.content.DialogInterface;

class gj implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ dz a;
    private final /* synthetic */ String b;
    private final /* synthetic */ boolean[] c;

    gj(dz dzVar, String str, boolean[] zArr) {
        this.a = dzVar;
        this.b = str;
        this.c = zArr;
    }

    public void run() {
        this.a.b.a.a("Nandroid", "Restore", "Restore");
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a.b.a);
        builder.setIcon(0);
        builder.setTitle((int) C0000R.string.restore);
        builder.setMessage((int) C0000R.string.confirm_restore);
        builder.setCancelable(true);
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton(17039370, new bi(this, this.b, this.c));
        builder.create().show();
    }
}
