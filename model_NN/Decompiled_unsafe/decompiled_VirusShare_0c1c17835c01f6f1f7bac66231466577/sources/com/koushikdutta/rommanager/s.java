package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.preference.CheckBoxPreference;
import android.preference.Preference;

class s implements Preference.OnPreferenceClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ SettingsScreen a;
    private final /* synthetic */ CheckBoxPreference b;

    s(SettingsScreen settingsScreen, CheckBoxPreference checkBoxPreference) {
        this.a = settingsScreen;
        this.b = checkBoxPreference;
    }

    public boolean onPreferenceClick(Preference preference) {
        this.a.c.a("developer_mode", this.b.isChecked());
        if (this.b.isChecked()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
            builder.setIcon(17301543);
            builder.setTitle((int) C0000R.string.developer_mode);
            builder.setCancelable(true);
            builder.setMessage((int) C0000R.string.developer_mode_details);
            builder.setNegativeButton(17039360, new ct(this, this.b));
            builder.setPositiveButton(17039370, new cu(this, this.b));
            builder.create().show();
        }
        this.b.setSummary(this.b.isChecked() ? C0000R.string.developer_mode_on : C0000R.string.developer_mode_off);
        return true;
    }
}
