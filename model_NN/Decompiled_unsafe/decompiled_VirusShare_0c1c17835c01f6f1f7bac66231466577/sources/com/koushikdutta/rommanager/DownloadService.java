package com.koushikdutta.rommanager;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemProperties;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

public class DownloadService extends Service {
    NotificationManager a;
    final String b = "DownloadService";
    boolean c = false;
    Thread d = null;
    Handler e = new Handler();
    h f;
    String g;

    private JSONObject a(JSONObject jSONObject, String str) {
        JSONArray jSONArray = jSONObject.getJSONArray("roms");
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                String optString = jSONObject2.optString("modversion");
                if (optString != null && optString.equals(str)) {
                    return jSONObject2;
                }
            } catch (Exception e2) {
                Log.e("DownloadService", e2.getLocalizedMessage(), e2);
            }
        }
        return null;
    }

    private JSONObject a(JSONObject jSONObject, String str, int i) {
        String optString;
        int i2;
        JSONArray jSONArray = jSONObject.getJSONArray("roms");
        JSONObject jSONObject2 = null;
        int i3 = i;
        for (int i4 = 0; i4 < jSONArray.length(); i4++) {
            try {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i4);
                String optString2 = jSONObject3.optString("product");
                if (jSONObject3.optBoolean("visible", true) && optString2 != null && optString2.equals(str) && (optString = jSONObject3.optString("device")) != null && optString.equals(this.g) && (i2 = jSONObject3.getInt("incremental")) > i3) {
                    i3 = i2;
                    jSONObject2 = jSONObject3;
                }
            } catch (Exception e2) {
                Log.e("DownloadService", e2.getLocalizedMessage(), e2);
            }
        }
        return jSONObject2;
    }

    /* access modifiers changed from: private */
    public void a() {
        this.e.post(new eg(this));
    }

    private void a(Intent intent) {
        RomPackage romPackage = (RomPackage) intent.getParcelableExtra("rompackage");
        this.a.cancelAll();
        this.c = true;
        if (this.d != null) {
            try {
                this.d.join();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        this.c = false;
        this.d = null;
        this.d = new ee(this, romPackage);
        this.d.start();
    }

    /* access modifiers changed from: private */
    public JSONObject b(JSONObject jSONObject, String str) {
        JSONObject jSONObject2 = new JSONObject(bg.b(jSONObject.getString("manifest")));
        JSONObject a2 = a(jSONObject2, str);
        if (a2 == null) {
            return null;
        }
        String optString = a2.optString("product");
        if (optString == null) {
            return null;
        }
        JSONObject a3 = a(jSONObject2, optString, a2.getInt("incremental"));
        if (a3 == null) {
            return null;
        }
        return a3;
    }

    private void b(Intent intent) {
        String str = SystemProperties.get("ro.modversion");
        String str2 = SystemProperties.get("ro.rommanager.developerid");
        if (!gd.a(this, (dw) null) || str == null || str2 == null || this.g == null) {
            a();
        } else {
            new ef(this, str2, str, intent).start();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.f = h.a(this);
        gd.a(this.f);
        this.g = this.f.a("detected_device");
        this.a = (NotificationManager) getSystemService("notification");
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        if (intent.hasExtra("rompackage")) {
            a(intent);
        } else if (intent.getBooleanExtra("cancel_download", false)) {
            this.e.post(new ei(this));
        } else {
            b(intent);
        }
    }
}
