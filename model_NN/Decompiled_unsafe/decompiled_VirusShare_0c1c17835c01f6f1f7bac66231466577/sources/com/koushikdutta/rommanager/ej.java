package com.koushikdutta.rommanager;

import android.content.Context;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

class ej implements cq {
    final /* synthetic */ CloudBackups a;
    private final /* synthetic */ String b;

    ej(CloudBackups cloudBackups, String str) {
        this.a = cloudBackups;
        this.b = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.CloudBackups, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void a(String str) {
        try {
            String a2 = bg.a(str);
            System.out.println(a2);
            JSONObject jSONObject = new JSONObject(a2);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                JSONArray jSONArray = jSONObject.getJSONArray(next);
                int i = this.b.equals(next) ? C0000R.string.backups_for_current_device : C0000R.string.other_backups;
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    System.out.println(jSONObject2.toString());
                    this.a.a(i, new ck(this.a, jSONObject2.getString("name"), "", 0));
                }
            }
            this.a.f.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
            gd.a((Context) this.a, (int) C0000R.string.no_connection);
        }
    }

    public boolean a() {
        return this.a.h;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.CloudBackups, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void b() {
        gd.a((Context) this.a, (int) C0000R.string.no_connection);
    }
}
