package com.koushikdutta.rommanager;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;

class ee extends Thread {
    int a = 0;
    String b;
    PendingIntent c;
    Notification d;
    final /* synthetic */ DownloadService e;
    private final /* synthetic */ RomPackage f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    ee(DownloadService downloadService, RomPackage romPackage) {
        this.e = downloadService;
        this.f = romPackage;
        this.d = new Notification(C0000R.drawable.progress, romPackage.a, 0);
        Intent intent = new Intent();
        intent.setAction("com.koushikdutta.rommanager.canceldownload");
        intent.putExtra("id", 2323);
        Intent intent2 = new Intent(downloadService, RomManager.class);
        intent2.setFlags(335544320);
        intent2.putExtra("cancel_download_prompt", true);
        this.c = PendingIntent.getActivity(downloadService, 0, intent2, 268435456);
        this.d.deleteIntent = PendingIntent.getBroadcast(downloadService, 0, intent, 0);
        this.d.setLatestEventInfo(downloadService, romPackage.a, romPackage.a, this.c);
        this.d.tickerText = romPackage.a;
        this.d.flags |= 2;
        downloadService.a.notify(2323, this.d);
    }

    private void a(int i) {
        RemoteViews remoteViews = new RemoteViews(this.e.getPackageName(), (int) C0000R.layout.notification_progress);
        remoteViews.setProgressBar(C0000R.id.ProgressBar01, 100, i, false);
        remoteViews.setTextViewText(C0000R.id.notificationTitle, this.b);
        remoteViews.setImageViewResource(C0000R.id.notificationIcon, C0000R.drawable.progress);
        remoteViews.setTextViewText(C0000R.id.notificationPercentage, String.valueOf(i) + "%");
        this.d.setLatestEventInfo(this.e.getApplicationContext(), null, null, this.c);
        this.d.contentView = remoteViews;
        this.e.a.notify(2323, this.d);
    }

    private void a(int i, String str) {
        String string = this.e.getString(i);
        this.e.a.cancel(2323);
        this.d.tickerText = string;
        this.d.setLatestEventInfo(this.e, string, str, this.c);
        this.e.a.notify(2323, this.d);
    }

    /* access modifiers changed from: package-private */
    public String a(URL url) {
        File a2 = gd.a(url);
        if (!a2.exists()) {
            return null;
        }
        return a2.getAbsolutePath();
    }

    /* access modifiers changed from: package-private */
    public void a(RomPart romPart) {
        this.b = String.valueOf(romPart.a) + String.format(" (%d of %d)", Integer.valueOf(this.a + 1), Integer.valueOf(this.f.b.length));
        this.d.setLatestEventInfo(this.e, this.f.a, this.b, this.c);
        this.e.a.notify(2323, this.d);
        Iterator it = romPart.c.iterator();
        boolean z = false;
        while (it.hasNext()) {
            String str = (String) it.next();
            if (str != null && !str.equals("") && !str.equals("null")) {
                String a2 = a(new URL(str));
                if (a2 != null) {
                    Log.i("DownloadService", String.valueOf(str) + " found in cache.");
                    if (!gd.c(a2)) {
                        try {
                            new File(a2).delete();
                            z = true;
                        } catch (Exception e2) {
                            z = true;
                        }
                    } else {
                        this.a++;
                        this.f.c.add(a2);
                        return;
                    }
                } else {
                    z = true;
                }
            }
        }
        if (!z) {
            this.a++;
            return;
        }
        String str2 = "Unable to download ROM part";
        int round = (int) Math.round(Math.random() * ((double) romPart.c.size()));
        int i = 0;
        while (i < romPart.c.size() * 2) {
            if (!this.e.c) {
                try {
                    a((String) romPart.c.get((round + i) % romPart.c.size()), romPart.b);
                    this.a++;
                    return;
                } catch (Exception e3) {
                    String localizedMessage = e3.getLocalizedMessage();
                    Log.e("DownloadService", e3.getLocalizedMessage(), e3);
                    i++;
                    str2 = localizedMessage;
                }
            } else {
                return;
            }
        }
        throw new Exception(str2);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        Log.i("DownloadService", "Attempting download of " + str);
        URL url = new URL(str);
        File a2 = gd.a(url);
        Log.i("DownloadService", "Downloading: " + url);
        URLConnection openConnection = url.openConnection();
        if (!gd.b(str2)) {
            openConnection.addRequestProperty("Referer", str2);
        }
        DataInputStream dataInputStream = new DataInputStream(gd.a(openConnection));
        int contentLength = openConnection.getContentLength();
        Log.i("DownloadService", "File size: " + contentLength);
        Log.i("DownloadService", "Download starting...");
        File file = new File(String.valueOf(a2.toString()) + ".tmp");
        a2.getParentFile().mkdirs();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        byte[] bArr = new byte[200000];
        int i = 0;
        int i2 = 0;
        while (true) {
            int read = dataInputStream.read(bArr);
            if (read == -1) {
                fileOutputStream.close();
                dataInputStream.close();
                if (!gd.c(file.getAbsolutePath())) {
                    throw new Exception(this.e.getString(C0000R.string.corrupt));
                }
                file.renameTo(a2);
                Log.i("DownloadService", "Download complete. Bytes received: " + i);
                this.f.c.add(a2.getAbsolutePath());
                Log.i("DownloadService", "Download complete.");
                return;
            } else if (!this.e.c) {
                i += read;
                int i3 = (int) ((((double) i) / ((double) contentLength)) * 100.0d);
                if (contentLength != -1 && i > 100000 + i2) {
                    a(i3);
                    i2 = i;
                }
                fileOutputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public void run() {
        String str;
        Exception e2;
        String str2 = this.f.a;
        try {
            bd.a(this.e);
            RomPart[] romPartArr = this.f.b;
            int length = romPartArr.length;
            String str3 = str2;
            int i = 0;
            while (true) {
                if (i >= length) {
                    str2 = str3;
                    break;
                }
                try {
                    RomPart romPart = romPartArr[i];
                    str3 = romPart.a;
                    if (this.e.c) {
                        str2 = str3;
                        break;
                    } else {
                        a(romPart);
                        i++;
                    }
                } catch (Exception e3) {
                    e2 = e3;
                    str = str3;
                    try {
                        Log.e("DownloadService", e2.getLocalizedMessage(), e2);
                        this.d.flags &= -3;
                        a((int) C0000R.string.rom_download_error_short, String.valueOf(str) + ": " + e2.getLocalizedMessage());
                        return;
                    } finally {
                        bd.a();
                    }
                }
            }
            if (!this.e.c) {
                Intent intent = new Intent(this.e, RomManager.class);
                intent.putExtra("rompackage", this.f);
                intent.setFlags(335544320);
                this.c = PendingIntent.getActivity(this.e, 0, intent, 268435456);
                this.e.startActivity(intent);
                this.d.flags &= -3;
                a((int) C0000R.string.rom_download_complete, this.f.a);
            } else {
                this.e.a.cancelAll();
            }
            bd.a();
        } catch (Exception e4) {
            Exception exc = e4;
            str = str2;
            e2 = exc;
        }
    }
}
