package com.koushikdutta.rommanager;

import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.koushikdutta.a.a;

class bj implements AdapterView.OnItemClickListener {
    int a;
    final /* synthetic */ bv b;
    private final /* synthetic */ ArrayAdapter c;

    bj(bv bvVar, ArrayAdapter arrayAdapter) {
        this.b = bvVar;
        this.c = arrayAdapter;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        try {
            this.a = i;
            AlertDialog.Builder builder = new AlertDialog.Builder(this.b.b);
            ImageView imageView = new ImageView(this.b.b);
            imageView.setOnClickListener(new el(this, this.c, imageView));
            builder.setView(imageView);
            a.a(imageView, (String) this.c.getItem(i));
            builder.create().show();
        } catch (Exception e) {
        }
    }
}
