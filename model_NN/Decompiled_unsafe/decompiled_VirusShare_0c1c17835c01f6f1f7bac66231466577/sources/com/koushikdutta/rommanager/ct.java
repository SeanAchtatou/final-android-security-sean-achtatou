package com.koushikdutta.rommanager;

import android.content.DialogInterface;
import android.preference.CheckBoxPreference;

class ct implements DialogInterface.OnClickListener {
    final /* synthetic */ s a;
    private final /* synthetic */ CheckBoxPreference b;

    ct(s sVar, CheckBoxPreference checkBoxPreference) {
        this.a = sVar;
        this.b = checkBoxPreference;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.b.setChecked(false);
        this.b.setSummary((int) C0000R.string.developer_mode_off);
    }
}
