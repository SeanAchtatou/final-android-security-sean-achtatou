package com.koushikdutta.rommanager;

import android.content.Context;

class gt implements cq {
    final /* synthetic */ RomManager a;
    private final /* synthetic */ String b;

    gt(RomManager romManager, String str) {
        this.a = romManager;
        this.b = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.a(java.lang.String, int):void
      com.koushikdutta.rommanager.h.a(java.lang.String, long):void
      com.koushikdutta.rommanager.h.a(java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void a(String str) {
        String format;
        try {
            this.a.c.a("is_clockworkmod", true);
            this.a.c.a("current_recovery_version", this.b);
            this.a.k();
            String a2 = this.a.c.a("flash_recovery");
            if (gd.b(a2)) {
                format = " cat " + str + " > " + "/sdcard/clockworkmod/recovery-update.zip" + " ; cat " + str + " > /sdcard/update.zip ;";
            } else {
                format = String.format(a2, str);
            }
            gd.c(this.a, format);
            gd.a((Context) this.a, (int) C0000R.string.download_recovery_success);
            this.a.h();
        } catch (Exception e) {
            e.printStackTrace();
            gd.a((Context) this.a, (int) C0000R.string.script_error);
        }
        this.a.e.setEnabled(true);
        this.a.i();
    }

    public boolean a() {
        return this.a.i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void b() {
        this.a.e.setEnabled(true);
        this.a.i();
        gd.a((Context) this.a, (int) C0000R.string.download_recovery_error);
    }
}
