package com.koushikdutta.rommanager;

import android.preference.Preference;
import android.widget.Toast;

class x implements Preference.OnPreferenceClickListener {
    final /* synthetic */ SettingsScreen a;

    x(SettingsScreen settingsScreen) {
        this.a = settingsScreen;
    }

    public boolean onPreferenceClick(Preference preference) {
        this.a.e++;
        if (this.a.e % 10 == 0) {
            boolean z = !this.a.c.b("superuser", false);
            this.a.c.a("superuser", z);
            Toast.makeText(this.a, "Superuser: " + z, 1).show();
        }
        return true;
    }
}
