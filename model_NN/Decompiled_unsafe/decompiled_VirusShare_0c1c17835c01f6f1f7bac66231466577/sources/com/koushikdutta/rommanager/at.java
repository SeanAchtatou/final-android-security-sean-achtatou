package com.koushikdutta.rommanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import java.util.ArrayList;

class at extends AlertDialog.Builder {
    final boolean[] a;

    at(Context context, Activity activity, boolean z, ArrayList arrayList) {
        super(context);
        boolean[] zArr = new boolean[3];
        zArr[2] = true;
        this.a = zArr;
        setIcon(0);
        setTitle((int) C0000R.string.preinstallation);
        setCancelable(true);
        setMultiChoiceItems((int) C0000R.array.install_options, this.a, new gb(this));
        setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        setPositiveButton(17039370, new gc(this, activity, z, arrayList));
    }
}
