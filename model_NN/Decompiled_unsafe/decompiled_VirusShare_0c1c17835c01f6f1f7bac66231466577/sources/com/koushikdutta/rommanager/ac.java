package com.koushikdutta.rommanager;

import android.app.TimePickerDialog;
import android.preference.CheckBoxPreference;
import android.preference.Preference;

class ac implements Preference.OnPreferenceClickListener {
    int a = 3;
    /* access modifiers changed from: package-private */
    public final /* synthetic */ SettingsScreen b;
    private final /* synthetic */ CheckBoxPreference c;

    ac(SettingsScreen settingsScreen, CheckBoxPreference checkBoxPreference) {
        this.b = settingsScreen;
        this.c = checkBoxPreference;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.h.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.koushikdutta.rommanager.h.a(java.lang.String, int):void
      com.koushikdutta.rommanager.h.a(java.lang.String, java.lang.String):void
      com.koushikdutta.rommanager.h.a(java.lang.String, boolean):void
      com.koushikdutta.rommanager.h.a(java.lang.String, long):void */
    public boolean onPreferenceClick(Preference preference) {
        if (this.c.isChecked()) {
            TimePickerDialog timePickerDialog = new TimePickerDialog(this.b, new gr(this, this.c), 0, 0, false);
            timePickerDialog.setTitle((int) C0000R.string.automatic_backup_time);
            timePickerDialog.setOnDismissListener(new gq(this, this.c));
            timePickerDialog.setCancelable(false);
            timePickerDialog.show();
            return true;
        }
        this.b.c.a("backup_frequency", 0);
        this.b.c.a("last_backup", 0L);
        this.b.c();
        return true;
    }
}
