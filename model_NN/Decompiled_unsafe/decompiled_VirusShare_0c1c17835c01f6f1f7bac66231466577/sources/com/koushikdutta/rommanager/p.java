package com.koushikdutta.rommanager;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import java.util.ArrayList;

public class p {
    private p() {
    }

    public static void a(Activity activity, String str, fs fsVar, k kVar) {
        AccountManager accountManager = AccountManager.get(activity);
        Account account = new Account(str, "com.google");
        String a = h.a(activity).a("web_connect_auth_token");
        if (!gd.b(a)) {
            accountManager.invalidateAuthToken(account.type, a);
        }
        accountManager.getAuthToken(account, "ah", false, new n(kVar, activity, str, fsVar), null);
    }

    public static String[] a(Context context) {
        ArrayList arrayList = new ArrayList();
        for (Account account : AccountManager.get(context).getAccounts()) {
            if (account.type.equals("com.google")) {
                arrayList.add(account.name);
            }
        }
        String[] strArr = new String[arrayList.size()];
        arrayList.toArray(strArr);
        return strArr;
    }
}
