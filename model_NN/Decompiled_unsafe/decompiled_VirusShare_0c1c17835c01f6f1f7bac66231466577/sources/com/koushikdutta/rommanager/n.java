package com.koushikdutta.rommanager;

import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

class n implements AccountManagerCallback {
    private final /* synthetic */ k a;
    private final /* synthetic */ Activity b;
    private final /* synthetic */ String c;
    private final /* synthetic */ fs d;

    n(k kVar, Activity activity, String str, fs fsVar) {
        this.a = kVar;
        this.b = activity;
        this.c = str;
        this.d = fsVar;
    }

    public void run(AccountManagerFuture accountManagerFuture) {
        try {
            Bundle bundle = (Bundle) accountManagerFuture.getResult();
            String string = bundle.getString("authtoken");
            if (string != null) {
                h a2 = h.a(this.b);
                a2.a("web_connect_auth_token", string);
                a2.a("account", this.c.toLowerCase());
                try {
                    this.d.a((Bundle) accountManagerFuture.getResult());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (this.a == null) {
                throw new Exception();
            } else {
                this.a.a(new ez(this, this.b, this.c, this.d));
                this.b.startActivityForResult((Intent) bundle.get("intent"), 242424);
            }
        } catch (Exception e2) {
            this.d.a(null);
            e2.printStackTrace();
        }
    }
}
