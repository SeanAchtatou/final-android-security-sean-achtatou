package com.koushikdutta.rommanager;

import java.util.Comparator;

class bm implements Comparator {
    final /* synthetic */ DeveloperList a;

    bm(DeveloperList developerList) {
        this.a = developerList;
    }

    /* renamed from: a */
    public int compare(ck ckVar, ck ckVar2) {
        if (((dk) ckVar).i == ((dk) ckVar2).i) {
            return 0;
        }
        return ((dk) ckVar).i > ((dk) ckVar2).i ? -1 : 1;
    }
}
