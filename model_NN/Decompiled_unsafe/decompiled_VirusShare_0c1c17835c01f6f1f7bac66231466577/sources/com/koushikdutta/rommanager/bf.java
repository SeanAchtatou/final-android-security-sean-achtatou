package com.koushikdutta.rommanager;

import android.content.Context;
import android.content.DialogInterface;
import com.koushikdutta.rommanager.a.b;

class bf implements DialogInterface.OnClickListener {
    final /* synthetic */ cv a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;

    bf(cv cvVar, String str, String str2) {
        this.a = cvVar;
        this.b = str;
        this.c = str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.koushikdutta.rommanager.gd.a(android.content.Context, int):void
     arg types: [com.koushikdutta.rommanager.RomManager, ?]
     candidates:
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.String):android.os.Bundle
      com.koushikdutta.rommanager.gd.a(android.app.Activity, int):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.google.ads.d):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.a.b):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.lang.StringBuilder):void
      com.koushikdutta.rommanager.gd.a(android.content.Context, com.koushikdutta.rommanager.dw):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, java.io.File):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, android.content.Context):boolean
      com.koushikdutta.rommanager.gd.a(java.lang.String, java.lang.String):boolean
      com.koushikdutta.rommanager.gd.a(android.content.Context, int):void */
    public void onClick(DialogInterface dialogInterface, int i) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(". %s/%s ; ", this.a.a.a.h.getFilesDir().getAbsolutePath(), "preparepartition.sh"));
        b a2 = b.a(this.a.a.a.h);
        gd.a(this.a.a.a.h, a2);
        a2.a("Partitioning SD Card...", new String[0]);
        a2.b("/cache/dopartition.sh", this.b, this.c);
        a2.a(this.a.a.a.h, sb);
        sb.append("sync ; ");
        gd.a(this.a.a.a.h, sb);
        try {
            gd.c(this.a.a.a.h, sb.toString());
        } catch (Exception e) {
            gd.a((Context) this.a.a.a.h, (int) C0000R.string.script_error);
            e.printStackTrace();
        }
    }
}
