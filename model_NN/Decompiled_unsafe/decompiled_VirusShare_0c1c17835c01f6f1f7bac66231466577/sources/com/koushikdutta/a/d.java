package com.koushikdutta.a;

import java.lang.ref.SoftReference;
import java.util.Hashtable;

public class d {
    Hashtable a = new Hashtable();

    public Object a(Object obj) {
        SoftReference softReference = (SoftReference) this.a.get(obj);
        if (softReference == null) {
            return null;
        }
        Object obj2 = softReference.get();
        if (obj2 != null) {
            return obj2;
        }
        this.a.remove(obj);
        return obj2;
    }

    public Object a(Object obj, Object obj2) {
        SoftReference softReference = (SoftReference) this.a.put(obj, new SoftReference(obj2));
        if (softReference == null) {
            return null;
        }
        return softReference.get();
    }
}
