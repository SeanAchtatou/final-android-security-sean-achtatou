package com.koushikdutta.a;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;

public final class a {
    static Resources a;
    static DisplayMetrics b;
    private static boolean c = false;
    /* access modifiers changed from: private */
    public static Hashtable d = new Hashtable();
    /* access modifiers changed from: private */
    public static Hashtable e = new Hashtable();

    public static int a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[1024];
        int i = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return i;
            }
            outputStream.write(bArr, 0, read);
            i += read;
        }
    }

    public static String a(String str) {
        return str.hashCode() + ".urlimage";
    }

    private static void a(Context context) {
        if (b == null) {
            b = new DisplayMetrics();
            ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(b);
            a = new Resources(context.getAssets(), b, null);
        }
    }

    private static void a(Context context, ImageView imageView, String str, int i, long j) {
        a(context, imageView, str, i != 0 ? imageView.getResources().getDrawable(i) : null, j);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0048, code lost:
        if (java.lang.System.currentTimeMillis() < (r0.lastModified() + r12)) goto L_0x004a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.content.Context r8, android.widget.ImageView r9, java.lang.String r10, android.graphics.drawable.Drawable r11, long r12) {
        /*
            b(r8)
            if (r9 == 0) goto L_0x000a
            java.util.Hashtable r0 = com.koushikdutta.a.a.d
            r0.remove(r9)
        L_0x000a:
            boolean r0 = a(r10)
            if (r0 == 0) goto L_0x0016
            if (r9 == 0) goto L_0x0015
            r9.setImageDrawable(r11)
        L_0x0015:
            return
        L_0x0016:
            com.koushikdutta.a.c r5 = com.koushikdutta.a.c.a()
            java.lang.Object r0 = r5.a(r10)
            android.graphics.drawable.Drawable r0 = (android.graphics.drawable.Drawable) r0
            if (r0 == 0) goto L_0x0028
            if (r9 == 0) goto L_0x0015
            r9.setImageDrawable(r0)
            goto L_0x0015
        L_0x0028:
            java.lang.String r3 = a(r10)
            java.io.File r0 = r8.getFileStreamPath(r3)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x005f
            r1 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r1 = (r12 > r1 ? 1 : (r12 == r1 ? 0 : -1))
            if (r1 == 0) goto L_0x004a
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x005e }
            long r6 = r0.lastModified()     // Catch:{ Exception -> 0x005e }
            long r6 = r6 + r12
            int r0 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r0 >= 0) goto L_0x005f
        L_0x004a:
            java.io.FileInputStream r0 = r8.openFileInput(r3)     // Catch:{ Exception -> 0x005e }
            android.graphics.drawable.BitmapDrawable r1 = b(r8, r0)     // Catch:{ Exception -> 0x005e }
            r0.close()     // Catch:{ Exception -> 0x005e }
            if (r9 == 0) goto L_0x005a
            r9.setImageDrawable(r1)     // Catch:{ Exception -> 0x005e }
        L_0x005a:
            r5.a(r10, r1)     // Catch:{ Exception -> 0x005e }
            goto L_0x0015
        L_0x005e:
            r0 = move-exception
        L_0x005f:
            if (r9 == 0) goto L_0x0064
            r9.setImageDrawable(r11)
        L_0x0064:
            if (r9 == 0) goto L_0x006b
            java.util.Hashtable r0 = com.koushikdutta.a.a.d
            r0.put(r9, r10)
        L_0x006b:
            java.util.Hashtable r0 = com.koushikdutta.a.a.e
            java.lang.Object r0 = r0.get(r10)
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            if (r0 == 0) goto L_0x007b
            if (r9 == 0) goto L_0x0015
            r0.add(r9)
            goto L_0x0015
        L_0x007b:
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            if (r9 == 0) goto L_0x0085
            r6.add(r9)
        L_0x0085:
            java.util.Hashtable r0 = com.koushikdutta.a.a.e
            r0.put(r10, r6)
            com.koushikdutta.a.b r0 = new com.koushikdutta.a.b
            r1 = r10
            r2 = r8
            r4 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r1 = 0
            java.lang.Void[] r1 = new java.lang.Void[r1]
            r0.execute(r1)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.koushikdutta.a.a.a(android.content.Context, android.widget.ImageView, java.lang.String, android.graphics.drawable.Drawable, long):void");
    }

    public static void a(ImageView imageView, String str) {
        a(imageView.getContext(), imageView, str, (Drawable) null, 259200000);
    }

    public static void a(ImageView imageView, String str, int i) {
        a(imageView.getContext(), imageView, str, i, 259200000);
    }

    private static boolean a(CharSequence charSequence) {
        return charSequence == null || charSequence.equals("") || charSequence.equals("null") || charSequence.equals("NULL");
    }

    /* access modifiers changed from: private */
    public static BitmapDrawable b(Context context, InputStream inputStream) {
        a(context);
        return new BitmapDrawable(a, BitmapFactory.decodeStream(inputStream));
    }

    private static void b(Context context) {
        if (!c) {
            c = true;
            try {
                String[] list = context.getFilesDir().list();
                if (list != null) {
                    for (String str : list) {
                        if (str.endsWith(".urlimage")) {
                            File file = new File(str);
                            if (System.currentTimeMillis() > file.lastModified() + 604800000) {
                                file.delete();
                            }
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
