package com.koushikdutta.a;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

class b extends AsyncTask {
    private final /* synthetic */ String a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ String c;
    private final /* synthetic */ Drawable d;
    private final /* synthetic */ c e;
    private final /* synthetic */ ArrayList f;

    b(String str, Context context, String str2, Drawable drawable, c cVar, ArrayList arrayList) {
        this.a = str;
        this.b = context;
        this.c = str2;
        this.d = drawable;
        this.e = cVar;
        this.f = arrayList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Drawable doInBackground(Void... voidArr) {
        try {
            HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(this.a));
            if (execute.getStatusLine().getStatusCode() != 200) {
                return null;
            }
            InputStream content = execute.getEntity().getContent();
            FileOutputStream openFileOutput = this.b.openFileOutput(this.c, 0);
            a.a(content, openFileOutput);
            openFileOutput.close();
            content.close();
            return a.b(this.b, this.b.openFileInput(this.c));
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Drawable drawable) {
        Drawable drawable2 = drawable == null ? this.d : drawable;
        a.e.remove(this.a);
        this.e.a(this.a, drawable2);
        Iterator it = this.f.iterator();
        while (it.hasNext()) {
            ImageView imageView = (ImageView) it.next();
            if (this.a.equals((String) a.d.get(imageView))) {
                a.d.remove(imageView);
                if (drawable2 != null) {
                    imageView.setImageDrawable(drawable2);
                }
            }
        }
    }
}
