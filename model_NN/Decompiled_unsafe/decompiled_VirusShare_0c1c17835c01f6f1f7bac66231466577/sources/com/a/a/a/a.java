package com.a.a.a;

import android.text.TextUtils;
import java.util.Iterator;
import java.util.regex.Pattern;

public class a {
    public int a;
    public int b;
    public String c;
    public String d;
    public String e;
    public long f;
    public String g;
    public String h;
    public String i;

    public static a a(String str, String str2) {
        TextUtils.SimpleStringSplitter simpleStringSplitter = new TextUtils.SimpleStringSplitter(':');
        simpleStringSplitter.setString(str);
        Iterator it = simpleStringSplitter.iterator();
        if (!it.hasNext()) {
            throw new IllegalArgumentException("Blank response.");
        }
        String str3 = (String) it.next();
        String str4 = it.hasNext() ? (String) it.next() : "";
        String[] split = TextUtils.split(str3, Pattern.quote("|"));
        if (split.length < 6) {
            throw new IllegalArgumentException("Wrong number of fields.");
        }
        a aVar = new a();
        aVar.g = str4;
        aVar.a = Integer.parseInt(split[0]);
        aVar.b = Integer.parseInt(split[1]);
        aVar.c = split[2];
        aVar.d = split[3];
        aVar.e = split[4];
        aVar.f = Long.parseLong(split[5]);
        aVar.h = str;
        aVar.i = str2;
        return aVar;
    }

    public String toString() {
        return TextUtils.join("|", new Object[]{Integer.valueOf(this.a), Integer.valueOf(this.b), this.c, this.d, this.e, Long.valueOf(this.f)});
    }
}
