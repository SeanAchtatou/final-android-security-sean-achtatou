package com.a.a.b;

public enum b {
    PURCHASED,
    CANCELED,
    REFUNDED;

    public static b a(int i) {
        b[] values = values();
        return (i < 0 || i >= values.length) ? CANCELED : values[i];
    }
}
