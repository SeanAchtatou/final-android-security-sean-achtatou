package defpackage;

import android.webkit.WebView;

/* renamed from: aa  reason: default package */
final class aa implements Runnable {
    final /* synthetic */ f a;
    private final String b;
    private final String c;
    private final WebView d;

    public aa(f fVar, WebView webView, String str, String str2) {
        this.a = fVar;
        this.d = webView;
        this.b = str;
        this.c = str2;
    }

    public final void run() {
        this.d.loadDataWithBaseURL(this.b, this.c, "text/html", "utf-8", null);
    }
}
