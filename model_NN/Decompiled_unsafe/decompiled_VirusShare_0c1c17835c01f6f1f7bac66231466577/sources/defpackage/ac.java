package defpackage;

import java.util.LinkedList;

/* renamed from: ac  reason: default package */
final class ac implements Runnable {
    final /* synthetic */ f a;
    private final c b;
    private final LinkedList c;
    private final int d;

    public ac(f fVar, c cVar, LinkedList linkedList, int i) {
        this.a = fVar;
        this.b = cVar;
        this.c = linkedList;
        this.d = i;
    }

    public final void run() {
        this.b.a(this.c);
        this.b.a(this.d);
        this.b.o();
    }
}
