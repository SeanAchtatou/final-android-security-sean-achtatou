package defpackage;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.c;
import com.google.ads.d;
import com.google.ads.g;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

/* renamed from: f  reason: default package */
public final class f implements Runnable {
    private String a;
    private String b = null;
    private e c;
    private c d;
    private d e;
    private WebView f;
    private String g = null;
    private LinkedList h = new LinkedList();
    private volatile boolean i;
    private c j = null;
    private boolean k = false;
    private int l = -1;
    private Thread m;

    public f(c cVar) {
        this.d = cVar;
        Activity d2 = cVar.d();
        if (d2 != null) {
            this.f = new b(d2.getApplicationContext(), null);
            this.f.setWebViewClient(new n(cVar, g.a, false, false));
            this.f.setVisibility(8);
            this.f.setWillNotDraw(true);
            this.c = new e(this, cVar);
            return;
        }
        this.f = null;
        this.c = null;
        com.google.ads.util.d.e("activity was null while trying to create an AdLoader.");
    }

    private String a(d dVar, Activity activity) {
        Context applicationContext = activity.getApplicationContext();
        Map a2 = dVar.a(applicationContext);
        a k2 = this.d.k();
        long h2 = k2.h();
        if (h2 > 0) {
            a2.put("prl", Long.valueOf(h2));
        }
        String g2 = k2.g();
        if (g2 != null) {
            a2.put("ppcl", g2);
        }
        String f2 = k2.f();
        if (f2 != null) {
            a2.put("pcl", f2);
        }
        long e2 = k2.e();
        if (e2 > 0) {
            a2.put("pcc", Long.valueOf(e2));
        }
        a2.put("preqs", Long.valueOf(a.i()));
        String j2 = k2.j();
        if (j2 != null) {
            a2.put("pai", j2);
        }
        if (k2.k()) {
            a2.put("aoi_timeout", "true");
        }
        if (k2.m()) {
            a2.put("aoi_nofill", "true");
        }
        String p = k2.p();
        if (p != null) {
            a2.put("pit", p);
        }
        k2.a();
        k2.d();
        if (this.d.e() instanceof com.google.ads.f) {
            a2.put("format", "interstitial_mb");
        } else {
            g j3 = this.d.j();
            String gVar = j3.toString();
            if (gVar != null) {
                a2.put("format", gVar);
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("w", Integer.valueOf(j3.a()));
                hashMap.put("h", Integer.valueOf(j3.b()));
                a2.put("ad_frame", hashMap);
            }
        }
        a2.put("slotname", this.d.g());
        a2.put("js", "afma-sdk-a-v4.1.1");
        try {
            int i2 = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
            a2.put("msid", applicationContext.getPackageName());
            a2.put("app_name", i2 + ".android." + applicationContext.getPackageName());
            a2.put("isu", AdUtil.a(applicationContext));
            String d2 = AdUtil.d(applicationContext);
            if (d2 == null) {
                throw new ab(this, "NETWORK_ERROR");
            }
            a2.put("net", d2);
            String e3 = AdUtil.e(applicationContext);
            if (!(e3 == null || e3.length() == 0)) {
                a2.put("cap", e3);
            }
            a2.put("u_audio", Integer.valueOf(AdUtil.f(applicationContext).ordinal()));
            DisplayMetrics a3 = AdUtil.a(activity);
            a2.put("u_sd", Float.valueOf(a3.density));
            a2.put("u_h", Integer.valueOf(AdUtil.a(applicationContext, a3)));
            a2.put("u_w", Integer.valueOf(AdUtil.b(applicationContext, a3)));
            a2.put("hl", Locale.getDefault().getLanguage());
            if (AdUtil.c()) {
                a2.put("simulator", 1);
            }
            String str = "<html><head><script src=\"http://www.gstatic.com/afma/sdk-core-v40.js\"></script><script>AFMA_buildAdURL(" + AdUtil.a(a2) + ");" + "</script></head><body></body></html>";
            com.google.ads.util.d.c("adRequestUrlHtml: " + str);
            return str;
        } catch (PackageManager.NameNotFoundException e4) {
            throw new z(this, "NameNotFoundException");
        }
    }

    private void a(c cVar, boolean z) {
        this.c.a();
        this.d.a(new y(this, this.d, this.f, this.c, cVar, z));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        this.k = true;
        notify();
    }

    public final synchronized void a(int i2) {
        this.l = i2;
    }

    public final synchronized void a(c cVar) {
        this.j = cVar;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final void a(d dVar) {
        this.e = dVar;
        this.i = false;
        this.m = new Thread(this);
        this.m.start();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        this.h.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.a = str2;
        this.b = str;
        notify();
    }

    public final synchronized void b(String str) {
        this.g = str;
        notify();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: f.a(com.google.ads.c, boolean):void
     arg types: [com.google.ads.c, int]
     candidates:
      f.a(com.google.ads.d, android.app.Activity):java.lang.String
      f.a(java.lang.String, java.lang.String):void
      f.a(com.google.ads.c, boolean):void */
    public final void run() {
        synchronized (this) {
            if (this.f == null || this.c == null) {
                com.google.ads.util.d.e("adRequestWebView was null while trying to load an ad.");
                a(c.INTERNAL_ERROR, false);
                return;
            }
            Activity d2 = this.d.d();
            if (d2 == null) {
                com.google.ads.util.d.e("activity was null while forming an ad request.");
                a(c.INTERNAL_ERROR, false);
                return;
            }
            try {
                this.d.a(new aa(this, this.f, null, a(this.e, d2)));
                long m2 = this.d.m();
                long elapsedRealtime = SystemClock.elapsedRealtime();
                if (m2 > 0) {
                    try {
                        wait(m2);
                    } catch (InterruptedException e2) {
                        com.google.ads.util.d.a("AdLoader InterruptedException while getting the URL: " + e2);
                        return;
                    }
                }
                try {
                    if (!this.i) {
                        if (this.j != null) {
                            a(this.j, false);
                            return;
                        } else if (this.g == null) {
                            com.google.ads.util.d.c("AdLoader timed out after " + m2 + "ms while getting the URL.");
                            a(c.NETWORK_ERROR, false);
                            return;
                        } else {
                            this.c.a(this.g);
                            long elapsedRealtime2 = m2 - (SystemClock.elapsedRealtime() - elapsedRealtime);
                            if (elapsedRealtime2 > 0) {
                                try {
                                    wait(elapsedRealtime2);
                                } catch (InterruptedException e3) {
                                    com.google.ads.util.d.a("AdLoader InterruptedException while getting the HTML: " + e3);
                                    return;
                                }
                            }
                            if (!this.i) {
                                if (this.j != null) {
                                    a(this.j, false);
                                    return;
                                } else if (this.b == null) {
                                    com.google.ads.util.d.c("AdLoader timed out after " + m2 + "ms while getting the HTML.");
                                    a(c.NETWORK_ERROR, false);
                                    return;
                                } else {
                                    b h2 = this.d.h();
                                    this.d.i().a();
                                    this.d.a(new aa(this, h2, this.a, this.b));
                                    long elapsedRealtime3 = m2 - (SystemClock.elapsedRealtime() - elapsedRealtime);
                                    if (elapsedRealtime3 > 0) {
                                        try {
                                            wait(elapsedRealtime3);
                                        } catch (InterruptedException e4) {
                                            com.google.ads.util.d.a("AdLoader InterruptedException while loading the HTML: " + e4);
                                            return;
                                        }
                                    }
                                    if (this.k) {
                                        this.d.a(new ac(this, this.d, this.h, this.l));
                                    } else {
                                        com.google.ads.util.d.c("AdLoader timed out after " + m2 + "ms while loading the HTML.");
                                        a(c.NETWORK_ERROR, true);
                                    }
                                    return;
                                }
                            } else {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                } catch (Exception e5) {
                    com.google.ads.util.d.a("An unknown error occurred in AdLoader.", e5);
                    a(c.INTERNAL_ERROR, true);
                }
            } catch (ab e6) {
                com.google.ads.util.d.c("Unable to connect to network: " + e6);
                a(c.NETWORK_ERROR, false);
                return;
            } catch (z e7) {
                com.google.ads.util.d.c("Caught internal exception: " + e7);
                a(c.INTERNAL_ERROR, false);
                return;
            }
        }
    }
}
