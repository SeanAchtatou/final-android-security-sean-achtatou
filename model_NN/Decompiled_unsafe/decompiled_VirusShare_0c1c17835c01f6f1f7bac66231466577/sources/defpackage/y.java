package defpackage;

import android.webkit.WebView;
import com.google.ads.c;

/* renamed from: y  reason: default package */
final class y implements Runnable {
    final /* synthetic */ f a;
    private final c b;
    private final WebView c;
    private final e d;
    private final c e;
    private final boolean f;

    public y(f fVar, c cVar, WebView webView, e eVar, c cVar2, boolean z) {
        this.a = fVar;
        this.b = cVar;
        this.c = webView;
        this.d = eVar;
        this.e = cVar2;
        this.f = z;
    }

    public final void run() {
        this.c.stopLoading();
        this.c.destroy();
        this.d.a();
        if (this.f) {
            b h = this.b.h();
            h.stopLoading();
            h.setVisibility(8);
        }
        this.b.a(this.e);
    }
}
