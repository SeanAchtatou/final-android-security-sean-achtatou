package defpackage;

import android.app.Activity;
import com.google.ads.c;
import com.google.ads.util.AdUtil;
import com.google.ads.util.d;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

/* renamed from: e  reason: default package */
public final class e implements Runnable {
    private f a;
    private c b;
    private volatile boolean c;
    private String d;

    e(f fVar, c cVar) {
        this.a = fVar;
        this.b = cVar;
    }

    private void a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.b.a(stringTokenizer.nextToken());
            }
        }
        b(httpURLConnection);
        String headerField2 = httpURLConnection.getHeaderField("X-Afma-Refresh-Rate");
        if (headerField2 != null) {
            try {
                this.b.a(Float.parseFloat(headerField2));
                if (!this.b.n()) {
                    this.b.c();
                }
            } catch (NumberFormatException e) {
                d.b("Could not get refresh value: " + headerField2, e);
            }
        }
        String headerField3 = httpURLConnection.getHeaderField("X-Afma-Interstitial-Timeout");
        if (headerField3 != null) {
            try {
                this.b.a((long) (Float.parseFloat(headerField3) * 1000.0f));
            } catch (NumberFormatException e2) {
                d.b("Could not get timeout value: " + headerField3, e2);
            }
        }
        String headerField4 = httpURLConnection.getHeaderField("X-Afma-Orientation");
        if (headerField4 == null) {
            return;
        }
        if (headerField4.equals("portrait")) {
            this.a.a(AdUtil.b());
        } else if (headerField4.equals("landscape")) {
            this.a.a(AdUtil.a());
        }
    }

    private void b(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Click-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.a.a(stringTokenizer.nextToken());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.c = true;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.d = str;
        this.c = false;
        new Thread(this).start();
    }

    public final void run() {
        HttpURLConnection httpURLConnection;
        String readLine;
        while (!this.c) {
            try {
                httpURLConnection = (HttpURLConnection) new URL(this.d).openConnection();
                Activity d2 = this.b.d();
                if (d2 == null) {
                    d.c("activity was null in AdHtmlLoader.");
                    this.a.a(c.INTERNAL_ERROR);
                    httpURLConnection.disconnect();
                    return;
                }
                AdUtil.a(httpURLConnection, d2.getApplicationContext());
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                if (300 <= responseCode && responseCode < 400) {
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (headerField == null) {
                        d.c("Could not get redirect location from a " + responseCode + " redirect.");
                        this.a.a(c.INTERNAL_ERROR);
                        httpURLConnection.disconnect();
                        return;
                    }
                    a(httpURLConnection);
                    this.d = headerField;
                    httpURLConnection.disconnect();
                } else if (responseCode == 200) {
                    a(httpURLConnection);
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()), 4096);
                    StringBuilder sb = new StringBuilder();
                    while (!this.c && (readLine = bufferedReader.readLine()) != null) {
                        sb.append(readLine);
                        sb.append("\n");
                    }
                    String sb2 = sb.toString();
                    d.a("Response content is: " + sb2);
                    if (sb2 == null || sb2.trim().length() <= 0) {
                        d.a("Response message is null or zero length: " + sb2);
                        this.a.a(c.NO_FILL);
                        httpURLConnection.disconnect();
                        return;
                    }
                    this.a.a(sb2, this.d);
                    httpURLConnection.disconnect();
                    return;
                } else if (responseCode == 400) {
                    d.c("Bad request");
                    this.a.a(c.INVALID_REQUEST);
                    httpURLConnection.disconnect();
                    return;
                } else {
                    d.c("Invalid response code: " + responseCode);
                    this.a.a(c.INTERNAL_ERROR);
                    httpURLConnection.disconnect();
                    return;
                }
            } catch (MalformedURLException e) {
                d.a("Received malformed ad url from javascript.", e);
                this.a.a(c.INTERNAL_ERROR);
                return;
            } catch (IOException e2) {
                d.b("IOException connecting to ad url.", e2);
                this.a.a(c.NETWORK_ERROR);
                return;
            } catch (Exception e3) {
                d.a("An unknown error occurred in AdHtmlLoader.", e3);
                this.a.a(c.INTERNAL_ERROR);
                return;
            } catch (Throwable th) {
                httpURLConnection.disconnect();
                throw th;
            }
        }
    }
}
