package com.wiyun.ad;

class v {
    private final s a;
    private final String b;
    private final StringBuffer c;
    private int d = 0;
    private int e = -1;
    private int f = -2;

    public v(s sVar, String str) {
        this.a = sVar;
        this.b = str;
        this.c = new StringBuffer(this.b.length());
    }

    private void a(int i) {
        this.e = Math.max(i, this.f - 10);
        do {
            this.e = this.b.indexOf(38, this.e);
            if (this.f != -1 && this.f < this.e) {
                this.f = this.b.indexOf(59, this.e + 1);
            }
            if (((this.e == -1 || this.f == -1 || this.e - this.f >= 10) ? false : true) || this.e == -1) {
                return;
            }
            if (this.f == -1) {
                this.e = -1;
                return;
            }
            this.e++;
        } while (this.e != -1);
    }

    private String b(int i) {
        return this.b.substring(this.e + i, this.f);
    }

    private void b() {
        if (this.e != this.d) {
            int length = this.e != -1 ? this.e : this.b.length();
            if (length - this.d > 3) {
                this.c.append(this.b.substring(this.d, length));
                this.d = length;
                return;
            }
            while (this.d < length) {
                StringBuffer stringBuffer = this.c;
                String str = this.b;
                int i = this.d;
                this.d = i + 1;
                stringBuffer.append(str.charAt(i));
            }
        }
    }

    private void c() {
        if (this.e != -1) {
            if (this.b.charAt(this.d + 1) == '#' ? d() : e()) {
                this.d = this.f + 1;
                return;
            }
            this.c.append(this.b.charAt(this.d));
            this.d++;
        }
    }

    private boolean d() {
        int parseInt;
        if (!(this.b.charAt(this.e + 2) == 'x' || this.b.charAt(this.e + 2) == 'X')) {
            try {
                parseInt = Integer.parseInt(b(2));
            } catch (NumberFormatException e2) {
                return false;
            }
        } else {
            parseInt = Integer.parseInt(b(3), 16);
        }
        this.c.append((char) parseInt);
        return true;
    }

    private boolean e() {
        char a2 = this.a.a(b(1));
        if (a2 == 65535) {
            return false;
        }
        this.c.append(a2);
        return true;
    }

    public String a() {
        while (this.d < this.b.length()) {
            a(this.d);
            b();
            c();
        }
        return this.c.toString();
    }
}
