package com.wiyun.ad;

import android.view.View;
import android.view.inputmethod.InputMethodManager;

class al implements Runnable {
    final /* synthetic */ o a;
    private final /* synthetic */ View b;

    al(o oVar, View view) {
        this.a = oVar;
        this.b = view;
    }

    public void run() {
        InputMethodManager inputMethodManager = (InputMethodManager) this.a.a.getContext().getSystemService("input_method");
        if (inputMethodManager.isActive(this.b)) {
            inputMethodManager.showSoftInput(this.b, 0);
        } else {
            this.b.postDelayed(this, 50);
        }
    }
}
