package com.wiyun.ad;

import java.io.UnsupportedEncodingException;

public class y {
    private static IllegalStateException a(String str, UnsupportedEncodingException unsupportedEncodingException) {
        return new IllegalStateException(String.valueOf(str) + ": " + unsupportedEncodingException);
    }

    public static byte[] a(String str) {
        return a(str, "UTF-8");
    }

    public static byte[] a(String str, String str2) {
        if (str == null) {
            return null;
        }
        try {
            return str.getBytes(str2);
        } catch (UnsupportedEncodingException e) {
            throw a(str2, e);
        }
    }
}
