package com.wiyun.ad;

import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

class u extends JSONObject {
    public u() {
    }

    public u(String str) {
        super(str);
    }

    public u(Map map) {
        super(map);
    }

    public u(JSONObject jSONObject, String[] strArr) {
        super(jSONObject, strArr);
    }

    public u(JSONTokener jSONTokener) {
        super(jSONTokener);
    }

    public Object get(String str) {
        try {
            return super.get(str);
        } catch (JSONException e) {
            return null;
        }
    }

    public boolean getBoolean(String str) {
        try {
            if (super.has(str)) {
                return super.getBoolean(str);
            }
            return false;
        } catch (JSONException e) {
            return false;
        }
    }

    public double getDouble(String str) {
        try {
            if (super.has(str)) {
                return super.getDouble(str);
            }
            return 0.0d;
        } catch (JSONException e) {
            return 0.0d;
        }
    }

    public int getInt(String str) {
        try {
            if (super.has(str)) {
                return super.getInt(str);
            }
            return 0;
        } catch (JSONException e) {
            return 0;
        }
    }

    public JSONArray getJSONArray(String str) {
        try {
            if (super.has(str)) {
                return super.getJSONArray(str);
            }
            return null;
        } catch (JSONException e) {
            return null;
        }
    }

    public JSONObject getJSONObject(String str) {
        try {
            if (super.has(str)) {
                return super.getJSONObject(str);
            }
            return null;
        } catch (JSONException e) {
            return null;
        }
    }

    public long getLong(String str) {
        try {
            if (super.has(str)) {
                return super.getLong(str);
            }
            return 0;
        } catch (JSONException e) {
            return 0;
        }
    }

    public String getString(String str) {
        try {
            if (super.has(str)) {
                return super.getString(str);
            }
            return null;
        } catch (JSONException e) {
            return null;
        }
    }
}
