package com.wiyun.ad;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

class ac {
    static boolean a;
    private static int b = 10000;
    private static final Map c = new HashMap();

    ac() {
    }

    private static int a(AdView adView) {
        Integer num = (Integer) c.get(adView.g());
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0341  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.wiyun.ad.ai a(android.content.Context r13, com.wiyun.ad.AdView r14) {
        /*
            r0 = 0
            java.lang.String r1 = r14.g()
            java.lang.String r1 = com.wiyun.ad.r.a(r1)
            boolean r2 = r14.i()
            if (r2 == 0) goto L_0x0016
            int r2 = r14.j()
            switch(r2) {
                case 1: goto L_0x02db;
                case 2: goto L_0x02e3;
                case 3: goto L_0x02eb;
                default: goto L_0x0016;
            }
        L_0x0016:
            java.lang.String r2 = "android.permission.INTERNET"
            int r2 = r13.checkCallingOrSelfPermission(r2)
            r3 = -1
            if (r2 != r3) goto L_0x0024
            java.lang.String r2 = "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            com.wiyun.ad.aa.a(r2)
        L_0x0024:
            r2 = 0
            org.apache.http.impl.client.DefaultHttpClient r3 = a()
            java.io.File r4 = new java.io.File     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.io.File r5 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "wiyun_last_ad_json"
            r4.<init>(r5, r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            int r5 = a(r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            android.content.res.Resources r6 = r13.getResources()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            android.content.res.Configuration r6 = r6.getConfiguration()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            int r6 = r6.orientation     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r7 = -1
            if (r5 == r7) goto L_0x02f3
            if (r5 == r6) goto L_0x006b
            boolean r5 = r4.exists()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r5 == 0) goto L_0x005e
            byte[] r5 = a(r4)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r5 == 0) goto L_0x005e
            java.lang.String r5 = a(r5)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            boolean r7 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r7 != 0) goto L_0x005e
            r0 = r5
        L_0x005e:
            java.util.Map r5 = com.wiyun.ad.ac.c     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r7 = r14.g()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r5.put(r7, r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x006b:
            boolean r5 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r5 == 0) goto L_0x0170
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "http://d.wiyun.com/adv/d"
            r5.<init>(r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r6 = 63
            java.lang.StringBuilder r6 = r5.append(r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r7 = "t"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r7 = "="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r6.append(r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "a"
            a(r5, r6, r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "r"
            java.lang.String r7 = r14.g()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "s"
            java.lang.String r7 = "1.0.8"
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            android.content.res.Resources r6 = r13.getResources()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            android.util.DisplayMetrics r6 = r6.getDisplayMetrics()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r7 = "h"
            int r8 = r6.heightPixels     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r7, r8)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r7 = "w"
            int r6 = r6.widthPixels     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r7, r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "o"
            boolean r7 = com.wiyun.ad.aa.b(r13)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r7 == 0) goto L_0x031c
            java.lang.String r7 = "Android Emulator"
        L_0x00c6:
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "v"
            java.lang.String r7 = android.os.Build.VERSION.RELEASE     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "b"
            java.lang.String r7 = android.os.Build.BRAND     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "m"
            java.lang.String r7 = android.os.Build.MODEL     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "u"
            java.lang.String r7 = com.wiyun.ad.aa.a(r13)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "n"
            int r7 = com.wiyun.ad.aa.c(r13)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "f"
            r7 = 0
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "l"
            java.util.Locale r7 = java.util.Locale.getDefault()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r7 = r7.getLanguage()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "c"
            java.lang.String r7 = com.wiyun.ad.aa.e(r13)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "mm"
            java.lang.String r7 = r14.h()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r6, r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            boolean r6 = r14.i()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r6 != 0) goto L_0x012b
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r1 != 0) goto L_0x012b
            boolean r1 = com.wiyun.ad.ac.a     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r1 != 0) goto L_0x012b
            java.lang.String r1 = "e"
            r6 = 1
            a(r5, r1, r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x012b:
            java.lang.String r1 = r14.k()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r1 == 0) goto L_0x013e
            java.lang.String r1 = "k"
            java.lang.String r6 = r14.k()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r5, r1, r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x013e:
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r1.<init>(r5)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r5 = "User-Agent"
            java.lang.String r6 = com.wiyun.ad.aa.a()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r1.setHeader(r5, r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            org.apache.http.HttpResponse r1 = r3.execute(r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            org.apache.http.StatusLine r5 = r1.getStatusLine()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            int r5 = r5.getStatusCode()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r6 = 300(0x12c, float:4.2E-43)
            if (r5 >= r6) goto L_0x0320
            org.apache.http.HttpEntity r0 = r1.getEntity()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = a(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r1 = 1
            com.wiyun.ad.ac.a = r1     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x016b:
            if (r0 == 0) goto L_0x0170
            a(r4, r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x0170:
            boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r1 != 0) goto L_0x04f2
            com.wiyun.ad.u r1 = new com.wiyun.ad.u     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            org.json.JSONTokener r4 = new org.json.JSONTokener     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r4.<init>(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r1.<init>(r4)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = "p"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r4 = "q"
            java.lang.String r4 = r1.optString(r4)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r5 = "a"
            int r5 = r1.optInt(r5)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r6 = "m"
            java.lang.String r6 = r1.optString(r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r7 = "z"
            java.lang.String r7 = r1.optString(r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r8 = "ra"
            java.lang.String r8 = r1.optString(r8)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r9 = "c"
            java.lang.String r9 = r1.optString(r9)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            boolean r10 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r10 != 0) goto L_0x050a
            boolean r10 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r10 != 0) goto L_0x050a
            boolean r10 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r10 != 0) goto L_0x050a
            boolean r10 = android.text.TextUtils.isEmpty(r8)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r10 != 0) goto L_0x050a
            boolean r10 = android.text.TextUtils.isEmpty(r9)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r10 != 0) goto L_0x050a
            r10 = 2
            if (r5 == r10) goto L_0x01d3
            java.lang.String r10 = "application/x-search"
            boolean r10 = r10.equals(r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r10 != 0) goto L_0x050a
        L_0x01d3:
            java.lang.String r10 = "application/x-search"
            boolean r10 = r10.equals(r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r10 == 0) goto L_0x01e4
            java.lang.String r10 = "%query%"
            int r10 = r9.indexOf(r10)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r11 = -1
            if (r10 == r11) goto L_0x050a
        L_0x01e4:
            java.lang.String r10 = "application/x-map"
            boolean r10 = r10.equals(r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r10 == 0) goto L_0x0204
            java.lang.String r10 = "addr://"
            boolean r10 = r9.startsWith(r10)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r10 != 0) goto L_0x0204
            java.lang.String r10 = "http://"
            boolean r10 = r9.startsWith(r10)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r10 != 0) goto L_0x0204
            java.lang.String r10 = "loc://"
            boolean r10 = r9.startsWith(r10)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r10 == 0) goto L_0x050a
        L_0x0204:
            com.wiyun.ad.ai r10 = new com.wiyun.ad.ai     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.<init>()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.a = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.b = r4     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.d = r5     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = "i"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.t = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = "t"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.f = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = "st"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.j = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = "sh"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.g = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.o = r7     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.p = r9     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = "b"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.l = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = "h"
            int r0 = r1.optInt(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.n = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = "w"
            int r0 = r1.optInt(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.m = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.e = r8     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.q = r6     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = "bc"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            boolean r4 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r4 != 0) goto L_0x0269
            r4 = 1
            java.lang.String r0 = r0.substring(r4)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r4 = 16
            long r4 = java.lang.Long.parseLong(r0, r4)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            int r0 = (int) r4     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.k = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x0269:
            int r0 = r10.k     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r4 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r0 & r4
            if (r0 != 0) goto L_0x0277
            int r0 = r10.k     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r4 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r0 | r4
            r10.k = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x0277:
            java.lang.String r0 = "tc"
            java.lang.String r0 = r1.optString(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            boolean r4 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r4 != 0) goto L_0x0291
            r4 = 1
            java.lang.String r0 = r0.substring(r4)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r4 = 16
            long r4 = java.lang.Long.parseLong(r0, r4)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            int r0 = (int) r4     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.h = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x0291:
            int r0 = r10.h     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r4 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r0 & r4
            if (r0 != 0) goto L_0x029f
            int r0 = r10.h     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r4 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r0 | r4
            r10.h = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x029f:
            java.lang.String r0 = "ts"
            int r0 = r1.optInt(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            float r0 = (float) r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r1 = 1095761920(0x41500000, float:13.0)
            float r0 = java.lang.Math.max(r0, r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.i = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = r14.g()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.c = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = r10.f     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r0 != 0) goto L_0x02c4
            java.lang.String r0 = r10.f     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = com.wiyun.ad.ag.a(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.f = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x02c4:
            int r0 = r10.d     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            switch(r0) {
                case 1: goto L_0x04ef;
                case 2: goto L_0x0349;
                case 3: goto L_0x0459;
                default: goto L_0x02c9;
            }
        L_0x02c9:
            r13 = r2
        L_0x02ca:
            org.apache.http.conn.ClientConnectionManager r14 = r3.getConnectionManager()
            r14.shutdown()
            if (r13 != 0) goto L_0x02da
            java.lang.String r14 = "WiYun"
            java.lang.String r0 = "Failed to get ad"
            android.util.Log.w(r14, r0)
        L_0x02da:
            return r13
        L_0x02db:
            java.lang.String r0 = "test_text_ad"
            java.lang.String r0 = com.wiyun.ad.w.a(r0)
            goto L_0x0016
        L_0x02e3:
            java.lang.String r0 = "test_banner_ad"
            java.lang.String r0 = com.wiyun.ad.w.a(r0)
            goto L_0x0016
        L_0x02eb:
            java.lang.String r0 = "test_fullscreen_ad"
            java.lang.String r0 = com.wiyun.ad.w.a(r0)
            goto L_0x0016
        L_0x02f3:
            java.util.Map r5 = com.wiyun.ad.ac.c     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r7 = r14.g()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r5.put(r7, r6)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            goto L_0x006b
        L_0x0302:
            r13 = move-exception
            r13 = r2
        L_0x0304:
            java.lang.String r14 = "WiYun"
            java.lang.String r0 = "Failed to get ad"
            android.util.Log.w(r14, r0)     // Catch:{ all -> 0x0500 }
            org.apache.http.conn.ClientConnectionManager r14 = r3.getConnectionManager()
            r14.shutdown()
            if (r13 != 0) goto L_0x02da
            java.lang.String r14 = "WiYun"
            java.lang.String r0 = "Failed to get ad"
            android.util.Log.w(r14, r0)
            goto L_0x02da
        L_0x031c:
            java.lang.String r7 = "Android"
            goto L_0x00c6
        L_0x0320:
            java.lang.String r1 = "WiYun"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r7 = "Failed to get ad, statusCode: "
            r6.<init>(r7)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            android.util.Log.w(r1, r5)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            goto L_0x016b
        L_0x0336:
            r13 = move-exception
            r14 = r2
        L_0x0338:
            org.apache.http.conn.ClientConnectionManager r0 = r3.getConnectionManager()
            r0.shutdown()
            if (r14 != 0) goto L_0x0348
            java.lang.String r14 = "WiYun"
            java.lang.String r0 = "Failed to get ad"
            android.util.Log.w(r14, r0)
        L_0x0348:
            throw r13
        L_0x0349:
            boolean r14 = r14.i()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r14 != 0) goto L_0x043e
            java.lang.String r14 = r10.t     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            boolean r14 = android.text.TextUtils.isEmpty(r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r14 != 0) goto L_0x03c0
            java.io.File r14 = new java.io.File     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            java.io.File r0 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            java.lang.String r1 = r10.t     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            byte[] r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            java.lang.String r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            r14.<init>(r0, r1)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            if (r14 == 0) goto L_0x0378
            r0 = 0
            int r1 = r14.length     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            android.graphics.Bitmap r14 = android.graphics.BitmapFactory.decodeByteArray(r14, r0, r1)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            r10.s = r14     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
        L_0x0378:
            android.graphics.Bitmap r14 = r10.s     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            if (r14 != 0) goto L_0x050d
            org.apache.http.client.methods.HttpGet r14 = new org.apache.http.client.methods.HttpGet     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            java.lang.String r0 = r10.t     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            java.lang.String r0 = a(r0)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            r14.<init>(r0)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            org.apache.http.HttpResponse r14 = r3.execute(r14)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            org.apache.http.StatusLine r0 = r14.getStatusLine()     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            int r0 = r0.getStatusCode()     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            r1 = 300(0x12c, float:4.2E-43)
            if (r0 >= r1) goto L_0x050d
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            r0 = 0
            int r1 = r14.length     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeByteArray(r14, r0, r1)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            r10.s = r0     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            android.graphics.Bitmap r0 = r10.s     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            if (r0 == 0) goto L_0x050d
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            java.io.File r13 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            java.lang.String r1 = r10.t     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            byte[] r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            java.lang.String r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            r0.<init>(r13, r1)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            a(r0, r14)     // Catch:{ Throwable -> 0x0506, all -> 0x04fc }
            r13 = r10
            goto L_0x02ca
        L_0x03c0:
            java.lang.String r14 = r10.l     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            boolean r14 = android.text.TextUtils.isEmpty(r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r14 != 0) goto L_0x050a
            java.io.File r14 = new java.io.File     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.io.File r0 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r1 = r10.l     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            byte[] r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r14.<init>(r0, r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r14 == 0) goto L_0x03e9
            r0 = 0
            int r1 = r14.length     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            android.graphics.Bitmap r14 = android.graphics.BitmapFactory.decodeByteArray(r14, r0, r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.r = r14     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x03e9:
            android.graphics.Bitmap r14 = r10.r     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r14 != 0) goto L_0x042e
            org.apache.http.client.methods.HttpGet r14 = new org.apache.http.client.methods.HttpGet     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = r10.l     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = a(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r14.<init>(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            org.apache.http.HttpResponse r14 = r3.execute(r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            org.apache.http.StatusLine r0 = r14.getStatusLine()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            int r0 = r0.getStatusCode()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r1 = 300(0x12c, float:4.2E-43)
            if (r0 >= r1) goto L_0x042e
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r0 = 0
            int r1 = r14.length     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeByteArray(r14, r0, r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.r = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            android.graphics.Bitmap r0 = r10.r     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r0 == 0) goto L_0x042e
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.io.File r13 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r1 = r10.l     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            byte[] r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r0.<init>(r13, r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r0, r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x042e:
            android.graphics.Bitmap r13 = r10.r     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r13 == 0) goto L_0x050a
            r13 = 0
            r10.s = r13     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r13 = 0
            r10.t = r13     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r13 = 0
            r10.f = r13     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r13 = r10
            goto L_0x02ca
        L_0x043e:
            android.content.res.Resources r14 = r13.getResources()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = "icon"
            java.lang.String r1 = "drawable"
            java.lang.String r13 = r13.getPackageName()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            int r13 = r14.getIdentifier(r0, r1, r13)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r13 == 0) goto L_0x0456
            android.graphics.Bitmap r13 = android.graphics.BitmapFactory.decodeResource(r14, r13)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.s = r13     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x0456:
            r13 = r10
            goto L_0x02ca
        L_0x0459:
            boolean r14 = r14.i()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r14 != 0) goto L_0x04d4
            java.lang.String r14 = r10.l     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            boolean r14 = android.text.TextUtils.isEmpty(r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r14 != 0) goto L_0x050a
            java.io.File r14 = new java.io.File     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.io.File r0 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r1 = r10.l     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            byte[] r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r14.<init>(r0, r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r14 == 0) goto L_0x0488
            r0 = 0
            int r1 = r14.length     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            android.graphics.Bitmap r14 = android.graphics.BitmapFactory.decodeByteArray(r14, r0, r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.r = r14     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x0488:
            android.graphics.Bitmap r14 = r10.r     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r14 != 0) goto L_0x04cd
            org.apache.http.client.methods.HttpGet r14 = new org.apache.http.client.methods.HttpGet     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = r10.l     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = a(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r14.<init>(r0)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            org.apache.http.HttpResponse r14 = r3.execute(r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            org.apache.http.StatusLine r0 = r14.getStatusLine()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            int r0 = r0.getStatusCode()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r1 = 300(0x12c, float:4.2E-43)
            if (r0 >= r1) goto L_0x04cd
            byte[] r14 = a(r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r0 = 0
            int r1 = r14.length     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeByteArray(r14, r0, r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.r = r0     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            android.graphics.Bitmap r0 = r10.r     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r0 == 0) goto L_0x04cd
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.io.File r13 = r13.getCacheDir()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r1 = r10.l     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            byte[] r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r1 = com.wiyun.ad.ae.b(r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r0.<init>(r13, r1)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            a(r0, r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x04cd:
            android.graphics.Bitmap r13 = r10.r     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r13 == 0) goto L_0x050a
            r13 = r10
            goto L_0x02ca
        L_0x04d4:
            android.content.res.Resources r14 = r13.getResources()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            java.lang.String r0 = "icon"
            java.lang.String r1 = "drawable"
            java.lang.String r13 = r13.getPackageName()     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            int r13 = r14.getIdentifier(r0, r1, r13)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            if (r13 == 0) goto L_0x04ec
            android.graphics.Bitmap r13 = android.graphics.BitmapFactory.decodeResource(r14, r13)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r10.r = r13     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
        L_0x04ec:
            r13 = r10
            goto L_0x02ca
        L_0x04ef:
            r13 = r10
            goto L_0x02ca
        L_0x04f2:
            java.lang.String r13 = "WiYun"
            java.lang.String r14 = "Failed to get ad"
            android.util.Log.w(r13, r14)     // Catch:{ Throwable -> 0x0302, all -> 0x0336 }
            r13 = r2
            goto L_0x02ca
        L_0x04fc:
            r13 = move-exception
            r14 = r10
            goto L_0x0338
        L_0x0500:
            r14 = move-exception
            r12 = r14
            r14 = r13
            r13 = r12
            goto L_0x0338
        L_0x0506:
            r13 = move-exception
            r13 = r10
            goto L_0x0304
        L_0x050a:
            r13 = r2
            goto L_0x02ca
        L_0x050d:
            r13 = r10
            goto L_0x02ca
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.ad.ac.a(android.content.Context, com.wiyun.ad.AdView):com.wiyun.ad.ai");
    }

    private static String a(String str) {
        return str.replace(" ", "%20").replace("[", "%5B").replace("]", "%5D").replace("|", "%7C");
    }

    private static String a(HttpEntity httpEntity) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[2048];
            InputStream content = httpEntity.getContent();
            for (int i = 0; i != -1; i = content.read(bArr)) {
                byteArrayOutputStream.write(bArr, 0, i);
            }
            return a(byteArrayOutputStream.toByteArray());
        } catch (Exception e) {
            return "";
        }
    }

    private static String a(byte[] bArr) {
        return bArr == null ? "" : a(bArr, 0, bArr.length);
    }

    private static String a(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            return "";
        }
        try {
            return new String(bArr, i, i2, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    private static DefaultHttpClient a() {
        HttpHost d;
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
        HttpProtocolParams.setUseExpectContinue(basicHttpParams, false);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, b);
        HttpConnectionParams.setSoTimeout(basicHttpParams, b);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
        if (!ab.b() && ab.c() && (d = ab.d()) != null) {
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", d);
        }
        return defaultHttpClient;
    }

    static void a(Context context, ai aiVar) {
        if (aiVar != null && !TextUtils.isEmpty(aiVar.o)) {
            if (context.checkCallingOrSelfPermission("android.permission.INTERNET") == -1) {
                aa.a("Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />");
            }
            DefaultHttpClient a2 = a();
            try {
                StringBuilder sb = new StringBuilder(aiVar.o);
                sb.append('?').append("r").append("=").append(aiVar.c);
                a(sb, "p", aiVar.a);
                a(sb, "q", aiVar.b);
                a(sb, "ra", aiVar.e);
                a2.execute(new HttpGet(sb.toString()));
            } catch (Exception e) {
                Log.w("WiYun", "Failed to record ad click", e);
            } finally {
                a2.getConnectionManager().shutdown();
            }
        }
    }

    private static void a(StringBuilder sb, String str, int i) {
        sb.append("&").append(str).append("=").append(i);
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            sb.append("&").append(str).append("=").append(URLEncoder.encode(str2, "UTF-8"));
        }
    }

    private static boolean a(File file, String str) {
        try {
            a(file, str.getBytes("utf-8"));
            return true;
        } catch (UnsupportedEncodingException e) {
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0048 A[SYNTHETIC, Splitter:B:20:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0053 A[SYNTHETIC, Splitter:B:26:0x0053] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(java.io.File r5, byte[] r6) {
        /*
            java.io.File r0 = r5.getParentFile()
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0011
            java.io.File r0 = r5.getParentFile()
            r0.mkdirs()
        L_0x0011:
            r0 = 0
            boolean r1 = r5.exists()     // Catch:{ IOException -> 0x002d, all -> 0x004d }
            if (r1 != 0) goto L_0x001b
            r5.createNewFile()     // Catch:{ IOException -> 0x002d, all -> 0x004d }
        L_0x001b:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x002d, all -> 0x004d }
            r1.<init>(r5)     // Catch:{ IOException -> 0x002d, all -> 0x004d }
            r1.write(r6)     // Catch:{ IOException -> 0x0064, all -> 0x005d }
            r1.flush()     // Catch:{ IOException -> 0x0064, all -> 0x005d }
            if (r1 == 0) goto L_0x002b
            r1.close()     // Catch:{ IOException -> 0x0057 }
        L_0x002b:
            r0 = 1
        L_0x002c:
            return r0
        L_0x002d:
            r1 = move-exception
        L_0x002e:
            java.lang.String r1 = "WiYun"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x005f }
            java.lang.String r3 = "Failed to cache ad to file: "
            r2.<init>(r3)     // Catch:{ all -> 0x005f }
            java.lang.String r3 = r5.getAbsolutePath()     // Catch:{ all -> 0x005f }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x005f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x005f }
            android.util.Log.w(r1, r2)     // Catch:{ all -> 0x005f }
            if (r0 == 0) goto L_0x004b
            r0.close()     // Catch:{ IOException -> 0x0059 }
        L_0x004b:
            r0 = 0
            goto L_0x002c
        L_0x004d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.close()     // Catch:{ IOException -> 0x005b }
        L_0x0056:
            throw r0
        L_0x0057:
            r0 = move-exception
            goto L_0x002b
        L_0x0059:
            r0 = move-exception
            goto L_0x004b
        L_0x005b:
            r1 = move-exception
            goto L_0x0056
        L_0x005d:
            r0 = move-exception
            goto L_0x0051
        L_0x005f:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0051
        L_0x0064:
            r0 = move-exception
            r0 = r1
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.ad.ac.a(java.io.File, byte[]):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x002c A[SYNTHETIC, Splitter:B:25:0x002c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(java.io.File r4) {
        /*
            r2 = 0
            r0 = 0
            boolean r1 = r4.exists()     // Catch:{ FileNotFoundException -> 0x001f, all -> 0x0028 }
            if (r1 != 0) goto L_0x000f
            if (r2 == 0) goto L_0x000d
            r0.close()     // Catch:{ IOException -> 0x0030 }
        L_0x000d:
            r0 = r2
        L_0x000e:
            return r0
        L_0x000f:
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x001f, all -> 0x0028 }
            r0.<init>(r4)     // Catch:{ FileNotFoundException -> 0x001f, all -> 0x0028 }
            byte[] r1 = a(r0)     // Catch:{ FileNotFoundException -> 0x003d, all -> 0x0038 }
            if (r0 == 0) goto L_0x001d
            r0.close()     // Catch:{ IOException -> 0x0032 }
        L_0x001d:
            r0 = r1
            goto L_0x000e
        L_0x001f:
            r0 = move-exception
            r0 = r2
        L_0x0021:
            if (r0 == 0) goto L_0x0026
            r0.close()     // Catch:{ IOException -> 0x0034 }
        L_0x0026:
            r0 = r2
            goto L_0x000e
        L_0x0028:
            r0 = move-exception
            r1 = r2
        L_0x002a:
            if (r1 == 0) goto L_0x002f
            r1.close()     // Catch:{ IOException -> 0x0036 }
        L_0x002f:
            throw r0
        L_0x0030:
            r0 = move-exception
            goto L_0x000d
        L_0x0032:
            r0 = move-exception
            goto L_0x001d
        L_0x0034:
            r0 = move-exception
            goto L_0x0026
        L_0x0036:
            r1 = move-exception
            goto L_0x002f
        L_0x0038:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x002a
        L_0x003d:
            r1 = move-exception
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.wiyun.ad.ac.a(java.io.File):byte[]");
    }

    private static byte[] a(InputStream inputStream) {
        int i = 0;
        byte[] bArr = null;
        if (inputStream != null) {
            try {
                byte[] bArr2 = new byte[1024];
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while (i != -1) {
                    byteArrayOutputStream.write(bArr2, 0, i);
                    i = inputStream.read(bArr2);
                }
                bArr = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.flush();
                byteArrayOutputStream.close();
            } catch (Throwable th) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                    }
                }
                throw th;
            }
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException e2) {
            }
        }
        return bArr;
    }

    private static byte[] a(HttpResponse httpResponse) {
        byte[] bArr = new byte[1024];
        InputStream content = httpResponse.getEntity().getContent();
        if (content == null) {
            return null;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (int i = 0; i != -1; i = content.read(bArr)) {
                byteArrayOutputStream.write(bArr, 0, i);
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (content == null) {
                return byteArray;
            }
            try {
                content.close();
                return byteArray;
            } catch (IOException e) {
                return byteArray;
            }
        } catch (Exception e2) {
            if (content != null) {
                try {
                    content.close();
                } catch (IOException e3) {
                }
            }
            return null;
        } catch (Throwable th) {
            if (content != null) {
                try {
                    content.close();
                } catch (IOException e4) {
                }
            }
            throw th;
        }
    }
}
