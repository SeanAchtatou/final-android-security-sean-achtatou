package com.wiyun.ad;

import android.view.animation.DecelerateInterpolator;

final class c implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ AdView a;
    /* access modifiers changed from: private */
    public z b;
    private int c;

    public c(AdView adView, z zVar, int i) {
        this.a = adView;
        this.b = zVar;
        this.c = i;
    }

    public void run() {
        this.a.a.setVisibility(8);
        this.b.setVisibility(0);
        aj ajVar = new aj(90.0f, 0.0f, ((float) this.a.getWidth()) / 2.0f, ((float) this.a.getHeight()) / 2.0f, -0.4f * ((float) (this.c == 2 ? this.a.getHeight() : this.a.getWidth())), false, this.c != 2);
        ajVar.setDuration(700);
        ajVar.setFillAfter(true);
        ajVar.setInterpolator(new DecelerateInterpolator());
        ajVar.setAnimationListener(new g(this));
        this.a.startAnimation(ajVar);
    }
}
