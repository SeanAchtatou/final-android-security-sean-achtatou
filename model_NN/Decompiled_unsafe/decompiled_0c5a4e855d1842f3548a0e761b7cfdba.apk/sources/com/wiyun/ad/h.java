package com.wiyun.ad;

import android.view.animation.Animation;

class h implements Animation.AnimationListener {
    final /* synthetic */ AdView a;
    private final /* synthetic */ z b;

    h(AdView adView, z zVar) {
        this.a = adView;
        this.b = zVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void
     arg types: [com.wiyun.ad.AdView, int]
     candidates:
      com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.view.View):void
      com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.widget.LinearLayout):void
      com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.ai):void
      com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.z):void
      com.wiyun.ad.AdView.a(com.wiyun.ad.z, int):void
      com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void */
    public void onAnimationEnd(Animation animation) {
        this.a.a.clearAnimation();
        this.a.removeView(this.a.a);
        this.a.m();
        if (this.a.a.a() != null) {
            this.a.a.a().a();
        }
        this.a.a = this.b;
        this.a.c = false;
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
