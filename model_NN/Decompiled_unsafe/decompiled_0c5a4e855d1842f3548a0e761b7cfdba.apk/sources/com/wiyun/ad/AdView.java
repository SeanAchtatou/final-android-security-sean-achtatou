package com.wiyun.ad;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import java.io.File;
import java.util.Timer;

public class AdView extends FrameLayout implements View.OnClickListener {
    /* access modifiers changed from: private */
    public z a;
    private boolean b;
    /* access modifiers changed from: private */
    public boolean c;
    private int d;
    private Timer e;
    private int f;
    private int g;
    private boolean h;
    private boolean i;
    private int j;
    private boolean k;
    private int l;
    private String m;
    private String n;
    private String o;
    private boolean p;
    /* access modifiers changed from: private */
    public af q;
    private x r;
    /* access modifiers changed from: private */
    public boolean s;
    /* access modifiers changed from: private */
    public ai t;
    /* access modifiers changed from: private */
    public View u;
    private View v;
    private Drawable w;

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f = 0;
        this.g = 0;
        this.l = 2;
        this.m = "*/*";
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "textColor", 0);
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", 0);
            if (attributeUnsignedIntValue != 0) {
                b(attributeUnsignedIntValue);
            }
            if (attributeUnsignedIntValue2 != 0) {
                setBackgroundColor(attributeUnsignedIntValue2);
            }
            this.n = attributeSet.getAttributeValue(str, "resId");
            if (this.n != null) {
                this.n = this.n.trim();
            }
            this.k = attributeSet.getAttributeBooleanValue(str, "testMode", false);
            this.l = attributeSet.getAttributeIntValue(str, "testAdType", 2);
            this.p = attributeSet.getAttributeBooleanValue(str, "showLoadingHint", false);
            a(attributeSet.getAttributeIntValue(str, "refreshInterval", 0));
            a(attributeSet.getAttributeBooleanValue(str, "goneIfFail", e()));
            this.j = attributeSet.getAttributeIntValue(str, "transition", 1);
            this.i = attributeSet.getAttributeBooleanValue(str, "autoStart", false);
        }
        File[] listFiles = getContext().getCacheDir().listFiles();
        if (listFiles != null) {
            long currentTimeMillis = System.currentTimeMillis();
            for (File file : listFiles) {
                if (file.lastModified() - currentTimeMillis > 86400000) {
                    file.delete();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(LinearLayout linearLayout) {
        if (!this.s) {
            this.s = true;
            TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, 0.0f, 1, 0.0f, 1, -1.0f);
            translateAnimation.setDuration(200);
            translateAnimation.setFillAfter(true);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setAnimationListener(new p(this, linearLayout));
            linearLayout.startAnimation(translateAnimation);
        }
    }

    private void a(z zVar) {
        this.a = zVar;
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(350);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        startAnimation(alphaAnimation);
    }

    private void a(z zVar, int i2) {
        TranslateAnimation translateAnimation;
        TranslateAnimation translateAnimation2;
        switch (i2) {
            case 3:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, 1.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation2 = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                break;
            case 4:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, -1.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation2 = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                break;
            case 5:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
                translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                break;
            case 6:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
                translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                break;
            default:
                translateAnimation = null;
                translateAnimation2 = null;
                break;
        }
        translateAnimation.setDuration(700);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new DecelerateInterpolator());
        translateAnimation2.setDuration(700);
        translateAnimation2.setFillAfter(true);
        translateAnimation2.setInterpolator(new DecelerateInterpolator());
        translateAnimation2.setAnimationListener(new i(this, zVar));
        this.a.startAnimation(translateAnimation);
        zVar.startAnimation(translateAnimation2);
    }

    private void b(z zVar) {
        if (!this.c) {
            this.c = true;
            int i2 = this.j;
            if (i2 == 0) {
                i2 = (Math.abs((int) (SystemClock.uptimeMillis() / 1000)) % 7) + 1;
            }
            switch (i2) {
                case 3:
                case 4:
                case 5:
                case 6:
                    a(zVar, i2);
                    return;
                case 7:
                    c(zVar);
                    return;
                default:
                    b(zVar, i2);
                    return;
            }
        }
    }

    private void b(z zVar, int i2) {
        zVar.setVisibility(8);
        aj ajVar = new aj(0.0f, -90.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, -0.4f * ((float) (i2 == 2 ? getHeight() : getWidth())), true, i2 != 2);
        ajVar.setDuration(700);
        ajVar.setFillAfter(true);
        ajVar.setInterpolator(new AccelerateInterpolator());
        ajVar.setAnimationListener(new j(this, zVar, i2));
        startAnimation(ajVar);
    }

    private void b(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.d > 0) {
                    if (this.e == null) {
                        this.e = new Timer();
                        this.e.schedule(new q(this), (long) (this.d * 1000), (long) (this.d * 1000));
                    }
                }
            }
            if (this.e != null) {
                this.e.cancel();
                this.e = null;
            }
        }
    }

    private void c(z zVar) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(700);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation2.setDuration(700);
        alphaAnimation2.setFillAfter(true);
        alphaAnimation2.setInterpolator(new AccelerateInterpolator());
        alphaAnimation2.setAnimationListener(new h(this, zVar));
        this.a.startAnimation(alphaAnimation);
        zVar.startAnimation(alphaAnimation2);
    }

    /* access modifiers changed from: private */
    public void l() {
        if (!(this.r == null || this.r.getParent() == null)) {
            this.r.a(false);
            removeView(this.r);
        }
        if (this.a != null) {
            this.a.setVisibility(0);
        }
        if (this.t != null) {
            synchronized (this) {
                if (this.a == null || !this.t.equals(this.a.a())) {
                    boolean z = this.a == null;
                    z zVar = new z(getContext(), this.t, this.q);
                    int d2 = d();
                    if (d2 == 0) {
                        d2 = this.t.k;
                    }
                    if (d2 == 0) {
                        d2 = -16777216;
                    }
                    int c2 = c();
                    if (c2 == 0) {
                        c2 = this.t.h;
                    }
                    if (c2 == 0) {
                        c2 = -1;
                    }
                    zVar.setBackgroundColor(d2);
                    zVar.a(c2);
                    int d3 = super.getVisibility();
                    zVar.setVisibility(d3);
                    try {
                        if (this.q != null) {
                            try {
                                this.q.c();
                            } catch (Exception e2) {
                                Log.w("WiYun", "Unhandled exception raised in your AdListener.onAdLoaded().", e2);
                            }
                        }
                        addView(zVar);
                        if (this.t.d == 3 && this.v == null) {
                            this.v = n();
                        }
                        if (d3 != 0) {
                            removeView(this.a);
                            m();
                            if (this.a.a() != null) {
                                this.a.a().a();
                            }
                            this.a = zVar;
                        } else if (z) {
                            a(zVar);
                        } else if (this.p) {
                            removeView(this.a);
                            m();
                            if (this.a.a() != null) {
                                this.a.a().a();
                            }
                            a(zVar);
                        } else {
                            b(zVar);
                        }
                    } catch (Exception e3) {
                        Log.e("WiYun", "Unhandled exception placing AdContainer into AdView.", e3);
                    }
                } else {
                    if (this.a.getParent() == null) {
                        addView(this.a);
                    }
                    this.t.a();
                }
            }
        } else {
            if (this.a != null && this.a.getParent() == null) {
                addView(this.a);
            }
            if (this.q != null) {
                try {
                    this.q.b();
                } catch (Exception e4) {
                    Log.w("WiYun", "Unhandled exception raised in your AdListener.onLoadAdFailed().", e4);
                }
            }
        }
        this.b = false;
    }

    /* access modifiers changed from: private */
    public void m() {
        if (this.t != null && this.t.d != 3 && this.v != null) {
            removeView(this.v);
            this.v = null;
            if (this.w != null) {
                this.w.setCallback(null);
                this.w = null;
            }
        }
    }

    private View n() {
        Bitmap bitmap;
        Bitmap bitmap2;
        Bitmap bitmap3;
        if (this.w == null) {
            try {
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(t.c, 0, t.c.length);
                try {
                    bitmap2 = BitmapFactory.decodeByteArray(t.a, 0, t.a.length);
                    try {
                        Bitmap decodeByteArray2 = BitmapFactory.decodeByteArray(t.b, 0, t.b.length);
                        try {
                            BitmapDrawable bitmapDrawable = new BitmapDrawable(decodeByteArray);
                            BitmapDrawable bitmapDrawable2 = new BitmapDrawable(bitmap2);
                            BitmapDrawable bitmapDrawable3 = new BitmapDrawable(decodeByteArray2);
                            StateListDrawable stateListDrawable = new StateListDrawable();
                            stateListDrawable.addState(new int[]{16842919}, bitmapDrawable3);
                            stateListDrawable.addState(new int[]{16842908}, bitmapDrawable2);
                            stateListDrawable.addState(new int[]{16842913}, bitmapDrawable2);
                            stateListDrawable.addState(new int[]{16842919, 16842909}, bitmapDrawable3);
                            stateListDrawable.addState(new int[]{16842908, 16842909}, bitmapDrawable2);
                            stateListDrawable.addState(new int[]{16842913, 16842909}, bitmapDrawable2);
                            stateListDrawable.addState(new int[0], bitmapDrawable);
                            this.w = stateListDrawable;
                        } catch (OutOfMemoryError e2) {
                            Bitmap bitmap4 = decodeByteArray2;
                            bitmap = decodeByteArray;
                            bitmap3 = bitmap4;
                            if (bitmap != null && !bitmap.isRecycled()) {
                                bitmap.recycle();
                            }
                            if (bitmap2 != null && !bitmap2.isRecycled()) {
                                bitmap2.recycle();
                            }
                            if (bitmap3 != null && !bitmap3.isRecycled()) {
                                bitmap3.recycle();
                            }
                            Button button = new Button(getContext());
                            button.setBackgroundDrawable(this.w);
                            button.setOnClickListener(this);
                            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
                            layoutParams.gravity = 53;
                            addView(button, layoutParams);
                            return button;
                        }
                    } catch (OutOfMemoryError e3) {
                        bitmap = decodeByteArray;
                        bitmap3 = null;
                    }
                } catch (OutOfMemoryError e4) {
                    bitmap2 = null;
                    bitmap = decodeByteArray;
                    bitmap3 = null;
                }
            } catch (OutOfMemoryError e5) {
                bitmap3 = null;
                bitmap2 = null;
                bitmap = null;
            }
        }
        Button button2 = new Button(getContext());
        button2.setBackgroundDrawable(this.w);
        button2.setOnClickListener(this);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 53;
        addView(button2, layoutParams2);
        return button2;
    }

    public void a() {
        if (!this.b && !this.c) {
            this.t = null;
            this.b = true;
            if (this.p) {
                if (this.r == null) {
                    this.r = new x(w.a(), 1, getContext());
                    this.r.a(-1);
                    this.r.a(16.0f);
                }
                if (this.u != null) {
                    removeView(this.u);
                    this.u = null;
                }
                if (this.a != null) {
                    this.a.setVisibility(4);
                }
                if (this.r.getParent() == null) {
                    addView(this.r, new FrameLayout.LayoutParams(-1, -1));
                }
                this.r.a(true);
            }
            new k(this).start();
        }
    }

    public void a(int i2) {
        int i3;
        if (i2 <= 0) {
            i3 = 0;
        } else {
            if (i2 < 30) {
                aa.a("AdView.setRefreshInterval(" + i2 + ") seconds must be >= " + 30);
            }
            i3 = i2;
        }
        this.d = i3;
        if (i3 == 0) {
            b(false);
        } else {
            b(true);
        }
    }

    public void a(af afVar) {
        synchronized (this) {
            this.q = afVar;
        }
    }

    public void a(boolean z) {
        this.h = z;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (!this.s) {
            this.s = true;
            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setGravity(16);
            linearLayout.setOrientation(0);
            linearLayout.setPadding(5, 5, 0, 0);
            linearLayout.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3879462, -7167046}));
            EditText editText = new EditText(getContext());
            editText.setTextAppearance(getContext(), 16973895);
            editText.setTypeface(Typeface.DEFAULT_BOLD);
            editText.setSingleLine(true);
            editText.setHint(this.t.g);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.weight = 1.0f;
            editText.setLayoutParams(layoutParams);
            linearLayout.addView(editText);
            editText.setOnKeyListener(new n(this, linearLayout));
            editText.setOnFocusChangeListener(new o(this, linearLayout));
            addView(linearLayout, new FrameLayout.LayoutParams(-1, getHeight()));
            TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, 0.0f, 1, -1.0f, 1, 0.0f);
            translateAnimation.setDuration(200);
            translateAnimation.setFillAfter(true);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setAnimationListener(new l(this, editText));
            editText.startAnimation(translateAnimation);
            this.u = editText;
        }
    }

    public void b(int i2) {
        this.g = i2;
        if (this.a != null) {
            this.a.a(i2);
        }
        invalidate();
    }

    public int c() {
        return this.g;
    }

    public int d() {
        return this.f;
    }

    public boolean e() {
        return this.h;
    }

    public boolean f() {
        return this.a != null;
    }

    public String g() {
        return this.n;
    }

    public int getVisibility() {
        if (f()) {
            return super.getVisibility();
        }
        if (!this.h) {
            return super.getVisibility();
        }
        if (!this.b || !this.p) {
            return 8;
        }
        return super.getVisibility();
    }

    /* access modifiers changed from: package-private */
    public String h() {
        return this.m;
    }

    public boolean i() {
        return this.k;
    }

    public int j() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public String k() {
        return this.o;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ab.a(getContext(), (e) null);
        if (this.i) {
            a();
        }
    }

    public void onClick(View view) {
        if (this.q != null) {
            this.q.d();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        ab.a();
        ac.a = false;
        super.onDetachedFromWindow();
    }

    public void onWindowFocusChanged(boolean z) {
        b(z);
    }

    public void setBackgroundColor(int i2) {
        super.setBackgroundColor(i2);
        this.f = i2;
        if (this.a != null) {
            this.a.setBackgroundColor(i2);
        }
        invalidate();
    }

    public void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
            }
        }
    }
}
