package com.wiyun.ad;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.LinearLayout;

class p implements Animation.AnimationListener {
    final /* synthetic */ AdView a;
    private final /* synthetic */ LinearLayout b;

    p(AdView adView, LinearLayout linearLayout) {
        this.a = adView;
        this.b = linearLayout;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.wiyun.ad.AdView.b(com.wiyun.ad.AdView, boolean):void
     arg types: [com.wiyun.ad.AdView, int]
     candidates:
      com.wiyun.ad.AdView.b(com.wiyun.ad.z, int):void
      com.wiyun.ad.AdView.b(com.wiyun.ad.AdView, boolean):void */
    public void onAnimationEnd(Animation animation) {
        ViewGroup viewGroup = (ViewGroup) this.b.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(this.b);
        }
        this.a.s = false;
        this.a.u = (View) null;
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
