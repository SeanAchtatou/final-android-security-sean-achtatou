package com.wiyun.ad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.Proxy;
import android.text.TextUtils;
import org.apache.http.HttpHost;

public class ab {
    private static ab a;
    private Context b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public e d;
    private BroadcastReceiver e = new ak(this);

    private ab(Context context, e eVar) {
        this.b = context.getApplicationContext();
        this.d = eVar;
        this.b.registerReceiver(this.e, new IntentFilter("android.net.wifi.STATE_CHANGE"));
        this.c = aa.d(context);
    }

    public static void a() {
        if (a != null) {
            a.b.unregisterReceiver(a.e);
            a.b = null;
            a.d = null;
            a = null;
        }
    }

    public static void a(Context context, e eVar) {
        if (a == null) {
            a = new ab(context, eVar);
        }
    }

    public static boolean b() {
        if (a == null) {
            return false;
        }
        return a.c;
    }

    public static boolean c() {
        return !TextUtils.isEmpty(Proxy.getDefaultHost());
    }

    public static HttpHost d() {
        return new HttpHost(Proxy.getDefaultHost(), Proxy.getDefaultPort());
    }
}
