package com.wiyun.ad;

import android.view.View;
import android.widget.LinearLayout;

class o implements View.OnFocusChangeListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ AdView a;
    private final /* synthetic */ LinearLayout b;

    o(AdView adView, LinearLayout linearLayout) {
        this.a = adView;
        this.b = linearLayout;
    }

    public void onFocusChange(View view, boolean z) {
        if (z) {
            view.post(new al(this, view));
            return;
        }
        view.post(new am(this, view));
        if (view.getParent() != null) {
            this.a.a(this.b);
        }
    }
}
