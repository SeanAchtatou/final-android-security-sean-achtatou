package com.wiyun.ad;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import java.net.URLEncoder;

class n implements View.OnKeyListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ AdView a;
    private final /* synthetic */ LinearLayout b;

    n(AdView adView, LinearLayout linearLayout) {
        this.a = adView;
        this.b = linearLayout;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            if (i == 66) {
                String editable = ((EditText) view).getEditableText().toString();
                if (!TextUtils.isEmpty(editable)) {
                    Context applicationContext = this.a.getContext().getApplicationContext();
                    new d(this, applicationContext).start();
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.addFlags(268435456);
                    try {
                        intent.setData(Uri.parse(this.a.t.p.replace("%query%", URLEncoder.encode(editable, "utf-8"))));
                        applicationContext.startActivity(intent);
                    } catch (Exception e) {
                        Log.e("WiYun", "Could not open browser on ad click to " + this.a.t.p, e);
                    }
                    if (this.a.q != null) {
                        this.a.q.a();
                    }
                    this.a.a(this.b);
                }
                return true;
            } else if (i == 4) {
                this.a.a(this.b);
                return true;
            }
        }
        return false;
    }
}
