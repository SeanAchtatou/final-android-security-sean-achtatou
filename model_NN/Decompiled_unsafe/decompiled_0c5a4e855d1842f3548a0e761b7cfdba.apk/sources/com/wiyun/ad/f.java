package com.wiyun.ad;

import android.view.animation.Interpolator;

final class f implements Interpolator {
    private float a;

    public f(int i) {
        this.a = 1.0f / ((float) i);
    }

    public float getInterpolation(float f) {
        return ((float) ((int) (f / this.a))) * this.a;
    }
}
