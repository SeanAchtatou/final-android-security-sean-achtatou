package com.admob.android.ads;

import android.app.Activity;
import android.view.KeyEvent;
import com.admob.android.ads.a.a;
import java.lang.ref.WeakReference;

public final class s extends a {
    String a;
    boolean b = true;
    private ae e;

    public s(Activity activity, String str, ae aeVar) {
        super(activity, false, new WeakReference(activity));
        this.a = str;
        this.e = aeVar;
    }

    /* access modifiers changed from: protected */
    public final ba a(WeakReference weakReference) {
        return new bt(this, this, weakReference);
    }

    public final void a() {
        if (this.e != null) {
            this.e.a();
        }
    }

    public final void a(String str) {
        this.c = str + "#sdk";
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        a();
        return true;
    }
}
