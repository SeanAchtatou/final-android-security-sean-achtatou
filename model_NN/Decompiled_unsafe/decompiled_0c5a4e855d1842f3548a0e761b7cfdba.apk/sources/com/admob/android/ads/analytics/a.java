package com.admob.android.ads.analytics;

import android.util.Log;
import com.admob.android.ads.b;
import com.admob.android.ads.bh;
import com.admob.android.ads.n;

final class a implements b {
    a(InstallReceiver installReceiver) {
    }

    public final void a(n nVar) {
        if (bh.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Recorded install from an AdMob ad.");
        }
    }

    public final void a(n nVar, Exception exc) {
        if (bh.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Failed to record install from an AdMob ad.", exc);
        }
    }
}
