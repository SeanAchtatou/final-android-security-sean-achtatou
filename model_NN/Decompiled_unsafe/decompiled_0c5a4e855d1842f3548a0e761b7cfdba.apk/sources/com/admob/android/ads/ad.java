package com.admob.android.ads;

import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

public class ad implements h {
    private WeakReference a;

    public ad() {
    }

    public ad(AdView adView) {
        this.a = new WeakReference(adView);
    }

    public static Bundle a(i iVar) {
        if (iVar == null) {
            return null;
        }
        return iVar.a();
    }

    public static ArrayList a(Vector vector) {
        if (vector == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = vector.iterator();
        while (it.hasNext()) {
            i iVar = (i) it.next();
            if (iVar == null) {
                arrayList.add(null);
            } else {
                arrayList.add(iVar.a());
            }
        }
        return arrayList;
    }

    public final void a() {
        AdView adView = (AdView) this.a.get();
        if (adView != null) {
            AdView.f(adView);
        }
    }

    public final void a(e eVar) {
        AdView adView = (AdView) this.a.get();
        if (adView != null) {
            synchronized (adView) {
                if (adView.b == null || !eVar.equals(adView.b.b())) {
                    if (bh.a("AdMobSDK", 4)) {
                        Log.i("AdMobSDK", "Ad returned (" + (SystemClock.uptimeMillis() - adView.o) + " ms):  " + eVar);
                    }
                    adView.getContext();
                    adView.a(eVar, eVar.b());
                } else if (bh.a("AdMobSDK", 3)) {
                    Log.d("AdMobSDK", "Received the same ad we already had.  Discarding it.");
                }
            }
        }
    }
}
