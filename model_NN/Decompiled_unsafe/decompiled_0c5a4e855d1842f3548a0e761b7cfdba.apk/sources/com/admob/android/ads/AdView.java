package com.admob.android.ads;

import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AdView extends RelativeLayout {
    private static Boolean a;
    private static Handler s = null;
    /* access modifiers changed from: private */
    public f b;
    /* access modifiers changed from: private */
    public int c;
    private boolean d;
    private x e;
    private int f;
    private int g;
    private int h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public cm k;
    private boolean l;
    private boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public long o;
    private ad p;
    private ch q;
    private u r;

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, u.a);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2, u uVar) {
        super(context, attributeSet, i2);
        int i3;
        int i4;
        int i5;
        this.m = true;
        if (a == null) {
            a = new Boolean(isInEditMode());
        }
        if (s == null && !a.booleanValue()) {
            Handler handler = new Handler();
            s = handler;
            e.a(handler);
        }
        this.r = uVar;
        if (uVar != u.a) {
            this.q = ch.VIEW;
        }
        setDescendantFocusability(262144);
        setClickable(true);
        setLongClickable(false);
        setGravity(17);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            if (attributeSet.getAttributeBooleanValue(str, "testing", false) && bh.a("AdMobSDK", 5)) {
                Log.w("AdMobSDK", "AdView's \"testing\" XML attribute has been deprecated and will be ignored.  Please delete it from your XML layout and use AdManager.setTestDevices instead.");
            }
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", -16777216);
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", -1);
            if (attributeUnsignedIntValue2 >= 0) {
                b(attributeUnsignedIntValue2);
            }
            int attributeUnsignedIntValue3 = attributeSet.getAttributeUnsignedIntValue(str, "primaryTextColor", -1);
            int attributeUnsignedIntValue4 = attributeSet.getAttributeUnsignedIntValue(str, "secondaryTextColor", -1);
            this.i = attributeSet.getAttributeValue(str, "keywords");
            a(attributeSet.getAttributeIntValue(str, "refreshInterval", 0));
            boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue(str, "isGoneWithoutAd", false);
            if (attributeBooleanValue) {
                a(attributeBooleanValue);
            }
            i3 = attributeUnsignedIntValue4;
            int i6 = attributeUnsignedIntValue3;
            i5 = attributeUnsignedIntValue;
            i4 = i6;
        } else {
            i3 = -1;
            i4 = -1;
            i5 = -16777216;
        }
        setBackgroundColor(i5);
        c(i4);
        d(i3);
        this.b = null;
        this.p = null;
        if (a.booleanValue()) {
            TextView textView = new TextView(context, attributeSet, i2);
            textView.setBackgroundColor(d());
            textView.setTextColor(b());
            textView.setPadding(10, 10, 10, 10);
            textView.setTextSize(16.0f);
            textView.setGravity(16);
            textView.setText("Ads by AdMob");
            addView(textView, new RelativeLayout.LayoutParams(-1, -1));
            return;
        }
        g();
    }

    static /* synthetic */ void a(AdView adView, e eVar) {
        if (adView.k == null) {
            return;
        }
        if (adView.b == null || adView.b.getParent() == null) {
            try {
                adView.k.c(adView);
            } catch (Exception e2) {
                Log.w("AdMobSDK", "Unhandled exception raised in your AdListener.onReceiveAd.", e2);
            }
        } else {
            try {
                adView.k.d(adView);
            } catch (Exception e3) {
                Log.w("AdMobSDK", "Unhandled exception raised in your AdListener.onReceiveRefreshedAd.", e3);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(f fVar) {
        this.b = fVar;
        if (this.l) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            startAnimation(alphaAnimation);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.ap.<init>(float, float, float, float, float, boolean):void
     arg types: [int, int, float, float, float, int]
     candidates:
      com.admob.android.ads.ap.<init>(float[], float[], float, float, float, boolean):void
      com.admob.android.ads.ap.<init>(float, float, float, float, float, boolean):void */
    static /* synthetic */ void b(AdView adView, f fVar) {
        fVar.setVisibility(8);
        ap apVar = new ap(0.0f, -90.0f, ((float) adView.getWidth()) / 2.0f, ((float) adView.getHeight()) / 2.0f, -0.4f * ((float) adView.getWidth()), true);
        apVar.setDuration(700);
        apVar.setFillAfter(true);
        apVar.setInterpolator(new AccelerateInterpolator());
        apVar.setAnimationListener(new bw(adView, fVar));
        adView.startAnimation(apVar);
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.c > 0 && getVisibility() == 0) {
                    int i2 = this.c;
                    h();
                    if (i()) {
                        this.e = new x(this);
                        s.postDelayed(this.e, (long) i2);
                        if (bh.a("AdMobSDK", 3)) {
                            Log.d("AdMobSDK", "Ad refresh scheduled for " + i2 + " from now.");
                        }
                    }
                }
            }
            if (!z || this.c == 0) {
                h();
            }
        }
    }

    static /* synthetic */ ad c(AdView adView) {
        if (adView.p == null) {
            adView.p = new ad(adView);
        }
        return adView.p;
    }

    static /* synthetic */ void f(AdView adView) {
        if (adView.k != null) {
            s.post(new z(adView));
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        l.a(getContext());
        if (this.m || super.getVisibility() == 0) {
            if (!this.n) {
                this.n = true;
                this.o = SystemClock.uptimeMillis();
                new ab(this).start();
            } else if (bh.a("AdMobSDK", 5)) {
                Log.w("AdMobSDK", "Ignoring requestFreshAd() because we are requesting an ad right now already.");
            }
        } else if (bh.a("AdMobSDK", 5)) {
            Log.w("AdMobSDK", "Cannot requestFreshAd() when the AdView is not visible.  Call AdView.setVisibility(View.VISIBLE) first.");
        }
    }

    private void h() {
        if (this.e != null) {
            this.e.a = true;
            this.e = null;
            if (bh.a("AdMobSDK", 2)) {
                Log.v("AdMobSDK", "Cancelled an ad refresh scheduled for the future.");
            }
        }
    }

    private boolean i() {
        e b2;
        if (this.b == null || (b2 = this.b.b()) == null || !b2.d() || this.b.h() >= 120) {
            return true;
        }
        if (bh.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Cannot refresh CPM ads.  Ignoring request to refresh the ad.");
        }
        return false;
    }

    public void a() {
        if (!this.d) {
            long uptimeMillis = (SystemClock.uptimeMillis() - this.o) / 1000;
            if (uptimeMillis <= 0 || uptimeMillis >= 13) {
                if (i()) {
                    g();
                }
            } else if (bh.a("AdMobSDK", 3)) {
                Log.d("AdMobSDK", "Ignoring requestFreshAd.  Called " + uptimeMillis + " seconds since last refresh.  " + "Refreshes must be at least " + 13 + " apart.");
            }
        } else if (bh.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Request interval overridden by the server.  Ignoring requestFreshAd.");
        }
    }

    public void a(int i2) {
        int i3 = i2 * 1000;
        if (this.c != i3) {
            if (i2 > 0) {
                if (i2 < 13) {
                    if (bh.a("AdMobSDK", 5)) {
                        Log.w("AdMobSDK", "AdView.setRequestInterval(" + i2 + ") seconds must be >= " + 13);
                    }
                    i3 = 13000;
                } else if (i2 > 600) {
                    if (bh.a("AdMobSDK", 5)) {
                        Log.w("AdMobSDK", "AdView.setRequestInterval(" + i2 + ") seconds must be <= " + 600);
                    }
                    i3 = 600000;
                }
            }
            this.c = i3;
            if (i2 <= 0) {
                h();
            }
            if (bh.a("AdMobSDK", 4)) {
                Log.i("AdMobSDK", "Requesting fresh ads every " + i2 + " seconds.");
            }
        }
    }

    public void a(cm cmVar) {
        synchronized (this) {
            this.k = cmVar;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(e eVar, f fVar) {
        int visibility = super.getVisibility();
        double a2 = eVar.a();
        if (a2 >= 0.0d) {
            this.d = true;
            a((int) a2);
            b(true);
        } else {
            this.d = false;
        }
        boolean z = this.m;
        if (z) {
            this.m = false;
        }
        fVar.a(eVar);
        fVar.setVisibility(visibility);
        fVar.setGravity(17);
        eVar.a(fVar);
        fVar.setLayoutParams(new RelativeLayout.LayoutParams(eVar.a(eVar.e()), eVar.a(eVar.f())));
        s.post(new w(this, fVar, visibility, z));
    }

    public void a(boolean z) {
        if (bh.a("AdMobSDK", 5)) {
            Log.w("AdMobSDK", "Deprecated method setGoneWithoutAd was called.  See JavaDoc for instructions to remove.");
        }
    }

    public int b() {
        return this.g;
    }

    public void b(int i2) {
        if (bh.a("AdMobSDK", 5)) {
            Log.w("AdMobSDK", "Calling the deprecated method setTextColor!  Please use setPrimaryTextColor and setSecondaryTextColor instead.");
        }
        c(i2);
        d(i2);
    }

    public int c() {
        return this.h;
    }

    public void c(int i2) {
        this.g = -16777216 | i2;
    }

    public int d() {
        return this.f;
    }

    public void d(int i2) {
        this.h = -16777216 | i2;
    }

    /* access modifiers changed from: package-private */
    public final ch e() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public final u f() {
        return this.r;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.l = true;
        b(true);
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.l = false;
        b(false);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (bh.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "AdView size is " + measuredWidth + " by " + measuredHeight);
        }
        if (a.booleanValue()) {
            return;
        }
        if (((float) ((int) (((float) measuredWidth) / f.c()))) <= 310.0f) {
            if (bh.a("AdMobSDK", 3)) {
                Log.d("AdMobSDK", "We need to have a minimum width of 320 device independent pixels to show an ad.");
            }
            try {
                this.b.setVisibility(8);
            } catch (NullPointerException e2) {
            }
        } else {
            try {
                int visibility = this.b.getVisibility();
                this.b.setVisibility(super.getVisibility());
                if (visibility != 0 && this.b.getVisibility() == 0) {
                    a(this.b);
                }
            } catch (NullPointerException e3) {
            }
        }
    }

    public void onWindowFocusChanged(boolean z) {
        b(z);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        b(i2 == 0);
    }

    public void setBackgroundColor(int i2) {
        this.f = -16777216 | i2;
        invalidate();
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (z) {
            setVisibility(0);
        } else {
            setVisibility(8);
        }
    }

    public void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
                invalidate();
            }
        }
        b(i2 == 0);
    }
}
