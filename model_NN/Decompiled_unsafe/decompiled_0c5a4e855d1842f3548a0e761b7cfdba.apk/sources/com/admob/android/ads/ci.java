package com.admob.android.ads;

public enum ci {
    CLICK_TO_MAP("map"),
    CLICK_TO_VIDEO("video"),
    CLICK_TO_APP("app"),
    CLICK_TO_BROWSER("url"),
    CLICK_TO_CALL("call"),
    CLICK_TO_MUSIC("itunes"),
    CLICK_TO_CANVAS("canvas"),
    CLICK_TO_CONTACT("contact"),
    CLICK_TO_INTERACTIVE_VIDEO("movie"),
    CLICK_TO_FULLSCREEN_BROWSER("screen");
    
    private String k;

    private ci(String str) {
        this.k = str;
    }

    public static ci a(String str) {
        for (ci ciVar : values()) {
            if (ciVar.toString().equals(str)) {
                return ciVar;
            }
        }
        return null;
    }

    public final String toString() {
        return this.k;
    }
}
