package com.admob.android.ads.a;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.admob.android.ads.ba;
import com.admob.android.ads.bh;
import com.admob.android.ads.e;
import com.admob.android.ads.o;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

public class a extends WebView implements View.OnClickListener {
    private boolean a;
    private WeakReference b;
    public String c;
    protected ba d;

    public a(Context context, boolean z, WeakReference weakReference) {
        super(context);
        this.a = z;
        this.b = weakReference;
        WebSettings settings = getSettings();
        settings.setLoadsImagesAutomatically(true);
        settings.setPluginsEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        settings.setUserAgentString(o.h());
        this.d = a(weakReference);
        setWebViewClient(this.d);
    }

    public static View a(Context context, String str, boolean z, boolean z2, Point point, float f, WeakReference weakReference) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setGravity(17);
        a aVar = new a(context, z2, weakReference);
        aVar.setBackgroundColor(0);
        relativeLayout.addView(aVar, new RelativeLayout.LayoutParams(-1, -1));
        if (z2) {
            ImageButton imageButton = new ImageButton(context);
            imageButton.setImageResource(17301527);
            imageButton.setBackgroundDrawable(null);
            imageButton.setPadding(0, 0, 0, 0);
            imageButton.setOnClickListener(aVar);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(e.a(point.x, (double) f), e.a(point.y, (double) f), 0, 0);
            relativeLayout.addView(imageButton, layoutParams);
        }
        aVar.c = str;
        aVar.loadUrl(str);
        return relativeLayout;
    }

    private String a(Object obj) {
        if (obj == null) {
            return "{}";
        }
        if ((obj instanceof Integer) || (obj instanceof Double)) {
            return obj.toString();
        }
        if (obj instanceof String) {
            return "'" + ((String) obj) + "'";
        } else if (obj instanceof Map) {
            String str = "{";
            Iterator it = ((Map) obj).entrySet().iterator();
            while (true) {
                String str2 = str;
                if (!it.hasNext()) {
                    return str2.concat("}");
                }
                Map.Entry entry = (Map.Entry) it.next();
                Object key = entry.getKey();
                Object value = entry.getValue();
                String a2 = a(key);
                str = str2.concat(a2 + ":" + a(value));
                if (it.hasNext()) {
                    str = str.concat(",");
                }
            }
        } else if (obj instanceof JSONObject) {
            return ((JSONObject) obj).toString();
        } else {
            if (bh.a("AdMobSDK", 5)) {
                Log.w("AdMobSDK", "Unable to create JSON from object: " + obj);
            }
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public ba a(WeakReference weakReference) {
        return new ba(this, weakReference);
    }

    public void a() {
        Activity activity;
        if (this.b != null && (activity = (Activity) this.b.get()) != null) {
            activity.finish();
        }
    }

    public final void a(String str, Object... objArr) {
        String str2 = "";
        Iterator it = Arrays.asList(objArr).iterator();
        while (it.hasNext()) {
            str2 = str2.concat(a(it.next()));
            if (it.hasNext()) {
                str2 = str2.concat(",");
            }
        }
        String str3 = "javascript:admob.".concat(str) + "(" + str2 + ");";
        if (bh.a("AdMobSDK", 3)) {
            Log.w("AdMobSDK", "Sending url to webView: " + str3);
        }
    }

    public final void b(String str) {
        this.c = str;
    }

    public void loadUrl(String str) {
        super.loadUrl(this.a ? str + "#sdk_close" : str);
    }

    public void onClick(View view) {
        a();
    }
}
