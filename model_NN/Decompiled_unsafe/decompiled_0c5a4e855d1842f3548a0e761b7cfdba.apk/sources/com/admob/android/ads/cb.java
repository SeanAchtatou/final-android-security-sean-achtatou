package com.admob.android.ads;

import android.view.MotionEvent;
import android.view.View;
import java.lang.ref.WeakReference;

public final class cb implements View.OnTouchListener {
    private WeakReference a;

    public cb(bd bdVar) {
        this.a = new WeakReference(bdVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.bd.a(com.admob.android.ads.bd, boolean):void
     arg types: [com.admob.android.ads.bd, int]
     candidates:
      com.admob.android.ads.bd.a(com.admob.android.ads.bd, android.content.Context):void
      com.admob.android.ads.bd.a(com.admob.android.ads.bd, android.view.MotionEvent):void
      com.admob.android.ads.bd.a(com.admob.android.ads.bd, boolean):void */
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        bd bdVar = (bd) this.a.get();
        if (bdVar == null) {
            return false;
        }
        bdVar.b(false);
        bd.a(bdVar, motionEvent);
        return false;
    }
}
