package com.admob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Hashtable;
import java.util.PriorityQueue;

public final class ag {
    private static ag a = null;
    private File b;
    private long c = 0;
    private long d = 524288;
    private PriorityQueue e = null;
    private Hashtable f = null;

    private ag(Context context, long j) {
        File file = new File(context.getCacheDir(), "admob_img_cache");
        if (!file.exists()) {
            file.mkdir();
        } else if (!file.isDirectory()) {
            file.delete();
            file.mkdir();
        }
        this.b = file;
        a(this.b);
    }

    public static ag a(Context context) {
        if (a == null) {
            a = new ag(context, 524288);
        }
        return a;
    }

    public static void a() {
        new aq(a).start();
    }

    private void a(File file) {
        File[] listFiles = file.listFiles();
        this.e = new PriorityQueue(20, new ao());
        this.f = new Hashtable();
        for (File file2 : listFiles) {
            if (file2 != null && file2.canRead()) {
                this.e.add(file2);
                this.f.put(file2.getName(), file2);
                this.c += file2.length();
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b() {
        while (this.c > this.d && this.e.size() > 0) {
            File file = (File) this.e.peek();
            if (bh.a("AdMobSDK", 2)) {
                Log.v("AdMobSDK", "cache: evicting bitmap " + file.getName() + " totalBytes " + this.c);
            }
            b(file);
            file.delete();
        }
    }

    private synchronized void b(File file) {
        if (file != null) {
            if (this.e.remove(file) && (this.f.remove(file.getName()) != null)) {
                this.c -= file.length();
                if (bh.a("AdMobSDK", 2)) {
                    Log.v("AdMobSDK", "Cache: removed file " + file.getName() + " totalBytes " + this.c);
                }
            }
        }
    }

    private synchronized void c(File file) {
        if (file != null) {
            if (this.e.contains(file) || this.f.get(file.getName()) != null) {
                if (bh.a("AdMobSDK", 2)) {
                    Log.v("AdMobSDK", "Cache: trying to add a file that's already in index");
                }
                b(file);
            }
            this.e.add(file);
            this.f.put(file.getName(), file);
            this.c += file.length();
            if (bh.a("AdMobSDK", 2)) {
                Log.v("AdMobSDK", "cache: added file: " + file.getName() + " totalBytes " + this.c);
            }
        }
    }

    public final synchronized Bitmap a(String str) {
        Bitmap bitmap;
        Bitmap decodeFile;
        File file = (File) this.f.get(str);
        if (file == null || (decodeFile = BitmapFactory.decodeFile(file.getAbsolutePath())) == null) {
            bitmap = null;
        } else {
            this.e.remove(file);
            file.setLastModified(System.currentTimeMillis());
            this.e.add(file);
            if (bh.a("AdMobSDK", 2)) {
                Log.v("AdMobSDK", "cache: found bitmap " + file.getName() + " totalBytes " + this.c + " new modified " + file.lastModified());
            }
            bitmap = decodeFile;
        }
        return bitmap;
    }

    public final synchronized void a(String str, Bitmap bitmap) {
        File file = new File(this.b, str);
        File file2 = (File) this.f.get(str);
        if (file2 != null) {
            if (bh.a("AdMobSDK", 2)) {
                Log.v("AdMobSDK", "cache: found bitmap " + file.getName() + " and removing ");
            }
            b(file2);
        }
        if (file.exists()) {
            file.delete();
        }
        try {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(file));
            c(file);
            if (bh.a("AdMobSDK", 2)) {
                Log.v("AdMobSDK", "cache: added bitmap " + file.getName() + " totalBytes " + this.c + " lastModified " + file.lastModified());
            }
        } catch (FileNotFoundException e2) {
        }
    }
}
