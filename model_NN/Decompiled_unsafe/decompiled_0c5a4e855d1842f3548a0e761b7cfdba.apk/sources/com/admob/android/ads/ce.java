package com.admob.android.ads;

import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Map;

public final class ce implements View.OnClickListener {
    private WeakReference a;

    public ce(bd bdVar) {
        this.a = new WeakReference(bdVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.bd.a(com.admob.android.ads.bd, boolean):void
     arg types: [com.admob.android.ads.bd, int]
     candidates:
      com.admob.android.ads.bd.a(com.admob.android.ads.bd, android.content.Context):void
      com.admob.android.ads.bd.a(com.admob.android.ads.bd, android.view.MotionEvent):void
      com.admob.android.ads.bd.a(com.admob.android.ads.bd, boolean):void */
    public final void onClick(View view) {
        bd bdVar = (bd) this.a.get();
        if (bdVar != null) {
            bdVar.f.a("replay", (Map) null);
            if (bdVar.d != null) {
                bd.b(bdVar.d);
            }
            bdVar.b(false);
            bdVar.h = true;
            bdVar.a(bdVar.getContext());
        }
    }
}
