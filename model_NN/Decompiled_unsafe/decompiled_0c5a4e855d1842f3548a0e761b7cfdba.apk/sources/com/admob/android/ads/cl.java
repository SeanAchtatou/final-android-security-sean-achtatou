package com.admob.android.ads;

import android.media.MediaPlayer;
import android.util.Log;
import java.lang.ref.WeakReference;

final class cl implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private WeakReference a;

    public cl(bd bdVar) {
        this.a = new WeakReference(bdVar);
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        bd bdVar = (bd) this.a.get();
        if (bdVar != null) {
            bdVar.i = true;
            bdVar.f();
            bdVar.a(true);
        }
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        if (bh.a("AdMobSDK", 6)) {
            Log.e("AdMobSDK", "error playing video, what: " + i + ", extra: " + i2);
        }
        bd bdVar = (bd) this.a.get();
        if (bdVar == null) {
            return false;
        }
        bdVar.c();
        return true;
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        bd bdVar = (bd) this.a.get();
        if (bdVar != null) {
            bdVar.a();
        }
    }
}
