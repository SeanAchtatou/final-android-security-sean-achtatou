package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;

final class z implements Runnable {
    private WeakReference a;

    public z(AdView adView) {
        this.a = new WeakReference(adView);
    }

    public final void run() {
        AdView adView = (AdView) this.a.get();
        if (adView == null) {
            return;
        }
        if ((adView.b == null || adView.b.getParent() == null) && adView.k != null) {
            try {
                adView.k.a(adView);
            } catch (Exception e) {
                Log.w("AdMobSDK", "Unhandled exception raised in your AdListener.onFailedToReceiveAd.", e);
            }
        } else {
            try {
                adView.k.b(adView);
            } catch (Exception e2) {
                Log.w("AdMobSDK", "Unhandled exception raised in your AdListener.onFailedToReceiveRefreshedAd.", e2);
            }
        }
    }
}
