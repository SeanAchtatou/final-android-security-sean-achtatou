package com.admob.android.ads;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

public final class y implements i {
    public String a;
    public String b;
    public int c;
    public int d;
    public String e;
    public String f;
    public double g;
    public String h;
    public String i;
    public boolean j = false;
    public String k;
    public String l;
    public Vector m = new Vector();

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("u", this.a);
        bundle.putString("t", this.b);
        bundle.putInt("c", this.c);
        bundle.putInt("msm", this.d);
        bundle.putString("s", this.e);
        bundle.putString("sin", this.f);
        bundle.putDouble("sd", this.g);
        bundle.putString("skd", this.h);
        bundle.putString("sku", this.i);
        bundle.putByte("nosk", ac.a(this.j));
        bundle.putString("rd", this.k);
        bundle.putString("ru", this.l);
        bundle.putParcelableArrayList("b", ad.a(this.m));
        return bundle;
    }

    public final boolean a(Bundle bundle) {
        if (bundle == null) {
            return false;
        }
        this.a = bundle.getString("u");
        this.b = bundle.getString("t");
        this.c = bundle.getInt("c");
        this.d = bundle.getInt("msm");
        this.e = bundle.getString("s");
        this.f = bundle.getString("sin");
        this.g = bundle.getDouble("sd");
        this.h = bundle.getString("skd");
        this.i = bundle.getString("sku");
        this.j = ac.a(bundle.getByte("nosk"));
        this.k = bundle.getString("rd");
        this.l = bundle.getString("ru");
        this.m = null;
        ArrayList parcelableArrayList = bundle.getParcelableArrayList("b");
        if (parcelableArrayList != null) {
            Vector vector = new Vector();
            Iterator it = parcelableArrayList.iterator();
            while (it.hasNext()) {
                Bundle bundle2 = (Bundle) it.next();
                if (bundle2 != null) {
                    aa aaVar = new aa();
                    if (aaVar.a(bundle2)) {
                        vector.add(aaVar);
                    }
                }
            }
            this.m = vector;
        }
        return true;
    }

    public final boolean b() {
        return this.c == 0 || this.m == null || this.m.size() == 0;
    }

    public final boolean c() {
        return this.f != null && this.f.length() > 0 && this.g > 0.0d;
    }
}
