package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import org.json.JSONObject;
import org.json.JSONTokener;

final class l {
    private static boolean a = false;

    l() {
    }

    public static void a(Context context) {
        byte[] c;
        String string;
        if (!a) {
            a = true;
            if (ak.b()) {
                try {
                    String a2 = k.a(context, null, null, 0);
                    n a3 = a.a("http://api.admob.com/v1/pubcode/android_sdk_emulator_notice" + "?" + a2, "developer_message", ak.g(context));
                    if (a3.a() && (c = a3.c()) != null && (string = new JSONObject(new JSONTokener(new String(c))).getString("data")) != null && !string.equals("")) {
                        Log.w("AdMobSDK", string);
                    }
                } catch (Exception e) {
                    if (bh.a("AdMobSDK", 2)) {
                        Log.v("AdMobSDK", "Unhandled exception retrieving developer message.", e);
                    }
                }
            }
        }
    }
}
