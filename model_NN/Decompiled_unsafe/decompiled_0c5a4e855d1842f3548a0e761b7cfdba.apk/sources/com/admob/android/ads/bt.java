package com.admob.android.ads;

import android.webkit.WebView;
import java.lang.ref.WeakReference;

public final class bt extends ba {
    private /* synthetic */ s b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bt(s sVar, s sVar2, WeakReference weakReference) {
        super(sVar2, weakReference);
        this.b = sVar;
    }

    public final void onPageFinished(WebView webView, String str) {
        if ("http://mm.admob.com/static/android/canvas.html".equals(str) && this.b.b) {
            this.b.b = false;
            this.b.loadUrl("javascript:cb('" + this.b.c + "','" + this.b.a + "')");
        }
    }
}
