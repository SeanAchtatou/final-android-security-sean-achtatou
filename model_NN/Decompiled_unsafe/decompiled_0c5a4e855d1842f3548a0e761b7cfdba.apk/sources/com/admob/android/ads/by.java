package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

public final class by implements View.OnClickListener {
    private WeakReference a;
    private WeakReference b;
    private WeakReference c;

    public by(bd bdVar, aa aaVar, WeakReference weakReference) {
        this.a = new WeakReference(bdVar);
        this.b = new WeakReference(aaVar);
        this.c = weakReference;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.bd.a(com.admob.android.ads.bd, boolean):void
     arg types: [com.admob.android.ads.bd, int]
     candidates:
      com.admob.android.ads.bd.a(com.admob.android.ads.bd, android.content.Context):void
      com.admob.android.ads.bd.a(com.admob.android.ads.bd, android.view.MotionEvent):void
      com.admob.android.ads.bd.a(com.admob.android.ads.bd, boolean):void */
    public final void onClick(View view) {
        HashMap hashMap;
        Activity activity;
        bd bdVar = (bd) this.a.get();
        if (bdVar != null) {
            bdVar.b(false);
            aa aaVar = (aa) this.b.get();
            if (aaVar != null) {
                Context context = bdVar.getContext();
                if (!bdVar.j) {
                    bdVar.j = true;
                    hashMap = new HashMap();
                    hashMap.put("event", "interaction");
                } else {
                    hashMap = null;
                }
                bdVar.f.a(aaVar.e, hashMap);
                boolean e = bdVar.e();
                if (e) {
                    bdVar.f();
                }
                bdVar.a(e);
                ae aeVar = new ae();
                try {
                    aeVar.a(context, new JSONObject(aaVar.f), (ai) null);
                } catch (JSONException e2) {
                    if (bh.a("AdMobSDK", 6)) {
                        Log.e("AdMobSDK", "Could not create JSONObject from button click", e2);
                    }
                }
                aeVar.b();
                if (this.c != null && (activity = (Activity) this.c.get()) != null) {
                    aeVar.a(activity, bdVar);
                }
            }
        }
    }
}
