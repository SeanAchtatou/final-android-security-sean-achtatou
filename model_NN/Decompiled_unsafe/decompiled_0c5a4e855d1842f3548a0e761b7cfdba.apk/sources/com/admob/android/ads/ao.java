package com.admob.android.ads;

import java.io.File;
import java.util.Comparator;

final class ao implements Comparator {
    ao() {
    }

    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        File file = (File) obj;
        File file2 = (File) obj2;
        if (file == null && file2 == null) {
            return 0;
        }
        if (file != null && file2 != null) {
            long lastModified = file.lastModified();
            long lastModified2 = file2.lastModified();
            if (lastModified == lastModified2) {
                return 0;
            }
            if (lastModified >= lastModified2) {
                return 1;
            }
        } else if (file != null) {
            return 1;
        }
        return -1;
    }
}
