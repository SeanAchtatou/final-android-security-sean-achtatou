package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;
import org.json.JSONObject;

public final class bn extends Thread {
    private JSONObject a;
    private WeakReference b;

    public bn(JSONObject jSONObject, f fVar) {
        this.a = jSONObject;
        this.b = new WeakReference(fVar);
    }

    public final void run() {
        try {
            f fVar = (f) this.b.get();
            if (fVar != null && fVar.a != null) {
                fVar.a.a(this.a);
                if (fVar.b != null) {
                    fVar.b.performClick();
                }
            }
        } catch (Exception e) {
            if (bh.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in AdClickThread.run(), ", e);
            }
        }
    }
}
