package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;

final class x implements Runnable {
    boolean a;
    private WeakReference b;

    public x(AdView adView) {
        this.b = new WeakReference(adView);
    }

    public final void run() {
        try {
            AdView adView = (AdView) this.b.get();
            if (!this.a && adView != null) {
                if (bh.a("AdMobSDK", 3)) {
                    int h = adView.c / 1000;
                    if (bh.a("AdMobSDK", 3)) {
                        Log.d("AdMobSDK", "Requesting a fresh ad because a request interval passed (" + h + " seconds).");
                    }
                }
                adView.g();
            }
        } catch (Exception e) {
            if (bh.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in RefreshHandler.run(), " + e.getMessage());
            }
        }
    }
}
