package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.lang.ref.WeakReference;

final class ab extends Thread {
    private WeakReference a;

    public ab(AdView adView) {
        this.a = new WeakReference(adView);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, boolean):boolean
     arg types: [com.admob.android.ads.AdView, int]
     candidates:
      com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, com.admob.android.ads.e):void
      com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, com.admob.android.ads.f):void
      com.admob.android.ads.AdView.a(com.admob.android.ads.e, com.admob.android.ads.f):void
      com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.AdView.b(com.admob.android.ads.AdView, boolean):void
     arg types: [com.admob.android.ads.AdView, int]
     candidates:
      com.admob.android.ads.AdView.b(com.admob.android.ads.AdView, com.admob.android.ads.f):void
      com.admob.android.ads.AdView.b(com.admob.android.ads.AdView, boolean):void */
    public final void run() {
        AdView adView = (AdView) this.a.get();
        if (adView != null) {
            try {
                Context context = adView.getContext();
                if (k.a(AdView.c(adView), context, adView.i, adView.j, adView.b(), adView.c(), adView.d(), new f(null, context, adView), (int) (((float) adView.getMeasuredWidth()) / f.c()), adView.e(), null, adView.f()) == null) {
                    AdView.f(adView);
                }
            } catch (Exception e) {
                if (bh.a("AdMobSDK", 6)) {
                    Log.e("AdMobSDK", "Unhandled exception requesting a fresh ad.", e);
                }
                AdView.f(adView);
            } finally {
                boolean unused = adView.n = false;
                adView.b(true);
            }
        }
    }
}
