package com.admob.android.ads;

import android.content.Context;
import android.media.AudioManager;

public class ah {
    public AudioManager a;

    public ah(Context context) {
        this.a = (AudioManager) context.getSystemService("audio");
    }

    public int a() {
        return this.a.getMode();
    }

    public boolean b() {
        return this.a.isMusicActive();
    }

    public boolean c() {
        return this.a.isSpeakerphoneOn();
    }

    public int d() {
        return this.a.getRingerMode();
    }
}
