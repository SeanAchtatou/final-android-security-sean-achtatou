package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;
import org.json.JSONArray;

final class cf implements Runnable {
    private WeakReference a;
    private JSONArray b;

    public cf(e eVar, JSONArray jSONArray) {
        this.a = new WeakReference(eVar);
        this.b = jSONArray;
    }

    public final void run() {
        try {
            e eVar = (e) this.a.get();
            if (eVar != null) {
                e.a(eVar, this.b);
            }
        } catch (Exception e) {
            if (bh.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in Ad$ViewAdd.run(), " + e.getMessage());
                e.printStackTrace();
            }
            e eVar2 = (e) this.a.get();
            if (eVar2 != null) {
                eVar2.p();
            }
        }
    }
}
