package com.admob.android.ads;

final class u {
    public static final u a = new u(320, 48);
    private int b;
    private int c;

    static {
        new u(320, 270);
        new u(748, 110);
        new u(488, 80);
    }

    private u(int i, int i2) {
        this.b = i;
        this.c = i2;
    }

    public final String toString() {
        return String.valueOf(this.b) + "x" + String.valueOf(this.c);
    }
}
