package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.lang.ref.WeakReference;

final class bg extends Thread {
    boolean a;
    private br b;
    private WeakReference c;

    public final void run() {
        Context context = (Context) this.c.get();
        if (context != null) {
            try {
                e a2 = k.a(this.b.h(), context, this.b.f(), this.b.g(), this.b.e());
                if (!this.a && a2 == null) {
                    this.b.c();
                }
            } catch (Exception e) {
                if (bh.a("AdMobSDK", 6)) {
                    Log.e("AdMobSDK", "Unhandled exception requesting a fresh ad.", e);
                }
                if (!this.a) {
                    this.b.c();
                }
            }
        } else if (!this.a) {
            this.b.c();
        }
    }
}
