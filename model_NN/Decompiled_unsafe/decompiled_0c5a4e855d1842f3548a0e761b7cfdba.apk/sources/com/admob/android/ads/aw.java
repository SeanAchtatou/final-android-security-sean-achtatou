package com.admob.android.ads;

import android.graphics.PointF;
import android.view.View;

public final class aw {
    public float a = 0.0f;
    public PointF b = new PointF(0.5f, 0.5f);

    public static float a(View view) {
        if (view != null) {
            return c(view).a;
        }
        return 0.0f;
    }

    public static PointF b(View view) {
        if (view != null) {
            return c(view).b;
        }
        return null;
    }

    public static aw c(View view) {
        Object tag = view.getTag();
        return (tag == null || !(tag instanceof aw)) ? new aw() : (aw) tag;
    }
}
