package com.admob.android.ads;

import android.view.View;
import java.util.Comparator;

final class bk implements Comparator {
    bk() {
    }

    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        float a = aw.a((View) obj);
        float a2 = aw.a((View) obj2);
        if (a < a2) {
            return -1;
        }
        return a > a2 ? 1 : 0;
    }
}
