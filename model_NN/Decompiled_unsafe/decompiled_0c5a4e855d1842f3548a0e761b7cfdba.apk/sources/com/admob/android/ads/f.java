package com.admob.android.ads;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import java.util.Collection;
import java.util.Collections;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONObject;

public final class f extends RelativeLayout implements Animation.AnimationListener, cg, cn, g {
    private static float i = -1.0f;
    private static bk j = null;
    protected e a;
    final AdView b;
    protected ProgressBar c;
    private Vector d;
    private View e;
    private long f = -1;
    private boolean g;
    private View h;

    public f(e eVar, Context context, AdView adView) {
        super(context);
        this.b = adView;
        setId(1);
        b(context);
        this.e = null;
        a((e) null);
    }

    public static float a(Context context) {
        b(context);
        return i;
    }

    private static Vector a(int i2, int i3, int i4, long j2, Vector vector) {
        Vector vector2 = vector == null ? new Vector() : vector;
        float f2 = ((float) j2) / 1000.0f;
        String format = (i3 == -1 || i4 == -1) ? String.format("{%d,%f}", Integer.valueOf(i2), Float.valueOf(f2)) : String.format("{%d,%d,%d,%f}", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Float.valueOf(f2));
        vector2.add(format);
        if (bh.a("AdMobSDK", 2)) {
            Log.v("AdMobSDK", "recordEvent:" + format);
        }
        return vector2;
    }

    private Vector a(KeyEvent keyEvent, Vector vector) {
        int action = keyEvent.getAction();
        long eventTime = keyEvent.getEventTime() - this.f;
        if (action != 0 && action != 1) {
            return vector;
        }
        return a(action == 1 ? 1 : 0, -1, -1, eventTime, vector);
    }

    private Vector a(MotionEvent motionEvent, boolean z, Vector vector) {
        int action = motionEvent.getAction();
        long eventTime = motionEvent.getEventTime() - this.f;
        if (action != 0 && action != 1) {
            return vector;
        }
        return a(action == 1 ? 1 : 0, (int) motionEvent.getX(), (int) motionEvent.getY(), eventTime, vector);
    }

    private static void a(View view, JSONObject jSONObject) {
        if (view instanceof g) {
            g gVar = (g) view;
            JSONObject j2 = gVar.j();
            String i2 = gVar.i();
            if (!(j2 == null || i2 == null)) {
                try {
                    jSONObject.put(i2, j2);
                } catch (Exception e2) {
                }
            }
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i3 = 0; i3 < viewGroup.getChildCount(); i3++) {
                a(viewGroup.getChildAt(i3), jSONObject);
            }
        }
    }

    private static void b(Context context) {
        if (i < 0.0f) {
            i = context.getResources().getDisplayMetrics().density;
        }
    }

    public static float c() {
        return i;
    }

    private boolean k() {
        return this.a == null || !this.a.l();
    }

    private boolean l() {
        return this.a != null && SystemClock.uptimeMillis() - this.f > this.a.c();
    }

    private void m() {
        if (this.a != null && isPressed()) {
            setPressed(false);
            if (!this.g) {
                this.g = true;
                JSONObject n = n();
                if (this.h != null) {
                    AnimationSet animationSet = new AnimationSet(true);
                    float width = ((float) this.h.getWidth()) / 2.0f;
                    float height = ((float) this.h.getHeight()) / 2.0f;
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, width, height);
                    scaleAnimation.setDuration(200);
                    animationSet.addAnimation(scaleAnimation);
                    ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, width, height);
                    scaleAnimation2.setDuration(299);
                    scaleAnimation2.setStartOffset(200);
                    scaleAnimation2.setAnimationListener(this);
                    animationSet.addAnimation(scaleAnimation2);
                    postDelayed(new bn(n, this), 500);
                    this.h.startAnimation(animationSet);
                    return;
                }
                this.a.a(n);
                if (this.b != null) {
                    this.b.performClick();
                }
            }
        }
    }

    private JSONObject n() {
        JSONObject jSONObject;
        Exception e2;
        try {
            JSONObject jSONObject2 = new JSONObject();
            a(this, jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            try {
                jSONObject3.put("interactions", jSONObject2);
                return jSONObject3;
            } catch (Exception e3) {
                e2 = e3;
                jSONObject = jSONObject3;
                Log.w("AdMobSDK", "Exception while processing interaction history.", e2);
                return jSONObject;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            jSONObject = null;
            e2 = exc;
            Log.w("AdMobSDK", "Exception while processing interaction history.", e2);
            return jSONObject;
        }
    }

    public final void a() {
        Context context = getContext();
        setBackgroundDrawable(context.getResources().getDrawable(17301602));
        Drawable drawable = context.getResources().getDrawable(17301602);
        drawable.setAlpha(128);
        this.e = new View(context);
        this.e.setBackgroundDrawable(drawable);
        this.e.setVisibility(4);
        addView(this.e, new RelativeLayout.LayoutParams(-1, -1));
    }

    public final void a(View view, RelativeLayout.LayoutParams layoutParams) {
        if (view != null && view != this.h) {
            this.h = view;
            this.c = new ProgressBar(getContext());
            this.c.setIndeterminate(true);
            this.c.setId(2);
            if (layoutParams != null) {
                this.c.setLayoutParams(layoutParams);
            }
            this.c.setVisibility(4);
            post(new bm(this));
        }
    }

    public final void a(e eVar) {
        this.a = eVar;
        if (eVar == null) {
            setFocusable(false);
            setClickable(false);
            return;
        }
        eVar.a((cg) this);
        setFocusable(true);
        setClickable(true);
    }

    public final e b() {
        return this.a;
    }

    public final void d() {
        if (this.a != null) {
            this.a.h();
            this.a = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0074  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean dispatchTouchEvent(android.view.MotionEvent r8) {
        /*
            r7 = this;
            r6 = 2
            r5 = 0
            r4 = 1
            java.lang.String r3 = "AdMobSDK"
            boolean r0 = r7.k()
            if (r0 == 0) goto L_0x009d
            int r0 = r8.getAction()
            java.lang.String r1 = "AdMobSDK"
            boolean r1 = com.admob.android.ads.bh.a(r3, r6)
            if (r1 == 0) goto L_0x004b
            java.lang.String r1 = "AdMobSDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "dispatchTouchEvent: action="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r2 = " x="
            java.lang.StringBuilder r1 = r1.append(r2)
            float r2 = r8.getX()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " y="
            java.lang.StringBuilder r1 = r1.append(r2)
            float r2 = r8.getY()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.v(r3, r1)
        L_0x004b:
            boolean r1 = r7.l()
            if (r1 == 0) goto L_0x0083
            com.admob.android.ads.e r1 = r7.a
            if (r1 == 0) goto L_0x00a2
            com.admob.android.ads.e r1 = r7.a
            android.graphics.Rect r1 = r1.g()
            com.admob.android.ads.e r2 = r7.a
            android.graphics.Rect r1 = r2.a(r1)
            float r2 = r8.getX()
            int r2 = (int) r2
            float r3 = r8.getY()
            int r3 = (int) r3
            boolean r1 = r1.contains(r2, r3)
            if (r1 != 0) goto L_0x00a2
            r1 = r5
        L_0x0072:
            if (r1 == 0) goto L_0x007c
            java.util.Vector r2 = r7.d
            java.util.Vector r2 = r7.a(r8, r4, r2)
            r7.d = r2
        L_0x007c:
            if (r0 == 0) goto L_0x0080
            if (r0 != r6) goto L_0x0085
        L_0x0080:
            r7.setPressed(r1)
        L_0x0083:
            r0 = r4
        L_0x0084:
            return r0
        L_0x0085:
            if (r0 != r4) goto L_0x0096
            boolean r0 = r7.isPressed()
            if (r0 == 0) goto L_0x0092
            if (r1 == 0) goto L_0x0092
            r7.m()
        L_0x0092:
            r7.setPressed(r5)
            goto L_0x0083
        L_0x0096:
            r1 = 3
            if (r0 != r1) goto L_0x0083
            r7.setPressed(r5)
            goto L_0x0083
        L_0x009d:
            boolean r0 = super.dispatchTouchEvent(r8)
            goto L_0x0084
        L_0x00a2:
            r1 = r4
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.f.dispatchTouchEvent(android.view.MotionEvent):boolean");
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (k()) {
            if (bh.a("AdMobSDK", 2)) {
                Log.v("AdMobSDK", "dispatchTrackballEvent: action=" + motionEvent.getAction());
            }
            if (l()) {
                this.d = a(motionEvent, true, this.d);
                if (motionEvent.getAction() == 0) {
                    setPressed(true);
                } else if (motionEvent.getAction() == 1) {
                    if (hasFocus()) {
                        m();
                    }
                    setPressed(false);
                }
            }
        }
        return super.onTrackballEvent(motionEvent);
    }

    public final void e() {
        post(new bo(this));
    }

    /* access modifiers changed from: protected */
    public final void f() {
        this.g = false;
        if (this.c != null) {
            this.c.setVisibility(4);
        }
        if (this.h != null) {
            this.h.setVisibility(0);
        }
    }

    public final void g() {
        Vector vector = new Vector();
        for (int i2 = 0; i2 < getChildCount(); i2++) {
            vector.add(getChildAt(i2));
        }
        if (j == null) {
            j = new bk();
        }
        Collections.sort(vector, j);
        for (int size = vector.size() - 1; size >= 0; size--) {
            if (indexOfChild((View) vector.elementAt(size)) != size) {
                bringChildToFront((View) vector.elementAt(size));
            }
        }
        if (this.e != null) {
            this.e.bringToFront();
        }
    }

    public final long h() {
        long uptimeMillis = SystemClock.uptimeMillis() - this.f;
        if (this.f < 0 || uptimeMillis < 0 || uptimeMillis > 10000000) {
            return 0;
        }
        return uptimeMillis;
    }

    public final String i() {
        return "container";
    }

    public final JSONObject j() {
        JSONObject jSONObject = null;
        if (this.d != null) {
            jSONObject = new JSONObject();
            try {
                jSONObject.put("touches", new JSONArray((Collection) this.d));
            } catch (Exception e2) {
            }
        }
        return jSONObject;
    }

    public final void onAnimationEnd(Animation animation) {
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        if (isPressed() || isFocused()) {
            canvas.clipRect(3, 3, getWidth() - 3, getHeight() - 3);
        }
        super.onDraw(canvas);
        if (this.f == -1) {
            this.f = SystemClock.uptimeMillis();
            if (this.a != null) {
                this.a.i();
            }
        }
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (k()) {
            if (bh.a("AdMobSDK", 2)) {
                Log.v("AdMobSDK", "onKeyDown: keyCode=" + i2);
            }
            if (i2 == 66 || i2 == 23) {
                this.d = a(keyEvent, this.d);
                setPressed(true);
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (k()) {
            if (bh.a("AdMobSDK", 2)) {
                Log.v("AdMobSDK", "onKeyUp: keyCode=" + i2);
            }
            if (l() && (i2 == 66 || i2 == 23)) {
                this.d = a(keyEvent, this.d);
                m();
            }
            setPressed(false);
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public final void setPressed(boolean z) {
        if (!k()) {
            return;
        }
        if ((!this.g || !z) && isPressed() != z) {
            if (this.e != null) {
                if (z) {
                    this.e.bringToFront();
                    this.e.setVisibility(0);
                } else {
                    this.e.setVisibility(4);
                }
            }
            super.setPressed(z);
            invalidate();
        }
    }
}
