package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;

final class bm implements Runnable {
    private WeakReference a;

    public bm(f fVar) {
        this.a = new WeakReference(fVar);
    }

    public final void run() {
        try {
            f fVar = (f) this.a.get();
            if (fVar != null) {
                fVar.addView(fVar.c);
            }
        } catch (Exception e) {
            if (bh.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in AdContainer post run(), " + e.getMessage());
            }
        }
    }
}
