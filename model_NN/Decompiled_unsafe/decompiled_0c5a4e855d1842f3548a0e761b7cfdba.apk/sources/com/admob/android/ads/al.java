package com.admob.android.ads;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import java.util.Date;

final class al implements LocationListener {
    private /* synthetic */ LocationManager a;

    al(LocationManager locationManager) {
        this.a = locationManager;
    }

    public final void onLocationChanged(Location location) {
        Location unused = ak.h = location;
        long unused2 = ak.k = System.currentTimeMillis();
        this.a.removeUpdates(this);
        if (bh.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Acquired location " + ak.h.getLatitude() + "," + ak.h.getLongitude() + " at " + new Date(ak.k).toString() + ".");
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
