package com.admob.android.ads;

public enum be {
    PORTRAIT("p"),
    LANDSCAPE("l"),
    ANY("a");
    
    private String d;

    private be(String str) {
        this.d = str;
    }

    public static be a(int i) {
        be beVar = ANY;
        be beVar2 = beVar;
        for (be beVar3 : values()) {
            if (beVar3.ordinal() == i) {
                beVar2 = beVar3;
            }
        }
        return beVar2;
    }

    public final String toString() {
        return this.d;
    }
}
