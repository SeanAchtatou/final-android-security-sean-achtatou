package com.admob.android.ads;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.Timer;

public class br {
    private static Handler a = null;
    private static Timer b = null;
    private static bg c = null;
    private bv d;
    private WeakReference e;
    private boolean f;
    /* access modifiers changed from: private */
    public e g;
    private String h;
    private String i;
    private bh j;
    private long k;

    private static void i() {
        if (b != null) {
            b.cancel();
            b = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (a != null) {
            a.post(new bj(this));
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        i();
        if (this.k != -1 && bh.a("AdMobSDK", 2)) {
            Log.v("AdMobSDK", "total request time: " + (SystemClock.uptimeMillis() - this.k));
        }
        this.f = true;
        c = null;
        bq bqVar = (bq) this.e.get();
        if (bqVar != null) {
            try {
                bqVar.a(this);
            } catch (Exception e2) {
                Log.w("AdMobSDK", "Unhandled exception raised in your InterstitialAdListener.onReceiveInterstitial.", e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        if (a != null) {
            a.post(new bi(this));
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        c = null;
        bq bqVar = (bq) this.e.get();
        if (bqVar != null) {
            try {
                bqVar.b(this);
            } catch (Exception e2) {
                Log.w("AdMobSDK", "Unhandled exception raised in your InterstitialAdListener.onFailedToReceiveInterstitial.", e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final bv e() {
        return this.d;
    }

    public String f() {
        return this.i;
    }

    public String g() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public final bh h() {
        return this.j;
    }
}
