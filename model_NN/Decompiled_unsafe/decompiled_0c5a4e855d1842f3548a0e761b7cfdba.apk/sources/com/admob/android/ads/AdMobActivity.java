package com.admob.android.ads;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.admob.android.ads.a.a;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONObject;

public class AdMobActivity extends Activity {
    private ac a;
    private Vector b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void finish() {
        if (this.a != null && this.a.l) {
            Intent intent = new Intent();
            intent.putExtra("admob_activity", true);
            setResult(-1, intent);
        }
        super.finish();
    }

    public void onConfigurationChanged(Configuration configuration) {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            ((bb) it.next()).a(configuration);
        }
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        View view;
        super.onCreate(bundle);
        this.b = new Vector();
        Bundle bundleExtra = getIntent().getBundleExtra("o");
        ac acVar = new ac();
        if (acVar.a(bundleExtra)) {
            this.a = acVar;
        } else {
            this.a = null;
        }
        if (this.a != null) {
            ae.a(this.a.c, (JSONObject) null, ak.g(this));
            ci ciVar = this.a.a;
            WeakReference weakReference = new WeakReference(this);
            switch (cj.a[ciVar.ordinal()]) {
                case 1:
                    setTheme(16973831);
                    view = a.a(getApplicationContext(), this.a.d, false, this.a.f, this.a.g, f.a(this), weakReference);
                    break;
                case 2:
                    ac acVar2 = this.a;
                    bd bdVar = new bd(getApplicationContext(), weakReference);
                    bdVar.a(acVar2);
                    this.b.add(bdVar);
                    view = bdVar;
                    break;
                default:
                    view = null;
                    break;
            }
            if (view != null) {
                switch (cj.b[this.a.e.ordinal()]) {
                    case 1:
                        if (bh.a("AdMobSDK", 2)) {
                            Log.v("AdMobSDK", "Setting target orientation to landscape");
                        }
                        setRequestedOrientation(0);
                        break;
                    case 2:
                        if (bh.a("AdMobSDK", 2)) {
                            Log.v("AdMobSDK", "Setting target orientation to portrait");
                        }
                        setRequestedOrientation(1);
                        break;
                    default:
                        if (bh.a("AdMobSDK", 2)) {
                            Log.v("AdMobSDK", "Setting target orientation to sensor");
                        }
                        setRequestedOrientation(4);
                        break;
                }
                setContentView(view);
                return;
            }
            finish();
        } else if (bh.a("AdMobSDK", 6)) {
            Log.e("AdMobSDK", "Unable to get openerInfo from intent");
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.b.clear();
        super.onDestroy();
    }
}
