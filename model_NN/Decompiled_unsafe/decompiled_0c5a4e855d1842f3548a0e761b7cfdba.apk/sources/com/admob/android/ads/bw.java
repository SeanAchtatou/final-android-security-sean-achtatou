package com.admob.android.ads;

import android.view.animation.Animation;

final class bw implements Animation.AnimationListener {
    private /* synthetic */ f a;
    private /* synthetic */ AdView b;

    bw(AdView adView, f fVar) {
        this.b = adView;
        this.a = fVar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.b.post(new t(this.a, this.b));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
