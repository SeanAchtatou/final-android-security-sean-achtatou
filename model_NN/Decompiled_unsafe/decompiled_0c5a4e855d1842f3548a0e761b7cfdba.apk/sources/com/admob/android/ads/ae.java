package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import java.lang.ref.WeakReference;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class ae implements bp {
    public ac a = new ac();
    public Vector b = new Vector();
    private ai c = new ai(this);
    private PopupWindow d = null;

    private static Bundle a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        Iterator<String> keys = jSONObject.keys();
        Bundle bundle = keys.hasNext() ? new Bundle() : null;
        while (keys.hasNext()) {
            String next = keys.next();
            Object opt = jSONObject.opt(next);
            if (!(next == null || opt == null)) {
                a(bundle, next, opt);
            }
        }
        return bundle;
    }

    private void a(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            Intent intent = (Intent) it.next();
            if (packageManager.resolveActivity(intent, 65536) != null) {
                try {
                    context.startActivity(intent);
                    return;
                } catch (Exception e) {
                }
            }
        }
        if (bh.a("AdMobSDK", 6)) {
            Log.e("AdMobSDK", "Could not find a resolving intent on ad click");
        }
    }

    private void a(Context context, String str) {
        a(b(context, str));
    }

    private void a(Intent intent) {
        if (intent != null) {
            this.b.add(intent);
        }
    }

    private static void a(Bundle bundle, String str, Object obj) {
        if (str != null && obj != null) {
            if (obj instanceof String) {
                bundle.putString(str, (String) obj);
            } else if (obj instanceof Integer) {
                bundle.putInt(str, ((Integer) obj).intValue());
            } else if (obj instanceof Boolean) {
                bundle.putBoolean(str, ((Boolean) obj).booleanValue());
            } else if (obj instanceof Double) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            } else if (obj instanceof Long) {
                bundle.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof JSONObject) {
                bundle.putBundle(str, a((JSONObject) obj));
            } else if (obj instanceof JSONArray) {
                JSONArray jSONArray = (JSONArray) obj;
                if (str != null && jSONArray != null) {
                    Vector vector = new Vector();
                    int length = jSONArray.length();
                    int i = 0;
                    while (i < length) {
                        try {
                            vector.add(jSONArray.get(i));
                            i++;
                        } catch (JSONException e) {
                            if (bh.a("AdMobSDK", 6)) {
                                Log.e("AdMobSDK", "couldn't read bundle array while adding extras");
                            }
                        }
                    }
                    if (length != 0) {
                        try {
                            Object obj2 = vector.get(0);
                            if (obj2 instanceof String) {
                                bundle.putStringArray(str, (String[]) vector.toArray(new String[0]));
                            } else if (obj2 instanceof Integer) {
                                Integer[] numArr = (Integer[]) vector.toArray(new Integer[0]);
                                int[] iArr = new int[numArr.length];
                                for (int i2 = 0; i2 < numArr.length; i2++) {
                                    iArr[i2] = numArr[i2].intValue();
                                }
                                bundle.putIntArray(str, iArr);
                            } else if (obj2 instanceof Boolean) {
                                Boolean[] boolArr = (Boolean[]) vector.toArray(new Boolean[0]);
                                boolean[] zArr = new boolean[boolArr.length];
                                for (int i3 = 0; i3 < zArr.length; i3++) {
                                    zArr[i3] = boolArr[i3].booleanValue();
                                }
                                bundle.putBooleanArray(str, zArr);
                            } else if (obj2 instanceof Double) {
                                Double[] dArr = (Double[]) vector.toArray(new Double[0]);
                                double[] dArr2 = new double[dArr.length];
                                for (int i4 = 0; i4 < dArr2.length; i4++) {
                                    dArr2[i4] = dArr[i4].doubleValue();
                                }
                                bundle.putDoubleArray(str, dArr2);
                            } else if (obj2 instanceof Long) {
                                Long[] lArr = (Long[]) vector.toArray(new Long[0]);
                                long[] jArr = new long[lArr.length];
                                for (int i5 = 0; i5 < jArr.length; i5++) {
                                    jArr[i5] = lArr[i5].longValue();
                                }
                                bundle.putLongArray(str, jArr);
                            }
                        } catch (ArrayStoreException e2) {
                            if (bh.a("AdMobSDK", 6)) {
                                Log.e("AdMobSDK", "Couldn't read in array when making extras");
                            }
                        }
                    }
                }
            }
        }
    }

    private void a(String str) {
        this.a.d = str;
    }

    public static void a(List list, JSONObject jSONObject, String str) {
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                q qVar = (q) it.next();
                d dVar = new d();
                JSONObject jSONObject2 = null;
                if (qVar.b) {
                    jSONObject2 = jSONObject;
                }
                a.a(qVar.a, "click_time_tracking", str, jSONObject2, dVar).f();
            }
        }
    }

    private static Intent b(Context context) {
        return new Intent(context, AdMobActivity.class);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private Intent b(Context context, String str) {
        ci ciVar = this.a.a;
        if (ciVar == null) {
            return null;
        }
        switch (am.a[ciVar.ordinal()]) {
            case 1:
                a(str);
                return null;
            case 2:
                return b(context);
            case 3:
                if (this.a.h != null && !this.a.h.b()) {
                    return b(context);
                }
        }
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        return intent;
    }

    public final void a() {
        if (this.d != null) {
            this.d.dismiss();
            this.d = null;
        }
    }

    public final void a(Activity activity, View view) {
        if (this.a.a == ci.CLICK_TO_CANVAS) {
            String str = this.a.d;
            String str2 = this.a.b;
            this.d = new PopupWindow(activity);
            Rect rect = new Rect();
            view.getWindowVisibleDisplayFrame(rect);
            double c2 = (double) f.c();
            RelativeLayout relativeLayout = new RelativeLayout(activity);
            relativeLayout.setGravity(17);
            s sVar = new s(activity, str2, this);
            sVar.setBackgroundColor(-1);
            sVar.setId(1);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(e.a(320, c2), e.a(295, c2));
            layoutParams.addRule(13);
            relativeLayout.addView(sVar, layoutParams);
            sVar.a(str);
            sVar.loadUrl("http://mm.admob.com/static/android/canvas.html");
            this.d.setBackgroundDrawable(null);
            this.d.setFocusable(true);
            this.d.setClippingEnabled(false);
            this.d.setWidth(rect.width());
            this.d.setHeight(rect.height());
            this.d.setContentView(relativeLayout);
            this.d.showAtLocation(view.getRootView(), 0, rect.left, rect.top);
            ViewGroup.LayoutParams layoutParams2 = relativeLayout.getLayoutParams();
            if (layoutParams2 instanceof WindowManager.LayoutParams) {
                WindowManager.LayoutParams layoutParams3 = (WindowManager.LayoutParams) layoutParams2;
                layoutParams3.flags |= 6;
                layoutParams3.dimAmount = 0.5f;
                ((WindowManager) activity.getSystemService("window")).updateViewLayout(relativeLayout, layoutParams2);
            }
        } else if (!this.c.a()) {
            this.c.d = new WeakReference(activity);
            this.c.b();
        } else {
            a(activity);
        }
    }

    public final void a(Context context, JSONArray jSONArray) {
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                a(context, jSONArray.getJSONObject(i));
            } catch (JSONException e) {
                if (bh.a("AdMobSDK", 6)) {
                    Log.e("AdMobSDK", "Could not form an intent from ad action response: " + jSONArray.toString());
                }
            }
        }
    }

    public final void a(Context context, JSONObject jSONObject) {
        Intent intent;
        if (jSONObject != null) {
            String optString = jSONObject.optString("u");
            if (optString == null || optString.equals("")) {
                String optString2 = jSONObject.optString("a", "android.intent.action.VIEW");
                String optString3 = jSONObject.optString("d", null);
                if (this.a.d == null) {
                    a(optString3);
                }
                int optInt = jSONObject.optInt("f", 0);
                Bundle a2 = a(jSONObject.optJSONObject("b"));
                if (this.a.a != null) {
                    switch (am.a[this.a.a.ordinal()]) {
                        case 1:
                            intent = b(context, optString3);
                            break;
                        default:
                            Intent intent2 = new Intent(optString2, Uri.parse(optString3));
                            if (optInt != 0) {
                                intent2.addFlags(optInt);
                            }
                            if (a2 != null) {
                                intent2.putExtras(a2);
                            }
                            intent = intent2;
                            break;
                    }
                } else {
                    intent = null;
                }
                a(intent);
                return;
            }
            a(context, optString);
        }
    }

    public final void a(Context context, JSONObject jSONObject, ai aiVar) {
        this.a.a(jSONObject, aiVar == null ? this.c : aiVar, ak.g(context));
        this.a.d = jSONObject.optString("u");
        JSONArray optJSONArray = jSONObject.optJSONArray("ua");
        JSONObject optJSONObject = jSONObject.optJSONObject("ac");
        JSONArray optJSONArray2 = jSONObject.optJSONArray("ac");
        if (optJSONArray2 != null) {
            a(context, optJSONArray2);
        } else if (optJSONObject != null) {
            a(context, optJSONObject);
        } else if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                String optString = optJSONArray.optString(i);
                if (optString != null) {
                    a(context, optString);
                }
            }
        } else if (this.a.d != null && this.a.d.length() > 0) {
            a(context, this.a.d);
        }
    }

    public final void a(Hashtable hashtable) {
        if (this.a != null) {
            ac acVar = this.a;
            if (hashtable != null) {
                for (String str : hashtable.keySet()) {
                    acVar.k.putParcelable(str, (Parcelable) hashtable.get(str));
                }
            }
        }
    }

    public final void b() {
        if (this.a != null && c()) {
            Intent intent = null;
            if (this.b.size() > 0) {
                intent = (Intent) this.b.firstElement();
            }
            if (intent != null) {
                intent.putExtra("o", this.a.a());
            }
        }
    }

    public final boolean c() {
        return this.a != null && ((this.a.a == ci.CLICK_TO_INTERACTIVE_VIDEO && this.a.h != null && !this.a.h.b()) || this.a.a == ci.CLICK_TO_FULLSCREEN_BROWSER);
    }

    public final void j() {
        a(this.c.a);
        b();
        ai aiVar = this.c;
        Context context = aiVar.d != null ? (Context) aiVar.d.get() : null;
        if (context != null) {
            a(context);
        }
    }

    public final void k() {
    }
}
