package com.admob.android.ads;

import android.content.Context;
import com.admob.android.ads.a.a;
import org.json.JSONObject;

public final class r extends a {
    public JSONObject a = null;
    public JSONObject b = null;
    private boolean e = false;

    public r(Context context, e eVar) {
        super(context, false, null);
        this.d = new bu(this, this, eVar);
        setWebViewClient(this.d);
    }

    public final void a_() {
        if ((this.d instanceof bu) && ((bu) this.d).c && !this.e) {
            this.e = true;
            a("init", this.a == null ? "null" : this.a, this.b == null ? "null" : this.b);
        }
    }
}
