package com.admob.android.ads;

import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class m implements b {
    private final String a;
    private final String b;
    private final String c;
    private String d;
    private aj e = null;
    private HashSet f = new HashSet();
    private int g = 0;

    public m(String str, String str2, String str3, String str4) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
    }

    private String a(String str, Map map, boolean z) {
        Set<String> keySet;
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("rt=1&ex=1");
            sb.append("&a=").append(this.a);
            sb.append("&p=").append(URLEncoder.encode(str, "UTF-8"));
            sb.append("&o=").append(this.d);
            sb.append("&v=").append("20101109-ANDROID-3312276cc1406347");
            long currentTimeMillis = System.currentTimeMillis();
            sb.append("&z").append("=").append(currentTimeMillis / 1000).append(".").append(currentTimeMillis % 1000);
            sb.append("&h%5BHTTP_HOST%5D=").append(URLEncoder.encode(this.c, "UTF-8"));
            sb.append("&h%5BHTTP_REFERER%5D=http%3A%2F%2F").append(this.b);
            if (z) {
                sb.append("&startvisit=1");
            }
            if (!(map == null || (keySet = map.keySet()) == null)) {
                for (String str2 : keySet) {
                    sb.append("&").append(str2).append("=").append(URLEncoder.encode((String) map.get(str2)));
                }
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e2) {
            return null;
        }
    }

    private void b(n nVar) {
        this.f.remove(nVar);
    }

    public final void a(n nVar) {
        if (bh.a("AdMobSDK", 2)) {
            Log.v("AdMobSDK", "Analytics event " + nVar.d() + " has been recorded.");
            int size = this.f.size();
            if (size > 0) {
                Log.v("AdMobSDK", "Pending Analytics requests: " + size);
            }
        }
        b(nVar);
    }

    public final void a(n nVar, Exception exc) {
        if (bh.a("AdMobSDK", 5)) {
            Log.w("AdMobSDK", "analytics request failed for " + nVar.d(), exc);
        }
        b(nVar);
    }

    public final void a(String str, Map map) {
        if (this.c != null && str != null) {
            this.g++;
            String a2 = a(str, map, this.g == 1);
            if (a2 != null) {
                if (this.f != null) {
                    n a3 = a.a("http://r.admob.com/ad_source.php", "AnalyticsData", this.d, this, 5000, null, a2);
                    this.f.add(a3);
                    a3.f();
                }
                if (bh.a("AdMobSDK", 3)) {
                    Log.d("AdMobSDK", "Analytics event " + this.c + "/" + str + " data:" + a2 + " has been recorded.");
                }
            } else if (bh.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "Could not create analytics URL.  Analytics data not tracked.");
            }
        }
    }
}
