package com.tiger;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioTrack;
import android.view.SurfaceHolder;

public class EmuNative {
    private static SurfaceHolder a;
    private static Rect b = new Rect();
    private static AudioTrack c;
    private static b d;
    private static float e = AudioTrack.getMaxVolume();

    static boolean audioCreate(int i, int i2, int i3) {
        int i4 = 1500;
        int i5 = i2 == 16 ? 2 : 3;
        int i6 = i3 == 2 ? 3 : 2;
        if (c != null && c.getSampleRate() == i && c.getAudioFormat() == i5 && c.getChannelCount() == i3) {
            return true;
        }
        int minBufferSize = AudioTrack.getMinBufferSize(i, i6, i5) * 2;
        if (minBufferSize >= 1500) {
            i4 = minBufferSize;
        }
        try {
            c = new AudioTrack(3, i, i6, i5, i4, 1);
            if (c.getState() == 0) {
                c = null;
            }
        } catch (IllegalArgumentException e2) {
            c = null;
        }
        if (c == null) {
            return false;
        }
        c.setStereoVolume(e, e);
        return true;
    }

    static void audioDestroy() {
        if (c != null) {
            c.stop();
            c = null;
        }
    }

    static void audioPause() {
        if (c != null) {
            c.pause();
        }
    }

    static void audioPlay(byte[] bArr, int i) {
        if (c != null) {
            c.write(bArr, 0, i);
        }
    }

    static void audioSetVolume(int i) {
        float minVolume = AudioTrack.getMinVolume();
        e = minVolume + (((AudioTrack.getMaxVolume() - minVolume) * ((float) i)) / 100.0f);
        if (c != null) {
            c.setStereoVolume(e, e);
        }
    }

    static void audioStart() {
        if (c != null) {
            c.play();
        }
    }

    static void audioStop() {
        if (c != null) {
            c.stop();
            c.flush();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Canvas.drawBitmap(int[], int, int, int, int, int, int, boolean, android.graphics.Paint):void}
     arg types: [int[], int, int, int, int, int, int, int, ?[OBJECT, ARRAY]]
     candidates:
      ClspMth{android.graphics.Canvas.drawBitmap(int[], int, int, float, float, int, int, boolean, android.graphics.Paint):void}
      ClspMth{android.graphics.Canvas.drawBitmap(int[], int, int, int, int, int, int, boolean, android.graphics.Paint):void} */
    static void bitBlt(int[] iArr, boolean z) {
        Canvas lockCanvas = a.lockCanvas();
        lockCanvas.drawColor(-16777216);
        lockCanvas.drawBitmap(iArr, 0, b.width(), b.left, b.top, b.width(), b.height(), false, (Paint) null);
        if (d != null) {
            d.a(lockCanvas);
        }
        a.unlockCanvasAndPost(lockCanvas);
    }

    static void destroy() {
        if (c != null) {
            c.stop();
            c = null;
        }
    }

    public static void setOnFrameDrawnListener(b bVar) {
        d = bVar;
    }

    static void setSurface(SurfaceHolder surfaceHolder) {
        a = surfaceHolder;
    }

    static void setSurfaceRegion(int i, int i2, int i3, int i4) {
        b.set(i, i2, i + i3, i2 + i4);
    }
}
