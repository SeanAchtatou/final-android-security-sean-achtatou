package com.tiger.list;

import android.app.ListActivity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;

public abstract class ImageListBase extends ListActivity {
    /* access modifiers changed from: private */
    public LayoutInflater a;
    private a b;

    /* access modifiers changed from: protected */
    public abstract Bitmap a(int i);

    /* access modifiers changed from: protected */
    public void a() {
        this.b.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public abstract int b();

    /* access modifiers changed from: protected */
    public abstract String b(int i);

    /* access modifiers changed from: protected */
    public abstract int c();

    /* access modifiers changed from: protected */
    public abstract String c(int i);

    /* access modifiers changed from: protected */
    public abstract Object d(int i);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.a = (LayoutInflater) getSystemService("layout_inflater");
        this.b = new a(this);
        setListAdapter(this.b);
    }
}
