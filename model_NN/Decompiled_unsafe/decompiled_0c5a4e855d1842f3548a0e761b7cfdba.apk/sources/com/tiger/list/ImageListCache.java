package com.tiger.list;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public abstract class ImageListCache extends ImageListBase {
    private ArrayList a;

    private int a(String str) {
        this.a.clear();
        File file = new File(str);
        if (!file.exists()) {
        }
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            int i = 0;
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    } else if (new File(readLine).exists()) {
                        HashMap hashMap = new HashMap();
                        this.a.add(hashMap);
                        a(hashMap, readLine, i);
                        i++;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            bufferedReader.close();
            fileReader.close();
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
        Comparator i2 = i();
        if (i2 != null) {
            Collections.sort(this.a, i2);
        }
        return this.a.size();
    }

    private void a(HashMap hashMap, String str, int i) {
        a(str, i);
        hashMap.put("ITEM_FILE", str);
        hashMap.put("ITEM_TITLE", e(i));
        hashMap.put("ITEM_SUMMARY", f(i));
        hashMap.put("ITEM_INDEX", new Integer(i));
        Bitmap g = g(i);
        if (g != null) {
            hashMap.put("ITEM_BITMAP", g);
        } else {
            hashMap.put("ITEM_BITMAP", d(h(i)));
        }
    }

    /* access modifiers changed from: protected */
    public int a(HashMap hashMap) {
        if (hashMap == null) {
            return -1;
        }
        return ((Integer) hashMap.get("ITEM_INDEX")).intValue();
    }

    /* access modifiers changed from: protected */
    public Bitmap a(int i) {
        return (Bitmap) d(i).get("ITEM_BITMAP");
    }

    public void a() {
        a(e());
        super.a();
    }

    /* access modifiers changed from: protected */
    public abstract void a(String str, int i);

    /* access modifiers changed from: protected */
    public String b(int i) {
        return (String) d(i).get("ITEM_TITLE");
    }

    /* access modifiers changed from: protected */
    public int c() {
        return this.a.size();
    }

    /* access modifiers changed from: protected */
    public String c(int i) {
        return (String) d(i).get("ITEM_SUMMARY");
    }

    /* access modifiers changed from: protected */
    public Bitmap d(String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            if (fileInputStream != null) {
                return BitmapFactory.decodeStream(fileInputStream);
            }
        } catch (Exception e) {
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract String e();

    /* access modifiers changed from: protected */
    public abstract String e(int i);

    /* access modifiers changed from: protected */
    public abstract String f(int i);

    /* access modifiers changed from: protected */
    public abstract Bitmap g(int i);

    /* access modifiers changed from: protected */
    public abstract String h(int i);

    /* access modifiers changed from: protected */
    public Comparator i() {
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public HashMap d(int i) {
        return (HashMap) this.a.get(i);
    }

    /* access modifiers changed from: protected */
    public String l(int i) {
        return (String) d(i).get("ITEM_FILE");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        this.a = new ArrayList();
        a(e());
        super.onCreate(bundle);
    }
}
