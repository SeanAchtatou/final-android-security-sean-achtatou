package com.tiger.list;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tiger.nds.R;

class a extends BaseAdapter {
    final /* synthetic */ ImageListBase a;

    public a(ImageListBase imageListBase) {
        this.a = imageListBase;
    }

    public int getCount() {
        return this.a.c();
    }

    public Object getItem(int i) {
        return this.a.d(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.a.inflate(this.a.b(), (ViewGroup) null) : view;
        ((TextView) inflate.findViewById(R.id.title)).setText(this.a.b(i));
        ((ImageView) inflate.findViewById(R.id.image_shot)).setImageBitmap(this.a.a(i));
        ((TextView) inflate.findViewById(R.id.summary)).setText(this.a.c(i));
        return inflate;
    }
}
