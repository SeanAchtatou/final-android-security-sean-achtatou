package com.tiger.a;

import android.view.View;
import android.widget.LinearLayout;
import com.admob.android.ads.AdView;
import java.util.Calendar;

public class a {
    private boolean a = false;
    private boolean b = false;
    private long c = 0;
    private AdView d;
    private com.wiyun.ad.AdView e;
    private LinearLayout f;
    private c g;
    private b h;
    private int i = 0;

    public a(LinearLayout linearLayout, View view, View view2) {
        this.f = linearLayout;
        this.g = new c(this);
        this.d = (AdView) view;
        this.d.a(30);
        this.d.a(this.g);
        this.h = new b(this);
        this.e = (com.wiyun.ad.AdView) view2;
        this.e.a(this.h);
        this.e.a(30);
        a();
    }

    private void a(int i2, boolean z) {
        int i3 = z ? 0 : 8;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        switch (i2) {
            case 1:
                this.d.setVisibility(i3);
                if (z) {
                    this.f.addView(this.d, layoutParams);
                    return;
                } else {
                    this.f.removeView(this.d);
                    return;
                }
            case 2:
                this.e.setVisibility(i3);
                if (z) {
                    this.f.addView(this.e, layoutParams);
                    return;
                } else {
                    this.f.removeView(this.e);
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (z) {
            this.a = true;
        }
        d();
        if (z) {
            b();
        }
    }

    private void b() {
        this.c = Calendar.getInstance().getTime().getTime();
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        if (z) {
            this.b = true;
        }
        d();
    }

    private boolean c() {
        return Calendar.getInstance().getTime().getTime() - this.c > 60000;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tiger.a.a.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tiger.a.a.a(com.tiger.a.a, boolean):void
      com.tiger.a.a.a(int, boolean):void */
    private void d() {
        if (this.a && this.i != 1) {
            a(2, false);
            a(1, true);
            this.i = 1;
            this.f.requestLayout();
            this.f.invalidate();
        } else if (this.b && this.i != 2 && c()) {
            a(1, false);
            a(2, true);
            this.i = 2;
            this.f.requestLayout();
            this.f.invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.d.setVisibility(0);
        this.e.a();
    }
}
