package com.tiger.b;

import android.view.MotionEvent;

class b {
    b() {
    }

    public static final int a(MotionEvent motionEvent) {
        return motionEvent.getPointerCount();
    }

    public static final int a(MotionEvent motionEvent, int i) {
        return motionEvent.getPointerId(i);
    }

    public static final float b(MotionEvent motionEvent, int i) {
        return motionEvent.getX(i);
    }

    public static final float c(MotionEvent motionEvent, int i) {
        return motionEvent.getY(i);
    }

    public static final float d(MotionEvent motionEvent, int i) {
        return motionEvent.getSize(i);
    }
}
