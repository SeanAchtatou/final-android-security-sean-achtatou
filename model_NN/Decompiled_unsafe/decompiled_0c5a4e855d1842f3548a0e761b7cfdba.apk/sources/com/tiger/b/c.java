package com.tiger.b;

import android.os.Build;
import android.view.MotionEvent;

public class c {
    static final int a = Integer.parseInt(Build.VERSION.SDK);

    public static final int a(MotionEvent motionEvent) {
        if (a >= 5) {
            return b.a(motionEvent);
        }
        return 1;
    }

    public static final int a(MotionEvent motionEvent, int i) {
        if (a >= 5) {
            return b.a(motionEvent, i);
        }
        return 0;
    }

    public static final float b(MotionEvent motionEvent, int i) {
        return a >= 5 ? b.b(motionEvent, i) : motionEvent.getX();
    }

    public static final float c(MotionEvent motionEvent, int i) {
        return a >= 5 ? b.c(motionEvent, i) : motionEvent.getY();
    }

    public static final float d(MotionEvent motionEvent, int i) {
        return a >= 5 ? b.d(motionEvent, i) : motionEvent.getSize();
    }
}
