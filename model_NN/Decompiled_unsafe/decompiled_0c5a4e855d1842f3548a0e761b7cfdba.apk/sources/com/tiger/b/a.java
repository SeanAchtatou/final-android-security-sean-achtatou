package com.tiger.b;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class a {
    private static boolean a = true;
    private static String b = "ROM_DOWNLOAD";
    private static final Uri c = Uri.parse("market://details?id=org.coolcode.emuroms");

    public static void a(Context context, String str) {
        if (!str.equalsIgnoreCase("nds") && context != null) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("emuroms://category?type=" + str));
            try {
                intent.addFlags(268435456);
                if (a) {
                    Log.d(b, "start rom downloader...");
                }
                context.startActivity(intent);
                if (a) {
                    Log.d(b, "start rom downloader done");
                }
            } catch (ActivityNotFoundException e) {
                if (a) {
                    Log.d(b, "start rom downloader fail, no found, let us go to market..");
                }
                context.startActivity(new Intent("android.intent.action.VIEW", c));
                if (a) {
                    Log.d(b, "goto market done.");
                }
            }
        }
    }
}
