package com.tiger.c;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;

class d {
    private int a;
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public Bitmap d;
    private RectF e = new RectF();
    private String f;

    d(int i, String str) {
        this.a = i;
        this.f = str;
    }

    /* access modifiers changed from: package-private */
    public final float a() {
        return this.e.left;
    }

    /* access modifiers changed from: package-private */
    public final int a(float f2, float f3) {
        if (this.c || this.b) {
            return Integer.MAX_VALUE;
        }
        return ((int) (f2 < this.e.left ? this.e.left - f2 : f2 > this.e.right ? f2 - this.e.right : 0.0f)) + ((int) (f3 < this.e.top ? this.e.top - f3 : f3 > this.e.bottom ? f3 - this.e.bottom : 0.0f));
    }

    /* access modifiers changed from: package-private */
    public final void a(Resources resources, float f2, float f3) {
        this.d = ((BitmapDrawable) resources.getDrawable(this.a)).getBitmap();
        this.d = Bitmap.createScaledBitmap(this.d, (int) (((float) this.d.getWidth()) * f2), (int) (((float) this.d.getHeight()) * f3), true);
    }

    /* access modifiers changed from: package-private */
    public final void a(Resources resources, int i) {
        int width = this.d.getWidth();
        int height = this.d.getHeight();
        this.d = ((BitmapDrawable) resources.getDrawable(i)).getBitmap();
        this.d = Bitmap.createScaledBitmap(this.d, width, height, true);
    }

    /* access modifiers changed from: package-private */
    public final void a(Canvas canvas, Paint paint) {
        if (!this.b && !this.c) {
            canvas.drawBitmap(this.d, this.e.left, this.e.top, paint);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        this.b = z;
    }

    /* access modifiers changed from: package-private */
    public final float b() {
        return this.e.top;
    }

    /* access modifiers changed from: package-private */
    public final void b(float f2, float f3) {
        this.e.set(f2, f3, ((float) this.d.getWidth()) + f2, ((float) this.d.getHeight()) + f3);
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        return this.d.getWidth();
    }

    /* access modifiers changed from: package-private */
    public final int d() {
        return this.d.getHeight();
    }
}
