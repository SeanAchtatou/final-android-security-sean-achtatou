package com.tiger.c;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class a implements SensorEventListener {
    private static final float[] a = {30.0f, 20.0f, 15.0f, 10.0f, 8.0f, 6.0f, 5.0f, 3.0f, 2.0f, 1.0f};
    private Context b;
    private c c;
    private int d;
    private float e = a[7];

    public a(Context context) {
        this.b = context;
    }

    public final int a() {
        return this.d;
    }

    public final void a(int i) {
        int i2 = 9;
        if (i < 0) {
            i2 = 0;
        } else if (i <= 9) {
            i2 = i;
        }
        this.e = a[i2];
    }

    public final void a(c cVar) {
        if (this.c != cVar) {
            SensorManager sensorManager = (SensorManager) this.b.getSystemService("sensor");
            if (this.c != null) {
                sensorManager.unregisterListener(this);
            }
            this.c = cVar;
            if (this.c != null) {
                sensorManager.registerListener(this, sensorManager.getDefaultSensor(3), 1);
            }
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        float f;
        if (this.b.getResources().getConfiguration().orientation == 2) {
            f = -sensorEvent.values[1];
            float f2 = sensorEvent.values[2];
        } else {
            f = -sensorEvent.values[2];
            float f3 = -sensorEvent.values[1];
        }
        int i = f < (-this.e) ? 0 | 32 : f > this.e ? 0 | 16 : 0;
        if (i != this.d) {
            this.d = i;
            if (this.c != null) {
                this.c.a();
            }
        }
    }
}
