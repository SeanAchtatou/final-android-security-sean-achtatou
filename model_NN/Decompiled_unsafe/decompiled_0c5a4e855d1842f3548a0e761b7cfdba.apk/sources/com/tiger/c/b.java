package com.tiger.c;

import android.view.KeyEvent;
import android.view.View;

public class b implements View.OnKeyListener {
    private c a;
    private int[] b = new int[128];
    private int c;

    public b(View view, c cVar) {
        this.a = cVar;
        view.setOnKeyListener(this);
    }

    public final int a() {
        return this.c;
    }

    public void a(int i, int i2) {
        if (i2 >= 0 && i2 < this.b.length) {
            int[] iArr = this.b;
            iArr[i2] = iArr[i2] | i;
        }
    }

    public void b() {
        this.c = 0;
    }

    public void c() {
        for (int i = 0; i < this.b.length; i++) {
            this.b[i] = 0;
        }
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i >= this.b.length) {
            return false;
        }
        int i2 = this.b[i];
        if (i2 == 0) {
            return false;
        }
        if (keyEvent.getRepeatCount() == 0) {
            if (keyEvent.getAction() == 0) {
                this.c = i2 | this.c;
            } else {
                this.c = (i2 ^ -1) & this.c;
            }
            this.a.a();
        }
        return true;
    }
}
