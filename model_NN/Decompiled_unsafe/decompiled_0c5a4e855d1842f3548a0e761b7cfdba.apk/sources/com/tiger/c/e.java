package com.tiger.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.MotionEvent;
import android.view.View;
import com.tiger.EmuCore;
import com.tiger.b.c;
import com.tiger.nds.R;
import java.util.ArrayList;
import java.util.Iterator;

public class e {
    private static final int[] a = {32, 64, 16, 128};
    private static final int[] b = {2048, 1024, 1, 2};
    private static final float[] c = {0.1f, 0.14f, 0.1667f, 0.2f, 0.25f};
    private Context d;
    private View e;
    private float f;
    private float g;
    private int h;
    private c i;
    private int j;
    private Vibrator k;
    private boolean l;
    private float m = c[2];
    private float n;
    private ArrayList o = new ArrayList();
    private d p;
    private d q;
    private d r;
    private d s;
    private d t;
    private d[] u;
    private EmuCore v = EmuCore.a();

    public e(View view, c cVar) {
        this.e = view;
        this.d = this.e.getContext();
        this.i = cVar;
        this.k = (Vibrator) this.d.getSystemService("vibrator");
        this.p = a((int) R.drawable.dpad, "dpad");
        this.q = a((int) R.drawable.buttons, "buttons");
        this.r = a((int) R.drawable.select_start_buttons, "select_start");
        this.s = a((int) R.drawable.tl_button_top, "button_TL");
        this.t = a((int) R.drawable.tr_button_top, "button_TR");
        this.u = new d[5];
    }

    private static float a(SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("vkeypadSize", null);
        if ("small".equals(string)) {
            return 1.0f;
        }
        return "large".equals(string) ? 1.33333f : 1.2f;
    }

    private float a(MotionEvent motionEvent, int i2, boolean z) {
        float b2 = c.b(motionEvent, i2);
        if (z) {
            b2 = ((float) this.e.getWidth()) - b2;
        }
        return b2 * this.f;
    }

    private int a(float f2, float f3) {
        float f4 = f2 - 0.5f;
        float f5 = f3 - 0.5f;
        return Math.abs(f4) >= Math.abs(f5) ? f4 < 0.0f ? 0 : 2 : f5 < 0.0f ? 1 : 3;
    }

    private int a(float f2, float f3, float f4) {
        int i2 = b[a(f2, f3)];
        if (f4 <= this.n) {
            return i2;
        }
        switch (i2) {
            case 1:
            case 1024:
                return 1025;
            case 2:
            case 2048:
                return 2050;
            default:
                return i2;
        }
    }

    private int a(d dVar, float f2, float f3, float f4) {
        float a2 = (f2 - dVar.a()) / ((float) dVar.c());
        float b2 = (f3 - dVar.b()) / ((float) dVar.d());
        if (dVar == this.p) {
            return b(a2, b2);
        }
        if (dVar == this.q) {
            return a(a2, b2, f4);
        }
        if (dVar == this.r) {
            return c(a2, b2);
        }
        if (dVar == this.s) {
            return 512;
        }
        return dVar == this.t ? 256 : 0;
    }

    private d a(int i2, String str) {
        d dVar = new d(i2, str);
        this.o.add(dVar);
        return dVar;
    }

    private void a(int i2) {
        if (this.j != i2) {
            if (this.l && f(this.j, i2)) {
                this.k.vibrate(18);
            }
            this.j = i2;
            this.i.a();
        }
    }

    private void a(int i2, int i3, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("vkeypadLayout", "top_bottom");
        if ("top_bottom".equals(string)) {
            d(i2, i3);
        } else if ("bottom_top".equals(string)) {
            e(i2, i3);
        } else if ("top_top".equals(string)) {
            c(i2, i3);
        } else {
            b(i2, i3);
        }
    }

    private float b(MotionEvent motionEvent, int i2, boolean z) {
        float c2 = c.c(motionEvent, i2);
        if (z) {
            c2 = ((float) this.e.getHeight()) - c2;
        }
        return c2 * this.g;
    }

    private int b(float f2, float f3) {
        int i2 = 0;
        if (f2 < 0.5f - this.m) {
            i2 = 0 | 32;
        } else if (f2 > this.m + 0.5f) {
            i2 = 0 | 16;
        }
        return f3 < 0.5f - this.m ? i2 | 64 : f3 > this.m + 0.5f ? i2 | 128 : i2;
    }

    private void b(int i2, int i3) {
        if (this.p.c() + this.q.c() > i2) {
            e(i2, i3);
            return;
        }
        this.p.b(0.0f, (float) (i3 - this.p.d()));
        this.q.b((float) (i2 - this.q.c()), (float) (i3 - this.q.d()));
        this.s.b(0.0f, 0.0f);
        this.t.b((float) (i2 - this.t.c()), 0.0f);
        int c2 = (((this.p.c() + i2) - this.q.c()) - this.r.c()) / 2;
        if (c2 > this.p.c()) {
            this.r.b((float) c2, (float) (i3 - this.r.d()));
        } else {
            this.r.b((float) ((i2 - this.r.c()) / 2), 0.0f);
        }
    }

    private int c(float f2, float f3) {
        return f2 < 0.5f ? 4 : 8;
    }

    private void c(int i2, int i3) {
        if (this.p.c() + this.q.c() > i2) {
            e(i2, i3);
            return;
        }
        this.p.b(0.0f, 0.0f);
        this.q.b((float) (i2 - this.q.c()), 0.0f);
        this.s.a(this.d.getResources(), (int) R.drawable.tl_button_bottom);
        this.t.a(this.d.getResources(), (int) R.drawable.tr_button_bottom);
        this.s.b(0.0f, (float) (i3 - this.s.d()));
        this.t.b((float) (i2 - this.t.c()), (float) (i3 - this.t.d()));
        this.r.b((float) ((i2 - this.r.c()) / 2), (float) (i3 - this.r.d()));
    }

    private d d(float f2, float f3) {
        Iterator it = this.o.iterator();
        int i2 = Integer.MAX_VALUE;
        d dVar = null;
        while (it.hasNext()) {
            d dVar2 = (d) it.next();
            if (!dVar2.b && !dVar2.c) {
                int a2 = dVar2.a(f2, f3);
                if (a2 == 0) {
                    return dVar2;
                }
                if (dVar2 != this.r && a2 < i2) {
                    i2 = a2;
                    dVar = dVar2;
                }
            }
        }
        if (i2 <= 100) {
            return dVar;
        }
        return null;
    }

    private void d(int i2, int i3) {
        this.p.b(0.0f, 0.0f);
        this.q.b((float) (i2 - this.q.c()), (float) (i3 - this.q.d()));
        this.s.a(this.d.getResources(), (int) R.drawable.tl_button_bottom);
        this.s.b(0.0f, (float) (i3 - this.s.d()));
        this.t.b((float) (i2 - this.t.c()), 0.0f);
        int c2 = (((this.s.c() + i2) - this.q.c()) - this.r.c()) / 2;
        if (c2 > this.s.c()) {
            this.r.b((float) c2, (float) (i3 - this.r.d()));
        } else {
            this.r.b((float) (((this.p.c() + i2) - this.r.c()) / 2), (float) this.t.d());
        }
    }

    private void e(int i2, int i3) {
        this.p.b(0.0f, (float) (i3 - this.p.d()));
        this.q.b((float) (i2 - this.q.c()), 0.0f);
        this.t.a(this.d.getResources(), (int) R.drawable.tr_button_bottom);
        this.s.b(0.0f, 0.0f);
        this.t.b((float) (i2 - this.t.c()), (float) (i3 - this.t.d()));
        int c2 = (((this.p.c() + i2) - this.t.c()) - this.r.c()) / 2;
        if (c2 > this.p.c()) {
            this.r.b((float) c2, (float) (i3 - this.r.d()));
        } else {
            this.r.b((float) (((this.p.c() + i2) - this.r.c()) / 2), (float) ((i3 - this.s.d()) - this.r.d()));
        }
    }

    private boolean f(int i2, int i3) {
        return ((i2 ^ i3) & i3) != 0;
    }

    public final int a() {
        return this.j;
    }

    public final void a(int i2, int i3) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.d);
        this.l = defaultSharedPreferences.getBoolean("enableVibrator", true);
        int i4 = defaultSharedPreferences.getInt("dpadDeadZone", 2);
        if (i4 < 0) {
            i4 = 0;
        } else if (i4 > 4) {
            i4 = 4;
        }
        this.m = c[i4];
        this.n = 1.0f;
        this.p.a(defaultSharedPreferences.getBoolean("hideDpad", false));
        this.q.a(defaultSharedPreferences.getBoolean("hideButtons", false));
        this.r.a(defaultSharedPreferences.getBoolean("hideSelectStart", false));
        this.s.a(defaultSharedPreferences.getBoolean("hideShoulders", false));
        this.t.a(defaultSharedPreferences.getBoolean("hideShoulders", false));
        this.f = ((float) i2) / ((float) this.e.getWidth());
        this.g = ((float) i3) / ((float) this.e.getHeight());
        float a2 = a(defaultSharedPreferences);
        float f2 = this.f * a2;
        float f3 = this.g * a2;
        Resources resources = this.d.getResources();
        Iterator it = this.o.iterator();
        while (it.hasNext()) {
            ((d) it.next()).a(resources, f2, f3);
        }
        int i5 = defaultSharedPreferences.getInt("layoutMargin", 2) * 5;
        a(i2 - ((int) (((float) i5) * this.f)), i3 - ((int) (((float) i5) * this.g)), defaultSharedPreferences);
        this.h = defaultSharedPreferences.getInt("vkeypadTransparency", 75);
    }

    public void a(Canvas canvas) {
        Paint paint = new Paint();
        paint.setAlpha((this.h * 2) + 30);
        Iterator it = this.o.iterator();
        while (it.hasNext()) {
            ((d) it.next()).a(canvas, paint);
        }
    }

    public boolean a(MotionEvent motionEvent, boolean z) {
        d dVar;
        if (this.p.d == null) {
            return false;
        }
        int action = motionEvent.getAction();
        switch (action & 255) {
            case 0:
            case 2:
            case 4:
            case 5:
            case 6:
                int i2 = (65280 & action) >> 8;
                int a2 = c.a(motionEvent, i2);
                if (a2 < this.u.length) {
                    if (action != 6) {
                        this.u[a2] = d(a(motionEvent, i2, z), b(motionEvent, i2, z));
                        break;
                    } else {
                        this.u[a2] = null;
                        break;
                    }
                }
                break;
            case 1:
            case 3:
                for (int i3 = 0; i3 < this.u.length; i3++) {
                    this.u[i3] = null;
                }
                break;
            default:
                return false;
        }
        int a3 = c.a(motionEvent);
        int i4 = 0;
        for (int i5 = 0; i5 < a3; i5++) {
            int a4 = c.a(motionEvent, i5);
            if (a4 <= this.u.length && (dVar = this.u[a4]) != null) {
                i4 |= a(dVar, a(motionEvent, i5, z), b(motionEvent, i5, z), c.d(motionEvent, i5));
            }
        }
        a(i4);
        return true;
    }

    public void b() {
        this.j = 0;
    }

    public final void c() {
    }
}
