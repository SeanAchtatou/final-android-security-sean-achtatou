package com.tiger.core;

import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.tiger.nds.R;
import java.io.File;

class d extends BaseAdapter {
    final /* synthetic */ GameStateActivity a;
    private File[] b = new File[8];

    public d(GameStateActivity gameStateActivity, String str) {
        this.a = gameStateActivity;
        for (int i = 0; i < this.b.length; i++) {
            this.b[i] = new File(GameStateActivity.a(str, i));
        }
    }

    public void a(int i) {
        if (((File) getItem(i)).delete()) {
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        return this.b.length;
    }

    public Object getItem(int i) {
        return this.b[i];
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.a.a.inflate((int) R.layout.state_slot_item, (ViewGroup) null) : view;
        ((TextView) inflate.findViewById(R.id.name)).setText(this.a.a(i));
        TextView textView = (TextView) inflate.findViewById(R.id.detail);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.screenshot);
        File file = (File) getItem(i);
        if (file.exists()) {
            textView.setText(DateFormat.format("yyyy-MM-dd hh:mm:ss", file.lastModified()));
            imageView.setImageBitmap(GameStateActivity.a(new File(GameStateActivity.a(file.getAbsolutePath()))));
        } else {
            textView.setText(this.a.getString(R.string.slot_empty));
            imageView.setImageBitmap(null);
        }
        return inflate;
    }
}
