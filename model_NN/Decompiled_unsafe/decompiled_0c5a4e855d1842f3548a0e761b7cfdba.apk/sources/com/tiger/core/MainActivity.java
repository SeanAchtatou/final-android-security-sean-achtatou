package com.tiger.core;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.tiger.a.a;
import com.tiger.nds.R;
import java.io.File;
import java.util.Locale;

public class MainActivity extends RomChooser {
    private static final Uri c = Uri.parse("file:///android_asset/faq.html");
    private static final Uri d = Uri.parse("file:///android_asset/faq_cn.html");
    private static final Uri e = Uri.parse("file:///android_asset/faq_cn_mm.html");
    private static final Uri f = Uri.parse("file:///android_asset/faq_mm.html");
    private static Intent i;
    private boolean g = false;
    private int h = 0;
    private SharedPreferences j;
    private boolean k;
    private a l;
    private final Handler m = new j(this);

    private void a(String str, String str2) {
        new AlertDialog.Builder(this).setTitle(str).setMessage(str2).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
    }

    private Uri j() {
        String country = getResources().getConfiguration().locale.getCountry();
        return (country.equals(Locale.CHINESE.getCountry()) || country.equals(Locale.CHINA.getCountry())) ? com.tiger.nds.a.a() ? e : d : com.tiger.nds.a.a() ? f : c;
    }

    private void k() {
        a(getString(R.string.about_title_msg), String.valueOf(getString(R.string.about_contexnt_msg)) + "\n" + getString(R.string.about_contexnt_ver) + "0.0.1b_demo" + "\n" + getString(R.string.about_contexnt_release) + "2010-06-11" + "\n\n" + getString(R.string.about_contexnt_author) + "Tiger King" + "\n" + getString(R.string.about_contexnt_bug) + "\n");
    }

    /* access modifiers changed from: private */
    public void l() {
        a(getString(R.string.ad_help_title), getString(R.string.ad_help_contexnt));
    }

    /* access modifiers changed from: protected */
    public void a(Uri uri) {
        this.h++;
        this.j.edit().putString("lastGame", uri.getPath()).commit();
        Intent intent = new Intent("android.intent.action.VIEW", uri, this, EmuActivity.class);
        if (!this.k) {
            startActivity(intent);
            this.a = true;
            return;
        }
        i = intent;
        showDialog(1);
    }

    /* access modifiers changed from: protected */
    public String[] d() {
        return com.tiger.nds.a.a;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        this.j = getSharedPreferences("MainActivity", 0);
        super.onCreate(bundle);
        setContentView((int) R.layout.rom_list_content_view);
        setVolumeControlStream(3);
        setTitle((int) R.string.title_rom_list);
        if (c() == 0) {
            this.b.sendEmptyMessageDelayed(0, 500);
        }
        if (c() != 0) {
            this.m.sendEmptyMessageDelayed(101, 500);
        }
        this.l = new a((LinearLayout) findViewById(R.id.ad_container), findViewById(R.id.admob_ad), findViewById(R.id.wy_ad));
        ((ListView) findViewById(16908298)).setOnCreateContextMenuListener(this);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        menu.removeItem(R.id.menu_download_rom);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_settings /*2131099678*/:
                startActivity(new Intent(this, EmuSettings.class));
                return true;
            case R.id.menu_download_rom /*2131099679*/:
                com.tiger.b.a.a(this, "nds");
                return true;
            case R.id.menu_search_roms /*2131099680*/:
            case R.id.menu_sort /*2131099681*/:
            default:
                return super.onOptionsItemSelected(menuItem);
            case R.id.menu_sort_by_name /*2131099682*/:
                j(1);
                return true;
            case R.id.menu_sort_by_recent_play /*2131099683*/:
                j(2);
                return true;
            case R.id.menu_help /*2131099684*/:
                startActivity(new Intent(this, HelpActivity.class).setData(j()));
                return true;
            case R.id.menu_about /*2131099685*/:
                k();
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        switch (i2) {
            case 1:
                String name = new File(i.getData().getPath()).getName();
                int lastIndexOf = name.lastIndexOf(46);
                if (lastIndexOf > 0) {
                    name = name.substring(0, lastIndexOf);
                }
                EditText editText = (EditText) dialog.findViewById(R.id.name);
                editText.setText(name);
                editText.selectAll();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.b.sendEmptyMessageDelayed(1, 1000);
    }

    public void onStart() {
        super.onStart();
    }

    public void onStop() {
        super.onStop();
    }
}
