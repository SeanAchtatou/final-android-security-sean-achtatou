package com.tiger.core;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.tiger.nds.R;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class GameStateActivity extends ListActivity {
    /* access modifiers changed from: private */
    public LayoutInflater a;
    private d b;
    private boolean c;

    public static Bitmap a(File file) {
        ZipInputStream zipInputStream;
        ZipEntry nextEntry;
        try {
            ZipInputStream zipInputStream2 = new ZipInputStream(new BufferedInputStream(new FileInputStream(file)));
            do {
                try {
                    nextEntry = zipInputStream2.getNextEntry();
                    if (nextEntry == null) {
                        break;
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    zipInputStream = zipInputStream2;
                    th = th2;
                }
            } while (!nextEntry.getName().equals("screen.png"));
            if (nextEntry != null) {
                Bitmap decodeStream = BitmapFactory.decodeStream(zipInputStream2);
                if (zipInputStream2 != null) {
                    try {
                        zipInputStream2.close();
                    } catch (Exception e) {
                    }
                }
                return decodeStream;
            }
            if (zipInputStream2 != null) {
                zipInputStream2.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            zipInputStream = null;
            if (zipInputStream != null) {
                zipInputStream.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public String a(int i) {
        if (i == 0) {
            return getString(R.string.slot_quick);
        }
        return getString(R.string.slot_nth, new Object[]{Integer.valueOf(i)});
    }

    protected static String a(String str) {
        return String.valueOf(str) + ".scr";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuffer.append(java.lang.CharSequence, int, int):java.lang.StringBuffer}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuffer.append(char[], int, int):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuffer.append(java.lang.CharSequence, int, int):java.lang.StringBuffer} */
    public static String a(String str, int i) {
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf < 0) {
            lastIndexOf = str.length();
        }
        return new StringBuffer(lastIndexOf + 4).append((CharSequence) str, 0, lastIndexOf).append(".gs").append(i).toString();
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case 1:
                this.b.a(adapterContextMenuInfo.position);
                return true;
            default:
                return super.onContextItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.a = (LayoutInflater) getSystemService("layout_inflater");
        Intent intent = getIntent();
        this.c = intent.getBooleanExtra("saveMode", false);
        setTitle(this.c ? R.string.save_state_title : R.string.load_state_title);
        getListView().setOnCreateContextMenuListener(this);
        this.b = new d(this, intent.getData().getPath());
        setListAdapter(this.b);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) contextMenuInfo;
        contextMenu.setHeaderTitle(a(adapterContextMenuInfo.position));
        if (((File) getListView().getItemAtPosition(adapterContextMenuInfo.position)).exists()) {
            contextMenu.add(0, 1, 0, (int) R.string.menu_delete);
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i, long j) {
        File file = (File) listView.getItemAtPosition(i);
        if (this.c || file.exists()) {
            Intent intent = new Intent();
            intent.setData(Uri.fromFile(file));
            setResult(-1, intent);
            finish();
        }
    }
}
