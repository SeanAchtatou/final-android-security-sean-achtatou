package com.tiger.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class a {
    private ArrayList a = new ArrayList();
    private String[] b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public volatile boolean f = false;

    public a(String str, String str2, String[] strArr, boolean z) {
        this.b = strArr;
        this.c = str2;
        this.d = str;
        this.e = z;
    }

    /* access modifiers changed from: private */
    public synchronized void a() {
        while (this.f) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        this.f = true;
    }

    private boolean a(File file) {
        if (a(file.getAbsolutePath())) {
            return false;
        }
        return d(file.getName().toLowerCase());
    }

    private boolean a(String str) {
        return this.a.contains(str);
    }

    /* access modifiers changed from: private */
    public void b() {
        this.a.clear();
        File file = new File(this.c);
        if (!file.exists()) {
        }
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        bufferedReader.close();
                        fileReader.close();
                        return;
                    } else if (new File(readLine).exists()) {
                        this.a.add(readLine);
                    }
                } catch (IOException e2) {
                    e2.printStackTrace();
                    return;
                }
            }
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        File file = new File(str);
        File file2 = new File(file.getParent());
        if (!file2.exists()) {
            file2.mkdirs();
        }
        try {
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            PrintWriter printWriter = new PrintWriter(fileOutputStream);
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.a.size()) {
                    printWriter.flush();
                    printWriter.close();
                    fileOutputStream.close();
                    return;
                }
                printWriter.println((String) this.a.get(i2));
                i = i2 + 1;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        File[] listFiles;
        File file = new File(str);
        if (file != null && (listFiles = file.listFiles()) != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    c(file2.getAbsolutePath());
                } else if (a(file2)) {
                    this.a.add(file2.getAbsolutePath());
                }
            }
        }
    }

    private boolean d(String str) {
        String lowerCase = str.toLowerCase();
        for (String endsWith : this.b) {
            if (lowerCase.endsWith(endsWith)) {
                return true;
            }
        }
        return false;
    }

    public void a(m mVar) {
        new e(this, mVar).start();
    }
}
