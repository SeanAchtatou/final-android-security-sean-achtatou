package com.tiger.core;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class HelpActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        WebView webView = new WebView(this);
        webView.setWebChromeClient(new k(this));
        setContentView(webView);
        webView.loadUrl(getIntent().getData().toString());
    }
}
