package com.tiger.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import com.tiger.EmuCore;
import com.tiger.EmuNative;
import com.tiger.EmuView;
import com.tiger.a;
import com.tiger.b;
import com.tiger.c.c;
import com.tiger.c.e;
import com.tiger.nds.EmuService;
import com.tiger.nds.R;
import java.io.File;
import java.nio.ByteBuffer;

public class EmuActivity extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener, SurfaceHolder.Callback, View.OnTouchListener, a, b, c {
    private static final int[] k = {32, 16, 64, 128};
    private static final int[] l;
    private EmuCore a;
    private EmuView b;
    private Rect c = new Rect();
    private com.tiger.c.b d;
    private e e;
    private com.tiger.c.a f;
    private int[] g;
    private int h;
    private SharedPreferences i;
    /* access modifiers changed from: private */
    public Intent j;
    private final Handler m = new i(this);

    static {
        int[] iArr = new int[4];
        iArr[0] = 512;
        iArr[1] = 256;
        l = iArr;
    }

    private static int a(String str) {
        if (str.equals("original")) {
            return 0;
        }
        if (str.equals("2x")) {
            return 1;
        }
        return str.equals("proportional") ? 2 : 3;
    }

    private String a(SharedPreferences sharedPreferences) {
        return "tigernds";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tiger.EmuCore.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tiger.EmuCore.a(android.content.Context, java.lang.String):com.tiger.EmuCore
      com.tiger.EmuCore.a(java.lang.String, boolean):void */
    private void a(SharedPreferences sharedPreferences, Configuration configuration) {
        this.a.a("flipScreen", false);
    }

    private int b(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & 255) {
            case 0:
            case 5:
                return 1;
            case 1:
            case 6:
                return 2;
            case 2:
                return 3;
            case 3:
            case 4:
            default:
                return 0;
        }
    }

    private static int b(String str) {
        if (str.equals("landscape")) {
            return 0;
        }
        return str.equals("portrait") ? 1 : -1;
    }

    private void b() {
        this.a.pause();
    }

    private void b(SharedPreferences sharedPreferences) {
        int[] iArr = EmuSettings.a;
        int[] a2 = f.a(this);
        this.d.c();
        String[] strArr = EmuSettings.b;
        for (int i2 = 0; i2 < strArr.length; i2++) {
            this.d.a(iArr[i2], sharedPreferences.getInt(strArr[i2], a2[i2]));
        }
    }

    private void c() {
        if (hasWindowFocus()) {
            this.a.resume();
        }
    }

    private boolean c(String str) {
        String lowerCase = str.toLowerCase();
        for (String endsWith : com.tiger.nds.a.a) {
            if (lowerCase.endsWith(endsWith)) {
                return true;
            }
        }
        return false;
    }

    private Dialog d() {
        return new AlertDialog.Builder(this).setTitle((int) R.string.quit_game_title).setItems((int) R.array.exit_game_options, new g(this)).create();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0043 A[SYNTHETIC, Splitter:B:13:0x0043] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d(java.lang.String r7) {
        /*
            r6 = this;
            r6.b()
            r0 = 0
            java.lang.String r1 = com.tiger.core.GameStateActivity.a(r7)     // Catch:{ all -> 0x003d }
            java.util.zip.ZipOutputStream r2 = new java.util.zip.ZipOutputStream     // Catch:{ all -> 0x003d }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x003d }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ all -> 0x003d }
            r4.<init>(r1)     // Catch:{ all -> 0x003d }
            r3.<init>(r4)     // Catch:{ all -> 0x003d }
            r2.<init>(r3)     // Catch:{ all -> 0x003d }
            java.util.zip.ZipEntry r0 = new java.util.zip.ZipEntry     // Catch:{ all -> 0x0049 }
            java.lang.String r1 = "screen.png"
            r0.<init>(r1)     // Catch:{ all -> 0x0049 }
            r2.putNextEntry(r0)     // Catch:{ all -> 0x0049 }
            android.graphics.Bitmap r0 = r6.j()     // Catch:{ all -> 0x0049 }
            android.graphics.Bitmap$CompressFormat r1 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ all -> 0x0049 }
            r3 = 100
            r0.compress(r1, r3, r2)     // Catch:{ all -> 0x0049 }
            r0.recycle()     // Catch:{ all -> 0x0049 }
            if (r2 == 0) goto L_0x0034
            r2.close()     // Catch:{ Exception -> 0x0047 }
        L_0x0034:
            com.tiger.EmuCore r0 = r6.a
            r0.saveState(r7)
            r6.c()
            return
        L_0x003d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ Exception -> 0x0047 }
        L_0x0046:
            throw r0     // Catch:{ Exception -> 0x0047 }
        L_0x0047:
            r0 = move-exception
            goto L_0x0034
        L_0x0049:
            r0 = move-exception
            r1 = r2
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tiger.core.EmuActivity.d(java.lang.String):void");
    }

    private Dialog e() {
        h hVar = new h(this);
        return new AlertDialog.Builder(this).setCancelable(false).setTitle((int) R.string.replace_game_title).setMessage((int) R.string.replace_game_message).setPositiveButton(17039379, hVar).setNegativeButton(17039369, hVar).create();
    }

    private void e(String str) {
        if (new File(str).exists()) {
            b();
            this.a.loadState(str);
            c();
        }
    }

    private String f() {
        return getIntent().getData().getPath();
    }

    /* access modifiers changed from: private */
    public boolean g() {
        String f2 = f();
        if (!c(f2)) {
            Toast.makeText(this, (int) R.string.rom_not_supported, 0).show();
            finish();
            return false;
        } else if (!this.a.a(f2)) {
            Toast.makeText(this, (int) R.string.load_rom_failed, 0).show();
            finish();
            return false;
        } else {
            this.b.a(this.a.getGameScreenWidth(), this.a.getGameScreenHeight());
            if (this.i.getBoolean("quickLoadOnStart", true)) {
                m();
            }
            return true;
        }
    }

    private void h() {
        Intent intent = new Intent(this, GameStateActivity.class);
        intent.setData(getIntent().getData());
        startActivityForResult(intent, 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void i() {
        Intent intent = new Intent(this, GameStateActivity.class);
        intent.setData(getIntent().getData());
        intent.putExtra("saveMode", true);
        startActivityForResult(intent, 2);
    }

    private Bitmap j() {
        int gameScreenWidth = this.a.getGameScreenWidth();
        int gameScreenHeight = this.a.getGameScreenHeight();
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(gameScreenWidth * gameScreenHeight * 2);
        this.a.getScreenshot(allocateDirect);
        Bitmap createBitmap = Bitmap.createBitmap(gameScreenWidth, gameScreenHeight, Bitmap.Config.RGB_565);
        createBitmap.copyPixelsFromBuffer(allocateDirect);
        return createBitmap;
    }

    private String k() {
        return GameStateActivity.a(f(), 0);
    }

    /* access modifiers changed from: private */
    public void l() {
        d(k());
    }

    private void m() {
        e(k());
    }

    public void a() {
        int a2 = this.d.a();
        if (this.f != null) {
            int a3 = this.f.a();
            if ((a3 & 1) != 0) {
                a2 |= this.g[0];
            }
            if ((a3 & 2) != 0) {
                a2 |= this.g[1];
            }
            if ((a3 & 4) != 0) {
                a2 |= this.g[2];
            }
            if ((a3 & 8) != 0) {
                a2 |= this.g[3];
            }
        }
        if (this.e != null) {
            a2 |= this.e.a();
        }
        if ((a2 & 48) == 48) {
            a2 &= -49;
        }
        if ((a2 & 192) == 192) {
            a2 &= -193;
        }
        this.a.setKeys(a2);
    }

    public void a(Canvas canvas) {
        if (this.e != null) {
            this.e.a(canvas);
        }
    }

    public boolean a(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        int i2 = (int) (x * ((float) this.h));
        int i3 = (int) (y * ((float) this.h));
        int i4 = i2 < 0 ? 32 : i2 > 0 ? 16 : 0;
        int i5 = i3 < 0 ? 64 : i3 > 0 ? 128 : 0;
        if (i4 == 0 && i5 == 0) {
            return false;
        }
        this.a.setTrackball(i4, Math.abs(i2), i5, Math.abs(i3));
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        switch (i2) {
            case 1:
                if (i3 == -1) {
                    e(intent.getData().getPath());
                    return;
                }
                return;
            case 2:
                if (i3 == -1) {
                    d(intent.getData().getPath());
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        b();
        a(this.i, configuration);
        c();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!"android.intent.action.VIEW".equals(getIntent().getAction())) {
            finish();
            return;
        }
        requestWindowFeature(1);
        setVolumeControlStream(3);
        this.i = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences sharedPreferences = this.i;
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        this.a = EmuCore.a(getApplicationContext(), a(sharedPreferences));
        EmuNative.setOnFrameDrawnListener(this);
        setContentView((int) R.layout.emulator);
        this.b = (EmuView) findViewById(R.id.emulator);
        this.b.getHolder().addCallback(this);
        this.b.setOnTouchListener(this);
        this.b.requestFocus();
        this.d = new com.tiger.c.b(this.b, this);
        for (String onSharedPreferenceChanged : new String[]{"fullScreenMode", "flipScreen", "soundEnabled", "enableTrackball", "trackballSensitivity", "enableSensor", "sensorSensitivity", "enableVKeypad", "scalingMode", "orientation", "useInputMethod"}) {
            onSharedPreferenceChanged(sharedPreferences, onSharedPreferenceChanged);
        }
        b(sharedPreferences);
        if (!g()) {
            finish();
            return;
        }
        startService(new Intent(this, EmuService.class).setAction("com.tiger.actions.FOREGROUND"));
        getWindow().setFlags(0, 131072);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        switch (i2) {
            case 1:
                return d();
            case 2:
                return e();
            default:
                return super.onCreateDialog(i2);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.emulator, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.a != null) {
            this.a.b();
        }
        stopService(new Intent(this, EmuService.class));
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 27 || i2 == 84) {
            return true;
        }
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        b();
        showDialog(1);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        if ("android.intent.action.VIEW".equals(intent.getAction())) {
            this.j = intent;
            b();
            showDialog(2);
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_load_state /*2131099674*/:
                h();
                return true;
            case R.id.menu_save_state /*2131099675*/:
                i();
                return true;
            case R.id.menu_reset /*2131099676*/:
                this.a.reset();
                return true;
            case R.id.menu_close /*2131099677*/:
                finish();
                return true;
            case R.id.menu_settings /*2131099678*/:
                startActivity(new Intent(this, EmuSettings.class));
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        b();
        if (this.f != null) {
            this.f.a((c) null);
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        b();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.f != null) {
            this.f.a(this);
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if (str.startsWith("gamepad")) {
            b(sharedPreferences);
        } else if ("fullScreenMode".equals(str)) {
            WindowManager.LayoutParams attributes = getWindow().getAttributes();
            if (sharedPreferences.getBoolean("fullScreenMode", true)) {
                attributes.flags |= 1024;
            } else {
                attributes.flags &= -1025;
            }
            getWindow().setAttributes(attributes);
        } else if ("flipScreen".equals(str)) {
            a(sharedPreferences, getResources().getConfiguration());
        } else if ("soundEnabled".equals(str)) {
            this.a.a(str, sharedPreferences.getBoolean(str, true));
        } else if ("enableTrackball".equals(str)) {
            this.b.a(sharedPreferences.getBoolean(str, true) ? this : null);
        } else if ("trackballSensitivity".equals(str)) {
            this.h = (sharedPreferences.getInt(str, 2) * 5) + 10;
        } else if ("enableSensor".equals(str)) {
            if (!sharedPreferences.getBoolean(str, false)) {
                this.f = null;
            } else if (this.f == null) {
                this.f = new com.tiger.c.a(this);
                this.f.a(sharedPreferences.getInt("sensorSensitivity", 7));
            }
        } else if ("sensorSensitivity".equals(str)) {
            if (this.f != null) {
                this.f.a(sharedPreferences.getInt(str, 7));
            }
        } else if ("enableVKeypad".equals(str)) {
            if (!sharedPreferences.getBoolean(str, true)) {
                if (this.e != null) {
                    this.e.c();
                    this.e = null;
                }
            } else if (this.e == null) {
                this.e = new e(this.b, this);
            }
        } else if ("scalingMode".equals(str)) {
            this.b.a(a(sharedPreferences.getString(str, "proportional")));
        } else if ("orientation".equals(str)) {
            setRequestedOrientation(b(sharedPreferences.getString(str, "unspecified")));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tiger.c.e.a(android.view.MotionEvent, boolean):boolean
     arg types: [android.view.MotionEvent, int]
     candidates:
      com.tiger.c.e.a(float, float):int
      com.tiger.c.e.a(int, java.lang.String):com.tiger.c.d
      com.tiger.c.e.a(int, int):void
      com.tiger.c.e.a(android.view.MotionEvent, boolean):boolean */
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.b != null) {
            int a2 = this.b.a(motionEvent.getX());
            int b2 = this.b.b(motionEvent.getY());
            int b3 = b(motionEvent);
            if (a2 > 0 && b2 > 0 && b3 != 0) {
                this.a.setMotions(b3, a2, b2);
            }
        }
        if (this.e == null) {
            return true;
        }
        this.e.a(motionEvent, false);
        return true;
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            this.d.b();
            if (this.e != null) {
                this.e.b();
            }
            this.a.setKeys(0);
            this.a.resume();
            return;
        }
        this.a.pause();
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        if (this.e != null) {
            this.e.a(i3, i4);
        }
        int gameScreenWidth = this.a.getGameScreenWidth();
        int gameScreenHeight = this.a.getGameScreenHeight();
        this.c.left = (i3 - gameScreenWidth) / 2;
        this.c.top = (i4 - gameScreenHeight) / 2;
        this.c.right = this.c.left + gameScreenWidth;
        this.c.bottom = this.c.top + gameScreenHeight;
        this.a.setSurfaceRegion(this.c.left, this.c.top, gameScreenWidth, gameScreenHeight);
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.a.setSurface(surfaceHolder);
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (this.e != null) {
            this.e.c();
        }
        this.a.setSurface(null);
    }
}
