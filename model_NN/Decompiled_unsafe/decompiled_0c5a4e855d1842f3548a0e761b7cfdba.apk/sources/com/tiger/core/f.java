package com.tiger.core;

import android.content.Context;

public class f {
    private static final int[] a;
    private static final int[] b;

    static {
        int[] iArr = new int[16];
        iArr[0] = 8;
        iArr[1] = 29;
        iArr[2] = 45;
        iArr[3] = 51;
        iArr[8] = 67;
        iArr[9] = 66;
        iArr[10] = 39;
        iArr[11] = 40;
        iArr[12] = 55;
        iArr[13] = 37;
        iArr[14] = 43;
        iArr[15] = 44;
        a = iArr;
        int[] iArr2 = new int[16];
        iArr2[10] = 5;
        iArr2[12] = 84;
        b = iArr2;
        if (b.length != a.length) {
            throw new AssertionError("Key configurations are not consistent");
        }
    }

    public static int[] a(Context context) {
        int[] iArr = b(context) ? a : b;
        if (c(context)) {
            iArr[0] = 19;
            iArr[1] = 20;
            iArr[2] = 21;
            iArr[3] = 22;
        }
        return iArr;
    }

    private static boolean b(Context context) {
        return context.getResources().getConfiguration().keyboard == 2;
    }

    private static boolean c(Context context) {
        return context.getResources().getConfiguration().navigation != 3;
    }
}
