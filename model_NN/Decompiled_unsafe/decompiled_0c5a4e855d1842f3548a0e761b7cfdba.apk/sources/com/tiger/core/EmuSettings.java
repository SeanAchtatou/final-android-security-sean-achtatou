package com.tiger.core;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.widget.Toast;
import com.tiger.b.a;
import com.tiger.nds.R;

public class EmuSettings extends PreferenceActivity implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {
    public static final int[] a = {64, 128, 32, 16, 96, 80, 160, 144, 4, 8, 1, 2, 1024, 2048, 512, 256};
    public static final String[] b = {"gamepad_up", "gamepad_down", "gamepad_left", "gamepad_right", "gamepad_up_left", "gamepad_up_right", "gamepad_down_left", "gamepad_down_right", "gamepad_select", "gamepad_start", "gamepad_A", "gamepad_B", "gamepad_X", "gamepad_Y", "gamepad_TL", "gamepad_TR"};
    private static final Uri c = Uri.parse("file:///android_asset/about.html");
    private static final Uri d = Uri.parse("file:///android_asset/about_cn.html");
    private static final int[] e = {R.string.gamepad_up, R.string.gamepad_down, R.string.gamepad_left, R.string.gamepad_right, R.string.gamepad_up_left, R.string.gamepad_up_right, R.string.gamepad_down_left, R.string.gamepad_down_right, R.string.gamepad_select, R.string.gamepad_start, R.string.gamepad_A, R.string.gamepad_B, R.string.gamepad_X, R.string.gamepad_Y, R.string.gamepad_TL, R.string.gamepad_TR};
    private SharedPreferences f;

    static {
        int length = a.length;
        if (b.length != length || e.length != length) {
            throw new AssertionError("Key configurations are not consistent");
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle((int) R.string.settings);
        addPreferencesFromResource(R.xml.preferences);
        this.f = PreferenceManager.getDefaultSharedPreferences(this);
        int[] a2 = f.a(this);
        PreferenceGroup preferenceGroup = (PreferenceGroup) findPreference("gamepad1");
        for (int i = 0; i < b.length; i++) {
            KeyPreference keyPreference = new KeyPreference(this);
            keyPreference.setKey(b[i]);
            keyPreference.setTitle(e[i]);
            keyPreference.setDefaultValue(Integer.valueOf(a2[i]));
            preferenceGroup.addPreference(keyPreference);
        }
        findPreference("soundEnabled").setEnabled(false);
    }

    public boolean onPreferenceChange(Preference preference, Object obj) {
        Toast.makeText(this, (int) R.string.game_reset_needed_prompt, 0).show();
        return true;
    }

    public boolean onPreferenceClick(Preference preference) {
        a.a(this, "nds");
        return true;
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        preference.getKey();
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }
}
