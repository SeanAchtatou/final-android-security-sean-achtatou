package com.tiger.core;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.tiger.list.ImageListCache;
import com.tiger.nds.R;
import com.tiger.nds.a;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class RomChooser extends ImageListCache implements m {
    protected boolean a = false;
    protected final Handler b = new b(this);
    /* access modifiers changed from: private */
    public int c = 2;
    /* access modifiers changed from: private */
    public boolean d = true;
    private File e = null;
    private ProgressDialog f;

    private Date a(File file) {
        if (file != null) {
            return new Date(file.lastModified());
        }
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse("2000-01-01");
        } catch (ParseException e2) {
            return null;
        }
    }

    private boolean e(String str) {
        for (int i = 0; i < 8; i++) {
            if (new File(GameStateActivity.a(str, i)).exists()) {
                return true;
            }
        }
        return false;
    }

    private void f(String str) {
        for (int i = 0; i < 8; i++) {
            String a2 = GameStateActivity.a(str, i);
            File file = new File(a2);
            if (file.exists()) {
                file.delete();
            }
            File file2 = new File(GameStateActivity.a(a2));
            if (file2.exists()) {
                file2.delete();
            }
        }
    }

    private void g(String str) {
        f(str);
        new File(str).delete();
    }

    private File h(String str) {
        File file = null;
        for (int i = 0; i < 8; i++) {
            File file2 = new File(GameStateActivity.a(str, i));
            if (file2.exists()) {
                if (file == null) {
                    file = file2;
                } else if (file.lastModified() < file2.lastModified()) {
                    file = file2;
                }
            }
        }
        return file;
    }

    private Bitmap i(String str) {
        if (this.e != null) {
            return GameStateActivity.a(new File(GameStateActivity.a(this.e.getAbsolutePath())));
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Date a(String str) {
        return a(h(str));
    }

    /* access modifiers changed from: protected */
    public void a(Uri uri) {
        setResult(-1, new Intent().setData(uri));
        finish();
    }

    /* access modifiers changed from: protected */
    public void a(String str, int i) {
        this.e = h(str);
    }

    /* access modifiers changed from: protected */
    public int b() {
        return R.layout.rom_list;
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        if (this.f == null) {
            this.f = new ProgressDialog(this);
        }
        this.f.setMessage(str);
        this.f.setIndeterminate(true);
        this.f.setCancelable(false);
        this.f.show();
    }

    public void c(String str) {
        new a(str, "/sdcard/.ltiger/nds/rom_cache.txt", d(), true).a(this);
    }

    /* access modifiers changed from: protected */
    public String[] d() {
        return a.a;
    }

    /* access modifiers changed from: protected */
    public String e() {
        File file = new File("/sdcard/.ltiger/nds/.");
        if (file.exists()) {
            return "/sdcard/.ltiger/nds/rom_cache.txt";
        }
        file.mkdirs();
        return "/sdcard/.ltiger/nds/rom_cache.txt";
    }

    /* access modifiers changed from: protected */
    public String e(int i) {
        String l = l(i);
        int lastIndexOf = l.lastIndexOf("/");
        int lastIndexOf2 = l.lastIndexOf(".");
        return lastIndexOf2 == -1 ? l : l.substring(lastIndexOf + 1, lastIndexOf2);
    }

    /* access modifiers changed from: protected */
    public String f(int i) {
        return this.e != null ? String.valueOf(getString(R.string.rom_last_saved)) + ((Object) DateFormat.format("  yyyy-MM-dd hh:mm:ss", this.e.lastModified())) : "";
    }

    /* access modifiers changed from: protected */
    public void f() {
        if (this.f != null) {
            this.f.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap g(int i) {
        return i(l(i));
    }

    public void g() {
        b(getString(R.string.dialog_wait));
        String str = "/sdcard";
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (defaultSharedPreferences != null && defaultSharedPreferences.getBoolean("quick_search_sdcard", true)) {
            str = "/sdcard/roms/nds";
        }
        new a(str, "/sdcard/.ltiger/nds/rom_cache.txt", d(), false).a(this);
    }

    /* access modifiers changed from: protected */
    public String h(int i) {
        return null;
    }

    public void h() {
        this.b.post(new c(this));
    }

    /* access modifiers changed from: protected */
    public Comparator i() {
        if (this.c != -1) {
            return new l(this);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Date i(int i) {
        String l = l(i);
        if (l == null) {
            return null;
        }
        return a(l);
    }

    /* access modifiers changed from: protected */
    public void j(int i) {
        if (i == this.c) {
            this.d = !this.d;
        } else if (i == 1) {
            this.d = false;
        } else {
            this.d = true;
        }
        this.c = i;
        a();
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        switch (menuItem.getItemId()) {
            case 1:
                g(l(adapterContextMenuInfo.position));
                a();
                return true;
            case 2:
                f(l(adapterContextMenuInfo.position));
                a();
                return true;
            case 3:
                return true;
            default:
                return super.onContextItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        boolean z;
        boolean z2;
        AdapterView.AdapterContextMenuInfo adapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo) contextMenuInfo;
        contextMenu.setHeaderTitle(b(adapterContextMenuInfo.position));
        String l = l(adapterContextMenuInfo.position);
        if (new File(l).exists()) {
            contextMenu.add(0, 1, 0, (int) R.string.menu_delete_rom);
            z = true;
        } else {
            z = false;
        }
        if (e(l)) {
            contextMenu.add(0, 2, 0, (int) R.string.menu_clear_save);
            z2 = true;
        } else {
            z2 = z;
        }
        if (z2) {
            contextMenu.add(0, 3, 0, (int) R.string.menu_cancel);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i, long j) {
        a(Uri.fromFile(new File(l(i))));
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_search_roms /*2131099680*/:
                g();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.a) {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }
}
