package com.tiger.core;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

class l implements Comparator {
    final /* synthetic */ RomChooser a;

    l(RomChooser romChooser) {
        this.a = romChooser;
    }

    /* renamed from: a */
    public int compare(HashMap hashMap, HashMap hashMap2) {
        int i;
        int a2 = this.a.a(hashMap);
        int a3 = this.a.a(hashMap2);
        if (this.a.c == 1) {
            i = this.a.e(a2).compareTo(this.a.e(a3));
        } else if (this.a.c == 2) {
            Date i2 = this.a.i(a2);
            Date i3 = this.a.i(a3);
            i = (i2 == null || i3 == null) ? 0 : i2.compareTo(i3);
        } else {
            i = 0;
        }
        return this.a.d ? -i : i;
    }
}
