package com.tiger.core;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import com.tiger.nds.R;

public class SeekBarPreference extends DialogPreference implements SeekBar.OnSeekBarChangeListener {
    private SeekBar a;
    private TextView b;
    private int c;
    private int d;
    private int e;
    private int f;

    public SeekBarPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = attributeSet.getAttributeIntValue("http://tiger.com/apk/res/android", "minValue", 0);
        this.d = attributeSet.getAttributeIntValue("http://tiger.com/apk/res/android", "maxValue", 100);
        setDialogLayoutResource(R.layout.seekbar_dialog);
        setPositiveButtonText(17039370);
        setNegativeButtonText(17039360);
    }

    /* access modifiers changed from: protected */
    public void onBindDialogView(View view) {
        super.onBindDialogView(view);
        if (this.f < this.c) {
            this.f = this.c;
        }
        if (this.f > this.d) {
            this.f = this.d;
        }
        this.a = (SeekBar) view.findViewById(R.id.seekbar);
        this.a.setMax(this.d - this.c);
        this.a.setProgress(this.f - this.c);
        this.a.setSecondaryProgress(this.f - this.c);
        this.a.setOnSeekBarChangeListener(this);
        this.b = (TextView) view.findViewById(R.id.value);
        this.b.setText(Integer.toString(this.f));
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean z) {
        super.onDialogClosed(z);
        if (!z) {
            this.f = this.e;
            return;
        }
        this.e = this.f;
        persistInt(this.f);
    }

    /* access modifiers changed from: protected */
    public Object onGetDefaultValue(TypedArray typedArray, int i) {
        return Integer.valueOf(typedArray.getInteger(i, 0));
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        this.f = this.c + i;
        this.b.setText(Integer.toString(this.f));
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean z, Object obj) {
        this.e = z ? getPersistedInt(0) : ((Integer) obj).intValue();
        this.f = this.e;
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
