package com.tiger.core;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.KeyEvent;
import com.tiger.nds.R;

public class KeyPreference extends DialogPreference implements DialogInterface.OnKeyListener {
    private Resources a;
    private int b;
    private int c;

    public KeyPreference(Context context) {
        this(context, null);
    }

    public KeyPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = context.getResources();
        setPositiveButtonText((int) R.string.key_clear);
        setDefaultValue(0);
    }

    private void a() {
        setSummary(b(this.c));
    }

    private static boolean a(int i) {
        switch (i) {
            case 3:
            case 26:
            case 82:
                return false;
            default:
                return true;
        }
    }

    private String b(int i) {
        switch (i) {
            case 0:
                return this.a.getString(R.string.key_none);
            case 1:
            case 2:
            case 3:
            case 6:
            case 17:
            case 18:
            case 26:
            case 28:
            case 61:
            case 63:
            case 64:
            case 65:
            case 68:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 75:
            case 76:
            case 78:
            case 79:
            case 81:
            case 82:
            case 83:
            default:
                return this.a.getString(R.string.key_unknown);
            case 4:
                return "BACK";
            case 5:
                return "CALL";
            case 7:
                return "0";
            case 8:
                return "1";
            case 9:
                return "2";
            case 10:
                return "3";
            case 11:
                return "4";
            case 12:
                return "5";
            case 13:
                return "6";
            case 14:
                return "7";
            case 15:
                return "8";
            case 16:
                return "9";
            case 19:
                return "DPAD Up";
            case 20:
                return "DPAD Down";
            case 21:
                return "DPAD Left";
            case 22:
                return "DPAD Right";
            case 23:
                return "DPAD Center";
            case 24:
                return "Volume UP";
            case 25:
                return "Volume DOWN";
            case 27:
                return "CAMERA";
            case 29:
                return "A";
            case 30:
                return "B";
            case 31:
                return "C";
            case 32:
                return "D";
            case 33:
                return "E";
            case 34:
                return "F";
            case 35:
                return "G";
            case 36:
                return "H";
            case 37:
                return "I";
            case 38:
                return "J";
            case 39:
                return "K";
            case 40:
                return "L";
            case 41:
                return "M";
            case 42:
                return "N";
            case 43:
                return "O";
            case 44:
                return "P";
            case 45:
                return "Q";
            case 46:
                return "R";
            case 47:
                return "S";
            case 48:
                return "T";
            case 49:
                return "U";
            case 50:
                return "V";
            case 51:
                return "W";
            case 52:
                return "X";
            case 53:
                return "Y";
            case 54:
                return "Z";
            case 55:
                return ",";
            case 56:
                return ".";
            case 57:
                return "ALT (left)";
            case 58:
                return "ALT (right)";
            case 59:
                return "SHIFT (left)";
            case 60:
                return "SHIFT (right)";
            case 62:
                return "SPACE";
            case 66:
                return "ENTER";
            case 67:
                return "DEL";
            case 77:
                return "@";
            case 80:
                return "FOCUS";
            case 84:
                return "SEARCH";
        }
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == -1) {
            this.c = 0;
        }
        super.onClick(dialogInterface, i);
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean z) {
        super.onDialogClosed(z);
        if (!z) {
            this.c = this.b;
            return;
        }
        this.b = this.c;
        persistInt(this.c);
        a();
    }

    /* access modifiers changed from: protected */
    public Object onGetDefaultValue(TypedArray typedArray, int i) {
        return Integer.valueOf(typedArray.getInteger(i, 0));
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (!a(i)) {
            return false;
        }
        this.c = i;
        super.onClick(dialogInterface, -1);
        dialogInterface.dismiss();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        super.onPrepareDialogBuilder(builder);
        builder.setMessage((int) R.string.press_key_prompt).setOnKeyListener(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean z, Object obj) {
        this.b = z ? getPersistedInt(0) : ((Integer) obj).intValue();
        this.c = this.b;
        a();
    }

    /* access modifiers changed from: protected */
    public void showDialog(Bundle bundle) {
        super.showDialog(bundle);
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().clearFlags(131072);
        }
    }
}
