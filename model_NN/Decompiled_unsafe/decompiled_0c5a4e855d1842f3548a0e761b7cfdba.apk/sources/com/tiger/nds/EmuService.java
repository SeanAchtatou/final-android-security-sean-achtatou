package com.tiger.nds;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.tiger.core.EmuActivity;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class EmuService extends Service {
    private static final Class[] a = {Integer.TYPE, Notification.class};
    private static final Class[] b = {Boolean.TYPE};
    private NotificationManager c;
    private Method d;
    private Method e;
    private Object[] f = new Object[2];
    private Object[] g = new Object[1];

    /* access modifiers changed from: package-private */
    public void a(int i) {
        if (this.e != null) {
            this.g[0] = Boolean.TRUE;
            try {
                this.e.invoke(this, this.g);
            } catch (InvocationTargetException e2) {
                Log.w("EmulatorService", "Unable to invoke stopForeground", e2);
            } catch (IllegalAccessException e3) {
                Log.w("EmulatorService", "Unable to invoke stopForeground", e3);
            }
        } else {
            this.c.cancel(i);
            setForeground(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i, Notification notification) {
        if (this.d != null) {
            this.f[0] = Integer.valueOf(i);
            this.f[1] = notification;
            try {
                this.d.invoke(this, this.f);
            } catch (InvocationTargetException e2) {
                Log.w("EmulatorService", "Unable to invoke startForeground", e2);
            } catch (IllegalAccessException e3) {
                Log.w("EmulatorService", "Unable to invoke startForeground", e3);
            }
        } else {
            setForeground(true);
            this.c.notify(i, notification);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Intent intent) {
        if ("com.tiger.actions.FOREGROUND".equals(intent.getAction())) {
            CharSequence text = getText(R.string.emulator_service_running);
            Notification notification = new Notification(R.drawable.app_icon, text, System.currentTimeMillis());
            notification.setLatestEventInfo(this, getText(R.string.app_label), text, PendingIntent.getActivity(this, 0, new Intent(this, EmuActivity.class), 0));
            a(R.string.emulator_service_running, notification);
        } else if ("com.tiger.actions.BACKGROUND".equals(intent.getAction())) {
            a((int) R.string.emulator_service_running);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        this.c = (NotificationManager) getSystemService("notification");
        try {
            this.d = getClass().getMethod("startForeground", a);
            this.e = getClass().getMethod("stopForeground", b);
        } catch (NoSuchMethodException e2) {
            this.e = null;
            this.d = null;
        }
    }

    public void onDestroy() {
        a((int) R.string.emulator_service_running);
    }

    public void onStart(Intent intent, int i) {
        a(intent);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        a(intent);
        return 0;
    }
}
