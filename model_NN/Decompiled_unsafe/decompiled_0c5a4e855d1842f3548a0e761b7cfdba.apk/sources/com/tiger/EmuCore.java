package com.tiger;

import android.content.Context;
import android.os.Build;
import android.view.SurfaceHolder;
import java.nio.Buffer;

public class EmuCore {
    private static String a;
    private static EmuCore b;
    private Thread c = new c(this);
    private String d;

    private EmuCore(String str) {
        initialize(str, Integer.parseInt(Build.VERSION.SDK));
        this.c.start();
    }

    public static EmuCore a() {
        return b;
    }

    public static EmuCore a(Context context, String str) {
        if (b == null) {
            System.loadLibrary("tigercore");
        }
        String str2 = "/data/data/" + context.getPackageName() + "/lib";
        if (!str.equals(a)) {
            a = str;
            loadGameEngine(str2, str);
        }
        if (b == null) {
            b = new EmuCore(str2);
        }
        return b;
    }

    private native boolean initialize(String str, int i);

    private static native boolean loadGameEngine(String str, String str2);

    private native boolean loadRomFile(String str);

    /* access modifiers changed from: private */
    public native void runEmu();

    private native void unloadRomFile();

    public void a(String str, boolean z) {
        setOption(str, z ? "true" : "false");
    }

    public final boolean a(String str) {
        if (!loadRomFile(str)) {
            return false;
        }
        this.d = str;
        return true;
    }

    public final void b() {
        unloadRomFile();
        this.d = null;
    }

    public native int getGameScreenHeight();

    public native int getGameScreenWidth();

    public native void getScreenshot(Buffer buffer);

    public native boolean loadState(String str);

    public native void pause();

    public native void reset();

    public native void resume();

    public native boolean saveState(String str);

    public native void setKeys(int i);

    public native void setMotions(int i, int i2, int i3);

    public native void setOption(String str, String str2);

    public native void setScreenUpdateListener(d dVar);

    public native void setSurface(SurfaceHolder surfaceHolder);

    public native void setSurfaceRegion(int i, int i2, int i3, int i4);

    public native void setTrackball(int i, int i2, int i3, int i4);
}
