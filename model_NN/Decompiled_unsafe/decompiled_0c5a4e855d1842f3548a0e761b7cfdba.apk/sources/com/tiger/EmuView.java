package com.tiger;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class EmuView extends SurfaceView {
    private a a;
    private int b = 2;
    private int c;
    private int d;
    private float e;
    private int f;
    private int g;
    private float h;
    private float i;
    private float j;
    private float k;

    public EmuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        SurfaceHolder holder = getHolder();
        holder.setFormat(4);
        holder.setKeepScreenOn(true);
        setFocusableInTouchMode(true);
    }

    private void a() {
        int i2;
        int i3;
        int i4;
        int i5;
        float f2;
        int width = getWidth();
        int height = getHeight();
        if (width != 0 && height != 0 && this.c != 0 && this.d != 0) {
            this.f = width;
            this.g = height;
            if (!(this.b == 3 || this.e == 0.0f)) {
                width = (int) (((float) width) / ((this.e * ((float) this.d)) / ((float) this.c)));
            }
            switch (this.b) {
                case 0:
                    i3 = height;
                    i2 = width;
                    break;
                case 1:
                    i2 = width / 2;
                    i3 = height / 2;
                    break;
                case 3:
                    if (this.d * width >= this.c * height) {
                        i2 = this.c;
                        i3 = this.d;
                        break;
                    }
                case 2:
                default:
                    i3 = 0;
                    i2 = 0;
                    break;
            }
            float f3 = 1.0f;
            if (i2 < this.c || i3 < this.d) {
                i3 = this.d;
                i2 = (i3 * width) / height;
                f3 = (float) (height / i3);
                if (i2 < this.c) {
                    int i6 = this.c;
                    i5 = (i6 * height) / width;
                    i4 = i6;
                    f2 = (float) (width / i6);
                    int i7 = (i4 + 3) & -4;
                    int i8 = (i5 + 3) & -4;
                    getHolder().setFixedSize(i7, i8);
                    this.h = (((float) width) * f2) / ((float) i7);
                    this.i = (((float) height) * f2) / ((float) i8);
                    this.j = (this.h * ((float) (i7 - this.c))) / 2.0f;
                    this.k = (this.i * ((float) (i8 - this.d))) / 2.0f;
                }
            }
            float f4 = f3;
            i4 = i2;
            i5 = i3;
            f2 = f4;
            int i72 = (i4 + 3) & -4;
            int i82 = (i5 + 3) & -4;
            getHolder().setFixedSize(i72, i82);
            this.h = (((float) width) * f2) / ((float) i72);
            this.i = (((float) height) * f2) / ((float) i82);
            this.j = (this.h * ((float) (i72 - this.c))) / 2.0f;
            this.k = (this.i * ((float) (i82 - this.d))) / 2.0f;
        }
    }

    public int a(float f2) {
        if (f2 < this.j || f2 >= ((float) this.f) - this.j) {
            return -1;
        }
        return (int) (((double) ((f2 - this.j) / this.h)) + 0.5d);
    }

    public void a(int i2) {
        if (this.b != i2) {
            this.b = i2;
            a();
        }
    }

    public void a(int i2, int i3) {
        if (this.c != i2 || this.d != i3) {
            this.c = i2;
            this.d = i3;
            a();
        }
    }

    public void a(a aVar) {
        this.a = aVar;
    }

    public int b(float f2) {
        if (f2 < this.k || f2 >= ((float) this.g) - this.k) {
            return -1;
        }
        return (int) (((double) ((f2 - this.k) / this.i)) + 0.5d);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        a();
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        if (this.a == null || !this.a.a(motionEvent)) {
            return super.onTrackballEvent(motionEvent);
        }
        return true;
    }
}
