package android.support.v4.util;

public final class Pools {

    public interface Pool<T> {
        T acquire();

        boolean release(T t);
    }

    private Pools() {
    }

    public static class SimplePool<T> implements Pool<T> {
        private final Object[] mPool;
        private int mPoolSize;

        public SimplePool(int i) {
            Throwable th;
            int i2 = i;
            if (i2 <= 0) {
                Throwable th2 = th;
                new IllegalArgumentException("The max pool size must be > 0");
                throw th2;
            }
            this.mPool = new Object[i2];
        }

        public T acquire() {
            if (this.mPoolSize <= 0) {
                return null;
            }
            int i = this.mPoolSize - 1;
            T t = this.mPool[i];
            this.mPool[i] = null;
            this.mPoolSize = this.mPoolSize - 1;
            return t;
        }

        public boolean release(T t) {
            Throwable th;
            T t2 = t;
            if (isInPool(t2)) {
                Throwable th2 = th;
                new IllegalStateException("Already in the pool!");
                throw th2;
            } else if (this.mPoolSize >= this.mPool.length) {
                return false;
            } else {
                this.mPool[this.mPoolSize] = t2;
                this.mPoolSize = this.mPoolSize + 1;
                return true;
            }
        }

        private boolean isInPool(T t) {
            T t2 = t;
            for (int i = 0; i < this.mPoolSize; i++) {
                if (this.mPool[i] == t2) {
                    return true;
                }
            }
            return false;
        }
    }

    public static class SynchronizedPool<T> extends SimplePool<T> {
        private final Object mLock;

        public SynchronizedPool(int i) {
            super(i);
            Object obj;
            new Object();
            this.mLock = obj;
        }

        public T acquire() {
            Object obj = this.mLock;
            Object obj2 = obj;
            synchronized (obj) {
                try {
                    T acquire = super.acquire();
                    return acquire;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    Object obj3 = obj2;
                    throw th2;
                }
            }
        }

        public boolean release(T t) {
            T t2 = t;
            Object obj = this.mLock;
            Object obj2 = obj;
            synchronized (obj) {
                try {
                    boolean release = super.release(t2);
                    return release;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    Object obj3 = obj2;
                    throw th2;
                }
            }
        }
    }
}
