package android.support.v4.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

abstract class MapCollections<K, V> {
    MapCollections<K, V>.EntrySet mEntrySet;
    MapCollections<K, V>.KeySet mKeySet;
    MapCollections<K, V>.ValuesCollection mValues;

    /* access modifiers changed from: protected */
    public abstract void colClear();

    /* access modifiers changed from: protected */
    public abstract Object colGetEntry(int i, int i2);

    /* access modifiers changed from: protected */
    public abstract Map<K, V> colGetMap();

    /* access modifiers changed from: protected */
    public abstract int colGetSize();

    /* access modifiers changed from: protected */
    public abstract int colIndexOfKey(Object obj);

    /* access modifiers changed from: protected */
    public abstract int colIndexOfValue(Object obj);

    /* access modifiers changed from: protected */
    public abstract void colPut(K k, V v);

    /* access modifiers changed from: protected */
    public abstract void colRemoveAt(int i);

    /* access modifiers changed from: protected */
    public abstract V colSetValue(int i, V v);

    MapCollections() {
    }

    final class ArrayIterator<T> implements Iterator<T> {
        boolean mCanRemove = false;
        int mIndex;
        final int mOffset;
        int mSize;
        final /* synthetic */ MapCollections this$0;

        ArrayIterator(MapCollections mapCollections, int i) {
            MapCollections mapCollections2 = mapCollections;
            this.this$0 = mapCollections2;
            this.mOffset = i;
            this.mSize = mapCollections2.colGetSize();
        }

        public boolean hasNext() {
            return this.mIndex < this.mSize;
        }

        public T next() {
            this.mIndex = this.mIndex + 1;
            this.mCanRemove = true;
            return this.this$0.colGetEntry(this.mIndex, this.mOffset);
        }

        public void remove() {
            Throwable th;
            if (!this.mCanRemove) {
                Throwable th2 = th;
                new IllegalStateException();
                throw th2;
            }
            this.mIndex = this.mIndex - 1;
            this.mSize = this.mSize - 1;
            this.mCanRemove = false;
            this.this$0.colRemoveAt(this.mIndex);
        }
    }

    final class MapIterator implements Iterator<Map.Entry<K, V>>, Map.Entry<K, V> {
        int mEnd;
        boolean mEntryValid = false;
        int mIndex;
        final /* synthetic */ MapCollections this$0;

        MapIterator(MapCollections mapCollections) {
            MapCollections mapCollections2 = mapCollections;
            this.this$0 = mapCollections2;
            this.mEnd = mapCollections2.colGetSize() - 1;
            this.mIndex = -1;
        }

        public boolean hasNext() {
            return this.mIndex < this.mEnd;
        }

        public Map.Entry<K, V> next() {
            this.mIndex = this.mIndex + 1;
            this.mEntryValid = true;
            return this;
        }

        public void remove() {
            Throwable th;
            if (!this.mEntryValid) {
                Throwable th2 = th;
                new IllegalStateException();
                throw th2;
            }
            this.this$0.colRemoveAt(this.mIndex);
            this.mIndex = this.mIndex - 1;
            this.mEnd = this.mEnd - 1;
            this.mEntryValid = false;
        }

        public K getKey() {
            Throwable th;
            if (this.mEntryValid) {
                return this.this$0.colGetEntry(this.mIndex, 0);
            }
            Throwable th2 = th;
            new IllegalStateException("This container does not support retaining Map.Entry objects");
            throw th2;
        }

        public V getValue() {
            Throwable th;
            if (this.mEntryValid) {
                return this.this$0.colGetEntry(this.mIndex, 1);
            }
            Throwable th2 = th;
            new IllegalStateException("This container does not support retaining Map.Entry objects");
            throw th2;
        }

        public V setValue(V v) {
            Throwable th;
            V v2 = v;
            if (this.mEntryValid) {
                return this.this$0.colSetValue(this.mIndex, v2);
            }
            Throwable th2 = th;
            new IllegalStateException("This container does not support retaining Map.Entry objects");
            throw th2;
        }

        public final boolean equals(Object obj) {
            Throwable th;
            Object obj2 = obj;
            if (!this.mEntryValid) {
                Throwable th2 = th;
                new IllegalStateException("This container does not support retaining Map.Entry objects");
                throw th2;
            } else if (!(obj2 instanceof Map.Entry)) {
                return false;
            } else {
                Map.Entry entry = (Map.Entry) obj2;
                return ContainerHelpers.equal(entry.getKey(), this.this$0.colGetEntry(this.mIndex, 0)) && ContainerHelpers.equal(entry.getValue(), this.this$0.colGetEntry(this.mIndex, 1));
            }
        }

        public final int hashCode() {
            Throwable th;
            if (!this.mEntryValid) {
                Throwable th2 = th;
                new IllegalStateException("This container does not support retaining Map.Entry objects");
                throw th2;
            }
            Object colGetEntry = this.this$0.colGetEntry(this.mIndex, 0);
            Object colGetEntry2 = this.this$0.colGetEntry(this.mIndex, 1);
            return (colGetEntry == null ? 0 : colGetEntry.hashCode()) ^ (colGetEntry2 == null ? 0 : colGetEntry2.hashCode());
        }

        public final String toString() {
            StringBuilder sb;
            new StringBuilder();
            return sb.append(getKey()).append("=").append(getValue()).toString();
        }
    }

    final class EntrySet implements Set<Map.Entry<K, V>> {
        EntrySet() {
        }

        public boolean add(Map.Entry<K, V> entry) {
            Throwable th;
            Throwable th2 = th;
            new UnsupportedOperationException();
            throw th2;
        }

        public boolean addAll(Collection<? extends Map.Entry<K, V>> collection) {
            int colGetSize = MapCollections.this.colGetSize();
            for (Map.Entry entry : collection) {
                MapCollections.this.colPut(entry.getKey(), entry.getValue());
            }
            return colGetSize != MapCollections.this.colGetSize();
        }

        public void clear() {
            MapCollections.this.colClear();
        }

        public boolean contains(Object obj) {
            Object obj2 = obj;
            if (!(obj2 instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj2;
            int colIndexOfKey = MapCollections.this.colIndexOfKey(entry.getKey());
            if (colIndexOfKey < 0) {
                return false;
            }
            return ContainerHelpers.equal(MapCollections.this.colGetEntry(colIndexOfKey, 1), entry.getValue());
        }

        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        public boolean isEmpty() {
            return MapCollections.this.colGetSize() == 0;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            Iterator<Map.Entry<K, V>> it;
            new MapIterator(MapCollections.this);
            return it;
        }

        public boolean remove(Object obj) {
            Throwable th;
            Throwable th2 = th;
            new UnsupportedOperationException();
            throw th2;
        }

        public boolean removeAll(Collection<?> collection) {
            Throwable th;
            Throwable th2 = th;
            new UnsupportedOperationException();
            throw th2;
        }

        public boolean retainAll(Collection<?> collection) {
            Throwable th;
            Throwable th2 = th;
            new UnsupportedOperationException();
            throw th2;
        }

        public int size() {
            return MapCollections.this.colGetSize();
        }

        public Object[] toArray() {
            Throwable th;
            Throwable th2 = th;
            new UnsupportedOperationException();
            throw th2;
        }

        public <T> T[] toArray(T[] tArr) {
            Throwable th;
            Throwable th2 = th;
            new UnsupportedOperationException();
            throw th2;
        }

        public boolean equals(Object obj) {
            return MapCollections.equalsSetHelper(this, obj);
        }

        public int hashCode() {
            int i = 0;
            for (int colGetSize = MapCollections.this.colGetSize() - 1; colGetSize >= 0; colGetSize--) {
                Object colGetEntry = MapCollections.this.colGetEntry(colGetSize, 0);
                Object colGetEntry2 = MapCollections.this.colGetEntry(colGetSize, 1);
                i += (colGetEntry == null ? 0 : colGetEntry.hashCode()) ^ (colGetEntry2 == null ? 0 : colGetEntry2.hashCode());
            }
            return i;
        }
    }

    final class KeySet implements Set<K> {
        KeySet() {
        }

        public boolean add(K k) {
            Throwable th;
            Throwable th2 = th;
            new UnsupportedOperationException();
            throw th2;
        }

        public boolean addAll(Collection<? extends K> collection) {
            Throwable th;
            Throwable th2 = th;
            new UnsupportedOperationException();
            throw th2;
        }

        public void clear() {
            MapCollections.this.colClear();
        }

        public boolean contains(Object obj) {
            return MapCollections.this.colIndexOfKey(obj) >= 0;
        }

        public boolean containsAll(Collection<?> collection) {
            return MapCollections.containsAllHelper(MapCollections.this.colGetMap(), collection);
        }

        public boolean isEmpty() {
            return MapCollections.this.colGetSize() == 0;
        }

        public Iterator<K> iterator() {
            Iterator<K> it;
            new ArrayIterator(MapCollections.this, 0);
            return it;
        }

        public boolean remove(Object obj) {
            int colIndexOfKey = MapCollections.this.colIndexOfKey(obj);
            if (colIndexOfKey < 0) {
                return false;
            }
            MapCollections.this.colRemoveAt(colIndexOfKey);
            return true;
        }

        public boolean removeAll(Collection<?> collection) {
            return MapCollections.removeAllHelper(MapCollections.this.colGetMap(), collection);
        }

        public boolean retainAll(Collection<?> collection) {
            return MapCollections.retainAllHelper(MapCollections.this.colGetMap(), collection);
        }

        public int size() {
            return MapCollections.this.colGetSize();
        }

        public Object[] toArray() {
            return MapCollections.this.toArrayHelper(0);
        }

        public <T> T[] toArray(T[] tArr) {
            return MapCollections.this.toArrayHelper(tArr, 0);
        }

        public boolean equals(Object obj) {
            return MapCollections.equalsSetHelper(this, obj);
        }

        public int hashCode() {
            int i = 0;
            for (int colGetSize = MapCollections.this.colGetSize() - 1; colGetSize >= 0; colGetSize--) {
                Object colGetEntry = MapCollections.this.colGetEntry(colGetSize, 0);
                i += colGetEntry == null ? 0 : colGetEntry.hashCode();
            }
            return i;
        }
    }

    final class ValuesCollection implements Collection<V> {
        ValuesCollection() {
        }

        public boolean add(V v) {
            Throwable th;
            Throwable th2 = th;
            new UnsupportedOperationException();
            throw th2;
        }

        public boolean addAll(Collection<? extends V> collection) {
            Throwable th;
            Throwable th2 = th;
            new UnsupportedOperationException();
            throw th2;
        }

        public void clear() {
            MapCollections.this.colClear();
        }

        public boolean contains(Object obj) {
            return MapCollections.this.colIndexOfValue(obj) >= 0;
        }

        public boolean containsAll(Collection<?> collection) {
            for (Object contains : collection) {
                if (!contains(contains)) {
                    return false;
                }
            }
            return true;
        }

        public boolean isEmpty() {
            return MapCollections.this.colGetSize() == 0;
        }

        public Iterator<V> iterator() {
            Iterator<V> it;
            new ArrayIterator(MapCollections.this, 1);
            return it;
        }

        public boolean remove(Object obj) {
            int colIndexOfValue = MapCollections.this.colIndexOfValue(obj);
            if (colIndexOfValue < 0) {
                return false;
            }
            MapCollections.this.colRemoveAt(colIndexOfValue);
            return true;
        }

        public boolean removeAll(Collection<?> collection) {
            Collection<?> collection2 = collection;
            int colGetSize = MapCollections.this.colGetSize();
            boolean z = false;
            int i = 0;
            while (i < colGetSize) {
                if (collection2.contains(MapCollections.this.colGetEntry(i, 1))) {
                    MapCollections.this.colRemoveAt(i);
                    i--;
                    colGetSize--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        public boolean retainAll(Collection<?> collection) {
            Collection<?> collection2 = collection;
            int colGetSize = MapCollections.this.colGetSize();
            boolean z = false;
            int i = 0;
            while (i < colGetSize) {
                if (!collection2.contains(MapCollections.this.colGetEntry(i, 1))) {
                    MapCollections.this.colRemoveAt(i);
                    i--;
                    colGetSize--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        public int size() {
            return MapCollections.this.colGetSize();
        }

        public Object[] toArray() {
            return MapCollections.this.toArrayHelper(1);
        }

        public <T> T[] toArray(T[] tArr) {
            return MapCollections.this.toArrayHelper(tArr, 1);
        }
    }

    public static <K, V> boolean containsAllHelper(Map<K, V> map, Collection<?> collection) {
        Map<K, V> map2 = map;
        for (Object containsKey : collection) {
            if (!map2.containsKey(containsKey)) {
                return false;
            }
        }
        return true;
    }

    public static <K, V> boolean removeAllHelper(Map<K, V> map, Collection<?> collection) {
        Map<K, V> map2 = map;
        int size = map2.size();
        for (Object remove : collection) {
            V remove2 = map2.remove(remove);
        }
        return size != map2.size();
    }

    public static <K, V> boolean retainAllHelper(Map<K, V> map, Collection<?> collection) {
        Map<K, V> map2 = map;
        Collection<?> collection2 = collection;
        int size = map2.size();
        Iterator<K> it = map2.keySet().iterator();
        while (it.hasNext()) {
            if (!collection2.contains(it.next())) {
                it.remove();
            }
        }
        return size != map2.size();
    }

    public Object[] toArrayHelper(int i) {
        int i2 = i;
        int colGetSize = colGetSize();
        Object[] objArr = new Object[colGetSize];
        for (int i3 = 0; i3 < colGetSize; i3++) {
            objArr[i3] = colGetEntry(i3, i2);
        }
        return objArr;
    }

    public <T> T[] toArrayHelper(T[] tArr, int i) {
        T[] tArr2 = tArr;
        int i2 = i;
        int colGetSize = colGetSize();
        if (tArr2.length < colGetSize) {
            tArr2 = (Object[]) Array.newInstance(tArr2.getClass().getComponentType(), colGetSize);
        }
        for (int i3 = 0; i3 < colGetSize; i3++) {
            tArr2[i3] = colGetEntry(i3, i2);
        }
        if (tArr2.length > colGetSize) {
            tArr2[colGetSize] = null;
        }
        return tArr2;
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> boolean equalsSetHelper(java.util.Set<T> r6, java.lang.Object r7) {
        /*
            r0 = r6
            r1 = r7
            r4 = r0
            r5 = r1
            if (r4 != r5) goto L_0x0009
            r4 = 1
            r0 = r4
        L_0x0008:
            return r0
        L_0x0009:
            r4 = r1
            boolean r4 = r4 instanceof java.util.Set
            if (r4 == 0) goto L_0x0035
            r4 = r1
            java.util.Set r4 = (java.util.Set) r4
            r2 = r4
            r4 = r0
            int r4 = r4.size()     // Catch:{ NullPointerException -> 0x002b, ClassCastException -> 0x0030 }
            r5 = r2
            int r5 = r5.size()     // Catch:{ NullPointerException -> 0x002b, ClassCastException -> 0x0030 }
            if (r4 != r5) goto L_0x0029
            r4 = r0
            r5 = r2
            boolean r4 = r4.containsAll(r5)     // Catch:{ NullPointerException -> 0x002b, ClassCastException -> 0x0030 }
            if (r4 == 0) goto L_0x0029
            r4 = 1
        L_0x0027:
            r0 = r4
            goto L_0x0008
        L_0x0029:
            r4 = 0
            goto L_0x0027
        L_0x002b:
            r4 = move-exception
            r3 = r4
            r4 = 0
            r0 = r4
            goto L_0x0008
        L_0x0030:
            r4 = move-exception
            r3 = r4
            r4 = 0
            r0 = r4
            goto L_0x0008
        L_0x0035:
            r4 = 0
            r0 = r4
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.util.MapCollections.equalsSetHelper(java.util.Set, java.lang.Object):boolean");
    }

    public Set<Map.Entry<K, V>> getEntrySet() {
        MapCollections<K, V>.EntrySet entrySet;
        if (this.mEntrySet == null) {
            new EntrySet();
            this.mEntrySet = entrySet;
        }
        return this.mEntrySet;
    }

    public Set<K> getKeySet() {
        MapCollections<K, V>.KeySet keySet;
        if (this.mKeySet == null) {
            new KeySet();
            this.mKeySet = keySet;
        }
        return this.mKeySet;
    }

    public Collection<V> getValues() {
        MapCollections<K, V>.ValuesCollection valuesCollection;
        if (this.mValues == null) {
            new ValuesCollection();
            this.mValues = valuesCollection;
        }
        return this.mValues;
    }
}
