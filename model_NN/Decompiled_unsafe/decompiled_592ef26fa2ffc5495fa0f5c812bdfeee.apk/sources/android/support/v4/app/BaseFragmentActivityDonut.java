package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

abstract class BaseFragmentActivityDonut extends Activity {
    /* access modifiers changed from: package-private */
    public abstract View dispatchFragmentsOnCreateView(View view, String str, Context context, AttributeSet attributeSet);

    BaseFragmentActivityDonut() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Bundle bundle2 = bundle;
        if (Build.VERSION.SDK_INT < 11 && getLayoutInflater().getFactory() == null) {
            getLayoutInflater().setFactory(this);
        }
        super.onCreate(bundle2);
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        String str2 = str;
        Context context2 = context;
        AttributeSet attributeSet2 = attributeSet;
        View dispatchFragmentsOnCreateView = dispatchFragmentsOnCreateView(null, str2, context2, attributeSet2);
        if (dispatchFragmentsOnCreateView == null) {
            return super.onCreateView(str2, context2, attributeSet2);
        }
        return dispatchFragmentsOnCreateView;
    }
}
