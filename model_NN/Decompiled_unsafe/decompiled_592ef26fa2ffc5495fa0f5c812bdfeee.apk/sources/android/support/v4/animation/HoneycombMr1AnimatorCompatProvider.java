package android.support.v4.animation;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewPropertyAnimator;

class HoneycombMr1AnimatorCompatProvider implements AnimatorProvider {
    private TimeInterpolator mDefaultInterpolator;

    HoneycombMr1AnimatorCompatProvider() {
    }

    public ValueAnimatorCompat emptyValueAnimator() {
        ValueAnimatorCompat valueAnimatorCompat;
        new HoneycombValueAnimatorCompat(ValueAnimator.ofFloat(0.0f, 1.0f));
        return valueAnimatorCompat;
    }

    static class HoneycombValueAnimatorCompat implements ValueAnimatorCompat {
        final Animator mWrapped;

        public HoneycombValueAnimatorCompat(Animator animator) {
            this.mWrapped = animator;
        }

        public void setTarget(View view) {
            this.mWrapped.setTarget(view);
        }

        public void addListener(AnimatorListenerCompat animatorListenerCompat) {
            Animator.AnimatorListener animatorListener;
            new AnimatorListenerCompatWrapper(animatorListenerCompat, this);
            this.mWrapped.addListener(animatorListener);
        }

        public void setDuration(long j) {
            Animator duration = this.mWrapped.setDuration(j);
        }

        public void start() {
            this.mWrapped.start();
        }

        public void cancel() {
            this.mWrapped.cancel();
        }

        public void addUpdateListener(AnimatorUpdateListenerCompat animatorUpdateListenerCompat) {
            ValueAnimator.AnimatorUpdateListener animatorUpdateListener;
            AnimatorUpdateListenerCompat animatorUpdateListenerCompat2 = animatorUpdateListenerCompat;
            if (this.mWrapped instanceof ValueAnimator) {
                final AnimatorUpdateListenerCompat animatorUpdateListenerCompat3 = animatorUpdateListenerCompat2;
                new ValueAnimator.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        animatorUpdateListenerCompat3.onAnimationUpdate(HoneycombValueAnimatorCompat.this);
                    }
                };
                ((ValueAnimator) this.mWrapped).addUpdateListener(animatorUpdateListener);
            }
        }

        public float getAnimatedFraction() {
            return ((ValueAnimator) this.mWrapped).getAnimatedFraction();
        }
    }

    static class AnimatorListenerCompatWrapper implements Animator.AnimatorListener {
        final ValueAnimatorCompat mValueAnimatorCompat;
        final AnimatorListenerCompat mWrapped;

        public AnimatorListenerCompatWrapper(AnimatorListenerCompat animatorListenerCompat, ValueAnimatorCompat valueAnimatorCompat) {
            this.mWrapped = animatorListenerCompat;
            this.mValueAnimatorCompat = valueAnimatorCompat;
        }

        public void onAnimationStart(Animator animator) {
            this.mWrapped.onAnimationStart(this.mValueAnimatorCompat);
        }

        public void onAnimationEnd(Animator animator) {
            this.mWrapped.onAnimationEnd(this.mValueAnimatorCompat);
        }

        public void onAnimationCancel(Animator animator) {
            this.mWrapped.onAnimationCancel(this.mValueAnimatorCompat);
        }

        public void onAnimationRepeat(Animator animator) {
            this.mWrapped.onAnimationRepeat(this.mValueAnimatorCompat);
        }
    }

    public void clearInterpolator(View view) {
        ValueAnimator valueAnimator;
        View view2 = view;
        if (this.mDefaultInterpolator == null) {
            new ValueAnimator();
            this.mDefaultInterpolator = valueAnimator.getInterpolator();
        }
        ViewPropertyAnimator interpolator = view2.animate().setInterpolator(this.mDefaultInterpolator);
    }
}
