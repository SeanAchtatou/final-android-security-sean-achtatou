package android.support.v4.content;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Process;
import android.support.annotation.NonNull;
import android.util.Log;
import java.io.File;

public class ContextCompat {
    private static final String DIR_ANDROID = "Android";
    private static final String DIR_CACHE = "cache";
    private static final String DIR_DATA = "data";
    private static final String DIR_FILES = "files";
    private static final String DIR_OBB = "obb";
    private static final String TAG = "ContextCompat";

    public static boolean startActivities(Context context, Intent[] intentArr) {
        return startActivities(context, intentArr, null);
    }

    public static boolean startActivities(Context context, Intent[] intentArr, Bundle bundle) {
        Context context2 = context;
        Intent[] intentArr2 = intentArr;
        Bundle bundle2 = bundle;
        int i = Build.VERSION.SDK_INT;
        if (i >= 16) {
            ContextCompatJellybean.startActivities(context2, intentArr2, bundle2);
            return true;
        } else if (i < 11) {
            return false;
        } else {
            ContextCompatHoneycomb.startActivities(context2, intentArr2);
            return true;
        }
    }

    public static File[] getObbDirs(Context context) {
        File buildPath;
        Context context2 = context;
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return ContextCompatKitKat.getObbDirs(context2);
        }
        if (i >= 11) {
            buildPath = ContextCompatHoneycomb.getObbDir(context2);
        } else {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            String[] strArr = new String[3];
            strArr[0] = DIR_ANDROID;
            String[] strArr2 = strArr;
            strArr2[1] = DIR_OBB;
            String[] strArr3 = strArr2;
            strArr3[2] = context2.getPackageName();
            buildPath = buildPath(externalStorageDirectory, strArr3);
        }
        return new File[]{buildPath};
    }

    public static File[] getExternalFilesDirs(Context context, String str) {
        File buildPath;
        Context context2 = context;
        String str2 = str;
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return ContextCompatKitKat.getExternalFilesDirs(context2, str2);
        }
        if (i >= 8) {
            buildPath = ContextCompatFroyo.getExternalFilesDir(context2, str2);
        } else {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            String[] strArr = new String[5];
            strArr[0] = DIR_ANDROID;
            String[] strArr2 = strArr;
            strArr2[1] = DIR_DATA;
            String[] strArr3 = strArr2;
            strArr3[2] = context2.getPackageName();
            String[] strArr4 = strArr3;
            strArr4[3] = DIR_FILES;
            String[] strArr5 = strArr4;
            strArr5[4] = str2;
            buildPath = buildPath(externalStorageDirectory, strArr5);
        }
        return new File[]{buildPath};
    }

    public static File[] getExternalCacheDirs(Context context) {
        File buildPath;
        Context context2 = context;
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            return ContextCompatKitKat.getExternalCacheDirs(context2);
        }
        if (i >= 8) {
            buildPath = ContextCompatFroyo.getExternalCacheDir(context2);
        } else {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            String[] strArr = new String[4];
            strArr[0] = DIR_ANDROID;
            String[] strArr2 = strArr;
            strArr2[1] = DIR_DATA;
            String[] strArr3 = strArr2;
            strArr3[2] = context2.getPackageName();
            String[] strArr4 = strArr3;
            strArr4[3] = DIR_CACHE;
            buildPath = buildPath(externalStorageDirectory, strArr4);
        }
        return new File[]{buildPath};
    }

    private static File buildPath(File file, String... strArr) {
        File file2;
        File file3;
        File file4 = file;
        String[] strArr2 = strArr;
        int length = strArr2.length;
        for (int i = 0; i < length; i++) {
            String str = strArr2[i];
            if (file4 == null) {
                new File(str);
                file4 = file3;
            } else if (str != null) {
                new File(file4, str);
                file4 = file2;
            }
        }
        return file4;
    }

    public static final Drawable getDrawable(Context context, int i) {
        Context context2 = context;
        int i2 = i;
        if (Build.VERSION.SDK_INT >= 21) {
            return ContextCompatApi21.getDrawable(context2, i2);
        }
        return context2.getResources().getDrawable(i2);
    }

    public static final ColorStateList getColorStateList(Context context, int i) {
        Context context2 = context;
        int i2 = i;
        if (Build.VERSION.SDK_INT >= 23) {
            return ContextCompatApi23.getColorStateList(context2, i2);
        }
        return context2.getResources().getColorStateList(i2);
    }

    public static final int getColor(Context context, int i) {
        Context context2 = context;
        int i2 = i;
        if (Build.VERSION.SDK_INT >= 23) {
            return ContextCompatApi23.getColor(context2, i2);
        }
        return context2.getResources().getColor(i2);
    }

    public static int checkSelfPermission(@NonNull Context context, @NonNull String str) {
        Throwable th;
        Context context2 = context;
        String str2 = str;
        if (str2 != null) {
            return context2.checkPermission(str2, Process.myPid(), Process.myUid());
        }
        Throwable th2 = th;
        new IllegalArgumentException("permission is null");
        throw th2;
    }

    public final File getNoBackupFilesDir(Context context) {
        File file;
        Context context2 = context;
        if (Build.VERSION.SDK_INT >= 21) {
            return ContextCompatApi21.getNoBackupFilesDir(context2);
        }
        new File(context2.getApplicationInfo().dataDir, "no_backup");
        return createFilesDir(file);
    }

    public final File getCodeCacheDir(Context context) {
        File file;
        Context context2 = context;
        if (Build.VERSION.SDK_INT >= 21) {
            return ContextCompatApi21.getCodeCacheDir(context2);
        }
        new File(context2.getApplicationInfo().dataDir, "code_cache");
        return createFilesDir(file);
    }

    private static synchronized File createFilesDir(File file) {
        File file2;
        StringBuilder sb;
        File file3 = file;
        synchronized (ContextCompat.class) {
            if (file3.exists() || file3.mkdirs()) {
                file2 = file3;
            } else if (file3.exists()) {
                file2 = file3;
            } else {
                new StringBuilder();
                int w = Log.w(TAG, sb.append("Unable to create files subdir ").append(file3.getPath()).toString());
                file2 = null;
            }
        }
        return file2;
    }
}
