package android.support.v4.hardware.fingerprint;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.hardware.fingerprint.FingerprintManagerCompatApi23;
import android.support.v4.os.CancellationSignal;
import java.security.Signature;
import javax.crypto.Cipher;
import javax.crypto.Mac;

public class FingerprintManagerCompat {
    static final FingerprintManagerCompatImpl IMPL;
    private Context mContext;

    private interface FingerprintManagerCompatImpl {
        void authenticate(Context context, CryptoObject cryptoObject, int i, CancellationSignal cancellationSignal, AuthenticationCallback authenticationCallback, Handler handler);

        boolean hasEnrolledFingerprints(Context context);

        boolean isHardwareDetected(Context context);
    }

    public static FingerprintManagerCompat from(Context context) {
        FingerprintManagerCompat fingerprintManagerCompat;
        new FingerprintManagerCompat(context);
        return fingerprintManagerCompat;
    }

    private FingerprintManagerCompat(Context context) {
        this.mContext = context;
    }

    static {
        FingerprintManagerCompatImpl fingerprintManagerCompatImpl;
        FingerprintManagerCompatImpl fingerprintManagerCompatImpl2;
        if (Build.VERSION.SDK_INT >= 23) {
            new Api23FingerprintManagerCompatImpl();
            IMPL = fingerprintManagerCompatImpl2;
            return;
        }
        new LegacyFingerprintManagerCompatImpl();
        IMPL = fingerprintManagerCompatImpl;
    }

    public boolean hasEnrolledFingerprints() {
        return IMPL.hasEnrolledFingerprints(this.mContext);
    }

    public boolean isHardwareDetected() {
        return IMPL.isHardwareDetected(this.mContext);
    }

    public void authenticate(@Nullable CryptoObject cryptoObject, int i, @Nullable CancellationSignal cancellationSignal, @NonNull AuthenticationCallback authenticationCallback, @Nullable Handler handler) {
        IMPL.authenticate(this.mContext, cryptoObject, i, cancellationSignal, authenticationCallback, handler);
    }

    public static class CryptoObject {
        private final Cipher mCipher;
        private final Mac mMac;
        private final Signature mSignature;

        public CryptoObject(Signature signature) {
            this.mSignature = signature;
            this.mCipher = null;
            this.mMac = null;
        }

        public CryptoObject(Cipher cipher) {
            this.mCipher = cipher;
            this.mSignature = null;
            this.mMac = null;
        }

        public CryptoObject(Mac mac) {
            this.mMac = mac;
            this.mCipher = null;
            this.mSignature = null;
        }

        public Signature getSignature() {
            return this.mSignature;
        }

        public Cipher getCipher() {
            return this.mCipher;
        }

        public Mac getMac() {
            return this.mMac;
        }
    }

    public static final class AuthenticationResult {
        private CryptoObject mCryptoObject;

        public AuthenticationResult(CryptoObject cryptoObject) {
            this.mCryptoObject = cryptoObject;
        }

        public CryptoObject getCryptoObject() {
            return this.mCryptoObject;
        }
    }

    public static abstract class AuthenticationCallback {
        public void onAuthenticationError(int i, CharSequence charSequence) {
        }

        public void onAuthenticationHelp(int i, CharSequence charSequence) {
        }

        public void onAuthenticationSucceeded(AuthenticationResult authenticationResult) {
        }

        public void onAuthenticationFailed() {
        }
    }

    private static class LegacyFingerprintManagerCompatImpl implements FingerprintManagerCompatImpl {
        public boolean hasEnrolledFingerprints(Context context) {
            return false;
        }

        public boolean isHardwareDetected(Context context) {
            return false;
        }

        public void authenticate(Context context, CryptoObject cryptoObject, int i, CancellationSignal cancellationSignal, AuthenticationCallback authenticationCallback, Handler handler) {
        }
    }

    private static class Api23FingerprintManagerCompatImpl implements FingerprintManagerCompatImpl {
        public boolean hasEnrolledFingerprints(Context context) {
            return FingerprintManagerCompatApi23.hasEnrolledFingerprints(context);
        }

        public boolean isHardwareDetected(Context context) {
            return FingerprintManagerCompatApi23.isHardwareDetected(context);
        }

        public void authenticate(Context context, CryptoObject cryptoObject, int i, CancellationSignal cancellationSignal, AuthenticationCallback authenticationCallback, Handler handler) {
            CancellationSignal cancellationSignal2 = cancellationSignal;
            FingerprintManagerCompatApi23.authenticate(context, wrapCryptoObject(cryptoObject), i, cancellationSignal2 != null ? cancellationSignal2.getCancellationSignalObject() : null, wrapCallback(authenticationCallback), handler);
        }

        private static FingerprintManagerCompatApi23.CryptoObject wrapCryptoObject(CryptoObject cryptoObject) {
            FingerprintManagerCompatApi23.CryptoObject cryptoObject2;
            FingerprintManagerCompatApi23.CryptoObject cryptoObject3;
            FingerprintManagerCompatApi23.CryptoObject cryptoObject4;
            CryptoObject cryptoObject5 = cryptoObject;
            if (cryptoObject5 == null) {
                return null;
            }
            if (cryptoObject5.getCipher() != null) {
                new FingerprintManagerCompatApi23.CryptoObject(cryptoObject5.getCipher());
                return cryptoObject4;
            } else if (cryptoObject5.getSignature() != null) {
                new FingerprintManagerCompatApi23.CryptoObject(cryptoObject5.getSignature());
                return cryptoObject3;
            } else if (cryptoObject5.getMac() == null) {
                return null;
            } else {
                new FingerprintManagerCompatApi23.CryptoObject(cryptoObject5.getMac());
                return cryptoObject2;
            }
        }

        /* access modifiers changed from: private */
        public static CryptoObject unwrapCryptoObject(FingerprintManagerCompatApi23.CryptoObject cryptoObject) {
            CryptoObject cryptoObject2;
            CryptoObject cryptoObject3;
            CryptoObject cryptoObject4;
            FingerprintManagerCompatApi23.CryptoObject cryptoObject5 = cryptoObject;
            if (cryptoObject5 == null) {
                return null;
            }
            if (cryptoObject5.getCipher() != null) {
                new CryptoObject(cryptoObject5.getCipher());
                return cryptoObject4;
            } else if (cryptoObject5.getSignature() != null) {
                new CryptoObject(cryptoObject5.getSignature());
                return cryptoObject3;
            } else if (cryptoObject5.getMac() == null) {
                return null;
            } else {
                new CryptoObject(cryptoObject5.getMac());
                return cryptoObject2;
            }
        }

        private static FingerprintManagerCompatApi23.AuthenticationCallback wrapCallback(AuthenticationCallback authenticationCallback) {
            FingerprintManagerCompatApi23.AuthenticationCallback authenticationCallback2;
            final AuthenticationCallback authenticationCallback3 = authenticationCallback;
            new FingerprintManagerCompatApi23.AuthenticationCallback() {
                public void onAuthenticationError(int i, CharSequence charSequence) {
                    authenticationCallback3.onAuthenticationError(i, charSequence);
                }

                public void onAuthenticationHelp(int i, CharSequence charSequence) {
                    authenticationCallback3.onAuthenticationHelp(i, charSequence);
                }

                public void onAuthenticationSucceeded(FingerprintManagerCompatApi23.AuthenticationResultInternal authenticationResultInternal) {
                    AuthenticationResult authenticationResult;
                    new AuthenticationResult(Api23FingerprintManagerCompatImpl.unwrapCryptoObject(authenticationResultInternal.getCryptoObject()));
                    authenticationCallback3.onAuthenticationSucceeded(authenticationResult);
                }

                public void onAuthenticationFailed() {
                    authenticationCallback3.onAuthenticationFailed();
                }
            };
            return authenticationCallback2;
        }
    }
}
