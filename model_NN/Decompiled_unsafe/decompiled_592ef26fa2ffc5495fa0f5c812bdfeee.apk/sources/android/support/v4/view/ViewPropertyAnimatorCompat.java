package android.support.v4.view;

import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;
import java.util.WeakHashMap;

public class ViewPropertyAnimatorCompat {
    static final ViewPropertyAnimatorCompatImpl IMPL;
    static final int LISTENER_TAG_ID = 2113929216;
    private static final String TAG = "ViewAnimatorCompat";
    /* access modifiers changed from: private */
    public Runnable mEndAction = null;
    /* access modifiers changed from: private */
    public int mOldLayerType = -1;
    /* access modifiers changed from: private */
    public Runnable mStartAction = null;
    private WeakReference<View> mView;

    interface ViewPropertyAnimatorCompatImpl {
        void alpha(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void alphaBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void cancel(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view);

        long getDuration(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view);

        Interpolator getInterpolator(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view);

        long getStartDelay(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view);

        void rotation(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void rotationBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void rotationX(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void rotationXBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void rotationY(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void rotationYBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void scaleX(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void scaleXBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void scaleY(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void scaleYBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void setDuration(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, long j);

        void setInterpolator(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, Interpolator interpolator);

        void setListener(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, ViewPropertyAnimatorListener viewPropertyAnimatorListener);

        void setStartDelay(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, long j);

        void setUpdateListener(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener);

        void start(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view);

        void translationX(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void translationXBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void translationY(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void translationYBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void translationZ(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void translationZBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void withEndAction(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, Runnable runnable);

        void withLayer(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view);

        void withStartAction(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, Runnable runnable);

        void x(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void xBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void y(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void yBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void z(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);

        void zBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f);
    }

    static /* synthetic */ Runnable access$002(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, Runnable runnable) {
        Runnable runnable2 = runnable;
        Runnable runnable3 = runnable2;
        viewPropertyAnimatorCompat.mEndAction = runnable3;
        return runnable2;
    }

    static /* synthetic */ Runnable access$102(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, Runnable runnable) {
        Runnable runnable2 = runnable;
        Runnable runnable3 = runnable2;
        viewPropertyAnimatorCompat.mStartAction = runnable3;
        return runnable2;
    }

    static /* synthetic */ int access$402(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, int i) {
        int i2 = i;
        int i3 = i2;
        viewPropertyAnimatorCompat.mOldLayerType = i3;
        return i2;
    }

    ViewPropertyAnimatorCompat(View view) {
        WeakReference<View> weakReference;
        new WeakReference<>(view);
        this.mView = weakReference;
    }

    static class BaseViewPropertyAnimatorCompatImpl implements ViewPropertyAnimatorCompatImpl {
        WeakHashMap<View, Runnable> mStarterMap = null;

        BaseViewPropertyAnimatorCompatImpl() {
        }

        public void setDuration(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, long j) {
        }

        public void alpha(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void translationX(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void translationY(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void withEndAction(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, Runnable runnable) {
            ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2 = viewPropertyAnimatorCompat;
            Runnable access$002 = ViewPropertyAnimatorCompat.access$002(viewPropertyAnimatorCompat2, runnable);
            postStartMessage(viewPropertyAnimatorCompat2, view);
        }

        public long getDuration(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            return 0;
        }

        public void setInterpolator(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, Interpolator interpolator) {
        }

        public Interpolator getInterpolator(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            return null;
        }

        public void setStartDelay(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, long j) {
        }

        public long getStartDelay(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            return 0;
        }

        public void alphaBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void rotation(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void rotationBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void rotationX(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void rotationXBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void rotationY(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void rotationYBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void scaleX(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void scaleXBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void scaleY(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void scaleYBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void cancel(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void x(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void xBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void y(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void yBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void z(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
        }

        public void zBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
        }

        public void translationXBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void translationYBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            postStartMessage(viewPropertyAnimatorCompat, view);
        }

        public void translationZ(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
        }

        public void translationZBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
        }

        public void start(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            View view2 = view;
            removeStartMessage(view2);
            startAnimation(viewPropertyAnimatorCompat, view2);
        }

        public void withLayer(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
        }

        public void withStartAction(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, Runnable runnable) {
            ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2 = viewPropertyAnimatorCompat;
            Runnable access$102 = ViewPropertyAnimatorCompat.access$102(viewPropertyAnimatorCompat2, runnable);
            postStartMessage(viewPropertyAnimatorCompat2, view);
        }

        public void setListener(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
            view.setTag(ViewPropertyAnimatorCompat.LISTENER_TAG_ID, viewPropertyAnimatorListener);
        }

        public void setUpdateListener(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener) {
        }

        /* access modifiers changed from: private */
        public void startAnimation(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2 = viewPropertyAnimatorCompat;
            View view2 = view;
            Object tag = view2.getTag(ViewPropertyAnimatorCompat.LISTENER_TAG_ID);
            ViewPropertyAnimatorListener viewPropertyAnimatorListener = null;
            if (tag instanceof ViewPropertyAnimatorListener) {
                viewPropertyAnimatorListener = (ViewPropertyAnimatorListener) tag;
            }
            Runnable access$100 = viewPropertyAnimatorCompat2.mStartAction;
            Runnable access$000 = viewPropertyAnimatorCompat2.mEndAction;
            if (access$100 != null) {
                access$100.run();
            }
            if (viewPropertyAnimatorListener != null) {
                viewPropertyAnimatorListener.onAnimationStart(view2);
                viewPropertyAnimatorListener.onAnimationEnd(view2);
            }
            if (access$000 != null) {
                access$000.run();
            }
            if (this.mStarterMap != null) {
                Runnable remove = this.mStarterMap.remove(view2);
            }
        }

        class Starter implements Runnable {
            WeakReference<View> mViewRef;
            ViewPropertyAnimatorCompat mVpa;

            private Starter(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
                WeakReference<View> weakReference;
                new WeakReference<>(view);
                this.mViewRef = weakReference;
                this.mVpa = viewPropertyAnimatorCompat;
            }

            public void run() {
                View view = this.mViewRef.get();
                if (view != null) {
                    BaseViewPropertyAnimatorCompatImpl.this.startAnimation(this.mVpa, view);
                }
            }
        }

        private void removeStartMessage(View view) {
            Runnable runnable;
            View view2 = view;
            if (this.mStarterMap != null && (runnable = this.mStarterMap.get(view2)) != null) {
                boolean removeCallbacks = view2.removeCallbacks(runnable);
            }
        }

        private void postStartMessage(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            Runnable runnable;
            WeakHashMap<View, Runnable> weakHashMap;
            ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2 = viewPropertyAnimatorCompat;
            View view2 = view;
            Runnable runnable2 = null;
            if (this.mStarterMap != null) {
                runnable2 = this.mStarterMap.get(view2);
            }
            if (runnable2 == null) {
                new Starter(viewPropertyAnimatorCompat2, view2);
                runnable2 = runnable;
                if (this.mStarterMap == null) {
                    new WeakHashMap<>();
                    this.mStarterMap = weakHashMap;
                }
                Runnable put = this.mStarterMap.put(view2, runnable2);
            }
            boolean removeCallbacks = view2.removeCallbacks(runnable2);
            boolean post = view2.post(runnable2);
        }
    }

    static class ICSViewPropertyAnimatorCompatImpl extends BaseViewPropertyAnimatorCompatImpl {
        WeakHashMap<View, Integer> mLayerMap = null;

        ICSViewPropertyAnimatorCompatImpl() {
        }

        public void setDuration(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, long j) {
            ViewPropertyAnimatorCompatICS.setDuration(view, j);
        }

        public void alpha(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.alpha(view, f);
        }

        public void translationX(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.translationX(view, f);
        }

        public void translationY(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.translationY(view, f);
        }

        public long getDuration(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            return ViewPropertyAnimatorCompatICS.getDuration(view);
        }

        public void setInterpolator(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, Interpolator interpolator) {
            ViewPropertyAnimatorCompatICS.setInterpolator(view, interpolator);
        }

        public void setStartDelay(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, long j) {
            ViewPropertyAnimatorCompatICS.setStartDelay(view, j);
        }

        public long getStartDelay(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            return ViewPropertyAnimatorCompatICS.getStartDelay(view);
        }

        public void alphaBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.alphaBy(view, f);
        }

        public void rotation(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.rotation(view, f);
        }

        public void rotationBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.rotationBy(view, f);
        }

        public void rotationX(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.rotationX(view, f);
        }

        public void rotationXBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.rotationXBy(view, f);
        }

        public void rotationY(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.rotationY(view, f);
        }

        public void rotationYBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.rotationYBy(view, f);
        }

        public void scaleX(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.scaleX(view, f);
        }

        public void scaleXBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.scaleXBy(view, f);
        }

        public void scaleY(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.scaleY(view, f);
        }

        public void scaleYBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.scaleYBy(view, f);
        }

        public void cancel(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            ViewPropertyAnimatorCompatICS.cancel(view);
        }

        public void x(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.x(view, f);
        }

        public void xBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.xBy(view, f);
        }

        public void y(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.y(view, f);
        }

        public void yBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.yBy(view, f);
        }

        public void translationXBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.translationXBy(view, f);
        }

        public void translationYBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatICS.translationYBy(view, f);
        }

        public void start(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            ViewPropertyAnimatorCompatICS.start(view);
        }

        public void setListener(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
            ViewPropertyAnimatorListener viewPropertyAnimatorListener2;
            View view2 = view;
            view2.setTag(ViewPropertyAnimatorCompat.LISTENER_TAG_ID, viewPropertyAnimatorListener);
            new MyVpaListener(viewPropertyAnimatorCompat);
            ViewPropertyAnimatorCompatICS.setListener(view2, viewPropertyAnimatorListener2);
        }

        public void withEndAction(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, Runnable runnable) {
            ViewPropertyAnimatorListener viewPropertyAnimatorListener;
            ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2 = viewPropertyAnimatorCompat;
            new MyVpaListener(viewPropertyAnimatorCompat2);
            ViewPropertyAnimatorCompatICS.setListener(view, viewPropertyAnimatorListener);
            Runnable access$002 = ViewPropertyAnimatorCompat.access$002(viewPropertyAnimatorCompat2, runnable);
        }

        public void withStartAction(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, Runnable runnable) {
            ViewPropertyAnimatorListener viewPropertyAnimatorListener;
            ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2 = viewPropertyAnimatorCompat;
            new MyVpaListener(viewPropertyAnimatorCompat2);
            ViewPropertyAnimatorCompatICS.setListener(view, viewPropertyAnimatorListener);
            Runnable access$102 = ViewPropertyAnimatorCompat.access$102(viewPropertyAnimatorCompat2, runnable);
        }

        public void withLayer(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            ViewPropertyAnimatorListener viewPropertyAnimatorListener;
            ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2 = viewPropertyAnimatorCompat;
            View view2 = view;
            int access$402 = ViewPropertyAnimatorCompat.access$402(viewPropertyAnimatorCompat2, ViewCompat.getLayerType(view2));
            new MyVpaListener(viewPropertyAnimatorCompat2);
            ViewPropertyAnimatorCompatICS.setListener(view2, viewPropertyAnimatorListener);
        }

        static class MyVpaListener implements ViewPropertyAnimatorListener {
            ViewPropertyAnimatorCompat mVpa;

            MyVpaListener(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat) {
                this.mVpa = viewPropertyAnimatorCompat;
            }

            public void onAnimationStart(View view) {
                View view2 = view;
                if (this.mVpa.mOldLayerType >= 0) {
                    ViewCompat.setLayerType(view2, 2, null);
                }
                if (this.mVpa.mStartAction != null) {
                    this.mVpa.mStartAction.run();
                }
                Object tag = view2.getTag(ViewPropertyAnimatorCompat.LISTENER_TAG_ID);
                ViewPropertyAnimatorListener viewPropertyAnimatorListener = null;
                if (tag instanceof ViewPropertyAnimatorListener) {
                    viewPropertyAnimatorListener = (ViewPropertyAnimatorListener) tag;
                }
                if (viewPropertyAnimatorListener != null) {
                    viewPropertyAnimatorListener.onAnimationStart(view2);
                }
            }

            public void onAnimationEnd(View view) {
                View view2 = view;
                if (this.mVpa.mOldLayerType >= 0) {
                    ViewCompat.setLayerType(view2, this.mVpa.mOldLayerType, null);
                    int access$402 = ViewPropertyAnimatorCompat.access$402(this.mVpa, -1);
                }
                if (this.mVpa.mEndAction != null) {
                    this.mVpa.mEndAction.run();
                }
                Object tag = view2.getTag(ViewPropertyAnimatorCompat.LISTENER_TAG_ID);
                ViewPropertyAnimatorListener viewPropertyAnimatorListener = null;
                if (tag instanceof ViewPropertyAnimatorListener) {
                    viewPropertyAnimatorListener = (ViewPropertyAnimatorListener) tag;
                }
                if (viewPropertyAnimatorListener != null) {
                    viewPropertyAnimatorListener.onAnimationEnd(view2);
                }
            }

            public void onAnimationCancel(View view) {
                View view2 = view;
                Object tag = view2.getTag(ViewPropertyAnimatorCompat.LISTENER_TAG_ID);
                ViewPropertyAnimatorListener viewPropertyAnimatorListener = null;
                if (tag instanceof ViewPropertyAnimatorListener) {
                    viewPropertyAnimatorListener = (ViewPropertyAnimatorListener) tag;
                }
                if (viewPropertyAnimatorListener != null) {
                    viewPropertyAnimatorListener.onAnimationCancel(view2);
                }
            }
        }
    }

    static class JBViewPropertyAnimatorCompatImpl extends ICSViewPropertyAnimatorCompatImpl {
        JBViewPropertyAnimatorCompatImpl() {
        }

        public void setListener(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
            ViewPropertyAnimatorCompatJB.setListener(view, viewPropertyAnimatorListener);
        }

        public void withStartAction(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, Runnable runnable) {
            ViewPropertyAnimatorCompatJB.withStartAction(view, runnable);
        }

        public void withEndAction(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, Runnable runnable) {
            ViewPropertyAnimatorCompatJB.withEndAction(view, runnable);
        }

        public void withLayer(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            ViewPropertyAnimatorCompatJB.withLayer(view);
        }
    }

    static class JBMr2ViewPropertyAnimatorCompatImpl extends JBViewPropertyAnimatorCompatImpl {
        JBMr2ViewPropertyAnimatorCompatImpl() {
        }

        public Interpolator getInterpolator(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view) {
            return ViewPropertyAnimatorCompatJellybeanMr2.getInterpolator(view);
        }
    }

    static class KitKatViewPropertyAnimatorCompatImpl extends JBMr2ViewPropertyAnimatorCompatImpl {
        KitKatViewPropertyAnimatorCompatImpl() {
        }

        public void setUpdateListener(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener) {
            ViewPropertyAnimatorCompatKK.setUpdateListener(view, viewPropertyAnimatorUpdateListener);
        }
    }

    static class LollipopViewPropertyAnimatorCompatImpl extends KitKatViewPropertyAnimatorCompatImpl {
        LollipopViewPropertyAnimatorCompatImpl() {
        }

        public void translationZ(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatLollipop.translationZ(view, f);
        }

        public void translationZBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatLollipop.translationZBy(view, f);
        }

        public void z(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatLollipop.z(view, f);
        }

        public void zBy(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, View view, float f) {
            ViewPropertyAnimatorCompatLollipop.zBy(view, f);
        }
    }

    static {
        ViewPropertyAnimatorCompatImpl viewPropertyAnimatorCompatImpl;
        ViewPropertyAnimatorCompatImpl viewPropertyAnimatorCompatImpl2;
        ViewPropertyAnimatorCompatImpl viewPropertyAnimatorCompatImpl3;
        ViewPropertyAnimatorCompatImpl viewPropertyAnimatorCompatImpl4;
        ViewPropertyAnimatorCompatImpl viewPropertyAnimatorCompatImpl5;
        ViewPropertyAnimatorCompatImpl viewPropertyAnimatorCompatImpl6;
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            new LollipopViewPropertyAnimatorCompatImpl();
            IMPL = viewPropertyAnimatorCompatImpl6;
        } else if (i >= 19) {
            new KitKatViewPropertyAnimatorCompatImpl();
            IMPL = viewPropertyAnimatorCompatImpl5;
        } else if (i >= 18) {
            new JBMr2ViewPropertyAnimatorCompatImpl();
            IMPL = viewPropertyAnimatorCompatImpl4;
        } else if (i >= 16) {
            new JBViewPropertyAnimatorCompatImpl();
            IMPL = viewPropertyAnimatorCompatImpl3;
        } else if (i >= 14) {
            new ICSViewPropertyAnimatorCompatImpl();
            IMPL = viewPropertyAnimatorCompatImpl2;
        } else {
            new BaseViewPropertyAnimatorCompatImpl();
            IMPL = viewPropertyAnimatorCompatImpl;
        }
    }

    public ViewPropertyAnimatorCompat setDuration(long j) {
        long j2 = j;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.setDuration(this, view2, j2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat alpha(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.alpha(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat alphaBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.alphaBy(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat translationX(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.translationX(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat translationY(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.translationY(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat withEndAction(Runnable runnable) {
        Runnable runnable2 = runnable;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.withEndAction(this, view2, runnable2);
        }
        return this;
    }

    public long getDuration() {
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            return IMPL.getDuration(this, view2);
        }
        return 0;
    }

    public ViewPropertyAnimatorCompat setInterpolator(Interpolator interpolator) {
        Interpolator interpolator2 = interpolator;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.setInterpolator(this, view2, interpolator2);
        }
        return this;
    }

    public Interpolator getInterpolator() {
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            return IMPL.getInterpolator(this, view2);
        }
        return null;
    }

    public ViewPropertyAnimatorCompat setStartDelay(long j) {
        long j2 = j;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.setStartDelay(this, view2, j2);
        }
        return this;
    }

    public long getStartDelay() {
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            return IMPL.getStartDelay(this, view2);
        }
        return 0;
    }

    public ViewPropertyAnimatorCompat rotation(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.rotation(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat rotationBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.rotationBy(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat rotationX(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.rotationX(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat rotationXBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.rotationXBy(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat rotationY(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.rotationY(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat rotationYBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.rotationYBy(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat scaleX(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.scaleX(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat scaleXBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.scaleXBy(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat scaleY(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.scaleY(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat scaleYBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.scaleYBy(this, view2, f2);
        }
        return this;
    }

    public void cancel() {
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.cancel(this, view2);
        }
    }

    public ViewPropertyAnimatorCompat x(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.x(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat xBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.xBy(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat y(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.y(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat yBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.yBy(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat translationXBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.translationXBy(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat translationYBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.translationYBy(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat translationZBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.translationZBy(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat translationZ(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.translationZ(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat z(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.z(this, view2, f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat zBy(float f) {
        float f2 = f;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.zBy(this, view2, f2);
        }
        return this;
    }

    public void start() {
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.start(this, view2);
        }
    }

    public ViewPropertyAnimatorCompat withLayer() {
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.withLayer(this, view2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat withStartAction(Runnable runnable) {
        Runnable runnable2 = runnable;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.withStartAction(this, view2, runnable2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat setListener(ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
        ViewPropertyAnimatorListener viewPropertyAnimatorListener2 = viewPropertyAnimatorListener;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.setListener(this, view2, viewPropertyAnimatorListener2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat setUpdateListener(ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener) {
        ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener2 = viewPropertyAnimatorUpdateListener;
        View view = this.mView.get();
        View view2 = view;
        if (view != null) {
            IMPL.setUpdateListener(this, view2, viewPropertyAnimatorUpdateListener2);
        }
        return this;
    }
}
