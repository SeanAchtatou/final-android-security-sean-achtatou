package android.support.v4.view;

import android.view.View;
import android.view.ViewParent;

public class NestedScrollingChildHelper {
    private boolean mIsNestedScrollingEnabled;
    private ViewParent mNestedScrollingParent;
    private int[] mTempNestedScrollConsumed;
    private final View mView;

    public NestedScrollingChildHelper(View view) {
        this.mView = view;
    }

    public void setNestedScrollingEnabled(boolean z) {
        boolean z2 = z;
        if (this.mIsNestedScrollingEnabled) {
            ViewCompat.stopNestedScroll(this.mView);
        }
        this.mIsNestedScrollingEnabled = z2;
    }

    public boolean isNestedScrollingEnabled() {
        return this.mIsNestedScrollingEnabled;
    }

    public boolean hasNestedScrollingParent() {
        return this.mNestedScrollingParent != null;
    }

    public boolean startNestedScroll(int i) {
        int i2 = i;
        if (hasNestedScrollingParent()) {
            return true;
        }
        if (isNestedScrollingEnabled()) {
            View view = this.mView;
            for (ViewParent parent = this.mView.getParent(); parent != null; parent = parent.getParent()) {
                if (ViewParentCompat.onStartNestedScroll(parent, view, this.mView, i2)) {
                    this.mNestedScrollingParent = parent;
                    ViewParentCompat.onNestedScrollAccepted(parent, view, this.mView, i2);
                    return true;
                }
                if (parent instanceof View) {
                    view = (View) parent;
                }
            }
        }
        return false;
    }

    public void stopNestedScroll() {
        if (this.mNestedScrollingParent != null) {
            ViewParentCompat.onStopNestedScroll(this.mNestedScrollingParent, this.mView);
            this.mNestedScrollingParent = null;
        }
    }

    public boolean dispatchNestedScroll(int i, int i2, int i3, int i4, int[] iArr) {
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        int[] iArr2 = iArr;
        if (isNestedScrollingEnabled() && this.mNestedScrollingParent != null) {
            if (i5 != 0 || i6 != 0 || i7 != 0 || i8 != 0) {
                int i9 = 0;
                int i10 = 0;
                if (iArr2 != null) {
                    this.mView.getLocationInWindow(iArr2);
                    i9 = iArr2[0];
                    i10 = iArr2[1];
                }
                ViewParentCompat.onNestedScroll(this.mNestedScrollingParent, this.mView, i5, i6, i7, i8);
                if (iArr2 != null) {
                    this.mView.getLocationInWindow(iArr2);
                    int[] iArr3 = iArr2;
                    iArr3[0] = iArr3[0] - i9;
                    int[] iArr4 = iArr2;
                    iArr4[1] = iArr4[1] - i10;
                }
                return true;
            } else if (iArr2 != null) {
                iArr2[0] = 0;
                iArr2[1] = 0;
            }
        }
        return false;
    }

    public boolean dispatchNestedPreScroll(int i, int i2, int[] iArr, int[] iArr2) {
        int i3 = i;
        int i4 = i2;
        int[] iArr3 = iArr;
        int[] iArr4 = iArr2;
        if (isNestedScrollingEnabled() && this.mNestedScrollingParent != null) {
            if (i3 != 0 || i4 != 0) {
                int i5 = 0;
                int i6 = 0;
                if (iArr4 != null) {
                    this.mView.getLocationInWindow(iArr4);
                    i5 = iArr4[0];
                    i6 = iArr4[1];
                }
                if (iArr3 == null) {
                    if (this.mTempNestedScrollConsumed == null) {
                        this.mTempNestedScrollConsumed = new int[2];
                    }
                    iArr3 = this.mTempNestedScrollConsumed;
                }
                iArr3[0] = 0;
                iArr3[1] = 0;
                ViewParentCompat.onNestedPreScroll(this.mNestedScrollingParent, this.mView, i3, i4, iArr3);
                if (iArr4 != null) {
                    this.mView.getLocationInWindow(iArr4);
                    int[] iArr5 = iArr4;
                    iArr5[0] = iArr5[0] - i5;
                    int[] iArr6 = iArr4;
                    iArr6[1] = iArr6[1] - i6;
                }
                return (iArr3[0] == 0 && iArr3[1] == 0) ? false : true;
            } else if (iArr4 != null) {
                iArr4[0] = 0;
                iArr4[1] = 0;
            }
        }
        return false;
    }

    public boolean dispatchNestedFling(float f, float f2, boolean z) {
        float f3 = f;
        float f4 = f2;
        boolean z2 = z;
        if (!isNestedScrollingEnabled() || this.mNestedScrollingParent == null) {
            return false;
        }
        return ViewParentCompat.onNestedFling(this.mNestedScrollingParent, this.mView, f3, f4, z2);
    }

    public boolean dispatchNestedPreFling(float f, float f2) {
        float f3 = f;
        float f4 = f2;
        if (!isNestedScrollingEnabled() || this.mNestedScrollingParent == null) {
            return false;
        }
        return ViewParentCompat.onNestedPreFling(this.mNestedScrollingParent, this.mView, f3, f4);
    }

    public void onDetachedFromWindow() {
        ViewCompat.stopNestedScroll(this.mView);
    }

    public void onStopNestedScroll(View view) {
        ViewCompat.stopNestedScroll(this.mView);
    }
}
