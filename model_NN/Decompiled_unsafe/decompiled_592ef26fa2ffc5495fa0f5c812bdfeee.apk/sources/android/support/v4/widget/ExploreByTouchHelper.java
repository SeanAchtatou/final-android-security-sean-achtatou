package android.support.v4.widget;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewParentCompat;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityManagerCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityNodeProviderCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public abstract class ExploreByTouchHelper extends AccessibilityDelegateCompat {
    private static final String DEFAULT_CLASS_NAME = View.class.getName();
    public static final int HOST_ID = -1;
    public static final int INVALID_ID = Integer.MIN_VALUE;
    private int mFocusedVirtualViewId = Integer.MIN_VALUE;
    private int mHoveredVirtualViewId = Integer.MIN_VALUE;
    private final AccessibilityManager mManager;
    private ExploreByTouchNodeProvider mNodeProvider;
    private final int[] mTempGlobalRect = new int[2];
    private final Rect mTempParentRect;
    private final Rect mTempScreenRect;
    private final Rect mTempVisibleRect;
    private final View mView;

    /* access modifiers changed from: protected */
    public abstract int getVirtualViewAt(float f, float f2);

    /* access modifiers changed from: protected */
    public abstract void getVisibleVirtualViews(List<Integer> list);

    /* access modifiers changed from: protected */
    public abstract boolean onPerformActionForVirtualView(int i, int i2, Bundle bundle);

    /* access modifiers changed from: protected */
    public abstract void onPopulateEventForVirtualView(int i, AccessibilityEvent accessibilityEvent);

    /* access modifiers changed from: protected */
    public abstract void onPopulateNodeForVirtualView(int i, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat);

    public ExploreByTouchHelper(View view) {
        Rect rect;
        Rect rect2;
        Rect rect3;
        Throwable th;
        View view2 = view;
        new Rect();
        this.mTempScreenRect = rect;
        new Rect();
        this.mTempParentRect = rect2;
        new Rect();
        this.mTempVisibleRect = rect3;
        if (view2 == null) {
            Throwable th2 = th;
            new IllegalArgumentException("View may not be null");
            throw th2;
        }
        this.mView = view2;
        this.mManager = (AccessibilityManager) view2.getContext().getSystemService("accessibility");
    }

    public AccessibilityNodeProviderCompat getAccessibilityNodeProvider(View view) {
        ExploreByTouchNodeProvider exploreByTouchNodeProvider;
        if (this.mNodeProvider == null) {
            new ExploreByTouchNodeProvider();
            this.mNodeProvider = exploreByTouchNodeProvider;
        }
        return this.mNodeProvider;
    }

    public boolean dispatchHoverEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        if (!this.mManager.isEnabled() || !AccessibilityManagerCompat.isTouchExplorationEnabled(this.mManager)) {
            return false;
        }
        switch (motionEvent2.getAction()) {
            case 7:
            case 9:
                int virtualViewAt = getVirtualViewAt(motionEvent2.getX(), motionEvent2.getY());
                updateHoveredVirtualView(virtualViewAt);
                return virtualViewAt != Integer.MIN_VALUE;
            case 8:
            default:
                return false;
            case 10:
                if (this.mFocusedVirtualViewId == Integer.MIN_VALUE) {
                    return false;
                }
                updateHoveredVirtualView(Integer.MIN_VALUE);
                return true;
        }
    }

    public boolean sendEventForVirtualView(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (i3 == Integer.MIN_VALUE || !this.mManager.isEnabled()) {
            return false;
        }
        ViewParent parent = this.mView.getParent();
        if (parent == null) {
            return false;
        }
        return ViewParentCompat.requestSendAccessibilityEvent(parent, this.mView, createEvent(i3, i4));
    }

    public void invalidateRoot() {
        invalidateVirtualView(-1);
    }

    public void invalidateVirtualView(int i) {
        boolean sendEventForVirtualView = sendEventForVirtualView(i, 2048);
    }

    public int getFocusedVirtualView() {
        return this.mFocusedVirtualViewId;
    }

    private void updateHoveredVirtualView(int i) {
        int i2 = i;
        if (this.mHoveredVirtualViewId != i2) {
            int i3 = this.mHoveredVirtualViewId;
            this.mHoveredVirtualViewId = i2;
            boolean sendEventForVirtualView = sendEventForVirtualView(i2, 128);
            boolean sendEventForVirtualView2 = sendEventForVirtualView(i3, 256);
        }
    }

    private AccessibilityEvent createEvent(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        switch (i3) {
            case -1:
                return createEventForHost(i4);
            default:
                return createEventForChild(i3, i4);
        }
    }

    private AccessibilityEvent createEventForHost(int i) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i);
        ViewCompat.onInitializeAccessibilityEvent(this.mView, obtain);
        return obtain;
    }

    private AccessibilityEvent createEventForChild(int i, int i2) {
        Throwable th;
        int i3 = i;
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
        obtain.setEnabled(true);
        obtain.setClassName(DEFAULT_CLASS_NAME);
        onPopulateEventForVirtualView(i3, obtain);
        if (!obtain.getText().isEmpty() || obtain.getContentDescription() != null) {
            obtain.setPackageName(this.mView.getContext().getPackageName());
            AccessibilityEventCompat.asRecord(obtain).setSource(this.mView, i3);
            return obtain;
        }
        Throwable th2 = th;
        new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
        throw th2;
    }

    /* access modifiers changed from: private */
    public AccessibilityNodeInfoCompat createNode(int i) {
        int i2 = i;
        switch (i2) {
            case -1:
                return createNodeForHost();
            default:
                return createNodeForChild(i2);
        }
    }

    private AccessibilityNodeInfoCompat createNodeForHost() {
        LinkedList linkedList;
        AccessibilityNodeInfoCompat obtain = AccessibilityNodeInfoCompat.obtain(this.mView);
        ViewCompat.onInitializeAccessibilityNodeInfo(this.mView, obtain);
        onPopulateNodeForHost(obtain);
        new LinkedList();
        LinkedList linkedList2 = linkedList;
        getVisibleVirtualViews(linkedList2);
        Iterator it = linkedList2.iterator();
        while (it.hasNext()) {
            obtain.addChild(this.mView, ((Integer) it.next()).intValue());
        }
        return obtain;
    }

    private AccessibilityNodeInfoCompat createNodeForChild(int i) {
        Throwable th;
        Throwable th2;
        Throwable th3;
        Throwable th4;
        int i2 = i;
        AccessibilityNodeInfoCompat obtain = AccessibilityNodeInfoCompat.obtain();
        obtain.setEnabled(true);
        obtain.setClassName(DEFAULT_CLASS_NAME);
        onPopulateNodeForVirtualView(i2, obtain);
        if (obtain.getText() == null && obtain.getContentDescription() == null) {
            Throwable th5 = th4;
            new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
            throw th5;
        }
        obtain.getBoundsInParent(this.mTempParentRect);
        if (this.mTempParentRect.isEmpty()) {
            Throwable th6 = th3;
            new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
            throw th6;
        }
        int actions = obtain.getActions();
        if ((actions & 64) != 0) {
            Throwable th7 = th2;
            new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            throw th7;
        } else if ((actions & 128) != 0) {
            Throwable th8 = th;
            new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            throw th8;
        } else {
            obtain.setPackageName(this.mView.getContext().getPackageName());
            obtain.setSource(this.mView, i2);
            obtain.setParent(this.mView);
            if (this.mFocusedVirtualViewId == i2) {
                obtain.setAccessibilityFocused(true);
                obtain.addAction(128);
            } else {
                obtain.setAccessibilityFocused(false);
                obtain.addAction(64);
            }
            if (intersectVisibleToUser(this.mTempParentRect)) {
                obtain.setVisibleToUser(true);
                obtain.setBoundsInParent(this.mTempParentRect);
            }
            this.mView.getLocationOnScreen(this.mTempGlobalRect);
            int i3 = this.mTempGlobalRect[0];
            int i4 = this.mTempGlobalRect[1];
            this.mTempScreenRect.set(this.mTempParentRect);
            this.mTempScreenRect.offset(i3, i4);
            obtain.setBoundsInScreen(this.mTempScreenRect);
            return obtain;
        }
    }

    /* access modifiers changed from: private */
    public boolean performAction(int i, int i2, Bundle bundle) {
        int i3 = i;
        int i4 = i2;
        Bundle bundle2 = bundle;
        switch (i3) {
            case -1:
                return performActionForHost(i4, bundle2);
            default:
                return performActionForChild(i3, i4, bundle2);
        }
    }

    private boolean performActionForHost(int i, Bundle bundle) {
        return ViewCompat.performAccessibilityAction(this.mView, i, bundle);
    }

    private boolean performActionForChild(int i, int i2, Bundle bundle) {
        int i3 = i;
        int i4 = i2;
        Bundle bundle2 = bundle;
        switch (i4) {
            case 64:
            case 128:
                return manageFocusForChild(i3, i4, bundle2);
            default:
                return onPerformActionForVirtualView(i3, i4, bundle2);
        }
    }

    private boolean manageFocusForChild(int i, int i2, Bundle bundle) {
        int i3 = i;
        switch (i2) {
            case 64:
                return requestAccessibilityFocus(i3);
            case 128:
                return clearAccessibilityFocus(i3);
            default:
                return false;
        }
    }

    private boolean intersectVisibleToUser(Rect rect) {
        Rect rect2 = rect;
        if (rect2 == null || rect2.isEmpty()) {
            return false;
        }
        if (this.mView.getWindowVisibility() != 0) {
            return false;
        }
        ViewParent parent = this.mView.getParent();
        while (true) {
            ViewParent viewParent = parent;
            if (viewParent instanceof View) {
                View view = (View) viewParent;
                if (ViewCompat.getAlpha(view) > 0.0f && view.getVisibility() == 0) {
                    parent = view.getParent();
                }
            } else if (viewParent == null) {
                return false;
            } else {
                if (!this.mView.getLocalVisibleRect(this.mTempVisibleRect)) {
                    return false;
                }
                return rect2.intersect(this.mTempVisibleRect);
            }
        }
        return false;
    }

    private boolean isAccessibilityFocused(int i) {
        return this.mFocusedVirtualViewId == i;
    }

    private boolean requestAccessibilityFocus(int i) {
        int i2 = i;
        if (!this.mManager.isEnabled() || !AccessibilityManagerCompat.isTouchExplorationEnabled(this.mManager)) {
            return false;
        }
        if (isAccessibilityFocused(i2)) {
            return false;
        }
        if (this.mFocusedVirtualViewId != Integer.MIN_VALUE) {
            boolean sendEventForVirtualView = sendEventForVirtualView(this.mFocusedVirtualViewId, 65536);
        }
        this.mFocusedVirtualViewId = i2;
        this.mView.invalidate();
        boolean sendEventForVirtualView2 = sendEventForVirtualView(i2, 32768);
        return true;
    }

    private boolean clearAccessibilityFocus(int i) {
        int i2 = i;
        if (!isAccessibilityFocused(i2)) {
            return false;
        }
        this.mFocusedVirtualViewId = Integer.MIN_VALUE;
        this.mView.invalidate();
        boolean sendEventForVirtualView = sendEventForVirtualView(i2, 65536);
        return true;
    }

    public void onPopulateNodeForHost(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
    }

    private class ExploreByTouchNodeProvider extends AccessibilityNodeProviderCompat {
        private ExploreByTouchNodeProvider() {
        }

        public AccessibilityNodeInfoCompat createAccessibilityNodeInfo(int i) {
            return ExploreByTouchHelper.this.createNode(i);
        }

        public boolean performAction(int i, int i2, Bundle bundle) {
            return ExploreByTouchHelper.this.performAction(i, i2, bundle);
        }
    }
}
