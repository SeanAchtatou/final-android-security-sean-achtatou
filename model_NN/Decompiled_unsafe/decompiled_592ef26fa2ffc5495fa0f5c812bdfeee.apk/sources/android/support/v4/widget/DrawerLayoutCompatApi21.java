package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowInsets;

class DrawerLayoutCompatApi21 {
    private static final int[] THEME_ATTRS = {16843828};

    DrawerLayoutCompatApi21() {
    }

    public static void configureApplyInsets(View view) {
        View.OnApplyWindowInsetsListener onApplyWindowInsetsListener;
        View view2 = view;
        if (view2 instanceof DrawerLayoutImpl) {
            new InsetsListener();
            view2.setOnApplyWindowInsetsListener(onApplyWindowInsetsListener);
            view2.setSystemUiVisibility(1280);
        }
    }

    public static void dispatchChildInsets(View view, Object obj, int i) {
        View view2 = view;
        int i2 = i;
        WindowInsets windowInsets = (WindowInsets) obj;
        if (i2 == 3) {
            windowInsets = windowInsets.replaceSystemWindowInsets(windowInsets.getSystemWindowInsetLeft(), windowInsets.getSystemWindowInsetTop(), 0, windowInsets.getSystemWindowInsetBottom());
        } else if (i2 == 5) {
            windowInsets = windowInsets.replaceSystemWindowInsets(0, windowInsets.getSystemWindowInsetTop(), windowInsets.getSystemWindowInsetRight(), windowInsets.getSystemWindowInsetBottom());
        }
        WindowInsets dispatchApplyWindowInsets = view2.dispatchApplyWindowInsets(windowInsets);
    }

    public static void applyMarginInsets(ViewGroup.MarginLayoutParams marginLayoutParams, Object obj, int i) {
        ViewGroup.MarginLayoutParams marginLayoutParams2 = marginLayoutParams;
        int i2 = i;
        WindowInsets windowInsets = (WindowInsets) obj;
        if (i2 == 3) {
            windowInsets = windowInsets.replaceSystemWindowInsets(windowInsets.getSystemWindowInsetLeft(), windowInsets.getSystemWindowInsetTop(), 0, windowInsets.getSystemWindowInsetBottom());
        } else if (i2 == 5) {
            windowInsets = windowInsets.replaceSystemWindowInsets(0, windowInsets.getSystemWindowInsetTop(), windowInsets.getSystemWindowInsetRight(), windowInsets.getSystemWindowInsetBottom());
        }
        marginLayoutParams2.leftMargin = windowInsets.getSystemWindowInsetLeft();
        marginLayoutParams2.topMargin = windowInsets.getSystemWindowInsetTop();
        marginLayoutParams2.rightMargin = windowInsets.getSystemWindowInsetRight();
        marginLayoutParams2.bottomMargin = windowInsets.getSystemWindowInsetBottom();
    }

    public static int getTopInset(Object obj) {
        Object obj2 = obj;
        return obj2 != null ? ((WindowInsets) obj2).getSystemWindowInsetTop() : 0;
    }

    /* JADX INFO: finally extract failed */
    public static Drawable getDefaultStatusBarBackground(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(THEME_ATTRS);
        try {
            Drawable drawable = obtainStyledAttributes.getDrawable(0);
            obtainStyledAttributes.recycle();
            return drawable;
        } catch (Throwable th) {
            Throwable th2 = th;
            obtainStyledAttributes.recycle();
            throw th2;
        }
    }

    static class InsetsListener implements View.OnApplyWindowInsetsListener {
        InsetsListener() {
        }

        public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
            WindowInsets windowInsets2 = windowInsets;
            ((DrawerLayoutImpl) view).setChildInsets(windowInsets2, windowInsets2.getSystemWindowInsetTop() > 0);
            return windowInsets2.consumeSystemWindowInsets();
        }
    }
}
