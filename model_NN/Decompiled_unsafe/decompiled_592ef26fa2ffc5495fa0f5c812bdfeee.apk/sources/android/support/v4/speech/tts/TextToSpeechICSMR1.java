package android.support.v4.speech.tts;

import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import java.util.Locale;
import java.util.Set;

class TextToSpeechICSMR1 {
    public static final String KEY_FEATURE_EMBEDDED_SYNTHESIS = "embeddedTts";
    public static final String KEY_FEATURE_NETWORK_SYNTHESIS = "networkTts";

    interface UtteranceProgressListenerICSMR1 {
        void onDone(String str);

        void onError(String str);

        void onStart(String str);
    }

    TextToSpeechICSMR1() {
    }

    static Set<String> getFeatures(TextToSpeech textToSpeech, Locale locale) {
        TextToSpeech textToSpeech2 = textToSpeech;
        Locale locale2 = locale;
        if (Build.VERSION.SDK_INT >= 15) {
            return textToSpeech2.getFeatures(locale2);
        }
        return null;
    }

    static void setUtteranceProgressListener(TextToSpeech textToSpeech, UtteranceProgressListenerICSMR1 utteranceProgressListenerICSMR1) {
        TextToSpeech.OnUtteranceCompletedListener onUtteranceCompletedListener;
        UtteranceProgressListener utteranceProgressListener;
        TextToSpeech textToSpeech2 = textToSpeech;
        UtteranceProgressListenerICSMR1 utteranceProgressListenerICSMR12 = utteranceProgressListenerICSMR1;
        if (Build.VERSION.SDK_INT >= 15) {
            final UtteranceProgressListenerICSMR1 utteranceProgressListenerICSMR13 = utteranceProgressListenerICSMR12;
            new UtteranceProgressListener() {
                public void onStart(String str) {
                    utteranceProgressListenerICSMR13.onStart(str);
                }

                public void onError(String str) {
                    utteranceProgressListenerICSMR13.onError(str);
                }

                public void onDone(String str) {
                    utteranceProgressListenerICSMR13.onDone(str);
                }
            };
            int onUtteranceProgressListener = textToSpeech2.setOnUtteranceProgressListener(utteranceProgressListener);
            return;
        }
        final UtteranceProgressListenerICSMR1 utteranceProgressListenerICSMR14 = utteranceProgressListenerICSMR12;
        new TextToSpeech.OnUtteranceCompletedListener() {
            public void onUtteranceCompleted(String str) {
                String str2 = str;
                utteranceProgressListenerICSMR14.onStart(str2);
                utteranceProgressListenerICSMR14.onDone(str2);
            }
        };
        int onUtteranceCompletedListener2 = textToSpeech2.setOnUtteranceCompletedListener(onUtteranceCompletedListener);
    }
}
