package android.support.v7.app;

import android.content.Context;
import android.support.v7.app.AppCompatDelegateImplV14;
import android.view.ActionMode;
import android.view.Window;

class AppCompatDelegateImplV23 extends AppCompatDelegateImplV14 {
    AppCompatDelegateImplV23(Context context, Window window, AppCompatCallback appCompatCallback) {
        super(context, window, appCompatCallback);
    }

    /* access modifiers changed from: package-private */
    public Window.Callback wrapWindowCallback(Window.Callback callback) {
        Window.Callback callback2;
        new AppCompatWindowCallbackV23(this, callback);
        return callback2;
    }

    class AppCompatWindowCallbackV23 extends AppCompatDelegateImplV14.AppCompatWindowCallbackV14 {
        final /* synthetic */ AppCompatDelegateImplV23 this$0;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        AppCompatWindowCallbackV23(android.support.v7.app.AppCompatDelegateImplV23 r7, android.view.Window.Callback r8) {
            /*
                r6 = this;
                r0 = r6
                r1 = r7
                r2 = r8
                r3 = r0
                r4 = r1
                r3.this$0 = r4
                r3 = r0
                r4 = r1
                r5 = r2
                r3.<init>(r4, r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.AppCompatDelegateImplV23.AppCompatWindowCallbackV23.<init>(android.support.v7.app.AppCompatDelegateImplV23, android.view.Window$Callback):void");
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
            ActionMode.Callback callback2 = callback;
            int i2 = i;
            if (this.this$0.isHandleNativeActionModesEnabled()) {
                switch (i2) {
                    case 0:
                        return startAsSupportActionMode(callback2);
                }
            }
            return super.onWindowStartingActionMode(callback2, i2);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            return null;
        }
    }
}
