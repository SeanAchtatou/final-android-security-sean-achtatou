package android.support.v7.widget;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

public class AppCompatCheckedTextView extends CheckedTextView {
    private static final int[] TINT_ATTRS = {16843016};
    private AppCompatTextHelper mTextHelper;
    private TintManager mTintManager;

    public AppCompatCheckedTextView(Context context) {
        this(context, null);
    }

    public AppCompatCheckedTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16843720);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatCheckedTextView(android.content.Context r11, android.util.AttributeSet r12, int r13) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r3 = r13
            r5 = r0
            r6 = r1
            r7 = r2
            r8 = r3
            r5.<init>(r6, r7, r8)
            r5 = r0
            r6 = r0
            android.support.v7.widget.AppCompatTextHelper r6 = android.support.v7.widget.AppCompatTextHelper.create(r6)
            r5.mTextHelper = r6
            r5 = r0
            android.support.v7.widget.AppCompatTextHelper r5 = r5.mTextHelper
            r6 = r2
            r7 = r3
            r5.loadFromAttributes(r6, r7)
            r5 = r0
            android.support.v7.widget.AppCompatTextHelper r5 = r5.mTextHelper
            r5.applyCompoundDrawablesTints()
            boolean r5 = android.support.v7.widget.TintManager.SHOULD_BE_USED
            if (r5 == 0) goto L_0x004a
            r5 = r0
            android.content.Context r5 = r5.getContext()
            r6 = r2
            int[] r7 = android.support.v7.widget.AppCompatCheckedTextView.TINT_ATTRS
            r8 = r3
            r9 = 0
            android.support.v7.widget.TintTypedArray r5 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r5, r6, r7, r8, r9)
            r4 = r5
            r5 = r0
            r6 = r4
            r7 = 0
            android.graphics.drawable.Drawable r6 = r6.getDrawable(r7)
            r5.setCheckMarkDrawable(r6)
            r5 = r4
            r5.recycle()
            r5 = r0
            r6 = r4
            android.support.v7.widget.TintManager r6 = r6.getTintManager()
            r5.mTintManager = r6
        L_0x004a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AppCompatCheckedTextView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setCheckMarkDrawable(@DrawableRes int i) {
        int i2 = i;
        if (this.mTintManager != null) {
            setCheckMarkDrawable(this.mTintManager.getDrawable(i2));
        } else {
            super.setCheckMarkDrawable(i2);
        }
    }

    public void setTextAppearance(Context context, int i) {
        Context context2 = context;
        int i2 = i;
        super.setTextAppearance(context2, i2);
        if (this.mTextHelper != null) {
            this.mTextHelper.onSetTextAppearance(context2, i2);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.mTextHelper != null) {
            this.mTextHelper.applyCompoundDrawablesTints();
        }
    }
}
