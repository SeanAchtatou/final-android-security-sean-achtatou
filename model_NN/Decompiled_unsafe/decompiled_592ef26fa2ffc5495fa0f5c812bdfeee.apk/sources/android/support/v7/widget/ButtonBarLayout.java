package android.support.v7.widget;

import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.view.View;
import android.widget.LinearLayout;

public class ButtonBarLayout extends LinearLayout {
    private boolean mAllowStacking;
    private int mLastWidthSize = -1;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ButtonBarLayout(android.content.Context r9, android.util.AttributeSet r10) {
        /*
            r8 = this;
            r0 = r8
            r1 = r9
            r2 = r10
            r4 = r0
            r5 = r1
            r6 = r2
            r4.<init>(r5, r6)
            r4 = r0
            r5 = -1
            r4.mLastWidthSize = r5
            r4 = r1
            r5 = r2
            int[] r6 = android.support.v7.appcompat.R.styleable.ButtonBarLayout
            android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r6)
            r3 = r4
            r4 = r0
            r5 = r3
            int r6 = android.support.v7.appcompat.R.styleable.ButtonBarLayout_allowStacking
            r7 = 0
            boolean r5 = r5.getBoolean(r6, r7)
            r4.mAllowStacking = r5
            r4 = r3
            r4.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ButtonBarLayout.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public void setAllowStacking(boolean z) {
        boolean z2 = z;
        if (this.mAllowStacking != z2) {
            this.mAllowStacking = z2;
            if (!this.mAllowStacking && getOrientation() == 1) {
                setStacked(false);
            }
            requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4 = i;
        int i5 = i2;
        int size = View.MeasureSpec.getSize(i4);
        if (this.mAllowStacking) {
            if (size > this.mLastWidthSize && isStacked()) {
                setStacked(false);
            }
            this.mLastWidthSize = size;
        }
        boolean z = false;
        if (isStacked() || View.MeasureSpec.getMode(i4) != 1073741824) {
            i3 = i4;
        } else {
            i3 = View.MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE);
            z = true;
        }
        super.onMeasure(i3, i5);
        if (this.mAllowStacking && !isStacked() && (getMeasuredWidthAndState() & ViewCompat.MEASURED_STATE_MASK) == 16777216) {
            setStacked(true);
            z = true;
        }
        if (z) {
            super.onMeasure(i4, i5);
        }
    }

    private void setStacked(boolean z) {
        boolean z2 = z;
        setOrientation(z2 ? 1 : 0);
        setGravity(z2 ? 5 : 80);
        View findViewById = findViewById(R.id.spacer);
        if (findViewById != null) {
            findViewById.setVisibility(z2 ? 8 : 4);
        }
        for (int childCount = getChildCount() - 2; childCount >= 0; childCount--) {
            bringChildToFront(getChildAt(childCount));
        }
    }

    private boolean isStacked() {
        return getOrientation() == 1;
    }
}
