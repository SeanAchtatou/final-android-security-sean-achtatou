package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;

class ScrollbarHelper {
    ScrollbarHelper() {
    }

    static int computeScrollOffset(RecyclerView.State state, OrientationHelper orientationHelper, View view, View view2, RecyclerView.LayoutManager layoutManager, boolean z, boolean z2) {
        RecyclerView.State state2 = state;
        OrientationHelper orientationHelper2 = orientationHelper;
        View view3 = view;
        View view4 = view2;
        RecyclerView.LayoutManager layoutManager2 = layoutManager;
        boolean z3 = z;
        boolean z4 = z2;
        if (layoutManager2.getChildCount() == 0 || state2.getItemCount() == 0 || view3 == null || view4 == null) {
            return 0;
        }
        int max = z4 ? Math.max(0, (state2.getItemCount() - Math.max(layoutManager2.getPosition(view3), layoutManager2.getPosition(view4))) - 1) : Math.max(0, Math.min(layoutManager2.getPosition(view3), layoutManager2.getPosition(view4)));
        if (!z3) {
            return max;
        }
        return Math.round((((float) max) * (((float) Math.abs(orientationHelper2.getDecoratedEnd(view4) - orientationHelper2.getDecoratedStart(view3))) / ((float) (Math.abs(layoutManager2.getPosition(view3) - layoutManager2.getPosition(view4)) + 1)))) + ((float) (orientationHelper2.getStartAfterPadding() - orientationHelper2.getDecoratedStart(view3))));
    }

    static int computeScrollExtent(RecyclerView.State state, OrientationHelper orientationHelper, View view, View view2, RecyclerView.LayoutManager layoutManager, boolean z) {
        RecyclerView.State state2 = state;
        OrientationHelper orientationHelper2 = orientationHelper;
        View view3 = view;
        View view4 = view2;
        RecyclerView.LayoutManager layoutManager2 = layoutManager;
        boolean z2 = z;
        if (layoutManager2.getChildCount() == 0 || state2.getItemCount() == 0 || view3 == null || view4 == null) {
            return 0;
        }
        if (!z2) {
            return Math.abs(layoutManager2.getPosition(view3) - layoutManager2.getPosition(view4)) + 1;
        }
        return Math.min(orientationHelper2.getTotalSpace(), orientationHelper2.getDecoratedEnd(view4) - orientationHelper2.getDecoratedStart(view3));
    }

    static int computeScrollRange(RecyclerView.State state, OrientationHelper orientationHelper, View view, View view2, RecyclerView.LayoutManager layoutManager, boolean z) {
        RecyclerView.State state2 = state;
        OrientationHelper orientationHelper2 = orientationHelper;
        View view3 = view;
        View view4 = view2;
        RecyclerView.LayoutManager layoutManager2 = layoutManager;
        boolean z2 = z;
        if (layoutManager2.getChildCount() == 0 || state2.getItemCount() == 0 || view3 == null || view4 == null) {
            return 0;
        }
        if (!z2) {
            return state2.getItemCount();
        }
        return (int) ((((float) (orientationHelper2.getDecoratedEnd(view4) - orientationHelper2.getDecoratedStart(view3))) / ((float) (Math.abs(layoutManager2.getPosition(view3) - layoutManager2.getPosition(view4)) + 1))) * ((float) state2.getItemCount()));
    }
}
