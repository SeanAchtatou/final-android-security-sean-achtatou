package android.support.v7.widget;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.view.accessibility.AccessibilityRecordCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class StaggeredGridLayoutManager extends RecyclerView.LayoutManager {
    private static final boolean DEBUG = false;
    @Deprecated
    public static final int GAP_HANDLING_LAZY = 1;
    public static final int GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS = 2;
    public static final int GAP_HANDLING_NONE = 0;
    public static final int HORIZONTAL = 0;
    private static final int INVALID_OFFSET = Integer.MIN_VALUE;
    public static final String TAG = "StaggeredGridLayoutManager";
    public static final int VERTICAL = 1;
    private final AnchorInfo mAnchorInfo;
    private final Runnable mCheckForGapsRunnable;
    private int mFullSizeSpec;
    private int mGapStrategy;
    private int mHeightSpec;
    private boolean mLaidOutInvalidFullSpan;
    private boolean mLastLayoutFromEnd;
    private boolean mLastLayoutRTL;
    private LayoutState mLayoutState;
    LazySpanLookup mLazySpanLookup;
    /* access modifiers changed from: private */
    public int mOrientation;
    private SavedState mPendingSavedState;
    int mPendingScrollPosition = -1;
    int mPendingScrollPositionOffset = Integer.MIN_VALUE;
    OrientationHelper mPrimaryOrientation;
    private BitSet mRemainingSpans;
    /* access modifiers changed from: private */
    public boolean mReverseLayout = false;
    OrientationHelper mSecondaryOrientation;
    boolean mShouldReverseLayout = false;
    private int mSizePerSpan;
    private boolean mSmoothScrollbarEnabled;
    private int mSpanCount = -1;
    private Span[] mSpans;
    private final Rect mTmpRect;
    private int mWidthSpec;

    public StaggeredGridLayoutManager(Context context, AttributeSet attributeSet, int i, int i2) {
        LazySpanLookup lazySpanLookup;
        Rect rect;
        AnchorInfo anchorInfo;
        Runnable runnable;
        new LazySpanLookup();
        this.mLazySpanLookup = lazySpanLookup;
        this.mGapStrategy = 2;
        new Rect();
        this.mTmpRect = rect;
        new AnchorInfo();
        this.mAnchorInfo = anchorInfo;
        this.mLaidOutInvalidFullSpan = false;
        this.mSmoothScrollbarEnabled = true;
        new Runnable() {
            public void run() {
                boolean access$100 = StaggeredGridLayoutManager.this.checkForGaps();
            }
        };
        this.mCheckForGapsRunnable = runnable;
        RecyclerView.LayoutManager.Properties properties = getProperties(context, attributeSet, i, i2);
        setOrientation(properties.orientation);
        setSpanCount(properties.spanCount);
        setReverseLayout(properties.reverseLayout);
    }

    public StaggeredGridLayoutManager(int i, int i2) {
        LazySpanLookup lazySpanLookup;
        Rect rect;
        AnchorInfo anchorInfo;
        Runnable runnable;
        new LazySpanLookup();
        this.mLazySpanLookup = lazySpanLookup;
        this.mGapStrategy = 2;
        new Rect();
        this.mTmpRect = rect;
        new AnchorInfo();
        this.mAnchorInfo = anchorInfo;
        this.mLaidOutInvalidFullSpan = false;
        this.mSmoothScrollbarEnabled = true;
        new Runnable() {
            public void run() {
                boolean access$100 = StaggeredGridLayoutManager.this.checkForGaps();
            }
        };
        this.mCheckForGapsRunnable = runnable;
        this.mOrientation = i2;
        setSpanCount(i);
    }

    /* access modifiers changed from: private */
    public boolean checkForGaps() {
        int firstChildPosition;
        int lastChildPosition;
        if (getChildCount() == 0 || this.mGapStrategy == 0 || !isAttachedToWindow()) {
            return false;
        }
        if (this.mShouldReverseLayout) {
            firstChildPosition = getLastChildPosition();
            lastChildPosition = getFirstChildPosition();
        } else {
            firstChildPosition = getFirstChildPosition();
            lastChildPosition = getLastChildPosition();
        }
        if (firstChildPosition == 0 && hasGapsToFix() != null) {
            this.mLazySpanLookup.clear();
            requestSimpleAnimationsInNextLayout();
            requestLayout();
            return true;
        } else if (!this.mLaidOutInvalidFullSpan) {
            return false;
        } else {
            int i = this.mShouldReverseLayout ? -1 : 1;
            LazySpanLookup.FullSpanItem firstFullSpanItemInRange = this.mLazySpanLookup.getFirstFullSpanItemInRange(firstChildPosition, lastChildPosition + 1, i, true);
            if (firstFullSpanItemInRange == null) {
                this.mLaidOutInvalidFullSpan = false;
                int forceInvalidateAfter = this.mLazySpanLookup.forceInvalidateAfter(lastChildPosition + 1);
                return false;
            }
            LazySpanLookup.FullSpanItem firstFullSpanItemInRange2 = this.mLazySpanLookup.getFirstFullSpanItemInRange(firstChildPosition, firstFullSpanItemInRange.mPosition, i * -1, true);
            if (firstFullSpanItemInRange2 == null) {
                int forceInvalidateAfter2 = this.mLazySpanLookup.forceInvalidateAfter(firstFullSpanItemInRange.mPosition);
            } else {
                int forceInvalidateAfter3 = this.mLazySpanLookup.forceInvalidateAfter(firstFullSpanItemInRange2.mPosition + 1);
            }
            requestSimpleAnimationsInNextLayout();
            requestLayout();
            return true;
        }
    }

    public void onScrollStateChanged(int i) {
        if (i == 0) {
            boolean checkForGaps = checkForGaps();
        }
    }

    public void onDetachedFromWindow(RecyclerView recyclerView, RecyclerView.Recycler recycler) {
        boolean removeCallbacks = removeCallbacks(this.mCheckForGapsRunnable);
        for (int i = 0; i < this.mSpanCount; i++) {
            this.mSpans[i].clear();
        }
    }

    /* access modifiers changed from: package-private */
    public View hasGapsToFix() {
        BitSet bitSet;
        int i;
        int i2;
        int childCount = getChildCount() - 1;
        new BitSet(this.mSpanCount);
        BitSet bitSet2 = bitSet;
        bitSet2.set(0, this.mSpanCount, true);
        char c = (this.mOrientation != 1 || !isLayoutRTL()) ? (char) 65535 : 1;
        if (this.mShouldReverseLayout) {
            i = childCount;
            i2 = 0 - 1;
        } else {
            i = 0;
            i2 = childCount + 1;
        }
        int i3 = i < i2 ? 1 : -1;
        int i4 = i;
        while (true) {
            int i5 = i4;
            if (i5 == i2) {
                return null;
            }
            View childAt = getChildAt(i5);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (bitSet2.get(layoutParams.mSpan.mIndex)) {
                if (checkSpanForGap(layoutParams.mSpan)) {
                    return childAt;
                }
                bitSet2.clear(layoutParams.mSpan.mIndex);
            }
            if (!layoutParams.mFullSpan && i5 + i3 != i2) {
                View childAt2 = getChildAt(i5 + i3);
                boolean z = false;
                if (this.mShouldReverseLayout) {
                    int decoratedEnd = this.mPrimaryOrientation.getDecoratedEnd(childAt);
                    int decoratedEnd2 = this.mPrimaryOrientation.getDecoratedEnd(childAt2);
                    if (decoratedEnd < decoratedEnd2) {
                        return childAt;
                    }
                    if (decoratedEnd == decoratedEnd2) {
                        z = true;
                    }
                } else {
                    int decoratedStart = this.mPrimaryOrientation.getDecoratedStart(childAt);
                    int decoratedStart2 = this.mPrimaryOrientation.getDecoratedStart(childAt2);
                    if (decoratedStart > decoratedStart2) {
                        return childAt;
                    }
                    if (decoratedStart == decoratedStart2) {
                        z = true;
                    }
                }
                if (z) {
                    if ((layoutParams.mSpan.mIndex - ((LayoutParams) childAt2.getLayoutParams()).mSpan.mIndex < 0) != (c < 0)) {
                        return childAt;
                    }
                } else {
                    continue;
                }
            }
            i4 = i5 + i3;
        }
    }

    private boolean checkSpanForGap(Span span) {
        Span span2 = span;
        if (this.mShouldReverseLayout) {
            if (span2.getEndLine() < this.mPrimaryOrientation.getEndAfterPadding()) {
                return true;
            }
        } else if (span2.getStartLine() > this.mPrimaryOrientation.getStartAfterPadding()) {
            return true;
        }
        return false;
    }

    public void setSpanCount(int i) {
        BitSet bitSet;
        Span span;
        int i2 = i;
        assertNotInLayoutOrScroll(null);
        if (i2 != this.mSpanCount) {
            invalidateSpanAssignments();
            this.mSpanCount = i2;
            new BitSet(this.mSpanCount);
            this.mRemainingSpans = bitSet;
            this.mSpans = new Span[this.mSpanCount];
            for (int i3 = 0; i3 < this.mSpanCount; i3++) {
                new Span(i3);
                this.mSpans[i3] = span;
            }
            requestLayout();
        }
    }

    public void setOrientation(int i) {
        Throwable th;
        int i2 = i;
        if (i2 == 0 || i2 == 1) {
            assertNotInLayoutOrScroll(null);
            if (i2 != this.mOrientation) {
                this.mOrientation = i2;
                if (!(this.mPrimaryOrientation == null || this.mSecondaryOrientation == null)) {
                    this.mPrimaryOrientation = this.mSecondaryOrientation;
                    this.mSecondaryOrientation = this.mPrimaryOrientation;
                }
                requestLayout();
                return;
            }
            return;
        }
        Throwable th2 = th;
        new IllegalArgumentException("invalid orientation.");
        throw th2;
    }

    public void setReverseLayout(boolean z) {
        boolean z2 = z;
        assertNotInLayoutOrScroll(null);
        if (!(this.mPendingSavedState == null || this.mPendingSavedState.mReverseLayout == z2)) {
            this.mPendingSavedState.mReverseLayout = z2;
        }
        this.mReverseLayout = z2;
        requestLayout();
    }

    public int getGapStrategy() {
        return this.mGapStrategy;
    }

    public void setGapStrategy(int i) {
        Throwable th;
        int i2 = i;
        assertNotInLayoutOrScroll(null);
        if (i2 != this.mGapStrategy) {
            if (i2 == 0 || i2 == 2) {
                this.mGapStrategy = i2;
                requestLayout();
                return;
            }
            Throwable th2 = th;
            new IllegalArgumentException("invalid gap strategy. Must be GAP_HANDLING_NONE or GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS");
            throw th2;
        }
    }

    public void assertNotInLayoutOrScroll(String str) {
        String str2 = str;
        if (this.mPendingSavedState == null) {
            super.assertNotInLayoutOrScroll(str2);
        }
    }

    public int getSpanCount() {
        return this.mSpanCount;
    }

    public void invalidateSpanAssignments() {
        this.mLazySpanLookup.clear();
        requestLayout();
    }

    private void ensureOrientationHelper() {
        LayoutState layoutState;
        if (this.mPrimaryOrientation == null) {
            this.mPrimaryOrientation = OrientationHelper.createOrientationHelper(this, this.mOrientation);
            this.mSecondaryOrientation = OrientationHelper.createOrientationHelper(this, 1 - this.mOrientation);
            new LayoutState();
            this.mLayoutState = layoutState;
        }
    }

    private void resolveShouldLayoutReverse() {
        if (this.mOrientation == 1 || !isLayoutRTL()) {
            this.mShouldReverseLayout = this.mReverseLayout;
        } else {
            this.mShouldReverseLayout = !this.mReverseLayout;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isLayoutRTL() {
        return getLayoutDirection() == 1;
    }

    public boolean getReverseLayout() {
        return this.mReverseLayout;
    }

    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        ensureOrientationHelper();
        AnchorInfo anchorInfo = this.mAnchorInfo;
        anchorInfo.reset();
        if (!(this.mPendingSavedState == null && this.mPendingScrollPosition == -1) && state2.getItemCount() == 0) {
            removeAndRecycleAllViews(recycler2);
            return;
        }
        if (this.mPendingSavedState != null) {
            applyPendingSavedState(anchorInfo);
        } else {
            resolveShouldLayoutReverse();
            anchorInfo.mLayoutFromEnd = this.mShouldReverseLayout;
        }
        updateAnchorInfoForLayout(state2, anchorInfo);
        if (this.mPendingSavedState == null && !(anchorInfo.mLayoutFromEnd == this.mLastLayoutFromEnd && isLayoutRTL() == this.mLastLayoutRTL)) {
            this.mLazySpanLookup.clear();
            anchorInfo.mInvalidateOffsets = true;
        }
        if (getChildCount() > 0 && (this.mPendingSavedState == null || this.mPendingSavedState.mSpanOffsetsSize < 1)) {
            if (anchorInfo.mInvalidateOffsets) {
                for (int i = 0; i < this.mSpanCount; i++) {
                    this.mSpans[i].clear();
                    if (anchorInfo.mOffset != Integer.MIN_VALUE) {
                        this.mSpans[i].setLine(anchorInfo.mOffset);
                    }
                }
            } else {
                for (int i2 = 0; i2 < this.mSpanCount; i2++) {
                    this.mSpans[i2].cacheReferenceLineAndClear(this.mShouldReverseLayout, anchorInfo.mOffset);
                }
            }
        }
        detachAndScrapAttachedViews(recycler2);
        this.mLaidOutInvalidFullSpan = false;
        updateMeasureSpecs();
        updateLayoutState(anchorInfo.mPosition, state2);
        if (anchorInfo.mLayoutFromEnd) {
            setLayoutStateDirection(-1);
            int fill = fill(recycler2, this.mLayoutState, state2);
            setLayoutStateDirection(1);
            this.mLayoutState.mCurrentPosition = anchorInfo.mPosition + this.mLayoutState.mItemDirection;
            int fill2 = fill(recycler2, this.mLayoutState, state2);
        } else {
            setLayoutStateDirection(1);
            int fill3 = fill(recycler2, this.mLayoutState, state2);
            setLayoutStateDirection(-1);
            this.mLayoutState.mCurrentPosition = anchorInfo.mPosition + this.mLayoutState.mItemDirection;
            int fill4 = fill(recycler2, this.mLayoutState, state2);
        }
        if (getChildCount() > 0) {
            if (this.mShouldReverseLayout) {
                fixEndGap(recycler2, state2, true);
                fixStartGap(recycler2, state2, false);
            } else {
                fixStartGap(recycler2, state2, true);
                fixEndGap(recycler2, state2, false);
            }
        }
        if (!state2.isPreLayout()) {
            if (this.mGapStrategy != 0 && getChildCount() > 0 && (this.mLaidOutInvalidFullSpan || hasGapsToFix() != null)) {
                boolean removeCallbacks = removeCallbacks(this.mCheckForGapsRunnable);
                postOnAnimation(this.mCheckForGapsRunnable);
            }
            this.mPendingScrollPosition = -1;
            this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        }
        this.mLastLayoutFromEnd = anchorInfo.mLayoutFromEnd;
        this.mLastLayoutRTL = isLayoutRTL();
        this.mPendingSavedState = null;
    }

    private void applyPendingSavedState(AnchorInfo anchorInfo) {
        AnchorInfo anchorInfo2 = anchorInfo;
        if (this.mPendingSavedState.mSpanOffsetsSize > 0) {
            if (this.mPendingSavedState.mSpanOffsetsSize == this.mSpanCount) {
                for (int i = 0; i < this.mSpanCount; i++) {
                    this.mSpans[i].clear();
                    int i2 = this.mPendingSavedState.mSpanOffsets[i];
                    if (i2 != Integer.MIN_VALUE) {
                        if (this.mPendingSavedState.mAnchorLayoutFromEnd) {
                            i2 += this.mPrimaryOrientation.getEndAfterPadding();
                        } else {
                            i2 += this.mPrimaryOrientation.getStartAfterPadding();
                        }
                    }
                    this.mSpans[i].setLine(i2);
                }
            } else {
                this.mPendingSavedState.invalidateSpanInfo();
                this.mPendingSavedState.mAnchorPosition = this.mPendingSavedState.mVisibleAnchorPosition;
            }
        }
        this.mLastLayoutRTL = this.mPendingSavedState.mLastLayoutRTL;
        setReverseLayout(this.mPendingSavedState.mReverseLayout);
        resolveShouldLayoutReverse();
        if (this.mPendingSavedState.mAnchorPosition != -1) {
            this.mPendingScrollPosition = this.mPendingSavedState.mAnchorPosition;
            anchorInfo2.mLayoutFromEnd = this.mPendingSavedState.mAnchorLayoutFromEnd;
        } else {
            anchorInfo2.mLayoutFromEnd = this.mShouldReverseLayout;
        }
        if (this.mPendingSavedState.mSpanLookupSize > 1) {
            this.mLazySpanLookup.mData = this.mPendingSavedState.mSpanLookup;
            this.mLazySpanLookup.mFullSpanItems = this.mPendingSavedState.mFullSpanItems;
        }
    }

    /* access modifiers changed from: package-private */
    public void updateAnchorInfoForLayout(RecyclerView.State state, AnchorInfo anchorInfo) {
        RecyclerView.State state2 = state;
        AnchorInfo anchorInfo2 = anchorInfo;
        if (!updateAnchorFromPendingData(state2, anchorInfo2) && !updateAnchorFromChildren(state2, anchorInfo2)) {
            anchorInfo2.assignCoordinateFromPadding();
            anchorInfo2.mPosition = 0;
        }
    }

    private boolean updateAnchorFromChildren(RecyclerView.State state, AnchorInfo anchorInfo) {
        RecyclerView.State state2 = state;
        AnchorInfo anchorInfo2 = anchorInfo;
        anchorInfo2.mPosition = this.mLastLayoutFromEnd ? findLastReferenceChildPosition(state2.getItemCount()) : findFirstReferenceChildPosition(state2.getItemCount());
        anchorInfo2.mOffset = Integer.MIN_VALUE;
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean updateAnchorFromPendingData(RecyclerView.State state, AnchorInfo anchorInfo) {
        RecyclerView.State state2 = state;
        AnchorInfo anchorInfo2 = anchorInfo;
        if (state2.isPreLayout() || this.mPendingScrollPosition == -1) {
            return false;
        }
        if (this.mPendingScrollPosition < 0 || this.mPendingScrollPosition >= state2.getItemCount()) {
            this.mPendingScrollPosition = -1;
            this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
            return false;
        }
        if (this.mPendingSavedState == null || this.mPendingSavedState.mAnchorPosition == -1 || this.mPendingSavedState.mSpanOffsetsSize < 1) {
            View findViewByPosition = findViewByPosition(this.mPendingScrollPosition);
            if (findViewByPosition != null) {
                anchorInfo2.mPosition = this.mShouldReverseLayout ? getLastChildPosition() : getFirstChildPosition();
                if (this.mPendingScrollPositionOffset != Integer.MIN_VALUE) {
                    if (anchorInfo2.mLayoutFromEnd) {
                        anchorInfo2.mOffset = (this.mPrimaryOrientation.getEndAfterPadding() - this.mPendingScrollPositionOffset) - this.mPrimaryOrientation.getDecoratedEnd(findViewByPosition);
                    } else {
                        anchorInfo2.mOffset = (this.mPrimaryOrientation.getStartAfterPadding() + this.mPendingScrollPositionOffset) - this.mPrimaryOrientation.getDecoratedStart(findViewByPosition);
                    }
                    return true;
                } else if (this.mPrimaryOrientation.getDecoratedMeasurement(findViewByPosition) > this.mPrimaryOrientation.getTotalSpace()) {
                    anchorInfo2.mOffset = anchorInfo2.mLayoutFromEnd ? this.mPrimaryOrientation.getEndAfterPadding() : this.mPrimaryOrientation.getStartAfterPadding();
                    return true;
                } else {
                    int decoratedStart = this.mPrimaryOrientation.getDecoratedStart(findViewByPosition) - this.mPrimaryOrientation.getStartAfterPadding();
                    if (decoratedStart < 0) {
                        anchorInfo2.mOffset = -decoratedStart;
                        return true;
                    }
                    int endAfterPadding = this.mPrimaryOrientation.getEndAfterPadding() - this.mPrimaryOrientation.getDecoratedEnd(findViewByPosition);
                    if (endAfterPadding < 0) {
                        anchorInfo2.mOffset = endAfterPadding;
                        return true;
                    }
                    anchorInfo2.mOffset = Integer.MIN_VALUE;
                }
            } else {
                anchorInfo2.mPosition = this.mPendingScrollPosition;
                if (this.mPendingScrollPositionOffset == Integer.MIN_VALUE) {
                    int calculateScrollDirectionForPosition = calculateScrollDirectionForPosition(anchorInfo2.mPosition);
                    anchorInfo2.mLayoutFromEnd = calculateScrollDirectionForPosition == 1;
                    anchorInfo2.assignCoordinateFromPadding();
                } else {
                    anchorInfo2.assignCoordinateFromPadding(this.mPendingScrollPositionOffset);
                }
                anchorInfo2.mInvalidateOffsets = true;
            }
        } else {
            anchorInfo2.mOffset = Integer.MIN_VALUE;
            anchorInfo2.mPosition = this.mPendingScrollPosition;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void updateMeasureSpecs() {
        this.mSizePerSpan = this.mSecondaryOrientation.getTotalSpace() / this.mSpanCount;
        this.mFullSizeSpec = View.MeasureSpec.makeMeasureSpec(this.mSecondaryOrientation.getTotalSpace(), 1073741824);
        if (this.mOrientation == 1) {
            this.mWidthSpec = View.MeasureSpec.makeMeasureSpec(this.mSizePerSpan, 1073741824);
            this.mHeightSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
            return;
        }
        this.mHeightSpec = View.MeasureSpec.makeMeasureSpec(this.mSizePerSpan, 1073741824);
        this.mWidthSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
    }

    public boolean supportsPredictiveItemAnimations() {
        return this.mPendingSavedState == null;
    }

    public int[] findFirstVisibleItemPositions(int[] iArr) {
        Throwable th;
        StringBuilder sb;
        int[] iArr2 = iArr;
        if (iArr2 == null) {
            iArr2 = new int[this.mSpanCount];
        } else if (iArr2.length < this.mSpanCount) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("Provided int[]'s size must be more than or equal to span count. Expected:").append(this.mSpanCount).append(", array size:").append(iArr2.length).toString());
            throw th2;
        }
        for (int i = 0; i < this.mSpanCount; i++) {
            iArr2[i] = this.mSpans[i].findFirstVisibleItemPosition();
        }
        return iArr2;
    }

    public int[] findFirstCompletelyVisibleItemPositions(int[] iArr) {
        Throwable th;
        StringBuilder sb;
        int[] iArr2 = iArr;
        if (iArr2 == null) {
            iArr2 = new int[this.mSpanCount];
        } else if (iArr2.length < this.mSpanCount) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("Provided int[]'s size must be more than or equal to span count. Expected:").append(this.mSpanCount).append(", array size:").append(iArr2.length).toString());
            throw th2;
        }
        for (int i = 0; i < this.mSpanCount; i++) {
            iArr2[i] = this.mSpans[i].findFirstCompletelyVisibleItemPosition();
        }
        return iArr2;
    }

    public int[] findLastVisibleItemPositions(int[] iArr) {
        Throwable th;
        StringBuilder sb;
        int[] iArr2 = iArr;
        if (iArr2 == null) {
            iArr2 = new int[this.mSpanCount];
        } else if (iArr2.length < this.mSpanCount) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("Provided int[]'s size must be more than or equal to span count. Expected:").append(this.mSpanCount).append(", array size:").append(iArr2.length).toString());
            throw th2;
        }
        for (int i = 0; i < this.mSpanCount; i++) {
            iArr2[i] = this.mSpans[i].findLastVisibleItemPosition();
        }
        return iArr2;
    }

    public int[] findLastCompletelyVisibleItemPositions(int[] iArr) {
        Throwable th;
        StringBuilder sb;
        int[] iArr2 = iArr;
        if (iArr2 == null) {
            iArr2 = new int[this.mSpanCount];
        } else if (iArr2.length < this.mSpanCount) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("Provided int[]'s size must be more than or equal to span count. Expected:").append(this.mSpanCount).append(", array size:").append(iArr2.length).toString());
            throw th2;
        }
        for (int i = 0; i < this.mSpanCount; i++) {
            iArr2[i] = this.mSpans[i].findLastCompletelyVisibleItemPosition();
        }
        return iArr2;
    }

    public int computeHorizontalScrollOffset(RecyclerView.State state) {
        return computeScrollOffset(state);
    }

    private int computeScrollOffset(RecyclerView.State state) {
        RecyclerView.State state2 = state;
        if (getChildCount() == 0) {
            return 0;
        }
        ensureOrientationHelper();
        return ScrollbarHelper.computeScrollOffset(state2, this.mPrimaryOrientation, findFirstVisibleItemClosestToStart(!this.mSmoothScrollbarEnabled, true), findFirstVisibleItemClosestToEnd(!this.mSmoothScrollbarEnabled, true), this, this.mSmoothScrollbarEnabled, this.mShouldReverseLayout);
    }

    public int computeVerticalScrollOffset(RecyclerView.State state) {
        return computeScrollOffset(state);
    }

    public int computeHorizontalScrollExtent(RecyclerView.State state) {
        return computeScrollExtent(state);
    }

    private int computeScrollExtent(RecyclerView.State state) {
        RecyclerView.State state2 = state;
        if (getChildCount() == 0) {
            return 0;
        }
        ensureOrientationHelper();
        return ScrollbarHelper.computeScrollExtent(state2, this.mPrimaryOrientation, findFirstVisibleItemClosestToStart(!this.mSmoothScrollbarEnabled, true), findFirstVisibleItemClosestToEnd(!this.mSmoothScrollbarEnabled, true), this, this.mSmoothScrollbarEnabled);
    }

    public int computeVerticalScrollExtent(RecyclerView.State state) {
        return computeScrollExtent(state);
    }

    public int computeHorizontalScrollRange(RecyclerView.State state) {
        return computeScrollRange(state);
    }

    private int computeScrollRange(RecyclerView.State state) {
        RecyclerView.State state2 = state;
        if (getChildCount() == 0) {
            return 0;
        }
        ensureOrientationHelper();
        return ScrollbarHelper.computeScrollRange(state2, this.mPrimaryOrientation, findFirstVisibleItemClosestToStart(!this.mSmoothScrollbarEnabled, true), findFirstVisibleItemClosestToEnd(!this.mSmoothScrollbarEnabled, true), this, this.mSmoothScrollbarEnabled);
    }

    public int computeVerticalScrollRange(RecyclerView.State state) {
        return computeScrollRange(state);
    }

    private void measureChildWithDecorationsAndMargin(View view, LayoutParams layoutParams) {
        View view2 = view;
        LayoutParams layoutParams2 = layoutParams;
        if (layoutParams2.mFullSpan) {
            if (this.mOrientation == 1) {
                measureChildWithDecorationsAndMargin(view2, this.mFullSizeSpec, getSpecForDimension(layoutParams2.height, this.mHeightSpec));
            } else {
                measureChildWithDecorationsAndMargin(view2, getSpecForDimension(layoutParams2.width, this.mWidthSpec), this.mFullSizeSpec);
            }
        } else if (this.mOrientation == 1) {
            measureChildWithDecorationsAndMargin(view2, this.mWidthSpec, getSpecForDimension(layoutParams2.height, this.mHeightSpec));
        } else {
            measureChildWithDecorationsAndMargin(view2, getSpecForDimension(layoutParams2.width, this.mWidthSpec), this.mHeightSpec);
        }
    }

    private int getSpecForDimension(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (i3 < 0) {
            return i4;
        }
        return View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
    }

    private void measureChildWithDecorationsAndMargin(View view, int i, int i2) {
        View view2 = view;
        calculateItemDecorationsForChild(view2, this.mTmpRect);
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        view2.measure(updateSpecWithExtra(i, layoutParams.leftMargin + this.mTmpRect.left, layoutParams.rightMargin + this.mTmpRect.right), updateSpecWithExtra(i2, layoutParams.topMargin + this.mTmpRect.top, layoutParams.bottomMargin + this.mTmpRect.bottom));
    }

    private int updateSpecWithExtra(int i, int i2, int i3) {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        if (i5 == 0 && i6 == 0) {
            return i4;
        }
        int mode = View.MeasureSpec.getMode(i4);
        if (mode == Integer.MIN_VALUE || mode == 1073741824) {
            return View.MeasureSpec.makeMeasureSpec(Math.max(0, (View.MeasureSpec.getSize(i4) - i5) - i6), mode);
        }
        return i4;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2 = parcelable;
        if (parcelable2 instanceof SavedState) {
            this.mPendingSavedState = (SavedState) parcelable2;
            requestLayout();
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState;
        int firstChildPosition;
        int startLine;
        Parcelable parcelable;
        if (this.mPendingSavedState != null) {
            new SavedState(this.mPendingSavedState);
            return parcelable;
        }
        new SavedState();
        SavedState savedState2 = savedState;
        savedState2.mReverseLayout = this.mReverseLayout;
        savedState2.mAnchorLayoutFromEnd = this.mLastLayoutFromEnd;
        savedState2.mLastLayoutRTL = this.mLastLayoutRTL;
        if (this.mLazySpanLookup == null || this.mLazySpanLookup.mData == null) {
            savedState2.mSpanLookupSize = 0;
        } else {
            savedState2.mSpanLookup = this.mLazySpanLookup.mData;
            savedState2.mSpanLookupSize = savedState2.mSpanLookup.length;
            savedState2.mFullSpanItems = this.mLazySpanLookup.mFullSpanItems;
        }
        if (getChildCount() > 0) {
            ensureOrientationHelper();
            SavedState savedState3 = savedState2;
            if (this.mLastLayoutFromEnd) {
                firstChildPosition = getLastChildPosition();
            } else {
                firstChildPosition = getFirstChildPosition();
            }
            savedState3.mAnchorPosition = firstChildPosition;
            savedState2.mVisibleAnchorPosition = findFirstVisibleItemPositionInt();
            savedState2.mSpanOffsetsSize = this.mSpanCount;
            savedState2.mSpanOffsets = new int[this.mSpanCount];
            for (int i = 0; i < this.mSpanCount; i++) {
                if (this.mLastLayoutFromEnd) {
                    startLine = this.mSpans[i].getEndLine(Integer.MIN_VALUE);
                    if (startLine != Integer.MIN_VALUE) {
                        startLine -= this.mPrimaryOrientation.getEndAfterPadding();
                    }
                } else {
                    startLine = this.mSpans[i].getStartLine(Integer.MIN_VALUE);
                    if (startLine != Integer.MIN_VALUE) {
                        startLine -= this.mPrimaryOrientation.getStartAfterPadding();
                    }
                }
                savedState2.mSpanOffsets[i] = startLine;
            }
        } else {
            savedState2.mAnchorPosition = -1;
            savedState2.mVisibleAnchorPosition = -1;
            savedState2.mSpanOffsetsSize = 0;
        }
        return savedState2;
    }

    public void onInitializeAccessibilityNodeInfoForItem(RecyclerView.Recycler recycler, RecyclerView.State state, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        View view2 = view;
        AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
        ViewGroup.LayoutParams layoutParams = view2.getLayoutParams();
        if (!(layoutParams instanceof LayoutParams)) {
            super.onInitializeAccessibilityNodeInfoForItem(view2, accessibilityNodeInfoCompat2);
            return;
        }
        LayoutParams layoutParams2 = (LayoutParams) layoutParams;
        if (this.mOrientation == 0) {
            accessibilityNodeInfoCompat2.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(layoutParams2.getSpanIndex(), layoutParams2.mFullSpan ? this.mSpanCount : 1, -1, -1, layoutParams2.mFullSpan, false));
        } else {
            accessibilityNodeInfoCompat2.setCollectionItemInfo(AccessibilityNodeInfoCompat.CollectionItemInfoCompat.obtain(-1, -1, layoutParams2.getSpanIndex(), layoutParams2.mFullSpan ? this.mSpanCount : 1, layoutParams2.mFullSpan, false));
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
        super.onInitializeAccessibilityEvent(accessibilityEvent2);
        if (getChildCount() > 0) {
            AccessibilityRecordCompat asRecord = AccessibilityEventCompat.asRecord(accessibilityEvent2);
            View findFirstVisibleItemClosestToStart = findFirstVisibleItemClosestToStart(false, true);
            View findFirstVisibleItemClosestToEnd = findFirstVisibleItemClosestToEnd(false, true);
            if (findFirstVisibleItemClosestToStart != null && findFirstVisibleItemClosestToEnd != null) {
                int position = getPosition(findFirstVisibleItemClosestToStart);
                int position2 = getPosition(findFirstVisibleItemClosestToEnd);
                if (position < position2) {
                    asRecord.setFromIndex(position);
                    asRecord.setToIndex(position2);
                    return;
                }
                asRecord.setFromIndex(position2);
                asRecord.setToIndex(position);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int findFirstVisibleItemPositionInt() {
        View findFirstVisibleItemClosestToEnd = this.mShouldReverseLayout ? findFirstVisibleItemClosestToEnd(true, true) : findFirstVisibleItemClosestToStart(true, true);
        return findFirstVisibleItemClosestToEnd == null ? -1 : getPosition(findFirstVisibleItemClosestToEnd);
    }

    public int getRowCountForAccessibility(RecyclerView.Recycler recycler, RecyclerView.State state) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        if (this.mOrientation == 0) {
            return this.mSpanCount;
        }
        return super.getRowCountForAccessibility(recycler2, state2);
    }

    public int getColumnCountForAccessibility(RecyclerView.Recycler recycler, RecyclerView.State state) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        if (this.mOrientation == 1) {
            return this.mSpanCount;
        }
        return super.getColumnCountForAccessibility(recycler2, state2);
    }

    /* access modifiers changed from: package-private */
    public View findFirstVisibleItemClosestToStart(boolean z, boolean z2) {
        boolean z3 = z;
        boolean z4 = z2;
        ensureOrientationHelper();
        int startAfterPadding = this.mPrimaryOrientation.getStartAfterPadding();
        int endAfterPadding = this.mPrimaryOrientation.getEndAfterPadding();
        int childCount = getChildCount();
        View view = null;
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            int decoratedStart = this.mPrimaryOrientation.getDecoratedStart(childAt);
            if (this.mPrimaryOrientation.getDecoratedEnd(childAt) > startAfterPadding && decoratedStart < endAfterPadding) {
                if (decoratedStart >= startAfterPadding || !z3) {
                    return childAt;
                }
                if (z4 && view == null) {
                    view = childAt;
                }
            }
        }
        return view;
    }

    /* access modifiers changed from: package-private */
    public View findFirstVisibleItemClosestToEnd(boolean z, boolean z2) {
        boolean z3 = z;
        boolean z4 = z2;
        ensureOrientationHelper();
        int startAfterPadding = this.mPrimaryOrientation.getStartAfterPadding();
        int endAfterPadding = this.mPrimaryOrientation.getEndAfterPadding();
        View view = null;
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            int decoratedStart = this.mPrimaryOrientation.getDecoratedStart(childAt);
            int decoratedEnd = this.mPrimaryOrientation.getDecoratedEnd(childAt);
            if (decoratedEnd > startAfterPadding && decoratedStart < endAfterPadding) {
                if (decoratedEnd <= endAfterPadding || !z3) {
                    return childAt;
                }
                if (z4 && view == null) {
                    view = childAt;
                }
            }
        }
        return view;
    }

    private void fixEndGap(RecyclerView.Recycler recycler, RecyclerView.State state, boolean z) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        boolean z2 = z;
        int endAfterPadding = this.mPrimaryOrientation.getEndAfterPadding() - getMaxEnd(this.mPrimaryOrientation.getEndAfterPadding());
        if (endAfterPadding > 0) {
            int i = endAfterPadding - (-scrollBy(-endAfterPadding, recycler2, state2));
            if (z2 && i > 0) {
                this.mPrimaryOrientation.offsetChildren(i);
            }
        }
    }

    private void fixStartGap(RecyclerView.Recycler recycler, RecyclerView.State state, boolean z) {
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        boolean z2 = z;
        int minStart = getMinStart(this.mPrimaryOrientation.getStartAfterPadding()) - this.mPrimaryOrientation.getStartAfterPadding();
        if (minStart > 0) {
            int scrollBy = minStart - scrollBy(minStart, recycler2, state2);
            if (z2 && scrollBy > 0) {
                this.mPrimaryOrientation.offsetChildren(-scrollBy);
            }
        }
    }

    private void updateLayoutState(int i, RecyclerView.State state) {
        int targetScrollPosition;
        int i2 = i;
        RecyclerView.State state2 = state;
        this.mLayoutState.mAvailable = 0;
        this.mLayoutState.mCurrentPosition = i2;
        int i3 = 0;
        int i4 = 0;
        if (isSmoothScrolling() && (targetScrollPosition = state2.getTargetScrollPosition()) != -1) {
            if (this.mShouldReverseLayout == (targetScrollPosition < i2)) {
                i4 = this.mPrimaryOrientation.getTotalSpace();
            } else {
                i3 = this.mPrimaryOrientation.getTotalSpace();
            }
        }
        if (getClipToPadding()) {
            this.mLayoutState.mStartLine = this.mPrimaryOrientation.getStartAfterPadding() - i3;
            this.mLayoutState.mEndLine = this.mPrimaryOrientation.getEndAfterPadding() + i4;
            return;
        }
        this.mLayoutState.mEndLine = this.mPrimaryOrientation.getEnd() + i4;
        this.mLayoutState.mStartLine = -i3;
    }

    private void setLayoutStateDirection(int i) {
        int i2 = i;
        this.mLayoutState.mLayoutDirection = i2;
        this.mLayoutState.mItemDirection = this.mShouldReverseLayout == (i2 == -1) ? 1 : -1;
    }

    public void offsetChildrenHorizontal(int i) {
        int i2 = i;
        super.offsetChildrenHorizontal(i2);
        for (int i3 = 0; i3 < this.mSpanCount; i3++) {
            this.mSpans[i3].onOffset(i2);
        }
    }

    public void offsetChildrenVertical(int i) {
        int i2 = i;
        super.offsetChildrenVertical(i2);
        for (int i3 = 0; i3 < this.mSpanCount; i3++) {
            this.mSpans[i3].onOffset(i2);
        }
    }

    public void onItemsRemoved(RecyclerView recyclerView, int i, int i2) {
        handleUpdate(i, i2, 2);
    }

    public void onItemsAdded(RecyclerView recyclerView, int i, int i2) {
        handleUpdate(i, i2, 1);
    }

    public void onItemsChanged(RecyclerView recyclerView) {
        this.mLazySpanLookup.clear();
        requestLayout();
    }

    public void onItemsMoved(RecyclerView recyclerView, int i, int i2, int i3) {
        handleUpdate(i, i2, 8);
    }

    public void onItemsUpdated(RecyclerView recyclerView, int i, int i2, Object obj) {
        handleUpdate(i, i2, 4);
    }

    private void handleUpdate(int i, int i2, int i3) {
        int i4;
        int i5;
        int lastChildPosition;
        int i6 = i;
        int i7 = i2;
        int i8 = i3;
        int lastChildPosition2 = this.mShouldReverseLayout ? getLastChildPosition() : getFirstChildPosition();
        if (i8 != 8) {
            i4 = i6;
            i5 = i6 + i7;
        } else if (i6 < i7) {
            i5 = i7 + 1;
            i4 = i6;
        } else {
            i5 = i6 + 1;
            i4 = i7;
        }
        int invalidateAfter = this.mLazySpanLookup.invalidateAfter(i4);
        switch (i8) {
            case 1:
                this.mLazySpanLookup.offsetForAddition(i6, i7);
                break;
            case 2:
                this.mLazySpanLookup.offsetForRemoval(i6, i7);
                break;
            case 8:
                this.mLazySpanLookup.offsetForRemoval(i6, 1);
                this.mLazySpanLookup.offsetForAddition(i7, 1);
                break;
        }
        if (i5 > lastChildPosition2) {
            if (this.mShouldReverseLayout) {
                lastChildPosition = getFirstChildPosition();
            } else {
                lastChildPosition = getLastChildPosition();
            }
            if (i4 <= lastChildPosition) {
                requestLayout();
            }
        }
    }

    private int fill(RecyclerView.Recycler recycler, LayoutState layoutState, RecyclerView.State state) {
        int i;
        boolean z;
        int maxEnd;
        Span span;
        int startLine;
        int i2;
        int decoratedMeasurement;
        boolean z2;
        int endLine;
        RecyclerView.Recycler recycler2 = recycler;
        LayoutState layoutState2 = layoutState;
        RecyclerView.State state2 = state;
        this.mRemainingSpans.set(0, this.mSpanCount, true);
        if (layoutState2.mLayoutDirection == 1) {
            i = layoutState2.mEndLine + layoutState2.mAvailable;
        } else {
            i = layoutState2.mStartLine - layoutState2.mAvailable;
        }
        updateAllRemainingSpans(layoutState2.mLayoutDirection, i);
        int endAfterPadding = this.mShouldReverseLayout ? this.mPrimaryOrientation.getEndAfterPadding() : this.mPrimaryOrientation.getStartAfterPadding();
        boolean z3 = false;
        while (true) {
            z = z3;
            if (layoutState2.hasMore(state2) && !this.mRemainingSpans.isEmpty()) {
                View next = layoutState2.next(recycler2);
                LayoutParams layoutParams = (LayoutParams) next.getLayoutParams();
                int viewLayoutPosition = layoutParams.getViewLayoutPosition();
                int span2 = this.mLazySpanLookup.getSpan(viewLayoutPosition);
                boolean z4 = span2 == -1;
                if (z4) {
                    span = layoutParams.mFullSpan ? this.mSpans[0] : getNextSpan(layoutState2);
                    this.mLazySpanLookup.setSpan(viewLayoutPosition, span);
                } else {
                    span = this.mSpans[span2];
                }
                layoutParams.mSpan = span;
                if (layoutState2.mLayoutDirection == 1) {
                    addView(next);
                } else {
                    addView(next, 0);
                }
                measureChildWithDecorationsAndMargin(next, layoutParams);
                if (layoutState2.mLayoutDirection == 1) {
                    if (layoutParams.mFullSpan) {
                        endLine = getMaxEnd(endAfterPadding);
                    } else {
                        endLine = span.getEndLine(endAfterPadding);
                    }
                    decoratedMeasurement = endLine;
                    i2 = decoratedMeasurement + this.mPrimaryOrientation.getDecoratedMeasurement(next);
                    if (z4 && layoutParams.mFullSpan) {
                        LazySpanLookup.FullSpanItem createFullSpanItemFromEnd = createFullSpanItemFromEnd(decoratedMeasurement);
                        createFullSpanItemFromEnd.mGapDir = -1;
                        createFullSpanItemFromEnd.mPosition = viewLayoutPosition;
                        this.mLazySpanLookup.addFullSpanItem(createFullSpanItemFromEnd);
                    }
                } else {
                    if (layoutParams.mFullSpan) {
                        startLine = getMinStart(endAfterPadding);
                    } else {
                        startLine = span.getStartLine(endAfterPadding);
                    }
                    i2 = startLine;
                    decoratedMeasurement = i2 - this.mPrimaryOrientation.getDecoratedMeasurement(next);
                    if (z4 && layoutParams.mFullSpan) {
                        LazySpanLookup.FullSpanItem createFullSpanItemFromStart = createFullSpanItemFromStart(i2);
                        createFullSpanItemFromStart.mGapDir = 1;
                        createFullSpanItemFromStart.mPosition = viewLayoutPosition;
                        this.mLazySpanLookup.addFullSpanItem(createFullSpanItemFromStart);
                    }
                }
                if (layoutParams.mFullSpan && layoutState2.mItemDirection == -1) {
                    if (z4) {
                        this.mLaidOutInvalidFullSpan = true;
                    } else {
                        if (layoutState2.mLayoutDirection == 1) {
                            z2 = !areAllEndsEqual();
                        } else {
                            z2 = !areAllStartsEqual();
                        }
                        if (z2) {
                            LazySpanLookup.FullSpanItem fullSpanItem = this.mLazySpanLookup.getFullSpanItem(viewLayoutPosition);
                            if (fullSpanItem != null) {
                                fullSpanItem.mHasUnwantedGapAfter = true;
                            }
                            this.mLaidOutInvalidFullSpan = true;
                        }
                    }
                }
                attachViewToSpans(next, layoutParams, layoutState2);
                int startAfterPadding = layoutParams.mFullSpan ? this.mSecondaryOrientation.getStartAfterPadding() : (span.mIndex * this.mSizePerSpan) + this.mSecondaryOrientation.getStartAfterPadding();
                int decoratedMeasurement2 = startAfterPadding + this.mSecondaryOrientation.getDecoratedMeasurement(next);
                if (this.mOrientation == 1) {
                    layoutDecoratedWithMargins(next, startAfterPadding, decoratedMeasurement, decoratedMeasurement2, i2);
                } else {
                    layoutDecoratedWithMargins(next, decoratedMeasurement, startAfterPadding, i2, decoratedMeasurement2);
                }
                if (layoutParams.mFullSpan) {
                    updateAllRemainingSpans(this.mLayoutState.mLayoutDirection, i);
                } else {
                    updateRemainingSpans(span, this.mLayoutState.mLayoutDirection, i);
                }
                recycle(recycler2, this.mLayoutState);
                z3 = true;
            }
        }
        if (!z) {
            recycle(recycler2, this.mLayoutState);
        }
        if (this.mLayoutState.mLayoutDirection == -1) {
            maxEnd = this.mPrimaryOrientation.getStartAfterPadding() - getMinStart(this.mPrimaryOrientation.getStartAfterPadding());
        } else {
            maxEnd = getMaxEnd(this.mPrimaryOrientation.getEndAfterPadding()) - this.mPrimaryOrientation.getEndAfterPadding();
        }
        return maxEnd > 0 ? Math.min(layoutState2.mAvailable, maxEnd) : 0;
    }

    private LazySpanLookup.FullSpanItem createFullSpanItemFromEnd(int i) {
        LazySpanLookup.FullSpanItem fullSpanItem;
        int i2 = i;
        new LazySpanLookup.FullSpanItem();
        LazySpanLookup.FullSpanItem fullSpanItem2 = fullSpanItem;
        fullSpanItem2.mGapPerSpan = new int[this.mSpanCount];
        for (int i3 = 0; i3 < this.mSpanCount; i3++) {
            fullSpanItem2.mGapPerSpan[i3] = i2 - this.mSpans[i3].getEndLine(i2);
        }
        return fullSpanItem2;
    }

    private LazySpanLookup.FullSpanItem createFullSpanItemFromStart(int i) {
        LazySpanLookup.FullSpanItem fullSpanItem;
        int i2 = i;
        new LazySpanLookup.FullSpanItem();
        LazySpanLookup.FullSpanItem fullSpanItem2 = fullSpanItem;
        fullSpanItem2.mGapPerSpan = new int[this.mSpanCount];
        for (int i3 = 0; i3 < this.mSpanCount; i3++) {
            fullSpanItem2.mGapPerSpan[i3] = this.mSpans[i3].getStartLine(i2) - i2;
        }
        return fullSpanItem2;
    }

    private void attachViewToSpans(View view, LayoutParams layoutParams, LayoutState layoutState) {
        View view2 = view;
        LayoutParams layoutParams2 = layoutParams;
        if (layoutState.mLayoutDirection == 1) {
            if (layoutParams2.mFullSpan) {
                appendViewToAllSpans(view2);
            } else {
                layoutParams2.mSpan.appendToSpan(view2);
            }
        } else if (layoutParams2.mFullSpan) {
            prependViewToAllSpans(view2);
        } else {
            layoutParams2.mSpan.prependToSpan(view2);
        }
    }

    private void recycle(RecyclerView.Recycler recycler, LayoutState layoutState) {
        int min;
        int min2;
        RecyclerView.Recycler recycler2 = recycler;
        LayoutState layoutState2 = layoutState;
        if (layoutState2.mAvailable == 0) {
            if (layoutState2.mLayoutDirection == -1) {
                recycleFromEnd(recycler2, layoutState2.mEndLine);
            } else {
                recycleFromStart(recycler2, layoutState2.mStartLine);
            }
        } else if (layoutState2.mLayoutDirection == -1) {
            int maxStart = layoutState2.mStartLine - getMaxStart(layoutState2.mStartLine);
            if (maxStart < 0) {
                min2 = layoutState2.mEndLine;
            } else {
                min2 = layoutState2.mEndLine - Math.min(maxStart, layoutState2.mAvailable);
            }
            recycleFromEnd(recycler2, min2);
        } else {
            int minEnd = getMinEnd(layoutState2.mEndLine) - layoutState2.mEndLine;
            if (minEnd < 0) {
                min = layoutState2.mStartLine;
            } else {
                min = layoutState2.mStartLine + Math.min(minEnd, layoutState2.mAvailable);
            }
            recycleFromStart(recycler2, min);
        }
    }

    private void appendViewToAllSpans(View view) {
        View view2 = view;
        for (int i = this.mSpanCount - 1; i >= 0; i--) {
            this.mSpans[i].appendToSpan(view2);
        }
    }

    private void prependViewToAllSpans(View view) {
        View view2 = view;
        for (int i = this.mSpanCount - 1; i >= 0; i--) {
            this.mSpans[i].prependToSpan(view2);
        }
    }

    private void layoutDecoratedWithMargins(View view, int i, int i2, int i3, int i4) {
        View view2 = view;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        layoutDecorated(view2, i + layoutParams.leftMargin, i2 + layoutParams.topMargin, i3 - layoutParams.rightMargin, i4 - layoutParams.bottomMargin);
    }

    private void updateAllRemainingSpans(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        for (int i5 = 0; i5 < this.mSpanCount; i5++) {
            if (!this.mSpans[i5].mViews.isEmpty()) {
                updateRemainingSpans(this.mSpans[i5], i3, i4);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.BitSet.set(int, boolean):void}
     arg types: [int, int]
     candidates:
      ClspMth{java.util.BitSet.set(int, int):void}
      ClspMth{java.util.BitSet.set(int, boolean):void} */
    private void updateRemainingSpans(Span span, int i, int i2) {
        Span span2 = span;
        int i3 = i2;
        int deletedSize = span2.getDeletedSize();
        if (i == -1) {
            if (span2.getStartLine() + deletedSize <= i3) {
                this.mRemainingSpans.set(span2.mIndex, false);
            }
        } else if (span2.getEndLine() - deletedSize >= i3) {
            this.mRemainingSpans.set(span2.mIndex, false);
        }
    }

    private int getMaxStart(int i) {
        int i2 = i;
        int startLine = this.mSpans[0].getStartLine(i2);
        for (int i3 = 1; i3 < this.mSpanCount; i3++) {
            int startLine2 = this.mSpans[i3].getStartLine(i2);
            if (startLine2 > startLine) {
                startLine = startLine2;
            }
        }
        return startLine;
    }

    private int getMinStart(int i) {
        int i2 = i;
        int startLine = this.mSpans[0].getStartLine(i2);
        for (int i3 = 1; i3 < this.mSpanCount; i3++) {
            int startLine2 = this.mSpans[i3].getStartLine(i2);
            if (startLine2 < startLine) {
                startLine = startLine2;
            }
        }
        return startLine;
    }

    /* access modifiers changed from: package-private */
    public boolean areAllEndsEqual() {
        int endLine = this.mSpans[0].getEndLine(Integer.MIN_VALUE);
        for (int i = 1; i < this.mSpanCount; i++) {
            if (this.mSpans[i].getEndLine(Integer.MIN_VALUE) != endLine) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean areAllStartsEqual() {
        int startLine = this.mSpans[0].getStartLine(Integer.MIN_VALUE);
        for (int i = 1; i < this.mSpanCount; i++) {
            if (this.mSpans[i].getStartLine(Integer.MIN_VALUE) != startLine) {
                return false;
            }
        }
        return true;
    }

    private int getMaxEnd(int i) {
        int i2 = i;
        int endLine = this.mSpans[0].getEndLine(i2);
        for (int i3 = 1; i3 < this.mSpanCount; i3++) {
            int endLine2 = this.mSpans[i3].getEndLine(i2);
            if (endLine2 > endLine) {
                endLine = endLine2;
            }
        }
        return endLine;
    }

    private int getMinEnd(int i) {
        int i2 = i;
        int endLine = this.mSpans[0].getEndLine(i2);
        for (int i3 = 1; i3 < this.mSpanCount; i3++) {
            int endLine2 = this.mSpans[i3].getEndLine(i2);
            if (endLine2 < endLine) {
                endLine = endLine2;
            }
        }
        return endLine;
    }

    private void recycleFromStart(RecyclerView.Recycler recycler, int i) {
        RecyclerView.Recycler recycler2 = recycler;
        int i2 = i;
        while (getChildCount() > 0) {
            View childAt = getChildAt(0);
            if (this.mPrimaryOrientation.getDecoratedEnd(childAt) <= i2) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.mFullSpan) {
                    int i3 = 0;
                    while (i3 < this.mSpanCount) {
                        if (this.mSpans[i3].mViews.size() != 1) {
                            i3++;
                        } else {
                            return;
                        }
                    }
                    for (int i4 = 0; i4 < this.mSpanCount; i4++) {
                        this.mSpans[i4].popStart();
                    }
                } else if (layoutParams.mSpan.mViews.size() != 1) {
                    layoutParams.mSpan.popStart();
                } else {
                    return;
                }
                removeAndRecycleView(childAt, recycler2);
            } else {
                return;
            }
        }
    }

    private void recycleFromEnd(RecyclerView.Recycler recycler, int i) {
        RecyclerView.Recycler recycler2 = recycler;
        int i2 = i;
        int childCount = getChildCount() - 1;
        while (childCount >= 0) {
            View childAt = getChildAt(childCount);
            if (this.mPrimaryOrientation.getDecoratedStart(childAt) >= i2) {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.mFullSpan) {
                    int i3 = 0;
                    while (i3 < this.mSpanCount) {
                        if (this.mSpans[i3].mViews.size() != 1) {
                            i3++;
                        } else {
                            return;
                        }
                    }
                    for (int i4 = 0; i4 < this.mSpanCount; i4++) {
                        this.mSpans[i4].popEnd();
                    }
                } else if (layoutParams.mSpan.mViews.size() != 1) {
                    layoutParams.mSpan.popEnd();
                } else {
                    return;
                }
                removeAndRecycleView(childAt, recycler2);
                childCount--;
            } else {
                return;
            }
        }
    }

    private boolean preferLastSpan(int i) {
        int i2 = i;
        if (this.mOrientation == 0) {
            return (i2 == -1) != this.mShouldReverseLayout;
        }
        return ((i2 == -1) == this.mShouldReverseLayout) == isLayoutRTL();
    }

    private Span getNextSpan(LayoutState layoutState) {
        int i;
        int i2;
        int i3;
        LayoutState layoutState2 = layoutState;
        if (preferLastSpan(layoutState2.mLayoutDirection)) {
            i = this.mSpanCount - 1;
            i2 = -1;
            i3 = -1;
        } else {
            i = 0;
            i2 = this.mSpanCount;
            i3 = 1;
        }
        if (layoutState2.mLayoutDirection == 1) {
            Span span = null;
            int i4 = Integer.MAX_VALUE;
            int startAfterPadding = this.mPrimaryOrientation.getStartAfterPadding();
            int i5 = i;
            while (true) {
                int i6 = i5;
                if (i6 == i2) {
                    return span;
                }
                Span span2 = this.mSpans[i6];
                int endLine = span2.getEndLine(startAfterPadding);
                if (endLine < i4) {
                    span = span2;
                    i4 = endLine;
                }
                i5 = i6 + i3;
            }
        } else {
            Span span3 = null;
            int i7 = Integer.MIN_VALUE;
            int endAfterPadding = this.mPrimaryOrientation.getEndAfterPadding();
            int i8 = i;
            while (true) {
                int i9 = i8;
                if (i9 == i2) {
                    return span3;
                }
                Span span4 = this.mSpans[i9];
                int startLine = span4.getStartLine(endAfterPadding);
                if (startLine > i7) {
                    span3 = span4;
                    i7 = startLine;
                }
                i8 = i9 + i3;
            }
        }
    }

    public boolean canScrollVertically() {
        return this.mOrientation == 1;
    }

    public boolean canScrollHorizontally() {
        return this.mOrientation == 0;
    }

    public int scrollHorizontallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        return scrollBy(i, recycler, state);
    }

    public int scrollVerticallyBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        return scrollBy(i, recycler, state);
    }

    /* access modifiers changed from: private */
    public int calculateScrollDirectionForPosition(int i) {
        int i2 = i;
        if (getChildCount() == 0) {
            return this.mShouldReverseLayout ? 1 : -1;
        }
        return (i2 < getFirstChildPosition()) != this.mShouldReverseLayout ? -1 : 1;
    }

    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int i) {
        LinearSmoothScroller linearSmoothScroller;
        new LinearSmoothScroller(recyclerView.getContext()) {
            public PointF computeScrollVectorForPosition(int i) {
                PointF pointF;
                PointF pointF2;
                int access$400 = StaggeredGridLayoutManager.this.calculateScrollDirectionForPosition(i);
                if (access$400 == 0) {
                    return null;
                }
                if (StaggeredGridLayoutManager.this.mOrientation == 0) {
                    new PointF((float) access$400, 0.0f);
                    return pointF2;
                }
                new PointF(0.0f, (float) access$400);
                return pointF;
            }
        };
        LinearSmoothScroller linearSmoothScroller2 = linearSmoothScroller;
        linearSmoothScroller2.setTargetPosition(i);
        startSmoothScroll(linearSmoothScroller2);
    }

    public void scrollToPosition(int i) {
        int i2 = i;
        if (!(this.mPendingSavedState == null || this.mPendingSavedState.mAnchorPosition == i2)) {
            this.mPendingSavedState.invalidateAnchorPositionInfo();
        }
        this.mPendingScrollPosition = i2;
        this.mPendingScrollPositionOffset = Integer.MIN_VALUE;
        requestLayout();
    }

    public void scrollToPositionWithOffset(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (this.mPendingSavedState != null) {
            this.mPendingSavedState.invalidateAnchorPositionInfo();
        }
        this.mPendingScrollPosition = i3;
        this.mPendingScrollPositionOffset = i4;
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public int scrollBy(int i, RecyclerView.Recycler recycler, RecyclerView.State state) {
        int i2;
        int firstChildPosition;
        int i3;
        int i4 = i;
        RecyclerView.Recycler recycler2 = recycler;
        RecyclerView.State state2 = state;
        ensureOrientationHelper();
        if (i4 > 0) {
            i2 = 1;
            firstChildPosition = getLastChildPosition();
        } else {
            i2 = -1;
            firstChildPosition = getFirstChildPosition();
        }
        updateLayoutState(firstChildPosition, state2);
        setLayoutStateDirection(i2);
        this.mLayoutState.mCurrentPosition = firstChildPosition + this.mLayoutState.mItemDirection;
        int abs = Math.abs(i4);
        this.mLayoutState.mAvailable = abs;
        int fill = fill(recycler2, this.mLayoutState, state2);
        if (abs < fill) {
            i3 = i4;
        } else if (i4 < 0) {
            i3 = -fill;
        } else {
            i3 = fill;
        }
        this.mPrimaryOrientation.offsetChildren(-i3);
        this.mLastLayoutFromEnd = this.mShouldReverseLayout;
        return i3;
    }

    private int getLastChildPosition() {
        int childCount = getChildCount();
        return childCount == 0 ? 0 : getPosition(getChildAt(childCount - 1));
    }

    private int getFirstChildPosition() {
        return getChildCount() == 0 ? 0 : getPosition(getChildAt(0));
    }

    private int findFirstReferenceChildPosition(int i) {
        int i2 = i;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            int position = getPosition(getChildAt(i3));
            if (position >= 0 && position < i2) {
                return position;
            }
        }
        return 0;
    }

    private int findLastReferenceChildPosition(int i) {
        int i2 = i;
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            int position = getPosition(getChildAt(childCount));
            if (position >= 0 && position < i2) {
                return position;
            }
        }
        return 0;
    }

    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        RecyclerView.LayoutParams layoutParams;
        new LayoutParams(-2, -2);
        return layoutParams;
    }

    public RecyclerView.LayoutParams generateLayoutParams(Context context, AttributeSet attributeSet) {
        RecyclerView.LayoutParams layoutParams;
        new LayoutParams(context, attributeSet);
        return layoutParams;
    }

    public RecyclerView.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        RecyclerView.LayoutParams layoutParams2;
        RecyclerView.LayoutParams layoutParams3;
        ViewGroup.LayoutParams layoutParams4 = layoutParams;
        if (layoutParams4 instanceof ViewGroup.MarginLayoutParams) {
            new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams4);
            return layoutParams3;
        }
        new LayoutParams(layoutParams4);
        return layoutParams2;
    }

    public boolean checkLayoutParams(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public int getOrientation() {
        return this.mOrientation;
    }

    public static class LayoutParams extends RecyclerView.LayoutParams {
        public static final int INVALID_SPAN_ID = -1;
        boolean mFullSpan;
        Span mSpan;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(RecyclerView.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public void setFullSpan(boolean z) {
            this.mFullSpan = z;
        }

        public boolean isFullSpan() {
            return this.mFullSpan;
        }

        public final int getSpanIndex() {
            if (this.mSpan == null) {
                return -1;
            }
            return this.mSpan.mIndex;
        }
    }

    class Span {
        static final int INVALID_LINE = Integer.MIN_VALUE;
        int mCachedEnd;
        int mCachedStart;
        int mDeletedSize;
        final int mIndex;
        /* access modifiers changed from: private */
        public ArrayList<View> mViews;

        private Span(int i) {
            ArrayList<View> arrayList;
            new ArrayList<>();
            this.mViews = arrayList;
            this.mCachedStart = Integer.MIN_VALUE;
            this.mCachedEnd = Integer.MIN_VALUE;
            this.mDeletedSize = 0;
            this.mIndex = i;
        }

        /* access modifiers changed from: package-private */
        public int getStartLine(int i) {
            int i2 = i;
            if (this.mCachedStart != Integer.MIN_VALUE) {
                return this.mCachedStart;
            }
            if (this.mViews.size() == 0) {
                return i2;
            }
            calculateCachedStart();
            return this.mCachedStart;
        }

        /* access modifiers changed from: package-private */
        public void calculateCachedStart() {
            LazySpanLookup.FullSpanItem fullSpanItem;
            View view = this.mViews.get(0);
            LayoutParams layoutParams = getLayoutParams(view);
            this.mCachedStart = StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedStart(view);
            if (layoutParams.mFullSpan && (fullSpanItem = StaggeredGridLayoutManager.this.mLazySpanLookup.getFullSpanItem(layoutParams.getViewLayoutPosition())) != null && fullSpanItem.mGapDir == -1) {
                this.mCachedStart = this.mCachedStart - fullSpanItem.getGapForSpan(this.mIndex);
            }
        }

        /* access modifiers changed from: package-private */
        public int getStartLine() {
            if (this.mCachedStart != Integer.MIN_VALUE) {
                return this.mCachedStart;
            }
            calculateCachedStart();
            return this.mCachedStart;
        }

        /* access modifiers changed from: package-private */
        public int getEndLine(int i) {
            int i2 = i;
            if (this.mCachedEnd != Integer.MIN_VALUE) {
                return this.mCachedEnd;
            }
            if (this.mViews.size() == 0) {
                return i2;
            }
            calculateCachedEnd();
            return this.mCachedEnd;
        }

        /* access modifiers changed from: package-private */
        public void calculateCachedEnd() {
            LazySpanLookup.FullSpanItem fullSpanItem;
            View view = this.mViews.get(this.mViews.size() - 1);
            LayoutParams layoutParams = getLayoutParams(view);
            this.mCachedEnd = StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedEnd(view);
            if (layoutParams.mFullSpan && (fullSpanItem = StaggeredGridLayoutManager.this.mLazySpanLookup.getFullSpanItem(layoutParams.getViewLayoutPosition())) != null && fullSpanItem.mGapDir == 1) {
                this.mCachedEnd = this.mCachedEnd + fullSpanItem.getGapForSpan(this.mIndex);
            }
        }

        /* access modifiers changed from: package-private */
        public int getEndLine() {
            if (this.mCachedEnd != Integer.MIN_VALUE) {
                return this.mCachedEnd;
            }
            calculateCachedEnd();
            return this.mCachedEnd;
        }

        /* access modifiers changed from: package-private */
        public void prependToSpan(View view) {
            View view2 = view;
            LayoutParams layoutParams = getLayoutParams(view2);
            layoutParams.mSpan = this;
            this.mViews.add(0, view2);
            this.mCachedStart = Integer.MIN_VALUE;
            if (this.mViews.size() == 1) {
                this.mCachedEnd = Integer.MIN_VALUE;
            }
            if (layoutParams.isItemRemoved() || layoutParams.isItemChanged()) {
                this.mDeletedSize = this.mDeletedSize + StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedMeasurement(view2);
            }
        }

        /* access modifiers changed from: package-private */
        public void appendToSpan(View view) {
            View view2 = view;
            LayoutParams layoutParams = getLayoutParams(view2);
            layoutParams.mSpan = this;
            boolean add = this.mViews.add(view2);
            this.mCachedEnd = Integer.MIN_VALUE;
            if (this.mViews.size() == 1) {
                this.mCachedStart = Integer.MIN_VALUE;
            }
            if (layoutParams.isItemRemoved() || layoutParams.isItemChanged()) {
                this.mDeletedSize = this.mDeletedSize + StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedMeasurement(view2);
            }
        }

        /* access modifiers changed from: package-private */
        public void cacheReferenceLineAndClear(boolean z, int i) {
            int startLine;
            boolean z2 = z;
            int i2 = i;
            if (z2) {
                startLine = getEndLine(Integer.MIN_VALUE);
            } else {
                startLine = getStartLine(Integer.MIN_VALUE);
            }
            clear();
            if (startLine != Integer.MIN_VALUE) {
                if (z2 && startLine < StaggeredGridLayoutManager.this.mPrimaryOrientation.getEndAfterPadding()) {
                    return;
                }
                if (z2 || startLine <= StaggeredGridLayoutManager.this.mPrimaryOrientation.getStartAfterPadding()) {
                    if (i2 != Integer.MIN_VALUE) {
                        startLine += i2;
                    }
                    int i3 = startLine;
                    this.mCachedEnd = i3;
                    this.mCachedStart = i3;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void clear() {
            this.mViews.clear();
            invalidateCache();
            this.mDeletedSize = 0;
        }

        /* access modifiers changed from: package-private */
        public void invalidateCache() {
            this.mCachedStart = Integer.MIN_VALUE;
            this.mCachedEnd = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        public void setLine(int i) {
            int i2 = i;
            this.mCachedStart = i2;
            this.mCachedEnd = i2;
        }

        /* access modifiers changed from: package-private */
        public void popEnd() {
            int size = this.mViews.size();
            View remove = this.mViews.remove(size - 1);
            LayoutParams layoutParams = getLayoutParams(remove);
            layoutParams.mSpan = null;
            if (layoutParams.isItemRemoved() || layoutParams.isItemChanged()) {
                this.mDeletedSize = this.mDeletedSize - StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedMeasurement(remove);
            }
            if (size == 1) {
                this.mCachedStart = Integer.MIN_VALUE;
            }
            this.mCachedEnd = Integer.MIN_VALUE;
        }

        /* access modifiers changed from: package-private */
        public void popStart() {
            View remove = this.mViews.remove(0);
            LayoutParams layoutParams = getLayoutParams(remove);
            layoutParams.mSpan = null;
            if (this.mViews.size() == 0) {
                this.mCachedEnd = Integer.MIN_VALUE;
            }
            if (layoutParams.isItemRemoved() || layoutParams.isItemChanged()) {
                this.mDeletedSize = this.mDeletedSize - StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedMeasurement(remove);
            }
            this.mCachedStart = Integer.MIN_VALUE;
        }

        public int getDeletedSize() {
            return this.mDeletedSize;
        }

        /* access modifiers changed from: package-private */
        public LayoutParams getLayoutParams(View view) {
            return (LayoutParams) view.getLayoutParams();
        }

        /* access modifiers changed from: package-private */
        public void onOffset(int i) {
            int i2 = i;
            if (this.mCachedStart != Integer.MIN_VALUE) {
                this.mCachedStart = this.mCachedStart + i2;
            }
            if (this.mCachedEnd != Integer.MIN_VALUE) {
                this.mCachedEnd = this.mCachedEnd + i2;
            }
        }

        /* access modifiers changed from: package-private */
        public int getNormalizedOffset(int i, int i2, int i3) {
            int i4 = i;
            int i5 = i2;
            int i6 = i3;
            if (this.mViews.size() == 0) {
                return 0;
            }
            if (i4 < 0) {
                int endLine = getEndLine() - i6;
                if (endLine <= 0) {
                    return 0;
                }
                return (-i4) > endLine ? -endLine : i4;
            }
            int startLine = i5 - getStartLine();
            if (startLine <= 0) {
                return 0;
            }
            return startLine < i4 ? startLine : i4;
        }

        /* access modifiers changed from: package-private */
        public boolean isEmpty(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            int size = this.mViews.size();
            for (int i5 = 0; i5 < size; i5++) {
                View view = this.mViews.get(i5);
                if (StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedStart(view) < i4 && StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedEnd(view) > i3) {
                    return false;
                }
            }
            return true;
        }

        public int findFirstVisibleItemPosition() {
            return StaggeredGridLayoutManager.this.mReverseLayout ? findOneVisibleChild(this.mViews.size() - 1, -1, false) : findOneVisibleChild(0, this.mViews.size(), false);
        }

        public int findFirstCompletelyVisibleItemPosition() {
            return StaggeredGridLayoutManager.this.mReverseLayout ? findOneVisibleChild(this.mViews.size() - 1, -1, true) : findOneVisibleChild(0, this.mViews.size(), true);
        }

        public int findLastVisibleItemPosition() {
            return StaggeredGridLayoutManager.this.mReverseLayout ? findOneVisibleChild(0, this.mViews.size(), false) : findOneVisibleChild(this.mViews.size() - 1, -1, false);
        }

        public int findLastCompletelyVisibleItemPosition() {
            return StaggeredGridLayoutManager.this.mReverseLayout ? findOneVisibleChild(0, this.mViews.size(), true) : findOneVisibleChild(this.mViews.size() - 1, -1, true);
        }

        /* access modifiers changed from: package-private */
        public int findOneVisibleChild(int i, int i2, boolean z) {
            int i3 = i;
            int i4 = i2;
            boolean z2 = z;
            int startAfterPadding = StaggeredGridLayoutManager.this.mPrimaryOrientation.getStartAfterPadding();
            int endAfterPadding = StaggeredGridLayoutManager.this.mPrimaryOrientation.getEndAfterPadding();
            int i5 = i4 > i3 ? 1 : -1;
            int i6 = i3;
            while (true) {
                int i7 = i6;
                if (i7 == i4) {
                    return -1;
                }
                View view = this.mViews.get(i7);
                int decoratedStart = StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedStart(view);
                int decoratedEnd = StaggeredGridLayoutManager.this.mPrimaryOrientation.getDecoratedEnd(view);
                if (decoratedStart < endAfterPadding && decoratedEnd > startAfterPadding) {
                    if (!z2) {
                        return StaggeredGridLayoutManager.this.getPosition(view);
                    }
                    if (decoratedStart >= startAfterPadding && decoratedEnd <= endAfterPadding) {
                        return StaggeredGridLayoutManager.this.getPosition(view);
                    }
                }
                i6 = i7 + i5;
            }
        }
    }

    static class LazySpanLookup {
        private static final int MIN_SIZE = 10;
        int[] mData;
        List<FullSpanItem> mFullSpanItems;

        LazySpanLookup() {
        }

        /* access modifiers changed from: package-private */
        public int forceInvalidateAfter(int i) {
            int i2 = i;
            if (this.mFullSpanItems != null) {
                for (int size = this.mFullSpanItems.size() - 1; size >= 0; size--) {
                    if (this.mFullSpanItems.get(size).mPosition >= i2) {
                        FullSpanItem remove = this.mFullSpanItems.remove(size);
                    }
                }
            }
            return invalidateAfter(i2);
        }

        /* access modifiers changed from: package-private */
        public int invalidateAfter(int i) {
            int i2 = i;
            if (this.mData == null) {
                return -1;
            }
            if (i2 >= this.mData.length) {
                return -1;
            }
            int invalidateFullSpansAfter = invalidateFullSpansAfter(i2);
            if (invalidateFullSpansAfter == -1) {
                Arrays.fill(this.mData, i2, this.mData.length, -1);
                return this.mData.length;
            }
            Arrays.fill(this.mData, i2, invalidateFullSpansAfter + 1, -1);
            return invalidateFullSpansAfter + 1;
        }

        /* access modifiers changed from: package-private */
        public int getSpan(int i) {
            int i2 = i;
            if (this.mData == null || i2 >= this.mData.length) {
                return -1;
            }
            return this.mData[i2];
        }

        /* access modifiers changed from: package-private */
        public void setSpan(int i, Span span) {
            int i2 = i;
            ensureSize(i2);
            this.mData[i2] = span.mIndex;
        }

        /* access modifiers changed from: package-private */
        public int sizeForPosition(int i) {
            int i2 = i;
            int length = this.mData.length;
            while (true) {
                int i3 = length;
                if (i3 > i2) {
                    return i3;
                }
                length = i3 * 2;
            }
        }

        /* access modifiers changed from: package-private */
        public void ensureSize(int i) {
            int i2 = i;
            if (this.mData == null) {
                this.mData = new int[(Math.max(i2, 10) + 1)];
                Arrays.fill(this.mData, -1);
            } else if (i2 >= this.mData.length) {
                int[] iArr = this.mData;
                this.mData = new int[sizeForPosition(i2)];
                System.arraycopy(iArr, 0, this.mData, 0, iArr.length);
                Arrays.fill(this.mData, iArr.length, this.mData.length, -1);
            }
        }

        /* access modifiers changed from: package-private */
        public void clear() {
            if (this.mData != null) {
                Arrays.fill(this.mData, -1);
            }
            this.mFullSpanItems = null;
        }

        /* access modifiers changed from: package-private */
        public void offsetForRemoval(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (this.mData != null && i3 < this.mData.length) {
                ensureSize(i3 + i4);
                System.arraycopy(this.mData, i3 + i4, this.mData, i3, (this.mData.length - i3) - i4);
                Arrays.fill(this.mData, this.mData.length - i4, this.mData.length, -1);
                offsetFullSpansForRemoval(i3, i4);
            }
        }

        private void offsetFullSpansForRemoval(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (this.mFullSpanItems != null) {
                int i5 = i3 + i4;
                for (int size = this.mFullSpanItems.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.mFullSpanItems.get(size);
                    if (fullSpanItem.mPosition >= i3) {
                        if (fullSpanItem.mPosition < i5) {
                            FullSpanItem remove = this.mFullSpanItems.remove(size);
                        } else {
                            fullSpanItem.mPosition -= i4;
                        }
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void offsetForAddition(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (this.mData != null && i3 < this.mData.length) {
                ensureSize(i3 + i4);
                System.arraycopy(this.mData, i3, this.mData, i3 + i4, (this.mData.length - i3) - i4);
                Arrays.fill(this.mData, i3, i3 + i4, -1);
                offsetFullSpansForAddition(i3, i4);
            }
        }

        private void offsetFullSpansForAddition(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (this.mFullSpanItems != null) {
                for (int size = this.mFullSpanItems.size() - 1; size >= 0; size--) {
                    FullSpanItem fullSpanItem = this.mFullSpanItems.get(size);
                    if (fullSpanItem.mPosition >= i3) {
                        fullSpanItem.mPosition += i4;
                    }
                }
            }
        }

        private int invalidateFullSpansAfter(int i) {
            int i2 = i;
            if (this.mFullSpanItems == null) {
                return -1;
            }
            FullSpanItem fullSpanItem = getFullSpanItem(i2);
            if (fullSpanItem != null) {
                boolean remove = this.mFullSpanItems.remove(fullSpanItem);
            }
            int i3 = -1;
            int size = this.mFullSpanItems.size();
            int i4 = 0;
            while (true) {
                if (i4 >= size) {
                    break;
                } else if (this.mFullSpanItems.get(i4).mPosition >= i2) {
                    i3 = i4;
                    break;
                } else {
                    i4++;
                }
            }
            if (i3 == -1) {
                return -1;
            }
            FullSpanItem fullSpanItem2 = this.mFullSpanItems.get(i3);
            FullSpanItem remove2 = this.mFullSpanItems.remove(i3);
            return fullSpanItem2.mPosition;
        }

        public void addFullSpanItem(FullSpanItem fullSpanItem) {
            List<FullSpanItem> list;
            FullSpanItem fullSpanItem2 = fullSpanItem;
            if (this.mFullSpanItems == null) {
                new ArrayList();
                this.mFullSpanItems = list;
            }
            int size = this.mFullSpanItems.size();
            for (int i = 0; i < size; i++) {
                FullSpanItem fullSpanItem3 = this.mFullSpanItems.get(i);
                if (fullSpanItem3.mPosition == fullSpanItem2.mPosition) {
                    FullSpanItem remove = this.mFullSpanItems.remove(i);
                }
                if (fullSpanItem3.mPosition >= fullSpanItem2.mPosition) {
                    this.mFullSpanItems.add(i, fullSpanItem2);
                    return;
                }
            }
            boolean add = this.mFullSpanItems.add(fullSpanItem2);
        }

        public FullSpanItem getFullSpanItem(int i) {
            int i2 = i;
            if (this.mFullSpanItems == null) {
                return null;
            }
            for (int size = this.mFullSpanItems.size() - 1; size >= 0; size--) {
                FullSpanItem fullSpanItem = this.mFullSpanItems.get(size);
                if (fullSpanItem.mPosition == i2) {
                    return fullSpanItem;
                }
            }
            return null;
        }

        public FullSpanItem getFirstFullSpanItemInRange(int i, int i2, int i3, boolean z) {
            int i4 = i;
            int i5 = i2;
            int i6 = i3;
            boolean z2 = z;
            if (this.mFullSpanItems == null) {
                return null;
            }
            int size = this.mFullSpanItems.size();
            for (int i7 = 0; i7 < size; i7++) {
                FullSpanItem fullSpanItem = this.mFullSpanItems.get(i7);
                if (fullSpanItem.mPosition >= i5) {
                    return null;
                }
                if (fullSpanItem.mPosition >= i4 && (i6 == 0 || fullSpanItem.mGapDir == i6 || (z2 && fullSpanItem.mHasUnwantedGapAfter))) {
                    return fullSpanItem;
                }
            }
            return null;
        }

        static class FullSpanItem implements Parcelable {
            public static final Parcelable.Creator<FullSpanItem> CREATOR;
            int mGapDir;
            int[] mGapPerSpan;
            boolean mHasUnwantedGapAfter;
            int mPosition;

            public FullSpanItem(Parcel parcel) {
                Parcel parcel2 = parcel;
                this.mPosition = parcel2.readInt();
                this.mGapDir = parcel2.readInt();
                this.mHasUnwantedGapAfter = parcel2.readInt() == 1;
                int readInt = parcel2.readInt();
                if (readInt > 0) {
                    this.mGapPerSpan = new int[readInt];
                    parcel2.readIntArray(this.mGapPerSpan);
                }
            }

            public FullSpanItem() {
            }

            /* access modifiers changed from: package-private */
            public int getGapForSpan(int i) {
                return this.mGapPerSpan == null ? 0 : this.mGapPerSpan[i];
            }

            public void invalidateSpanGaps() {
                this.mGapPerSpan = null;
            }

            public int describeContents() {
                return 0;
            }

            public void writeToParcel(Parcel parcel, int i) {
                Parcel parcel2 = parcel;
                parcel2.writeInt(this.mPosition);
                parcel2.writeInt(this.mGapDir);
                parcel2.writeInt(this.mHasUnwantedGapAfter ? 1 : 0);
                if (this.mGapPerSpan == null || this.mGapPerSpan.length <= 0) {
                    parcel2.writeInt(0);
                    return;
                }
                parcel2.writeInt(this.mGapPerSpan.length);
                parcel2.writeIntArray(this.mGapPerSpan);
            }

            public String toString() {
                StringBuilder sb;
                new StringBuilder();
                return sb.append("FullSpanItem{mPosition=").append(this.mPosition).append(", mGapDir=").append(this.mGapDir).append(", mHasUnwantedGapAfter=").append(this.mHasUnwantedGapAfter).append(", mGapPerSpan=").append(Arrays.toString(this.mGapPerSpan)).append('}').toString();
            }

            static {
                Parcelable.Creator<FullSpanItem> creator;
                new Parcelable.Creator<FullSpanItem>() {
                    public FullSpanItem createFromParcel(Parcel parcel) {
                        FullSpanItem fullSpanItem;
                        new FullSpanItem(parcel);
                        return fullSpanItem;
                    }

                    public FullSpanItem[] newArray(int i) {
                        return new FullSpanItem[i];
                    }
                };
                CREATOR = creator;
            }
        }
    }

    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR;
        boolean mAnchorLayoutFromEnd;
        int mAnchorPosition;
        List<LazySpanLookup.FullSpanItem> mFullSpanItems;
        boolean mLastLayoutRTL;
        boolean mReverseLayout;
        int[] mSpanLookup;
        int mSpanLookupSize;
        int[] mSpanOffsets;
        int mSpanOffsetsSize;
        int mVisibleAnchorPosition;

        public SavedState() {
        }

        SavedState(Parcel parcel) {
            Parcel parcel2 = parcel;
            this.mAnchorPosition = parcel2.readInt();
            this.mVisibleAnchorPosition = parcel2.readInt();
            this.mSpanOffsetsSize = parcel2.readInt();
            if (this.mSpanOffsetsSize > 0) {
                this.mSpanOffsets = new int[this.mSpanOffsetsSize];
                parcel2.readIntArray(this.mSpanOffsets);
            }
            this.mSpanLookupSize = parcel2.readInt();
            if (this.mSpanLookupSize > 0) {
                this.mSpanLookup = new int[this.mSpanLookupSize];
                parcel2.readIntArray(this.mSpanLookup);
            }
            this.mReverseLayout = parcel2.readInt() == 1;
            this.mAnchorLayoutFromEnd = parcel2.readInt() == 1;
            this.mLastLayoutRTL = parcel2.readInt() == 1;
            this.mFullSpanItems = parcel2.readArrayList(LazySpanLookup.FullSpanItem.class.getClassLoader());
        }

        public SavedState(SavedState savedState) {
            SavedState savedState2 = savedState;
            this.mSpanOffsetsSize = savedState2.mSpanOffsetsSize;
            this.mAnchorPosition = savedState2.mAnchorPosition;
            this.mVisibleAnchorPosition = savedState2.mVisibleAnchorPosition;
            this.mSpanOffsets = savedState2.mSpanOffsets;
            this.mSpanLookupSize = savedState2.mSpanLookupSize;
            this.mSpanLookup = savedState2.mSpanLookup;
            this.mReverseLayout = savedState2.mReverseLayout;
            this.mAnchorLayoutFromEnd = savedState2.mAnchorLayoutFromEnd;
            this.mLastLayoutRTL = savedState2.mLastLayoutRTL;
            this.mFullSpanItems = savedState2.mFullSpanItems;
        }

        /* access modifiers changed from: package-private */
        public void invalidateSpanInfo() {
            this.mSpanOffsets = null;
            this.mSpanOffsetsSize = 0;
            this.mSpanLookupSize = 0;
            this.mSpanLookup = null;
            this.mFullSpanItems = null;
        }

        /* access modifiers changed from: package-private */
        public void invalidateAnchorPositionInfo() {
            this.mSpanOffsets = null;
            this.mSpanOffsetsSize = 0;
            this.mAnchorPosition = -1;
            this.mVisibleAnchorPosition = -1;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            parcel2.writeInt(this.mAnchorPosition);
            parcel2.writeInt(this.mVisibleAnchorPosition);
            parcel2.writeInt(this.mSpanOffsetsSize);
            if (this.mSpanOffsetsSize > 0) {
                parcel2.writeIntArray(this.mSpanOffsets);
            }
            parcel2.writeInt(this.mSpanLookupSize);
            if (this.mSpanLookupSize > 0) {
                parcel2.writeIntArray(this.mSpanLookup);
            }
            parcel2.writeInt(this.mReverseLayout ? 1 : 0);
            parcel2.writeInt(this.mAnchorLayoutFromEnd ? 1 : 0);
            parcel2.writeInt(this.mLastLayoutRTL ? 1 : 0);
            parcel2.writeList(this.mFullSpanItems);
        }

        static {
            Parcelable.Creator<SavedState> creator;
            new Parcelable.Creator<SavedState>() {
                public SavedState createFromParcel(Parcel parcel) {
                    SavedState savedState;
                    new SavedState(parcel);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = creator;
        }
    }

    private class AnchorInfo {
        boolean mInvalidateOffsets;
        boolean mLayoutFromEnd;
        int mOffset;
        int mPosition;

        private AnchorInfo() {
        }

        /* access modifiers changed from: package-private */
        public void reset() {
            this.mPosition = -1;
            this.mOffset = Integer.MIN_VALUE;
            this.mLayoutFromEnd = false;
            this.mInvalidateOffsets = false;
        }

        /* access modifiers changed from: package-private */
        public void assignCoordinateFromPadding() {
            this.mOffset = this.mLayoutFromEnd ? StaggeredGridLayoutManager.this.mPrimaryOrientation.getEndAfterPadding() : StaggeredGridLayoutManager.this.mPrimaryOrientation.getStartAfterPadding();
        }

        /* access modifiers changed from: package-private */
        public void assignCoordinateFromPadding(int i) {
            int i2 = i;
            if (this.mLayoutFromEnd) {
                this.mOffset = StaggeredGridLayoutManager.this.mPrimaryOrientation.getEndAfterPadding() - i2;
            } else {
                this.mOffset = StaggeredGridLayoutManager.this.mPrimaryOrientation.getStartAfterPadding() + i2;
            }
        }
    }
}
