package android.support.v7.widget;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ResourceCursorAdapter;
import android.support.v7.appcompat.R;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.WeakHashMap;

class SuggestionsAdapter extends ResourceCursorAdapter implements View.OnClickListener {
    private static final boolean DBG = false;
    static final int INVALID_INDEX = -1;
    private static final String LOG_TAG = "SuggestionsAdapter";
    private static final int QUERY_LIMIT = 50;
    static final int REFINE_ALL = 2;
    static final int REFINE_BY_ENTRY = 1;
    static final int REFINE_NONE = 0;
    private boolean mClosed = false;
    private final int mCommitIconResId;
    private int mFlagsCol = -1;
    private int mIconName1Col = -1;
    private int mIconName2Col = -1;
    private final WeakHashMap<String, Drawable.ConstantState> mOutsideDrawablesCache;
    private final Context mProviderContext;
    private int mQueryRefinement = 1;
    private final SearchManager mSearchManager = ((SearchManager) this.mContext.getSystemService("search"));
    private final SearchView mSearchView;
    private final SearchableInfo mSearchable;
    private int mText1Col = -1;
    private int mText2Col = -1;
    private int mText2UrlCol = -1;
    private ColorStateList mUrlColor;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.ResourceCursorAdapter.<init>(android.content.Context, int, android.database.Cursor, boolean):void
     arg types: [android.content.Context, int, ?[OBJECT, ARRAY], int]
     candidates:
      android.support.v4.widget.ResourceCursorAdapter.<init>(android.content.Context, int, android.database.Cursor, int):void
      android.support.v4.widget.ResourceCursorAdapter.<init>(android.content.Context, int, android.database.Cursor, boolean):void */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SuggestionsAdapter(android.content.Context r11, android.support.v7.widget.SearchView r12, android.app.SearchableInfo r13, java.util.WeakHashMap<java.lang.String, android.graphics.drawable.Drawable.ConstantState> r14) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r3 = r13
            r4 = r14
            r5 = r0
            r6 = r1
            r7 = r2
            int r7 = r7.getSuggestionRowLayout()
            r8 = 0
            r9 = 1
            r5.<init>(r6, r7, r8, r9)
            r5 = r0
            r6 = 0
            r5.mClosed = r6
            r5 = r0
            r6 = 1
            r5.mQueryRefinement = r6
            r5 = r0
            r6 = -1
            r5.mText1Col = r6
            r5 = r0
            r6 = -1
            r5.mText2Col = r6
            r5 = r0
            r6 = -1
            r5.mText2UrlCol = r6
            r5 = r0
            r6 = -1
            r5.mIconName1Col = r6
            r5 = r0
            r6 = -1
            r5.mIconName2Col = r6
            r5 = r0
            r6 = -1
            r5.mFlagsCol = r6
            r5 = r0
            r6 = r0
            android.content.Context r6 = r6.mContext
            java.lang.String r7 = "search"
            java.lang.Object r6 = r6.getSystemService(r7)
            android.app.SearchManager r6 = (android.app.SearchManager) r6
            r5.mSearchManager = r6
            r5 = r0
            r6 = r2
            r5.mSearchView = r6
            r5 = r0
            r6 = r3
            r5.mSearchable = r6
            r5 = r0
            r6 = r2
            int r6 = r6.getSuggestionCommitIconResId()
            r5.mCommitIconResId = r6
            r5 = r0
            r6 = r1
            r5.mProviderContext = r6
            r5 = r0
            r6 = r4
            r5.mOutsideDrawablesCache = r6
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.SuggestionsAdapter.<init>(android.content.Context, android.support.v7.widget.SearchView, android.app.SearchableInfo, java.util.WeakHashMap):void");
    }

    public void setQueryRefinement(int i) {
        this.mQueryRefinement = i;
    }

    public int getQueryRefinement() {
        return this.mQueryRefinement;
    }

    public boolean hasStableIds() {
        return false;
    }

    public Cursor runQueryOnBackgroundThread(CharSequence charSequence) {
        String charSequence2;
        CharSequence charSequence3 = charSequence;
        if (charSequence3 == null) {
            charSequence2 = "";
        } else {
            charSequence2 = charSequence3.toString();
        }
        String str = charSequence2;
        if (this.mSearchView.getVisibility() != 0 || this.mSearchView.getWindowVisibility() != 0) {
            return null;
        }
        try {
            Cursor searchManagerSuggestions = getSearchManagerSuggestions(this.mSearchable, str, 50);
            if (searchManagerSuggestions != null) {
                int count = searchManagerSuggestions.getCount();
                return searchManagerSuggestions;
            }
        } catch (RuntimeException e) {
            int w = Log.w(LOG_TAG, "Search suggestions query threw an exception.", e);
        }
        return null;
    }

    public void close() {
        changeCursor(null);
        this.mClosed = true;
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        updateSpinnerState(getCursor());
    }

    public void notifyDataSetInvalidated() {
        super.notifyDataSetInvalidated();
        updateSpinnerState(getCursor());
    }

    private void updateSpinnerState(Cursor cursor) {
        Cursor cursor2 = cursor;
        Bundle extras = cursor2 != null ? cursor2.getExtras() : null;
        if (extras == null || extras.getBoolean("in_progress")) {
        }
    }

    public void changeCursor(Cursor cursor) {
        Cursor cursor2 = cursor;
        if (this.mClosed) {
            int w = Log.w(LOG_TAG, "Tried to change cursor after adapter was closed.");
            if (cursor2 != null) {
                cursor2.close();
                return;
            }
            return;
        }
        try {
            super.changeCursor(cursor2);
            if (cursor2 != null) {
                this.mText1Col = cursor2.getColumnIndex("suggest_text_1");
                this.mText2Col = cursor2.getColumnIndex("suggest_text_2");
                this.mText2UrlCol = cursor2.getColumnIndex("suggest_text_2_url");
                this.mIconName1Col = cursor2.getColumnIndex("suggest_icon_1");
                this.mIconName2Col = cursor2.getColumnIndex("suggest_icon_2");
                this.mFlagsCol = cursor2.getColumnIndex("suggest_flags");
            }
        } catch (Exception e) {
            int e2 = Log.e(LOG_TAG, "error changing cursor and caching columns", e);
        }
    }

    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        Object obj;
        View newView = super.newView(context, cursor, viewGroup);
        new ChildViewCache(newView);
        newView.setTag(obj);
        ((ImageView) newView.findViewById(R.id.edit_query)).setImageResource(this.mCommitIconResId);
        return newView;
    }

    private static final class ChildViewCache {
        public final ImageView mIcon1;
        public final ImageView mIcon2;
        public final ImageView mIconRefine;
        public final TextView mText1;
        public final TextView mText2;

        public ChildViewCache(View view) {
            View view2 = view;
            this.mText1 = (TextView) view2.findViewById(16908308);
            this.mText2 = (TextView) view2.findViewById(16908309);
            this.mIcon1 = (ImageView) view2.findViewById(16908295);
            this.mIcon2 = (ImageView) view2.findViewById(16908296);
            this.mIconRefine = (ImageView) view2.findViewById(R.id.edit_query);
        }
    }

    public void bindView(View view, Context context, Cursor cursor) {
        CharSequence stringOrNull;
        Cursor cursor2 = cursor;
        ChildViewCache childViewCache = (ChildViewCache) view.getTag();
        int i = 0;
        if (this.mFlagsCol != -1) {
            i = cursor2.getInt(this.mFlagsCol);
        }
        if (childViewCache.mText1 != null) {
            setViewText(childViewCache.mText1, getStringOrNull(cursor2, this.mText1Col));
        }
        if (childViewCache.mText2 != null) {
            String stringOrNull2 = getStringOrNull(cursor2, this.mText2UrlCol);
            if (stringOrNull2 != null) {
                stringOrNull = formatUrl(stringOrNull2);
            } else {
                stringOrNull = getStringOrNull(cursor2, this.mText2Col);
            }
            if (TextUtils.isEmpty(stringOrNull)) {
                if (childViewCache.mText1 != null) {
                    childViewCache.mText1.setSingleLine(false);
                    childViewCache.mText1.setMaxLines(2);
                }
            } else if (childViewCache.mText1 != null) {
                childViewCache.mText1.setSingleLine(true);
                childViewCache.mText1.setMaxLines(1);
            }
            setViewText(childViewCache.mText2, stringOrNull);
        }
        if (childViewCache.mIcon1 != null) {
            setViewDrawable(childViewCache.mIcon1, getIcon1(cursor2), 4);
        }
        if (childViewCache.mIcon2 != null) {
            setViewDrawable(childViewCache.mIcon2, getIcon2(cursor2), 8);
        }
        if (this.mQueryRefinement == 2 || (this.mQueryRefinement == 1 && (i & 1) != 0)) {
            childViewCache.mIconRefine.setVisibility(0);
            childViewCache.mIconRefine.setTag(childViewCache.mText1.getText());
            childViewCache.mIconRefine.setOnClickListener(this);
            return;
        }
        childViewCache.mIconRefine.setVisibility(8);
    }

    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag instanceof CharSequence) {
            this.mSearchView.onQueryRefine((CharSequence) tag);
        }
    }

    private CharSequence formatUrl(CharSequence charSequence) {
        SpannableString spannableString;
        Object obj;
        TypedValue typedValue;
        CharSequence charSequence2 = charSequence;
        if (this.mUrlColor == null) {
            new TypedValue();
            TypedValue typedValue2 = typedValue;
            boolean resolveAttribute = this.mContext.getTheme().resolveAttribute(R.attr.textColorSearchUrl, typedValue2, true);
            this.mUrlColor = this.mContext.getResources().getColorStateList(typedValue2.resourceId);
        }
        new SpannableString(charSequence2);
        SpannableString spannableString2 = spannableString;
        new TextAppearanceSpan(null, 0, 0, this.mUrlColor, null);
        spannableString2.setSpan(obj, 0, charSequence2.length(), 33);
        return spannableString2;
    }

    private void setViewText(TextView textView, CharSequence charSequence) {
        TextView textView2 = textView;
        CharSequence charSequence2 = charSequence;
        textView2.setText(charSequence2);
        if (TextUtils.isEmpty(charSequence2)) {
            textView2.setVisibility(8);
        } else {
            textView2.setVisibility(0);
        }
    }

    private Drawable getIcon1(Cursor cursor) {
        Cursor cursor2 = cursor;
        if (this.mIconName1Col == -1) {
            return null;
        }
        Drawable drawableFromResourceValue = getDrawableFromResourceValue(cursor2.getString(this.mIconName1Col));
        if (drawableFromResourceValue != null) {
            return drawableFromResourceValue;
        }
        return getDefaultIcon1(cursor2);
    }

    private Drawable getIcon2(Cursor cursor) {
        Cursor cursor2 = cursor;
        if (this.mIconName2Col == -1) {
            return null;
        }
        return getDrawableFromResourceValue(cursor2.getString(this.mIconName2Col));
    }

    private void setViewDrawable(ImageView imageView, Drawable drawable, int i) {
        ImageView imageView2 = imageView;
        Drawable drawable2 = drawable;
        int i2 = i;
        imageView2.setImageDrawable(drawable2);
        if (drawable2 == null) {
            imageView2.setVisibility(i2);
            return;
        }
        imageView2.setVisibility(0);
        boolean visible = drawable2.setVisible(false, false);
        boolean visible2 = drawable2.setVisible(true, false);
    }

    public CharSequence convertToString(Cursor cursor) {
        String columnString;
        String columnString2;
        Cursor cursor2 = cursor;
        if (cursor2 == null) {
            return null;
        }
        String columnString3 = getColumnString(cursor2, "suggest_intent_query");
        if (columnString3 != null) {
            return columnString3;
        }
        if (this.mSearchable.shouldRewriteQueryFromData() && (columnString2 = getColumnString(cursor2, "suggest_intent_data")) != null) {
            return columnString2;
        }
        if (!this.mSearchable.shouldRewriteQueryFromText() || (columnString = getColumnString(cursor2, "suggest_text_1")) == null) {
            return null;
        }
        return columnString;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewGroup viewGroup2 = viewGroup;
        try {
            return super.getView(i, view, viewGroup2);
        } catch (RuntimeException e) {
            RuntimeException runtimeException = e;
            int w = Log.w(LOG_TAG, "Search suggestions cursor threw exception.", runtimeException);
            View newView = newView(this.mContext, this.mCursor, viewGroup2);
            if (newView != null) {
                ((ChildViewCache) newView.getTag()).mText1.setText(runtimeException.toString());
            }
            return newView;
        }
    }

    private Drawable getDrawableFromResourceValue(String str) {
        StringBuilder sb;
        StringBuilder sb2;
        String str2 = str;
        if (str2 == null || str2.length() == 0 || "0".equals(str2)) {
            return null;
        }
        try {
            int parseInt = Integer.parseInt(str2);
            new StringBuilder();
            String sb3 = sb2.append("android.resource://").append(this.mProviderContext.getPackageName()).append("/").append(parseInt).toString();
            Drawable checkIconCache = checkIconCache(sb3);
            if (checkIconCache != null) {
                return checkIconCache;
            }
            Drawable drawable = ContextCompat.getDrawable(this.mProviderContext, parseInt);
            storeInIconCache(sb3, drawable);
            return drawable;
        } catch (NumberFormatException e) {
            Drawable checkIconCache2 = checkIconCache(str2);
            if (checkIconCache2 != null) {
                return checkIconCache2;
            }
            Drawable drawable2 = getDrawable(Uri.parse(str2));
            storeInIconCache(str2, drawable2);
            return drawable2;
        } catch (Resources.NotFoundException e2) {
            new StringBuilder();
            int w = Log.w(LOG_TAG, sb.append("Icon resource not found: ").append(str2).toString());
            return null;
        }
    }

    private Drawable getDrawable(Uri uri) {
        StringBuilder sb;
        InputStream openInputStream;
        StringBuilder sb2;
        StringBuilder sb3;
        Throwable th;
        StringBuilder sb4;
        Throwable th2;
        StringBuilder sb5;
        Uri uri2 = uri;
        try {
            if ("android.resource".equals(uri2.getScheme())) {
                return getDrawableFromResourceUri(uri2);
            }
            openInputStream = this.mProviderContext.getContentResolver().openInputStream(uri2);
            if (openInputStream == null) {
                Throwable th3 = th;
                new StringBuilder();
                new FileNotFoundException(sb4.append("Failed to open ").append(uri2).toString());
                throw th3;
            }
            Drawable createFromStream = Drawable.createFromStream(openInputStream, null);
            try {
                openInputStream.close();
            } catch (IOException e) {
                IOException iOException = e;
                new StringBuilder();
                int e2 = Log.e(LOG_TAG, sb3.append("Error closing icon stream for ").append(uri2).toString(), iOException);
            }
            return createFromStream;
        } catch (Resources.NotFoundException e3) {
            Throwable th4 = th2;
            new StringBuilder();
            new FileNotFoundException(sb5.append("Resource does not exist: ").append(uri2).toString());
            throw th4;
        } catch (FileNotFoundException e4) {
            new StringBuilder();
            int w = Log.w(LOG_TAG, sb.append("Icon not found: ").append(uri2).append(", ").append(e4.getMessage()).toString());
            return null;
        } catch (Throwable th5) {
            Throwable th6 = th5;
            try {
                openInputStream.close();
            } catch (IOException e5) {
                new StringBuilder();
                int e6 = Log.e(LOG_TAG, sb2.append("Error closing icon stream for ").append(uri2).toString(), e5);
            }
            throw th6;
        }
    }

    private Drawable checkIconCache(String str) {
        Drawable.ConstantState constantState = this.mOutsideDrawablesCache.get(str);
        if (constantState == null) {
            return null;
        }
        return constantState.newDrawable();
    }

    private void storeInIconCache(String str, Drawable drawable) {
        String str2 = str;
        Drawable drawable2 = drawable;
        if (drawable2 != null) {
            Drawable.ConstantState put = this.mOutsideDrawablesCache.put(str2, drawable2.getConstantState());
        }
    }

    private Drawable getDefaultIcon1(Cursor cursor) {
        Drawable activityIconWithCache = getActivityIconWithCache(this.mSearchable.getSearchActivity());
        if (activityIconWithCache != null) {
            return activityIconWithCache;
        }
        return this.mContext.getPackageManager().getDefaultActivityIcon();
    }

    private Drawable getActivityIconWithCache(ComponentName componentName) {
        ComponentName componentName2 = componentName;
        String flattenToShortString = componentName2.flattenToShortString();
        if (this.mOutsideDrawablesCache.containsKey(flattenToShortString)) {
            Drawable.ConstantState constantState = this.mOutsideDrawablesCache.get(flattenToShortString);
            return constantState == null ? null : constantState.newDrawable(this.mProviderContext.getResources());
        }
        Drawable activityIcon = getActivityIcon(componentName2);
        Drawable.ConstantState put = this.mOutsideDrawablesCache.put(flattenToShortString, activityIcon == null ? null : activityIcon.getConstantState());
        return activityIcon;
    }

    private Drawable getActivityIcon(ComponentName componentName) {
        StringBuilder sb;
        ComponentName componentName2 = componentName;
        PackageManager packageManager = this.mContext.getPackageManager();
        try {
            ActivityInfo activityInfo = packageManager.getActivityInfo(componentName2, 128);
            int iconResource = activityInfo.getIconResource();
            if (iconResource == 0) {
                return null;
            }
            Drawable drawable = packageManager.getDrawable(componentName2.getPackageName(), iconResource, activityInfo.applicationInfo);
            if (drawable != null) {
                return drawable;
            }
            new StringBuilder();
            int w = Log.w(LOG_TAG, sb.append("Invalid icon resource ").append(iconResource).append(" for ").append(componentName2.flattenToShortString()).toString());
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            int w2 = Log.w(LOG_TAG, e.toString());
            return null;
        }
    }

    public static String getColumnString(Cursor cursor, String str) {
        Cursor cursor2 = cursor;
        return getStringOrNull(cursor2, cursor2.getColumnIndex(str));
    }

    private static String getStringOrNull(Cursor cursor, int i) {
        Cursor cursor2 = cursor;
        int i2 = i;
        if (i2 == -1) {
            return null;
        }
        try {
            return cursor2.getString(i2);
        } catch (Exception e) {
            int e2 = Log.e(LOG_TAG, "unexpected error retrieving valid column from cursor, did the remote process die?", e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public Drawable getDrawableFromResourceUri(Uri uri) throws FileNotFoundException {
        Throwable th;
        StringBuilder sb;
        Throwable th2;
        StringBuilder sb2;
        int identifier;
        Throwable th3;
        StringBuilder sb3;
        Throwable th4;
        StringBuilder sb4;
        Throwable th5;
        StringBuilder sb5;
        Throwable th6;
        StringBuilder sb6;
        Uri uri2 = uri;
        String authority = uri2.getAuthority();
        if (TextUtils.isEmpty(authority)) {
            Throwable th7 = th6;
            new StringBuilder();
            new FileNotFoundException(sb6.append("No authority: ").append(uri2).toString());
            throw th7;
        }
        try {
            Resources resourcesForApplication = this.mContext.getPackageManager().getResourcesForApplication(authority);
            List<String> pathSegments = uri2.getPathSegments();
            if (pathSegments == null) {
                Throwable th8 = th5;
                new StringBuilder();
                new FileNotFoundException(sb5.append("No path: ").append(uri2).toString());
                throw th8;
            }
            int size = pathSegments.size();
            if (size == 1) {
                try {
                    identifier = Integer.parseInt(pathSegments.get(0));
                } catch (NumberFormatException e) {
                    Throwable th9 = th4;
                    new StringBuilder();
                    new FileNotFoundException(sb4.append("Single path segment is not a resource ID: ").append(uri2).toString());
                    throw th9;
                }
            } else if (size == 2) {
                identifier = resourcesForApplication.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
            } else {
                Throwable th10 = th2;
                new StringBuilder();
                new FileNotFoundException(sb2.append("More than two path segments: ").append(uri2).toString());
                throw th10;
            }
            if (identifier != 0) {
                return resourcesForApplication.getDrawable(identifier);
            }
            Throwable th11 = th3;
            new StringBuilder();
            new FileNotFoundException(sb3.append("No resource found for: ").append(uri2).toString());
            throw th11;
        } catch (PackageManager.NameNotFoundException e2) {
            Throwable th12 = th;
            new StringBuilder();
            new FileNotFoundException(sb.append("No package found for authority: ").append(uri2).toString());
            throw th12;
        }
    }

    /* access modifiers changed from: package-private */
    public Cursor getSearchManagerSuggestions(SearchableInfo searchableInfo, String str, int i) {
        Uri.Builder builder;
        SearchableInfo searchableInfo2 = searchableInfo;
        String str2 = str;
        int i2 = i;
        if (searchableInfo2 == null) {
            return null;
        }
        String suggestAuthority = searchableInfo2.getSuggestAuthority();
        if (suggestAuthority == null) {
            return null;
        }
        new Uri.Builder();
        Uri.Builder fragment = builder.scheme("content").authority(suggestAuthority).query("").fragment("");
        String suggestPath = searchableInfo2.getSuggestPath();
        if (suggestPath != null) {
            Uri.Builder appendEncodedPath = fragment.appendEncodedPath(suggestPath);
        }
        Uri.Builder appendPath = fragment.appendPath("search_suggest_query");
        String suggestSelection = searchableInfo2.getSuggestSelection();
        String[] strArr = null;
        if (suggestSelection != null) {
            strArr = new String[]{str2};
        } else {
            Uri.Builder appendPath2 = fragment.appendPath(str2);
        }
        if (i2 > 0) {
            Uri.Builder appendQueryParameter = fragment.appendQueryParameter("limit", String.valueOf(i2));
        }
        return this.mContext.getContentResolver().query(fragment.build(), null, suggestSelection, strArr, null);
    }
}
