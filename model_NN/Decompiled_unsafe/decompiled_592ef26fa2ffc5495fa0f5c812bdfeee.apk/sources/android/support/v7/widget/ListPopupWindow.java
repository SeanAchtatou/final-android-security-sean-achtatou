package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.text.TextUtilsCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.widget.AutoScrollHelper;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.support.v4.widget.PopupWindowCompat;
import android.support.v7.appcompat.R;
import android.support.v7.widget.ActivityChooserView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import java.lang.reflect.Method;

public class ListPopupWindow {
    private static final boolean DEBUG = false;
    private static final int EXPAND_LIST_TIMEOUT = 250;
    public static final int INPUT_METHOD_FROM_FOCUSABLE = 0;
    public static final int INPUT_METHOD_NEEDED = 1;
    public static final int INPUT_METHOD_NOT_NEEDED = 2;
    public static final int MATCH_PARENT = -1;
    public static final int POSITION_PROMPT_ABOVE = 0;
    public static final int POSITION_PROMPT_BELOW = 1;
    private static final String TAG = "ListPopupWindow";
    public static final int WRAP_CONTENT = -2;
    private static Method sClipToWindowEnabledMethod;
    private static Method sGetMaxAvailableHeightMethod;
    private ListAdapter mAdapter;
    private Context mContext;
    private boolean mDropDownAlwaysVisible;
    private View mDropDownAnchorView;
    private int mDropDownGravity;
    private int mDropDownHeight;
    private int mDropDownHorizontalOffset;
    /* access modifiers changed from: private */
    public DropDownListView mDropDownList;
    private Drawable mDropDownListHighlight;
    private int mDropDownVerticalOffset;
    private boolean mDropDownVerticalOffsetSet;
    private int mDropDownWidth;
    private int mDropDownWindowLayoutType;
    private boolean mForceIgnoreOutsideTouch;
    /* access modifiers changed from: private */
    public final Handler mHandler;
    private final ListSelectorHider mHideSelector;
    private AdapterView.OnItemClickListener mItemClickListener;
    private AdapterView.OnItemSelectedListener mItemSelectedListener;
    private int mLayoutDirection;
    int mListItemExpandMaximum;
    private boolean mModal;
    private DataSetObserver mObserver;
    /* access modifiers changed from: private */
    public PopupWindow mPopup;
    private int mPromptPosition;
    private View mPromptView;
    /* access modifiers changed from: private */
    public final ResizePopupRunnable mResizePopupRunnable;
    private final PopupScrollListener mScrollListener;
    private Runnable mShowDropDownRunnable;
    private Rect mTempRect;
    private final PopupTouchInterceptor mTouchInterceptor;

    static {
        Class<PopupWindow> cls = PopupWindow.class;
        try {
            sClipToWindowEnabledMethod = cls.getDeclaredMethod("setClipToScreenEnabled", Boolean.TYPE);
        } catch (NoSuchMethodException e) {
            int i = Log.i(TAG, "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
        }
        Class<PopupWindow> cls2 = PopupWindow.class;
        try {
            Class[] clsArr = new Class[3];
            clsArr[0] = View.class;
            Class[] clsArr2 = clsArr;
            clsArr2[1] = Integer.TYPE;
            Class[] clsArr3 = clsArr2;
            clsArr3[2] = Boolean.TYPE;
            sGetMaxAvailableHeightMethod = cls2.getDeclaredMethod("getMaxAvailableHeight", clsArr3);
        } catch (NoSuchMethodException e2) {
            int i2 = Log.i(TAG, "Could not find method getMaxAvailableHeight(View, int, boolean) on PopupWindow. Oh well.");
        }
    }

    public ListPopupWindow(Context context) {
        this(context, null, R.attr.listPopupWindowStyle);
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.listPopupWindowStyle);
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet, int i, int i2) {
        ResizePopupRunnable resizePopupRunnable;
        PopupTouchInterceptor popupTouchInterceptor;
        PopupScrollListener popupScrollListener;
        ListSelectorHider listSelectorHider;
        Rect rect;
        Handler handler;
        PopupWindow popupWindow;
        Context context2 = context;
        AttributeSet attributeSet2 = attributeSet;
        int i3 = i;
        this.mDropDownHeight = -2;
        this.mDropDownWidth = -2;
        this.mDropDownWindowLayoutType = 1002;
        this.mDropDownGravity = 0;
        this.mDropDownAlwaysVisible = false;
        this.mForceIgnoreOutsideTouch = false;
        this.mListItemExpandMaximum = ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED;
        this.mPromptPosition = 0;
        new ResizePopupRunnable();
        this.mResizePopupRunnable = resizePopupRunnable;
        new PopupTouchInterceptor();
        this.mTouchInterceptor = popupTouchInterceptor;
        new PopupScrollListener();
        this.mScrollListener = popupScrollListener;
        new ListSelectorHider();
        this.mHideSelector = listSelectorHider;
        new Rect();
        this.mTempRect = rect;
        this.mContext = context2;
        new Handler(context2.getMainLooper());
        this.mHandler = handler;
        TypedArray obtainStyledAttributes = context2.obtainStyledAttributes(attributeSet2, R.styleable.ListPopupWindow, i3, i2);
        this.mDropDownHorizontalOffset = obtainStyledAttributes.getDimensionPixelOffset(R.styleable.ListPopupWindow_android_dropDownHorizontalOffset, 0);
        this.mDropDownVerticalOffset = obtainStyledAttributes.getDimensionPixelOffset(R.styleable.ListPopupWindow_android_dropDownVerticalOffset, 0);
        if (this.mDropDownVerticalOffset != 0) {
            this.mDropDownVerticalOffsetSet = true;
        }
        obtainStyledAttributes.recycle();
        new AppCompatPopupWindow(context2, attributeSet2, i3);
        this.mPopup = popupWindow;
        this.mPopup.setInputMethodMode(1);
        this.mLayoutDirection = TextUtilsCompat.getLayoutDirectionFromLocale(this.mContext.getResources().getConfiguration().locale);
    }

    public void setAdapter(ListAdapter listAdapter) {
        DataSetObserver dataSetObserver;
        ListAdapter listAdapter2 = listAdapter;
        if (this.mObserver == null) {
            new PopupDataSetObserver();
            this.mObserver = dataSetObserver;
        } else if (this.mAdapter != null) {
            this.mAdapter.unregisterDataSetObserver(this.mObserver);
        }
        this.mAdapter = listAdapter2;
        if (this.mAdapter != null) {
            listAdapter2.registerDataSetObserver(this.mObserver);
        }
        if (this.mDropDownList != null) {
            this.mDropDownList.setAdapter(this.mAdapter);
        }
    }

    public void setPromptPosition(int i) {
        this.mPromptPosition = i;
    }

    public int getPromptPosition() {
        return this.mPromptPosition;
    }

    public void setModal(boolean z) {
        boolean z2 = z;
        this.mModal = z2;
        this.mPopup.setFocusable(z2);
    }

    public boolean isModal() {
        return this.mModal;
    }

    public void setForceIgnoreOutsideTouch(boolean z) {
        this.mForceIgnoreOutsideTouch = z;
    }

    public void setDropDownAlwaysVisible(boolean z) {
        this.mDropDownAlwaysVisible = z;
    }

    public boolean isDropDownAlwaysVisible() {
        return this.mDropDownAlwaysVisible;
    }

    public void setSoftInputMode(int i) {
        this.mPopup.setSoftInputMode(i);
    }

    public int getSoftInputMode() {
        return this.mPopup.getSoftInputMode();
    }

    public void setListSelector(Drawable drawable) {
        this.mDropDownListHighlight = drawable;
    }

    public Drawable getBackground() {
        return this.mPopup.getBackground();
    }

    public void setBackgroundDrawable(Drawable drawable) {
        this.mPopup.setBackgroundDrawable(drawable);
    }

    public void setAnimationStyle(int i) {
        this.mPopup.setAnimationStyle(i);
    }

    public int getAnimationStyle() {
        return this.mPopup.getAnimationStyle();
    }

    public View getAnchorView() {
        return this.mDropDownAnchorView;
    }

    public void setAnchorView(View view) {
        this.mDropDownAnchorView = view;
    }

    public int getHorizontalOffset() {
        return this.mDropDownHorizontalOffset;
    }

    public void setHorizontalOffset(int i) {
        this.mDropDownHorizontalOffset = i;
    }

    public int getVerticalOffset() {
        if (!this.mDropDownVerticalOffsetSet) {
            return 0;
        }
        return this.mDropDownVerticalOffset;
    }

    public void setVerticalOffset(int i) {
        this.mDropDownVerticalOffset = i;
        this.mDropDownVerticalOffsetSet = true;
    }

    public void setDropDownGravity(int i) {
        this.mDropDownGravity = i;
    }

    public int getWidth() {
        return this.mDropDownWidth;
    }

    public void setWidth(int i) {
        this.mDropDownWidth = i;
    }

    public void setContentWidth(int i) {
        int i2 = i;
        Drawable background = this.mPopup.getBackground();
        if (background != null) {
            boolean padding = background.getPadding(this.mTempRect);
            this.mDropDownWidth = this.mTempRect.left + this.mTempRect.right + i2;
            return;
        }
        setWidth(i2);
    }

    public int getHeight() {
        return this.mDropDownHeight;
    }

    public void setHeight(int i) {
        this.mDropDownHeight = i;
    }

    public void setWindowLayoutType(int i) {
        this.mDropDownWindowLayoutType = i;
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.mItemClickListener = onItemClickListener;
    }

    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
        this.mItemSelectedListener = onItemSelectedListener;
    }

    public void setPromptView(View view) {
        View view2 = view;
        boolean isShowing = isShowing();
        if (isShowing) {
            removePromptView();
        }
        this.mPromptView = view2;
        if (isShowing) {
            show();
        }
    }

    public void postShow() {
        boolean post = this.mHandler.post(this.mShowDropDownRunnable);
    }

    public void show() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int buildDropDown = buildDropDown();
        boolean isInputMethodNotNeeded = isInputMethodNotNeeded();
        PopupWindowCompat.setWindowLayoutType(this.mPopup, this.mDropDownWindowLayoutType);
        if (this.mPopup.isShowing()) {
            if (this.mDropDownWidth == -1) {
                i3 = -1;
            } else if (this.mDropDownWidth == -2) {
                i3 = getAnchorView().getWidth();
            } else {
                i3 = this.mDropDownWidth;
            }
            if (this.mDropDownHeight == -1) {
                i4 = isInputMethodNotNeeded ? buildDropDown : -1;
                if (isInputMethodNotNeeded) {
                    this.mPopup.setWidth(this.mDropDownWidth == -1 ? -1 : 0);
                    this.mPopup.setHeight(0);
                } else {
                    this.mPopup.setWidth(this.mDropDownWidth == -1 ? -1 : 0);
                    this.mPopup.setHeight(-1);
                }
            } else {
                i4 = this.mDropDownHeight == -2 ? buildDropDown : this.mDropDownHeight;
            }
            this.mPopup.setOutsideTouchable(!this.mForceIgnoreOutsideTouch && !this.mDropDownAlwaysVisible);
            PopupWindow popupWindow = this.mPopup;
            View anchorView = getAnchorView();
            int i7 = this.mDropDownHorizontalOffset;
            int i8 = this.mDropDownVerticalOffset;
            if (i3 < 0) {
                i5 = -1;
            } else {
                i5 = i3;
            }
            if (i4 < 0) {
                i6 = -1;
            } else {
                i6 = i4;
            }
            popupWindow.update(anchorView, i7, i8, i5, i6);
            return;
        }
        if (this.mDropDownWidth == -1) {
            i = -1;
        } else if (this.mDropDownWidth == -2) {
            i = getAnchorView().getWidth();
        } else {
            i = this.mDropDownWidth;
        }
        if (this.mDropDownHeight == -1) {
            i2 = -1;
        } else if (this.mDropDownHeight == -2) {
            i2 = buildDropDown;
        } else {
            i2 = this.mDropDownHeight;
        }
        this.mPopup.setWidth(i);
        this.mPopup.setHeight(i2);
        setPopupClipToScreenEnabled(true);
        this.mPopup.setOutsideTouchable(!this.mForceIgnoreOutsideTouch && !this.mDropDownAlwaysVisible);
        this.mPopup.setTouchInterceptor(this.mTouchInterceptor);
        PopupWindowCompat.showAsDropDown(this.mPopup, getAnchorView(), this.mDropDownHorizontalOffset, this.mDropDownVerticalOffset, this.mDropDownGravity);
        this.mDropDownList.setSelection(-1);
        if (!this.mModal || this.mDropDownList.isInTouchMode()) {
            clearListSelection();
        }
        if (!this.mModal) {
            boolean post = this.mHandler.post(this.mHideSelector);
        }
    }

    public void dismiss() {
        this.mPopup.dismiss();
        removePromptView();
        this.mPopup.setContentView(null);
        this.mDropDownList = null;
        this.mHandler.removeCallbacks(this.mResizePopupRunnable);
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
        this.mPopup.setOnDismissListener(onDismissListener);
    }

    private void removePromptView() {
        if (this.mPromptView != null) {
            ViewParent parent = this.mPromptView.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.mPromptView);
            }
        }
    }

    public void setInputMethodMode(int i) {
        this.mPopup.setInputMethodMode(i);
    }

    public int getInputMethodMode() {
        return this.mPopup.getInputMethodMode();
    }

    public void setSelection(int i) {
        int i2 = i;
        DropDownListView dropDownListView = this.mDropDownList;
        if (isShowing() && dropDownListView != null) {
            boolean access$502 = DropDownListView.access$502(dropDownListView, false);
            dropDownListView.setSelection(i2);
            if (Build.VERSION.SDK_INT >= 11 && dropDownListView.getChoiceMode() != 0) {
                dropDownListView.setItemChecked(i2, true);
            }
        }
    }

    public void clearListSelection() {
        DropDownListView dropDownListView = this.mDropDownList;
        if (dropDownListView != null) {
            boolean access$502 = DropDownListView.access$502(dropDownListView, true);
            dropDownListView.requestLayout();
        }
    }

    public boolean isShowing() {
        return this.mPopup.isShowing();
    }

    public boolean isInputMethodNotNeeded() {
        return this.mPopup.getInputMethodMode() == 2;
    }

    public boolean performItemClick(int i) {
        int i2 = i;
        if (!isShowing()) {
            return false;
        }
        if (this.mItemClickListener != null) {
            DropDownListView dropDownListView = this.mDropDownList;
            this.mItemClickListener.onItemClick(dropDownListView, dropDownListView.getChildAt(i2 - dropDownListView.getFirstVisiblePosition()), i2, dropDownListView.getAdapter().getItemId(i2));
        }
        return true;
    }

    public Object getSelectedItem() {
        if (!isShowing()) {
            return null;
        }
        return this.mDropDownList.getSelectedItem();
    }

    public int getSelectedItemPosition() {
        if (!isShowing()) {
            return -1;
        }
        return this.mDropDownList.getSelectedItemPosition();
    }

    public long getSelectedItemId() {
        if (!isShowing()) {
            return Long.MIN_VALUE;
        }
        return this.mDropDownList.getSelectedItemId();
    }

    public View getSelectedView() {
        if (!isShowing()) {
            return null;
        }
        return this.mDropDownList.getSelectedView();
    }

    public ListView getListView() {
        return this.mDropDownList;
    }

    /* access modifiers changed from: package-private */
    public void setListItemExpandMax(int i) {
        this.mListItemExpandMaximum = i;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        int lookForSelectablePosition;
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        if (isShowing() && i2 != 62 && (this.mDropDownList.getSelectedItemPosition() >= 0 || !isConfirmKey(i2))) {
            int selectedItemPosition = this.mDropDownList.getSelectedItemPosition();
            boolean z = !this.mPopup.isAboveAnchor();
            ListAdapter listAdapter = this.mAdapter;
            int i3 = Integer.MAX_VALUE;
            int i4 = Integer.MIN_VALUE;
            if (listAdapter != null) {
                boolean areAllItemsEnabled = listAdapter.areAllItemsEnabled();
                i3 = areAllItemsEnabled ? 0 : this.mDropDownList.lookForSelectablePosition(0, true);
                if (areAllItemsEnabled) {
                    lookForSelectablePosition = listAdapter.getCount() - 1;
                } else {
                    lookForSelectablePosition = this.mDropDownList.lookForSelectablePosition(listAdapter.getCount() - 1, false);
                }
                i4 = lookForSelectablePosition;
            }
            if ((!z || i2 != 19 || selectedItemPosition > i3) && (z || i2 != 20 || selectedItemPosition < i4)) {
                boolean access$502 = DropDownListView.access$502(this.mDropDownList, false);
                if (this.mDropDownList.onKeyDown(i2, keyEvent2)) {
                    this.mPopup.setInputMethodMode(2);
                    boolean requestFocusFromTouch = this.mDropDownList.requestFocusFromTouch();
                    show();
                    switch (i2) {
                        case 19:
                        case 20:
                        case 23:
                        case 66:
                            return true;
                    }
                } else if (!z || i2 != 20) {
                    if (!z && i2 == 19 && selectedItemPosition == i3) {
                        return true;
                    }
                } else if (selectedItemPosition == i4) {
                    return true;
                }
            } else {
                clearListSelection();
                this.mPopup.setInputMethodMode(1);
                show();
                return true;
            }
        }
        return false;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        if (!isShowing() || this.mDropDownList.getSelectedItemPosition() < 0) {
            return false;
        }
        boolean onKeyUp = this.mDropDownList.onKeyUp(i2, keyEvent2);
        if (onKeyUp && isConfirmKey(i2)) {
            dismiss();
        }
        return onKeyUp;
    }

    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        KeyEvent keyEvent2 = keyEvent;
        if (i == 4 && isShowing()) {
            View view = this.mDropDownAnchorView;
            if (keyEvent2.getAction() == 0 && keyEvent2.getRepeatCount() == 0) {
                KeyEvent.DispatcherState keyDispatcherState = view.getKeyDispatcherState();
                if (keyDispatcherState != null) {
                    keyDispatcherState.startTracking(keyEvent2, this);
                }
                return true;
            } else if (keyEvent2.getAction() == 1) {
                KeyEvent.DispatcherState keyDispatcherState2 = view.getKeyDispatcherState();
                if (keyDispatcherState2 != null) {
                    keyDispatcherState2.handleUpEvent(keyEvent2);
                }
                if (keyEvent2.isTracking() && !keyEvent2.isCanceled()) {
                    dismiss();
                    return true;
                }
            }
        }
        return false;
    }

    public View.OnTouchListener createDragToOpenListener(View view) {
        View.OnTouchListener onTouchListener;
        new ForwardingListener(view) {
            public ListPopupWindow getPopup() {
                return ListPopupWindow.this;
            }
        };
        return onTouchListener;
    }

    /* JADX WARN: Type inference failed for: r11v103, types: [android.widget.LinearLayout] */
    /* JADX WARN: Type inference failed for: r11v108, types: [android.widget.LinearLayout] */
    /* JADX WARN: Type inference failed for: r11v109, types: [android.widget.LinearLayout] */
    /* JADX WARN: Type inference failed for: r11v110, types: [android.widget.LinearLayout] */
    /* JADX WARN: Type inference failed for: r11v111, types: [android.widget.LinearLayout] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int buildDropDown() {
        /*
            r18 = this;
            r0 = r18
            r11 = 0
            r2 = r11
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r11 = r11.mDropDownList
            if (r11 != 0) goto L_0x0194
            r11 = r0
            android.content.Context r11 = r11.mContext
            r3 = r11
            r11 = r0
            android.support.v7.widget.ListPopupWindow$2 r12 = new android.support.v7.widget.ListPopupWindow$2
            r17 = r12
            r12 = r17
            r13 = r17
            r14 = r0
            r13.<init>()
            r11.mShowDropDownRunnable = r12
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r12 = new android.support.v7.widget.ListPopupWindow$DropDownListView
            r17 = r12
            r12 = r17
            r13 = r17
            r14 = r3
            r15 = r0
            boolean r15 = r15.mModal
            if (r15 != 0) goto L_0x0171
            r15 = 1
        L_0x002c:
            r13.<init>(r14, r15)
            r11.mDropDownList = r12
            r11 = r0
            android.graphics.drawable.Drawable r11 = r11.mDropDownListHighlight
            if (r11 == 0) goto L_0x003f
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r11 = r11.mDropDownList
            r12 = r0
            android.graphics.drawable.Drawable r12 = r12.mDropDownListHighlight
            r11.setSelector(r12)
        L_0x003f:
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r11 = r11.mDropDownList
            r12 = r0
            android.widget.ListAdapter r12 = r12.mAdapter
            r11.setAdapter(r12)
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r11 = r11.mDropDownList
            r12 = r0
            android.widget.AdapterView$OnItemClickListener r12 = r12.mItemClickListener
            r11.setOnItemClickListener(r12)
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r11 = r11.mDropDownList
            r12 = 1
            r11.setFocusable(r12)
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r11 = r11.mDropDownList
            r12 = 1
            r11.setFocusableInTouchMode(r12)
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r11 = r11.mDropDownList
            android.support.v7.widget.ListPopupWindow$3 r12 = new android.support.v7.widget.ListPopupWindow$3
            r17 = r12
            r12 = r17
            r13 = r17
            r14 = r0
            r13.<init>()
            r11.setOnItemSelectedListener(r12)
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r11 = r11.mDropDownList
            r12 = r0
            android.support.v7.widget.ListPopupWindow$PopupScrollListener r12 = r12.mScrollListener
            r11.setOnScrollListener(r12)
            r11 = r0
            android.widget.AdapterView$OnItemSelectedListener r11 = r11.mItemSelectedListener
            if (r11 == 0) goto L_0x0088
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r11 = r11.mDropDownList
            r12 = r0
            android.widget.AdapterView$OnItemSelectedListener r12 = r12.mItemSelectedListener
            r11.setOnItemSelectedListener(r12)
        L_0x0088:
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r11 = r11.mDropDownList
            r1 = r11
            r11 = r0
            android.view.View r11 = r11.mPromptView
            r4 = r11
            r11 = r4
            if (r11 == 0) goto L_0x0110
            android.widget.LinearLayout r11 = new android.widget.LinearLayout
            r17 = r11
            r11 = r17
            r12 = r17
            r13 = r3
            r12.<init>(r13)
            r5 = r11
            r11 = r5
            r12 = 1
            r11.setOrientation(r12)
            android.widget.LinearLayout$LayoutParams r11 = new android.widget.LinearLayout$LayoutParams
            r17 = r11
            r11 = r17
            r12 = r17
            r13 = -1
            r14 = 0
            r15 = 1065353216(0x3f800000, float:1.0)
            r12.<init>(r13, r14, r15)
            r6 = r11
            r11 = r0
            int r11 = r11.mPromptPosition
            switch(r11) {
                case 0: goto L_0x0181;
                case 1: goto L_0x0174;
                default: goto L_0x00bb;
            }
        L_0x00bb:
            java.lang.String r11 = "ListPopupWindow"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r17 = r12
            r12 = r17
            r13 = r17
            r13.<init>()
            java.lang.String r13 = "Invalid hint position "
            java.lang.StringBuilder r12 = r12.append(r13)
            r13 = r0
            int r13 = r13.mPromptPosition
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r12 = r12.toString()
            int r11 = android.util.Log.e(r11, r12)
        L_0x00dd:
            r11 = r0
            int r11 = r11.mDropDownWidth
            if (r11 < 0) goto L_0x018e
            r11 = -2147483648(0xffffffff80000000, float:-0.0)
            r8 = r11
            r11 = r0
            int r11 = r11.mDropDownWidth
            r7 = r11
        L_0x00e9:
            r11 = r7
            r12 = r8
            int r11 = android.view.View.MeasureSpec.makeMeasureSpec(r11, r12)
            r9 = r11
            r11 = 0
            r10 = r11
            r11 = r4
            r12 = r9
            r13 = 0
            r11.measure(r12, r13)
            r11 = r4
            android.view.ViewGroup$LayoutParams r11 = r11.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r11 = (android.widget.LinearLayout.LayoutParams) r11
            r6 = r11
            r11 = r4
            int r11 = r11.getMeasuredHeight()
            r12 = r6
            int r12 = r12.topMargin
            int r11 = r11 + r12
            r12 = r6
            int r12 = r12.bottomMargin
            int r11 = r11 + r12
            r2 = r11
            r11 = r5
            r1 = r11
        L_0x0110:
            r11 = r0
            android.widget.PopupWindow r11 = r11.mPopup
            r12 = r1
            r11.setContentView(r12)
        L_0x0117:
            r11 = 0
            r3 = r11
            r11 = r0
            android.widget.PopupWindow r11 = r11.mPopup
            android.graphics.drawable.Drawable r11 = r11.getBackground()
            r4 = r11
            r11 = r4
            if (r11 == 0) goto L_0x01bd
            r11 = r4
            r12 = r0
            android.graphics.Rect r12 = r12.mTempRect
            boolean r11 = r11.getPadding(r12)
            r11 = r0
            android.graphics.Rect r11 = r11.mTempRect
            int r11 = r11.top
            r12 = r0
            android.graphics.Rect r12 = r12.mTempRect
            int r12 = r12.bottom
            int r11 = r11 + r12
            r3 = r11
            r11 = r0
            boolean r11 = r11.mDropDownVerticalOffsetSet
            if (r11 != 0) goto L_0x0146
            r11 = r0
            r12 = r0
            android.graphics.Rect r12 = r12.mTempRect
            int r12 = r12.top
            int r12 = -r12
            r11.mDropDownVerticalOffset = r12
        L_0x0146:
            r11 = r0
            android.widget.PopupWindow r11 = r11.mPopup
            int r11 = r11.getInputMethodMode()
            r12 = 2
            if (r11 != r12) goto L_0x01c4
            r11 = 1
        L_0x0151:
            r5 = r11
            r11 = r0
            r12 = r0
            android.view.View r12 = r12.getAnchorView()
            r13 = r0
            int r13 = r13.mDropDownVerticalOffset
            r14 = r5
            int r11 = r11.getMaxAvailableHeight(r12, r13, r14)
            r6 = r11
            r11 = r0
            boolean r11 = r11.mDropDownAlwaysVisible
            if (r11 != 0) goto L_0x016c
            r11 = r0
            int r11 = r11.mDropDownHeight
            r12 = -1
            if (r11 != r12) goto L_0x01c6
        L_0x016c:
            r11 = r6
            r12 = r3
            int r11 = r11 + r12
            r0 = r11
        L_0x0170:
            return r0
        L_0x0171:
            r15 = 0
            goto L_0x002c
        L_0x0174:
            r11 = r5
            r12 = r1
            r13 = r6
            r11.addView(r12, r13)
            r11 = r5
            r12 = r4
            r11.addView(r12)
            goto L_0x00dd
        L_0x0181:
            r11 = r5
            r12 = r4
            r11.addView(r12)
            r11 = r5
            r12 = r1
            r13 = r6
            r11.addView(r12, r13)
            goto L_0x00dd
        L_0x018e:
            r11 = 0
            r8 = r11
            r11 = 0
            r7 = r11
            goto L_0x00e9
        L_0x0194:
            r11 = r0
            android.widget.PopupWindow r11 = r11.mPopup
            android.view.View r11 = r11.getContentView()
            android.view.ViewGroup r11 = (android.view.ViewGroup) r11
            r1 = r11
            r11 = r0
            android.view.View r11 = r11.mPromptView
            r3 = r11
            r11 = r3
            if (r11 == 0) goto L_0x0117
            r11 = r3
            android.view.ViewGroup$LayoutParams r11 = r11.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r11 = (android.widget.LinearLayout.LayoutParams) r11
            r4 = r11
            r11 = r3
            int r11 = r11.getMeasuredHeight()
            r12 = r4
            int r12 = r12.topMargin
            int r11 = r11 + r12
            r12 = r4
            int r12 = r12.bottomMargin
            int r11 = r11 + r12
            r2 = r11
            goto L_0x0117
        L_0x01bd:
            r11 = r0
            android.graphics.Rect r11 = r11.mTempRect
            r11.setEmpty()
            goto L_0x0146
        L_0x01c4:
            r11 = 0
            goto L_0x0151
        L_0x01c6:
            r11 = r0
            int r11 = r11.mDropDownWidth
            switch(r11) {
                case -2: goto L_0x01f5;
                case -1: goto L_0x0216;
                default: goto L_0x01cc;
            }
        L_0x01cc:
            r11 = r0
            int r11 = r11.mDropDownWidth
            r12 = 1073741824(0x40000000, float:2.0)
            int r11 = android.view.View.MeasureSpec.makeMeasureSpec(r11, r12)
            r7 = r11
        L_0x01d6:
            r11 = r0
            android.support.v7.widget.ListPopupWindow$DropDownListView r11 = r11.mDropDownList
            r12 = r7
            r13 = 0
            r14 = -1
            r15 = r6
            r16 = r2
            int r15 = r15 - r16
            r16 = -1
            int r11 = r11.measureHeightOfChildrenCompat(r12, r13, r14, r15, r16)
            r8 = r11
            r11 = r8
            if (r11 <= 0) goto L_0x01ef
            r11 = r2
            r12 = r3
            int r11 = r11 + r12
            r2 = r11
        L_0x01ef:
            r11 = r8
            r12 = r2
            int r11 = r11 + r12
            r0 = r11
            goto L_0x0170
        L_0x01f5:
            r11 = r0
            android.content.Context r11 = r11.mContext
            android.content.res.Resources r11 = r11.getResources()
            android.util.DisplayMetrics r11 = r11.getDisplayMetrics()
            int r11 = r11.widthPixels
            r12 = r0
            android.graphics.Rect r12 = r12.mTempRect
            int r12 = r12.left
            r13 = r0
            android.graphics.Rect r13 = r13.mTempRect
            int r13 = r13.right
            int r12 = r12 + r13
            int r11 = r11 - r12
            r12 = -2147483648(0xffffffff80000000, float:-0.0)
            int r11 = android.view.View.MeasureSpec.makeMeasureSpec(r11, r12)
            r7 = r11
            goto L_0x01d6
        L_0x0216:
            r11 = r0
            android.content.Context r11 = r11.mContext
            android.content.res.Resources r11 = r11.getResources()
            android.util.DisplayMetrics r11 = r11.getDisplayMetrics()
            int r11 = r11.widthPixels
            r12 = r0
            android.graphics.Rect r12 = r12.mTempRect
            int r12 = r12.left
            r13 = r0
            android.graphics.Rect r13 = r13.mTempRect
            int r13 = r13.right
            int r12 = r12 + r13
            int r11 = r11 - r12
            r12 = 1073741824(0x40000000, float:2.0)
            int r11 = android.view.View.MeasureSpec.makeMeasureSpec(r11, r12)
            r7 = r11
            goto L_0x01d6
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ListPopupWindow.buildDropDown():int");
    }

    public static abstract class ForwardingListener implements View.OnTouchListener {
        private int mActivePointerId;
        private Runnable mDisallowIntercept;
        private boolean mForwarding;
        private final int mLongPressTimeout;
        private final float mScaledTouchSlop;
        /* access modifiers changed from: private */
        public final View mSrc;
        private final int mTapTimeout;
        private final int[] mTmpLocation = new int[2];
        private Runnable mTriggerLongPress;
        private boolean mWasLongPress;

        public abstract ListPopupWindow getPopup();

        public ForwardingListener(View view) {
            View view2 = view;
            this.mSrc = view2;
            this.mScaledTouchSlop = (float) ViewConfiguration.get(view2.getContext()).getScaledTouchSlop();
            this.mTapTimeout = ViewConfiguration.getTapTimeout();
            this.mLongPressTimeout = (this.mTapTimeout + ViewConfiguration.getLongPressTimeout()) / 2;
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            boolean z;
            boolean z2;
            MotionEvent motionEvent2 = motionEvent;
            boolean z3 = this.mForwarding;
            if (!z3) {
                z = onTouchObserved(motionEvent2) && onForwardingStarted();
                if (z) {
                    long uptimeMillis = SystemClock.uptimeMillis();
                    MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                    boolean onTouchEvent = this.mSrc.onTouchEvent(obtain);
                    obtain.recycle();
                }
            } else if (this.mWasLongPress) {
                z = onTouchForwarded(motionEvent2);
            } else {
                z = onTouchForwarded(motionEvent2) || !onForwardingStopped();
            }
            this.mForwarding = z;
            if (z || z3) {
                z2 = true;
            } else {
                z2 = false;
            }
            return z2;
        }

        /* access modifiers changed from: protected */
        public boolean onForwardingStarted() {
            ListPopupWindow popup = getPopup();
            if (popup != null && !popup.isShowing()) {
                popup.show();
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public boolean onForwardingStopped() {
            ListPopupWindow popup = getPopup();
            if (popup != null && popup.isShowing()) {
                popup.dismiss();
            }
            return true;
        }

        private boolean onTouchObserved(MotionEvent motionEvent) {
            Runnable runnable;
            Runnable runnable2;
            MotionEvent motionEvent2 = motionEvent;
            View view = this.mSrc;
            if (!view.isEnabled()) {
                return false;
            }
            switch (MotionEventCompat.getActionMasked(motionEvent2)) {
                case 0:
                    this.mActivePointerId = motionEvent2.getPointerId(0);
                    this.mWasLongPress = false;
                    if (this.mDisallowIntercept == null) {
                        new DisallowIntercept();
                        this.mDisallowIntercept = runnable2;
                    }
                    boolean postDelayed = view.postDelayed(this.mDisallowIntercept, (long) this.mTapTimeout);
                    if (this.mTriggerLongPress == null) {
                        new TriggerLongPress();
                        this.mTriggerLongPress = runnable;
                    }
                    boolean postDelayed2 = view.postDelayed(this.mTriggerLongPress, (long) this.mLongPressTimeout);
                    break;
                case 1:
                case 3:
                    clearCallbacks();
                    break;
                case 2:
                    int findPointerIndex = motionEvent2.findPointerIndex(this.mActivePointerId);
                    if (findPointerIndex >= 0) {
                        if (!pointInView(view, motionEvent2.getX(findPointerIndex), motionEvent2.getY(findPointerIndex), this.mScaledTouchSlop)) {
                            clearCallbacks();
                            view.getParent().requestDisallowInterceptTouchEvent(true);
                            return true;
                        }
                    }
                    break;
            }
            return false;
        }

        private void clearCallbacks() {
            if (this.mTriggerLongPress != null) {
                boolean removeCallbacks = this.mSrc.removeCallbacks(this.mTriggerLongPress);
            }
            if (this.mDisallowIntercept != null) {
                boolean removeCallbacks2 = this.mSrc.removeCallbacks(this.mDisallowIntercept);
            }
        }

        /* access modifiers changed from: private */
        public void onLongPress() {
            clearCallbacks();
            View view = this.mSrc;
            if (view.isEnabled() && !view.isLongClickable() && onForwardingStarted()) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                boolean onTouchEvent = view.onTouchEvent(obtain);
                obtain.recycle();
                this.mForwarding = true;
                this.mWasLongPress = true;
            }
        }

        private boolean onTouchForwarded(MotionEvent motionEvent) {
            MotionEvent motionEvent2 = motionEvent;
            View view = this.mSrc;
            ListPopupWindow popup = getPopup();
            if (popup == null || !popup.isShowing()) {
                return false;
            }
            DropDownListView access$600 = popup.mDropDownList;
            if (access$600 == null || !access$600.isShown()) {
                return false;
            }
            MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent2);
            boolean globalMotionEvent = toGlobalMotionEvent(view, obtainNoHistory);
            boolean localMotionEvent = toLocalMotionEvent(access$600, obtainNoHistory);
            boolean onForwardedEvent = access$600.onForwardedEvent(obtainNoHistory, this.mActivePointerId);
            obtainNoHistory.recycle();
            int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
            return onForwardedEvent && (actionMasked != 1 && actionMasked != 3);
        }

        private static boolean pointInView(View view, float f, float f2, float f3) {
            View view2 = view;
            float f4 = f;
            float f5 = f2;
            float f6 = f3;
            return f4 >= (-f6) && f5 >= (-f6) && f4 < ((float) (view2.getRight() - view2.getLeft())) + f6 && f5 < ((float) (view2.getBottom() - view2.getTop())) + f6;
        }

        private boolean toLocalMotionEvent(View view, MotionEvent motionEvent) {
            int[] iArr = this.mTmpLocation;
            view.getLocationOnScreen(iArr);
            motionEvent.offsetLocation((float) (-iArr[0]), (float) (-iArr[1]));
            return true;
        }

        private boolean toGlobalMotionEvent(View view, MotionEvent motionEvent) {
            int[] iArr = this.mTmpLocation;
            view.getLocationOnScreen(iArr);
            motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
            return true;
        }

        private class DisallowIntercept implements Runnable {
            private DisallowIntercept() {
            }

            public void run() {
                ForwardingListener.this.mSrc.getParent().requestDisallowInterceptTouchEvent(true);
            }
        }

        private class TriggerLongPress implements Runnable {
            private TriggerLongPress() {
            }

            public void run() {
                ForwardingListener.this.onLongPress();
            }
        }
    }

    private static class DropDownListView extends ListViewCompat {
        private ViewPropertyAnimatorCompat mClickAnimation;
        private boolean mDrawsInPressedState;
        private boolean mHijackFocus;
        private boolean mListSelectionHidden;
        private ListViewAutoScrollHelper mScrollHelper;

        static /* synthetic */ boolean access$502(DropDownListView dropDownListView, boolean z) {
            boolean z2 = z;
            boolean z3 = z2;
            dropDownListView.mListSelectionHidden = z3;
            return z2;
        }

        public DropDownListView(Context context, boolean z) {
            super(context, null, R.attr.dropDownListViewStyle);
            this.mHijackFocus = z;
            setCacheColorHint(0);
        }

        public boolean onForwardedEvent(MotionEvent motionEvent, int i) {
            ListViewAutoScrollHelper listViewAutoScrollHelper;
            MotionEvent motionEvent2 = motionEvent;
            int i2 = i;
            boolean z = true;
            boolean z2 = false;
            int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
            switch (actionMasked) {
                case 1:
                    z = false;
                case 2:
                    int findPointerIndex = motionEvent2.findPointerIndex(i2);
                    if (findPointerIndex >= 0) {
                        int x = (int) motionEvent2.getX(findPointerIndex);
                        int y = (int) motionEvent2.getY(findPointerIndex);
                        int pointToPosition = pointToPosition(x, y);
                        if (pointToPosition != -1) {
                            View childAt = getChildAt(pointToPosition - getFirstVisiblePosition());
                            setPressedItem(childAt, pointToPosition, (float) x, (float) y);
                            z = true;
                            if (actionMasked == 1) {
                                clickPressedItem(childAt, pointToPosition);
                                break;
                            }
                        } else {
                            z2 = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                    break;
                case 3:
                    z = false;
                    break;
            }
            if (!z || z2) {
                clearPressedItem();
            }
            if (z) {
                if (this.mScrollHelper == null) {
                    new ListViewAutoScrollHelper(this);
                    this.mScrollHelper = listViewAutoScrollHelper;
                }
                AutoScrollHelper enabled = this.mScrollHelper.setEnabled(true);
                boolean onTouch = this.mScrollHelper.onTouch(this, motionEvent2);
            } else if (this.mScrollHelper != null) {
                AutoScrollHelper enabled2 = this.mScrollHelper.setEnabled(false);
            }
            return z;
        }

        private void clickPressedItem(View view, int i) {
            int i2 = i;
            boolean performItemClick = performItemClick(view, i2, getItemIdAtPosition(i2));
        }

        private void clearPressedItem() {
            this.mDrawsInPressedState = false;
            setPressed(false);
            drawableStateChanged();
            View childAt = getChildAt(this.mMotionPosition - getFirstVisiblePosition());
            if (childAt != null) {
                childAt.setPressed(false);
            }
            if (this.mClickAnimation != null) {
                this.mClickAnimation.cancel();
                this.mClickAnimation = null;
            }
        }

        private void setPressedItem(View view, int i, float f, float f2) {
            View childAt;
            View view2 = view;
            int i2 = i;
            float f3 = f;
            float f4 = f2;
            this.mDrawsInPressedState = true;
            if (Build.VERSION.SDK_INT >= 21) {
                drawableHotspotChanged(f3, f4);
            }
            if (!isPressed()) {
                setPressed(true);
            }
            layoutChildren();
            if (!(this.mMotionPosition == -1 || (childAt = getChildAt(this.mMotionPosition - getFirstVisiblePosition())) == null || childAt == view2 || !childAt.isPressed())) {
                childAt.setPressed(false);
            }
            this.mMotionPosition = i2;
            float left = f3 - ((float) view2.getLeft());
            float top = f4 - ((float) view2.getTop());
            if (Build.VERSION.SDK_INT >= 21) {
                view2.drawableHotspotChanged(left, top);
            }
            if (!view2.isPressed()) {
                view2.setPressed(true);
            }
            setSelection(i2);
            positionSelectorLikeTouchCompat(i2, view2, f3, f4);
            setSelectorEnabled(false);
            refreshDrawableState();
        }

        /* access modifiers changed from: protected */
        public boolean touchModeDrawsInPressedStateCompat() {
            return this.mDrawsInPressedState || super.touchModeDrawsInPressedStateCompat();
        }

        public boolean isInTouchMode() {
            return (this.mHijackFocus && this.mListSelectionHidden) || super.isInTouchMode();
        }

        public boolean hasWindowFocus() {
            return this.mHijackFocus || super.hasWindowFocus();
        }

        public boolean isFocused() {
            return this.mHijackFocus || super.isFocused();
        }

        public boolean hasFocus() {
            return this.mHijackFocus || super.hasFocus();
        }
    }

    private class PopupDataSetObserver extends DataSetObserver {
        private PopupDataSetObserver() {
        }

        public void onChanged() {
            if (ListPopupWindow.this.isShowing()) {
                ListPopupWindow.this.show();
            }
        }

        public void onInvalidated() {
            ListPopupWindow.this.dismiss();
        }
    }

    private class ListSelectorHider implements Runnable {
        private ListSelectorHider() {
        }

        public void run() {
            ListPopupWindow.this.clearListSelection();
        }
    }

    private class ResizePopupRunnable implements Runnable {
        private ResizePopupRunnable() {
        }

        public void run() {
            if (ListPopupWindow.this.mDropDownList != null && ViewCompat.isAttachedToWindow(ListPopupWindow.this.mDropDownList) && ListPopupWindow.this.mDropDownList.getCount() > ListPopupWindow.this.mDropDownList.getChildCount() && ListPopupWindow.this.mDropDownList.getChildCount() <= ListPopupWindow.this.mListItemExpandMaximum) {
                ListPopupWindow.this.mPopup.setInputMethodMode(2);
                ListPopupWindow.this.show();
            }
        }
    }

    private class PopupTouchInterceptor implements View.OnTouchListener {
        private PopupTouchInterceptor() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            MotionEvent motionEvent2 = motionEvent;
            int action = motionEvent2.getAction();
            int x = (int) motionEvent2.getX();
            int y = (int) motionEvent2.getY();
            if (action == 0 && ListPopupWindow.this.mPopup != null && ListPopupWindow.this.mPopup.isShowing() && x >= 0 && x < ListPopupWindow.this.mPopup.getWidth() && y >= 0 && y < ListPopupWindow.this.mPopup.getHeight()) {
                boolean postDelayed = ListPopupWindow.this.mHandler.postDelayed(ListPopupWindow.this.mResizePopupRunnable, 250);
            } else if (action == 1) {
                ListPopupWindow.this.mHandler.removeCallbacks(ListPopupWindow.this.mResizePopupRunnable);
            }
            return false;
        }
    }

    private class PopupScrollListener implements AbsListView.OnScrollListener {
        private PopupScrollListener() {
        }

        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (i == 1 && !ListPopupWindow.this.isInputMethodNotNeeded() && ListPopupWindow.this.mPopup.getContentView() != null) {
                ListPopupWindow.this.mHandler.removeCallbacks(ListPopupWindow.this.mResizePopupRunnable);
                ListPopupWindow.this.mResizePopupRunnable.run();
            }
        }
    }

    private static boolean isConfirmKey(int i) {
        int i2 = i;
        return i2 == 66 || i2 == 23;
    }

    private void setPopupClipToScreenEnabled(boolean z) {
        boolean z2 = z;
        if (sClipToWindowEnabledMethod != null) {
            try {
                Object invoke = sClipToWindowEnabledMethod.invoke(this.mPopup, Boolean.valueOf(z2));
            } catch (Exception e) {
                int i = Log.i(TAG, "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
            }
        }
    }

    private int getMaxAvailableHeight(View view, int i, boolean z) {
        View view2 = view;
        int i2 = i;
        boolean z2 = z;
        if (sGetMaxAvailableHeightMethod != null) {
            try {
                Method method = sGetMaxAvailableHeightMethod;
                PopupWindow popupWindow = this.mPopup;
                Object[] objArr = new Object[3];
                objArr[0] = view2;
                Object[] objArr2 = objArr;
                objArr2[1] = Integer.valueOf(i2);
                Object[] objArr3 = objArr2;
                objArr3[2] = Boolean.valueOf(z2);
                return ((Integer) method.invoke(popupWindow, objArr3)).intValue();
            } catch (Exception e) {
                int i3 = Log.i(TAG, "Could not call getMaxAvailableHeightMethod(View, int, boolean) on PopupWindow. Using the public version.");
            }
        }
        return this.mPopup.getMaxAvailableHeight(view2, i2);
    }
}
