package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.support.design.widget.ValueAnimatorCompat;
import android.view.animation.Interpolator;

class ValueAnimatorCompatImplHoneycombMr1 extends ValueAnimatorCompat.Impl {
    final ValueAnimator mValueAnimator;

    ValueAnimatorCompatImplHoneycombMr1() {
        ValueAnimator valueAnimator;
        new ValueAnimator();
        this.mValueAnimator = valueAnimator;
    }

    public void start() {
        this.mValueAnimator.start();
    }

    public boolean isRunning() {
        return this.mValueAnimator.isRunning();
    }

    public void setInterpolator(Interpolator interpolator) {
        this.mValueAnimator.setInterpolator(interpolator);
    }

    public void setUpdateListener(ValueAnimatorCompat.Impl.AnimatorUpdateListenerProxy animatorUpdateListenerProxy) {
        ValueAnimator.AnimatorUpdateListener animatorUpdateListener;
        final ValueAnimatorCompat.Impl.AnimatorUpdateListenerProxy animatorUpdateListenerProxy2 = animatorUpdateListenerProxy;
        new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                animatorUpdateListenerProxy2.onAnimationUpdate();
            }
        };
        this.mValueAnimator.addUpdateListener(animatorUpdateListener);
    }

    public void setListener(ValueAnimatorCompat.Impl.AnimatorListenerProxy animatorListenerProxy) {
        Animator.AnimatorListener animatorListener;
        final ValueAnimatorCompat.Impl.AnimatorListenerProxy animatorListenerProxy2 = animatorListenerProxy;
        new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animator) {
                animatorListenerProxy2.onAnimationStart();
            }

            public void onAnimationEnd(Animator animator) {
                animatorListenerProxy2.onAnimationEnd();
            }

            public void onAnimationCancel(Animator animator) {
                animatorListenerProxy2.onAnimationCancel();
            }
        };
        this.mValueAnimator.addListener(animatorListener);
    }

    public void setIntValues(int i, int i2) {
        ValueAnimator valueAnimator = this.mValueAnimator;
        int[] iArr = new int[2];
        iArr[0] = i;
        int[] iArr2 = iArr;
        iArr2[1] = i2;
        valueAnimator.setIntValues(iArr2);
    }

    public int getAnimatedIntValue() {
        return ((Integer) this.mValueAnimator.getAnimatedValue()).intValue();
    }

    public void setFloatValues(float f, float f2) {
        ValueAnimator valueAnimator = this.mValueAnimator;
        float[] fArr = new float[2];
        fArr[0] = f;
        float[] fArr2 = fArr;
        fArr2[1] = f2;
        valueAnimator.setFloatValues(fArr2);
    }

    public float getAnimatedFloatValue() {
        return ((Float) this.mValueAnimator.getAnimatedValue()).floatValue();
    }

    public void setDuration(int i) {
        ValueAnimator duration = this.mValueAnimator.setDuration((long) i);
    }

    public void cancel() {
        this.mValueAnimator.cancel();
    }

    public float getAnimatedFraction() {
        return this.mValueAnimator.getAnimatedFraction();
    }

    public void end() {
        this.mValueAnimator.end();
    }

    public long getDuration() {
        return this.mValueAnimator.getDuration();
    }
}
