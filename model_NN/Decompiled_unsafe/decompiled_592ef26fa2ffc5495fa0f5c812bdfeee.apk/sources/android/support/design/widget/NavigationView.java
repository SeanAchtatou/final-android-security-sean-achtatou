package android.support.design.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.design.R;
import android.support.design.internal.NavigationMenu;
import android.support.design.internal.NavigationMenuPresenter;
import android.support.design.internal.ScrimInsetsFrameLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v7.view.SupportMenuInflater;
import android.support.v7.view.menu.MenuItemImpl;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class NavigationView extends ScrimInsetsFrameLayout {
    private static final int[] CHECKED_STATE_SET = {16842912};
    private static final int[] DISABLED_STATE_SET = {-16842910};
    private static final int PRESENTER_NAVIGATION_VIEW_ID = 1;
    /* access modifiers changed from: private */
    public OnNavigationItemSelectedListener mListener;
    private int mMaxWidth;
    private final NavigationMenu mMenu;
    private MenuInflater mMenuInflater;
    private final NavigationMenuPresenter mPresenter;

    public interface OnNavigationItemSelectedListener {
        boolean onNavigationItemSelected(MenuItem menuItem);
    }

    public NavigationView(Context context) {
        this(context, null);
    }

    public NavigationView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public NavigationView(android.content.Context r17, android.util.AttributeSet r18, int r19) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            r3 = r19
            r10 = r0
            r11 = r1
            r12 = r2
            r13 = r3
            r10.<init>(r11, r12, r13)
            r10 = r0
            android.support.design.internal.NavigationMenuPresenter r11 = new android.support.design.internal.NavigationMenuPresenter
            r15 = r11
            r11 = r15
            r12 = r15
            r12.<init>()
            r10.mPresenter = r11
            r10 = r1
            android.support.design.widget.ThemeUtils.checkAppCompatTheme(r10)
            r10 = r0
            android.support.design.internal.NavigationMenu r11 = new android.support.design.internal.NavigationMenu
            r15 = r11
            r11 = r15
            r12 = r15
            r13 = r1
            r12.<init>(r13)
            r10.mMenu = r11
            r10 = r1
            r11 = r2
            int[] r12 = android.support.design.R.styleable.NavigationView
            r13 = r3
            int r14 = android.support.design.R.style.Widget_Design_NavigationView
            android.content.res.TypedArray r10 = r10.obtainStyledAttributes(r11, r12, r13, r14)
            r4 = r10
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.NavigationView_android_background
            android.graphics.drawable.Drawable r11 = r11.getDrawable(r12)
            r10.setBackgroundDrawable(r11)
            r10 = r4
            int r11 = android.support.design.R.styleable.NavigationView_elevation
            boolean r10 = r10.hasValue(r11)
            if (r10 == 0) goto L_0x0057
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.NavigationView_elevation
            r13 = 0
            int r11 = r11.getDimensionPixelSize(r12, r13)
            float r11 = (float) r11
            android.support.v4.view.ViewCompat.setElevation(r10, r11)
        L_0x0057:
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.NavigationView_android_fitsSystemWindows
            r13 = 0
            boolean r11 = r11.getBoolean(r12, r13)
            android.support.v4.view.ViewCompat.setFitsSystemWindows(r10, r11)
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.NavigationView_android_maxWidth
            r13 = 0
            int r11 = r11.getDimensionPixelSize(r12, r13)
            r10.mMaxWidth = r11
            r10 = r4
            int r11 = android.support.design.R.styleable.NavigationView_itemIconTint
            boolean r10 = r10.hasValue(r11)
            if (r10 == 0) goto L_0x0147
            r10 = r4
            int r11 = android.support.design.R.styleable.NavigationView_itemIconTint
            android.content.res.ColorStateList r10 = r10.getColorStateList(r11)
            r5 = r10
        L_0x007f:
            r10 = 0
            r6 = r10
            r10 = 0
            r7 = r10
            r10 = r4
            int r11 = android.support.design.R.styleable.NavigationView_itemTextAppearance
            boolean r10 = r10.hasValue(r11)
            if (r10 == 0) goto L_0x0097
            r10 = r4
            int r11 = android.support.design.R.styleable.NavigationView_itemTextAppearance
            r12 = 0
            int r10 = r10.getResourceId(r11, r12)
            r7 = r10
            r10 = 1
            r6 = r10
        L_0x0097:
            r10 = 0
            r8 = r10
            r10 = r4
            int r11 = android.support.design.R.styleable.NavigationView_itemTextColor
            boolean r10 = r10.hasValue(r11)
            if (r10 == 0) goto L_0x00aa
            r10 = r4
            int r11 = android.support.design.R.styleable.NavigationView_itemTextColor
            android.content.res.ColorStateList r10 = r10.getColorStateList(r11)
            r8 = r10
        L_0x00aa:
            r10 = r6
            if (r10 != 0) goto L_0x00b9
            r10 = r8
            if (r10 != 0) goto L_0x00b9
            r10 = r0
            r11 = 16842806(0x1010036, float:2.369371E-38)
            android.content.res.ColorStateList r10 = r10.createDefaultColorStateList(r11)
            r8 = r10
        L_0x00b9:
            r10 = r4
            int r11 = android.support.design.R.styleable.NavigationView_itemBackground
            android.graphics.drawable.Drawable r10 = r10.getDrawable(r11)
            r9 = r10
            r10 = r0
            android.support.design.internal.NavigationMenu r10 = r10.mMenu
            android.support.design.widget.NavigationView$1 r11 = new android.support.design.widget.NavigationView$1
            r15 = r11
            r11 = r15
            r12 = r15
            r13 = r0
            r12.<init>()
            r10.setCallback(r11)
            r10 = r0
            android.support.design.internal.NavigationMenuPresenter r10 = r10.mPresenter
            r11 = 1
            r10.setId(r11)
            r10 = r0
            android.support.design.internal.NavigationMenuPresenter r10 = r10.mPresenter
            r11 = r1
            r12 = r0
            android.support.design.internal.NavigationMenu r12 = r12.mMenu
            r10.initForMenu(r11, r12)
            r10 = r0
            android.support.design.internal.NavigationMenuPresenter r10 = r10.mPresenter
            r11 = r5
            r10.setItemIconTintList(r11)
            r10 = r6
            if (r10 == 0) goto L_0x00f2
            r10 = r0
            android.support.design.internal.NavigationMenuPresenter r10 = r10.mPresenter
            r11 = r7
            r10.setItemTextAppearance(r11)
        L_0x00f2:
            r10 = r0
            android.support.design.internal.NavigationMenuPresenter r10 = r10.mPresenter
            r11 = r8
            r10.setItemTextColor(r11)
            r10 = r0
            android.support.design.internal.NavigationMenuPresenter r10 = r10.mPresenter
            r11 = r9
            r10.setItemBackground(r11)
            r10 = r0
            android.support.design.internal.NavigationMenu r10 = r10.mMenu
            r11 = r0
            android.support.design.internal.NavigationMenuPresenter r11 = r11.mPresenter
            r10.addMenuPresenter(r11)
            r10 = r0
            r11 = r0
            android.support.design.internal.NavigationMenuPresenter r11 = r11.mPresenter
            r12 = r0
            android.support.v7.view.menu.MenuView r11 = r11.getMenuView(r12)
            android.view.View r11 = (android.view.View) r11
            r10.addView(r11)
            r10 = r4
            int r11 = android.support.design.R.styleable.NavigationView_menu
            boolean r10 = r10.hasValue(r11)
            if (r10 == 0) goto L_0x012c
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.NavigationView_menu
            r13 = 0
            int r11 = r11.getResourceId(r12, r13)
            r10.inflateMenu(r11)
        L_0x012c:
            r10 = r4
            int r11 = android.support.design.R.styleable.NavigationView_headerLayout
            boolean r10 = r10.hasValue(r11)
            if (r10 == 0) goto L_0x0142
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.NavigationView_headerLayout
            r13 = 0
            int r11 = r11.getResourceId(r12, r13)
            android.view.View r10 = r10.inflateHeaderView(r11)
        L_0x0142:
            r10 = r4
            r10.recycle()
            return
        L_0x0147:
            r10 = r0
            r11 = 16842808(0x1010038, float:2.3693715E-38)
            android.content.res.ColorStateList r10 = r10.createDefaultColorStateList(r11)
            r5 = r10
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.NavigationView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState;
        Bundle bundle;
        new SavedState(super.onSaveInstanceState());
        SavedState savedState2 = savedState;
        new Bundle();
        savedState2.menuState = bundle;
        this.mMenu.savePresenterStates(savedState2.menuState);
        return savedState2;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.mMenu.restorePresenterStates(savedState.menuState);
    }

    public void setNavigationItemSelectedListener(OnNavigationItemSelectedListener onNavigationItemSelectedListener) {
        this.mListener = onNavigationItemSelectedListener;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        switch (View.MeasureSpec.getMode(i3)) {
            case Integer.MIN_VALUE:
                i3 = View.MeasureSpec.makeMeasureSpec(Math.min(View.MeasureSpec.getSize(i3), this.mMaxWidth), 1073741824);
                break;
            case 0:
                i3 = View.MeasureSpec.makeMeasureSpec(this.mMaxWidth, 1073741824);
                break;
        }
        super.onMeasure(i3, i4);
    }

    public void inflateMenu(int i) {
        this.mPresenter.setUpdateSuspended(true);
        getMenuInflater().inflate(i, this.mMenu);
        this.mPresenter.setUpdateSuspended(false);
        this.mPresenter.updateMenuView(false);
    }

    public Menu getMenu() {
        return this.mMenu;
    }

    public View inflateHeaderView(@LayoutRes int i) {
        return this.mPresenter.inflateHeaderView(i);
    }

    public void addHeaderView(@NonNull View view) {
        this.mPresenter.addHeaderView(view);
    }

    public void removeHeaderView(@NonNull View view) {
        this.mPresenter.removeHeaderView(view);
    }

    public int getHeaderCount() {
        return this.mPresenter.getHeaderCount();
    }

    public View getHeaderView(int i) {
        return this.mPresenter.getHeaderView(i);
    }

    @Nullable
    public ColorStateList getItemIconTintList() {
        return this.mPresenter.getItemTintList();
    }

    public void setItemIconTintList(@Nullable ColorStateList colorStateList) {
        this.mPresenter.setItemIconTintList(colorStateList);
    }

    @Nullable
    public ColorStateList getItemTextColor() {
        return this.mPresenter.getItemTextColor();
    }

    public void setItemTextColor(@Nullable ColorStateList colorStateList) {
        this.mPresenter.setItemTextColor(colorStateList);
    }

    public Drawable getItemBackground() {
        return this.mPresenter.getItemBackground();
    }

    public void setItemBackgroundResource(@DrawableRes int i) {
        setItemBackground(ContextCompat.getDrawable(getContext(), i));
    }

    public void setItemBackground(Drawable drawable) {
        this.mPresenter.setItemBackground(drawable);
    }

    public void setCheckedItem(@IdRes int i) {
        MenuItem findItem = this.mMenu.findItem(i);
        if (findItem != null) {
            this.mPresenter.setCheckedItem((MenuItemImpl) findItem);
        }
    }

    public void setItemTextAppearance(@StyleRes int i) {
        this.mPresenter.setItemTextAppearance(i);
    }

    private MenuInflater getMenuInflater() {
        MenuInflater menuInflater;
        if (this.mMenuInflater == null) {
            new SupportMenuInflater(getContext());
            this.mMenuInflater = menuInflater;
        }
        return this.mMenuInflater;
    }

    private ColorStateList createDefaultColorStateList(int i) {
        TypedValue typedValue;
        ColorStateList colorStateList;
        new TypedValue();
        TypedValue typedValue2 = typedValue;
        if (!getContext().getTheme().resolveAttribute(i, typedValue2, true)) {
            return null;
        }
        ColorStateList colorStateList2 = getResources().getColorStateList(typedValue2.resourceId);
        if (!getContext().getTheme().resolveAttribute(R.attr.colorPrimary, typedValue2, true)) {
            return null;
        }
        int i2 = typedValue2.data;
        int defaultColor = colorStateList2.getDefaultColor();
        ColorStateList colorStateList3 = colorStateList;
        int[][] iArr = new int[3][];
        iArr[0] = DISABLED_STATE_SET;
        int[][] iArr2 = iArr;
        iArr2[1] = CHECKED_STATE_SET;
        int[][] iArr3 = iArr2;
        int[][] iArr4 = iArr3;
        iArr3[2] = EMPTY_STATE_SET;
        int[] iArr5 = new int[3];
        iArr5[0] = colorStateList2.getColorForState(DISABLED_STATE_SET, defaultColor);
        int[] iArr6 = iArr5;
        iArr6[1] = i2;
        int[] iArr7 = iArr6;
        iArr7[2] = defaultColor;
        new ColorStateList(iArr4, iArr7);
        return colorStateList3;
    }

    public static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR;
        public Bundle menuState;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public SavedState(android.os.Parcel r7, java.lang.ClassLoader r8) {
            /*
                r6 = this;
                r0 = r6
                r1 = r7
                r2 = r8
                r3 = r0
                r4 = r1
                r3.<init>(r4)
                r3 = r0
                r4 = r1
                r5 = r2
                android.os.Bundle r4 = r4.readBundle(r5)
                r3.menuState = r4
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.NavigationView.SavedState.<init>(android.os.Parcel, java.lang.ClassLoader):void");
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(@NonNull Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            super.writeToParcel(parcel2, i);
            parcel2.writeBundle(this.menuState);
        }

        static {
            Object obj;
            new ParcelableCompatCreatorCallbacks<SavedState>() {
                public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    SavedState savedState;
                    new SavedState(parcel, classLoader);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = ParcelableCompat.newCreator(obj);
        }
    }
}
