package android.support.design.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.R;

class ThemeUtils {
    private static final int[] APPCOMPAT_CHECK_ATTRS = {R.attr.colorPrimary};

    ThemeUtils() {
    }

    static void checkAppCompatTheme(Context context) {
        Throwable th;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(APPCOMPAT_CHECK_ATTRS);
        boolean z = !obtainStyledAttributes.hasValue(0);
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.recycle();
        }
        if (z) {
            Throwable th2 = th;
            new IllegalArgumentException("You need to use a Theme.AppCompat theme (or descendant) with the design library.");
            throw th2;
        }
    }
}
