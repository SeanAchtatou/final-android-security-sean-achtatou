package android.support.design.widget;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import java.util.List;

abstract class HeaderScrollingViewBehavior extends ViewOffsetBehavior<View> {
    /* access modifiers changed from: package-private */
    public abstract View findFirstDependency(List<View> list);

    public HeaderScrollingViewBehavior() {
    }

    public HeaderScrollingViewBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public boolean onMeasureChild(CoordinatorLayout coordinatorLayout, View view, int i, int i2, int i3, int i4) {
        CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
        View view2 = view;
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        int i9 = view2.getLayoutParams().height;
        if (i9 == -1 || i9 == -2) {
            List<View> dependencies = coordinatorLayout2.getDependencies(view2);
            if (dependencies.isEmpty()) {
                return false;
            }
            View findFirstDependency = findFirstDependency(dependencies);
            if (findFirstDependency != null && ViewCompat.isLaidOut(findFirstDependency)) {
                if (ViewCompat.getFitsSystemWindows(findFirstDependency)) {
                    ViewCompat.setFitsSystemWindows(view2, true);
                }
                int size = View.MeasureSpec.getSize(i7);
                if (size == 0) {
                    size = coordinatorLayout2.getHeight();
                }
                coordinatorLayout2.onMeasureChild(view2, i5, i6, View.MeasureSpec.makeMeasureSpec((size - findFirstDependency.getMeasuredHeight()) + getScrollRange(findFirstDependency), i9 == -1 ? 1073741824 : Integer.MIN_VALUE), i8);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public int getScrollRange(View view) {
        return view.getMeasuredHeight();
    }
}
