package android.support.design.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.Nullable;
import android.support.design.R;
import android.support.design.widget.AnimationUtils;
import android.support.design.widget.FloatingActionButtonImpl;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;

class FloatingActionButtonEclairMr1 extends FloatingActionButtonImpl {
    private int mAnimationDuration;
    CircularBorderDrawable mBorderDrawable;
    /* access modifiers changed from: private */
    public float mElevation;
    private boolean mIsHiding;
    /* access modifiers changed from: private */
    public float mPressedTranslationZ;
    Drawable mRippleDrawable;
    ShadowDrawableWrapper mShadowDrawable;
    Drawable mShapeDrawable;
    private StateListAnimator mStateListAnimator;

    static /* synthetic */ boolean access$202(FloatingActionButtonEclairMr1 floatingActionButtonEclairMr1, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        floatingActionButtonEclairMr1.mIsHiding = z3;
        return z2;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    FloatingActionButtonEclairMr1(android.view.View r12, android.support.design.widget.ShadowViewDelegate r13) {
        /*
            r11 = this;
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r0
            r4 = r1
            r5 = r2
            r3.<init>(r4, r5)
            r3 = r0
            r4 = r1
            android.content.res.Resources r4 = r4.getResources()
            r5 = 17694720(0x10e0000, float:2.608128E-38)
            int r4 = r4.getInteger(r5)
            r3.mAnimationDuration = r4
            r3 = r0
            android.support.design.widget.StateListAnimator r4 = new android.support.design.widget.StateListAnimator
            r10 = r4
            r4 = r10
            r5 = r10
            r5.<init>()
            r3.mStateListAnimator = r4
            r3 = r0
            android.support.design.widget.StateListAnimator r3 = r3.mStateListAnimator
            r4 = r1
            r3.setTarget(r4)
            r3 = r0
            android.support.design.widget.StateListAnimator r3 = r3.mStateListAnimator
            int[] r4 = android.support.design.widget.FloatingActionButtonEclairMr1.PRESSED_ENABLED_STATE_SET
            r5 = r0
            android.support.design.widget.FloatingActionButtonEclairMr1$ElevateToTranslationZAnimation r6 = new android.support.design.widget.FloatingActionButtonEclairMr1$ElevateToTranslationZAnimation
            r10 = r6
            r6 = r10
            r7 = r10
            r8 = r0
            r9 = 0
            r7.<init>()
            android.view.animation.Animation r5 = r5.setupAnimation(r6)
            r3.addState(r4, r5)
            r3 = r0
            android.support.design.widget.StateListAnimator r3 = r3.mStateListAnimator
            int[] r4 = android.support.design.widget.FloatingActionButtonEclairMr1.FOCUSED_ENABLED_STATE_SET
            r5 = r0
            android.support.design.widget.FloatingActionButtonEclairMr1$ElevateToTranslationZAnimation r6 = new android.support.design.widget.FloatingActionButtonEclairMr1$ElevateToTranslationZAnimation
            r10 = r6
            r6 = r10
            r7 = r10
            r8 = r0
            r9 = 0
            r7.<init>()
            android.view.animation.Animation r5 = r5.setupAnimation(r6)
            r3.addState(r4, r5)
            r3 = r0
            android.support.design.widget.StateListAnimator r3 = r3.mStateListAnimator
            int[] r4 = android.support.design.widget.FloatingActionButtonEclairMr1.EMPTY_STATE_SET
            r5 = r0
            android.support.design.widget.FloatingActionButtonEclairMr1$ResetElevationAnimation r6 = new android.support.design.widget.FloatingActionButtonEclairMr1$ResetElevationAnimation
            r10 = r6
            r6 = r10
            r7 = r10
            r8 = r0
            r9 = 0
            r7.<init>()
            android.view.animation.Animation r5 = r5.setupAnimation(r6)
            r3.addState(r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.FloatingActionButtonEclairMr1.<init>(android.view.View, android.support.design.widget.ShadowViewDelegate):void");
    }

    /* access modifiers changed from: package-private */
    public void setBackgroundDrawable(ColorStateList colorStateList, PorterDuff.Mode mode, int i, int i2) {
        Drawable[] drawableArr;
        ShadowDrawableWrapper shadowDrawableWrapper;
        Drawable drawable;
        ColorStateList colorStateList2 = colorStateList;
        PorterDuff.Mode mode2 = mode;
        int i3 = i;
        int i4 = i2;
        this.mShapeDrawable = DrawableCompat.wrap(createShapeDrawable());
        DrawableCompat.setTintList(this.mShapeDrawable, colorStateList2);
        if (mode2 != null) {
            DrawableCompat.setTintMode(this.mShapeDrawable, mode2);
        }
        this.mRippleDrawable = DrawableCompat.wrap(createShapeDrawable());
        DrawableCompat.setTintList(this.mRippleDrawable, createColorStateList(i3));
        DrawableCompat.setTintMode(this.mRippleDrawable, PorterDuff.Mode.MULTIPLY);
        if (i4 > 0) {
            this.mBorderDrawable = createBorderDrawable(i4, colorStateList2);
            Drawable[] drawableArr2 = new Drawable[3];
            drawableArr2[0] = this.mBorderDrawable;
            Drawable[] drawableArr3 = drawableArr2;
            drawableArr3[1] = this.mShapeDrawable;
            Drawable[] drawableArr4 = drawableArr3;
            drawableArr4[2] = this.mRippleDrawable;
            drawableArr = drawableArr4;
        } else {
            this.mBorderDrawable = null;
            Drawable[] drawableArr5 = new Drawable[2];
            drawableArr5[0] = this.mShapeDrawable;
            Drawable[] drawableArr6 = drawableArr5;
            drawableArr6[1] = this.mRippleDrawable;
            drawableArr = drawableArr6;
        }
        new LayerDrawable(drawableArr);
        new ShadowDrawableWrapper(this.mView.getResources(), drawable, this.mShadowViewDelegate.getRadius(), this.mElevation, this.mElevation + this.mPressedTranslationZ);
        this.mShadowDrawable = shadowDrawableWrapper;
        this.mShadowDrawable.setAddPaddingForCorners(false);
        this.mShadowViewDelegate.setBackgroundDrawable(this.mShadowDrawable);
        updatePadding();
    }

    /* access modifiers changed from: package-private */
    public void setBackgroundTintList(ColorStateList colorStateList) {
        ColorStateList colorStateList2 = colorStateList;
        DrawableCompat.setTintList(this.mShapeDrawable, colorStateList2);
        if (this.mBorderDrawable != null) {
            this.mBorderDrawable.setBorderTint(colorStateList2);
        }
    }

    /* access modifiers changed from: package-private */
    public void setBackgroundTintMode(PorterDuff.Mode mode) {
        DrawableCompat.setTintMode(this.mShapeDrawable, mode);
    }

    /* access modifiers changed from: package-private */
    public void setRippleColor(int i) {
        DrawableCompat.setTintList(this.mRippleDrawable, createColorStateList(i));
    }

    /* access modifiers changed from: package-private */
    public void setElevation(float f) {
        float f2 = f;
        if (this.mElevation != f2 && this.mShadowDrawable != null) {
            this.mShadowDrawable.setShadowSize(f2, f2 + this.mPressedTranslationZ);
            this.mElevation = f2;
            updatePadding();
        }
    }

    /* access modifiers changed from: package-private */
    public void setPressedTranslationZ(float f) {
        float f2 = f;
        if (this.mPressedTranslationZ != f2 && this.mShadowDrawable != null) {
            this.mPressedTranslationZ = f2;
            this.mShadowDrawable.setMaxShadowSize(this.mElevation + f2);
            updatePadding();
        }
    }

    /* access modifiers changed from: package-private */
    public void onDrawableStateChanged(int[] iArr) {
        this.mStateListAnimator.setState(iArr);
    }

    /* access modifiers changed from: package-private */
    public void jumpDrawableToCurrentState() {
        this.mStateListAnimator.jumpToCurrentState();
    }

    /* access modifiers changed from: package-private */
    public void hide(@Nullable FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener) {
        Animation.AnimationListener animationListener;
        FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener2 = internalVisibilityChangedListener;
        if (!this.mIsHiding && this.mView.getVisibility() == 0) {
            Animation loadAnimation = AnimationUtils.loadAnimation(this.mView.getContext(), R.anim.design_fab_out);
            loadAnimation.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
            loadAnimation.setDuration(200);
            final FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener3 = internalVisibilityChangedListener2;
            new AnimationUtils.AnimationListenerAdapter() {
                public void onAnimationStart(Animation animation) {
                    boolean access$202 = FloatingActionButtonEclairMr1.access$202(FloatingActionButtonEclairMr1.this, true);
                }

                public void onAnimationEnd(Animation animation) {
                    boolean access$202 = FloatingActionButtonEclairMr1.access$202(FloatingActionButtonEclairMr1.this, false);
                    FloatingActionButtonEclairMr1.this.mView.setVisibility(8);
                    if (internalVisibilityChangedListener3 != null) {
                        internalVisibilityChangedListener3.onHidden();
                    }
                }
            };
            loadAnimation.setAnimationListener(animationListener);
            this.mView.startAnimation(loadAnimation);
        } else if (internalVisibilityChangedListener2 != null) {
            internalVisibilityChangedListener2.onHidden();
        }
    }

    /* access modifiers changed from: package-private */
    public void show(@Nullable FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener) {
        Animation.AnimationListener animationListener;
        FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener2 = internalVisibilityChangedListener;
        if (this.mView.getVisibility() != 0 || this.mIsHiding) {
            this.mView.clearAnimation();
            this.mView.setVisibility(0);
            Animation loadAnimation = android.view.animation.AnimationUtils.loadAnimation(this.mView.getContext(), R.anim.design_fab_in);
            loadAnimation.setDuration(200);
            loadAnimation.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
            final FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener3 = internalVisibilityChangedListener2;
            new AnimationUtils.AnimationListenerAdapter() {
                public void onAnimationEnd(Animation animation) {
                    if (internalVisibilityChangedListener3 != null) {
                        internalVisibilityChangedListener3.onShown();
                    }
                }
            };
            loadAnimation.setAnimationListener(animationListener);
            this.mView.startAnimation(loadAnimation);
        } else if (internalVisibilityChangedListener2 != null) {
            internalVisibilityChangedListener2.onShown();
        }
    }

    private void updatePadding() {
        Rect rect;
        new Rect();
        Rect rect2 = rect;
        boolean padding = this.mShadowDrawable.getPadding(rect2);
        this.mShadowViewDelegate.setShadowPadding(rect2.left, rect2.top, rect2.right, rect2.bottom);
    }

    private Animation setupAnimation(Animation animation) {
        Animation animation2 = animation;
        animation2.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
        animation2.setDuration((long) this.mAnimationDuration);
        return animation2;
    }

    private abstract class BaseShadowAnimation extends Animation {
        private float mShadowSizeDiff;
        private float mShadowSizeStart;

        /* access modifiers changed from: protected */
        public abstract float getTargetShadowSize();

        private BaseShadowAnimation() {
        }

        public void reset() {
            super.reset();
            this.mShadowSizeStart = FloatingActionButtonEclairMr1.this.mShadowDrawable.getShadowSize();
            this.mShadowSizeDiff = getTargetShadowSize() - this.mShadowSizeStart;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f, Transformation transformation) {
            FloatingActionButtonEclairMr1.this.mShadowDrawable.setShadowSize(this.mShadowSizeStart + (this.mShadowSizeDiff * f));
        }
    }

    private class ResetElevationAnimation extends BaseShadowAnimation {
        final /* synthetic */ FloatingActionButtonEclairMr1 this$0;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private ResetElevationAnimation(android.support.design.widget.FloatingActionButtonEclairMr1 r6) {
            /*
                r5 = this;
                r0 = r5
                r1 = r6
                r2 = r0
                r3 = r1
                r2.this$0 = r3
                r2 = r0
                r3 = r1
                r4 = 0
                r2.<init>()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.FloatingActionButtonEclairMr1.ResetElevationAnimation.<init>(android.support.design.widget.FloatingActionButtonEclairMr1):void");
        }

        /* access modifiers changed from: protected */
        public float getTargetShadowSize() {
            return this.this$0.mElevation;
        }
    }

    private class ElevateToTranslationZAnimation extends BaseShadowAnimation {
        final /* synthetic */ FloatingActionButtonEclairMr1 this$0;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private ElevateToTranslationZAnimation(android.support.design.widget.FloatingActionButtonEclairMr1 r6) {
            /*
                r5 = this;
                r0 = r5
                r1 = r6
                r2 = r0
                r3 = r1
                r2.this$0 = r3
                r2 = r0
                r3 = r1
                r4 = 0
                r2.<init>()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.FloatingActionButtonEclairMr1.ElevateToTranslationZAnimation.<init>(android.support.design.widget.FloatingActionButtonEclairMr1):void");
        }

        /* access modifiers changed from: protected */
        public float getTargetShadowSize() {
            return this.this$0.mElevation + this.this$0.mPressedTranslationZ;
        }
    }

    private static ColorStateList createColorStateList(int i) {
        ColorStateList colorStateList;
        int i2 = i;
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        iArr[0] = FOCUSED_ENABLED_STATE_SET;
        iArr2[0] = i2;
        int i3 = 0 + 1;
        iArr[i3] = PRESSED_ENABLED_STATE_SET;
        iArr2[i3] = i2;
        int i4 = i3 + 1;
        iArr[i4] = new int[0];
        iArr2[i4] = 0;
        int i5 = i4 + 1;
        new ColorStateList(iArr, iArr2);
        return colorStateList;
    }
}
