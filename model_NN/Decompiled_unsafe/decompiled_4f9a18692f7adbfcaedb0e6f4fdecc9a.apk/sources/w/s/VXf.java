package w.s;

import android.app.Application;
import android.content.Context;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;

public final class VXf extends Application {
    private static final String a = "hello.apk";
    private static final String b = "demo.innerappshell.LoadTargetApp";
    private Class<?> c;

    private void a(Context context, String str) {
        Method declaredMethod = this.c.getDeclaredMethod(str, Context.class);
        declaredMethod.setAccessible(true);
        declaredMethod.invoke(null, context);
    }

    private static String c(Context context) {
        File file = new File(context.getFilesDir().getAbsolutePath(), a);
        if (file.exists() || !file.createNewFile()) {
        }
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        InputStream open = context.getAssets().open(a);
        byte[] bArr = new byte[1024];
        while (true) {
            int read = open.read(bArr);
            if (read != -1) {
                for (int i = 0; i < read; i++) {
                    bArr[i] = (byte) (bArr[i] - 1);
                }
                fileOutputStream.write(bArr, 0, read);
            } else {
                open.close();
                fileOutputStream.close();
                return file.getAbsolutePath();
            }
        }
    }

    private static String d(Context context) {
        File file = new File(context.getFilesDir().getAbsolutePath(), "hello");
        if (file.exists() || !file.mkdir()) {
        }
        return file.getAbsolutePath();
    }

    public void a(Context context) {
        this.c = new DexClassLoader(c(context), d(context), null, context.getClassLoader()).loadClass(b);
        a(context, "load");
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        try {
            a(this);
        } catch (Exception e) {
        }
    }

    public void b(Context context) {
        a(context, "initCreate");
    }

    public void onCreate() {
        super.onCreate();
        try {
            b(this);
        } catch (Exception e) {
        }
    }
}
