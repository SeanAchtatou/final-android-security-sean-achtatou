package com.waps;

import android.util.Log;

public class AppLog {
    private static boolean showLog = false;

    public static void d(String str, String str2) {
        if (showLog) {
            Log.d(str, str2);
        }
    }

    public static void e(String str, String str2) {
        if (showLog) {
            Log.e(str, str2);
        }
    }

    public static void e(String str, String str2, Throwable th) {
        if (showLog) {
            Log.e(str, str2, th);
        }
    }

    public static void enableLogging(boolean z) {
        showLog = z;
    }

    public static void i(String str, String str2) {
        if (showLog) {
            Log.i(str, str2);
        }
    }

    public static void v(String str, String str2) {
        if (showLog) {
            Log.v(str, str2);
        }
    }

    public static void w(String str, String str2) {
        if (showLog) {
            Log.w(str, str2);
        }
    }

    public static void w(String str, String str2, Throwable th) {
        if (showLog) {
            Log.w(str, str2, th);
        }
    }
}
