package com.waps;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

class v extends AsyncTask {
    String a = "";
    final /* synthetic */ AppConnect b;
    private String c = "";
    private String d = "";

    public v(AppConnect appConnect, String str, String str2) {
        this.b = appConnect;
        this.c = str;
        this.d = str2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        if (AppConnect.aQ) {
            this.a = AppConnect.T.a(this.c, this.d);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        if (!SDKUtils.isNull(this.a)) {
            boolean unused = this.b.f(this.a);
            if (this.c.contains("push/api_ad")) {
                Log.d("APP_SDK", "== Get pushad by api ==");
                SharedPreferences.Editor edit = this.b.Q.getSharedPreferences("AppSettings", 0).edit();
                edit.putLong("PushAd_CreateTime", System.currentTimeMillis());
                edit.commit();
            }
        }
    }
}
