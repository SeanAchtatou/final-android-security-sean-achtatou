package com.waps;

import android.app.Activity;
import android.app.ActivityManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.PowerManager;
import android.webkit.WebView;
import java.util.List;

class b extends Thread {
    final /* synthetic */ WebView a;
    final /* synthetic */ AdView b;

    b(AdView adView, WebView webView) {
        this.b = adView;
        this.a = webView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.AdView.a(com.waps.AdView, boolean):boolean
     arg types: [com.waps.AdView, int]
     candidates:
      com.waps.AdView.a(com.waps.AdView, int):int
      com.waps.AdView.a(com.waps.AdView, java.lang.String):java.lang.String
      com.waps.AdView.a(com.waps.AdView, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.AdView.c(com.waps.AdView, boolean):boolean
     arg types: [com.waps.AdView, int]
     candidates:
      com.waps.AdView.c(com.waps.AdView, java.lang.String):java.lang.String
      com.waps.AdView.c(com.waps.AdView, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.AdView.b(com.waps.AdView, boolean):boolean
     arg types: [com.waps.AdView, int]
     candidates:
      com.waps.AdView.b(com.waps.AdView, java.lang.String):java.lang.String
      com.waps.AdView.b(com.waps.AdView, boolean):boolean */
    public void run() {
        try {
            if (((ConnectivityManager) this.b.a.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
                while (this.b.k) {
                    try {
                        if (this.a != null) {
                            if (this.b.l) {
                                this.a.loadUrl(this.b.q);
                                boolean unused = this.b.l = false;
                            }
                            List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) ((Activity) this.b.a).getSystemService("activity")).getRunningTasks(2);
                            if (runningTasks != null && !runningTasks.isEmpty()) {
                                long currentTimeMillis = System.currentTimeMillis() - this.b.d;
                                String unused2 = this.b.e = runningTasks.get(0).topActivity.getShortClassName();
                                String unused3 = this.b.f = Build.VERSION.SDK;
                                int parseInt = Integer.parseInt(this.b.f);
                                PowerManager powerManager = (PowerManager) ((Activity) this.b.a).getSystemService("power");
                                if (parseInt >= 7) {
                                    if (!((Boolean) powerManager.getClass().getDeclaredMethod("isScreenOn", new Class[0]).invoke(powerManager, new Object[0])).booleanValue()) {
                                        this.a.loadUrl("");
                                    } else if (currentTimeMillis > ((long) ((this.b.h * 1000) - 1000))) {
                                        if (((Activity) this.b.a).getClass().toString().contains(this.b.e)) {
                                            if (this.b.i) {
                                                this.a.loadUrl(this.b.q);
                                                boolean unused4 = this.b.i = false;
                                            }
                                        } else if (!this.b.i) {
                                            this.a.loadUrl("");
                                            boolean unused5 = this.b.i = true;
                                        }
                                    }
                                } else if (!((Activity) this.b.a).getClass().toString().contains(this.b.e) || currentTimeMillis <= ((long) ((this.b.h * 1000) - 1000))) {
                                    this.a.loadDataWithBaseURL("", "<html></html>", "text/html", "utf-8", "");
                                } else {
                                    this.a.loadUrl(this.b.q);
                                }
                            }
                            if (((Activity) this.b.a).isFinishing()) {
                                boolean unused6 = this.b.k = false;
                                boolean unused7 = this.b.l = true;
                                this.a.loadDataWithBaseURL("", "<html></html>", "text/html", "utf-8", "");
                            }
                            sleep((long) (this.b.h * 1000));
                        } else {
                            return;
                        }
                    } catch (Exception e) {
                    }
                }
            }
        } catch (Exception e2) {
        }
    }
}
