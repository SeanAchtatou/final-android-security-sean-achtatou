package com.waps;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Looper;
import android.util.Log;
import android.util.Xml;
import com.lxx.wchw.z5root.R;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;

public final class AppConnect {
    protected static ActivityManager.RunningTaskInfo A = null;
    protected static String B = "";
    protected static String C = "";
    protected static String D = "";
    protected static Map E = null;
    protected static int F = Color.argb(50, 240, 240, 240);
    protected static int G = -1;
    protected static List H = null;
    protected static String I = "ADVIEW";
    public static final String LIBRARY_VERSION_NUMBER = "1.6.2";
    private static AppConnect S = null;
    /* access modifiers changed from: private */
    public static z T = null;
    private static ad U = null;
    /* access modifiers changed from: private */
    public static String aE = "";
    /* access modifiers changed from: private */
    public static String aF = "receiver/install?";
    /* access modifiers changed from: private */
    public static String aG = "install";
    private static boolean aI = true;
    private static boolean aK = false;
    private static boolean aL = false;
    private static boolean aN = false;
    private static String aO = "";
    private static String aP = "";
    /* access modifiers changed from: private */
    public static boolean aQ = false;
    private static boolean aS = true;
    private static boolean aT = true;
    private static boolean aW = true;
    private static Map aX = null;
    /* access modifiers changed from: private */
    public static boolean aY = false;
    /* access modifiers changed from: private */
    public static boolean aZ = true;
    /* access modifiers changed from: private */
    public static boolean ar = true;
    /* access modifiers changed from: private */
    public static boolean as = false;
    /* access modifiers changed from: private */
    public static UpdatePointsNotifier ay;
    private static boolean ba = false;
    /* access modifiers changed from: private */
    public static boolean bb = false;
    /* access modifiers changed from: private */
    public static boolean bc = false;
    /* access modifiers changed from: private */
    public static boolean bd = true;
    /* access modifiers changed from: private */
    public static boolean be = false;
    private static i bf;
    /* access modifiers changed from: private */
    public static String bg = "";
    private static final byte[] bp = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    private static k bv = null;
    private static int bw = 0;
    private static String bx = "";
    public static boolean finalize_tag = false;
    static String w = "make";
    protected static ComponentName z;
    s J;
    String K;
    String L;
    String M;
    v N;
    n O;
    private r P = null;
    /* access modifiers changed from: private */
    public Context Q = null;
    /* access modifiers changed from: private */
    public final ScheduledExecutorService R = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */
    public String V = "";
    private String W = "";
    private String X = "";
    private String Y = "";
    private String Z = "";
    final String a = "loc";
    private q aA = null;
    private l aB = null;
    private p aC = null;
    /* access modifiers changed from: private */
    public o aD = null;
    private int aH;
    private int aJ = 0;
    private boolean aM = false;
    private String aR = "";
    private String aU = "";
    private String aV = "";
    private String aa = "";
    /* access modifiers changed from: private */
    public String ab = "";
    /* access modifiers changed from: private */
    public String ac = "";
    private String ad = "";
    private String ae = "";
    /* access modifiers changed from: private */
    public String af = "";
    /* access modifiers changed from: private */
    public String ag = "";
    private String ah = "http://app.wapx.cn/action/account/offerlist?";
    private String ai = "http://app.wapx.cn/action/account/ownslist?";
    /* access modifiers changed from: private */
    public String aj = "";
    /* access modifiers changed from: private */
    public String ak = "";
    /* access modifiers changed from: private */
    public String al = "";
    private String am = "";
    private String an = "";
    private int ao = 0;
    private int ap = 0;
    /* access modifiers changed from: private */
    public String aq = "";
    private String at = "";
    private String au = "";
    private String av = "";
    private String aw = "";
    private String ax = "";
    private m az = null;
    final String b = "mac";
    private String bh = "";
    private String bi = "";
    private String bj = "";
    private Bitmap bk;
    private String bl = "";
    private String bm = "";
    private String bn = "";
    private String bo = "";
    private byte[] bq;
    private int br;
    private int bs;
    private boolean bt;
    private int bu;
    final String c = "x";
    final String d = "y";
    final String e = "net";
    final String f = "imsi";
    final String g = "udid";
    final String h = "device_name";
    final String i = "device_type";
    final String j = "os_version";
    final String k = "country_code";
    final String l = "language";
    final String m = "app_version";
    final String n = "sdk_version";
    final String o = "act";
    final String p = "userid";
    final String q = "channel";
    final String r = "points";
    final String s = "points";
    final String t = "install";
    final String u = "uninstall";
    final String v = "load";
    final String x = "device_width";
    final String y = "device_height";

    public AppConnect() {
    }

    private AppConnect(Context context) {
        y();
        this.ak = c(context);
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        this.P = new r(this, null);
        this.P.execute(new Void[0]);
    }

    public AppConnect(Context context, int i2) {
        this.ak = c(context);
    }

    private AppConnect(Context context, String str) {
        this.Q = context;
        y();
        this.ak = c(context);
        if (!SDKUtils.isNull(str)) {
            this.ak += "&userid=" + str;
        }
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        this.P = new r(this, null);
        this.P.execute(new Void[0]);
    }

    protected static String a() {
        return aO;
    }

    private static String a(ApplicationInfo applicationInfo) {
        if (applicationInfo != null) {
            try {
                if (!SDKUtils.isNull(a())) {
                    String a2 = a();
                    Log.d("APP_SDK", "AppId is setted by code,the value is: " + a2);
                    if (applicationInfo.metaData == null) {
                        return a2;
                    }
                    String string = applicationInfo.metaData.getString("WAPS_ID");
                    if (SDKUtils.isNull(string)) {
                        string = applicationInfo.metaData.getString("APP_ID");
                    }
                    if (SDKUtils.isNull(string)) {
                        return a2;
                    }
                    if (!a2.equals(string.trim())) {
                        Log.w("APP_SDK", "AppId is setted by code is not equals the value be setted by manifest! Please chick it!");
                        return a2;
                    }
                    Log.d("APP_SDK", "The AppId in manifest is: " + string.trim());
                    return a2;
                } else if (applicationInfo.metaData != null) {
                    String string2 = applicationInfo.metaData.getString("WAPS_ID");
                    if (SDKUtils.isNull(string2)) {
                        string2 = applicationInfo.metaData.getString("APP_ID");
                    }
                    if (!SDKUtils.isNull(string2)) {
                        String trim = string2.trim();
                        Log.d("APP_SDK", "AppId is setted by manifest, the value is: " + trim);
                        return trim;
                    }
                    Log.w("APP_SDK", "AppId is not setted！ Please check it!");
                    return "";
                }
            } catch (Exception e2) {
            }
        }
        return "";
    }

    private String a(NodeList nodeList) {
        Element element = (Element) nodeList.item(0);
        if (element == null) {
            return null;
        }
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        String str = "";
        for (int i2 = 0; i2 < length; i2++) {
            Node item = childNodes.item(i2);
            if (item != null) {
                str = str + item.getNodeValue();
            }
        }
        if (!SDKUtils.isNull(str)) {
            return str.trim();
        }
        return null;
    }

    protected static String a(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(bArr);
            return a(instance.digest(), "");
        } catch (Exception e2) {
            return "";
        }
    }

    protected static String a(byte[] bArr, String str) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (hexString.length() == 1) {
                sb.append("0");
            }
            sb.append(hexString).append(str);
        }
        return sb.toString();
    }

    private List a(InputStream inputStream) {
        a aVar;
        ArrayList arrayList;
        try {
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            a aVar2 = null;
            ArrayList arrayList2 = null;
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 0:
                        a aVar3 = aVar2;
                        arrayList = new ArrayList();
                        aVar = aVar3;
                        continue;
                        arrayList2 = arrayList;
                        aVar2 = aVar;
                    case 2:
                        aVar = "Ad".equals(newPullParser.getName()) ? new a() : aVar2;
                        if (aVar != null) {
                            if ("AdId".equals(newPullParser.getName())) {
                                aVar.a(newPullParser.nextText());
                            }
                            if ("Image".equals(newPullParser.getName())) {
                                aVar.b(newPullParser.nextText());
                            }
                            if ("Title".equals(newPullParser.getName())) {
                                aVar.h(newPullParser.nextText());
                            }
                            if ("ClickUrl".equals(newPullParser.getName())) {
                                aVar.d(newPullParser.nextText());
                            }
                            if ("BrowserPackage".equals(newPullParser.getName())) {
                                aVar.i(newPullParser.nextText());
                            }
                            if ("BrowserActivity".equals(newPullParser.getName())) {
                                aVar.j(newPullParser.nextText());
                            }
                            if ("Flag".equals(newPullParser.getName())) {
                                aVar.k(newPullParser.nextText());
                                arrayList = arrayList2;
                                continue;
                                arrayList2 = arrayList;
                                aVar2 = aVar;
                            }
                        }
                        arrayList = arrayList2;
                        arrayList2 = arrayList;
                        aVar2 = aVar;
                    case R.styleable.com_admob_android_ads_AdView_keywords:
                        if ("Ad".equals(newPullParser.getName()) && aVar2 != null && !SDKUtils.isNull(aVar2.a())) {
                            arrayList2.add(aVar2);
                            aVar = null;
                            arrayList = arrayList2;
                            continue;
                            arrayList2 = arrayList;
                            aVar2 = aVar;
                        }
                }
                aVar = aVar2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                aVar2 = aVar;
            }
            return arrayList2;
        } catch (Exception e2) {
            return null;
        }
    }

    private List a(List list) {
        try {
            ArrayList arrayList = new ArrayList();
            if (list != null && list.size() > 0) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    a aVar = (a) it.next();
                    String b2 = aVar.b();
                    String a2 = aVar.a();
                    Bitmap a3 = a(this.Q, b2, a2, "/Android/data/cache/iconCache");
                    AdInfo adInfo = new AdInfo();
                    adInfo.a(a2);
                    adInfo.b(aVar.h());
                    adInfo.c(aVar.c());
                    adInfo.a(a3);
                    adInfo.a(aVar.l());
                    adInfo.d(aVar.m());
                    adInfo.e(aVar.n());
                    adInfo.f(aVar.o());
                    adInfo.g(aVar.p());
                    adInfo.a(aVar.q());
                    arrayList.add(adInfo);
                }
                return arrayList;
            }
        } catch (Exception e2) {
        }
        return null;
    }

    protected static void a(Context context) {
        try {
            if (aI) {
                SharedPreferences.Editor edit = context.getSharedPreferences("AppSettings", 0).edit();
                List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(2);
                if (runningTasks != null && !runningTasks.isEmpty()) {
                    ActivityManager.RunningTaskInfo runningTaskInfo = runningTasks.get(0);
                    if (!runningTaskInfo.baseActivity.getPackageName().equals(context.getPackageName())) {
                        runningTaskInfo = runningTasks.get(1);
                    }
                    z = runningTaskInfo.baseActivity;
                    A = runningTaskInfo;
                    if (!z.getShortClassName().equals(f(context)) && !z.getShortClassName().contains("opda")) {
                        String packageName = runningTaskInfo.baseActivity.getPackageName();
                        String className = runningTaskInfo.baseActivity.getClassName();
                        String shortClassName = runningTaskInfo.baseActivity.getShortClassName();
                        edit.putString("packageName", packageName);
                        edit.putString("className", className);
                        edit.putString("short_className", shortClassName);
                        edit.commit();
                    }
                }
                aI = false;
            }
        } catch (Exception e2) {
        }
    }

    protected static void a(Context context, z zVar) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.getExtraInfo() != null && activeNetworkInfo.getExtraInfo().toLowerCase().contains("wap")) {
                zVar.a(true);
            }
        } catch (Exception e2) {
        }
    }

    private void a(String str, String str2) {
        b("http://push.wapx.cn/action/", str, str2);
    }

    private void a(String str, String str2, String str3, String str4, String str5) {
        try {
            if (new SDKUtils(this.Q).isTimeLimited("00:00:01", "07:00:00")) {
                aL = false;
            }
            new x(this.Q).a(str, str2, str3, str4, str5, aL);
        } catch (Exception e2) {
        }
    }

    protected static void b(Context context) {
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences("PushFlag", 3);
            if (sharedPreferences != null) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.clear();
                edit.commit();
            }
            SharedPreferences sharedPreferences2 = context.getSharedPreferences("Start_Tag", 3);
            if (sharedPreferences2 != null) {
                SharedPreferences.Editor edit2 = sharedPreferences2.edit();
                edit2.clear();
                edit2.commit();
            }
        } catch (Exception e2) {
        }
    }

    private void b(String str, String str2) {
        String str3;
        boolean z2;
        String str4;
        Intent intent;
        if (H != null && H.size() > 0) {
            List list = H;
            int i2 = 0;
            int i3 = -1;
            while (i2 < list.size()) {
                try {
                    int i4 = ((a) list.get(i2)).a().equals(str) ? i2 : i3;
                    i2++;
                    i3 = i4;
                } catch (Exception e2) {
                    return;
                }
            }
            if (i3 != -1) {
                a aVar = (a) list.get(i3);
                String f2 = aVar.f();
                String g2 = aVar.g();
                String d2 = aVar.d();
                String e3 = aVar.e();
                String r2 = ((a) list.get(i3)).r();
                try {
                    ApplicationInfo applicationInfo = this.Q.getPackageManager().getApplicationInfo(this.Q.getPackageName(), 128);
                    String packageName = this.Q.getPackageName();
                    try {
                        str3 = applicationInfo.metaData != null ? applicationInfo.metaData.getString("CLIENT_PACKAGE") : "";
                        if (SDKUtils.isNull(str3)) {
                            str3 = packageName;
                        }
                    } catch (PackageManager.NameNotFoundException e4) {
                        str3 = packageName;
                    }
                } catch (PackageManager.NameNotFoundException e5) {
                    str3 = "";
                }
                if (!SDKUtils.isNull(f2)) {
                    try {
                        intent = this.Q.getPackageManager().getLaunchIntentForPackage(f2);
                    } catch (Exception e6) {
                        intent = null;
                    }
                    if (intent != null) {
                        this.Q.startActivity(intent);
                        getInstanceNoConnect(this.Q).a(f2, 2);
                        z2 = false;
                    } else {
                        z2 = true;
                    }
                } else {
                    z2 = true;
                }
                if (!z2) {
                    return;
                }
                if (!SDKUtils.isNull(g2)) {
                    new SDKUtils(this.Q).openUrlByBrowser(g2, d2);
                    return;
                }
                Intent intent2 = new Intent(this.Q, g(this.Q));
                if (SDKUtils.isNull(str2) || !str2.equals("download")) {
                    intent2.putExtra("URL", d2);
                    str4 = e3;
                } else {
                    intent2.putExtra("URL", r2);
                    str4 = "false";
                }
                intent2.putExtra("CLIENT_PACKAGE", str3);
                intent2.putExtra("offers_webview_tag", "OffersWebView");
                if (SDKUtils.isNull(str4) || str4.equals("false")) {
                    intent2.putExtra("isFinshClose", "true");
                }
                this.Q.startActivity(intent2);
            }
        }
    }

    private void b(String str, String str2, String str3) {
        this.J = new s(this, str + str2, str3);
        this.J.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public Map c(String str) {
        aX = new HashMap();
        String string = SDKUtils.isNull(str) ? this.Q.getSharedPreferences("AppSettings", 0).getString("AttrConfig", "") : str;
        if (string.endsWith("[;]")) {
            string = string.substring(0, string.lastIndexOf("[;]"));
        }
        if (string.indexOf("[;]") > 0) {
            String[] split = string.split("\\[;\\]");
            for (int i2 = 0; i2 < split.length; i2++) {
                if (split[i2].trim().indexOf("[=]") > 0) {
                    String[] split2 = split[i2].trim().split("\\[=\\]");
                    if (split2.length == 2) {
                        aX.put(split2[0], split2[1]);
                    }
                }
            }
        } else if (string.trim().indexOf("[=]") > 0) {
            String[] split3 = string.trim().split("\\[=\\]");
            if (split3.length == 2) {
                aX.put(split3[0], split3[1]);
            }
        }
        return aX;
    }

    /* access modifiers changed from: private */
    public void c(String str, String str2, String str3) {
        d("http://push.wapx.cn/action/", str2, str3);
    }

    protected static Map d(Context context) {
        if (0 == 0) {
            try {
                return new SDKUtils(context).getLanguageCode().toLowerCase().equals("zh") ? ag.a() : ag.b();
            } catch (Exception e2) {
            }
        }
        return null;
    }

    private void d(String str, String str2, String str3) {
        this.N = new v(this, str + str2, str3);
        this.N.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public boolean d(String str) {
        String str2;
        String str3 = "";
        try {
            Document l2 = l(str);
            if (!(l2 == null || l2.getElementsByTagName("Version") == null)) {
                String a2 = a(l2.getElementsByTagName("Success"));
                String a3 = a(l2.getElementsByTagName("Version"));
                String a4 = a(l2.getElementsByTagName("Clear"));
                this.aV = a(l2.getElementsByTagName("Update_tip"));
                String a5 = a(l2.getElementsByTagName("AppList"));
                if (l2.getElementsByTagName("Push") == null || l2.getElementsByTagName("Push").getLength() <= 0) {
                    aQ = false;
                    str2 = str3;
                } else {
                    String a6 = a(l2.getElementsByTagName("Wait"));
                    String a7 = a(l2.getElementsByTagName("Loop"));
                    aQ = true;
                    String str4 = a7;
                    str3 = a6;
                    str2 = str4;
                }
                this.aR = a(l2.getElementsByTagName("AD"));
                if (SDKUtils.isNull(this.aR) || !this.aR.equals("0")) {
                    aS = true;
                } else {
                    aS = false;
                }
                this.aU = a(l2.getElementsByTagName("MINI"));
                if (SDKUtils.isNull(this.aU) || !this.aU.equals("0")) {
                    aT = true;
                } else {
                    aT = false;
                }
                SharedPreferences.Editor edit = this.Q.getSharedPreferences("ShowAdFlag", 3).edit();
                edit.putBoolean("show_ad_flag", aS);
                edit.putBoolean("show_mini_flag", aT);
                edit.commit();
                try {
                    String a8 = a(l2.getElementsByTagName("ItemReturn"));
                    if (!SDKUtils.isNull(a8) && a8.length() > 0) {
                        SharedPreferences.Editor edit2 = this.Q.getSharedPreferences("AppSettings", 0).edit();
                        edit2.putString("AttrConfig", a8);
                        edit2.commit();
                        c(a8);
                    }
                } catch (Exception e2) {
                }
                String a9 = a(l2.getElementsByTagName("IconAd"));
                String a10 = a(l2.getElementsByTagName("Exception"));
                if (a2 != null && a2.equals("true")) {
                    if (!SDKUtils.isNull(a3)) {
                        this.aq = a3;
                    }
                    if (!SDKUtils.isNull(a4)) {
                        as = true;
                    }
                    if (!SDKUtils.isNull(a5)) {
                        SharedPreferences.Editor edit3 = this.Q.getSharedPreferences("Finalize_Flag", 3).edit();
                        edit3.putString("appList", a5);
                        edit3.commit();
                    }
                    if (!SDKUtils.isNull(str3)) {
                        B = str3;
                    }
                    if (!SDKUtils.isNull(str2)) {
                        C = str2;
                    }
                    if (!SDKUtils.isNull(a9) && a9.equals("true")) {
                        aY = true;
                    }
                    if (!SDKUtils.isNull(a10) && a10.equals("true")) {
                        be = true;
                    }
                    return true;
                }
            }
        } catch (Exception e3) {
        }
        return false;
    }

    protected static String e(Context context) {
        bx = context.getSharedPreferences("AppSettings", 0).getString("OfferName", "");
        return bx;
    }

    private void e(String str, String str2, String str3) {
        this.O = new n(this, str, str2, str3);
        this.O.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00e5, code lost:
        a("make_b", "0", r16);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e(java.lang.String r20) {
        /*
            r19 = this;
            r10 = 0
            java.lang.String r11 = "DesktopI.dat"
            java.lang.String r12 = "DesktopB.dat"
            java.lang.String r13 = "/Android/data/cache"
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0181 }
            java.lang.String r5 = "UTF-8"
            r0 = r20
            r1 = r5
            byte[] r5 = r0.getBytes(r1)     // Catch:{ Exception -> 0x0181 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0181 }
            r0 = r19
            r1 = r4
            java.util.List r14 = r0.a(r1)     // Catch:{ Exception -> 0x0181 }
            if (r14 == 0) goto L_0x0182
            int r4 = r14.size()     // Catch:{ Exception -> 0x0181 }
            if (r4 <= 0) goto L_0x0182
            r4 = 0
            r15 = r4
        L_0x0026:
            int r4 = r14.size()     // Catch:{ Exception -> 0x0181 }
            if (r15 >= r4) goto L_0x0182
            java.lang.Object r20 = r14.get(r15)     // Catch:{ Exception -> 0x0181 }
            com.waps.a r20 = (com.waps.a) r20     // Catch:{ Exception -> 0x0181 }
            java.lang.String r16 = r20.a()     // Catch:{ Exception -> 0x0181 }
            java.lang.Object r20 = r14.get(r15)     // Catch:{ Exception -> 0x0181 }
            com.waps.a r20 = (com.waps.a) r20     // Catch:{ Exception -> 0x0181 }
            java.lang.String r7 = r20.b()     // Catch:{ Exception -> 0x0181 }
            java.lang.Object r20 = r14.get(r15)     // Catch:{ Exception -> 0x0181 }
            com.waps.a r20 = (com.waps.a) r20     // Catch:{ Exception -> 0x0181 }
            java.lang.String r5 = r20.h()     // Catch:{ Exception -> 0x0181 }
            java.lang.Object r20 = r14.get(r15)     // Catch:{ Exception -> 0x0181 }
            com.waps.a r20 = (com.waps.a) r20     // Catch:{ Exception -> 0x0181 }
            java.lang.String r6 = r20.d()     // Catch:{ Exception -> 0x0181 }
            java.lang.Object r20 = r14.get(r15)     // Catch:{ Exception -> 0x0181 }
            com.waps.a r20 = (com.waps.a) r20     // Catch:{ Exception -> 0x0181 }
            java.lang.String r8 = r20.i()     // Catch:{ Exception -> 0x0181 }
            java.lang.Object r20 = r14.get(r15)     // Catch:{ Exception -> 0x0181 }
            com.waps.a r20 = (com.waps.a) r20     // Catch:{ Exception -> 0x0181 }
            java.lang.String r9 = r20.j()     // Catch:{ Exception -> 0x0181 }
            java.lang.Object r20 = r14.get(r15)     // Catch:{ Exception -> 0x0181 }
            com.waps.a r20 = (com.waps.a) r20     // Catch:{ Exception -> 0x0181 }
            java.lang.String r4 = r20.k()     // Catch:{ Exception -> 0x0181 }
            boolean r17 = com.waps.SDKUtils.isNull(r4)     // Catch:{ Exception -> 0x00f3 }
            if (r17 != 0) goto L_0x00f5
            java.lang.String r17 = "bookmark"
            r0 = r4
            r1 = r17
            boolean r17 = r0.equals(r1)     // Catch:{ Exception -> 0x00f3 }
            if (r17 == 0) goto L_0x00f5
            boolean r17 = com.waps.AppConnect.bc     // Catch:{ Exception -> 0x00f3 }
            if (r17 == 0) goto L_0x00f5
            com.waps.SDKUtils r4 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x00f3 }
            r0 = r19
            android.content.Context r0 = r0.Q     // Catch:{ Exception -> 0x00f3 }
            r7 = r0
            r4.<init>(r7)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r4 = r4.loadStringFromLocal(r12, r13)     // Catch:{ Exception -> 0x00f3 }
            boolean r7 = com.waps.SDKUtils.isNull(r4)     // Catch:{ Exception -> 0x00f3 }
            if (r7 != 0) goto L_0x00a1
            boolean r7 = r4.contains(r5)     // Catch:{ Exception -> 0x00f3 }
            if (r7 != 0) goto L_0x00df
        L_0x00a1:
            com.waps.SDKUtils r7 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x00e4 }
            r0 = r19
            android.content.Context r0 = r0.Q     // Catch:{ Exception -> 0x00e4 }
            r8 = r0
            r7.<init>(r8)     // Catch:{ Exception -> 0x00e4 }
            r7.addBrowserBookmark(r5, r6)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r6 = "make_b"
            java.lang.String r7 = "1"
            r0 = r19
            r1 = r6
            r2 = r7
            r3 = r16
            r0.a(r1, r2, r3)     // Catch:{ Exception -> 0x00e4 }
        L_0x00bb:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f3 }
            r6.<init>()     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r5 = ";"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00f3 }
            com.waps.SDKUtils r5 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x00f3 }
            r0 = r19
            android.content.Context r0 = r0.Q     // Catch:{ Exception -> 0x00f3 }
            r6 = r0
            r5.<init>(r6)     // Catch:{ Exception -> 0x00f3 }
            r5.saveDataToLocal(r4, r12, r13)     // Catch:{ Exception -> 0x00f3 }
        L_0x00df:
            int r4 = r15 + 1
            r15 = r4
            goto L_0x0026
        L_0x00e4:
            r6 = move-exception
            java.lang.String r6 = "make_b"
            java.lang.String r7 = "0"
            r0 = r19
            r1 = r6
            r2 = r7
            r3 = r16
            r0.a(r1, r2, r3)     // Catch:{ Exception -> 0x00f3 }
            goto L_0x00bb
        L_0x00f3:
            r4 = move-exception
            goto L_0x00df
        L_0x00f5:
            boolean r17 = com.waps.SDKUtils.isNull(r4)     // Catch:{ Exception -> 0x00f3 }
            if (r17 != 0) goto L_0x00df
            java.lang.String r17 = "iconad"
            r0 = r4
            r1 = r17
            boolean r4 = r0.equals(r1)     // Catch:{ Exception -> 0x00f3 }
            if (r4 == 0) goto L_0x00df
            boolean r4 = com.waps.AppConnect.bb     // Catch:{ Exception -> 0x00f3 }
            if (r4 == 0) goto L_0x00df
            com.waps.SDKUtils r4 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x00f3 }
            r0 = r19
            android.content.Context r0 = r0.Q     // Catch:{ Exception -> 0x00f3 }
            r17 = r0
            r0 = r4
            r1 = r17
            r0.<init>(r1)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r17 = r4.loadStringFromLocal(r11, r13)     // Catch:{ Exception -> 0x00f3 }
            boolean r4 = com.waps.SDKUtils.isNull(r17)     // Catch:{ Exception -> 0x00f3 }
            if (r4 != 0) goto L_0x012b
            r0 = r17
            r1 = r5
            boolean r4 = r0.contains(r1)     // Catch:{ Exception -> 0x00f3 }
            if (r4 != 0) goto L_0x00df
        L_0x012b:
            com.waps.SDKUtils r4 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x0172 }
            r0 = r19
            android.content.Context r0 = r0.Q     // Catch:{ Exception -> 0x0172 }
            r18 = r0
            r0 = r4
            r1 = r18
            r0.<init>(r1)     // Catch:{ Exception -> 0x0172 }
            r4.createMoreShortcut_forUrl(r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0172 }
            java.lang.String r4 = "make_i"
            java.lang.String r6 = "1"
            r0 = r19
            r1 = r4
            r2 = r6
            r3 = r16
            r0.a(r1, r2, r3)     // Catch:{ Exception -> 0x0172 }
        L_0x0149:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f3 }
            r4.<init>()     // Catch:{ Exception -> 0x00f3 }
            r0 = r4
            r1 = r17
            java.lang.StringBuilder r4 = r0.append(r1)     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r5 = ";"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00f3 }
            com.waps.SDKUtils r5 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x00f3 }
            r0 = r19
            android.content.Context r0 = r0.Q     // Catch:{ Exception -> 0x00f3 }
            r6 = r0
            r5.<init>(r6)     // Catch:{ Exception -> 0x00f3 }
            r5.saveDataToLocal(r4, r11, r13)     // Catch:{ Exception -> 0x00f3 }
            goto L_0x00df
        L_0x0172:
            r4 = move-exception
            java.lang.String r4 = "make_i"
            java.lang.String r6 = "0"
            r0 = r19
            r1 = r4
            r2 = r6
            r3 = r16
            r0.a(r1, r2, r3)     // Catch:{ Exception -> 0x00f3 }
            goto L_0x0149
        L_0x0181:
            r4 = move-exception
        L_0x0182:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.AppConnect.e(java.lang.String):boolean");
    }

    protected static String f(Context context) {
        try {
            String e2 = e(context);
            return SDKUtils.isNull(e2) ? SDKUtils.getClassName(context, I) : e2;
        } catch (Exception e3) {
            return "";
        }
    }

    /* access modifiers changed from: private */
    public boolean f(String str) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            this.bh = a(parse.getElementsByTagName("Title"));
            this.bi = a(parse.getElementsByTagName("Content"));
            this.bj = a(parse.getElementsByTagName("ClickUrl"));
            String a2 = a(parse.getElementsByTagName("Image"));
            this.bl = a(parse.getElementsByTagName("Show_detail"));
            this.bm = a(parse.getElementsByTagName("AdPackage"));
            this.bn = a(parse.getElementsByTagName("Push_ring"));
            this.bo = a(parse.getElementsByTagName("NewBrowser"));
            a(a2.getBytes(), 0, a2.getBytes().length);
            this.bk = BitmapFactory.decodeByteArray(this.bq, 0, this.br);
            new x(this.Q).a(this.bl, this.aM ? this.aJ : 0, new SDKUtils(this.Q).isTimeLimited("00:00:01", "07:00:00") ? false : (this.bn == null || !this.bn.equals("true")) ? (this.bn == null || !this.bn.equals("false")) ? (!SDKUtils.isNull(this.bn) || !aN) ? false : aK : false : true, this.bk, this.bh, this.bi, this.bj, this.bm, this.bo);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    protected static Class g(Context context) {
        try {
            return Class.forName(f(context));
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean g(String str) {
        try {
            Document l2 = l(str);
            if (!(l2 == null || l2.getElementsByTagName("Version") == null)) {
                String a2 = a(l2.getElementsByTagName("Success"));
                String a3 = a(l2.getElementsByTagName("Version"));
                this.aV = a(l2.getElementsByTagName("Update_tip"));
                if (a2 != null && a2.equals("true")) {
                    if (!SDKUtils.isNull(a3)) {
                        this.aq = a3;
                    }
                    return true;
                }
            }
        } catch (Exception e2) {
        }
        return false;
    }

    public static int getHistoryPoints(Context context) {
        return context.getSharedPreferences("AppSettings", 0).getInt("AllPoints", 0);
    }

    public static String getHistoryPointsName(Context context) {
        return context.getSharedPreferences("AppSettings", 0).getString("PointName", "");
    }

    public static AppConnect getInstance(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null && SDKUtils.isNull(a(applicationInfo))) {
                Log.e("APP_SDK", "WAPS_ID is not setted!");
                return null;
            }
        } catch (Exception e2) {
        }
        S = getInstance(context, "");
        return S;
    }

    public static AppConnect getInstance(Context context, int i2) {
        try {
            if (aW) {
                b(context);
                aW = false;
            }
            SharedPreferences sharedPreferences = context.getSharedPreferences("PushFlag", 3);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            if (sharedPreferences.getString("push_flag", "").equals("")) {
                edit.putString("push_flag", "true");
            }
            edit.commit();
            a(context);
            if (T == null) {
                T = new z(context);
            }
            if (T != null) {
                a(context, T);
            }
            if (S == null) {
                S = new AppConnect(context, i2);
            }
            if (U == null) {
                U = new ad(context);
            }
        } catch (Exception e2) {
        }
        return S;
    }

    public static AppConnect getInstance(Context context, String str) {
        try {
            if (aW) {
                b(context);
                aW = false;
            }
            SharedPreferences sharedPreferences = context.getSharedPreferences("PushFlag", 3);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            if (sharedPreferences.getString("push_flag", "").equals("")) {
                edit.putString("push_flag", "true");
            }
            edit.commit();
            a(context);
            if (T == null) {
                T = new z(context);
            }
            if (T != null) {
                a(context, T);
            }
            if (S == null) {
                if (!SDKUtils.isNull(str)) {
                    S = new AppConnect(context, str);
                } else {
                    S = new AppConnect(context);
                }
            }
            if (U == null) {
                U = new ad(context);
            }
        } catch (Exception e2) {
        }
        return S;
    }

    public static AppConnect getInstance(String str, Context context) {
        aO = str;
        S = getInstance(context);
        return S;
    }

    public static AppConnect getInstance(String str, String str2, Context context) {
        aP = str2;
        S = getInstance(str, context);
        return S;
    }

    public static AppConnect getInstanceNoConnect(Context context) {
        try {
            a(context);
            if (T == null) {
                T = new z(context);
            }
            if (T != null) {
                a(context, T);
            }
            if (S == null) {
                S = new AppConnect(context, 0);
            }
            if (U == null) {
                U = new ad(context);
            }
        } catch (Exception e2) {
        }
        return S;
    }

    /* access modifiers changed from: private */
    public void h(String str) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.Q);
            builder.setTitle((CharSequence) E.get("update_version"));
            if (!SDKUtils.isNull(this.aV)) {
                builder.setMessage(((String) E.get("new_version")) + this.aq + ((String) E.get("version_is_found")) + "\n" + this.aV);
            } else {
                builder.setMessage(((String) E.get("new_version")) + this.aq + ((String) E.get("version_is_found")));
            }
            builder.setPositiveButton((CharSequence) E.get("download"), new g(this, str));
            builder.setNegativeButton((CharSequence) E.get("next_time"), new h(this));
            builder.show();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public boolean i(String str) {
        String a2;
        Document l2 = l(str);
        if (!(l2 == null || (a2 = a(l2.getElementsByTagName("Success"))) == null || !a2.equals("true"))) {
            SharedPreferences.Editor edit = this.Q.getSharedPreferences("AppSettings", 0).edit();
            String a3 = a(l2.getElementsByTagName("Points"));
            String a4 = a(l2.getElementsByTagName("CurrencyName"));
            if (!(a3 == null || a4 == null)) {
                ay.getUpdatePoints(a4, Integer.parseInt(a3));
                edit.putInt("AllPoints", Integer.parseInt(a3));
                edit.putString("PointName", a4);
                edit.putLong("GetPointsTime", System.currentTimeMillis());
                edit.commit();
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean j(String str) {
        return k(str);
    }

    /* access modifiers changed from: private */
    public boolean k(String str) {
        Document l2 = l(str);
        if (l2 != null) {
            String a2 = a(l2.getElementsByTagName("Success"));
            if (a2 != null && a2.equals("true")) {
                SharedPreferences.Editor edit = this.Q.getSharedPreferences("AppSettings", 0).edit();
                String a3 = a(l2.getElementsByTagName("Points"));
                String a4 = a(l2.getElementsByTagName("CurrencyName"));
                if (!(a3 == null || a4 == null)) {
                    ay.getUpdatePoints(a4, Integer.parseInt(a3));
                    edit.putInt("AllPoints", Integer.parseInt(a3));
                    edit.putString("PointName", a4);
                    edit.commit();
                    return true;
                }
            } else if (a2 != null && a2.endsWith("false")) {
                ay.getUpdatePointsFailed(a(l2.getElementsByTagName("Message")));
                return true;
            }
        }
        return false;
    }

    private Document l(String str) {
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            return newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
        } catch (Exception e2) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* access modifiers changed from: private */
    public void t() {
        BufferedReader bufferedReader;
        FileOutputStream fileOutputStream;
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        FileOutputStream fileOutputStream2;
        BufferedReader bufferedReader2;
        String str;
        File file;
        FileOutputStream fileOutputStream3;
        int i2 = 0;
        new Intent("android.intent.action.MAIN", (Uri) null).addCategory("android.intent.category.LAUNCHER");
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file2 = new File(Environment.getExternalStorageDirectory().toString() + "/Android");
                File file3 = new File(Environment.getExternalStorageDirectory().toString() + "/Android" + "/Package.dat");
                if (!file2.exists()) {
                    file2.mkdir();
                }
                if (!file3.exists()) {
                    file3.createNewFile();
                }
                FileInputStream fileInputStream3 = new FileInputStream(file3);
                try {
                    BufferedReader bufferedReader3 = new BufferedReader(new InputStreamReader(fileInputStream3));
                    if (bufferedReader3 != null) {
                        String str2 = "";
                        while (true) {
                            try {
                                String readLine = bufferedReader3.readLine();
                                if (readLine == null) {
                                    break;
                                }
                                str2 = str2 + readLine;
                            } catch (Exception e2) {
                                fileOutputStream = null;
                                BufferedReader bufferedReader4 = bufferedReader3;
                                fileInputStream = fileInputStream3;
                                bufferedReader = bufferedReader4;
                                try {
                                    fileOutputStream.close();
                                    fileInputStream.close();
                                    bufferedReader.close();
                                } catch (Exception e3) {
                                    return;
                                }
                            } catch (Throwable th) {
                                fileInputStream2 = fileInputStream3;
                                th = th;
                                bufferedReader2 = bufferedReader3;
                                fileOutputStream2 = null;
                                try {
                                    fileOutputStream2.close();
                                    fileInputStream2.close();
                                    bufferedReader2.close();
                                } catch (Exception e4) {
                                }
                                throw th;
                            }
                        }
                        String str3 = str2;
                        file = file3;
                        bufferedReader2 = bufferedReader3;
                        fileInputStream = fileInputStream3;
                        str = str3;
                    } else {
                        file = file3;
                        bufferedReader2 = bufferedReader3;
                        fileInputStream = fileInputStream3;
                        str = "";
                    }
                } catch (Exception e5) {
                    fileOutputStream = null;
                    fileInputStream = fileInputStream3;
                    bufferedReader = null;
                    fileOutputStream.close();
                    fileInputStream.close();
                    bufferedReader.close();
                } catch (Throwable th2) {
                    fileOutputStream2 = null;
                    fileInputStream2 = fileInputStream3;
                    th = th2;
                    bufferedReader2 = null;
                    fileOutputStream2.close();
                    fileInputStream2.close();
                    bufferedReader2.close();
                    throw th;
                }
            } else {
                str = "";
                bufferedReader2 = null;
                fileInputStream = null;
                file = null;
            }
            try {
                List<PackageInfo> installedPackages = this.Q.getPackageManager().getInstalledPackages(0);
                for (int i3 = 0; i3 < installedPackages.size(); i3++) {
                    PackageInfo packageInfo = installedPackages.get(i3);
                    int i4 = packageInfo.applicationInfo.flags;
                    ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                    if ((i4 & 1) <= 0) {
                        i2++;
                        String str4 = packageInfo.packageName;
                        if (!str.contains(str4)) {
                            if (str4.startsWith("com.")) {
                                aE += str4.substring(3, str4.length()) + ";";
                            } else {
                                aE += str4 + ";";
                            }
                        }
                    }
                }
                byte[] bytes = aE.getBytes("UTF-8");
                if (file != null) {
                    FileOutputStream fileOutputStream4 = new FileOutputStream(file, true);
                    try {
                        fileOutputStream4.write(bytes);
                        fileOutputStream3 = fileOutputStream4;
                    } catch (Exception e6) {
                        bufferedReader = bufferedReader2;
                        fileOutputStream = fileOutputStream4;
                        fileOutputStream.close();
                        fileInputStream.close();
                        bufferedReader.close();
                    } catch (Throwable th3) {
                        th = th3;
                        fileInputStream2 = fileInputStream;
                        fileOutputStream2 = fileOutputStream4;
                        fileOutputStream2.close();
                        fileInputStream2.close();
                        bufferedReader2.close();
                        throw th;
                    }
                } else {
                    fileOutputStream3 = null;
                }
                try {
                    fileOutputStream3.close();
                    fileInputStream.close();
                    bufferedReader2.close();
                } catch (Exception e7) {
                }
            } catch (Exception e8) {
                bufferedReader = bufferedReader2;
                fileOutputStream = null;
                fileOutputStream.close();
                fileInputStream.close();
                bufferedReader.close();
            } catch (Throwable th4) {
                th = th4;
                fileInputStream2 = fileInputStream;
                fileOutputStream2 = null;
                fileOutputStream2.close();
                fileInputStream2.close();
                bufferedReader2.close();
                throw th;
            }
        } catch (Exception e9) {
            bufferedReader = null;
            fileOutputStream = null;
            fileInputStream = null;
            fileOutputStream.close();
            fileInputStream.close();
            bufferedReader.close();
        } catch (Throwable th5) {
            th = th5;
            bufferedReader2 = null;
            fileOutputStream2 = null;
            fileInputStream2 = null;
            fileOutputStream2.close();
            fileInputStream2.close();
            bufferedReader2.close();
            throw th;
        }
    }

    private void u() {
        this.az = new m(this, null);
        this.az.execute(new Void[0]);
    }

    private void v() {
        this.aA = new q(this, null);
        this.aA.execute(new Void[0]);
    }

    private void w() {
        this.aB = new l(this, null);
        this.aB.execute(new Void[0]);
    }

    private void x() {
        this.aC = new p(this, this.am);
        this.aC.execute(new Void[0]);
    }

    private void y() {
        try {
            String loadStringFromLocal = new SDKUtils(this.Q).loadStringFromLocal("CacheTime.dat", "/Android/data/cache");
            if (SDKUtils.isNull(loadStringFromLocal)) {
                new SDKUtils(this.Q).saveDataToLocal(String.valueOf(System.currentTimeMillis()), "CacheTime.dat", "/Android/data/cache");
            } else if (!SDKUtils.isNull(loadStringFromLocal)) {
                if (System.currentTimeMillis() - Long.parseLong(loadStringFromLocal.replaceAll("\n", "")) >= 2592000000L) {
                    try {
                        if (Environment.getExternalStorageState().equals("mounted")) {
                            new SDKUtils(this.Q).deleteLocalFiles(new File(Environment.getExternalStorageDirectory().toString() + "/Android/data/cache"));
                        }
                    } catch (Exception e2) {
                    }
                    new SDKUtils(this.Q).saveDataToLocal(System.currentTimeMillis() + "", "CacheTime.dat", "/Android/data/cache");
                }
            }
        } catch (NumberFormatException e3) {
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0047 A[SYNTHETIC, Splitter:B:27:0x0047] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap a(android.content.Context r6, java.lang.String r7, java.lang.String r8, java.lang.String r9) {
        /*
            r5 = this;
            r3 = 0
            com.waps.SDKUtils r0 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x003a, all -> 0x0043 }
            r0.<init>(r6)     // Catch:{ Exception -> 0x003a, all -> 0x0043 }
            java.io.InputStream r0 = r0.loadStreamFromLocal(r8, r9)     // Catch:{ Exception -> 0x003a, all -> 0x0043 }
            if (r0 != 0) goto L_0x002f
            com.waps.SDKUtils r1 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            java.io.InputStream r1 = r1.getNetDataToStream(r7)     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            if (r1 != 0) goto L_0x001e
            if (r0 == 0) goto L_0x001c
            r0.close()     // Catch:{ IOException -> 0x004b }
        L_0x001c:
            r0 = r3
        L_0x001d:
            return r0
        L_0x001e:
            com.waps.SDKUtils r2 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            r2.<init>(r6)     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            r2.saveDataToLocal(r1, r8, r9)     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            com.waps.SDKUtils r1 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            java.io.InputStream r0 = r1.loadStreamFromLocal(r8, r9)     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
        L_0x002f:
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x0058, all -> 0x0053 }
            if (r0 == 0) goto L_0x0038
            r0.close()     // Catch:{ IOException -> 0x004d }
        L_0x0038:
            r0 = r1
            goto L_0x001d
        L_0x003a:
            r0 = move-exception
            r0 = r3
        L_0x003c:
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ IOException -> 0x004f }
        L_0x0041:
            r0 = r3
            goto L_0x001d
        L_0x0043:
            r0 = move-exception
            r1 = r3
        L_0x0045:
            if (r1 == 0) goto L_0x004a
            r1.close()     // Catch:{ IOException -> 0x0051 }
        L_0x004a:
            throw r0
        L_0x004b:
            r0 = move-exception
            goto L_0x001c
        L_0x004d:
            r0 = move-exception
            goto L_0x0038
        L_0x004f:
            r0 = move-exception
            goto L_0x0041
        L_0x0051:
            r1 = move-exception
            goto L_0x004a
        L_0x0053:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0045
        L_0x0058:
            r1 = move-exception
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.AppConnect.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String):android.graphics.Bitmap");
    }

    /* access modifiers changed from: protected */
    public void a(DisplayAdNotifier displayAdNotifier) {
        U.a("http://ads.wapx.cn/action/", this.ak, displayAdNotifier);
    }

    /* access modifiers changed from: protected */
    public void a(String str, int i2) {
        switch (i2) {
            case 0:
                aF = "receiver/install?";
                aG = "install";
                break;
            case 1:
                aF = "receiver/load_offer?";
                aG = "load";
                break;
            case 2:
                aF = "receiver/load_ad?";
                aG = "load";
                break;
            case R.styleable.com_admob_android_ads_AdView_keywords:
                aF = "receiver/uninstall?";
                aF = "uninstall";
                break;
            default:
                aF = "receiver/install?";
                aG = "install";
                break;
        }
        this.am = str;
        if (S != null) {
            S.x();
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2, String str3) {
        if (S != null) {
            S.e(str, str2, str3);
            return;
        }
        S = new AppConnect(this.Q);
        S.e(str, str2, str3);
    }

    /* access modifiers changed from: package-private */
    public void a(byte[] bArr, int i2, int i3) {
        byte b2;
        this.bq = new byte[bArr.length];
        this.br = 0;
        this.bt = false;
        this.bs = 0;
        if (i3 < 0) {
            this.bt = true;
        }
        int i4 = 0;
        int i5 = i2;
        while (true) {
            if (i4 >= i3) {
                break;
            }
            int i6 = i5 + 1;
            byte b3 = bArr[i5];
            if (b3 == 61) {
                this.bt = true;
                break;
            }
            if (b3 >= 0 && b3 < bp.length && (b2 = bp[b3]) >= 0) {
                int i7 = this.bs + 1;
                this.bs = i7;
                this.bs = i7 % 4;
                this.bu = b2 + (this.bu << 6);
                if (this.bs == 0) {
                    byte[] bArr2 = this.bq;
                    int i8 = this.br;
                    this.br = i8 + 1;
                    bArr2[i8] = (byte) ((this.bu >> 16) & 255);
                    byte[] bArr3 = this.bq;
                    int i9 = this.br;
                    this.br = i9 + 1;
                    bArr3[i9] = (byte) ((this.bu >> 8) & 255);
                    byte[] bArr4 = this.bq;
                    int i10 = this.br;
                    this.br = i10 + 1;
                    bArr4[i10] = (byte) (this.bu & 255);
                }
            }
            i4++;
            i5 = i6;
        }
        if (this.bt && this.bs != 0) {
            this.bu <<= 6;
            switch (this.bs) {
                case 2:
                    this.bu <<= 6;
                    byte[] bArr5 = this.bq;
                    int i11 = this.br;
                    this.br = i11 + 1;
                    bArr5[i11] = (byte) ((this.bu >> 16) & 255);
                    return;
                case R.styleable.com_admob_android_ads_AdView_keywords:
                    byte[] bArr6 = this.bq;
                    int i12 = this.br;
                    this.br = i12 + 1;
                    bArr6[i12] = (byte) ((this.bu >> 16) & 255);
                    byte[] bArr7 = this.bq;
                    int i13 = this.br;
                    this.br = i13 + 1;
                    bArr7[i13] = (byte) ((this.bu >> 8) & 255);
                    return;
                default:
                    return;
            }
        }
    }

    public void awardPoints(int i2, UpdatePointsNotifier updatePointsNotifier) {
        if (i2 >= 0) {
            this.ag = "" + i2;
            if (S != null) {
                ay = updatePointsNotifier;
                S.w();
            }
        }
    }

    /* access modifiers changed from: protected */
    public String b() {
        return aP;
    }

    /* access modifiers changed from: protected */
    public String c(Context context) {
        this.Q = context;
        c();
        this.ak += "app_id=" + this.ab + "&";
        this.ak += "udid=" + this.V + "&";
        this.ak += "imsi=" + this.at + "&";
        this.ak += "net=" + this.au + "&";
        this.ak += "loc=" + this.ax + "&";
        this.ak += "app_version=" + this.ac + "&";
        this.ak += "sdk_version=" + this.ad + "&";
        this.ak += "device_name=" + this.W + "&";
        this.ak += "y=" + this.av + "&";
        this.ak += "device_type=" + this.X + "&";
        this.ak += "os_version=" + this.Y + "&";
        this.ak += "country_code=" + this.Z + "&";
        this.ak += "language=" + this.aa + "&";
        this.ak += "act=" + context.getPackageName() + "." + context.getClass().getSimpleName();
        if (!SDKUtils.isNull(this.ae)) {
            this.ak += "&";
            this.ak += "channel=" + this.ae;
        }
        if (this.ao > 0 && this.ap > 0) {
            this.ak += "&";
            this.ak += "device_width=" + this.ao + "&";
            this.ak += "device_height=" + this.ap;
        }
        return this.ak.replaceAll(" ", "%20");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x036f  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0189 A[Catch:{ Exception -> 0x0229 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0197 A[Catch:{ Exception -> 0x0229 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01fa A[Catch:{ Exception -> 0x0229 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x02bf A[SYNTHETIC, Splitter:B:81:0x02bf] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c() {
        /*
            r8 = this;
            r6 = 0
            java.lang.String r0 = "0"
            java.lang.String r3 = ""
            java.lang.String r0 = "APP_SDK"
            java.util.Map r0 = com.waps.AppConnect.E     // Catch:{ Exception -> 0x036c }
            if (r0 != 0) goto L_0x0013
            android.content.Context r0 = r8.Q     // Catch:{ Exception -> 0x036c }
            java.util.Map r0 = d(r0)     // Catch:{ Exception -> 0x036c }
            com.waps.AppConnect.E = r0     // Catch:{ Exception -> 0x036c }
        L_0x0013:
            android.content.Context r0 = r8.Q
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            android.content.Context r1 = r8.Q     // Catch:{ Exception -> 0x0229 }
            java.lang.String r1 = r1.getPackageName()     // Catch:{ Exception -> 0x0229 }
            r2 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r1 = r0.getApplicationInfo(r1, r2)     // Catch:{ Exception -> 0x0229 }
            if (r1 == 0) goto L_0x0209
            java.lang.String r2 = ""
            java.lang.String r2 = a(r1)     // Catch:{ Exception -> 0x0229 }
            r8.ab = r2     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r8.ab     // Catch:{ Exception -> 0x0229 }
            boolean r2 = com.waps.SDKUtils.isNull(r2)     // Catch:{ Exception -> 0x0229 }
            if (r2 == 0) goto L_0x0038
        L_0x0037:
            return
        L_0x0038:
            android.content.Context r2 = r8.Q     // Catch:{ Exception -> 0x0229 }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ Exception -> 0x0229 }
            r8.aj = r2     // Catch:{ Exception -> 0x0229 }
            android.os.Bundle r2 = r1.metaData     // Catch:{ Exception -> 0x0229 }
            if (r2 == 0) goto L_0x0372
            android.os.Bundle r2 = r1.metaData     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = "CLIENT_PACKAGE"
            java.lang.String r2 = r2.getString(r3)     // Catch:{ Exception -> 0x0229 }
            boolean r3 = com.waps.SDKUtils.isNull(r2)     // Catch:{ Exception -> 0x0229 }
            if (r3 != 0) goto L_0x0054
            r8.aj = r2     // Catch:{ Exception -> 0x0229 }
        L_0x0054:
            java.lang.String r3 = r8.b()     // Catch:{ Exception -> 0x0229 }
            boolean r3 = com.waps.SDKUtils.isNull(r3)     // Catch:{ Exception -> 0x0229 }
            if (r3 != 0) goto L_0x022c
            java.lang.String r3 = r8.b()     // Catch:{ Exception -> 0x0229 }
            r8.ae = r3     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = "APP_SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0229 }
            r4.<init>()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r5 = "App_Pid is setted by code, the value is: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r5 = r8.ae     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0229 }
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x0229 }
        L_0x007e:
            java.lang.String r3 = r8.ae     // Catch:{ Exception -> 0x0229 }
            boolean r3 = com.waps.SDKUtils.isNull(r3)     // Catch:{ Exception -> 0x0229 }
            if (r3 == 0) goto L_0x00a4
            java.lang.String r3 = "waps"
            r8.ae = r3     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = "APP_SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0229 }
            r4.<init>()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r5 = "App_Pid is setted by default, the value is: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r5 = r8.ae     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0229 }
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x0229 }
        L_0x00a4:
            android.content.Context r3 = r8.Q     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = r3.getPackageName()     // Catch:{ Exception -> 0x0229 }
            r4 = 0
            android.content.pm.PackageInfo r0 = r0.getPackageInfo(r3, r4)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.versionName     // Catch:{ Exception -> 0x0229 }
            r8.ac = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = "android"
            r8.X = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0229 }
            r8.W = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x0229 }
            r8.Y = r0     // Catch:{ Exception -> 0x0229 }
            java.util.Locale r0 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.getCountry()     // Catch:{ Exception -> 0x0229 }
            r8.Z = r0     // Catch:{ Exception -> 0x0229 }
            java.util.Locale r0 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.getLanguage()     // Catch:{ Exception -> 0x0229 }
            r8.aa = r0     // Catch:{ Exception -> 0x0229 }
            android.content.Context r0 = r8.Q     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = "phone"
            java.lang.Object r0 = r0.getSystemService(r3)     // Catch:{ Exception -> 0x0229 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.getSubscriberId()     // Catch:{ Exception -> 0x0229 }
            r8.at = r0     // Catch:{ Exception -> 0x0229 }
            com.waps.SDKUtils r0 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x029d }
            android.content.Context r3 = r8.Q     // Catch:{ Exception -> 0x029d }
            r0.<init>(r3)     // Catch:{ Exception -> 0x029d }
            java.lang.String r3 = "ACCESS_WIFI_STATE"
            boolean r0 = r0.hasThePermission(r3)     // Catch:{ Exception -> 0x029d }
            com.waps.AppConnect.ba = r0     // Catch:{ Exception -> 0x029d }
            boolean r0 = com.waps.AppConnect.ba     // Catch:{ Exception -> 0x029d }
            if (r0 == 0) goto L_0x02a7
            com.waps.SDKUtils r0 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x029d }
            android.content.Context r3 = r8.Q     // Catch:{ Exception -> 0x029d }
            r0.<init>(r3)     // Catch:{ Exception -> 0x029d }
            boolean r0 = r0.isClient()     // Catch:{ Exception -> 0x029d }
            if (r0 == 0) goto L_0x0294
            android.content.Context r0 = r8.Q     // Catch:{ Exception -> 0x029d }
            java.lang.String r3 = "wifi"
            java.lang.Object r0 = r0.getSystemService(r3)     // Catch:{ Exception -> 0x029d }
            android.net.wifi.WifiManager r0 = (android.net.wifi.WifiManager) r0     // Catch:{ Exception -> 0x029d }
            android.net.wifi.WifiInfo r0 = r0.getConnectionInfo()     // Catch:{ Exception -> 0x029d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x029d }
            r3.<init>()     // Catch:{ Exception -> 0x029d }
            java.lang.String r4 = "mac"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x029d }
            java.lang.String r0 = r0.getMacAddress()     // Catch:{ Exception -> 0x029d }
            java.lang.String r4 = ":"
            java.lang.String r5 = ""
            java.lang.String r0 = r0.replaceAll(r4, r5)     // Catch:{ Exception -> 0x029d }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x029d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x029d }
            r8.aw = r0     // Catch:{ Exception -> 0x029d }
        L_0x0132:
            android.content.Context r0 = r8.Q     // Catch:{ Exception -> 0x02bc }
            android.content.Context r3 = r8.Q     // Catch:{ Exception -> 0x02bc }
            java.lang.String r3 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r3)     // Catch:{ Exception -> 0x02bc }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Exception -> 0x02bc }
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Exception -> 0x02bc }
            if (r0 == 0) goto L_0x02b0
            java.lang.String r3 = r0.getTypeName()     // Catch:{ Exception -> 0x02bc }
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ Exception -> 0x02bc }
            java.lang.String r4 = "mobile"
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x02bc }
            if (r3 != 0) goto L_0x02b0
            java.lang.String r0 = r0.getTypeName()     // Catch:{ Exception -> 0x02bc }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x02bc }
            r8.au = r0     // Catch:{ Exception -> 0x02bc }
        L_0x015e:
            java.lang.String r0 = "APP_SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02bc }
            r3.<init>()     // Catch:{ Exception -> 0x02bc }
            java.lang.String r4 = "The net is: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x02bc }
            java.lang.String r4 = r8.au     // Catch:{ Exception -> 0x02bc }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x02bc }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x02bc }
            android.util.Log.d(r0, r3)     // Catch:{ Exception -> 0x02bc }
        L_0x0178:
            java.lang.String r0 = "1.6.2"
            r8.ad = r0     // Catch:{ Exception -> 0x0229 }
            android.content.Context r0 = r8.Q     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = "appPrefrences"
            r4 = 0
            android.content.SharedPreferences r3 = r0.getSharedPreferences(r3, r4)     // Catch:{ Exception -> 0x0229 }
            android.os.Bundle r0 = r1.metaData     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x036f
            android.os.Bundle r0 = r1.metaData     // Catch:{ Exception -> 0x0229 }
            java.lang.String r1 = "DEVICE_ID"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x0229 }
        L_0x0191:
            boolean r1 = com.waps.SDKUtils.isNull(r0)     // Catch:{ Exception -> 0x0229 }
            if (r1 != 0) goto L_0x02bf
            r8.V = r0     // Catch:{ Exception -> 0x0229 }
        L_0x0199:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0229 }
            r0.<init>()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r1 = "kingxiaoguang@gmail.com"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r1 = r8.V     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r1 = r8.ab     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x0229 }
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = a(r0)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x0229 }
            r8.av = r0     // Catch:{ Exception -> 0x0229 }
            android.util.DisplayMetrics r1 = new android.util.DisplayMetrics     // Catch:{ Exception -> 0x0369 }
            r1.<init>()     // Catch:{ Exception -> 0x0369 }
            android.content.Context r0 = r8.Q     // Catch:{ Exception -> 0x0369 }
            java.lang.String r2 = "window"
            java.lang.Object r0 = r0.getSystemService(r2)     // Catch:{ Exception -> 0x0369 }
            android.view.WindowManager r0 = (android.view.WindowManager) r0     // Catch:{ Exception -> 0x0369 }
            android.view.Display r0 = r0.getDefaultDisplay()     // Catch:{ Exception -> 0x0369 }
            r0.getMetrics(r1)     // Catch:{ Exception -> 0x0369 }
            int r0 = r1.widthPixels     // Catch:{ Exception -> 0x0369 }
            r8.ao = r0     // Catch:{ Exception -> 0x0369 }
            int r0 = r1.heightPixels     // Catch:{ Exception -> 0x0369 }
            r8.ap = r0     // Catch:{ Exception -> 0x0369 }
        L_0x01e4:
            java.lang.String r0 = "PrimaryColor"
            r1 = 0
            int r0 = r3.getInt(r0, r1)     // Catch:{ Exception -> 0x0229 }
            r8.aH = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = "InstallReferral"
            r1 = 0
            java.lang.String r0 = r3.getString(r0, r1)     // Catch:{ Exception -> 0x0229 }
            boolean r1 = com.waps.SDKUtils.isNull(r0)     // Catch:{ Exception -> 0x0229 }
            if (r1 != 0) goto L_0x01fc
            r8.al = r0     // Catch:{ Exception -> 0x0229 }
        L_0x01fc:
            com.waps.SDKUtils r0 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x0229 }
            android.content.Context r1 = r8.Q     // Catch:{ Exception -> 0x0229 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r0.getLocation()     // Catch:{ Exception -> 0x0229 }
            r8.ax = r0     // Catch:{ Exception -> 0x0229 }
        L_0x0209:
            com.waps.SDKUtils r0 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x0229 }
            android.content.Context r1 = r8.Q     // Catch:{ Exception -> 0x0229 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r1 = "INSTALL_SHORTCUT"
            boolean r0 = r0.hasThePermission(r1)     // Catch:{ Exception -> 0x0229 }
            com.waps.AppConnect.bb = r0     // Catch:{ Exception -> 0x0229 }
            com.waps.SDKUtils r0 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x0229 }
            android.content.Context r1 = r8.Q     // Catch:{ Exception -> 0x0229 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r1 = "WRITE_HISTORY_BOOKMARKS"
            boolean r0 = r0.hasThePermission(r1)     // Catch:{ Exception -> 0x0229 }
            com.waps.AppConnect.bc = r0     // Catch:{ Exception -> 0x0229 }
            goto L_0x0037
        L_0x0229:
            r0 = move-exception
            goto L_0x0037
        L_0x022c:
            android.os.Bundle r3 = r1.metaData     // Catch:{ Exception -> 0x0229 }
            if (r3 == 0) goto L_0x007e
            android.os.Bundle r3 = r1.metaData     // Catch:{ Exception -> 0x0229 }
            java.lang.String r4 = "WAPS_PID"
            java.lang.Object r3 = r3.get(r4)     // Catch:{ Exception -> 0x0229 }
            if (r3 == 0) goto L_0x0262
            java.lang.String r2 = r3.toString()     // Catch:{ Exception -> 0x0229 }
            boolean r3 = com.waps.SDKUtils.isNull(r2)     // Catch:{ Exception -> 0x0229 }
            if (r3 != 0) goto L_0x007e
            r8.ae = r2     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = "APP_SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0229 }
            r4.<init>()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r5 = "App_Pid is setted by manifest, the value is: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r5 = r8.ae     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0229 }
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x0229 }
            goto L_0x007e
        L_0x0262:
            android.os.Bundle r3 = r1.metaData     // Catch:{ Exception -> 0x0229 }
            java.lang.String r4 = "APP_PID"
            java.lang.Object r3 = r3.get(r4)     // Catch:{ Exception -> 0x0229 }
            if (r3 == 0) goto L_0x007e
            java.lang.String r2 = r3.toString()     // Catch:{ Exception -> 0x0229 }
            boolean r3 = com.waps.SDKUtils.isNull(r2)     // Catch:{ Exception -> 0x0229 }
            if (r3 != 0) goto L_0x007e
            r8.ae = r2     // Catch:{ Exception -> 0x0229 }
            java.lang.String r3 = "APP_SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0229 }
            r4.<init>()     // Catch:{ Exception -> 0x0229 }
            java.lang.String r5 = "App_Pid is setted by manifest, the value is: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r5 = r8.ae     // Catch:{ Exception -> 0x0229 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0229 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0229 }
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x0229 }
            goto L_0x007e
        L_0x0294:
            java.lang.String r0 = "APP_SDK"
            java.lang.String r3 = "NetWork is not client."
            android.util.Log.w(r0, r3)     // Catch:{ Exception -> 0x029d }
            goto L_0x0132
        L_0x029d:
            r0 = move-exception
            java.lang.String r0 = "APP_SDK"
            java.lang.String r3 = "Permission.ACCESS_WIFI_STATE is not found or the device is Emulator, Please check it!"
            android.util.Log.w(r0, r3)     // Catch:{ Exception -> 0x0229 }
            goto L_0x0132
        L_0x02a7:
            java.lang.String r0 = "APP_SDK"
            java.lang.String r3 = "Permission.ACCESS_WIFI_STATE is not found!"
            android.util.Log.w(r0, r3)     // Catch:{ Exception -> 0x029d }
            goto L_0x0132
        L_0x02b0:
            java.lang.String r0 = r0.getExtraInfo()     // Catch:{ Exception -> 0x02bc }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x02bc }
            r8.au = r0     // Catch:{ Exception -> 0x02bc }
            goto L_0x015e
        L_0x02bc:
            r0 = move-exception
            goto L_0x0178
        L_0x02bf:
            android.content.Context r0 = r8.Q     // Catch:{ Exception -> 0x0229 }
            java.lang.String r1 = "phone"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x0229 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x0364
            java.lang.String r0 = r0.getDeviceId()     // Catch:{ Exception -> 0x0229 }
            r8.V = r0     // Catch:{ Exception -> 0x0229 }
            java.lang.String r0 = r8.V     // Catch:{ Exception -> 0x0229 }
            boolean r0 = com.waps.SDKUtils.isNull(r0)     // Catch:{ Exception -> 0x0229 }
            if (r0 == 0) goto L_0x02dd
            java.lang.String r0 = "0"
            r8.V = r0     // Catch:{ Exception -> 0x0229 }
        L_0x02dd:
            java.lang.String r0 = r8.V     // Catch:{ Exception -> 0x0314 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x0314 }
            r8.V = r0     // Catch:{ Exception -> 0x0314 }
            java.lang.String r0 = r8.V     // Catch:{ Exception -> 0x0314 }
            java.lang.String r1 = "0"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0314 }
            if (r0 == 0) goto L_0x034c
            java.lang.String r0 = r8.aw     // Catch:{ Exception -> 0x0314 }
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0314 }
            if (r0 == 0) goto L_0x034c
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0314 }
            r0.<init>()     // Catch:{ Exception -> 0x0314 }
            java.lang.String r1 = "EMULATOR"
            r0.append(r1)     // Catch:{ Exception -> 0x0314 }
            java.lang.String r1 = "emulatorDeviceId"
            r2 = 0
            java.lang.String r1 = r3.getString(r1, r2)     // Catch:{ Exception -> 0x0314 }
            boolean r2 = com.waps.SDKUtils.isNull(r1)     // Catch:{ Exception -> 0x0314 }
            if (r2 != 0) goto L_0x0317
            r8.V = r1     // Catch:{ Exception -> 0x0314 }
            goto L_0x0199
        L_0x0314:
            r0 = move-exception
            goto L_0x0199
        L_0x0317:
            java.lang.String r1 = "1234567890abcdefghijklmnopqrstuvw"
            r2 = r6
        L_0x031a:
            r4 = 32
            if (r2 >= r4) goto L_0x0332
            double r4 = java.lang.Math.random()     // Catch:{ Exception -> 0x0314 }
            r6 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r4 = r4 * r6
            int r4 = (int) r4     // Catch:{ Exception -> 0x0314 }
            int r4 = r4 % 30
            char r4 = r1.charAt(r4)     // Catch:{ Exception -> 0x0314 }
            r0.append(r4)     // Catch:{ Exception -> 0x0314 }
            int r2 = r2 + 1
            goto L_0x031a
        L_0x0332:
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0314 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x0314 }
            r8.V = r0     // Catch:{ Exception -> 0x0314 }
            android.content.SharedPreferences$Editor r0 = r3.edit()     // Catch:{ Exception -> 0x0314 }
            java.lang.String r1 = "emulatorDeviceId"
            java.lang.String r2 = r8.V     // Catch:{ Exception -> 0x0314 }
            r0.putString(r1, r2)     // Catch:{ Exception -> 0x0314 }
            r0.commit()     // Catch:{ Exception -> 0x0314 }
            goto L_0x0199
        L_0x034c:
            java.lang.String r0 = r8.V     // Catch:{ Exception -> 0x0314 }
            java.lang.String r1 = "0"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0314 }
            if (r0 == 0) goto L_0x0199
            java.lang.String r0 = r8.aw     // Catch:{ Exception -> 0x0314 }
            boolean r0 = com.waps.SDKUtils.isNull(r0)     // Catch:{ Exception -> 0x0314 }
            if (r0 != 0) goto L_0x0199
            java.lang.String r0 = r8.aw     // Catch:{ Exception -> 0x0314 }
            r8.V = r0     // Catch:{ Exception -> 0x0314 }
            goto L_0x0199
        L_0x0364:
            r0 = 0
            r8.V = r0     // Catch:{ Exception -> 0x0229 }
            goto L_0x0199
        L_0x0369:
            r0 = move-exception
            goto L_0x01e4
        L_0x036c:
            r0 = move-exception
            goto L_0x0013
        L_0x036f:
            r0 = r2
            goto L_0x0191
        L_0x0372:
            r2 = r3
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.AppConnect.c():void");
    }

    public void checkUpdate() {
        bf = new i(this, null);
        bf.execute(new Void[0]);
    }

    public void clickAd(String str) {
        try {
            b(str, "");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public String d() {
        String str;
        String loadStringFromLocal = new SDKUtils(this.Q).loadStringFromLocal("UnPackage.dat", "/Android/data/cache");
        try {
            String installed = new SDKUtils(this.Q).getInstalled();
            if (!SDKUtils.isNull(loadStringFromLocal)) {
                String[] split = loadStringFromLocal.replaceAll("\n", "").split(";");
                String str2 = "";
                for (int i2 = 0; i2 < split.length; i2++) {
                    if (!installed.contains(split[i2])) {
                        if (split[i2].startsWith("com.")) {
                            str2 = str2 + split[i2].substring(3, split[i2].length()) + ";";
                        } else {
                            str2 = str2 + split[i2] + ";";
                        }
                    }
                }
                if (str2.endsWith(";")) {
                    str2.substring(0, str2.length() - 1);
                }
                str = str2;
            } else {
                str = "";
            }
            if (!SDKUtils.isNull(installed)) {
                new SDKUtils(this.Q).saveDataToLocal(installed, "UnPackage.dat", "/Android/data/cache");
            }
            return str;
        } catch (Exception e2) {
            return "";
        }
    }

    public void downloadAd(String str) {
        try {
            b(str, "download");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void finalize() {
        try {
            if (((Activity) this.Q).isFinishing()) {
                S = null;
                SharedPreferences.Editor edit = this.Q.getSharedPreferences("Start_Tag", 3).edit();
                edit.clear();
                edit.commit();
                OffersWebView.outsideFlag = false;
                aW = true;
                aZ = true;
                bd = true;
                aX = null;
                aN = false;
                aK = false;
                aL = false;
                bv = null;
            }
        } catch (Exception e2) {
        }
    }

    public AdInfo getAdInfo() {
        AdInfo adInfo = null;
        try {
            List adInfoList = getAdInfoList();
            if (adInfoList != null && adInfoList.size() > 0) {
                SharedPreferences sharedPreferences = this.Q.getSharedPreferences("AppSettings", 0);
                int i2 = sharedPreferences.getInt("DiyAd_ShowTag", -1);
                if (i2 != -1) {
                    bw = i2;
                }
                if (bw >= adInfoList.size()) {
                    bw = 0;
                }
                if (adInfoList != null && adInfoList.size() > 0) {
                    adInfo = (AdInfo) adInfoList.get(bw);
                }
                bw++;
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putInt("DiyAd_ShowTag", bw);
                edit.commit();
            }
        } catch (Exception e2) {
        }
        return adInfo;
    }

    public List getAdInfoList() {
        try {
            if (H == null) {
                H = MiniAdView.h;
            }
            if (H != null && H.size() > 0) {
                return a(H);
            }
            if (bv == null || H == null) {
                bv = new k(this, null);
                bv.execute(new Void[0]);
            }
            Long valueOf = Long.valueOf(System.currentTimeMillis());
            boolean z2 = true;
            while (z2) {
                if (H != null && H.size() > 0) {
                    return a(H);
                }
                if (System.currentTimeMillis() - valueOf.longValue() > 4000) {
                    z2 = false;
                }
                Thread.sleep(400);
            }
            return null;
        } catch (Exception e2) {
        }
    }

    public String getConfig(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean z2 = true;
        while (z2) {
            try {
                Map c2 = c("");
                if (c2 != null && c2.size() > 0 && ((str != null && !SDKUtils.isNull((String) c2.get(str))) || System.currentTimeMillis() - currentTimeMillis > 4000)) {
                    return (String) c2.get(str);
                }
                Thread.sleep(100);
                if (System.currentTimeMillis() - currentTimeMillis > 4000) {
                    return "";
                }
            } catch (Exception e2) {
                z2 = z2;
            }
        }
        return "";
    }

    public Map getConfigMap() {
        long currentTimeMillis = System.currentTimeMillis();
        while (1 != 0) {
            try {
                Map c2 = c("");
                if (c2 != null || System.currentTimeMillis() - currentTimeMillis > 1000) {
                    return c2;
                }
                Thread.sleep(100);
                if (System.currentTimeMillis() - currentTimeMillis > 1000) {
                    return null;
                }
            } catch (Exception e2) {
            }
        }
        return null;
    }

    public void getIconAd() {
        a("iconad/ad?", this.ak);
    }

    public void getPoints(UpdatePointsNotifier updatePointsNotifier) {
        if (S != null) {
            ay = updatePointsNotifier;
            S.u();
        }
    }

    public void getPushAd() {
        long j2 = this.Q.getSharedPreferences("AppSettings", 0).getLong("PushAd_CreateTime", 0);
        if (j2 == 0) {
            c("", "push/api_ad?", this.ak);
        } else if (System.currentTimeMillis() - j2 > 600000) {
            c("", "push/api_ad?", this.ak);
        } else {
            Log.d("APP_SDK", "The getPushAd() only can execute one time in ten minite.");
        }
    }

    public void initAdInfo() {
        if (H == null) {
            bv = new k(this, null);
            bv.execute(new Void[0]);
        }
    }

    public void pushMessage(String str, String str2, String str3) {
        this.K = str;
        this.L = str2;
        this.M = str3;
        if (!SDKUtils.isNull(this.K)) {
            a("", this.K, this.L, this.M, this.ak);
        }
    }

    public void setAdBackColor(int i2) {
        F = i2;
    }

    public void setAdForeColor(int i2) {
        G = i2;
    }

    public void setAdViewClassName(String str) {
        SharedPreferences.Editor edit = this.Q.getSharedPreferences("AppSettings", 0).edit();
        edit.putString("OfferName", str);
        edit.commit();
    }

    public void setCrashReport(boolean z2) {
        bd = z2;
        if (bd) {
            aa.a(this.Q, T, this.ak).a();
            bd = false;
        }
    }

    public void setPushAudio(boolean z2) {
        aK = z2;
        aL = z2;
        aN = true;
    }

    public void setPushIcon(int i2) {
        this.aJ = i2;
        this.aM = true;
    }

    public void showFeedback() {
        this.Q.startActivity(showFeedback_forTab());
    }

    public Intent showFeedback_forTab() {
        Intent intent = new Intent();
        intent.setClass(this.Q, g(this.Q));
        intent.putExtra("UrlPath", "http://app.wapx.cn/action/feedback/form");
        intent.putExtra("ACTIVITY_FLAG", "feedback");
        intent.putExtra("URL_PARAMS", this.ak);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    public void showMore(Context context) {
        showMore(context, this.V);
    }

    public void showMore(Context context, String str) {
        try {
            Intent intent = new Intent(context, g(context));
            intent.putExtra("Offers_URL", this.ai);
            intent.putExtra("USER_ID", str);
            intent.putExtra("URL_PARAMS", this.ak);
            intent.putExtra("CLIENT_PACKAGE", this.aj);
            intent.putExtra("offers_webview_tag", "OffersWebView");
            context.startActivity(intent);
        } catch (Exception e2) {
            Log.v("APP_SDK", SDKUtils.getErrorLog(e2));
        }
    }

    public Intent showMore_forTab(Context context) {
        return showMore_forTab(context, this.V);
    }

    public Intent showMore_forTab(Context context, String str) {
        Intent intent = new Intent(context, g(context));
        intent.putExtra("Offers_URL", this.ai);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.ak);
        intent.putExtra("CLIENT_PACKAGE", this.aj);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    public void showOffers(Context context) {
        showOffers(context, this.V);
    }

    public void showOffers(Context context, String str) {
        try {
            Intent intent = new Intent(context, g(context));
            intent.putExtra("Offers_URL", this.ah);
            intent.putExtra("USER_ID", str);
            intent.putExtra("URL_PARAMS", this.ak);
            intent.putExtra("CLIENT_PACKAGE", this.aj);
            intent.putExtra("offers_webview_tag", "OffersWebView");
            context.startActivity(intent);
        } catch (Exception e2) {
            Log.v("APP_SDK", SDKUtils.getErrorLog(e2));
        }
    }

    public Intent showOffers_forTab(Context context) {
        return showOffers_forTab(context, this.V);
    }

    public Intent showOffers_forTab(Context context, String str) {
        Intent intent = new Intent(context, g(context));
        intent.putExtra("Offers_URL", this.ah);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.ak);
        intent.putExtra("CLIENT_PACKAGE", this.aj);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    public void spendPoints(int i2, UpdatePointsNotifier updatePointsNotifier) {
        if (i2 >= 0) {
            this.af = "" + i2;
            if (S != null) {
                ay = updatePointsNotifier;
                S.v();
            }
        }
    }
}
