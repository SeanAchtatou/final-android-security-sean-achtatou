package com.waps;

import android.webkit.DownloadListener;
import android.widget.Toast;

class am implements DownloadListener {
    final /* synthetic */ OffersWebView a;

    am(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        if (SDKUtils.isNull(this.a.s)) {
            Toast.makeText(this.a, (CharSequence) OffersWebView.z.get("wrong_url"), 1).show();
            this.a.finish();
            return;
        }
        this.a.a = new w(this.a, this.a.j, this.a.s);
        this.a.b();
    }
}
