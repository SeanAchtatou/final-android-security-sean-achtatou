package com.waps;

import android.os.AsyncTask;

class s extends AsyncTask {
    String a = "";
    String b = "";
    final /* synthetic */ AppConnect c;

    public s(AppConnect appConnect, String str, String str2) {
        this.c = appConnect;
        this.a = str;
        this.b = str2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.T.a(this.a, this.b);
        if (!SDKUtils.isNull(a2)) {
            z = this.c.e(a2);
        }
        return Boolean.valueOf(z);
    }
}
