package com.waps;

import android.content.Intent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class c extends WebViewClient {
    final /* synthetic */ AdView a;

    private c(AdView adView) {
        this.a = adView;
    }

    /* synthetic */ c(AdView adView, b bVar) {
        this(adView);
    }

    public void onPageFinished(WebView webView, String str) {
        try {
            this.a.mHandler.post(new e(this));
        } catch (Exception e) {
        }
        super.onPageFinished(webView, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.AdView.b(com.waps.AdView, boolean):boolean
     arg types: [com.waps.AdView, int]
     candidates:
      com.waps.AdView.b(com.waps.AdView, java.lang.String):java.lang.String
      com.waps.AdView.b(com.waps.AdView, boolean):boolean */
    public void onReceivedError(WebView webView, int i, String str, String str2) {
        webView.setScrollBarStyle(0);
        if (i == -2 || i == -6 || i == -12 || i == -10 || i == -13) {
        }
        try {
            webView.loadDataWithBaseURL("", "<html></html>", "text/html", "utf-8", "");
            this.a.mHandler.post(new d(this));
            boolean unused = this.a.i = true;
            boolean unused2 = this.a.r = true;
        } catch (Exception e) {
        }
        super.onReceivedError(webView, i, str, str2);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        try {
            if (SDKUtils.isNull(str) || !str.contains("?")) {
                return true;
            }
            String unused = this.a.m = str;
            Intent intent = new Intent(this.a.a, AppConnect.g(this.a.a));
            intent.putExtra("URL", this.a.m);
            intent.putExtra("offers_webview_tag", "OffersWebView");
            if (str.indexOf("display/ad_click?") < 0) {
                intent.putExtra("isFinshClose", "true");
            }
            this.a.a.startActivity(intent);
            return true;
        } catch (Exception e) {
            return true;
        }
    }
}
