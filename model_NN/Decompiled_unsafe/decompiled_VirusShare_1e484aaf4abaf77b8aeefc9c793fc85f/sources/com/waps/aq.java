package com.waps;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class aq extends WebViewClient {
    final /* synthetic */ OffersWebView a;

    private aq(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    /* synthetic */ aq(OffersWebView offersWebView, am amVar) {
        this(offersWebView);
    }

    public void onPageFinished(WebView webView, String str) {
        this.a.l.setVisibility(8);
        super.onPageFinished(webView, str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        this.a.l.setVisibility(0);
        super.onPageStarted(webView, str, bitmap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.OffersWebView.a(com.waps.OffersWebView, boolean):boolean
     arg types: [com.waps.OffersWebView, int]
     candidates:
      com.waps.OffersWebView.a(com.waps.OffersWebView, java.lang.String):java.lang.String
      com.waps.OffersWebView.a(com.waps.OffersWebView, boolean):boolean */
    public void onReceivedError(WebView webView, int i, String str, String str2) {
        try {
            webView.setScrollBarStyle(0);
            webView.loadDataWithBaseURL("", "<html><body bgcolor=\"000000\" align=\"center\"><br/><font color=\"ffffff\">" + ((String) OffersWebView.z.get("network_links_failure")) + "</font>" + "<br/></body></html>", "text/html", "utf-8", "");
            boolean unused = this.a.t = false;
        } catch (Exception e) {
        }
        super.onReceivedError(webView, i, str, str2);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String unused = this.a.s = str;
        return new SDKUtils(this.a).openUrl(webView, str);
    }
}
