package com.waps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class y extends BroadcastReceiver {
    public static boolean c = false;
    private static String d = "";
    int a;
    x b;
    private String e;
    private SharedPreferences f;
    private SharedPreferences.Editor g;

    public y(x xVar, int i, String str) {
        this.a = i;
        this.b = xVar;
        this.e = str;
    }

    public y(String str) {
        this.e = str;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            d = intent.getDataString().substring(8);
            AppConnect.getInstanceNoConnect(context).a(d, 0);
            try {
                if (this.b != null) {
                    this.b.a(this.a, this.e);
                }
                context.startActivity(context.getPackageManager().getLaunchIntentForPackage(d));
                c = true;
                if (w.e) {
                    this.f = context.getSharedPreferences("DownLoadSave", 3);
                    this.g = this.f.edit();
                    this.g.putString(this.f.getAll().size() + "", d);
                    this.g.commit();
                    c = true;
                }
                if (AppConnect.D != null && AppConnect.D.equals("shortcut")) {
                    new SDKUtils(context).createShortcut_forApp(d);
                }
            } catch (Exception e2) {
            }
        }
        if (intent.getAction().equals("android.intent.action.PACKAGE_REMOVED")) {
            AppConnect.getInstanceNoConnect(context).a(intent.getDataString().substring(8), 3);
        }
        context.unregisterReceiver(this);
    }
}
