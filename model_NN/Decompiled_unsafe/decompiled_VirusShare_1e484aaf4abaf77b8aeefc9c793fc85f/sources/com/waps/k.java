package com.waps;

import android.os.AsyncTask;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

class k extends AsyncTask {
    String a;
    final /* synthetic */ AppConnect b;

    private k(AppConnect appConnect) {
        this.b = appConnect;
        this.a = "";
    }

    /* synthetic */ k(AppConnect appConnect, g gVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        this.a = AppConnect.T.a("http://ads.wapx.cn/action/miniad/ad?", this.b.ak);
        try {
            if (!SDKUtils.isNull(this.a)) {
                AppConnect.H = new ad(this.b.Q).a(new ByteArrayInputStream(this.a.getBytes("UTF-8")));
                z = true;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
    }
}
