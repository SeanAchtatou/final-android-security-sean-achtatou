package com.waps;

import android.os.AsyncTask;

class l extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private l(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ l(AppConnect appConnect, g gVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.T.a("http://app.wapx.cn/action/account/award?", this.a.ak + "&points=" + this.a.ag);
        if (!SDKUtils.isNull(a2)) {
            z = this.a.j(a2);
        }
        if (!z) {
            AppConnect.ay.getUpdatePointsFailed((String) AppConnect.E.get("failed_to_award_points"));
        }
        return Boolean.valueOf(z);
    }
}
