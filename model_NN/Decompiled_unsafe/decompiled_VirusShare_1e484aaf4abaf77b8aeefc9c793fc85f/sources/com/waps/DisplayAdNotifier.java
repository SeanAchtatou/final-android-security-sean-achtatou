package com.waps;

import android.view.View;

public interface DisplayAdNotifier {
    void getDisplayAdResponse(View view);

    void getDisplayAdResponse(Object obj);

    void getDisplayAdResponseFailed(String str);
}
