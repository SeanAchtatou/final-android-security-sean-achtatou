package com.waps;

import android.content.Context;
import android.os.Process;
import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread;
import java.text.SimpleDateFormat;
import java.util.Date;

public class aa implements Thread.UncaughtExceptionHandler {
    private static aa b;
    /* access modifiers changed from: private */
    public static Context c;
    /* access modifiers changed from: private */
    public static z e = null;
    private Thread.UncaughtExceptionHandler a;
    private ac d;
    /* access modifiers changed from: private */
    public String f = "";
    private String g = "";
    /* access modifiers changed from: private */
    public String h = "errorLog.txt";

    private aa(Context context, z zVar, String str) {
        c = context;
        e = zVar;
        this.f = str;
    }

    public static aa a(Context context, z zVar, String str) {
        if (b == null) {
            b = new aa(context, zVar, str);
        }
        return b;
    }

    private boolean a(Throwable th) {
        if (th == null) {
            return true;
        }
        new ab(this, th.getLocalizedMessage()).start();
        b(th);
        if (e != null) {
            a(c);
        }
        return true;
    }

    private void b(Throwable th) {
        String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis()));
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        this.g = stringWriter.toString();
        Log.v("Exception", this.g);
        printWriter.close();
        try {
            if (this.g.length() > 5000) {
                this.g = this.g.substring(0, 5000);
            }
            this.g += "[" + format + "]";
            new SDKUtils(c).saveDataToLocal(this.g, this.h, "/Android/data/cache");
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public String d() {
        try {
            return new SDKUtils(c).loadStringFromLocal(this.h, "/Android/data/cache");
        } catch (Exception e2) {
            return "";
        }
    }

    public void a() {
        this.a = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
        if (e != null) {
            AppConnect.a(c, e);
            a(c);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        this.d = new ac(this, null);
        this.d.execute(new Void[0]);
    }

    public void uncaughtException(Thread thread, Throwable th) {
        if (a(th) || this.a == null) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e2) {
            }
            Process.killProcess(Process.myPid());
            System.exit(10);
            return;
        }
        this.a.uncaughtException(thread, th);
    }
}
