package com.waps;

import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;

class ac extends AsyncTask {
    final /* synthetic */ aa a;

    private ac(aa aaVar) {
        this.a = aaVar;
    }

    /* synthetic */ ac(aa aaVar, ab abVar) {
        this(aaVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        String a2 = this.a.d();
        if (!SDKUtils.isNull(a2)) {
            try {
                String b = aa.e.b("http://app.wapx.cn/action/report/error?" + this.a.f, a2);
                if (!SDKUtils.isNull(b) && "ok".equals(b.trim())) {
                    AppLog.w("APP_SDK", "Send error log!");
                    File file = new File(Environment.getExternalStorageDirectory().toString() + "/Android/data/cache" + "/" + this.a.h);
                    File fileStreamPath = aa.c.getFileStreamPath(this.a.h);
                    if (Environment.getExternalStorageState().equals("mounted") && file.exists()) {
                        file.delete();
                    }
                    if (fileStreamPath.exists()) {
                        fileStreamPath.delete();
                    }
                }
            } catch (Exception e) {
            }
        }
        return true;
    }
}
