package com.waps;

import android.content.Context;
import android.util.Xml;
import android.widget.LinearLayout;
import com.lxx.wchw.z5root.R;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class ad {
    LinearLayout a;
    final String b = "Display Ad";
    private af c = null;
    /* access modifiers changed from: private */
    public DisplayAdNotifier d;
    /* access modifiers changed from: private */
    public z e = null;
    private Context f;
    /* access modifiers changed from: private */
    public String g = "";
    private String h = "";
    /* access modifiers changed from: private */
    public String i = "";

    public ad(Context context) {
        this.f = context;
        this.e = new z(this.f);
    }

    /* access modifiers changed from: private */
    public boolean a(String str) {
        try {
            List a2 = a(new ByteArrayInputStream(str.getBytes("UTF-8")));
            if (a2 != null && a2.size() > 0) {
                this.d.getDisplayAdResponse(a2);
            }
            this.a = null;
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public List a(InputStream inputStream) {
        a aVar;
        ArrayList arrayList;
        try {
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            a aVar2 = null;
            ArrayList arrayList2 = null;
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 0:
                        a aVar3 = aVar2;
                        arrayList = new ArrayList();
                        aVar = aVar3;
                        continue;
                        arrayList2 = arrayList;
                        aVar2 = aVar;
                    case 2:
                        aVar = "Ad".equals(newPullParser.getName()) ? new a() : aVar2;
                        if (aVar != null) {
                            if ("Id".equals(newPullParser.getName())) {
                                aVar.a(newPullParser.nextText());
                            }
                            if ("Title".equals(newPullParser.getName())) {
                                aVar.h(newPullParser.nextText());
                            }
                            if ("Image".equals(newPullParser.getName())) {
                                aVar.b(newPullParser.nextText());
                            }
                            if ("Content".equals(newPullParser.getName())) {
                                aVar.c(newPullParser.nextText());
                            }
                            if ("ClickUrl".equals(newPullParser.getName())) {
                                aVar.d(newPullParser.nextText());
                            }
                            if ("Show_detail".equals(newPullParser.getName())) {
                                aVar.e(newPullParser.nextText());
                            }
                            if ("AdPackage".equals(newPullParser.getName())) {
                                aVar.f(newPullParser.nextText());
                            }
                            if ("NewBrowser".equals(newPullParser.getName())) {
                                aVar.g(newPullParser.nextText());
                            }
                            if ("Points".equals(newPullParser.getName())) {
                                aVar.a(Integer.parseInt(newPullParser.nextText()));
                            }
                            if ("Description".equals(newPullParser.getName())) {
                                aVar.l(newPullParser.nextText());
                            }
                            if ("Version".equals(newPullParser.getName())) {
                                aVar.m(newPullParser.nextText());
                            }
                            if ("Filesize".equals(newPullParser.getName())) {
                                aVar.n(newPullParser.nextText());
                            }
                            if ("Provider".equals(newPullParser.getName())) {
                                aVar.o(newPullParser.nextText());
                            }
                            if ("ImageUrls".equals(newPullParser.getName())) {
                                aVar.a(newPullParser.nextText().split("\\|"));
                            }
                            if ("DownUrl".equals(newPullParser.getName())) {
                                aVar.p(newPullParser.nextText());
                                arrayList = arrayList2;
                                continue;
                                arrayList2 = arrayList;
                                aVar2 = aVar;
                            }
                        }
                        arrayList = arrayList2;
                        arrayList2 = arrayList;
                        aVar2 = aVar;
                    case R.styleable.com_admob_android_ads_AdView_keywords:
                        if ("Ad".equals(newPullParser.getName()) && aVar2 != null && !SDKUtils.isNull(aVar2.a())) {
                            arrayList2.add(aVar2);
                            aVar = null;
                            arrayList = arrayList2;
                            continue;
                            arrayList2 = arrayList;
                            aVar2 = aVar;
                        }
                }
                aVar = aVar2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                aVar2 = aVar;
            }
            return arrayList2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2, DisplayAdNotifier displayAdNotifier) {
        this.h = str;
        this.g = this.h + "miniad/ad?";
        this.i = str2;
        this.d = displayAdNotifier;
        if (this.e != null) {
            AppConnect.a(this.f, this.e);
        }
        if (AppConnect.H == null || AppConnect.H.size() <= 0) {
            this.c = new af(this);
            this.c.execute(new Void[0]);
            return;
        }
        this.d.getDisplayAdResponse(AppConnect.H);
    }
}
