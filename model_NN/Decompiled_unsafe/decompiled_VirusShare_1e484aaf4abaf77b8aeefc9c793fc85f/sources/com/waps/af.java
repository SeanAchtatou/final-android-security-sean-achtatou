package com.waps;

import android.os.AsyncTask;

class af extends AsyncTask {
    final /* synthetic */ ad a;

    private af(ad adVar) {
        this.a = adVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = this.a.e.a(this.a.g, this.a.i);
        if (SDKUtils.isNull(a2)) {
            this.a.d.getDisplayAdResponseFailed("Network Error.");
        } else {
            z = this.a.a(a2);
            if (!z) {
                this.a.d.getDisplayAdResponseFailed("Ad content has error.");
            }
        }
        return Boolean.valueOf(z);
    }
}
