package com.waps;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.io.File;
import java.util.Map;

public class x {
    protected static boolean b = false;
    public static boolean c = true;
    public static String d = "";
    static long e;
    public Notification a;
    private NotificationManager f;
    private Context g;
    private String h;
    private String i = "";
    private String j = "";
    private String k = "";
    private String l = "";
    private String m = "";
    private boolean n = true;
    private boolean o = true;
    private Map p;

    public x(Context context) {
        this.g = context;
        e = System.currentTimeMillis();
    }

    private ImageView a(View view) {
        if (view instanceof ViewGroup) {
            for (int childCount = ((ViewGroup) view).getChildCount(); childCount > 0; childCount--) {
                ImageView a2 = a(((ViewGroup) view).getChildAt(childCount - 1));
                if (a2 != null) {
                    return a2;
                }
            }
        }
        if (view instanceof ImageView) {
            return (ImageView) view;
        }
        return null;
    }

    public void a(int i2, String str) {
        File fileStreamPath;
        this.f.cancel(i2);
        if (!Environment.getExternalStorageState().equals("mounted") && (fileStreamPath = this.g.getFileStreamPath(str)) != null) {
            fileStreamPath.delete();
        }
    }

    public void a(View view, String str, int i2, String str2) {
        if (this.p == null || this.p.size() == 0) {
            this.p = AppConnect.d(this.g);
        }
        this.a = new Notification();
        this.a.icon = 17301633;
        this.a.tickerText = (CharSequence) this.p.get("downloading");
        this.a.when = e;
        this.a.flags = 16;
        this.a.setLatestEventInfo(this.g, str, ((String) this.p.get("finish_download")) + str2 + "", PendingIntent.getActivity(this.g, 100, new Intent(), 268435456));
        this.f = (NotificationManager) this.g.getSystemService("notification");
        this.f.notify(i2, this.a);
    }

    public void a(View view, String str, int i2, String str2, String str3) {
        if (this.p == null || this.p.size() == 0) {
            this.p = AppConnect.d(this.g);
        }
        this.h = str2;
        this.a = new Notification();
        this.a.icon = 17301634;
        this.a.tickerText = "";
        this.a.when = System.currentTimeMillis();
        this.a.defaults = 1;
        this.a.flags = 16;
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        if (Environment.getExternalStorageState().equals("mounted")) {
            intent.setDataAndType(Uri.fromFile(new File("/sdcard/download/" + str)), "application/vnd.android.package-archive");
        } else {
            intent.setDataAndType(Uri.fromFile(this.g.getFileStreamPath(str)), "application/vnd.android.package-archive");
        }
        this.a.setLatestEventInfo(this.g, str, str3, PendingIntent.getActivity(this.g, 100, intent, 0));
        this.f = (NotificationManager) this.g.getSystemService("notification");
        this.f.notify(i2, this.a);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c1 A[Catch:{ Exception -> 0x0211 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0229 A[Catch:{ Exception -> 0x0211 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r11, int r12, boolean r13, android.graphics.Bitmap r14, java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, java.lang.String r19) {
        /*
            r10 = this;
            java.util.Map r2 = r10.p
            if (r2 == 0) goto L_0x000c
            java.util.Map r2 = r10.p
            int r2 = r2.size()
            if (r2 != 0) goto L_0x0014
        L_0x000c:
            android.content.Context r2 = r10.g
            java.util.Map r2 = com.waps.AppConnect.d(r2)
            r10.p = r2
        L_0x0014:
            r0 = r17
            r1 = r10
            r1.l = r0
            r10.j = r15
            r0 = r16
            r1 = r10
            r1.k = r0
            java.lang.String r2 = r10.j
            android.app.Notification r3 = new android.app.Notification
            r3.<init>()
            r10.a = r3
            if (r12 != 0) goto L_0x0162
            android.app.Notification r3 = r10.a
            r4 = 17301651(0x1080093, float:2.4979667E-38)
            r3.icon = r4
        L_0x0032:
            android.app.Notification r3 = r10.a
            java.lang.String r4 = r10.k
            r3.tickerText = r4
            android.app.Notification r3 = r10.a
            long r4 = java.lang.System.currentTimeMillis()
            r3.when = r4
            if (r13 == 0) goto L_0x0047
            android.app.Notification r3 = r10.a
            r4 = 1
            r3.defaults = r4
        L_0x0047:
            android.app.Notification r3 = r10.a
            r4 = 16
            r3.flags = r4
            android.content.Intent r3 = new android.content.Intent
            r3.<init>()
            boolean r4 = com.waps.SDKUtils.isNull(r18)
            if (r4 != 0) goto L_0x006a
            android.content.Context r4 = r10.g
            android.content.pm.PackageManager r4 = r4.getPackageManager()
            r0 = r4
            r1 = r18
            android.content.Intent r3 = r0.getLaunchIntentForPackage(r1)     // Catch:{ Exception -> 0x0168 }
        L_0x0065:
            if (r3 == 0) goto L_0x0177
            r4 = 0
            r10.o = r4
        L_0x006a:
            boolean r4 = r10.o
            if (r4 == 0) goto L_0x009d
            boolean r4 = com.waps.SDKUtils.isNull(r19)
            if (r4 != 0) goto L_0x01a4
            java.lang.String r4 = ""
            com.waps.SDKUtils r4 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x0191 }
            android.content.Context r5 = r10.g     // Catch:{ Exception -> 0x0191 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0191 }
            r0 = r4
            r1 = r19
            java.lang.String r4 = r0.getBrowserPackageName(r1)     // Catch:{ Exception -> 0x0191 }
            boolean r5 = com.waps.SDKUtils.isNull(r4)     // Catch:{ Exception -> 0x0191 }
            if (r5 != 0) goto L_0x017c
            com.waps.SDKUtils r5 = new com.waps.SDKUtils     // Catch:{ Exception -> 0x0191 }
            android.content.Context r6 = r10.g     // Catch:{ Exception -> 0x0191 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0191 }
            java.lang.String r6 = r10.l     // Catch:{ Exception -> 0x0191 }
            android.content.Context r7 = r10.g     // Catch:{ Exception -> 0x0191 }
            android.content.pm.PackageManager r7 = r7.getPackageManager()     // Catch:{ Exception -> 0x0191 }
            android.content.Intent r3 = r5.goToTargetBrowser_Intent(r4, r6, r7)     // Catch:{ Exception -> 0x0191 }
        L_0x009d:
            android.content.Context r4 = r10.g
            r5 = 100
            r6 = 268435456(0x10000000, float:2.5243549E-29)
            android.app.PendingIntent r3 = android.app.PendingIntent.getActivity(r4, r5, r3, r6)
            android.app.Notification r4 = r10.a
            r4.contentIntent = r3
            android.content.Context r4 = r10.g     // Catch:{ Exception -> 0x0211 }
            android.content.res.Resources r4 = r4.getResources()     // Catch:{ Exception -> 0x0211 }
            java.lang.String r5 = "push_layout"
            java.lang.String r6 = "layout"
            android.content.Context r7 = r10.g     // Catch:{ Exception -> 0x0211 }
            java.lang.String r7 = r7.getPackageName()     // Catch:{ Exception -> 0x0211 }
            int r4 = r4.getIdentifier(r5, r6, r7)     // Catch:{ Exception -> 0x0211 }
            if (r4 == 0) goto L_0x0229
            android.widget.RemoteViews r3 = new android.widget.RemoteViews     // Catch:{ Exception -> 0x0211 }
            android.content.Context r4 = r10.g     // Catch:{ Exception -> 0x0211 }
            java.lang.String r4 = r4.getPackageName()     // Catch:{ Exception -> 0x0211 }
            android.content.Context r5 = r10.g     // Catch:{ Exception -> 0x0211 }
            android.content.res.Resources r5 = r5.getResources()     // Catch:{ Exception -> 0x0211 }
            java.lang.String r6 = "push_layout"
            java.lang.String r7 = "layout"
            android.content.Context r8 = r10.g     // Catch:{ Exception -> 0x0211 }
            java.lang.String r8 = r8.getPackageName()     // Catch:{ Exception -> 0x0211 }
            int r5 = r5.getIdentifier(r6, r7, r8)     // Catch:{ Exception -> 0x0211 }
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x0211 }
            java.lang.String r4 = r10.j     // Catch:{ Exception -> 0x0211 }
            boolean r4 = com.waps.SDKUtils.isNull(r4)     // Catch:{ Exception -> 0x0211 }
            if (r4 != 0) goto L_0x01f6
            android.content.Context r4 = r10.g     // Catch:{ Exception -> 0x0211 }
            android.content.res.Resources r4 = r4.getResources()     // Catch:{ Exception -> 0x0211 }
            java.lang.String r5 = "notify_image"
            java.lang.String r6 = "id"
            android.content.Context r7 = r10.g     // Catch:{ Exception -> 0x0211 }
            java.lang.String r7 = r7.getPackageName()     // Catch:{ Exception -> 0x0211 }
            int r4 = r4.getIdentifier(r5, r6, r7)     // Catch:{ Exception -> 0x0211 }
            r3.setImageViewBitmap(r4, r14)     // Catch:{ Exception -> 0x0211 }
            android.content.Context r4 = r10.g     // Catch:{ Exception -> 0x0211 }
            android.content.res.Resources r4 = r4.getResources()     // Catch:{ Exception -> 0x0211 }
            java.lang.String r5 = "notify_text"
            java.lang.String r6 = "id"
            android.content.Context r7 = r10.g     // Catch:{ Exception -> 0x0211 }
            java.lang.String r7 = r7.getPackageName()     // Catch:{ Exception -> 0x0211 }
            int r4 = r4.getIdentifier(r5, r6, r7)     // Catch:{ Exception -> 0x0211 }
            r3.setTextViewText(r4, r2)     // Catch:{ Exception -> 0x0211 }
            android.content.Context r2 = r10.g     // Catch:{ Exception -> 0x0211 }
            android.content.res.Resources r2 = r2.getResources()     // Catch:{ Exception -> 0x0211 }
            java.lang.String r4 = "content_text"
            java.lang.String r5 = "id"
            android.content.Context r6 = r10.g     // Catch:{ Exception -> 0x0211 }
            java.lang.String r6 = r6.getPackageName()     // Catch:{ Exception -> 0x0211 }
            int r2 = r2.getIdentifier(r4, r5, r6)     // Catch:{ Exception -> 0x0211 }
            java.lang.String r4 = r10.k     // Catch:{ Exception -> 0x0211 }
            r3.setTextViewText(r2, r4)     // Catch:{ Exception -> 0x0211 }
        L_0x012f:
            android.app.Notification r2 = r10.a     // Catch:{ Exception -> 0x0211 }
            r2.contentView = r3     // Catch:{ Exception -> 0x0211 }
        L_0x0133:
            android.content.Context r2 = r10.g     // Catch:{ Exception -> 0x0211 }
            java.lang.String r3 = "notification"
            java.lang.Object r11 = r2.getSystemService(r3)     // Catch:{ Exception -> 0x0211 }
            android.app.NotificationManager r11 = (android.app.NotificationManager) r11     // Catch:{ Exception -> 0x0211 }
            r10.f = r11     // Catch:{ Exception -> 0x0211 }
            android.app.NotificationManager r2 = r10.f     // Catch:{ Exception -> 0x0211 }
            r3 = 2
            android.app.Notification r4 = r10.a     // Catch:{ Exception -> 0x0211 }
            r2.notify(r3, r4)     // Catch:{ Exception -> 0x0211 }
        L_0x0147:
            r2 = 1
            com.waps.x.b = r2
            android.content.Context r2 = r10.g
            java.lang.String r3 = "Start_Tag"
            r4 = 3
            android.content.SharedPreferences r2 = r2.getSharedPreferences(r3, r4)
            android.content.SharedPreferences$Editor r2 = r2.edit()
            java.lang.String r3 = "notify_start_tag"
            java.lang.String r4 = "Notify"
            r2.putString(r3, r4)
            r2.commit()
            return
        L_0x0162:
            android.app.Notification r3 = r10.a
            r3.icon = r12
            goto L_0x0032
        L_0x0168:
            r4 = move-exception
            java.lang.String r5 = "APP_SDK"
            java.lang.String r6 = com.waps.SDKUtils.getErrorLog(r4)
            android.util.Log.v(r5, r6)
            r4.printStackTrace()
            goto L_0x0065
        L_0x0177:
            r4 = 1
            r10.o = r4
            goto L_0x006a
        L_0x017c:
            android.content.Intent r4 = new android.content.Intent     // Catch:{ Exception -> 0x0191 }
            java.lang.String r5 = "android.intent.action.VIEW"
            java.lang.String r6 = r10.l     // Catch:{ Exception -> 0x0191 }
            android.net.Uri r6 = android.net.Uri.parse(r6)     // Catch:{ Exception -> 0x0191 }
            r4.<init>(r5, r6)     // Catch:{ Exception -> 0x0191 }
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            r4.setFlags(r3)     // Catch:{ Exception -> 0x0265 }
            r3 = r4
            goto L_0x009d
        L_0x0191:
            r4 = move-exception
            r9 = r4
            r4 = r3
            r3 = r9
        L_0x0195:
            java.lang.String r5 = "APP_SDK"
            java.lang.String r6 = com.waps.SDKUtils.getErrorLog(r3)
            android.util.Log.v(r5, r6)
            r3.printStackTrace()
            r3 = r4
            goto L_0x009d
        L_0x01a4:
            java.lang.String r4 = r10.l
            boolean r4 = com.waps.SDKUtils.isNull(r4)
            if (r4 != 0) goto L_0x009d
            android.content.Intent r3 = new android.content.Intent
            r3.<init>()
            android.content.Context r4 = r10.g
            android.content.Context r5 = r10.g
            java.lang.Class r5 = com.waps.AppConnect.g(r5)
            r3.setClass(r4, r5)
            android.content.Context r4 = r10.g
            java.lang.String r5 = "Notify"
            r6 = 3
            android.content.SharedPreferences r4 = r4.getSharedPreferences(r5, r6)
            android.content.SharedPreferences$Editor r4 = r4.edit()
            java.lang.String r5 = "Notify_Title"
            java.lang.String r6 = r10.j
            r4.putString(r5, r6)
            java.lang.String r5 = "Notify_Content"
            java.lang.String r6 = r10.k
            r4.putString(r5, r6)
            java.lang.String r5 = "Notify_UrlPath"
            java.lang.String r6 = r10.l
            r4.putString(r5, r6)
            java.lang.String r5 = "offers_webview_tag"
            java.lang.String r6 = "OffersWebView"
            r4.putString(r5, r6)
            java.lang.String r5 = "NotifyAd_Tag"
            java.lang.String r6 = "true"
            r4.putString(r5, r6)
            java.lang.String r5 = "Notify_Show_detail"
            r4.putString(r5, r11)
            r4.commit()
            goto L_0x009d
        L_0x01f6:
            android.content.Context r2 = r10.g     // Catch:{ Exception -> 0x0211 }
            android.content.res.Resources r2 = r2.getResources()     // Catch:{ Exception -> 0x0211 }
            java.lang.String r4 = "notify_full_image"
            java.lang.String r5 = "id"
            android.content.Context r6 = r10.g     // Catch:{ Exception -> 0x0211 }
            java.lang.String r6 = r6.getPackageName()     // Catch:{ Exception -> 0x0211 }
            int r2 = r2.getIdentifier(r4, r5, r6)     // Catch:{ Exception -> 0x0211 }
            if (r2 == 0) goto L_0x0220
            r3.setImageViewBitmap(r2, r14)     // Catch:{ Exception -> 0x0211 }
            goto L_0x012f
        L_0x0211:
            r2 = move-exception
            java.lang.String r3 = "APP_SDK"
            java.lang.String r4 = com.waps.SDKUtils.getErrorLog(r2)
            android.util.Log.v(r3, r4)
            r2.printStackTrace()
            goto L_0x0147
        L_0x0220:
            java.lang.String r2 = "APP_SDK"
            java.lang.String r4 = "Please check and update the file which the name is \"push_layout.xml\""
            android.util.Log.w(r2, r4)     // Catch:{ Exception -> 0x0211 }
            goto L_0x012f
        L_0x0229:
            java.lang.String r4 = r10.j     // Catch:{ Exception -> 0x0211 }
            boolean r4 = com.waps.SDKUtils.isNull(r4)     // Catch:{ Exception -> 0x0211 }
            if (r4 != 0) goto L_0x025c
            android.app.Notification r4 = r10.a     // Catch:{ Exception -> 0x0211 }
            android.content.Context r5 = r10.g     // Catch:{ Exception -> 0x0211 }
            java.lang.String r6 = r10.k     // Catch:{ Exception -> 0x0211 }
            r4.setLatestEventInfo(r5, r2, r6, r3)     // Catch:{ Exception -> 0x0211 }
            if (r14 == 0) goto L_0x0133
            android.content.Context r2 = r10.g     // Catch:{ Exception -> 0x0211 }
            android.app.Notification r3 = r10.a     // Catch:{ Exception -> 0x0211 }
            android.widget.RemoteViews r3 = r3.contentView     // Catch:{ Exception -> 0x0211 }
            int r3 = r3.getLayoutId()     // Catch:{ Exception -> 0x0211 }
            r4 = 0
            android.view.View r2 = android.view.View.inflate(r2, r3, r4)     // Catch:{ Exception -> 0x0211 }
            android.widget.ImageView r2 = r10.a(r2)     // Catch:{ Exception -> 0x0211 }
            android.app.Notification r3 = r10.a     // Catch:{ Exception -> 0x0211 }
            android.widget.RemoteViews r3 = r3.contentView     // Catch:{ Exception -> 0x0211 }
            int r2 = r2.getId()     // Catch:{ Exception -> 0x0211 }
            r3.setImageViewBitmap(r2, r14)     // Catch:{ Exception -> 0x0211 }
            goto L_0x0133
        L_0x025c:
            java.lang.String r2 = "APP_SDK"
            java.lang.String r3 = "The notification is not getted,becuse the notifyTitle is null or \"push_layout.xml\" is not found."
            android.util.Log.w(r2, r3)     // Catch:{ Exception -> 0x0211 }
            goto L_0x0133
        L_0x0265:
            r3 = move-exception
            goto L_0x0195
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.x.a(java.lang.String, int, boolean, android.graphics.Bitmap, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):void");
    }

    public void a(String str, String str2, String str3, String str4, String str5, boolean z) {
        try {
            if (this.p == null || this.p.size() == 0) {
                this.p = AppConnect.d(this.g);
            }
            this.i = str;
            this.j = str2;
            this.k = str3;
            this.l = str4;
            this.m = str5;
            String str6 = this.j;
            this.a = new Notification();
            int identifier = this.g.getResources().getIdentifier("icon", "drawable", this.g.getPackageName());
            if (identifier != 0) {
                this.a.icon = identifier;
            } else {
                this.a.icon = 17301618;
            }
            this.a.tickerText = this.k;
            this.a.when = System.currentTimeMillis();
            if (z) {
                this.a.defaults = 1;
            }
            this.a.flags = 16;
            Intent intent = new Intent();
            if (!SDKUtils.isNull(this.l)) {
                if (!this.l.startsWith("http://") && !this.l.startsWith("https://")) {
                    this.l = "http://" + this.l;
                }
                intent = new Intent("android.intent.action.VIEW", Uri.parse(this.l));
                intent.setFlags(268435456);
            }
            PendingIntent activity = PendingIntent.getActivity(this.g, 100, intent, 268435456);
            this.a.contentIntent = activity;
            this.a.setLatestEventInfo(this.g, str6, this.k, activity);
            this.f = (NotificationManager) this.g.getSystemService("notification");
            if (this.n) {
                this.f.notify(3, this.a);
                this.n = false;
            }
            b = true;
            SharedPreferences.Editor edit = this.g.getSharedPreferences("Start_Tag", 3).edit();
            edit.putString("notify_start_tag", "Notify");
            edit.commit();
        } catch (Exception e2) {
            Log.v("APP_SDK", SDKUtils.getErrorLog(e2));
        }
    }
}
