package com.waps;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.Browser;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SDKUtils {
    WebView a;
    /* access modifiers changed from: private */
    public Context b;
    private PackageManager c;
    private ApplicationInfo d;
    private String e = "";
    private PackageInfo f;
    private Handler g;
    /* access modifiers changed from: private */
    public RelativeLayout h;
    /* access modifiers changed from: private */
    public LinearLayout i;

    public SDKUtils(Context context) {
        this.b = context;
    }

    public SDKUtils(Context context, Handler handler, RelativeLayout relativeLayout, LinearLayout linearLayout) {
        this.b = context;
        this.g = handler;
        this.h = relativeLayout;
        this.i = linearLayout;
        this.a = this.a;
    }

    public static String getClassName(Context context, String str) {
        try {
            String metaData = !isNull(str) ? getMetaData(context, str) : "";
            return isNull(metaData) ? "com.waps.OffersWebView" : metaData;
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getErrorLog(Throwable th) {
        try {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
                cause.printStackTrace(printWriter);
            }
            return stringWriter.toString();
        } catch (Exception e2) {
            return "";
        }
    }

    public static String getMetaData(Context context, String str) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (!(applicationInfo == null || applicationInfo.metaData == null)) {
                String string = applicationInfo.metaData.getString(str);
                if (!isNull(string)) {
                    return string;
                }
            }
        } catch (Exception e2) {
        }
        return "";
    }

    protected static Class getOfferClass(Context context, String str) {
        try {
            return Class.forName(getClassName(context, str));
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static boolean isNull(String str) {
        return str == null || "".equals(str.trim());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void addBrowserBookmark(String str, String str2) {
        try {
            ContentResolver contentResolver = this.b.getContentResolver();
            contentResolver.delete(Browser.BOOKMARKS_URI, "title=?", new String[]{str});
            ContentValues contentValues = new ContentValues();
            contentValues.put("bookmark", (Integer) 1);
            contentValues.put("title", str);
            contentValues.put("url", str2);
            contentResolver.insert(Browser.BOOKMARKS_URI, contentValues);
        } catch (Exception e2) {
        }
    }

    public void callTel(String str) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.DIAL");
        intent.setData(Uri.parse("tel:" + str));
        this.b.startActivity(intent);
    }

    public void close() {
        ((Activity) this.b).finish();
    }

    public void closeAd() {
        try {
            this.g.post(new as(this));
        } catch (Exception e2) {
        }
    }

    public void closeOfDialog(String str) {
        submit((String) AppConnect.d(this.b).get("message_title"), str);
    }

    public void closeSubmit(String str) {
        Toast.makeText(this.b, str, 1).show();
        ((Activity) this.b).finish();
    }

    public void createMoreShortcut_forUrl(String str, String str2, String str3, String str4, String str5) {
        if (str4 != null) {
            try {
                if (!str4.equals("")) {
                    if (str5 == null || str5.equals("")) {
                        createShortcut_forUrl(str, str2, str3, str4);
                        return;
                    } else {
                        createShortcut_forUrl(str, str2, str3, str4, str5);
                        return;
                    }
                }
            } catch (Exception e2) {
                return;
            }
        }
        createShortcut_forUrl(str, str2, str3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void createShortcut_forApp(String str) {
        try {
            Map appInfoMap = getAppInfoMap(str);
            if (appInfoMap != null && appInfoMap.size() > 0) {
                int intValue = ((Integer) appInfoMap.get("appIcon")).intValue();
                Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
                intent.putExtra("android.intent.extra.shortcut.NAME", (String) appInfoMap.get("appName"));
                intent.putExtra("duplicate", false);
                intent.putExtra("android.intent.extra.shortcut.INTENT", new Intent("android.intent.action.MAIN").setComponent(new ComponentName(str, (String) appInfoMap.get("activityName"))));
                Context context = null;
                if (this.b.getPackageName().equals(str)) {
                    context = this.b;
                } else {
                    try {
                        context = this.b.createPackageContext(str, 3);
                    } catch (PackageManager.NameNotFoundException e2) {
                    }
                }
                if (context != null) {
                    intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(context, intValue));
                }
                this.b.sendBroadcast(intent);
            }
        } catch (Exception e3) {
        }
    }

    public void createShortcut_forUrl(String str, String str2, String str3) {
        createShortcut_forUrl(str, str2, str3, "");
    }

    public void createShortcut_forUrl(String str, String str2, String str3, String str4) {
        createShortcut_forUrl(str, str2, str3, str4, "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void createShortcut_forUrl(String str, String str2, String str3, String str4, String str5) {
        String loadStringFromLocal = loadStringFromLocal("DesktopI.dat", "/Android/data/cache");
        if (loadStringFromLocal == null || loadStringFromLocal.equals("") || !loadStringFromLocal.contains(str)) {
            Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
            intent.putExtra("android.intent.extra.shortcut.NAME", str);
            intent.putExtra("duplicate", false);
            if (str4 == null || "".equals(str4)) {
                intent.putExtra("android.intent.extra.shortcut.INTENT", new Intent("android.intent.action.VIEW", Uri.parse(str2)));
            } else {
                Intent goToTargetBrowser_Intent = (str5 == null || "".equals(str5.trim())) ? goToTargetBrowser_Intent(str4, str2, this.b.getPackageManager()) : goToTargetBrowser_Intent(str4, str5, str2, this.b.getPackageManager());
                if (goToTargetBrowser_Intent != null) {
                    intent.putExtra("android.intent.extra.shortcut.INTENT", goToTargetBrowser_Intent);
                } else {
                    intent.putExtra("android.intent.extra.shortcut.INTENT", new Intent("android.intent.action.VIEW", Uri.parse(str2)));
                }
            }
            Bitmap bitmap = null;
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(getNetDataToStream(str3));
                bitmap = getDeviceName().toLowerCase().contains("motoa953") ? Bitmap.createScaledBitmap(decodeStream, 48, 48, true) : Bitmap.createScaledBitmap(decodeStream, 80, 80, true);
            } catch (Exception e2) {
            }
            if (bitmap != null) {
                intent.putExtra("android.intent.extra.shortcut.ICON", bitmap);
            } else {
                intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this.b, 17301516));
            }
            this.b.sendBroadcast(intent);
            saveDataToLocal(loadStringFromLocal + str + ";", "DesktopI.dat", "/Android/data/cache");
        }
    }

    public void deleteLocalFiles(File file) {
        try {
            if (!file.exists()) {
                return;
            }
            if (file.isFile()) {
                file.delete();
            } else if (file.isDirectory()) {
                for (File deleteLocalFiles : file.listFiles()) {
                    deleteLocalFiles(deleteLocalFiles);
                }
            }
        } catch (Exception e2) {
        }
    }

    public void full_screen() {
        this.g.post(new au(this));
    }

    public String[] getAllPermissions() {
        try {
            return this.b.getPackageManager().getPackageInfo(this.b.getPackageName(), 4096).requestedPermissions;
        } catch (Exception e2) {
            return null;
        }
    }

    public Map getAppInfoMap(String str) {
        try {
            HashMap hashMap = new HashMap();
            PackageManager packageManager = this.b.getPackageManager();
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 1);
            for (int i2 = 0; i2 < queryIntentActivities.size(); i2++) {
                ResolveInfo resolveInfo = queryIntentActivities.get(i2);
                if (resolveInfo.activityInfo.packageName.equals(str)) {
                    String obj = resolveInfo.loadLabel(packageManager).toString();
                    int i3 = resolveInfo.activityInfo.applicationInfo.icon;
                    String str2 = resolveInfo.activityInfo.name;
                    if (str2 != null && !"".equals(str2.trim())) {
                        hashMap.put("appName", obj);
                        hashMap.put("appIcon", Integer.valueOf(i3));
                        hashMap.put("activityName", str2);
                        return hashMap;
                    }
                }
            }
        } catch (Exception e2) {
        }
        return null;
    }

    public String getAppVersion(String str) {
        try {
            Context createPackageContext = this.b.createPackageContext(str, 3);
            this.c = createPackageContext.getPackageManager();
            this.f = this.c.getPackageInfo(createPackageContext.getPackageName(), 0);
            if (this.f != null) {
                String str2 = this.f.versionName;
                if (str2 != null && !"".equals(str2.trim())) {
                    return str2;
                }
                Log.i("WAPS_SDK", "The app is not exist.");
            }
        } catch (PackageManager.NameNotFoundException e2) {
        }
        return "";
    }

    public String getBrowserPackageName(String str) {
        try {
            String installed = getInstalled();
            if (str == null || "".equals(str.trim())) {
                return "";
            }
            if (str.indexOf(";") >= 0) {
                String[] split = str.split(";");
                for (int i2 = 0; i2 < split.length; i2++) {
                    if (installed.contains(split[i2])) {
                        return split[i2];
                    }
                }
            } else if (installed.contains(str)) {
                return str;
            }
            return "";
        } catch (Exception e2) {
            return "";
        }
    }

    public String getCountryCode() {
        return Locale.getDefault().getCountry();
    }

    public String getDeviceName() {
        return Build.MODEL;
    }

    public int getDeviceOSVersion() {
        return Integer.parseInt(Build.VERSION.SDK);
    }

    public HttpResponse getHttpResponse(String str) {
        HttpHost httpHost;
        String replaceFirst;
        if (!isCmwap()) {
            try {
                HttpGet httpGet = new HttpGet(str.replaceAll(" ", "%20"));
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams, 150000);
                HttpConnectionParams.setSoTimeout(basicHttpParams, 300000);
                return new DefaultHttpClient(basicHttpParams).execute(httpGet);
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        } else {
            try {
                HttpHost httpHost2 = new HttpHost("10.0.0.172", 80, "http");
                if (str.startsWith("http://app")) {
                    httpHost = new HttpHost("app.wapx.cn", 80, "http");
                    replaceFirst = str.replaceAll(" ", "%20").replaceFirst("http://app.wapx.cn", "");
                } else {
                    httpHost = new HttpHost("ads.wapx.cn", 80, "http");
                    replaceFirst = str.replaceAll(" ", "%20").replaceFirst("http://ads.wapx.cn", "");
                }
                HttpGet httpGet2 = new HttpGet(replaceFirst);
                BasicHttpParams basicHttpParams2 = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams2, 15000);
                HttpConnectionParams.setSoTimeout(basicHttpParams2, 30000);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams2);
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost2);
                return defaultHttpClient.execute(httpHost, httpGet2);
            } catch (Exception e3) {
                return null;
            }
        }
    }

    public String getImsi() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.b.getSystemService("phone");
            return telephonyManager != null ? telephonyManager.getSubscriberId() : "";
        } catch (Exception e2) {
            return "";
        }
    }

    public String getInstalled() {
        try {
            this.c = this.b.getPackageManager();
            List<PackageInfo> installedPackages = this.c.getInstalledPackages(0);
            String str = "";
            int i2 = 0;
            while (i2 < installedPackages.size()) {
                try {
                    PackageInfo packageInfo = installedPackages.get(i2);
                    int i3 = packageInfo.applicationInfo.flags;
                    ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                    if ((i3 & 1) <= 0) {
                        str = str + packageInfo.packageName + ";";
                    }
                    i2++;
                } catch (Exception e2) {
                    return str;
                }
            }
            return str;
        } catch (Exception e3) {
            return "";
        }
    }

    public String getLanguageCode() {
        return Locale.getDefault().getLanguage();
    }

    public List getList(String str) {
        ArrayList arrayList = new ArrayList();
        if (str == null || "".equals(str) || str.indexOf("[;]") < 0) {
            arrayList.add(str);
        } else {
            String[] split = str.split("\\[;\\]");
            for (String add : split) {
                arrayList.add(add);
            }
        }
        return arrayList;
    }

    public String getLocation() {
        try {
            String locationByGPS = getLocationByGPS();
            if (locationByGPS != null) {
                try {
                    if (!locationByGPS.equals("") && !locationByGPS.equals("0.0;0.0")) {
                        return locationByGPS;
                    }
                } catch (Exception e2) {
                    return locationByGPS;
                }
            }
            return getLocationByNetwork();
        } catch (Exception e3) {
            return "";
        }
    }

    public String getLocationByGPS() {
        Location lastKnownLocation;
        try {
            if (hasThePermission("ACCESS_FINE_LOCATION")) {
                LocationManager locationManager = (LocationManager) this.b.getSystemService("location");
                if (locationManager.isProviderEnabled("gps") && (lastKnownLocation = locationManager.getLastKnownLocation("gps")) != null) {
                    return lastKnownLocation.getLongitude() + ";" + lastKnownLocation.getLatitude();
                }
            }
        } catch (Exception e2) {
        }
        return "";
    }

    public String getLocationByNetwork() {
        try {
            LocationManager locationManager = (LocationManager) this.b.getSystemService("location");
            locationManager.requestLocationUpdates("network", 1000, 0.0f, new av(this));
            Location lastKnownLocation = locationManager.getLastKnownLocation("network");
            if (lastKnownLocation != null) {
                double longitude = lastKnownLocation.getLongitude();
                return longitude + ";" + lastKnownLocation.getLatitude();
            }
        } catch (Exception e2) {
        }
        return "";
    }

    public String getMac_Address() {
        try {
            if (hasThePermission("ACCESS_WIFI_STATE")) {
                WifiInfo connectionInfo = ((WifiManager) this.b.getSystemService("wifi")).getConnectionInfo();
                if (connectionInfo != null) {
                    String macAddress = connectionInfo.getMacAddress();
                    if (macAddress != null && !"".equals(macAddress.trim())) {
                        return macAddress;
                    }
                    Log.i("WAPS_SDK", "The mac address is not found!");
                } else {
                    Log.i("WAPS_SDK", "Pleass check the Network connection!");
                }
            } else {
                Log.i("WAPS_SDK", "Permission.ACCESS_WIFI_STATE is not found or the device is Emulator, Please check it!");
            }
        } catch (Exception e2) {
        }
        return "";
    }

    public InputStream getNetDataToStream(String str) {
        HttpResponse execute;
        try {
            String substring = str.substring(str.indexOf("http://") + 7, str.indexOf("/", str.indexOf("http://") + 8));
            String substring2 = str.substring(0, str.indexOf("/", str.indexOf("http://") + 8));
            if (!isCmwap()) {
                execute = new DefaultHttpClient().execute(new HttpGet(str.replaceAll(" ", "%20")));
            } else {
                HttpHost httpHost = new HttpHost("10.0.0.172", 80, "http");
                HttpHost httpHost2 = new HttpHost(substring, 80, "http");
                HttpGet httpGet = new HttpGet(str.replaceAll(" ", "%20").replaceFirst(substring2, ""));
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
                HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost);
                execute = defaultHttpClient.execute(httpHost2, httpGet);
            }
            return execute.getEntity().getContent();
        } catch (Exception e2) {
            return null;
        }
    }

    public String getNodeTrimValues(NodeList nodeList) {
        String str = "";
        for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
            Element element = (Element) nodeList.item(i2);
            if (element != null) {
                NodeList childNodes = element.getChildNodes();
                if (childNodes.getLength() > 0) {
                    String str2 = str;
                    for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                        Node item = childNodes.item(i3);
                        if (item != null) {
                            str2 = str2 + item.getNodeValue() + "[;]";
                        }
                    }
                    str = str2;
                } else {
                    str = str + "a[;]";
                }
            }
        }
        if (str == null || str.equals("")) {
            return null;
        }
        return str.substring(0, str.length() - 3).trim();
    }

    public String getParams() {
        return AppConnect.getInstance(this.b).c(this.b);
    }

    public void getPushAd() {
        AppConnect.getInstance(this.b).getPushAd();
    }

    public String getResponseResult(HttpResponse httpResponse) {
        try {
            return EntityUtils.toString(httpResponse.getEntity());
        } catch (Exception e2) {
            return "";
        }
    }

    public String getRunningAppPackageNames() {
        try {
            String str = "";
            for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) this.b.getSystemService("activity")).getRunningAppProcesses()) {
                str = getInstalled().contains(next.processName) ? str + next.processName + ";" : str;
            }
            if (str != null && !"".equals(str.trim()) && str.endsWith(";")) {
                return str.substring(0, str.length() - 1);
            }
        } catch (Exception e2) {
        }
        return "";
    }

    public String getSDKVersion() {
        return AppConnect.LIBRARY_VERSION_NUMBER;
    }

    public String getScreenStatus() {
        try {
            return this.b.getResources().getConfiguration().orientation == 1 ? "true" : this.b.getResources().getConfiguration().orientation == 2 ? "false" : "";
        } catch (Exception e2) {
        }
    }

    public String getUdid() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.b.getSystemService("phone");
            return telephonyManager != null ? telephonyManager.getDeviceId() : "";
        } catch (Exception e2) {
            return "";
        }
    }

    public String getWAPS_ID() {
        try {
            AppConnect.getInstance(this.b);
            String a2 = AppConnect.a();
            if (a2 != null && !"".equals(a2.trim())) {
                return a2;
            }
            this.c = this.b.getPackageManager();
            this.d = this.c.getApplicationInfo(this.b.getPackageName(), 128);
            if (!(this.d == null || this.d.metaData == null)) {
                String string = this.d.metaData.getString("WAPS_ID");
                if (string == null || "".equals(string.trim())) {
                    string = this.d.metaData.getString("APP_ID");
                }
                if (string == null || "".equals(string.trim())) {
                    return "";
                }
                this.e = string.toString();
                if (this.e != null && !this.e.equals("")) {
                    return this.e;
                }
            }
            return "";
        } catch (Exception e2) {
        }
    }

    public String getWAPS_PID() {
        Object obj;
        this.c = this.b.getPackageManager();
        try {
            this.d = this.c.getApplicationInfo(this.b.getPackageName(), 128);
            if (!(this.d == null || this.d.metaData == null || (obj = this.d.metaData.get("WAPS_PID")) == null)) {
                this.e = obj.toString();
                if (this.e != null && !this.e.equals("")) {
                    return this.e;
                }
            }
            return "";
        } catch (Exception e2) {
            return "";
        }
    }

    public void goToTargetBrowser(String str, String str2, PackageManager packageManager) {
        try {
            this.b.startActivity(goToTargetBrowser_Intent(str, str2, packageManager));
        } catch (Exception e2) {
        }
    }

    public Intent goToTargetBrowser_Intent(String str, String str2, PackageManager packageManager) {
        try {
            Intent goToTargetBrowser_Intent = goToTargetBrowser_Intent(str, "", str2, packageManager);
            if (goToTargetBrowser_Intent != null) {
                return goToTargetBrowser_Intent;
            }
            return null;
        } catch (Exception e2) {
        }
    }

    public Intent goToTargetBrowser_Intent(String str, String str2, String str3, PackageManager packageManager) {
        try {
            if (getInstalled().contains(str)) {
                Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
                if (str2 != null && !"".equals(str2.trim())) {
                    launchIntentForPackage = new Intent();
                    launchIntentForPackage.setClassName(str, str2);
                }
                launchIntentForPackage.setAction("android.intent.action.VIEW");
                launchIntentForPackage.addCategory("android.intent.category.DEFAULT");
                launchIntentForPackage.setData(Uri.parse(str3));
                return launchIntentForPackage;
            }
            new Intent("android.intent.action.VIEW", Uri.parse(str3)).setFlags(268435456);
            return null;
        } catch (Exception e2) {
        }
    }

    public boolean hasThePermission(String str) {
        try {
            String[] allPermissions = getAllPermissions();
            if (allPermissions != null && allPermissions.length > 0) {
                for (String str2 : allPermissions) {
                    if (!isNull(str) && str2.contains(str)) {
                        return true;
                    }
                }
            }
        } catch (Exception e2) {
        }
        return false;
    }

    public int initAdWidth() {
        try {
            if (this.b.getResources().getConfiguration().orientation == 1) {
                return ((Activity) this.b).getWindowManager().getDefaultDisplay().getWidth();
            }
            if (this.b.getResources().getConfiguration().orientation == 2) {
                return ((Activity) this.b).getWindowManager().getDefaultDisplay().getHeight();
            }
            return 0;
        } catch (Exception e2) {
        }
    }

    public boolean isClient() {
        try {
            return ((ConnectivityManager) this.b.getSystemService("connectivity")).getActiveNetworkInfo() != null;
        } catch (Exception e2) {
        }
    }

    public boolean isCmwap() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.b.getSystemService("connectivity")).getActiveNetworkInfo();
        return (activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().toLowerCase().contains("wap")) ? false : true;
    }

    public String isInstalled(String str) {
        try {
            return this.b.getPackageManager().getLaunchIntentForPackage(str) != null ? "true" : "false";
        } catch (Exception e2) {
            return "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isShortcutInstalled(java.lang.String r11) {
        /*
            r10 = this;
            r8 = 0
            r7 = 1
            r6 = 0
            android.content.Context r0 = r10.b     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            java.lang.String r1 = ""
            int r1 = r10.getDeviceOSVersion()     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            r2 = 8
            if (r1 >= r2) goto L_0x003e
            java.lang.String r1 = "content://com.android.launcher.settings/favorites?notify=true"
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x0045, all -> 0x004e }
        L_0x0019:
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            r3 = 0
            java.lang.String r4 = "title"
            r2[r3] = r4     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            java.lang.String r3 = "title=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            r5 = 0
            r4[r5] = r11     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            if (r0 == 0) goto L_0x0061
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x005b, all -> 0x0056 }
            if (r1 == 0) goto L_0x0061
            r1 = r7
        L_0x0037:
            if (r0 == 0) goto L_0x005f
            r0.close()
            r0 = r1
        L_0x003d:
            return r0
        L_0x003e:
            java.lang.String r1 = "content://com.android.launcher2.settings/favorites?notify=true"
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Exception -> 0x0045, all -> 0x004e }
            goto L_0x0019
        L_0x0045:
            r0 = move-exception
            r0 = r8
        L_0x0047:
            if (r0 == 0) goto L_0x005d
            r0.close()
            r0 = r6
            goto L_0x003d
        L_0x004e:
            r0 = move-exception
            r1 = r8
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()
        L_0x0055:
            throw r0
        L_0x0056:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0050
        L_0x005b:
            r1 = move-exception
            goto L_0x0047
        L_0x005d:
            r0 = r6
            goto L_0x003d
        L_0x005f:
            r0 = r1
            goto L_0x003d
        L_0x0061:
            r1 = r6
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.SDKUtils.isShortcutInstalled(java.lang.String):boolean");
    }

    public boolean isTimeLimited(String str, String str2) {
        String str3;
        String str4;
        try {
            Date date = new Date(System.currentTimeMillis());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("HH:mm:ss");
            try {
                str3 = simpleDateFormat2.format(simpleDateFormat3.parse(str)).equals("1970-01-01") ? simpleDateFormat2.format(date) + " " + str : str;
            } catch (Exception e2) {
                str3 = str;
            }
            try {
                str4 = simpleDateFormat2.format(simpleDateFormat3.parse(str)).equals("1970-01-01") ? simpleDateFormat2.format(date) + " " + str2 : str2;
            } catch (Exception e3) {
                str4 = str2;
            }
            return date.after(simpleDateFormat.parse(str3)) && date.before(simpleDateFormat.parse(str4));
        } catch (Exception e4) {
        }
    }

    public void load(String str) {
        if (str != null) {
            try {
                if (!"".equals(str)) {
                    this.c = this.b.getPackageManager();
                    Intent launchIntentForPackage = this.c.getLaunchIntentForPackage(str);
                    if (launchIntentForPackage != null) {
                        this.b.startActivity(launchIntentForPackage);
                    } else {
                        Log.i("WAPS_SDK", "The app is not exist.");
                    }
                }
            } catch (Exception e2) {
            }
        }
    }

    public InputStream loadStreamFromLocal(String str, String str2) {
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file = new File((Environment.getExternalStorageDirectory().toString() + str2) + "/" + str);
                if (file.exists() && file.length() > 0) {
                    return new FileInputStream(file);
                }
            }
            File fileStreamPath = this.b.getFileStreamPath(str);
            if (fileStreamPath.exists() && fileStreamPath.length() > 0) {
                return new FileInputStream(fileStreamPath);
            }
        } catch (Exception e2) {
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0101 A[SYNTHETIC, Splitter:B:56:0x0101] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0106 A[Catch:{ IOException -> 0x010a }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0111 A[SYNTHETIC, Splitter:B:64:0x0111] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0116 A[Catch:{ IOException -> 0x011a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String loadStringFromLocal(java.lang.String r10, java.lang.String r11) {
        /*
            r9 = this;
            r6 = 0
            r4 = 0
            java.lang.String r0 = "\n"
            java.lang.String r5 = ""
            java.lang.String r0 = ""
            java.lang.String r0 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            if (r0 == 0) goto L_0x014c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            r0.<init>()     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.lang.StringBuilder r0 = r0.append(r11)     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            r2.<init>()     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.lang.String r2 = "/"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.lang.StringBuilder r0 = r0.append(r10)     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            boolean r0 = r1.exists()     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            if (r0 == 0) goto L_0x014c
            long r2 = r1.length()     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            int r0 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x014c
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00fc, all -> 0x010c }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0135, all -> 0x011c }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0135, all -> 0x011c }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0135, all -> 0x011c }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0135, all -> 0x011c }
            java.lang.String r2 = ""
            if (r1 == 0) goto L_0x0096
            r2 = r5
        L_0x006c:
            java.lang.String r3 = r1.readLine()     // Catch:{ Exception -> 0x0139, all -> 0x0121 }
            if (r3 == 0) goto L_0x008a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0139, all -> 0x0121 }
            r4.<init>()     // Catch:{ Exception -> 0x0139, all -> 0x0121 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Exception -> 0x0139, all -> 0x0121 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0139, all -> 0x0121 }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0139, all -> 0x0121 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0139, all -> 0x0121 }
            goto L_0x006c
        L_0x008a:
            if (r0 == 0) goto L_0x008f
            r0.close()     // Catch:{ IOException -> 0x0149 }
        L_0x008f:
            if (r1 == 0) goto L_0x0094
            r1.close()     // Catch:{ IOException -> 0x0149 }
        L_0x0094:
            r0 = r2
        L_0x0095:
            return r0
        L_0x0096:
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x0099:
            android.content.Context r2 = r9.b     // Catch:{ Exception -> 0x013e, all -> 0x0126 }
            java.io.File r2 = r2.getFileStreamPath(r10)     // Catch:{ Exception -> 0x013e, all -> 0x0126 }
            boolean r3 = r2.exists()     // Catch:{ Exception -> 0x013e, all -> 0x0126 }
            if (r3 == 0) goto L_0x00ee
            long r3 = r2.length()     // Catch:{ Exception -> 0x013e, all -> 0x0126 }
            int r3 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r3 <= 0) goto L_0x00ee
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x013e, all -> 0x0126 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x013e, all -> 0x0126 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0140, all -> 0x012c }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0140, all -> 0x012c }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0140, all -> 0x012c }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0140, all -> 0x012c }
            java.lang.String r0 = ""
            if (r1 == 0) goto L_0x00ec
            r0 = r5
        L_0x00c1:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0143, all -> 0x0132 }
            if (r2 == 0) goto L_0x00df
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0143, all -> 0x0132 }
            r4.<init>()     // Catch:{ Exception -> 0x0143, all -> 0x0132 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x0143, all -> 0x0132 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0143, all -> 0x0132 }
            java.lang.String r2 = "\n"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0143, all -> 0x0132 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0143, all -> 0x0132 }
            goto L_0x00c1
        L_0x00df:
            if (r3 == 0) goto L_0x00e4
            r3.close()     // Catch:{ IOException -> 0x00ea }
        L_0x00e4:
            if (r1 == 0) goto L_0x0095
            r1.close()     // Catch:{ IOException -> 0x00ea }
            goto L_0x0095
        L_0x00ea:
            r1 = move-exception
            goto L_0x0095
        L_0x00ec:
            r0 = r1
            r1 = r3
        L_0x00ee:
            if (r1 == 0) goto L_0x00f3
            r1.close()     // Catch:{ IOException -> 0x0147 }
        L_0x00f3:
            if (r0 == 0) goto L_0x00f8
            r0.close()     // Catch:{ IOException -> 0x0147 }
        L_0x00f8:
            java.lang.String r0 = ""
            r0 = r5
            goto L_0x0095
        L_0x00fc:
            r0 = move-exception
            r0 = r4
            r1 = r4
        L_0x00ff:
            if (r1 == 0) goto L_0x0104
            r1.close()     // Catch:{ IOException -> 0x010a }
        L_0x0104:
            if (r0 == 0) goto L_0x00f8
            r0.close()     // Catch:{ IOException -> 0x010a }
            goto L_0x00f8
        L_0x010a:
            r0 = move-exception
            goto L_0x00f8
        L_0x010c:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x010f:
            if (r2 == 0) goto L_0x0114
            r2.close()     // Catch:{ IOException -> 0x011a }
        L_0x0114:
            if (r1 == 0) goto L_0x0119
            r1.close()     // Catch:{ IOException -> 0x011a }
        L_0x0119:
            throw r0
        L_0x011a:
            r1 = move-exception
            goto L_0x0119
        L_0x011c:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r4
            goto L_0x010f
        L_0x0121:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x010f
        L_0x0126:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r0
            r0 = r8
            goto L_0x010f
        L_0x012c:
            r1 = move-exception
            r2 = r3
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x010f
        L_0x0132:
            r0 = move-exception
            r2 = r3
            goto L_0x010f
        L_0x0135:
            r1 = move-exception
            r1 = r0
            r0 = r4
            goto L_0x00ff
        L_0x0139:
            r2 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00ff
        L_0x013e:
            r2 = move-exception
            goto L_0x00ff
        L_0x0140:
            r1 = move-exception
            r1 = r3
            goto L_0x00ff
        L_0x0143:
            r0 = move-exception
            r0 = r1
            r1 = r3
            goto L_0x00ff
        L_0x0147:
            r0 = move-exception
            goto L_0x00f8
        L_0x0149:
            r0 = move-exception
            goto L_0x0094
        L_0x014c:
            r0 = r4
            r1 = r4
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.SDKUtils.loadStringFromLocal(java.lang.String, java.lang.String):java.lang.String");
    }

    public void openAd() {
        openAd("");
    }

    public void openAd(String str) {
        try {
            this.g.post(new at(this, str));
        } catch (Exception e2) {
        }
    }

    public boolean openUrl(WebView webView, String str) {
        Intent intent;
        try {
            if (str.startsWith("http://")) {
                webView.loadUrl(str);
                return true;
            }
            String substring = str.substring(0, str.indexOf(";http://"));
            String substring2 = str.substring(str.indexOf("http://"));
            if (!isNull(substring)) {
                if (substring.startsWith("load://")) {
                    String substring3 = substring.substring("load://".length());
                    if (substring3 != "") {
                        try {
                            intent = this.b.getPackageManager().getLaunchIntentForPackage(substring3);
                        } catch (Exception e2) {
                            intent = null;
                        }
                        if (intent != null) {
                            this.b.startActivity(intent);
                            AppConnect.getInstanceNoConnect(this.b).a(substring3, 1);
                            return true;
                        }
                        webView.loadUrl(substring2);
                    }
                } else {
                    webView.loadUrl(substring);
                    return true;
                }
            }
            return false;
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public void openUrlByBrowser(String str, String str2) {
        try {
            String browserPackageName = getBrowserPackageName(str);
            if (browserPackageName == null || "".equals(browserPackageName.trim())) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
                intent.setFlags(268435456);
                this.b.startActivity(intent);
                return;
            }
            goToTargetBrowser(browserPackageName, str2, this.b.getPackageManager());
        } catch (Exception e2) {
        }
    }

    public Intent openUrlByBrowser_Intent(String str, String str2) {
        try {
            String browserPackageName = getBrowserPackageName(str);
            if (browserPackageName != null && !"".equals(browserPackageName.trim())) {
                return goToTargetBrowser_Intent(browserPackageName, str2, this.b.getPackageManager());
            }
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
            intent.setFlags(268435456);
            return intent;
        } catch (Exception e2) {
            return null;
        }
    }

    public String replaceData(String str) {
        return (str.equals("") || !str.equals("a")) ? str : str.replace("a", "");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0095, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0096, code lost:
        r7 = r1;
        r1 = null;
        r0 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0070 A[SYNTHETIC, Splitter:B:19:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x008a A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0004] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x009b A[SYNTHETIC, Splitter:B:38:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void saveDataToLocal(java.io.InputStream r9, java.lang.String r10, java.lang.String r11) {
        /*
            r8 = this;
            r6 = -1
            r0 = 0
            r1 = 10240(0x2800, float:1.4349E-41)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.String r2 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.String r3 = "mounted"
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            if (r2 == 0) goto L_0x0074
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r2.<init>()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r5.<init>()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r4.<init>(r2, r10)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            boolean r2 = r3.exists()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            if (r2 != 0) goto L_0x0051
            r3.mkdirs()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
        L_0x0051:
            boolean r2 = r4.exists()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            if (r2 != 0) goto L_0x005a
            r4.createNewFile()     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
        L_0x005a:
            if (r4 == 0) goto L_0x008d
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
        L_0x0061:
            int r0 = r9.read(r1)     // Catch:{ Exception -> 0x006c, all -> 0x00a3 }
            if (r0 == r6) goto L_0x008c
            r3 = 0
            r2.write(r1, r3, r0)     // Catch:{ Exception -> 0x006c, all -> 0x00a3 }
            goto L_0x0061
        L_0x006c:
            r0 = move-exception
            r0 = r2
        L_0x006e:
            if (r0 == 0) goto L_0x0073
            r0.close()     // Catch:{ IOException -> 0x009f }
        L_0x0073:
            return
        L_0x0074:
            android.content.Context r1 = r8.b     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r2 = 3
            java.io.FileOutputStream r0 = r1.openFileOutput(r10, r2)     // Catch:{ Exception -> 0x008a, all -> 0x0095 }
            r1 = 10240(0x2800, float:1.4349E-41)
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x008a, all -> 0x00a6 }
        L_0x007f:
            int r2 = r9.read(r1)     // Catch:{ Exception -> 0x008a, all -> 0x00a6 }
            if (r2 == r6) goto L_0x008d
            r3 = 0
            r0.write(r1, r3, r2)     // Catch:{ Exception -> 0x008a, all -> 0x00a6 }
            goto L_0x007f
        L_0x008a:
            r1 = move-exception
            goto L_0x006e
        L_0x008c:
            r0 = r2
        L_0x008d:
            if (r0 == 0) goto L_0x0073
            r0.close()     // Catch:{ IOException -> 0x0093 }
            goto L_0x0073
        L_0x0093:
            r0 = move-exception
            goto L_0x0073
        L_0x0095:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0099:
            if (r1 == 0) goto L_0x009e
            r1.close()     // Catch:{ IOException -> 0x00a1 }
        L_0x009e:
            throw r0
        L_0x009f:
            r0 = move-exception
            goto L_0x0073
        L_0x00a1:
            r1 = move-exception
            goto L_0x009e
        L_0x00a3:
            r0 = move-exception
            r1 = r2
            goto L_0x0099
        L_0x00a6:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.SDKUtils.saveDataToLocal(java.io.InputStream, java.lang.String, java.lang.String):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0083, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0084, code lost:
        r6 = r1;
        r1 = null;
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0094, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0095, code lost:
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a A[ExcHandler: Exception (e java.lang.Exception), PHI: r0 
      PHI: (r0v5 java.io.FileOutputStream) = (r0v0 java.io.FileOutputStream), (r0v6 java.io.FileOutputStream) binds: [B:1:0x0003, B:15:0x006a] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x0003] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0089 A[SYNTHETIC, Splitter:B:30:0x0089] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void saveDataToLocal(java.lang.String r8, java.lang.String r9, java.lang.String r10) {
        /*
            r7 = this;
            r0 = 0
            java.lang.String r1 = "UTF-8"
            byte[] r1 = r8.getBytes(r1)     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.lang.String r2 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.lang.String r3 = "mounted"
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            if (r2 == 0) goto L_0x006a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            r2.<init>()     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            r5.<init>()     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.lang.String r5 = "/"
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            boolean r2 = r3.exists()     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            if (r2 != 0) goto L_0x0056
            r3.mkdirs()     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
        L_0x0056:
            boolean r2 = r4.exists()     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            if (r2 != 0) goto L_0x005f
            r4.createNewFile()     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
        L_0x005f:
            if (r4 == 0) goto L_0x006a
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x007a, all -> 0x0083 }
            r2.write(r1)     // Catch:{ Exception -> 0x009e, all -> 0x0091 }
            r0 = r2
        L_0x006a:
            android.content.Context r2 = r7.b     // Catch:{ Exception -> 0x007a, all -> 0x0094 }
            r3 = 3
            java.io.FileOutputStream r0 = r2.openFileOutput(r9, r3)     // Catch:{ Exception -> 0x007a, all -> 0x0094 }
            r0.write(r1)     // Catch:{ Exception -> 0x007a, all -> 0x0099 }
            if (r0 == 0) goto L_0x0079
            r0.close()     // Catch:{ IOException -> 0x008d }
        L_0x0079:
            return
        L_0x007a:
            r1 = move-exception
        L_0x007b:
            if (r0 == 0) goto L_0x0079
            r0.close()     // Catch:{ IOException -> 0x0081 }
            goto L_0x0079
        L_0x0081:
            r0 = move-exception
            goto L_0x0079
        L_0x0083:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0087:
            if (r1 == 0) goto L_0x008c
            r1.close()     // Catch:{ IOException -> 0x008f }
        L_0x008c:
            throw r0
        L_0x008d:
            r0 = move-exception
            goto L_0x0079
        L_0x008f:
            r1 = move-exception
            goto L_0x008c
        L_0x0091:
            r0 = move-exception
            r1 = r2
            goto L_0x0087
        L_0x0094:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0087
        L_0x0099:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0087
        L_0x009e:
            r0 = move-exception
            r0 = r2
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.SDKUtils.saveDataToLocal(java.lang.String, java.lang.String, java.lang.String):void");
    }

    public void sendSMS(String str, String str2) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SENDTO");
        intent.setData(Uri.parse("smsto:" + str));
        intent.putExtra("sms_body", str2);
        this.b.startActivity(intent);
    }

    public void showToast(String str) {
        Toast.makeText(this.b, str, 1).show();
    }

    public String[] splitString(String str, String str2, String str3) {
        if (str2 != null) {
            try {
                if (!"".equals(str2.trim())) {
                    String str4 = (str3 == null || str3.equals("")) ? str2 : str3;
                    if (str == null || "".equals(str.trim())) {
                        return null;
                    }
                    String substring = str.endsWith(str2) ? str.substring(0, str.lastIndexOf(str2)) : str;
                    if (substring.indexOf(str2) > 0) {
                        return substring.split(str4);
                    }
                    if (substring.indexOf(str2) == 0) {
                        return new String[]{substring.substring(1)};
                    }
                    return new String[]{substring};
                }
            } catch (Exception e2) {
                return null;
            }
        }
        return new String[]{str};
    }

    public void submit(String str, String str2) {
        if (str2 == null || "".equals(str2)) {
            ((Activity) this.b).finish();
        } else {
            new AlertDialog.Builder(this.b).setTitle(str).setMessage(str2).setPositiveButton("确定", new ar(this)).create().show();
        }
    }
}
