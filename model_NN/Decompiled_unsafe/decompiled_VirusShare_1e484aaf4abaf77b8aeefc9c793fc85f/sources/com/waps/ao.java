package com.waps;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import java.io.File;

class ao implements DialogInterface.OnClickListener {
    final /* synthetic */ OffersWebView a;

    ao(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        w.e = true;
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(this.a.c + this.a.b)), "application/vnd.android.package-archive");
        this.a.startActivity(intent);
        this.a.d = new y(this.a.b);
        this.a.a.a(this.a.d);
        if (this.a.r != null && "true".equals(this.a.r)) {
            this.a.finish();
        }
    }
}
