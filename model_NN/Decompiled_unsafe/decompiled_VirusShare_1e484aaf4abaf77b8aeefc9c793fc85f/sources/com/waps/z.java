package com.waps;

import android.content.Context;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;

public class z {
    private boolean a = false;
    private SDKUtils b;

    public z() {
    }

    public z(Context context) {
        if (this.b == null) {
            this.b = new SDKUtils(context);
        }
    }

    private String b() {
        try {
            String udid = this.b.getUdid();
            try {
                if (SDKUtils.isNull(udid)) {
                    udid = "mac" + this.b.getMac_Address().replaceAll(":", "");
                    if (SDKUtils.isNull(udid)) {
                        udid = "EMULATOR";
                    }
                }
                return AppConnect.a((udid.toLowerCase() + "kingxiaoguang@gmail.com").getBytes()).toLowerCase();
            } catch (Exception e) {
                return udid;
            }
        } catch (Exception e2) {
            return "";
        }
    }

    public String a(String str, String str2) {
        String replaceFirst;
        HttpHost httpHost;
        String b2 = b();
        if (!a()) {
            try {
                String str3 = str + str2 + "&" + "at" + "=" + System.currentTimeMillis();
                if (str3.contains("status")) {
                }
                HttpGet httpGet = new HttpGet(str3.replaceAll(" ", "%20"));
                httpGet.setHeader("Udid", b2);
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams, 150000);
                HttpConnectionParams.setSoTimeout(basicHttpParams, 300000);
                return EntityUtils.toString(new DefaultHttpClient(basicHttpParams).execute(httpGet).getEntity());
            } catch (Exception e) {
                return null;
            }
        } else {
            try {
                String str4 = str + str2;
                HttpHost httpHost2 = new HttpHost("10.0.0.172", 80, "http");
                if (str4.startsWith("http://app")) {
                    HttpHost httpHost3 = new HttpHost("app.wapx.cn", 80, "http");
                    replaceFirst = str4.replaceAll(" ", "%20").replaceFirst("http://app.wapx.cn", "");
                    httpHost = httpHost3;
                } else {
                    HttpHost httpHost4 = new HttpHost("ads.wapx.cn", 80, "http");
                    replaceFirst = str4.replaceAll(" ", "%20").replaceFirst("http://ads.wapx.cn", "");
                    httpHost = httpHost4;
                }
                HttpGet httpGet2 = new HttpGet(replaceFirst);
                httpGet2.setHeader("Udid", b2);
                BasicHttpParams basicHttpParams2 = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams2, 15000);
                HttpConnectionParams.setSoTimeout(basicHttpParams2, 30000);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams2);
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost2);
                return EntityUtils.toString(defaultHttpClient.execute(httpHost, httpGet2).getEntity());
            } catch (Exception e2) {
                return null;
            }
        }
    }

    public String a(byte[] bArr, String str) {
        HttpHost httpHost;
        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(bArr);
        HttpPost httpPost = new HttpPost(str);
        httpPost.setEntity(byteArrayEntity);
        httpPost.setHeader("Udid", b());
        httpPost.setHeader("Accept-Encoding", "gzip");
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        if (this.a) {
            HttpHost httpHost2 = new HttpHost("10.0.0.172", 80, "http");
            if (str.startsWith("http://app")) {
                httpHost = new HttpHost("app.wapx.cn", 80, "http");
                str.replaceAll(" ", "%20").replaceFirst("http://app.wapx.cn", "");
            } else {
                httpHost = new HttpHost("ads.wapx.cn", 80, "http");
                str.replaceAll(" ", "%20").replaceFirst("http://ads.wapx.cn", "");
            }
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost2);
            try {
                defaultHttpClient.execute(httpHost, httpPost).getStatusLine().getStatusCode();
                return EntityUtils.toString(defaultHttpClient.execute(httpHost, httpPost).getEntity());
            } catch (Exception e) {
                return "";
            }
        } else {
            try {
                HttpResponse execute = defaultHttpClient.execute(httpPost);
                execute.getStatusLine().getStatusCode();
                return EntityUtils.toString(execute.getEntity());
            } catch (Exception e2) {
                return "";
            }
        }
    }

    public void a(boolean z) {
        this.a = z;
    }

    public boolean a() {
        return this.a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002a A[SYNTHETIC, Splitter:B:13:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0034 A[SYNTHETIC, Splitter:B:19:0x0034] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] a(java.lang.String r6) {
        /*
            r5 = this;
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream
            r0.<init>()
            java.io.DataOutputStream r1 = new java.io.DataOutputStream
            r1.<init>(r0)
            r2 = 0
            java.util.zip.GZIPOutputStream r3 = new java.util.zip.GZIPOutputStream     // Catch:{ Exception -> 0x0026, all -> 0x0030 }
            java.io.BufferedOutputStream r4 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x0026, all -> 0x0030 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0026, all -> 0x0030 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0026, all -> 0x0030 }
            byte[] r1 = r6.getBytes()     // Catch:{ Exception -> 0x003f, all -> 0x003c }
            r3.write(r1)     // Catch:{ Exception -> 0x003f, all -> 0x003c }
            if (r3 == 0) goto L_0x0021
            r3.close()     // Catch:{ IOException -> 0x0038 }
        L_0x0021:
            byte[] r0 = r0.toByteArray()
            return r0
        L_0x0026:
            r1 = move-exception
            r1 = r2
        L_0x0028:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x002e }
            goto L_0x0021
        L_0x002e:
            r1 = move-exception
            goto L_0x0021
        L_0x0030:
            r0 = move-exception
            r1 = r2
        L_0x0032:
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x003a }
        L_0x0037:
            throw r0
        L_0x0038:
            r1 = move-exception
            goto L_0x0021
        L_0x003a:
            r1 = move-exception
            goto L_0x0037
        L_0x003c:
            r0 = move-exception
            r1 = r3
            goto L_0x0032
        L_0x003f:
            r1 = move-exception
            r1 = r3
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.z.a(java.lang.String):byte[]");
    }

    public String b(String str, String str2) {
        return a(a(str2), str);
    }
}
