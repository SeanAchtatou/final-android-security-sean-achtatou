package com.waps;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class w extends AsyncTask {
    public static boolean e = false;
    public static boolean f = false;
    protected int a;
    z b;
    String c = "";
    String d = "";
    float g = 0.0f;
    float h = 0.0f;
    NumberFormat i = new DecimalFormat("#0");
    float j;
    InputStream k = null;
    FileOutputStream l = null;
    String m = "";
    String n = "";
    private String o;
    private View p;
    private x q;
    private y r;
    private Context s;

    public w(Context context, View view, String str) {
        try {
            this.s = context;
            this.p = view;
            this.o = str;
            this.c = str.substring(str.indexOf("http://") + 7, str.indexOf("/", str.indexOf("http://") + 8));
            this.d = str.substring(0, str.indexOf("/", str.indexOf("http://") + 8));
            this.q = new x(this.s);
            this.b = new z(this.s);
            AppConnect.a(this.s, this.b);
        } catch (Exception e2) {
        }
    }

    public String a(String str) {
        try {
            return str.substring(str.lastIndexOf("/") + 1, str.indexOf(".apk") + 4);
        } catch (Exception e2) {
            return "";
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        HttpResponse execute;
        try {
            this.a = (int) System.currentTimeMillis();
            this.q.a(this.p, (String) AppConnect.d(this.s).get("getting_filename"), this.a, "0 %");
            this.m = a(this.o);
            this.n = "/sdcard/download/";
            this.g = (float) b(this.o);
            if (!this.b.a()) {
                execute = new DefaultHttpClient().execute(new HttpGet(strArr[0].replaceAll(" ", "%20")));
            } else {
                String str = strArr[0];
                HttpHost httpHost = new HttpHost("10.0.0.172", 80, "http");
                HttpHost httpHost2 = new HttpHost(this.c, 80, "http");
                HttpGet httpGet = new HttpGet(str.replaceAll(" ", "%20").replaceFirst(this.d, ""));
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
                HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost);
                execute = defaultHttpClient.execute(httpHost2, httpGet);
            }
            this.k = execute.getEntity().getContent();
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file = new File(this.n);
                File file2 = new File(this.n, this.m);
                if (!file.exists()) {
                    file.mkdir();
                }
                if (!file2.exists()) {
                    file2.createNewFile();
                }
                this.l = new FileOutputStream(file2);
            } else {
                this.l = this.s.openFileOutput(this.m, 3);
            }
            if (this.k != null) {
                byte[] bArr = new byte[51200];
                while (true) {
                    int read = this.k.read(bArr);
                    if (read == -1) {
                        break;
                    } else if (Integer.parseInt(this.i.format((double) this.j)) > 100) {
                        this.q.a(this.p, this.m, this.a, this.n + this.m, (String) AppConnect.d(this.s).get("failed_to_download"));
                        break;
                    } else {
                        this.l.write(bArr, 0, read);
                        this.h = ((float) read) + this.h;
                        this.j = (this.h / this.g) * 100.0f;
                        publishProgress(Integer.valueOf(((int) (this.h / this.g)) * 100));
                        this.i.format((double) this.j);
                        this.q.a(this.p, this.m, this.a, this.i.format((double) this.j) + " %");
                        f = true;
                    }
                }
                int read2 = this.k.read(bArr);
                Thread.sleep(1000);
                if (read2 == -1) {
                    f = false;
                    String str2 = this.n + this.m;
                    File file3 = Environment.getExternalStorageState().equals("mounted") ? new File(str2) : this.s.getFileStreamPath(this.m);
                    if (this.m.endsWith(".apk")) {
                        this.q.a(this.p, this.m, this.a, str2, (String) AppConnect.d(this.s).get("click_to_install"));
                    } else {
                        this.q.a(this.p, this.m, this.a, str2, (String) AppConnect.d(this.s).get("download_complete"));
                    }
                    e = true;
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.fromFile(file3), "application/vnd.android.package-archive");
                    this.s.startActivity(intent);
                    this.r = new y(this.q, this.a, this.m);
                    a(this.r);
                }
            }
            try {
                if (this.k != null) {
                    this.k.close();
                }
                if (this.l != null) {
                    this.l.close();
                }
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            try {
                if (this.k != null) {
                    this.k.close();
                }
                if (this.l != null) {
                    this.l.close();
                }
            } catch (Exception e4) {
            }
        } catch (Throwable th) {
            try {
                if (this.k != null) {
                    this.k.close();
                }
                if (this.l != null) {
                    this.l.close();
                }
            } catch (Exception e5) {
            }
            throw th;
        }
        return "";
    }

    public void a(y yVar) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        this.s.registerReceiver(yVar, intentFilter);
    }

    public long b(String str) {
        try {
            if (!this.b.a()) {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                httpURLConnection.setConnectTimeout(5000);
                httpURLConnection.setRequestMethod("GET");
                return (long) httpURLConnection.getContentLength();
            }
            HttpHost httpHost = new HttpHost("10.0.0.172", 80, "http");
            HttpHost httpHost2 = new HttpHost(this.c, 80, "http");
            HttpGet httpGet = new HttpGet(str.replaceAll(" ", "%20").replaceFirst(this.d, ""));
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost);
            return defaultHttpClient.execute(httpHost2, httpGet).getEntity().getContentLength();
        } catch (Exception e2) {
            return 0;
        }
    }
}
