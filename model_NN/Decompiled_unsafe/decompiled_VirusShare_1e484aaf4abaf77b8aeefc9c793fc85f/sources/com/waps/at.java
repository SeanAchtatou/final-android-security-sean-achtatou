package com.waps;

import android.widget.LinearLayout;

class at implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ SDKUtils b;

    at(SDKUtils sDKUtils, String str) {
        this.b = sDKUtils;
        this.a = str;
    }

    public void run() {
        int initAdWidth = this.b.initAdWidth();
        int i = (int) (((float) initAdWidth) / 6.4f);
        if (!SDKUtils.isNull(this.a)) {
            i = Integer.parseInt(this.a);
        }
        this.b.h.setLayoutParams(new LinearLayout.LayoutParams(initAdWidth, i));
        if (this.b.h.getParent() == null) {
            this.b.i.addView(this.b.h);
        }
    }
}
