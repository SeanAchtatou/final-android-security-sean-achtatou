package com.waps;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.io.File;
import java.util.List;
import java.util.Map;

public class OffersWebView extends Activity {
    public static boolean outsideFlag = false;
    /* access modifiers changed from: private */
    public static Map z;
    private String A = "";
    private String B = "";
    private String C = "";
    private String D = "";
    private String E = "";
    private String F = "";
    w a;
    String b = "";
    String c = "";
    y d;
    String e;
    String f;
    x g = null;
    File h = null;
    AlertDialog i = null;
    /* access modifiers changed from: private */
    public WebView j = null;
    private String k = null;
    /* access modifiers changed from: private */
    public ProgressBar l;
    private String m = "";
    private String n = "";
    private String o = "";
    private String p = "";
    private String q = "";
    /* access modifiers changed from: private */
    public String r = "false";
    /* access modifiers changed from: private */
    public String s;
    /* access modifiers changed from: private */
    public boolean t = true;
    private String u = "";
    private String v = "";
    private String w = "";
    private String x = "";
    private String y = "";

    private void a(Context context) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        String str = "";
        for (int i2 = 0; i2 < installedPackages.size(); i2++) {
            str = str + installedPackages.get(i2).packageName;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences("Package_Name", 3).edit();
        edit.putString("Package_Names", str);
        edit.commit();
    }

    private void a(SharedPreferences sharedPreferences) {
        if (sharedPreferences != null) {
            this.A = sharedPreferences.getString("Notify_UrlPath", "");
            this.C = sharedPreferences.getString("offers_webview_tag", "");
            this.D = sharedPreferences.getString("NotifyAd_Tag", "");
            if (this.D != null && this.D.equals("true")) {
                this.E = sharedPreferences.getString("Notify_Show_detail", "");
            }
            if (!SDKUtils.isNull(this.F)) {
                this.F = sharedPreferences.getString("Notify_Ad_Package", "");
            }
            if (this.A.contains("down_type")) {
                this.B = sharedPreferences.getString("Notify_UrlParams", "");
                this.B += "&at=" + System.currentTimeMillis();
            }
        }
    }

    private void a(Bundle bundle) {
        if (bundle != null) {
            if (bundle.getString("Offers_URL") != null) {
                this.o = bundle.getString("Offers_URL");
            }
            if (bundle.getString("URL") != null) {
                this.o = bundle.getString("URL");
            }
            if (bundle.getString("UrlPath") != null) {
                this.u = bundle.getString("Notify_Id");
                this.v = bundle.getString("UrlPath");
                this.w = bundle.getString("ACTIVITY_FLAG");
                this.x = bundle.getString("SHWO_FLAG");
                if (this.v.contains("down_type")) {
                    this.y = bundle.getString("Notify_Url_Params");
                }
            }
            this.p = bundle.getString("URL_PARAMS");
            if (SDKUtils.isNull(this.p)) {
                this.p = "&" + this.p;
            }
            this.n = bundle.getString("CLIENT_PACKAGE");
            this.q = bundle.getString("USER_ID");
            this.r = bundle.getString("isFinshClose");
            this.p += "&publisher_user_id=" + this.q;
            this.p += "&at=" + System.currentTimeMillis();
            this.m = bundle.getString("offers_webview_tag");
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        try {
            this.b = this.a.a(this.s);
            this.c = "/sdcard/download/";
            int b2 = (int) this.a.b(this.s);
            if (Environment.getExternalStorageState().equals("mounted")) {
                this.h = new File(this.c, this.b);
                if (this.h != null && this.h.length() == ((long) b2)) {
                    this.i = new AlertDialog.Builder(this).setTitle((CharSequence) z.get("reminder")).setIcon(17301618).setMessage((CharSequence) z.get("app_already_exists")).setPositiveButton((CharSequence) z.get("install"), new ao(this)).setNeutralButton((CharSequence) z.get("download_again"), new an(this)).create();
                    this.i.show();
                    this.i.setOnCancelListener(new ap(this));
                } else if (this.h.length() >= ((long) b2) || this.h.length() == 0) {
                    if (this.h.length() == 0) {
                        Toast.makeText(this, (CharSequence) z.get("prepare_to_download"), 0).show();
                        this.a.execute(this.s);
                        if (this.r != null && "true".equals(this.r)) {
                            finish();
                        }
                    }
                } else if (w.f) {
                    Toast.makeText(this, (CharSequence) z.get("waitting_for_download"), 0).show();
                } else {
                    Toast.makeText(this, (CharSequence) z.get("prepare_to_download"), 0).show();
                    this.h.delete();
                    this.a.execute(this.s);
                    if (this.r != null && "true".equals(this.r)) {
                        finish();
                    }
                }
            } else {
                this.h = getFileStreamPath(this.b);
                if (this.h != null && this.h.length() == ((long) b2)) {
                    this.h.delete();
                    Toast.makeText(this, (CharSequence) z.get("prepare_to_download"), 0).show();
                    this.a.execute(this.s);
                    if (this.r != null && "true".equals(this.r)) {
                        finish();
                    }
                } else if (this.h != null && this.h.length() != 0) {
                    Toast.makeText(this, (CharSequence) z.get("waitting_for_download"), 0).show();
                } else if (this.h.length() == 0) {
                    Toast.makeText(this, (CharSequence) z.get("prepare_to_download"), 0).show();
                    this.a.execute(this.s);
                    if (this.r != null && "true".equals(this.r)) {
                        finish();
                    }
                }
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        RelativeLayout.LayoutParams layoutParams;
        RelativeLayout.LayoutParams layoutParams2;
        super.onCreate(bundle);
        try {
            if (z == null || z.size() == 0) {
                z = AppConnect.d(this);
            }
            a(getIntent().getExtras());
            a(getSharedPreferences("Notify", 3));
            this.e = ((ActivityManager) getSystemService("activity")).getRunningTasks(2).get(0).baseActivity.getShortClassName();
            SharedPreferences sharedPreferences = getSharedPreferences("AppSettings", 1);
            this.f = sharedPreferences.getString("short_className", "");
            String f2 = AppConnect.f(this);
            String substring = f2.substring(f2.lastIndexOf("."), f2.length());
            if (!SDKUtils.isNull(this.f) && !this.e.contains(substring)) {
                outsideFlag = false;
            }
            if (outsideFlag) {
                String string = sharedPreferences.getString("packageName", "");
                String string2 = sharedPreferences.getString("className", "");
                super.onCreate(bundle);
                if (!"".equals(string.trim()) && !"".equals(string2.trim()) && !string2.contains("OffersWebView")) {
                    finish();
                    try {
                        Intent launchIntentForPackage = getPackageManager().getLaunchIntentForPackage(string);
                        if (launchIntentForPackage != null) {
                            startActivity(launchIntentForPackage);
                        }
                    } catch (Exception e2) {
                    }
                }
            }
            if ("feedback".equals(this.w)) {
                this.k = this.v + "?" + this.p;
            } else if (SDKUtils.isNull(this.m)) {
                if (!this.A.contains("down_type")) {
                    this.m = this.C;
                    this.k = this.A + "?" + "nyid" + "=" + this.u + this.B;
                } else if (this.D.equals("true")) {
                    this.m = this.C;
                    this.k = this.A + "&publisher_user_id=" + this.q + "&" + "at" + "=" + System.currentTimeMillis();
                    if (SDKUtils.isNull(this.E) || this.E.equals("false")) {
                        this.r = "true";
                    }
                } else if (this.D.equals("false")) {
                    this.m = this.C;
                    this.k = this.A + "&" + this.B;
                    this.r = "true";
                }
            } else if (this.o.indexOf("?") > -1) {
                this.k = this.o + this.p;
            } else {
                this.k = this.o + "?a=1" + this.p;
            }
            this.k = this.k.replaceAll(" ", "%20");
            if (!SDKUtils.isNull(this.m) && this.m.equals("OffersWebView")) {
                requestWindowFeature(1);
                RelativeLayout relativeLayout = new RelativeLayout(this);
                this.j = new WebView(this);
                this.l = new ProgressBar(this);
                this.l.setEnabled(true);
                this.l.setVisibility(0);
                WebSettings settings = this.j.getSettings();
                if (!SDKUtils.isNull(this.w)) {
                    if ("notify".equals(this.w)) {
                        layoutParams = new RelativeLayout.LayoutParams(-1, -2);
                        layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
                    } else {
                        layoutParams = null;
                        layoutParams2 = null;
                    }
                    if ("feedback".equals(this.w)) {
                        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
                        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -1);
                        relativeLayout.setBackgroundColor(-1);
                        setRequestedOrientation(1);
                        RelativeLayout.LayoutParams layoutParams5 = layoutParams4;
                        layoutParams = layoutParams3;
                        layoutParams2 = layoutParams5;
                    }
                    RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-2, -2);
                    this.j.setId(2);
                    this.j.setLayoutParams(layoutParams2);
                    this.l.setLayoutParams(layoutParams6);
                    layoutParams.addRule(12);
                    layoutParams2.addRule(2, 1);
                    layoutParams6.addRule(13);
                    relativeLayout.addView(this.j, layoutParams2);
                    relativeLayout.addView(this.l, layoutParams6);
                    setContentView(relativeLayout);
                } else {
                    RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(-1, -1);
                    LinearLayout linearLayout = new LinearLayout(this);
                    linearLayout.setOrientation(1);
                    linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                    relativeLayout.setGravity(17);
                    RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(-2, -2);
                    layoutParams8.addRule(13);
                    relativeLayout.addView(this.j, layoutParams7);
                    relativeLayout.addView(this.l, layoutParams8);
                    linearLayout.addView(relativeLayout, new ViewGroup.LayoutParams(-1, -1));
                    setContentView(linearLayout);
                }
                this.j.setWebViewClient(new aq(this, null));
                try {
                    this.j.addJavascriptInterface(new SDKUtils(this), "SDKUtils");
                    settings.setJavaScriptEnabled(true);
                } catch (Exception e3) {
                }
                settings.setCacheMode(2);
                WebView webView = this.j;
                WebView.enablePlatformNotifications();
                this.j.setScrollBarStyle(0);
                this.j.setVerticalScrollBarEnabled(false);
                this.j.setHorizontalScrollBarEnabled(false);
                this.j.loadUrl(this.k);
                this.j.setDownloadListener(new am(this));
                if (getSharedPreferences("Package_Name", 3).getString("Package_Names", "") == "") {
                    a((Context) this);
                }
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            if (this.j != null) {
                this.j.clearHistory();
                this.j.clearCache(true);
                this.j.destroy();
            }
            z = null;
            finish();
        } catch (Exception e2) {
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.j != null) {
            if (i2 == 4 && !this.f.equals(this.e)) {
                outsideFlag = true;
            }
            if (i2 == 4 && this.j.canGoBack()) {
                if (!this.t) {
                    finish();
                    this.t = true;
                }
                this.j.goBack();
                x.c = true;
                return true;
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (!(this.k == null || this.j == null)) {
            this.j.loadUrl(this.k);
        }
        super.onResume();
    }
}
