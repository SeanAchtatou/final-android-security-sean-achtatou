package com.waps;

import android.app.AlertDialog;
import android.os.AsyncTask;

class i extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private i(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ i(AppConnect appConnect, g gVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        try {
            String a2 = this.a.ak;
            if (SDKUtils.isNull(AppConnect.bg)) {
                String unused = AppConnect.bg = AppConnect.T.a("http://app.wapx.cn/action/connect/active?", a2);
            }
            if (!SDKUtils.isNull(AppConnect.bg)) {
                z = this.a.g(AppConnect.bg);
            }
        } catch (Exception e) {
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        try {
            if (SDKUtils.isNull(this.a.aq) || this.a.aq.compareTo(this.a.ac) <= 0) {
                new AlertDialog.Builder(this.a.Q).setTitle((CharSequence) AppConnect.E.get("update_version")).setMessage((CharSequence) AppConnect.E.get("is_newest_version")).setPositiveButton((CharSequence) AppConnect.E.get("ok"), new j(this)).create().show();
            } else {
                this.a.h("http://app.wapx.cn/action/app/update?" + this.a.ak);
            }
        } catch (Exception e) {
        }
    }
}
