package com.waps;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MiniAdView implements DisplayAdNotifier {
    private static final long DELETE_ICON_LIMIT_TIME = 604800000;
    static long e = 0;
    static int f = 5;
    protected static List h;
    /* access modifiers changed from: private */
    public static List q;
    /* access modifiers changed from: private */
    public static int w = 0;
    private SharedPreferences.Editor A = null;
    LinearLayout a;
    boolean b = false;
    View c;
    Context d;
    String g;
    TextView i;
    private int[] j;
    private int k;
    private AnimationType l;
    private int m = -1;
    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new ah(this);
    /* access modifiers changed from: private */
    public String n = "";
    /* access modifiers changed from: private */
    public int o = AppConnect.G;
    /* access modifiers changed from: private */
    public boolean p = true;
    /* access modifiers changed from: private */
    public int r = 0;
    private String s;
    private String t;
    private String u;
    private int v;
    private String x = "/Android/data/cache/iconCache";
    private String y = "CacheTime";
    private SharedPreferences z = null;

    public MiniAdView() {
    }

    public MiniAdView(Context context) {
        this.d = context;
    }

    public MiniAdView(Context context, LinearLayout linearLayout) {
        a(this.x);
        this.d = context;
        this.a = linearLayout;
        this.a.setBackgroundColor(AppConnect.F);
        this.j = new int[]{-2};
        this.k = -2;
        this.m = 0;
        this.z = context.getSharedPreferences("AppSettings", 0);
        this.A = this.z.edit();
    }

    private LinearLayout a(Context context, Bitmap bitmap, String str, int i2) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        linearLayout.setBackgroundColor(Color.argb(0, 0, 0, 0));
        linearLayout.setPadding(5, 0, 0, 0);
        int i3 = (int) (((float) i2) / 16.0f);
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new ViewGroup.LayoutParams((int) (((float) i2) / 16.0f), i3));
        if (bitmap != null) {
            if (bitmap.getWidth() > i3) {
                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            } else {
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            }
            imageView.setImageBitmap(bitmap);
        } else {
            imageView.setImageResource(17301516);
        }
        this.i = new TextView(context);
        this.i.setText(str);
        this.i.setTextSize(15.0f);
        this.i.setTextColor(this.o);
        this.i.setPadding(5, 0, 0, 0);
        linearLayout.addView(imageView);
        linearLayout.addView(this.i);
        return linearLayout;
    }

    private void a(String str) {
        try {
            String loadStringFromLocal = new SDKUtils(this.d).loadStringFromLocal(this.y, str);
            if (SDKUtils.isNull(loadStringFromLocal)) {
                new SDKUtils(this.d).saveDataToLocal(String.valueOf(System.currentTimeMillis()), this.y, str);
            } else if (!SDKUtils.isNull(loadStringFromLocal)) {
                if (System.currentTimeMillis() - Long.parseLong(loadStringFromLocal.replaceAll("\n", "")) >= DELETE_ICON_LIMIT_TIME) {
                    try {
                        if (Environment.getExternalStorageState().equals("mounted")) {
                            new SDKUtils(this.d).deleteLocalFiles(new File(Environment.getExternalStorageDirectory().toString() + str));
                        }
                    } catch (Exception e2) {
                    }
                    new SDKUtils(this.d).saveDataToLocal(System.currentTimeMillis() + "", this.y, str);
                }
            }
        } catch (Exception e3) {
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        try {
            this.v = i2;
            if (w == 0) {
                this.b = true;
                this.mHandler.post(this.mUpdateResults);
                return;
            }
            this.s = ((a) h.get(this.v)).a();
            this.t = ((a) h.get(this.v)).b();
            this.u = ((a) h.get(this.v)).c();
            Bitmap a2 = a(this.d, this.t, this.s, this.x);
            int initAdWidth = new SDKUtils(this.d).initAdWidth();
            this.c = a(this.d, a2, this.u, initAdWidth);
            this.c.setLayoutParams(new ViewGroup.LayoutParams(initAdWidth, (int) (((float) initAdWidth) / 13.333333f)));
            this.mHandler.post(new aj(this));
            this.c.setOnClickListener(getMiniAdClickListener(this.d, this.v));
            if (this.c != null) {
                this.b = true;
                this.mHandler.post(this.mUpdateResults);
            }
            this.A.putInt("MiniAd_ShowTag", this.v);
            this.A.commit();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        try {
            this.a.removeView(this.a.getChildAt(0));
            this.a.removeAllViews();
            if (this.c != null && this.b) {
                if (w == 0) {
                    this.c.setLayoutParams(new ViewGroup.LayoutParams(0, 0));
                    return;
                }
                this.a.refreshDrawableState();
                this.a.setAlwaysDrawnWithCacheEnabled(true);
                this.a.clearFocus();
                this.a.clearDisappearingChildren();
                this.a.addView(this.c);
                this.n = Build.VERSION.SDK;
                this.a.clearAnimation();
                this.c.clearAnimation();
                if (this.m == 0) {
                    this.l = new AnimationType(this.j);
                } else if (this.m == 1) {
                    this.l = new AnimationType(this.k);
                } else if (this.m == 2) {
                    this.l = new AnimationType(this.j);
                }
                this.l.startAnimation(this.c);
                this.b = false;
            }
        } catch (Exception e2) {
        }
    }

    static /* synthetic */ int d(MiniAdView miniAdView) {
        int i2 = miniAdView.r;
        miniAdView.r = i2 + 1;
        return i2;
    }

    private void d() {
        e = System.currentTimeMillis();
        this.z = this.d.getSharedPreferences("AppSettings", 0);
        int i2 = this.z.getInt("MiniAd_ShowTag", -1);
        if (i2 != -1) {
            this.r = i2 + 1;
        }
        if (this.r >= w) {
            this.r = 0;
        }
        b(this.r);
        new ai(this).start();
    }

    /* access modifiers changed from: private */
    public void e() {
        for (int i2 = 0; i2 < q.size(); i2++) {
            for (int i3 = 0; i3 < h.size(); i3++) {
                if (((a) h.get(i3)).a().equals(q.get(i2))) {
                    h.remove(h.get(i3));
                }
            }
        }
        q = null;
        q = new ArrayList();
    }

    public void DisplayAd() {
        DisplayAd(f);
    }

    public void DisplayAd(int i2) {
        try {
            if (this.d.getSharedPreferences("ShowAdFlag", 3).getBoolean("show_mini_flag", true)) {
                showADS();
                q = new ArrayList();
            }
            f = i2;
            if (i2 < 5) {
                f = 5;
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap a(Context context, String str, String str2, String str3) {
        return new AppConnect().a(context, str, str2, str3);
    }

    public void getDisplayAdResponse(View view) {
    }

    public void getDisplayAdResponse(Object obj) {
        h = (List) obj;
        w = h.size();
        d();
    }

    public void getDisplayAdResponseFailed(String str) {
        this.b = false;
        this.mHandler.post(this.mUpdateResults);
    }

    public View.OnClickListener getMiniAdClickListener(Context context, int i2) {
        return new al(this, context, i2);
    }

    public MiniAdView setAnimationType(int i2) {
        this.k = i2;
        this.m = 1;
        return this;
    }

    public MiniAdView setAnimationType(int[] iArr) {
        this.j = iArr;
        this.m = 2;
        return this;
    }

    public void showADS() {
        AppConnect.getInstance(this.d).a(this);
    }
}
