package com.waps;

import android.os.AsyncTask;

class q extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private q(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ q(AppConnect appConnect, g gVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.T.a("http://app.wapx.cn/action/account/spend?", this.a.ak + "&points=" + this.a.af);
        if (!SDKUtils.isNull(a2)) {
            z = this.a.k(a2);
        }
        if (!z) {
            AppConnect.ay.getUpdatePointsFailed((String) AppConnect.E.get("failed_to_spend_points"));
        }
        return Boolean.valueOf(z);
    }
}
