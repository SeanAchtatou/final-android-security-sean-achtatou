package com.waps;

import android.content.DialogInterface;
import android.content.Intent;

class g implements DialogInterface.OnClickListener {
    final /* synthetic */ String a;
    final /* synthetic */ AppConnect b;

    g(AppConnect appConnect, String str) {
        this.b = appConnect;
        this.a = str;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this.b.Q, AppConnect.g(this.b.Q));
        intent.putExtra("URL", this.a);
        intent.putExtra("isFinshClose", "true");
        intent.putExtra("CLIENT_PACKAGE", this.b.aj);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        this.b.Q.startActivity(intent);
        boolean unused = AppConnect.ar = false;
    }
}
