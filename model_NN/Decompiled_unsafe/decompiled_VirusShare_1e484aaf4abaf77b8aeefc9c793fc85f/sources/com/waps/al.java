package com.waps;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.view.View;

public final class al implements View.OnClickListener {
    int a = 0;
    Context b;
    final /* synthetic */ MiniAdView c;

    public al(MiniAdView miniAdView, Context context, int i) {
        this.c = miniAdView;
        this.a = i;
        this.b = context;
    }

    public void onClick(View view) {
        String str;
        boolean z;
        Intent intent;
        try {
            MiniAdView.q.add(((a) MiniAdView.h.get(this.a)).a());
            String f = ((a) MiniAdView.h.get(this.a)).f();
            String g = ((a) MiniAdView.h.get(this.a)).g();
            String d = ((a) MiniAdView.h.get(this.a)).d();
            String e = ((a) MiniAdView.h.get(this.a)).e();
            try {
                ApplicationInfo applicationInfo = this.b.getPackageManager().getApplicationInfo(this.b.getPackageName(), 128);
                String packageName = this.b.getPackageName();
                try {
                    str = applicationInfo.metaData != null ? applicationInfo.metaData.getString("CLIENT_PACKAGE") : "";
                    if (SDKUtils.isNull(str)) {
                        str = packageName;
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    str = packageName;
                }
            } catch (PackageManager.NameNotFoundException e3) {
                str = "";
            }
            if (!SDKUtils.isNull(f)) {
                try {
                    intent = this.b.getPackageManager().getLaunchIntentForPackage(f);
                } catch (Exception e4) {
                    intent = null;
                }
                if (intent != null) {
                    this.b.startActivity(intent);
                    AppConnect.getInstanceNoConnect(this.b).a(f, 2);
                    z = false;
                } else {
                    z = true;
                }
            } else {
                z = true;
            }
            if (!z) {
                return;
            }
            if (!SDKUtils.isNull(g)) {
                new SDKUtils(this.b).openUrlByBrowser(g, d);
                return;
            }
            Intent intent2 = new Intent(this.b, AppConnect.g(this.b));
            intent2.putExtra("URL", d);
            intent2.putExtra("CLIENT_PACKAGE", str);
            intent2.putExtra("offers_webview_tag", "OffersWebView");
            if (SDKUtils.isNull(e) || e.equals("false")) {
                intent2.putExtra("isFinshClose", "true");
            }
            this.b.startActivity(intent2);
        } catch (Exception e5) {
        }
    }
}
