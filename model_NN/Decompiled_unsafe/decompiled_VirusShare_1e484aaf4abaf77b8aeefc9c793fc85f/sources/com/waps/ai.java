package com.waps;

import android.app.Activity;
import android.app.ActivityManager;
import android.os.Build;
import android.os.PowerManager;
import java.util.List;

class ai extends Thread {
    final /* synthetic */ MiniAdView a;

    ai(MiniAdView miniAdView) {
        this.a = miniAdView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.MiniAdView.a(com.waps.MiniAdView, boolean):boolean
     arg types: [com.waps.MiniAdView, int]
     candidates:
      com.waps.MiniAdView.a(com.waps.MiniAdView, int):int
      com.waps.MiniAdView.a(com.waps.MiniAdView, java.lang.String):java.lang.String
      com.waps.MiniAdView.a(com.waps.MiniAdView, boolean):boolean */
    public void run() {
        while (this.a.p) {
            try {
                List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) this.a.d.getSystemService("activity")).getRunningTasks(2);
                if (runningTasks != null && !runningTasks.isEmpty()) {
                    long currentTimeMillis = System.currentTimeMillis() - MiniAdView.e;
                    this.a.g = runningTasks.get(0).topActivity.getShortClassName();
                    String unused = this.a.n = Build.VERSION.SDK;
                    int parseInt = Integer.parseInt(this.a.n);
                    PowerManager powerManager = (PowerManager) this.a.d.getSystemService("power");
                    if ((parseInt >= 7 ? ((Boolean) powerManager.getClass().getDeclaredMethod("isScreenOn", new Class[0]).invoke(powerManager, new Object[0])).booleanValue() : true) && this.a.d.getClass().toString().contains(this.a.g) && currentTimeMillis > ((long) ((MiniAdView.f * 1000) - 1000))) {
                        MiniAdView.d(this.a);
                        if (this.a.r >= MiniAdView.w) {
                            int unused2 = this.a.r = 0;
                            this.a.e();
                            int unused3 = MiniAdView.w = MiniAdView.h.size();
                        }
                        this.a.b(this.a.r);
                        MiniAdView.e = System.currentTimeMillis();
                    }
                }
                sleep((long) (MiniAdView.f * 1000));
                if (((Activity) this.a.d).isFinishing() || MiniAdView.w == 0) {
                    boolean unused4 = this.a.p = false;
                }
            } catch (Exception e) {
                return;
            }
        }
    }
}
