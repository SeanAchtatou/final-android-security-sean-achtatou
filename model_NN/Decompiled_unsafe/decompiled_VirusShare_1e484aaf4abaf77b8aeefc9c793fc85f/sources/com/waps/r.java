package com.waps;

import android.os.AsyncTask;
import java.util.Map;

class r extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private r(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ r(AppConnect appConnect, g gVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z;
        String a2 = this.a.ak;
        if (!this.a.al.equals("")) {
            a2 = a2 + "&" + this.a.al;
        }
        String a3 = AppConnect.T.a("http://app.wapx.cn/action/connect/active?", a2);
        if (!SDKUtils.isNull(a3)) {
            z = this.a.d(a3);
        } else {
            try {
                String string = this.a.Q.getSharedPreferences("AppSettings", 0).getString("AttrConfig", "");
                if (!SDKUtils.isNull(string)) {
                    Map unused = this.a.c(string);
                }
                z = false;
            } catch (Exception e) {
                z = false;
            }
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x013e, code lost:
        boolean unused = com.waps.AppConnect.as = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0142, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0143, code lost:
        boolean unused = com.waps.AppConnect.as = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0146, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0142 A[ExcHandler: all (r0v1 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0003] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onPostExecute(java.lang.Boolean r9) {
        /*
            r8 = this;
            r7 = 0
            java.lang.String r0 = "push_flag"
            boolean r0 = com.waps.AppConnect.ar     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            if (r0 == 0) goto L_0x0049
            com.waps.AppConnect r0 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r0 = r0.aq     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            boolean r0 = com.waps.SDKUtils.isNull(r0)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            if (r0 != 0) goto L_0x0049
            com.waps.AppConnect r0 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r0 = r0.aq     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.AppConnect r1 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r1 = r1.ac     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            int r0 = r0.compareTo(r1)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            if (r0 <= 0) goto L_0x0049
            com.waps.AppConnect r0 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r1.<init>()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r2 = "http://app.wapx.cn/action/app/update?"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.AppConnect r2 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r2 = r2.ak     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r0.h(r1)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r0 = 0
            boolean unused = com.waps.AppConnect.ar = r0     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
        L_0x0049:
            boolean r0 = com.waps.AppConnect.as     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            if (r0 == 0) goto L_0x008a
            java.lang.String r0 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            if (r0 == 0) goto L_0x008a
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r1.<init>()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.io.File r2 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r2 = "/Android/data/cache"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r2 = "/Package.dat"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            boolean r1 = r0.exists()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            if (r1 == 0) goto L_0x008a
            r0.delete()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
        L_0x008a:
            java.lang.String r0 = com.waps.AppConnect.aE     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            boolean r0 = com.waps.SDKUtils.isNull(r0)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            if (r0 == 0) goto L_0x00b2
            com.waps.AppConnect r0 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r0.t()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.AppConnect r0 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.o r1 = new com.waps.o     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.AppConnect r2 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r3 = 0
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.o unused = r0.aD = r1     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.AppConnect r0 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.o r0 = r0.aD     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r1 = 0
            java.lang.Void[] r1 = new java.lang.Void[r1]     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r0.execute(r1)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
        L_0x00b2:
            com.waps.AppConnect r0 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            android.content.Context r0 = r0.Q     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r1 = "PushFlag"
            r2 = 3
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r1 = "push_flag"
            java.lang.String r2 = ""
            java.lang.String r1 = r0.getString(r1, r2)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r2 = "true"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            if (r1 == 0) goto L_0x00fe
            java.lang.String r1 = com.waps.AppConnect.B     // Catch:{ Exception -> 0x013a, all -> 0x0142 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x013a, all -> 0x0142 }
        L_0x00d5:
            com.waps.AppConnect r2 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.util.concurrent.ScheduledExecutorService r2 = r2.R     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.u r3 = new com.waps.u     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.AppConnect r4 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r5 = com.waps.AppConnect.C     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.AppConnect r6 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r6 = r6.ak     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r3.<init>(r4, r5, r6)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            long r4 = (long) r1     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r2.schedule(r3, r4, r1)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.lang.String r1 = "push_flag"
            java.lang.String r2 = "false"
            r0.putString(r1, r2)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r0.commit()     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
        L_0x00fe:
            boolean r0 = com.waps.AppConnect.aY     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            if (r0 == 0) goto L_0x011d
            com.waps.AppConnect r0 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            java.util.concurrent.ScheduledExecutorService r0 = r0.R     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.t r1 = new com.waps.t     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            com.waps.AppConnect r2 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r3 = 0
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r2 = 0
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r0.schedule(r1, r2, r4)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r0 = 0
            boolean unused = com.waps.AppConnect.aY = r0     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
        L_0x011d:
            boolean r0 = com.waps.AppConnect.be     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            if (r0 == 0) goto L_0x0136
            boolean r0 = com.waps.AppConnect.bd     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            if (r0 == 0) goto L_0x0132
            com.waps.AppConnect r0 = r8.a     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            boolean r1 = com.waps.AppConnect.bd     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
            r0.setCrashReport(r1)     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
        L_0x0132:
            r0 = 0
            boolean unused = com.waps.AppConnect.be = r0     // Catch:{ Exception -> 0x013d, all -> 0x0142 }
        L_0x0136:
            boolean unused = com.waps.AppConnect.as = r7
        L_0x0139:
            return
        L_0x013a:
            r1 = move-exception
            r1 = r7
            goto L_0x00d5
        L_0x013d:
            r0 = move-exception
            boolean unused = com.waps.AppConnect.as = r7
            goto L_0x0139
        L_0x0142:
            r0 = move-exception
            boolean unused = com.waps.AppConnect.as = r7
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.waps.r.onPostExecute(java.lang.Boolean):void");
    }
}
