package com.waps;

import android.os.AsyncTask;

class m extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private m(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ m(AppConnect appConnect, g gVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z;
        long j = this.a.Q.getSharedPreferences("AppSettings", 0).getLong("GetPointsTime", 0);
        long currentTimeMillis = System.currentTimeMillis();
        int historyPoints = AppConnect.getHistoryPoints(this.a.Q);
        String historyPointsName = AppConnect.getHistoryPointsName(this.a.Q);
        if (j == 0 || (j != 0 && currentTimeMillis - j >= 20000)) {
            String a2 = AppConnect.T.a("http://app.wapx.cn/action/account/getinfo?", this.a.ak);
            if (!SDKUtils.isNull(a2)) {
                z = this.a.i(a2);
            } else if (historyPoints < 0 || historyPointsName == null) {
                z = false;
            } else {
                AppConnect.ay.getUpdatePoints(historyPointsName, historyPoints);
                return true;
            }
            if (!z) {
                AppConnect.ay.getUpdatePointsFailed((String) AppConnect.E.get("failed_to_update_points"));
            }
            return Boolean.valueOf(z);
        } else if (historyPoints < 0 || historyPointsName == null) {
            return false;
        } else {
            AppConnect.ay.getUpdatePoints(historyPointsName, historyPoints);
            return true;
        }
    }
}
