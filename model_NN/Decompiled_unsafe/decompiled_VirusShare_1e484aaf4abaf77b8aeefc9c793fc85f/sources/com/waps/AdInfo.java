package com.waps;

import android.graphics.Bitmap;

public class AdInfo {
    private String a = "";
    private String b = "";
    private String c = "";
    private Bitmap d = null;
    private int e = 0;
    private String f = "";
    private String g = "";
    private String h = "";
    private String i = "";
    private String[] j = null;

    /* access modifiers changed from: protected */
    public void a(int i2) {
        this.e = i2;
    }

    /* access modifiers changed from: protected */
    public void a(Bitmap bitmap) {
        this.d = bitmap;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public void a(String[] strArr) {
        this.j = strArr;
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        this.c = str;
    }

    /* access modifiers changed from: protected */
    public void d(String str) {
        this.f = str;
    }

    /* access modifiers changed from: protected */
    public void e(String str) {
        this.g = str;
    }

    /* access modifiers changed from: protected */
    public void f(String str) {
        this.h = str;
    }

    /* access modifiers changed from: protected */
    public void g(String str) {
        this.i = str;
    }

    public Bitmap getAdIcon() {
        return this.d;
    }

    public String getAdId() {
        return this.a;
    }

    public String getAdName() {
        return this.b;
    }

    public int getAdPoints() {
        return this.e;
    }

    public String getAdText() {
        return this.c;
    }

    public String getDescription() {
        return this.f;
    }

    public String getFilesize() {
        return this.h;
    }

    public String[] getImageUrls() {
        return this.j;
    }

    public String getProvider() {
        return this.i;
    }

    public String getVersion() {
        return this.g;
    }
}
