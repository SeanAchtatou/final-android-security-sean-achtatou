package com.waps;

import android.content.Context;
import android.os.Handler;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class AdView {
    private static String p = "ad/show?";
    /* access modifiers changed from: private */
    public Context a;
    private LinearLayout b;
    private z c = null;
    /* access modifiers changed from: private */
    public long d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f = "";
    /* access modifiers changed from: private */
    public RelativeLayout g = null;
    /* access modifiers changed from: private */
    public int h = 5;
    /* access modifiers changed from: private */
    public boolean i = false;
    private WebView j;
    /* access modifiers changed from: private */
    public boolean k = true;
    /* access modifiers changed from: private */
    public boolean l = true;
    /* access modifiers changed from: private */
    public String m = "";
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public int n = 0;
    private float o = 6.4f;
    /* access modifiers changed from: private */
    public String q = ("http://ads.wapx.cn/action/" + p);
    /* access modifiers changed from: private */
    public boolean r = false;

    public AdView(Context context, LinearLayout linearLayout) {
        this.a = context;
        this.b = linearLayout;
        this.q += new AppConnect().c(context);
        this.c = new z(context);
        AppConnect.a(context, this.c);
    }

    private WebView a() {
        WebView webView = new WebView(this.a);
        webView.setLayoutParams(new ViewGroup.LayoutParams(new SDKUtils(this.a).initAdWidth(), -2));
        webView.setScrollBarStyle(0);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setBackgroundColor(0);
        try {
            webView.getSettings().setJavaScriptEnabled(true);
        } catch (Exception e2) {
        }
        this.d = System.currentTimeMillis();
        new b(this, webView).start();
        return webView;
    }

    public void DisplayAd() {
        DisplayAd(this.h);
    }

    public void DisplayAd(int i2) {
        if (this.a.getSharedPreferences("ShowAdFlag", 3).getBoolean("show_ad_flag", true)) {
            try {
                this.h = 5;
                this.g = new RelativeLayout(this.a);
                this.g.setLayoutParams(new LinearLayout.LayoutParams(-2, 0));
                this.j = a();
                try {
                    this.j.addJavascriptInterface(new SDKUtils(this.a, this.mHandler, this.g, this.b), "SDKUtils");
                } catch (Exception e2) {
                }
                WebView webView = this.j;
                WebView.enablePlatformNotifications();
                this.j.setWebViewClient(new c(this, null));
                this.j.getSettings().setCacheMode(2);
                this.g.addView(this.j);
                this.b.addView(this.g);
            } catch (Exception e3) {
            }
        }
    }
}
