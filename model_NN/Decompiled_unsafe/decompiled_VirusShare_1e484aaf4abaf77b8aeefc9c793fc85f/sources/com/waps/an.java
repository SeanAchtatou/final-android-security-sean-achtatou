package com.waps;

import android.content.DialogInterface;
import android.widget.Toast;

class an implements DialogInterface.OnClickListener {
    final /* synthetic */ OffersWebView a;

    an(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Toast.makeText(this.a, (CharSequence) OffersWebView.z.get("prepare_to_download"), 0).show();
        this.a.h.delete();
        this.a.a.execute(this.a.s);
        if (this.a.r != null && "true".equals(this.a.r)) {
            this.a.finish();
        }
    }
}
