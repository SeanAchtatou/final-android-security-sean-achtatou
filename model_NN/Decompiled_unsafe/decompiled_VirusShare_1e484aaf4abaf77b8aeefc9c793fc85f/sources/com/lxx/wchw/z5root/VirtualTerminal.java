package com.lxx.wchw.z5root;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class VirtualTerminal {
    final Object ReadLock = new Object();
    final Object WriteLock = new Object();
    ByteArrayOutputStream errbuffer = new ByteArrayOutputStream();
    InputReader errreader = new InputReader(this.process.getErrorStream(), this.errbuffer);
    ByteArrayOutputStream inpbuffer = new ByteArrayOutputStream();
    InputReader inpreader = new InputReader(this.process.getInputStream(), this.inpbuffer);
    Process process = Runtime.getRuntime().exec("su");
    DataOutputStream toProcess = new DataOutputStream(this.process.getOutputStream());

    public VirtualTerminal() throws IOException, InterruptedException {
        Thread.sleep(10);
        this.inpreader.start();
        this.errreader.start();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007e, code lost:
        if (r3.contains(":RET=0") == false) goto L_0x009d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0080, code lost:
        android.util.Log.i("oclf success", r3);
        r6 = new com.lxx.wchw.z5root.VirtualTerminal.VTCommandResult(r10, 0, r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        android.util.Log.i("oclf error", r1);
        r6 = new com.lxx.wchw.z5root.VirtualTerminal.VTCommandResult(r10, 1, r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return r6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.lxx.wchw.z5root.VirtualTerminal.VTCommandResult runCommand(java.lang.String r11) throws java.lang.Exception {
        /*
            r10 = this;
            r9 = 1
            r8 = 0
            java.lang.String r5 = ":RET="
            java.lang.String r5 = "oclf"
            android.util.Log.i(r5, r11)
            java.lang.Object r5 = r10.WriteLock
            monitor-enter(r5)
            java.io.ByteArrayOutputStream r6 = r10.inpbuffer     // Catch:{ all -> 0x0092 }
            r6.reset()     // Catch:{ all -> 0x0092 }
            java.io.ByteArrayOutputStream r6 = r10.errbuffer     // Catch:{ all -> 0x0092 }
            r6.reset()     // Catch:{ all -> 0x0092 }
            monitor-exit(r5)     // Catch:{ all -> 0x0092 }
            java.io.DataOutputStream r5 = r10.toProcess
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = java.lang.String.valueOf(r11)
            r6.<init>(r7)
            java.lang.String r7 = "\necho :RET=$?\n"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r5.writeBytes(r6)
            java.io.DataOutputStream r5 = r10.toProcess
            r5.flush()
        L_0x0034:
            java.lang.Object r5 = r10.ReadLock
            monitor-enter(r5)
            java.lang.Object r6 = r10.WriteLock     // Catch:{ all -> 0x009a }
            monitor-enter(r6)     // Catch:{ all -> 0x009a }
            java.io.ByteArrayOutputStream r7 = r10.inpbuffer     // Catch:{ all -> 0x0097 }
            byte[] r4 = r7.toByteArray()     // Catch:{ all -> 0x0097 }
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x0097 }
            r3.<init>(r4)     // Catch:{ all -> 0x0097 }
            java.lang.String r7 = ":RET="
            boolean r7 = r3.contains(r7)     // Catch:{ all -> 0x0097 }
            if (r7 == 0) goto L_0x0095
            r0 = r8
        L_0x004e:
            monitor-exit(r6)     // Catch:{ all -> 0x0097 }
            if (r0 == 0) goto L_0x0056
            java.lang.Object r6 = r10.ReadLock     // Catch:{ all -> 0x009a }
            r6.wait()     // Catch:{ all -> 0x009a }
        L_0x0056:
            monitor-exit(r5)     // Catch:{ all -> 0x009a }
            java.lang.Object r5 = r10.WriteLock
            monitor-enter(r5)
            java.io.ByteArrayOutputStream r6 = r10.inpbuffer     // Catch:{ all -> 0x00b1 }
            byte[] r4 = r6.toByteArray()     // Catch:{ all -> 0x00b1 }
            java.io.ByteArrayOutputStream r6 = r10.errbuffer     // Catch:{ all -> 0x00b1 }
            byte[] r2 = r6.toByteArray()     // Catch:{ all -> 0x00b1 }
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x00b1 }
            r3.<init>(r4)     // Catch:{ all -> 0x00b1 }
            java.lang.String r1 = new java.lang.String     // Catch:{ all -> 0x00b1 }
            r1.<init>(r2)     // Catch:{ all -> 0x00b1 }
            java.lang.String r6 = ":RET="
            boolean r6 = r3.contains(r6)     // Catch:{ all -> 0x00b1 }
            if (r6 == 0) goto L_0x00af
            java.lang.String r6 = ":RET=0"
            boolean r6 = r3.contains(r6)     // Catch:{ all -> 0x00b1 }
            if (r6 == 0) goto L_0x009d
            java.lang.String r6 = "oclf success"
            android.util.Log.i(r6, r3)     // Catch:{ all -> 0x00b1 }
            com.lxx.wchw.z5root.VirtualTerminal$VTCommandResult r6 = new com.lxx.wchw.z5root.VirtualTerminal$VTCommandResult     // Catch:{ all -> 0x00b1 }
            r7 = 0
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x00b1 }
            r6.<init>(r7, r3, r1)     // Catch:{ all -> 0x00b1 }
            monitor-exit(r5)     // Catch:{ all -> 0x00b1 }
            r5 = r6
        L_0x0091:
            return r5
        L_0x0092:
            r6 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0092 }
            throw r6
        L_0x0095:
            r0 = r9
            goto L_0x004e
        L_0x0097:
            r7 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0097 }
            throw r7     // Catch:{ all -> 0x009a }
        L_0x009a:
            r6 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x009a }
            throw r6
        L_0x009d:
            java.lang.String r6 = "oclf error"
            android.util.Log.i(r6, r1)     // Catch:{ all -> 0x00b1 }
            com.lxx.wchw.z5root.VirtualTerminal$VTCommandResult r6 = new com.lxx.wchw.z5root.VirtualTerminal$VTCommandResult     // Catch:{ all -> 0x00b1 }
            r7 = 1
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x00b1 }
            r6.<init>(r7, r3, r1)     // Catch:{ all -> 0x00b1 }
            monitor-exit(r5)     // Catch:{ all -> 0x00b1 }
            r5 = r6
            goto L_0x0091
        L_0x00af:
            monitor-exit(r5)     // Catch:{ all -> 0x00b1 }
            goto L_0x0034
        L_0x00b1:
            r6 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x00b1 }
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lxx.wchw.z5root.VirtualTerminal.runCommand(java.lang.String):com.lxx.wchw.z5root.VirtualTerminal$VTCommandResult");
    }

    public void FNF(String command) throws Exception {
        this.toProcess.writeBytes(String.valueOf(command) + "\n");
        this.toProcess.flush();
    }

    public void shutdown() {
        this.inpreader.interrupt();
        this.errreader.interrupt();
        this.process.destroy();
    }

    public class InputReader extends Thread {
        ByteArrayOutputStream baos;
        InputStream is;

        public InputReader(InputStream is2, ByteArrayOutputStream baos2) {
            this.is = is2;
            this.baos = baos2;
        }

        public void run() {
            try {
                byte[] buffer = new byte[1024];
                while (true) {
                    int read = this.is.read(buffer);
                    if (read < 0) {
                        synchronized (VirtualTerminal.this.WriteLock) {
                            this.baos.write(":RET=EOF".getBytes());
                        }
                        synchronized (VirtualTerminal.this.ReadLock) {
                            VirtualTerminal.this.ReadLock.notifyAll();
                        }
                        return;
                    } else if (read > 0) {
                        synchronized (VirtualTerminal.this.WriteLock) {
                            this.baos.write(buffer, 0, read);
                        }
                        synchronized (VirtualTerminal.this.ReadLock) {
                            VirtualTerminal.this.ReadLock.notifyAll();
                        }
                    }
                }
                while (true) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class VTCommandResult {
        public final Integer exit_value;
        public final String stderr;
        public final String stdout;

        VTCommandResult(Integer exit_value_in, String stdout_in, String stderr_in) {
            this.exit_value = exit_value_in;
            this.stdout = stdout_in;
            this.stderr = stderr_in;
        }

        VTCommandResult(VirtualTerminal virtualTerminal, Integer exit_value_in) {
            this(exit_value_in, null, null);
        }

        public boolean success() {
            return this.exit_value != null && this.exit_value.intValue() == 0;
        }
    }
}
