package com.lxx.wchw.z5root;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

public class PhaseRemove extends Activity {
    TextView infotext;
    ProgressBar progressBar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.pundo);
        this.infotext = (TextView) findViewById(R.id.infotv);
        this.progressBar = (ProgressBar) findViewById(R.id.spinner);
        new Thread() {
            public void run() {
                PhaseRemove.this.dostuff();
            }
        }.start();
    }

    public void saystuff(final String stuff) {
        runOnUiThread(new Runnable() {
            public void run() {
                PhaseRemove.this.progressBar.setVisibility(8);
                PhaseRemove.this.infotext.setVisibility(0);
                PhaseRemove.this.infotext.setText(stuff);
            }
        });
    }

    public void dostuff() {
        try {
            VirtualTerminal vt = new VirtualTerminal();
            vt.runCommand("busybox mount -o remount,rw /system");
            vt.runCommand("rm /system/bin/busybox");
            vt.runCommand("rm /system/bin/su");
            vt.runCommand("rm /system/xbin/busybox");
            vt.runCommand("rm /system/xbin/su");
            vt.runCommand("rm /system/app/SuperUser.apk");
            saystuff("Completed. Your root should now be completely removed.");
        } catch (Exception e) {
            Exception ex = e;
            ex.printStackTrace();
            saystuff("Encountered an error attempting to unroot you!\n\n\n" + ex.getLocalizedMessage());
        }
    }
}
