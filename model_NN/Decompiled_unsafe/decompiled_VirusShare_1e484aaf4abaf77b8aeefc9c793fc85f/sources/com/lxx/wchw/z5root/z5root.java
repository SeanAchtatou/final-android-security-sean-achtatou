package com.lxx.wchw.z5root;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.waps.AppConnect;
import com.waps.UpdatePointsNotifier;
import java.io.File;
import java.io.IOException;

public class z5root extends Activity implements UpdatePointsNotifier {
    public static int score = 0;
    Button gorootbutton;
    final File helloFile = new File("/data/data/com.lxx.wchw.z5root/hello");
    Button knowrootbutton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppConnect.getInstance(this);
        setContentView((int) R.layout.main);
        this.gorootbutton = (Button) findViewById(R.id.gorootbn);
        this.knowrootbutton = (Button) findViewById(R.id.knowrootbutton);
        this.gorootbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                z5root.this.dialog();
            }
        });
        this.knowrootbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AppConnect.getInstance(z5root.this).getPoints(z5root.this);
                z5root.this.startActivity(new Intent(z5root.this, cpuInfo.class));
            }
        });
    }

    /* access modifiers changed from: protected */
    public void dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("在 root之前建议您先阅读     “我先了解root@” 选项 ， 您是否继续去root？");
        builder.setTitle("温馨提示");
        builder.setPositiveButton("继续去root", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                AppConnect.getInstance(z5root.this).getPushAd();
                z5root.this.startActivity(new Intent(z5root.this, z5root_a.class));
            }
        });
        builder.setNegativeButton("返回去阅读", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AppConnect.getInstance(this).getPoints(this);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        AppConnect.getInstance(this).finalize();
        super.onDestroy();
    }

    public void getUpdatePoints(String arg0, int arg1) {
        score = arg1;
        try {
            if (score > 0 && !this.helloFile.exists()) {
                this.helloFile.createNewFile();
            }
        } catch (IOException e) {
        }
    }

    public void getUpdatePointsFailed(String arg0) {
    }
}
