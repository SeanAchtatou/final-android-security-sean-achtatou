package com.lxx.wchw.z5root;

public final class R {

    public static final class array {
        public static final int entries_color_preference = 2131034120;
        public static final int entries_controlkey_preference = 2131034122;
        public static final int entries_cursorblink_preference = 2131034114;
        public static final int entries_cursorstyle_preference = 2131034116;
        public static final int entries_fontsize_preference = 2131034118;
        public static final int entries_ime_preference = 2131034124;
        public static final int entries_statusbar_preference = 2131034112;
        public static final int entryvalues_color_preference = 2131034121;
        public static final int entryvalues_controlkey_preference = 2131034123;
        public static final int entryvalues_cursorblink_preference = 2131034115;
        public static final int entryvalues_cursorstyle_preference = 2131034117;
        public static final int entryvalues_fontsize_preference = 2131034119;
        public static final int entryvalues_ime_preference = 2131034125;
        public static final int entryvalues_statusbar_preference = 2131034113;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int bg = 2130837504;
        public static final int menu_bg = 2130837505;
        public static final int menu_hov = 2130837506;
        public static final int z4small = 2130837507;
    }

    public static final class id {
        public static final int about = 2131296270;
        public static final int copyright = 2131296264;
        public static final int detailtext = 2131296266;
        public static final int disableads = 2131296271;
        public static final int getscore_bn = 2131296258;
        public static final int gorootbn = 2131296261;
        public static final int infotv = 2131296265;
        public static final int iniAdLinearLayout = 2131296256;
        public static final int knowrootbutton = 2131296263;
        public static final int return_bn = 2131296257;
        public static final int rootbutton = 2131296268;
        public static final int scrollView = 2131296259;
        public static final int spinner = 2131296267;
        public static final int temprootbutton = 2131296269;
        public static final int textInfo = 2131296260;
        public static final int unrootbutton = 2131296262;
    }

    public static final class layout {
        public static final int hello = 2130903040;
        public static final int main = 2130903041;
        public static final int p1 = 2130903042;
        public static final int p2 = 2130903043;
        public static final int pundo = 2130903044;
        public static final int z4root = 2130903045;
        public static final int z4rootwadd = 2130903046;
    }

    public static final class menu {
        public static final int mainmenu = 2131230720;
    }

    public static final class raw {
        public static final int busybox = 2130968576;
        public static final int rageagainstthecage = 2130968577;
        public static final int su = 2130968578;
        public static final int superuser = 2130968579;
    }

    public static final class string {
        public static final int copyright = 2131099649;
        public static final int z4root = 2131099648;
    }

    public static final class style {
        public static final int Theme = 2131165184;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
