package com.lxx.wchw.z5root;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.io.File;

public class z5root_a extends Activity {
    public static final int MODE_PERMROOT = 0;
    public static final int MODE_TEMPROOT = 1;
    public static final int MODE_UNROOT = 2;
    public static final String PREFS_ADS = "AdsEnabled";
    public static final String PREFS_MODE = "rootmode";
    public static final String PREFS_NAME = "z4rootprefs";
    static final String VERSION = "1.3.0";
    TextView detailtext;
    boolean disabled = false;
    boolean forceunroot = true;
    Button rootbutton;
    Button temprootbutton;
    Button unrootbutton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSharedPreferences(PREFS_NAME, 0).getBoolean(PREFS_ADS, true)) {
            setContentView((int) R.layout.z4rootwadd);
        } else {
            setContentView((int) R.layout.z4root);
        }
        this.rootbutton = (Button) findViewById(R.id.rootbutton);
        this.unrootbutton = (Button) findViewById(R.id.unrootbutton);
        this.detailtext = (TextView) findViewById(R.id.detailtext);
        this.temprootbutton = (Button) findViewById(R.id.temprootbutton);
        this.rootbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!z5root_a.this.disabled) {
                    z5root_a.this.disabled = true;
                    Intent i = new Intent(z5root_a.this, Phase1.class);
                    SharedPreferences.Editor editor = z5root_a.this.getSharedPreferences(z5root_a.PREFS_NAME, 0).edit();
                    editor.putInt(z5root_a.PREFS_MODE, 0);
                    editor.commit();
                    z5root_a.this.startActivity(i);
                    z5root_a.this.finish();
                }
            }
        });
        this.temprootbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!z5root_a.this.disabled) {
                    z5root_a.this.disabled = true;
                    Intent i = new Intent(z5root_a.this, Phase1.class);
                    SharedPreferences.Editor editor = z5root_a.this.getSharedPreferences(z5root_a.PREFS_NAME, 0).edit();
                    editor.putInt(z5root_a.PREFS_MODE, 1);
                    editor.commit();
                    z5root_a.this.startActivity(i);
                    z5root_a.this.finish();
                }
            }
        });
        this.unrootbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i;
                if (!z5root_a.this.disabled) {
                    z5root_a.this.disabled = true;
                    if (z5root_a.this.forceunroot) {
                        i = new Intent(z5root_a.this, Phase1.class);
                        SharedPreferences.Editor editor = z5root_a.this.getSharedPreferences(z5root_a.PREFS_NAME, 0).edit();
                        editor.putInt(z5root_a.PREFS_MODE, 2);
                        editor.commit();
                    } else {
                        i = new Intent(z5root_a.this, PhaseRemove.class);
                    }
                    z5root_a.this.startActivity(i);
                    z5root_a.this.finish();
                }
            }
        });
        new Thread() {
            public void run() {
                z5root_a.this.dostuff();
            }
        }.start();
    }

    public void dostuff() {
        try {
            VirtualTerminal vt = new VirtualTerminal();
            if (vt.runCommand("id").success()) {
                this.forceunroot = false;
                runOnUiThread(new Runnable() {
                    public void run() {
                        z5root_a.this.unrootbutton.setVisibility(0);
                        z5root_a.this.temprootbutton.setVisibility(8);
                        z5root_a.this.rootbutton.setText("重新 root");
                        z5root_a.this.detailtext.setText("Your device is already rooted. You can remove the root using the Un-root button, which will delete all of the files installed to root your device. You can also re-root your device if your root is malfunctioning.");
                    }
                });
            }
            vt.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.forceunroot) {
            Log.i("exists", "/system/bin/su " + new File("/system/bin/su").exists());
            Log.i("exists", "/system/xbin/su " + new File("/system/xbin/su").exists());
            if (new File("/system/bin/su").exists() || new File("/system/xbin/su").exists()) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        z5root_a.this.unrootbutton.setVisibility(0);
                        z5root_a.this.detailtext.setText(((Object) z5root_a.this.detailtext.getText()) + " Your device already has a 'su' binary, and so the force unroot option has been activated for you.");
                    }
                });
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        if (!getSharedPreferences(PREFS_NAME, 0).getBoolean(PREFS_ADS, true)) {
            menu.getItem(1).setTitle("Enable Ads");
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean z;
        switch (item.getItemId()) {
            case R.id.about:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("z4root Version 1.3.0\nMade by RyanZA").setCancelable(true);
                AlertDialog alert = builder.create();
                alert.setOwnerActivity(this);
                alert.show();
                return true;
            case R.id.disableads:
                SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                boolean AdsEnabled = settings.getBoolean(PREFS_ADS, true);
                SharedPreferences.Editor editor = settings.edit();
                if (AdsEnabled) {
                    z = false;
                } else {
                    z = true;
                }
                editor.putBoolean(PREFS_ADS, z);
                editor.commit();
                finish();
                startActivity(new Intent(getApplicationContext(), z5root.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
