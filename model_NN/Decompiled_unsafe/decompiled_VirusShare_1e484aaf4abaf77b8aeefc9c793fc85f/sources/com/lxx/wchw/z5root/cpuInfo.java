package com.lxx.wchw.z5root;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.waps.AppConnect;

public class cpuInfo extends Activity {
    private static StringBuffer buffer = null;
    private Button getscore_bn = null;
    private String result = null;
    private Button return_bn = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.hello);
        TextView textView = (TextView) findViewById(R.id.textInfo);
        buffer = new StringBuffer();
        this.getscore_bn = (Button) findViewById(R.id.getscore_bn);
        this.getscore_bn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Log.i("fetch_process_info", "start.-------------------------- . . . ");
                AppConnect.getInstance(cpuInfo.this).showOffers(cpuInfo.this);
            }
        });
        this.return_bn = (Button) findViewById(R.id.return_bn);
        this.return_bn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                cpuInfo.this.finish();
            }
        });
    }
}
