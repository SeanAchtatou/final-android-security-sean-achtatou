package com.lxx.wchw.z5root;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.widget.TextView;
import jackpal.androidterm.Exec;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class Phase2 extends Activity {
    int MODE;
    TextView detailtext;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.MODE = getSharedPreferences(z5root_a.PREFS_NAME, 0).getInt(z5root_a.PREFS_MODE, 0);
        setContentView((int) R.layout.p2);
        this.detailtext = (TextView) findViewById(R.id.detailtext);
        new Thread() {
            public void run() {
                if (Phase2.this.MODE == 2) {
                    Phase2.this.dounroot();
                } else if (Phase2.this.MODE == 1) {
                    Phase2.this.dotemproot();
                } else {
                    Phase2.this.dopermroot();
                }
            }
        }.start();
    }

    public void saystuff(final String stuff) {
        runOnUiThread(new Runnable() {
            public void run() {
                Phase2.this.detailtext.setText(stuff);
            }
        });
    }

    public void dounroot() {
        final PowerManager.WakeLock wl = ((PowerManager) getSystemService("power")).newWakeLock(805306394, "z4root");
        wl.acquire();
        Log.i("AAA", "Starting");
        int[] processId = new int[1];
        FileDescriptor fd = Exec.createSubprocess("/system/bin/sh", "-", null, processId);
        Log.i("AAA", "Got processid: " + processId[0]);
        FileOutputStream out = new FileOutputStream(fd);
        final FileInputStream in = new FileInputStream(fd);
        new Thread() {
            public void run() {
                byte[] mBuffer = new byte[4096];
                int read = 0;
                while (read >= 0) {
                    try {
                        read = in.read(mBuffer);
                        Log.i("AAA", new String(mBuffer, 0, read));
                    } catch (Exception e) {
                    }
                }
                wl.release();
            }
        }.start();
        try {
            write(out, "id");
            try {
                SaveIncludedZippedFileIntoFilesFolder(R.raw.busybox, "busybox", getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            write(out, "chmod 777 " + getFilesDir() + "/busybox");
            write(out, getFilesDir() + "/busybox mount -o remount,rw /system");
            write(out, getFilesDir() + "/busybox rm /system/bin/su");
            write(out, getFilesDir() + "/busybox rm /system/xbin/su");
            write(out, getFilesDir() + "/busybox rm /system/bin/busybox");
            write(out, getFilesDir() + "/busybox rm /system/xbin/busybox");
            write(out, getFilesDir() + "/busybox rm /system/app/SuperUser.apk");
            write(out, "echo \"reboot now!\"");
            saystuff("Rebooting...");
            Thread.sleep(3000);
            write(out, "sync\nsync");
            write(out, "reboot");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void dotemproot() {
        final PowerManager.WakeLock wl = ((PowerManager) getSystemService("power")).newWakeLock(805306394, "z4root");
        wl.acquire();
        Log.i("AAA", "Starting");
        int[] processId = new int[1];
        FileDescriptor fd = Exec.createSubprocess("/system/bin/sh", "-", null, processId);
        Log.i("AAA", "Got processid: " + processId[0]);
        FileOutputStream out = new FileOutputStream(fd);
        final FileInputStream in = new FileInputStream(fd);
        new Thread() {
            public void run() {
                byte[] mBuffer = new byte[4096];
                int read = 0;
                while (true) {
                    if (read < 0) {
                        break;
                    }
                    try {
                        read = in.read(mBuffer);
                        String str = new String(mBuffer, 0, read);
                        Log.i("AAA", str);
                        if (str.contains("finished checked")) {
                            Phase2.this.saystuff("Temporary root applied! You are now rooted until your next reboot.");
                            break;
                        }
                    } catch (Exception e) {
                    }
                }
                wl.release();
            }
        }.start();
        try {
            write(out, "id");
            try {
                SaveIncludedZippedFileIntoFilesFolder(R.raw.busybox, "busybox", getApplicationContext());
                SaveIncludedZippedFileIntoFilesFolder(R.raw.su, "su", getApplicationContext());
                SaveIncludedZippedFileIntoFilesFolder(R.raw.superuser, "SuperUser.apk", getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            write(out, "chmod 777 " + getFilesDir() + "/busybox");
            write(out, getFilesDir() + "/busybox killall rageagainstthecage");
            write(out, getFilesDir() + "/busybox killall rageagainstthecage");
            write(out, getFilesDir() + "/busybox rm " + getFilesDir() + "/temproot.ext");
            write(out, getFilesDir() + "/busybox rm -rf " + getFilesDir() + "/bin");
            write(out, getFilesDir() + "/busybox cp -rp /system/bin " + getFilesDir());
            write(out, getFilesDir() + "/busybox dd if=/dev/zero of=" + getFilesDir() + "/temproot.ext bs=1M count=15");
            write(out, getFilesDir() + "/busybox mknod /dev/loop9 b 7 9");
            write(out, getFilesDir() + "/busybox losetup /dev/loop9 " + getFilesDir() + "/temproot.ext");
            write(out, getFilesDir() + "/busybox mkfs.ext2 /dev/loop9");
            write(out, getFilesDir() + "/busybox mount -t ext2 /dev/loop9 /system/bin");
            write(out, getFilesDir() + "/busybox cp -rp " + getFilesDir() + "/bin/* /system/bin/");
            write(out, getFilesDir() + "/busybox cp " + getFilesDir() + "/su /vendor/bin");
            write(out, getFilesDir() + "/busybox cp " + getFilesDir() + "/su /system/bin");
            write(out, getFilesDir() + "/busybox cp " + getFilesDir() + "/busybox /system/bin");
            write(out, getFilesDir() + "/busybox chown 0 /system/bin/su");
            write(out, getFilesDir() + "/busybox chown 0 /system/bin/busybox");
            write(out, getFilesDir() + "/busybox chmod 4755 /system/bin/su");
            write(out, getFilesDir() + "/busybox chmod 755 /system/bin/busybox");
            write(out, "pm install " + getFilesDir() + "/SuperUser.apk");
            write(out, "checkvar=checked");
            write(out, "echo finished $checkvar");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void dopermroot() {
        final PowerManager.WakeLock wl = ((PowerManager) getSystemService("power")).newWakeLock(805306394, "z4root");
        wl.acquire();
        Log.i("AAA", "Starting");
        int[] processId = new int[1];
        FileDescriptor fd = Exec.createSubprocess("/system/bin/sh", "-", null, processId);
        Log.i("AAA", "Got processid: " + processId[0]);
        FileOutputStream out = new FileOutputStream(fd);
        final FileInputStream in = new FileInputStream(fd);
        new Thread() {
            public void run() {
                byte[] mBuffer = new byte[4096];
                int read = 0;
                while (read >= 0) {
                    try {
                        read = in.read(mBuffer);
                        Log.i("AAA", new String(mBuffer, 0, read));
                    } catch (Exception e) {
                    }
                }
                wl.release();
            }
        }.start();
        try {
            out.write("id\n".getBytes());
            out.flush();
            try {
                SaveIncludedZippedFileIntoFilesFolder(R.raw.busybox, "busybox", getApplicationContext());
                SaveIncludedZippedFileIntoFilesFolder(R.raw.su, "su", getApplicationContext());
                SaveIncludedZippedFileIntoFilesFolder(R.raw.superuser, "SuperUser.apk", getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.write(("chmod 777 " + getFilesDir() + "/busybox\n").getBytes());
            out.flush();
            out.write((getFilesDir() + "/busybox mount -o remount,rw /system\n").getBytes());
            out.flush();
            out.write((getFilesDir() + "/busybox cp " + getFilesDir() + "/su /system/bin/\n").getBytes());
            out.flush();
            out.write((getFilesDir() + "/busybox cp " + getFilesDir() + "/SuperUser.apk /system/app\n").getBytes());
            out.flush();
            out.write((getFilesDir() + "/busybox cp " + getFilesDir() + "/busybox /system/bin/\n").getBytes());
            out.flush();
            out.write("chown root.root /system/bin/busybox\nchmod 755 /system/bin/busybox\n".getBytes());
            out.flush();
            out.write("chown root.root /system/bin/su\n".getBytes());
            out.flush();
            out.write((getFilesDir() + "/busybox chmod 6755 /system/bin/su\n").getBytes());
            out.flush();
            out.write("chown root.root /system/app/SuperUser.apk\nchmod 755 /system/app/SuperUser.apk\n".getBytes());
            out.flush();
            out.write(("rm " + getFilesDir() + "/busybox\n").getBytes());
            out.flush();
            out.write(("rm " + getFilesDir() + "/su\n").getBytes());
            out.flush();
            out.write(("rm " + getFilesDir() + "/SuperUser.apk\n").getBytes());
            out.flush();
            out.write(("rm " + getFilesDir() + "/rageagainstthecage\n").getBytes());
            out.flush();
            saystuff("Rebooting...");
            out.write("echo \"reboot now!\"\n".getBytes());
            out.flush();
            Thread.sleep(3000);
            out.write("sync\nsync\n".getBytes());
            out.flush();
            out.write("reboot\n".getBytes());
            out.flush();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void SaveIncludedZippedFileIntoFilesFolder(int resourceid, String filename, Context ApplicationContext) throws Exception {
        InputStream is = ApplicationContext.getResources().openRawResource(resourceid);
        FileOutputStream fos = ApplicationContext.openFileOutput(filename, 1);
        GZIPInputStream gzis = new GZIPInputStream(is);
        byte[] bytebuf = new byte[1024];
        while (true) {
            int read = gzis.read(bytebuf);
            if (read < 0) {
                gzis.close();
                fos.getChannel().force(true);
                fos.flush();
                fos.close();
                return;
            }
            fos.write(bytebuf, 0, read);
        }
    }

    public void write(FileOutputStream out, String command) throws IOException {
        out.write((String.valueOf(command) + "\n").getBytes());
        out.flush();
    }
}
