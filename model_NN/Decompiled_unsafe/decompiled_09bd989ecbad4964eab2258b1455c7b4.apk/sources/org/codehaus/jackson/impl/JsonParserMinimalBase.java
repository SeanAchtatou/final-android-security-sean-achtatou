package org.codehaus.jackson.impl;

import android.support.v4.view.MotionEventCompat;
import java.io.IOException;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonStreamContext;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.io.NumberInput;

public abstract class JsonParserMinimalBase extends JsonParser {
    protected static final int INT_APOSTROPHE = 39;
    protected static final int INT_ASTERISK = 42;
    protected static final int INT_BACKSLASH = 92;
    protected static final int INT_COLON = 58;
    protected static final int INT_COMMA = 44;
    protected static final int INT_CR = 13;
    protected static final int INT_LBRACKET = 91;
    protected static final int INT_LCURLY = 123;
    protected static final int INT_LF = 10;
    protected static final int INT_QUOTE = 34;
    protected static final int INT_RBRACKET = 93;
    protected static final int INT_RCURLY = 125;
    protected static final int INT_SLASH = 47;
    protected static final int INT_SPACE = 32;
    protected static final int INT_TAB = 9;
    protected static final int INT_b = 98;
    protected static final int INT_f = 102;
    protected static final int INT_n = 110;
    protected static final int INT_r = 114;
    protected static final int INT_t = 116;
    protected static final int INT_u = 117;

    /* access modifiers changed from: protected */
    public abstract void _handleEOF() throws JsonParseException;

    public abstract void close() throws IOException;

    public abstract byte[] getBinaryValue(Base64Variant base64Variant) throws IOException, JsonParseException;

    public abstract String getCurrentName() throws IOException, JsonParseException;

    public abstract JsonStreamContext getParsingContext();

    public abstract String getText() throws IOException, JsonParseException;

    public abstract char[] getTextCharacters() throws IOException, JsonParseException;

    public abstract int getTextLength() throws IOException, JsonParseException;

    public abstract int getTextOffset() throws IOException, JsonParseException;

    public abstract boolean hasTextCharacters();

    public abstract boolean isClosed();

    public abstract JsonToken nextToken() throws IOException, JsonParseException;

    protected JsonParserMinimalBase() {
    }

    protected JsonParserMinimalBase(int features) {
        super(features);
    }

    public JsonParser skipChildren() throws IOException, JsonParseException {
        if (this._currToken == JsonToken.START_OBJECT || this._currToken == JsonToken.START_ARRAY) {
            int open = 1;
            while (true) {
                JsonToken t = nextToken();
                if (t != null) {
                    switch (t) {
                        case START_OBJECT:
                        case START_ARRAY:
                            open++;
                            break;
                        case END_OBJECT:
                        case END_ARRAY:
                            open--;
                            if (open != 0) {
                                break;
                            } else {
                                break;
                            }
                    }
                } else {
                    _handleEOF();
                }
            }
        }
        return this;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean getValueAsBoolean(boolean r7) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r6 = this;
            r3 = 0
            r2 = 1
            org.codehaus.jackson.JsonToken r4 = r6._currToken
            if (r4 == 0) goto L_0x0013
            int[] r4 = org.codehaus.jackson.impl.JsonParserMinimalBase.AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken
            org.codehaus.jackson.JsonToken r5 = r6._currToken
            int r5 = r5.ordinal()
            r4 = r4[r5]
            switch(r4) {
                case 5: goto L_0x0015;
                case 6: goto L_0x0014;
                case 7: goto L_0x001d;
                case 8: goto L_0x001d;
                case 9: goto L_0x001f;
                case 10: goto L_0x002e;
                default: goto L_0x0013;
            }
        L_0x0013:
            r2 = r7
        L_0x0014:
            return r2
        L_0x0015:
            int r4 = r6.getIntValue()
            if (r4 != 0) goto L_0x0014
            r2 = r3
            goto L_0x0014
        L_0x001d:
            r2 = r3
            goto L_0x0014
        L_0x001f:
            java.lang.Object r1 = r6.getEmbeddedObject()
            boolean r3 = r1 instanceof java.lang.Boolean
            if (r3 == 0) goto L_0x002e
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r2 = r1.booleanValue()
            goto L_0x0014
        L_0x002e:
            java.lang.String r3 = r6.getText()
            java.lang.String r0 = r3.trim()
            java.lang.String r3 = "true"
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0013
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.JsonParserMinimalBase.getValueAsBoolean(boolean):boolean");
    }

    public int getValueAsInt(int defaultValue) throws IOException, JsonParseException {
        if (this._currToken == null) {
            return defaultValue;
        }
        switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[this._currToken.ordinal()]) {
            case 5:
            case 11:
                return getIntValue();
            case 6:
                return 1;
            case MotionEventCompat.ACTION_HOVER_MOVE:
            case 8:
                return 0;
            case INT_TAB /*9*/:
                Object value = getEmbeddedObject();
                if (value instanceof Number) {
                    return ((Number) value).intValue();
                }
                return defaultValue;
            case INT_LF /*10*/:
                return NumberInput.parseAsInt(getText(), defaultValue);
            default:
                return defaultValue;
        }
    }

    public long getValueAsLong(long defaultValue) throws IOException, JsonParseException {
        if (this._currToken == null) {
            return defaultValue;
        }
        switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[this._currToken.ordinal()]) {
            case 5:
            case 11:
                return getLongValue();
            case 6:
                return 1;
            case MotionEventCompat.ACTION_HOVER_MOVE:
            case 8:
                return 0;
            case INT_TAB /*9*/:
                Object value = getEmbeddedObject();
                if (value instanceof Number) {
                    return ((Number) value).longValue();
                }
                return defaultValue;
            case INT_LF /*10*/:
                return NumberInput.parseAsLong(getText(), defaultValue);
            default:
                return defaultValue;
        }
    }

    public double getValueAsDouble(double defaultValue) throws IOException, JsonParseException {
        if (this._currToken == null) {
            return defaultValue;
        }
        switch (AnonymousClass1.$SwitchMap$org$codehaus$jackson$JsonToken[this._currToken.ordinal()]) {
            case 5:
            case 11:
                return getDoubleValue();
            case 6:
                return 1.0d;
            case MotionEventCompat.ACTION_HOVER_MOVE:
            case 8:
                return 0.0d;
            case INT_TAB /*9*/:
                Object value = getEmbeddedObject();
                if (value instanceof Number) {
                    return ((Number) value).doubleValue();
                }
                return defaultValue;
            case INT_LF /*10*/:
                return NumberInput.parseAsDouble(getText(), defaultValue);
            default:
                return defaultValue;
        }
    }

    /* access modifiers changed from: protected */
    public void _reportUnexpectedChar(int ch, String comment) throws JsonParseException {
        String msg = "Unexpected character (" + _getCharDesc(ch) + ")";
        if (comment != null) {
            msg = msg + ": " + comment;
        }
        _reportError(msg);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidEOF() throws JsonParseException {
        _reportInvalidEOF(" in " + this._currToken);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidEOF(String msg) throws JsonParseException {
        _reportError("Unexpected end-of-input" + msg);
    }

    /* access modifiers changed from: protected */
    public void _throwInvalidSpace(int i) throws JsonParseException {
        _reportError("Illegal character (" + _getCharDesc((char) i) + "): only regular white space (\\r, \\n, \\t) is allowed between tokens");
    }

    /* access modifiers changed from: protected */
    public void _throwUnquotedSpace(int i, String ctxtDesc) throws JsonParseException {
        if (!isEnabled(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS) || i >= INT_SPACE) {
            _reportError("Illegal unquoted character (" + _getCharDesc((char) i) + "): has to be escaped using backslash to be included in " + ctxtDesc);
        }
    }

    /* access modifiers changed from: protected */
    public char _handleUnrecognizedCharacterEscape(char ch) throws JsonProcessingException {
        if (!isEnabled(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER)) {
            _reportError("Unrecognized character escape " + _getCharDesc(ch));
        }
        return ch;
    }

    protected static final String _getCharDesc(int ch) {
        char c = (char) ch;
        if (Character.isISOControl(c)) {
            return "(CTRL-CHAR, code " + ch + ")";
        }
        if (ch > 255) {
            return "'" + c + "' (code " + ch + " / 0x" + Integer.toHexString(ch) + ")";
        }
        return "'" + c + "' (code " + ch + ")";
    }

    /* access modifiers changed from: protected */
    public final void _reportError(String msg) throws JsonParseException {
        throw _constructError(msg);
    }

    /* access modifiers changed from: protected */
    public final void _wrapError(String msg, Throwable t) throws JsonParseException {
        throw _constructError(msg, t);
    }

    /* access modifiers changed from: protected */
    public final void _throwInternal() {
        throw new RuntimeException("Internal error: this code path should never get executed");
    }

    /* access modifiers changed from: protected */
    public final JsonParseException _constructError(String msg, Throwable t) {
        return new JsonParseException(msg, getCurrentLocation(), t);
    }
}
