package org.codehaus.jackson.map.deser;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.DeserializerFactory;
import org.codehaus.jackson.map.DeserializerProvider;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.KeyDeserializer;
import org.codehaus.jackson.map.TypeDeserializer;
import org.codehaus.jackson.map.deser.SettableBeanProperty;
import org.codehaus.jackson.map.deser.StdDeserializer;
import org.codehaus.jackson.map.deser.impl.StringCollectionDeserializer;
import org.codehaus.jackson.map.ext.OptionalHandlerFactory;
import org.codehaus.jackson.map.introspect.Annotated;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.AnnotatedConstructor;
import org.codehaus.jackson.map.introspect.AnnotatedMember;
import org.codehaus.jackson.map.introspect.AnnotatedMethod;
import org.codehaus.jackson.map.introspect.AnnotatedParameter;
import org.codehaus.jackson.map.introspect.BasicBeanDescription;
import org.codehaus.jackson.map.jsontype.NamedType;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.map.type.ArrayType;
import org.codehaus.jackson.map.type.CollectionType;
import org.codehaus.jackson.map.type.MapType;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.map.util.ClassUtil;
import org.codehaus.jackson.type.JavaType;

public abstract class BasicDeserializerFactory extends DeserializerFactory {
    static final JavaType TYPE_STRING = TypeFactory.type(String.class);
    protected static final HashMap<JavaType, JsonDeserializer<Object>> _arrayDeserializers = ArrayDeserializers.getAll();
    static final HashMap<String, Class<? extends Collection>> _collectionFallbacks = new HashMap<>();
    static final HashMap<String, Class<? extends Map>> _mapFallbacks = new HashMap<>();
    static final HashMap<JavaType, JsonDeserializer<Object>> _simpleDeserializers = StdDeserializers.constructAll();
    protected OptionalHandlerFactory optionalHandlers = OptionalHandlerFactory.instance;

    /* access modifiers changed from: protected */
    public abstract JsonDeserializer<?> _findCustomArrayDeserializer(ArrayType arrayType, DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, BeanProperty beanProperty, TypeDeserializer typeDeserializer, JsonDeserializer<?> jsonDeserializer) throws JsonMappingException;

    /* access modifiers changed from: protected */
    public abstract JsonDeserializer<?> _findCustomCollectionDeserializer(CollectionType collectionType, DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty, TypeDeserializer typeDeserializer, JsonDeserializer<?> jsonDeserializer) throws JsonMappingException;

    /* access modifiers changed from: protected */
    public abstract JsonDeserializer<?> _findCustomEnumDeserializer(Class<?> cls, DeserializationConfig deserializationConfig, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) throws JsonMappingException;

    /* access modifiers changed from: protected */
    public abstract JsonDeserializer<?> _findCustomMapDeserializer(MapType mapType, DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty, KeyDeserializer keyDeserializer, TypeDeserializer typeDeserializer, JsonDeserializer<?> jsonDeserializer) throws JsonMappingException;

    /* access modifiers changed from: protected */
    public abstract JsonDeserializer<?> _findCustomTreeNodeDeserializer(Class<? extends JsonNode> cls, DeserializationConfig deserializationConfig, BeanProperty beanProperty) throws JsonMappingException;

    public abstract DeserializerFactory withConfig(DeserializerFactory.Config config);

    static {
        _mapFallbacks.put(Map.class.getName(), LinkedHashMap.class);
        _mapFallbacks.put(ConcurrentMap.class.getName(), ConcurrentHashMap.class);
        _mapFallbacks.put(SortedMap.class.getName(), TreeMap.class);
        _mapFallbacks.put("java.util.NavigableMap", TreeMap.class);
        try {
            Class<?> key = Class.forName("java.util.ConcurrentNavigableMap");
            _mapFallbacks.put(key.getName(), Class.forName("java.util.ConcurrentSkipListMap"));
        } catch (ClassNotFoundException e) {
        }
        _collectionFallbacks.put(Collection.class.getName(), ArrayList.class);
        _collectionFallbacks.put(List.class.getName(), ArrayList.class);
        _collectionFallbacks.put(Set.class.getName(), HashSet.class);
        _collectionFallbacks.put(SortedSet.class.getName(), TreeSet.class);
        _collectionFallbacks.put(Queue.class.getName(), LinkedList.class);
        _collectionFallbacks.put("java.util.Deque", LinkedList.class);
        _collectionFallbacks.put("java.util.NavigableSet", TreeSet.class);
    }

    protected BasicDeserializerFactory() {
    }

    public JsonDeserializer<?> createArrayDeserializer(DeserializationConfig config, DeserializerProvider p, ArrayType type, BeanProperty property) throws JsonMappingException {
        JavaType elemType = type.getContentType();
        JsonDeserializer<Object> contentDeser = (JsonDeserializer) elemType.getValueHandler();
        if (contentDeser == null) {
            JsonDeserializer<?> deser = _arrayDeserializers.get(elemType);
            if (deser != null) {
                JsonDeserializer<?> custom = _findCustomArrayDeserializer(type, config, p, property, null, null);
                return custom != null ? custom : deser;
            } else if (elemType.isPrimitive()) {
                throw new IllegalArgumentException("Internal error: primitive type (" + type + ") passed, no array deserializer found");
            }
        }
        TypeDeserializer elemTypeDeser = (TypeDeserializer) elemType.getTypeHandler();
        if (elemTypeDeser == null) {
            elemTypeDeser = findTypeDeserializer(config, elemType, property);
        }
        JsonDeserializer<?> custom2 = _findCustomArrayDeserializer(type, config, p, property, elemTypeDeser, contentDeser);
        if (custom2 != null) {
            return custom2;
        }
        if (contentDeser == null) {
            contentDeser = p.findValueDeserializer(config, elemType, property);
        }
        return new ArrayDeserializer(type, contentDeser, elemTypeDeser);
    }

    public JsonDeserializer<?> createCollectionDeserializer(DeserializationConfig config, DeserializerProvider p, CollectionType type, BeanProperty property) throws JsonMappingException {
        Class<?> collectionClass = type.getRawClass();
        BasicBeanDescription beanDesc = (BasicBeanDescription) config.introspectClassAnnotations(collectionClass);
        JsonDeserializer<Object> deser = findDeserializerFromAnnotation(config, beanDesc.getClassInfo(), property);
        if (deser != null) {
            return deser;
        }
        CollectionType type2 = (CollectionType) modifyTypeByAnnotation(config, beanDesc.getClassInfo(), type, null);
        JavaType contentType = type2.getContentType();
        JsonDeserializer<Object> contentDeser = (JsonDeserializer) contentType.getValueHandler();
        TypeDeserializer contentTypeDeser = (TypeDeserializer) contentType.getTypeHandler();
        if (contentTypeDeser == null) {
            contentTypeDeser = findTypeDeserializer(config, contentType, property);
        }
        JsonDeserializer<Object> custom = _findCustomCollectionDeserializer(type2, config, p, beanDesc, property, contentTypeDeser, contentDeser);
        if (custom != null) {
            return custom;
        }
        if (contentDeser == null) {
            if (EnumSet.class.isAssignableFrom(collectionClass)) {
                return new EnumSetDeserializer(constructEnumResolver(contentType.getRawClass(), config));
            }
            contentDeser = p.findValueDeserializer(config, contentType, property);
        }
        if (type2.isInterface() || type2.isAbstract()) {
            Class<? extends Collection> fallback = _collectionFallbacks.get(collectionClass.getName());
            if (fallback == null) {
                throw new IllegalArgumentException("Can not find a deserializer for non-concrete Collection type " + type2);
            }
            collectionClass = fallback;
        }
        Constructor<Collection<Object>> ctor = ClassUtil.findConstructor(collectionClass, config.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS));
        if (contentType.getRawClass() == String.class) {
            return new StringCollectionDeserializer(type2, contentDeser, ctor);
        }
        return new CollectionDeserializer(type2, contentDeser, contentTypeDeser, ctor);
    }

    public JsonDeserializer<?> createMapDeserializer(DeserializationConfig config, DeserializerProvider p, MapType type, BeanProperty property) throws JsonMappingException {
        Class<?> mapClass = type.getRawClass();
        BasicBeanDescription beanDesc = (BasicBeanDescription) config.introspectForCreation(type);
        JsonDeserializer<Object> deser = findDeserializerFromAnnotation(config, beanDesc.getClassInfo(), property);
        if (deser != null) {
            return deser;
        }
        MapType type2 = (MapType) modifyTypeByAnnotation(config, beanDesc.getClassInfo(), type, null);
        JavaType keyType = type2.getKeyType();
        JavaType contentType = type2.getContentType();
        JsonDeserializer<Object> contentDeser = (JsonDeserializer) contentType.getValueHandler();
        KeyDeserializer keyDes = (KeyDeserializer) keyType.getValueHandler();
        if (keyDes == null) {
            if (TYPE_STRING.equals(keyType)) {
                keyDes = null;
            } else {
                keyDes = p.findKeyDeserializer(config, keyType, property);
            }
        }
        TypeDeserializer contentTypeDeser = (TypeDeserializer) contentType.getTypeHandler();
        if (contentTypeDeser == null) {
            contentTypeDeser = findTypeDeserializer(config, contentType, property);
        }
        JsonDeserializer<Object> custom = _findCustomMapDeserializer(type2, config, p, beanDesc, property, keyDes, contentTypeDeser, contentDeser);
        if (custom != null) {
            return custom;
        }
        if (contentDeser == null) {
            contentDeser = p.findValueDeserializer(config, contentType, property);
        }
        if (EnumMap.class.isAssignableFrom(mapClass)) {
            Class<?> kt = keyType.getRawClass();
            if (kt != null && kt.isEnum()) {
                return new EnumMapDeserializer(constructEnumResolver(kt, config), contentDeser);
            }
            throw new IllegalArgumentException("Can not construct EnumMap; generic (key) type not available");
        }
        if (type2.isInterface() || type2.isAbstract()) {
            Class<?> fallback = _mapFallbacks.get(mapClass.getName());
            if (fallback == null) {
                throw new IllegalArgumentException("Can not find a deserializer for non-concrete Map type " + type2);
            }
            type2 = (MapType) type2.forcedNarrowBy(fallback);
            beanDesc = (BasicBeanDescription) config.introspectForCreation(type2);
        }
        boolean fixAccess = config.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS);
        Constructor<?> findDefaultConstructor = beanDesc.findDefaultConstructor();
        if (findDefaultConstructor != null && fixAccess) {
            ClassUtil.checkAndFixAccess(findDefaultConstructor);
        }
        MapDeserializer md = new MapDeserializer(type2, findDefaultConstructor, keyDes, contentDeser, contentTypeDeser);
        md.setIgnorableProperties(config.getAnnotationIntrospector().findPropertiesToIgnore(beanDesc.getClassInfo()));
        md.setCreators(findMapCreators(config, beanDesc));
        return md;
    }

    public JsonDeserializer<?> createEnumDeserializer(DeserializationConfig config, DeserializerProvider p, JavaType type, BeanProperty property) throws JsonMappingException {
        BasicBeanDescription beanDesc = (BasicBeanDescription) config.introspectForCreation(type);
        JsonDeserializer<?> des = findDeserializerFromAnnotation(config, beanDesc.getClassInfo(), property);
        if (des != null) {
            return des;
        }
        Class<?> enumClass = type.getRawClass();
        JsonDeserializer<?> custom = _findCustomEnumDeserializer(enumClass, config, beanDesc, property);
        if (custom != null) {
            return custom;
        }
        for (AnnotatedMethod factory : beanDesc.getFactoryMethods()) {
            if (config.getAnnotationIntrospector().hasCreatorAnnotation(factory)) {
                if (factory.getParameterCount() == 1 && factory.getRawType().isAssignableFrom(enumClass)) {
                    return EnumDeserializer.deserializerForCreator(config, enumClass, factory);
                }
                throw new IllegalArgumentException("Unsuitable method (" + factory + ") decorated with @JsonCreator (for Enum type " + enumClass.getName() + ")");
            }
        }
        return new EnumDeserializer(constructEnumResolver(enumClass, config));
    }

    public JsonDeserializer<?> createTreeDeserializer(DeserializationConfig config, DeserializerProvider p, JavaType nodeType, BeanProperty property) throws JsonMappingException {
        Class<?> rawClass = nodeType.getRawClass();
        JsonDeserializer<?> custom = _findCustomTreeNodeDeserializer(rawClass, config, property);
        return custom != null ? custom : JsonNodeDeserializer.getDeserializer(rawClass);
    }

    public JsonDeserializer<Object> createBeanDeserializer(DeserializationConfig config, DeserializerProvider p, JavaType type, BeanProperty property) throws JsonMappingException {
        JsonDeserializer<Object> deser = _simpleDeserializers.get(type);
        if (deser != null) {
            return deser;
        }
        if (AtomicReference.class.isAssignableFrom(type.getRawClass())) {
            return new StdDeserializer.AtomicReferenceDeserializer(type, property);
        }
        JsonDeserializer<Object> d = this.optionalHandlers.findDeserializer(type, config, p);
        if (d != null) {
            return d;
        }
        return null;
    }

    public TypeDeserializer findTypeDeserializer(DeserializationConfig config, JavaType baseType, BeanProperty property) {
        AnnotatedClass ac = ((BasicBeanDescription) config.introspectClassAnnotations(baseType.getRawClass())).getClassInfo();
        AnnotationIntrospector ai = config.getAnnotationIntrospector();
        TypeResolverBuilder<?> b = ai.findTypeResolver(ac, baseType);
        Collection<NamedType> subtypes = null;
        if (b == null) {
            b = config.getDefaultTyper(baseType);
            if (b == null) {
                return null;
            }
        } else {
            subtypes = config.getSubtypeResolver().collectAndResolveSubtypes(ac, config, ai);
        }
        return b.buildTypeDeserializer(baseType, subtypes, property);
    }

    public TypeDeserializer findPropertyTypeDeserializer(DeserializationConfig config, JavaType baseType, AnnotatedMember annotated, BeanProperty property) {
        AnnotationIntrospector ai = config.getAnnotationIntrospector();
        TypeResolverBuilder<?> b = ai.findPropertyTypeResolver(annotated, baseType);
        if (b == null) {
            return findTypeDeserializer(config, baseType, property);
        }
        return b.buildTypeDeserializer(baseType, config.getSubtypeResolver().collectAndResolveSubtypes(annotated, config, ai), property);
    }

    public TypeDeserializer findPropertyContentTypeDeserializer(DeserializationConfig config, JavaType containerType, AnnotatedMember propertyEntity, BeanProperty property) {
        AnnotationIntrospector ai = config.getAnnotationIntrospector();
        TypeResolverBuilder<?> b = ai.findPropertyContentTypeResolver(propertyEntity, containerType);
        JavaType contentType = containerType.getContentType();
        if (b == null) {
            return findTypeDeserializer(config, contentType, property);
        }
        return b.buildTypeDeserializer(contentType, config.getSubtypeResolver().collectAndResolveSubtypes(propertyEntity, config, ai), property);
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> findDeserializerFromAnnotation(DeserializationConfig config, Annotated a, BeanProperty property) {
        Object deserDef = config.getAnnotationIntrospector().findDeserializer(a, property);
        if (deserDef != null) {
            return _constructDeserializer(config, deserDef);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public JsonDeserializer<Object> _constructDeserializer(DeserializationConfig config, Object deserDef) {
        if (deserDef instanceof JsonDeserializer) {
            return (JsonDeserializer) deserDef;
        }
        if (!(deserDef instanceof Class)) {
            throw new IllegalStateException("AnnotationIntrospector returned deserializer definition of type " + deserDef.getClass().getName() + "; expected type JsonDeserializer or Class<JsonDeserializer> instead");
        }
        Class<?> cls = (Class) deserDef;
        if (JsonDeserializer.class.isAssignableFrom(cls)) {
            return (JsonDeserializer) ClassUtil.createInstance(cls, config.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS));
        }
        throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<JsonDeserializer>");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected <T extends org.codehaus.jackson.type.JavaType> T modifyTypeByAnnotation(org.codehaus.jackson.map.DeserializationConfig r11, org.codehaus.jackson.map.introspect.Annotated r12, T r13, java.lang.String r14) throws org.codehaus.jackson.map.JsonMappingException {
        /*
            r10 = this;
            r9 = 0
            org.codehaus.jackson.map.AnnotationIntrospector r3 = r11.getAnnotationIntrospector()
            java.lang.Class r5 = r3.findDeserializationType(r12, r13, r14)
            if (r5 == 0) goto L_0x000f
            org.codehaus.jackson.type.JavaType r13 = r13.narrowBy(r5)     // Catch:{ IllegalArgumentException -> 0x0042 }
        L_0x000f:
            boolean r6 = r13.isContainerType()
            if (r6 == 0) goto L_0x009c
            org.codehaus.jackson.type.JavaType r6 = r13.getKeyType()
            java.lang.Class r4 = r3.findDeserializationKeyType(r12, r6, r14)
            if (r4 == 0) goto L_0x008e
            boolean r6 = r13 instanceof org.codehaus.jackson.map.type.MapType
            if (r6 != 0) goto L_0x0086
            org.codehaus.jackson.map.JsonMappingException r6 = new org.codehaus.jackson.map.JsonMappingException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Illegal key-type annotation: type "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r13)
            java.lang.String r8 = " is not a Map type"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.<init>(r7)
            throw r6
        L_0x0042:
            r2 = move-exception
            org.codehaus.jackson.map.JsonMappingException r6 = new org.codehaus.jackson.map.JsonMappingException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Failed to narrow type "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r13)
            java.lang.String r8 = " with concrete-type annotation (value "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r5.getName()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "), method '"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r12.getName()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "': "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r2.getMessage()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.<init>(r7, r9, r2)
            throw r6
        L_0x0086:
            r0 = r13
            org.codehaus.jackson.map.type.MapType r0 = (org.codehaus.jackson.map.type.MapType) r0     // Catch:{ IllegalArgumentException -> 0x009d }
            r6 = r0
            org.codehaus.jackson.type.JavaType r13 = r6.narrowKey(r4)     // Catch:{ IllegalArgumentException -> 0x009d }
        L_0x008e:
            org.codehaus.jackson.type.JavaType r6 = r13.getContentType()
            java.lang.Class r1 = r3.findDeserializationContentType(r12, r6, r14)
            if (r1 == 0) goto L_0x009c
            org.codehaus.jackson.type.JavaType r13 = r13.narrowContentsBy(r1)     // Catch:{ IllegalArgumentException -> 0x00d3 }
        L_0x009c:
            return r13
        L_0x009d:
            r2 = move-exception
            org.codehaus.jackson.map.JsonMappingException r6 = new org.codehaus.jackson.map.JsonMappingException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Failed to narrow key type "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r13)
            java.lang.String r8 = " with key-type annotation ("
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r4.getName()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "): "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r2.getMessage()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.<init>(r7, r9, r2)
            throw r6
        L_0x00d3:
            r2 = move-exception
            org.codehaus.jackson.map.JsonMappingException r6 = new org.codehaus.jackson.map.JsonMappingException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Failed to narrow content type "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r13)
            java.lang.String r8 = " with content-type annotation ("
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r1.getName()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "): "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r2.getMessage()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.<init>(r7, r9, r2)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.deser.BasicDeserializerFactory.modifyTypeByAnnotation(org.codehaus.jackson.map.DeserializationConfig, org.codehaus.jackson.map.introspect.Annotated, org.codehaus.jackson.type.JavaType, java.lang.String):org.codehaus.jackson.type.JavaType");
    }

    /* access modifiers changed from: protected */
    public JavaType resolveType(DeserializationConfig config, BasicBeanDescription beanDesc, JavaType type, AnnotatedMember member, BeanProperty property) {
        TypeDeserializer valueTypeDeser;
        TypeDeserializer contentTypeDeser;
        Class<? extends KeyDeserializer> kdClass;
        if (type.isContainerType()) {
            AnnotationIntrospector intr = config.getAnnotationIntrospector();
            boolean canForceAccess = config.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS);
            JavaType keyType = type.getKeyType();
            if (!(keyType == null || (kdClass = intr.findKeyDeserializer(member)) == null || kdClass == KeyDeserializer.None.class)) {
                keyType.setValueHandler((KeyDeserializer) ClassUtil.createInstance(kdClass, canForceAccess));
            }
            Class<? extends JsonDeserializer<?>> cdClass = intr.findContentDeserializer(member);
            if (!(cdClass == null || cdClass == JsonDeserializer.None.class)) {
                type.getContentType().setValueHandler((JsonDeserializer) ClassUtil.createInstance(cdClass, canForceAccess));
            }
            if ((member instanceof AnnotatedMember) && (contentTypeDeser = findPropertyContentTypeDeserializer(config, type, member, property)) != null) {
                type = type.withContentTypeHandler(contentTypeDeser);
            }
        }
        if (member instanceof AnnotatedMember) {
            valueTypeDeser = findPropertyTypeDeserializer(config, type, member, property);
        } else {
            valueTypeDeser = findTypeDeserializer(config, type, null);
        }
        if (valueTypeDeser != null) {
            return type.withTypeHandler(valueTypeDeser);
        }
        return type;
    }

    /* access modifiers changed from: protected */
    public EnumResolver<?> constructEnumResolver(Class<?> enumClass, DeserializationConfig config) {
        if (config.isEnabled(DeserializationConfig.Feature.READ_ENUMS_USING_TO_STRING)) {
            return EnumResolver.constructUnsafeUsingToString(enumClass);
        }
        return EnumResolver.constructUnsafe(enumClass, config.getAnnotationIntrospector());
    }

    /* access modifiers changed from: protected */
    public CreatorContainer findMapCreators(DeserializationConfig config, BasicBeanDescription beanDesc) throws JsonMappingException {
        Class<?> mapClass = beanDesc.getBeanClass();
        AnnotationIntrospector intr = config.getAnnotationIntrospector();
        CreatorContainer creators = new CreatorContainer(mapClass, config.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS));
        for (AnnotatedConstructor ctor : beanDesc.getConstructors()) {
            int argCount = ctor.getParameterCount();
            if (argCount >= 1 && intr.hasCreatorAnnotation(ctor)) {
                SettableBeanProperty[] properties = new SettableBeanProperty[argCount];
                int nameCount = 0;
                for (int i = 0; i < argCount; i++) {
                    AnnotatedParameter param = ctor.getParameter(i);
                    String name = param == null ? null : intr.findPropertyNameForParam(param);
                    if (name == null || name.length() == 0) {
                        throw new IllegalArgumentException("Parameter #" + i + " of constructor " + ctor + " has no property name annotation: must have for @JsonCreator for a Map type");
                    }
                    nameCount++;
                    properties[i] = constructCreatorProperty(config, beanDesc, name, i, param);
                }
                creators.addPropertyConstructor(ctor, properties);
            }
        }
        for (AnnotatedMethod factory : beanDesc.getFactoryMethods()) {
            int argCount2 = factory.getParameterCount();
            if (argCount2 >= 1 && intr.hasCreatorAnnotation(factory)) {
                SettableBeanProperty[] properties2 = new SettableBeanProperty[argCount2];
                int nameCount2 = 0;
                for (int i2 = 0; i2 < argCount2; i2++) {
                    AnnotatedParameter param2 = factory.getParameter(i2);
                    String name2 = param2 == null ? null : intr.findPropertyNameForParam(param2);
                    if (name2 == null || name2.length() == 0) {
                        throw new IllegalArgumentException("Parameter #" + i2 + " of factory method " + factory + " has no property name annotation: must have for @JsonCreator for a Map type");
                    }
                    nameCount2++;
                    properties2[i2] = constructCreatorProperty(config, beanDesc, name2, i2, param2);
                }
                creators.addPropertyFactory(factory, properties2);
            }
        }
        return creators;
    }

    /* access modifiers changed from: protected */
    public SettableBeanProperty constructCreatorProperty(DeserializationConfig config, BasicBeanDescription beanDesc, String name, int index, AnnotatedParameter param) throws JsonMappingException {
        JavaType t0 = TypeFactory.type(param.getParameterType(), beanDesc.bindingsForBeanType());
        BeanProperty.Std property = new BeanProperty.Std(name, t0, beanDesc.getClassAnnotations(), param);
        JavaType type = resolveType(config, beanDesc, t0, param, property);
        if (type != t0) {
            property = property.withType(type);
        }
        JsonDeserializer<Object> deser = findDeserializerFromAnnotation(config, param, property);
        JavaType type2 = modifyTypeByAnnotation(config, param, type, name);
        SettableBeanProperty prop = new SettableBeanProperty.CreatorProperty(name, type2, findTypeDeserializer(config, type2, property), beanDesc.getClassAnnotations(), param, index);
        if (deser != null) {
            prop.setValueDeserializer(deser);
        }
        return prop;
    }
}
