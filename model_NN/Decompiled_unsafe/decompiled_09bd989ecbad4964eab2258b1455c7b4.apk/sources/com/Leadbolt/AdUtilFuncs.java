package com.Leadbolt;

import android.support.v4.app.FragmentTransaction;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AdUtilFuncs {
    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), FragmentTransaction.TRANSIT_EXIT_MASK);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        AdLog.printStackTrace(AdController.LB_LOG, e);
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                AdLog.printStackTrace(AdController.LB_LOG, e2);
                try {
                    is.close();
                } catch (IOException e3) {
                    AdLog.printStackTrace(AdController.LB_LOG, e3);
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    AdLog.printStackTrace(AdController.LB_LOG, e4);
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }
}
