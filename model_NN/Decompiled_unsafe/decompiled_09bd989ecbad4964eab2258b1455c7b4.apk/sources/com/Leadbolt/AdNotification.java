package com.Leadbolt;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AdNotification extends BroadcastReceiver {
    NotificationManager nm;

    public void onReceive(Context context, Intent intent) {
        AdWakeLock.acquire(context);
        try {
            String sectionid = intent.getExtras().getString("sectionid");
            AdLog.i(AdController.LB_LOG, "Alarm triggered with sectionid - " + sectionid);
            if (!sectionid.equals("0")) {
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                }
                new AdController(context, sectionid).loadNotificationOnRequest("Alarm");
            }
        } catch (Exception e2) {
            AdLog.printStackTrace(AdController.LB_LOG, e2);
            AdLog.d(AdController.LB_LOG, "Exception AdNotification.class - " + e2.getMessage());
        } finally {
            AdWakeLock.release();
        }
    }
}
