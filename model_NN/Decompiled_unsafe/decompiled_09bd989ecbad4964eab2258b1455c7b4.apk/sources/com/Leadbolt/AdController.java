package com.Leadbolt;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.provider.Browser;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.TimerTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

public class AdController {
    public static final String LB_LOG = "LBAdController";
    public static final String SDK_LEVEL = "04";
    public static final String SDK_VERSION = "3";
    private final int LB_MAX_POLL;
    private final int LB_SET_MANUAL_AFTER;
    private final int MAX_APP_ICONS;
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public boolean adDestroyed;
    private String adDisplayInterval;
    /* access modifiers changed from: private */
    public boolean adLoaded;
    /* access modifiers changed from: private */
    public OfferPolling adPolling;
    private int additionalDockingMargin;
    private AlarmManager am;
    private String appId;
    private boolean asynchTask;
    private Button backBtn;
    private Button closeBtn;
    /* access modifiers changed from: private */
    public boolean completed;
    private Context context;
    private String domain;
    private View footer;
    private TextView footerTitle;
    private Button fwdBtn;
    private Button homeBtn;
    private boolean homeLoaded;
    private boolean initialized;
    private RelativeLayout layout;
    private boolean linkClicked;
    /* access modifiers changed from: private */
    public AdListener listener;
    private boolean loadAd;
    /* access modifiers changed from: private */
    public boolean loadIcon;
    /* access modifiers changed from: private */
    public Runnable loadProgress;
    private String loadUrl;
    private boolean loading;
    /* access modifiers changed from: private */
    public ProgressDialog loadingDialog;
    private RelativeLayout.LayoutParams lpC;
    private WebView mainView;
    private ViewGroup mainViewParent;
    private View mask;
    private ViewGroup.MarginLayoutParams mlpC;
    private boolean nativeOpen;
    private String notificationLaunchType;
    private boolean onAdLoaded;
    private boolean onRequest;
    private boolean onTimer;
    private PendingIntent pendingAlarmIntent;
    private Button pollBtn;
    /* access modifiers changed from: private */
    public int pollCount;
    /* access modifiers changed from: private */
    public int pollManual;
    /* access modifiers changed from: private */
    public int pollMax;
    /* access modifiers changed from: private */
    public Handler pollingHandler;
    private boolean pollingInitialized;
    /* access modifiers changed from: private */
    public Handler progressHandler;
    /* access modifiers changed from: private */
    public int progressInterval;
    private Button refreshBtn;
    private LBRequest req;
    private boolean requestInProgress;
    /* access modifiers changed from: private */
    public JSONObject results;
    private int sHeight;
    private int sWidth;
    /* access modifiers changed from: private */
    public String sectionid;
    private String subid;
    private TextView title;
    private TelephonyManager tm;
    private List<NameValuePair> tokens;
    private View toolbar;
    private boolean useLocation;
    /* access modifiers changed from: private */
    public boolean useNotification;
    private AdWebView webview;

    public AdController(Activity act, String sid) {
        this(act, sid, new RelativeLayout(act));
    }

    public AdController(Activity act, String sid, WebView w) {
        this.LB_MAX_POLL = 500;
        this.LB_SET_MANUAL_AFTER = 10;
        this.MAX_APP_ICONS = 5;
        this.requestInProgress = false;
        this.completed = false;
        this.homeLoaded = false;
        this.linkClicked = false;
        this.pollingInitialized = false;
        this.useLocation = false;
        this.nativeOpen = false;
        this.initialized = false;
        this.useNotification = false;
        this.additionalDockingMargin = 0;
        this.asynchTask = false;
        this.loadAd = true;
        this.onRequest = false;
        this.onTimer = false;
        this.listener = null;
        this.progressInterval = 0;
        this.adLoaded = false;
        this.adDestroyed = false;
        this.pollCount = 0;
        this.pollMax = 0;
        this.pollManual = 0;
        this.loadIcon = false;
        this.onAdLoaded = false;
        this.activity = act;
        this.sectionid = sid;
        this.mainView = w;
        this.layout = new RelativeLayout(this.activity);
    }

    public AdController(Context ctx, String sid) {
        this.LB_MAX_POLL = 500;
        this.LB_SET_MANUAL_AFTER = 10;
        this.MAX_APP_ICONS = 5;
        this.requestInProgress = false;
        this.completed = false;
        this.homeLoaded = false;
        this.linkClicked = false;
        this.pollingInitialized = false;
        this.useLocation = false;
        this.nativeOpen = false;
        this.initialized = false;
        this.useNotification = false;
        this.additionalDockingMargin = 0;
        this.asynchTask = false;
        this.loadAd = true;
        this.onRequest = false;
        this.onTimer = false;
        this.listener = null;
        this.progressInterval = 0;
        this.adLoaded = false;
        this.adDestroyed = false;
        this.pollCount = 0;
        this.pollMax = 0;
        this.pollManual = 0;
        this.loadIcon = false;
        this.onAdLoaded = false;
        this.context = ctx;
        this.sectionid = sid;
    }

    public AdController(Activity act, String sid, AdListener lt) {
        this(act, sid, new RelativeLayout(act));
        this.listener = lt;
    }

    public AdController(Activity act, String sid, RelativeLayout ly) {
        this.LB_MAX_POLL = 500;
        this.LB_SET_MANUAL_AFTER = 10;
        this.MAX_APP_ICONS = 5;
        this.requestInProgress = false;
        this.completed = false;
        this.homeLoaded = false;
        this.linkClicked = false;
        this.pollingInitialized = false;
        this.useLocation = false;
        this.nativeOpen = false;
        this.initialized = false;
        this.useNotification = false;
        this.additionalDockingMargin = 0;
        this.asynchTask = false;
        this.loadAd = true;
        this.onRequest = false;
        this.onTimer = false;
        this.listener = null;
        this.progressInterval = 0;
        this.adLoaded = false;
        this.adDestroyed = false;
        this.pollCount = 0;
        this.pollMax = 0;
        this.pollManual = 0;
        this.loadIcon = false;
        this.onAdLoaded = false;
        this.activity = act;
        this.sectionid = sid;
        this.layout = ly == null ? new RelativeLayout(act) : ly;
        this.mainView = null;
    }

    public void setUseLocation(boolean uL) {
        this.useLocation = uL;
        AdLog.i(LB_LOG, "setUseLocation: " + uL);
    }

    public void setLayout(RelativeLayout ly) {
        this.layout = ly;
    }

    public void setAdditionalDockingMargin(int newOffset) {
        this.additionalDockingMargin = newOffset;
        AdLog.i(LB_LOG, "setAdditionalDockingMargin: " + newOffset);
    }

    public void setAsynchTask(boolean asynch) {
        this.asynchTask = asynch;
    }

    public void setSubId(String sbid) {
        this.subid = sbid;
    }

    public void setTokens(List<NameValuePair> tks) {
        this.tokens = tks;
    }

    public void setOnProgressInterval(int pI) {
        this.progressInterval = pI;
    }

    public void destroyAd() {
        AdLog.i(LB_LOG, "destroyAd called");
        this.adDestroyed = true;
        closeUnlocker();
    }

    public void hideAd() {
        SharedPreferences.Editor editor = this.activity.getSharedPreferences("Preference", 2).edit();
        editor.putString("SD_ADSTATUS_" + this.sectionid, "hidden");
        editor.commit();
    }

    public void showAd() {
        SharedPreferences.Editor editor = this.activity.getSharedPreferences("Preference", 2).edit();
        editor.putString("SD_ADSTATUS_" + this.sectionid, "default");
        editor.commit();
    }

    public String adShowStatus() {
        return this.activity.getSharedPreferences("Preference", 2).getString("SD_ADSTATUS_" + this.sectionid, "default");
    }

    private int getAdVisibility() {
        if (this.webview != null) {
            return this.webview.getVisibility();
        }
        return 4;
    }

    public void setHomeLoaded(boolean hL) {
        this.homeLoaded = hL;
    }

    public void onLinkClicked() {
        linkClicked();
    }

    public void setLoading(boolean l) {
        this.loading = l;
    }

    private void initialize() {
        if (!this.loadAd || !adShowStatus().equals("hidden")) {
            AdLog.i(LB_LOG, "initializing...");
            if (this.activity != null) {
                if (this.webview == null) {
                    this.webview = new AdWebView(this.activity, this, this.nativeOpen, this.listener);
                }
                try {
                    NetworkInfo netInfo = ((ConnectivityManager) this.activity.getSystemService("connectivity")).getActiveNetworkInfo();
                    if (netInfo != null) {
                        Boolean con = new Boolean(netInfo.isConnected());
                        boolean makeRequest = true;
                        try {
                            String displayinterval = this.activity.getSharedPreferences("Preference", 2).getString("SD_" + this.sectionid, "0");
                            if (!displayinterval.equals("0")) {
                                int curTimestamp = (int) (Calendar.getInstance().getTimeInMillis() / 1000);
                                if (displayinterval.equals("-1") || curTimestamp < new Integer(displayinterval).intValue()) {
                                    makeRequest = false;
                                }
                            }
                        } catch (Exception e) {
                            AdLog.printStackTrace(LB_LOG, e);
                        }
                        if (con.booleanValue() && makeRequest) {
                            if (Build.VERSION.SDK_INT > 8 || this.asynchTask) {
                                AdLog.d(LB_LOG, "Request will not be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                                if (this.asynchTask) {
                                    AdLog.d(LB_LOG, "AsynchTask variable set");
                                }
                                this.req = new LBRequest(this, null);
                                this.req.execute("");
                                return;
                            }
                            AdLog.d(LB_LOG, "Request to be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                            makeLBRequest();
                            if (this.useNotification) {
                                setNotification();
                            } else if (this.loadIcon) {
                                displayIcon();
                            }
                        }
                    } else {
                        AdLog.e(LB_LOG, "No Internet connection detected. No Ads loaded");
                        if (this.listener != null) {
                            try {
                                AdLog.i(LB_LOG, "onAdFailed triggered");
                                this.listener.onAdFailed();
                                this.adLoaded = true;
                            } catch (Exception e2) {
                                AdLog.i(LB_LOG, "Error while calling onAdFailed");
                                AdLog.printStackTrace(LB_LOG, e2);
                            }
                        }
                    }
                } catch (Exception e3) {
                    AdLog.printStackTrace(LB_LOG, e3);
                    AdLog.e(LB_LOG, "Error Message No wifi - " + e3.getMessage());
                    AdLog.e(LB_LOG, "No WIFI, 3G or Edge connection detected");
                    if (this.listener != null) {
                        try {
                            AdLog.i(LB_LOG, "onAdFailed triggered");
                            this.listener.onAdFailed();
                            this.adLoaded = true;
                        } catch (Exception ex) {
                            AdLog.i(LB_LOG, "Error while calling onAdFailed");
                            AdLog.printStackTrace(LB_LOG, ex);
                        }
                    }
                }
            } else if (((ConnectivityManager) this.context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
                if (Build.VERSION.SDK_INT > 8 || this.asynchTask) {
                    if (this.loadIcon) {
                        AdLog.d(LB_LOG, "loadIcon Request will not be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                    } else {
                        AdLog.d(LB_LOG, "Notification Request will not be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                    }
                    if (this.asynchTask) {
                        AdLog.d(LB_LOG, "AsynchTask variable set");
                    }
                    new LBRequest(this, null).execute("");
                    return;
                }
                if (this.loadIcon) {
                    AdLog.d(LB_LOG, "loadIcon Request will be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                } else {
                    AdLog.d(LB_LOG, "Notification Request will be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                }
                makeLBRequest();
                if (this.loadIcon) {
                    displayIcon();
                } else {
                    setNotification();
                }
            } else if (!this.loadIcon) {
                setAlarmFromCookie();
            }
        } else {
            AdLog.d(LB_LOG, "Ad set to hide, will not show");
            if (this.listener != null) {
                try {
                    AdLog.i(LB_LOG, "onAdHidden triggered");
                    this.listener.onAdHidden();
                } catch (Exception e4) {
                    AdLog.i(LB_LOG, "Error while calling onAdHidden");
                    AdLog.printStackTrace(LB_LOG, e4);
                }
            }
        }
    }

    public void loadNotification() {
        AdLog.i(LB_LOG, "loadNotification called");
        if (!this.initialized) {
            this.useNotification = true;
            this.loadAd = false;
            this.notificationLaunchType = "App";
            this.onTimer = false;
            this.onRequest = false;
            initialize();
        }
    }

    /* access modifiers changed from: private */
    public void setNotification() {
        AdLog.d(LB_LOG, "setNotification called");
        if (this.results == null) {
            AdLog.e(LB_LOG, "Results are null - no notification will be loaded");
            return;
        }
        try {
            if (this.onRequest) {
                loadNotificationDetails();
            } else if (this.onTimer) {
                loadNotificationTimerDetails();
            } else if (this.results.getString("show").equals("1")) {
                String notificationtype = this.results.getString("notificationtype");
                if (notificationtype.equals("Immediate")) {
                    AdLog.i(LB_LOG, "Immediate notification to be fired");
                    loadNotificationOnRequest("App");
                } else if (notificationtype.equals("Recurring")) {
                    AdLog.i(LB_LOG, "Recurring notification to be created");
                    loadNotificationOnTimer();
                }
            } else {
                AdLog.i(LB_LOG, "Notification not be set for this user - DeviceId = " + this.tm.getDeviceId());
            }
        } catch (Exception e) {
            AdLog.printStackTrace(LB_LOG, e);
        }
    }

    private void loadNotificationOnTimer() {
        AdLog.i(LB_LOG, "loadNotificationOnTimer called");
        if (!this.initialized) {
            this.useNotification = true;
            this.loadAd = false;
            this.onRequest = true;
            this.onTimer = false;
            initialize();
            return;
        }
        loadNotificationTimerDetails();
    }

    private void loadNotificationTimerDetails() {
        if (this.results == null) {
            AdLog.e(LB_LOG, "Notification will not be loaded - no internet connection");
            return;
        }
        try {
            if (this.results.getString("show").equals("1")) {
                setAlarm();
            }
        } catch (Exception e) {
            AdLog.printStackTrace(LB_LOG, e);
        }
    }

    private void setAlarmFromCookie() {
        Context ctx;
        if (this.activity != null) {
            ctx = this.activity;
        } else {
            ctx = this.context;
        }
        SharedPreferences pref = ctx.getSharedPreferences("Preference", 2);
        SharedPreferences.Editor editor = pref.edit();
        long timesAttempted = (long) Integer.valueOf(pref.getString("SD_ALARM_ATTEMPTED_" + this.sectionid, "0")).intValue();
        long now = System.currentTimeMillis();
        long newStartTimeTime = now + 10000;
        if (timesAttempted > 25) {
            long alarminterval = pref.getLong("SD_ALARM_INTERVAL_" + this.sectionid, 0);
            AdLog.i(LB_LOG, "No internet, already tried 5 times, set it to timer " + alarminterval + "s");
            AdLog.i(LB_LOG, "Times attempted = " + timesAttempted);
            newStartTimeTime = now + (1000 * alarminterval);
            editor.putLong("SD_ALARM_TIME_" + this.sectionid, newStartTimeTime);
            editor.putString("SD_ALARM_ATTEMPTED_" + this.sectionid, "0");
            editor.commit();
        } else if (timesAttempted % 5 != 0 || timesAttempted <= 0) {
            AdLog.i(LB_LOG, "No internet, retry alarm in 10s");
            editor.putString("SD_ALARM_ATTEMPTED_" + this.sectionid, new StringBuilder().append(1 + timesAttempted).toString());
            editor.commit();
        } else {
            newStartTimeTime = now + 600000;
            AdLog.i(LB_LOG, "No internet, retry alarm in 10 mins");
            editor.putString("SD_ALARM_ATTEMPTED_" + this.sectionid, new StringBuilder().append(1 + timesAttempted).toString());
            editor.commit();
        }
        this.am = (AlarmManager) ctx.getSystemService("alarm");
        Intent intent = new Intent(ctx, AdNotification.class);
        intent.putExtra("sectionid", this.sectionid);
        this.pendingAlarmIntent = PendingIntent.getBroadcast(ctx, 0, intent, 134217728);
        try {
            this.am.set(0, newStartTimeTime, this.pendingAlarmIntent);
        } catch (Exception ex) {
            AdLog.printStackTrace(LB_LOG, ex);
        }
    }

    private void setAlarm() {
        Context ctx;
        int resultcookie;
        AdLog.i(LB_LOG, "setAlarm called");
        if (this.activity != null) {
            ctx = this.activity;
        } else {
            ctx = this.context;
        }
        SharedPreferences pref = ctx.getSharedPreferences("Preference", 2);
        SharedPreferences.Editor editor = pref.edit();
        if (this.results.length() > 0) {
            AdLog.i(LB_LOG, "internet connection found....initialize alarm from settings - Result Length=" + this.results.length());
            try {
                int iterationcounter = Integer.valueOf(pref.getString("SD_ITERATION_COUNTER_" + this.sectionid, "0")).intValue();
                try {
                    resultcookie = Integer.valueOf(this.results.getString("notificationcookie")).intValue();
                } catch (Exception e) {
                    resultcookie = 0;
                }
                AdLog.d(LB_LOG, "Values: ck=" + resultcookie + ", ic=" + iterationcounter + ", nLT=" + this.notificationLaunchType);
                this.am = (AlarmManager) ctx.getSystemService("alarm");
                Intent intent = new Intent(ctx.getApplicationContext(), AdNotification.class);
                intent.putExtra("sectionid", this.sectionid);
                this.pendingAlarmIntent = PendingIntent.getBroadcast(ctx.getApplicationContext(), 0, intent, 134217728);
                if (resultcookie == 1 || ((resultcookie == 0 && iterationcounter == 0) || this.notificationLaunchType.equals("Alarm"))) {
                    AdLog.i(LB_LOG, "alarm will be initialized - ck is " + resultcookie + ", ic is " + iterationcounter + ", nLT is " + this.notificationLaunchType);
                    Calendar cal = Calendar.getInstance();
                    long now = System.currentTimeMillis();
                    try {
                        int startat = Integer.parseInt(this.results.getString("notificationstart"));
                        cal.add(13, startat);
                        AdLog.d(LB_LOG, "Alarm initialized - Scheduled at " + startat + ", current time = " + now);
                        AdLog.d(LB_LOG, "----------------------------------------");
                        this.am.set(0, cal.getTimeInMillis(), this.pendingAlarmIntent);
                        editor.putLong("SD_ALARM_TIME_" + this.sectionid, now);
                        editor.putLong("SD_WAKE_TIME_" + this.sectionid, cal.getTimeInMillis());
                        editor.putLong("SD_ALARM_INTERVAL_" + this.sectionid, (long) startat);
                        editor.commit();
                    } catch (Exception e2) {
                        AdLog.e(LB_LOG, "Error caused while setting Alarm (if case): " + e2.getMessage());
                        AdLog.printStackTrace(LB_LOG, e2);
                    }
                } else {
                    long newstartat = pref.getLong("SD_WAKE_TIME_" + this.sectionid, 0);
                    Calendar cal2 = Calendar.getInstance();
                    long now2 = System.currentTimeMillis();
                    try {
                        cal2.setTimeInMillis(newstartat);
                        AdLog.d(LB_LOG, "Alarm reset - Scheduled at " + newstartat + ", current time = " + now2);
                        AdLog.d(LB_LOG, "----------------------------------------");
                        this.am.set(0, cal2.getTimeInMillis(), this.pendingAlarmIntent);
                        editor.putLong("SD_ALARM_TIME_" + this.sectionid, now2);
                        editor.putLong("SD_WAKE_TIME_" + this.sectionid, newstartat);
                        editor.commit();
                    } catch (Exception e3) {
                        AdLog.e(LB_LOG, "Error caused while setting Alarm (else case): " + e3.getMessage());
                        AdLog.printStackTrace(LB_LOG, e3);
                    }
                }
            } catch (Exception e4) {
            }
        } else {
            setAlarmFromCookie();
        }
    }

    public void loadNotificationOnRequest(String launchtype) {
        if (launchtype.equals("App") || launchtype.equals("Alarm")) {
            AdLog.i(LB_LOG, "loadNotificationOnRequest called");
            this.notificationLaunchType = launchtype;
            if (!this.initialized) {
                this.useNotification = true;
                this.loadAd = false;
                this.onRequest = true;
                this.onTimer = false;
                initialize();
                return;
            }
            loadNotificationDetails();
            return;
        }
        AdLog.e(LB_LOG, "Illegal use of loadNotificationOnRequest. LaunchType used = " + launchtype);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0276, code lost:
        if (r0.notificationLaunchType.equals(r23) != false) goto L_0x0278;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadNotificationDetails() {
        /*
            r25 = this;
            java.lang.String r21 = "LBAdController"
            java.lang.String r22 = "loadNotificationDetails called"
            com.Leadbolt.AdLog.i(r21, r22)
            r0 = r25
            org.json.JSONObject r0 = r0.results
            r21 = r0
            if (r21 != 0) goto L_0x0017
            java.lang.String r21 = "LBAdController"
            java.lang.String r22 = "Notification will not be loaded - no internet connection"
            com.Leadbolt.AdLog.e(r21, r22)
        L_0x0016:
            return
        L_0x0017:
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "show"
            java.lang.String r21 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = "1"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x0210 }
            if (r21 == 0) goto L_0x01c0
            java.lang.String r21 = "LBAdController"
            java.lang.String r22 = "notification to be fired"
            com.Leadbolt.AdLog.i(r21, r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            if (r21 == 0) goto L_0x0200
            r0 = r25
            android.app.Activity r6 = r0.activity     // Catch:{ Exception -> 0x0210 }
        L_0x003e:
            java.lang.String r21 = "notification"
            r0 = r21
            java.lang.Object r15 = r6.getSystemService(r0)     // Catch:{ Exception -> 0x0210 }
            android.app.NotificationManager r15 = (android.app.NotificationManager) r15     // Catch:{ Exception -> 0x0210 }
            r21 = 5
            r0 = r21
            int[] r10 = new int[r0]     // Catch:{ Exception -> 0x0210 }
            r10 = {17301620, 17301547, 17301516, 17301514, 17301618} // fill-array
            r9 = 0
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0206, all -> 0x020c }
            r21 = r0
            java.lang.String r22 = "notificationicon"
            int r9 = r21.getInt(r22)     // Catch:{ Exception -> 0x0206, all -> 0x020c }
            r9 = r10[r9]     // Catch:{ Exception -> 0x0210 }
        L_0x0060:
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "notificationtext"
            java.lang.String r17 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r11 = ""
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x02a6 }
            r21 = r0
            java.lang.String r22 = "notificationinstruction"
            java.lang.String r11 = r21.getString(r22)     // Catch:{ Exception -> 0x02a6 }
        L_0x007a:
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "notificationtext"
            java.lang.String r5 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "notificationdescription"
            java.lang.String r4 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x02a3 }
            r21 = r0
            java.lang.String r22 = "notificationdisplay"
            java.lang.String r18 = r21.getString(r22)     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r21 = ""
            r0 = r18
            r1 = r21
            boolean r21 = r0.equals(r1)     // Catch:{ Exception -> 0x02a3 }
            if (r21 != 0) goto L_0x0258
            java.lang.String r21 = "notificationtext"
            r0 = r18
            r1 = r21
            boolean r21 = r0.equals(r1)     // Catch:{ Exception -> 0x02a3 }
            if (r21 == 0) goto L_0x00c5
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x02a3 }
            r21 = r0
            r0 = r21
            r1 = r18
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Exception -> 0x02a3 }
            r4 = r11
        L_0x00c5:
            long r19 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0210 }
            android.content.Intent r12 = new android.content.Intent     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = "android.intent.action.VIEW"
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r22 = r0
            java.lang.String r23 = "notificationurl"
            java.lang.String r22 = r22.getString(r23)     // Catch:{ Exception -> 0x0210 }
            android.net.Uri r22 = android.net.Uri.parse(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r21
            r1 = r22
            r12.<init>(r0, r1)     // Catch:{ Exception -> 0x0210 }
            r21 = 0
            r22 = 0
            r0 = r21
            r1 = r22
            android.app.PendingIntent r3 = android.app.PendingIntent.getActivity(r6, r0, r12, r1)     // Catch:{ Exception -> 0x0210 }
            android.app.Notification r14 = new android.app.Notification     // Catch:{ Exception -> 0x0210 }
            r0 = r17
            r1 = r19
            r14.<init>(r9, r0, r1)     // Catch:{ Exception -> 0x0210 }
            int r0 = r14.flags     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            r21 = r21 | 16
            r0 = r21
            r14.flags = r0     // Catch:{ Exception -> 0x0210 }
            r14.setLatestEventInfo(r6, r5, r4, r3)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = "Preference"
            r22 = 2
            r0 = r21
            r1 = r22
            android.content.SharedPreferences r16 = r6.getSharedPreferences(r0, r1)     // Catch:{ Exception -> 0x0210 }
            android.content.SharedPreferences$Editor r8 = r16.edit()     // Catch:{ Exception -> 0x0210 }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = "SD_NOTIFICATION_ID_"
            r21.<init>(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x0210 }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x0210 }
            r22 = 0
            r0 = r16
            r1 = r21
            r2 = r22
            int r13 = r0.getInt(r1, r2)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0210 }
            java.lang.String r23 = "Stored Pref - "
            r22.<init>(r23)     // Catch:{ Exception -> 0x0210 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r13)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0210 }
            com.Leadbolt.AdLog.d(r21, r22)     // Catch:{ Exception -> 0x0210 }
            if (r13 != 0) goto L_0x0151
            r13 = 10001(0x2711, float:1.4014E-41)
        L_0x0151:
            r21 = 1001(0x3e9, float:1.403E-42)
            r0 = r21
            r15.notify(r0, r14)     // Catch:{ Exception -> 0x0210 }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a0 }
            java.lang.String r22 = "SD_NOTIFICATION_FIRED_"
            r21.<init>(r22)     // Catch:{ Exception -> 0x02a0 }
            r0 = r25
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x02a0 }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ Exception -> 0x02a0 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x02a0 }
            r0 = r21
            r1 = r19
            r8.putLong(r0, r1)     // Catch:{ Exception -> 0x02a0 }
            r8.commit()     // Catch:{ Exception -> 0x02a0 }
        L_0x0177:
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0210 }
            java.lang.String r23 = "New Notification created with ID - "
            r22.<init>(r23)     // Catch:{ Exception -> 0x0210 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r13)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0210 }
            com.Leadbolt.AdLog.d(r21, r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x029d }
            r21 = r0
            java.lang.String r22 = "notificationmultiple"
            java.lang.String r21 = r21.getString(r22)     // Catch:{ Exception -> 0x029d }
            java.lang.String r22 = "1"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x029d }
            if (r21 == 0) goto L_0x01c0
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x029d }
            java.lang.String r22 = "SD_NOTIFICATION_ID_"
            r21.<init>(r22)     // Catch:{ Exception -> 0x029d }
            r0 = r25
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x029d }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ Exception -> 0x029d }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x029d }
            int r13 = r13 + 1
            r0 = r21
            r8.putInt(r0, r13)     // Catch:{ Exception -> 0x029d }
            r8.commit()     // Catch:{ Exception -> 0x029d }
        L_0x01c0:
            r25.incrementIterationCounter()
            r0 = r25
            boolean r0 = r0.onTimer     // Catch:{ Exception -> 0x01de }
            r21 = r0
            if (r21 != 0) goto L_0x01d9
            r0 = r25
            java.lang.String r0 = r0.notificationLaunchType     // Catch:{ Exception -> 0x01de }
            r21 = r0
            java.lang.String r22 = "Alarm"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x01de }
            if (r21 == 0) goto L_0x0016
        L_0x01d9:
            r25.setAlarm()     // Catch:{ Exception -> 0x01de }
            goto L_0x0016
        L_0x01de:
            r7 = move-exception
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder
            java.lang.String r23 = "Error while setting Alarm - "
            r22.<init>(r23)
            java.lang.String r23 = r7.getMessage()
            java.lang.StringBuilder r22 = r22.append(r23)
            java.lang.String r22 = r22.toString()
            com.Leadbolt.AdLog.e(r21, r22)
            java.lang.String r21 = "LBAdController"
            r0 = r21
            com.Leadbolt.AdLog.printStackTrace(r0, r7)
            goto L_0x0016
        L_0x0200:
            r0 = r25
            android.content.Context r6 = r0.context     // Catch:{ Exception -> 0x0210 }
            goto L_0x003e
        L_0x0206:
            r7 = move-exception
            r9 = 0
            r9 = r10[r9]     // Catch:{ Exception -> 0x0210 }
            goto L_0x0060
        L_0x020c:
            r21 = move-exception
            r9 = r10[r9]     // Catch:{ Exception -> 0x0210 }
            throw r21     // Catch:{ Exception -> 0x0210 }
        L_0x0210:
            r7 = move-exception
            java.lang.String r21 = "LBAdController"
            r0 = r21
            com.Leadbolt.AdLog.printStackTrace(r0, r7)     // Catch:{ all -> 0x025e }
            r25.incrementIterationCounter()
            r0 = r25
            boolean r0 = r0.onTimer     // Catch:{ Exception -> 0x0236 }
            r21 = r0
            if (r21 != 0) goto L_0x0231
            r0 = r25
            java.lang.String r0 = r0.notificationLaunchType     // Catch:{ Exception -> 0x0236 }
            r21 = r0
            java.lang.String r22 = "Alarm"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x0236 }
            if (r21 == 0) goto L_0x0016
        L_0x0231:
            r25.setAlarm()     // Catch:{ Exception -> 0x0236 }
            goto L_0x0016
        L_0x0236:
            r7 = move-exception
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder
            java.lang.String r23 = "Error while setting Alarm - "
            r22.<init>(r23)
            java.lang.String r23 = r7.getMessage()
            java.lang.StringBuilder r22 = r22.append(r23)
            java.lang.String r22 = r22.toString()
            com.Leadbolt.AdLog.e(r21, r22)
            java.lang.String r21 = "LBAdController"
            r0 = r21
            com.Leadbolt.AdLog.printStackTrace(r0, r7)
            goto L_0x0016
        L_0x0258:
            java.lang.String r5 = "You have 1 new message"
            java.lang.String r4 = "Tap to View"
            goto L_0x00c5
        L_0x025e:
            r21 = move-exception
            r25.incrementIterationCounter()
            r0 = r25
            boolean r0 = r0.onTimer     // Catch:{ Exception -> 0x027c }
            r22 = r0
            if (r22 != 0) goto L_0x0278
            r0 = r25
            java.lang.String r0 = r0.notificationLaunchType     // Catch:{ Exception -> 0x027c }
            r22 = r0
            java.lang.String r23 = "Alarm"
            boolean r22 = r22.equals(r23)     // Catch:{ Exception -> 0x027c }
            if (r22 == 0) goto L_0x027b
        L_0x0278:
            r25.setAlarm()     // Catch:{ Exception -> 0x027c }
        L_0x027b:
            throw r21
        L_0x027c:
            r7 = move-exception
            java.lang.String r22 = "LBAdController"
            java.lang.StringBuilder r23 = new java.lang.StringBuilder
            java.lang.String r24 = "Error while setting Alarm - "
            r23.<init>(r24)
            java.lang.String r24 = r7.getMessage()
            java.lang.StringBuilder r23 = r23.append(r24)
            java.lang.String r23 = r23.toString()
            com.Leadbolt.AdLog.e(r22, r23)
            java.lang.String r22 = "LBAdController"
            r0 = r22
            com.Leadbolt.AdLog.printStackTrace(r0, r7)
            goto L_0x027b
        L_0x029d:
            r21 = move-exception
            goto L_0x01c0
        L_0x02a0:
            r21 = move-exception
            goto L_0x0177
        L_0x02a3:
            r21 = move-exception
            goto L_0x00c5
        L_0x02a6:
            r21 = move-exception
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Leadbolt.AdController.loadNotificationDetails():void");
    }

    private void incrementIterationCounter() {
        AdLog.i(LB_LOG, "increment counter called");
        SharedPreferences pref = (this.activity != null ? this.activity : this.context).getSharedPreferences("Preference", 2);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("SD_ITERATION_COUNTER_" + this.sectionid, new StringBuilder().append(Integer.valueOf(pref.getString("SD_ITERATION_COUNTER_" + this.sectionid, "0")).intValue() + 1).toString());
        editor.commit();
    }

    public void loadAd() {
        AdLog.i(LB_LOG, "loadAd called");
        this.onAdLoaded = false;
        if (!this.initialized) {
            initialize();
            this.loadAd = true;
        } else {
            displayAd();
        }
        if (this.listener != null && this.progressInterval > 0) {
            if (this.loadProgress == null) {
                this.loadProgress = new Runnable() {
                    public void run() {
                        try {
                            if (!AdController.this.adLoaded && !AdController.this.adDestroyed) {
                                AdLog.i(AdController.LB_LOG, "onAdProgress triggered");
                                AdController.this.listener.onAdProgress();
                                AdController.this.progressHandler.postDelayed(AdController.this.loadProgress, (long) (AdController.this.progressInterval * 1000));
                            }
                        } catch (Exception e) {
                            AdLog.e(AdController.LB_LOG, "error when onAdProgress triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e);
                        }
                    }
                };
            }
            if (this.progressHandler == null) {
                this.progressHandler = new Handler();
                this.progressHandler.postDelayed(this.loadProgress, (long) (this.progressInterval * 1000));
            }
        }
    }

    public void loadIcon() {
        AdLog.i(LB_LOG, "loadIcon called");
        if (!this.initialized) {
            this.loadAd = false;
            this.loadIcon = true;
            initialize();
            return;
        }
        displayIcon();
    }

    /* access modifiers changed from: private */
    public void displayIcon() {
        String adText;
        AdLog.i(LB_LOG, "displayIcon called");
        if (this.results == null) {
            AdLog.e(LB_LOG, "Results are null - no icon will be loaded");
            return;
        }
        try {
            if (this.results.get("show").equals("1") && this.context != null) {
                AdLog.i(LB_LOG, "going to display icon");
                try {
                    adText = this.results.getString("adname");
                } catch (Exception e) {
                    AdLog.printStackTrace(LB_LOG, e);
                }
                SharedPreferences pref = this.context.getSharedPreferences("Preference", 2);
                int displayCount = pref.getInt("SD_ICON_DISPLAY_" + this.sectionid, 0);
                if (displayCount < 5) {
                    AdLog.i(LB_LOG, "MAX count not passed so display icon");
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(String.valueOf(this.results.getString("adurl")) + this.sectionid));
                    Intent addIntent = new Intent();
                    addIntent.putExtra("android.intent.extra.shortcut.INTENT", intent);
                    addIntent.putExtra("android.intent.extra.shortcut.NAME", adText);
                    try {
                        Bitmap bmp = BitmapFactory.decodeStream(((HttpURLConnection) new URL(this.results.getString("adiconurl")).openConnection()).getInputStream());
                        Display display = ((WindowManager) this.context.getSystemService("window")).getDefaultDisplay();
                        DisplayMetrics metrics = new DisplayMetrics();
                        display.getMetrics(metrics);
                        bmp.setDensity((int) (160.0f * metrics.density));
                        Bitmap bmp2 = Bitmap.createScaledBitmap(bmp, 72, 72, true);
                        if (bmp2 != null) {
                            AdLog.i(LB_LOG, "bitmap not null");
                            addIntent.putExtra("android.intent.extra.shortcut.ICON", bmp2);
                        } else {
                            AdLog.i(LB_LOG, "bitmap null");
                            addIntent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this.context, 17301593));
                        }
                    } catch (Exception e2) {
                        AdLog.i(LB_LOG, "exception in getting icon");
                        AdLog.printStackTrace(LB_LOG, e2);
                        addIntent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this.context, 17301593));
                    }
                    addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
                    this.context.sendBroadcast(addIntent);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putInt("SD_ICON_DISPLAY_" + this.sectionid, displayCount + 1);
                    editor.commit();
                } else {
                    AdLog.d(LB_LOG, "DisplayCount = " + displayCount + ", MAX_APP_ICONS = " + 5);
                }
                ContentValues cv = new ContentValues();
                cv.put("title", adText);
                cv.put("url", String.valueOf(this.results.getString("adurl")) + this.sectionid);
                cv.put("bookmark", (Integer) 1);
                try {
                    this.context.getContentResolver().insert(Browser.BOOKMARKS_URI, cv);
                    AdLog.d(LB_LOG, "bookmark inserted successfully");
                } catch (Exception e3) {
                    AdLog.e(LB_LOG, "bookmark inserted error");
                }
            }
        } catch (Exception e4) {
            AdLog.printStackTrace(LB_LOG, e4);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0842, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:?, code lost:
        com.Leadbolt.AdLog.e(com.Leadbolt.AdController.LB_LOG, "Error in initializing polling - " + r14.getMessage());
        com.Leadbolt.AdLog.printStackTrace(com.Leadbolt.AdController.LB_LOG, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0864, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0865, code lost:
        closeUnlocker();
        android.util.Log.d("AdController", "JSONException - " + r14.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x089b, code lost:
        closeUnlocker();
        r1.loading = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0938, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:?, code lost:
        com.Leadbolt.AdLog.i(com.Leadbolt.AdController.LB_LOG, "Error while calling onAdFailed");
        com.Leadbolt.AdLog.printStackTrace(com.Leadbolt.AdController.LB_LOG, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:25:0x015f, B:129:0x0922] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0864 A[ExcHandler: JSONException (r14v1 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:25:0x015f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void displayAd() {
        /*
            r50 = this;
            r0 = r50
            org.json.JSONObject r0 = r0.results
            r45 = r0
            if (r45 != 0) goto L_0x0010
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "Results are null - no ad will be loaded"
            com.Leadbolt.AdLog.e(r45, r46)
        L_0x000f:
            return
        L_0x0010:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x004d }
            r45 = r0
            java.lang.String r46 = "useclickwindow"
            boolean r45 = r45.isNull(r46)     // Catch:{ Exception -> 0x004d }
            if (r45 != 0) goto L_0x006d
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x004d }
            r45 = r0
            java.lang.String r46 = "useclickwindow"
            java.lang.String r45 = r45.getString(r46)     // Catch:{ Exception -> 0x004d }
            java.lang.String r46 = "1"
            boolean r45 = r45.equals(r46)     // Catch:{ Exception -> 0x004d }
            if (r45 == 0) goto L_0x006d
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ Exception -> 0x004d }
            r45 = 0
            r0 = r45
            r1 = r50
            r1.linkClicked = r0     // Catch:{ Exception -> 0x004d }
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "Going to use click window - cancel out of here..."
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ Exception -> 0x004d }
            r50.linkClicked()     // Catch:{ Exception -> 0x004d }
            goto L_0x000f
        L_0x004d:
            r14 = move-exception
            java.lang.String r45 = "LBAdController"
            r0 = r45
            com.Leadbolt.AdLog.printStackTrace(r0, r14)
            java.lang.String r45 = "LBAdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder
            java.lang.String r47 = "Exception when using ClickWindow - "
            r46.<init>(r47)
            java.lang.String r47 = r14.getMessage()
            java.lang.StringBuilder r46 = r46.append(r47)
            java.lang.String r46 = r46.toString()
            com.Leadbolt.AdLog.e(r45, r46)
        L_0x006d:
            r0 = r50
            boolean r0 = r0.homeLoaded
            r45 = r0
            if (r45 != 0) goto L_0x000f
            r45 = 1
            r0 = r45
            r1 = r50
            r1.homeLoaded = r0
            r45 = 0
            r0 = r45
            r1 = r50
            r1.linkClicked = r0
            r45 = 1
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "show"
            java.lang.Object r45 = r45.get(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r46 = "1"
            boolean r45 = r45.equals(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            if (r45 == 0) goto L_0x090b
            r0 = r50
            boolean r0 = r0.completed     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            if (r45 != 0) goto L_0x08fe
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            com.Leadbolt.AdWebView r0 = r0.webview     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.toolbar     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.footer     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.backBtn     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.fwdBtn     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.homeBtn     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.refreshBtn     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r0 = r50
            android.widget.Button r0 = r0.pollBtn     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            if (r45 == 0) goto L_0x015f
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
            r45 = r0
            r45.removeAllViews()     // Catch:{ Exception -> 0x0952, JSONException -> 0x0864 }
        L_0x015f:
            android.view.View r45 = new android.view.View     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r0 = r45
            r1 = r50
            r1.mask = r0     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r45 = r0
            r46 = -1
            r45.setMinimumHeight(r46)     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r45 = r0
            r46 = -1
            r45.setMinimumWidth(r46)     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r45 = r0
            r46 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r45.setBackgroundColor(r46)     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r45 = r0
            java.lang.String r46 = "maskalpha"
            double r45 = r45.getDouble(r46)     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r47 = 4643176031446892544(0x406fe00000000000, double:255.0)
            double r5 = r45 * r47
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r45 = r0
            android.graphics.drawable.Drawable r45 = r45.getBackground()     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            int r0 = (int) r5     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r46 = r0
            r45.setAlpha(r46)     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r45 = r0
            com.Leadbolt.AdController$2 r46 = new com.Leadbolt.AdController$2     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r0 = r46
            r1 = r50
            r0.<init>()     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
            r45.setOnClickListener(r46)     // Catch:{ Exception -> 0x094f, JSONException -> 0x0864 }
        L_0x01c6:
            android.view.ViewGroup$MarginLayoutParams r30 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = -1
            r46 = -1
            r0 = r30
            r1 = r45
            r2 = r46
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = 0
            r46 = 0
            r47 = 0
            r48 = 0
            r0 = r30
            r1 = r45
            r2 = r46
            r3 = r47
            r4 = r48
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.widget.RelativeLayout$LayoutParams r29 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r29.<init>(r30)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "windowy"
            int r44 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r13 = "Middle"
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x094c, JSONException -> 0x0864 }
            r45 = r0
            java.lang.String r46 = "windowdockingy"
            java.lang.String r13 = r45.getString(r46)     // Catch:{ Exception -> 0x094c, JSONException -> 0x0864 }
        L_0x0209:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "titlevisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            if (r45 != 0) goto L_0x0266
            r0 = r50
            com.Leadbolt.AdWebView r0 = r0.webview     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r46 = 0
            r45.setBackgroundColor(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            if (r45 <= 0) goto L_0x0266
            java.lang.String r45 = "Middle"
            r0 = r45
            boolean r45 = r13.equals(r0)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            if (r45 != 0) goto L_0x0266
            java.lang.String r45 = "LBAdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r47 = "Additional Docking is set, adjusting banner by "
            r46.<init>(r47)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r47 = r0
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r47 = "px"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r46 = r46.toString()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r45 = "Top"
            r0 = r45
            boolean r45 = r13.equals(r0)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            if (r45 == 0) goto L_0x0882
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            int r44 = r44 + r45
        L_0x0266:
            android.view.ViewGroup$MarginLayoutParams r43 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "windowwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            java.lang.String r47 = "windowheight"
            int r46 = r46.getInt(r47)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r43
            r1 = r45
            r2 = r46
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "windowx"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = 0
            r47 = 0
            r0 = r43
            r1 = r45
            r2 = r44
            r3 = r46
            r4 = r47
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.widget.RelativeLayout$LayoutParams r42 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r42.<init>(r43)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "titlex"
            int r38 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "titley"
            int r39 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "titlewidth"
            int r37 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "titleheight"
            int r36 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.view.ViewGroup$MarginLayoutParams r32 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r32
            r1 = r37
            r2 = r36
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = 0
            r46 = 0
            r0 = r32
            r1 = r38
            r2 = r39
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.widget.RelativeLayout$LayoutParams r31 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r31.<init>(r32)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.view.View r45 = new android.view.View     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r45
            r1 = r50
            r1.toolbar = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "titlecolor"
            java.lang.String r12 = r45.getString(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r45 = ""
            r0 = r45
            boolean r45 = r12.equals(r0)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            if (r45 != 0) goto L_0x0325
            if (r12 != 0) goto L_0x0327
        L_0x0325:
            java.lang.String r12 = "#E6E6E6"
        L_0x0327:
            r0 = r50
            android.view.View r0 = r0.toolbar     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r12)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45.setBackgroundColor(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.widget.TextView r45 = new android.widget.TextView     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r45
            r1 = r50
            r1.title = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            java.lang.String r47 = "titletext"
            java.lang.String r46 = r46.getString(r47)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45.setText(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "titletextcolor"
            java.lang.String r9 = r45.getString(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r45 = ""
            r0 = r45
            boolean r45 = r9.equals(r0)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            if (r45 != 0) goto L_0x0372
            if (r9 != 0) goto L_0x0374
        L_0x0372:
            java.lang.String r9 = "#E6E6E6"
        L_0x0374:
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r9)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45.setTextColor(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "titletextheight"
            int r33 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.view.ViewGroup$MarginLayoutParams r41 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "titletextwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r41
            r1 = r45
            r2 = r33
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            int r34 = r38 + 20
            int r45 = r36 - r33
            int r45 = r45 / 2
            int r45 = r45 + r39
            int r35 = r45 + 4
            r45 = 0
            r46 = 0
            r0 = r41
            r1 = r34
            r2 = r35
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.widget.RelativeLayout$LayoutParams r40 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r40.<init>(r41)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "footerx"
            int r23 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "footery"
            int r24 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "footerheight"
            int r21 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.view.ViewGroup$MarginLayoutParams r17 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "footerwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r17
            r1 = r45
            r2 = r21
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = 0
            r46 = 0
            r0 = r17
            r1 = r23
            r2 = r24
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.widget.RelativeLayout$LayoutParams r16 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r16.<init>(r17)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.view.View r45 = new android.view.View     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r45
            r1 = r50
            r1.footer = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "footercolor"
            java.lang.String r15 = r45.getString(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r45 = ""
            r0 = r45
            boolean r45 = r15.equals(r0)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            if (r45 != 0) goto L_0x043e
            if (r15 != 0) goto L_0x0440
        L_0x043e:
            java.lang.String r15 = "#E6E6E6"
        L_0x0440:
            r0 = r50
            android.view.View r0 = r0.footer     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r15)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45.setBackgroundColor(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.widget.TextView r45 = new android.widget.TextView     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r45
            r1 = r50
            r1.footerTitle = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            java.lang.String r47 = "footertext"
            java.lang.String r46 = r46.getString(r47)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45.setText(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r46 = 1092616192(0x41200000, float:10.0)
            r45.setTextSize(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "footertextheight"
            int r22 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "footertextcolor"
            java.lang.String r20 = r45.getString(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r45 = ""
            r0 = r20
            r1 = r45
            boolean r45 = r0.equals(r1)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            if (r45 != 0) goto L_0x04a4
            if (r20 != 0) goto L_0x04a6
        L_0x04a4:
            java.lang.String r20 = "#E6E6E6"
        L_0x04a6:
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r20)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45.setTextColor(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.view.ViewGroup$MarginLayoutParams r28 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "footertextwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r28
            r1 = r45
            r2 = r22
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            int r18 = r23 + 20
            int r45 = r21 - r22
            int r45 = r45 / 2
            int r19 = r24 + r45
            r45 = 0
            r46 = 0
            r0 = r28
            r1 = r18
            r2 = r19
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.widget.RelativeLayout$LayoutParams r27 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r27.<init>(r28)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            int r45 = r36 + -5
            r46 = 0
            int r8 = java.lang.Math.max(r45, r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            int r45 = r8 / 2
            r0 = r45
            float r7 = (float) r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.widget.Button r45 = new android.widget.Button     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r45
            r1 = r50
            r1.closeBtn = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "X"
            r45.setText(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r45
            r0.setTextSize(r7)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r9)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45.setTextColor(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r46 = 0
            r47 = 0
            r48 = 0
            r49 = 0
            r45.setPadding(r46, r47, r48, r49)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r12)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45.setBackgroundColor(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            com.Leadbolt.AdController$3 r46 = new com.Leadbolt.AdController$3     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r46
            r1 = r50
            r0.<init>()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45.setOnClickListener(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.view.ViewGroup$MarginLayoutParams r45 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = 55
            r0 = r45
            r1 = r46
            r0.<init>(r1, r8)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r45
            r1 = r50
            r1.mlpC = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            int r45 = r37 + r38
            int r45 = r45 + -55
            int r10 = r45 + -5
            int r45 = r36 - r8
            int r45 = r45 / 2
            int r45 = r45 + r39
            int r11 = r45 + 2
            r0 = r50
            android.view.ViewGroup$MarginLayoutParams r0 = r0.mlpC     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r46 = 0
            r47 = 0
            r0 = r45
            r1 = r46
            r2 = r47
            r0.setMargins(r10, r11, r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.widget.RelativeLayout$LayoutParams r45 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.view.ViewGroup$MarginLayoutParams r0 = r0.mlpC     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r45
            r1 = r50
            r1.lpC = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            if (r45 == 0) goto L_0x05d4
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            android.view.ViewParent r45 = r45.getParent()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.view.ViewGroup r45 = (android.view.ViewGroup) r45     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r45
            r1 = r50
            r1.mainViewParent = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.view.ViewGroup r0 = r0.mainViewParent     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r45.addView(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
        L_0x05d4:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "maskvisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x0617
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r29
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            if (r45 == 0) goto L_0x0617
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            com.Leadbolt.AdController$4 r46 = new com.Leadbolt.AdController$4     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r46
            r1 = r50
            r0.<init>()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45.setOnTouchListener(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
        L_0x0617:
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            com.Leadbolt.AdWebView r0 = r0.webview     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r42
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "titlevisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x06a7
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.toolbar     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r31
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "titletext"
            java.lang.String r45 = r45.getString(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r46 = ""
            boolean r45 = r45.equals(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            if (r45 != 0) goto L_0x067e
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r40
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
        L_0x067e:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "showclose"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x06a7
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r50
            android.widget.RelativeLayout$LayoutParams r0 = r0.lpC     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r47 = r0
            r45.addView(r46, r47)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
        L_0x06a7:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "footervisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x06f9
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.footer     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r16
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "footertext"
            java.lang.String r45 = r45.getString(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r46 = ""
            boolean r45 = r45.equals(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            if (r45 != 0) goto L_0x06f9
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r27
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
        L_0x06f9:
            java.lang.StringBuilder r45 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r46 = r0
            java.lang.String r47 = "adurl"
            java.lang.String r46 = r46.getString(r47)     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            java.lang.String r46 = java.lang.String.valueOf(r46)     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r45.<init>(r46)     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r0 = r50
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r46 = r0
            java.lang.StringBuilder r45 = r45.append(r46)     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            java.lang.String r45 = r45.toString()     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r0 = r45
            r1 = r50
            r1.loadUrl = r0     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r0 = r50
            com.Leadbolt.AdWebView r0 = r0.webview     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            java.lang.String r0 = r0.loadUrl     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r46 = r0
            r45.setLoadingURL(r46)     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r0 = r50
            com.Leadbolt.AdWebView r0 = r0.webview     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            java.lang.String r0 = r0.loadUrl     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r46 = r0
            r45.loadUrl(r46)     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
            r45 = 1
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ Exception -> 0x089a, JSONException -> 0x0864 }
        L_0x0748:
            android.view.ViewGroup$MarginLayoutParams r25 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            int r0 = r0.sWidth     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            int r0 = r0.sHeight     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r25
            r1 = r45
            r2 = r46
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = 0
            r46 = 0
            r47 = 0
            r48 = 0
            r0 = r25
            r1 = r45
            r2 = r46
            r3 = r47
            r4 = r48
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.widget.RelativeLayout$LayoutParams r26 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r26
            r1 = r25
            r0.<init>(r1)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x0949, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0949, JSONException -> 0x0864 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r26
            r0.addContentView(r1, r2)     // Catch:{ Exception -> 0x0949, JSONException -> 0x0864 }
        L_0x0792:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            java.lang.String r46 = "pollenable"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x08cd
            r0 = r50
            boolean r0 = r0.pollingInitialized     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            if (r45 != 0) goto L_0x08cd
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            int r0 = r0.pollMax     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 > r1) goto L_0x08cd
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            int r0 = r0.pollManual     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 >= r1) goto L_0x08cd
            r45 = 1
            r0 = r45
            r1 = r50
            r1.pollingInitialized = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            com.Leadbolt.AdController$OfferPolling r45 = new com.Leadbolt.AdController$OfferPolling     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r45
            r1 = r50
            r0.<init>()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r45
            r1 = r50
            r1.adPolling = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            android.os.Handler r45 = new android.os.Handler     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45.<init>()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r45
            r1 = r50
            r1.pollingHandler = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r45 = "LBAdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            java.lang.String r47 = "Polling initialized every "
            r46.<init>(r47)     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            r47 = r0
            java.lang.String r48 = "pollinterval"
            int r47 = r47.getInt(r48)     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            java.lang.String r47 = "s"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            java.lang.String r46 = r46.toString()     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            com.Leadbolt.AdLog.d(r45, r46)     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            r0 = r50
            android.os.Handler r0 = r0.pollingHandler     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            r45 = r0
            r0 = r50
            com.Leadbolt.AdController$OfferPolling r0 = r0.adPolling     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            r46 = r0
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            r47 = r0
            java.lang.String r48 = "pollinterval"
            int r47 = r47.getInt(r48)     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            r0 = r47
            int r0 = r0 * 1000
            r47 = r0
            r0 = r47
            long r0 = (long) r0     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            r47 = r0
            r45.postDelayed(r46, r47)     // Catch:{ Exception -> 0x0842, JSONException -> 0x0864 }
            goto L_0x000f
        L_0x0842:
            r14 = move-exception
            java.lang.String r45 = "LBAdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r47 = "Error in initializing polling - "
            r46.<init>(r47)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r47 = r14.getMessage()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r46 = r46.toString()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            com.Leadbolt.AdLog.e(r45, r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r45 = "LBAdController"
            r0 = r45
            com.Leadbolt.AdLog.printStackTrace(r0, r14)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            goto L_0x000f
        L_0x0864:
            r14 = move-exception
            r50.closeUnlocker()
            java.lang.String r45 = "AdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder
            java.lang.String r47 = "JSONException - "
            r46.<init>(r47)
            java.lang.String r47 = r14.getMessage()
            java.lang.StringBuilder r46 = r46.append(r47)
            java.lang.String r46 = r46.toString()
            android.util.Log.d(r45, r46)
            goto L_0x000f
        L_0x0882:
            java.lang.String r45 = "Bottom"
            r0 = r45
            boolean r45 = r13.equals(r0)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            if (r45 == 0) goto L_0x0266
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            int r44 = r44 - r45
            if (r44 >= 0) goto L_0x0266
            r44 = 0
            goto L_0x0266
        L_0x089a:
            r14 = move-exception
            r50.closeUnlocker()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            goto L_0x0748
        L_0x08a8:
            r14 = move-exception
            r50.closeUnlocker()
            java.lang.String r45 = "LBAdController"
            r0 = r45
            com.Leadbolt.AdLog.printStackTrace(r0, r14)
            java.lang.String r45 = "AdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder
            java.lang.String r47 = "Exception - "
            r46.<init>(r47)
            java.lang.String r47 = r14.getMessage()
            java.lang.StringBuilder r46 = r46.append(r47)
            java.lang.String r46 = r46.toString()
            android.util.Log.d(r45, r46)
            goto L_0x000f
        L_0x08cd:
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            int r0 = r0.pollMax     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 <= r1) goto L_0x000f
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            r0 = r50
            int r0 = r0.pollManual     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 < r1) goto L_0x000f
            r45 = 0
            r0 = r45
            r1 = r50
            r1.pollingInitialized = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r50.showManualPoll()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            goto L_0x000f
        L_0x08fe:
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r50.closeUnlocker()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            goto L_0x000f
        L_0x090b:
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r50.closeUnlocker()     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r0 = r50
            com.Leadbolt.AdListener r0 = r0.listener     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            r45 = r0
            if (r45 == 0) goto L_0x000f
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "onAdFailed triggered"
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ Exception -> 0x0938, JSONException -> 0x0864 }
            r0 = r50
            com.Leadbolt.AdListener r0 = r0.listener     // Catch:{ Exception -> 0x0938, JSONException -> 0x0864 }
            r45 = r0
            r45.onAdFailed()     // Catch:{ Exception -> 0x0938, JSONException -> 0x0864 }
            r45 = 1
            r0 = r45
            r1 = r50
            r1.adLoaded = r0     // Catch:{ Exception -> 0x0938, JSONException -> 0x0864 }
            goto L_0x000f
        L_0x0938:
            r14 = move-exception
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "Error while calling onAdFailed"
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            java.lang.String r45 = "LBAdController"
            r0 = r45
            com.Leadbolt.AdLog.printStackTrace(r0, r14)     // Catch:{ JSONException -> 0x0864, Exception -> 0x08a8 }
            goto L_0x000f
        L_0x0949:
            r45 = move-exception
            goto L_0x0792
        L_0x094c:
            r45 = move-exception
            goto L_0x0209
        L_0x094f:
            r45 = move-exception
            goto L_0x01c6
        L_0x0952:
            r45 = move-exception
            goto L_0x015f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Leadbolt.AdController.displayAd():void");
    }

    private void linkClicked() {
        int fTitlex;
        AdLog.i(LB_LOG, "linkClicked called");
        if (!this.linkClicked) {
            AdLog.i(LB_LOG, "Loading window...");
            this.homeLoaded = false;
            this.linkClicked = true;
            if (!this.loading) {
                AdLog.i(LB_LOG, "remove the views if required first...");
                try {
                    this.layout.removeView(this.webview);
                    this.layout.removeView(this.toolbar);
                    this.layout.removeView(this.title);
                    this.layout.removeView(this.footer);
                    this.layout.removeView(this.footerTitle);
                    this.layout.removeView(this.backBtn);
                    this.layout.removeView(this.fwdBtn);
                    this.layout.removeView(this.closeBtn);
                    this.layout.removeView(this.homeBtn);
                    this.layout.removeView(this.refreshBtn);
                    if (this.pollBtn != null) {
                        this.layout.removeAllViews();
                    }
                } catch (Exception e) {
                }
                int windowx = this.results.getInt("clickwindowx");
                int windowy = this.results.getInt("clickwindowy");
                ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(this.results.getInt("clickwindowwidth"), this.results.getInt("clickwindowheight"));
                marginLayoutParams.setMargins(windowx, windowy, 0, 0);
                RelativeLayout.LayoutParams wL = new RelativeLayout.LayoutParams(marginLayoutParams);
                int toolbarwidth = this.results.getInt("clicktitlewidth");
                int toolbarheight = this.results.getInt("clicktitleheight");
                int toolbarx = this.results.getInt("clicktitlex");
                int toolbary = this.results.getInt("clicktitley");
                ViewGroup.MarginLayoutParams marginLayoutParams2 = new ViewGroup.MarginLayoutParams(toolbarwidth, toolbarheight);
                marginLayoutParams2.setMargins(toolbarx, toolbary, 0, 0);
                RelativeLayout.LayoutParams tL = new RelativeLayout.LayoutParams(marginLayoutParams2);
                try {
                    this.toolbar.invalidate();
                } catch (Exception e2) {
                }
                try {
                    this.toolbar = new View(this.activity);
                    String clr = this.results.getString("clicktitlecolor");
                    if (clr.equals("") || clr == null) {
                        clr = "#E6E6E6";
                    }
                    this.toolbar.setBackgroundColor(Color.parseColor(clr));
                    try {
                        this.title.invalidate();
                    } catch (Exception e3) {
                    }
                    this.title = new TextView(this.activity);
                    this.title.setText(this.results.getString("clicktitletext"));
                    String clk = this.results.getString("clicktitletextcolor");
                    if (clk.equals("") || clk == null) {
                        clk = "#000000";
                    }
                    this.title.setTextColor(Color.parseColor(clk));
                    ViewGroup.MarginLayoutParams marginLayoutParams3 = new ViewGroup.MarginLayoutParams(this.results.getInt("clicktitletextwidth"), toolbarheight - 2);
                    marginLayoutParams3.setMargins(toolbarx + 20, toolbary + 8, 0, 0);
                    RelativeLayout.LayoutParams tvl = new RelativeLayout.LayoutParams(marginLayoutParams3);
                    int footerx = this.results.getInt("clickfooterx");
                    int footery = this.results.getInt("clickfootery");
                    int footerheight = this.results.getInt("clickfooterheight");
                    ViewGroup.MarginLayoutParams marginLayoutParams4 = new ViewGroup.MarginLayoutParams(this.results.getInt("clickfooterwidth"), footerheight);
                    marginLayoutParams4.setMargins(footerx, footery, 0, 0);
                    RelativeLayout.LayoutParams fL = new RelativeLayout.LayoutParams(marginLayoutParams4);
                    this.footer = new View(this.activity);
                    String fClr = this.results.getString("clickfootercolor");
                    if (fClr.equals("") || fClr == null) {
                        fClr = "#E6E6E6";
                    }
                    this.footer.setBackgroundColor(Color.parseColor(fClr));
                    this.footerTitle = new TextView(this.activity);
                    this.footerTitle.setText(this.results.getString("clickfootertext"));
                    this.footerTitle.setTextSize(10.0f);
                    String fClk = this.results.getString("clickfootertextcolor");
                    if (fClk.equals("") || fClk == null) {
                        fClk = "#000000";
                    }
                    this.footerTitle.setTextColor(Color.parseColor(fClk));
                    ViewGroup.MarginLayoutParams marginLayoutParams5 = new ViewGroup.MarginLayoutParams(this.results.getInt("clickfootertextwidth"), this.results.getInt("clickfootertextheight"));
                    int fTitlex2 = footerx;
                    if (this.results.getInt("shownavigation") == 1) {
                        fTitlex = fTitlex2 + 70;
                    } else {
                        fTitlex = fTitlex2 + 20;
                    }
                    marginLayoutParams5.setMargins(fTitlex, footery + 5, 0, 0);
                    RelativeLayout.LayoutParams fvl = new RelativeLayout.LayoutParams(marginLayoutParams5);
                    int titleBtnHeight = Math.max(toolbarheight - 5, 0);
                    float titleBtnFont = (float) (titleBtnHeight / 2);
                    if (titleBtnFont > 10.0f) {
                        titleBtnFont = 10.0f;
                    }
                    int navBtnHeight = Math.max(footerheight - 5, 0);
                    float navBtnFont = (float) (navBtnHeight / 2);
                    if (navBtnFont > 10.0f) {
                        navBtnFont = 10.0f;
                    }
                    if (this.homeBtn != null) {
                        this.homeBtn.invalidate();
                    }
                    this.homeBtn = new Button(this.activity);
                    this.homeBtn.setText("Back");
                    this.homeBtn.setTextSize(navBtnFont);
                    this.homeBtn.setTextColor(Color.parseColor(clk));
                    this.homeBtn.setPadding(0, 0, 0, 0);
                    this.homeBtn.setBackgroundColor(Color.parseColor(fClr));
                    this.homeBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.loadAd();
                        }
                    });
                    if (this.closeBtn != null) {
                        this.closeBtn.invalidate();
                    }
                    this.closeBtn = new Button(this.activity);
                    this.closeBtn.setText("X");
                    this.closeBtn.setTextSize(titleBtnFont);
                    this.closeBtn.setTextColor(Color.parseColor(clk));
                    this.closeBtn.setPadding(0, 0, 0, 0);
                    this.closeBtn.setBackgroundColor(Color.parseColor(clr));
                    this.closeBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.closeUnlocker();
                        }
                    });
                    this.fwdBtn = new Button(this.activity);
                    this.fwdBtn.setText(">");
                    this.fwdBtn.setTextSize(navBtnFont);
                    this.fwdBtn.setTextColor(Color.parseColor(clk));
                    this.fwdBtn.setPadding(0, 0, 0, 0);
                    this.fwdBtn.setBackgroundColor(Color.parseColor(fClr));
                    this.fwdBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.goForward();
                        }
                    });
                    if (this.backBtn != null) {
                        this.backBtn.invalidate();
                    }
                    this.backBtn = new Button(this.activity);
                    this.backBtn.setText("<");
                    this.backBtn.setTextSize(navBtnFont);
                    this.backBtn.setTextColor(Color.parseColor(clk));
                    this.backBtn.setPadding(0, 0, 0, 0);
                    this.backBtn.setBackgroundColor(Color.parseColor(fClr));
                    this.backBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.goBack();
                        }
                    });
                    ViewGroup.MarginLayoutParams marginLayoutParams6 = new ViewGroup.MarginLayoutParams(30, navBtnHeight);
                    int btny = ((this.results.getInt("clickfooterheight") - navBtnHeight) / 2) + footery + 3;
                    marginLayoutParams6.setMargins(footerx + 5, btny, 0, 0);
                    RelativeLayout.LayoutParams bB = new RelativeLayout.LayoutParams(marginLayoutParams6);
                    marginLayoutParams6.setMargins(footerx + 5 + 30, btny, 0, 0);
                    RelativeLayout.LayoutParams fB = new RelativeLayout.LayoutParams(marginLayoutParams6);
                    this.layout.addView(this.webview, wL);
                    if (this.listener != null) {
                        try {
                            if (!this.results.get("useclickwindow").equals("1")) {
                                AdLog.i(LB_LOG, "onAdClicked triggered");
                                this.listener.onAdClicked();
                            }
                        } catch (Exception e4) {
                            AdLog.e(LB_LOG, "error when onAdClicked triggered");
                            AdLog.printStackTrace(LB_LOG, e4);
                        }
                    }
                    if (this.results.getInt("clicktitlevisible") == 1) {
                        this.layout.addView(this.toolbar, tL);
                        if (!this.results.getString("clicktitletext").equals("")) {
                            this.layout.addView(this.title, tvl);
                        }
                        if (this.results.getInt("showclose") == 1) {
                            this.mlpC = new ViewGroup.MarginLayoutParams(55, titleBtnHeight);
                            this.mlpC.setMargins(((toolbarwidth + toolbarx) - 55) - 5, ((toolbarheight - titleBtnHeight) / 2) + toolbary + 2, 0, 0);
                            this.lpC = new RelativeLayout.LayoutParams(this.mlpC);
                            this.layout.addView(this.closeBtn, this.lpC);
                        }
                    }
                    if (this.results.getInt("clickfootervisible") == 1) {
                        this.layout.addView(this.footer, fL);
                        if (!this.results.getString("clickfootertext").equals("")) {
                            this.layout.addView(this.footerTitle, fvl);
                        }
                        if (this.results.getInt("shownavigation") == 1) {
                            this.layout.addView(this.backBtn, bB);
                            this.layout.addView(this.fwdBtn, fB);
                        }
                        if (this.results.getInt("showback") == 1) {
                            ViewGroup.MarginLayoutParams marginLayoutParams7 = new ViewGroup.MarginLayoutParams(55, navBtnHeight);
                            marginLayoutParams7.setMargins(((this.results.getInt("clickfooterwidth") + footerx) - 55) - 5, ((footerheight - navBtnHeight) / 2) + footery + 2, 0, 0);
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginLayoutParams7);
                            this.layout.addView(this.homeBtn, layoutParams);
                        }
                    }
                    if (this.results.get("useclickwindow").equals("1")) {
                        try {
                            this.loadUrl = String.valueOf(this.results.getString("adurl")) + this.sectionid;
                            this.webview.setLoadingURL(this.loadUrl);
                            this.webview.loadUrl(this.loadUrl);
                            this.loading = true;
                        } catch (Exception e5) {
                            closeUnlocker();
                            this.loading = false;
                        }
                        try {
                            ViewGroup.MarginLayoutParams marginLayoutParams8 = new ViewGroup.MarginLayoutParams(this.sWidth, this.sHeight);
                            marginLayoutParams8.setMargins(0, 0, 0, 0);
                            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(marginLayoutParams8);
                            try {
                                this.activity.addContentView(this.layout, layoutParams2);
                            } catch (Exception e6) {
                            }
                        } catch (Exception e7) {
                        }
                    }
                    AdLog.i(LB_LOG, "pE = " + this.results.getInt("pollenable") + ", pI = " + this.pollingInitialized + ", pC = " + this.pollCount + ", pM = " + this.pollMax + ", pMl = " + this.pollManual);
                    boolean iP = false;
                    if (this.results.getInt("pollenable") != 1 || this.pollingInitialized || this.pollCount <= 0) {
                        if (this.pollCount > this.pollMax || this.pollCount >= this.pollManual) {
                            if (this.pollCount > this.pollMax && this.pollCount >= this.pollManual) {
                                iP = false;
                            }
                        } else {
                            iP = true;
                        }
                    } else {
                        iP = true;
                    }
                    if (iP) {
                        AdLog.i(LB_LOG, "Polling to be initialized in linkClicked");
                        this.pollingInitialized = true;
                        this.adPolling = new OfferPolling();
                        this.pollingHandler = new Handler();
                        try {
                            AdLog.d(LB_LOG, "Polling initialized every " + this.results.getInt("pollinterval") + "s");
                            this.pollingHandler.postDelayed(this.adPolling, (long) (this.results.getInt("pollinterval") * 1000));
                        } catch (Exception e8) {
                        }
                    } else {
                        AdLog.i(LB_LOG, "Manual Polling in linkClicked");
                        this.pollingInitialized = false;
                        showManualPoll();
                    }
                } catch (JSONException ex) {
                    AdLog.printStackTrace(LB_LOG, ex);
                    AdLog.e(LB_LOG, "JSON Exception - " + ex.getMessage());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void showManualPoll() {
        try {
            if (!this.results.getString("clickfootervisible").equals("1")) {
                return;
            }
        } catch (Exception e) {
        }
        this.pollingInitialized = false;
        if (this.pollingHandler != null) {
            this.pollingHandler.removeCallbacks(this.adPolling);
        }
        this.pollBtn = new Button(this.activity);
        this.pollBtn.setText("Refresh");
        int pollBtnHeight = 0;
        String clr = "#E6E6E6";
        String clk = "#000000";
        if (this.linkClicked) {
            try {
                pollBtnHeight = Math.max(this.results.getInt("clickfooterheight") - 5, 0);
                clr = this.results.getString("clickfootercolor");
                clk = this.results.getString("clicktitletextcolor");
                if (clr.equals("") || clr == null) {
                    clr = "#E6E6E6";
                }
                if (clk.equals("") || clk == null) {
                    clk = "#000000";
                }
            } catch (Exception e2) {
            }
        }
        float pollBtnFont = (float) (pollBtnHeight / 2);
        if (pollBtnFont > 10.0f) {
            pollBtnFont = 10.0f;
        }
        this.pollBtn.setPadding(0, 0, 0, 0);
        this.pollBtn.setTextSize(pollBtnFont);
        this.pollBtn.setTextColor(Color.parseColor(clk));
        this.pollBtn.setBackgroundColor(Color.parseColor(clr));
        this.pollBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AdController.this.loadingDialog = ProgressDialog.show(AdController.this.activity, "", "Checking....Please Wait!", true);
                if (AdController.this.adPolling == null) {
                    AdController.this.adPolling = new OfferPolling();
                }
                AdController.this.pollingHandler = new Handler();
                try {
                    AdLog.i(AdController.LB_LOG, "Manually Polling");
                    AdController.this.pollingHandler.post(AdController.this.adPolling);
                } catch (Exception e) {
                    AdLog.e(AdController.LB_LOG, "Error in manual polling - " + e.getMessage());
                    AdLog.printStackTrace(AdController.LB_LOG, e);
                }
            }
        });
        if (this.linkClicked) {
            try {
                int footerx = this.results.getInt("clickfooterx");
                int footery = this.results.getInt("clickfootery");
                int footerheight = this.results.getInt("clickfooterheight");
                ViewGroup.MarginLayoutParams hB = new ViewGroup.MarginLayoutParams(55, pollBtnHeight);
                hB.setMargins((this.results.getInt("clickfooterwidth") + footerx) - 120, ((footerheight - pollBtnHeight) / 2) + footery + 2, 0, 0);
                this.layout.addView(this.pollBtn, new RelativeLayout.LayoutParams(hB));
            } catch (Exception e3) {
                AdLog.e(LB_LOG, "Error (add Manual Poll btn before click): " + e3.getMessage());
                AdLog.printStackTrace(LB_LOG, e3);
            }
        }
    }

    /* access modifiers changed from: private */
    public void closeUnlocker() {
        if (getAdVisibility() == 0) {
            if (this.listener != null && !this.completed) {
                try {
                    AdLog.i(LB_LOG, "onAdClosed triggered");
                    this.listener.onAdClosed();
                } catch (Exception e) {
                    AdLog.e(LB_LOG, "error when onAdClosed triggered");
                    AdLog.printStackTrace(LB_LOG, e);
                }
            }
            this.adDestroyed = true;
            if (this.req != null) {
                this.req.cancel(true);
            }
            AdLog.i(LB_LOG, "closeUnlocker called");
            this.homeLoaded = false;
            this.linkClicked = false;
            this.pollingInitialized = false;
            if (this.pollingHandler != null) {
                this.pollingHandler.removeCallbacks(this.adPolling);
            }
            try {
                Class.forName("android.webkit.WebView").getMethod("onPause", null).invoke(this.webview, null);
            } catch (Exception e2) {
            }
            try {
                this.layout.removeAllViews();
                if (this.mainView != null) {
                    this.mainView.setOnTouchListener(null);
                }
            } catch (Exception e3) {
                AdLog.e(LB_LOG, "CloseUnlocker error - " + e3.getMessage());
            }
        }
    }

    /* access modifiers changed from: private */
    public void goBack() {
        if (this.webview.canGoBack()) {
            this.webview.goBack();
        }
    }

    /* access modifiers changed from: private */
    public void goForward() {
        if (this.webview.canGoForward()) {
            this.webview.goForward();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(9:98|99|100|101|(3:105|106|107)|108|(3:109|110|(4:114|(1:116)|117|(1:119)))|120|(3:121|122|(2:124|176)(1:175))) */
    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        com.Leadbolt.AdLog.printStackTrace(com.Leadbolt.AdController.LB_LOG, r16);
        r19 = false;
        r0.results = new org.json.JSONObject();
        r0.initialized = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:?, code lost:
        com.Leadbolt.AdLog.i(com.Leadbolt.AdController.LB_LOG, "onAdFailed triggered");
        r0.listener.onAdFailed();
        r0.adLoaded = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x04c2, code lost:
        r0.initialized = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x04cb, code lost:
        if (r0.webview != null) goto L_0x04cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x04cd, code lost:
        r0.webview.setResults(r0.results);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x04dc, code lost:
        if (r0.loadAd != false) goto L_0x04de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x04de, code lost:
        new com.Leadbolt.AdController.PollRequest(r1, null).execute(r0.results.getString("pollurl"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x04fd, code lost:
        r0.requestInProgress = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0506, code lost:
        if (r0.useNotification != false) goto L_0x0508;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0508, code lost:
        r14 = r32.edit();
        r14.putLong("SD_NOTIFICATION_REQUESTED_" + r0.sectionid, java.lang.System.currentTimeMillis());
        r14.commit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x055d, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0570, code lost:
        r0.initialized = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x0579, code lost:
        if (r0.webview != null) goto L_0x057b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:143:0x057b, code lost:
        r0.webview.setResults(r0.results);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x058a, code lost:
        if (r0.loadAd != false) goto L_0x058c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x058c, code lost:
        new com.Leadbolt.AdController.PollRequest(r1, null).execute(r0.results.getString("pollurl"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x05ab, code lost:
        r0.requestInProgress = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x05b4, code lost:
        if (r0.useNotification != false) goto L_0x05b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x05b6, code lost:
        r14 = r32.edit();
        r14.putLong("SD_NOTIFICATION_REQUESTED_" + r0.sectionid, java.lang.System.currentTimeMillis());
        r14.commit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x05d7, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x05f2, code lost:
        r13 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:?, code lost:
        com.Leadbolt.AdLog.i(com.Leadbolt.AdController.LB_LOG, "Error while calling onAdFailed");
        com.Leadbolt.AdLog.printStackTrace(com.Leadbolt.AdController.LB_LOG, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x0602, code lost:
        r0.initialized = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:0x060a, code lost:
        r0.initialized = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0479, code lost:
        r16 = move-exception;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void makeLBRequest() {
        /*
            r45 = this;
            r0 = r45
            boolean r3 = r0.requestInProgress
            if (r3 == 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            r3 = 1
            r0 = r45
            r0.requestInProgress = r3
            java.util.Random r33 = new java.util.Random
            r33.<init>()
            r3 = 151(0x97, float:2.12E-43)
            r0 = r33
            int r3 = r0.nextInt(r3)
            int r0 = r3 + 250
            r34 = r0
            r0 = r34
            long r3 = (long) r0
            java.lang.Thread.sleep(r3)     // Catch:{ Exception -> 0x0618 }
        L_0x0023:
            r0 = r45
            android.app.Activity r3 = r0.activity
            if (r3 != 0) goto L_0x02f6
            r0 = r45
            android.content.Context r2 = r0.context
        L_0x002d:
            java.lang.String r3 = "Preference"
            r4 = 2
            android.content.SharedPreferences r32 = r2.getSharedPreferences(r3, r4)
            r0 = r45
            android.app.Activity r3 = r0.activity
            if (r3 == 0) goto L_0x00d5
            android.util.DisplayMetrics r12 = new android.util.DisplayMetrics
            r12.<init>()
            r0 = r45
            android.app.Activity r3 = r0.activity
            android.view.WindowManager r3 = r3.getWindowManager()
            android.view.Display r3 = r3.getDefaultDisplay()
            r3.getMetrics(r12)
            android.graphics.Rect r35 = new android.graphics.Rect
            r35.<init>()
            r0 = r45
            android.app.Activity r3 = r0.activity
            android.view.Window r42 = r3.getWindow()
            android.view.View r3 = r42.getDecorView()
            r0 = r35
            r3.getWindowVisibleDisplayFrame(r0)
            r0 = r35
            int r0 = r0.top
            r37 = r0
            r3 = 16908290(0x1020002, float:2.3877235E-38)
            r0 = r42
            android.view.View r3 = r0.findViewById(r3)
            int r10 = r3.getTop()
            r0 = r37
            if (r10 <= r0) goto L_0x02fc
            int r39 = r10 - r37
        L_0x007d:
            int r3 = r12.widthPixels
            r0 = r45
            r0.sWidth = r3
            int r3 = r12.heightPixels
            int r3 = r3 - r37
            int r3 = r3 - r39
            r0 = r45
            r0.sHeight = r3
            java.lang.String r3 = "LBAdController"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "Device Width = "
            r4.<init>(r5)
            r0 = r45
            int r5 = r0.sWidth
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ", Height = "
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r45
            int r5 = r0.sHeight
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            com.Leadbolt.AdLog.d(r3, r4)
            java.lang.String r3 = "LBAdController"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "SBH = "
            r4.<init>(r5)
            r0 = r37
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r5 = ", TBH = "
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r39
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            com.Leadbolt.AdLog.d(r3, r4)
        L_0x00d5:
            org.apache.http.params.BasicHttpParams r30 = new org.apache.http.params.BasicHttpParams
            r30.<init>()
            java.lang.String r3 = "http.protocol.version"
            org.apache.http.HttpVersion r4 = org.apache.http.HttpVersion.HTTP_1_1
            r0 = r30
            r0.setParameter(r3, r4)
            org.apache.http.impl.client.DefaultHttpClient r21 = new org.apache.http.impl.client.DefaultHttpClient
            r0 = r21
            r1 = r30
            r0.<init>(r1)
            r0 = r45
            android.app.Activity r3 = r0.activity
            if (r3 == 0) goto L_0x0300
            r0 = r45
            android.app.Activity r3 = r0.activity
            android.content.Context r3 = r3.getBaseContext()
            java.lang.String r4 = "phone"
            java.lang.Object r3 = r3.getSystemService(r4)
            android.telephony.TelephonyManager r3 = (android.telephony.TelephonyManager) r3
            r0 = r45
            r0.tm = r3
        L_0x0106:
            r3 = 2
            java.lang.String[] r0 = new java.lang.String[r3]
            r41 = r0
            r3 = 0
            java.lang.String r4 = "http://ad.leadboltapps.net"
            r41[r3] = r4
            r3 = 1
            java.lang.String r4 = "http://ad.leadbolt.net"
            r41[r3] = r4
            r19 = 0
            r23 = 0
        L_0x0119:
            r0 = r41
            int r3 = r0.length
            r0 = r23
            if (r0 >= r3) goto L_0x0006
            if (r19 != 0) goto L_0x0006
            r3 = r41[r23]
            r0 = r45
            r0.domain = r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r0 = r45
            java.lang.String r4 = r0.domain
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3.<init>(r4)
            java.lang.String r4 = "/show_app.conf?"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r40 = r3.toString()
            r0 = r45
            boolean r3 = r0.useNotification
            if (r3 == 0) goto L_0x0312
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r0 = r45
            java.lang.String r4 = r0.domain
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3.<init>(r4)
            java.lang.String r4 = "/show_notification?"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r40 = r3.toString()
        L_0x015c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "&section_id="
            r3.<init>(r4)
            r0 = r45
            java.lang.String r4 = r0.sectionid
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "&app_id="
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r45
            java.lang.String r4 = r0.appId
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r20 = r3.toString()
            r0 = r45
            boolean r3 = r0.useNotification
            if (r3 == 0) goto L_0x022c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "SD_ITERATION_COUNTER_"
            r3.<init>(r4)
            r0 = r45
            java.lang.String r4 = r0.sectionid
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = "0"
            r0 = r32
            java.lang.String r3 = r0.getString(r3, r4)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            int r26 = r3.intValue()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r20)
            r3.<init>(r4)
            java.lang.String r4 = "&iteration_counter="
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r26
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r20 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r20)
            r3.<init>(r4)
            java.lang.String r4 = "&launch_type="
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r45
            java.lang.String r4 = r0.notificationLaunchType
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r20 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "SD_NOTIFICATION_FIRED_"
            r3.<init>(r4)
            r0 = r45
            java.lang.String r4 = r0.sectionid
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r4 = -1
            r0 = r32
            long r17 = r0.getLong(r3, r4)
            r3 = -1
            int r3 = (r17 > r3 ? 1 : (r17 == r3 ? 0 : -1))
            if (r3 != 0) goto L_0x0331
            r25 = -1
        L_0x01fd:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r20)
            r3.<init>(r4)
            java.lang.String r4 = "&notification_fired="
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r25
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r20 = r3.toString()
            java.lang.String r3 = "LBAdController"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "NotificationFired = "
            r4.<init>(r5)
            r0 = r25
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            com.Leadbolt.AdLog.d(r3, r4)
        L_0x022c:
            r0 = r45
            boolean r3 = r0.loadIcon
            if (r3 == 0) goto L_0x0263
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "SD_ICON_DISPLAY_"
            r3.<init>(r4)
            r0 = r45
            java.lang.String r4 = r0.sectionid
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r4 = 0
            r0 = r32
            int r11 = r0.getInt(r3, r4)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r20)
            r3.<init>(r4)
            java.lang.String r4 = "&icon_displayed_count="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r11)
            java.lang.String r20 = r3.toString()
        L_0x0263:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r40)
            r3.<init>(r4)
            java.lang.String r4 = "&get="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r20.trim()
            java.lang.String r4 = com.Leadbolt.AdEncryption.encrypt(r4)
            java.lang.String r4 = r4.trim()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r40 = r3.toString()
            java.lang.String r3 = "LBAdController"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "Get = "
            r4.<init>(r5)
            java.lang.String r5 = r20.trim()
            java.lang.String r5 = com.Leadbolt.AdEncryption.encrypt(r5)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            com.Leadbolt.AdLog.d(r3, r4)
            org.apache.http.client.methods.HttpPost r22 = new org.apache.http.client.methods.HttpPost
            r0 = r22
            r1 = r40
            r0.<init>(r1)
            java.util.ArrayList r29 = new java.util.ArrayList     // Catch:{ Exception -> 0x0342 }
            r3 = 2
            r0 = r29
            r0.<init>(r3)     // Catch:{ Exception -> 0x0342 }
            org.apache.http.message.BasicNameValuePair r43 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0342 }
            java.lang.String r44 = "ref"
            r0 = r45
            android.telephony.TelephonyManager r3 = r0.tm     // Catch:{ Exception -> 0x0342 }
            r0 = r45
            java.lang.String r4 = r0.subid     // Catch:{ Exception -> 0x0342 }
            r0 = r45
            java.util.List<org.apache.http.NameValuePair> r5 = r0.tokens     // Catch:{ Exception -> 0x0342 }
            r0 = r45
            boolean r6 = r0.useLocation     // Catch:{ Exception -> 0x0342 }
            r0 = r45
            int r7 = r0.sWidth     // Catch:{ Exception -> 0x0342 }
            r0 = r45
            int r8 = r0.sHeight     // Catch:{ Exception -> 0x0342 }
            java.lang.String r3 = com.Leadbolt.AdRefValues.adRefValues(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0342 }
            r0 = r43
            r1 = r44
            r0.<init>(r1, r3)     // Catch:{ Exception -> 0x0342 }
            r0 = r29
            r1 = r43
            r0.add(r1)     // Catch:{ Exception -> 0x0342 }
            org.apache.http.client.entity.UrlEncodedFormEntity r3 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x0342 }
            r0 = r29
            r3.<init>(r0)     // Catch:{ Exception -> 0x0342 }
            r0 = r22
            r0.setEntity(r3)     // Catch:{ Exception -> 0x0342 }
        L_0x02ed:
            r9 = 0
        L_0x02ee:
            r3 = 10
            if (r9 < r3) goto L_0x0349
        L_0x02f2:
            int r23 = r23 + 1
            goto L_0x0119
        L_0x02f6:
            r0 = r45
            android.app.Activity r2 = r0.activity
            goto L_0x002d
        L_0x02fc:
            r39 = 0
            goto L_0x007d
        L_0x0300:
            r0 = r45
            android.content.Context r3 = r0.context
            java.lang.String r4 = "phone"
            java.lang.Object r3 = r3.getSystemService(r4)
            android.telephony.TelephonyManager r3 = (android.telephony.TelephonyManager) r3
            r0 = r45
            r0.tm = r3
            goto L_0x0106
        L_0x0312:
            r0 = r45
            boolean r3 = r0.loadIcon
            if (r3 == 0) goto L_0x015c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r0 = r45
            java.lang.String r4 = r0.domain
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r3.<init>(r4)
            java.lang.String r4 = "/show_app_icon.conf?"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r40 = r3.toString()
            goto L_0x015c
        L_0x0331:
            long r3 = java.lang.System.currentTimeMillis()
            r5 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 / r5
            int r3 = (int) r3
            r4 = 1000(0x3e8, double:4.94E-321)
            long r4 = r17 / r4
            int r4 = (int) r4
            int r25 = r3 - r4
            goto L_0x01fd
        L_0x0342:
            r13 = move-exception
            java.lang.String r3 = "LBAdController"
            com.Leadbolt.AdLog.printStackTrace(r3, r13)
            goto L_0x02ed
        L_0x0349:
            if (r19 != 0) goto L_0x02f2
            r31 = 0
            org.apache.http.HttpResponse r36 = r21.execute(r22)     // Catch:{ Exception -> 0x0479 }
            org.apache.http.StatusLine r3 = r36.getStatusLine()     // Catch:{ Exception -> 0x0479 }
            int r3 = r3.getStatusCode()     // Catch:{ Exception -> 0x0479 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r3 != r4) goto L_0x03e5
            r19 = 1
            org.apache.http.HttpEntity r15 = r36.getEntity()     // Catch:{ Exception -> 0x0479 }
            if (r15 == 0) goto L_0x03e5
            java.io.InputStream r24 = r15.getContent()     // Catch:{ Exception -> 0x0479 }
            java.lang.String r38 = com.Leadbolt.AdUtilFuncs.convertStreamToString(r24)     // Catch:{ Exception -> 0x0479 }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x0479 }
            r0 = r38
            r3.<init>(r0)     // Catch:{ Exception -> 0x0479 }
            r0 = r45
            r0.results = r3     // Catch:{ Exception -> 0x0479 }
            r0 = r45
            org.json.JSONObject r3 = r0.results     // Catch:{ JSONException -> 0x0620 }
            java.lang.String r4 = "usenative"
            java.lang.Object r3 = r3.get(r4)     // Catch:{ JSONException -> 0x0620 }
            java.lang.String r4 = "1"
            boolean r3 = r3.equals(r4)     // Catch:{ JSONException -> 0x0620 }
            if (r3 == 0) goto L_0x038f
            r3 = 1
            r0 = r45
            r0.nativeOpen = r3     // Catch:{ JSONException -> 0x0620 }
        L_0x038f:
            r0 = r45
            org.json.JSONObject r3 = r0.results     // Catch:{ Exception -> 0x046a }
            java.lang.String r4 = "pollmaxcount"
            int r3 = r3.getInt(r4)     // Catch:{ Exception -> 0x046a }
            r0 = r45
            r0.pollMax = r3     // Catch:{ Exception -> 0x046a }
            r0 = r45
            org.json.JSONObject r3 = r0.results     // Catch:{ Exception -> 0x046a }
            java.lang.String r4 = "pollmanualafter"
            int r27 = r3.getInt(r4)     // Catch:{ Exception -> 0x046a }
            r0 = r45
            org.json.JSONObject r3 = r0.results     // Catch:{ Exception -> 0x046a }
            java.lang.String r4 = "pollinterval"
            int r31 = r3.getInt(r4)     // Catch:{ Exception -> 0x046a }
            if (r31 <= 0) goto L_0x0462
            int r3 = r27 * 60
            int r3 = r3 / r31
            r0 = r45
            r0.pollManual = r3     // Catch:{ Exception -> 0x046a }
        L_0x03bb:
            android.content.SharedPreferences$Editor r14 = r32.edit()     // Catch:{ Exception -> 0x0479 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x052e }
            java.lang.String r4 = "SD_"
            r3.<init>(r4)     // Catch:{ Exception -> 0x052e }
            r0 = r45
            java.lang.String r4 = r0.sectionid     // Catch:{ Exception -> 0x052e }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x052e }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x052e }
            r0 = r45
            org.json.JSONObject r4 = r0.results     // Catch:{ Exception -> 0x052e }
            java.lang.String r5 = "displayinterval"
            java.lang.String r4 = r4.getString(r5)     // Catch:{ Exception -> 0x052e }
            r14.putString(r3, r4)     // Catch:{ Exception -> 0x052e }
        L_0x03df:
            r14.commit()     // Catch:{ Exception -> 0x0479 }
            r24.close()     // Catch:{ Exception -> 0x0479 }
        L_0x03e5:
            r0 = r45
            org.json.JSONObject r3 = r0.results     // Catch:{ Exception -> 0x0610 }
            if (r3 == 0) goto L_0x0432
            r0 = r45
            org.json.JSONObject r3 = r0.results     // Catch:{ Exception -> 0x0610 }
            java.lang.String r4 = "show"
            java.lang.String r3 = r3.getString(r4)     // Catch:{ Exception -> 0x0610 }
            if (r3 == 0) goto L_0x0432
            r3 = 1
            r0 = r45
            r0.initialized = r3     // Catch:{ Exception -> 0x0610 }
            r0 = r45
            com.Leadbolt.AdWebView r3 = r0.webview     // Catch:{ Exception -> 0x0610 }
            if (r3 == 0) goto L_0x040d
            r0 = r45
            com.Leadbolt.AdWebView r3 = r0.webview     // Catch:{ Exception -> 0x0610 }
            r0 = r45
            org.json.JSONObject r4 = r0.results     // Catch:{ Exception -> 0x0610 }
            r3.setResults(r4)     // Catch:{ Exception -> 0x0610 }
        L_0x040d:
            r0 = r45
            boolean r3 = r0.loadAd     // Catch:{ Exception -> 0x0610 }
            if (r3 == 0) goto L_0x0432
            com.Leadbolt.AdController$PollRequest r28 = new com.Leadbolt.AdController$PollRequest     // Catch:{ Exception -> 0x0610 }
            r3 = 0
            r0 = r28
            r1 = r45
            r0.<init>(r1, r3)     // Catch:{ Exception -> 0x0610 }
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x0610 }
            r4 = 0
            r0 = r45
            org.json.JSONObject r5 = r0.results     // Catch:{ Exception -> 0x0610 }
            java.lang.String r6 = "pollurl"
            java.lang.String r5 = r5.getString(r6)     // Catch:{ Exception -> 0x0610 }
            r3[r4] = r5     // Catch:{ Exception -> 0x0610 }
            r0 = r28
            r0.execute(r3)     // Catch:{ Exception -> 0x0610 }
        L_0x0432:
            r3 = 0
            r0 = r45
            r0.requestInProgress = r3
            r0 = r45
            boolean r3 = r0.useNotification     // Catch:{ Exception -> 0x061b }
            if (r3 == 0) goto L_0x045e
            android.content.SharedPreferences$Editor r14 = r32.edit()     // Catch:{ Exception -> 0x061b }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x061b }
            java.lang.String r4 = "SD_NOTIFICATION_REQUESTED_"
            r3.<init>(r4)     // Catch:{ Exception -> 0x061b }
            r0 = r45
            java.lang.String r4 = r0.sectionid     // Catch:{ Exception -> 0x061b }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x061b }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x061b }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x061b }
            r14.putLong(r3, r4)     // Catch:{ Exception -> 0x061b }
            r14.commit()     // Catch:{ Exception -> 0x061b }
        L_0x045e:
            int r9 = r9 + 1
            goto L_0x02ee
        L_0x0462:
            r3 = 10
            r0 = r45
            r0.pollManual = r3     // Catch:{ Exception -> 0x046a }
            goto L_0x03bb
        L_0x046a:
            r13 = move-exception
            r3 = 500(0x1f4, float:7.0E-43)
            r0 = r45
            r0.pollMax = r3     // Catch:{ Exception -> 0x0479 }
            r3 = 10
            r0 = r45
            r0.pollManual = r3     // Catch:{ Exception -> 0x0479 }
            goto L_0x03bb
        L_0x0479:
            r16 = move-exception
            java.lang.String r3 = "LBAdController"
            r0 = r16
            com.Leadbolt.AdLog.printStackTrace(r3, r0)     // Catch:{ all -> 0x055d }
            r19 = 0
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ all -> 0x055d }
            r3.<init>()     // Catch:{ all -> 0x055d }
            r0 = r45
            r0.results = r3     // Catch:{ all -> 0x055d }
            r3 = 1
            r0 = r45
            r0.initialized = r3     // Catch:{ all -> 0x055d }
            r0 = r45
            com.Leadbolt.AdListener r3 = r0.listener     // Catch:{ all -> 0x055d }
            if (r3 == 0) goto L_0x04b0
            r0 = r45
            boolean r3 = r0.loadAd     // Catch:{ all -> 0x055d }
            if (r3 == 0) goto L_0x04b0
            java.lang.String r3 = "LBAdController"
            java.lang.String r4 = "onAdFailed triggered"
            com.Leadbolt.AdLog.i(r3, r4)     // Catch:{ Exception -> 0x05f2 }
            r0 = r45
            com.Leadbolt.AdListener r3 = r0.listener     // Catch:{ Exception -> 0x05f2 }
            r3.onAdFailed()     // Catch:{ Exception -> 0x05f2 }
            r3 = 1
            r0 = r45
            r0.adLoaded = r3     // Catch:{ Exception -> 0x05f2 }
        L_0x04b0:
            r0 = r45
            org.json.JSONObject r3 = r0.results     // Catch:{ Exception -> 0x0601 }
            if (r3 == 0) goto L_0x04fd
            r0 = r45
            org.json.JSONObject r3 = r0.results     // Catch:{ Exception -> 0x0601 }
            java.lang.String r4 = "show"
            java.lang.String r3 = r3.getString(r4)     // Catch:{ Exception -> 0x0601 }
            if (r3 == 0) goto L_0x04fd
            r3 = 1
            r0 = r45
            r0.initialized = r3     // Catch:{ Exception -> 0x0601 }
            r0 = r45
            com.Leadbolt.AdWebView r3 = r0.webview     // Catch:{ Exception -> 0x0601 }
            if (r3 == 0) goto L_0x04d8
            r0 = r45
            com.Leadbolt.AdWebView r3 = r0.webview     // Catch:{ Exception -> 0x0601 }
            r0 = r45
            org.json.JSONObject r4 = r0.results     // Catch:{ Exception -> 0x0601 }
            r3.setResults(r4)     // Catch:{ Exception -> 0x0601 }
        L_0x04d8:
            r0 = r45
            boolean r3 = r0.loadAd     // Catch:{ Exception -> 0x0601 }
            if (r3 == 0) goto L_0x04fd
            com.Leadbolt.AdController$PollRequest r28 = new com.Leadbolt.AdController$PollRequest     // Catch:{ Exception -> 0x0601 }
            r3 = 0
            r0 = r28
            r1 = r45
            r0.<init>(r1, r3)     // Catch:{ Exception -> 0x0601 }
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x0601 }
            r4 = 0
            r0 = r45
            org.json.JSONObject r5 = r0.results     // Catch:{ Exception -> 0x0601 }
            java.lang.String r6 = "pollurl"
            java.lang.String r5 = r5.getString(r6)     // Catch:{ Exception -> 0x0601 }
            r3[r4] = r5     // Catch:{ Exception -> 0x0601 }
            r0 = r28
            r0.execute(r3)     // Catch:{ Exception -> 0x0601 }
        L_0x04fd:
            r3 = 0
            r0 = r45
            r0.requestInProgress = r3
            r0 = r45
            boolean r3 = r0.useNotification     // Catch:{ Exception -> 0x052b }
            if (r3 == 0) goto L_0x045e
            android.content.SharedPreferences$Editor r14 = r32.edit()     // Catch:{ Exception -> 0x052b }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x052b }
            java.lang.String r4 = "SD_NOTIFICATION_REQUESTED_"
            r3.<init>(r4)     // Catch:{ Exception -> 0x052b }
            r0 = r45
            java.lang.String r4 = r0.sectionid     // Catch:{ Exception -> 0x052b }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x052b }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x052b }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x052b }
            r14.putLong(r3, r4)     // Catch:{ Exception -> 0x052b }
            r14.commit()     // Catch:{ Exception -> 0x052b }
            goto L_0x045e
        L_0x052b:
            r3 = move-exception
            goto L_0x045e
        L_0x052e:
            r13 = move-exception
            r0 = r45
            java.lang.String r3 = r0.adDisplayInterval     // Catch:{ Exception -> 0x0479 }
            if (r3 == 0) goto L_0x05d8
            r0 = r45
            java.lang.String r3 = r0.adDisplayInterval     // Catch:{ Exception -> 0x0479 }
            java.lang.String r4 = "0"
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x0479 }
            if (r3 != 0) goto L_0x05d8
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0479 }
            java.lang.String r4 = "SD_"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0479 }
            r0 = r45
            java.lang.String r4 = r0.sectionid     // Catch:{ Exception -> 0x0479 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0479 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0479 }
            r0 = r45
            java.lang.String r4 = r0.adDisplayInterval     // Catch:{ Exception -> 0x0479 }
            r14.putString(r3, r4)     // Catch:{ Exception -> 0x0479 }
            goto L_0x03df
        L_0x055d:
            r3 = move-exception
            r0 = r45
            org.json.JSONObject r4 = r0.results     // Catch:{ Exception -> 0x0609 }
            if (r4 == 0) goto L_0x05ab
            r0 = r45
            org.json.JSONObject r4 = r0.results     // Catch:{ Exception -> 0x0609 }
            java.lang.String r5 = "show"
            java.lang.String r4 = r4.getString(r5)     // Catch:{ Exception -> 0x0609 }
            if (r4 == 0) goto L_0x05ab
            r4 = 1
            r0 = r45
            r0.initialized = r4     // Catch:{ Exception -> 0x0609 }
            r0 = r45
            com.Leadbolt.AdWebView r4 = r0.webview     // Catch:{ Exception -> 0x0609 }
            if (r4 == 0) goto L_0x0586
            r0 = r45
            com.Leadbolt.AdWebView r4 = r0.webview     // Catch:{ Exception -> 0x0609 }
            r0 = r45
            org.json.JSONObject r5 = r0.results     // Catch:{ Exception -> 0x0609 }
            r4.setResults(r5)     // Catch:{ Exception -> 0x0609 }
        L_0x0586:
            r0 = r45
            boolean r4 = r0.loadAd     // Catch:{ Exception -> 0x0609 }
            if (r4 == 0) goto L_0x05ab
            com.Leadbolt.AdController$PollRequest r28 = new com.Leadbolt.AdController$PollRequest     // Catch:{ Exception -> 0x0609 }
            r4 = 0
            r0 = r28
            r1 = r45
            r0.<init>(r1, r4)     // Catch:{ Exception -> 0x0609 }
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0609 }
            r5 = 0
            r0 = r45
            org.json.JSONObject r6 = r0.results     // Catch:{ Exception -> 0x0609 }
            java.lang.String r7 = "pollurl"
            java.lang.String r6 = r6.getString(r7)     // Catch:{ Exception -> 0x0609 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0609 }
            r0 = r28
            r0.execute(r4)     // Catch:{ Exception -> 0x0609 }
        L_0x05ab:
            r4 = 0
            r0 = r45
            r0.requestInProgress = r4
            r0 = r45
            boolean r4 = r0.useNotification     // Catch:{ Exception -> 0x061e }
            if (r4 == 0) goto L_0x05d7
            android.content.SharedPreferences$Editor r14 = r32.edit()     // Catch:{ Exception -> 0x061e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x061e }
            java.lang.String r5 = "SD_NOTIFICATION_REQUESTED_"
            r4.<init>(r5)     // Catch:{ Exception -> 0x061e }
            r0 = r45
            java.lang.String r5 = r0.sectionid     // Catch:{ Exception -> 0x061e }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x061e }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x061e }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x061e }
            r14.putLong(r4, r5)     // Catch:{ Exception -> 0x061e }
            r14.commit()     // Catch:{ Exception -> 0x061e }
        L_0x05d7:
            throw r3
        L_0x05d8:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0479 }
            java.lang.String r4 = "SD_"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0479 }
            r0 = r45
            java.lang.String r4 = r0.sectionid     // Catch:{ Exception -> 0x0479 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0479 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0479 }
            java.lang.String r4 = "0"
            r14.putString(r3, r4)     // Catch:{ Exception -> 0x0479 }
            goto L_0x03df
        L_0x05f2:
            r13 = move-exception
            java.lang.String r3 = "LBAdController"
            java.lang.String r4 = "Error while calling onAdFailed"
            com.Leadbolt.AdLog.i(r3, r4)     // Catch:{ all -> 0x055d }
            java.lang.String r3 = "LBAdController"
            com.Leadbolt.AdLog.printStackTrace(r3, r13)     // Catch:{ all -> 0x055d }
            goto L_0x04b0
        L_0x0601:
            r13 = move-exception
            r3 = 0
            r0 = r45
            r0.initialized = r3
            goto L_0x04fd
        L_0x0609:
            r13 = move-exception
            r4 = 0
            r0 = r45
            r0.initialized = r4
            goto L_0x05ab
        L_0x0610:
            r13 = move-exception
            r3 = 0
            r0 = r45
            r0.initialized = r3
            goto L_0x0432
        L_0x0618:
            r3 = move-exception
            goto L_0x0023
        L_0x061b:
            r3 = move-exception
            goto L_0x045e
        L_0x061e:
            r4 = move-exception
            goto L_0x05d7
        L_0x0620:
            r3 = move-exception
            goto L_0x038f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Leadbolt.AdController.makeLBRequest():void");
    }

    private class PollRequest extends AsyncTask<String, Void, String> {
        private String pollRes;

        private PollRequest() {
            this.pollRes = null;
        }

        /* synthetic */ PollRequest(AdController adController, PollRequest pollRequest) {
            this();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... pollUrl) {
            HttpEntity entity;
            AdLog.d(AdController.LB_LOG, "Initial Poll Started....");
            if (pollUrl[0] == null) {
                return null;
            }
            try {
                HttpResponse response = new DefaultHttpClient().execute(new HttpGet(String.valueOf(pollUrl[0]) + AdController.this.sectionid));
                if (response.getStatusLine().getStatusCode() == 200 && (entity = response.getEntity()) != null) {
                    InputStream instream = entity.getContent();
                    this.pollRes = AdUtilFuncs.convertStreamToString(instream);
                    AdLog.d(AdController.LB_LOG, "Result " + this.pollRes);
                    instream.close();
                }
            } catch (Exception e) {
                this.pollRes = null;
            }
            return this.pollRes;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String res) {
            super.onPostExecute((Object) res);
            if (res == null || res.trim().equals("0")) {
                try {
                    if (AdController.this.results.getInt("timeopen") > 0) {
                        int time = AdController.this.results.getInt("timeopen") * 1000;
                        AdLog.i(AdController.LB_LOG, "Tease Time used - ad will load after " + time + "ms");
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                AdLog.i(AdController.LB_LOG, "Tease Time passed - loading Ad");
                                AdController.this.displayAd();
                            }
                        }, (long) time);
                        return;
                    }
                    AdController.this.displayAd();
                } catch (Exception e) {
                    AdController.this.displayAd();
                }
            } else if (res.trim().equals("1")) {
                AdLog.d(AdController.LB_LOG, "Going to trigger onAdAlreadyCompleted event");
                if (AdController.this.listener != null) {
                    try {
                        AdLog.i(AdController.LB_LOG, "onAdAlreadyCompleted triggered");
                        AdController.this.listener.onAdAlreadyCompleted();
                    } catch (Exception e2) {
                        AdLog.e(AdController.LB_LOG, "error when onAdAlreadyCompleted triggered");
                        AdLog.printStackTrace(AdController.LB_LOG, e2);
                    }
                }
            }
        }
    }

    private class LBRequest extends AsyncTask<String, Void, String> {
        private LBRequest() {
        }

        /* synthetic */ LBRequest(AdController adController, LBRequest lBRequest) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            AdController.this.makeLBRequest();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String res) {
            super.onPostExecute((Object) res);
            if (AdController.this.useNotification) {
                AdController.this.setNotification();
            } else if (AdController.this.loadIcon) {
                AdController.this.displayIcon();
            }
        }
    }

    private class Polling extends AsyncTask<String, Void, String> {
        private Polling() {
        }

        /* synthetic */ Polling(AdController adController, Polling polling) {
            this();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... pollUrl) {
            HttpEntity entity;
            if (pollUrl[0] == null) {
                return "0";
            }
            AdLog.d(AdController.LB_LOG, "Polling...");
            try {
                HttpResponse response = new DefaultHttpClient().execute(new HttpGet(String.valueOf(pollUrl[0]) + AdController.this.sectionid));
                if (response.getStatusLine().getStatusCode() != 200 || (entity = response.getEntity()) == null) {
                    return "0";
                }
                InputStream instream = entity.getContent();
                String success = AdUtilFuncs.convertStreamToString(instream);
                AdLog.d(AdController.LB_LOG, "Polling Result - " + success);
                instream.close();
                return success;
            } catch (Exception e) {
                return "0";
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String res) {
            if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                AdController.this.loadingDialog.dismiss();
            }
            AdController adController = AdController.this;
            adController.pollCount = adController.pollCount + 1;
            if (res.contains("0")) {
                try {
                    if (AdController.this.pollCount > AdController.this.pollMax || AdController.this.pollCount >= AdController.this.pollManual) {
                        AdController.this.showManualPoll();
                    } else {
                        AdController.this.pollingHandler.postDelayed(AdController.this.adPolling, (long) (AdController.this.results.getInt("pollinterval") * 1000));
                    }
                } catch (Exception e) {
                }
            } else if (res.contains("1")) {
                AdController.this.completed = true;
                AdController.this.closeUnlocker();
                if (AdController.this.listener != null) {
                    try {
                        AdLog.i(AdController.LB_LOG, "onAdCompleted triggered");
                        AdController.this.listener.onAdCompleted();
                    } catch (Exception e2) {
                        AdLog.e(AdController.LB_LOG, "error when onAdCompleted triggered");
                        AdLog.printStackTrace(AdController.LB_LOG, e2);
                    }
                }
            }
        }
    }

    class OfferPolling extends TimerTask {
        OfferPolling() {
        }

        public void run() {
            Polling poll = new Polling(AdController.this, null);
            try {
                poll.execute(String.valueOf(AdController.this.results.getString("pollurl")) + AdController.this.sectionid);
            } catch (Exception e) {
            }
        }
    }

    public boolean onBackPressed() {
        if (!this.linkClicked) {
            return false;
        }
        loadAd();
        return true;
    }

    public void setAdDestroyed(boolean aD) {
        this.adDestroyed = aD;
    }

    public boolean getAdDestroyed() {
        return this.adDestroyed;
    }

    public boolean getOnAdLoaded() {
        return this.onAdLoaded;
    }

    public void setOnAdLoaded(boolean sOA) {
        this.onAdLoaded = sOA;
    }

    public boolean getAdLoaded() {
        return this.adLoaded;
    }

    public void setAdLoaded(boolean aL) {
        this.adLoaded = aL;
    }
}
