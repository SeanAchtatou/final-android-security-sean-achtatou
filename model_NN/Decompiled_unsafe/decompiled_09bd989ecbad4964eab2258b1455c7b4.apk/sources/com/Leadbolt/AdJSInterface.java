package com.Leadbolt;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;

public class AdJSInterface {
    private Context activity;
    /* access modifiers changed from: private */
    public AdController controller;
    private AdListener listener;

    public AdJSInterface(Context act, AdController cont, AdListener list) {
        this.activity = act;
        this.controller = cont;
        this.listener = list;
    }

    public void processHTML(String content) {
        if (content != null && content.equals("0")) {
            this.controller.setAdDestroyed(true);
            if (this.listener != null) {
                try {
                    AdLog.i(AdController.LB_LOG, "onAdFailed triggered");
                    this.listener.onAdFailed();
                } catch (Exception e) {
                    AdLog.i(AdController.LB_LOG, "Error while calling onAdFailed");
                }
            }
        }
        if (this.listener != null && !this.controller.getAdDestroyed()) {
            try {
                AdLog.i(AdController.LB_LOG, "onAdLoaded triggered");
                if (!this.controller.getOnAdLoaded()) {
                    this.listener.onAdLoaded();
                    this.controller.setOnAdLoaded(true);
                }
                this.controller.setAdLoaded(true);
            } catch (Exception e2) {
                AdLog.i(AdController.LB_LOG, "Error while calling onAdLoaded");
                AdLog.printStackTrace(AdController.LB_LOG, e2);
            }
        }
    }

    public void close() {
        new Handler();
        try {
            ((Activity) this.activity).runOnUiThread(new Runnable() {
                public void run() {
                    AdJSInterface.this.controller.destroyAd();
                }
            });
        } catch (Exception e) {
        }
    }
}
