package com.Leadbolt;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class AdRefValues {
    private static boolean dataretrieve = true;

    public static String adRefValues(Context ctx, TelephonyManager tm, String subid, List<NameValuePair> tokens, boolean uL, int sWidth, int sHeight) {
        List<NameValuePair> nameValuePairs = new ArrayList<>(2);
        if (subid != null) {
            nameValuePairs.add(new BasicNameValuePair("subid", subid));
        }
        if (tokens != null) {
            String str = "";
            int j = 0;
            while (j < tokens.size()) {
                try {
                    BasicNameValuePair tmp = tokens.get(j);
                    str = String.valueOf(str) + AdEncryption.base64encode(tmp.getName().getBytes()) + ":" + AdEncryption.base64encode(tmp.getValue().getBytes()) + ",";
                    j++;
                } catch (Exception e) {
                    AdLog.e(AdController.LB_LOG, "Error while adding tokens - " + e.getMessage());
                }
            }
            nameValuePairs.add(new BasicNameValuePair("tokens", str));
            AdLog.i(AdController.LB_LOG, "Token Str = " + str);
        }
        try {
            String deviceId = Settings.Secure.getString(ctx.getContentResolver(), "android_id");
            Calendar cal = Calendar.getInstance();
            nameValuePairs.add(new BasicNameValuePair("ref1", deviceId));
            nameValuePairs.add(new BasicNameValuePair("ref2", Build.VERSION.RELEASE));
            nameValuePairs.add(new BasicNameValuePair("ref3", "Android"));
            nameValuePairs.add(new BasicNameValuePair("ref4", getLocalIpAddress()));
            nameValuePairs.add(new BasicNameValuePair("ref5", new StringBuilder().append(cal.get(15)).toString()));
            nameValuePairs.add(new BasicNameValuePair("ref6", new StringBuilder().append((int) (cal.getTimeInMillis() / 1000)).toString()));
            nameValuePairs.add(new BasicNameValuePair("ref7", new StringBuilder().append(sWidth).toString()));
            nameValuePairs.add(new BasicNameValuePair("ref8", new StringBuilder().append(sHeight).toString()));
            if (uL) {
                try {
                    Location location = ((LocationManager) ctx.getSystemService("location")).getLastKnownLocation("gps");
                    String longitude = String.valueOf(location.getLongitude());
                    nameValuePairs.add(new BasicNameValuePair("ref9", String.valueOf(location.getLatitude())));
                    nameValuePairs.add(new BasicNameValuePair("ref10", longitude));
                } catch (Exception e2) {
                }
            }
            if (dataretrieve) {
                try {
                    nameValuePairs.add(new BasicNameValuePair("ref11", tm.getNetworkCountryIso()));
                    nameValuePairs.add(new BasicNameValuePair("ref12", tm.getNetworkOperator()));
                    nameValuePairs.add(new BasicNameValuePair("ref13", tm.getNetworkOperatorName()));
                } catch (Exception e3) {
                }
            }
            nameValuePairs.add(new BasicNameValuePair("ref15", AdController.SDK_VERSION));
            nameValuePairs.add(new BasicNameValuePair("ref16", AdController.SDK_LEVEL));
            nameValuePairs.add(new BasicNameValuePair("ref17", tm.getDeviceId() != null ? tm.getDeviceId() : "0"));
            nameValuePairs.add(new BasicNameValuePair("ref18", Build.MANUFACTURER));
            nameValuePairs.add(new BasicNameValuePair("ref19", Build.MODEL));
            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService("connectivity");
            NetworkInfo mobileNet = cm.getNetworkInfo(0);
            NetworkInfo wifiNet = cm.getNetworkInfo(1);
            NetworkInfo.State mobile = mobileNet != null ? mobileNet.getState() : null;
            NetworkInfo.State wifi = wifiNet != null ? wifiNet.getState() : null;
            String networkState = "";
            if (wifi == NetworkInfo.State.CONNECTED || wifi == NetworkInfo.State.CONNECTING) {
                networkState = "wifi";
            } else if (mobile == NetworkInfo.State.CONNECTED || mobile == NetworkInfo.State.CONNECTING) {
                networkState = "carrier";
            }
            nameValuePairs.add(new BasicNameValuePair("ref20", networkState));
            String simStatus = "";
            switch (tm.getSimState()) {
                case 0:
                    simStatus = "sim_unknown";
                    break;
                case 1:
                    simStatus = "no_sim";
                    break;
                case 2:
                    simStatus = "sim_user_locked";
                    break;
                case 3:
                    simStatus = "sim_puk_locked";
                    break;
                case 4:
                    simStatus = "sim_carrier_locked";
                    break;
                case 5:
                    simStatus = "sim_ok";
                    break;
            }
            nameValuePairs.add(new BasicNameValuePair("ref21", simStatus));
            AdLog.d(AdController.LB_LOG, "r20 - " + (networkState.equals("wifi") ? "w" : "c") + ", r21 - " + simStatus);
            String refStr = "";
            for (int x = 0; x < nameValuePairs.size(); x++) {
                NameValuePair tmp2 = (NameValuePair) nameValuePairs.get(x);
                String val = tmp2.getValue();
                if (val != null) {
                    refStr = String.valueOf(refStr) + tmp2.getName() + "=" + URLEncoder.encode(val, "UTF-8") + "&";
                }
            }
            return AdEncryption.encrypt(refStr.substring(0, refStr.length() - 1));
        } catch (Exception e4) {
            AdLog.printStackTrace(AdController.LB_LOG, e4);
            return "";
        }
    }

    private static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            AdLog.printStackTrace(AdController.LB_LOG, ex);
        }
        return null;
    }
}
