package com.Leadbolt;

public interface AdListener {
    void onAdAlreadyCompleted();

    void onAdClicked();

    void onAdClosed();

    void onAdCompleted();

    void onAdFailed();

    void onAdHidden();

    void onAdLoaded();

    void onAdProgress();
}
