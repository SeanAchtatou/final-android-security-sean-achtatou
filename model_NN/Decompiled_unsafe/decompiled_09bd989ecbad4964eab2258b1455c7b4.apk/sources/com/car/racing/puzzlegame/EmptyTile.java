package com.car.racing.puzzlegame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class EmptyTile extends TileModel {
    private Bitmap mEmptyBitmap = null;

    public Bitmap getmEmptyBitmap() {
        return this.mEmptyBitmap;
    }

    public void setmEmptyBitmap(Bitmap mEmptyBitmap2) {
        this.mEmptyBitmap = mEmptyBitmap2;
    }

    public EmptyTile(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.drawColor(-1);
        super.onDraw(canvas);
        if (getmBitmapIndex() == 0) {
            Paint tp = new Paint();
            tp.setAlpha(200);
            canvas.drawBitmap(this.mEmptyBitmap, (Rect) null, getmAreaRect(), tp);
        }
    }
}
