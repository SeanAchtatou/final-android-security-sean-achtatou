package com.car.racing.puzzlegame;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.Leadbolt.AdController;

public class BootReceiver extends BroadcastReceiver {
    public void onReceive(Context arg0, Intent arg1) {
        new AdController(arg0, "681823086").loadNotification();
        new AdController(arg0, "848569541").loadIcon();
    }
}
