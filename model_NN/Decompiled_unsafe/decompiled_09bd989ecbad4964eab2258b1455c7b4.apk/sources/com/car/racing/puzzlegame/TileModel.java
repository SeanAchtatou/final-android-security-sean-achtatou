package com.car.racing.puzzlegame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

public class TileModel extends View {
    private Rect mAreaRect = new Rect(0, 0, 0, 0);
    private Bitmap mBitmap = null;
    private int mBitmapIndex = -1;
    private Rect mOldArea = new Rect();
    private int mXIndex = -1;
    private int mYIndex = -1;

    public TileModel(Context context) {
        super(context);
    }

    public Rect getmOldArea() {
        return this.mOldArea;
    }

    public void setmOldArea(Rect mOldArea2) {
        this.mOldArea = mOldArea2;
    }

    public Bitmap getmBitmap() {
        return this.mBitmap;
    }

    public void setmBitmap(Bitmap mBitmap2) {
        this.mBitmap = mBitmap2;
    }

    public int getmXIndex() {
        return this.mXIndex;
    }

    public void setmXIndex(int mXIndex2) {
        this.mXIndex = mXIndex2;
    }

    public int getmYIndex() {
        return this.mYIndex;
    }

    public void setmYIndex(int mYIndex2) {
        this.mYIndex = mYIndex2;
    }

    public int getmBitmapIndex() {
        return this.mBitmapIndex;
    }

    public void setmBitmapIndex(int mBitmapIndex2) {
        this.mBitmapIndex = mBitmapIndex2;
    }

    public Rect getmAreaRect() {
        return this.mAreaRect;
    }

    public void setmAreaRect(Rect mAreaRect2) {
        this.mAreaRect = mAreaRect2;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(this.mBitmap, (Rect) null, getmAreaRect(), new Paint());
    }
}
