package com.car.racing.puzzlegame;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;
import java.lang.ref.WeakReference;

public class UtilFuncs {
    private static final String TAG = "UtilFuncs";
    private static boolean enableLog = true;
    private static int mLastStrId = 0;
    private static Toast toast = null;

    public static void logE(String tag, String msg) {
        if (enableLog) {
            Log.e(tag, msg);
        }
    }

    public static void showToast(Context context, int strId) {
        if (strId != mLastStrId) {
            mLastStrId = strId;
            if (toast == null) {
                toast = Toast.makeText(context, strId, 1);
            } else {
                toast.setText(strId);
            }
        }
        toast.show();
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0081 A[SYNTHETIC, Splitter:B:42:0x0081] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0091 A[SYNTHETIC, Splitter:B:51:0x0091] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap decodeFile(java.lang.String r15, int r16, int r17) {
        /*
            r9 = 0
            r2 = 0
            r4 = 0
            java.io.File r3 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0063 }
            r3.<init>(r15)     // Catch:{ FileNotFoundException -> 0x0063 }
            boolean r12 = r3.exists()     // Catch:{ FileNotFoundException -> 0x00a7, all -> 0x00a0 }
            if (r12 != 0) goto L_0x001e
            if (r3 == 0) goto L_0x00b2
            r2 = 0
        L_0x0011:
            if (r4 == 0) goto L_0x0017
            r4.close()     // Catch:{ IOException -> 0x0019 }
        L_0x0016:
            r4 = 0
        L_0x0017:
            r12 = 0
        L_0x0018:
            return r12
        L_0x0019:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0016
        L_0x001e:
            android.graphics.BitmapFactory$Options r7 = new android.graphics.BitmapFactory$Options     // Catch:{ FileNotFoundException -> 0x00a7, all -> 0x00a0 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x00a7, all -> 0x00a0 }
            r12 = 1
            r7.inJustDecodeBounds = r12     // Catch:{ FileNotFoundException -> 0x00a7, all -> 0x00a0 }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00a7, all -> 0x00a0 }
            r5.<init>(r3)     // Catch:{ FileNotFoundException -> 0x00a7, all -> 0x00a0 }
            r12 = 0
            android.graphics.BitmapFactory.decodeStream(r5, r12, r7)     // Catch:{ FileNotFoundException -> 0x00aa, all -> 0x00a3 }
            int r11 = r7.outWidth     // Catch:{ FileNotFoundException -> 0x00aa, all -> 0x00a3 }
            int r6 = r7.outHeight     // Catch:{ FileNotFoundException -> 0x00aa, all -> 0x00a3 }
            r10 = 1
        L_0x0034:
            int r12 = r11 / 2
            r0 = r16
            if (r12 < r0) goto L_0x0040
            int r12 = r6 / 2
            r0 = r17
            if (r12 >= r0) goto L_0x005c
        L_0x0040:
            android.graphics.BitmapFactory$Options r8 = new android.graphics.BitmapFactory$Options     // Catch:{ FileNotFoundException -> 0x00aa, all -> 0x00a3 }
            r8.<init>()     // Catch:{ FileNotFoundException -> 0x00aa, all -> 0x00a3 }
            r8.inSampleSize = r10     // Catch:{ FileNotFoundException -> 0x00aa, all -> 0x00a3 }
            java.io.FileInputStream r12 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00aa, all -> 0x00a3 }
            r12.<init>(r3)     // Catch:{ FileNotFoundException -> 0x00aa, all -> 0x00a3 }
            r13 = 0
            android.graphics.Bitmap r9 = android.graphics.BitmapFactory.decodeStream(r12, r13, r8)     // Catch:{ FileNotFoundException -> 0x00aa, all -> 0x00a3 }
            if (r3 == 0) goto L_0x00b0
            r2 = 0
        L_0x0054:
            if (r5 == 0) goto L_0x00ae
            r5.close()     // Catch:{ IOException -> 0x009b }
        L_0x0059:
            r4 = 0
        L_0x005a:
            r12 = r9
            goto L_0x0018
        L_0x005c:
            int r11 = r11 / 2
            int r6 = r6 / 2
            int r10 = r10 * 2
            goto L_0x0034
        L_0x0063:
            r1 = move-exception
        L_0x0064:
            java.lang.String r12 = "UtilFuncs"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x008b }
            java.lang.String r14 = "error:"
            r13.<init>(r14)     // Catch:{ all -> 0x008b }
            java.lang.StackTraceElement[] r14 = r1.getStackTrace()     // Catch:{ all -> 0x008b }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x008b }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x008b }
            logE(r12, r13)     // Catch:{ all -> 0x008b }
            if (r2 == 0) goto L_0x007f
            r2 = 0
        L_0x007f:
            if (r4 == 0) goto L_0x005a
            r4.close()     // Catch:{ IOException -> 0x0086 }
        L_0x0084:
            r4 = 0
            goto L_0x005a
        L_0x0086:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0084
        L_0x008b:
            r12 = move-exception
        L_0x008c:
            if (r2 == 0) goto L_0x008f
            r2 = 0
        L_0x008f:
            if (r4 == 0) goto L_0x0095
            r4.close()     // Catch:{ IOException -> 0x0096 }
        L_0x0094:
            r4 = 0
        L_0x0095:
            throw r12
        L_0x0096:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0094
        L_0x009b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0059
        L_0x00a0:
            r12 = move-exception
            r2 = r3
            goto L_0x008c
        L_0x00a3:
            r12 = move-exception
            r4 = r5
            r2 = r3
            goto L_0x008c
        L_0x00a7:
            r1 = move-exception
            r2 = r3
            goto L_0x0064
        L_0x00aa:
            r1 = move-exception
            r4 = r5
            r2 = r3
            goto L_0x0064
        L_0x00ae:
            r4 = r5
            goto L_0x005a
        L_0x00b0:
            r2 = r3
            goto L_0x0054
        L_0x00b2:
            r2 = r3
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.car.racing.puzzlegame.UtilFuncs.decodeFile(java.lang.String, int, int):android.graphics.Bitmap");
    }

    public static String getRealPathFromURI(Uri contentUri, WeakReference<Activity> activityRef) {
        Cursor cursor = activityRef.get().managedQuery(contentUri, new String[]{"_data"}, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
