package com.car.racing.puzzlegame;

import java.util.Timer;
import java.util.TimerTask;

public class PlayTimer {
    private static Timer timer;
    /* access modifiers changed from: private */
    public TimerCallBack callback;
    /* access modifiers changed from: private */
    public int ecliptSeconds = 0;
    private TimerTask mTask = null;

    public interface TimerCallBack {
        boolean updateTime(String str);
    }

    public void startTimer() {
        if (timer == null) {
            timer = new Timer();
        }
        if (this.mTask != null) {
            this.mTask.cancel();
        }
        this.mTask = new TimerTask() {
            public void run() {
                PlayTimer playTimer = PlayTimer.this;
                playTimer.ecliptSeconds = playTimer.ecliptSeconds + 1;
                if (PlayTimer.this.callback != null) {
                    PlayTimer.this.callback.updateTime(new StringBuilder().append(PlayTimer.this.ecliptSeconds).toString());
                }
            }
        };
        timer.purge();
        timer.schedule(this.mTask, 0, 1000);
    }

    public void pauseTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        this.ecliptSeconds = 0;
    }

    public TimerCallBack getCallback() {
        return this.callback;
    }

    public void setCallback(TimerCallBack callback2) {
        this.callback = callback2;
    }
}
