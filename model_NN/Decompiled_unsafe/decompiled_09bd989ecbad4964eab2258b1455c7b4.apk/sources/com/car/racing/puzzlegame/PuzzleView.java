package com.car.racing.puzzlegame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.animation.TranslateAnimation;
import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

public class PuzzleView extends TileView {
    private static final int GAME_STATE_PAUSE = 0;
    private static final int GAME_STATE_READY = 0;
    private static final int GAME_STATE_RUNNING = 0;
    private static final int GAME_STATE_STOP = 0;
    private static final String TAG = "PuzzleView";
    private WeakReference<Context> mContext = null;
    private TileModel mEmptyTile = null;
    private int mGameState = 0;
    private int mLevel = -1;
    private int mXTileCount = 0;
    private int mYTileCount = 0;
    private ArrayList<TileModel> needMoveTiles = new ArrayList<>();

    public PuzzleView(Context context) {
        super(context);
        this.mContext = new WeakReference<>(context);
    }

    public boolean init(int level, int width, int height, Bitmap srcBitmap) {
        init(level, width, height);
        if (srcBitmap == null) {
            return false;
        }
        int tWidth = srcBitmap.getWidth();
        int tHeight = srcBitmap.getHeight();
        if (tWidth < this.mScreenWidth || tHeight < this.mScreenHeight) {
            return false;
        }
        int splitWidth = tWidth / this.mXTileCount;
        int splitHeight = tHeight / this.mYTileCount;
        for (int i = 0; i < this.mYTileCount; i++) {
            for (int j = 0; j < this.mXTileCount; j++) {
                int childBmpIndex = (this.mYTileCount * i) + j;
                if (childBmpIndex > this.mXTileCount * this.mYTileCount) {
                    return false;
                }
                Rect rect = new Rect();
                int left = ((j + 1) * 2) + (this.mTileWidth * j);
                int top = ((i + 1) * 2) + (this.mTileHeight * i);
                rect.set(left, top, this.mTileWidth + left, this.mTileHeight + top);
                if (childBmpIndex == 0) {
                    EmptyTile emptyTile = new EmptyTile(getContext());
                    emptyTile.setmXIndex(j);
                    emptyTile.setmYIndex(i);
                    emptyTile.setmBitmapIndex(childBmpIndex);
                    emptyTile.setmAreaRect(rect);
                    this.mTileArray[i][j] = emptyTile;
                    int[] colors = new int[(splitWidth * splitHeight)];
                    for (int k = 0; k < splitWidth * splitHeight; k++) {
                        colors[k] = -1;
                    }
                    emptyTile.setmEmptyBitmap(Bitmap.createBitmap(colors, splitWidth, splitHeight, Bitmap.Config.RGB_565));
                    emptyTile.setmBitmap(Bitmap.createBitmap(srcBitmap, splitWidth * j, splitHeight * i, splitWidth, splitHeight));
                    this.mEmptyTile = emptyTile;
                    addView(emptyTile);
                } else {
                    TileModel tileModel = new TileModel(getContext());
                    tileModel.setmXIndex(j);
                    tileModel.setmYIndex(i);
                    tileModel.setmBitmapIndex(childBmpIndex);
                    tileModel.setmAreaRect(rect);
                    this.mTileArray[i][j] = tileModel;
                    tileModel.setmBitmap(Bitmap.createBitmap(srcBitmap, splitWidth * j, splitHeight * i, splitWidth, splitHeight));
                    addView(tileModel);
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void init(int level, int width, int height) {
        switch (level) {
            case 1:
                this.mLevel = 1;
                this.mXTileCount = 3;
                this.mYTileCount = 3;
                this.mScreenWidth = width;
                this.mScreenHeight = height;
                this.mTileWidth = (this.mScreenWidth - ((this.mXTileCount + 1) * 2)) / this.mXTileCount;
                this.mTileHeight = (this.mScreenHeight - ((this.mYTileCount + 1) * 2)) / this.mYTileCount;
                break;
            case 2:
                this.mLevel = 2;
                this.mXTileCount = 4;
                this.mYTileCount = 4;
                this.mScreenWidth = width;
                this.mScreenHeight = height;
                this.mTileWidth = (this.mScreenWidth - ((this.mXTileCount + 1) * 2)) / this.mXTileCount;
                this.mTileHeight = (this.mScreenHeight - ((this.mYTileCount + 1) * 2)) / this.mYTileCount;
                break;
            case 3:
                this.mLevel = 3;
                this.mXTileCount = 5;
                this.mYTileCount = 5;
                this.mScreenWidth = width;
                this.mScreenHeight = height;
                this.mTileWidth = (this.mScreenWidth - ((this.mXTileCount + 1) * 2)) / this.mXTileCount;
                this.mTileHeight = (this.mScreenHeight - ((this.mYTileCount + 1) * 2)) / this.mYTileCount;
                break;
            case 4:
                this.mLevel = 4;
                this.mXTileCount = 6;
                this.mYTileCount = 6;
                this.mScreenWidth = width;
                this.mScreenHeight = height;
                this.mTileWidth = (this.mScreenWidth - ((this.mXTileCount + 1) * 2)) / this.mXTileCount;
                this.mTileHeight = (this.mScreenHeight - ((this.mYTileCount + 1) * 2)) / this.mYTileCount;
                break;
        }
        this.mTileArray = (TileModel[][]) Array.newInstance(TileModel.class, this.mYTileCount, this.mXTileCount);
    }

    private void doMoveAction(float x, float y) {
        findNeedMoveTiles(getClickedTile(x, y));
        if (this.needMoveTiles.size() > 0 && isGameOver() && this.puzzleCallBack != null) {
            UtilFuncs.logE(TAG, ">>>>>>>>>>>>>>>>>>> game over!");
            this.puzzleCallBack.gameOverCallBack();
        }
        moveTiles();
    }

    private void moveTiles() {
        for (int i = 0; i < this.needMoveTiles.size(); i++) {
            TileModel tm = this.needMoveTiles.get(i);
            Rect oldrect = tm.getmOldArea();
            Rect areaRect = tm.getmAreaRect();
            TranslateAnimation transAni = new TranslateAnimation((float) (oldrect.left - areaRect.left), 0.0f, (float) (oldrect.top - areaRect.top), 0.0f);
            transAni.setDuration(350);
            tm.startAnimation(transAni);
        }
        this.needMoveTiles.clear();
    }

    private void findNeedMoveTiles(TileModel tileModel) {
        if (this.mEmptyTile != null && tileModel != null) {
            if (this.mEmptyTile.getmXIndex() == tileModel.getmXIndex()) {
                int yIndex = this.mEmptyTile.getmYIndex();
                int xIndex = this.mEmptyTile.getmXIndex();
                if (yIndex > tileModel.getmYIndex()) {
                    for (int k = yIndex - 1; k >= tileModel.getmYIndex(); k--) {
                        TileModel tileModel2 = getTile(xIndex, k);
                        this.needMoveTiles.add(tileModel2);
                        changeInfo(tileModel2, this.mEmptyTile);
                    }
                } else if (yIndex < tileModel.getmYIndex()) {
                    for (int k2 = yIndex + 1; k2 <= tileModel.getmYIndex(); k2++) {
                        TileModel tileModel22 = getTile(xIndex, k2);
                        this.needMoveTiles.add(tileModel22);
                        changeInfo(tileModel22, this.mEmptyTile);
                    }
                }
            } else if (this.mEmptyTile.getmYIndex() == tileModel.getmYIndex()) {
                int xIndex2 = this.mEmptyTile.getmXIndex();
                int yIndex2 = this.mEmptyTile.getmYIndex();
                if (xIndex2 > tileModel.getmXIndex()) {
                    for (int k3 = xIndex2 - 1; k3 >= tileModel.getmXIndex(); k3--) {
                        TileModel tTileModel = getTile(k3, yIndex2);
                        this.needMoveTiles.add(tTileModel);
                        changeInfo(tTileModel, this.mEmptyTile);
                    }
                } else if (xIndex2 < tileModel.getmXIndex()) {
                    for (int k4 = xIndex2 + 1; k4 <= tileModel.getmXIndex(); k4++) {
                        TileModel tTileModel2 = getTile(k4, yIndex2);
                        this.needMoveTiles.add(tTileModel2);
                        changeInfo(tTileModel2, this.mEmptyTile);
                    }
                }
            }
        }
    }

    public void randomDisrupt() {
        int widthFactor = this.mScreenWidth;
        int heightFactor = this.mScreenHeight;
        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            findNeedMoveTiles(getClickedTile((float) random.nextInt(widthFactor), (float) random.nextInt(heightFactor)));
            moveTiles();
        }
        if (this.mEmptyTile.getmXIndex() != 0 || this.mEmptyTile.getmYIndex() != 0) {
            if (this.mEmptyTile.getmXIndex() != 0) {
                findNeedMoveTiles(getTile(0, this.mEmptyTile.getmYIndex()));
                moveTiles();
            }
            if (this.mEmptyTile.getmYIndex() != 0) {
                findNeedMoveTiles(getTile(this.mEmptyTile.getmXIndex(), 0));
                moveTiles();
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 1:
                doMoveAction(event.getX(), event.getY());
                break;
        }
        postInvalidate();
        return true;
    }

    private void changeInfo(TileModel x, TileModel y) {
        if (x != null && y != null) {
            Rect r = x.getmAreaRect();
            int xindex = x.getmXIndex();
            int yindex = x.getmYIndex();
            x.setmOldArea(r);
            x.setmYIndex(y.getmYIndex());
            x.setmXIndex(y.getmXIndex());
            x.setmAreaRect(y.getmAreaRect());
            y.setmOldArea(y.getmAreaRect());
            y.setmYIndex(yindex);
            y.setmXIndex(xindex);
            y.setmAreaRect(r);
        }
    }

    private TileModel getTile(int x, int y) {
        for (int i = 0; i < this.mYTileCount; i++) {
            for (int j = 0; j < this.mXTileCount; j++) {
                TileModel tileModel = this.mTileArray[i][j];
                if (tileModel.getmXIndex() == x && tileModel.getmYIndex() == y) {
                    return tileModel;
                }
            }
        }
        return null;
    }

    private TileModel getClickedTile(float x, float y) {
        for (int i = 0; i < this.mYTileCount; i++) {
            for (int j = 0; j < this.mXTileCount; j++) {
                TileModel tileModel = this.mTileArray[i][j];
                if (tileModel != null && tileModel.getmAreaRect().contains((int) x, (int) y)) {
                    return tileModel;
                }
            }
        }
        return null;
    }

    private boolean isGameOver() {
        for (int i = 0; i < this.mYTileCount; i++) {
            for (int j = 0; j < this.mXTileCount; j++) {
                TileModel tileModel = this.mTileArray[i][j];
                if (tileModel != null && (tileModel.getmXIndex() != j || tileModel.getmYIndex() != i)) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint p = new Paint();
        p.setStrokeWidth(0.0f);
        for (int i = 0; i <= this.mYTileCount; i++) {
            canvas.drawLine(0.0f, (float) ((this.mTileHeight * i) + (i * 2)), (float) this.mScreenWidth, (float) ((this.mTileHeight * i) + (i * 2)), p);
        }
        for (int i2 = 0; i2 <= this.mXTileCount; i2++) {
            canvas.drawLine((float) ((this.mTileWidth * i2) + (i2 * 2)), 0.0f, (float) ((this.mTileWidth * i2) + (i2 * 2)), (float) this.mScreenHeight, p);
        }
    }

    public void setmGameState(int mGameState2) {
        this.mGameState = mGameState2;
    }
}
