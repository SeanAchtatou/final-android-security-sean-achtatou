package com.car.racing.puzzlegame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.Leadbolt.AdController;
import com.Leadbolt.AdListener;
import com.jree.jigsawpool.R;

public class Splash extends Activity implements AdListener {
    /* access modifiers changed from: private */
    public AdController myControllerForm;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.myControllerForm = new AdController(this, "996806588", new AdListener() {
            public void onAdProgress() {
            }

            public void onAdLoaded() {
            }

            public void onAdFailed() {
            }

            public void onAdCompleted() {
                Splash.this.myControllerForm.hideAd();
            }

            public void onAdClosed() {
                Splash.this.launchMain();
            }

            public void onAdClicked() {
            }

            public void onAdAlreadyCompleted() {
                Splash.this.launchMain();
            }

            public void onAdHidden() {
                Splash.this.launchMain();
            }
        });
        this.myControllerForm.loadAd();
    }

    public void launchMain() {
        finish();
        startActivity(new Intent(this, JigdrawPuzzleMain.class));
    }

    public void onAdAlreadyCompleted() {
    }

    public void onAdClicked() {
    }

    public void onAdClosed() {
    }

    public void onAdCompleted() {
    }

    public void onAdFailed() {
        launchMain();
    }

    public void onAdHidden() {
    }

    public void onAdLoaded() {
        this.myControllerForm.hideAd();
    }

    public void onAdProgress() {
    }
}
