package com.car.racing.puzzlegame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.LinearLayout;
import com.Leadbolt.AdController;

public class Ads extends Activity {
    private Runnable send_Task1 = new Runnable() {
        public void run() {
            Ads.this.CallSpl();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Handler().postDelayed(this.send_Task1, 8000);
        LinearLayout layout = new LinearLayout(this);
        setContentView(layout);
        layout.post(new Runnable() {
            public void run() {
                new AdController(this, "380995027").loadAd();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void CallSpl() {
        startActivity(new Intent(this, JigdrawPuzzleMain.class));
    }
}
