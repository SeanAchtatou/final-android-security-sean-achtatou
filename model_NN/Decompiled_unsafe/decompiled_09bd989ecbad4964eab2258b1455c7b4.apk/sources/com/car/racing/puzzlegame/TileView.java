package com.car.racing.puzzlegame;

import android.content.Context;
import android.widget.RelativeLayout;

public class TileView extends RelativeLayout {
    protected static final int SPLIT_LINE_WIDTH = 2;
    private static final String TAG = "TileView";
    protected int mScreenHeight = 0;
    protected int mScreenWidth = 0;
    protected TileModel[][] mTileArray = null;
    protected int mTileHeight = 0;
    protected int mTileWidth = 0;
    protected PuzzleCallBack puzzleCallBack;

    public interface PuzzleCallBack {
        void gameOverCallBack();
    }

    public TileView(Context context) {
        super(context);
    }

    public PuzzleCallBack getPuzzleCallBack() {
        return this.puzzleCallBack;
    }

    public void setPuzzleCallBack(PuzzleCallBack puzzleCallBack2) {
        this.puzzleCallBack = puzzleCallBack2;
    }
}
