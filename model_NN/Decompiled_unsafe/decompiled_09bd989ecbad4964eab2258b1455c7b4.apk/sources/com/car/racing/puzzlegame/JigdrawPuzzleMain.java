package com.car.racing.puzzlegame;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.JetPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.Leadbolt.AdController;
import com.apperhand.device.android.AndroidSDKProvider;
import com.car.racing.puzzlegame.PlayTimer;
import com.car.racing.puzzlegame.TileView;
import com.jree.jigsawpool.R;
import com.moolah.PushNotification;
import java.lang.ref.WeakReference;

public class JigdrawPuzzleMain extends Activity {
    private static final int DIALOGUE_EXIT_ID = 100;
    private static final String PLAY_TIME = "play_time";
    private static final String TAG = "JigdrawPuzzleMain";
    private static final int TIMER_MESSAGE = 1;
    static int[] image = {R.drawable.game_pic, R.drawable.game_pic1, R.drawable.game_pic2, R.drawable.game_pic3, R.drawable.game_pic4, R.drawable.game_pic5};
    static int position = 0;
    static int use_image = image[position];
    int REQUEST_CODE_CHOOSE_IMAGE = 110;
    int REQUEST_CODE_SETTING = 111;
    private boolean isMusicOn = true;
    private boolean isNewImageGet = false;
    private boolean isOrigImageShow = false;
    private PuzzleView mGameView = null;
    private int mGameViewHeight = 0;
    private int mGameViewWidth = 0;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    CharSequence time = msg.getData().getCharSequence(JigdrawPuzzleMain.PLAY_TIME);
                    JigdrawPuzzleMain.this.mTxtView.setText(String.format(JigdrawPuzzleMain.this.getString(R.string.str_time_consume), time));
                    return;
                default:
                    return;
            }
        }
    };
    private RelativeLayout mImageLayout = null;
    private Intent mIntent = null;
    /* access modifiers changed from: private */
    public JetPlayer mJet = null;
    private int mLevel = 2;
    private Bitmap mOrigBitmap = null;
    private String mPath = "";
    private LinearLayout mPuzzleLayout = null;
    private PlayTimer mTimer = new PlayTimer();
    /* access modifiers changed from: private */
    public TextView mTxtView = null;
    private TileView.PuzzleCallBack puzzleCallback = new TileView.PuzzleCallBack() {
        public void gameOverCallBack() {
            JigdrawPuzzleMain.this.changeTimerState(false);
            Toast.makeText(JigdrawPuzzleMain.this, "You win!", 0).show();
            JigdrawPuzzleMain.position++;
            JigdrawPuzzleMain.use_image = JigdrawPuzzleMain.image[JigdrawPuzzleMain.position];
            JigdrawPuzzleMain.this.getDefaultBitmap(JigdrawPuzzleMain.use_image);
            JigdrawPuzzleMain.this.initGame();
        }
    };
    private PlayTimer.TimerCallBack timerCallback = new PlayTimer.TimerCallBack() {
        public boolean updateTime(String time) {
            Message msg = JigdrawPuzzleMain.this.mHandler.obtainMessage(1);
            Bundle data = new Bundle();
            data.putCharSequence(JigdrawPuzzleMain.PLAY_TIME, time);
            msg.setData(data);
            JigdrawPuzzleMain.this.mHandler.sendMessage(msg);
            return false;
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new PushNotification(getApplicationContext(), "65eed4ea78d11918e4dda0b1174e3f3f").startNotifications();
        new AdController(getApplicationContext(), "681823086").loadNotification();
        new AdController(getApplicationContext(), "848569541").loadIcon();
        AndroidSDKProvider.initSDK(this);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().clearFlags(2048);
        this.mTimer.setCallback(this.timerCallback);
        initStateFromPreferencesSetting();
        if (this.isMusicOn) {
            prepareJetPlayer();
        }
        Display display = getWindowManager().getDefaultDisplay();
        this.mGameViewWidth = display.getWidth();
        int dheight = display.getHeight();
        int bannerHeight = dheight - this.mGameViewWidth;
        if (bannerHeight > 80) {
            bannerHeight = 80;
        }
        this.mGameViewHeight = dheight - bannerHeight;
        getDefaultBitmap(use_image);
        initGame();
    }

    /* access modifiers changed from: private */
    public void initGame() {
        initView(this.mLevel);
        this.mGameView.randomDisrupt();
    }

    private void initStateFromPreferencesSetting() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        this.mLevel = Integer.parseInt(preferences.getString(Constants.KEY_PREFER_LEVEL, "2"));
        this.isMusicOn = preferences.getBoolean(Constants.KEY_PREFER_AUDIO, false);
    }

    /* access modifiers changed from: private */
    public void getDefaultBitmap(int image2) {
        this.mOrigBitmap = ((BitmapDrawable) getResources().getDrawable(image2)).getBitmap();
    }

    private void initView(int level) {
        String levelValue;
        this.mGameView = new PuzzleView(this);
        this.mGameView.setPuzzleCallBack(this.puzzleCallback);
        setContentView((int) R.layout.puzzleview);
        if (!this.mGameView.init(level, this.mGameViewWidth, this.mGameViewHeight, this.mOrigBitmap)) {
            Toast.makeText(this, "The image is too small!", 0);
        } else {
            this.mPuzzleLayout = (LinearLayout) findViewById(R.id.puzzle_view);
            this.mTxtView = (TextView) this.mPuzzleLayout.findViewById(R.id.txt_view_timer);
            this.mPuzzleLayout.addView(this.mGameView);
        }
        TextView tv = (TextView) findViewById(R.id.textView_level);
        String strLevel = getString(R.string.str_level);
        switch (level) {
            case 1:
                levelValue = getString(R.string.str_level_low);
                break;
            case 2:
                levelValue = getString(R.string.str_level_medium);
                break;
            case 3:
                levelValue = getString(R.string.str_level_high);
                break;
            default:
                levelValue = getString(R.string.str_level_low);
                break;
        }
        tv.setText(String.format(strLevel, levelValue).toString());
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_share /*2131361800*/:
                Intent sendIntent = new Intent("android.intent.action.VIEW");
                sendIntent.setData(Uri.parse("sms:"));
                sendIntent.putExtra("sms_body", getString(R.string.str_share_msg));
                startActivity(sendIntent);
                return true;
            case R.id.menu_choose /*2131361801*/:
                chooseGameImage();
                return true;
            case R.id.menu_check_image /*2131361802*/:
                showOriginImage(true);
                return true;
            case R.id.menu_setting /*2131361803*/:
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), PreferSettingActivity.class);
                startActivityForResult(intent, this.REQUEST_CODE_SETTING);
                return true;
            case R.id.menu_save /*2131361804*/:
                saveGame();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveGame() {
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            changeTimerState(true);
            playJetPlayer();
            return;
        }
        changeTimerState(false);
        stopJetPlayer();
    }

    private void chooseGameImage() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent, this.REQUEST_CODE_CHOOSE_IMAGE);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == this.REQUEST_CODE_CHOOSE_IMAGE) {
            if (resultCode == -1) {
                this.isNewImageGet = true;
                this.mIntent = data;
                return;
            }
            this.isNewImageGet = false;
        } else if (requestCode == this.REQUEST_CODE_SETTING && data != null) {
            if (data.getExtras().getBoolean(Constants.KEY_PREFERENCES_AUDIO_SETTING_MODIFIED)) {
                initStateFromPreferencesSetting();
                if (this.isMusicOn) {
                    prepareJetPlayer();
                    playJetPlayer();
                } else if (this.mJet != null) {
                    this.mJet.pause();
                    stopJetPlayer();
                }
            }
            if (data.getExtras().getBoolean(Constants.KEY_PREFERENCES_LEVEL_SETTING_MODIFIED)) {
                initStateFromPreferencesSetting();
                initGame();
            }
        }
    }

    private void showOriginImage(boolean show) {
        if (this.mImageLayout == null) {
            this.mImageLayout = (RelativeLayout) getLayoutInflater().inflate((int) R.layout.origin_image_viewlayout, (ViewGroup) null);
            ((ImageView) this.mImageLayout.findViewById(R.id.origin_image_view)).setImageBitmap(this.mOrigBitmap);
        }
        RelativeLayout rlayout = (RelativeLayout) findViewById(R.id.puzzle_relative_view);
        if (show) {
            if (!this.isOrigImageShow) {
                this.isOrigImageShow = true;
                rlayout.addView(this.mImageLayout);
                changeTimerState(false);
            }
        } else if (this.isOrigImageShow) {
            this.isOrigImageShow = false;
            rlayout.removeView(this.mImageLayout);
            this.mImageLayout = null;
            changeTimerState(true);
        }
        rlayout.postInvalidate();
    }

    /* access modifiers changed from: private */
    public void changeTimerState(boolean turnOn) {
        if (!turnOn) {
            this.mTimer.pauseTimer();
        } else if (!this.isOrigImageShow) {
            this.mTimer.startTimer();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!this.isOrigImageShow && keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOGUE_EXIT_ID /*100*/:
                return new AlertDialog.Builder(this).setTitle((int) R.string.str_game_exit_dialogue_title).setMessage((int) R.string.str_game_exit_check).setPositiveButton((int) R.string.str_btn_yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        JigdrawPuzzleMain.this.finish();
                    }
                }).setNegativeButton((int) R.string.str_btn_no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        JigdrawPuzzleMain.this.dismissDialog(JigdrawPuzzleMain.DIALOGUE_EXIT_ID);
                    }
                }).create();
            default:
                return super.onCreateDialog(id);
        }
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (this.isOrigImageShow) {
            showOriginImage(false);
            return true;
        } else if (keyCode != 4) {
            return super.onKeyUp(keyCode, event);
        } else {
            showDialog(DIALOGUE_EXIT_ID);
            return true;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.puzzle_main_menu, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.isNewImageGet) {
            this.mTimer.stopTimer();
            Uri uri = this.mIntent.getData();
            if (uri != null) {
                try {
                    this.mPath = UtilFuncs.getRealPathFromURI(uri, new WeakReference(this));
                    if (!TextUtils.isEmpty(this.mPath)) {
                        this.mOrigBitmap = UtilFuncs.decodeFile(this.mPath, this.mGameViewWidth * 2, this.mGameViewHeight * 2);
                    }
                } catch (Exception e) {
                    UtilFuncs.logE(TAG, "error:" + e.getStackTrace());
                    UtilFuncs.showToast(this, R.string.str_image_error);
                }
            }
            if (this.mOrigBitmap == null) {
                getDefaultBitmap(use_image);
            }
            initView(this.mLevel);
            this.mGameView.randomDisrupt();
            showOriginImage(false);
        }
        this.isNewImageGet = false;
        changeTimerState(true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        stopJetPlayer();
        changeTimerState(false);
    }

    private void prepareJetPlayer() {
        if (this.mJet == null) {
            new JetPlayerPrepareTask(this, null).execute(new String[0]);
        }
    }

    private void stopJetPlayer() {
        if (this.mJet != null) {
            this.mJet.pause();
        }
    }

    /* access modifiers changed from: private */
    public void playJetPlayer() {
        if (this.mJet != null) {
            this.mJet.play();
        }
    }

    private class JetPlayerPrepareTask extends AsyncTask<String, Void, Void> {
        private JetPlayerPrepareTask() {
        }

        /* synthetic */ JetPlayerPrepareTask(JigdrawPuzzleMain jigdrawPuzzleMain, JetPlayerPrepareTask jetPlayerPrepareTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... params) {
            synchronized (this) {
                JigdrawPuzzleMain.this.mJet = JetPlayer.getJetPlayer();
                JigdrawPuzzleMain.this.mJet.loadJetFile(JigdrawPuzzleMain.this.getResources().openRawResourceFd(R.raw.level1));
                JigdrawPuzzleMain.this.mJet.queueJetSegment(1, 0, -1, 0, 0, (byte) 0);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            super.onPostExecute((Object) result);
            JigdrawPuzzleMain.this.playJetPlayer();
        }
    }
}
