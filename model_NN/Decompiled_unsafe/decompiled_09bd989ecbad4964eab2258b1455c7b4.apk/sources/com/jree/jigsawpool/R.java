package com.jree.jigsawpool;

public final class R {

    public static final class array {
        public static final int entries_list_preferences = 2131099648;
        public static final int entryvalues_list_preferences = 2131099649;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int LG_GRAY = 2131165202;
        public static final int bk_gallery_scaleleveltime = 2131165189;
        public static final int bk_info_panel = 2131165188;
        public static final int black = 2131165186;
        public static final int color_gallerydelete_mask = 2131165197;
        public static final int color_selectmedia_bk = 2131165201;
        public static final int color_text_artist = 2131165199;
        public static final int color_text_bg = 2131165198;
        public static final int gray = 2131165187;
        public static final int textcolor_audiotrack_startpos = 2131165191;
        public static final int textcolor_bgmusic_duration = 2131165194;
        public static final int textcolor_content_focus = 2131165193;
        public static final int textcolor_help_title = 2131165196;
        public static final int textcolor_nobgmusic_duration = 2131165195;
        public static final int textcolor_project_text = 2131165200;
        public static final int textcolor_transition_time = 2131165192;
        public static final int textcolor_trim_range = 2131165190;
        public static final int transparent = 2131165184;
        public static final int white = 2131165185;
    }

    public static final class drawable {
        public static final int bk_button = 2130837511;
        public static final int black = 2130837518;
        public static final int blue = 2130837513;
        public static final int game_pic = 2130837504;
        public static final int game_pic1 = 2130837505;
        public static final int game_pic2 = 2130837506;
        public static final int game_pic3 = 2130837507;
        public static final int game_pic4 = 2130837508;
        public static final int game_pic5 = 2130837509;
        public static final int gray = 2130837517;
        public static final int green = 2130837514;
        public static final int ic_launcher = 2130837510;
        public static final int media_browser_bottom_line = 2130837516;
        public static final int red = 2130837512;
        public static final int screen_background_black = 2130837520;
        public static final int translucent_background = 2130837521;
        public static final int transparent_background = 2130837522;
        public static final int white = 2130837519;
        public static final int yellow = 2130837515;
    }

    public static final class id {
        public static final int button1 = 2131361796;
        public static final int menu_ad = 2131361805;
        public static final int menu_check_image = 2131361802;
        public static final int menu_choose = 2131361801;
        public static final int menu_save = 2131361804;
        public static final int menu_setting = 2131361803;
        public static final int menu_share = 2131361800;
        public static final int origin_image_view = 2131361792;
        public static final int puzzle_relative_view = 2131361794;
        public static final int puzzle_view = 2131361795;
        public static final int surface_view = 2131361799;
        public static final int textView1 = 2131361793;
        public static final int textView_level = 2131361797;
        public static final int txt_view_timer = 2131361798;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int origin_image_viewlayout = 2130903041;
        public static final int puzzleview = 2130903042;
        public static final int videoview_layout = 2130903043;
    }

    public static final class menu {
        public static final int puzzle_main_menu = 2131296256;
    }

    public static final class raw {
        public static final int level1 = 2131034112;
    }

    public static final class string {
        public static final int app_name = 2131230723;
        public static final int hello = 2131230722;
        public static final int startapp_appid = 2131230721;
        public static final int startapp_devid = 2131230720;
        public static final int str_btn_no = 2131230734;
        public static final int str_btn_yes = 2131230733;
        public static final int str_donate_title = 2131230746;
        public static final int str_game_exit_check = 2131230731;
        public static final int str_game_exit_dialogue_title = 2131230732;
        public static final int str_image_error = 2131230747;
        public static final int str_level = 2131230727;
        public static final int str_level_high = 2131230730;
        public static final int str_level_low = 2131230728;
        public static final int str_level_medium = 2131230729;
        public static final int str_menu_ad = 2131230741;
        public static final int str_menu_main_check_img = 2131230736;
        public static final int str_menu_main_choose = 2131230735;
        public static final int str_menu_main_setting = 2131230737;
        public static final int str_menu_main_setting_audioset = 2131230740;
        public static final int str_menu_main_setting_levelset = 2131230738;
        public static final int str_menu_save = 2131230739;
        public static final int str_origin_image_show = 2131230742;
        public static final int str_pause = 2131230725;
        public static final int str_preferences_setting_audio = 2131230744;
        public static final int str_preferences_setting_level = 2131230743;
        public static final int str_share = 2131230726;
        public static final int str_share_msg = 2131230745;
        public static final int str_time_consume = 2131230724;
    }

    public static final class xml {
        public static final int setting_preferences = 2130968576;
    }
}
