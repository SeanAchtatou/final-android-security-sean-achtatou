package com.apperhand.common.dto;

import java.io.Serializable;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseDTO implements Serializable {
    protected static String NEW_LINE = System.getProperty("line.separator");
    private static final long serialVersionUID = 3527891079407840172L;

    public abstract String toString();
}
