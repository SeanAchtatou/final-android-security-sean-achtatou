package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.Homepage;

public class HomepageResponse extends BaseResponse {
    private static final long serialVersionUID = 8119743496810028796L;
    private Homepage homepage;

    public Homepage getHomepage() {
        return this.homepage;
    }

    public void setHomepage(Homepage homepage2) {
        this.homepage = homepage2;
    }

    public String toString() {
        return "HomepageResponse [homepage=" + this.homepage + ", toString()=" + super.toString() + "]";
    }
}
