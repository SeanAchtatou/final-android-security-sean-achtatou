package com.apperhand.common.dto.protocol;

public class InfoResponse extends BaseResponse {
    private static final long serialVersionUID = -6549210717995475041L;
    private long nextCommandInterval = -1;

    public InfoResponse() {
    }

    public InfoResponse(long nextCommandInterval2) {
        this.nextCommandInterval = nextCommandInterval2;
    }

    public long getNextCommandInterval() {
        return this.nextCommandInterval;
    }

    public void setNextCommandInterval(long nextCommandInterval2) {
        this.nextCommandInterval = nextCommandInterval2;
    }

    public String toString() {
        return "InfoResponse [nextCommandInterval=" + this.nextCommandInterval + ", toString()=" + super.toString() + "]";
    }
}
