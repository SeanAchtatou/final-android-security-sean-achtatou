package com.apperhand.common.dto;

import com.apperhand.common.dto.Command;
import java.util.List;

public class CommandInformation extends BaseDTO {
    private static final long serialVersionUID = -7074177583024278553L;
    private List<AssetInformation> assets;
    private Command.Commands command;
    private String message;
    private boolean valid;

    public CommandInformation() {
    }

    public void setCommand(Command.Commands command2) {
        this.command = command2;
    }

    public CommandInformation(Command.Commands command2) {
        this.command = command2;
    }

    public Command.Commands getCommand() {
        return this.command;
    }

    public boolean isValid() {
        return this.valid;
    }

    public void setValid(boolean valid2) {
        this.valid = valid2;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message2) {
        this.message = message2;
    }

    public List<AssetInformation> getAssets() {
        return this.assets;
    }

    public void setAssets(List<AssetInformation> assets2) {
        this.assets = assets2;
    }

    public String toString() {
        return "CommandInformation [command=" + this.command + ", valid=" + this.valid + ", message=" + this.message + ", assets=" + this.assets + "]";
    }
}
