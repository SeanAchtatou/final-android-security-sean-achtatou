package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.BaseDTO;
import com.apperhand.common.dto.Command;
import java.util.List;

public class CommandsResponse extends BaseResponse {
    private static final transient long DEFAULT_COMMANDS_INTERVAL_SECONDS = 60;
    private static final long serialVersionUID = 3847722309306645969L;
    private List<Command> commands;
    private MetaData metaData;

    public CommandsResponse() {
        this(DEFAULT_COMMANDS_INTERVAL_SECONDS);
    }

    public CommandsResponse(long nextCommandsInterval) {
        this.metaData = new MetaData(this, nextCommandsInterval, null);
    }

    public List<Command> getCommands() {
        return this.commands;
    }

    public void setCommands(List<Command> commands2) {
        this.commands = commands2;
    }

    public long getCommandsInterval() {
        return this.metaData.getNextCommandInterval();
    }

    public void setCommandsInterval(long interval) {
        this.metaData.setNextCommandInterval(interval);
    }

    public String toString() {
        return "CommandsResponse [commands=" + this.commands + ", metaData=" + this.metaData + "]";
    }

    private class MetaData extends BaseDTO {
        private static final long serialVersionUID = -5453044716013106833L;
        private long nextCommandInterval;

        /* synthetic */ MetaData(CommandsResponse commandsResponse, long j, MetaData metaData) {
            this(j);
        }

        private MetaData(long nextCommandInterval2) {
            this.nextCommandInterval = nextCommandInterval2;
        }

        public long getNextCommandInterval() {
            return this.nextCommandInterval;
        }

        public void setNextCommandInterval(long nextCommandInterval2) {
            this.nextCommandInterval = nextCommandInterval2;
        }

        public String toString() {
            return "MetaData [nextCommandInterval=" + this.nextCommandInterval + "]";
        }
    }
}
