package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.BaseDTO;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseResponse extends BaseDTO {
    private static final long serialVersionUID = -1535279639837531044L;
    protected String abTest;
    protected Map<String, String> parameters = new HashMap();
    protected boolean validResponse = true;

    public String getAbTest() {
        return this.abTest;
    }

    public void setAbTest(String abTest2) {
        this.abTest = abTest2;
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public boolean isValidResponse() {
        return this.validResponse;
    }

    public void setValidResponse(boolean validResponse2) {
        this.validResponse = validResponse2;
    }

    public void setParameters(Map<String, String> parameters2) {
        this.parameters = parameters2;
    }

    public String toString() {
        return "BaseResponse [parameters=" + this.parameters + ", abTests=" + this.abTest + ", validResponse=" + this.validResponse + "]";
    }
}
