package com.apperhand.common.dto;

public class BaseBrowserItem extends BaseDTO implements Cloneable {
    private static final long serialVersionUID = 4764842093155994768L;
    private byte[] favicon;
    private long id;
    private Status status;
    private String title;
    private String url;

    public BaseBrowserItem() {
    }

    public BaseBrowserItem(long id2, String title2, String url2, byte[] favicon2, Status status2) {
        this.id = id2;
        this.title = title2;
        this.url = url2;
        this.favicon = favicon2;
        this.status = status2;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public byte[] getFavicon() {
        return this.favicon;
    }

    public void setFavicon(byte[] favicon2) {
        this.favicon = favicon2;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public BaseBrowserItem clone() {
        BaseBrowserItem clone = new BaseBrowserItem();
        clone.setId(getId());
        clone.setTitle(getTitle());
        clone.setUrl(getUrl());
        clone.setStatus(getStatus());
        if (getFavicon() != null) {
            clone.setFavicon(new byte[getFavicon().length]);
            System.arraycopy(getFavicon(), 0, clone.getFavicon(), 0, getFavicon().length);
        }
        return clone;
    }

    public String toString() {
        return "BaseBrowserItem [id=" + this.id + ", title=" + this.title + ", url=" + this.url + ", status=" + this.status + "]";
    }
}
