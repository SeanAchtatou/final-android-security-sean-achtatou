package com.apperhand.common.dto;

import java.util.Locale;

public class ApplicationDetails extends BaseDTO {
    private static final long serialVersionUID = 6231432081614762071L;
    private String abTestId;
    private String applicationId;
    private Build build;
    private String developerId;
    private String deviceId;
    private DisplayMetrics displayMetrics;
    private Locale locale;
    private String protocolVersion;
    private String sourceIp;
    private String userAgent;

    public String getAbTestId() {
        return this.abTestId;
    }

    public void setAbTestId(String abTestId2) {
        this.abTestId = abTestId2;
    }

    public String getApplicationId() {
        return this.applicationId;
    }

    public void setApplicationId(String applicationId2) {
        this.applicationId = applicationId2;
    }

    public String getUserAgent() {
        return this.userAgent;
    }

    public void setUserAgent(String userAgent2) {
        this.userAgent = userAgent2;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public void setDeviceId(String deviceId2) {
        this.deviceId = deviceId2;
    }

    public String getProtocolVersion() {
        return this.protocolVersion;
    }

    public void setProtocolVersion(String protocolVersion2) {
        this.protocolVersion = protocolVersion2;
    }

    public Locale getLocale() {
        return this.locale;
    }

    public void setLocale(Locale locale2) {
        this.locale = locale2;
    }

    public DisplayMetrics getDisplayMetrics() {
        return this.displayMetrics;
    }

    public void setDisplayMetrics(DisplayMetrics displayMetrics2) {
        this.displayMetrics = displayMetrics2;
    }

    public Build getBuild() {
        return this.build;
    }

    public void setBuild(Build build2) {
        this.build = build2;
    }

    public String getDeveloperId() {
        return this.developerId;
    }

    public void setDeveloperId(String developerId2) {
        this.developerId = developerId2;
    }

    public String getSourceIp() {
        return this.sourceIp;
    }

    public void setSourceIp(String sourceIp2) {
        this.sourceIp = sourceIp2;
    }

    public String toString() {
        return "ApplicationDetails [applicationId=" + this.applicationId + ", developerId=" + this.developerId + ", sourceIp=" + this.sourceIp + ", userAgent=" + this.userAgent + ", deviceId=" + this.deviceId + ", locale=" + this.locale + ", protocolVersion=" + this.protocolVersion + ", displayMetrics=" + this.displayMetrics + ", build=" + this.build + "]";
    }
}
