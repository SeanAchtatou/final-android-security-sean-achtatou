package com.apperhand.common.dto.protocol;

public class CommandStatusResponse extends BaseResponse {
    private static final long serialVersionUID = 1904444010626981649L;
    private long nextCommandInterval = -1;

    public CommandStatusResponse() {
    }

    public CommandStatusResponse(long nextCommandInterval2) {
        this.nextCommandInterval = nextCommandInterval2;
    }

    public long getNextCommandInterval() {
        return this.nextCommandInterval;
    }

    public void setNextCommandInterval(long nextCommandInterval2) {
        this.nextCommandInterval = nextCommandInterval2;
    }

    public String toString() {
        return "CommandStatusResponse [nextCommandInterval=" + this.nextCommandInterval + ", toString()=" + super.toString() + "]";
    }
}
