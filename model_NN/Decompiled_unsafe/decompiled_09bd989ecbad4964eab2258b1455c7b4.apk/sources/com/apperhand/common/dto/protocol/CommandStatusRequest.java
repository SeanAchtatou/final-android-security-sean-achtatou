package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.CommandStatus;
import java.util.List;

public class CommandStatusRequest extends BaseRequest {
    private static final long serialVersionUID = 5346676359806508659L;
    private List<CommandStatus> statuses;

    public List<CommandStatus> getStatuses() {
        return this.statuses;
    }

    public void setStatuses(List<CommandStatus> statuses2) {
        this.statuses = statuses2;
    }

    public String toString() {
        return "CommandStatusRequest [statuses=" + this.statuses + ", toString()=" + super.toString() + "]";
    }
}
