package com.apperhand.common.dto.protocol;

public class NotificationsRequest extends BaseRequest {
    private static final long serialVersionUID = -5758027612681877585L;

    public String toString() {
        return "NotificationsRequest [toString()=" + super.toString() + "]";
    }
}
