package com.apperhand.device.android.c;

import android.util.Log;
import com.apperhand.device.a.d.c;

/* compiled from: LoggerDecorator */
public final class b implements c {
    public final void a(c.a aVar, String str, String str2) {
        a(aVar, str, str2, null);
    }

    public final void a(c.a aVar, String str, String str2, Throwable th) {
        switch (aVar) {
            case DEBUG:
                Log.d(str, str2, th);
                return;
            case ERROR:
                Log.e(str, str2, th);
                return;
            case INFO:
                Log.i(str, str2, th);
                return;
            default:
                return;
        }
    }
}
