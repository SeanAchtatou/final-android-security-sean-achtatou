package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.device.a.a;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.f;
import java.util.Map;

/* compiled from: TerminateService */
public final class i extends b {
    public i(b bVar, a aVar, String str, Command.Commands commands) {
        super(bVar, aVar, str, commands);
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws f {
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws f {
        this.e.f().a();
        this.f.f();
        return new BaseResponse() {
            public final String toString() {
                return new StringBuilder("DummyResponse").toString();
            }
        };
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws f {
        a(b());
    }

    /* access modifiers changed from: protected */
    public final CommandStatusRequest b() throws f {
        CommandStatusRequest b = super.b();
        b.setStatuses(a(Command.Commands.TERMINATE, CommandStatus.Status.SUCCESS, "SABABA!!!", null));
        return b;
    }
}
