package com.apperhand.device.a.d;

/* compiled from: SDKException */
public final class f extends Exception {
    private a a;
    private boolean b;

    public f() {
        this.b = true;
    }

    public f(a aVar, String str, Throwable th) {
        this(aVar, str, th, true);
    }

    public f(a aVar, String str, Throwable th, boolean z) {
        super(str, th);
        this.b = true;
        this.a = aVar;
        this.b = z;
    }

    public f(a aVar, String str) {
        this(aVar, str, null);
    }

    public final boolean a() {
        return this.b;
    }

    /* compiled from: SDKException */
    public enum a {
        GENERAL_ERROR(1, "general error"),
        SHORTCUT_ERROR(10, "shortcut error"),
        BOOKMARK_ERROR(20, "bookmark error"),
        HISTORY_ERROR(30, "history error"),
        NOTIFICATION_ERROR(40, "notification error");
        
        private long f;
        private String g;

        private a(long j, String str) {
            this.f = j;
            this.g = str;
        }
    }
}
