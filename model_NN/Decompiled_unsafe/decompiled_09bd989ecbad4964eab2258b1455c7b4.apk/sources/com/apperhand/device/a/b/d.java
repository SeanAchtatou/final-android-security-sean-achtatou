package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.common.dto.protocol.HomepageRequest;
import com.apperhand.common.dto.protocol.HomepageResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.c;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.c;
import com.apperhand.device.a.d.f;
import java.util.HashMap;
import java.util.Map;

/* compiled from: HomepageService */
public final class d extends b {
    private c g;

    public d(b bVar, a aVar, String str, Command command) {
        super(bVar, aVar, str, command.getCommand());
        this.g = aVar.g();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws f {
        HomepageResponse homepageResponse = (HomepageResponse) baseResponse;
        if (homepageResponse == null) {
            return null;
        }
        boolean a = this.g.a(homepageResponse.getHomepage());
        HashMap hashMap = new HashMap(1);
        hashMap.put("output_flag", Boolean.valueOf(a));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws f {
        HomepageRequest homepageRequest = new HomepageRequest();
        homepageRequest.setApplicationDetails(this.e.i());
        return a(homepageRequest);
    }

    private BaseResponse a(HomepageRequest homepageRequest) {
        try {
            return (HomepageResponse) this.e.b().a(homepageRequest, Command.Commands.HOMEPAGE, HomepageResponse.class);
        } catch (f e) {
            this.e.a().a(c.a.DEBUG, this.a, "Unable to handle Homepage command!!!!", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws f {
        boolean z;
        if (map != null) {
            z = ((Boolean) map.get("output_flag")) != null ? ((Boolean) map.get("output_flag")).booleanValue() : false;
        } else {
            z = false;
        }
        CommandStatusRequest b = super.b();
        b.setStatuses(a(Command.Commands.HOMEPAGE, z ? CommandStatus.Status.SUCCESS : CommandStatus.Status.FAILURE, z ? "Sababa" : "Didn't attemp to change the homepage", null));
        a(b);
    }
}
