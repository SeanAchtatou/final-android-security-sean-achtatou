package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.common.dto.protocol.CommandStatusResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.d.c;
import com.apperhand.device.a.d.e;
import com.apperhand.device.a.d.f;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: BaseService */
public abstract class b {
    protected final String a = ("FRM." + getClass().getSimpleName());
    protected final c b;
    protected final String c;
    protected final Command.Commands d;
    protected a e;
    protected com.apperhand.device.a.b f;

    /* access modifiers changed from: protected */
    public abstract BaseResponse a() throws f;

    /* access modifiers changed from: protected */
    public abstract Map<String, Object> a(BaseResponse baseResponse) throws f;

    /* access modifiers changed from: protected */
    public abstract void a(Map<String, Object> map) throws f;

    public b(com.apperhand.device.a.b bVar, a aVar, String str, Command.Commands commands) {
        this.f = bVar;
        this.e = aVar;
        this.b = aVar.a();
        this.c = str;
        this.d = commands;
    }

    public final void c() {
        this.b.a(c.a.INFO, this.a, "Entering execute()");
        try {
            BaseResponse a2 = a();
            if (a2 == null || !a2.isValidResponse()) {
                this.b.a(c.a.INFO, this.a, "Server Error in " + this.d.name());
                CommandStatusRequest b2 = b();
                b2.setStatuses(a(this.d, CommandStatus.Status.FAILURE, "Got server error", null));
                a(b2);
                return;
            }
            this.f.b(e.a(a2));
            a(a(a2));
        } catch (f e2) {
            this.b.a(c.a.ERROR, this.a, "Unable to send command", e2);
            try {
                CommandStatusRequest commandStatusRequest = new CommandStatusRequest();
                commandStatusRequest.setApplicationDetails(this.e.i());
                commandStatusRequest.setStatuses(a(this.d, CommandStatus.Status.FAILURE, e2.toString(), null));
                a(commandStatusRequest);
            } catch (f e3) {
                this.b.a(c.a.ERROR, this.a, "Unable to send exception status command", e3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public CommandStatusRequest b() throws f {
        CommandStatusRequest commandStatusRequest = new CommandStatusRequest();
        commandStatusRequest.setApplicationDetails(this.e.i());
        return commandStatusRequest;
    }

    /* access modifiers changed from: protected */
    public final List<CommandStatus> a(Command.Commands commands, CommandStatus.Status status, String str, Map<String, Object> map) {
        ArrayList arrayList = new ArrayList(1);
        CommandStatus commandStatus = new CommandStatus();
        commandStatus.setCommand(commands);
        commandStatus.setId(this.c);
        commandStatus.setMessage(str);
        commandStatus.setStatus(status);
        commandStatus.setParameters(map);
        arrayList.add(commandStatus);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a(CommandStatusRequest commandStatusRequest) throws f {
        try {
            CommandStatusResponse commandStatusResponse = (CommandStatusResponse) this.e.b().a(commandStatusRequest, Command.Commands.COMMANDS_STATUS, CommandStatusResponse.class);
            this.f.a(commandStatusResponse.getNextCommandInterval());
            this.f.b(e.a(commandStatusResponse));
        } catch (f e2) {
            this.e.a().a(c.a.DEBUG, this.a, String.format("Unable to send command status for command [%s]!!!!", this.d.getString()), e2);
        }
    }
}
