package com.moolah;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import com.moolah.AdTargeting;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class Moolah {
    private static final String MOOLAH_URL = "http://mobroll.160tracker.com";
    private Context mContext;
    private String mPublisherSignature;

    public Moolah(Context context, String str) {
        this.mContext = context;
        this.mPublisherSignature = str;
    }

    public void showAd(AdTargeting adTargeting) {
        getURL();
        String url = getURL(adTargeting, "ad");
        Intent intent = new Intent(this.mContext, MoolahActivity.class);
        intent.putExtra("URL", url);
        this.mContext.startActivity(intent);
    }

    public void showCoReg(AdTargeting adTargeting) {
        String url = getURL(adTargeting, "coreg");
        Intent intent = new Intent(this.mContext, MoolahActivity.class);
        intent.putExtra("URL", url);
        this.mContext.startActivity(intent);
    }

    private String getURL(AdTargeting adTargeting, String str) {
        byte[] bArr;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(MOOLAH_URL);
        try {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(new BasicNameValuePair("sig", this.mPublisherSignature));
            arrayList.add(new BasicNameValuePair("device_type", "android"));
            String string = Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
            byte[] bArr2 = new byte[40];
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-1");
                instance.update(string.getBytes("iso-8859-1"), 0, string.length());
                bArr = instance.digest();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                bArr = bArr2;
            }
            arrayList.add(new BasicNameValuePair("unique_key", MoolahUtils.convertToHex(bArr)));
            arrayList.add(new BasicNameValuePair("request_type", str));
            if (adTargeting != null) {
                arrayList.add(new BasicNameValuePair("first_name", adTargeting.getFirstName()));
                arrayList.add(new BasicNameValuePair("last_name", adTargeting.getLastName()));
                arrayList.add(new BasicNameValuePair("email", adTargeting.getEmail()));
                arrayList.add(new BasicNameValuePair("branding_image", adTargeting.getBrandingImage()));
                arrayList.add(new BasicNameValuePair("street_address", adTargeting.getStreetAddress()));
                arrayList.add(new BasicNameValuePair("city", adTargeting.getCity()));
                arrayList.add(new BasicNameValuePair("state", adTargeting.getState()));
                arrayList.add(new BasicNameValuePair("month_dob", String.valueOf(adTargeting.getMonthOfBirth())));
                arrayList.add(new BasicNameValuePair("day_dob", String.valueOf(adTargeting.getDayOfBirth())));
                arrayList.add(new BasicNameValuePair("year_dob", String.valueOf(adTargeting.getYearOfBirth())));
                arrayList.add(new BasicNameValuePair("gender", adTargeting.getGender() == AdTargeting.Gender.FEMALE ? "F" : "M"));
                arrayList.add(new BasicNameValuePair("branding_image", adTargeting.getBrandingImage()));
            }
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
            String read = MoolahUtils.read(defaultHttpClient.execute(httpPost).getEntity().getContent());
            System.out.println("");
            return read;
        } catch (IOException | ClientProtocolException e2) {
            return null;
        }
    }

    public String getURL() {
        byte[] bArr;
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(NotificationService.MOOLAH_NOTIF_URL);
        try {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(new BasicNameValuePair("sig", "fdd2250930aab313d4d043a0030eff32"));
            arrayList.add(new BasicNameValuePair("device_type", "android"));
            String string = Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
            byte[] bArr2 = new byte[40];
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-1");
                instance.update(string.getBytes("iso-8859-1"), 0, string.length());
                bArr = instance.digest();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                bArr = bArr2;
            }
            arrayList.add(new BasicNameValuePair("unique_key", MoolahUtils.convertToHex(bArr)));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
            String read = MoolahUtils.read(defaultHttpClient.execute(httpPost).getEntity().getContent());
            System.out.println("");
            return read;
        } catch (ClientProtocolException e2) {
            return null;
        } catch (IOException e3) {
            System.out.println(e3);
            return null;
        }
    }
}
