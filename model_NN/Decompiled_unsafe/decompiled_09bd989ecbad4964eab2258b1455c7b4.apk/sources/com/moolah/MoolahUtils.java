package com.moolah;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import java.io.IOException;
import java.io.InputStream;

public class MoolahUtils {
    public static String read(InputStream inputStream) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        byte[] bArr = new byte[FragmentTransaction.TRANSIT_ENTER_MASK];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return stringBuffer.toString();
            }
            stringBuffer.append(new String(bArr, 0, read));
        }
    }

    public static String convertToHex(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bArr.length; i++) {
            byte b = (bArr[i] >>> 4) & 15;
            int i2 = 0;
            while (true) {
                if (b < 0 || b > 9) {
                    stringBuffer.append((char) ((b - 10) + 97));
                } else {
                    stringBuffer.append((char) (b + 48));
                }
                byte b2 = bArr[i] & 15;
                int i3 = i2 + 1;
                if (i2 >= 1) {
                    break;
                }
                i2 = i3;
                b = b2;
            }
        }
        return stringBuffer.toString();
    }

    public static String getPhoneNumber(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
    }

    public String getEmail(Context context) {
        Account[] accounts = AccountManager.get(context).getAccounts();
        if (accounts == null || accounts.length <= 0) {
            return null;
        }
        return accounts[0].name;
    }
}
