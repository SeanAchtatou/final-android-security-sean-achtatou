package com.moolah;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

public class MoolahActivity extends Activity {
    private static final String TERMINATE_STRING = "/terminate";
    LinearLayout lLayout;
    WebView webview;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTheme(16973830);
        this.lLayout = new LinearLayout(this);
        this.lLayout.setOrientation(1);
        this.lLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.webview = new WebView(this);
        this.webview.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.lLayout.addView(this.webview);
        this.webview.getSettings().setJavaScriptEnabled(true);
        this.webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        this.webview.setWebChromeClient(new MyWebChromeClient());
        String string = getIntent().getExtras().getString("URL");
        this.webview.loadUrl(string);
        if (string.trim().endsWith(TERMINATE_STRING)) {
            finish();
        }
        this.webview.setWebViewClient(new HelloWebViewClient());
        setContentView(this.lLayout);
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsConfirm(WebView webView, String str, String str2, final JsResult jsResult) {
            new AlertDialog.Builder(MoolahActivity.this).setTitle("").setMessage(str2).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    jsResult.confirm();
                }
            }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    jsResult.cancel();
                }
            }).create().show();
            return true;
        }
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!str.trim().endsWith(MoolahActivity.TERMINATE_STRING)) {
                return false;
            }
            MoolahActivity.this.finish();
            return false;
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            if (str.trim().endsWith(MoolahActivity.TERMINATE_STRING)) {
                MoolahActivity.this.finish();
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            MoolahActivity.this.finish();
        }
    }
}
