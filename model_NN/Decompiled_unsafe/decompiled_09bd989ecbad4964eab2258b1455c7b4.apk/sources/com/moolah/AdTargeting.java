package com.moolah;

public class AdTargeting {
    private String brandingImage;
    private String city;
    private String country;
    private int dayOfBirth;
    private String email;
    private String firstName;
    private Gender gender;
    private String lastName;
    private int monthOfBirth;
    private String phoneNumber;
    private String state;
    private String streetAddress;
    private int yearOfBirth;
    private String zipcode;

    public enum Gender {
        UNKNOWN,
        MALE,
        FEMALE
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String str) {
        this.firstName = str;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String str) {
        this.lastName = str;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String str) {
        this.email = str;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String str) {
        this.phoneNumber = str;
    }

    public String getStreetAddress() {
        return this.streetAddress;
    }

    public void setStreetAddress(String str) {
        this.streetAddress = str;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String str) {
        this.city = str;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String str) {
        this.state = str;
    }

    public String getZipcode() {
        return this.zipcode;
    }

    public void setZipcode(String str) {
        this.zipcode = str;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String str) {
        this.country = str;
    }

    public int getDayOfBirth() {
        return this.dayOfBirth;
    }

    public void setDayOfBirth(int i) {
        this.dayOfBirth = i;
    }

    public int getMonthOfBirth() {
        return this.monthOfBirth;
    }

    public void setMonthOfBirth(int i) {
        this.monthOfBirth = i;
    }

    public int getYearOfBirth() {
        return this.yearOfBirth;
    }

    public void setYearOfBirth(int i) {
        this.yearOfBirth = i;
    }

    public Gender getGender() {
        return this.gender;
    }

    public void setGender(Gender gender2) {
        this.gender = gender2;
    }

    public String getBrandingImage() {
        return this.brandingImage;
    }

    public void setBrandingImage(String str) {
        this.brandingImage = str;
    }
}
