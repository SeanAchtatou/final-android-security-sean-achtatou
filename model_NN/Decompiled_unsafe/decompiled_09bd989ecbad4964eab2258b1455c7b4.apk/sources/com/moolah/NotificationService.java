package com.moolah;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class NotificationService extends Service {
    public static final long DEFAULT_NEXT_TIME = 21600000;
    public static final String MARKET = "c2a";
    public static final int MAX_REGISTRATION_RETRY = 4;
    public static final String MOOLAH_LAUNCHREPORT_URL = "http://appnotify.forehandmedia.com/launch.php";
    public static final String MOOLAH_NOTIF_URL = "http://appnotify.forehandmedia.com/fetch.php";
    public static final String MOOLAH_OPT_OUT_URL = "http://appnotify.forehandmedia.com/optout.php";
    public static final String MOOLAH_REGISTRATION_URL = "http://appnotify.forehandmedia.com/livefeed.php";
    static final String NEXT_CALL_PREF = "next_call";
    static final String NEXT_NOTIFICATION_PREF = "next_notif";
    static final String NOTIFICATION_ID_PREF = "notif_ID";
    static final String NOTIFICATION_ON_PREF = "notif_on";
    static final String NOTIFICATION_PREF = "notifications";
    static final String NOTIFICATION_SIG_PREF = "notif_sig";
    public static final String PHONE = "c2c";
    static final String REGISTRATION_RETRY_PREF = "registration_retry";
    public static final long REGISTRATION_RETRY_TIME = 21600000;
    static final String REGISTRATION_TESTMODE = "test_mode";
    static final String SHARED_PREFS = "notif_prefs";
    public static final String SMS = "c2sms";
    public static final String WEB = "c2w";
    private Intent mIntent;
    private ServiceHandler mServiceHandler;
    private Looper mServiceLooper;
    String signature;

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            NotificationService.this.process();
            NotificationService.this.stopSelf(message.arg1);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        HandlerThread handlerThread = new HandlerThread("ServiceStartArguments");
        handlerThread.start();
        this.mServiceLooper = handlerThread.getLooper();
        this.mServiceHandler = new ServiceHandler(this.mServiceLooper);
    }

    public void onStart(Intent intent, int i) {
        Log.i("MoolahNotificationService", "onStart");
        this.mIntent = intent;
        Message obtainMessage = this.mServiceHandler.obtainMessage();
        obtainMessage.arg1 = i;
        this.mServiceHandler.sendMessage(obtainMessage);
    }

    public void process() {
        if (this.mIntent == null) {
            stopSelf();
        }
        String action = this.mIntent.getAction();
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, 0);
        this.signature = sharedPreferences.getString(NOTIFICATION_SIG_PREF, "");
        if (!action.equals(MessageReceiver.ACTION_START) && (sharedPreferences.getInt(NOTIFICATION_ON_PREF, 1) == 0 || !NOTIFICATION_SIG_PREF.equals(this.mIntent.getStringExtra(this.signature)))) {
            stopSelf();
        }
        if (action.equals(MessageReceiver.ACTION_BOOT)) {
            long j = sharedPreferences.getLong(NEXT_CALL_PREF, 0);
            if (j < System.currentTimeMillis()) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                j = System.currentTimeMillis() + 21600000;
                edit.putLong(NEXT_CALL_PREF, j);
                edit.commit();
            }
            registerNextCallAlarm(j);
            createNotification();
        } else if (action.equals(MessageReceiver.ACTION_NEXT_CALL)) {
            boolean json = getJSON();
            long currentTimeMillis = System.currentTimeMillis() + 21600000;
            if (json) {
                currentTimeMillis = sharedPreferences.getLong(NEXT_CALL_PREF, currentTimeMillis);
            } else {
                SharedPreferences.Editor edit2 = sharedPreferences.edit();
                edit2.putLong(NEXT_CALL_PREF, currentTimeMillis);
                edit2.commit();
            }
            registerNextCallAlarm(currentTimeMillis);
            createNotification();
        } else if (action.equals(MessageReceiver.ACTION_NEXT_NOTIFICATION)) {
            createNotification();
        } else if (action.equals(MessageReceiver.ACTION_STOP)) {
            Log.i("MoolahNotificationService", "stopping notifications");
            register(MOOLAH_OPT_OUT_URL);
            stopNextCallAlarm();
            stopNextNotificationAlarm();
            SharedPreferences.Editor edit3 = sharedPreferences.edit();
            edit3.putInt(NOTIFICATION_ON_PREF, 0);
            edit3.commit();
        } else if (action.equals(MessageReceiver.ACTION_START)) {
            if (sharedPreferences.getInt(REGISTRATION_RETRY_PREF, 0) <= 4) {
                this.signature = this.mIntent.getStringExtra(NOTIFICATION_SIG_PREF);
                int intExtra = this.mIntent.getIntExtra(REGISTRATION_TESTMODE, 0);
                SharedPreferences.Editor edit4 = sharedPreferences.edit();
                edit4.putString(NOTIFICATION_SIG_PREF, this.signature);
                edit4.putInt(REGISTRATION_TESTMODE, intExtra);
                if (sharedPreferences.getInt(NOTIFICATION_ON_PREF, 0) != 0) {
                    launchReport(MOOLAH_LAUNCHREPORT_URL);
                    stopSelf();
                } else if (!register(MOOLAH_REGISTRATION_URL)) {
                    Log.i("MoolahNotificationService", "registration failed");
                    edit4.putInt(REGISTRATION_RETRY_PREF, sharedPreferences.getInt(REGISTRATION_RETRY_PREF, 0) + 1);
                    registerRetryAlarm(System.currentTimeMillis() + 21600000);
                    edit4.commit();
                    return;
                } else {
                    edit4.putInt(NOTIFICATION_ON_PREF, 1);
                }
                edit4.commit();
                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                boolean json2 = getJSON();
                long currentTimeMillis2 = System.currentTimeMillis() + 21600000;
                if (json2) {
                    currentTimeMillis2 = sharedPreferences.getLong(NEXT_CALL_PREF, currentTimeMillis2);
                } else {
                    SharedPreferences.Editor edit5 = sharedPreferences.edit();
                    edit5.putLong(NEXT_CALL_PREF, currentTimeMillis2);
                    edit5.commit();
                }
                registerNextCallAlarm(currentTimeMillis2);
                createNotification();
            } else {
                return;
            }
        }
        stopSelf();
    }

    public boolean register(String str) {
        Log.i("MoolahNotificationService", "register");
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(str);
        try {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(new BasicNameValuePair("sig", this.signature));
            String deviceId = getDeviceId(this);
            byte[] bArr = new byte[40];
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-1");
                instance.update(deviceId.getBytes("iso-8859-1"), 0, deviceId.length());
                bArr = instance.digest();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            String networkOperator = ((TelephonyManager) getSystemService("phone")).getNetworkOperator();
            TimeZone timeZone = Calendar.getInstance().getTimeZone();
            arrayList.add(new BasicNameValuePair("unique_key", MoolahUtils.convertToHex(bArr)));
            arrayList.add(new BasicNameValuePair("app_name", getAppName(this)));
            arrayList.add(new BasicNameValuePair("device_type", "Android"));
            arrayList.add(new BasicNameValuePair("os_version", Build.VERSION.RELEASE));
            arrayList.add(new BasicNameValuePair("phone_number", MoolahUtils.getPhoneNumber(this)));
            if (networkOperator != null) {
                arrayList.add(new BasicNameValuePair("network", networkOperator));
            }
            arrayList.add(new BasicNameValuePair("phone_number", MoolahUtils.getPhoneNumber(this)));
            arrayList.add(new BasicNameValuePair("timezone", String.valueOf(timeZone.getRawOffset())));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
            try {
                if (new JSONObject(MoolahUtils.read(defaultHttpClient.execute(httpPost).getEntity().getContent())).getInt("success") == 1) {
                    return true;
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            System.out.println("");
        } catch (ClientProtocolException e3) {
            System.out.println(e3);
        } catch (IOException e4) {
            System.out.println(e4);
        }
        return false;
    }

    public boolean launchReport(String str) {
        byte[] bArr;
        Log.i("MoolahNotificationService", "launchreport");
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(str);
        try {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(new BasicNameValuePair("sig", this.signature));
            String deviceId = getDeviceId(this);
            byte[] bArr2 = new byte[40];
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-1");
                instance.update(deviceId.getBytes("iso-8859-1"), 0, deviceId.length());
                bArr = instance.digest();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                bArr = bArr2;
            }
            arrayList.add(new BasicNameValuePair("unique_key", MoolahUtils.convertToHex(bArr)));
            arrayList.add(new BasicNameValuePair("app_name", getAppName(this)));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
            defaultHttpClient.execute(httpPost).getEntity();
        } catch (ClientProtocolException e2) {
            System.out.println(e2);
        } catch (IOException e3) {
            System.out.println(e3);
        }
        return false;
    }

    public void createNotification() {
        int i;
        int i2 = 0;
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, 0);
        String string = sharedPreferences.getString(NOTIFICATION_PREF, "");
        long j = Long.MAX_VALUE;
        int i3 = -1;
        JSONArray jSONArray = new JSONArray();
        if (!TextUtils.isEmpty(string.trim())) {
            try {
                JSONArray jSONArray2 = new JSONArray(string);
                while (i2 < jSONArray2.length()) {
                    JSONObject jSONObject = (JSONObject) jSONArray2.get(i2);
                    long j2 = jSONObject.getLong("delay_display") * 1000;
                    if (j2 < System.currentTimeMillis()) {
                        createNotification(jSONObject.getString("title"), jSONObject.getString("body"), jSONObject.getString("action"), jSONObject.getString("data"));
                        i = i3;
                    } else {
                        jSONArray.put(jSONObject);
                        if (j > j2) {
                            i = i2;
                            j = j2;
                        } else {
                            i = i3;
                        }
                    }
                    i2++;
                    i3 = i;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(NOTIFICATION_PREF, jSONArray.toString());
        if (i3 >= 0) {
            registerNextNotificationAlarm(j);
        }
        edit.commit();
    }

    public String getAppName(Context context) {
        Resources resources = context.getResources();
        return resources.getText(resources.getIdentifier("app_name", "string", context.getPackageName())).toString();
    }

    public void createNotification(String str, String str2, String str3, String str4) {
        Notification notification = new Notification(17301620, str, System.currentTimeMillis());
        Intent intent = null;
        String lowerCase = str3.trim().toLowerCase();
        if (lowerCase.equals(SMS)) {
            intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + str4));
        } else if (lowerCase.equals(PHONE)) {
            return;
        } else {
            if (lowerCase.equals(WEB)) {
                intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse(str4));
            } else if (lowerCase.equals(MARKET)) {
                intent = new Intent("android.intent.action.VIEW", Uri.parse(str4));
            }
        }
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, 0);
        int i = sharedPreferences.getInt(NOTIFICATION_ID_PREF, 1334344) + 1;
        notification.when = System.currentTimeMillis();
        notification.setLatestEventInfo(this, str, str2, PendingIntent.getActivity(this, 0, intent, 0));
        notification.flags |= 16;
        ((NotificationManager) getSystemService("notification")).notify(i, notification);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt(NOTIFICATION_ID_PREF, i);
        edit.commit();
    }

    public boolean getJSON() {
        byte[] bArr;
        Log.i("MoolahNotificationService", "getting notifications");
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(MOOLAH_NOTIF_URL);
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, 0);
        try {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(new BasicNameValuePair("sig", this.signature));
            String deviceId = getDeviceId(this);
            byte[] bArr2 = new byte[40];
            try {
                MessageDigest instance = MessageDigest.getInstance("SHA-1");
                instance.update(deviceId.getBytes("iso-8859-1"), 0, deviceId.length());
                bArr = instance.digest();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                bArr = bArr2;
            }
            arrayList.add(new BasicNameValuePair("unique_key", MoolahUtils.convertToHex(bArr)));
            arrayList.add(new BasicNameValuePair(REGISTRATION_TESTMODE, String.valueOf(sharedPreferences.getInt(REGISTRATION_TESTMODE, 0))));
            arrayList.add(new BasicNameValuePair("app_name", getAppName(this)));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
            String read = MoolahUtils.read(defaultHttpClient.execute(httpPost).getEntity().getContent());
            Log.i("MoolahNotificationService", read);
            try {
                JSONObject jSONObject = new JSONObject(read);
                String string = jSONObject.getString("notification");
                String string2 = sharedPreferences.getString(NOTIFICATION_PREF, "");
                if (!TextUtils.isEmpty(string2) && !TextUtils.isEmpty(string)) {
                    JSONArray jSONArray = new JSONArray(string);
                    JSONArray jSONArray2 = new JSONArray(string2);
                    if (jSONArray2.length() > 0) {
                        for (int i = 0; i < jSONArray2.length(); i++) {
                            jSONArray.put(jSONArray2.get(i));
                        }
                    }
                    string = jSONArray.toString();
                }
                int i2 = jSONObject.getInt(NEXT_CALL_PREF);
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString(NOTIFICATION_PREF, string);
                edit.putLong(NEXT_CALL_PREF, System.currentTimeMillis() + ((long) (i2 * 1000)));
                edit.commit();
                return true;
            } catch (JSONException e2) {
                e2.printStackTrace();
                System.out.println("");
                return false;
            }
        } catch (IOException | ClientProtocolException e3) {
            return false;
        }
    }

    public void registerNextCallAlarm(long j) {
        AlarmManager alarmManager = (AlarmManager) getSystemService("alarm");
        Intent intent = new Intent(this, MessageReceiver.class);
        intent.setAction(MessageReceiver.ACTION_NEXT_CALL);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 0, intent, 0);
        alarmManager.cancel(broadcast);
        alarmManager.set(0, j, broadcast);
    }

    public void stopNextCallAlarm() {
        Intent intent = new Intent(this, MessageReceiver.class);
        intent.setAction(MessageReceiver.ACTION_NEXT_CALL);
        ((AlarmManager) getSystemService("alarm")).cancel(PendingIntent.getBroadcast(this, 0, intent, 0));
    }

    public void registerNextNotificationAlarm(long j) {
        AlarmManager alarmManager = (AlarmManager) getSystemService("alarm");
        Intent intent = new Intent(this, MessageReceiver.class);
        intent.setAction(MessageReceiver.ACTION_NEXT_NOTIFICATION);
        intent.putExtra(NOTIFICATION_SIG_PREF, this.signature);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 0, intent, 0);
        alarmManager.cancel(broadcast);
        alarmManager.set(0, j, broadcast);
    }

    public void stopNextNotificationAlarm() {
        Intent intent = new Intent(this, MessageReceiver.class);
        intent.setAction(MessageReceiver.ACTION_NEXT_NOTIFICATION);
        intent.putExtra(NOTIFICATION_SIG_PREF, this.signature);
        ((AlarmManager) getSystemService("alarm")).cancel(PendingIntent.getBroadcast(this, 0, intent, 0));
    }

    public void registerRetryAlarm(long j) {
        AlarmManager alarmManager = (AlarmManager) getSystemService("alarm");
        Intent intent = new Intent(this, MessageReceiver.class);
        intent.setAction(MessageReceiver.ACTION_START);
        intent.putExtra(NOTIFICATION_SIG_PREF, this.signature);
        PendingIntent broadcast = PendingIntent.getBroadcast(this, 0, intent, 0);
        alarmManager.cancel(broadcast);
        alarmManager.set(0, j, broadcast);
    }

    public String getDeviceId(Context context) {
        String str;
        TelephonyManager telephonyManager = (TelephonyManager) getBaseContext().getSystemService("phone");
        if (telephonyManager != null) {
            str = ("" + telephonyManager.getDeviceId()) + telephonyManager.getSimSerialNumber();
        } else {
            str = "";
        }
        return str + Settings.Secure.getString(context.getContentResolver(), "android_id");
    }
}
