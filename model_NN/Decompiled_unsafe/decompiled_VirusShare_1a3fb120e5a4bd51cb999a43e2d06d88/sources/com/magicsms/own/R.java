package com.magicsms.own;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int envoyer = 2131034114;
        public static final int message = 2131034113;
        public static final int numero = 2131034112;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int envoyer = 2130968580;
        public static final int hello = 2130968576;
        public static final int message = 2130968579;
        public static final int numero = 2130968578;
    }
}
