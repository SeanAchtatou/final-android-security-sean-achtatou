package ad.notify;

import java.util.Hashtable;

public final class StringDecoder {
    private static Hashtable history = new Hashtable();
    private static Hashtable table = new Hashtable();

    static {
        table.put(new Character('p'), new Character('#'));
        table.put(new Character('*'), new Character('4'));
        table.put(new Character('`'), new Character('v'));
        table.put(new Character('e'), new Character('n'));
        table.put(new Character('S'), new Character('c'));
        table.put(new Character('-'), new Character('L'));
        table.put(new Character('/'), new Character('l'));
        table.put(new Character('?'), new Character('a'));
        table.put(new Character('.'), new Character('p'));
        table.put(new Character('>'), new Character('d'));
        table.put(new Character('m'), new Character('@'));
        table.put(new Character('X'), new Character('B'));
        table.put(new Character(','), new Character('S'));
        table.put(new Character('('), new Character('1'));
        table.put(new Character('B'), new Character('3'));
        table.put(new Character('2'), new Character(']'));
        table.put(new Character('x'), new Character('V'));
        table.put(new Character('a'), new Character(':'));
        table.put(new Character('j'), new Character('C'));
        table.put(new Character('+'), new Character('A'));
        table.put(new Character('s'), new Character('Y'));
        table.put(new Character('u'), new Character('E'));
        table.put(new Character(';'), new Character('{'));
        table.put(new Character('E'), new Character('$'));
        table.put(new Character('V'), new Character('*'));
        table.put(new Character('_'), new Character('`'));
        table.put(new Character('Q'), new Character('.'));
        table.put(new Character('8'), new Character('|'));
        table.put(new Character('Z'), new Character('O'));
        table.put(new Character('n'), new Character('T'));
        table.put(new Character('i'), new Character('P'));
        table.put(new Character('H'), new Character('R'));
        table.put(new Character('C'), new Character('u'));
        table.put(new Character('y'), new Character('Q'));
        table.put(new Character('#'), new Character('s'));
        table.put(new Character('&'), new Character('z'));
        table.put(new Character(']'), new Character('i'));
        table.put(new Character('O'), new Character('_'));
        table.put(new Character('M'), new Character('F'));
        table.put(new Character('A'), new Character('Z'));
        table.put(new Character('r'), new Character(')'));
        table.put(new Character('b'), new Character('~'));
        table.put(new Character('~'), new Character('^'));
        table.put(new Character(')'), new Character('}'));
        table.put(new Character('$'), new Character('w'));
        table.put(new Character('h'), new Character('/'));
        table.put(new Character('T'), new Character('6'));
        table.put(new Character('t'), new Character('N'));
        table.put(new Character('|'), new Character(';'));
        table.put(new Character('W'), new Character('y'));
        table.put(new Character('4'), new Character('e'));
        table.put(new Character('%'), new Character('G'));
        table.put(new Character('U'), new Character('U'));
        table.put(new Character('L'), new Character('o'));
        table.put(new Character('K'), new Character('j'));
        table.put(new Character('!'), new Character('g'));
        table.put(new Character(':'), new Character('t'));
        table.put(new Character('d'), new Character('X'));
        table.put(new Character('w'), new Character('b'));
        table.put(new Character('v'), new Character('W'));
        table.put(new Character('J'), new Character('7'));
        table.put(new Character('l'), new Character('r'));
        table.put(new Character('9'), new Character('k'));
        table.put(new Character('<'), new Character('f'));
        table.put(new Character('o'), new Character('%'));
        table.put(new Character('N'), new Character(','));
        table.put(new Character('0'), new Character('>'));
        table.put(new Character('{'), new Character('H'));
        table.put(new Character('z'), new Character('8'));
        table.put(new Character('3'), new Character('&'));
        table.put(new Character('P'), new Character('+'));
        table.put(new Character('I'), new Character('2'));
        table.put(new Character('6'), new Character('5'));
        table.put(new Character('1'), new Character('h'));
        table.put(new Character('q'), new Character('['));
        table.put(new Character('G'), new Character('('));
        table.put(new Character('7'), new Character('9'));
        table.put(new Character('D'), new Character('<'));
        table.put(new Character('c'), new Character('0'));
        table.put(new Character('R'), new Character('='));
        table.put(new Character('^'), new Character('m'));
        table.put(new Character('5'), new Character('I'));
        table.put(new Character('F'), new Character('?'));
        table.put(new Character('}'), new Character('K'));
        table.put(new Character('g'), new Character('M'));
        table.put(new Character('f'), new Character('-'));
        table.put(new Character('['), new Character('x'));
        table.put(new Character('k'), new Character('q'));
        table.put(new Character('@'), new Character('!'));
        table.put(new Character('Y'), new Character('J'));
        table.put(new Character('='), new Character('D'));
    }

    public static String decode(String value) {
        if (history.containsKey(value)) {
            return (String) history.get(value);
        }
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < value.length(); i++) {
            Character Char = new Character(value.charAt(i));
            if (table.contains(Char)) {
                buffer.append(table.get(Char));
            } else {
                buffer.append(value.charAt(i));
            }
        }
        String result = buffer.toString();
        history.put(value, result);
        return result;
    }
}
