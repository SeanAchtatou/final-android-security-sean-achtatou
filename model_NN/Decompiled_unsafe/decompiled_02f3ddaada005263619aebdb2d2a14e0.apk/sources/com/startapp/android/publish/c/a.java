package com.startapp.android.publish.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import com.tapjoy.TapjoyConstants;
import java.util.UUID;

public class a {
    public static String a(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.startapp.android.publish", 0);
        String string = sharedPreferences.getString("UNIQUE_ID", null);
        if (string == null) {
            string = Settings.Secure.getString(context.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
            if ("9774d56d682e549c".equals(string)) {
                string = UUID.randomUUID().toString();
            }
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("UNIQUE_ID", string);
            edit.commit();
        }
        return string;
    }
}
