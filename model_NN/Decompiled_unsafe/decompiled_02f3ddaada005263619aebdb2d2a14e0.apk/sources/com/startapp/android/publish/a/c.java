package com.startapp.android.publish.a;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.startapp.android.publish.AdEventListener;
import com.startapp.android.publish.HtmlAd;
import com.startapp.android.publish.b.b;
import com.startapp.android.publish.c.e;
import com.startapp.android.publish.c.g;
import com.startapp.android.publish.model.GetAdRequest;
import java.io.FileOutputStream;

public class c extends a {
    /* access modifiers changed from: private */
    public Handler f = new Handler();
    private String g = null;

    public c(Context context, GetAdRequest getAdRequest, HtmlAd htmlAd, AdEventListener adEventListener) {
        super(context, getAdRequest, htmlAd, adEventListener);
    }

    /* access modifiers changed from: protected */
    public Object a() {
        try {
            return b.a(this.a, "http://www.startappexchange.com/1.1/gethtmlad", this.b, null);
        } catch (e e) {
            com.startapp.android.publish.c.c.a(6, "Unable to handle GetHtmlAdService command!!!!", e);
            this.e = e.getMessage();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        super.onPostExecute(bool);
        com.startapp.android.publish.c.c.a(4, "Html onPostExecute, result=[" + bool + "]");
        if (bool.booleanValue()) {
            final WebView webView = new WebView(this.a);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setWebChromeClient(new WebChromeClient());
            webView.setWebViewClient(new WebViewClient() {
                public void onPageFinished(final WebView webView, String str) {
                    super.onPageFinished(webView, str);
                    com.startapp.android.publish.c.c.a(4, "onPageFinished url=[" + str + "]");
                    c.this.f.removeCallbacksAndMessages(null);
                    c.this.f.post(new Runnable() {
                        public void run() {
                            webView.destroy();
                            if (c.this.d != null) {
                                c.this.d.onReceiveAd(c.this.c);
                            }
                        }
                    });
                }

                public void onReceivedError(final WebView webView, int i, final String str, String str2) {
                    super.onReceivedError(webView, i, str, str2);
                    com.startapp.android.publish.c.c.a(6, "onReceivedError failingUrl=[" + str2 + "], description=[" + str + "]");
                    c.this.f.removeCallbacksAndMessages(null);
                    c.this.f.post(new Runnable() {
                        public void run() {
                            webView.destroy();
                            c.this.c.setErrorMessage(str);
                            if (c.this.d != null) {
                                c.this.d.onFailedToReceiveAd(c.this.c);
                            }
                        }
                    });
                }

                public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                    com.startapp.android.publish.c.c.a(4, "shouldOverrideUrlLoading url=[" + str + "]");
                    return super.shouldOverrideUrlLoading(webView, str);
                }
            });
            com.startapp.android.publish.c.c.a(4, "Html onPostExecute load url=[" + this.g + "]");
            webView.loadUrl(this.g);
            this.f.postDelayed(new Runnable() {
                public void run() {
                    webView.destroy();
                    if (c.this.d != null) {
                        c.this.d.onReceiveAd(c.this.c);
                    }
                }
            }, 10000);
            return;
        }
        com.startapp.android.publish.c.c.a(6, "Html onPostExecute failed error=[" + this.e + "]");
    }

    /* access modifiers changed from: protected */
    public boolean a(Object obj) {
        com.startapp.android.publish.c.c.a(4, "Handle Html Response");
        String str = (String) obj;
        if (TextUtils.isEmpty(str)) {
            if (this.e == null) {
                this.e = "Empty Ad";
            }
            return false;
        }
        com.startapp.android.publish.c.c.a(4, "Preparing to save to file");
        ((HtmlAd) this.c).trackingUrl = g.a(str, "@tracking@", "@tracking@");
        ((HtmlAd) this.c).smartRedirect = Boolean.parseBoolean(g.a(str, "@smartRedirect@", "@smartRedirect@"));
        byte[] bytes = str.getBytes();
        try {
            FileOutputStream openFileOutput = this.a.openFileOutput("exit.html", 0);
            openFileOutput.write(bytes);
            openFileOutput.close();
            com.startapp.android.publish.c.c.a(4, "Saving was success");
            this.g = "file:///" + this.a.getFilesDir().getAbsolutePath() + "/" + "exit.html";
            return true;
        } catch (Exception e) {
            com.startapp.android.publish.c.c.a(6, "Failed to save to file");
            return false;
        }
    }
}
