package com.startapp.android.publish;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.startapp.android.publish.c.c;

public class AppWallDelegateActivity extends Activity {
    /* access modifiers changed from: private */
    public boolean a;

    private class a extends WebViewClient {
        private a() {
        }

        public void onPageFinished(WebView webView, String str) {
            c.a(2, "MyWebViewClient::onPageFinished - [" + str + "]");
            super.onPageFinished(webView, str);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            c.a(2, "MyWebViewClient::onPageStarted - [" + str + "]");
            super.onPageStarted(webView, str, bitmap);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            AppWallDelegateActivity.this.finish();
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            c.a(2, "MyWebViewClient::shouldOverrideUrlLoading - [" + str + "]");
            String lowerCase = str.toLowerCase();
            if (!(lowerCase.startsWith("market") || lowerCase.startsWith("http://play.google.com") || lowerCase.startsWith("https://play.google.com")) && AppWallDelegateActivity.this.a) {
                return false;
            }
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(76021760);
            AppWallDelegateActivity.this.startActivity(intent);
            if (webView != null) {
                webView.destroy();
            }
            AppWallDelegateActivity.this.finish();
            return true;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        c.a(2, "AppWallActivity::onConfigurationChanged orientation - [" + configuration.orientation + "]");
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        this.a = intent.getBooleanExtra("smartRedirect", true);
        String stringExtra = intent.getStringExtra("clickUrl");
        WebView webView = new WebView(getApplicationContext());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new a());
        webView.loadUrl(stringExtra);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        c.a(2, "AppWallActivity::onDestroy");
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        c.a(2, "AppWallActivity::onPause");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        c.a(2, "AppWallActivity::onRestart");
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        c.a(2, "AppWallActivity::onResume");
        super.onResume();
    }
}
