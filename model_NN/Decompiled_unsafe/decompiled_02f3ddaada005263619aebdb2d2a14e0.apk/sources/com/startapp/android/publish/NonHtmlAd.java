package com.startapp.android.publish;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import com.startapp.android.publish.Ad;
import com.startapp.android.publish.b.b;
import com.startapp.android.publish.c.e;
import com.startapp.android.publish.model.AdDetails;
import com.startapp.android.publish.model.AdPreferences;
import java.util.List;

public class NonHtmlAd extends Ad {
    private List<AdDetails> adsDetails = null;

    public NonHtmlAd(Context context) {
        super(context);
    }

    public boolean click(AdDetails adDetails) {
        if (getState() != Ad.AdState.READY || adDetails == null) {
            return false;
        }
        String clickUrl = adDetails.getClickUrl();
        if (TextUtils.isEmpty(clickUrl)) {
            return false;
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(clickUrl));
        intent.addFlags(335544320);
        this.context.startActivity(intent);
        return true;
    }

    public boolean clickNew(AdDetails adDetails) {
        if (getState() != Ad.AdState.READY || adDetails == null) {
            return false;
        }
        String clickUrl = adDetails.getClickUrl();
        if (TextUtils.isEmpty(clickUrl)) {
            return false;
        }
        Intent intent = new Intent(this.context, AppWallDelegateActivity.class);
        intent.addFlags(344457216);
        intent.putExtra("clickUrl", clickUrl);
        this.context.startActivity(intent);
        return true;
    }

    public List<AdDetails> getAdsDetails() {
        return this.adsDetails;
    }

    public boolean impress(AdDetails adDetails) {
        if (getState() != Ad.AdState.READY || adDetails == null) {
            return false;
        }
        final String trackingUrl = adDetails.getTrackingUrl();
        if (!TextUtils.isEmpty(trackingUrl)) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                /* renamed from: a */
                public Void doInBackground(Void... voidArr) {
                    try {
                        b.a(NonHtmlAd.this.context, trackingUrl, null);
                    } catch (e e) {
                    }
                    return null;
                }
            }.execute(new Void[0]);
        }
        return true;
    }

    public boolean load(AdPreferences adPreferences, AdEventListener adEventListener) {
        if (!super.load(adPreferences, adEventListener)) {
            return true;
        }
        new com.startapp.android.publish.a.b(this.context, this.getAdRequest, this, adEventListener).execute(new Void[0]);
        return true;
    }

    public void setAdDetails(List<AdDetails> list) {
        this.adsDetails = list;
    }
}
