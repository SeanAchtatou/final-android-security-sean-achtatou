package com.startapp.android.publish.a;

import android.content.Context;
import com.startapp.android.publish.AdEventListener;
import com.startapp.android.publish.NonHtmlAd;
import com.startapp.android.publish.c.c;
import com.startapp.android.publish.c.e;
import com.startapp.android.publish.model.BaseResponse;
import com.startapp.android.publish.model.GetAdRequest;
import com.startapp.android.publish.model.GetAdResponse;
import java.util.Map;

public class b extends a {
    public b(Context context, GetAdRequest getAdRequest, NonHtmlAd nonHtmlAd, AdEventListener adEventListener) {
        super(context, getAdRequest, nonHtmlAd, adEventListener);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        super.onPostExecute(bool);
        if (bool.booleanValue() && this.d != null) {
            this.d.onReceiveAd(this.c);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(Object obj) {
        GetAdResponse getAdResponse = (GetAdResponse) obj;
        if (obj == null) {
            this.e = "Empty Response";
            c.a(6, "Error Empty Response");
            return false;
        } else if (!getAdResponse.isValidResponse()) {
            this.e = getAdResponse.getErrorMessage();
            c.a(6, "Error msg = [" + this.e + "]");
            return false;
        } else {
            ((NonHtmlAd) this.c).setAdDetails(getAdResponse.getAdsDetails());
            boolean z = getAdResponse.getAdsDetails() != null && getAdResponse.getAdsDetails().size() > 0;
            if (z) {
                return z;
            }
            this.e = "Empty Response";
            return z;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public BaseResponse a() {
        try {
            return (GetAdResponse) com.startapp.android.publish.b.b.a(this.a, "http://www.startappexchange.com/1.1/getads", this.b, (Map<String, String>) null, GetAdResponse.class);
        } catch (e e) {
            c.a(6, "Unable to handle GetAdsSetService command!!!!", e);
            this.e = e.getMessage();
            return null;
        }
    }
}
