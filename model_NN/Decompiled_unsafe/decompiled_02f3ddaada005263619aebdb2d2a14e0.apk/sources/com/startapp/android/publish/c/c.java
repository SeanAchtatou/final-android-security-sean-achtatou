package com.startapp.android.publish.c;

import android.util.Log;

public class c {
    public static void a(int i, String str) {
        a(i, str, null);
    }

    public static void a(int i, String str, Throwable th) {
        switch (i) {
            case 2:
                Log.v("startapp", str, th);
                return;
            case 3:
                Log.d("startapp", str, th);
                return;
            case 4:
                Log.i("startapp", str, th);
                return;
            case 5:
            default:
                return;
            case 6:
                Log.e("startapp", str, th);
                return;
        }
    }
}
