package com.startapp.android.publish;

import android.content.Context;
import com.startapp.android.publish.model.AdPreferences;

public class AdsRepository {

    public static class Html {
        public static boolean getAd(Context context, AdPreferences adPreferences, AdEventListener adEventListener) {
            return new HtmlAd(context.getApplicationContext()).load(adPreferences, adEventListener);
        }
    }

    public static class NonHtml {
        public static boolean getAd(Context context, AdPreferences adPreferences, AdEventListener adEventListener) {
            return new NonHtmlAd(context.getApplicationContext()).load(adPreferences, adEventListener);
        }
    }
}
