package com.startapp.android.publish.model;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetAdResponse extends BaseResponse {
    private List<AdDetails> adsDetails;
    private String productId;
    private String publisherId;

    public void fromJson(JSONObject jSONObject) {
        super.fromJson(jSONObject);
        this.publisherId = jSONObject.optString("publisherId", null);
        this.productId = jSONObject.optString("productId", null);
        JSONArray optJSONArray = jSONObject.optJSONArray("adsDetails");
        if (optJSONArray != null) {
            this.adsDetails = new ArrayList();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < optJSONArray.length()) {
                    AdDetails adDetails = new AdDetails();
                    adDetails.fromJson((JSONObject) optJSONArray.opt(i2));
                    this.adsDetails.add(adDetails);
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public List<AdDetails> getAdsDetails() {
        return this.adsDetails;
    }

    public String getProductId() {
        return this.productId;
    }

    public String getPublisherId() {
        return this.publisherId;
    }

    public void setAdsDetails(List<AdDetails> list) {
        this.adsDetails = list;
    }

    public void setProductId(String str) {
        this.productId = str;
    }

    public void setPublisherId(String str) {
        this.publisherId = str;
    }

    public String toString() {
        return "GetAdResponse [publisherId=" + this.publisherId + ", productId=" + this.productId + ", adsDetails=" + this.adsDetails + ", toString()=" + super.toString() + "]";
    }
}
