package com.startapp.android.publish;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.startapp.android.publish.c.c;
import com.startapp.android.publish.c.e;

public class AppWallActivity extends Activity {
    boolean a = false;
    /* access modifiers changed from: private */
    public WebView b;
    /* access modifiers changed from: private */
    public ProgressDialog c = null;
    private String d;
    private String e = null;
    /* access modifiers changed from: private */
    public boolean f = true;
    private boolean g = false;
    /* access modifiers changed from: private */
    public a h = null;

    public static class a extends BroadcastReceiver {
        private Activity a = null;

        public void a(Activity activity) {
            this.a = activity;
        }

        public void onReceive(Context context, Intent intent) {
            c.a(2, "DismissActivityBroadcastReceiver::onReceive - action = [" + intent.getAction() + "]");
            if (this.a != null) {
                this.a.finish();
            }
        }
    }

    private class b extends WebViewClient {
        private b() {
        }

        public void onPageFinished(WebView webView, String str) {
            c.a(2, "MyWebViewClient::onPageFinished - [" + str + "]");
            super.onPageFinished(webView, str);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            c.a(2, "MyWebViewClient::onPageStarted - [" + str + "]");
            super.onPageStarted(webView, str, bitmap);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            c.a(2, "MyWebViewClient::onReceivedError - [" + str + "], [" + str2 + "]");
            if (AppWallActivity.this.c != null) {
                AppWallActivity.this.c.dismiss();
            }
            super.onReceivedError(webView, i, str, str2);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            c.a(2, "MyWebViewClient::shouldOverrideUrlLoading - [" + str + "]");
            String lowerCase = str.toLowerCase();
            if ((lowerCase.startsWith("market") || lowerCase.startsWith("http://play.google.com") || lowerCase.startsWith("https://play.google.com")) || !AppWallActivity.this.f) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent.addFlags(344457216);
                AppWallActivity.this.startActivity(intent);
                if (AppWallActivity.this.b != null) {
                    AppWallActivity.this.b.destroy();
                }
                if (AppWallActivity.this.c != null) {
                    AppWallActivity.this.c.dismiss();
                }
                AppWallActivity.this.finish();
                return true;
            } else if (AppWallActivity.this.c != null) {
                return false;
            } else {
                ProgressDialog unused = AppWallActivity.this.c = ProgressDialog.show(AppWallActivity.this, null, "Loading....");
                AppWallActivity.this.c.setCancelable(true);
                return false;
            }
        }
    }

    private void b() {
        Intent intent = getIntent();
        final String stringExtra = intent.getStringExtra("tracking");
        if (stringExtra != null) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                /* renamed from: a */
                public Void doInBackground(Void... voidArr) {
                    try {
                        com.startapp.android.publish.b.b.a(AppWallActivity.this, stringExtra, null);
                    } catch (e e) {
                    }
                    return null;
                }
            }.execute(new Void[0]);
        }
        this.f = intent.getBooleanExtra("smartRedirect", true);
        this.e = "file:///" + this.d + "/" + intent.getStringExtra("fileUrl");
        this.b = new WebView(getApplicationContext());
        this.b.setBackgroundColor(-8947849);
        getWindow().getDecorView().findViewById(16908290).setBackgroundColor(-8947849);
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.setWebChromeClient(new WebChromeClient());
        this.b.setVerticalScrollBarEnabled(false);
        this.b.setHorizontalScrollBarEnabled(false);
        this.b.setWebViewClient(new b());
        if (Build.VERSION.SDK_INT > 10) {
            this.b.setLayerType(1, null);
        }
        this.b.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return motionEvent.getAction() == 2;
            }
        });
        this.b.addJavascriptInterface(new JsInterface(new Runnable() {
            public void run() {
                AppWallActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        AppWallActivity.this.finish();
                    }
                });
            }
        }), "startappwall");
        this.b.loadUrl(this.e);
        setContentView(this.b);
        this.h = null;
        if (this.g) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (AppWallActivity.this.h != null) {
                        try {
                            AppWallActivity.this.unregisterReceiver(AppWallActivity.this.h);
                        } catch (Exception e) {
                            c.a(6, "AppWallActivity::onCreate::run - [" + e.getClass() + "]");
                        }
                        a unused = AppWallActivity.this.h = (a) null;
                    }
                    AppWallActivity.this.sendBroadcast(new Intent("com.startapp.android.publish.DISMISS_OVERLAY"));
                }
            }, 700);
            this.h = new a();
            this.h.a(this);
            registerReceiver(this.h, new IntentFilter("com.startapp.android.publish.DISMISS_ACTIVITY"));
        }
    }

    public void a() {
        int i = 8;
        int i2 = getResources().getConfiguration().orientation;
        int orientation = getWindowManager().getDefaultDisplay().getOrientation();
        int i3 = 9;
        if (Build.VERSION.SDK_INT <= 8) {
            i3 = 1;
            i = 0;
        }
        if (orientation == 0 || orientation == 1) {
            if (i2 == 1) {
                setRequestedOrientation(1);
            } else if (i2 == 2) {
                setRequestedOrientation(0);
            }
        } else if (orientation != 2 && orientation != 3) {
        } else {
            if (i2 == 1) {
                setRequestedOrientation(i3);
            } else if (i2 == 2) {
                setRequestedOrientation(i);
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        c.a(2, "AppWallActivity::onConfigurationChanged orientation - [" + configuration.orientation + "]");
        super.onConfigurationChanged(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c.a(2, "AppWallActivity::onCreate");
        this.g = getIntent().getBooleanExtra("overlay", false);
        if (this.g && a.a) {
            finish();
        }
        this.d = getFilesDir().getAbsolutePath();
        requestWindowFeature(1);
        a();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        c.a(2, "AppWallActivity::onDestroy");
        if (this.h != null) {
            try {
                unregisterReceiver(this.h);
            } catch (Exception e2) {
                c.a(6, "AppWallActivity::onDestroy - [" + e2.getClass() + "]");
            }
        }
        this.h = null;
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        c.a(2, "AppWallActivity::onPause");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        c.a(2, "AppWallActivity::onRestart");
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        c.a(2, "AppWallActivity::onResume");
        b();
        super.onResume();
    }
}
