package com.startapp.android.publish.model;

import org.json.JSONObject;

public interface JsonDeserializer {
    void fromJson(JSONObject jSONObject);
}
