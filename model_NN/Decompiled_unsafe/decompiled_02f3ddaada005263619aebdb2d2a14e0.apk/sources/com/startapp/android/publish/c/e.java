package com.startapp.android.publish.c;

public class e extends Exception {
    private boolean a;

    public e() {
        this.a = false;
    }

    public e(String str, Throwable th) {
        this(str, th, false);
    }

    public e(String str, Throwable th, boolean z) {
        super(str, th);
        this.a = false;
        this.a = z;
    }

    public boolean a() {
        return this.a;
    }
}
