package com.startapp.android.publish.model;

import org.json.JSONObject;

public interface JsonSerializer {
    JSONObject toJson();
}
