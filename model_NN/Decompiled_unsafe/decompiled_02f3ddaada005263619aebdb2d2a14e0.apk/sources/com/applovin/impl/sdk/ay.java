package com.applovin.impl.sdk;

import android.content.SharedPreferences;

class ay extends aq {
    ay(AppLovinSdkImpl appLovinSdkImpl) {
        super("TrackConversion", appLovinSdkImpl);
    }

    /* access modifiers changed from: private */
    public String a(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(k.b("conv", this.d));
        stringBuffer.append("?");
        stringBuffer.append("did=").append((String) this.d.a(y.c)).append("&");
        stringBuffer.append("aid=").append(str);
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public String b() {
        return (String) this.d.a(y.d);
    }

    /* access modifiers changed from: protected */
    public void c() {
        SharedPreferences.Editor edit = this.d.getSettingsManager().a().edit();
        edit.putBoolean("conversion_tracked", true);
        edit.commit();
    }

    public void run() {
        String b = b();
        this.e.d(this.c, "Tracking conversion for advertiser \"" + b + "\" and device \"" + ((String) this.d.a(y.c)) + "\"...");
        az azVar = new az(this, "RepeatTrackConversion", y.j, this.d, b);
        azVar.a(y.n);
        azVar.run();
    }
}
