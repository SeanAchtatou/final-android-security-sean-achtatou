package com.applovin.impl.sdk;

import android.content.Context;
import com.applovin.sdk.Logger;

abstract class aq implements Runnable {
    protected final String c;
    protected final AppLovinSdkImpl d;
    protected final Logger e;
    protected final Context f;

    aq(String str, AppLovinSdkImpl appLovinSdkImpl) {
        if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        }
        this.d = appLovinSdkImpl;
        this.c = str == null ? getClass().getSimpleName() : str;
        this.e = appLovinSdkImpl.getLogger();
        this.f = appLovinSdkImpl.getApplicationContext();
    }

    /* access modifiers changed from: package-private */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public void a(aq aqVar, ap apVar) {
        this.d.a().a(aqVar, apVar);
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public m f() {
        return new m(this.d);
    }
}
