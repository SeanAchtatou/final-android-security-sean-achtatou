package com.applovin.impl.sdk;

import android.os.Build;
import android.os.StrictMode;

class h {
    static void a() {
        try {
            if (Build.VERSION.SDK_INT >= 9) {
                StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
            }
        } catch (Throwable th) {
        }
    }

    static String b() {
        try {
            if (Build.VERSION.SDK_INT >= 9) {
                return Build.SERIAL;
            }
        } catch (Throwable th) {
        }
        return null;
    }
}
