package com.applovin.impl.sdk;

import android.os.PowerManager;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdUpdateListener;
import com.applovin.sdk.Logger;
import java.util.HashMap;
import java.util.Map;

class b implements AppLovinAdService {
    /* access modifiers changed from: private */
    public final AppLovinSdkImpl a;
    /* access modifiers changed from: private */
    public final Logger b;
    private final x c;
    /* access modifiers changed from: private */
    public final Map d;

    b(AppLovinSdkImpl appLovinSdkImpl) {
        if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        }
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
        this.c = new x(appLovinSdkImpl);
        this.d = new HashMap();
        this.d.put(AppLovinAdSize.BANNER, new e(AppLovinAdSize.BANNER));
        this.d.put(AppLovinAdSize.MREC, new e(AppLovinAdSize.MREC));
        this.d.put(AppLovinAdSize.INTERSTITIAL, new e(AppLovinAdSize.INTERSTITIAL));
        this.d.put(AppLovinAdSize.LEADER, new e(AppLovinAdSize.LEADER));
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAdSize appLovinAdSize, String str) {
        d dVar = new d(this, (e) this.d.get(appLovinAdSize));
        AppLovinAd b2 = this.c.b(appLovinAdSize);
        if (b2 != null) {
            this.b.d("AppLovinAdService", "Using pre-loaded ad: " + b2 + "for size " + appLovinAdSize);
            dVar.adReceived(b2);
        } else {
            ah ahVar = new ah(appLovinAdSize, dVar, this.a);
            ahVar.a(str);
            this.a.a().a(ahVar, ap.MAIN);
        }
        this.c.a(appLovinAdSize);
    }

    /* access modifiers changed from: private */
    public boolean a(AppLovinAdSize appLovinAdSize) {
        if (appLovinAdSize == AppLovinAdSize.BANNER) {
            return ((Boolean) this.a.a(y.F)).booleanValue();
        }
        if (appLovinAdSize == AppLovinAdSize.MREC) {
            return ((Boolean) this.a.a(y.H)).booleanValue();
        }
        if (appLovinAdSize == AppLovinAdSize.LEADER) {
            return ((Boolean) this.a.a(y.J)).booleanValue();
        }
        return false;
    }

    /* access modifiers changed from: private */
    public long b(AppLovinAdSize appLovinAdSize) {
        if (appLovinAdSize == AppLovinAdSize.BANNER) {
            return ((Long) this.a.a(y.G)).longValue();
        }
        if (appLovinAdSize == AppLovinAdSize.MREC) {
            return ((Long) this.a.a(y.I)).longValue();
        }
        if (appLovinAdSize == AppLovinAdSize.LEADER) {
            return ((Long) this.a.a(y.K)).longValue();
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public boolean b() {
        return ((PowerManager) this.a.getApplicationContext().getSystemService("power")).isScreenOn();
    }

    /* access modifiers changed from: private */
    public void c(AppLovinAdSize appLovinAdSize) {
        long b2 = b(appLovinAdSize);
        if (b2 > 0) {
            this.a.a().a(new f(this, appLovinAdSize), ap.MAIN, (b2 + 2) * 1000);
        }
    }

    public x a() {
        return this.c;
    }

    public void addAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener) {
        addAdUpdateListener(appLovinAdUpdateListener, AppLovinAdSize.BANNER);
    }

    public void addAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener, AppLovinAdSize appLovinAdSize) {
        boolean z;
        if (appLovinAdUpdateListener == null) {
            throw new IllegalArgumentException("No ad listener specified");
        }
        e eVar = (e) this.d.get(appLovinAdSize);
        synchronized (eVar.b) {
            if (!eVar.f.contains(appLovinAdUpdateListener)) {
                eVar.f.add(appLovinAdUpdateListener);
                z = true;
                this.b.d("AppLovinAdService", "Added update listener: " + appLovinAdUpdateListener);
            } else {
                z = false;
            }
        }
        if (z) {
            this.a.a().a(new f(this, appLovinAdSize), ap.MAIN);
        }
    }

    public void loadNextAd(AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener) {
        loadNextAd(appLovinAdSize, null, appLovinAdLoadListener);
    }

    public void loadNextAd(AppLovinAdSize appLovinAdSize, String str, AppLovinAdLoadListener appLovinAdLoadListener) {
        AppLovinAd appLovinAd;
        boolean z = true;
        if (appLovinAdSize == null) {
            throw new IllegalArgumentException("No ad size specified");
        } else if (appLovinAdLoadListener == null) {
            throw new IllegalArgumentException("No callback specified");
        } else {
            e eVar = (e) this.d.get(appLovinAdSize);
            synchronized (eVar.b) {
                if (System.currentTimeMillis() <= eVar.d) {
                    z = false;
                }
                if (eVar.c == null || z) {
                    this.b.d("AppLovinAdService", "Loading next ad...");
                    eVar.g.add(appLovinAdLoadListener);
                    if (!eVar.e) {
                        eVar.e = true;
                        a(appLovinAdSize, str);
                    }
                    appLovinAd = null;
                } else {
                    appLovinAd = eVar.c;
                }
            }
            if (appLovinAd != null) {
                appLovinAdLoadListener.adReceived(appLovinAd);
            }
        }
    }

    public void loadNextAd(String str, AppLovinAdLoadListener appLovinAdLoadListener) {
        if (str == null) {
            throw new IllegalArgumentException("No ad ID specified");
        } else if (appLovinAdLoadListener == null) {
            throw new IllegalArgumentException("No callback specified");
        } else {
            this.a.a().a(new ag(str, appLovinAdLoadListener, this.a), ap.MAIN);
        }
    }

    public void removeAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener, AppLovinAdSize appLovinAdSize) {
        if (appLovinAdUpdateListener != null) {
            e eVar = (e) this.d.get(appLovinAdSize);
            synchronized (eVar.b) {
                eVar.f.remove(appLovinAdUpdateListener);
            }
            this.b.d("AppLovinAdService", "Removed update listener: " + appLovinAdUpdateListener);
        }
    }

    public void trackAdClick(AppLovinAd appLovinAd) {
        if (appLovinAd == null) {
            throw new IllegalArgumentException("No ad specified");
        }
        this.a.a().a(new ax(appLovinAd, this.a), ap.MAIN, 500);
        e eVar = (e) this.d.get(appLovinAd.getSize());
        synchronized (eVar.b) {
            eVar.c = null;
            eVar.d = 0;
        }
    }
}
