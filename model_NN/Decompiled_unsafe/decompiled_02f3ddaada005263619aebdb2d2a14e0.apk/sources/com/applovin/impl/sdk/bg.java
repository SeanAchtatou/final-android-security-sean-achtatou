package com.applovin.impl.sdk;

import android.util.Log;
import com.applovin.sdk.Logger;
import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;

class bg extends Thread {
    private final AppLovinSdkImpl a;
    private final Logger b;
    private ServerSocket c = null;
    private volatile boolean d = false;

    bg(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
        setName("AppLovinSdk:WebServer");
    }

    public void run() {
        this.b.d("AdWebServer", "Staring applovin web server...");
        int intValue = ((Integer) this.a.a(y.A)).intValue();
        try {
            this.c = new ServerSocket(intValue);
        } catch (BindException e) {
            Log.w("AdWebServer", "Unable to bind to port " + intValue + ", a server must be already running");
        } catch (IOException e2) {
            Log.w("AdWebServer", "Unable to create server socket", e2);
        }
        while (this.c != null && !this.d) {
            try {
                this.b.d("AdWebServer", "Waiting for a client on " + intValue + "...");
                Socket accept = this.c.accept();
                this.b.d("AdWebServer", "Client accepted, rendering request...");
                this.a.a().a(new aj(accept, this.a), ap.MAIN);
            } catch (IOException e3) {
                if (!this.d) {
                    this.b.e("AdWebServer", "Web server caught IO error", e3);
                }
            }
        }
    }
}
