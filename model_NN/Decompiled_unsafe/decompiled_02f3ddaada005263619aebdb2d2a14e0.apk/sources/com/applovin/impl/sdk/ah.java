package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class ah extends aq {
    private final AppLovinAdSize a;
    private final AppLovinAdLoadListener b;
    private String g;

    ah(AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener, AppLovinSdkImpl appLovinSdkImpl) {
        super("FetchNextAd", appLovinSdkImpl);
        this.a = appLovinAdSize;
        this.b = appLovinAdLoadListener;
    }

    private List a(List list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((p) it.next()).c);
        }
        return arrayList;
    }

    private void a(ac acVar) {
        if (System.currentTimeMillis() - acVar.b("ad_session_start") > ((long) (((Integer) this.d.a(y.D)).intValue() * 60000))) {
            acVar.b("ad_session_start", System.currentTimeMillis());
            acVar.c("ad_dsp_session");
        }
    }

    private void b(Map map) {
        map.put("network", k.a(this.d));
        ac c = this.d.c();
        map.put("total_imps", String.valueOf(c.b("ad_dsp")));
        map.put("session_imps", String.valueOf(c.b("ad_dsp_session")));
    }

    private void c(Map map) {
        Map a2 = ((g) this.d.getTargetingData()).a();
        if (a2 != null && !a2.isEmpty()) {
            map.putAll(a2);
        }
    }

    private void d(Map map) {
        String str = (String) this.d.a(y.E);
        if (str.length() > 2) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    String string = jSONObject.getString(next);
                    map.put(next, string);
                    this.e.d(this.c, "Ad request parameter \"" + next + "\" overriden with \"" + string + "\"");
                }
            } catch (JSONException e) {
                this.e.e(this.c, "Unable to parse ad request parameter overrides", e);
            }
        }
    }

    private void e(Map map) {
        Map a2 = a.a(this.d);
        if (a2.isEmpty()) {
            try {
                f(a2);
                a.a(a2, this.d);
            } catch (Exception e) {
                this.e.e(this.c, "Unable to populate device information", e);
            }
        }
        map.putAll(a2);
        map.put("sdk_version", AppLovinSdkImpl.FULL_VERSION);
        String str = (String) this.d.a(y.L);
        if (str != null && str.length() > 0) {
            map.put("plugin_version", str);
        }
        map.put("accept", "inter_pages,inter_size,custom_size,launch_app");
        map.put("api_did", this.d.a(y.c));
        map.put(TapjoyConstants.TJC_APP_ID_NAME, bf.b(this.d.getApplicationContext().getPackageName(), this.d));
        map.put("sdk_key", this.d.getSdkKey());
    }

    private void f(Map map) {
        m f = f();
        q b2 = f.b();
        String str = b2.a;
        if (bf.e(str)) {
            map.put("hudid", bf.a(bf.c(str), this.d));
        }
        String str2 = b2.b;
        if (bf.e(str2)) {
            map.put("hadid", bf.a(bf.c(str2), this.d));
            map.put("adid", str2);
        }
        String str3 = b2.c;
        if (bf.e(str3)) {
            map.put("hserial", bf.a(bf.c(str3), this.d));
        }
        String str4 = b2.d;
        if (bf.e(str4)) {
            map.put("hwmac", bf.a(bf.c(str4), this.d));
        }
        String str5 = b2.i;
        if (bf.e(str5)) {
            map.put("hphone", str5);
        }
        Collection collection = b2.n;
        if (collection != null && collection.size() > 0) {
            map.put("hemails", bf.a(collection, ((Integer) this.d.a(y.r)).intValue()));
        }
        map.put("brand", bf.d(b2.g));
        map.put("carrier", bf.d(b2.k));
        map.put("cpu_speed", b2.l);
        map.put("locale", b2.m.toString());
        map.put("model", bf.d(b2.e));
        map.put("os", bf.d(b2.f));
        map.put(TapjoyConstants.TJC_PLATFORM, TapjoyConstants.TJC_DEVICE_PLATFORM_TYPE);
        if (f.e()) {
            map.put("sources", "tpa");
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        super.a();
        a(-410);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.e.e(this.c, "Unable to fetch " + this.a + " ad: server returned " + i);
        try {
            if (this.b != null) {
                this.b.failedToReceiveAd(i);
            }
        } catch (Throwable th) {
            this.e.e(this.c, "Unable process a failure to recieve an ad", th);
        }
        k.b(i, this.d);
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.g = str;
    }

    /* access modifiers changed from: protected */
    public void a(StringBuffer stringBuffer) {
        if (((Boolean) this.d.a(y.s)).booleanValue()) {
            try {
                stringBuffer.append("&apps=").append(d());
            } catch (Exception e) {
                this.e.e(this.c, "Unable to populate apps", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Map map) {
        c(map);
        e(map);
        b(map);
        if (this.g != null && this.g.length() > 0) {
            map.put("placement", this.g);
        }
        map.put(TapjoyConstants.TJC_DISPLAY_AD_SIZE, this.a.getLabel());
        map.put("format", "json");
        d(map);
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        this.d.a().a(new au(jSONObject, this.b, this.d), ap.MAIN);
        k.a(jSONObject, this.d);
    }

    /* access modifiers changed from: protected */
    public String b() {
        HashMap hashMap = new HashMap();
        a(hashMap);
        StringBuffer stringBuffer = new StringBuffer(c());
        stringBuffer.append("?").append(bf.a(hashMap));
        a(stringBuffer);
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public String c() {
        return k.b("ad", this.d);
    }

    /* access modifiers changed from: protected */
    public String d() {
        return bf.a(a(f().a()), ",", ((Integer) this.d.a(y.q)).intValue());
    }

    public void run() {
        this.e.d(this.c, "Fetching next ad...");
        ac c = this.d.c();
        c.a("ad_req");
        a(c);
        try {
            ai aiVar = new ai(this, "RepeatFetchNextAd", y.i, this.d);
            aiVar.a(y.n);
            aiVar.run();
        } catch (Throwable th) {
            this.e.e(this.c, "Unable to fetch " + this.a + " ad", th);
            a(0);
        }
    }
}
