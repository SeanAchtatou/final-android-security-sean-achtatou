package com.applovin.impl.sdk;

import java.net.Socket;

class am extends aq {
    private final Socket a;
    private final int b;
    private final String g;
    private final String h;

    public am(int i, AppLovinSdkImpl appLovinSdkImpl, Socket socket) {
        this(i, "", "text/html", appLovinSdkImpl, socket);
    }

    public am(int i, String str, String str2, AppLovinSdkImpl appLovinSdkImpl, Socket socket) {
        super("FinishWebRequest", appLovinSdkImpl);
        this.b = i;
        this.g = str;
        this.h = str2;
        this.a = socket;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0034 A[SYNTHETIC, Splitter:B:12:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003b A[Catch:{ IOException -> 0x009c }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0082 A[SYNTHETIC, Splitter:B:37:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0089 A[Catch:{ IOException -> 0x0095 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            r2 = 0
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x0099, all -> 0x007e }
            java.net.Socket r0 = r5.a     // Catch:{ IOException -> 0x0099, all -> 0x007e }
            java.io.OutputStream r0 = r0.getOutputStream()     // Catch:{ IOException -> 0x0099, all -> 0x007e }
            r3 = 2048(0x800, float:2.87E-42)
            r1.<init>(r0, r3)     // Catch:{ IOException -> 0x0099, all -> 0x007e }
            java.lang.String r0 = r5.g     // Catch:{ Exception -> 0x004d }
            if (r0 == 0) goto L_0x0041
            java.lang.String r0 = r5.g     // Catch:{ Exception -> 0x004d }
            int r0 = r0.length()     // Catch:{ Exception -> 0x004d }
            if (r0 <= 0) goto L_0x0041
            java.lang.String r0 = r5.g     // Catch:{ Exception -> 0x004d }
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x004d }
            int r2 = r5.b     // Catch:{ Exception -> 0x004d }
            int r3 = r0.length     // Catch:{ Exception -> 0x004d }
            java.lang.String r4 = r5.h     // Catch:{ Exception -> 0x004d }
            byte[] r2 = com.applovin.impl.sdk.aj.b(r2, r3, r4)     // Catch:{ Exception -> 0x004d }
            r1.write(r2)     // Catch:{ Exception -> 0x004d }
            r1.write(r0)     // Catch:{ Exception -> 0x004d }
        L_0x002f:
            r1.flush()     // Catch:{ IOException -> 0x0063 }
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x008f }
        L_0x0037:
            java.net.Socket r0 = r5.a     // Catch:{ IOException -> 0x009c }
            if (r0 == 0) goto L_0x0040
            java.net.Socket r0 = r5.a     // Catch:{ IOException -> 0x009c }
            r0.close()     // Catch:{ IOException -> 0x009c }
        L_0x0040:
            return
        L_0x0041:
            int r0 = r5.b     // Catch:{ Exception -> 0x004d }
            java.lang.String r2 = r5.h     // Catch:{ Exception -> 0x004d }
            byte[] r0 = com.applovin.impl.sdk.aj.b(r0, r2)     // Catch:{ Exception -> 0x004d }
            r1.write(r0)     // Catch:{ Exception -> 0x004d }
            goto L_0x002f
        L_0x004d:
            r0 = move-exception
            com.applovin.sdk.Logger r2 = r5.e     // Catch:{ IOException -> 0x0063 }
            java.lang.String r3 = r5.c     // Catch:{ IOException -> 0x0063 }
            java.lang.String r4 = "Unable to write body request"
            r2.e(r3, r4, r0)     // Catch:{ IOException -> 0x0063 }
            r0 = 500(0x1f4, float:7.0E-43)
            java.lang.String r2 = r5.h     // Catch:{ IOException -> 0x0063 }
            byte[] r0 = com.applovin.impl.sdk.aj.b(r0, r2)     // Catch:{ IOException -> 0x0063 }
            r1.write(r0)     // Catch:{ IOException -> 0x0063 }
            goto L_0x002f
        L_0x0063:
            r0 = move-exception
        L_0x0064:
            com.applovin.sdk.Logger r2 = r5.e     // Catch:{ all -> 0x0097 }
            java.lang.String r3 = r5.c     // Catch:{ all -> 0x0097 }
            java.lang.String r4 = "Unable complete local request"
            r2.e(r3, r4, r0)     // Catch:{ all -> 0x0097 }
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ IOException -> 0x0091 }
        L_0x0072:
            java.net.Socket r0 = r5.a     // Catch:{ IOException -> 0x007c }
            if (r0 == 0) goto L_0x0040
            java.net.Socket r0 = r5.a     // Catch:{ IOException -> 0x007c }
            r0.close()     // Catch:{ IOException -> 0x007c }
            goto L_0x0040
        L_0x007c:
            r0 = move-exception
            goto L_0x0040
        L_0x007e:
            r0 = move-exception
            r1 = r2
        L_0x0080:
            if (r1 == 0) goto L_0x0085
            r1.close()     // Catch:{ IOException -> 0x0093 }
        L_0x0085:
            java.net.Socket r1 = r5.a     // Catch:{ IOException -> 0x0095 }
            if (r1 == 0) goto L_0x008e
            java.net.Socket r1 = r5.a     // Catch:{ IOException -> 0x0095 }
            r1.close()     // Catch:{ IOException -> 0x0095 }
        L_0x008e:
            throw r0
        L_0x008f:
            r0 = move-exception
            goto L_0x0037
        L_0x0091:
            r0 = move-exception
            goto L_0x0072
        L_0x0093:
            r1 = move-exception
            goto L_0x0085
        L_0x0095:
            r1 = move-exception
            goto L_0x008e
        L_0x0097:
            r0 = move-exception
            goto L_0x0080
        L_0x0099:
            r0 = move-exception
            r1 = r2
            goto L_0x0064
        L_0x009c:
            r0 = move-exception
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.am.run():void");
    }
}
