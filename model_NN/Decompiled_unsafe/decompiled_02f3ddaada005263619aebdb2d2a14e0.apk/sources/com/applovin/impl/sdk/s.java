package com.applovin.impl.sdk;

import com.tapjoy.TapjoyConstants;
import org.json.JSONObject;

class s {
    private final String a;
    private final String b;
    private final long c;

    s(String str, String str2) {
        this(str, str2, System.currentTimeMillis());
    }

    s(String str, String str2, long j) {
        this.a = str != null ? str.substring(0, Math.min(str.length(), 400)) : "UnknownErrorCode";
        this.b = str2 != null ? str2.substring(0, Math.min(str2.length(), 400)) : "";
        this.c = j;
    }

    static s a(JSONObject jSONObject) {
        return new s(jSONObject.getString("code"), jSONObject.getString("details"), jSONObject.getLong(TapjoyConstants.TJC_TIMESTAMP) * 1000);
    }

    /* access modifiers changed from: package-private */
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("code", this.a);
        jSONObject.put("details", this.b);
        jSONObject.put(TapjoyConstants.TJC_TIMESTAMP, this.c / 1000);
        return jSONObject;
    }
}
