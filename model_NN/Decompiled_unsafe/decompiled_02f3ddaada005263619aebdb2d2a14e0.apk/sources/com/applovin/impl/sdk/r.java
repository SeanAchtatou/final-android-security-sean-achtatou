package com.applovin.impl.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import com.applovin.sdk.bootstrap.SdkBoostrapTasks;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class r {
    private final AppLovinSdkImpl a;
    private final s[] b;
    private int c = 0;

    r(AppLovinSdkImpl appLovinSdkImpl) {
        if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        }
        this.b = new s[((Integer) appLovinSdkImpl.a(y.y)).intValue()];
        this.a = appLovinSdkImpl;
    }

    private void a(s sVar) {
        synchronized (this.b) {
            this.c = (this.c + 1) % this.b.length;
            this.b[this.c] = sVar;
        }
        d();
    }

    private void d() {
        if (this.a.getApplicationContext() != null) {
            SharedPreferences.Editor edit = this.a.getSettingsManager().a().edit();
            edit.putString("errors", c().toString());
            edit.commit();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Context applicationContext = this.a.getApplicationContext();
        if (applicationContext != null) {
            try {
                JSONArray jSONArray = new JSONArray(this.a.getSettingsManager().a().getString("errors", "[]"));
                synchronized (this.b) {
                    for (int i = 0; i < jSONArray.length(); i++) {
                        a(s.a((JSONObject) jSONArray.get(i)));
                    }
                }
                SharedPreferences sharedPreferences = applicationContext.getSharedPreferences(SdkBoostrapTasks.SHARED_PREFERENCES_KEY, 0);
                String string = sharedPreferences.getString(SdkBoostrapTasks.LAST_BOOSTRAP_ERROR, "");
                if (string != null && string.length() > 0) {
                    a(new s("BoostrapError", string));
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString(SdkBoostrapTasks.LAST_BOOSTRAP_ERROR, "");
                    edit.commit();
                }
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, Throwable th) {
        s sVar;
        String str3;
        if (th != null) {
            if (th instanceof NullPointerException) {
                StackTraceElement[] stackTrace = ((NullPointerException) th).getStackTrace();
                str3 = (stackTrace == null || stackTrace.length <= 2) ? "" : stackTrace[0] + ", " + stackTrace[1] + ", " + stackTrace[2];
            } else {
                str3 = ": " + th.getMessage();
            }
            sVar = new s(th.getClass().getSimpleName(), str + ": " + str2 + str3);
        } else {
            sVar = new s("ReportedError", str + ":" + str2);
        }
        a(sVar);
    }

    /* access modifiers changed from: package-private */
    public JSONArray b() {
        JSONArray c2 = c();
        synchronized (this.b) {
            Arrays.fill(this.b, (Object) null);
            this.c = 0;
            d();
        }
        return c2;
    }

    /* access modifiers changed from: package-private */
    public void b(String str, String str2, Throwable th) {
        if (th != null) {
            str2 = str2 + ": " + th.getMessage();
        }
        a(new s("UserError", str + ": " + str2));
    }

    /* access modifiers changed from: package-private */
    public JSONArray c() {
        JSONArray jSONArray = new JSONArray();
        synchronized (this.b) {
            for (int i = 0; i < this.b.length; i++) {
                if (this.b[i] != null) {
                    try {
                        jSONArray.put(this.b[i].a());
                    } catch (JSONException e) {
                    }
                }
            }
        }
        return jSONArray;
    }
}
