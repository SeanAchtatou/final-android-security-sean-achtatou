package com.applovin.impl.sdk;

import android.support.v4.view.accessibility.AccessibilityEventCompat;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

class o extends Thread {
    final /* synthetic */ AtomicReference a;
    final /* synthetic */ CountDownLatch b;
    final /* synthetic */ m c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    o(m mVar, String str, AtomicReference atomicReference, CountDownLatch countDownLatch) {
        super(str);
        this.c = mVar;
        this.a = atomicReference;
        this.b = countDownLatch;
    }

    public void run() {
        String str = "";
        try {
            InputStream inputStream = new ProcessBuilder("/system/bin/cat", "/proc/cpuinfo").start().getInputStream();
            byte[] bArr = new byte[AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END];
            for (int read = inputStream.read(bArr); read > 0; read = inputStream.read(bArr)) {
                str = str + new String(bArr, 0, read);
            }
            inputStream.close();
            this.a.set(str);
        } catch (Exception e) {
            this.c.d.e("DataCollector", "Unable to collect CPU into", e);
        } finally {
            this.b.countDown();
        }
    }
}
