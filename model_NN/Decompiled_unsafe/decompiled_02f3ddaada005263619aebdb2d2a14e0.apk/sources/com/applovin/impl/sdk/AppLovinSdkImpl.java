package com.applovin.impl.sdk;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkSettings;
import com.applovin.sdk.AppLovinTargetingData;
import com.applovin.sdk.Logger;

public class AppLovinSdkImpl extends AppLovinSdk {
    public static final String FULL_VERSION = "5.0.0-5.0.0";
    public static final String IMPL_VERSION = "5.0.0";
    private String a;
    private AppLovinSdkSettings b;
    private Context c;
    private Logger d;
    private r e;
    private ao f;
    private ab g;
    private i h;
    private ac i;
    private bg j;
    private g k;
    private b l;
    private boolean m = true;
    private boolean n = false;
    private boolean o = false;
    private boolean p = false;
    private boolean q = false;

    private static boolean h() {
        return !Build.VERSION.RELEASE.startsWith("1.") && !Build.VERSION.RELEASE.startsWith("2.0") && !Build.VERSION.RELEASE.startsWith("2.1");
    }

    /* access modifiers changed from: package-private */
    public ao a() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public Object a(aa aaVar) {
        return this.g.a(aaVar);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.m = false;
        this.n = z;
        this.o = true;
    }

    /* access modifiers changed from: package-private */
    public r b() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public ac c() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.m = true;
        this.f.a(new an(this), 0);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        this.g.d();
        this.g.b();
        this.i.a();
    }

    public AppLovinAdService getAdService() {
        return this.l;
    }

    public Context getApplicationContext() {
        return this.c;
    }

    public i getConnectionManager() {
        return this.h;
    }

    public Logger getLogger() {
        return this.d;
    }

    public String getSdkKey() {
        return this.a;
    }

    public AppLovinSdkSettings getSettings() {
        return this.b;
    }

    public ab getSettingsManager() {
        return this.g;
    }

    public AppLovinTargetingData getTargetingData() {
        return this.k;
    }

    public boolean hasCriticalErrors() {
        return this.p || this.q;
    }

    /* access modifiers changed from: protected */
    public void initialize(String str, AppLovinSdkSettings appLovinSdkSettings, Context context) {
        this.a = str;
        this.b = appLovinSdkSettings;
        this.c = context;
        try {
            u uVar = new u();
            this.d = uVar;
            this.g = new ab(this);
            this.e = new r(this);
            this.f = new ao(this);
            this.h = new i(this);
            this.i = new ac(this);
            this.j = new bg(this);
            this.l = new b(this);
            this.k = new g(this);
            if (!h()) {
                this.p = true;
                Log.e(Logger.SDK_TAG, "Unable to initalize AppLovin SDK: Android SDK version has to be at least level 8");
            }
            if (str == null || str.length() < 1) {
                this.q = true;
                Log.e(Logger.SDK_TAG, "Unable to find AppLovin SDK key. Please add     meta-data android:name=\"applovin.sdk.key\" android:value=\"YOUR_SDK_KEY_HERE\" into AndroidManifest.xml.");
            }
            if (!hasCriticalErrors()) {
                uVar.a(this.e);
                uVar.a(this.g);
                if (appLovinSdkSettings instanceof t) {
                    uVar.a(((t) appLovinSdkSettings).a());
                }
                this.g.c();
                if (((Boolean) this.g.a(y.b)).booleanValue()) {
                    this.g.a(appLovinSdkSettings);
                    this.g.a(y.b, false);
                    this.g.b();
                }
                f();
                return;
            }
            a(false);
        } catch (Throwable th) {
            Log.e(Logger.SDK_TAG, "Failed to load AppLovin SDK, ad serving will be disabled", th);
            a(false);
        }
    }

    public void initializeSdk() {
    }

    public boolean isEnabled() {
        return this.n;
    }

    public void setPluginVersion(String str) {
        if (str == null) {
            throw new IllegalArgumentException("No version specified");
        }
        this.g.a(y.L, str);
        this.g.b();
    }
}
