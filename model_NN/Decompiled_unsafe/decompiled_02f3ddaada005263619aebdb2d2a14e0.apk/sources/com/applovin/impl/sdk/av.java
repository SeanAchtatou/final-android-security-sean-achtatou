package com.applovin.impl.sdk;

import org.json.JSONObject;

abstract class av extends aq implements j {
    /* access modifiers changed from: private */
    public int a;
    private final j b;
    private aa g;

    private av(String str, int i, AppLovinSdkImpl appLovinSdkImpl) {
        super(str, appLovinSdkImpl);
        this.g = null;
        this.a = i;
        this.b = new aw(this, appLovinSdkImpl, str);
    }

    av(String str, aa aaVar, AppLovinSdkImpl appLovinSdkImpl) {
        this(str, ((Integer) appLovinSdkImpl.a(aaVar)).intValue(), appLovinSdkImpl);
    }

    static /* synthetic */ int b(av avVar, int i) {
        int i2 = avVar.a - i;
        avVar.a = i2;
        return i2;
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.g != null) {
            ab settingsManager = this.d.getSettingsManager();
            settingsManager.a(this.g, this.g.c());
            settingsManager.b();
        }
    }

    public void a(int i) {
    }

    public void a(aa aaVar) {
        this.g = aaVar;
    }

    /* access modifiers changed from: protected */
    public abstract void a(i iVar, j jVar);

    public void a(JSONObject jSONObject, int i) {
    }

    public void run() {
        a(this.d.getConnectionManager(), this.b);
    }
}
