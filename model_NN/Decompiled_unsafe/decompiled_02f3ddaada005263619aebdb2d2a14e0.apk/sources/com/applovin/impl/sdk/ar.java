package com.applovin.impl.sdk;

import com.applovin.sdk.bootstrap.SdkBootstrap;
import java.util.concurrent.ThreadFactory;

class ar implements ThreadFactory {
    final /* synthetic */ ao a;
    private final String b;

    public ar(ao aoVar, String str) {
        this.a = aoVar;
        this.b = str;
    }

    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable, "AppLovinSdk:" + this.b + ":" + bf.a(this.a.a.getSdkKey()));
        thread.setDaemon(true);
        thread.setPriority(10);
        thread.setContextClassLoader(SdkBootstrap.getInstance(this.a.a.getApplicationContext()).getClassLoader());
        thread.setUncaughtExceptionHandler(new as(this));
        return thread;
    }
}
