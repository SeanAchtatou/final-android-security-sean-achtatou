package com.applovin.impl.sdk;

import com.applovin.sdk.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import org.json.JSONException;
import org.json.JSONObject;

class i {
    private final AppLovinSdkImpl a;
    private final Logger b;

    i(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
    }

    private int a(Throwable th) {
        if (th instanceof SocketTimeoutException) {
            return -102;
        }
        if (!(th instanceof IOException)) {
            return th instanceof JSONException ? -104 : 0;
        }
        String message = th.getMessage();
        return (message == null || !message.toLowerCase().contains("authentication challenge")) ? -100 : 401;
    }

    private HttpURLConnection a(String str, String str2, int i) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setRequestMethod(str2);
        httpURLConnection.setConnectTimeout(i < 0 ? ((Integer) this.a.a(y.w)).intValue() : i);
        if (i < 0) {
            i = ((Integer) this.a.a(y.x)).intValue();
        }
        httpURLConnection.setReadTimeout(i);
        httpURLConnection.setDefaultUseCaches(false);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setDoInput(true);
        return httpURLConnection;
    }

    private static void a(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception e) {
            }
        }
    }

    private void a(String str, int i, String str2, j jVar) {
        this.b.d("ConnectionManager", i + " received from from \"" + str2 + "\": " + str);
        if (i < 200 || i >= 300) {
            this.b.e("ConnectionManager", i + " error received from \"" + str2 + "\"");
            jVar.a(i);
            return;
        }
        JSONObject jSONObject = new JSONObject();
        if (!(i == 204 || str == null || str.length() <= 2)) {
            jSONObject = new JSONObject(str);
        }
        jVar.a(jSONObject, i);
    }

    private void a(String str, String str2, int i, long j) {
        this.b.i("ConnectionManager", "Succesfull " + str + " returned " + i + " in " + (((float) (System.currentTimeMillis() - j)) / 1000.0f) + " s" + " over " + k.a(this.a) + " to \"" + str2 + "\"");
    }

    private void a(String str, String str2, int i, long j, Throwable th) {
        this.b.e("ConnectionManager", "Failed " + str + " returned " + i + " in " + (((float) (System.currentTimeMillis() - j)) / 1000.0f) + " s" + " over " + k.a(this.a) + " to \"" + str2 + "\"", th);
    }

    private static void a(HttpURLConnection httpURLConnection) {
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, int i, j jVar) {
        a(str, "GET", i, (JSONObject) null, jVar);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, j jVar) {
        a(str, -1, jVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.k.a(boolean, android.content.Context):boolean
     arg types: [int, android.content.Context]
     candidates:
      com.applovin.impl.sdk.k.a(java.lang.String, com.applovin.impl.sdk.AppLovinSdkImpl):java.lang.String
      com.applovin.impl.sdk.k.a(int, com.applovin.impl.sdk.AppLovinSdkImpl):void
      com.applovin.impl.sdk.k.a(org.json.JSONObject, com.applovin.impl.sdk.AppLovinSdkImpl):void
      com.applovin.impl.sdk.k.a(int, int[]):boolean
      com.applovin.impl.sdk.k.a(boolean, android.content.Context):boolean */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e6 A[SYNTHETIC, Splitter:B:32:0x00e6] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r11, java.lang.String r12, int r13, org.json.JSONObject r14, com.applovin.impl.sdk.j r15) {
        /*
            r10 = this;
            r3 = 0
            r7 = 0
            if (r11 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No endpoint specified"
            r0.<init>(r1)
            throw r0
        L_0x000c:
            if (r12 != 0) goto L_0x0016
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No method specified"
            r0.<init>(r1)
            throw r0
        L_0x0016:
            if (r15 != 0) goto L_0x0020
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No callback specified"
            r0.<init>(r1)
            throw r0
        L_0x0020:
            com.applovin.impl.sdk.AppLovinSdkImpl r0 = r10.a
            android.content.Context r0 = r0.getApplicationContext()
            boolean r0 = com.applovin.impl.sdk.k.a(r3, r0)
            if (r0 == 0) goto L_0x0103
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.String r0 = ""
            com.applovin.sdk.Logger r0 = r10.b     // Catch:{ Throwable -> 0x00e2, all -> 0x00fa }
            java.lang.String r1 = "ConnectionManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00e2, all -> 0x00fa }
            r2.<init>()     // Catch:{ Throwable -> 0x00e2, all -> 0x00fa }
            java.lang.String r6 = "Sending "
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Throwable -> 0x00e2, all -> 0x00fa }
            java.lang.StringBuilder r2 = r2.append(r12)     // Catch:{ Throwable -> 0x00e2, all -> 0x00fa }
            java.lang.String r6 = " request to \""
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Throwable -> 0x00e2, all -> 0x00fa }
            java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ Throwable -> 0x00e2, all -> 0x00fa }
            java.lang.String r6 = "\"..."
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Throwable -> 0x00e2, all -> 0x00fa }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x00e2, all -> 0x00fa }
            r0.i(r1, r2)     // Catch:{ Throwable -> 0x00e2, all -> 0x00fa }
            java.net.HttpURLConnection r8 = r10.a(r11, r12, r13)     // Catch:{ Throwable -> 0x00e2, all -> 0x00fa }
            if (r14 == 0) goto L_0x00b3
            java.lang.String r0 = r14.toString()     // Catch:{ Throwable -> 0x010e }
            com.applovin.sdk.Logger r1 = r10.b     // Catch:{ Throwable -> 0x010e }
            java.lang.String r2 = "ConnectionManager"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x010e }
            r6.<init>()     // Catch:{ Throwable -> 0x010e }
            java.lang.String r9 = "Request to \""
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ Throwable -> 0x010e }
            java.lang.StringBuilder r6 = r6.append(r11)     // Catch:{ Throwable -> 0x010e }
            java.lang.String r9 = "\" is "
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ Throwable -> 0x010e }
            java.lang.StringBuilder r6 = r6.append(r0)     // Catch:{ Throwable -> 0x010e }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x010e }
            r1.d(r2, r6)     // Catch:{ Throwable -> 0x010e }
            java.lang.String r1 = "Content-Type"
            java.lang.String r2 = "application/json; charset=utf-8"
            r8.setRequestProperty(r1, r2)     // Catch:{ Throwable -> 0x010e }
            r1 = 1
            r8.setDoOutput(r1)     // Catch:{ Throwable -> 0x010e }
            byte[] r1 = r0.getBytes()     // Catch:{ Throwable -> 0x010e }
            int r1 = r1.length     // Catch:{ Throwable -> 0x010e }
            r8.setFixedLengthStreamingMode(r1)     // Catch:{ Throwable -> 0x010e }
            java.io.PrintWriter r1 = new java.io.PrintWriter     // Catch:{ Throwable -> 0x010e }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ Throwable -> 0x010e }
            java.io.OutputStream r6 = r8.getOutputStream()     // Catch:{ Throwable -> 0x010e }
            java.lang.String r9 = "UTF8"
            r2.<init>(r6, r9)     // Catch:{ Throwable -> 0x010e }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x010e }
            r1.print(r0)     // Catch:{ Throwable -> 0x010e }
            r1.close()     // Catch:{ Throwable -> 0x010e }
        L_0x00b3:
            int r3 = r8.getResponseCode()     // Catch:{ Throwable -> 0x010e }
            if (r3 <= 0) goto L_0x00d6
            java.io.InputStream r6 = r8.getInputStream()     // Catch:{ Throwable -> 0x010e }
            java.lang.String r7 = com.applovin.impl.sdk.k.a(r6)     // Catch:{ Throwable -> 0x0110, all -> 0x010b }
            r0 = r10
            r1 = r12
            r2 = r11
            r0.a(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x0110, all -> 0x010b }
            int r0 = r8.getResponseCode()     // Catch:{ Throwable -> 0x0110, all -> 0x010b }
            r10.a(r7, r0, r11, r15)     // Catch:{ Throwable -> 0x0110, all -> 0x010b }
            r0 = r6
        L_0x00cf:
            a(r0)
            a(r8)
        L_0x00d5:
            return
        L_0x00d6:
            r6 = 0
            r0 = r10
            r1 = r12
            r2 = r11
            r0.a(r1, r2, r3, r4, r6)     // Catch:{ Throwable -> 0x010e }
            r15.a(r3)     // Catch:{ Throwable -> 0x010e }
            r0 = r7
            goto L_0x00cf
        L_0x00e2:
            r6 = move-exception
            r8 = r7
        L_0x00e4:
            if (r3 != 0) goto L_0x00ea
            int r3 = r10.a(r6)     // Catch:{ all -> 0x0109 }
        L_0x00ea:
            r0 = r10
            r1 = r12
            r2 = r11
            r0.a(r1, r2, r3, r4, r6)     // Catch:{ all -> 0x0109 }
            r15.a(r3)     // Catch:{ all -> 0x0109 }
            a(r7)
            a(r8)
            goto L_0x00d5
        L_0x00fa:
            r0 = move-exception
            r8 = r7
        L_0x00fc:
            a(r7)
            a(r8)
            throw r0
        L_0x0103:
            r0 = -103(0xffffffffffffff99, float:NaN)
            r15.a(r0)
            goto L_0x00d5
        L_0x0109:
            r0 = move-exception
            goto L_0x00fc
        L_0x010b:
            r0 = move-exception
            r7 = r6
            goto L_0x00fc
        L_0x010e:
            r6 = move-exception
            goto L_0x00e4
        L_0x0110:
            r0 = move-exception
            r7 = r6
            r6 = r0
            goto L_0x00e4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.i.a(java.lang.String, java.lang.String, int, org.json.JSONObject, com.applovin.impl.sdk.j):void");
    }

    /* access modifiers changed from: package-private */
    public void a(String str, JSONObject jSONObject, j jVar) {
        a(str, "PUT", -1, jSONObject, jVar);
    }

    /* access modifiers changed from: package-private */
    public void b(String str, JSONObject jSONObject, j jVar) {
        a(str, "POST", -1, jSONObject, jVar);
    }
}
