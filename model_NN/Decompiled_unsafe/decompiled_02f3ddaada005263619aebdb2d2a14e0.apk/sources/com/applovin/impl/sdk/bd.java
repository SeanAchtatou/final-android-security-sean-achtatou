package com.applovin.impl.sdk;

import com.applovin.sdk.Logger;
import java.util.HashMap;
import java.util.Map;

class bd {
    private final CharSequence a;
    private final Logger b;
    private final Map c = new HashMap(1);
    private be d;

    public bd(CharSequence charSequence, Logger logger) {
        if (charSequence == null) {
            throw new IllegalArgumentException("No template specified");
        } else if (logger == null) {
            throw new IllegalArgumentException("No logger specified");
        } else {
            this.a = charSequence;
            this.b = logger;
        }
    }

    private String a(String str) {
        String lowerCase;
        String str2;
        int indexOf = str.indexOf(58);
        if (indexOf > 0) {
            lowerCase = str.substring(0, indexOf).trim();
            str2 = indexOf + 1 < str.length() ? str.substring(indexOf + 1).trim() : "";
        } else {
            lowerCase = str.trim().toLowerCase();
            str2 = null;
        }
        if (this.d != null) {
            String a2 = this.d.a(lowerCase);
            this.b.d("TemplateProcessor", lowerCase + " was resolved to \"" + (a2 != null ? a2.substring(0, Math.min(a2.length(), 30)) : "") + "\"");
            if (a2 != null) {
                return a2;
            }
        }
        String str3 = (String) this.c.get(lowerCase);
        if (str3 != null) {
            return str3;
        }
        String str4 = (String) this.c.get(lowerCase.toLowerCase());
        return str4 == null ? str2 : str4;
    }

    public String a() {
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        for (int i = 0; i < this.a.length(); i++) {
            char charAt = this.a.charAt(i);
            if (!z3) {
                if (charAt == '<') {
                    if (!z2) {
                        z2 = true;
                    } else {
                        stringBuffer.append("<");
                        z2 = false;
                    }
                } else if (charAt != '%') {
                    if (z2) {
                        stringBuffer.append("<");
                        z2 = false;
                    }
                    stringBuffer.append(charAt);
                } else if (z2) {
                    z2 = false;
                    z3 = true;
                } else {
                    stringBuffer.append(charAt);
                }
            } else if (charAt == '%') {
                if (!z) {
                    z = true;
                } else {
                    stringBuffer2.append("%");
                    z = false;
                }
            } else if (charAt != '>') {
                stringBuffer2.append(charAt);
            } else if (z) {
                String stringBuffer3 = stringBuffer2.toString();
                stringBuffer2.setLength(0);
                String a2 = a(stringBuffer3);
                if (a2 != null) {
                    stringBuffer.append(a2);
                } else {
                    stringBuffer.append("");
                    this.b.w("TemplateProcessor", "Unable to resolve \"" + stringBuffer3 + "\", using empty string");
                }
                z = false;
                z3 = false;
            } else {
                stringBuffer2.append(">");
            }
        }
        if (z2) {
            stringBuffer.append("<");
        }
        if (z3) {
            stringBuffer.append("<%").append(stringBuffer2);
            if (z) {
                stringBuffer.append("%");
            }
        }
        return stringBuffer.toString();
    }

    public void a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("No name specified");
        }
        this.c.put(str, str2);
    }
}
