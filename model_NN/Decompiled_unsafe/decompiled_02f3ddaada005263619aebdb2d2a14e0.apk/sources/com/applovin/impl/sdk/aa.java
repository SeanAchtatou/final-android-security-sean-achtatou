package com.applovin.impl.sdk;

public class aa {
    private static int a = 0;
    private final int b;
    private final String c;
    private final Object d;

    private aa(String str, Object obj) {
        if (str == null) {
            throw new IllegalArgumentException("No name specified");
        } else if (obj == null) {
            throw new IllegalArgumentException("No default value specified");
        } else {
            this.c = str;
            this.d = obj;
            this.b = a;
            a++;
        }
    }

    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public Object a(Object obj) {
        return this.d.getClass().cast(obj);
    }

    public String b() {
        return this.c;
    }

    public Object c() {
        return this.d;
    }
}
