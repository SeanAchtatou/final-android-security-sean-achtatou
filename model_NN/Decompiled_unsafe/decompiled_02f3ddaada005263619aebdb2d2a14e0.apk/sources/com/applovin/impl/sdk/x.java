package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.Logger;
import java.util.HashMap;
import java.util.Map;

public class x implements AppLovinAdLoadListener {
    private final AppLovinSdkImpl a;
    private final Logger b;
    private final String c = "PreloadManager";
    private final Map d;
    private final Object e;

    public x(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
        this.e = new Object();
        this.d = new HashMap();
    }

    public void a(AppLovinAdSize appLovinAdSize) {
        if (((Boolean) this.a.a(y.M)).booleanValue()) {
            this.b.d("PreloadManager", "Preloading ad for size " + appLovinAdSize + "...");
            this.a.a().a(new ah(appLovinAdSize, this, this.a), ap.BACKGROUND, 500);
        }
    }

    public void adReceived(AppLovinAd appLovinAd) {
        synchronized (this.e) {
            this.d.put(appLovinAd.getSize(), appLovinAd);
        }
        this.b.d("PreloadManager", "Pulled ad from network and saved to preload cache: " + appLovinAd);
    }

    public AppLovinAd b(AppLovinAdSize appLovinAdSize) {
        AppLovinAd appLovinAd;
        synchronized (this.e) {
            appLovinAd = (AppLovinAd) this.d.remove(appLovinAdSize);
        }
        return appLovinAd;
    }

    public void failedToReceiveAd(int i) {
        this.b.d("PreloadManager", "Failed to pre-load an ad, error code " + i);
    }
}
