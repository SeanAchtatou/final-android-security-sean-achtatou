package com.applovin.impl.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import com.applovin.sdk.AppLovinSdkSettings;
import com.applovin.sdk.Logger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class ab {
    private final AppLovinSdkImpl a;
    private final Logger b;
    private final Context c;
    private final Object[] d = new Object[y.b()];

    ab(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
        this.c = appLovinSdkImpl.getApplicationContext();
    }

    private static aa a(String str) {
        for (aa aaVar : y.a()) {
            if (aaVar.b().equals(str)) {
                return aaVar;
            }
        }
        return null;
    }

    private static Object a(String str, JSONObject jSONObject, Object obj) {
        if (obj instanceof Boolean) {
            return Boolean.valueOf(jSONObject.getBoolean(str));
        }
        if (obj instanceof Float) {
            return Float.valueOf((float) jSONObject.getDouble(str));
        }
        if (obj instanceof Integer) {
            return Integer.valueOf(jSONObject.getInt(str));
        }
        if (obj instanceof Long) {
            return Long.valueOf(jSONObject.getLong(str));
        }
        if (obj instanceof String) {
            return jSONObject.getString(str);
        }
        throw new RuntimeException("SDK Error: unknown value type: " + obj.getClass());
    }

    private String e() {
        return "com.applovin.sdk." + bf.a(this.a.getSdkKey()) + ".";
    }

    public SharedPreferences a() {
        if (this.c != null) {
            return this.c.getSharedPreferences("com.applovin.sdk.1", 0);
        }
        throw new IllegalArgumentException("No context specified");
    }

    public Object a(aa aaVar) {
        Object a2;
        if (aaVar == null) {
            throw new IllegalArgumentException("No setting type specified");
        }
        synchronized (this.d) {
            Object obj = this.d[aaVar.a()];
            a2 = obj != null ? aaVar.a(obj) : aaVar.c();
        }
        return a2;
    }

    public void a(aa aaVar, Object obj) {
        if (aaVar == null) {
            throw new IllegalArgumentException("No setting type specified");
        } else if (obj == null) {
            throw new IllegalArgumentException("No new value specified");
        } else {
            synchronized (this.d) {
                this.d[aaVar.a()] = obj;
            }
            this.b.d("SettingsManager", "Setting update: " + aaVar.b() + " set to \"" + obj + "\"");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: package-private */
    public void a(AppLovinSdkSettings appLovinSdkSettings) {
        long j = 0;
        this.b.i("SettingsManager", "Loading user-defined settings...");
        if (appLovinSdkSettings != null) {
            synchronized (this.d) {
                this.d[y.l.a()] = Boolean.valueOf(appLovinSdkSettings.isVerboseLoggingEnabled());
                long bannerAdRefreshSeconds = appLovinSdkSettings.getBannerAdRefreshSeconds();
                if (bannerAdRefreshSeconds >= 0) {
                    if (bannerAdRefreshSeconds > 0) {
                        j = Math.max(30L, bannerAdRefreshSeconds);
                    }
                    this.d[y.G.a()] = Long.valueOf(j);
                    this.d[y.F.a()] = true;
                } else if (bannerAdRefreshSeconds == -1) {
                    this.d[y.F.a()] = false;
                }
                this.d[y.P.a()] = appLovinSdkSettings.getAutoPreloadSizes();
                if (appLovinSdkSettings instanceof t) {
                    for (Map.Entry entry : ((t) appLovinSdkSettings).b().entrySet()) {
                        this.d[((aa) entry.getKey()).a()] = entry.getValue();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
        this.b.d("SettingsManager", "Loading settings from JSON array...");
        synchronized (this.d) {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next != null && next.length() > 0) {
                    try {
                        aa a2 = a(next);
                        if (a2 != null) {
                            Object a3 = a(next, jSONObject, a2.c());
                            this.d[a2.a()] = a3;
                            this.b.d("SettingsManager", "Setting update: " + a2.b() + " set to \"" + a3 + "\"");
                        } else {
                            this.b.w("SettingsManager", "Unknown setting recieved: " + next);
                        }
                    } catch (JSONException e) {
                        this.b.e("SettingsManager", "Unable to parse JSON settings array", e);
                    } catch (Throwable th) {
                        this.b.e("SettingsManager", "Unable to convert setting object ", th);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.c == null) {
            throw new IllegalArgumentException("No context specified");
        }
        this.b.i("SettingsManager", "Saving settings with the application...");
        String e = e();
        SharedPreferences.Editor edit = a().edit();
        synchronized (this.d) {
            for (aa aaVar : y.a()) {
                Object obj = this.d[aaVar.a()];
                if (obj != null) {
                    String str = e + aaVar.b();
                    if (obj instanceof Boolean) {
                        edit.putBoolean(str, ((Boolean) obj).booleanValue());
                    } else if (obj instanceof Float) {
                        edit.putFloat(str, ((Float) obj).floatValue());
                    } else if (obj instanceof Integer) {
                        edit.putInt(str, ((Integer) obj).intValue());
                    } else if (obj instanceof Long) {
                        edit.putLong(str, ((Long) obj).longValue());
                    } else if (obj instanceof String) {
                        edit.putString(str, (String) obj);
                    } else {
                        throw new RuntimeException("SDK Error: unknown value: " + obj.getClass());
                    }
                }
            }
        }
        edit.commit();
        this.b.d("SettingsManager", "Settings saved with the application.");
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Object string;
        if (this.c == null) {
            throw new IllegalArgumentException("No context specified");
        }
        this.b.i("SettingsManager", "Loading settings saved with the application...");
        String e = e();
        SharedPreferences a2 = a();
        synchronized (this.d) {
            for (aa aaVar : y.a()) {
                try {
                    String str = e + aaVar.b();
                    Object c2 = aaVar.c();
                    if (c2 instanceof Boolean) {
                        string = Boolean.valueOf(a2.getBoolean(str, ((Boolean) c2).booleanValue()));
                    } else if (c2 instanceof Float) {
                        string = Float.valueOf(a2.getFloat(str, ((Float) c2).floatValue()));
                    } else if (c2 instanceof Integer) {
                        string = Integer.valueOf(a2.getInt(str, ((Integer) c2).intValue()));
                    } else if (c2 instanceof Long) {
                        string = Long.valueOf(a2.getLong(str, ((Long) c2).longValue()));
                    } else if (c2 instanceof String) {
                        string = a2.getString(str, (String) c2);
                    } else {
                        throw new RuntimeException("SDK Error: unknown value: " + c2.getClass());
                    }
                    this.d[aaVar.a()] = string;
                } catch (Exception e2) {
                    this.b.e("SettingsManager", "Unable to load \"" + aaVar.b() + "\"", e2);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        synchronized (this.d) {
            Arrays.fill(this.d, (Object) null);
        }
        SharedPreferences.Editor edit = a().edit();
        edit.clear();
        edit.commit();
    }
}
