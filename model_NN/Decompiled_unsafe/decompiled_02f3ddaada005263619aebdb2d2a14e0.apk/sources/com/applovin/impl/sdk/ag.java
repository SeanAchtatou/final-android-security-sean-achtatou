package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import java.util.Map;

public class ag extends ah {
    private final String a;

    ag(String str, AppLovinAdLoadListener appLovinAdLoadListener, AppLovinSdkImpl appLovinSdkImpl) {
        super(AppLovinAdSize.BANNER, appLovinAdLoadListener, appLovinSdkImpl);
        this.a = str;
    }

    /* access modifiers changed from: protected */
    public void a(Map map) {
        map.put("inter_id", String.valueOf(this.a));
    }

    public /* bridge */ /* synthetic */ void run() {
        super.run();
    }
}
