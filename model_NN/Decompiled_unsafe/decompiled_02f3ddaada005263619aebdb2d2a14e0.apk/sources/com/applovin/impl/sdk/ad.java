package com.applovin.impl.sdk;

import com.tapjoy.TapjoyConstants;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ad extends aq {
    ad(AppLovinSdkImpl appLovinSdkImpl) {
        super("SubmitData", appLovinSdkImpl);
    }

    static JSONArray a(Collection collection) {
        JSONArray jSONArray = new JSONArray();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            p pVar = (p) it.next();
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("package_name", pVar.c);
            jSONObject.put("created_at", pVar.d / 1000);
            jSONArray.put(jSONObject);
        }
        return jSONArray;
    }

    static JSONObject a(Map map) {
        JSONObject jSONObject = new JSONObject();
        for (Map.Entry entry : map.entrySet()) {
            jSONObject.put((String) entry.getKey(), entry.getValue());
        }
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        try {
            JSONObject a = k.a(jSONObject);
            ab settingsManager = this.d.getSettingsManager();
            settingsManager.a(y.c, a.getString("device_id"));
            settingsManager.a(y.f, a.getString("device_token"));
            settingsManager.a(y.d, a.getString("publisher_id"));
            settingsManager.a(y.e, a.getString(TapjoyConstants.TJC_APP_ID_NAME));
            settingsManager.b();
            k.a(a, this.d);
            if (a.has("adserver_parameters")) {
                settingsManager.a(y.E, a.getJSONObject("adserver_parameters").toString());
            }
            if (((Boolean) this.d.a(y.z)).booleanValue()) {
                a_();
            }
        } catch (JSONException e) {
            this.e.e(this.c, "Unable to parse API response", e);
        }
    }

    /* access modifiers changed from: protected */
    public void a_() {
        this.d.a().a(new bb(this.d), ap.BACKGROUND);
    }

    /* access modifiers changed from: protected */
    public Collection b() {
        if (!((Boolean) this.d.a(y.s)).booleanValue() || !bf.a(y.t, this.d)) {
            return null;
        }
        return f().a();
    }

    /* access modifiers changed from: protected */
    public void b(JSONObject jSONObject) {
        m f = f();
        p c = f.c();
        q b = f.b();
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("model", b.e);
        jSONObject2.put("os", b.f);
        jSONObject2.put("brand", b.g);
        jSONObject2.put("sdk_version", b.h);
        jSONObject2.put("phone_number", b.i);
        jSONObject2.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, b.j);
        jSONObject2.put("carrier", b.k);
        jSONObject2.put("cpu_speed", b.l);
        if (b.n != null) {
            JSONArray jSONArray = new JSONArray();
            for (String put : b.n) {
                jSONArray.put(put);
            }
            jSONObject2.put("emails", jSONArray);
        }
        jSONObject2.put("type", TapjoyConstants.TJC_DEVICE_PLATFORM_TYPE);
        jSONObject2.put("adid", b.b);
        jSONObject2.put("h_android_id", bf.a(bf.c(b.b), this.d));
        jSONObject2.put("h_serial_id", bf.a(bf.c(b.c), this.d));
        jSONObject2.put("h_wifi_mac", bf.a(bf.c(b.d), this.d));
        jSONObject2.put("h_udid", bf.a(bf.c(b.a), this.d));
        jSONObject2.put("h_nn_android_id", bf.a(b.b, this.d));
        jSONObject2.put("h_nn_serial_id", bf.a(b.c, this.d));
        jSONObject2.put("h_nn_wifi_mac", bf.a(b.d, this.d));
        jSONObject2.put("h_nn_udid", bf.a(b.a, this.d));
        Locale locale = b.m;
        if (locale != null) {
            jSONObject2.put("locale", locale.toString());
        }
        jSONObject.put("device_info", jSONObject2);
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put("package_name", c.c);
        jSONObject3.put("app_name", c.a);
        jSONObject3.put(TapjoyConstants.TJC_APP_VERSION_NAME, c.b);
        jSONObject3.put("created_at", c.d / 1000);
        jSONObject3.put("applovin_sdk_version", AppLovinSdkImpl.FULL_VERSION);
        jSONObject.put("app_info", jSONObject3);
        Map a = ((g) this.d.getTargetingData()).a();
        if (a != null && !a.isEmpty()) {
            jSONObject.put("targeting", a(a));
        }
        jSONObject.put("errors", this.d.b().b());
        jSONObject.put("stats", this.d.c().b());
    }

    /* access modifiers changed from: protected */
    public void c(JSONObject jSONObject) {
        ae aeVar = new ae(this, "Repeat" + this.c, y.h, this.d, jSONObject);
        aeVar.a(y.m);
        aeVar.run();
    }

    public void run() {
        this.e.i(this.c, "Submiting user data...");
        JSONObject jSONObject = new JSONObject();
        try {
            b(jSONObject);
            Collection b = b();
            if (b != null) {
                jSONObject.put("apps", a(b));
            }
            c(jSONObject);
        } catch (JSONException e) {
            this.e.e(this.c, "Unable to create JSON message with collected data", e);
        }
    }
}
