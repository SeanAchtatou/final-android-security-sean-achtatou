package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.tapjoy.TapjoyConstants;
import java.util.List;
import java.util.Map;

class ak extends ah {
    final /* synthetic */ List a;
    final /* synthetic */ aj b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ak(aj ajVar, AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener, AppLovinSdkImpl appLovinSdkImpl, List list) {
        super(appLovinAdSize, appLovinAdLoadListener, appLovinSdkImpl);
        this.b = ajVar;
        this.a = list;
    }

    /* access modifiers changed from: protected */
    public void a(Map map) {
        super.a(map);
        map.put("p", aj.b("p", this.a));
        map.put(TapjoyConstants.TJC_APP_ID_NAME, aj.b(TapjoyConstants.TJC_APP_ID_NAME, this.a));
        map.put("api_key", "");
    }
}
