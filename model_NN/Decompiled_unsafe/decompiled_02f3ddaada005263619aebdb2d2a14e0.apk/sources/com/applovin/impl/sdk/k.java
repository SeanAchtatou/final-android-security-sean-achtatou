package com.applovin.impl.sdk;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import java.io.InputStream;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class k {
    private static final int[] a = {7, 4, 2, 1, 11};
    private static final int[] b = {5, 6, 10, 3, 9, 8, 14};
    private static final int[] c = {15, 12, 13};
    private static final String d = k.class.getSimpleName();

    private static NetworkInfo a(Context context) {
        ConnectivityManager connectivityManager;
        if (!m.a("android.permission.ACCESS_NETWORK_STATE", context) || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null) {
            return null;
        }
        return connectivityManager.getActiveNetworkInfo();
    }

    static String a(AppLovinSdkImpl appLovinSdkImpl) {
        NetworkInfo a2 = a(appLovinSdkImpl.getApplicationContext());
        if (a2 == null) {
            return "unknown";
        }
        int type = a2 != null ? a2.getType() : Integer.MAX_VALUE;
        int subtype = a2 != null ? a2.getSubtype() : 0;
        String str = type == 1 ? "wifi" : type == 0 ? a(subtype, a) ? "2g" : a(subtype, b) ? "3g" : a(subtype, c) ? "4g" : "mobile" : "unknown";
        appLovinSdkImpl.getLogger().d(d, "Network " + type + "/" + subtype + " resolved to " + str);
        return str;
    }

    static String a(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        Scanner scanner = new Scanner(inputStream);
        StringBuffer stringBuffer = new StringBuffer();
        while (scanner.hasNextLine()) {
            stringBuffer.append(scanner.nextLine());
        }
        return stringBuffer.toString();
    }

    static String a(String str, AppLovinSdkImpl appLovinSdkImpl) {
        if (str == null) {
            throw new IllegalArgumentException("No endpoint specified");
        } else if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else {
            String str2 = (String) appLovinSdkImpl.a(y.f);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append((String) appLovinSdkImpl.a(y.m));
            stringBuffer.append(str);
            if (str2 == null || str2.length() <= 0) {
                stringBuffer.append("?api_key=");
                stringBuffer.append(appLovinSdkImpl.getSdkKey());
            } else {
                stringBuffer.append("?device_token=");
                stringBuffer.append(str2);
            }
            return stringBuffer.toString();
        }
    }

    static JSONObject a(JSONObject jSONObject) {
        return (JSONObject) jSONObject.getJSONArray("results").get(0);
    }

    static void a(int i, AppLovinSdkImpl appLovinSdkImpl) {
        ab settingsManager = appLovinSdkImpl.getSettingsManager();
        if (i == 401) {
            settingsManager.a(y.c, "");
            settingsManager.a(y.f, "");
            settingsManager.a(y.o, 0L);
            settingsManager.b();
        } else if (i == 418) {
            settingsManager.a(y.a, true);
            settingsManager.b();
        } else if (i >= 400 && i < 500) {
            appLovinSdkImpl.g();
        } else if (i == 0) {
            appLovinSdkImpl.g();
        }
    }

    static void a(JSONObject jSONObject, AppLovinSdkImpl appLovinSdkImpl) {
        if (jSONObject == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else {
            try {
                if (jSONObject.has("settings")) {
                    ab settingsManager = appLovinSdkImpl.getSettingsManager();
                    settingsManager.a(jSONObject.getJSONObject("settings"));
                    settingsManager.b();
                    appLovinSdkImpl.getLogger().d(d, "New settings processed");
                }
            } catch (JSONException e) {
                appLovinSdkImpl.getLogger().e(d, "Unable to parse settings out of API response", e);
            }
        }
    }

    private static boolean a(int i, int[] iArr) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    private static boolean a(long j, TimeUnit timeUnit, Context context) {
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        CountDownLatch countDownLatch = new CountDownLatch(1);
        new l("AppLovinCheckWifiMobile", context, atomicBoolean, countDownLatch).start();
        try {
            countDownLatch.await(j, timeUnit);
        } catch (InterruptedException e) {
        }
        return atomicBoolean.get();
    }

    public static boolean a(boolean z, Context context) {
        ConnectivityManager connectivityManager;
        if (context == null) {
            throw new IllegalArgumentException("No context specified");
        }
        if (Settings.System.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0) {
            return false;
        }
        if (!m.a("android.permission.ACCESS_NETWORK_STATE", context) || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null) {
            return true;
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            return activeNetworkInfo.isConnected() && activeNetworkInfo.isAvailable();
        } else if (z) {
            return a(1, TimeUnit.SECONDS, context);
        } else {
            return true;
        }
    }

    static String b(String str, AppLovinSdkImpl appLovinSdkImpl) {
        if (str == null) {
            throw new IllegalArgumentException("No endpoint specified");
        } else if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append((String) appLovinSdkImpl.a(y.n));
            stringBuffer.append(str);
            return stringBuffer.toString();
        }
    }

    static void b(int i, AppLovinSdkImpl appLovinSdkImpl) {
        if (i == 418) {
            ab settingsManager = appLovinSdkImpl.getSettingsManager();
            settingsManager.a(y.a, true);
            settingsManager.b();
        }
    }
}
