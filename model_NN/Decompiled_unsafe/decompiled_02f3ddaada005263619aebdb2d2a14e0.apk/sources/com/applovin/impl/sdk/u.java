package com.applovin.impl.sdk;

import android.util.Log;
import com.applovin.sdk.Logger;

class u implements Logger {
    private r a;
    private ab b;
    private v c;

    u() {
    }

    /* access modifiers changed from: package-private */
    public void a(ab abVar) {
        this.b = abVar;
    }

    /* access modifiers changed from: package-private */
    public void a(r rVar) {
        this.a = rVar;
    }

    /* access modifiers changed from: package-private */
    public void a(v vVar) {
        this.c = vVar;
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        if (this.b != null) {
            return ((Boolean) this.b.a(y.l)).booleanValue();
        }
        return false;
    }

    public void d(String str, String str2) {
        if (a()) {
            Log.d(Logger.SDK_TAG, "[" + str + "] " + str2);
        }
        if (this.c != null) {
            this.c.a("DEBUG  [" + str + "] " + str2);
        }
    }

    public void e(String str, String str2) {
        e(str, str2, null);
    }

    public void e(String str, String str2, Throwable th) {
        if (a()) {
            Log.e(Logger.SDK_TAG, "[" + str + "] " + str2, th);
        }
        if (this.a != null) {
            this.a.a(str, str2, th);
        }
        if (this.c != null) {
            this.c.a("ERROR  [" + str + "] " + str2 + (th != null ? ": " + th.getMessage() : ""));
        }
    }

    public void i(String str, String str2) {
        if (a()) {
            Log.i(Logger.SDK_TAG, "[" + str + "] " + str2);
        }
        if (this.c != null) {
            this.c.a("INFO  [" + str + "] " + str2);
        }
    }

    public void userError(String str, String str2) {
        userError(str, str2, null);
    }

    public void userError(String str, String str2, Throwable th) {
        Log.e(Logger.SDK_TAG, "[" + str + "] " + str2, th);
        if (this.a != null) {
            this.a.b(str, str2, th);
        }
        if (this.c != null) {
            this.c.a("USER  [" + str + "] " + str2 + (th != null ? ": " + th.getMessage() : ""));
        }
    }

    public void w(String str, String str2) {
        w(str, str2, null);
    }

    public void w(String str, String str2, Throwable th) {
        if (a()) {
            Log.w(Logger.SDK_TAG, "[" + str + "] " + str2, th);
        }
        if (this.c != null) {
            this.c.a("WARN  [" + str + "] " + str2);
        }
    }
}
