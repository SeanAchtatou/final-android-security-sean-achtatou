package com.applovin.impl.sdk;

import android.content.Context;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.Logger;

class an implements Runnable {
    protected final AppLovinSdkImpl a;
    protected final Logger b;
    protected final Context c;

    an(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.c = appLovinSdkImpl.getApplicationContext();
        this.b = appLovinSdkImpl.getLogger();
    }

    private boolean d() {
        return ((Boolean) this.a.a(y.B)).booleanValue();
    }

    private void e() {
        String str = (String) this.a.a(y.P);
        if (str.length() > 0) {
            String[] split = str.split(",");
            x a2 = ((b) this.a.getAdService()).a();
            for (String fromString : split) {
                a2.a(AppLovinAdSize.fromString(fromString));
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        if (m.a("android.permission.INTERNET", this.c)) {
            return true;
        }
        this.b.userError("TaskInitializeSdk", "Unable to enable AppLovin SDK: no android.permission.INTERNET");
        return false;
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (bf.a(y.o, this.a)) {
            this.a.a().a(new ad(this.a), ap.BACKGROUND, 1500);
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        long longValue = ((Long) this.a.a(y.C)).longValue();
        if (longValue > 0) {
            this.a.a().a(new af(this.a), longValue * 60 * 1000);
        }
    }

    public void run() {
        long currentTimeMillis = System.currentTimeMillis();
        this.b.d("TaskInitializeSdk", "Initializing AppLovin SDK 5.0.0-5.0.0...");
        try {
            if (a()) {
                this.a.b().a();
                ac c2 = this.a.c();
                c2.c();
                c2.c("ad_dsp_session");
                if (d()) {
                    new bg(this.a).start();
                }
                b();
                e();
                c();
                this.a.a(true);
            } else {
                this.a.a(false);
            }
            this.b.d("TaskInitializeSdk", "AppLovin SDK 5.0.0-5.0.0 initialization " + (this.a.isEnabled() ? "succeeded" : "failed") + " in " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
        } catch (Throwable th) {
            Throwable th2 = th;
            this.b.d("TaskInitializeSdk", "AppLovin SDK 5.0.0-5.0.0 initialization " + (this.a.isEnabled() ? "succeeded" : "failed") + " in " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
            throw th2;
        }
    }
}
