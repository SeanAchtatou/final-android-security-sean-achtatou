package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAd;
import org.json.JSONObject;

class ax extends av {
    private final AppLovinAd a;

    ax(AppLovinAd appLovinAd, AppLovinSdkImpl appLovinSdkImpl) {
        super("TrackClick", y.k, appLovinSdkImpl);
        if (appLovinAd == null) {
            throw new IllegalArgumentException("No clicked ad specified");
        }
        this.a = appLovinAd;
        appLovinSdkImpl.c().a("clk_req");
    }

    public void a(int i) {
        k.b(i, this.d);
    }

    /* access modifiers changed from: protected */
    public void a(i iVar, j jVar) {
        iVar.a(this.a.getClickTrackerUrl(), jVar);
    }

    public void a(JSONObject jSONObject, int i) {
        this.d.c().a("clk_trk");
    }
}
