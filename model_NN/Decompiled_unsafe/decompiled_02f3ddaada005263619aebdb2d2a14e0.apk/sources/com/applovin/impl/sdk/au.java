package com.applovin.impl.sdk;

import android.content.Context;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.google.ads.AdActivity;
import com.tapjoy.TapjoyConstants;
import java.util.Arrays;
import java.util.Collection;
import org.json.JSONException;
import org.json.JSONObject;

class au extends aq {
    private static final Collection a = Arrays.asList(')', ']', '\"', '\'', ' ');
    private final JSONObject b;
    private final AppLovinAdLoadListener g;

    au(JSONObject jSONObject, AppLovinAdLoadListener appLovinAdLoadListener, AppLovinSdkImpl appLovinSdkImpl) {
        super("RenderAd", appLovinSdkImpl);
        this.b = jSONObject;
        this.g = appLovinAdLoadListener;
    }

    private String a(String str) {
        return (str == null || str.length() <= 0) ? "" : str.contains("#") ? str : str + "#sdk_version_" + AppLovinSdkImpl.FULL_VERSION;
    }

    private String a(String str, Context context) {
        bd bdVar = new bd(str, this.e);
        bdVar.a("click", "applovin://com.applovin.sdk/adservice/track_click");
        bdVar.a("click.javascript", "javascript:window.applovin_sdk.execute('track_click')");
        String a2 = bdVar.a();
        return ((Boolean) this.d.a(y.N)).booleanValue() ? c(a2) : a2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x013e A[SYNTHETIC, Splitter:B:25:0x013e] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0143 A[SYNTHETIC, Splitter:B:28:0x0143] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0148 A[SYNTHETIC, Splitter:B:31:0x0148] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0180 A[SYNTHETIC, Splitter:B:49:0x0180] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0185 A[SYNTHETIC, Splitter:B:52:0x0185] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x018a A[SYNTHETIC, Splitter:B:55:0x018a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.lang.String r12, java.lang.String r13) {
        /*
            r11 = this;
            r2 = 0
            java.lang.String r0 = "icon"
            boolean r0 = r13.contains(r0)
            if (r0 == 0) goto L_0x0078
            java.lang.String r0 = "/"
            java.lang.String r1 = "_"
            java.lang.String r0 = r13.replace(r0, r1)
            java.lang.String r1 = "."
            java.lang.String r3 = "_"
            java.lang.String r0 = r0.replace(r1, r3)
        L_0x0019:
            java.io.File r1 = new java.io.File
            com.applovin.impl.sdk.AppLovinSdkImpl r3 = r11.d
            android.content.Context r3 = r3.getApplicationContext()
            java.io.File r3 = r3.getCacheDir()
            java.lang.String r4 = "al"
            r1.<init>(r3, r4)
            java.io.File r5 = new java.io.File
            r5.<init>(r1, r0)
            r1.mkdirs()
            boolean r1 = r5.exists()
            if (r1 == 0) goto L_0x007a
            com.applovin.sdk.Logger r0 = r11.e
            java.lang.String r1 = r11.c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Loaded "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r13)
            java.lang.String r3 = " from cache: file://"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r5.getAbsolutePath()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "file://"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r5.getAbsolutePath()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x0077:
            return r0
        L_0x0078:
            r0 = r13
            goto L_0x0019
        L_0x007a:
            com.applovin.sdk.Logger r1 = r11.e
            java.lang.String r3 = r11.c
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "Starting caching of "
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.StringBuilder r4 = r4.append(r13)
            java.lang.String r6 = " into "
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r0 = r0.toString()
            r1.d(r3, r0)
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x01b1, all -> 0x017a }
            r4.<init>(r5)     // Catch:{ IOException -> 0x01b1, all -> 0x017a }
            java.net.URL r0 = new java.net.URL     // Catch:{ IOException -> 0x01b7, all -> 0x01a0 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01b7, all -> 0x01a0 }
            r1.<init>()     // Catch:{ IOException -> 0x01b7, all -> 0x01a0 }
            java.lang.StringBuilder r1 = r1.append(r12)     // Catch:{ IOException -> 0x01b7, all -> 0x01a0 }
            java.lang.StringBuilder r1 = r1.append(r13)     // Catch:{ IOException -> 0x01b7, all -> 0x01a0 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x01b7, all -> 0x01a0 }
            r0.<init>(r1)     // Catch:{ IOException -> 0x01b7, all -> 0x01a0 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x01b7, all -> 0x01a0 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x01b7, all -> 0x01a0 }
            com.applovin.impl.sdk.AppLovinSdkImpl r1 = r11.d     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            com.applovin.impl.sdk.aa r3 = com.applovin.impl.sdk.y.w     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            java.lang.Object r1 = r1.a(r3)     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            int r1 = r1.intValue()     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            r0.setConnectTimeout(r1)     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            com.applovin.impl.sdk.AppLovinSdkImpl r1 = r11.d     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            com.applovin.impl.sdk.aa r3 = com.applovin.impl.sdk.y.x     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            java.lang.Object r1 = r1.a(r3)     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            int r1 = r1.intValue()     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            r0.setReadTimeout(r1)     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            r1 = 1
            r0.setDefaultUseCaches(r1)     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            r1 = 1
            r0.setUseCaches(r1)     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            r1 = 0
            r0.setAllowUserInteraction(r1)     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            r1 = 1
            r0.setInstanceFollowRedirects(r1)     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ IOException -> 0x01bc, all -> 0x01a4 }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]     // Catch:{ IOException -> 0x0106, all -> 0x01aa }
        L_0x00f9:
            r6 = 0
            int r7 = r1.length     // Catch:{ IOException -> 0x0106, all -> 0x01aa }
            int r6 = r3.read(r1, r6, r7)     // Catch:{ IOException -> 0x0106, all -> 0x01aa }
            if (r6 < 0) goto L_0x014e
            r7 = 0
            r4.write(r1, r7, r6)     // Catch:{ IOException -> 0x0106, all -> 0x01aa }
            goto L_0x00f9
        L_0x0106:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x010a:
            com.applovin.sdk.Logger r6 = r11.e     // Catch:{ all -> 0x01af }
            java.lang.String r7 = r11.c     // Catch:{ all -> 0x01af }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x01af }
            r8.<init>()     // Catch:{ all -> 0x01af }
            java.lang.String r9 = "Failed to cache \""
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x01af }
            java.lang.StringBuilder r8 = r8.append(r12)     // Catch:{ all -> 0x01af }
            java.lang.StringBuilder r8 = r8.append(r13)     // Catch:{ all -> 0x01af }
            java.lang.String r9 = "\" into \""
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ all -> 0x01af }
            java.lang.String r5 = r5.getAbsolutePath()     // Catch:{ all -> 0x01af }
            java.lang.StringBuilder r5 = r8.append(r5)     // Catch:{ all -> 0x01af }
            java.lang.String r8 = "\""
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ all -> 0x01af }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x01af }
            r6.e(r7, r5, r0)     // Catch:{ all -> 0x01af }
            if (r3 == 0) goto L_0x0141
            r3.close()     // Catch:{ Exception -> 0x0194 }
        L_0x0141:
            if (r4 == 0) goto L_0x0146
            r4.close()     // Catch:{ Exception -> 0x0196 }
        L_0x0146:
            if (r1 == 0) goto L_0x014b
            r1.disconnect()     // Catch:{ Exception -> 0x0198 }
        L_0x014b:
            r0 = r2
            goto L_0x0077
        L_0x014e:
            r4.flush()     // Catch:{ IOException -> 0x0106, all -> 0x01aa }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0106, all -> 0x01aa }
            r1.<init>()     // Catch:{ IOException -> 0x0106, all -> 0x01aa }
            java.lang.String r6 = "file://"
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ IOException -> 0x0106, all -> 0x01aa }
            java.lang.String r6 = r5.getAbsolutePath()     // Catch:{ IOException -> 0x0106, all -> 0x01aa }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ IOException -> 0x0106, all -> 0x01aa }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0106, all -> 0x01aa }
            if (r3 == 0) goto L_0x016d
            r3.close()     // Catch:{ Exception -> 0x018e }
        L_0x016d:
            if (r4 == 0) goto L_0x0172
            r4.close()     // Catch:{ Exception -> 0x0190 }
        L_0x0172:
            if (r0 == 0) goto L_0x0177
            r0.disconnect()     // Catch:{ Exception -> 0x0192 }
        L_0x0177:
            r0 = r1
            goto L_0x0077
        L_0x017a:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r2
        L_0x017e:
            if (r3 == 0) goto L_0x0183
            r3.close()     // Catch:{ Exception -> 0x019a }
        L_0x0183:
            if (r4 == 0) goto L_0x0188
            r4.close()     // Catch:{ Exception -> 0x019c }
        L_0x0188:
            if (r1 == 0) goto L_0x018d
            r1.disconnect()     // Catch:{ Exception -> 0x019e }
        L_0x018d:
            throw r0
        L_0x018e:
            r2 = move-exception
            goto L_0x016d
        L_0x0190:
            r2 = move-exception
            goto L_0x0172
        L_0x0192:
            r0 = move-exception
            goto L_0x0177
        L_0x0194:
            r0 = move-exception
            goto L_0x0141
        L_0x0196:
            r0 = move-exception
            goto L_0x0146
        L_0x0198:
            r0 = move-exception
            goto L_0x014b
        L_0x019a:
            r2 = move-exception
            goto L_0x0183
        L_0x019c:
            r2 = move-exception
            goto L_0x0188
        L_0x019e:
            r1 = move-exception
            goto L_0x018d
        L_0x01a0:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x017e
        L_0x01a4:
            r1 = move-exception
            r3 = r2
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x017e
        L_0x01aa:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x017e
        L_0x01af:
            r0 = move-exception
            goto L_0x017e
        L_0x01b1:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r2
            goto L_0x010a
        L_0x01b7:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x010a
        L_0x01bc:
            r1 = move-exception
            r3 = r2
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x010a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.au.a(java.lang.String, java.lang.String):java.lang.String");
    }

    private String b(String str) {
        StringBuffer stringBuffer = new StringBuffer(k.b("click", this.d));
        stringBuffer.append("?");
        stringBuffer.append("clcode=").append(str);
        stringBuffer.append("&sdk_version=").append(AppLovinSdkImpl.FULL_VERSION);
        return stringBuffer.toString();
    }

    private String c(String str) {
        StringBuffer stringBuffer = new StringBuffer(str);
        String[] split = ((String) this.d.a(y.O)).split(",");
        int length = split.length;
        for (int i = 0; i < length; i++) {
            String str2 = split[i];
            int i2 = 0;
            int i3 = 0;
            while (i3 < stringBuffer.length() && (i3 = stringBuffer.indexOf(str2, i2)) != -1) {
                int length2 = stringBuffer.length();
                i2 = i3;
                while (!a.contains(Character.valueOf(stringBuffer.charAt(i2))) && i2 < length2) {
                    i2++;
                }
                if (i2 <= i3 || i2 == length2) {
                    System.out.println("HTML is bad");
                } else {
                    String a2 = a(str2, stringBuffer.substring(str2.length() + i3, i2));
                    if (a2 != null) {
                        stringBuffer.replace(i3, i2, a2);
                    }
                }
            }
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public void a(AppLovinAd appLovinAd) {
        if (this.g != null) {
            this.g.adReceived(appLovinAd);
        }
        ac c = this.d.c();
        c.a("ad_dsp");
        c.a("ad_dsp_session");
    }

    /* access modifiers changed from: protected */
    public void b() {
        try {
            if (this.g != null) {
                this.g.failedToReceiveAd(-6);
            }
        } catch (Throwable th) {
            this.e.e(this.c, "Unable process a failure to recieve an ad", th);
        }
    }

    public void run() {
        String string;
        this.e.d(this.c, "Rendering ad...");
        try {
            String string2 = this.b.getString(AdActivity.HTML_PARAM);
            AppLovinAdSize fromString = this.b.has(TapjoyConstants.TJC_DISPLAY_AD_SIZE) ? AppLovinAdSize.fromString(this.b.getString(TapjoyConstants.TJC_DISPLAY_AD_SIZE)) : AppLovinAdSize.BANNER;
            if (string2 == null || string2.length() <= 0) {
                this.e.e(this.c, "No HTML recieved for requseted ad");
                b();
                return;
            }
            if (this.b.has("clcode")) {
                string = b(this.b.getString("clcode"));
            } else if (this.b.has("click_url")) {
                string = this.b.getString("click_url");
            } else {
                this.e.e(this.c, "Ad server has not returned a clcode or click URL");
                b();
                return;
            }
            if (this.b.has("redirect_url")) {
                a(new AppLovinAd(a(string2, this.f), fromString, a(this.b.getString("redirect_url")), string));
                return;
            }
            this.e.e(this.c, "Ad server has not returned a redirect URL");
            b();
        } catch (JSONException e) {
            this.e.e(this.c, "Unable to parse ad service response", e);
            b();
        }
    }
}
