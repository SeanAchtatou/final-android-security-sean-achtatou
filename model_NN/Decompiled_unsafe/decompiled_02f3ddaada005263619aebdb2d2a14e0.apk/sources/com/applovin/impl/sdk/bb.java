package com.applovin.impl.sdk;

import android.util.Base64;
import org.json.JSONException;
import org.json.JSONObject;

class bb extends aq {
    bb(AppLovinSdkImpl appLovinSdkImpl) {
        super("UploadAppIcon", appLovinSdkImpl);
    }

    private String a(byte[] bArr) {
        return new String(Base64.encode(bArr, 0));
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    public void run() {
        byte[] d = f().d();
        if (d != null) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("icon", a(d));
                jSONObject.put("icon_type", "image/jpeg");
                jSONObject.put("package_name", this.d.getApplicationContext().getPackageName());
                StringBuffer stringBuffer = new StringBuffer("app");
                stringBuffer.append('/');
                stringBuffer.append((String) this.d.a(y.e));
                this.d.getConnectionManager().a(k.a(stringBuffer.toString(), this.d), jSONObject, new bc(this));
            } catch (JSONException e) {
                this.e.e(this.c, "Unable to create icon JSON request", e);
            }
        }
    }
}
