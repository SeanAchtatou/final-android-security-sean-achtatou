package com.applovin.impl.sdk;

import android.util.Log;
import org.json.JSONObject;

class aw implements j {
    final /* synthetic */ AppLovinSdkImpl a;
    final /* synthetic */ String b;
    final /* synthetic */ av c;

    aw(av avVar, AppLovinSdkImpl appLovinSdkImpl, String str) {
        this.c = avVar;
        this.a = appLovinSdkImpl;
        this.b = str;
    }

    public void a(int i) {
        boolean z = false;
        boolean z2 = i < 200 || i >= 500;
        if (i != -103) {
            z = true;
        }
        if (!z2 || !z || this.c.a <= 0) {
            this.c.a(i);
            return;
        }
        long longValue = ((Long) this.a.a(y.p)).longValue();
        Log.w(this.b, "Unable to send requset due to server failure (code " + i + "). " + this.c.a + " attempts left, retrying in " + (((double) longValue) / 1000.0d) + " seconds...");
        av.b(this.c, 1);
        if (this.c.a == 0) {
            this.c.b();
        }
        this.a.a().a(this.c, ap.BACKGROUND, longValue);
    }

    public void a(JSONObject jSONObject, int i) {
        int unused = this.c.a = 0;
        this.c.a(jSONObject, i);
    }
}
