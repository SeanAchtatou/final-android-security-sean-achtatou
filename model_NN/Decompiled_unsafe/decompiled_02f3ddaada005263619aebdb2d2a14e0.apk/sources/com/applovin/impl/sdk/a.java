package com.applovin.impl.sdk;

import android.content.SharedPreferences;
import java.util.HashMap;
import java.util.Map;

class a {
    private static final Map a = new HashMap();

    static Map a(AppLovinSdkImpl appLovinSdkImpl) {
        return a("ad_data_cache", appLovinSdkImpl);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0095  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.Map a(java.lang.String r7, com.applovin.impl.sdk.AppLovinSdkImpl r8) {
        /*
            java.util.Map r1 = com.applovin.impl.sdk.a.a
            monitor-enter(r1)
            java.util.Map r0 = com.applovin.impl.sdk.a.a     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r0.get(r7)     // Catch:{ all -> 0x003b }
            java.util.Map r0 = (java.util.Map) r0     // Catch:{ all -> 0x003b }
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            if (r0 != 0) goto L_0x00a0
            com.applovin.impl.sdk.ab r1 = r8.getSettingsManager()
            android.content.SharedPreferences r2 = r1.a()
            java.lang.String r1 = ""
            java.lang.String r3 = r2.getString(r7, r1)
            if (r3 == 0) goto L_0x00a0
            int r1 = r3.length()
            if (r1 <= 0) goto L_0x00a0
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ Exception -> 0x009b }
            r1.<init>()     // Catch:{ Exception -> 0x009b }
            java.lang.String r0 = "&"
            java.lang.String[] r3 = r3.split(r0)     // Catch:{ Exception -> 0x007c }
            int r4 = r3.length     // Catch:{ Exception -> 0x007c }
            r0 = 0
        L_0x0031:
            if (r0 >= r4) goto L_0x003e
            r5 = r3[r0]     // Catch:{ Exception -> 0x007c }
            a(r5, r1)     // Catch:{ Exception -> 0x007c }
            int r0 = r0 + 1
            goto L_0x0031
        L_0x003b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            throw r0
        L_0x003e:
            java.util.Map r3 = com.applovin.impl.sdk.a.a     // Catch:{ Exception -> 0x007c }
            monitor-enter(r3)     // Catch:{ Exception -> 0x007c }
            java.util.Map r0 = com.applovin.impl.sdk.a.a     // Catch:{ all -> 0x0079 }
            r0.put(r7, r1)     // Catch:{ all -> 0x0079 }
            monitor-exit(r3)     // Catch:{ all -> 0x0079 }
            com.applovin.sdk.Logger r0 = r8.getLogger()     // Catch:{ Exception -> 0x007c }
            java.lang.String r3 = "AdDataCache"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007c }
            r4.<init>()     // Catch:{ Exception -> 0x007c }
            int r5 = r1.size()     // Catch:{ Exception -> 0x007c }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x007c }
            java.lang.String r5 = " "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x007c }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x007c }
            java.lang.String r5 = " entries loaded from cache"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x007c }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x007c }
            r0.d(r3, r4)     // Catch:{ Exception -> 0x007c }
        L_0x0071:
            if (r1 == 0) goto L_0x0095
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>(r1)
        L_0x0078:
            return r0
        L_0x0079:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0079 }
            throw r0     // Catch:{ Exception -> 0x007c }
        L_0x007c:
            r0 = move-exception
        L_0x007d:
            com.applovin.sdk.Logger r3 = r8.getLogger()
            java.lang.String r4 = "AdDataCache"
            java.lang.String r5 = "Unable to load ad data"
            r3.e(r4, r5, r0)
            android.content.SharedPreferences$Editor r0 = r2.edit()
            java.lang.String r2 = ""
            r0.putString(r7, r2)
            r0.commit()
            goto L_0x0071
        L_0x0095:
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            goto L_0x0078
        L_0x009b:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x007d
        L_0x00a0:
            r1 = r0
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.a.a(java.lang.String, com.applovin.impl.sdk.AppLovinSdkImpl):java.util.Map");
    }

    private static void a(String str, Map map) {
        String[] split = str.split("=");
        if (split.length == 2) {
            map.put(split[0], split[1]);
        }
    }

    static void a(Map map, AppLovinSdkImpl appLovinSdkImpl) {
        a(map, "ad_data_cache", appLovinSdkImpl);
    }

    private static void a(Map map, String str, AppLovinSdkImpl appLovinSdkImpl) {
        if (map == null) {
            throw new IllegalArgumentException("No ad aata specified");
        } else if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else {
            try {
                synchronized (a) {
                    Map map2 = (Map) a.get(str);
                    if (map2 == null) {
                        map2 = new HashMap();
                    }
                    map2.clear();
                    map2.putAll(map);
                    a.put(str, map2);
                }
                SharedPreferences.Editor edit = appLovinSdkImpl.getSettingsManager().a().edit();
                edit.putString(str, bf.a(map));
                edit.commit();
                appLovinSdkImpl.getLogger().d("AdDataCache", map.size() + " " + str + " entries saved in cache");
            } catch (Exception e) {
                appLovinSdkImpl.getLogger().e("AdDataCache", "Unable to save ad data entries", e);
            }
        }
    }
}
