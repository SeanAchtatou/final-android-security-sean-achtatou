package com.applovin.impl.sdk;

import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.google.ads.AdActivity;
import com.tapjoy.TapjoyConstants;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONException;
import org.json.JSONObject;

class aj extends aq {
    /* access modifiers changed from: private */
    public final Socket a;

    aj(Socket socket, AppLovinSdkImpl appLovinSdkImpl) {
        super("HandleWebRequest", appLovinSdkImpl);
        if (socket == null) {
            throw new IllegalArgumentException("No clientspecified");
        }
        this.a = socket;
    }

    private void a(String str) {
        List emptyList;
        int indexOf = str.indexOf(32);
        int indexOf2 = str.indexOf(32, indexOf + 2);
        String substring = (indexOf <= 0 || indexOf2 <= indexOf) ? "" : str.substring(indexOf + 1, indexOf2);
        try {
            emptyList = URLEncodedUtils.parse(new URI(substring), "utf-8");
        } catch (Exception e) {
            this.e.e(this.c, "Unable to parse query string", e);
            emptyList = Collections.emptyList();
        }
        this.e.d(this.c, "Handling request: " + substring);
        if (substring.startsWith("/ad")) {
            a(emptyList);
        } else if (substring.startsWith("/conv")) {
            b(emptyList);
        } else if (substring.startsWith("/test.js")) {
            b();
        } else {
            a(new am(404, this.d, this.a), ap.MAIN);
        }
    }

    private void a(List list) {
        ak akVar = new ak(this, AppLovinAdSize.fromString(b(TapjoyConstants.TJC_DISPLAY_AD_SIZE, list)), new al(this, list), this.d, list);
        akVar.a(b("placement", list));
        a(akVar, ap.MAIN);
    }

    /* access modifiers changed from: private */
    public static String b(String str, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            NameValuePair nameValuePair = (NameValuePair) it.next();
            if (str.equals(nameValuePair.getName())) {
                return nameValuePair.getValue();
            }
        }
        return null;
    }

    private void b() {
        a(new am(200, "applovin_serverTest = '5.0.0';", "application/javascript", this.d, this.a), ap.MAIN);
    }

    private void b(List list) {
        new ba(b("advertiser_id", list), this.d).run();
        a(new am(200, this.d, this.a), ap.MAIN);
    }

    /* access modifiers changed from: private */
    public static byte[] b(int i, int i2, String str) {
        return ("HTTP/1.1 " + i + " OK\r\n" + "Content-Type: " + str + "; charset=utf-8\r\n" + "Content-Length: " + i2 + "\r\n" + "\r\n").getBytes();
    }

    /* access modifiers changed from: private */
    public static byte[] b(int i, String str) {
        return b(i, 0, str);
    }

    /* access modifiers changed from: private */
    public static String c(AppLovinAd appLovinAd) {
        return appLovinAd.getHtml().replace("applovin://com.applovin.sdk/adservice/track_click", appLovinAd.getClickTrackerUrl());
    }

    /* access modifiers changed from: private */
    public static String d(AppLovinAd appLovinAd) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(AdActivity.HTML_PARAM, appLovinAd.getHtml());
            jSONObject.put("redirect_url", appLovinAd.getDestinationUrl());
            jSONObject.put("click_url", appLovinAd.getClickTrackerUrl());
            return jSONObject.toString();
        } catch (JSONException e) {
            throw new RuntimeException("Programming error: unable to create JSON message", e);
        }
    }

    public void run() {
        byte[] bArr = new byte[AccessibilityEventCompat.TYPE_WINDOW_CONTENT_CHANGED];
        try {
            int read = new BufferedInputStream(this.a.getInputStream(), AccessibilityEventCompat.TYPE_WINDOW_CONTENT_CHANGED).read(bArr);
            a(read > 0 ? new String(bArr, 0, read) : "");
        } catch (IOException e) {
            this.e.e(this.c, "Unable to communicate with the client", e);
        }
    }
}
