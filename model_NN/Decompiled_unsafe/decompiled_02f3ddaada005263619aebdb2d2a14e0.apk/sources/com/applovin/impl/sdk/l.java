package com.applovin.impl.sdk;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

final class l extends Thread {
    final /* synthetic */ Context a;
    final /* synthetic */ AtomicBoolean b;
    final /* synthetic */ CountDownLatch c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    l(String str, Context context, AtomicBoolean atomicBoolean, CountDownLatch countDownLatch) {
        super(str);
        this.a = context;
        this.b = atomicBoolean;
        this.c = countDownLatch;
    }

    public void run() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.a.getSystemService("connectivity");
        if (connectivityManager != null) {
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
            if (networkInfo == null || !networkInfo.isConnected()) {
                NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(1);
                if (networkInfo2 != null && networkInfo2.isConnected()) {
                    this.b.set(true);
                }
            } else {
                this.b.set(true);
            }
        }
        this.c.countDown();
    }
}
