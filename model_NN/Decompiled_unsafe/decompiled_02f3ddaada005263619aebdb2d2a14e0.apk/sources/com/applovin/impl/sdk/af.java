package com.applovin.impl.sdk;

import com.applovin.sdk.bootstrap.SdkBootstrap;

class af extends aq {
    af(AppLovinSdkImpl appLovinSdkImpl) {
        super("CheckSdkUpdates", appLovinSdkImpl);
    }

    public void run() {
        SdkBootstrap.getInstance(this.d.getApplicationContext()).checkForUpdates();
    }
}
