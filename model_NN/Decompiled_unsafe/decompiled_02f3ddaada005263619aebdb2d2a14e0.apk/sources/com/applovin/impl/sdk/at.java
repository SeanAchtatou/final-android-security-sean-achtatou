package com.applovin.impl.sdk;

class at implements Runnable {
    final /* synthetic */ ao a;
    private final String b;
    private final aq c;
    private final ap d;

    at(ao aoVar, aq aqVar, ap apVar) {
        this.a = aoVar;
        this.b = aqVar.e();
        this.c = aqVar;
        this.d = apVar;
    }

    public void run() {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            h.a();
            if (this.a.a.d()) {
                this.a.b.i(this.b, "Task re-scheduled...");
                this.a.a(this.c, this.d, 2000);
            } else if (this.a.a.isEnabled()) {
                this.a.b.i(this.b, "Task  started exection...");
                this.c.run();
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                this.a.b.i(this.b, "Task executed successfully in " + currentTimeMillis2 + "ms.");
                ac c2 = this.a.a.c();
                c2.a(this.b + "_count");
                c2.a(this.b + "_time", currentTimeMillis2);
            } else {
                if (this.a.a.e()) {
                    this.a.a.f();
                } else {
                    this.a.b.w(this.b, "Task not executed, SDK is disabled");
                }
                this.c.a();
            }
        } catch (Throwable th) {
            this.a.b.e(this.b, "Task failed execution in " + (System.currentTimeMillis() - currentTimeMillis) + "ms.", th);
        }
    }
}
