package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAdSize;

class f extends aq {
    final /* synthetic */ b a;
    private final AppLovinAdSize b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public f(b bVar, AppLovinAdSize appLovinAdSize) {
        super("UpdateAdTask", bVar.a);
        this.a = bVar;
        this.b = appLovinAdSize;
    }

    public void run() {
        boolean z = true;
        e eVar = (e) this.a.d.get(this.b);
        synchronized (eVar.b) {
            boolean a2 = this.a.a(this.b);
            boolean d = this.a.b();
            boolean z2 = !eVar.f.isEmpty();
            if (System.currentTimeMillis() <= eVar.d) {
                z = false;
            }
            if (a2 && z2 && z && d && !eVar.e) {
                eVar.e = true;
                this.a.a(this.b, null);
            }
        }
    }
}
