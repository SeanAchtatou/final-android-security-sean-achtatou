package com.applovin.impl.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import com.applovin.sdk.AppLovinTargetingData;
import com.google.ads.AdActivity;
import java.util.HashMap;
import java.util.Map;

class g implements AppLovinTargetingData {
    private final AppLovinSdkImpl a;
    private final Context b;

    g(AppLovinSdkImpl appLovinSdkImpl) {
        if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        }
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getApplicationContext();
    }

    private static String a(String[] strArr) {
        if (strArr == null || strArr.length <= 0) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (String str : strArr) {
            if (bf.e(str)) {
                stringBuffer.append(bf.d(str));
                stringBuffer.append(",");
            }
        }
        if (stringBuffer.length() > 0) {
            stringBuffer.setLength(stringBuffer.length() - 1);
        }
        return stringBuffer.toString();
    }

    private void a(String str, String str2) {
        if (bf.e(str)) {
            SharedPreferences.Editor edit = this.b.getSharedPreferences("applovin.sdk.targeting", 0).edit();
            edit.putString(str, bf.d(str2));
            edit.commit();
        }
    }

    /* access modifiers changed from: package-private */
    public Map a() {
        HashMap hashMap = new HashMap();
        Map<String, ?> all = this.b.getSharedPreferences("applovin.sdk.targeting", 0).getAll();
        if (all != null && all.size() > 0) {
            for (Map.Entry next : all.entrySet()) {
                hashMap.put(next.getKey(), String.valueOf(next.getValue()));
            }
        }
        return hashMap;
    }

    public void clearData() {
        SharedPreferences.Editor edit = this.b.getSharedPreferences("applovin.sdk.targeting", 0).edit();
        edit.clear();
        edit.commit();
    }

    public void putExtra(String str, String str2) {
        if (bf.e(str) && bf.e(str2)) {
            a("ex_" + str, str2);
        }
    }

    public void setBirthYear(int i) {
        if (i < 9999 && i > 1900) {
            a("yob", Integer.toString(i));
        }
    }

    public void setCarrier(String str) {
        if (bf.e(str)) {
            a("carrier", str);
        }
    }

    public void setCountry(String str) {
        if (bf.e(str) && str.length() == 2) {
            a("country", str.toUpperCase());
        }
    }

    public void setEducation(String str) {
        if (bf.e(str)) {
            a("education", str);
        }
    }

    public void setEmail(String str) {
        if (bf.e(str)) {
            setEmails(str);
        }
    }

    public void setEmails(String... strArr) {
        if (strArr != null && strArr.length > 0) {
            StringBuffer stringBuffer = new StringBuffer();
            for (String str : strArr) {
                if (bf.e(str)) {
                    stringBuffer.append(bf.a(bf.c(str), this.a));
                    stringBuffer.append(",");
                }
            }
            if (stringBuffer.length() > 0) {
                stringBuffer.setLength(stringBuffer.length() - 1);
            }
            a("hemails", stringBuffer.toString());
        }
    }

    public void setEthnicity(String str) {
        if (bf.e(str)) {
            a("ethnicity", str);
        }
    }

    public void setFirstName(String str) {
        if (bf.e(str)) {
            a("uname", bf.b(str));
        }
    }

    public void setGender(char c) {
        a("gender", c == 'm' ? AdActivity.TYPE_PARAM : c == 'f' ? "f" : AdActivity.URL_PARAM);
    }

    public void setHashedEmail(String str) {
        if (bf.e(str)) {
            setHashedEmails(str);
        }
    }

    public void setHashedEmails(String... strArr) {
        if (strArr != null && strArr.length > 0) {
            StringBuffer stringBuffer = new StringBuffer();
            for (String str : strArr) {
                if (bf.e(str)) {
                    stringBuffer.append(str);
                    stringBuffer.append(",");
                }
            }
            if (stringBuffer.length() > 0) {
                stringBuffer.setLength(stringBuffer.length() - 1);
            }
            a("hemails", stringBuffer.toString());
        }
    }

    public void setHashedPhoneNumber(String str) {
        if (bf.e(str)) {
            a("hphone", str);
        }
    }

    public void setIncome(String str) {
        if (bf.e(str)) {
            a("income", str);
        }
    }

    public void setInterests(String... strArr) {
        if (strArr != null && strArr.length > 0) {
            a("interests", a(strArr));
        }
    }

    public void setKeywords(String... strArr) {
        if (strArr != null && strArr.length > 0) {
            a("keywords", a(strArr));
        }
    }

    public void setLanguage(String str) {
        if (bf.e(str)) {
            a("language", str.toLowerCase());
        }
    }

    public void setLocation(Location location) {
        if (location != null) {
            a("lat", Double.toString(location.getLatitude()));
            a("lon", Double.toString(location.getLongitude()));
        }
    }

    public void setMaritalStatus(String str) {
        if (bf.e(str)) {
            a("marital_status", str);
        }
    }

    public void setPhoneNumber(String str) {
        if (bf.e(str)) {
            a("hphone", bf.a(new w(this.a.getApplicationContext()).a(str), this.a));
        }
    }
}
