package com.applovin.impl.sdk;

import org.json.JSONObject;

class az extends av {
    final /* synthetic */ String a;
    final /* synthetic */ ay b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    az(ay ayVar, String str, aa aaVar, AppLovinSdkImpl appLovinSdkImpl, String str2) {
        super(str, aaVar, appLovinSdkImpl);
        this.b = ayVar;
        this.a = str2;
    }

    public void a(int i) {
        if (i > 400) {
            this.b.c();
        }
        this.e.e(this.c, "Unable to track conversion: server responded with " + i);
    }

    /* access modifiers changed from: protected */
    public void a(i iVar, j jVar) {
        iVar.a(this.b.a(this.a), jVar);
    }

    public void a(JSONObject jSONObject, int i) {
        this.b.c();
    }
}
