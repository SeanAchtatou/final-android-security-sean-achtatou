package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.List;

class al implements AppLovinAdLoadListener {
    final /* synthetic */ aj a;
    private final List b;

    al(aj ajVar, List list) {
        this.a = ajVar;
        this.b = list;
    }

    public void adReceived(AppLovinAd appLovinAd) {
        String b2;
        String str;
        String a2 = aj.b("format", this.b);
        if (a2 == null || !a2.equals("json")) {
            b2 = aj.c(appLovinAd);
            str = "text/html";
        } else {
            b2 = aj.d(appLovinAd);
            str = "application/json";
        }
        this.a.a(new am(200, b2, str, this.a.d, this.a.a), ap.MAIN);
    }

    public void failedToReceiveAd(int i) {
        this.a.a(new am(i, this.a.d, this.a.a), ap.MAIN);
    }
}
