package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinSdkUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class bf extends AppLovinSdkUtils {
    private static final char[] a = "0123456789abcdef".toCharArray();
    private static final char[] b = "-'".toCharArray();

    public static String a(String str) {
        return (str == null || str.length() <= 4) ? "NOKEY" : str.substring(str.length() - 4);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.bf.a(java.lang.String, java.lang.Integer, java.lang.String):java.lang.String
     arg types: [java.lang.String, int, java.lang.String]
     candidates:
      com.applovin.impl.sdk.bf.a(java.util.Collection, java.lang.String, int):java.lang.String
      com.applovin.impl.sdk.bf.a(java.lang.String, java.lang.Integer, java.lang.String):java.lang.String */
    public static String a(String str, AppLovinSdkImpl appLovinSdkImpl) {
        return a(str, (Integer) -1, (String) appLovinSdkImpl.a(y.u));
    }

    private static String a(String str, Integer num, String str2) {
        if (str2 == null) {
            throw new IllegalArgumentException("No algorithm specified");
        } else if (str == null || str.length() < 1) {
            return "";
        } else {
            if (str2.length() < 1 || "none".equals(str2)) {
                return str;
            }
            try {
                MessageDigest instance = MessageDigest.getInstance(str2);
                instance.update(str.getBytes("UTF-8"));
                String a2 = a(instance.digest());
                return (a2 == null || num.intValue() <= 0) ? a2 : a2.substring(0, Math.min(num.intValue(), a2.length()));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException("Unknown algorithm \"" + str2 + "\"", e);
            } catch (UnsupportedEncodingException e2) {
                throw new RuntimeException("Programming error: UTF-8 is not know encoding", e2);
            }
        }
    }

    static String a(Collection collection, int i) {
        return a(collection, ",", i);
    }

    static String a(Collection collection, String str, int i) {
        if (str == null) {
            throw new IllegalArgumentException("No glue specified");
        } else if (collection == null || collection.size() < 1) {
            return "";
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            Iterator it = collection.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                String str2 = (String) it.next();
                if (i2 >= i) {
                    break;
                }
                i2++;
                stringBuffer.append(str2).append(str);
            }
            if (stringBuffer.length() > str.length()) {
                stringBuffer.setLength(stringBuffer.length() - str.length());
            }
            return stringBuffer.toString();
        }
    }

    static String a(Map map) {
        if (map == null || map.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(entry.getKey()).append('=').append(entry.getValue());
        }
        return sb.toString();
    }

    public static String a(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("No data specified");
        }
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            cArr[i * 2] = a[(bArr[i] & 240) >>> 4];
            cArr[(i * 2) + 1] = a[bArr[i] & 15];
        }
        return new String(cArr);
    }

    private static boolean a(char c) {
        for (char c2 : b) {
            if (c == c2) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(aa aaVar, AppLovinSdkImpl appLovinSdkImpl) {
        return a(aaVar, appLovinSdkImpl.getSettingsManager());
    }

    public static boolean a(aa aaVar, ab abVar) {
        return System.currentTimeMillis() > ((Long) abVar.a(aaVar)).longValue() * 1000;
    }

    public static String b(String str) {
        if (str == null || str.length() < 2) {
            return str;
        }
        int length = str.length();
        int i = 0;
        while (true) {
            if (i >= str.length()) {
                i = length;
                break;
            }
            char charAt = str.charAt(i);
            if (!Character.isLetterOrDigit(charAt) && !a(charAt)) {
                break;
            }
            i++;
        }
        return str.substring(0, i);
    }

    public static String b(String str, AppLovinSdkImpl appLovinSdkImpl) {
        return a(str, (Integer) appLovinSdkImpl.a(y.v), (String) appLovinSdkImpl.a(y.u));
    }

    public static String c(String str) {
        return (str == null || str.length() < 1) ? "" : str.trim().toLowerCase();
    }

    static String d(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    static boolean e(String str) {
        return str != null && str.length() > 1;
    }
}
