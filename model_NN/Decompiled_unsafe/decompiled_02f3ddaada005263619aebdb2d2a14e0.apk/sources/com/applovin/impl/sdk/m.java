package com.applovin.impl.sdk;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.applovin.sdk.Logger;
import com.tapjoy.TapjoyConstants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

class m {
    protected static List a = new ArrayList();
    protected final AppLovinSdkImpl b;
    protected final Context c;
    protected final Logger d;
    protected final w e;

    m(AppLovinSdkImpl appLovinSdkImpl) {
        if (appLovinSdkImpl == null) {
            throw new IllegalArgumentException("No sdk specified");
        }
        this.b = appLovinSdkImpl;
        this.d = appLovinSdkImpl.getLogger();
        this.c = appLovinSdkImpl.getApplicationContext();
        this.e = f();
    }

    static boolean a(CharSequence charSequence) {
        return Pattern.compile("^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", 2).matcher(charSequence).matches();
    }

    static boolean a(String str, Context context) {
        if (str == null) {
            throw new IllegalArgumentException("No permission name specified");
        } else if (context != null) {
            return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    private w f() {
        return new w(this.b.getApplicationContext());
    }

    private String g() {
        String lowerCase;
        int indexOf;
        int i;
        int indexOf2;
        AtomicReference atomicReference = new AtomicReference();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        new o(this, "AppLovinCollectCpuSpeed", atomicReference, countDownLatch).start();
        try {
            countDownLatch.await(2, TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
        }
        String str = (String) atomicReference.get();
        return (str == null || str.length() <= 0 || (indexOf = (lowerCase = str.trim().toLowerCase()).indexOf("bogomips")) < 0 || (indexOf2 = lowerCase.indexOf(10, (i = indexOf + 11))) <= 0) ? "" : lowerCase.substring(i, indexOf2);
    }

    /* access modifiers changed from: package-private */
    public List a() {
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> queryIntentActivities = this.c.getPackageManager().queryIntentActivities(intent, 0);
        if (queryIntentActivities == null || queryIntentActivities.isEmpty()) {
            return Collections.emptyList();
        }
        if (a.size() == queryIntentActivities.size() && a.size() > 0 && ((p) a.get(0)).a.equals(queryIntentActivities.get(0).activityInfo.name)) {
            return new ArrayList(a);
        }
        ArrayList arrayList = new ArrayList(queryIntentActivities.size());
        HashSet hashSet = new HashSet();
        for (ResolveInfo next : queryIntentActivities) {
            try {
                long lastModified = new File(next.activityInfo.applicationInfo.sourceDir).lastModified();
                p pVar = new p();
                pVar.c = bf.b(next.activityInfo.packageName, this.b);
                pVar.d = lastModified;
                pVar.a = next.activityInfo.name;
                if (!hashSet.contains(pVar.c)) {
                    arrayList.add(pVar);
                    hashSet.add(pVar.c);
                }
            } catch (Throwable th) {
                this.d.w("DataCollector", "Unable to read information for app \"" + next + "\"", th);
            }
        }
        try {
            Collections.sort(arrayList, new n(this));
        } catch (Throwable th2) {
            this.d.w("DataCollector", "Unable to sort applications", th2);
        }
        a = arrayList;
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        return a(str, this.c);
    }

    /* access modifiers changed from: package-private */
    public q b() {
        q qVar = new q();
        TelephonyManager telephonyManager = (TelephonyManager) this.c.getSystemService("phone");
        if (a("android.permission.READ_PHONE_STATE")) {
            qVar.a = telephonyManager.getDeviceId();
            String a2 = this.e.a(telephonyManager.getLine1Number());
            if (a2 != null && a2.length() > 0) {
                qVar.i = bf.a(a2, this.b);
            }
        }
        if (a("android.permission.GET_ACCOUNTS")) {
            qVar.n = new ArrayList();
            for (Account account : AccountManager.get(this.c).getAccounts()) {
                if (a((CharSequence) account.name)) {
                    qVar.n.add(bf.a(bf.c(account.name), this.b));
                }
            }
        }
        qVar.l = g();
        qVar.b = Settings.Secure.getString(this.c.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
        qVar.c = h.b();
        if (a("android.permission.ACCESS_WIFI_STATE")) {
            try {
                WifiInfo connectionInfo = ((WifiManager) this.c.getSystemService("wifi")).getConnectionInfo();
                qVar.d = connectionInfo != null ? connectionInfo.getMacAddress() : null;
            } catch (Exception e2) {
                this.d.e("DataCollector", "Unable to collect device Wi-fi mac address", e2);
            }
        }
        qVar.m = Locale.getDefault();
        qVar.e = Build.MODEL;
        qVar.f = Build.VERSION.RELEASE;
        qVar.g = Build.MANUFACTURER;
        qVar.h = Build.VERSION.SDK_INT;
        qVar.j = telephonyManager.getSimCountryIso().toUpperCase();
        qVar.k = telephonyManager.getNetworkOperatorName();
        return qVar;
    }

    /* access modifiers changed from: package-private */
    public p c() {
        ApplicationInfo applicationInfo = this.c.getApplicationInfo();
        long lastModified = new File(applicationInfo.sourceDir).lastModified();
        PackageManager packageManager = this.c.getPackageManager();
        PackageInfo packageInfo = null;
        try {
            packageInfo = packageManager.getPackageInfo(this.c.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e2) {
        }
        p pVar = new p();
        pVar.c = applicationInfo.packageName;
        pVar.d = lastModified;
        pVar.a = String.valueOf(packageManager.getApplicationLabel(applicationInfo));
        pVar.b = packageInfo != null ? packageInfo.versionName : "";
        return pVar;
    }

    /* access modifiers changed from: package-private */
    public byte[] d() {
        Drawable applicationIcon = this.c.getPackageManager().getApplicationIcon(this.c.getApplicationInfo());
        if (applicationIcon instanceof BitmapDrawable) {
            try {
                Bitmap bitmap = ((BitmapDrawable) applicationIcon).getBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                return byteArrayOutputStream.toByteArray();
            } catch (Exception e2) {
                this.d.e("DataCollector", "Unable to create an app icon", e2);
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return Settings.Secure.getInt(this.c.getContentResolver(), "install_non_market_apps", 0) != 0;
    }
}
