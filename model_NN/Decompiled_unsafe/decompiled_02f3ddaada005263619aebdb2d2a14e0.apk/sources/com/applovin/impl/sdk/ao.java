package com.applovin.impl.sdk;

import com.applovin.sdk.Logger;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class ao {
    protected final AppLovinSdkImpl a;
    protected final Logger b;
    private final ScheduledExecutorService c = a("main");
    private final ScheduledExecutorService d = a(" back");

    ao(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
    }

    private static void a(Runnable runnable, long j, ScheduledExecutorService scheduledExecutorService) {
        if (j > 0) {
            scheduledExecutorService.schedule(runnable, j, TimeUnit.MILLISECONDS);
        } else {
            scheduledExecutorService.submit(runnable);
        }
    }

    /* access modifiers changed from: protected */
    public ScheduledExecutorService a(String str) {
        return Executors.newScheduledThreadPool(1, new ar(this, str));
    }

    /* access modifiers changed from: package-private */
    public void a(an anVar, long j) {
        if (anVar == null) {
            throw new IllegalArgumentException("No task specified");
        }
        a(anVar, j, this.c);
    }

    public void a(aq aqVar, long j) {
        if (aqVar == null) {
            throw new IllegalArgumentException("No task specified");
        } else if (j <= 0) {
            throw new IllegalArgumentException("Invalid interval specified: " + j);
        } else {
            this.b.d(aqVar.c, "Scheduling periodic task " + aqVar.c + " with period of " + j + "ms ");
            this.d.scheduleAtFixedRate(new at(this, aqVar, ap.BACKGROUND), j, j, TimeUnit.MILLISECONDS);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(aq aqVar, ap apVar) {
        a(aqVar, apVar, 0);
    }

    /* access modifiers changed from: package-private */
    public void a(aq aqVar, ap apVar, long j) {
        if (aqVar == null) {
            throw new IllegalArgumentException("No task specified");
        } else if (j < 0) {
            throw new IllegalArgumentException("Invalid delay specified: " + j);
        } else {
            this.b.d(aqVar.c, "Scheduling " + aqVar.c + " on " + apVar + " queue in " + j + "ms.");
            at atVar = new at(this, aqVar, apVar);
            if (apVar == ap.MAIN) {
                a(atVar, j, this.c);
            } else {
                a(atVar, j, this.d);
            }
        }
    }
}
