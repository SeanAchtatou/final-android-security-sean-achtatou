package com.applovin.impl.sdk;

import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

class y {
    public static final aa A = a("local_server_port", 7145);
    public static final aa B = a("local_server_enabled", true);
    public static final aa C = a("sdk_update_check_interval", 1440L);
    public static final aa D = a("ad_session_minutes", 60);
    public static final aa E = a("ad_request_parameters", "");
    public static final aa F = a("ad_refresh_enabled", true);
    public static final aa G = a("ad_refresh_seconds", 120L);
    public static final aa H = a("mrec_ad_refresh_enabled", true);
    public static final aa I = a("mrec_ad_refresh_seconds", 120L);
    public static final aa J = a("leader_ad_refresh_enabled", true);
    public static final aa K = a("leader_ad_refresh_seconds", 120L);
    public static final aa L = a("plugin_version", "");
    public static final aa M = a("ad_preload_enabled", true);
    public static final aa N = a("ad_resource_caching_enabled", true);
    public static final aa O = a("resource_cache_prefix", "http://pdn.applovin.com/,http://img.applovin.com/,http://d.applovin.com/");
    public static final aa P = a("ad_auto_preload_sizes", "BANNER,INTER");
    private static final List Q = Arrays.asList(Boolean.class, Float.class, Integer.class, Long.class, String.class);
    private static final List R = new ArrayList();
    public static final aa a = a("is_disabled", false);
    public static final aa b = a("should_load_user_settings", true);
    public static final aa c = a("device_id", "");
    public static final aa d = a("publisher_id", "");
    public static final aa e = a(TapjoyConstants.TJC_APP_ID_NAME, "");
    public static final aa f = a("device_token", "");
    public static final aa g = a("init_retry_count", 1);
    public static final aa h = a("submit_data_retry_count", 1);
    public static final aa i = a("fetch_ad_retry_count", 1);
    public static final aa j = a("conversion_retry_count", 1);
    public static final aa k = a("track_click_retry_count", 1);
    public static final aa l = a("is_verbose_logging", true);
    public static final aa m = a("api_endpoint", "http://d.applovin.com/");
    public static final aa n = a("adserver_endpoint", "http://a.applovin.com/");
    public static final aa o = a("next_device_init", 0L);
    public static final aa p = a("get_retry_dealy", 10000L);
    public static final aa q = a("max_apps_to_send", 100);
    public static final aa r = a("max_emails_to_send", 20);
    public static final aa s = a("is_app_list_shared", true);
    public static final aa t = a("next_app_list_update", 0L);
    public static final aa u = a("hash_algorithm", "SHA-1");
    public static final aa v = a("short_hash_size", 16);
    public static final aa w = a("http_connection_timeout", 20000);
    public static final aa x = a("http_socket_timeout", 20000);
    public static final aa y = a("error_save_count", 15);
    public static final aa z = a("is_app_icon_requested", false);

    private static aa a(String str, Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("No default value specified");
        } else if (!Q.contains(obj.getClass())) {
            throw new IllegalArgumentException("Unsupported value type: " + obj.getClass());
        } else {
            aa aaVar = new aa(str, obj);
            R.add(aaVar);
            return aaVar;
        }
    }

    public static Collection a() {
        return Collections.unmodifiableList(R);
    }

    public static int b() {
        return R.size();
    }
}
