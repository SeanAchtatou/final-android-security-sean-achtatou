package com.applovin.impl.adview;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.Logger;
import java.lang.ref.WeakReference;

class u implements AppLovinAdLoadListener {
    private final WeakReference a;
    private final Logger b;

    u(p pVar, Logger logger) {
        if (pVar == null) {
            throw new IllegalArgumentException("No view specified");
        } else if (logger == null) {
            throw new IllegalArgumentException("No logger specified");
        } else {
            this.a = new WeakReference(pVar);
            this.b = logger;
        }
    }

    public void adReceived(AppLovinAd appLovinAd) {
        p pVar = (p) this.a.get();
        if (pVar != null) {
            pVar.b(appLovinAd);
        } else {
            this.b.w("InterstitialAdDialog", "Interstitial ad view has been garbage collected by the time an ad was recieved");
        }
    }

    public void failedToReceiveAd(int i) {
        p pVar = (p) this.a.get();
        if (pVar != null) {
            pVar.a();
        }
    }
}
