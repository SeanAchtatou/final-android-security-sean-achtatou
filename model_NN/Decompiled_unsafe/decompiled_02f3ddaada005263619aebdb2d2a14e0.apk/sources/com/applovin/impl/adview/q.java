package com.applovin.impl.adview;

import android.app.Activity;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;

class q implements AppLovinAdDisplayListener {
    final /* synthetic */ Activity a;
    final /* synthetic */ p b;

    q(p pVar, Activity activity) {
        this.b = pVar;
        this.a = activity;
    }

    public void adDisplayed(AppLovinAd appLovinAd) {
        try {
            q.super.show();
            if (this.b.l != null) {
                this.b.l.adDisplayed(appLovinAd);
            }
        } catch (Throwable th) {
            this.b.c.userError("InterstitialAdDialog", "Exception while running app display callback", th);
        }
    }

    public void adHidden(AppLovinAd appLovinAd) {
        this.a.runOnUiThread(this.b.g);
        try {
            if (this.b.l != null) {
                this.b.l.adHidden(appLovinAd);
            }
        } catch (Throwable th) {
            this.b.c.userError("InterstitialAdDialog", "Exception while running app display callback", th);
        }
    }
}
