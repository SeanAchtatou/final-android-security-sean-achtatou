package com.applovin.impl.adview;

import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.Logger;

class k extends WebChromeClient {
    private final Logger a;

    public k(AppLovinSdk appLovinSdk) {
        this.a = appLovinSdk.getLogger();
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        this.a.d("AdWebView", consoleMessage.sourceId() + ": " + consoleMessage.lineNumber() + ": " + consoleMessage.message());
        return true;
    }

    public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        this.a.w("AdWebView", "Alert reported: " + str2);
        return super.onJsAlert(webView, str, str2, jsResult);
    }
}
