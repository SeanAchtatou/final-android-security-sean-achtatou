package com.applovin.impl.adview;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import com.applovin.adview.AppLovinAdView;
import com.applovin.sdk.AppLovinAdSize;
import com.tapjoy.TapjoyConstants;

class j {
    static AppLovinAdSize a(AttributeSet attributeSet) {
        String attributeValue;
        if (attributeSet == null || (attributeValue = attributeSet.getAttributeValue(AppLovinAdView.NAMESPACE, TapjoyConstants.TJC_DISPLAY_AD_SIZE)) == null) {
            return null;
        }
        return AppLovinAdSize.fromString(attributeValue);
    }

    static String a(AttributeSet attributeSet, Context context) {
        String str = null;
        if (attributeSet != null) {
            str = attributeSet.getAttributeValue(AppLovinAdView.NAMESPACE, "placement");
        }
        return (str != null || !(context instanceof Activity)) ? str : context.getClass().getName();
    }

    static boolean b(AttributeSet attributeSet) {
        if (attributeSet == null) {
            return false;
        }
        return attributeSet.getAttributeBooleanValue(AppLovinAdView.NAMESPACE, "loadAdOnCreate", false);
    }
}
