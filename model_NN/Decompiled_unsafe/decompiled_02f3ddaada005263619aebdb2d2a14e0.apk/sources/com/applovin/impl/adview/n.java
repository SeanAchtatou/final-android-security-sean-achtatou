package com.applovin.impl.adview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.ViewParent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.adview.AppLovinAdView;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.Logger;
import java.util.List;

class n extends WebViewClient {
    /* access modifiers changed from: private */
    public final AppLovinSdk a;
    private final Logger b;
    private AdViewControllerImpl c;

    public n(AdViewControllerImpl adViewControllerImpl, AppLovinSdk appLovinSdk) {
        this.a = appLovinSdk;
        this.b = appLovinSdk.getLogger();
        this.c = adViewControllerImpl;
    }

    private void a(Uri uri, l lVar) {
        try {
            lVar.getContext().startActivity(new Intent("android.intent.action.VIEW", uri));
        } catch (Throwable th) {
            this.b.e("AdWebViewClient", "Unable to show \"" + uri + "\".", th);
        }
    }

    private void e(l lVar) {
        AppLovinAd a2 = lVar.a();
        if (a2 != null) {
            this.c.a(a2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(WebView webView, String str) {
        this.b.d("AdWebViewClient", "Processing click on ad URL \"" + str + "\"");
        if (str != null && (webView instanceof l)) {
            Uri parse = Uri.parse(str);
            l lVar = (l) webView;
            String scheme = parse.getScheme();
            String host = parse.getHost();
            String path = parse.getPath();
            if (!AppLovinSdk.URI_SCHEME.equals(scheme) || !AppLovinSdk.URI_HOST.equals(host)) {
                e(lVar);
                a(parse, lVar);
            } else if (AppLovinAdService.URI_TRACK_CLICK.equals(path)) {
                d(lVar);
            } else if (AppLovinAdService.URI_NEXT_AD.equals(path)) {
                b(lVar);
            } else if (AppLovinAdService.URI_CLOSE_AD.equals(path)) {
                c(lVar);
            } else if (path.startsWith("/adservice/landing_page/")) {
                String substring = path.substring(AppLovinAdService.URI_LANDING_PAGE_AD.length() + 1);
                if (substring.length() > 0) {
                    e(lVar);
                    a(substring, lVar);
                    return;
                }
                b(lVar);
            } else if (path.startsWith("/launch/")) {
                List<String> pathSegments = parse.getPathSegments();
                if (pathSegments.size() > 1) {
                    String str2 = pathSegments.get(pathSegments.size() - 1);
                    try {
                        Context context = webView.getContext();
                        context.startActivity(context.getPackageManager().getLaunchIntentForPackage(str2));
                        e(lVar);
                    } catch (Exception e) {
                        this.b.e("AdWebViewClient", "Threw Exception Trying to Launch App for Package: " + str2, e);
                    }
                }
            } else {
                this.b.e("AdWebViewClient", "Unknown URL: " + str);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(l lVar) {
        AppLovinAd a2 = lVar.a();
        if (a2 != null) {
            a(lVar, a2.getDestinationUrl());
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, l lVar) {
        Context context = lVar.getContext();
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            activity.runOnUiThread(new o(this, activity, str));
            return;
        }
        this.b.e("AdWebViewClient", "Unable to show landing page, context is not activity");
    }

    /* access modifiers changed from: protected */
    public void b(l lVar) {
        ViewParent parent = lVar.getParent();
        if (parent instanceof AppLovinAdView) {
            ((AppLovinAdView) parent).loadNextAd();
        }
    }

    /* access modifiers changed from: protected */
    public void c(l lVar) {
        this.c.a(lVar);
    }

    /* access modifiers changed from: protected */
    public void d(l lVar) {
        AppLovinAd a2 = lVar.a();
        if (a2 != null) {
            a(lVar, a2.getDestinationUrl());
        }
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.c.onAdHtmlLoaded(webView);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a(webView, str);
        return true;
    }
}
