package com.applovin.impl.adview;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;

class r implements AppLovinAdLoadListener {
    final /* synthetic */ p a;

    r(p pVar) {
        this.a = pVar;
    }

    public void adReceived(AppLovinAd appLovinAd) {
        try {
            if (this.a.k != null) {
                this.a.k.adReceived(appLovinAd);
            }
        } catch (Throwable th) {
            this.a.c.userError("InterstitialAdDialog", "Exception while running app load callback", th);
        }
    }

    public void failedToReceiveAd(int i) {
        try {
            if (this.a.k != null) {
                this.a.k.failedToReceiveAd(i);
            }
        } catch (Throwable th) {
            this.a.c.userError("InterstitialAdDialog", "Exception while running app load callback", th);
        }
    }
}
