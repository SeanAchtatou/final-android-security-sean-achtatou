package com.applovin.impl.adview;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.RelativeLayout;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.impl.sdk.k;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.Logger;

class p extends Dialog implements AppLovinInterstitialAdDialog {
    private final Activity a;
    private final AppLovinSdk b;
    /* access modifiers changed from: private */
    public final Logger c;
    /* access modifiers changed from: private */
    public AppLovinAdView d;
    private u e;
    private t f;
    /* access modifiers changed from: private */
    public Runnable g;
    private AppLovinAd h;
    private String i;
    /* access modifiers changed from: private */
    public AppLovinAd j;
    /* access modifiers changed from: private */
    public AppLovinAdLoadListener k;
    /* access modifiers changed from: private */
    public AppLovinAdDisplayListener l;

    p(AppLovinSdk appLovinSdk, Activity activity) {
        super(activity, 16973840);
        if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        }
        this.b = appLovinSdk;
        this.c = appLovinSdk.getLogger();
        this.a = activity;
        this.e = new u(this, this.c);
        this.f = new t(this);
        this.g = new s(this, null);
        this.d = new AppLovinAdView(appLovinSdk, AppLovinAdSize.INTERSTITIAL, activity);
        this.d.setAdDisplayListener(new q(this, activity));
        this.d.setAdLoadListener(new r(this));
        requestWindowFeature(1);
        try {
            getWindow().setFlags(activity.getWindow().getAttributes().flags, activity.getWindow().getAttributes().flags);
        } catch (Exception e2) {
            this.c.e("InterstitialAdDialog", "Set window flags failed.", e2);
        }
    }

    private void b() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.d.setLayoutParams(layoutParams);
        RelativeLayout relativeLayout = new RelativeLayout(this.a);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        relativeLayout.setBackgroundColor(-1157627904);
        relativeLayout.addView(this.d);
        setContentView(relativeLayout);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.applovin.impl.sdk.k.a(boolean, android.content.Context):boolean
     arg types: [int, android.app.Activity]
     candidates:
      com.applovin.impl.sdk.k.a(java.lang.String, com.applovin.impl.sdk.AppLovinSdkImpl):java.lang.String
      com.applovin.impl.sdk.k.a(int, com.applovin.impl.sdk.AppLovinSdkImpl):void
      com.applovin.impl.sdk.k.a(org.json.JSONObject, com.applovin.impl.sdk.AppLovinSdkImpl):void
      com.applovin.impl.sdk.k.a(int, int[]):boolean
      com.applovin.impl.sdk.k.a(boolean, android.content.Context):boolean */
    private boolean c() {
        try {
            return k.a(true, (Context) this.a);
        } catch (Exception e2) {
            this.c.e("InterstitialAdDialog", "Unable to check network state", e2);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.i = null;
        try {
            if (this.k != null) {
                this.k.failedToReceiveAd(-103);
            }
        } catch (Throwable th) {
            this.c.userError("InterstitialAdDialog", "Exception while running app load callback", th);
        }
        this.a.runOnUiThread(this.g);
        this.c.w("InterstitialAdDialog", "Failed to receive an ad,activity finished");
    }

    /* access modifiers changed from: package-private */
    public void a(AppLovinAd appLovinAd) {
        this.h = appLovinAd;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.i = str;
    }

    /* access modifiers changed from: protected */
    public void b(AppLovinAd appLovinAd) {
        this.j = appLovinAd;
        this.i = null;
        this.h = null;
        this.a.runOnUiThread(this.f);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b();
    }

    public void setAdDisplayListener(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.l = appLovinAdDisplayListener;
    }

    public void setAdLoadListener(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.k = appLovinAdLoadListener;
    }

    public void show() {
        if (this.h != null) {
            b(this.h);
        } else if (c()) {
            AppLovinAdService adService = this.b.getAdService();
            if (this.i != null) {
                adService.loadNextAd(this.i, this.e);
            } else {
                adService.loadNextAd(AppLovinAdSize.INTERSTITIAL, this.e);
            }
        } else {
            this.e.failedToReceiveAd(-103);
        }
    }
}
