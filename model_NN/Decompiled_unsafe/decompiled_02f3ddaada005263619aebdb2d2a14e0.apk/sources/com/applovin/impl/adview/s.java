package com.applovin.impl.adview;

final class s implements Runnable {
    final /* synthetic */ p a;

    private s(p pVar) {
        this.a = pVar;
    }

    /* synthetic */ s(p pVar, q qVar) {
        this(pVar);
    }

    public void run() {
        try {
            this.a.dismiss();
        } catch (Throwable th) {
            if (this.a.c != null) {
                this.a.c.e("InterstitialAdDialog", "dismiss() threw exception", th);
            }
        }
    }
}
