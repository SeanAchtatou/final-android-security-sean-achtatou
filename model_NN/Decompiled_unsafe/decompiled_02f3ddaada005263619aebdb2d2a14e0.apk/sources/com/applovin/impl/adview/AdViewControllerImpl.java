package com.applovin.impl.adview;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewDatabase;
import android.widget.RelativeLayout;
import com.applovin.adview.AdViewController;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdService;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.Logger;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicReference;

public class AdViewControllerImpl implements AdViewController {
    /* access modifiers changed from: private */
    public Activity a;
    /* access modifiers changed from: private */
    public AppLovinSdk b;
    private AppLovinAdService c;
    /* access modifiers changed from: private */
    public Logger d;
    private AppLovinAdSize e;
    private String f;
    private n g;
    private i h;
    /* access modifiers changed from: private */
    public l i;
    private AppLovinAd j;
    private Runnable k;
    private Runnable l;
    private Runnable m;
    /* access modifiers changed from: private */
    public volatile AppLovinAd n;
    private final AtomicReference o = new AtomicReference();
    private volatile boolean p = false;
    private volatile boolean q = true;
    private volatile boolean r = false;
    private volatile boolean s = false;
    /* access modifiers changed from: private */
    public volatile AppLovinAdLoadListener t;
    /* access modifiers changed from: private */
    public volatile AppLovinAdDisplayListener u;
    /* access modifiers changed from: private */
    public volatile AppLovinAdClickListener v;

    private l a() {
        l lVar = new l(this.e, this.g, this.b, this.a);
        lVar.addJavascriptInterface(new AdWebViewJsInterface(this.g, lVar), "applovin_sdk");
        lVar.setBackgroundColor(0);
        lVar.setWillNotCacheDrawing(false);
        return lVar;
    }

    private void a(ViewGroup viewGroup, AppLovinSdk appLovinSdk, AppLovinAdSize appLovinAdSize, String str, Context context) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (appLovinAdSize == null) {
            throw new IllegalArgumentException("No ad size specified");
        } else if (!(context instanceof Activity)) {
            throw new IllegalArgumentException("Specified context is not an activity");
        } else {
            this.b = appLovinSdk;
            this.c = appLovinSdk.getAdService();
            this.d = appLovinSdk.getLogger();
            this.e = appLovinAdSize;
            this.a = (Activity) context;
            this.f = str;
            this.j = new AppLovinAd("", appLovinAdSize, "", "");
            this.g = new n(this, appLovinSdk);
            this.m = new c(this);
            this.k = new h(this);
            this.l = new f(this);
            this.h = new i(this, appLovinSdk);
            if (a(context)) {
                this.i = a();
                viewGroup.setBackgroundColor(0);
                viewGroup.addView(this.i);
                b(this.i, appLovinAdSize);
                this.i.setVisibility(8);
                a(new g(this));
                this.p = true;
                return;
            }
            this.d.userError("AppLovinAdView", "Web view database is currupt, AdView not loaded");
        }
    }

    private void a(Runnable runnable) {
        this.a.runOnUiThread(runnable);
    }

    private static boolean a(Context context) {
        try {
            if (Build.VERSION.SDK_INT >= 11) {
                return true;
            }
            WebViewDatabase instance = WebViewDatabase.getInstance(context);
            Method declaredMethod = WebViewDatabase.class.getDeclaredMethod("getCacheTotalSize", new Class[0]);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(instance, new Object[0]);
            return true;
        } catch (NoSuchMethodException e2) {
            Log.e("AppLovinAdView", "Error invoking getCacheTotalSize()", e2);
            return true;
        } catch (IllegalArgumentException e3) {
            Log.e("AppLovinAdView", "Error invoking getCacheTotalSize()", e3);
            return true;
        } catch (IllegalAccessException e4) {
            Log.e("AppLovinAdView", "Error invoking getCacheTotalSize()", e4);
            return true;
        } catch (InvocationTargetException e5) {
            Log.e("AppLovinAdView", "getCacheTotalSize() reported exception", e5);
            return false;
        } catch (Throwable th) {
            Log.e("AppLovinAdView", "Unexpected error while checking DB state", th);
            return false;
        }
    }

    /* access modifiers changed from: private */
    public static void b(View view, AppLovinAdSize appLovinAdSize) {
        DisplayMetrics displayMetrics = view.getResources().getDisplayMetrics();
        int applyDimension = appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel()) ? -1 : appLovinAdSize.getWidth() == -1 ? displayMetrics.widthPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getWidth(), displayMetrics);
        int applyDimension2 = appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel()) ? -1 : appLovinAdSize.getHeight() == -1 ? displayMetrics.heightPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getHeight(), displayMetrics);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        }
        layoutParams.width = applyDimension;
        layoutParams.height = applyDimension2;
        if (layoutParams instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) layoutParams;
            layoutParams2.addRule(10);
            layoutParams2.addRule(9);
        }
        view.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: package-private */
    public void a(WebView webView) {
        a(this.m);
        a(new e(this, this.n));
        this.n = null;
    }

    /* access modifiers changed from: package-private */
    public void a(AppLovinAd appLovinAd) {
        this.b.getAdService().trackAdClick(appLovinAd);
        a(new d(this, appLovinAd));
    }

    public void destroy() {
        if (this.c != null) {
            this.c.removeAdUpdateListener(this.h, getSize());
        }
        if (this.i != null) {
            this.i.destroy();
        }
        this.r = true;
    }

    public String getPlacement() {
        return this.f;
    }

    public AppLovinAdSize getSize() {
        return this.e;
    }

    public void initializeAdView(ViewGroup viewGroup, Context context, AppLovinAdSize appLovinAdSize, AppLovinSdk appLovinSdk, AttributeSet attributeSet) {
        AppLovinAdSize appLovinAdSize2;
        if (viewGroup == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (context == null) {
            Log.e(Logger.SDK_TAG, "Unable to create AppLovinAdView: no context provided. Please use a different constructor for this view.");
        } else {
            if (appLovinAdSize == null) {
                appLovinAdSize2 = j.a(attributeSet);
                if (appLovinAdSize2 == null) {
                    appLovinAdSize2 = AppLovinAdSize.BANNER;
                }
            } else {
                appLovinAdSize2 = appLovinAdSize;
            }
            String a2 = j.a(attributeSet, context);
            AppLovinSdk instance = appLovinSdk == null ? AppLovinSdk.getInstance(context) : appLovinSdk;
            if (instance != null && !instance.hasCriticalErrors()) {
                a(viewGroup, instance, appLovinAdSize2, a2, context);
                if (j.b(attributeSet)) {
                    loadNextAd();
                }
            }
        }
    }

    public boolean isAutoDestroy() {
        return this.q;
    }

    public void loadNextAd() {
        if (this.b == null || this.h == null || this.a == null || !this.p) {
            Log.i(Logger.SDK_TAG, "Unable to load next ad: AppLovinAdView is not initialized.");
        } else {
            this.c.loadNextAd(this.e, this.f, this.h);
        }
    }

    public void onAdHtmlLoaded(WebView webView) {
        if (this.n != null) {
            webView.setVisibility(0);
            try {
                if (this.u != null) {
                    this.u.adDisplayed(this.n);
                }
            } catch (Throwable th) {
                this.d.userError("AppLovinAdView", "Exception while notifying ad display listener", th);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAdRecieved(AppLovinAd appLovinAd) {
        if (appLovinAd != null) {
            this.s = true;
            if (!this.r) {
                this.c.addAdUpdateListener(this.h, this.e);
                renderAd(appLovinAd);
            } else {
                this.o.set(appLovinAd);
                this.d.d("AppLovinAdView", "Ad view has paused when an ad was recieved, ad saved for later");
            }
            a(new a(this, appLovinAd));
            return;
        }
        this.d.e("AppLovinAdView", "No provided when to the view controller");
        onFailedToRecieveAd(-1);
    }

    public void onDetachedFromWindow() {
        if (this.p) {
            a(new e(this, this.n));
            if (this.q) {
                destroy();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onFailedToRecieveAd(int i2) {
        if (!this.r) {
            this.c.addAdUpdateListener(this.h, this.e);
            a(this.m);
        }
        a(new b(this, i2));
    }

    public void onVisibilityChanged(int i2) {
        if (!this.p || !this.q) {
            return;
        }
        if (i2 == 8 || i2 == 4) {
            pause();
        } else if (i2 == 0) {
            resume();
        }
    }

    public void pause() {
        if (this.p) {
            this.c.removeAdUpdateListener(this.h, getSize());
            AppLovinAd appLovinAd = this.n;
            renderAd(this.j);
            if (appLovinAd != null) {
                this.o.set(appLovinAd);
            }
            this.r = true;
        }
    }

    public void renderAd(AppLovinAd appLovinAd) {
        if (appLovinAd == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (!this.p) {
            Log.i(Logger.SDK_TAG, "Unable to render ad: AppLovinAdView is not initialized.");
        } else if (appLovinAd != this.n) {
            this.d.d("AppLovinAdView", "Rendering " + appLovinAd.getSize() + " ad for \"" + appLovinAd.getDestinationUrl() + "\"...");
            a(new e(this, this.n));
            this.o.set(null);
            this.n = appLovinAd;
            if (appLovinAd.getSize() == this.e) {
                a(this.k);
            } else if (appLovinAd.getSize() == AppLovinAdSize.INTERSTITIAL) {
                a(this.m);
                a(this.l);
            }
        } else {
            this.d.w("AppLovinAdView", "Ad for \"" + appLovinAd.getDestinationUrl() + "\" is already showing, ignoring");
        }
    }

    public void resume() {
        if (this.p) {
            if (this.s) {
                this.c.addAdUpdateListener(this.h, this.e);
            }
            AppLovinAd appLovinAd = (AppLovinAd) this.o.getAndSet(null);
            if (appLovinAd != null) {
                renderAd(appLovinAd);
            }
            this.r = false;
        }
    }

    public void setAdClickListener(AppLovinAdClickListener appLovinAdClickListener) {
        this.v = appLovinAdClickListener;
    }

    public void setAdDisplayListener(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.u = appLovinAdDisplayListener;
    }

    public void setAdLoadListener(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.t = appLovinAdLoadListener;
    }

    public void setAutoDestroy(boolean z) {
        this.q = z;
    }

    public void setPlacement(String str) {
        this.f = str;
    }
}
