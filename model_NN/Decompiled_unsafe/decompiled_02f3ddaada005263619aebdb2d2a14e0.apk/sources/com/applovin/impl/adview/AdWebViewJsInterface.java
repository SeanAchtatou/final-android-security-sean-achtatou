package com.applovin.impl.adview;

import android.webkit.WebView;

public class AdWebViewJsInterface {
    private final n a;
    private final WebView b;

    AdWebViewJsInterface(n nVar, WebView webView) {
        this.a = nVar;
        this.b = webView;
    }

    public void execute(String str) {
        showUrl("applovin://com.applovin.sdk/adservice/" + str);
    }

    public void showUrl(String str) {
        this.a.a(this.b, str);
    }
}
