package com.applovin.impl.adview;

import android.view.MotionEvent;
import android.view.View;
import com.applovin.sdk.AppLovinAdSize;

class m implements View.OnTouchListener {
    final /* synthetic */ AppLovinAdSize a;
    final /* synthetic */ n b;
    final /* synthetic */ l c;

    m(l lVar, AppLovinAdSize appLovinAdSize, n nVar) {
        this.c = lVar;
        this.a = appLovinAdSize;
        this.b = nVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.a == AppLovinAdSize.BANNER || this.a == AppLovinAdSize.MREC || this.a == AppLovinAdSize.LEADER) {
            boolean z = motionEvent.getAction() == 2;
            boolean z2 = motionEvent.getAction() == 0;
            if (z) {
                return true;
            }
            if (z2) {
                this.b.a(this.c);
                return true;
            }
        }
        if (!view.hasFocus()) {
            view.requestFocus();
        }
        return false;
    }
}
