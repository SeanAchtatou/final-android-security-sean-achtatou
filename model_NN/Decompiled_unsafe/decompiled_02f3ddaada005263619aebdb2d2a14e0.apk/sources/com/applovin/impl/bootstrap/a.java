package com.applovin.impl.bootstrap;

import android.content.Context;
import android.os.Bundle;
import com.applovin.sdk.AppLovinSdkUtils;

class a extends AppLovinSdkUtils {
    static String a(Context context) {
        String string;
        Bundle retrieveMetadata = retrieveMetadata(context);
        return (retrieveMetadata == null || (string = retrieveMetadata.getString("applovin.sdk.autoupdate_endpoint")) == null || string.length() <= 5) ? "http://d.applovin.com/sdk/android" : string;
    }
}
