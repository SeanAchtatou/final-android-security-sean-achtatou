package com.applovin.impl.bootstrap;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.applovin.impl.sdk.bf;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.Logger;
import com.applovin.sdk.bootstrap.SdkBoostrapTasks;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SdkBoostrapTasksImpl implements SdkBoostrapTasks {
    private boolean a = true;

    /* access modifiers changed from: private */
    public void a(String str) {
        a(str, (Throwable) null);
    }

    /* access modifiers changed from: private */
    public void a(String str, Throwable th) {
        if (this.a) {
            Log.i(Logger.SDK_TAG, "[Boostrap] " + str, th);
        }
    }

    /* access modifiers changed from: private */
    public static String b(InputStream inputStream, File file) {
        FileOutputStream fileOutputStream;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            try {
                fileOutputStream = new FileOutputStream(file);
                byte[] bArr = new byte[51200];
                while (true) {
                    int read = inputStream.read(bArr, 0, bArr.length);
                    if (read >= 0) {
                        fileOutputStream.write(bArr, 0, read);
                        instance.update(bArr, 0, read);
                    } else {
                        fileOutputStream.close();
                        inputStream.close();
                        return bf.a(instance.digest());
                    }
                }
            } catch (Throwable th) {
                inputStream.close();
                throw th;
            }
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Programming error: unsupported algorithm: SHA1");
        }
    }

    public void startUpdateDownload(Context context) {
        this.a = AppLovinSdkUtils.isVerboseLoggingEnabled(context);
        SharedPreferences sharedPreferences = context.getSharedPreferences(SdkBoostrapTasks.SHARED_PREFERENCES_KEY, 0);
        long currentTimeMillis = System.currentTimeMillis();
        long j = sharedPreferences.getLong("NextAutoupdateTime", 0);
        if (j == 0 || currentTimeMillis > j) {
            new b(this, context).start();
        }
    }
}
