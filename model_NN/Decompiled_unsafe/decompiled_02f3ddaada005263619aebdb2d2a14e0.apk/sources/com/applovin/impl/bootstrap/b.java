package com.applovin.impl.bootstrap;

import android.content.Context;

class b extends Thread {
    final /* synthetic */ SdkBoostrapTasksImpl a;
    private final Context b;

    public b(SdkBoostrapTasksImpl sdkBoostrapTasksImpl, Context context) {
        this.a = sdkBoostrapTasksImpl;
        this.b = context;
        setName("AppLovinUpdateThread");
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:37:0x0200=Splitter:B:37:0x0200, B:23:0x01c4=Splitter:B:23:0x01c4, B:28:0x01cd=Splitter:B:28:0x01cd} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            r3 = 1
            r2 = 0
            android.content.Context r0 = r12.b
            java.lang.String r1 = "applovin.sdk.boostrap"
            android.content.SharedPreferences r2 = r0.getSharedPreferences(r1, r2)
            java.lang.String r0 = "version"
            java.lang.String r1 = ""
            java.lang.String r0 = r2.getString(r0, r1)
            if (r0 == 0) goto L_0x001a
            int r1 = r0.length()
            if (r1 >= r3) goto L_0x001c
        L_0x001a:
            java.lang.String r0 = "5.0.0"
        L_0x001c:
            r1 = 0
            com.applovin.impl.bootstrap.SdkBoostrapTasksImpl r3 = r12.a     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f0 }
            r4.<init>()     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "Checking for an update for the SDK interface: 5.0.0, implementation: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "..."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = r4.toString()     // Catch:{ Throwable -> 0x01f0 }
            r3.a(r4)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ Throwable -> 0x01f0 }
            android.content.Context r4 = r12.b     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = com.applovin.impl.bootstrap.a.a(r4)     // Catch:{ Throwable -> 0x01f0 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = "?"
            java.lang.StringBuffer r4 = r3.append(r4)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "interface"
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "5.0.0"
            r4.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = "&"
            java.lang.StringBuffer r4 = r3.append(r4)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "implementation"
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "="
            java.lang.StringBuffer r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            r4.append(r0)     // Catch:{ Throwable -> 0x01f0 }
            java.net.URL r0 = new java.net.URL     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = r3.toString()     // Catch:{ Throwable -> 0x01f0 }
            r0.<init>(r4)     // Catch:{ Throwable -> 0x01f0 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Throwable -> 0x01f0 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = "GET"
            r0.setRequestMethod(r4)     // Catch:{ Throwable -> 0x01f0 }
            r4 = 20000(0x4e20, float:2.8026E-41)
            r0.setConnectTimeout(r4)     // Catch:{ Throwable -> 0x01f0 }
            r4 = 20000(0x4e20, float:2.8026E-41)
            r0.setReadTimeout(r4)     // Catch:{ Throwable -> 0x01f0 }
            r4 = 0
            r0.setDefaultUseCaches(r4)     // Catch:{ Throwable -> 0x01f0 }
            r4 = 0
            r0.setAllowUserInteraction(r4)     // Catch:{ Throwable -> 0x01f0 }
            r4 = 0
            r0.setUseCaches(r4)     // Catch:{ Throwable -> 0x01f0 }
            r4 = 1
            r0.setInstanceFollowRedirects(r4)     // Catch:{ Throwable -> 0x01f0 }
            r4 = 1
            r0.setDoInput(r4)     // Catch:{ Throwable -> 0x01f0 }
            int r4 = r0.getResponseCode()     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "AppLovin-Sdk-Implementation"
            java.lang.String r5 = r0.getHeaderField(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r6 = "AppLovin-Sdk-Implementation-Checksum"
            java.lang.String r6 = r0.getHeaderField(r6)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r7 = "AppLovin-Sdk-Update-Interval"
            java.lang.String r7 = r0.getHeaderField(r7)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r8 = "AppLovin-Event-ID"
            java.lang.String r8 = r0.getHeaderField(r8)     // Catch:{ Throwable -> 0x01f0 }
            com.applovin.impl.bootstrap.SdkBoostrapTasksImpl r9 = r12.a     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f0 }
            r10.<init>()     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r11 = "Auto-update info: {code: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r10 = r10.append(r4)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r11 = ", "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r11 = "eventId: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r10 = r10.append(r8)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r11 = ", "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r11 = "fileName: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r10 = r10.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r11 = ", "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r11 = "checksum: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r10 = r10.append(r6)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r11 = ", "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r11 = "interval: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r10 = r10.append(r7)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r11 = "}"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r10 = r10.toString()     // Catch:{ Throwable -> 0x01f0 }
            r9.a(r10)     // Catch:{ Throwable -> 0x01f0 }
            r9 = 200(0xc8, float:2.8E-43)
            if (r4 != r9) goto L_0x0227
            if (r5 == 0) goto L_0x0200
            int r4 = r5.length()     // Catch:{ Throwable -> 0x01f0 }
            if (r4 <= 0) goto L_0x0200
            java.io.File r3 = new java.io.File     // Catch:{ Throwable -> 0x01f0 }
            android.content.Context r4 = r12.b     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r9 = "al_sdk"
            r10 = 0
            java.io.File r4 = r4.getDir(r9, r10)     // Catch:{ Throwable -> 0x01f0 }
            r3.<init>(r4, r5)     // Catch:{ Throwable -> 0x01f0 }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r0 = com.applovin.impl.bootstrap.SdkBoostrapTasksImpl.b(r1, r3)     // Catch:{ Throwable -> 0x01f0 }
            if (r0 == 0) goto L_0x01cd
            boolean r3 = r0.equals(r6)     // Catch:{ Throwable -> 0x01f0 }
            if (r3 == 0) goto L_0x01cd
            android.content.SharedPreferences$Editor r3 = r2.edit()     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = "version"
            r3.putString(r4, r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = "interface"
            java.lang.String r6 = "5.0.0"
            r3.putString(r4, r6)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = "ServerEventId"
            r3.putString(r4, r8)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = "ServerChecksum"
            r3.putString(r4, r0)     // Catch:{ Throwable -> 0x01f0 }
            r3.commit()     // Catch:{ Throwable -> 0x01f0 }
            com.applovin.impl.bootstrap.SdkBoostrapTasksImpl r0 = r12.a     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f0 }
            r3.<init>()     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = "New update processed: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x01f0 }
            r0.a(r3)     // Catch:{ Throwable -> 0x01f0 }
        L_0x017d:
            if (r7 == 0) goto L_0x01c7
            int r0 = r7.length()     // Catch:{ Throwable -> 0x01f0 }
            if (r0 <= 0) goto L_0x01c7
            android.content.SharedPreferences$Editor r0 = r2.edit()     // Catch:{ Throwable -> 0x01f0 }
            double r2 = java.lang.Double.parseDouble(r7)     // Catch:{ Exception -> 0x027e }
            java.lang.Double r2 = java.lang.Double.valueOf(r2)     // Catch:{ Exception -> 0x027e }
            double r2 = r2.doubleValue()     // Catch:{ Exception -> 0x027e }
            long r2 = java.lang.Math.round(r2)     // Catch:{ Exception -> 0x027e }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x027e }
            r6 = 1000(0x3e8, double:4.94E-321)
            long r2 = r2 * r6
            long r2 = r2 + r4
            java.lang.String r4 = "NextAutoupdateTime"
            r0.putLong(r4, r2)     // Catch:{ Exception -> 0x027e }
            com.applovin.impl.bootstrap.SdkBoostrapTasksImpl r4 = r12.a     // Catch:{ Exception -> 0x027e }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x027e }
            r5.<init>()     // Catch:{ Exception -> 0x027e }
            java.lang.String r6 = "Next update is at: \""
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x027e }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ Exception -> 0x027e }
            java.lang.String r3 = "\""
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x027e }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x027e }
            r4.a(r2)     // Catch:{ Exception -> 0x027e }
        L_0x01c4:
            r0.commit()     // Catch:{ Throwable -> 0x01f0 }
        L_0x01c7:
            if (r1 == 0) goto L_0x01cc
            r1.close()     // Catch:{ IOException -> 0x0279 }
        L_0x01cc:
            return
        L_0x01cd:
            com.applovin.impl.bootstrap.SdkBoostrapTasksImpl r3 = r12.a     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f0 }
            r4.<init>()     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "SDK update checksum does not match. Expected "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = ", but got "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x01f0 }
            r3.a(r0)     // Catch:{ Throwable -> 0x01f0 }
            goto L_0x017d
        L_0x01f0:
            r0 = move-exception
            com.applovin.impl.bootstrap.SdkBoostrapTasksImpl r2 = r12.a     // Catch:{ all -> 0x0220 }
            java.lang.String r3 = "Unable to receive SDK update"
            r2.a(r3, r0)     // Catch:{ all -> 0x0220 }
            if (r1 == 0) goto L_0x01cc
            r1.close()     // Catch:{ IOException -> 0x01fe }
            goto L_0x01cc
        L_0x01fe:
            r0 = move-exception
            goto L_0x01cc
        L_0x0200:
            com.applovin.impl.bootstrap.SdkBoostrapTasksImpl r0 = r12.a     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f0 }
            r4.<init>()     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "Unable to receive SDK update: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = " has not returend a file name"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x01f0 }
            r0.a(r3)     // Catch:{ Throwable -> 0x01f0 }
            goto L_0x017d
        L_0x0220:
            r0 = move-exception
            if (r1 == 0) goto L_0x0226
            r1.close()     // Catch:{ IOException -> 0x027c }
        L_0x0226:
            throw r0
        L_0x0227:
            r0 = 404(0x194, float:5.66E-43)
            if (r4 != r0) goto L_0x0255
            com.applovin.impl.bootstrap.SdkBoostrapTasksImpl r0 = r12.a     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f0 }
            r4.<init>()     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "No new updates available from \""
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = "\", will check again in "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r4 = " seconds"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x01f0 }
            r0.a(r3)     // Catch:{ Throwable -> 0x01f0 }
            goto L_0x017d
        L_0x0255:
            com.applovin.impl.bootstrap.SdkBoostrapTasksImpl r0 = r12.a     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01f0 }
            r5.<init>()     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r6 = "Unknown response code recieved from \""
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r5 = "\": "
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01f0 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x01f0 }
            r0.a(r3)     // Catch:{ Throwable -> 0x01f0 }
            goto L_0x017d
        L_0x0279:
            r0 = move-exception
            goto L_0x01cc
        L_0x027c:
            r1 = move-exception
            goto L_0x0226
        L_0x027e:
            r2 = move-exception
            goto L_0x01c4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.bootstrap.b.run():void");
    }
}
