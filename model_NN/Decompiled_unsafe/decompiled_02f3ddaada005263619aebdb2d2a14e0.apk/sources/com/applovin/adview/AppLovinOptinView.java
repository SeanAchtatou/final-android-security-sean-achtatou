package com.applovin.adview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

@Deprecated
public class AppLovinOptinView extends View {
    public AppLovinOptinView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AppLovinOptinView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setVisibility(8);
    }
}
