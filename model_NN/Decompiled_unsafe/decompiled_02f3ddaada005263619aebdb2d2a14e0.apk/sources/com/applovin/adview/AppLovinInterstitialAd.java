package com.applovin.adview;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.Logger;
import com.applovin.sdk.bootstrap.SdkBootstrap;

public class AppLovinInterstitialAd extends View {
    private AppLovinInterstitialAdDialog a;

    public AppLovinInterstitialAd(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AppLovinInterstitialAd(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        InterstitialAdDialogCreator interstitialAdDialogCreator;
        this.a = null;
        if (context instanceof Activity) {
            AppLovinSdk instance = AppLovinSdk.getInstance(context);
            if (!(instance == null || instance.hasCriticalErrors() || (interstitialAdDialogCreator = (InterstitialAdDialogCreator) SdkBootstrap.getInstance(context).loadImplementation(InterstitialAdDialogCreator.class)) == null)) {
                this.a = interstitialAdDialogCreator.createInterstitialAdDialog(instance, (Activity) context);
            }
        } else {
            Log.e(Logger.SDK_TAG, "Unable to create AppLovin interstitial dialog. The view was not created from an activity.");
        }
        setVisibility(8);
    }

    public static AppLovinInterstitialAdDialog create(AppLovinSdk appLovinSdk, Activity activity) {
        if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else {
            InterstitialAdDialogCreator interstitialAdDialogCreator = (InterstitialAdDialogCreator) SdkBootstrap.getInstance(activity).loadImplementation(InterstitialAdDialogCreator.class);
            if (interstitialAdDialogCreator != null) {
                return interstitialAdDialogCreator.createInterstitialAdDialog(appLovinSdk, activity);
            }
            return null;
        }
    }

    public static void show(Activity activity) {
        if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        }
        AppLovinSdk instance = AppLovinSdk.getInstance(activity);
        if (instance != null && !instance.hasCriticalErrors()) {
            show(instance, activity);
        }
    }

    public static void show(AppLovinSdk appLovinSdk, Activity activity) {
        if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else {
            activity.runOnUiThread(new a(activity, appLovinSdk));
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.a != null) {
            this.a.show();
        }
    }
}
