package com.applovin.adview;

import android.app.Activity;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.bootstrap.SdkBootstrap;

final class a implements Runnable {
    final /* synthetic */ Activity a;
    final /* synthetic */ AppLovinSdk b;

    a(Activity activity, AppLovinSdk appLovinSdk) {
        this.a = activity;
        this.b = appLovinSdk;
    }

    public void run() {
        InterstitialAdDialogCreator interstitialAdDialogCreator = (InterstitialAdDialogCreator) SdkBootstrap.getInstance(this.a).loadImplementation(InterstitialAdDialogCreator.class);
        if (interstitialAdDialogCreator != null) {
            interstitialAdDialogCreator.createInterstitialAdDialog(this.b, this.a).show();
        }
    }
}
