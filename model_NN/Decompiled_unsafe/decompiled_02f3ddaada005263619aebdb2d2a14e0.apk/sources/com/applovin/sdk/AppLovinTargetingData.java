package com.applovin.sdk;

import android.location.Location;

public interface AppLovinTargetingData {
    public static final String EDUCATION_BACHELORS = "bachelors";
    public static final String EDUCATION_COLLEGE = "college";
    public static final String EDUCATION_DOCTORAL = "doctoral";
    public static final String EDUCATION_HIGH_SCHOOL = "high_school";
    public static final String EDUCATION_IN_COLLEGE = "in_college";
    public static final String EDUCATION_MASTERS = "masters";
    public static final String EDUCATION_NONE = "none";
    public static final String EDUCATION_OTHER = "other";
    public static final String ETHNICITY_ASIAN = "asian";
    public static final String ETHNICITY_BLACK = "black";
    public static final String ETHNICITY_HISPANIC = "hispanic";
    public static final String ETHNICITY_MIXED = "mixed";
    public static final String ETHNICITY_NATIVE_AMERICAN = "native_american";
    public static final String ETHNICITY_NONE = "none";
    public static final String ETHNICITY_OTHER = "other";
    public static final String ETHNICITY_WHITE = "white";
    public static final char GENDER_FEMALE = 'f';
    public static final char GENDER_MALE = 'm';
    public static final String MARITAL_STATUS_DIVORCED = "divorced";
    public static final String MARITAL_STATUS_MARRIED = "married";
    public static final String MARITAL_STATUS_NONE = "none";
    public static final String MARITAL_STATUS_SINGLE = "single";
    public static final String MARITAL_STATUS_WIDOWED = "widowed";

    void clearData();

    void putExtra(String str, String str2);

    void setBirthYear(int i);

    void setCarrier(String str);

    void setCountry(String str);

    void setEducation(String str);

    void setEmail(String str);

    void setEmails(String... strArr);

    void setEthnicity(String str);

    void setFirstName(String str);

    void setGender(char c);

    void setHashedEmail(String str);

    void setHashedEmails(String... strArr);

    void setHashedPhoneNumber(String str);

    void setIncome(String str);

    void setInterests(String... strArr);

    void setKeywords(String... strArr);

    void setLanguage(String str);

    void setLocation(Location location);

    void setMaritalStatus(String str);

    void setPhoneNumber(String str);
}
