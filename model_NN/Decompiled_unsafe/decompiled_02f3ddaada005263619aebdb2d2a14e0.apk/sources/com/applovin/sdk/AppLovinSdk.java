package com.applovin.sdk;

import android.content.Context;
import android.util.Log;

public abstract class AppLovinSdk {
    public static final String URI_HOST = "com.applovin.sdk";
    public static final String URI_SCHEME = "applovin";
    public static final String VERSION = "5.0.0";
    private static AppLovinSdk[] a = new AppLovinSdk[0];
    private static final Object b = new Object();

    public static AppLovinSdk getInstance(Context context) {
        if (context != null) {
            return getInstance(AppLovinSdkUtils.retrieveSdkKey(context), AppLovinSdkUtils.retrieveUserSettings(context), context);
        }
        throw new IllegalArgumentException("No context specified");
    }

    public static AppLovinSdk getInstance(AppLovinSdkSettings appLovinSdkSettings, Context context) {
        if (context != null) {
            return getInstance(AppLovinSdkUtils.retrieveSdkKey(context), appLovinSdkSettings, context);
        }
        throw new IllegalArgumentException("No context specified");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r2 = com.applovin.sdk.bootstrap.SdkBootstrap.getInstance(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r0 = (com.applovin.sdk.AppLovinSdk) r2.loadImplementation(com.applovin.sdk.AppLovinSdk.class);
        r0.initialize(r8, r9, r10.getApplicationContext());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0086, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0087, code lost:
        android.util.Log.e(com.applovin.sdk.Logger.SDK_TAG, "[Bootstrap] Unable to load SDK implementation", r0);
        r7 = r0.getMessage();
        r0 = null;
        r1 = r7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.applovin.sdk.AppLovinSdk getInstance(java.lang.String r8, com.applovin.sdk.AppLovinSdkSettings r9, android.content.Context r10) {
        /*
            r1 = 0
            r0 = 0
            java.lang.Object r3 = com.applovin.sdk.AppLovinSdk.b
            monitor-enter(r3)
            com.applovin.sdk.AppLovinSdk[] r2 = com.applovin.sdk.AppLovinSdk.a     // Catch:{ all -> 0x0035 }
            int r2 = r2.length     // Catch:{ all -> 0x0035 }
            r4 = 1
            if (r2 != r4) goto L_0x0021
            com.applovin.sdk.AppLovinSdk[] r2 = com.applovin.sdk.AppLovinSdk.a     // Catch:{ all -> 0x0035 }
            r4 = 0
            r2 = r2[r4]     // Catch:{ all -> 0x0035 }
            java.lang.String r2 = r2.getSdkKey()     // Catch:{ all -> 0x0035 }
            boolean r2 = r2.equals(r8)     // Catch:{ all -> 0x0035 }
            if (r2 == 0) goto L_0x0021
            com.applovin.sdk.AppLovinSdk[] r0 = com.applovin.sdk.AppLovinSdk.a     // Catch:{ all -> 0x0035 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x0035 }
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
        L_0x0020:
            return r0
        L_0x0021:
            com.applovin.sdk.AppLovinSdk[] r4 = com.applovin.sdk.AppLovinSdk.a     // Catch:{ all -> 0x0035 }
            int r5 = r4.length     // Catch:{ all -> 0x0035 }
            r2 = r0
        L_0x0025:
            if (r2 >= r5) goto L_0x003c
            r0 = r4[r2]     // Catch:{ all -> 0x0035 }
            java.lang.String r6 = r0.getSdkKey()     // Catch:{ all -> 0x0035 }
            boolean r6 = r6.equals(r8)     // Catch:{ all -> 0x0035 }
            if (r6 == 0) goto L_0x0038
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
            goto L_0x0020
        L_0x0035:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
            throw r0
        L_0x0038:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0025
        L_0x003c:
            com.applovin.sdk.bootstrap.SdkBootstrap r2 = com.applovin.sdk.bootstrap.SdkBootstrap.getInstance(r10)     // Catch:{ all -> 0x0035 }
            java.lang.Class<com.applovin.sdk.AppLovinSdk> r0 = com.applovin.sdk.AppLovinSdk.class
            java.lang.Object r0 = r2.loadImplementation(r0)     // Catch:{ Throwable -> 0x0086 }
            com.applovin.sdk.AppLovinSdk r0 = (com.applovin.sdk.AppLovinSdk) r0     // Catch:{ Throwable -> 0x0086 }
            android.content.Context r4 = r10.getApplicationContext()     // Catch:{ Throwable -> 0x0086 }
            r0.initialize(r8, r9, r4)     // Catch:{ Throwable -> 0x0086 }
        L_0x004f:
            if (r0 != 0) goto L_0x006c
            java.lang.String r0 = "AppLovinSdk"
            java.lang.String r4 = "[Bootstrap] Re-trying SDK initialization without the boostrap..."
            android.util.Log.w(r0, r4)     // Catch:{ all -> 0x0035 }
            r2.disable(r1)     // Catch:{ all -> 0x0035 }
            java.lang.Class<com.applovin.sdk.AppLovinSdk> r0 = com.applovin.sdk.AppLovinSdk.class
            java.lang.Object r0 = r2.loadImplementation(r0)     // Catch:{ all -> 0x0035 }
            com.applovin.sdk.AppLovinSdk r0 = (com.applovin.sdk.AppLovinSdk) r0     // Catch:{ all -> 0x0035 }
            if (r0 == 0) goto L_0x0096
            android.content.Context r1 = r10.getApplicationContext()     // Catch:{ all -> 0x0035 }
            r0.initialize(r8, r9, r1)     // Catch:{ all -> 0x0035 }
        L_0x006c:
            com.applovin.sdk.AppLovinSdk[] r1 = com.applovin.sdk.AppLovinSdk.a     // Catch:{ all -> 0x0035 }
            int r1 = r1.length     // Catch:{ all -> 0x0035 }
            int r1 = r1 + 1
            com.applovin.sdk.AppLovinSdk[] r1 = new com.applovin.sdk.AppLovinSdk[r1]     // Catch:{ all -> 0x0035 }
            com.applovin.sdk.AppLovinSdk[] r2 = com.applovin.sdk.AppLovinSdk.a     // Catch:{ all -> 0x0035 }
            r4 = 0
            r5 = 0
            com.applovin.sdk.AppLovinSdk[] r6 = com.applovin.sdk.AppLovinSdk.a     // Catch:{ all -> 0x0035 }
            int r6 = r6.length     // Catch:{ all -> 0x0035 }
            java.lang.System.arraycopy(r2, r4, r1, r5, r6)     // Catch:{ all -> 0x0035 }
            com.applovin.sdk.AppLovinSdk[] r2 = com.applovin.sdk.AppLovinSdk.a     // Catch:{ all -> 0x0035 }
            int r2 = r2.length     // Catch:{ all -> 0x0035 }
            r1[r2] = r0     // Catch:{ all -> 0x0035 }
            com.applovin.sdk.AppLovinSdk.a = r1     // Catch:{ all -> 0x0035 }
            monitor-exit(r3)     // Catch:{ all -> 0x0035 }
            goto L_0x0020
        L_0x0086:
            r0 = move-exception
            java.lang.String r4 = "AppLovinSdk"
            java.lang.String r5 = "[Bootstrap] Unable to load SDK implementation"
            android.util.Log.e(r4, r5, r0)     // Catch:{ all -> 0x0035 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0035 }
            r7 = r0
            r0 = r1
            r1 = r7
            goto L_0x004f
        L_0x0096:
            java.lang.String r0 = "AppLovinSdk"
            java.lang.String r1 = "Failed to load AppLovin SDK. Try cleaning application data and starting the applion again."
            android.util.Log.e(r0, r1)     // Catch:{ all -> 0x0035 }
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x0035 }
            java.lang.String r1 = "Unable to load AppLovin SDK"
            r0.<init>(r1)     // Catch:{ all -> 0x0035 }
            throw r0     // Catch:{ all -> 0x0035 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.sdk.AppLovinSdk.getInstance(java.lang.String, com.applovin.sdk.AppLovinSdkSettings, android.content.Context):com.applovin.sdk.AppLovinSdk");
    }

    public static void initializeSdk(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("No context specified");
        }
        AppLovinSdk instance = getInstance(context);
        if (instance != null) {
            instance.initializeSdk();
        } else {
            Log.e(Logger.SDK_TAG, "Unable to initialize AppLovin SDK: SDK object not created");
        }
    }

    public abstract AppLovinAdService getAdService();

    public abstract Context getApplicationContext();

    public abstract Logger getLogger();

    public abstract String getSdkKey();

    public abstract AppLovinSdkSettings getSettings();

    public abstract AppLovinTargetingData getTargetingData();

    public abstract boolean hasCriticalErrors();

    /* access modifiers changed from: protected */
    public abstract void initialize(String str, AppLovinSdkSettings appLovinSdkSettings, Context context);

    public abstract void initializeSdk();

    public abstract boolean isEnabled();

    public abstract void setPluginVersion(String str);
}
