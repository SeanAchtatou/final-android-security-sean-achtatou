package com.applovin.sdk.bootstrap;

import android.util.Log;
import com.applovin.sdk.Logger;
import dalvik.system.DexClassLoader;
import java.io.File;

public class SdkClassLoader extends DexClassLoader {
    public SdkClassLoader(File file, File file2, ClassLoader classLoader) {
        super(file.getAbsolutePath(), file2.getAbsolutePath(), null, classLoader);
    }

    private boolean a(String str) {
        return str.startsWith("com.applovin.impl.");
    }

    /* access modifiers changed from: protected */
    public Class loadClass(String str, boolean z) {
        if (a(str)) {
            Log.d(Logger.SDK_TAG, "Loading SDK implementation class: " + str);
            try {
                return findClass(str);
            } catch (Exception e) {
                Log.w(Logger.SDK_TAG, "Unable to load SDK implementation class: " + str, e);
            }
        }
        return getParent().loadClass(str);
    }
}
