package com.applovin.sdk.bootstrap;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.Logger;
import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

public class SdkBootstrap {
    private static final SdkBootstrap a = new SdkBootstrap();
    private AtomicBoolean b = new AtomicBoolean();
    private boolean c = true;
    private Context d;
    private ClassLoader e;

    private void a(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("No context specified");
        } else if (this.b.compareAndSet(false, true)) {
            this.d = context;
            if (AppLovinSdkUtils.isAutoUpdateEnabled(context)) {
                b(context);
            } else {
                this.e = SdkBootstrap.class.getClassLoader();
            }
        }
    }

    private void a(String str) {
        a(str, null);
    }

    private void a(String str, Throwable th) {
        if (this.c) {
            Log.i(Logger.SDK_TAG, "[Boostrap] " + str, th);
        }
    }

    private void b(Context context) {
        this.c = AppLovinSdkUtils.isVerboseLoggingEnabled(context);
        SharedPreferences sharedPreferences = context.getSharedPreferences(SdkBoostrapTasks.SHARED_PREFERENCES_KEY, 0);
        String string = sharedPreferences.getString(SdkBoostrapTasks.SDK_VERSION_FILE, "");
        String string2 = sharedPreferences.getString(SdkBoostrapTasks.SDK_INTERFACE_VERSION, "");
        if (string.length() <= 0 || !"5.0.0".equals(string2)) {
            disable();
        } else {
            File file = new File(context.getDir(SdkBoostrapTasks.SDK_FOLDER, 0), string);
            if (!file.exists() || file.length() <= 0) {
                a("SDK implementation file " + string + " has no content, using default implementation");
                disable();
            } else {
                this.e = new SdkClassLoader(file, context.getDir("al_outdex", 0), SdkBootstrap.class.getClassLoader());
            }
        }
        checkForUpdates();
    }

    public static SdkBootstrap getInstance(Context context) {
        a.a(context);
        return a;
    }

    public void checkForUpdates() {
        SdkBoostrapTasks sdkBoostrapTasks;
        if (AppLovinSdkUtils.isAutoUpdateEnabled(this.d) && (sdkBoostrapTasks = (SdkBoostrapTasks) loadImplementation(SdkBoostrapTasks.class)) != null) {
            sdkBoostrapTasks.startUpdateDownload(this.d.getApplicationContext());
        }
    }

    public void disable() {
        disable(null);
    }

    public void disable(String str) {
        this.e = SdkBootstrap.class.getClassLoader();
        try {
            SharedPreferences sharedPreferences = this.d.getSharedPreferences(SdkBoostrapTasks.SHARED_PREFERENCES_KEY, 0);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            if (str != null && str.length() > 0) {
                String string = sharedPreferences.getString(SdkBoostrapTasks.SDK_VERSION_FILE, "");
                String string2 = sharedPreferences.getString(SdkBoostrapTasks.SDK_INTERFACE_VERSION, "");
                String string3 = sharedPreferences.getString(SdkBoostrapTasks.SERVER_EVENT_ID, "");
                edit.putString(SdkBoostrapTasks.LAST_BOOSTRAP_ERROR, string + "/" + string2 + "/" + string3 + "/" + sharedPreferences.getString(SdkBoostrapTasks.SERVER_CHECKSUM, "") + ": " + str);
            }
            edit.putString(SdkBoostrapTasks.SDK_VERSION_FILE, "");
            edit.putString(SdkBoostrapTasks.SDK_INTERFACE_VERSION, "");
            edit.commit();
        } catch (Throwable th) {
            a("Unable to disable SDK auto updated because of " + str, th);
        }
    }

    public ClassLoader getClassLoader() {
        return this.e;
    }

    public Object loadImplementation(Class cls) {
        if (cls == null) {
            throw new IllegalArgumentException("No master interface specified");
        }
        try {
            String simpleName = cls.getSimpleName();
            String name = cls.getPackage().getName();
            String str = "com.applovin.impl." + name.substring(name.lastIndexOf(46) + 1) + "." + simpleName + "Impl";
            a("Loading " + str + "...");
            return cls.cast(this.e.loadClass(str).newInstance());
        } catch (Throwable th) {
            a("Can't load " + cls.getSimpleName(), th);
            return null;
        }
    }
}
