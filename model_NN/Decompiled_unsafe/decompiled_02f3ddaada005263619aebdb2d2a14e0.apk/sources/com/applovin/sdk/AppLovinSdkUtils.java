package com.applovin.sdk;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.util.Log;

public class AppLovinSdkUtils {
    public static long getAdRefreshSeconds(Context context) {
        Bundle retrieveMetadata = retrieveMetadata(context);
        if (retrieveMetadata != null) {
            return (long) retrieveMetadata.getInt("applovin.sdk.ad_refresh_seconds", -100);
        }
        return -100;
    }

    public static String getAutoPreloadSizes(Context context) {
        String string;
        Bundle retrieveMetadata = retrieveMetadata(context);
        return (retrieveMetadata == null || (string = retrieveMetadata.getString("applovin.sdk.auto_preload_ad_sizes")) == null) ? "BANNER,INTER" : string;
    }

    public static boolean isAutoUpdateEnabled(Context context) {
        Bundle retrieveMetadata = retrieveMetadata(context);
        return retrieveMetadata == null || !retrieveMetadata.getBoolean("applovin.sdk.autoupdate_disabled", false);
    }

    public static boolean isVerboseLoggingEnabled(Context context) {
        Bundle retrieveMetadata = retrieveMetadata(context);
        if (retrieveMetadata != null) {
            return retrieveMetadata.getBoolean("applovin.sdk.verbose_logging", false);
        }
        return false;
    }

    protected static Bundle retrieveMetadata(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER).metaData;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(Logger.SDK_TAG, "Unable to retrieve application metadata", e);
            return null;
        }
    }

    public static String retrieveSdkKey(Context context) {
        Bundle retrieveMetadata = retrieveMetadata(context);
        if (retrieveMetadata == null) {
            return null;
        }
        String string = retrieveMetadata.getString("applovin.sdk.key");
        return string != null ? string : "";
    }

    public static AppLovinSdkSettings retrieveUserSettings(Context context) {
        AppLovinSdkSettings appLovinSdkSettings = new AppLovinSdkSettings();
        appLovinSdkSettings.setVerboseLogging(isVerboseLoggingEnabled(context));
        appLovinSdkSettings.setBannerAdRefreshSeconds(getAdRefreshSeconds(context));
        appLovinSdkSettings.setAutoPreloadSizes(getAutoPreloadSizes(context));
        return appLovinSdkSettings;
    }
}
