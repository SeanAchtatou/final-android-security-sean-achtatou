package com.applovin.sdk;

public interface AppLovinAdService {
    public static final String URI_AD_SERVICE = "/adservice";
    public static final String URI_API_SERVICE = "/api";
    public static final String URI_CLOSE_AD = "/adservice/close_ad";
    public static final String URI_LANDING_PAGE_AD = "/adservice/landing_page";
    public static final String URI_LAUNCH_APP = "/launch";
    public static final String URI_NEXT_AD = "/adservice/next_ad";
    public static final String URI_TRACK_CLICK = "/adservice/track_click";

    void addAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener);

    void addAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener, AppLovinAdSize appLovinAdSize);

    void loadNextAd(AppLovinAdSize appLovinAdSize, AppLovinAdLoadListener appLovinAdLoadListener);

    void loadNextAd(AppLovinAdSize appLovinAdSize, String str, AppLovinAdLoadListener appLovinAdLoadListener);

    void loadNextAd(String str, AppLovinAdLoadListener appLovinAdLoadListener);

    void removeAdUpdateListener(AppLovinAdUpdateListener appLovinAdUpdateListener, AppLovinAdSize appLovinAdSize);

    void trackAdClick(AppLovinAd appLovinAd);
}
