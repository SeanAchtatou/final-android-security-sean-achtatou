package com.applovin.sdk;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class AppLovinService extends Service {

    public class LocalBinder extends Binder {
    }

    public IBinder onBind(Intent intent) {
        return new LocalBinder();
    }

    public void onCreate() {
        super.onCreate();
        AppLovinSdk.getInstance(getApplicationContext());
    }
}
