package com.applovin.sdk;

public class AppLovinAd {
    private final String a;
    private final AppLovinAdSize b;
    private final String c;
    private final String d;

    public AppLovinAd(String str, AppLovinAdSize appLovinAdSize, String str2, String str3) {
        if (str == null) {
            throw new IllegalArgumentException("No html specified");
        } else if (appLovinAdSize == null) {
            throw new IllegalArgumentException("No size specified");
        } else {
            this.a = str;
            this.b = appLovinAdSize;
            this.c = str2;
            this.d = str3;
        }
    }

    public String getClickTrackerUrl() {
        return this.d;
    }

    public String getDestinationUrl() {
        return this.c;
    }

    public String getHtml() {
        return this.a;
    }

    public AppLovinAdSize getSize() {
        return this.b;
    }
}
