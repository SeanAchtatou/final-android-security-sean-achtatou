package com.tapjoy;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import java.io.ByteArrayInputStream;
import java.util.Timer;
import java.util.TimerTask;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

public class TapjoyDisplayAd {
    private static final byte[] DECODE_TABLE;
    private static final int MASK_8BITS = 255;
    private static final byte PAD = 61;
    /* access modifiers changed from: private */
    public static String adClickURL;
    private static Bitmap bitmapImage;
    /* access modifiers changed from: private */
    public static TapjoyDisplayAdNotifier displayAdNotifier;
    private static String displayAdSize;
    public static String displayAdURLParams;
    /* access modifiers changed from: private */
    public static TapjoyURLConnection tapjoyURLConnection = null;
    final String TAPJOY_DISPLAY_AD = "Banner Ad";
    View adView;
    private boolean autoRefresh;
    private byte[] buffer;
    /* access modifiers changed from: private */
    public Context context;
    long elapsed_time;
    private boolean eof;
    private int modulus;
    private int pos;
    Timer resumeTimer;
    Timer timer;
    private int x;

    static {
        byte[] bArr = new byte[123];
        bArr[0] = -1;
        bArr[1] = -1;
        bArr[2] = -1;
        bArr[3] = -1;
        bArr[4] = -1;
        bArr[5] = -1;
        bArr[6] = -1;
        bArr[7] = -1;
        bArr[8] = -1;
        bArr[9] = -1;
        bArr[10] = -1;
        bArr[11] = -1;
        bArr[12] = -1;
        bArr[13] = -1;
        bArr[14] = -1;
        bArr[15] = -1;
        bArr[16] = -1;
        bArr[17] = -1;
        bArr[18] = -1;
        bArr[19] = -1;
        bArr[20] = -1;
        bArr[21] = -1;
        bArr[22] = -1;
        bArr[23] = -1;
        bArr[24] = -1;
        bArr[25] = -1;
        bArr[26] = -1;
        bArr[27] = -1;
        bArr[28] = -1;
        bArr[29] = -1;
        bArr[30] = -1;
        bArr[31] = -1;
        bArr[32] = -1;
        bArr[33] = -1;
        bArr[34] = -1;
        bArr[35] = -1;
        bArr[36] = -1;
        bArr[37] = -1;
        bArr[38] = -1;
        bArr[39] = -1;
        bArr[40] = -1;
        bArr[41] = -1;
        bArr[42] = -1;
        bArr[43] = 62;
        bArr[44] = -1;
        bArr[45] = 62;
        bArr[46] = -1;
        bArr[47] = 63;
        bArr[48] = 52;
        bArr[49] = 53;
        bArr[50] = 54;
        bArr[51] = 55;
        bArr[52] = 56;
        bArr[53] = 57;
        bArr[54] = 58;
        bArr[55] = 59;
        bArr[56] = 60;
        bArr[57] = PAD;
        bArr[58] = -1;
        bArr[59] = -1;
        bArr[60] = -1;
        bArr[61] = -1;
        bArr[62] = -1;
        bArr[63] = -1;
        bArr[64] = -1;
        bArr[66] = 1;
        bArr[67] = 2;
        bArr[68] = 3;
        bArr[69] = 4;
        bArr[70] = 5;
        bArr[71] = 6;
        bArr[72] = 7;
        bArr[73] = 8;
        bArr[74] = 9;
        bArr[75] = 10;
        bArr[76] = 11;
        bArr[77] = 12;
        bArr[78] = 13;
        bArr[79] = 14;
        bArr[80] = 15;
        bArr[81] = 16;
        bArr[82] = 17;
        bArr[83] = 18;
        bArr[84] = 19;
        bArr[85] = 20;
        bArr[86] = 21;
        bArr[87] = 22;
        bArr[88] = 23;
        bArr[89] = 24;
        bArr[90] = 25;
        bArr[91] = -1;
        bArr[92] = -1;
        bArr[93] = -1;
        bArr[94] = -1;
        bArr[95] = 63;
        bArr[96] = -1;
        bArr[97] = 26;
        bArr[98] = 27;
        bArr[99] = 28;
        bArr[100] = 29;
        bArr[101] = 30;
        bArr[102] = 31;
        bArr[103] = 32;
        bArr[104] = 33;
        bArr[105] = 34;
        bArr[106] = 35;
        bArr[107] = 36;
        bArr[108] = 37;
        bArr[109] = 38;
        bArr[110] = 39;
        bArr[111] = 40;
        bArr[112] = 41;
        bArr[113] = 42;
        bArr[114] = 43;
        bArr[115] = 44;
        bArr[116] = 45;
        bArr[117] = 46;
        bArr[118] = 47;
        bArr[119] = 48;
        bArr[120] = 49;
        bArr[121] = 50;
        bArr[122] = 51;
        DECODE_TABLE = bArr;
    }

    public TapjoyDisplayAd(Context ctx) {
        displayAdSize = TapjoyDisplayAdSize.TJC_AD_BANNERSIZE_320X50;
        this.context = ctx;
        tapjoyURLConnection = new TapjoyURLConnection();
    }

    public void setBannerAdSize(String dimensions) {
        displayAdSize = dimensions;
    }

    public String getBannerAdSize() {
        return displayAdSize;
    }

    public void enableAutoRefresh(boolean shouldAutoRefresh) {
        this.autoRefresh = shouldAutoRefresh;
    }

    public void getDisplayAd(TapjoyDisplayAdNotifier notifier) {
        TapjoyLog.i("Banner Ad", "Get Banner Ad");
        getDisplayAd(null, notifier);
    }

    public void getDisplayAd(String currencyID, TapjoyDisplayAdNotifier notifier) {
        TapjoyLog.i("Banner Ad", "Get Banner Ad, currencyID: " + currencyID);
        displayAdNotifier = notifier;
        displayAdURLParams = TapjoyConnectCore.getURLParams();
        displayAdURLParams = String.valueOf(displayAdURLParams) + "&publisher_user_id=" + TapjoyConnectCore.getUserID();
        displayAdURLParams = String.valueOf(displayAdURLParams) + "&size=" + displayAdSize;
        if (currencyID != null) {
            displayAdURLParams = String.valueOf(displayAdURLParams) + "&currency_id=" + currencyID;
        }
        new Thread(new Runnable() {
            public void run() {
                String response = TapjoyDisplayAd.tapjoyURLConnection.connectToURL("https://ws.tapjoyads.com/display_ad?", TapjoyDisplayAd.displayAdURLParams);
                if (response == null || response.length() == 0) {
                    TapjoyDisplayAd.displayAdNotifier.getDisplayAdResponseFailed("Network error.");
                } else if (!TapjoyDisplayAd.this.buildResponse(response)) {
                    TapjoyDisplayAd.displayAdNotifier.getDisplayAdResponseFailed("No ad to display.");
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public boolean buildResponse(String response) {
        boolean retValue = false;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            Document document = factory.newDocumentBuilder().parse(new ByteArrayInputStream(response.getBytes("UTF-8")));
            adClickURL = TapjoyUtil.getNodeTrimValue(document.getElementsByTagName("ClickURL"));
            String image_data = TapjoyUtil.getNodeTrimValue(document.getElementsByTagName("Image"));
            TapjoyLog.i("Banner Ad", "decoding...");
            decodeBase64(image_data.getBytes(), 0, image_data.getBytes().length);
            TapjoyLog.i("Banner Ad", "pos: " + this.pos);
            TapjoyLog.i("Banner Ad", "buffer_size: " + this.buffer.length);
            bitmapImage = BitmapFactory.decodeByteArray(this.buffer, 0, this.pos);
            TapjoyLog.i("Banner Ad", "image: " + bitmapImage.getWidth() + "x" + bitmapImage.getHeight());
            this.adView = new View(this.context);
            this.adView.setLayoutParams(new ViewGroup.LayoutParams(bitmapImage.getWidth(), bitmapImage.getHeight()));
            this.adView.setBackgroundDrawable(new BitmapDrawable(bitmapImage));
            this.adView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TapjoyLog.i("Banner Ad", "Opening URL in new browser = [" + TapjoyDisplayAd.adClickURL + "]");
                    Intent intent = new Intent(TapjoyDisplayAd.this.context, TJCOffersWebView.class);
                    intent.putExtra(TapjoyConstants.EXTRA_DISPLAY_AD_URL, TapjoyDisplayAd.adClickURL);
                    intent.setFlags(268435456);
                    TapjoyDisplayAd.this.context.startActivity(intent);
                    if (TapjoyDisplayAd.this.resumeTimer != null) {
                        TapjoyDisplayAd.this.resumeTimer.cancel();
                    }
                    TapjoyDisplayAd.this.elapsed_time = 0;
                    TapjoyDisplayAd.this.resumeTimer = new Timer();
                    TapjoyDisplayAd.this.resumeTimer.schedule(new CheckForResumeTimer(TapjoyDisplayAd.this, null), 10000, 10000);
                }
            });
            TapjoyLog.i("Banner Ad", "notify displayAdNotifier");
            displayAdNotifier.getDisplayAdResponse(this.adView);
            retValue = true;
        } catch (Exception e) {
            TapjoyLog.e("Banner Ad", "Error parsing XML: " + e.toString());
        }
        if (this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
        if (this.autoRefresh && this.timer == null) {
            TapjoyLog.i("Banner Ad", "will refresh banner ad in 15s...");
            this.timer = new Timer();
            this.timer.schedule(new RefreshTimer(this, null), 15000);
        }
        TapjoyLog.i("Banner Ad", "return: " + retValue);
        return retValue;
    }

    private class RefreshTimer extends TimerTask {
        private RefreshTimer() {
        }

        /* synthetic */ RefreshTimer(TapjoyDisplayAd tapjoyDisplayAd, RefreshTimer refreshTimer) {
            this();
        }

        public void run() {
            TapjoyLog.i("Banner Ad", "refreshing banner ad...");
            TapjoyDisplayAd.this.getDisplayAd(TapjoyDisplayAd.displayAdNotifier);
            TapjoyDisplayAd.this.timer.cancel();
            TapjoyDisplayAd.this.timer = null;
        }
    }

    private class CheckForResumeTimer extends TimerTask {
        private CheckForResumeTimer() {
        }

        /* synthetic */ CheckForResumeTimer(TapjoyDisplayAd tapjoyDisplayAd, CheckForResumeTimer checkForResumeTimer) {
            this();
        }

        public void run() {
            TapjoyDisplayAd.this.elapsed_time += 10000;
            TapjoyLog.i("Banner Ad", "banner elapsed_time: " + TapjoyDisplayAd.this.elapsed_time + " (" + ((TapjoyDisplayAd.this.elapsed_time / 1000) / 60) + "m " + ((TapjoyDisplayAd.this.elapsed_time / 1000) % 60) + "s)");
            if (TapjoyDisplayAd.this.adView == null) {
                cancel();
                return;
            }
            TapjoyLog.i("Banner Ad", "adView.isShown: " + TapjoyDisplayAd.this.adView.isShown());
            if (TapjoyDisplayAd.this.adView.isShown() && TapjoyConnectCore.getInstance() != null) {
                TapjoyLog.i("Banner Ad", "call connect");
                TapjoyConnectCore.getInstance().callConnect();
                cancel();
            }
            if (TapjoyDisplayAd.this.elapsed_time >= TapjoyConstants.RESUME_TOTAL_TIME) {
                cancel();
            }
        }
    }

    public static Bitmap getBitmapImage() {
        return bitmapImage;
    }

    public static String getLinkURL() {
        return adClickURL;
    }

    /* access modifiers changed from: package-private */
    public void decodeBase64(byte[] in, int inPos, int inAvail) {
        byte b;
        this.buffer = new byte[in.length];
        this.pos = 0;
        this.eof = false;
        this.modulus = 0;
        if (inAvail < 0) {
            this.eof = true;
        }
        int i = 0;
        int inPos2 = inPos;
        while (true) {
            if (i >= inAvail) {
                break;
            }
            int inPos3 = inPos2 + 1;
            byte b2 = in[inPos2];
            if (b2 == 61) {
                this.eof = true;
                break;
            }
            if (b2 >= 0 && b2 < DECODE_TABLE.length && (b = DECODE_TABLE[b2]) >= 0) {
                int i2 = this.modulus + 1;
                this.modulus = i2;
                this.modulus = i2 % 4;
                this.x = (this.x << 6) + b;
                if (this.modulus == 0) {
                    byte[] bArr = this.buffer;
                    int i3 = this.pos;
                    this.pos = i3 + 1;
                    bArr[i3] = (byte) ((this.x >> 16) & 255);
                    byte[] bArr2 = this.buffer;
                    int i4 = this.pos;
                    this.pos = i4 + 1;
                    bArr2[i4] = (byte) ((this.x >> 8) & 255);
                    byte[] bArr3 = this.buffer;
                    int i5 = this.pos;
                    this.pos = i5 + 1;
                    bArr3[i5] = (byte) (this.x & 255);
                }
            }
            i++;
            inPos2 = inPos3;
        }
        if (this.eof && this.modulus != 0) {
            this.x <<= 6;
            switch (this.modulus) {
                case 2:
                    this.x <<= 6;
                    byte[] bArr4 = this.buffer;
                    int i6 = this.pos;
                    this.pos = i6 + 1;
                    bArr4[i6] = (byte) ((this.x >> 16) & 255);
                    return;
                case 3:
                    byte[] bArr5 = this.buffer;
                    int i7 = this.pos;
                    this.pos = i7 + 1;
                    bArr5[i7] = (byte) ((this.x >> 16) & 255);
                    byte[] bArr6 = this.buffer;
                    int i8 = this.pos;
                    this.pos = i8 + 1;
                    bArr6[i8] = (byte) ((this.x >> 8) & 255);
                    return;
                default:
                    return;
            }
        }
    }
}
