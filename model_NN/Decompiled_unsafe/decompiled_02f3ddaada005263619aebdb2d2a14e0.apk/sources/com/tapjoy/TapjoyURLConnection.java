package com.tapjoy;

import android.net.Uri;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

public class TapjoyURLConnection {
    private static final String TAPJOY_URL_CONNECTION = "TapjoyURLConnection";

    public TapjoyHttpURLResponse getResponseFromURL(String url, String params) {
        TapjoyHttpURLResponse tapjoyResponse = new TapjoyHttpURLResponse();
        try {
            String requestURL = (String.valueOf(url) + params).replaceAll(" ", "%20");
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "baseURL: " + url);
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "requestURL: " + requestURL);
            HttpURLConnection connection = (HttpURLConnection) new URL(requestURL).openConnection();
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(30000);
            connection.connect();
            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            try {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    try {
                        String line = rd.readLine();
                        if (line == null) {
                            break;
                        }
                        sb.append(String.valueOf(line) + 10);
                    } catch (Exception e) {
                        e = e;
                    }
                }
                tapjoyResponse.response = sb.toString();
                tapjoyResponse.statusCode = connection.getResponseCode();
                String contentLength = connection.getHeaderField("content-length");
                if (contentLength != null) {
                    try {
                        tapjoyResponse.contentLength = Integer.valueOf(contentLength).intValue();
                    } catch (Exception e2) {
                        TapjoyLog.e(TAPJOY_URL_CONNECTION, "Exception: " + e2.toString());
                    }
                }
                TapjoyLog.i(TAPJOY_URL_CONNECTION, "--------------------");
                TapjoyLog.i(TAPJOY_URL_CONNECTION, "response status: " + tapjoyResponse.statusCode);
                TapjoyLog.i(TAPJOY_URL_CONNECTION, "response size: " + contentLength);
                TapjoyLog.i(TAPJOY_URL_CONNECTION, "response: ");
                TapjoyLog.i(TAPJOY_URL_CONNECTION, tapjoyResponse.response);
                TapjoyLog.i(TAPJOY_URL_CONNECTION, "--------------------");
                StringBuilder sb2 = sb;
                BufferedReader bufferedReader = rd;
            } catch (Exception e3) {
                e = e3;
                TapjoyLog.e(TAPJOY_URL_CONNECTION, "Exception: " + e.toString());
                return tapjoyResponse;
            }
        } catch (Exception e4) {
            e = e4;
            TapjoyLog.e(TAPJOY_URL_CONNECTION, "Exception: " + e.toString());
            return tapjoyResponse;
        }
        return tapjoyResponse;
    }

    public String connectToURL(String url) {
        return connectToURL(url, "");
    }

    public String connectToURL(String url, String params) {
        String httpResponse = null;
        try {
            String requestURL = (String.valueOf(url) + params).replaceAll(" ", "%20");
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "baseURL: " + url);
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "requestURL: " + requestURL);
            HttpURLConnection connection = (HttpURLConnection) new URL(requestURL).openConnection();
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(30000);
            httpResponse = connection.getResponseMessage();
            connection.connect();
            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            try {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    try {
                        String line = rd.readLine();
                        if (line == null) {
                            String httpResponse2 = sb.toString();
                            TapjoyLog.i(TAPJOY_URL_CONNECTION, "--------------------");
                            TapjoyLog.i(TAPJOY_URL_CONNECTION, "response size: " + httpResponse2.length());
                            TapjoyLog.i(TAPJOY_URL_CONNECTION, "response: ");
                            TapjoyLog.i(TAPJOY_URL_CONNECTION, httpResponse2);
                            TapjoyLog.i(TAPJOY_URL_CONNECTION, "--------------------");
                            StringBuilder sb2 = sb;
                            BufferedReader bufferedReader = rd;
                            return httpResponse2;
                        }
                        sb.append(String.valueOf(line) + 10);
                    } catch (Exception e) {
                        e = e;
                        TapjoyLog.e(TAPJOY_URL_CONNECTION, "Exception: " + e.toString());
                        return httpResponse;
                    }
                }
            } catch (Exception e2) {
                e = e2;
                TapjoyLog.e(TAPJOY_URL_CONNECTION, "Exception: " + e.toString());
                return httpResponse;
            }
        } catch (Exception e3) {
            e = e3;
            TapjoyLog.e(TAPJOY_URL_CONNECTION, "Exception: " + e.toString());
            return httpResponse;
        }
    }

    public String getContentLength(String url) {
        String contentLength = null;
        try {
            String requestURL = url.replaceAll(" ", "%20");
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "requestURL: " + requestURL);
            HttpURLConnection connection = (HttpURLConnection) new URL(requestURL).openConnection();
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(30000);
            contentLength = connection.getHeaderField("content-length");
        } catch (Exception e) {
            TapjoyLog.e(TAPJOY_URL_CONNECTION, "Exception: " + e.toString());
        }
        TapjoyLog.i(TAPJOY_URL_CONNECTION, "content-length: " + contentLength);
        return contentLength;
    }

    public String connectToURLwithPOST(String url, Hashtable<String, String> params, Hashtable<String, String> paramsData) {
        try {
            String requestURL = url.replaceAll(" ", "%20");
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "baseURL: " + url);
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "requestURL: " + requestURL);
            HttpPost httpPost = new HttpPost(requestURL);
            List<NameValuePair> pairs = new ArrayList<>();
            for (Map.Entry<String, String> item : params.entrySet()) {
                pairs.add(new BasicNameValuePair((String) item.getKey(), (String) item.getValue()));
                TapjoyLog.i(TAPJOY_URL_CONNECTION, "key: " + ((String) item.getKey()) + ", value: " + Uri.encode((String) item.getValue()));
            }
            if (paramsData != null && paramsData.size() > 0) {
                for (Map.Entry<String, String> item2 : paramsData.entrySet()) {
                    pairs.add(new BasicNameValuePair("data[" + ((String) item2.getKey()) + "]", (String) item2.getValue()));
                    TapjoyLog.i(TAPJOY_URL_CONNECTION, "key: " + ((String) item2.getKey()) + ", value: " + Uri.encode((String) item2.getValue()));
                }
            }
            httpPost.setEntity(new UrlEncodedFormEntity(pairs));
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "HTTP POST: " + httpPost.toString());
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
            HttpConnectionParams.setSoTimeout(httpParameters, 30000);
            HttpResponse response = new DefaultHttpClient(httpParameters).execute(httpPost);
            String httpResponse = EntityUtils.toString(response.getEntity());
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "--------------------");
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "response status: " + response.getStatusLine().getStatusCode());
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "response size: " + httpResponse.length());
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "response: ");
            TapjoyLog.i(TAPJOY_URL_CONNECTION, httpResponse);
            TapjoyLog.i(TAPJOY_URL_CONNECTION, "--------------------");
            return httpResponse;
        } catch (Exception e) {
            TapjoyLog.e(TAPJOY_URL_CONNECTION, "Exception: " + e.toString());
            return null;
        }
    }
}
