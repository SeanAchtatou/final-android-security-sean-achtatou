package com.tapjoy;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TapjoyVideo {
    private static final String BACKGROUND_WATERMARK_NAME = "watermark";
    public static final String TAPJOY_VIDEO = "TapjoyVideo";
    public static String imageBackgroundLocation = null;
    public static String imagePlaceholderLocation = null;
    public static String imageTapjoyLocation = null;
    private static final String imageTapjoyUrl = "https://s3.amazonaws.com/tapjoy/videos/assets/watermark.png";
    private static TapjoyVideo tapjoyVideo = null;
    /* access modifiers changed from: private */
    public static TapjoyVideoNotifier tapjoyVideoNotifier;
    public static Bitmap watermark;
    /* access modifiers changed from: private */
    public Hashtable<String, TapjoyVideoObject> cachedVideos;
    Context context;
    /* access modifiers changed from: private */
    public boolean enableVideoCache = true;
    /* access modifiers changed from: private */
    public String imageCacheDir = null;
    /* access modifiers changed from: private */
    public Vector<String> imageQueue;
    /* access modifiers changed from: private */
    public Hashtable<String, String> uncachedImages;
    /* access modifiers changed from: private */
    public Hashtable<String, TapjoyVideoObject> uncachedVideos;
    /* access modifiers changed from: private */
    public String videoCacheDir = null;
    private int videoCacheLimit = 5;
    /* access modifiers changed from: private */
    public Vector<String> videoQueue;
    public TapjoyVideoObject videoToPlay;

    public TapjoyVideo(Context applicationContext) {
        this.context = applicationContext;
        tapjoyVideo = this;
    }

    public static TapjoyVideo getInstance() {
        return tapjoyVideo;
    }

    public void setVideoCacheCount(int count) {
        this.videoCacheLimit = count;
    }

    public void enableVideoCache(boolean enable) {
        this.enableVideoCache = enable;
        TapjoyConnectCore.enableVideoCache(enable);
    }

    public void initVideoAd(TapjoyVideoNotifier notifier) {
        TapjoyLog.i(TAPJOY_VIDEO, "initVideoAd");
        if (notifier == null) {
            Log.e(TAPJOY_VIDEO, "Error during initVideoAd -- TapjoyVideoNotifier is null");
            return;
        }
        tapjoyVideoNotifier = notifier;
        this.videoCacheDir = String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/tjcache/data/";
        this.imageCacheDir = String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/tjcache/tmp/";
        TapjoyUtil.deleteFileOrDirectory(new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/tapjoy/"));
        this.videoQueue = new Vector<>();
        this.uncachedVideos = new Hashtable<>();
        this.cachedVideos = new Hashtable<>();
        this.imageQueue = new Vector<>();
        this.imageQueue.addElement(imageTapjoyUrl);
        this.uncachedImages = new Hashtable<>();
        this.uncachedImages.put(BACKGROUND_WATERMARK_NAME, imageTapjoyUrl);
        imageTapjoyLocation = String.valueOf(this.imageCacheDir) + BACKGROUND_WATERMARK_NAME;
        setVideoIDs();
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            TapjoyLog.i(TAPJOY_VIDEO, "Media storage unavailable.");
            tapjoyVideoNotifier.videoError(1);
            return;
        }
        new Thread(new Runnable() {
            public void run() {
                boolean returnValue = false;
                String result = new TapjoyURLConnection().connectToURL("https://ws.tapjoyads.com/videos?", String.valueOf(TapjoyConnectCore.getURLParams()) + "&publisher_user_id=" + TapjoyConnectCore.getUserID());
                if (result != null && result.length() > 0) {
                    returnValue = TapjoyVideo.this.handleGetVideosResponse(result);
                }
                if (!TapjoyVideo.this.enableVideoCache) {
                    if (returnValue) {
                        if (TapjoyVideo.imageTapjoyUrl != 0 && TapjoyVideo.imageTapjoyUrl.length() > 0) {
                            try {
                                URL fileURL = new URL(TapjoyVideo.imageTapjoyUrl);
                                URLConnection connection = fileURL.openConnection();
                                connection.setConnectTimeout(15000);
                                connection.setReadTimeout(25000);
                                connection.connect();
                                TapjoyVideo.watermark = BitmapFactory.decodeStream(fileURL.openConnection().getInputStream());
                            } catch (Exception e) {
                                TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "e: " + e.toString());
                            }
                        }
                        TapjoyVideo.tapjoyVideoNotifier.videoReady();
                        return;
                    }
                    TapjoyVideo.tapjoyVideoNotifier.videoError(2);
                } else if (returnValue) {
                    File[] cachedFilesOnDisk = new File(TapjoyVideo.this.videoCacheDir).listFiles();
                    if (cachedFilesOnDisk != null) {
                        for (int i = 0; i < cachedFilesOnDisk.length; i++) {
                            String key = cachedFilesOnDisk[i].getName();
                            TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "file[" + i + "]: " + cachedFilesOnDisk[i].getAbsolutePath() + " --- " + cachedFilesOnDisk[i].getName());
                            TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "key: " + key + ", " + key);
                            TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "uncachedVideos: " + TapjoyVideo.this.uncachedVideos.keySet().toString());
                            if (TapjoyVideo.this.uncachedVideos.containsKey(key)) {
                                TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "*** Match found, comparing file sizes... ***");
                                TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "local file size: " + cachedFilesOnDisk[i].length());
                                TapjoyVideoObject videoObject = (TapjoyVideoObject) TapjoyVideo.this.uncachedVideos.get(key);
                                String contentLength = new TapjoyURLConnection().getContentLength(videoObject.videoURL);
                                if (contentLength == null || ((long) Integer.parseInt(contentLength)) != cachedFilesOnDisk[i].length()) {
                                    TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "file size mismatch --- deleting video: " + cachedFilesOnDisk[i].getAbsolutePath());
                                    TapjoyUtil.deleteFileOrDirectory(cachedFilesOnDisk[i]);
                                } else {
                                    TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "*** VIDEO ALREADY DOWNLOAD!!! " + key + " ***");
                                    videoObject.dataLocation = cachedFilesOnDisk[i].getAbsolutePath();
                                    TapjoyVideo.this.cachedVideos.put(key, (TapjoyVideoObject) TapjoyVideo.this.uncachedVideos.get(key));
                                    TapjoyVideo.this.uncachedVideos.remove(key);
                                    TapjoyVideo.this.videoQueue.remove(key);
                                    TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "setting video location to: " + videoObject.dataLocation);
                                }
                            } else {
                                TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "VIDEO EXPIRED? removing video from cache: " + key + " --- " + cachedFilesOnDisk[i].getAbsolutePath());
                                TapjoyUtil.deleteFileOrDirectory(cachedFilesOnDisk[i]);
                            }
                        }
                    }
                    TapjoyVideo.this.setVideoIDs();
                    if (TapjoyVideo.this.cachedVideos.size() > 0) {
                        TapjoyVideo.tapjoyVideoNotifier.videoReady();
                    }
                    TapjoyVideo.this.loadNextVideo();
                    File[] cachedImagesOnDisk = new File(TapjoyVideo.this.imageCacheDir).listFiles();
                    TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "------------------------------");
                    TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "checking image cache...");
                    if (cachedImagesOnDisk != null) {
                        for (int i2 = 0; i2 < cachedImagesOnDisk.length; i2++) {
                            String key2 = cachedImagesOnDisk[i2].getName();
                            TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "file[" + i2 + "]: " + cachedImagesOnDisk[i2].getAbsolutePath() + " --- " + cachedImagesOnDisk[i2].getName());
                            TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "key: " + key2);
                            TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "uncachedVideos: " + TapjoyVideo.this.uncachedVideos.keySet().toString());
                            if (TapjoyVideo.this.uncachedImages.containsKey(key2)) {
                                TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "*** Match found, comparing file sizes... ***");
                                TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "local file size: " + cachedImagesOnDisk[i2].length());
                                String contentLength2 = new TapjoyURLConnection().getContentLength((String) TapjoyVideo.this.uncachedImages.get(key2));
                                if (contentLength2 == null || ((long) Integer.parseInt(contentLength2)) != cachedImagesOnDisk[i2].length()) {
                                    TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "file size mismatch --- deleting image");
                                    TapjoyUtil.deleteFileOrDirectory(cachedImagesOnDisk[i2]);
                                } else {
                                    TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "*** image already downloaded, removing from queue: " + ((String) TapjoyVideo.this.uncachedImages.get(key2)));
                                    TapjoyVideo.this.imageQueue.remove(TapjoyVideo.this.uncachedImages.get(key2));
                                }
                            } else {
                                TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "invalid image");
                                TapjoyUtil.deleteFileOrDirectory(cachedImagesOnDisk[i2]);
                            }
                        }
                    }
                    if (TapjoyVideo.this.uncachedVideos.size() == 0 && TapjoyVideo.this.cachedVideos.size() == 0) {
                        TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "*** no videos returned ***");
                        TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "*** delete cache directory ***");
                        TapjoyUtil.deleteFileOrDirectory(new File(String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/tjcache/"));
                    } else if (TapjoyVideo.this.imageQueue.size() > 0) {
                        TapjoyVideo.this.cacheImage((String) TapjoyVideo.this.imageQueue.elementAt(0));
                    }
                    TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "------------------------------");
                    TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "------------------------------");
                    TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "INIT DONE!");
                    TapjoyLog.i(TapjoyVideo.TAPJOY_VIDEO, "------------------------------");
                }
            }
        }).start();
        TapjoyConnectCore.setVideoEnabled(true);
    }

    /* access modifiers changed from: private */
    public boolean handleGetVideosResponse(String response) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        TapjoyLog.i(TAPJOY_VIDEO, "========================================");
        try {
            Document document = factory.newDocumentBuilder().parse(new ByteArrayInputStream(response.getBytes("UTF-8")));
            document.getDocumentElement().normalize();
            NodeList nodelistParent = document.getElementsByTagName("TapjoyVideos");
            NodeList nodelist = nodelistParent.item(0).getChildNodes();
            TapjoyLog.i(TAPJOY_VIDEO, "nodelistParent length: " + nodelistParent.getLength());
            TapjoyLog.i(TAPJOY_VIDEO, "nodelist length: " + nodelist.getLength());
            for (int i = 0; i < nodelist.getLength(); i++) {
                Node node = nodelist.item(i);
                TapjoyVideoObject videoObject = new TapjoyVideoObject();
                if (node != null && node.getNodeType() == 1) {
                    Element element = (Element) node;
                    String value = TapjoyUtil.getNodeTrimValue(element.getElementsByTagName("ClickURL"));
                    if (value != null && !value.equals("")) {
                        videoObject.clickURL = value;
                    }
                    String value2 = TapjoyUtil.getNodeTrimValue(element.getElementsByTagName("OfferID"));
                    if (value2 != null && !value2.equals("")) {
                        videoObject.offerID = value2;
                    }
                    String value3 = TapjoyUtil.getNodeTrimValue(element.getElementsByTagName("Name"));
                    if (value3 != null && !value3.equals("")) {
                        videoObject.videoAdName = value3;
                    }
                    String value4 = TapjoyUtil.getNodeTrimValue(element.getElementsByTagName("Amount"));
                    if (value4 != null && !value4.equals("")) {
                        videoObject.currencyAmount = value4;
                    }
                    String value5 = TapjoyUtil.getNodeTrimValue(element.getElementsByTagName("CurrencyName"));
                    if (value5 != null && !value5.equals("")) {
                        videoObject.currencyName = value5;
                    }
                    String value6 = TapjoyUtil.getNodeTrimValue(element.getElementsByTagName("VideoURL"));
                    if (value6 != null && !value6.equals("")) {
                        videoObject.videoURL = value6;
                    }
                    String value7 = TapjoyUtil.getNodeTrimValue(element.getElementsByTagName("IconURL"));
                    if (value7 != null && !value7.equals("")) {
                        videoObject.iconURL = value7;
                    }
                    TapjoyLog.i(TAPJOY_VIDEO, "-----");
                    TapjoyLog.i(TAPJOY_VIDEO, "videoObject.clickURL: " + videoObject.clickURL);
                    TapjoyLog.i(TAPJOY_VIDEO, "videoObject.offerID: " + videoObject.offerID);
                    TapjoyLog.i(TAPJOY_VIDEO, "videoObject.videoAdName: " + videoObject.videoAdName);
                    TapjoyLog.i(TAPJOY_VIDEO, "videoObject.currencyAmount: " + videoObject.currencyAmount);
                    TapjoyLog.i(TAPJOY_VIDEO, "videoObject.currencyName: " + videoObject.currencyName);
                    TapjoyLog.i(TAPJOY_VIDEO, "videoObject.videoURL: " + videoObject.videoURL);
                    TapjoyLog.i(TAPJOY_VIDEO, "videoObject.iconURL: " + videoObject.iconURL);
                    NodeList itemNodeList = element.getElementsByTagName("Buttons").item(0).getChildNodes();
                    TapjoyLog.i(TAPJOY_VIDEO, "buttons node length: " + itemNodeList.getLength());
                    for (int j = 0; j < itemNodeList.getLength(); j++) {
                        NodeList child = itemNodeList.item(j).getChildNodes();
                        TapjoyLog.i(TAPJOY_VIDEO, "button length: " + child.getLength());
                        if (child.getLength() != 0) {
                            String name = "";
                            String url = "";
                            for (int k = 0; k < child.getLength(); k++) {
                                if (((Element) child.item(k)) != null) {
                                    String tagName = ((Element) child.item(k)).getTagName();
                                    if (tagName.equals("Name") && child.item(k).getFirstChild() != null) {
                                        name = child.item(k).getFirstChild().getNodeValue();
                                    } else if (tagName.equals("URL") && child.item(k).getFirstChild() != null) {
                                        url = child.item(k).getFirstChild().getNodeValue();
                                    }
                                }
                            }
                            TapjoyLog.i(TAPJOY_VIDEO, "name: " + name);
                            TapjoyLog.i(TAPJOY_VIDEO, "url: " + url);
                            videoObject.addButton(name, url);
                        }
                    }
                    this.videoQueue.addElement(videoObject.offerID);
                    this.uncachedVideos.put(videoObject.offerID, videoObject);
                    setVideoIDs();
                }
            }
            TapjoyLog.i(TAPJOY_VIDEO, "========================================");
            return true;
        } catch (Exception e) {
            TapjoyLog.e(TAPJOY_VIDEO, "Error parsing XML: " + e.toString());
            return false;
        }
    }

    public TapjoyVideoObject getCurrentVideoData() {
        return this.videoToPlay;
    }

    public boolean startVideo(String videoID, String currencyName, String currencyAmount, String clickURL, String webviewURL) {
        File video;
        boolean cachedVideo = true;
        TapjoyLog.i(TAPJOY_VIDEO, "Starting video activity with video: " + videoID);
        if (videoID == null || clickURL == null || webviewURL == null || videoID.length() == 0 || clickURL.length() == 0 || webviewURL.length() == 0) {
            TapjoyLog.i(TAPJOY_VIDEO, "aborting video playback... invalid or missing parameter");
            return false;
        }
        this.videoToPlay = this.cachedVideos.get(videoID);
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            TapjoyLog.e(TAPJOY_VIDEO, "Cannot access external storage");
            tapjoyVideoNotifier.videoError(1);
            return false;
        }
        if (this.videoToPlay == null) {
            TapjoyLog.i(TAPJOY_VIDEO, "video not cached... checking uncached videos");
            this.videoToPlay = this.uncachedVideos.get(videoID);
            if (this.videoToPlay == null) {
                TapjoyLog.e(TAPJOY_VIDEO, "null video object? aborting.");
                return false;
            }
            cachedVideo = false;
        }
        this.videoToPlay.currencyName = currencyName;
        this.videoToPlay.currencyAmount = currencyAmount;
        this.videoToPlay.clickURL = clickURL;
        this.videoToPlay.webviewURL = webviewURL;
        TapjoyLog.i(TAPJOY_VIDEO, "videoToPlay: " + this.videoToPlay.offerID);
        TapjoyLog.i(TAPJOY_VIDEO, "amount: " + this.videoToPlay.currencyAmount);
        TapjoyLog.i(TAPJOY_VIDEO, "currency: " + this.videoToPlay.currencyName);
        TapjoyLog.i(TAPJOY_VIDEO, "clickURL: " + this.videoToPlay.clickURL);
        TapjoyLog.i(TAPJOY_VIDEO, "location: " + this.videoToPlay.dataLocation);
        TapjoyLog.i(TAPJOY_VIDEO, "webviewURL: " + this.videoToPlay.webviewURL);
        if (!cachedVideo || this.videoToPlay.dataLocation == null || ((video = new File(this.videoToPlay.dataLocation)) != null && video.exists())) {
            Intent videoIntent = new Intent(this.context, TapjoyVideoView.class);
            videoIntent.setFlags(268435456);
            videoIntent.putExtra(TapjoyConstants.EXTRA_VIDEO_PATH, videoID);
            this.context.startActivity(videoIntent);
            return true;
        }
        TapjoyLog.e(TAPJOY_VIDEO, "video file does not exist.");
        return false;
    }

    public void loadNextVideo() {
        TapjoyLog.i(TAPJOY_VIDEO, "++++++++++++++++++++++++++++++++++++++++");
        TapjoyLog.i(TAPJOY_VIDEO, "LOAD NEXT VIDEO");
        TapjoyLog.i(TAPJOY_VIDEO, "videoQueue size: " + this.videoQueue.size());
        TapjoyLog.i(TAPJOY_VIDEO, "uncachedVideos size: " + this.uncachedVideos.size());
        TapjoyLog.i(TAPJOY_VIDEO, "cachedVideos size: " + this.cachedVideos.size());
        if (this.cachedVideos.size() < this.videoCacheLimit && this.videoQueue.size() > 0) {
            cacheVideo(this.uncachedVideos.get(this.videoQueue.elementAt(0)).videoURL);
        }
        TapjoyLog.i(TAPJOY_VIDEO, "++++++++++++++++++++++++++++++++++++++++");
    }

    public static TapjoyVideoNotifier getVideoNotifier() {
        return tapjoyVideoNotifier;
    }

    private void cacheVideo(final String url) {
        TapjoyLog.i(TAPJOY_VIDEO, "download and cache video from: " + url);
        new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:20:0x0166  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x0175 A[ADDED_TO_REGION] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r28 = this;
                    long r22 = java.lang.System.currentTimeMillis()
                    r14 = 0
                    r4 = 0
                    r10 = 0
                    r16 = 0
                    r8 = 0
                    r18 = 0
                    r19 = 0
                    java.net.URL r9 = new java.net.URL     // Catch:{ SocketTimeoutException -> 0x028d, Exception -> 0x0255 }
                    r0 = r28
                    java.lang.String r0 = r4     // Catch:{ SocketTimeoutException -> 0x028d, Exception -> 0x0255 }
                    r24 = r0
                    r0 = r24
                    r9.<init>(r0)     // Catch:{ SocketTimeoutException -> 0x028d, Exception -> 0x0255 }
                    java.net.URLConnection r3 = r9.openConnection()     // Catch:{ SocketTimeoutException -> 0x028d, Exception -> 0x0255 }
                    r24 = 15000(0x3a98, float:2.102E-41)
                    r0 = r24
                    r3.setConnectTimeout(r0)     // Catch:{ SocketTimeoutException -> 0x028d, Exception -> 0x0255 }
                    r24 = 30000(0x7530, float:4.2039E-41)
                    r0 = r24
                    r3.setReadTimeout(r0)     // Catch:{ SocketTimeoutException -> 0x028d, Exception -> 0x0255 }
                    r3.connect()     // Catch:{ SocketTimeoutException -> 0x028d, Exception -> 0x0255 }
                    java.io.BufferedInputStream r11 = new java.io.BufferedInputStream     // Catch:{ SocketTimeoutException -> 0x028d, Exception -> 0x0255 }
                    java.io.InputStream r24 = r3.getInputStream()     // Catch:{ SocketTimeoutException -> 0x028d, Exception -> 0x0255 }
                    r0 = r24
                    r11.<init>(r0)     // Catch:{ SocketTimeoutException -> 0x028d, Exception -> 0x0255 }
                    java.io.File r7 = new java.io.File     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r0 = r28
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r24 = r0
                    java.lang.String r24 = r24.videoCacheDir     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r0 = r24
                    r7.<init>(r0)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r0 = r28
                    java.lang.String r0 = r4     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r24 = r0
                    r25 = 0
                    r0 = r28
                    java.lang.String r0 = r4     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r26 = r0
                    java.lang.String r27 = "/"
                    int r26 = r26.lastIndexOf(r27)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    int r26 = r26 + 1
                    java.lang.String r18 = r24.substring(r25, r26)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r0 = r28
                    java.lang.String r0 = r4     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r24 = r0
                    r0 = r28
                    java.lang.String r0 = r4     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r25 = r0
                    java.lang.String r26 = "/"
                    int r25 = r25.lastIndexOf(r26)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    int r25 = r25 + 1
                    java.lang.String r8 = r24.substring(r25)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r24 = 0
                    r25 = 46
                    r0 = r25
                    int r25 = r8.indexOf(r0)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r0 = r24
                    r1 = r25
                    java.lang.String r8 = r8.substring(r0, r1)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r24 = "TapjoyVideo"
                    java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r26 = "fileDir: "
                    r25.<init>(r26)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r0 = r25
                    java.lang.StringBuilder r25 = r0.append(r7)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r25 = r25.toString()     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    com.tapjoy.TapjoyLog.i(r24, r25)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r24 = "TapjoyVideo"
                    java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r26 = "path: "
                    r25.<init>(r26)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r0 = r25
                    r1 = r18
                    java.lang.StringBuilder r25 = r0.append(r1)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r25 = r25.toString()     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    com.tapjoy.TapjoyLog.i(r24, r25)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r24 = "TapjoyVideo"
                    java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r26 = "file name: "
                    r25.<init>(r26)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r0 = r25
                    java.lang.StringBuilder r25 = r0.append(r8)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r25 = r25.toString()     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    com.tapjoy.TapjoyLog.i(r24, r25)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    boolean r24 = r7.mkdirs()     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    if (r24 == 0) goto L_0x00f2
                    java.lang.String r24 = "TapjoyVideo"
                    java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r26 = "created directory at: "
                    r25.<init>(r26)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r26 = r7.getPath()     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.StringBuilder r25 = r25.append(r26)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.lang.String r25 = r25.toString()     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    com.tapjoy.TapjoyLog.i(r24, r25)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                L_0x00f2:
                    java.io.File r20 = new java.io.File     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r0 = r28
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r24 = r0
                    java.lang.String r24 = r24.videoCacheDir     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    r0 = r20
                    r1 = r24
                    r0.<init>(r1, r8)     // Catch:{ SocketTimeoutException -> 0x028f, Exception -> 0x027e }
                    java.io.FileOutputStream r17 = new java.io.FileOutputStream     // Catch:{ SocketTimeoutException -> 0x0292, Exception -> 0x0281 }
                    r0 = r17
                    r1 = r20
                    r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x0292, Exception -> 0x0281 }
                    java.lang.String r24 = "TapjoyVideo"
                    java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    java.lang.String r26 = "downloading video file to: "
                    r25.<init>(r26)     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    java.lang.String r26 = r20.toString()     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    java.lang.StringBuilder r25 = r25.append(r26)     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    java.lang.String r25 = r25.toString()     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    com.tapjoy.TapjoyLog.i(r24, r25)     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    r24 = 1024(0x400, float:1.435E-42)
                    r0 = r24
                    byte[] r2 = new byte[r0]     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                L_0x012c:
                    int r13 = r11.read(r2)     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    r24 = -1
                    r0 = r24
                    if (r13 != r0) goto L_0x0228
                    r17.close()     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    r11.close()     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    java.lang.String r24 = "TapjoyVideo"
                    java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    java.lang.String r26 = "FILE SIZE: "
                    r25.<init>(r26)     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    long r26 = r20.length()     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    java.lang.StringBuilder r25 = r25.append(r26)     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    java.lang.String r25 = r25.toString()     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    com.tapjoy.TapjoyLog.i(r24, r25)     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    long r24 = r20.length()     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    r26 = 0
                    int r24 = (r24 > r26 ? 1 : (r24 == r26 ? 0 : -1))
                    if (r24 != 0) goto L_0x0297
                    r14 = 1
                    r19 = r20
                    r16 = r17
                    r10 = r11
                L_0x0164:
                    if (r14 == 0) goto L_0x0173
                    java.lang.String r24 = "TapjoyVideo"
                    java.lang.String r25 = "Network timeout"
                    com.tapjoy.TapjoyLog.i(r24, r25)
                    r10.close()     // Catch:{ Exception -> 0x027b }
                    r16.close()     // Catch:{ Exception -> 0x027b }
                L_0x0173:
                    if (r14 != 0) goto L_0x0271
                    if (r4 != 0) goto L_0x0271
                    r0 = r28
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r24 = r0
                    java.util.Vector r24 = r24.videoQueue
                    r25 = 0
                    java.lang.Object r12 = r24.elementAt(r25)
                    java.lang.String r12 = (java.lang.String) r12
                    r0 = r28
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r24 = r0
                    java.util.Hashtable r24 = r24.uncachedVideos
                    r0 = r24
                    java.lang.Object r15 = r0.get(r12)
                    com.tapjoy.TapjoyVideoObject r15 = (com.tapjoy.TapjoyVideoObject) r15
                    java.lang.String r24 = r19.getAbsolutePath()
                    r0 = r24
                    r15.dataLocation = r0
                    r0 = r28
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r24 = r0
                    r0 = r24
                    android.content.Context r0 = r0.context
                    r24 = r0
                    java.lang.String r25 = "TapjoyVideoPrefs"
                    r26 = 0
                    android.content.SharedPreferences r21 = r24.getSharedPreferences(r25, r26)
                    android.content.SharedPreferences$Editor r6 = r21.edit()
                    r6.putString(r12, r12)
                    r6.commit()
                    r0 = r28
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r24 = r0
                    java.util.Hashtable r24 = r24.cachedVideos
                    r0 = r24
                    r0.put(r12, r15)
                    r0 = r28
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r24 = r0
                    java.util.Hashtable r24 = r24.uncachedVideos
                    r0 = r24
                    r0.remove(r12)
                    r0 = r28
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r24 = r0
                    java.util.Vector r24 = r24.videoQueue
                    r25 = 0
                    r24.removeElementAt(r25)
                    r0 = r28
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r24 = r0
                    r24.setVideoIDs()
                    r0 = r28
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r24 = r0
                    r24.loadNextVideo()
                    java.lang.String r24 = "TapjoyVideo"
                    java.lang.StringBuilder r25 = new java.lang.StringBuilder
                    java.lang.String r26 = "video cached in: "
                    r25.<init>(r26)
                    long r26 = java.lang.System.currentTimeMillis()
                    long r26 = r26 - r22
                    java.lang.StringBuilder r25 = r25.append(r26)
                    java.lang.String r26 = "ms"
                    java.lang.StringBuilder r25 = r25.append(r26)
                    java.lang.String r25 = r25.toString()
                    com.tapjoy.TapjoyLog.i(r24, r25)
                    com.tapjoy.TapjoyVideoNotifier r24 = com.tapjoy.TapjoyVideo.tapjoyVideoNotifier
                    r24.videoReady()
                L_0x0227:
                    return
                L_0x0228:
                    r24 = 0
                    r0 = r17
                    r1 = r24
                    r0.write(r2, r1, r13)     // Catch:{ SocketTimeoutException -> 0x0233, Exception -> 0x0286 }
                    goto L_0x012c
                L_0x0233:
                    r5 = move-exception
                    r19 = r20
                    r16 = r17
                    r10 = r11
                L_0x0239:
                    java.lang.String r24 = "TapjoyVideo"
                    java.lang.StringBuilder r25 = new java.lang.StringBuilder
                    java.lang.String r26 = "Network timeout: "
                    r25.<init>(r26)
                    java.lang.String r26 = r5.toString()
                    java.lang.StringBuilder r25 = r25.append(r26)
                    java.lang.String r25 = r25.toString()
                    com.tapjoy.TapjoyLog.e(r24, r25)
                    r14 = 1
                    r4 = 1
                    goto L_0x0164
                L_0x0255:
                    r5 = move-exception
                L_0x0256:
                    java.lang.String r24 = "TapjoyVideo"
                    java.lang.StringBuilder r25 = new java.lang.StringBuilder
                    java.lang.String r26 = "Error caching video file: "
                    r25.<init>(r26)
                    java.lang.String r26 = r5.toString()
                    java.lang.StringBuilder r25 = r25.append(r26)
                    java.lang.String r25 = r25.toString()
                    com.tapjoy.TapjoyLog.e(r24, r25)
                    r4 = 1
                    goto L_0x0164
                L_0x0271:
                    com.tapjoy.TapjoyVideoNotifier r24 = com.tapjoy.TapjoyVideo.tapjoyVideoNotifier
                    r25 = 2
                    r24.videoError(r25)
                    goto L_0x0227
                L_0x027b:
                    r24 = move-exception
                    goto L_0x0173
                L_0x027e:
                    r5 = move-exception
                    r10 = r11
                    goto L_0x0256
                L_0x0281:
                    r5 = move-exception
                    r19 = r20
                    r10 = r11
                    goto L_0x0256
                L_0x0286:
                    r5 = move-exception
                    r19 = r20
                    r16 = r17
                    r10 = r11
                    goto L_0x0256
                L_0x028d:
                    r5 = move-exception
                    goto L_0x0239
                L_0x028f:
                    r5 = move-exception
                    r10 = r11
                    goto L_0x0239
                L_0x0292:
                    r5 = move-exception
                    r19 = r20
                    r10 = r11
                    goto L_0x0239
                L_0x0297:
                    r19 = r20
                    r16 = r17
                    r10 = r11
                    goto L_0x0164
                */
                throw new UnsupportedOperationException("Method not decompiled: com.tapjoy.TapjoyVideo.AnonymousClass2.run():void");
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void cacheImage(final String url) {
        TapjoyLog.i(TAPJOY_VIDEO, "cacheImage: " + url);
        new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:20:0x015f  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x016e A[ADDED_TO_REGION] */
            /* JADX WARNING: Removed duplicated region for block: B:39:0x0229  */
            /* JADX WARNING: Removed duplicated region for block: B:55:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r24 = this;
                    long r18 = java.lang.System.currentTimeMillis()
                    r12 = 0
                    r4 = 0
                    r9 = 0
                    r13 = 0
                    r7 = 0
                    r15 = 0
                    r16 = 0
                    java.net.URL r8 = new java.net.URL     // Catch:{ SocketTimeoutException -> 0x025e, Exception -> 0x01fd }
                    r0 = r24
                    java.lang.String r0 = r4     // Catch:{ SocketTimeoutException -> 0x025e, Exception -> 0x01fd }
                    r20 = r0
                    r0 = r20
                    r8.<init>(r0)     // Catch:{ SocketTimeoutException -> 0x025e, Exception -> 0x01fd }
                    java.net.URLConnection r3 = r8.openConnection()     // Catch:{ SocketTimeoutException -> 0x025e, Exception -> 0x01fd }
                    r20 = 15000(0x3a98, float:2.102E-41)
                    r0 = r20
                    r3.setConnectTimeout(r0)     // Catch:{ SocketTimeoutException -> 0x025e, Exception -> 0x01fd }
                    r20 = 30000(0x7530, float:4.2039E-41)
                    r0 = r20
                    r3.setReadTimeout(r0)     // Catch:{ SocketTimeoutException -> 0x025e, Exception -> 0x01fd }
                    r3.connect()     // Catch:{ SocketTimeoutException -> 0x025e, Exception -> 0x01fd }
                    java.io.BufferedInputStream r10 = new java.io.BufferedInputStream     // Catch:{ SocketTimeoutException -> 0x025e, Exception -> 0x01fd }
                    java.io.InputStream r20 = r3.getInputStream()     // Catch:{ SocketTimeoutException -> 0x025e, Exception -> 0x01fd }
                    r0 = r20
                    r10.<init>(r0)     // Catch:{ SocketTimeoutException -> 0x025e, Exception -> 0x01fd }
                    java.io.File r6 = new java.io.File     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r0 = r24
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r20 = r0
                    java.lang.String r20 = r20.imageCacheDir     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r0 = r20
                    r6.<init>(r0)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r0 = r24
                    java.lang.String r0 = r4     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r20 = r0
                    r21 = 0
                    r0 = r24
                    java.lang.String r0 = r4     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r22 = r0
                    java.lang.String r23 = "/"
                    int r22 = r22.lastIndexOf(r23)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    int r22 = r22 + 1
                    java.lang.String r15 = r20.substring(r21, r22)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r0 = r24
                    java.lang.String r0 = r4     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r20 = r0
                    r0 = r24
                    java.lang.String r0 = r4     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r21 = r0
                    java.lang.String r22 = "/"
                    int r21 = r21.lastIndexOf(r22)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    int r21 = r21 + 1
                    java.lang.String r7 = r20.substring(r21)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r20 = 0
                    r21 = 46
                    r0 = r21
                    int r21 = r7.indexOf(r0)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r0 = r20
                    r1 = r21
                    java.lang.String r7 = r7.substring(r0, r1)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r20 = "TapjoyVideo"
                    java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r22 = "fileDir: "
                    r21.<init>(r22)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r0 = r21
                    java.lang.StringBuilder r21 = r0.append(r6)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r21 = r21.toString()     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    com.tapjoy.TapjoyLog.i(r20, r21)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r20 = "TapjoyVideo"
                    java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r22 = "path: "
                    r21.<init>(r22)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r0 = r21
                    java.lang.StringBuilder r21 = r0.append(r15)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r21 = r21.toString()     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    com.tapjoy.TapjoyLog.i(r20, r21)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r20 = "TapjoyVideo"
                    java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r22 = "file name: "
                    r21.<init>(r22)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r0 = r21
                    java.lang.StringBuilder r21 = r0.append(r7)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r21 = r21.toString()     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    com.tapjoy.TapjoyLog.i(r20, r21)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    boolean r20 = r6.mkdirs()     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    if (r20 == 0) goto L_0x00ee
                    java.lang.String r20 = "TapjoyVideo"
                    java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r22 = "created directory at: "
                    r21.<init>(r22)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r22 = r6.getPath()     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.lang.String r21 = r21.toString()     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    com.tapjoy.TapjoyLog.i(r20, r21)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                L_0x00ee:
                    java.io.File r17 = new java.io.File     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r0 = r24
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r20 = r0
                    java.lang.String r20 = r20.imageCacheDir     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    r0 = r17
                    r1 = r20
                    r0.<init>(r1, r7)     // Catch:{ SocketTimeoutException -> 0x0260, Exception -> 0x0250 }
                    java.io.FileOutputStream r14 = new java.io.FileOutputStream     // Catch:{ SocketTimeoutException -> 0x0264, Exception -> 0x0253 }
                    r0 = r17
                    r14.<init>(r0)     // Catch:{ SocketTimeoutException -> 0x0264, Exception -> 0x0253 }
                    java.lang.String r20 = "TapjoyVideo"
                    java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    java.lang.String r22 = "downloading image file to: "
                    r21.<init>(r22)     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    java.lang.String r22 = r17.toString()     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    java.lang.String r21 = r21.toString()     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    com.tapjoy.TapjoyLog.i(r20, r21)     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    r20 = 1024(0x400, float:1.435E-42)
                    r0 = r20
                    byte[] r2 = new byte[r0]     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                L_0x0126:
                    int r11 = r10.read(r2)     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    r20 = -1
                    r0 = r20
                    if (r11 != r0) goto L_0x01d3
                    r14.close()     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    r10.close()     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    java.lang.String r20 = "TapjoyVideo"
                    java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    java.lang.String r22 = "FILE SIZE: "
                    r21.<init>(r22)     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    long r22 = r17.length()     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    java.lang.String r21 = r21.toString()     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    com.tapjoy.TapjoyLog.i(r20, r21)     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    long r20 = r17.length()     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    r22 = 0
                    int r20 = (r20 > r22 ? 1 : (r20 == r22 ? 0 : -1))
                    if (r20 != 0) goto L_0x026a
                    r12 = 1
                    r16 = r17
                    r13 = r14
                    r9 = r10
                L_0x015d:
                    if (r12 == 0) goto L_0x016c
                    java.lang.String r20 = "TapjoyVideo"
                    java.lang.String r21 = "Network timeout"
                    com.tapjoy.TapjoyLog.i(r20, r21)
                    r9.close()     // Catch:{ Exception -> 0x024d }
                    r13.close()     // Catch:{ Exception -> 0x024d }
                L_0x016c:
                    if (r12 != 0) goto L_0x0219
                    if (r4 != 0) goto L_0x0219
                    r0 = r24
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r20 = r0
                    java.util.Vector r20 = r20.imageQueue
                    r21 = 0
                    r20.removeElementAt(r21)
                    r0 = r24
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r20 = r0
                    java.util.Vector r20 = r20.imageQueue
                    int r20 = r20.size()
                    if (r20 <= 0) goto L_0x01b2
                    r0 = r24
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r21 = r0
                    r0 = r24
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r20 = r0
                    java.util.Vector r20 = r20.imageQueue
                    r22 = 0
                    r0 = r20
                    r1 = r22
                    java.lang.Object r20 = r0.elementAt(r1)
                    java.lang.String r20 = (java.lang.String) r20
                    r0 = r21
                    r1 = r20
                    r0.cacheImage(r1)
                L_0x01b2:
                    java.lang.String r20 = "TapjoyVideo"
                    java.lang.StringBuilder r21 = new java.lang.StringBuilder
                    java.lang.String r22 = "image cached in: "
                    r21.<init>(r22)
                    long r22 = java.lang.System.currentTimeMillis()
                    long r22 = r22 - r18
                    java.lang.StringBuilder r21 = r21.append(r22)
                    java.lang.String r22 = "ms"
                    java.lang.StringBuilder r21 = r21.append(r22)
                    java.lang.String r21 = r21.toString()
                    com.tapjoy.TapjoyLog.i(r20, r21)
                L_0x01d2:
                    return
                L_0x01d3:
                    r20 = 0
                    r0 = r20
                    r14.write(r2, r0, r11)     // Catch:{ SocketTimeoutException -> 0x01dc, Exception -> 0x0258 }
                    goto L_0x0126
                L_0x01dc:
                    r5 = move-exception
                    r16 = r17
                    r13 = r14
                    r9 = r10
                L_0x01e1:
                    java.lang.String r20 = "TapjoyVideo"
                    java.lang.StringBuilder r21 = new java.lang.StringBuilder
                    java.lang.String r22 = "Network timeout: "
                    r21.<init>(r22)
                    java.lang.String r22 = r5.toString()
                    java.lang.StringBuilder r21 = r21.append(r22)
                    java.lang.String r21 = r21.toString()
                    com.tapjoy.TapjoyLog.e(r20, r21)
                    r12 = 1
                    r4 = 1
                    goto L_0x015d
                L_0x01fd:
                    r5 = move-exception
                L_0x01fe:
                    java.lang.String r20 = "TapjoyVideo"
                    java.lang.StringBuilder r21 = new java.lang.StringBuilder
                    java.lang.String r22 = "Error caching image file: "
                    r21.<init>(r22)
                    java.lang.String r22 = r5.toString()
                    java.lang.StringBuilder r21 = r21.append(r22)
                    java.lang.String r21 = r21.toString()
                    com.tapjoy.TapjoyLog.e(r20, r21)
                    r4 = 1
                    goto L_0x015d
                L_0x0219:
                    r0 = r24
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r20 = r0
                    java.util.Vector r20 = r20.imageQueue
                    int r20 = r20.size()
                    if (r20 <= 0) goto L_0x01d2
                    r0 = r24
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r21 = r0
                    r0 = r24
                    com.tapjoy.TapjoyVideo r0 = com.tapjoy.TapjoyVideo.this
                    r20 = r0
                    java.util.Vector r20 = r20.imageQueue
                    r22 = 0
                    r0 = r20
                    r1 = r22
                    java.lang.Object r20 = r0.elementAt(r1)
                    java.lang.String r20 = (java.lang.String) r20
                    r0 = r21
                    r1 = r20
                    r0.cacheImage(r1)
                    goto L_0x01d2
                L_0x024d:
                    r20 = move-exception
                    goto L_0x016c
                L_0x0250:
                    r5 = move-exception
                    r9 = r10
                    goto L_0x01fe
                L_0x0253:
                    r5 = move-exception
                    r16 = r17
                    r9 = r10
                    goto L_0x01fe
                L_0x0258:
                    r5 = move-exception
                    r16 = r17
                    r13 = r14
                    r9 = r10
                    goto L_0x01fe
                L_0x025e:
                    r5 = move-exception
                    goto L_0x01e1
                L_0x0260:
                    r5 = move-exception
                    r9 = r10
                    goto L_0x01e1
                L_0x0264:
                    r5 = move-exception
                    r16 = r17
                    r9 = r10
                    goto L_0x01e1
                L_0x026a:
                    r16 = r17
                    r13 = r14
                    r9 = r10
                    goto L_0x015d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.tapjoy.TapjoyVideo.AnonymousClass3.run():void");
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void setVideoIDs() {
        String videoIDs = "";
        if (this.cachedVideos != null && this.cachedVideos.size() > 0) {
            Enumeration<String> keys = this.cachedVideos.keys();
            while (keys.hasMoreElements()) {
                videoIDs = String.valueOf(videoIDs) + keys.nextElement();
                if (keys.hasMoreElements()) {
                    videoIDs = String.valueOf(videoIDs) + ",";
                }
            }
            TapjoyLog.i(TAPJOY_VIDEO, "cachedVideos size: " + this.cachedVideos.size());
        }
        TapjoyLog.i(TAPJOY_VIDEO, "videoIDs: [" + videoIDs + "]");
        TapjoyConnectCore.setVideoIDs(videoIDs);
    }
}
