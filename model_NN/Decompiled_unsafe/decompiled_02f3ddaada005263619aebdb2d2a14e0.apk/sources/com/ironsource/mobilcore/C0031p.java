package com.ironsource.mobilcore;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.mobilcore.MobileCore;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.ironsource.mobilcore.p  reason: case insensitive filesystem */
class C0031p {
    private static MobileCore.LOG_TYPE a;
    private static String b;

    public static void a(MobileCore.LOG_TYPE log_type) {
        a = log_type;
    }

    public static void a(String str, int i) {
        switch (i) {
            case 2:
                Log.e("MobileCore", str);
                return;
            case 3:
                if (a == MobileCore.LOG_TYPE.DEBUG) {
                    Log.i("MobileCore", str);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public static void a(Context context, String str, String str2, String str3, C0038w... wVarArr) {
        Intent intent = new Intent(context, MobileCoreReport.class);
        intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 99);
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow", "events");
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_component", str);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_event", str2);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_action", str3);
        if (wVarArr != null && wVarArr.length > 0) {
            JSONObject jSONObject = new JSONObject();
            for (C0038w wVar : wVarArr) {
                if (wVar != null) {
                    try {
                        jSONObject.put(wVar.a, wVar.b);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_additional_params", jSONObject.toString());
        }
        if (TextUtils.isEmpty(b)) {
            b = C0017b.n((Build.VERSION.SDK_INT >= 9 ? context.getSharedPreferences("1%dss#gfs#ge1%dr1%dps#g_s#gds#ge1%drs#gas#ghs#gSs#g_s#ge1%dr1%dos#gCs#ge1%dls#gis#gb1%do1%dm", 4) : context.getSharedPreferences("1%dss#gfs#ge1%dr1%dps#g_s#gds#ge1%drs#gas#ghs#gSs#g_s#ge1%dr1%dos#gCs#ge1%dls#gis#gb1%do1%dm", 0)).getString("1%dns#ge1%dk1%do1%dts#g_1%dss#gfs#ge1%dr1%dp", ""));
        }
        intent.putExtra("1%dns#ge1%dk1%do1%dt", b);
        context.startService(intent);
    }
}
