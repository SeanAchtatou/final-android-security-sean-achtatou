package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.view.MotionEvent;

final class Q extends ak {
    Q(Activity activity, int i) {
        super(activity, i);
    }

    public final void a(boolean z) {
        a(-this.u, 0, z);
    }

    public final void b(boolean z) {
        a(0, 0, z);
    }

    public final void a(int i) {
        this.p = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-16777216, 0});
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int i7 = (int) this.c;
        this.s.layout(0, i6 - this.u, i5, i6);
        f(i7);
        if (m) {
            this.t.layout(0, 0, i5, i6);
        } else {
            this.t.layout(0, i7, i5, i6 + i7);
        }
    }

    @SuppressLint({"NewApi"})
    private void f(int i) {
        if (this.l && this.u != 0) {
            int height = getHeight();
            int i2 = this.u;
            float f = (((float) i2) + ((float) i)) / ((float) i2);
            if (!m) {
                this.s.offsetTopAndBottom(((height - this.u) + ((int) ((((float) i2) * f) * 0.25f))) - this.s.getTop());
                this.s.setVisibility(i == 0 ? 4 : 0);
            } else if (i != 0) {
                this.s.setTranslationY((float) ((int) (((float) i2) * f * 0.25f)));
            } else {
                this.s.setTranslationY((float) (height + i2));
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas, int i) {
        int width = getWidth();
        int height = getHeight();
        this.p.setBounds(0, height + i, width, height + i + this.q);
        this.p.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void b(Canvas canvas, int i) {
        int width = getWidth();
        int height = getHeight();
        float abs = ((float) Math.abs(i)) / ((float) this.u);
        this.n.setBounds(0, height + i, width, height);
        this.n.setAlpha((int) (185.0f * (1.0f - abs)));
        this.n.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.i.a(0, 0, (-this.u) / 3, 0, 1000);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    public final void b(int i) {
        if (m) {
            this.t.setTranslationY((float) i);
            f(i);
            invalidate();
            return;
        }
        this.t.offsetTopAndBottom(i - this.t.getTop());
        f(i);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final boolean a(MotionEvent motionEvent) {
        return motionEvent.getY() < ((float) getHeight()) + this.c;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        int height = getHeight();
        return (!this.w && this.f >= ((float) (height - this.z))) || (this.w && this.f <= ((float) height) + this.c);
    }

    /* access modifiers changed from: protected */
    public final boolean a(float f) {
        int height = getHeight();
        return (!this.w && this.f >= ((float) (height - this.z)) && f < 0.0f) || (this.w && this.f <= ((float) height) + this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    public final void b(float f) {
        c(Math.max(Math.min(this.c + f, 0.0f), (float) (-this.u)));
    }

    /* access modifiers changed from: protected */
    public final void b(MotionEvent motionEvent) {
        int i = (int) this.c;
        if (this.d) {
            this.j.computeCurrentVelocity(1000, (float) this.k);
            int xVelocity = (int) this.j.getXVelocity();
            this.h = motionEvent.getY();
            a(this.j.getYVelocity() < 0.0f ? -this.u : 0, xVelocity, true);
        } else if (this.w && motionEvent.getY() < ((float) (i + getHeight()))) {
            b(true);
        }
    }
}
