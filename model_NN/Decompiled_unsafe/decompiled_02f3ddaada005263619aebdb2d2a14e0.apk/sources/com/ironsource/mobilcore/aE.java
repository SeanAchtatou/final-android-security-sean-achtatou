package com.ironsource.mobilcore;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class aE extends ArrayList<aD> {
    private static final long serialVersionUID = 1;

    aE() {
    }

    public final void a(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < size()) {
                if (a(((aD) get(i2)).d(), str)) {
                    remove(i2);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public final int b(String str) {
        boolean z;
        for (int i = 0; i < size(); i++) {
            if (get(i) != null) {
                z = true;
            } else {
                z = false;
            }
            if (a(((aD) get(i)).d(), str) && z) {
                return i;
            }
        }
        return -1;
    }

    private boolean a(String str, String str2) {
        if (str2 == null) {
            return false;
        }
        return d(str).equals(d(str2));
    }

    private static String d(String str) {
        if (str.endsWith("/")) {
            str = str.substring(0, str.length() - 1);
        }
        if (str.startsWith("https://")) {
            str = str.substring(8, str.length());
        } else if (str.startsWith("http://")) {
            str = str.substring(7, str.length());
        }
        if (str.startsWith("www.")) {
            return str.substring(4, str.length());
        }
        return str;
    }

    public final aD c(String str) {
        int b = b(str);
        if (b == -1) {
            return null;
        }
        return (aD) get(b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.aE.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.aE.a(java.lang.String, java.lang.String):boolean
      com.ironsource.mobilcore.aE.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) {
        a(jSONObject, false);
    }

    public final void a(JSONObject jSONObject, boolean z) {
        clear();
        try {
            String optString = jSONObject.optString("id", "");
            JSONArray jSONArray = jSONObject.getJSONArray("ads");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject optJSONObject = jSONArray.optJSONObject(i);
                if (!z || optJSONObject.optBoolean("isSliderCompatible", true)) {
                    add(new aD(optJSONObject.optString("appId"), optJSONObject.optString("title"), i, optJSONObject.optString("aff"), optJSONObject.optString("type", "Market"), optJSONObject.optString("click"), optJSONObject.optDouble("cpi", 0.0d), optJSONObject.optString("img", ""), optJSONObject.optString("impression", ""), optJSONObject.optJSONArray("extra"), optString));
                }
            }
        } catch (JSONException e) {
            C0031p.a("parseJson | failed to parse offer json!", 55);
        }
    }
}
