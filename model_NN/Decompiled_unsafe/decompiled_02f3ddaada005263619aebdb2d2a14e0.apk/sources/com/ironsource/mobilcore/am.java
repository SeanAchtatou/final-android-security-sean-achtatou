package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.ironsource.mobilcore.C0036u;
import com.ironsource.mobilcore.H;
import com.ironsource.mobilcore.Y;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class am implements MCISliderAPI {
    private static am w;
    /* access modifiers changed from: private */
    public Context a;
    /* access modifiers changed from: private */
    public Activity b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;
    private boolean f;
    private aA g;
    /* access modifiers changed from: private */
    public HashMap<String, C0036u> h;
    private P i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public String k;
    private ArrayList<a> l;
    private OrientationEventListener m;
    /* access modifiers changed from: private */
    public int n = -1;
    private int[] o = {5000, 22000, 42000};
    private int p;
    private Runnable q;
    /* access modifiers changed from: private */
    public Y r;
    /* access modifiers changed from: private */
    public L s;
    /* access modifiers changed from: private */
    public H t;
    /* access modifiers changed from: private */
    public ImageView u;
    private ProgressBar v;
    private boolean x = false;

    static /* synthetic */ void a(am amVar, C0036u.a aVar) {
        if (av.b().getBoolean("sliderOpenedByUser", false)) {
            for (String str : amVar.h.keySet()) {
                C0036u uVar = amVar.h.get(str);
                if (uVar != null) {
                    uVar.a(aVar);
                }
            }
        }
    }

    private am() {
    }

    public static synchronized am a() {
        am amVar;
        synchronized (am.class) {
            if (w == null) {
                w = new am();
            }
            amVar = w;
        }
        return amVar;
    }

    /* access modifiers changed from: private */
    public int c() {
        try {
            return this.a.getPackageManager().getPackageInfo(this.a.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    public void setContentViewWithSlider(Activity activity, int i2) {
        setContentViewWithSlider(activity, i2, -1);
    }

    public void setContentViewWithSlider(Activity activity, int i2, int i3) {
        this.b = activity;
        this.s = new L(this.b);
        this.s.setVerticalScrollBarEnabled(false);
        this.s.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.t = new H(this.b, new H.a() {
            public final boolean a() {
                if (!am.this.isSliderMenuOpen()) {
                    return false;
                }
                am.this.closeSliderMenu(true);
                return true;
            }
        });
        this.t.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.t.setOrientation(1);
        this.t.setFocusableInTouchMode(true);
        this.s.addView(this.t);
        this.r = Y.a(this.b, 1);
        View inflate = activity.getLayoutInflater().inflate(i2, (ViewGroup) null);
        inflate.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        if (inflate.getBackground() == null) {
            C0031p.a("No background set. setting default default ", 3);
            inflate.setBackgroundColor(-1);
        }
        this.u = new ImageView(this.a);
        RelativeLayout relativeLayout = new RelativeLayout(this.a);
        relativeLayout.addView(inflate);
        relativeLayout.addView(this.u);
        this.u.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                am.this.toggleSliderMenu(true);
            }
        });
        this.r.b(relativeLayout);
        this.r.a(this.s);
        d();
        e();
        if (this.m == null) {
            this.m = new OrientationEventListener(MobileCore.b, 3) {
                public final void onOrientationChanged(int i) {
                    int i2 = am.this.a.getResources().getConfiguration().orientation;
                    if (i2 != am.this.n && av.h(am.this.a)) {
                        int unused = am.this.n = i2;
                        am.this.d();
                        am.this.e();
                    }
                }
            };
        }
        this.m.enable();
        this.r.a(new Y.a(this, this.b.getWindow().getAttributes().softInputMode));
        String a2 = this.e != null ? this.e : a(i3);
        if (!TextUtils.isEmpty(a2)) {
            a(a2);
        }
    }

    public void openSliderMenu(boolean z) {
        if (this.r != null) {
            this.r.a(z);
            g();
        }
    }

    public void closeSliderMenu(boolean z) {
        if (this.r != null) {
            this.r.b(z);
        }
    }

    public void toggleSliderMenu(boolean z) {
        if (this.r != null) {
            this.r.c(z);
            g();
        }
    }

    public void showWidget(String str) {
        C0036u uVar;
        if (this.h.containsKey(str) && (uVar = this.h.get(str)) != null) {
            uVar.a(true);
        }
    }

    public void hideWidget(String str) {
        C0036u uVar;
        if (this.h.containsKey(str) && (uVar = this.h.get(str)) != null) {
            uVar.a(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.am.a(java.lang.String, com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String, boolean):void
     arg types: [java.lang.String, com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String, int]
     candidates:
      com.ironsource.mobilcore.am.a(com.ironsource.mobilcore.am, java.lang.String, int, boolean):void
      com.ironsource.mobilcore.am.a(java.lang.String, com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String, boolean):void */
    public void setWidgetTextProperty(String str, MCEWidgetTextProperties mCEWidgetTextProperties, String str2) {
        a(str, mCEWidgetTextProperties, str2, true);
    }

    public String getWidgetTextProperty(String str, MCEWidgetTextProperties mCEWidgetTextProperties) {
        String a2;
        C0036u uVar = this.h.get(str);
        if (uVar == null || !(uVar instanceof C0037v) || (a2 = ((C0037v) uVar).a(mCEWidgetTextProperties)) == null) {
            return "";
        }
        return a2;
    }

    public void setWidgetIconResource(String str, int i2) {
        a(str, i2, true);
    }

    public boolean isSliderMenuOpen() {
        if (this.r != null) {
            return this.r.c();
        }
        return false;
    }

    public void setEmphasizedWidget(String str) {
        this.k = str;
    }

    public final void a(Context context, String str) {
        if (!this.x) {
            this.x = true;
            this.a = context;
            this.c = str;
            String str2 = this.a.getFilesDir().getPath() + "/" + "slider_assets";
            File file = new File(str2);
            if (!file.exists()) {
                file.mkdir();
            }
            this.d = str2;
            this.f = false;
            this.g = new aA(this.a);
            this.h = new HashMap<>();
            this.l = new ArrayList<>();
            this.j = false;
            b();
        }
    }

    @SuppressLint({"NewApi"})
    public final void b() {
        b bVar = new b(this, (byte) 0);
        if (Build.VERSION.SDK_INT >= 11) {
            bVar.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        } else {
            bVar.execute(new Void[0]);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(int r6) {
        /*
            r5 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r5.d
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "/sliderConfig.json"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ IOException -> 0x004f }
            r2.<init>(r0)     // Catch:{ IOException -> 0x004f }
            java.lang.String r0 = a(r2)     // Catch:{ IOException -> 0x004f }
            r2.close()     // Catch:{ IOException -> 0x0071 }
        L_0x0022:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x004e
            r1 = -1
            if (r1 == r6) goto L_0x0039
            android.content.Context r1 = r5.a     // Catch:{ Exception -> 0x0061 }
            android.content.res.Resources r1 = r1.getResources()     // Catch:{ Exception -> 0x0061 }
            java.io.InputStream r1 = r1.openRawResource(r6)     // Catch:{ Exception -> 0x0061 }
            java.lang.String r0 = a(r1)     // Catch:{ Exception -> 0x0061 }
        L_0x0039:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x004e
            java.lang.String r1 = "fallback/slider_fallback.json"
            java.lang.Class<com.ironsource.mobilcore.MobileCore> r2 = com.ironsource.mobilcore.MobileCore.class
            java.io.InputStream r1 = r2.getResourceAsStream(r1)     // Catch:{ Exception -> 0x0069 }
            java.lang.String r0 = a(r1)     // Catch:{ Exception -> 0x0069 }
            r1.close()     // Catch:{ Exception -> 0x0069 }
        L_0x004e:
            return r0
        L_0x004f:
            r0 = move-exception
            r4 = r0
            r0 = r1
            r1 = r4
        L_0x0053:
            r1.printStackTrace()
            java.lang.String r2 = "Couldn't find sliderConfig.json. using fallback"
            r3 = 55
            com.ironsource.mobilcore.C0031p.a(r2, r3)
            r5.a(r1)
            goto L_0x0022
        L_0x0061:
            r1 = move-exception
            r1.printStackTrace()
            r5.a(r1)
            goto L_0x0039
        L_0x0069:
            r1 = move-exception
            r1.printStackTrace()
            r5.a(r1)
            goto L_0x004e
        L_0x0071:
            r1 = move-exception
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.am.a(int):java.lang.String");
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        String g2;
        try {
            JSONObject jSONObject = new JSONObject(str).getJSONObject("slider");
            for (String str2 : this.h.keySet()) {
                C0036u uVar = this.h.get(str2);
                if (uVar != null) {
                    uVar.m();
                }
            }
            this.h.clear();
            this.i = new P(this.a, jSONObject);
            JSONArray jSONArray = jSONObject.getJSONArray("widgets");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                C0036u a2 = this.g.a(jSONArray.getJSONObject(i2), this.i.d());
                if (!(a2 == null || (g2 = a2.g()) == "" || this.h.containsKey(g2))) {
                    this.h.put(g2, a2);
                }
            }
            this.f = true;
            Iterator<a> it = this.l.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
            a(this.t);
            if (this.r != null) {
                this.r.removeCallbacks(this.q);
            }
            this.p = -1;
            this.q = null;
            if (this.i.b() && !av.b().getBoolean("sliderOpenedByUser", false)) {
                f();
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
            a(e2);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, MCEWidgetTextProperties mCEWidgetTextProperties, String str2, boolean z) {
        C0036u uVar;
        if (z) {
            this.l.add(new d(str, mCEWidgetTextProperties, str2));
        }
        if (this.f && this.h.containsKey(str) && (uVar = this.h.get(str)) != null && (uVar instanceof C0037v)) {
            ((C0037v) uVar).a(mCEWidgetTextProperties, str2);
            String str3 = null;
            switch (mCEWidgetTextProperties) {
                case MAIN_TEXT:
                    str3 = "text";
                    break;
                case SECONDARY_TEXT:
                    str3 = "secondary_text";
                    break;
                case BADGE_TEXT:
                    str3 = "badge_text";
                    break;
            }
            if (!TextUtils.isEmpty(str3)) {
                C0031p.a(this.a, "slider", "widget", "dynamic_widget_change", new C0038w("change_type", str3), new C0038w("widget_type", uVar.e()));
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, int i2, boolean z) {
        if (z) {
            this.l.add(new c(str, i2));
        }
        if (this.f && this.h.containsKey(str)) {
            C0036u uVar = this.h.get(str);
            if (uVar != null && (uVar instanceof C0037v)) {
                ((C0037v) uVar).b(i2);
            }
            C0031p.a(this.a, "slider", "widget", "dynamic_widget_change", new C0038w("change_type", "icon"), new C0038w("widget_type", uVar.e()));
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        double d2 = 0.5d;
        if (this.b != null) {
            Display defaultDisplay = this.b.getWindowManager().getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics);
            int i2 = displayMetrics.widthPixels;
            if (this.n == -1) {
                this.n = this.a.getResources().getConfiguration().orientation;
            }
            switch (this.n) {
                case 1:
                    d2 = 0.8d;
                    break;
            }
            this.r.c((int) (d2 * ((double) i2)));
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.u != null) {
            this.u.postDelayed(new Runnable() {
                public final void run() {
                    ViewParent parent = am.this.u.getParent();
                    if (parent instanceof View) {
                        Display defaultDisplay = am.this.b.getWindowManager().getDefaultDisplay();
                        DisplayMetrics displayMetrics = new DisplayMetrics();
                        defaultDisplay.getMetrics(displayMetrics);
                        am.this.u.measure(0, 0);
                        int i = displayMetrics.heightPixels;
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                        layoutParams.addRule(9);
                        int height = (((View) parent).getHeight() / 2) - (am.this.u.getHeight() / 2);
                        layoutParams.topMargin = height;
                        if (height <= 0 || height > i) {
                            layoutParams.addRule(15);
                        }
                        am.this.u.setLayoutParams(layoutParams);
                    }
                }
            }, 100);
        }
    }

    private void a(LinearLayout linearLayout) {
        if (linearLayout != null) {
            linearLayout.removeAllViews();
            if (!this.f) {
                this.v = new ProgressBar(this.a);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                layoutParams.topMargin = 20;
                layoutParams.gravity = 1;
                this.v.setLayoutParams(layoutParams);
                linearLayout.addView(this.v);
                C0031p.a(this.a, "slider", "slider_features", "progress_added", new C0038w[0]);
            } else if (this.i != null) {
                C0017b.a(this.s, this.i.d().m());
                if (this.u != null) {
                    if (this.i.a()) {
                        this.u.setVisibility(0);
                        this.u.setImageDrawable(this.i.d().k());
                    } else {
                        this.u.setVisibility(8);
                    }
                }
                JSONArray c2 = this.i.c();
                if (c2 != null) {
                    C0036u uVar = null;
                    for (int i2 = 0; i2 < c2.length(); i2++) {
                        JSONArray optJSONArray = c2.optJSONArray(i2);
                        if (optJSONArray != null) {
                            int length = optJSONArray.length();
                            if (length == 1) {
                                C0036u a2 = a(linearLayout, optJSONArray.optString(0));
                                if (a2 != null) {
                                    a2.a(i2);
                                    if (a2 instanceof N) {
                                        if (uVar != null) {
                                            uVar.a((N) a2);
                                        }
                                    }
                                    uVar = a2;
                                }
                                a2 = uVar;
                                uVar = a2;
                            } else {
                                LinearLayout linearLayout2 = new LinearLayout(this.a);
                                linearLayout2.setOrientation(0);
                                for (int i3 = 0; i3 < length; i3++) {
                                    C0036u a3 = a(linearLayout2, optJSONArray.optString(i3));
                                    if (a3 != null) {
                                        a3.a(i2);
                                        View h2 = a3.h();
                                        LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) h2.getLayoutParams();
                                        layoutParams2.width = -1;
                                        layoutParams2.height = -1;
                                        layoutParams2.weight = 1.0f;
                                        h2.setLayoutParams(layoutParams2);
                                    }
                                }
                                linearLayout.addView(linearLayout2);
                            }
                        }
                    }
                }
            }
        }
    }

    private C0036u a(ViewGroup viewGroup, String str) {
        if (!TextUtils.isEmpty(str) && this.h.containsKey(str)) {
            C0036u uVar = this.h.get(str);
            uVar.a(this.b);
            if (uVar != null) {
                View h2 = uVar.h();
                ViewParent parent = h2.getParent();
                if (parent != null && (parent instanceof ViewGroup)) {
                    ((ViewGroup) parent).removeView(h2);
                }
                viewGroup.addView(h2);
                return uVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void f() {
        this.p++;
        C0031p.a("MCSliderMenuManager , addFTUERunnable() | mFTUEIntervalArrIndex:" + this.p, 55);
        if (this.p < this.o.length) {
            this.q = new Runnable() {
                public final void run() {
                    if (!av.m(am.this.a).getBoolean("sliderOpenedByUser", false)) {
                        if (!C0018c.a) {
                            am.this.r.d();
                            C0031p.a(am.this.a, "slider", "slider_features", "ftue_shown", new C0038w[0]);
                        }
                        am.this.f();
                    }
                }
            };
            this.r.postDelayed(this.q, (long) this.o[this.p]);
        }
    }

    /* access modifiers changed from: private */
    public static void g() {
        av.b().edit().putBoolean("sliderOpenedByUser", true).commit();
    }

    private static String a(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return sb.toString();
            }
            sb.append(readLine);
        }
    }

    class b extends AsyncTask<Void, Void, String> {
        private b() {
        }

        /* synthetic */ b(am amVar, byte b) {
            this();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return a();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            String str = (String) obj;
            if (!TextUtils.isEmpty(str)) {
                if (am.this.t != null) {
                    am.this.a(str);
                } else {
                    String unused = am.this.e = str;
                }
            }
        }

        private String a() {
            InputStream inputStream;
            boolean z = true;
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(MessageFormat.format("http://developer.mobilecore.com/mobilecore/mobilecore/widgetSliderAppData?appId={0}&appVersion={1}&sdkVer={2}&devId={3}", av.d(am.this.a.getPackageName()), av.d(String.valueOf(am.this.c())), av.d("0.9"), av.d(am.this.c))).openConnection();
                httpURLConnection.setConnectTimeout(20000);
                httpURLConnection.connect();
                long lastModified = httpURLConnection.getLastModified();
                av.b().getLong("configJsonLastModified", 0);
                int responseCode = httpURLConnection.getResponseCode();
                if ((responseCode == 200 || responseCode == 201) && (inputStream = httpURLConnection.getInputStream()) != null) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                    StringWriter stringWriter = new StringWriter();
                    char[] cArr = new char[AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END];
                    while (true) {
                        int read = bufferedReader.read(cArr);
                        if (read == -1) {
                            break;
                        }
                        stringWriter.write(cArr, 0, read);
                    }
                    inputStream.close();
                    String obj = stringWriter.toString();
                    if (!TextUtils.isEmpty(obj)) {
                        try {
                            new JSONObject(obj).getJSONObject("slider");
                        } catch (JSONException e) {
                            z = false;
                        }
                        if (z) {
                            av.b().edit().putLong("configJsonLastModified", lastModified).commit();
                            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(am.this.d, "sliderConfig.json")));
                            bufferedWriter.write(obj);
                            bufferedWriter.close();
                            return obj;
                        }
                    }
                }
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
                am.this.a(e2);
            } catch (IOException e3) {
                e3.printStackTrace();
                am.this.a(e3);
            }
            return null;
        }
    }

    abstract class a {
        protected String a;

        public abstract void a();

        public a(am amVar, String str) {
            this.a = str;
        }
    }

    class d extends a {
        private MCEWidgetTextProperties b;
        private String c;

        public d(String str, MCEWidgetTextProperties mCEWidgetTextProperties, String str2) {
            super(am.this, str);
            this.b = mCEWidgetTextProperties;
            this.c = str2;
        }

        public final void a() {
            am.this.a(this.a, this.b, this.c, false);
        }
    }

    class c extends a {
        private int b;

        public c(String str, int i) {
            super(am.this, str);
            this.b = i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ironsource.mobilcore.am.a(com.ironsource.mobilcore.am, java.lang.String, int, boolean):void
         arg types: [com.ironsource.mobilcore.am, java.lang.String, int, int]
         candidates:
          com.ironsource.mobilcore.am.a(java.lang.String, com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String, boolean):void
          com.ironsource.mobilcore.am.a(com.ironsource.mobilcore.am, java.lang.String, int, boolean):void */
        public final void a() {
            am.this.a(this.a, this.b, false);
        }
    }

    /* access modifiers changed from: private */
    public void a(Exception exc) {
        if (this.a != null && exc != null) {
            av.a(this.a, am.class.getName(), exc);
        }
    }
}
