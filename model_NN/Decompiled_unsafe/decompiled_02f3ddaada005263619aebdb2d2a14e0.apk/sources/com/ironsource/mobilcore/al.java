package com.ironsource.mobilcore;

import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

final class al extends C0037v {
    public al(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "sliderHeader";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject, true);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g = new RelativeLayout(this.c);
        this.g.setPadding(this.d.h(), this.d.j(), this.d.h(), this.d.j());
        this.g.setEnabled(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
      com.ironsource.mobilcore.b.a(android.content.Context, float):int */
    /* access modifiers changed from: protected */
    public final void b() {
        ViewGroup viewGroup = (ViewGroup) this.g;
        this.p = new ImageView(this.c);
        this.p.setId(j());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(15);
        layoutParams.rightMargin = C0017b.a(this.c, 9.0f);
        this.p.setLayoutParams(layoutParams);
        viewGroup.addView(this.p);
        this.m = new TextView(this.c);
        this.m.setBackgroundColor(0);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(1, this.p.getId());
        layoutParams2.addRule(15);
        this.m.setLayoutParams(layoutParams2);
        this.m.setId(j());
        this.m.setSingleLine();
        this.m.setEllipsize(TextUtils.TruncateAt.END);
        this.m.setTypeface(null, 1);
        this.m.setTextSize(2, 18.0f);
        viewGroup.addView(this.m);
        this.d.a(true, this.d.b(), this.m);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:14:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c() {
        /*
            r5 = this;
            r2 = 0
            android.content.Context r0 = r5.c
            android.content.Context r0 = r0.getApplicationContext()
            android.content.pm.PackageManager r3 = r0.getPackageManager()
            android.content.Context r0 = r5.c     // Catch:{ NameNotFoundException -> 0x003d }
            java.lang.String r0 = r0.getPackageName()     // Catch:{ NameNotFoundException -> 0x003d }
            android.graphics.drawable.Drawable r1 = r3.getApplicationIcon(r0)     // Catch:{ NameNotFoundException -> 0x003d }
            android.content.Context r0 = r5.c     // Catch:{ NameNotFoundException -> 0x0043 }
            java.lang.String r0 = r0.getPackageName()     // Catch:{ NameNotFoundException -> 0x0043 }
            r4 = 0
            android.content.pm.ApplicationInfo r2 = r3.getApplicationInfo(r0, r4)     // Catch:{ NameNotFoundException -> 0x0043 }
        L_0x0020:
            if (r1 == 0) goto L_0x0027
            android.widget.ImageView r0 = r5.p
            r0.setImageDrawable(r1)
        L_0x0027:
            if (r2 == 0) goto L_0x003c
            java.lang.CharSequence r0 = r3.getApplicationLabel(r2)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r0 = r0.toUpperCase()
            android.widget.TextView r1 = r5.m
            java.lang.String r0 = r0.toUpperCase()
            r1.setText(r0)
        L_0x003c:
            return
        L_0x003d:
            r0 = move-exception
            r1 = r2
        L_0x003f:
            r0.printStackTrace()
            goto L_0x0020
        L_0x0043:
            r0 = move-exception
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.al.c():void");
    }
}
