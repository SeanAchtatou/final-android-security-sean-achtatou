package com.ironsource.mobilcore;

import com.ironsource.mobilcore.av;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/* renamed from: com.ironsource.mobilcore.g  reason: case insensitive filesystem */
public class C0022g implements av.a {
    private av.c a;

    C0022g(av.c cVar) {
        this.a = cVar;
    }

    public final boolean a(HttpEntity httpEntity) {
        try {
            aH.a(new JSONObject(EntityUtils.toString(httpEntity)), this.a);
            return false;
        } catch (Exception e) {
            av.a(MobileCore.b, C0022g.class.getName(), e);
            return false;
        }
    }

    public final void a(int i) {
        C0031p.a("ResourceFileHandler | processError failed to download resource file with error code " + i, 2);
    }
}
