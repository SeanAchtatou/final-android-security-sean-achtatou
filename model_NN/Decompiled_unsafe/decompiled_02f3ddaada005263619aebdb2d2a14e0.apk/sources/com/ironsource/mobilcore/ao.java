package com.ironsource.mobilcore;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.NinePatch;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

final class ao {
    private Context a;
    private b b;
    private b c;
    private b d;
    private b e;
    private b f;
    private int g;
    private int h;
    private int i;
    private a j;
    private a k;
    private a l;
    private a m;
    private a n;
    private a o;
    private a p;
    private a q;
    private a r;
    private a s;
    private int t;
    private int u;

    public ao(Context context, JSONObject jSONObject) throws JSONException {
        int i2 = 0;
        this.a = context;
        JSONObject optJSONObject = jSONObject.optJSONObject("textColors");
        if (optJSONObject != null) {
            this.b = new b(optJSONObject.optJSONObject("titleText"));
            this.c = new b(optJSONObject.optJSONObject("itemText"));
            this.d = new b(optJSONObject.optJSONObject("textShadow"));
            this.e = new b(optJSONObject.optJSONObject("searchHintColor"));
            this.f = new b(optJSONObject.optJSONObject("searchInputColor"));
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("colors");
        if (optJSONObject2 != null) {
            String optString = optJSONObject2.optString("miniSeparatorTopLineColor", "");
            String optString2 = optJSONObject2.optString("miniSeparatorBottomLineColor", "");
            String optString3 = optJSONObject2.optString("widgetButtonSeparatorColor", "");
            this.g = TextUtils.isEmpty(optString) ? 0 : Color.parseColor(optString);
            this.h = TextUtils.isEmpty(optString2) ? 0 : Color.parseColor(optString2);
            this.i = !TextUtils.isEmpty(optString3) ? Color.parseColor(optString3) : i2;
            if (this.i == 0) {
                this.i = this.g;
            }
        } else {
            this.g = 0;
            this.h = 0;
            this.i = 0;
        }
        JSONObject optJSONObject3 = jSONObject.optJSONObject("images");
        if (optJSONObject3 != null) {
            this.j = new a(optJSONObject3.optJSONObject("sliderHandle"));
            this.k = new a(optJSONObject3.optJSONObject("searchIcon"));
        }
        JSONObject optJSONObject4 = jSONObject.optJSONObject("backgrounds");
        if (optJSONObject4 != null) {
            this.l = new a(optJSONObject4.optJSONObject("sliderBG"));
            this.m = new a(optJSONObject4.optJSONObject("menuTitleBG"));
            this.n = new a(optJSONObject4.optJSONObject("widgetButtonBG"));
            this.o = new a(optJSONObject4.optJSONObject("badgeBG"));
            this.p = new a(optJSONObject4.optJSONObject("ironSourceWidgetsBG"));
            this.q = new a(optJSONObject4.optJSONObject("separatorWidgetsBG"));
            this.r = new a(optJSONObject4.optJSONObject("widgetPressedBG"));
            this.s = new a(optJSONObject4.optJSONObject("searchBG"));
        }
        JSONObject optJSONObject5 = jSONObject.optJSONObject("dimensions");
        if (optJSONObject5 != null) {
            this.t = optJSONObject5.optInt("separatorHeight", 5);
            this.u = optJSONObject5.optInt("miniSeparatorLineHeight", 2);
            return;
        }
        this.t = 5;
        this.u = 2;
    }

    public final b a() {
        return this.b;
    }

    public final b b() {
        return this.c;
    }

    public final int c() {
        return this.e.a(true);
    }

    public final int d() {
        return this.f.a(true);
    }

    public final int e() {
        return this.i;
    }

    public final int f() {
        return this.t;
    }

    public final int g() {
        return this.u;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
      com.ironsource.mobilcore.b.a(android.content.Context, float):int */
    public final int h() {
        return C0017b.a(this.a, 12.0f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
      com.ironsource.mobilcore.b.a(android.content.Context, float):int */
    public final int i() {
        return C0017b.a(this.a, 9.0f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
      com.ironsource.mobilcore.b.a(android.content.Context, float):int */
    public final int j() {
        return C0017b.a(this.a, 6.0f);
    }

    public final Drawable k() {
        return this.j.a(this.a);
    }

    public final Drawable l() {
        return this.k.a(this.a);
    }

    public final Drawable m() {
        return this.l.a(this.a);
    }

    public final Drawable n() {
        return this.m.a(this.a);
    }

    public final Drawable o() {
        return this.n.a(this.a);
    }

    public final Drawable p() {
        return this.o.a(this.a);
    }

    public final Drawable q() {
        return this.p.a(this.a);
    }

    public final Drawable r() {
        return this.q.a(this.a);
    }

    public final Drawable s() {
        return this.r.a(this.a);
    }

    public final Drawable t() {
        return this.s.a(this.a);
    }

    public final void a(boolean z, b bVar, TextView... textViewArr) {
        for (TextView textView : textViewArr) {
            if (textView != null) {
                if (bVar != null) {
                    textView.setTextColor(bVar.a(z));
                }
                if (this.d != null) {
                    textView.setShadowLayer(2.0f, 1.0f, 1.0f, this.d.a(z));
                }
            }
        }
    }

    public final void a(ViewGroup viewGroup) {
        if (this.g != 0) {
            View view = new View(this.a);
            view.setLayoutParams(new LinearLayout.LayoutParams(-1, C0017b.a(this.a, (float) this.u)));
            view.setBackgroundColor(this.g);
            viewGroup.addView(view);
        }
        if (this.h != 0) {
            View view2 = new View(this.a);
            view2.setLayoutParams(new LinearLayout.LayoutParams(-1, C0017b.a(this.a, (float) this.u)));
            view2.setBackgroundColor(this.h);
            viewGroup.addView(view2);
        }
    }

    public static class b {
        private int a;
        private int b;

        public b(JSONObject jSONObject) {
            if (jSONObject != null) {
                this.a = Color.parseColor(jSONObject.optString("normal", ""));
                String optString = jSONObject.optString("pressed", "");
                if (TextUtils.isEmpty(optString)) {
                    this.b = this.a;
                } else {
                    this.b = Color.parseColor(optString);
                }
            }
        }

        public final int a(boolean z) {
            return z ? this.a : this.b;
        }
    }

    static class a {
        private String a;
        private String b;

        public a(JSONObject jSONObject) {
            if (jSONObject != null) {
                this.a = jSONObject.optString("type", "");
                this.b = jSONObject.optString("value", "");
            }
        }

        public final Drawable a(Context context) {
            if (TextUtils.isEmpty(this.b)) {
                return null;
            }
            if (this.a.equals("image")) {
                return new BitmapDrawable(context.getResources(), C0017b.a(context, this.b));
            } else if (this.a.equals("tile")) {
                String str = this.b;
                if (TextUtils.isEmpty(str)) {
                    return null;
                }
                BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), C0017b.a(context, str));
                bitmapDrawable.setTileModeX(Shader.TileMode.REPEAT);
                bitmapDrawable.setTileModeY(Shader.TileMode.REPEAT);
                return bitmapDrawable;
            } else if (this.a.equals("9patch")) {
                Bitmap o = C0017b.o(this.b);
                byte[] ninePatchChunk = o.getNinePatchChunk();
                if (!NinePatch.isNinePatchChunk(ninePatchChunk)) {
                    return null;
                }
                C0032q a2 = C0032q.a(ninePatchChunk);
                System.gc();
                if (ninePatchChunk != null) {
                    return new NinePatchDrawable(o, ninePatchChunk, a2.a, null);
                }
                return null;
            } else if (this.a.equals("color")) {
                return new ColorDrawable(Color.parseColor(this.b));
            } else {
                return null;
            }
        }
    }
}
