package com.ironsource.mobilcore;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class MobileCoreReport extends Service {
    private ArrayList<b> a;
    /* access modifiers changed from: private */
    public LinkedBlockingQueue<a> b;
    private Handler c;
    private int d;
    private Runnable e = new Runnable() {
        public final void run() {
            C0031p.a("MobileCoreReport service , mStopSelfRunnable , run() | called. stopping self", 55);
            MobileCoreReport.this.stopSelf();
        }
    };

    static /* synthetic */ void b(MobileCoreReport mobileCoreReport) {
        if (mobileCoreReport.b.isEmpty() && mobileCoreReport.c() == 0) {
            mobileCoreReport.a(true);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        C0031p.a("MobileCoreReport service onCreate() | called", 55);
        this.b = new LinkedBlockingQueue<>(50);
        this.a = new ArrayList<>(5);
        this.c = new Handler();
        this.d = 0;
        for (int i = 0; i < 5; i++) {
            b bVar = new b();
            this.a.add(bVar);
            bVar.start();
        }
    }

    public void onDestroy() {
        C0031p.a("MobileCoreReport service onDestroy() | called", 55);
        super.onDestroy();
        this.b.clear();
        this.a.clear();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        C0031p.a("MobileCoreReport service , onStartCommand() | startId:" + i2, 55);
        a aVar = new a(i2, intent);
        try {
            a(false);
            this.b.add(aVar);
            return 1;
        } catch (IllegalStateException e2) {
            C0031p.a("MobileCoreReport service , ServiceTask , doJob() | dropping request:" + i2, 55);
            return 1;
        }
    }

    private void a(boolean z) {
        C0031p.a("MobileCoreReport service , addRemoveStopSelfRunnable() | called. doAdd:" + z, 55);
        if (z) {
            this.c.postDelayed(this.e, 5000);
        } else {
            this.c.removeCallbacks(this.e);
        }
    }

    private synchronized int c() {
        return this.d;
    }

    public final synchronized void a() {
        this.d--;
    }

    public final synchronized void b() {
        this.d++;
    }

    public class a {
        private int a;
        private Intent b;

        public a(int i, Intent intent) {
            this.a = i;
            this.b = intent;
        }

        public final void a() {
            if (this.b != null) {
                C0031p.a("MobileCoreReport service , ServiceTask , doJob() | mStartId:" + this.a, 55);
                new aC().a(MobileCoreReport.this, this.b);
            }
        }
    }

    public class b extends Thread {
        public b() {
        }

        public final void run() {
            super.run();
            while (true) {
                try {
                    MobileCoreReport.this.b();
                    ((a) MobileCoreReport.this.b.take()).a();
                    MobileCoreReport.this.a();
                    MobileCoreReport.b(MobileCoreReport.this);
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    }
}
