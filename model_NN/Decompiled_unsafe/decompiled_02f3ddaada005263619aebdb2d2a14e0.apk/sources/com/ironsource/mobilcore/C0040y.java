package com.ironsource.mobilcore;

import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.ironsource.mobilcore.y  reason: case insensitive filesystem */
final class C0040y extends C0037v implements C0041z {
    public C0040y(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "groupHeader";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject, true);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g = new RelativeLayout(this.c);
        this.g.setPadding(this.d.h(), this.d.j(), this.d.h(), this.d.j());
        this.g.setEnabled(false);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        ViewGroup viewGroup = (ViewGroup) this.g;
        n();
        viewGroup.addView(this.o);
        this.m = new TextView(this.c);
        this.m.setBackgroundColor(0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(9);
        layoutParams.addRule(15);
        layoutParams.addRule(0, this.o.getId());
        this.m.setLayoutParams(layoutParams);
        this.m.setId(j());
        this.m.setSingleLine();
        this.m.setEllipsize(TextUtils.TruncateAt.END);
        this.m.setTypeface(null, 1);
        this.m.setTextSize(2, 18.0f);
        viewGroup.addView(this.m);
        this.d.a(true, this.d.a(), this.m, this.o);
        C0017b.a(this.g, this.d.n());
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.m.setText(this.i.toUpperCase());
        if (this.o != null) {
            this.o.setText(this.k);
        }
    }

    public final void p() {
        boolean z;
        int i = 0;
        if (!TextUtils.isEmpty(this.k)) {
            z = true;
        } else {
            z = false;
        }
        TextView textView = this.o;
        if (!z) {
            i = 8;
        }
        textView.setVisibility(i);
    }
}
