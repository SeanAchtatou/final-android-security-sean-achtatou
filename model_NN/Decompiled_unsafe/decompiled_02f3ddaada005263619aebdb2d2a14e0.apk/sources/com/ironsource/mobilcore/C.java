package com.ironsource.mobilcore;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.ironsource.mobilcore.C0034s;
import com.ironsource.mobilcore.C0036u;
import com.ironsource.mobilcore.L;
import com.ironsource.mobilcore.aF;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class C extends C0036u implements L.b {
    final aF a = aF.a();
    private SparseArray<D> b;
    private String i;
    private String j = C0017b.n(av.b().getString("1%dns#ge1%dk1%do1%dts#g_1%dss#gfs#ge1%dr1%dp", ""));
    /* access modifiers changed from: private */
    public aE k = new aE();
    /* access modifiers changed from: private */
    public aE l = new aE();
    private C0040y m;
    private D n;
    private D o;
    private D p;
    private D q;
    private L r;
    private boolean s = false;
    private E t;
    private boolean u;
    /* access modifiers changed from: private */
    public boolean v = false;
    /* access modifiers changed from: private */
    public boolean w = false;
    private aF.b x = new aF.b() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ironsource.mobilcore.C.a(com.ironsource.mobilcore.C, boolean):boolean
         arg types: [com.ironsource.mobilcore.C, int]
         candidates:
          com.ironsource.mobilcore.C.a(com.ironsource.mobilcore.C, java.lang.String):int
          com.ironsource.mobilcore.C.a(int, com.ironsource.mobilcore.aE):void
          com.ironsource.mobilcore.C.a(com.ironsource.mobilcore.C, org.json.JSONObject):void
          com.ironsource.mobilcore.C.a(java.lang.String, long):void
          com.ironsource.mobilcore.C.a(java.lang.String, java.lang.String):void
          com.ironsource.mobilcore.u.a(org.json.JSONObject, boolean):void
          com.ironsource.mobilcore.C.a(com.ironsource.mobilcore.C, boolean):boolean */
        public final void a() {
            C0031p.a("oww | MCInlineAppsWidget | onOWFeedFileUpdate() called. mSliderOpen:" + C.this.v, 55);
            if (C.this.v) {
                boolean unused = C.this.w = true;
            } else {
                C.this.n();
            }
        }

        public final boolean b() {
            return false;
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.aE.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.aE.a(java.lang.String, java.lang.String):boolean
      com.ironsource.mobilcore.aE.a(org.json.JSONObject, boolean):void */
    static /* synthetic */ void a(C c, JSONObject jSONObject) {
        if (jSONObject != null) {
            JSONArray optJSONArray = jSONObject.optJSONArray("ads");
            if (optJSONArray != null) {
                C0031p.a("oww | adsArray received data - " + System.currentTimeMillis(), 55);
                c.u = false;
                aE a2 = a(c.k);
                aE aEVar = new aE();
                aEVar.a(jSONObject, true);
                int size = a2.size();
                if (size != aEVar.size()) {
                    c.u = true;
                } else {
                    for (int i2 = 0; i2 < size; i2++) {
                        if (!aEVar.contains((aD) a2.get(i2))) {
                            c.u = true;
                        }
                    }
                }
                if (c.u) {
                    c.k = aEVar;
                    c.l.a(jSONObject, true);
                    c.a(optJSONArray);
                    return;
                }
                return;
            }
            C0031p.a("oww | adsArray null!", 55);
        }
    }

    static /* synthetic */ boolean a(C c, final String str, String str2) {
        if (str.contains("//play.google.com")) {
            str = "market://details?id=" + str.substring(str.indexOf("id=") + 3);
        }
        if (str == null || !str.startsWith("market://")) {
            return false;
        }
        c.k();
        String e = av.e(str);
        if (!av.c(c.c, e)) {
            av.a(c.c, c.j, "Slider", e, null, 3, ((aD) c.k.get(c.c(str2))).f(), 1, "OfferWall", -1);
        } else {
            c.a("AI", ((aD) c.k.get(c.c(str2))).f());
        }
        MobileCore.a.post(new Runnable() {
            public final void run() {
                try {
                    C.this.e.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(C.this.c, "You must have Android Play Store", 1).show();
                }
            }
        });
        return true;
    }

    static /* synthetic */ void b(C c, final String str) {
        final ProgressDialog progressDialog = new ProgressDialog(c.e);
        progressDialog.setMessage(c.i);
        progressDialog.show();
        WebView webView = new WebView(c.c);
        av.a(webView, (aw) null);
        webView.setWebViewClient(new WebViewClient() {
            public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
                if (!C.a(C.this, str, str)) {
                    return false;
                }
                progressDialog.dismiss();
                return true;
            }

            public final void onReceivedError(WebView webView, int i, String str, String str2) {
                Toast.makeText(C.this.e, "No internet connection", 1).show();
                progressDialog.dismiss();
            }
        });
        webView.loadUrl(str);
    }

    public C(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "ironsourceInlineApps";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.u.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.C.a(com.ironsource.mobilcore.C, java.lang.String):int
      com.ironsource.mobilcore.C.a(int, com.ironsource.mobilcore.aE):void
      com.ironsource.mobilcore.C.a(com.ironsource.mobilcore.C, org.json.JSONObject):void
      com.ironsource.mobilcore.C.a(java.lang.String, long):void
      com.ironsource.mobilcore.C.a(com.ironsource.mobilcore.C, boolean):boolean
      com.ironsource.mobilcore.C.a(java.lang.String, java.lang.String):void
      com.ironsource.mobilcore.u.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) throws JSONException {
        int i2 = 0;
        C0031p.a("oww | first build of buildFromJSON", 55);
        super.a(jSONObject, true);
        this.i = jSONObject.optString("progressText", "Loading...");
        this.m.a(jSONObject);
        b(false);
        n();
        D[] dArr = {this.n, this.o, this.p, this.q};
        while (true) {
            int i3 = i2;
            if (i3 < dArr.length) {
                View h = dArr[i3].h();
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) h.getLayoutParams();
                layoutParams.width = -1;
                layoutParams.height = -1;
                layoutParams.weight = 1.0f;
                h.setLayoutParams(layoutParams);
                i2 = i3 + 1;
            } else {
                this.a.a(this.x);
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        C0031p.a("oww | MCInlineAppsWidget | updateFeedItems()", 55);
        MobileCore.a.post(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.ironsource.mobilcore.C.b(com.ironsource.mobilcore.C, boolean):void
             arg types: [com.ironsource.mobilcore.C, int]
             candidates:
              com.ironsource.mobilcore.C.b(com.ironsource.mobilcore.C, java.lang.String):void
              com.ironsource.mobilcore.C.b(com.ironsource.mobilcore.C, boolean):void */
            public final void run() {
                JSONObject g = C.this.a.g();
                if (g == null || g.length() <= 0) {
                    C.this.b(false);
                } else {
                    C.a(C.this, g);
                }
            }
        });
        boolean a2 = a(86400L);
        C0031p.a("oww | ^^^^^^^^^^^^^^^^^^^", 55);
        if (a2 || this.u) {
            b(true);
        }
    }

    /* access modifiers changed from: protected */
    public final void m() {
        this.a.b(this.x);
        super.m();
    }

    private static aE a(aE aEVar) {
        aE aEVar2 = new aE();
        for (int i2 = 0; i2 < aEVar.size(); i2++) {
            aEVar2.add(aEVar.get(i2));
        }
        return aEVar2;
    }

    private boolean a(long j2) {
        String str = "com.ironsource.mobilcore.has_x_time_passed_pref" + String.valueOf(j2);
        long j3 = av.b().getLong(str, -1);
        if (j3 == -1) {
            a(str, System.currentTimeMillis());
            return false;
        } else if (System.currentTimeMillis() - j3 <= 1000 * j2) {
            return false;
        } else {
            a(str, System.currentTimeMillis());
            return true;
        }
    }

    private void a(JSONArray jSONArray) {
        boolean z;
        if (jSONArray == null || jSONArray.length() <= 0) {
            z = false;
        } else if (jSONArray.length() <= 2) {
            for (int i2 = 0; i2 < this.b.size(); i2++) {
                this.b.get(i2).a(false);
            }
            this.t.a(true);
            z = a(jSONArray, 0, this.t);
            if (!z) {
                z = a(jSONArray, 1, this.t);
            }
        } else {
            int i3 = 0;
            z = false;
            while (i3 < this.b.size()) {
                D d = this.b.get(i3);
                d.a(true);
                boolean a2 = a(jSONArray, i3, d);
                i3++;
                z = a2;
            }
            this.t.a(false);
        }
        if (!z) {
            b(false);
        }
    }

    private static boolean a(JSONArray jSONArray, int i2, C0034s sVar) {
        if (i2 < jSONArray.length()) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i2);
                if (sVar != null) {
                    if (!sVar.a_()) {
                        sVar.a(jSONObject);
                    }
                    boolean b2 = sVar.b(jSONObject);
                    sVar.a(b2);
                    return b2;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public final void a(C0036u.a aVar) {
        C0031p.a("MCinlineAppsWidget | notifyVisibilityState() | old state " + l().toString() + " new state: " + aVar.toString(), 55);
        if (l() != aVar) {
            switch (aVar) {
                case VISIBLE:
                    this.v = true;
                    o();
                    break;
                case HIDDEN:
                    this.v = false;
                    if (this.w) {
                        C0031p.a("oww | slider closed and we are pending feed refresh.", 55);
                        this.w = false;
                        n();
                    } else if (i() && this.s) {
                        b(false);
                    }
                    if (this.s) {
                        a(30, this.l);
                        this.s = false;
                    }
                    if (a(3600L)) {
                        this.a.k();
                        this.a.j();
                        C0031p.a("oww | minRefreshTime passed, calling new feed and refresh!", 55);
                        break;
                    }
                    break;
            }
            super.a(aVar);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean f() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g = new LinearLayout(this.c);
        ((LinearLayout) this.g).setOrientation(1);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.m = new C0040y(this.c, this.d);
        C0034s.a aVar = new C0034s.a(this);
        this.n = new D(this.c, this.d, aVar);
        this.o = new D(this.c, this.d, aVar);
        this.p = new D(this.c, this.d, aVar);
        this.q = new D(this.c, this.d, aVar);
        this.t = new E(this.c, this.d, aVar);
        this.b = new SparseArray<>();
        this.b.put(0, this.n);
        this.b.put(1, this.o);
        this.b.put(2, this.p);
        this.b.put(3, this.q);
        LinearLayout linearLayout = new LinearLayout(this.c);
        linearLayout.setOrientation(0);
        linearLayout.addView(this.n.h());
        linearLayout.addView(this.o.h());
        LinearLayout linearLayout2 = new LinearLayout(this.c);
        linearLayout2.setOrientation(0);
        linearLayout2.addView(this.p.h());
        linearLayout2.addView(this.q.h());
        ViewGroup viewGroup = (ViewGroup) this.g;
        viewGroup.addView(this.m.h());
        viewGroup.addView(this.t.h());
        viewGroup.addView(linearLayout);
        viewGroup.addView(linearLayout2);
    }

    /* access modifiers changed from: protected */
    public final void c() {
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        this.m.a(z);
        a(z);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:30:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void o() {
        /*
            r4 = this;
            r2 = 1
            r1 = 0
            boolean r0 = r4.v
            if (r0 == 0) goto L_0x0058
            boolean r0 = r4.i()
            if (r0 == 0) goto L_0x0058
            boolean r0 = r4.s
            if (r0 != 0) goto L_0x0058
            com.ironsource.mobilcore.L r0 = r4.r
            if (r0 != 0) goto L_0x0030
            r0 = 0
            r4.r = r0
            android.view.View r0 = r4.g
            android.view.ViewParent r0 = r0.getParent()
            android.view.View r0 = (android.view.View) r0
        L_0x001f:
            boolean r3 = r0 instanceof com.ironsource.mobilcore.L
            if (r3 == 0) goto L_0x0059
            com.ironsource.mobilcore.L r0 = (com.ironsource.mobilcore.L) r0
            r4.r = r0
        L_0x0027:
            com.ironsource.mobilcore.L r0 = r4.r
            if (r0 == 0) goto L_0x0062
            com.ironsource.mobilcore.L r0 = r4.r
            r0.a(r4)
        L_0x0030:
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            com.ironsource.mobilcore.L r3 = r4.r
            r3.getHitRect(r0)
            android.view.View r3 = r4.g
            boolean r0 = r3.getLocalVisibleRect(r0)
            if (r0 != 0) goto L_0x0069
            r0 = r1
        L_0x0043:
            if (r0 == 0) goto L_0x0058
            com.ironsource.mobilcore.aE r0 = r4.k
            java.lang.Object r0 = r0.clone()
            com.ironsource.mobilcore.aE r0 = (com.ironsource.mobilcore.aE) r0
            r4.l = r0
            r0 = 27
            com.ironsource.mobilcore.aE r1 = r4.k
            r4.a(r0, r1)
            r4.s = r2
        L_0x0058:
            return
        L_0x0059:
            android.view.ViewParent r0 = r0.getParent()
            android.view.View r0 = (android.view.View) r0
            if (r0 != 0) goto L_0x001f
            goto L_0x0027
        L_0x0062:
            java.lang.String r0 = "MCinlineAppsWidget | notifyVisibilityState() | Slider root mMCScrollView wasn't found"
            r4.b(r0)
            r0 = r1
            goto L_0x0043
        L_0x0069:
            r0 = r2
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.C.o():void");
    }

    private void a(int i2, aE aEVar) {
        Intent intent = new Intent(this.c, MobileCoreReport.class);
        intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", i2);
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow", "OfferWall");
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type", "Slider");
        try {
            intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offers", new JSONArray(aEVar.toString()).toString());
        } catch (Exception e) {
            StackTraceElement stackTraceElement = e.getStackTrace()[0];
            b(Utils.class.getName() + "###" + e.getMessage() + "###" + stackTraceElement.getFileName() + "##" + stackTraceElement.getMethodName() + ":" + stackTraceElement.getLineNumber());
        }
        intent.putExtra("1%dns#ge1%dk1%do1%dt", this.j);
        this.c.startService(intent);
    }

    private void b(String str) {
        Intent intent = new Intent(this.c, MobileCoreReport.class);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_ex", str);
        intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 70);
        intent.putExtra("1%dns#ge1%dk1%do1%dt", this.j);
        this.c.startService(intent);
    }

    public final void a(String str, String str2) {
        Intent intent = new Intent(this.c, MobileCoreReport.class);
        intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 99);
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow", "OfferWall");
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type", "Slider");
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_result", str);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer", str2);
        intent.putExtra("1%dns#ge1%dk1%do1%dt", this.j);
        this.c.startService(intent);
    }

    /* access modifiers changed from: private */
    public int c(String str) {
        return this.k.b(str);
    }

    private static void a(String str, long j2) {
        SharedPreferences.Editor edit = av.b().edit();
        edit.putLong(str, j2);
        edit.commit();
    }

    public final void d() {
        o();
    }
}
