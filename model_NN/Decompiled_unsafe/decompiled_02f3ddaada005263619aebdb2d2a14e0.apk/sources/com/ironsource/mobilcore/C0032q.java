package com.ironsource.mobilcore;

import android.graphics.Rect;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/* renamed from: com.ironsource.mobilcore.q  reason: case insensitive filesystem */
final class C0032q {
    public Rect a = new Rect();
    private int[] b;
    private int[] c;
    private int[] d;

    private C0032q() {
    }

    private static void a(int[] iArr, ByteBuffer byteBuffer) {
        int length = iArr.length;
        for (int i = 0; i < length; i++) {
            iArr[i] = byteBuffer.getInt();
        }
    }

    private static void a(int i) {
        if (i == 0 || (i & 1) != 0) {
            throw new RuntimeException("invalid nine-patch: " + i);
        }
    }

    public static C0032q a(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.nativeOrder());
        if (order.get() == 0) {
            return null;
        }
        C0032q qVar = new C0032q();
        qVar.b = new int[order.get()];
        qVar.c = new int[order.get()];
        qVar.d = new int[order.get()];
        a(qVar.b.length);
        a(qVar.c.length);
        order.getInt();
        order.getInt();
        qVar.a.left = order.getInt();
        qVar.a.right = order.getInt();
        qVar.a.top = order.getInt();
        qVar.a.bottom = order.getInt();
        order.getInt();
        a(qVar.b, order);
        a(qVar.c, order);
        a(qVar.d, order);
        return qVar;
    }
}
