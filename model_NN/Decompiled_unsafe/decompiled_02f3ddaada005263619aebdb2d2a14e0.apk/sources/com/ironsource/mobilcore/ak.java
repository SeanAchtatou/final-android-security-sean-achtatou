package com.ironsource.mobilcore;

import android.app.Activity;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;

abstract class ak extends U {
    ak(Activity activity, int i) {
        super(activity, i);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        if (mode == 1073741824 && mode2 == 1073741824) {
            int size = View.MeasureSpec.getSize(i);
            int size2 = View.MeasureSpec.getSize(i2);
            if (!this.v) {
                this.u = (int) (((float) size2) * 0.25f);
            }
            if (this.c == -1.0f) {
                c((float) this.u);
            }
            this.s.measure(getChildMeasureSpec(i, 0, size), getChildMeasureSpec(i, 0, this.u));
            this.t.measure(getChildMeasureSpec(i, 0, size), getChildMeasureSpec(i, 0, size2));
            setMeasuredDimension(size, size2);
            f();
            return;
        }
        throw new IllegalStateException("Must measure with an exact size");
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int i;
        int action = motionEvent.getAction() & MotionEventCompat.ACTION_MASK;
        if (action == 0 && this.w && j()) {
            c(0.0f);
            g();
            i();
            e(0);
        }
        if (this.w && a(motionEvent)) {
            return true;
        }
        if (this.A == 0) {
            return false;
        }
        if (action != 0 && this.d) {
            return true;
        }
        switch (action) {
            case 0:
                float x = motionEvent.getX();
                this.e = x;
                this.g = x;
                float y = motionEvent.getY();
                this.f = y;
                this.h = y;
                if (b()) {
                    if (this.w) {
                        i = 8;
                    } else {
                        i = 0;
                    }
                    e(i);
                    g();
                    i();
                    this.d = false;
                    break;
                }
                break;
            case 1:
            case 3:
                if (Math.abs(this.c) <= ((float) (this.u / 2))) {
                    b(true);
                    break;
                } else {
                    a(true);
                    break;
                }
            case 2:
                float x2 = motionEvent.getX();
                float abs = Math.abs(x2 - this.g);
                float y2 = motionEvent.getY();
                float f = y2 - this.h;
                float abs2 = Math.abs(f);
                if (abs2 > ((float) this.b) && abs2 > abs && a(f)) {
                    e(2);
                    this.d = true;
                    this.g = x2;
                    this.h = y2;
                    break;
                }
        }
        if (this.j == null) {
            this.j = VelocityTracker.obtain();
        }
        this.j.addMovement(motionEvent);
        return this.d;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f;
        if (!this.w && this.A == 0) {
            return false;
        }
        int action = motionEvent.getAction() & MotionEventCompat.ACTION_MASK;
        if (this.j == null) {
            this.j = VelocityTracker.obtain();
        }
        this.j.addMovement(motionEvent);
        switch (action) {
            case 0:
                float x = motionEvent.getX();
                this.e = x;
                this.g = x;
                float y = motionEvent.getY();
                this.f = y;
                this.h = y;
                if (b()) {
                    g();
                    i();
                    e();
                    break;
                }
                break;
            case 1:
            case 3:
                b(motionEvent);
                break;
            case 2:
                if (!this.d) {
                    float abs = Math.abs(motionEvent.getX() - this.g);
                    float y2 = motionEvent.getY();
                    float f2 = y2 - this.h;
                    float abs2 = Math.abs(f2);
                    if (abs2 > ((float) this.b) && abs2 > abs && a(f2)) {
                        e(2);
                        this.d = true;
                        if (y2 - this.f > 0.0f) {
                            f = this.f + ((float) this.b);
                        } else {
                            f = this.f - ((float) this.b);
                        }
                        this.h = f;
                    }
                }
                if (this.d) {
                    e();
                    float y3 = motionEvent.getY();
                    this.h = y3;
                    b(y3 - this.h);
                    break;
                }
                break;
        }
        return true;
    }
}
