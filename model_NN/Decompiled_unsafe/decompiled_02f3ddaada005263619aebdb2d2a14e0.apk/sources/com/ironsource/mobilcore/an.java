package com.ironsource.mobilcore;

import android.app.Activity;

final class an implements MCISliderAPI {
    an() {
    }

    public final void setContentViewWithSlider(Activity activity, int i) {
        am.a().setContentViewWithSlider(activity, i);
    }

    public final void setContentViewWithSlider(Activity activity, int i, int i2) {
        am.a().setContentViewWithSlider(activity, i, i2);
    }

    public final void setWidgetTextProperty(String str, MCEWidgetTextProperties mCEWidgetTextProperties, String str2) {
        am.a().setWidgetTextProperty(str, mCEWidgetTextProperties, str2);
    }

    public final String getWidgetTextProperty(String str, MCEWidgetTextProperties mCEWidgetTextProperties) {
        return am.a().getWidgetTextProperty(str, mCEWidgetTextProperties);
    }

    public final void setWidgetIconResource(String str, int i) {
        am.a().setWidgetIconResource(str, i);
    }

    public final void openSliderMenu(boolean z) {
        am.a().openSliderMenu(z);
    }

    public final void closeSliderMenu(boolean z) {
        am.a().closeSliderMenu(z);
    }

    public final void toggleSliderMenu(boolean z) {
        am.a().toggleSliderMenu(z);
    }

    public final void showWidget(String str) {
        am.a().showWidget(str);
    }

    public final void hideWidget(String str) {
        am.a().hideWidget(str);
    }

    public final void setEmphasizedWidget(String str) {
        am.a().setEmphasizedWidget(str);
    }

    public final boolean isSliderMenuOpen() {
        return am.a().isSliderMenuOpen();
    }
}
