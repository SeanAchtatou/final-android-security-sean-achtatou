package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import com.ironsource.mobilcore.CallbackResponse;

/* renamed from: com.ironsource.mobilcore.c  reason: case insensitive filesystem */
final class C0018c extends aB {
    public static boolean a = false;
    /* access modifiers changed from: private */
    public CallbackResponse c;
    private String d;
    private String e;
    /* access modifiers changed from: private */
    public Dialog f;

    protected C0018c(Activity activity, String str, String str2, CallbackResponse callbackResponse) {
        super(activity);
        this.e = str;
        this.d = str2;
        this.c = callbackResponse;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        RelativeLayout relativeLayout = new RelativeLayout(this.b);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        relativeLayout.setLayoutParams(layoutParams);
        relativeLayout.setBackgroundColor(0);
        WebView webView = new WebView(this.b);
        WebView webView2 = new WebView(this.b);
        a(webView2);
        a(webView);
        webView.setWebViewClient(new WebViewClient() {
            public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                super.onPageStarted(webView, str, bitmap);
            }

            public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
                if (str.contains("accept")) {
                    C0018c.this.a("1%dt1%dns#ge1%dms#ges#ge1%drs#ggs#gas#g_1%dn1%dos#gcs#gi", true);
                    C0018c.this.a("1%dt1%dns#ge1%dms#ges#ge1%drs#ggs#gas#g_s#gds#ge1%dw1%dos#gh1%dss#g_1%dss#gfs#ge1%dr1%dp", true);
                    if (C0018c.this.c != null) {
                        C0018c.this.c.onConfirmation(CallbackResponse.TYPE.AGREEMENT_AGREE);
                    }
                    C0018c.this.f.dismiss();
                } else if (str.contains("decline")) {
                    C0018c.this.a("1%dt1%dns#ge1%dms#ges#ge1%drs#ggs#gas#g_1%dn1%dos#gcs#gi", false);
                    C0018c.this.a("1%dt1%dns#ge1%dms#ges#ge1%drs#ggs#gas#g_s#gds#ge1%dw1%dos#gh1%dss#g_1%dss#gfs#ge1%dr1%dp", true);
                    if (C0018c.this.c != null) {
                        C0018c.this.c.onConfirmation(CallbackResponse.TYPE.AGREEMENT_DECLINE);
                    }
                    C0018c.this.f.dismiss();
                }
                return true;
            }
        });
        webView.setId(556655);
        ((RelativeLayout.LayoutParams) webView.getLayoutParams()).addRule(12, -1);
        ((RelativeLayout.LayoutParams) webView2.getLayoutParams()).addRule(2, 556655);
        webView.loadDataWithBaseURL("http://iron/", this.d, "text/html", "utf-8", null);
        relativeLayout.addView(webView);
        webView2.loadDataWithBaseURL("http://iron/", this.e, "text/html", "utf-8", null);
        relativeLayout.addView(webView2);
        relativeLayout.setLayoutParams(layoutParams);
        relativeLayout.invalidate();
        a(relativeLayout);
        this.f = b();
        if (this.f == null) {
            av.a(MobileCore.b, getClass().getName(), "Failed to get dialog.");
            return;
        }
        a = true;
        WindowManager.LayoutParams attributes = this.f.getWindow().getAttributes();
        attributes.width = -2;
        attributes.height = -2;
        attributes.gravity = 17;
        this.f.getWindow().setBackgroundDrawableResource(17170445);
        this.f.getWindow().setAttributes(attributes);
        this.f.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public final void onCancel(DialogInterface dialogInterface) {
                if (C0018c.this.c != null) {
                    C0018c.this.c.onConfirmation(CallbackResponse.TYPE.AGREEMENT_BACK);
                }
            }
        });
        this.f.setOnDismissListener(new DialogInterface.OnDismissListener(this) {
            public final void onDismiss(DialogInterface dialogInterface) {
                C0018c.a = false;
            }
        });
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private static void a(WebView webView) {
        webView.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportMultipleWindows(false);
        webView.clearCache(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setSupportZoom(false);
        webView.setInitialScale(100);
        webView.setHorizontalScrollBarEnabled(false);
    }
}
