package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;

@SuppressLint({"NewApi"})
abstract class U extends Y {
    private static final Interpolator C = new C0008aa();
    private static final Interpolator D = new C0014ag();
    protected static final Interpolator a = new AccelerateInterpolator();
    private Runnable E = new Runnable() {
        public final void run() {
            U.this.m();
        }
    };
    private final Runnable F = new Runnable() {
        public final void run() {
            U.this.l();
        }
    };
    private long G;
    private int H;
    private Runnable I;
    private C0012ae J;
    private boolean K;
    protected int b;
    protected float c;
    protected boolean d;
    protected float e;
    protected float f;
    protected float g = -1.0f;
    protected float h = -1.0f;
    protected C0012ae i;
    protected VelocityTracker j;
    protected int k;
    protected boolean l = true;

    /* access modifiers changed from: protected */
    public abstract void a();

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas, int i2);

    /* access modifiers changed from: protected */
    public abstract boolean a(float f2);

    /* access modifiers changed from: protected */
    public abstract boolean a(MotionEvent motionEvent);

    /* access modifiers changed from: protected */
    public abstract void b(float f2);

    /* access modifiers changed from: protected */
    public abstract void b(int i2);

    /* access modifiers changed from: protected */
    public abstract void b(Canvas canvas, int i2);

    /* access modifiers changed from: protected */
    public abstract void b(MotionEvent motionEvent);

    /* access modifiers changed from: protected */
    public abstract boolean b();

    U(Activity activity, int i2) {
        super(activity, i2);
    }

    /* access modifiers changed from: protected */
    public final void a(Context context, AttributeSet attributeSet) {
        super.a(context, attributeSet);
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.b = viewConfiguration.getScaledTouchSlop();
        this.k = viewConfiguration.getScaledMaximumFlingVelocity();
        this.J = new C0012ae(context, D);
        this.i = new C0012ae(context, C);
        this.H = d(3);
    }

    public final void c(boolean z) {
        if (this.x == 8 || this.x == 4) {
            b(z);
        } else if (this.x == 0 || this.x == 1) {
            a(z);
        }
    }

    public final boolean c() {
        return this.w;
    }

    public final void c(int i2) {
        this.u = i2;
        this.v = true;
        if (this.x == 8 || this.x == 4) {
            c((float) this.u);
        }
        requestLayout();
        invalidate();
    }

    public final void d() {
        if (5000 < 0) {
            throw new IllegalArgumentException("startDelay must be zero or larger.");
        } else if (10000 < 0) {
            throw new IllegalArgumentException("delay must be zero or larger");
        } else {
            removeCallbacks(this.E);
            removeCallbacks(this.I);
            this.J.a(0, 0, this.u / 3, 0, 500);
            e();
            l();
            this.G = 0;
            this.I = new Runnable() {
                public final void run() {
                    U.this.h();
                }
            };
            postDelayed(this.I, 500);
        }
    }

    /* access modifiers changed from: protected */
    public final void c(float f2) {
        int i2 = (int) this.c;
        int i3 = (int) f2;
        this.c = f2;
        if (i3 != i2) {
            b(i3);
            this.w = i3 != 0;
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        if (m && this.B && !this.K) {
            this.K = true;
        }
    }

    private void k() {
        if (this.K) {
            this.K = false;
            this.t.setLayerType(0, null);
            this.s.setLayerType(0, null);
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (this.A == 1) {
            this.z = this.y;
        } else if (this.A == 2) {
            this.z = getMeasuredWidth();
        } else {
            this.z = 0;
        }
    }

    /* access modifiers changed from: protected */
    public final void g() {
        removeCallbacks(this.F);
        this.J.e();
        k();
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3, boolean z) {
        int abs;
        int i4 = 0;
        this.d = false;
        if (this.j != null) {
            this.j.recycle();
            this.j = null;
        }
        i();
        int i5 = (int) this.c;
        int i6 = i2 - i5;
        if (i6 == 0 || !z) {
            c((float) i2);
            if (i2 != 0) {
                i4 = 8;
            }
            e(i4);
            k();
            return;
        }
        int abs2 = Math.abs(i3);
        if (abs2 > 0) {
            abs = Math.round(Math.abs(((float) i6) / ((float) abs2)) * 1000.0f) * 4;
        } else {
            abs = (int) (600.0f * Math.abs(((float) i6) / ((float) this.u)));
        }
        int min = Math.min(abs, 600);
        if (i6 > 0) {
            e(4);
            this.J.a(i5, 0, i6, 0, min);
        } else {
            e(1);
            this.J.a(i5, 0, i6, 0, min);
        }
        e();
        l();
    }

    /* access modifiers changed from: private */
    public void l() {
        if (this.J.d()) {
            int i2 = (int) this.c;
            int b2 = this.J.b();
            if (b2 != i2) {
                c((float) b2);
            }
            if (b2 != this.J.c()) {
                postOnAnimation(this.F);
                return;
            }
        }
        this.J.e();
        int c2 = this.J.c();
        c((float) c2);
        e(c2 == 0 ? 0 : 8);
        k();
    }

    /* access modifiers changed from: protected */
    public final void h() {
        a();
        e();
        m();
    }

    /* access modifiers changed from: private */
    public void m() {
        if (this.i.d()) {
            int i2 = (int) this.c;
            int b2 = this.i.b();
            if (b2 != i2) {
                c((float) b2);
            }
            if (!this.i.a()) {
                postOnAnimation(this.E);
                return;
            } else if (0 > 0) {
                this.I = new Runnable() {
                    public final void run() {
                        U.this.h();
                    }
                };
                postDelayed(this.I, 0);
            }
        }
        this.i.e();
        c(0.0f);
        e(0);
        k();
    }

    /* access modifiers changed from: protected */
    public final void i() {
        removeCallbacks(this.I);
        removeCallbacks(this.E);
        k();
    }

    public void postOnAnimation(Runnable runnable) {
        if (Build.VERSION.SDK_INT >= 16) {
            super.postOnAnimation(runnable);
        } else {
            postDelayed(runnable, 16);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean j() {
        return Math.abs(this.c) <= ((float) this.H);
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        int i2 = (int) this.c;
        if (i2 != 0) {
            b(canvas, i2);
        }
        if (this.o) {
            a(canvas, i2);
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        a aVar = new a(super.onSaveInstanceState());
        aVar.a = this.x == 8 || this.x == 4;
        return aVar;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        a aVar = (a) parcelable;
        super.onRestoreInstanceState(aVar.getSuperState());
        c(aVar.a ? (float) this.u : 0.0f);
        this.x = aVar.a ? 8 : 0;
    }

    static class a extends View.BaseSavedState {
        boolean a;

        public a(Parcelable parcelable) {
            super(parcelable);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Parcel parcel) {
            super(parcel);
            boolean z = true;
            this.a = parcel.readInt() != 1 ? false : z;
        }

        public final void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a ? 1 : 0);
        }

        static {
            new Parcelable.Creator<a>() {
                public final /* synthetic */ Object createFromParcel(Parcel parcel) {
                    return new a(parcel);
                }

                public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
                    return new a[i];
                }
            };
        }
    }
}
