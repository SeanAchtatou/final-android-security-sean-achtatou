package com.ironsource.mobilcore;

import com.ironsource.mobilcore.av;
import java.io.File;
import org.apache.http.HttpEntity;

/* renamed from: com.ironsource.mobilcore.f  reason: case insensitive filesystem */
public final class C0021f extends C0023h {
    ay a;

    C0021f(ay ayVar, String str, String str2, av.b bVar) {
        super(str, str2, null);
        this.a = ayVar;
    }

    public final boolean a(HttpEntity httpEntity) {
        boolean z = false;
        File b = super.b(httpEntity);
        if (b == null) {
            C0031p.a("processFile | failed to get flow file from web. will use fallback", 55);
            return false;
        }
        MobileCore.a.post(new Runnable() {
            public final void run() {
                String str = "file://" + C0021f.this.c + "/" + C0021f.this.b;
                C0031p.a("Offerwall | runFlowHtmlFromPath | localUrl: " + str, 55);
                C0021f.this.a.loadUrl(str);
            }
        });
        if (this.d != null) {
            av.b bVar = this.d;
            String str = this.b;
            if (b != null) {
                z = true;
            }
            bVar.a(str, z);
        }
        return true;
    }
}
