package com.ironsource.mobilcore;

import android.util.Pair;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.json.JSONArray;
import org.json.JSONObject;

class ax extends WebChromeClient {
    private WebView a;
    private a b;
    private b c;

    public interface a {
    }

    public ax(WebView webView, a aVar) {
        this.a = webView;
        a(aVar);
    }

    public ax(WebView webView) {
        this.a = webView;
    }

    public final void a(a aVar) {
        this.b = aVar;
        this.c = new b(this, (byte) 0);
        this.c.a(Arrays.asList(this.b.getClass().getMethods()));
    }

    public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        jsPromptResult.confirm();
        Method method = null;
        int i = 0;
        while (true) {
            try {
                if (i >= this.c.size()) {
                    break;
                } else if (str2.equals(((Method) ((Pair) this.c.get(i)).first).getName())) {
                    method = (Method) ((Pair) this.c.get(i)).first;
                    break;
                } else {
                    i++;
                }
            } catch (Exception e) {
                C0031p.a("Error calling flow " + e.getLocalizedMessage(), 2);
                av.a(webView.getContext(), ax.class.getCanonicalName(), "Error invoking Method: " + str2 + " params: " + str3);
                return true;
            }
        }
        C0031p.a("values: " + str3, 55);
        JSONObject jSONObject = new JSONObject(str3);
        JSONObject optJSONObject = jSONObject.optJSONObject("callback");
        JSONArray optJSONArray = jSONObject.optJSONArray("params");
        Object[] objArr = new Object[optJSONArray.length()];
        for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
            objArr[i2] = optJSONArray.opt(i2);
        }
        if (method == null) {
            return true;
        }
        Object invoke = method.invoke(this.b, objArr);
        if (optJSONObject == null) {
            return true;
        }
        String optString = optJSONObject.optString("method");
        String str4 = "";
        if (invoke != null) {
            str4 = invoke.toString();
        }
        C0031p.a("before calling webview callback " + optString + " ans is: " + str4, 55);
        C0031p.a("javascript:" + optString + "(" + str4 + ");", 55);
        this.a.loadUrl("javascript:" + optString + "(" + str4 + ");");
        return true;
    }

    class b extends ArrayList<Pair<Method, ArrayList<String>>> {
        private static final long serialVersionUID = 1;

        private b(ax axVar) {
        }

        /* synthetic */ b(ax axVar, byte b) {
            this(axVar);
        }

        public final void a(Collection<? extends Method> collection) {
            for (Method method : collection) {
                ArrayList arrayList = new ArrayList();
                for (Class<?> name : method.getParameterTypes()) {
                    arrayList.add(name.getName());
                }
                add(new Pair(method, arrayList));
            }
        }
    }
}
