package com.ironsource.mobilcore;

import android.app.Activity;
import android.content.Context;
import android.webkit.WebView;

final class aw extends ax {
    private Utils a;

    public aw(String str, Context context, String str2, WebView webView) {
        super(webView);
        a(str, context, webView, null);
    }

    public aw(String str, Context context, String str2, WebView webView, CallbackResponse callbackResponse) {
        super(webView);
        a(str, context, webView, callbackResponse);
    }

    public final Utils a() {
        return this.a;
    }

    private void a(String str, Context context, WebView webView, CallbackResponse callbackResponse) {
        this.a = new Utils(str, context, webView, callbackResponse);
        a(this.a);
    }

    /* access modifiers changed from: protected */
    public final void a(Activity activity) {
        this.a.setActivity(activity);
    }
}
