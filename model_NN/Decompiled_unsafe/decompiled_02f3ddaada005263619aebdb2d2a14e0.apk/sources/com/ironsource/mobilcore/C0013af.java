package com.ironsource.mobilcore;

import android.view.animation.Interpolator;

/* renamed from: com.ironsource.mobilcore.af  reason: case insensitive filesystem */
final class C0013af implements Interpolator {
    C0013af() {
    }

    public final float getInterpolation(float f) {
        return (float) ((Math.sin((((double) f) * 3.141592653589793d) - 1.5707963267948966d) * 0.5d) + 0.5d);
    }
}
