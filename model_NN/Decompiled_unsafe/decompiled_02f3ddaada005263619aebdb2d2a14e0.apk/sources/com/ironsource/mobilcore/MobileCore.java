package com.ironsource.mobilcore;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import com.ironsource.mobilcore.CallbackResponse;
import com.ironsource.mobilcore.aF;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class MobileCore {
    protected static Handler a;
    protected static Context b;
    private static Intent c;
    private static an d;
    private static String e;

    public enum LOG_TYPE {
        DEBUG,
        PRODUCTION
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x007c A[Catch:{ Exception -> 0x013c }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00cd A[Catch:{ Exception -> 0x013c }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x01d0 A[SYNTHETIC, Splitter:B:23:0x01d0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void init(android.app.Activity r7, java.lang.String r8, com.ironsource.mobilcore.MobileCore.LOG_TYPE r9) {
        /*
            r5 = 70
            r6 = 55
            r3 = 32
            r4 = 0
            android.content.SharedPreferences r0 = com.ironsource.mobilcore.av.l(r7)
            android.os.Handler r1 = new android.os.Handler
            r1.<init>()
            com.ironsource.mobilcore.MobileCore.a = r1
            android.content.Context r1 = r7.getApplicationContext()
            com.ironsource.mobilcore.MobileCore.b = r1
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            java.lang.String r2 = "android.intent.action.MAIN"
            r1.setAction(r2)
            java.lang.String r2 = "android.intent.category.HOME"
            r1.addCategory(r2)
            android.content.Context r2 = com.ironsource.mobilcore.MobileCore.b
            android.content.pm.PackageManager r2 = r2.getPackageManager()
            android.content.pm.ResolveInfo r1 = r2.resolveActivity(r1, r4)
            if (r1 == 0) goto L_0x0047
            android.content.pm.ActivityInfo r2 = r1.activityInfo
            if (r2 == 0) goto L_0x0047
            android.content.pm.ActivityInfo r1 = r1.activityInfo
            java.lang.String r1 = r1.packageName
            com.ironsource.mobilcore.MobileCore.e = r1
            if (r1 == 0) goto L_0x0047
            java.lang.String r1 = com.ironsource.mobilcore.MobileCore.e
            java.lang.String r1 = r1.toLowerCase()
            com.ironsource.mobilcore.MobileCore.e = r1
        L_0x0047:
            android.content.Intent r1 = new android.content.Intent
            java.lang.Class<com.ironsource.mobilcore.MobileCoreReport> r2 = com.ironsource.mobilcore.MobileCoreReport.class
            r1.<init>(r7, r2)
            com.ironsource.mobilcore.MobileCore.c = r1
            java.lang.String r2 = "1%dns#ge1%dk1%do1%dt"
            r1.putExtra(r2, r8)
            android.content.Intent r1 = com.ironsource.mobilcore.MobileCore.c
            java.lang.String r2 = "s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr"
            r1.putExtra(r2, r5)
            if (r9 == 0) goto L_0x0135
            boolean r1 = r9 instanceof com.ironsource.mobilcore.MobileCore.LOG_TYPE     // Catch:{ Exception -> 0x013c }
            if (r1 == 0) goto L_0x0135
            com.ironsource.mobilcore.C0031p.a(r9)     // Catch:{ Exception -> 0x013c }
        L_0x0065:
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x013c }
            java.math.BigInteger r1 = new java.math.BigInteger     // Catch:{ Exception -> 0x013c }
            r2 = 36
            r1.<init>(r8, r2)     // Catch:{ Exception -> 0x013c }
            r2 = 16
            java.lang.String r1 = r1.toString(r2)     // Catch:{ Exception -> 0x013c }
            int r2 = r1.length()     // Catch:{ Exception -> 0x013c }
            if (r2 <= r3) goto L_0x01d0
            r2 = 32
            java.lang.String r1 = r1.substring(r2)     // Catch:{ Exception -> 0x013c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013c }
            java.lang.String r3 = "Account name in init. "
            r2.<init>(r3)     // Catch:{ Exception -> 0x013c }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x013c }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x013c }
            r3 = 55
            com.ironsource.mobilcore.C0031p.a(r2, r3)     // Catch:{ Exception -> 0x013c }
            java.lang.String r2 = "s#ge1%dms#ga1%dns#g_1%dt1%dn1%du1%dos#gcs#gcs#gas#g_1%dss#gfs#ge1%dr1%dp"
            java.lang.String r1 = com.ironsource.mobilcore.C0017b.m(r1)     // Catch:{ Exception -> 0x013c }
            r0.putString(r2, r1)     // Catch:{ Exception -> 0x013c }
        L_0x009f:
            java.lang.String r1 = "1%dns#ge1%dk1%do1%dts#g_1%dss#gfs#ge1%dr1%dp"
            java.lang.String r2 = com.ironsource.mobilcore.C0017b.m(r8)     // Catch:{ Exception -> 0x013c }
            r0.putString(r1, r2)     // Catch:{ Exception -> 0x013c }
            r0.commit()     // Catch:{ Exception -> 0x013c }
            android.content.SharedPreferences r0 = com.ironsource.mobilcore.av.b()     // Catch:{ Exception -> 0x013c }
            java.lang.String r1 = "prefs_first_run"
            java.lang.String r2 = "true"
            java.lang.String r0 = r0.getString(r1, r2)     // Catch:{ Exception -> 0x013c }
            android.content.SharedPreferences r1 = com.ironsource.mobilcore.av.b()     // Catch:{ Exception -> 0x013c }
            java.lang.String r2 = "prefs_first_run1.0"
            java.lang.String r3 = "true"
            java.lang.String r1 = r1.getString(r2, r3)     // Catch:{ Exception -> 0x013c }
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x013c }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x013c }
            if (r1 == 0) goto L_0x010f
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x013c }
            java.lang.Class<com.ironsource.mobilcore.MobileCoreReport> r2 = com.ironsource.mobilcore.MobileCoreReport.class
            r1.<init>(r7, r2)     // Catch:{ Exception -> 0x013c }
            java.lang.String r2 = "s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr"
            r3 = 34
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x013c }
            java.lang.String r2 = "com.ironsource.mobilcore.MobileCoreReport_extra_first_run"
            r1.putExtra(r2, r0)     // Catch:{ Exception -> 0x013c }
            java.lang.String r0 = "1%dns#ge1%dk1%do1%dt"
            r1.putExtra(r0, r8)     // Catch:{ Exception -> 0x013c }
            java.lang.String r0 = "com.ironsource.mobilcore.MobileCoreReport_extra_device_data"
            org.json.JSONObject r2 = a(r7)     // Catch:{ Exception -> 0x013c }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x013c }
            r1.putExtra(r0, r2)     // Catch:{ Exception -> 0x013c }
            r7.startService(r1)     // Catch:{ Exception -> 0x013c }
            android.content.SharedPreferences r0 = com.ironsource.mobilcore.av.b()     // Catch:{ Exception -> 0x013c }
            android.content.SharedPreferences$Editor r0 = r0.edit()     // Catch:{ Exception -> 0x013c }
            java.lang.String r1 = "prefs_first_run"
            java.lang.String r2 = "false"
            r0.putString(r1, r2)     // Catch:{ Exception -> 0x013c }
            java.lang.String r1 = "prefs_first_run1.0"
            java.lang.String r2 = "false"
            android.content.SharedPreferences$Editor r0 = r0.putString(r1, r2)     // Catch:{ Exception -> 0x013c }
            r0.commit()     // Catch:{ Exception -> 0x013c }
        L_0x010f:
            com.ironsource.mobilcore.aF r0 = com.ironsource.mobilcore.aF.a()     // Catch:{ Exception -> 0x013c }
            r0.a(r7, r8)     // Catch:{ Exception -> 0x013c }
            com.ironsource.mobilcore.o r0 = com.ironsource.mobilcore.C0030o.a()     // Catch:{ Exception -> 0x013c }
            r0.a(r7, r8)     // Catch:{ Exception -> 0x013c }
            com.ironsource.mobilcore.k r0 = com.ironsource.mobilcore.C0026k.a()     // Catch:{ Exception -> 0x013c }
            r0.a(r7, r8)     // Catch:{ Exception -> 0x013c }
            com.ironsource.mobilcore.an r0 = new com.ironsource.mobilcore.an     // Catch:{ Exception -> 0x013c }
            r0.<init>()     // Catch:{ Exception -> 0x013c }
            com.ironsource.mobilcore.MobileCore.d = r0     // Catch:{ Exception -> 0x013c }
            com.ironsource.mobilcore.am r0 = com.ironsource.mobilcore.am.a()     // Catch:{ Exception -> 0x013c }
            android.content.Context r1 = com.ironsource.mobilcore.MobileCore.b     // Catch:{ Exception -> 0x013c }
            r0.a(r1, r8)     // Catch:{ Exception -> 0x013c }
        L_0x0134:
            return
        L_0x0135:
            com.ironsource.mobilcore.MobileCore$LOG_TYPE r1 = com.ironsource.mobilcore.MobileCore.LOG_TYPE.PRODUCTION     // Catch:{ Exception -> 0x013c }
            com.ironsource.mobilcore.C0031p.a(r1)     // Catch:{ Exception -> 0x013c }
            goto L_0x0065
        L_0x013c:
            r0 = move-exception
            java.lang.StackTraceElement[] r1 = r0.getStackTrace()
            r1 = r1[r4]
            android.content.Intent r2 = com.ironsource.mobilcore.MobileCore.c
            java.lang.String r3 = "com.ironsource.mobilcore.MobileCoreReport_extra_ex"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.Class<com.ironsource.mobilcore.MobileCore> r5 = com.ironsource.mobilcore.MobileCore.class
            java.lang.String r5 = r5.toString()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "###"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r0.getMessage()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "###"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r1.getClassName()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "@"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r1.getFileName()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ":"
            java.lang.StringBuilder r4 = r4.append(r5)
            int r1 = r1.getLineNumber()
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            r2.putExtra(r3, r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "caught ex "
            r1.<init>(r2)
            java.lang.Class<com.ironsource.mobilcore.MobileCore> r2 = com.ironsource.mobilcore.MobileCore.class
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "###"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r0.getMessage()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "###"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = android.util.Log.getStackTraceString(r0)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.ironsource.mobilcore.C0031p.a(r0, r6)
            android.content.Intent r0 = com.ironsource.mobilcore.MobileCore.c
            r7.startService(r0)
            goto L_0x0134
        L_0x01d0:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013c }
            java.lang.String r2 = "The token passed '"
            r1.<init>(r2)     // Catch:{ Exception -> 0x013c }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ Exception -> 0x013c }
            java.lang.String r2 = "' is not valid"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x013c }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x013c }
            r2 = 2
            com.ironsource.mobilcore.C0031p.a(r1, r2)     // Catch:{ Exception -> 0x013c }
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Exception -> 0x013c }
            android.content.Context r2 = com.ironsource.mobilcore.MobileCore.b     // Catch:{ Exception -> 0x013c }
            java.lang.Class<com.ironsource.mobilcore.MobileCoreReport> r3 = com.ironsource.mobilcore.MobileCoreReport.class
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x013c }
            java.lang.String r2 = "com.ironsource.mobilcore.MobileCoreReport_extra_ex"
            java.lang.String r3 = "Can't extract affiliateAccount from the token passed"
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x013c }
            java.lang.String r2 = "s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr"
            r3 = 70
            r1.putExtra(r2, r3)     // Catch:{ Exception -> 0x013c }
            java.lang.String r2 = "1%dns#ge1%dk1%do1%dt"
            r1.putExtra(r2, r8)     // Catch:{ Exception -> 0x013c }
            android.content.Context r2 = com.ironsource.mobilcore.MobileCore.b     // Catch:{ Exception -> 0x013c }
            r2.startService(r1)     // Catch:{ Exception -> 0x013c }
            goto L_0x009f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.MobileCore.init(android.app.Activity, java.lang.String, com.ironsource.mobilcore.MobileCore$LOG_TYPE):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
     arg types: [android.app.Activity, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, float):int
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean */
    private static JSONObject a(Activity activity) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("Model", Build.MODEL);
            jSONObject.put("Device", Build.DEVICE);
            jSONObject.put("Brand", Build.BRAND);
            jSONObject.put("Language", Locale.getDefault().getDisplayLanguage(Locale.ENGLISH));
            jSONObject.put("Release", Build.VERSION.RELEASE);
            jSONObject.put("CellAvail", C0017b.a((Context) activity, 1));
            jSONObject.put("WifiAvail", C0017b.a((Context) activity, 0));
            jSONObject.put("Dpi", String.valueOf(av.d(activity)));
            jSONObject.put("ScreenSize", String.valueOf(av.a(Double.valueOf(av.e(activity)), activity)));
            jSONObject.put("LastAppInst", av.b(activity));
            jSONObject.put("InstalledApps", av.c(activity));
            jSONObject.put("CurrentConn", av.a(av.a(activity)));
            jSONObject.put("Uns", av.k(activity));
            jSONObject.put("Uid", av.g(activity));
        } catch (JSONException e2) {
            av.a(b, MobileCore.class.getName(), e2);
        }
        return jSONObject;
    }

    protected static int a(Context context) {
        try {
            return av.b().getInt("1%ds1%du1%dts#ga1%dt1%dss#g_1%drs#ges#gfs#gf1%dos#g_1%dk1%dps#ga", 6);
        } catch (Exception e2) {
            StackTraceElement stackTraceElement = e2.getStackTrace()[0];
            c.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_ex", MobileCore.class.toString() + "###" + e2.getMessage() + "###" + stackTraceElement.getClassName() + "@" + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber());
            C0031p.a("caught ex " + MobileCore.class.getName() + "###" + e2.getMessage() + "###" + Log.getStackTraceString(e2), 55);
            context.startService(c);
            return 6;
        }
    }

    public static void showOfferWall(Activity activity, CallbackResponse callbackResponse) {
        if (callbackResponse != null) {
            showOfferWall(activity, callbackResponse, false);
        } else {
            showOfferWall(activity, callbackResponse, true);
        }
    }

    public static void showOfferWall(final Activity activity, final CallbackResponse callbackResponse, final boolean z) {
        C0031p.a("offerwall | requesting showOfferWall", 55);
        a.post(new Runnable(false) {
            public final void run() {
                if (!C0017b.a(MobileCore.b)) {
                    C0031p.a("offerwall | no internet connection", 55);
                    if (z) {
                        Toast.makeText(MobileCore.b, "No internet connection", 1).show();
                    }
                    if (callbackResponse != null) {
                        callbackResponse.onConfirmation(CallbackResponse.TYPE.OFFERWALL_NO_CONNECTION);
                    }
                } else if (aF.a().c()) {
                    C0031p.a("offerwall | showOfferWall called and we are already showing one. return.", 55);
                } else {
                    aF.a().a(true);
                    C0031p.a("offerwall | requesting showOfferWall - runFlowHtml", 55);
                    aF.a().a(activity, callbackResponse, z, false);
                }
            }
        });
    }

    public static void openUrl(Activity activity, String str, boolean z) {
        av.a(activity, str, z);
    }

    public static void addOfferWallReadyListener(final IOnOfferwallReadyListener iOnOfferwallReadyListener) {
        aF.a();
        if (aF.d()) {
            iOnOfferwallReadyListener.onOfferWallReady();
            return;
        }
        aF.a().a(new aF.b() {
            public final void a() {
                iOnOfferwallReadyListener.onOfferWallReady();
            }

            public final boolean b() {
                return true;
            }
        });
    }

    public static MCISliderAPI getSlider() {
        return d;
    }
}
