package com.ironsource.mobilcore;

public enum MCEWidgetTextProperties {
    MAIN_TEXT,
    SECONDARY_TEXT,
    BADGE_TEXT
}
