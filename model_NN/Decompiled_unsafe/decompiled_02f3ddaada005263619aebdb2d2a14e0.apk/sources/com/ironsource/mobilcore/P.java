package com.ironsource.mobilcore;

import android.content.Context;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class P {
    private boolean a;
    private boolean b;
    private JSONArray c;
    private ao d;

    public P(Context context, JSONObject jSONObject) throws JSONException {
        jSONObject.optBoolean("enabled", true);
        jSONObject.optString("position", "left");
        this.a = jSONObject.optBoolean("showSliderHandle", true);
        this.b = jSONObject.optBoolean("ftue", false);
        this.c = jSONObject.getJSONArray("layout");
        this.d = new ao(context, jSONObject.getJSONObject("style"));
    }

    public final boolean a() {
        return this.a;
    }

    public final boolean b() {
        return this.b;
    }

    public final JSONArray c() {
        return this.c;
    }

    public final ao d() {
        return this.d;
    }
}
