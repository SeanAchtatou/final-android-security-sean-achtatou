package com.ironsource.mobilcore;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import org.json.JSONException;
import org.json.JSONObject;

final class O extends F {
    private Intent a;

    public O(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "ironsourceShare";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) throws JSONException {
        this.a = av.a(jSONObject);
        super.a(jSONObject, true);
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        super.a(view);
        if (this.a != null) {
            this.e.startActivity(this.a);
        }
    }
}
