package com.ironsource.mobilcore;

import android.view.animation.Interpolator;

/* renamed from: com.ironsource.mobilcore.aa  reason: case insensitive filesystem */
final class C0008aa implements Interpolator {
    private static final C0013af a = new C0013af();

    C0008aa() {
    }

    public final float getInterpolation(float f) {
        if (f < 0.33333334f) {
            return a.getInterpolation(f * 3.0f);
        }
        if (f <= 0.6666667f) {
            return 1.0f;
        }
        return 1.0f - a.getInterpolation(((f + 0.33333334f) - 1.0f) * 3.0f);
    }
}
