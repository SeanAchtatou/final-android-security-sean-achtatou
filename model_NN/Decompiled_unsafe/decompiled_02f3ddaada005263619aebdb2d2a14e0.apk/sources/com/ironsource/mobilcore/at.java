package com.ironsource.mobilcore;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

final class at extends C0037v {
    private ArrayList<C0035t> a = new ArrayList<>();

    public at(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "ironsourceSocialWidget";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.at.a(com.ironsource.mobilcore.t, org.json.JSONObject):void
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject, false);
        JSONObject optJSONObject = jSONObject.optJSONObject("facebookPostToWallItem");
        if (optJSONObject != null) {
            a(new ap(this.c, this.d), optJSONObject);
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("twitterTweetItem");
        if (optJSONObject2 != null) {
            a(new as(this.c, this.d), optJSONObject2);
        }
        JSONObject optJSONObject3 = jSONObject.optJSONObject("googlePlusPostItem");
        if (optJSONObject3 != null) {
            a(new ar(this.c, this.d), optJSONObject3);
        }
        JSONObject optJSONObject4 = jSONObject.optJSONObject("shareItem");
        if (optJSONObject4 != null) {
            a(new aq(this.c, this.d), optJSONObject4);
        }
        b();
    }

    public final void a(int i) {
        super.a(i);
        Iterator<C0035t> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().a(i);
        }
    }

    public final void a(Activity activity) {
        super.a(activity);
        Iterator<C0035t> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().a(activity);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g = new LinearLayout(this.c);
        this.g.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        ((LinearLayout) this.g).setOrientation(0);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        ViewGroup viewGroup = (ViewGroup) this.g;
        Iterator<C0035t> it = this.a.iterator();
        while (it.hasNext()) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
            layoutParams.weight = 1.0f;
            viewGroup.addView(it.next().h(), layoutParams);
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
    }

    private void a(C0035t tVar, JSONObject jSONObject) throws JSONException {
        tVar.a(jSONObject);
        tVar.h = this.h;
        this.a.add(tVar);
    }
}
