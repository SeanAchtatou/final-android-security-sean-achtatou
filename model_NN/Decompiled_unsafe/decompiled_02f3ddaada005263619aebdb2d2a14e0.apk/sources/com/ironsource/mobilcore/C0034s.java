package com.ironsource.mobilcore;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.ironsource.mobilcore.s  reason: case insensitive filesystem */
abstract class C0034s extends C0037v {
    protected String a;
    protected boolean b = true;
    /* access modifiers changed from: private */
    public String q;
    private boolean r = false;
    /* access modifiers changed from: private */
    public a s;

    public C0034s(Context context, ao aoVar, a aVar) {
        super(context, aoVar);
        this.s = aVar;
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return false;
    }

    public final String e() {
        return null;
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        if (!this.r) {
            this.r = true;
            b();
            this.g.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    if (C0034s.this.s != null && !TextUtils.isEmpty(C0034s.this.q)) {
                        C0034s.this.s.a(C0034s.this.q);
                    }
                }
            });
        }
    }

    public final boolean b(JSONObject jSONObject) {
        this.b = true;
        c(jSONObject);
        c();
        return this.b;
    }

    public final boolean a_() {
        return this.r;
    }

    /* access modifiers changed from: protected */
    public void c(JSONObject jSONObject) {
        this.i = jSONObject.optString("title");
        String e = aF.a().e();
        this.a = jSONObject.optString("img");
        if (this.a.indexOf("file://") == 0) {
            this.a = this.a.substring(7);
        }
        if (this.a.indexOf(e) != 0) {
            this.a = e + "/offerwall/" + this.a;
        }
        this.q = jSONObject.optString("click");
    }

    /* renamed from: com.ironsource.mobilcore.s$a */
    interface a {
        final /* synthetic */ C a;

        default a(C c) {
            this.a = c;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ironsource.mobilcore.av.a(android.app.Activity, java.lang.String, boolean):void
         arg types: [android.app.Activity, java.lang.String, int]
         candidates:
          com.ironsource.mobilcore.av.a(android.content.Context, java.lang.String, java.lang.Exception):void
          com.ironsource.mobilcore.av.a(android.content.Context, java.lang.String, java.lang.String):void
          com.ironsource.mobilcore.av.a(com.ironsource.mobilcore.B, java.lang.String, com.ironsource.mobilcore.B$a):void
          com.ironsource.mobilcore.av.a(android.app.Activity, java.lang.String, boolean):void */
        default void a(String str) {
            aD c;
            if (!TextUtils.isEmpty(str) && (c = this.a.k.c(str)) != null) {
                this.a.a("S", c.f());
                this.a.l.a(str);
                if (!C.a(this.a, str, str)) {
                    C0031p.a("slider ow_url: " + str, 55);
                    if (str == null || !c.c().equals("ApkDownload")) {
                        if (str == null || !c.c().equals("CPC")) {
                            C.b(this.a, str);
                            return;
                        }
                        av.a(this.a.e, str, true);
                        this.a.k();
                    } else if (av.c(this.a.c, c.h())) {
                        this.a.a("AI", ((aD) this.a.k.get(this.a.c(str))).f());
                        Toast.makeText(this.a.c, "Application already installed", 1).show();
                    } else if (C0026k.a().a(c.a())) {
                        Toast.makeText(this.a.c, "Already downloading this offer", 1).show();
                    } else {
                        Toast.makeText(this.a.c, "The app will be downloaded to your device shortly", 1).show();
                        C0026k.a().a(str, c.a(), c.e(), c.f(), "OfferWall", -1, true, "Slider");
                    }
                }
            }
        }
    }
}
