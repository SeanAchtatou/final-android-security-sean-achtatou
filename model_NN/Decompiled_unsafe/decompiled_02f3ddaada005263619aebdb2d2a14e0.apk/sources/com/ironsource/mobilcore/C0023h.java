package com.ironsource.mobilcore;

import com.ironsource.mobilcore.av;
import java.io.File;
import org.apache.http.HttpEntity;

/* renamed from: com.ironsource.mobilcore.h  reason: case insensitive filesystem */
public class C0023h implements av.a {
    protected String b;
    protected String c;
    protected av.b d;

    C0023h(String str, String str2, av.b bVar) {
        this.b = str2;
        this.c = str;
        this.d = bVar;
    }

    /* access modifiers changed from: protected */
    public final File b(HttpEntity httpEntity) {
        try {
            return a(httpEntity.getContent());
        } catch (Exception e) {
            if (this.d != null) {
                this.d.a(this.b, e);
            }
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0067 A[Catch:{ all -> 0x0099 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070 A[SYNTHETIC, Splitter:B:18:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0093 A[SYNTHETIC, Splitter:B:34:0x0093] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x009f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.io.File a(java.io.InputStream r7) {
        /*
            r6 = this;
            r1 = 0
            com.ironsource.mobilcore.av$b r0 = r6.d
            if (r0 == 0) goto L_0x000c
            com.ironsource.mobilcore.av$b r0 = r6.d
            java.lang.String r2 = r6.b
            r0.a(r2)
        L_0x000c:
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            java.lang.String r2 = r6.c     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            java.lang.String r3 = r6.b     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            r0.<init>(r2, r3)     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            java.lang.String r3 = "storeFile | downloading "
            r2.<init>(r3)     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            java.lang.String r3 = r6.c     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            java.lang.String r3 = r6.b     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            r3 = 55
            com.ironsource.mobilcore.C0031p.a(r2, r3)     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x009c, all -> 0x0090 }
            org.apache.http.util.ByteArrayBuffer r3 = new org.apache.http.util.ByteArrayBuffer     // Catch:{ IOException -> 0x0062 }
            r4 = 512(0x200, float:7.175E-43)
            r3.<init>(r4)     // Catch:{ IOException -> 0x0062 }
        L_0x0043:
            int r4 = r7.read()     // Catch:{ IOException -> 0x0062 }
            r5 = -1
            if (r4 == r5) goto L_0x007d
            byte r4 = (byte) r4     // Catch:{ IOException -> 0x0062 }
            r3.append(r4)     // Catch:{ IOException -> 0x0062 }
            boolean r4 = r3.isFull()     // Catch:{ IOException -> 0x0062 }
            if (r4 == 0) goto L_0x0043
            byte[] r4 = r3.toByteArray()     // Catch:{ IOException -> 0x0062 }
            r2.write(r4)     // Catch:{ IOException -> 0x0062 }
            r2.flush()     // Catch:{ IOException -> 0x0062 }
            r3.clear()     // Catch:{ IOException -> 0x0062 }
            goto L_0x0043
        L_0x0062:
            r0 = move-exception
        L_0x0063:
            com.ironsource.mobilcore.av$b r3 = r6.d     // Catch:{ all -> 0x0099 }
            if (r3 == 0) goto L_0x006e
            com.ironsource.mobilcore.av$b r3 = r6.d     // Catch:{ all -> 0x0099 }
            java.lang.String r4 = r6.b     // Catch:{ all -> 0x0099 }
            r3.a(r4, r0)     // Catch:{ all -> 0x0099 }
        L_0x006e:
            if (r2 == 0) goto L_0x009f
            r2.close()     // Catch:{ IOException -> 0x008d }
            r0 = r1
        L_0x0074:
            com.ironsource.mobilcore.av$b r1 = r6.d
            if (r1 == 0) goto L_0x007c
            com.ironsource.mobilcore.av$b r1 = r6.d
            java.lang.String r1 = r6.b
        L_0x007c:
            return r0
        L_0x007d:
            byte[] r3 = r3.toByteArray()     // Catch:{ IOException -> 0x0062 }
            r2.write(r3)     // Catch:{ IOException -> 0x0062 }
            r2.flush()     // Catch:{ IOException -> 0x0062 }
            r2.close()     // Catch:{ IOException -> 0x008b }
            goto L_0x0074
        L_0x008b:
            r1 = move-exception
            goto L_0x0074
        L_0x008d:
            r0 = move-exception
            r0 = r1
            goto L_0x0074
        L_0x0090:
            r0 = move-exception
        L_0x0091:
            if (r1 == 0) goto L_0x0096
            r1.close()     // Catch:{ IOException -> 0x0097 }
        L_0x0096:
            throw r0
        L_0x0097:
            r1 = move-exception
            goto L_0x0096
        L_0x0099:
            r0 = move-exception
            r1 = r2
            goto L_0x0091
        L_0x009c:
            r0 = move-exception
            r2 = r1
            goto L_0x0063
        L_0x009f:
            r0 = r1
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.C0023h.a(java.io.InputStream):java.io.File");
    }

    public boolean a(HttpEntity httpEntity) {
        boolean z;
        File b2 = b(httpEntity);
        if (this.d != null) {
            av.b bVar = this.d;
            String str = this.b;
            if (b2 != null) {
                z = true;
            } else {
                z = false;
            }
            bVar.a(str, z);
        }
        if (b2 != null) {
            return true;
        }
        return false;
    }

    public final void a(int i) {
        C0031p.a("StoreFileHandler | processError failed to download resource " + this.b + " file with error code " + i, 2);
        if (this.d != null) {
            this.d.a(this.b, i);
        }
    }
}
