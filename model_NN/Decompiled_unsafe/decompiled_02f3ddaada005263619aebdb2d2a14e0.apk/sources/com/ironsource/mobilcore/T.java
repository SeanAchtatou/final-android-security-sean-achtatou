package com.ironsource.mobilcore;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.view.MotionEventCompat;

final class T extends Drawable {
    private a a;
    private final Paint b;

    /* synthetic */ T(a aVar, byte b2) {
        this(aVar);
    }

    public T() {
        this((a) null);
    }

    public T(int i) {
        this((a) null);
        if (this.a.a != -16777216 || this.a.b != -16777216) {
            invalidateSelf();
            a aVar = this.a;
            this.a.b = -16777216;
            aVar.a = -16777216;
        }
    }

    private T(a aVar) {
        this.b = new Paint();
        this.a = new a(aVar);
    }

    public final int getChangingConfigurations() {
        return super.getChangingConfigurations() | this.a.c;
    }

    public final void draw(Canvas canvas) {
        if ((this.a.b >>> 24) != 0) {
            this.b.setColor(this.a.b);
            canvas.drawRect(getBounds(), this.b);
        }
    }

    public final void setAlpha(int i) {
        int i2 = this.a.b;
        this.a.b = (((((i >> 7) + i) * (this.a.a >>> 24)) >> 8) << 24) | ((this.a.a << 8) >>> 8);
        if (i2 != this.a.b) {
            invalidateSelf();
        }
    }

    public final void setColorFilter(ColorFilter colorFilter) {
    }

    public final int getOpacity() {
        switch (this.a.b >>> 24) {
            case 0:
                return -2;
            case MotionEventCompat.ACTION_MASK:
                return -1;
            default:
                return -3;
        }
    }

    public final Drawable.ConstantState getConstantState() {
        this.a.c = getChangingConfigurations();
        return this.a;
    }

    static final class a extends Drawable.ConstantState {
        int a;
        int b;
        int c;

        a(a aVar) {
            if (aVar != null) {
                this.a = aVar.a;
                this.b = aVar.b;
            }
        }

        public final Drawable newDrawable() {
            return new T(this, (byte) 0);
        }

        public final Drawable newDrawable(Resources resources) {
            return new T(this, (byte) 0);
        }

        public final int getChangingConfigurations() {
            return this.c;
        }
    }
}
