package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.ironsource.mobilcore.aG;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.GregorianCalendar;
import java.util.zip.CRC32;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

@SuppressLint({"InlinedApi"})
/* renamed from: com.ironsource.mobilcore.o  reason: case insensitive filesystem */
final class C0030o {
    private static C0030o h;
    /* access modifiers changed from: private */
    public a a = a.FLOW_INITIAL;
    /* access modifiers changed from: private */
    public boolean b = false;
    /* access modifiers changed from: private */
    public Activity c;
    private String d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    private boolean i = false;
    private boolean j;

    /* renamed from: com.ironsource.mobilcore.o$a */
    enum a {
        FLOW_INITIAL,
        FLOW_DOWNLOADING,
        FLOW_DOWNLOAD_FAILED,
        FLOW_DOWNLOADED,
        FLOW_FALLBACK_SHOWN
    }

    private C0030o() {
    }

    public static synchronized C0030o a() {
        C0030o oVar;
        synchronized (C0030o.class) {
            if (h == null) {
                h = new C0030o();
            }
            oVar = h;
        }
        return oVar;
    }

    public final String b() {
        return this.g;
    }

    public final void a(Activity activity, String str) {
        if (!this.i) {
            this.i = true;
            this.c = activity;
            this.d = str;
            this.j = av.b().getBoolean("processedUserAction", false);
            if (!this.j) {
                C0031p.a("icon offer , init() | called. did not process user action yet. download icon offer flow", 55);
                this.g = av.b().getString("userActionToProcess", "");
                C0031p.a("icon offer , downloadIconOfferFlowHtml() | called", 55);
                AnonymousClass1 r0 = new AsyncTask<Void, Void, Boolean>() {
                    /* access modifiers changed from: protected */
                    public final /* synthetic */ Object doInBackground(Object[] objArr) {
                        return a();
                    }

                    /* access modifiers changed from: protected */
                    public final /* synthetic */ void onPostExecute(Object obj) {
                        if (((Boolean) obj).booleanValue()) {
                            a unused = C0030o.this.a = a.FLOW_DOWNLOADED;
                            C0031p.a("icon offer | got flow file from web successfully", 55);
                            if (C0030o.this.b) {
                                C0030o.this.b(C0030o.this.c, C0030o.this.g);
                                return;
                            }
                            return;
                        }
                        a unused2 = C0030o.this.a = a.FLOW_DOWNLOAD_FAILED;
                        C0031p.a("icon offer  | failed to get flow file from web", 55);
                        if (C0030o.this.b) {
                            C0030o.this.b(C0030o.this.c, C0030o.this.g);
                        }
                    }

                    private Boolean a() {
                        a unused = C0030o.this.a = a.FLOW_DOWNLOADING;
                        String a2 = av.a(C0030o.this.c, "dropIcon");
                        try {
                            StringBuilder sb = new StringBuilder();
                            DefaultHttpClient a3 = av.a();
                            BasicHttpParams basicHttpParams = new BasicHttpParams();
                            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 5000);
                            HttpConnectionParams.setSoTimeout(basicHttpParams, 5000);
                            a3.setParams(basicHttpParams);
                            HttpResponse execute = a3.execute(new HttpGet(a2));
                            if (execute.getStatusLine().getStatusCode() != 200) {
                                return false;
                            }
                            HttpEntity entity = execute.getEntity();
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent()));
                            while (true) {
                                String readLine = bufferedReader.readLine();
                                if (readLine != null) {
                                    sb.append(readLine);
                                } else {
                                    String unused2 = C0030o.this.e = sb.toString();
                                    entity.consumeContent();
                                    return true;
                                }
                            }
                        } catch (Exception e) {
                            av.a(MobileCore.b, getClass().getName(), e);
                            a unused3 = C0030o.this.a = a.FLOW_DOWNLOAD_FAILED;
                            return false;
                        }
                    }
                };
                new aG(new aG.a(this), this.c, "agreement.html").start();
                r0.execute(new Void[0]);
                return;
            }
            C0031p.a("icon offer , init() | called. already processed user action. do nothing", 55);
        }
    }

    public final String c() {
        StringBuilder append = new StringBuilder("v=4&uid=").append(av.g(this.c)).append("&DT=C");
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        GregorianCalendar gregorianCalendar2 = new GregorianCalendar();
        gregorianCalendar.set(2012, 1, 1);
        gregorianCalendar2.setTimeInMillis(System.currentTimeMillis());
        String sb = append.append((int) ((gregorianCalendar2.getTimeInMillis() - gregorianCalendar.getTimeInMillis()) / 86400000)).append("&iv=0.9").toString();
        String a2 = a(sb);
        StringBuilder sb2 = new StringBuilder("&cr=");
        CRC32 crc32 = new CRC32();
        crc32.update(sb.getBytes());
        return "cd=" + a2 + sb2.append(String.valueOf((crc32.getValue() ^ -1) & 2147483647L)).toString();
    }

    private static String a(String str) {
        if (str == null) {
            return "";
        }
        String str2 = "";
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (charAt < 26) {
                str2 = str2.concat("Z").concat(String.valueOf((char) ('Z' - charAt)));
            } else if (charAt < '4') {
                str2 = str2.concat("t").concat(String.valueOf((char) ('t' - charAt)));
            } else if (charAt < '8') {
                str2 = str2.concat("y").concat(String.valueOf((char) ('y' - charAt)));
            } else if (charAt < 'A') {
                str2 = str2.concat("z").concat(String.valueOf((char) (('z' - charAt) + 56)));
            } else if (charAt < '[') {
                str2 = str2.concat("0").concat(String.valueOf(charAt));
            } else {
                str2 = str2.concat(String.valueOf((char) (((charAt - '[') / 25) + 49))).concat(String.valueOf((char) (90 - ((charAt - '[') % 25))));
            }
        }
        return str2;
    }

    public final void b(Activity activity, String str) {
        String str2;
        C0031p.a("DROPICON: processUserAction. action:" + str, 55);
        if (!this.j) {
            C0031p.a("DROPICON: setUserAgreementAction. action:" + str, 55);
            if ("AGREEMENT_BACK".equals(str)) {
                str2 = "";
            } else {
                str2 = (!"AGREEMENT_AGREE".equals(str) || !a.FLOW_FALLBACK_SHOWN.equals(this.a)) ? (!"AGREEMENT_AGREE_FINISH_LATER".equals(str) || !a.FLOW_DOWNLOADED.equals(this.a)) ? str : "AGREEMENT_AGREE" : "AGREEMENT_AGREE_FINISH_LATER";
            }
            this.g = str2;
            C0031p.a("icon offer , processUserAgreementAction() | called. action:" + this.g, 55);
            if (str.equals("")) {
                switch (this.a) {
                    case FLOW_DOWNLOADING:
                        this.b = true;
                        break;
                    case FLOW_DOWNLOADED:
                        a(activity);
                        break;
                    case FLOW_DOWNLOAD_FAILED:
                        this.e = this.f;
                        a(activity);
                        this.a = a.FLOW_FALLBACK_SHOWN;
                        break;
                    case FLOW_FALLBACK_SHOWN:
                        this.j = true;
                        break;
                }
            } else if (!str.equals("AGREEMENT_BACK") && !str.equals("")) {
                this.j = true;
                av.b().edit().putBoolean("processedUserAction", true).commit();
            }
            av.b().edit().putString("userActionToProcess", this.g).commit();
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void a(Activity activity) {
        if (this.i) {
            boolean z = !TextUtils.isEmpty(this.e);
            C0031p.a("icon offer , runFlowHtmlIfPossible() | called. hasFlow:" + z, 55);
            if (z) {
                WebView webView = new WebView(activity);
                aw awVar = new aw(this.d, activity, "http://wwww.dummyurl.com", webView, null);
                awVar.a(activity);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setSupportMultipleWindows(false);
                webView.clearCache(true);
                webView.setWebChromeClient(awVar);
                webView.getSettings().setSupportZoom(false);
                webView.setInitialScale(100);
                webView.setHorizontalScrollBarEnabled(false);
                webView.setWebViewClient(new WebViewClient());
                webView.loadDataWithBaseURL("http://wwww.dummyurl.com", this.e, "text/html", "UTF-8", null);
            }
        }
    }
}
