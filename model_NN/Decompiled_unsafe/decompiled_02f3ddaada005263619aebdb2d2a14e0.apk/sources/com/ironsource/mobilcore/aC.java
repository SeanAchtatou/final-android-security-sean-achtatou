package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import java.io.File;
import java.security.MessageDigest;
import java.util.Locale;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class aC {
    private String a = "MobilCoreLib";
    private String b;
    private String c;
    private Context d;
    private SharedPreferences e;
    private JSONObject f;

    public aC() {
        C0031p.a("in reporter", 3);
    }

    public final void a(Context context, Intent intent) {
        try {
            if ("download".equals(intent.getStringExtra("extra_service_type"))) {
                C0031p.a("MobileCoreReport calling downloadService", 55);
                new C0027l(context, intent);
                return;
            }
            this.d = context;
            if (intent.getExtras() != null) {
                this.d = context.getApplicationContext();
                if (intent.getExtras() == null) {
                    return;
                }
                if (intent.hasExtra("1%dns#ge1%dk1%do1%dt")) {
                    String stringExtra = intent.getStringExtra("1%dns#ge1%dk1%do1%dt");
                    if (intent.hasExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr")) {
                        a(stringExtra, intent.getIntExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", -1), intent);
                    } else {
                        C0031p.a("No reportType", 2);
                    }
                } else {
                    C0031p.a("No token", 2);
                }
            }
        } catch (Exception e2) {
            String stringExtra2 = intent.getStringExtra("1%dns#ge1%dk1%do1%dt");
            StackTraceElement stackTraceElement = e2.getStackTrace()[0];
            C0031p.a("MobileCoreReport | Exception | could not report error: " + MobileCoreReport.class.getName() + "###" + e2.getMessage() + "###" + stackTraceElement.getFileName() + "##" + stackTraceElement.getMethodName() + ":" + stackTraceElement.getLineNumber(), 55);
            intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_ex", MobileCoreReport.class.getName() + "###" + e2.getMessage() + "###" + stackTraceElement.getFileName() + "##" + stackTraceElement.getMethodName() + ":" + stackTraceElement.getLineNumber());
            intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 70);
            try {
                a(stringExtra2, 70, intent);
            } catch (Exception e3) {
                C0031p.a("MobileCoreReport | Exception | could not report error: " + e2.getMessage(), 2);
            }
        }
    }

    @SuppressLint({"InlinedApi"})
    private void a(String str, int i, Intent intent) throws Exception {
        this.c = str;
        this.b = "0.9";
        if (Build.VERSION.SDK_INT >= 9) {
            this.e = this.d.getSharedPreferences("1%dss#gfs#ge1%dr1%dps#g_s#gds#ge1%drs#gas#ghs#gSs#g_s#ge1%dr1%dos#gCs#ge1%dls#gis#gb1%do1%dm", 4);
        } else {
            this.e = this.d.getSharedPreferences("1%dss#gfs#ge1%dr1%dps#g_s#gds#ge1%drs#gas#ghs#gSs#g_s#ge1%dr1%dos#gCs#ge1%dls#gis#gb1%do1%dm", 0);
        }
        this.a = this.d.getPackageName().toString();
        C0031p.a("Library attached to: " + this.a, 3);
        C0017b a2 = a(i, intent);
        JSONObject a3 = a2.a();
        if (!C0017b.a(this.d)) {
            C0031p.a("MobileCoreReport | sendReport | We dont have network adding to log stack", 55);
            a2.b(true);
            a(a2.a());
        } else if (new File(this.d.getFilesDir().getPath() + "/log_stack.dat").exists()) {
            C0031p.a("MobileCoreReport | sendReport | We have network adding to log stack", 55);
            a();
            b(a3);
            a(this.f.toString());
        } else {
            try {
                a(a3.toString());
            } catch (Exception e2) {
                a(a3);
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private C0017b a(int i, Intent intent) throws JSONException {
        C0028m mVar;
        C0017b bVar = new C0017b();
        bVar.i("1.0");
        bVar.a(this.a);
        bVar.b(av.f(this.d));
        bVar.c(av.g(this.d));
        bVar.k("Android");
        if (C0029n.a != null) {
            bVar.a(C0029n.a.intValue());
        }
        if (!TextUtils.isEmpty(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type"))) {
            bVar.j(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type"));
        }
        if (!TextUtils.isEmpty(intent.getStringExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow"))) {
            bVar.d(intent.getStringExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow"));
        }
        String stringExtra = intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_free_fields");
        if (!TextUtils.isEmpty(stringExtra)) {
            bVar.a(new JSONArray(stringExtra));
        }
        switch (i) {
            case 2:
                bVar.d(this.a);
                bVar.e("+");
                SharedPreferences.Editor edit = this.e.edit();
                edit.putBoolean("s#ges#gc1%dn1%dos#g_1%drs#ga1%dl1%dus#ggs#ge1%drs#g_1%dt1%dr1%do1%dps#ge1%drs#g_1%dss#gfs#ge1%dr1%dp", true);
                if (!edit.commit()) {
                    C0031p.a("failed to commit " + "s#ges#gc1%dn1%dos#g_1%drs#ga1%dl1%dus#ggs#ge1%drs#g_1%dt1%dr1%do1%dps#ge1%drs#g_1%dss#gfs#ge1%dr1%dp", 2);
                    break;
                }
                break;
            case 27:
                bVar.e("D");
                bVar.b(new JSONArray(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offers")));
                break;
            case 29:
                bVar.e("-");
                bVar.b(new JSONArray(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offers")));
                break;
            case 30:
                bVar.e("Q");
                bVar.b(new JSONArray(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offers")));
                break;
            case 31:
                bVar.b(new JSONObject(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer")));
                bVar.e("S");
                break;
            case 32:
                bVar.e("+");
                bVar.b(new JSONObject(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer")));
                break;
            case 33:
                bVar.e("S-");
                bVar.b(new JSONObject(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer")));
                break;
            case 34:
                bVar.a(Boolean.valueOf(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_first_run")).booleanValue());
                bVar.e("D");
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("Model", Build.MODEL);
                    jSONObject.put("Device", Build.DEVICE);
                    jSONObject.put("Brand", Build.BRAND);
                    jSONObject.put("Language", Locale.getDefault().getDisplayLanguage(Locale.ENGLISH));
                    jSONObject.put("OS", Build.VERSION.RELEASE);
                    jSONObject.put("Cellular", C0017b.a(this.d, 1));
                    jSONObject.put("Wifi", C0017b.a(this.d, 0));
                    jSONObject.put("Dpi", String.valueOf(av.d(this.d)));
                    jSONObject.put("ScreenSize", String.valueOf(av.a(Double.valueOf(av.e(this.d)), this.d)));
                    jSONObject.put("LastAppInst", av.b(this.d));
                    jSONObject.put("InstalledApps", av.c(this.d));
                    jSONObject.put("CurrentConn", av.a(av.a(this.d)));
                    jSONObject.put("UNS", av.k(this.d));
                    bVar.a(jSONObject);
                    break;
                } catch (Exception e2) {
                    av.a(MobileCore.b, aC.class.getName(), e2);
                    break;
                }
            case 35:
                bVar.b(new JSONObject(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer")));
                bVar.e("AI");
                break;
            case 70:
                bVar.d(this.a);
                bVar.e("E");
                bVar.f(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_ex"));
                break;
            case 99:
                if (!TextUtils.isEmpty(intent.getStringExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow"))) {
                    bVar.d(intent.getStringExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow"));
                }
                if (!TextUtils.isEmpty(intent.getStringExtra("com.ironsource.mobilecore.MobileCoreReport_extra_result"))) {
                    bVar.e(intent.getStringExtra("com.ironsource.mobilecore.MobileCoreReport_extra_result"));
                }
                String stringExtra2 = intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer");
                if (!TextUtils.isEmpty(stringExtra2)) {
                    bVar.b(new JSONObject(stringExtra2));
                }
                if (!TextUtils.isEmpty(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_event"))) {
                    C0028m mVar2 = new C0028m(bVar);
                    mVar2.d(intent.getStringExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow"));
                    mVar2.p(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_component"));
                    mVar2.q(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_event"));
                    mVar2.r(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_action"));
                    mVar2.s(intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_additional_params"));
                    mVar = mVar2;
                } else {
                    Long valueOf = Long.valueOf(intent.getLongExtra("com.ironsource.mobilecore.MobileCoreReport_extra_timer", -1));
                    if (valueOf.longValue() > -1) {
                        bVar.l(String.valueOf(valueOf));
                    }
                    mVar = bVar;
                }
                bVar = mVar;
                break;
        }
        bVar.h(this.c);
        bVar.g(this.b);
        return bVar;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.io.BufferedWriter] */
    /* JADX WARN: Type inference failed for: r1v4, types: [java.io.BufferedWriter] */
    /* JADX WARN: Type inference failed for: r1v6, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARN: Type inference failed for: r1v9 */
    /* JADX WARN: Type inference failed for: r1v11 */
    /* JADX WARN: Type inference failed for: r1v13 */
    /* JADX WARN: Type inference failed for: r1v15 */
    /* JADX WARN: Type inference failed for: r1v22 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0056 A[SYNTHETIC, Splitter:B:21:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005b A[Catch:{ Exception -> 0x005f }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008a A[SYNTHETIC, Splitter:B:37:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x008f A[Catch:{ Exception -> 0x0093 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00a0 A[SYNTHETIC, Splitter:B:48:0x00a0] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00a5 A[Catch:{ Exception -> 0x00a9 }] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void a(org.json.JSONObject r7) {
        /*
            r6 = this;
            r1 = 0
            monitor-enter(r6)
            android.content.Context r0 = r6.d     // Catch:{ FileNotFoundException -> 0x0048, IOException -> 0x006b, all -> 0x009c }
            java.lang.String r2 = "log_stack.dat"
            r3 = 32768(0x8000, float:4.5918E-41)
            java.io.FileOutputStream r2 = r0.openFileOutput(r2, r3)     // Catch:{ FileNotFoundException -> 0x0048, IOException -> 0x006b, all -> 0x009c }
            java.io.BufferedWriter r0 = new java.io.BufferedWriter     // Catch:{ FileNotFoundException -> 0x00cf, IOException -> 0x00c8 }
            java.io.OutputStreamWriter r3 = new java.io.OutputStreamWriter     // Catch:{ FileNotFoundException -> 0x00cf, IOException -> 0x00c8 }
            r3.<init>(r2)     // Catch:{ FileNotFoundException -> 0x00cf, IOException -> 0x00c8 }
            r0.<init>(r3)     // Catch:{ FileNotFoundException -> 0x00cf, IOException -> 0x00c8 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00d4, IOException -> 0x00ca, all -> 0x00bd }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x00d4, IOException -> 0x00ca, all -> 0x00bd }
            java.lang.String r3 = r7.toString()     // Catch:{ FileNotFoundException -> 0x00d4, IOException -> 0x00ca, all -> 0x00bd }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ FileNotFoundException -> 0x00d4, IOException -> 0x00ca, all -> 0x00bd }
            java.lang.String r3 = "MY_FUCKING_DELIMITER"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ FileNotFoundException -> 0x00d4, IOException -> 0x00ca, all -> 0x00bd }
            java.lang.String r1 = r1.toString()     // Catch:{ FileNotFoundException -> 0x00d4, IOException -> 0x00ca, all -> 0x00bd }
            java.lang.String r1 = com.ironsource.mobilcore.C0017b.m(r1)     // Catch:{ FileNotFoundException -> 0x00d4, IOException -> 0x00ca, all -> 0x00bd }
            r0.write(r1)     // Catch:{ FileNotFoundException -> 0x00d4, IOException -> 0x00ca, all -> 0x00bd }
            java.lang.String r1 = "MobileCoreReport | appendLogStackFile | appending to log"
            r3 = 55
            com.ironsource.mobilcore.C0031p.a(r1, r3)     // Catch:{ FileNotFoundException -> 0x00d4, IOException -> 0x00ca, all -> 0x00bd }
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ Exception -> 0x00b2 }
        L_0x0041:
            if (r2 == 0) goto L_0x0046
            r2.close()     // Catch:{ Exception -> 0x00b2 }
        L_0x0046:
            monitor-exit(r6)
            return
        L_0x0048:
            r0 = move-exception
            r0 = r1
        L_0x004a:
            java.lang.String r2 = "MobileCoreReport | appendLogStackFile | Could not find log stack file"
            r3 = 55
            com.ironsource.mobilcore.C0031p.a(r2, r3)     // Catch:{ all -> 0x00c2 }
            r2 = 0
            r6.f = r2     // Catch:{ all -> 0x00c2 }
            if (r0 == 0) goto L_0x0059
            r0.close()     // Catch:{ Exception -> 0x005f }
        L_0x0059:
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ Exception -> 0x005f }
            goto L_0x0046
        L_0x005f:
            r0 = move-exception
            java.lang.String r0 = "MobileCoreReport | loadlogStack | Error couldn't close file "
            r1 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r1)     // Catch:{ all -> 0x0068 }
            goto L_0x0046
        L_0x0068:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x006b:
            r0 = move-exception
            r2 = r1
        L_0x006d:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bb }
            java.lang.String r4 = "MobileCoreReport | appendLogStackFile | Could read log stack file "
            r3.<init>(r4)     // Catch:{ all -> 0x00bb }
            java.lang.String r0 = r0.getLocalizedMessage()     // Catch:{ all -> 0x00bb }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00bb }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00bb }
            r3 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r3)     // Catch:{ all -> 0x00bb }
            r0 = 0
            r6.f = r0     // Catch:{ all -> 0x00bb }
            if (r1 == 0) goto L_0x008d
            r1.close()     // Catch:{ Exception -> 0x0093 }
        L_0x008d:
            if (r2 == 0) goto L_0x0046
            r2.close()     // Catch:{ Exception -> 0x0093 }
            goto L_0x0046
        L_0x0093:
            r0 = move-exception
            java.lang.String r0 = "MobileCoreReport | loadlogStack | Error couldn't close file "
            r1 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r1)     // Catch:{ all -> 0x0068 }
            goto L_0x0046
        L_0x009c:
            r0 = move-exception
            r2 = r1
        L_0x009e:
            if (r1 == 0) goto L_0x00a3
            r1.close()     // Catch:{ Exception -> 0x00a9 }
        L_0x00a3:
            if (r2 == 0) goto L_0x00a8
            r2.close()     // Catch:{ Exception -> 0x00a9 }
        L_0x00a8:
            throw r0     // Catch:{ all -> 0x0068 }
        L_0x00a9:
            r1 = move-exception
            java.lang.String r1 = "MobileCoreReport | loadlogStack | Error couldn't close file "
            r2 = 55
            com.ironsource.mobilcore.C0031p.a(r1, r2)     // Catch:{ all -> 0x0068 }
            goto L_0x00a8
        L_0x00b2:
            r0 = move-exception
            java.lang.String r0 = "MobileCoreReport | loadlogStack | Error couldn't close file "
            r1 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r1)     // Catch:{ all -> 0x0068 }
            goto L_0x0046
        L_0x00bb:
            r0 = move-exception
            goto L_0x009e
        L_0x00bd:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x009e
        L_0x00c2:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x009e
        L_0x00c8:
            r0 = move-exception
            goto L_0x006d
        L_0x00ca:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x006d
        L_0x00cf:
            r0 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x004a
        L_0x00d4:
            r1 = move-exception
            r1 = r2
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.aC.a(org.json.JSONObject):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0033 A[SYNTHETIC, Splitter:B:16:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0038 A[Catch:{ IOException -> 0x00eb }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0091 A[SYNTHETIC, Splitter:B:29:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0096 A[Catch:{ IOException -> 0x009a }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00c6 A[SYNTHETIC, Splitter:B:51:0x00c6] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00cb A[Catch:{ IOException -> 0x00d0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00db A[SYNTHETIC, Splitter:B:60:0x00db] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00e0 A[Catch:{ IOException -> 0x00e4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:87:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:90:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a() {
        /*
            r8 = this;
            r0 = 0
            r6 = 55
            android.content.Context r1 = r8.d     // Catch:{ FileNotFoundException -> 0x010e, IOException -> 0x0101, JSONException -> 0x00b7, all -> 0x00d4 }
            java.lang.String r2 = "log_stack.dat"
            java.io.FileInputStream r2 = r1.openFileInput(r2)     // Catch:{ FileNotFoundException -> 0x010e, IOException -> 0x0101, JSONException -> 0x00b7, all -> 0x00d4 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x0112, IOException -> 0x0108, JSONException -> 0x00fc, all -> 0x00ef }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ FileNotFoundException -> 0x0112, IOException -> 0x0108, JSONException -> 0x00fc, all -> 0x00ef }
            r3.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0112, IOException -> 0x0108, JSONException -> 0x00fc, all -> 0x00ef }
            r1.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0112, IOException -> 0x0108, JSONException -> 0x00fc, all -> 0x00ef }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            r0.<init>()     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
        L_0x001a:
            java.lang.String r3 = r1.readLine()     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            if (r3 == 0) goto L_0x003c
            r0.append(r3)     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            goto L_0x001a
        L_0x0024:
            r0 = move-exception
            r0 = r1
            r1 = r2
        L_0x0027:
            java.lang.String r2 = "MobileCoreReport | loadlogStackFile | Could not find log stack file"
            r3 = 55
            com.ironsource.mobilcore.C0031p.a(r2, r3)     // Catch:{ all -> 0x00f6 }
            r2 = 0
            r8.f = r2     // Catch:{ all -> 0x00f6 }
            if (r1 == 0) goto L_0x0036
            r1.close()     // Catch:{ IOException -> 0x00eb }
        L_0x0036:
            if (r0 == 0) goto L_0x003b
            r0.close()     // Catch:{ IOException -> 0x00eb }
        L_0x003b:
            return
        L_0x003c:
            java.lang.String r0 = r0.toString()     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            java.lang.String r0 = com.ironsource.mobilcore.C0017b.n(r0)     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            java.lang.String r3 = "MY_FUCKING_DELIMITER"
            java.lang.String[] r0 = r0.split(r3)     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            java.util.List r0 = java.util.Arrays.asList(r0)     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            r8.f = r3     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
        L_0x005e:
            boolean r0 = r4.hasNext()     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            if (r0 == 0) goto L_0x00a1
            java.lang.Object r0 = r4.next()     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            r5.<init>(r0)     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            r3.put(r5)     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            goto L_0x005e
        L_0x0073:
            r0 = move-exception
        L_0x0074:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f4 }
            java.lang.String r4 = "MobileCoreReport | loadlogStackFile | Could read log stack file "
            r3.<init>(r4)     // Catch:{ all -> 0x00f4 }
            java.lang.String r0 = r0.getLocalizedMessage()     // Catch:{ all -> 0x00f4 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00f4 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f4 }
            r3 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r3)     // Catch:{ all -> 0x00f4 }
            r0 = 0
            r8.f = r0     // Catch:{ all -> 0x00f4 }
            if (r2 == 0) goto L_0x0094
            r2.close()     // Catch:{ IOException -> 0x009a }
        L_0x0094:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x009a }
            goto L_0x003b
        L_0x009a:
            r0 = move-exception
            java.lang.String r0 = "MobileCoreReport | loadlogStackFile | Error couldn't close file "
        L_0x009d:
            com.ironsource.mobilcore.C0031p.a(r0, r6)
            goto L_0x003b
        L_0x00a1:
            org.json.JSONObject r0 = r8.f     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            java.lang.String r4 = "reportArr"
            r0.put(r4, r3)     // Catch:{ FileNotFoundException -> 0x0024, IOException -> 0x0073, JSONException -> 0x00ff }
            if (r2 == 0) goto L_0x00ad
            r2.close()     // Catch:{ IOException -> 0x00b3 }
        L_0x00ad:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x00b3 }
            goto L_0x003b
        L_0x00b3:
            r0 = move-exception
            java.lang.String r0 = "MobileCoreReport | loadlogStackFile | Error couldn't close file "
            goto L_0x009d
        L_0x00b7:
            r1 = move-exception
            r1 = r0
            r2 = r0
        L_0x00ba:
            java.lang.String r0 = "MobileCoreReport | loadlogStackFile | Error parsing json "
            r3 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r3)     // Catch:{ all -> 0x00f4 }
            r0 = 0
            r8.f = r0     // Catch:{ all -> 0x00f4 }
            if (r2 == 0) goto L_0x00c9
            r2.close()     // Catch:{ IOException -> 0x00d0 }
        L_0x00c9:
            if (r1 == 0) goto L_0x003b
            r1.close()     // Catch:{ IOException -> 0x00d0 }
            goto L_0x003b
        L_0x00d0:
            r0 = move-exception
            java.lang.String r0 = "MobileCoreReport | loadlogStackFile | Error couldn't close file "
            goto L_0x009d
        L_0x00d4:
            r1 = move-exception
            r2 = r0
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x00d9:
            if (r2 == 0) goto L_0x00de
            r2.close()     // Catch:{ IOException -> 0x00e4 }
        L_0x00de:
            if (r1 == 0) goto L_0x00e3
            r1.close()     // Catch:{ IOException -> 0x00e4 }
        L_0x00e3:
            throw r0
        L_0x00e4:
            r1 = move-exception
            java.lang.String r1 = "MobileCoreReport | loadlogStackFile | Error couldn't close file "
            com.ironsource.mobilcore.C0031p.a(r1, r6)
            goto L_0x00e3
        L_0x00eb:
            r0 = move-exception
            java.lang.String r0 = "MobileCoreReport | loadlogStackFile | Error couldn't close file "
            goto L_0x009d
        L_0x00ef:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00d9
        L_0x00f4:
            r0 = move-exception
            goto L_0x00d9
        L_0x00f6:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r0
            r0 = r7
            goto L_0x00d9
        L_0x00fc:
            r1 = move-exception
            r1 = r0
            goto L_0x00ba
        L_0x00ff:
            r0 = move-exception
            goto L_0x00ba
        L_0x0101:
            r1 = move-exception
            r2 = r0
            r7 = r0
            r0 = r1
            r1 = r7
            goto L_0x0074
        L_0x0108:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0074
        L_0x010e:
            r1 = move-exception
            r1 = r0
            goto L_0x0027
        L_0x0112:
            r1 = move-exception
            r1 = r2
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.aC.a():void");
    }

    private boolean b(JSONObject jSONObject) {
        JSONArray jSONArray = new JSONArray();
        if (this.f != null) {
            jSONArray = this.f.optJSONArray("reportArr");
        }
        C0031p.a("MobileCoreReport | addToLogStack | adding to stack " + jSONObject.toString(), 55);
        jSONArray.put(jSONObject);
        try {
            if (this.f == null) {
                this.f = new JSONObject();
            }
            this.f.putOpt("reportArr", jSONArray);
            return true;
        } catch (JSONException e2) {
            C0031p.a("MobileCoreReport | addToLogStack | Couldn't add json object to array " + e2.getLocalizedMessage(), 55);
            return false;
        }
    }

    private void a(String str) throws Exception {
        StringEntity stringEntity;
        C0031p.a("MobileCoreReport | encryptAndSend | posting pre encrypt: " + str, 55);
        byte[] b2 = b(str);
        boolean z = b2 != null;
        if (z) {
            stringEntity = new StringEntity(new String(C0007a.a(b2, 0)));
        } else {
            stringEntity = new StringEntity(new String(str.getBytes()));
        }
        HttpPost httpPost = new HttpPost(C0017b.n("1%dm1%dos#gcs#g.s#ge1%dr1%dos#gcs#ge1%dls#gis#gb1%do1%dms#g.1%dn1%dos#gds#gis#ge1%ds1%do1%dps#g/s#g/s#g:1%dp1%dt1%dts#gh"));
        httpPost.setHeader("Content-type", "application/json");
        if (z) {
            httpPost.setHeader("x-energy-type", "penguin");
        }
        httpPost.setEntity(stringEntity);
        new DefaultHttpClient().execute(httpPost);
        this.d.deleteFile("log_stack.dat");
    }

    private byte[] b(String str) throws Exception {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(MessageDigest.getInstance("MD5").digest("TheFamousGrouse".getBytes("UTF-8")), "AES/ECB/PKCS7Padding");
            Cipher instance = Cipher.getInstance("AES/ECB/PKCS7Padding");
            instance.init(1, secretKeySpec);
            return instance.doFinal(str.getBytes());
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
