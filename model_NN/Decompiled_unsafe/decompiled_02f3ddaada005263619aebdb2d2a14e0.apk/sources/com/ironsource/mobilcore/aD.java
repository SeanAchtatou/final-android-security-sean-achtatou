package com.ironsource.mobilcore;

import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class aD {
    private int a;
    private String b;
    private String c;
    private String d;
    private String e;
    private HashMap<String, String> f = new HashMap<>();
    private String g;
    private String h;
    private String i;

    public aD(String str, String str2, int i2, String str3, String str4, String str5, double d2, String str6, String str7, JSONArray jSONArray, String str8) {
        this.i = str;
        this.b = str2;
        this.a = i2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.g = str6;
        this.h = str7;
        this.f.put("Name", this.b);
        this.f.put("Feed", this.c);
        this.f.put("Number", String.valueOf(this.a));
        this.f.put("Type", this.d);
        this.f.put("cpi", String.valueOf(d2));
        this.f.put("TransID", str8);
        if (jSONArray != null && jSONArray.length() != 0) {
            C0031p.a("extra: " + jSONArray.toString(), 55);
            a(jSONArray);
        }
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj instanceof aD) {
            return this.b.equals(((aD) obj).b);
        }
        return false;
    }

    private void a(JSONArray jSONArray) {
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i2);
            try {
                String string = optJSONObject.getString("type");
                String string2 = optJSONObject.getString("name");
                String string3 = optJSONObject.getString("value");
                if ("report".equals(string)) {
                    this.f.put(string2, string3);
                }
            } catch (JSONException e2) {
                C0031p.a("Error: " + e2.getLocalizedMessage(), 2);
            }
        }
    }

    public final String a() {
        return this.b;
    }

    public final int b() {
        return this.a;
    }

    public final String c() {
        return this.d;
    }

    public final String d() {
        return this.e;
    }

    public final String e() {
        return this.g;
    }

    public final String f() {
        return new JSONObject(this.f).toString();
    }

    public final String g() {
        return this.h;
    }

    public final String toString() {
        return f();
    }

    public final void a(int i2) {
        this.f.put("impressionResult", String.valueOf(i2));
    }

    public final String h() {
        return this.i;
    }
}
