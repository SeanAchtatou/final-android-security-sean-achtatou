package com.ironsource.mobilcore;

public interface IOnOfferwallReadyListener {
    void onOfferWallReady();
}
