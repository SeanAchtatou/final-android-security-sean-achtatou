package com.ironsource.mobilcore;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

class MCConnectivityReceiver extends BroadcastReceiver {
    MCConnectivityReceiver() {
    }

    public void onReceive(Context context, Intent intent) {
        boolean z;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            z = activeNetworkInfo.isConnectedOrConnecting();
        } else {
            z = false;
        }
        if (z) {
            am.a().b();
        }
    }
}
