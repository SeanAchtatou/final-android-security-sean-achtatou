package com.ironsource.mobilcore;

import android.app.Activity;
import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;

abstract class V extends U {
    V(Activity activity, int i) {
        super(activity, i);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        if (mode == 1073741824 && mode2 == 1073741824) {
            int size = View.MeasureSpec.getSize(i);
            int size2 = View.MeasureSpec.getSize(i2);
            if (!this.v) {
                this.u = (int) (((float) size) * 0.8f);
            }
            if (this.c == -1.0f) {
                c((float) this.u);
            }
            this.s.measure(getChildMeasureSpec(i, 0, this.u), getChildMeasureSpec(i, 0, size2));
            this.t.measure(getChildMeasureSpec(i, 0, size), getChildMeasureSpec(i, 0, size2));
            setMeasuredDimension(size, size2);
            f();
            return;
        }
        throw new IllegalStateException("Must measure with an exact size");
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        int i;
        int action = motionEvent.getAction() & MotionEventCompat.ACTION_MASK;
        if (action == 0 && this.w && j()) {
            c(0.0f);
            g();
            i();
            e(0);
        }
        if (this.w && a(motionEvent)) {
            return true;
        }
        if (this.A == 0) {
            return false;
        }
        if (action != 0 && this.d) {
            return true;
        }
        switch (action) {
            case 0:
                float x = motionEvent.getX();
                this.e = x;
                this.g = x;
                float y = motionEvent.getY();
                this.f = y;
                this.h = y;
                if (b()) {
                    if (this.w) {
                        i = 8;
                    } else {
                        i = 0;
                    }
                    e(i);
                    g();
                    i();
                    this.d = false;
                    break;
                }
                break;
            case 1:
            case 3:
                if (Math.abs(this.c) <= ((float) (this.u / 2))) {
                    b(true);
                    break;
                } else {
                    a(true);
                    break;
                }
            case 2:
                float x2 = motionEvent.getX();
                float f = x2 - this.g;
                float abs = Math.abs(f);
                float y2 = motionEvent.getY();
                float abs2 = Math.abs(y2 - this.h);
                if (abs > ((float) this.b) && abs > abs2 && a(f)) {
                    e(2);
                    this.d = true;
                    this.g = x2;
                    this.h = y2;
                    break;
                }
        }
        if (this.j == null) {
            this.j = VelocityTracker.obtain();
        }
        this.j.addMovement(motionEvent);
        return this.d;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f;
        if (!this.w && this.A == 0) {
            return false;
        }
        int action = motionEvent.getAction() & MotionEventCompat.ACTION_MASK;
        if (this.j == null) {
            this.j = VelocityTracker.obtain();
        }
        this.j.addMovement(motionEvent);
        switch (action) {
            case 0:
                float x = motionEvent.getX();
                this.e = x;
                this.g = x;
                float y = motionEvent.getY();
                this.f = y;
                this.h = y;
                if (b()) {
                    g();
                    i();
                    e();
                    break;
                }
                break;
            case 1:
            case 3:
                b(motionEvent);
                break;
            case 2:
                if (!this.d) {
                    float x2 = motionEvent.getX();
                    float f2 = x2 - this.g;
                    float abs = Math.abs(f2);
                    float abs2 = Math.abs(motionEvent.getY() - this.h);
                    if (abs > ((float) this.b) && abs > abs2 && a(f2)) {
                        e(2);
                        this.d = true;
                        if (x2 - this.e > 0.0f) {
                            f = this.e + ((float) this.b);
                        } else {
                            f = this.e - ((float) this.b);
                        }
                        this.g = f;
                    }
                }
                if (this.d) {
                    e();
                    float x3 = motionEvent.getX();
                    this.g = x3;
                    b(x3 - this.g);
                    break;
                }
                break;
        }
        return true;
    }
}
