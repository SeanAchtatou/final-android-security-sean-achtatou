package com.ironsource.mobilcore;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;

final class R extends C0015ah {
    R(Activity activity, int i) {
        super(activity, i, C0009ab.BOTTOM);
    }

    public final void a(int i) {
        this.p = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-16777216, 0});
        invalidate();
    }
}
