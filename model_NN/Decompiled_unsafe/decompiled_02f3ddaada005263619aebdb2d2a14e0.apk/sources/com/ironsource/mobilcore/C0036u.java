package com.ironsource.mobilcore;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.ironsource.mobilcore.u  reason: case insensitive filesystem */
abstract class C0036u {
    private int a;
    private View b;
    protected Context c;
    protected ao d;
    protected Activity e;
    protected int f;
    protected View g;
    protected boolean h;
    private N i;
    private String j;
    /* access modifiers changed from: private */
    public String k;
    private a l = a.HIDDEN;

    /* renamed from: com.ironsource.mobilcore.u$a */
    public enum a {
        VISIBLE,
        HIDDEN
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public abstract void a(JSONObject jSONObject) throws JSONException;

    /* access modifiers changed from: protected */
    public abstract void b();

    /* access modifiers changed from: protected */
    public abstract void c();

    public abstract String e();

    public C0036u(Context context, ao aoVar) {
        this.c = context;
        this.d = aoVar;
        this.a = 1;
        this.f = -1;
        a();
        this.b = new LinearLayout(this.c);
        LinearLayout linearLayout = (LinearLayout) this.b;
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(1);
        linearLayout.addView(this.g);
        if (f()) {
            this.d.a(linearLayout);
        }
    }

    public final String g() {
        return this.j;
    }

    public final View h() {
        return this.b;
    }

    public final boolean i() {
        return this.b.getVisibility() == 0;
    }

    public final void a(boolean z) {
        while (true) {
            this.b.setVisibility(z ? 0 : 8);
            if (this.i != null) {
                this = this.i;
            } else {
                return;
            }
        }
    }

    public void a(Activity activity) {
        this.e = activity;
    }

    public void a(int i2) {
        this.f = i2;
    }

    public final void a(N n) {
        this.i = n;
        if (this.i != null && !i()) {
            this.i.a(false);
        }
    }

    public void a(a aVar) {
        C0031p.a("MCBaseWidget | notifyVisibilityState() | new visibility State is :" + aVar.toString(), 55);
        this.l = aVar;
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject, boolean z) throws JSONException {
        this.j = jSONObject.optString("id", "");
        this.h = jSONObject.optBoolean("closeSliderOnClick", true);
        JSONObject optJSONObject = jSONObject.optJSONObject("events");
        if (optJSONObject != null) {
            this.k = optJSONObject.optString("onClick", "");
        }
        this.g.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                C0036u.this.a(view);
                if (!TextUtils.isEmpty(C0036u.this.k)) {
                    C0036u.this.a(C0036u.this.k);
                }
                C0036u.this.a(new C0038w("type", C0036u.this.e()), new C0038w("position", C0036u.this.f >= 0 ? String.valueOf(C0036u.this.f) : null));
            }
        });
        if (z) {
            b();
            if (this instanceof C0041z) {
                ((C0041z) this).p();
            }
            c();
        }
    }

    /* access modifiers changed from: protected */
    public final int j() {
        int i2 = this.a;
        this.a = i2 + 1;
        return i2;
    }

    /* access modifiers changed from: protected */
    public void a(C0038w... wVarArr) {
        C0031p.a(this.c, "slider", "widget", "click", wVarArr);
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        Intent intent = new Intent(MCISliderAPI.ACTION_WIDGET_CALLBACK);
        intent.putExtra(MCISliderAPI.EXTRA_CALLBACK_ID, str);
        this.c.sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    public final void k() {
        if (this.h) {
            am.a().closeSliderMenu(true);
        }
    }

    /* access modifiers changed from: protected */
    public final a l() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
    }

    /* access modifiers changed from: protected */
    public void m() {
    }
}
