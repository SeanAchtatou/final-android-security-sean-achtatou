package com.ironsource.mobilcore;

public interface CallbackResponse {

    public enum TYPE {
        AGREEMENT_BACK,
        AGREEMENT_DECLINE,
        AGREEMENT_AGREE,
        OFFERWALL_BACK,
        OFFERWALL_QUIT,
        OFFERWALL_NO_CONNECTION
    }

    void onConfirmation(TYPE type);
}
