package com.ironsource.mobilcore;

import android.support.v4.view.accessibility.AccessibilityEventCompat;

/* renamed from: com.ironsource.mobilcore.a  reason: case insensitive filesystem */
public class C0007a {
    private static /* synthetic */ boolean a = (!C0007a.class.desiredAssertionStatus());

    /* renamed from: com.ironsource.mobilcore.a$a  reason: collision with other inner class name */
    static abstract class C0005a {
        public byte[] a;
        public int b;

        C0005a() {
        }
    }

    public static byte[] a(String str, int i) {
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        b bVar = new b(0, new byte[((length * 3) / 4)]);
        if (!bVar.a(bytes, 0, length, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (bVar.b == bVar.a.length) {
            return bVar.a;
        } else {
            byte[] bArr = new byte[bVar.b];
            System.arraycopy(bVar.a, 0, bArr, 0, bVar.b);
            return bArr;
        }
    }

    /* renamed from: com.ironsource.mobilcore.a$b */
    static class b extends C0005a {
        private static final int[] c;
        private static final int[] d;
        private int e;
        private int f;
        private final int[] g;

        static {
            int[] iArr = new int[AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT];
            // fill-array-data instruction
            iArr[0] = -1;
            iArr[1] = -1;
            iArr[2] = -1;
            iArr[3] = -1;
            iArr[4] = -1;
            iArr[5] = -1;
            iArr[6] = -1;
            iArr[7] = -1;
            iArr[8] = -1;
            iArr[9] = -1;
            iArr[10] = -1;
            iArr[11] = -1;
            iArr[12] = -1;
            iArr[13] = -1;
            iArr[14] = -1;
            iArr[15] = -1;
            iArr[16] = -1;
            iArr[17] = -1;
            iArr[18] = -1;
            iArr[19] = -1;
            iArr[20] = -1;
            iArr[21] = -1;
            iArr[22] = -1;
            iArr[23] = -1;
            iArr[24] = -1;
            iArr[25] = -1;
            iArr[26] = -1;
            iArr[27] = -1;
            iArr[28] = -1;
            iArr[29] = -1;
            iArr[30] = -1;
            iArr[31] = -1;
            iArr[32] = -1;
            iArr[33] = -1;
            iArr[34] = -1;
            iArr[35] = -1;
            iArr[36] = -1;
            iArr[37] = -1;
            iArr[38] = -1;
            iArr[39] = -1;
            iArr[40] = -1;
            iArr[41] = -1;
            iArr[42] = -1;
            iArr[43] = 62;
            iArr[44] = -1;
            iArr[45] = -1;
            iArr[46] = -1;
            iArr[47] = 63;
            iArr[48] = 52;
            iArr[49] = 53;
            iArr[50] = 54;
            iArr[51] = 55;
            iArr[52] = 56;
            iArr[53] = 57;
            iArr[54] = 58;
            iArr[55] = 59;
            iArr[56] = 60;
            iArr[57] = 61;
            iArr[58] = -1;
            iArr[59] = -1;
            iArr[60] = -1;
            iArr[61] = -2;
            iArr[62] = -1;
            iArr[63] = -1;
            iArr[64] = -1;
            iArr[65] = 0;
            iArr[66] = 1;
            iArr[67] = 2;
            iArr[68] = 3;
            iArr[69] = 4;
            iArr[70] = 5;
            iArr[71] = 6;
            iArr[72] = 7;
            iArr[73] = 8;
            iArr[74] = 9;
            iArr[75] = 10;
            iArr[76] = 11;
            iArr[77] = 12;
            iArr[78] = 13;
            iArr[79] = 14;
            iArr[80] = 15;
            iArr[81] = 16;
            iArr[82] = 17;
            iArr[83] = 18;
            iArr[84] = 19;
            iArr[85] = 20;
            iArr[86] = 21;
            iArr[87] = 22;
            iArr[88] = 23;
            iArr[89] = 24;
            iArr[90] = 25;
            iArr[91] = -1;
            iArr[92] = -1;
            iArr[93] = -1;
            iArr[94] = -1;
            iArr[95] = -1;
            iArr[96] = -1;
            iArr[97] = 26;
            iArr[98] = 27;
            iArr[99] = 28;
            iArr[100] = 29;
            iArr[101] = 30;
            iArr[102] = 31;
            iArr[103] = 32;
            iArr[104] = 33;
            iArr[105] = 34;
            iArr[106] = 35;
            iArr[107] = 36;
            iArr[108] = 37;
            iArr[109] = 38;
            iArr[110] = 39;
            iArr[111] = 40;
            iArr[112] = 41;
            iArr[113] = 42;
            iArr[114] = 43;
            iArr[115] = 44;
            iArr[116] = 45;
            iArr[117] = 46;
            iArr[118] = 47;
            iArr[119] = 48;
            iArr[120] = 49;
            iArr[121] = 50;
            iArr[122] = 51;
            iArr[123] = -1;
            iArr[124] = -1;
            iArr[125] = -1;
            iArr[126] = -1;
            iArr[127] = -1;
            iArr[128] = -1;
            iArr[129] = -1;
            iArr[130] = -1;
            iArr[131] = -1;
            iArr[132] = -1;
            iArr[133] = -1;
            iArr[134] = -1;
            iArr[135] = -1;
            iArr[136] = -1;
            iArr[137] = -1;
            iArr[138] = -1;
            iArr[139] = -1;
            iArr[140] = -1;
            iArr[141] = -1;
            iArr[142] = -1;
            iArr[143] = -1;
            iArr[144] = -1;
            iArr[145] = -1;
            iArr[146] = -1;
            iArr[147] = -1;
            iArr[148] = -1;
            iArr[149] = -1;
            iArr[150] = -1;
            iArr[151] = -1;
            iArr[152] = -1;
            iArr[153] = -1;
            iArr[154] = -1;
            iArr[155] = -1;
            iArr[156] = -1;
            iArr[157] = -1;
            iArr[158] = -1;
            iArr[159] = -1;
            iArr[160] = -1;
            iArr[161] = -1;
            iArr[162] = -1;
            iArr[163] = -1;
            iArr[164] = -1;
            iArr[165] = -1;
            iArr[166] = -1;
            iArr[167] = -1;
            iArr[168] = -1;
            iArr[169] = -1;
            iArr[170] = -1;
            iArr[171] = -1;
            iArr[172] = -1;
            iArr[173] = -1;
            iArr[174] = -1;
            iArr[175] = -1;
            iArr[176] = -1;
            iArr[177] = -1;
            iArr[178] = -1;
            iArr[179] = -1;
            iArr[180] = -1;
            iArr[181] = -1;
            iArr[182] = -1;
            iArr[183] = -1;
            iArr[184] = -1;
            iArr[185] = -1;
            iArr[186] = -1;
            iArr[187] = -1;
            iArr[188] = -1;
            iArr[189] = -1;
            iArr[190] = -1;
            iArr[191] = -1;
            iArr[192] = -1;
            iArr[193] = -1;
            iArr[194] = -1;
            iArr[195] = -1;
            iArr[196] = -1;
            iArr[197] = -1;
            iArr[198] = -1;
            iArr[199] = -1;
            iArr[200] = -1;
            iArr[201] = -1;
            iArr[202] = -1;
            iArr[203] = -1;
            iArr[204] = -1;
            iArr[205] = -1;
            iArr[206] = -1;
            iArr[207] = -1;
            iArr[208] = -1;
            iArr[209] = -1;
            iArr[210] = -1;
            iArr[211] = -1;
            iArr[212] = -1;
            iArr[213] = -1;
            iArr[214] = -1;
            iArr[215] = -1;
            iArr[216] = -1;
            iArr[217] = -1;
            iArr[218] = -1;
            iArr[219] = -1;
            iArr[220] = -1;
            iArr[221] = -1;
            iArr[222] = -1;
            iArr[223] = -1;
            iArr[224] = -1;
            iArr[225] = -1;
            iArr[226] = -1;
            iArr[227] = -1;
            iArr[228] = -1;
            iArr[229] = -1;
            iArr[230] = -1;
            iArr[231] = -1;
            iArr[232] = -1;
            iArr[233] = -1;
            iArr[234] = -1;
            iArr[235] = -1;
            iArr[236] = -1;
            iArr[237] = -1;
            iArr[238] = -1;
            iArr[239] = -1;
            iArr[240] = -1;
            iArr[241] = -1;
            iArr[242] = -1;
            iArr[243] = -1;
            iArr[244] = -1;
            iArr[245] = -1;
            iArr[246] = -1;
            iArr[247] = -1;
            iArr[248] = -1;
            iArr[249] = -1;
            iArr[250] = -1;
            iArr[251] = -1;
            iArr[252] = -1;
            iArr[253] = -1;
            iArr[254] = -1;
            iArr[255] = -1;
            c = iArr;
            int[] iArr2 = new int[AccessibilityEventCompat.TYPE_VIEW_HOVER_EXIT];
            // fill-array-data instruction
            iArr2[0] = -1;
            iArr2[1] = -1;
            iArr2[2] = -1;
            iArr2[3] = -1;
            iArr2[4] = -1;
            iArr2[5] = -1;
            iArr2[6] = -1;
            iArr2[7] = -1;
            iArr2[8] = -1;
            iArr2[9] = -1;
            iArr2[10] = -1;
            iArr2[11] = -1;
            iArr2[12] = -1;
            iArr2[13] = -1;
            iArr2[14] = -1;
            iArr2[15] = -1;
            iArr2[16] = -1;
            iArr2[17] = -1;
            iArr2[18] = -1;
            iArr2[19] = -1;
            iArr2[20] = -1;
            iArr2[21] = -1;
            iArr2[22] = -1;
            iArr2[23] = -1;
            iArr2[24] = -1;
            iArr2[25] = -1;
            iArr2[26] = -1;
            iArr2[27] = -1;
            iArr2[28] = -1;
            iArr2[29] = -1;
            iArr2[30] = -1;
            iArr2[31] = -1;
            iArr2[32] = -1;
            iArr2[33] = -1;
            iArr2[34] = -1;
            iArr2[35] = -1;
            iArr2[36] = -1;
            iArr2[37] = -1;
            iArr2[38] = -1;
            iArr2[39] = -1;
            iArr2[40] = -1;
            iArr2[41] = -1;
            iArr2[42] = -1;
            iArr2[43] = -1;
            iArr2[44] = -1;
            iArr2[45] = 62;
            iArr2[46] = -1;
            iArr2[47] = -1;
            iArr2[48] = 52;
            iArr2[49] = 53;
            iArr2[50] = 54;
            iArr2[51] = 55;
            iArr2[52] = 56;
            iArr2[53] = 57;
            iArr2[54] = 58;
            iArr2[55] = 59;
            iArr2[56] = 60;
            iArr2[57] = 61;
            iArr2[58] = -1;
            iArr2[59] = -1;
            iArr2[60] = -1;
            iArr2[61] = -2;
            iArr2[62] = -1;
            iArr2[63] = -1;
            iArr2[64] = -1;
            iArr2[65] = 0;
            iArr2[66] = 1;
            iArr2[67] = 2;
            iArr2[68] = 3;
            iArr2[69] = 4;
            iArr2[70] = 5;
            iArr2[71] = 6;
            iArr2[72] = 7;
            iArr2[73] = 8;
            iArr2[74] = 9;
            iArr2[75] = 10;
            iArr2[76] = 11;
            iArr2[77] = 12;
            iArr2[78] = 13;
            iArr2[79] = 14;
            iArr2[80] = 15;
            iArr2[81] = 16;
            iArr2[82] = 17;
            iArr2[83] = 18;
            iArr2[84] = 19;
            iArr2[85] = 20;
            iArr2[86] = 21;
            iArr2[87] = 22;
            iArr2[88] = 23;
            iArr2[89] = 24;
            iArr2[90] = 25;
            iArr2[91] = -1;
            iArr2[92] = -1;
            iArr2[93] = -1;
            iArr2[94] = -1;
            iArr2[95] = 63;
            iArr2[96] = -1;
            iArr2[97] = 26;
            iArr2[98] = 27;
            iArr2[99] = 28;
            iArr2[100] = 29;
            iArr2[101] = 30;
            iArr2[102] = 31;
            iArr2[103] = 32;
            iArr2[104] = 33;
            iArr2[105] = 34;
            iArr2[106] = 35;
            iArr2[107] = 36;
            iArr2[108] = 37;
            iArr2[109] = 38;
            iArr2[110] = 39;
            iArr2[111] = 40;
            iArr2[112] = 41;
            iArr2[113] = 42;
            iArr2[114] = 43;
            iArr2[115] = 44;
            iArr2[116] = 45;
            iArr2[117] = 46;
            iArr2[118] = 47;
            iArr2[119] = 48;
            iArr2[120] = 49;
            iArr2[121] = 50;
            iArr2[122] = 51;
            iArr2[123] = -1;
            iArr2[124] = -1;
            iArr2[125] = -1;
            iArr2[126] = -1;
            iArr2[127] = -1;
            iArr2[128] = -1;
            iArr2[129] = -1;
            iArr2[130] = -1;
            iArr2[131] = -1;
            iArr2[132] = -1;
            iArr2[133] = -1;
            iArr2[134] = -1;
            iArr2[135] = -1;
            iArr2[136] = -1;
            iArr2[137] = -1;
            iArr2[138] = -1;
            iArr2[139] = -1;
            iArr2[140] = -1;
            iArr2[141] = -1;
            iArr2[142] = -1;
            iArr2[143] = -1;
            iArr2[144] = -1;
            iArr2[145] = -1;
            iArr2[146] = -1;
            iArr2[147] = -1;
            iArr2[148] = -1;
            iArr2[149] = -1;
            iArr2[150] = -1;
            iArr2[151] = -1;
            iArr2[152] = -1;
            iArr2[153] = -1;
            iArr2[154] = -1;
            iArr2[155] = -1;
            iArr2[156] = -1;
            iArr2[157] = -1;
            iArr2[158] = -1;
            iArr2[159] = -1;
            iArr2[160] = -1;
            iArr2[161] = -1;
            iArr2[162] = -1;
            iArr2[163] = -1;
            iArr2[164] = -1;
            iArr2[165] = -1;
            iArr2[166] = -1;
            iArr2[167] = -1;
            iArr2[168] = -1;
            iArr2[169] = -1;
            iArr2[170] = -1;
            iArr2[171] = -1;
            iArr2[172] = -1;
            iArr2[173] = -1;
            iArr2[174] = -1;
            iArr2[175] = -1;
            iArr2[176] = -1;
            iArr2[177] = -1;
            iArr2[178] = -1;
            iArr2[179] = -1;
            iArr2[180] = -1;
            iArr2[181] = -1;
            iArr2[182] = -1;
            iArr2[183] = -1;
            iArr2[184] = -1;
            iArr2[185] = -1;
            iArr2[186] = -1;
            iArr2[187] = -1;
            iArr2[188] = -1;
            iArr2[189] = -1;
            iArr2[190] = -1;
            iArr2[191] = -1;
            iArr2[192] = -1;
            iArr2[193] = -1;
            iArr2[194] = -1;
            iArr2[195] = -1;
            iArr2[196] = -1;
            iArr2[197] = -1;
            iArr2[198] = -1;
            iArr2[199] = -1;
            iArr2[200] = -1;
            iArr2[201] = -1;
            iArr2[202] = -1;
            iArr2[203] = -1;
            iArr2[204] = -1;
            iArr2[205] = -1;
            iArr2[206] = -1;
            iArr2[207] = -1;
            iArr2[208] = -1;
            iArr2[209] = -1;
            iArr2[210] = -1;
            iArr2[211] = -1;
            iArr2[212] = -1;
            iArr2[213] = -1;
            iArr2[214] = -1;
            iArr2[215] = -1;
            iArr2[216] = -1;
            iArr2[217] = -1;
            iArr2[218] = -1;
            iArr2[219] = -1;
            iArr2[220] = -1;
            iArr2[221] = -1;
            iArr2[222] = -1;
            iArr2[223] = -1;
            iArr2[224] = -1;
            iArr2[225] = -1;
            iArr2[226] = -1;
            iArr2[227] = -1;
            iArr2[228] = -1;
            iArr2[229] = -1;
            iArr2[230] = -1;
            iArr2[231] = -1;
            iArr2[232] = -1;
            iArr2[233] = -1;
            iArr2[234] = -1;
            iArr2[235] = -1;
            iArr2[236] = -1;
            iArr2[237] = -1;
            iArr2[238] = -1;
            iArr2[239] = -1;
            iArr2[240] = -1;
            iArr2[241] = -1;
            iArr2[242] = -1;
            iArr2[243] = -1;
            iArr2[244] = -1;
            iArr2[245] = -1;
            iArr2[246] = -1;
            iArr2[247] = -1;
            iArr2[248] = -1;
            iArr2[249] = -1;
            iArr2[250] = -1;
            iArr2[251] = -1;
            iArr2[252] = -1;
            iArr2[253] = -1;
            iArr2[254] = -1;
            iArr2[255] = -1;
            d = iArr2;
        }

        public b(int i, byte[] bArr) {
            this.a = bArr;
            this.g = (i & 8) == 0 ? c : d;
            this.e = 0;
            this.f = 0;
        }

        /* JADX WARNING: Removed duplicated region for block: B:53:0x0113  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x0119  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x0122  */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x0131  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean a(byte[] r10, int r11, int r12, boolean r13) {
            /*
                r9 = this;
                int r0 = r9.e
                r1 = 6
                if (r0 != r1) goto L_0x0007
                r0 = 0
            L_0x0006:
                return r0
            L_0x0007:
                int r4 = r12 + r11
                int r2 = r9.e
                int r1 = r9.f
                r0 = 0
                byte[] r5 = r9.a
                int[] r6 = r9.g
                r3 = r2
                r2 = r11
            L_0x0014:
                if (r2 >= r4) goto L_0x0108
                if (r3 != 0) goto L_0x005d
            L_0x0018:
                int r7 = r2 + 4
                if (r7 > r4) goto L_0x005b
                byte r1 = r10[r2]
                r1 = r1 & 255(0xff, float:3.57E-43)
                r1 = r6[r1]
                int r1 = r1 << 18
                int r7 = r2 + 1
                byte r7 = r10[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                int r7 = r7 << 12
                r1 = r1 | r7
                int r7 = r2 + 2
                byte r7 = r10[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                int r7 = r7 << 6
                r1 = r1 | r7
                int r7 = r2 + 3
                byte r7 = r10[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                r1 = r1 | r7
                if (r1 < 0) goto L_0x005b
                int r7 = r0 + 2
                byte r8 = (byte) r1
                r5[r7] = r8
                int r7 = r0 + 1
                int r8 = r1 >> 8
                byte r8 = (byte) r8
                r5[r7] = r8
                int r7 = r1 >> 16
                byte r7 = (byte) r7
                r5[r0] = r7
                int r0 = r0 + 3
                int r2 = r2 + 4
                goto L_0x0018
            L_0x005b:
                if (r2 >= r4) goto L_0x0108
            L_0x005d:
                int r11 = r2 + 1
                byte r2 = r10[r2]
                r2 = r2 & 255(0xff, float:3.57E-43)
                r2 = r6[r2]
                switch(r3) {
                    case 0: goto L_0x006a;
                    case 1: goto L_0x007a;
                    case 2: goto L_0x008d;
                    case 3: goto L_0x00b1;
                    case 4: goto L_0x00ed;
                    case 5: goto L_0x00ff;
                    default: goto L_0x0068;
                }
            L_0x0068:
                r2 = r11
                goto L_0x0014
            L_0x006a:
                if (r2 < 0) goto L_0x0072
                int r1 = r3 + 1
                r3 = r1
                r1 = r2
                r2 = r11
                goto L_0x0014
            L_0x0072:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x007a:
                if (r2 < 0) goto L_0x0084
                int r1 = r1 << 6
                r1 = r1 | r2
                int r2 = r3 + 1
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x0084:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x008d:
                if (r2 < 0) goto L_0x0098
                int r1 = r1 << 6
                r1 = r1 | r2
                int r2 = r3 + 1
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x0098:
                r7 = -2
                if (r2 != r7) goto L_0x00a8
                int r2 = r0 + 1
                int r3 = r1 >> 4
                byte r3 = (byte) r3
                r5[r0] = r3
                r0 = 4
                r3 = r0
                r0 = r2
                r2 = r11
                goto L_0x0014
            L_0x00a8:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x00b1:
                if (r2 < 0) goto L_0x00ce
                int r1 = r1 << 6
                r1 = r1 | r2
                int r2 = r0 + 2
                byte r3 = (byte) r1
                r5[r2] = r3
                int r2 = r0 + 1
                int r3 = r1 >> 8
                byte r3 = (byte) r3
                r5[r2] = r3
                int r2 = r1 >> 16
                byte r2 = (byte) r2
                r5[r0] = r2
                int r0 = r0 + 3
                r2 = 0
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x00ce:
                r7 = -2
                if (r2 != r7) goto L_0x00e4
                int r2 = r0 + 1
                int r3 = r1 >> 2
                byte r3 = (byte) r3
                r5[r2] = r3
                int r2 = r1 >> 10
                byte r2 = (byte) r2
                r5[r0] = r2
                int r0 = r0 + 2
                r2 = 5
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x00e4:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x00ed:
                r7 = -2
                if (r2 != r7) goto L_0x00f6
                int r2 = r3 + 1
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x00f6:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x00ff:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0108:
                r2 = r1
                switch(r3) {
                    case 0: goto L_0x010c;
                    case 1: goto L_0x0113;
                    case 2: goto L_0x0119;
                    case 3: goto L_0x0122;
                    case 4: goto L_0x0131;
                    default: goto L_0x010c;
                }
            L_0x010c:
                r9.e = r3
                r9.b = r0
                r0 = 1
                goto L_0x0006
            L_0x0113:
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0119:
                int r1 = r0 + 1
                int r2 = r2 >> 4
                byte r2 = (byte) r2
                r5[r0] = r2
                r0 = r1
                goto L_0x010c
            L_0x0122:
                int r1 = r0 + 1
                int r4 = r2 >> 10
                byte r4 = (byte) r4
                r5[r0] = r4
                int r0 = r1 + 1
                int r2 = r2 >> 2
                byte r2 = (byte) r2
                r5[r1] = r2
                goto L_0x010c
            L_0x0131:
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.C0007a.b.a(byte[], int, int, boolean):boolean");
        }
    }

    public static byte[] a(byte[] bArr, int i) {
        int length = bArr.length;
        c cVar = new c(0, null);
        int i2 = (length / 3) << 2;
        if (!cVar.c) {
            switch (length % 3) {
                case 1:
                    i2 += 2;
                    break;
                case 2:
                    i2 += 3;
                    break;
            }
        } else if (length % 3 > 0) {
            i2 += 4;
        }
        if (cVar.d && length > 0) {
            i2 += (cVar.e ? 2 : 1) * (((length - 1) / 57) + 1);
        }
        cVar.a = new byte[i2];
        cVar.a(bArr, 0, length, true);
        if (a || cVar.b == i2) {
            return cVar.a;
        }
        throw new AssertionError();
    }

    /* renamed from: com.ironsource.mobilcore.a$c */
    static class c extends C0005a {
        private static final byte[] f = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        private static final byte[] g = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        private static /* synthetic */ boolean l;
        public final boolean c;
        public final boolean d;
        public final boolean e;
        private final byte[] h;
        private int i;
        private int j;
        private final byte[] k;

        static {
            boolean z;
            if (!C0007a.class.desiredAssertionStatus()) {
                z = true;
            } else {
                z = false;
            }
            l = z;
        }

        public c(int i2, byte[] bArr) {
            boolean z;
            boolean z2 = true;
            this.a = null;
            this.c = (i2 & 1) == 0;
            if ((i2 & 2) == 0) {
                z = true;
            } else {
                z = false;
            }
            this.d = z;
            this.e = (i2 & 4) == 0 ? false : z2;
            this.k = (i2 & 8) == 0 ? f : g;
            this.h = new byte[2];
            this.i = 0;
            this.j = this.d ? 19 : -1;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public final boolean a(byte[] bArr, int i2, int i3, boolean z) {
            int i4;
            byte b;
            int i5;
            int i6;
            int i7;
            int i8;
            byte b2;
            int i9;
            byte b3;
            int i10;
            byte b4;
            int i11;
            int i12;
            int i13;
            byte[] bArr2 = this.k;
            byte[] bArr3 = this.a;
            int i14 = 0;
            int i15 = this.j;
            int i16 = i3 + i2;
            switch (this.i) {
                case 0:
                    b = -1;
                    i4 = i2;
                    break;
                case 1:
                    if (i2 + 2 <= i16) {
                        int i17 = i2 + 1;
                        this.i = 0;
                        b = ((this.h[0] & 255) << 16) | ((bArr[i2] & 255) << 8) | (bArr[i17] & 255);
                        i4 = i17 + 1;
                        break;
                    }
                    b = -1;
                    i4 = i2;
                    break;
                case 2:
                    if (i2 + 1 <= i16) {
                        byte b5 = ((this.h[0] & 255) << 16) | ((this.h[1] & 255) << 8);
                        i4 = i2 + 1;
                        this.i = 0;
                        b = b5 | (bArr[i2] & 255);
                        break;
                    }
                    b = -1;
                    i4 = i2;
                    break;
                default:
                    b = -1;
                    i4 = i2;
                    break;
            }
            if (b != -1) {
                bArr3[0] = bArr2[(b >> 18) & 63];
                bArr3[1] = bArr2[(b >> 12) & 63];
                bArr3[2] = bArr2[(b >> 6) & 63];
                int i18 = 4;
                bArr3[3] = bArr2[b & 63];
                int i19 = i15 - 1;
                if (i19 == 0) {
                    if (this.e) {
                        i18 = 5;
                        bArr3[4] = 13;
                    }
                    i14 = i18 + 1;
                    bArr3[i18] = 10;
                    i6 = 19;
                } else {
                    i6 = i19;
                    i14 = 4;
                }
            } else {
                i6 = i15;
            }
            while (i5 + 3 <= i16) {
                byte b6 = ((bArr[i5] & 255) << 16) | ((bArr[i5 + 1] & 255) << 8) | (bArr[i5 + 2] & 255);
                bArr3[i7] = bArr2[(b6 >> 18) & 63];
                bArr3[i7 + 1] = bArr2[(b6 >> 12) & 63];
                bArr3[i7 + 2] = bArr2[(b6 >> 6) & 63];
                bArr3[i7 + 3] = bArr2[b6 & 63];
                int i20 = i5 + 3;
                int i21 = i7 + 4;
                int i22 = i6 - 1;
                if (i22 == 0) {
                    if (this.e) {
                        i13 = i21 + 1;
                        bArr3[i21] = 13;
                    } else {
                        i13 = i21;
                    }
                    i7 = i13 + 1;
                    bArr3[i13] = 10;
                    i5 = i20;
                    i12 = 19;
                } else {
                    i12 = i22;
                    i7 = i21;
                    i5 = i20;
                }
            }
            if (i5 - this.i == i16 - 1) {
                if (this.i > 0) {
                    i11 = 1;
                    b4 = this.h[0];
                } else {
                    b4 = bArr[i5];
                    i5++;
                    i11 = 0;
                }
                int i23 = (b4 & 255) << 4;
                this.i -= i11;
                int i24 = i7 + 1;
                bArr3[i7] = bArr2[(i23 >> 6) & 63];
                int i25 = i24 + 1;
                bArr3[i24] = bArr2[i23 & 63];
                if (this.c) {
                    int i26 = i25 + 1;
                    bArr3[i25] = 61;
                    i25 = i26 + 1;
                    bArr3[i26] = 61;
                }
                if (this.d) {
                    if (this.e) {
                        bArr3[i25] = 13;
                        i25++;
                    }
                    bArr3[i25] = 10;
                    i25++;
                }
                i7 = i25;
            } else if (i5 - this.i == i16 - 2) {
                if (this.i > 1) {
                    i9 = 1;
                    b2 = this.h[0];
                } else {
                    b2 = bArr[i5];
                    i5++;
                    i9 = 0;
                }
                int i27 = (b2 & 255) << 10;
                if (this.i > 0) {
                    b3 = this.h[i9];
                    i9++;
                } else {
                    b3 = bArr[i5];
                    i5++;
                }
                int i28 = ((b3 & 255) << 2) | i27;
                this.i -= i9;
                int i29 = i7 + 1;
                bArr3[i7] = bArr2[(i28 >> 12) & 63];
                int i30 = i29 + 1;
                bArr3[i29] = bArr2[(i28 >> 6) & 63];
                int i31 = i30 + 1;
                bArr3[i30] = bArr2[i28 & 63];
                if (this.c) {
                    i10 = i31 + 1;
                    bArr3[i31] = 61;
                } else {
                    i10 = i31;
                }
                if (this.d) {
                    if (this.e) {
                        bArr3[i10] = 13;
                        i10++;
                    }
                    bArr3[i10] = 10;
                    i10++;
                }
                i7 = i10;
            } else if (this.d && i7 > 0 && i6 != 19) {
                if (this.e) {
                    i8 = i7 + 1;
                    bArr3[i7] = 13;
                } else {
                    i8 = i7;
                }
                i7 = i8 + 1;
                bArr3[i8] = 10;
            }
            if (!l && this.i != 0) {
                throw new AssertionError();
            } else if (l || i5 == i16) {
                this.b = i7;
                this.j = i6;
                return true;
            } else {
                throw new AssertionError();
            }
        }
    }

    private C0007a() {
    }
}
