package com.ironsource.mobilcore;

import android.view.animation.Interpolator;

/* renamed from: com.ironsource.mobilcore.ag  reason: case insensitive filesystem */
final class C0014ag implements Interpolator {
    C0014ag() {
    }

    public final float getInterpolation(float f) {
        float f2 = f - 1.0f;
        return (f2 * f2 * f2 * f2 * f2) + 1.0f;
    }
}
