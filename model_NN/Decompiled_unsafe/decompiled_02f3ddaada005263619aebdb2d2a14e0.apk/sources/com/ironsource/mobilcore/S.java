package com.ironsource.mobilcore;

import android.content.Context;
import android.graphics.Canvas;
import android.widget.FrameLayout;

class S extends FrameLayout {
    /* access modifiers changed from: private */
    public boolean a;
    private boolean b = true;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public boolean d = true;

    public S(Context context) {
        super(context);
        if (Y.m) {
            setLayerType(2, null);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.c = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.c = false;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (Y.m && this.b) {
            post(new Runnable() {
                public final void run() {
                    boolean unused = S.this.a = true;
                    S.this.invalidate();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.a && Y.m) {
            post(new Runnable() {
                public final void run() {
                    if (!S.this.c) {
                        return;
                    }
                    if (S.this.getLayerType() != 2 || S.this.d) {
                        boolean unused = S.this.d = false;
                        S.this.setLayerType(2, null);
                        S.this.buildLayer();
                        S.this.setLayerType(0, null);
                    }
                }
            });
            this.a = false;
        }
    }
}
