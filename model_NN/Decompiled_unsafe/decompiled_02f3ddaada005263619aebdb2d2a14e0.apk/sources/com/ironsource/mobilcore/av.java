package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.WindowManager;
import android.webkit.WebView;
import com.applovin.sdk.bootstrap.SdkBoostrapTasks;
import com.ironsource.mobilcore.B;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONException;
import org.json.JSONObject;

class av {
    private static final String[] a = {"9774d56d682e549c"};
    private static String b;
    private static SharedPreferences c;

    public interface a {
        void a(int i);

        boolean a(HttpEntity httpEntity);
    }

    public interface c {
        void a(String str, boolean z);

        void a(boolean z);
    }

    av() {
    }

    public static String a(Context context, String str) {
        return MessageFormat.format("http://ads.mobilecore.com/?package={0}&ver={1}&type={2}", context.getPackageName(), "0.9", str);
    }

    public static HttpClient a() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 60000);
        return new DefaultHttpClient(basicHttpParams);
    }

    public static int a(Context context) {
        if (C0017b.a(context, 1)) {
            return 1;
        }
        if (C0017b.a(context, 0)) {
            return 0;
        }
        return -1;
    }

    public static String a(int i) {
        if (i == 1) {
            return "cell";
        }
        if (i == 0) {
            return "wifi";
        }
        return "none";
    }

    public static String b(Context context) {
        long j = Long.MIN_VALUE;
        for (ApplicationInfo applicationInfo : context.getPackageManager().getInstalledApplications(AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER)) {
            long lastModified = new File(applicationInfo.sourceDir).lastModified();
            if (lastModified <= j) {
                lastModified = j;
            }
            j = lastModified;
        }
        return new SimpleDateFormat("ddMMyyyy-HH:mm:ss", Locale.UK).format(new Date(j));
    }

    public static int c(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        int size = packageManager.queryIntentActivities(intent, 0).size();
        C0031p.a("found an app total of " + size, 55);
        return size;
    }

    public static int d(Context context) {
        return context.getResources().getDisplayMetrics().densityDpi;
    }

    @SuppressLint({"NewApi"})
    public static double e(Context context) {
        double d = 1.0d;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        if (Build.VERSION.SDK_INT >= 17) {
            windowManager.getDefaultDisplay().getRealMetrics(displayMetrics);
        } else {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        double d2 = (double) displayMetrics.xdpi;
        if (d2 == 0.0d) {
            d2 = 1.0d;
        }
        double d3 = (double) displayMetrics.ydpi;
        if (d3 != 0.0d) {
            d = d3;
        }
        return Math.sqrt(Math.pow(((double) displayMetrics.widthPixels) / d2, 2.0d) + Math.pow(((double) displayMetrics.heightPixels) / d, 2.0d));
    }

    public interface b {
        HashMap<String, Pair<String, JSONObject>> a = new HashMap<>();
        HashMap<String, String> b = new HashMap<>();
        String c = "pending";
        c d = null;

        default b(c cVar) {
            this.d = cVar;
        }

        default void a() throws JSONException {
            String str;
            this.c = "started";
            for (Map.Entry<String, Pair<String, JSONObject>> value : this.a.entrySet()) {
                Pair pair = (Pair) value.getValue();
                if ("pending".equals(pair.first)) {
                    JSONObject jSONObject = (JSONObject) pair.second;
                    String string = jSONObject.getString("location");
                    String string2 = jSONObject.getString("filename");
                    String string3 = jSONObject.getString("type");
                    if ("feed".equals(string3)) {
                        str = string + "?" + aF.a().n();
                    } else {
                        str = string;
                    }
                    String f = aF.a().f();
                    aH.a(str, "zip".equals(string3) ? new C0024i(f, string2, this) : "feed".equals(string3) ? new C0020e(f, string2, this) : new C0023h(f, string2, this));
                }
            }
        }

        default void a(JSONObject jSONObject) throws JSONException {
            String string = jSONObject.getString("id");
            String string2 = jSONObject.getString("filename");
            this.a.put(string, new Pair("pending", jSONObject));
            this.b.put(string2, string);
        }

        default void a(String str) {
            String str2 = this.b.get(str);
            this.a.put(str2, new Pair("pending", (JSONObject) this.a.get(str2).second));
        }

        private synchronized default void b() {
            boolean z;
            Iterator<Map.Entry<String, Pair<String, JSONObject>>> it = this.a.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = true;
                    break;
                }
                String str = (String) ((Pair) it.next().getValue()).first;
                if (!"finished".equals(str) && !"failed".equals(str) && !"failed-ignored".equals(str)) {
                    z = false;
                    break;
                }
            }
            if (z) {
                if (!this.c.equals("failed")) {
                    this.c = "finished";
                    try {
                        for (Map.Entry<String, Pair<String, JSONObject>> value : this.a.entrySet()) {
                            Pair pair = (Pair) value.getValue();
                            if (((String) pair.first) == "finished") {
                                JSONObject jSONObject = (JSONObject) pair.second;
                                String string = jSONObject.getString("id");
                                String optString = jSONObject.optString(SdkBoostrapTasks.SDK_VERSION_FILE);
                                if (optString != null) {
                                    av.b().edit().putString(string, optString).commit();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        av.a(MobileCore.b, getClass().getName(), e);
                        this.c = "failed";
                    }
                }
                this.d.a("finished".equals(this.c));
            }
        }

        default void a(String str, Exception exc) {
            av.a(MobileCore.b, getClass().getName(), exc);
            C0031p.a("ResourceManager | processException " + str, 2);
            String str2 = this.b.get(str);
            this.a.put(str2, new Pair("failed", (JSONObject) this.a.get(str2).second));
            this.d.a(str, false);
            b();
        }

        @SuppressLint({"DefaultLocale"})
        default void a(String str, int i) {
            av.a(MobileCore.b, getClass().getName(), String.format("Failed to get file %s with status code %d", str, Integer.valueOf(i)));
            String str2 = this.b.get(str);
            boolean optBoolean = ((JSONObject) this.a.get(this.b.get(str)).second).optBoolean("mandatory", true);
            JSONObject jSONObject = (JSONObject) this.a.get(str2).second;
            String str3 = "failed";
            if (optBoolean || i < 400 || i >= 500) {
                this.c = "failed";
            } else {
                str3 = "failed-ignored";
            }
            this.a.put(str2, new Pair(str3, jSONObject));
            this.d.a(str, false);
            b();
        }

        default void a(String str, boolean z) {
            C0031p.a("ResourceManager | processComplete " + str + " - " + (z ? "OK" : "NEY"), 55);
            String str2 = this.b.get(str);
            this.a.put(str2, new Pair(z ? "finished" : "failed", (JSONObject) this.a.get(str2).second));
            if (!z) {
                this.c = "failed";
            }
            this.d.a(str, z);
            b();
        }
    }

    public static Double a(Double d, Context context) {
        Double valueOf = Double.valueOf(-1.0d);
        try {
            DecimalFormat decimalFormat = new DecimalFormat("#0.00");
            DecimalFormatSymbols decimalFormatSymbols = decimalFormat.getDecimalFormatSymbols();
            decimalFormatSymbols.setDecimalSeparator('.');
            decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
            return Double.valueOf(decimalFormat.format(d));
        } catch (Exception e) {
            C0031p.a("Error formatting double value", 55);
            a(context, av.class.getName(), e);
            return valueOf;
        }
    }

    protected static String f(Context context) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo == null) {
            return "null";
        }
        return packageInfo.versionName;
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                stringBuffer.append(Integer.toHexString(b2 & 255));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    @SuppressLint({"InlinedApi"})
    public static String g(Context context) {
        String str;
        TelephonyManager telephonyManager;
        String string = m(context).getString("s#gDs#gI1%drs#ge1%ds1%dus#gLs#gSs#gI", "");
        if (!TextUtils.isEmpty(string)) {
            String n = C0017b.n(string);
            C0031p.a("MCUtils , getUniqueID() | found id in prefs. uniqueId: " + n, 55);
            return n;
        }
        String g = g(Settings.Secure.getString(context.getApplicationContext().getContentResolver(), TapjoyConstants.TJC_ANDROID_ID));
        C0031p.a("not from prefs: " + g, 55);
        if (!TextUtils.isEmpty(g) || (telephonyManager = (TelephonyManager) context.getSystemService("phone")) == null) {
            str = g;
        } else {
            str = telephonyManager.getDeviceId();
            C0031p.a("telephony Manager device id: " + str, 55);
            if (TextUtils.isEmpty(str)) {
                str = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
                C0031p.a("got MAC address: " + str, 55);
            }
        }
        if (!TextUtils.isEmpty(m(MobileCore.b).getString("s#gDs#gI1%drs#ge1%ds1%dus#gLs#gSs#gI", ""))) {
            return str;
        }
        SharedPreferences.Editor edit = m(context).edit();
        edit.putString("s#gDs#gI1%drs#ge1%ds1%dus#gLs#gSs#gI", C0017b.m(str));
        edit.commit();
        C0031p.a("Final UserID saved = " + str, 55);
        return str;
    }

    private static String g(String str) {
        for (String equals : a) {
            if (str.equals(equals)) {
                return null;
            }
        }
        return str;
    }

    public static String b(String str) {
        try {
            return str.substring(str.lastIndexOf(47) + 1);
        } catch (IndexOutOfBoundsException e) {
            C0031p.a("fileFromPath error: " + e.getMessage(), 55);
            return null;
        }
    }

    public static String c(String str) {
        String str2 = "";
        int lastIndexOf = str.lastIndexOf(47) + 1;
        while (lastIndexOf < str.length()) {
            char charAt = str.charAt(lastIndexOf);
            if (charAt == '%') {
                str2 = str2.concat(" ");
                lastIndexOf += 3;
            } else {
                str2 = str2.concat(String.valueOf(charAt));
                lastIndexOf++;
            }
        }
        return str2;
    }

    public static boolean h(Context context) {
        boolean z = false;
        String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (next.importance == 100 && next.processName.equals(packageName)) {
                z = true;
            }
            z = z;
        }
        return z;
    }

    public static String a(Context context, String str, String str2, String str3) {
        try {
            String str4 = str2 + "/" + str3;
            PackageInfo packageArchiveInfo = context.getPackageManager().getPackageArchiveInfo(str4, 0);
            if (TextUtils.isEmpty(str4)) {
            }
            packageArchiveInfo.applicationInfo.sourceDir = str4;
            packageArchiveInfo.applicationInfo.publicSourceDir = str4;
            String str5 = packageArchiveInfo.applicationInfo.packageName;
            C0031p.a("^^^appName: " + str5, 55);
            return str5;
        } catch (Exception e) {
            if (1 == 0) {
                Intent intent = new Intent(context, MobileCoreReport.class);
                intent.putExtra("1%dns#ge1%dk1%do1%dt", str);
                intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 70);
                StackTraceElement stackTraceElement = e.getStackTrace()[0];
                intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_ex", InstallationTracker.class.toString() + "###" + e.getMessage() + "###" + stackTraceElement.getClassName() + "@" + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber());
                context.startService(intent);
            }
            return null;
        }
    }

    public static String i(Context context) {
        return MessageFormat.format("https://play.google.com/store/apps/details?id={0}", context.getPackageName());
    }

    public static String d(String str) throws UnsupportedEncodingException {
        return URLEncoder.encode(str, "UTF-8");
    }

    public static void a(Activity activity, String str, boolean z) {
        if (z) {
            a(new B(activity, activity), str, (B.a) null);
            return;
        }
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        activity.startActivity(intent);
    }

    public static void a(B b2, String str, B.a aVar) {
        b2.loadUrl(str);
        if (aVar != null) {
            b2.a(aVar);
        }
        b2.c();
    }

    private static JSONObject n(Context context) {
        String str;
        String str2;
        WifiManager wifiManager;
        String str3 = null;
        JSONObject jSONObject = new JSONObject();
        try {
            String string = Settings.Secure.getString(context.getApplicationContext().getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
            if (!b(context, "android.permission.READ_PHONE_STATE")) {
                str2 = null;
            } else {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (telephonyManager != null) {
                    str = telephonyManager.getDeviceId();
                    C0031p.a("MCUtils , getDeviceIdFromTelephonyManager() | telephony Manager uniqueId: " + str, 55);
                } else {
                    str = null;
                }
                str2 = str;
            }
            if (b(context, "android.permission.ACCESS_WIFI_STATE") && (wifiManager = (WifiManager) context.getSystemService("wifi")) != null) {
                str3 = wifiManager.getConnectionInfo().getMacAddress();
                C0031p.a("MCUtils , getMACAddress() | got MAC address: " + str3, 55);
            }
            String n = C0017b.n(m(context).getString("1%dns#ge1%dk1%do1%dts#g_1%dss#gfs#ge1%dr1%dp", ""));
            jSONObject.putOpt("os", Build.VERSION.RELEASE);
            jSONObject.putOpt("deviceCode", Build.DEVICE);
            jSONObject.putOpt("uid", string);
            jSONObject.putOpt("imei", str2);
            jSONObject.putOpt("mac", str3);
            jSONObject.putOpt("devId", n);
            jSONObject.putOpt("appId", context.getApplicationContext().getPackageName());
            jSONObject.putOpt("deviceName", Build.MODEL);
            jSONObject.putOpt("deviceBrand", Build.MANUFACTURER);
            jSONObject.putOpt("uns", Boolean.valueOf(k(context)));
            jSONObject.putOpt("externalStorage", Boolean.valueOf(b(context, "android.permission.WRITE_EXTERNAL_STORAGE")));
            jSONObject.putOpt("sdkVer", "0.9");
        } catch (Exception e) {
            a(context, av.class.getName(), e);
        }
        C0031p.a("MobileParams " + jSONObject.toString(), 55);
        return jSONObject;
    }

    public static String j(Context context) {
        return "'" + n(context) + "'";
    }

    public static boolean b(Context context, String str) {
        return context.checkCallingOrSelfPermission(str) == 0;
    }

    public static boolean k(Context context) {
        if (Build.VERSION.SDK_INT <= 16) {
            try {
                if (Settings.Secure.getInt(context.getContentResolver(), "install_non_market_apps") == 1) {
                    return true;
                }
            } catch (Exception e) {
                a(context, Utils.class.getName(), e);
            }
        } else if (Settings.Secure.getInt(context.getContentResolver(), "install_non_market_apps") != 1) {
            return false;
        } else {
            return true;
        }
        return false;
    }

    public static final Intent a(JSONObject jSONObject) {
        String optString = jSONObject.optString("shareDialogTitle", "");
        String optString2 = jSONObject.optString("shareSubject", "");
        String optString3 = jSONObject.optString("shareText", "");
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.setType("text/plain");
        if (!TextUtils.isEmpty(optString2)) {
            intent.putExtra("android.intent.extra.SUBJECT", optString2);
        }
        if (!TextUtils.isEmpty(optString3)) {
            intent.putExtra("android.intent.extra.TEXT", optString3);
        }
        return Intent.createChooser(intent, optString);
    }

    public static void a(Context context, String str, String str2, String str3, String str4) {
        Intent intent = new Intent(context, MobileCoreReport.class);
        intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 99);
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow", str);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type", str2);
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_result", str3);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer", str4);
        intent.putExtra("1%dns#ge1%dk1%do1%dt", b);
        context.startService(intent);
    }

    public static void a(Context context, String str, Exception exc) {
        C0031p.a("Error: " + exc.getLocalizedMessage(), 2);
        StackTraceElement stackTraceElement = exc.getStackTrace()[0];
        a(context, str, str + "###" + exc.getMessage() + "###" + stackTraceElement.getFileName() + "##" + stackTraceElement.getMethodName() + ":" + stackTraceElement.getLineNumber());
    }

    public static void a(Context context, String str, String str2) {
        if (TextUtils.isEmpty(b)) {
            b = C0017b.n(m(context).getString("1%dns#ge1%dk1%do1%dts#g_1%dss#gfs#ge1%dr1%dp", ""));
        }
        Intent intent = new Intent(context, MobileCoreReport.class);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_ex", str + "###" + str2 + "###");
        intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 70);
        intent.putExtra("1%dns#ge1%dk1%do1%dt", b);
        context.startService(intent);
    }

    public static void a(Context context, String str, String str2, String str3, String str4, int i, String str5, int i2, String str6, int i3) {
        C0031p.a("monitorInstall | going to monitor package: " + str3, 55);
        SharedPreferences m = m(context);
        int i4 = m.getInt("com.ironsource.mobilecore.prefs_alarm_id", 0);
        SharedPreferences.Editor edit = m.edit();
        edit.putInt("com.ironsource.mobilecore.prefs_alarm_id", i4 + 1);
        edit.commit();
        C0031p.a("monitorInstall | alaram id " + i4, 55);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        Intent intent = new Intent(context, InstallationTracker.class);
        intent.setAction("track_install");
        intent.putExtra("1%dns#ge1%dk1%do1%dt", str);
        intent.putExtra("s#ge1%dms#ga1%dns#g_s#ges#ggs#ga1%dks#gcs#ga1%dps#g_s#ga1%dr1%dt1%dxs#ge", str3);
        intent.putExtra("1%dts#ge1%dk1%drs#ga1%dms#g_1%dks#gcs#ga1%dr1%dts#g_s#ga1%dr1%dt1%dxs#ge", i);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type", str2);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer", str5);
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow", str6);
        if (i3 >= 0) {
            intent.putExtra("com.ironsource.mobilecore.prefs_tracker_id", i3);
        }
        if (i == 4) {
            intent.putExtra("com.ironsource.mobilcore.extra_download_filename", str4);
        }
        alarmManager.set(0, System.currentTimeMillis() + ((long) (i2 * 60 * 1000)), PendingIntent.getBroadcast(context, i4, intent, 1073741824));
    }

    public static String e(String str) {
        int indexOf = str.indexOf("id=");
        int i = indexOf + 3;
        int indexOf2 = str.indexOf("&", indexOf);
        if (indexOf < 0) {
            return "idFieldMissing";
        }
        if (indexOf2 >= 0) {
            return str.substring(i, indexOf2);
        }
        return str.substring(i);
    }

    public static boolean c(Context context, String str) {
        try {
            context.getPackageManager().getPackageInfo(str, 1);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String f(String str) {
        String substring = str.substring(str.lastIndexOf("/") + 1, str.length());
        if (substring.lastIndexOf(".") < 0) {
            return ".png";
        }
        return substring.substring(substring.lastIndexOf("."), substring.length());
    }

    public static void a(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    a(file2);
                } else {
                    file2.delete();
                }
            }
        }
        file.delete();
    }

    @SuppressLint({"InlinedApi"})
    public static SharedPreferences l(Context context) {
        if (Build.VERSION.SDK_INT >= 9) {
            return context.getSharedPreferences("1%dss#gfs#ge1%dr1%dps#g_s#gds#ge1%drs#gas#ghs#gSs#g_s#ge1%dr1%dos#gCs#ge1%dls#gis#gb1%do1%dm", 4);
        }
        return context.getSharedPreferences("1%dss#gfs#ge1%dr1%dps#g_s#gds#ge1%drs#gas#ghs#gSs#g_s#ge1%dr1%dos#gCs#ge1%dls#gis#gb1%do1%dm", 0);
    }

    public static SharedPreferences b() {
        return m(MobileCore.b);
    }

    public static SharedPreferences m(Context context) {
        if (c == null) {
            c = l(context);
        }
        return c;
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public static void a(WebView webView, aw awVar) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportMultipleWindows(false);
        webView.clearCache(true);
        webView.getSettings().setSupportZoom(false);
        webView.setInitialScale(100);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setNeedInitialFocus(false);
        webView.getSettings().setDomStorageEnabled(true);
        if (awVar != null) {
            webView.setWebChromeClient(awVar);
        }
    }
}
