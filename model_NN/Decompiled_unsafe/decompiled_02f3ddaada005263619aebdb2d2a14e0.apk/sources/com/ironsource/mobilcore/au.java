package com.ironsource.mobilcore;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import org.json.JSONException;
import org.json.JSONObject;

final class au extends F {
    private String a;
    private boolean b;

    public au(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "ironsourceUrl";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) throws JSONException {
        this.a = jSONObject.optString("url", "");
        this.b = jSONObject.optBoolean("openInternaly", true);
        super.a(jSONObject, true);
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        super.a(view);
        if (!TextUtils.isEmpty(this.a)) {
            av.a(this.e, this.a, this.b);
        }
    }
}
