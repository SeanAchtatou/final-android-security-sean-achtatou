package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;

public class InstallationTracker extends BroadcastReceiver {
    private static int[] k = {1, 2, 5, 8, 10, 15, 20, 30, 45, 60, 120, 240};
    /* access modifiers changed from: private */
    public SharedPreferences a;
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public Intent e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public int l;

    static /* synthetic */ void h(InstallationTracker installationTracker) {
        String str;
        if (installationTracker.b(installationTracker.c)) {
            String str2 = installationTracker.c;
            try {
                str = installationTracker.b.getPackageManager().getApplicationInfo(str2, 0).name;
            } catch (Exception e2) {
                e2.printStackTrace();
                str = str2;
            }
            Bitmap a2 = installationTracker.a(str2);
            new Intent("android.intent.action.MAIN");
            Intent launchIntentForPackage = installationTracker.b.getPackageManager().getLaunchIntentForPackage(str2);
            launchIntentForPackage.addCategory("android.intent.category.LAUNCHER");
            Intent intent = new Intent();
            intent.putExtra("android.intent.extra.shortcut.INTENT", launchIntentForPackage);
            intent.putExtra("android.intent.extra.shortcut.NAME", str);
            if (a2 != null) {
                intent.putExtra("android.intent.extra.shortcut.ICON", a2);
            }
            intent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            installationTracker.b.sendBroadcast(intent);
            return;
        }
        C0031p.a("FALSE", 55);
    }

    @SuppressLint({"InlinedApi"})
    public void onReceive(final Context context, final Intent intent) {
        new Thread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.ironsource.mobilcore.InstallationTracker.a(boolean, java.lang.String):void
             arg types: [int, java.lang.String]
             candidates:
              com.ironsource.mobilcore.InstallationTracker.a(com.ironsource.mobilcore.InstallationTracker, int):int
              com.ironsource.mobilcore.InstallationTracker.a(com.ironsource.mobilcore.InstallationTracker, android.content.Context):android.content.Context
              com.ironsource.mobilcore.InstallationTracker.a(com.ironsource.mobilcore.InstallationTracker, android.content.Intent):android.content.Intent
              com.ironsource.mobilcore.InstallationTracker.a(com.ironsource.mobilcore.InstallationTracker, android.content.SharedPreferences):android.content.SharedPreferences
              com.ironsource.mobilcore.InstallationTracker.a(com.ironsource.mobilcore.InstallationTracker, java.lang.String):java.lang.String
              com.ironsource.mobilcore.InstallationTracker.a(java.lang.String, java.lang.String):void
              com.ironsource.mobilcore.InstallationTracker.a(boolean, java.lang.String):void */
            public final void run() {
                Context unused = InstallationTracker.this.b = context;
                String unused2 = InstallationTracker.this.c = intent.getStringExtra("s#ge1%dms#ga1%dns#g_s#ges#ggs#ga1%dks#gcs#ga1%dps#g_s#ga1%dr1%dt1%dxs#ge");
                String unused3 = InstallationTracker.this.d = intent.getStringExtra("1%dns#ge1%dk1%do1%dt");
                int unused4 = InstallationTracker.this.f = intent.getIntExtra("1%dts#ge1%dk1%drs#ga1%dms#g_1%dks#gcs#ga1%dr1%dts#g_s#ga1%dr1%dt1%dxs#ge", -1);
                Intent unused5 = InstallationTracker.this.e = new Intent(InstallationTracker.this.b, MobileCoreReport.class);
                InstallationTracker.this.e.putExtra("1%dns#ge1%dk1%do1%dt", InstallationTracker.this.d);
                String unused6 = InstallationTracker.this.g = intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer");
                String unused7 = InstallationTracker.this.h = intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type");
                String unused8 = InstallationTracker.this.i = intent.getStringExtra("com.ironsource.mobilcore.extra_download_filename");
                String unused9 = InstallationTracker.this.j = intent.getStringExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow");
                C0031p.a("got to installationTracker", 55);
                C0031p.a("^^^^ instPKG NAME " + InstallationTracker.this.c, 55);
                if (InstallationTracker.this.a == null) {
                    if (Build.VERSION.SDK_INT >= 9) {
                        SharedPreferences unused10 = InstallationTracker.this.a = InstallationTracker.this.b.getSharedPreferences("1%dss#gfs#ge1%dr1%dps#g_s#gds#ge1%drs#gas#ghs#gSs#g_s#ge1%dr1%dos#gCs#ge1%dls#gis#gb1%do1%dm", 4);
                    } else {
                        SharedPreferences unused11 = InstallationTracker.this.a = InstallationTracker.this.b.getSharedPreferences("1%dss#gfs#ge1%dr1%dps#g_s#gds#ge1%drs#gas#ghs#gSs#g_s#ge1%dr1%dos#gCs#ge1%dls#gis#gb1%do1%dm", 0);
                    }
                }
                int unused12 = InstallationTracker.this.l = intent.getIntExtra("com.ironsource.mobilecore.prefs_tracker_id", 0);
                C0031p.a("tracking count = " + InstallationTracker.this.l, 55);
                switch (InstallationTracker.this.f) {
                    case 1:
                        InstallationTracker.h(InstallationTracker.this);
                        return;
                    case 2:
                        InstallationTracker.this.a(false, InstallationTracker.this.c);
                        return;
                    case 3:
                        InstallationTracker.this.a(true, InstallationTracker.this.c);
                        return;
                    case 4:
                        InstallationTracker.this.a(true, InstallationTracker.this.c);
                        return;
                    default:
                        return;
                }
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z, String str) {
        int i2;
        if (!z) {
            if (b(str)) {
                C0031p.a("track installation: not already inst", 3);
            } else {
                C0031p.a("track installation: already inst", 3);
            }
        } else if (b(str)) {
            a(this.g, "+");
            C0031p.a("Market inst successful " + str, 55);
            if (this.i != null) {
                C0031p.a("Making sure deletion of: " + this.i + ". result: " + this.b.deleteFile(this.i), 55);
            }
        } else if (this.l < k.length - 1) {
            this.l++;
            int i3 = k[0];
            if (this.l > 1) {
                i2 = k[this.l - 1];
            } else {
                i2 = i3;
            }
            int i4 = k[this.l];
            C0031p.a("trackInstallationFromMarket | trackingCount = " + i2 + " next check will come in " + (i4 - i2), 55);
            C0031p.a("report failure false setting check time to " + i4, 55);
            av.a(this.b, this.d, this.h, this.c, this.i, this.f, this.g, i4 - i2, this.j, this.l);
        } else {
            C0031p.a("reporting install failure!", 55);
            a(this.g, "S-");
            if (this.i != null) {
                C0031p.a("Making sure deletion of: " + this.i + ". result: " + this.b.deleteFile(this.i), 55);
            }
            C0031p.a("Market inst unsuccessful" + str, 55);
        }
    }

    private Bitmap a(String str) {
        try {
            return ((BitmapDrawable) this.b.getPackageManager().getApplicationIcon(this.b.getPackageManager().getApplicationInfo(str, 0).packageName)).getBitmap();
        } catch (Exception e2) {
            Intent intent = new Intent(this.b, MobileCoreReport.class);
            intent.putExtra("1%dns#ge1%dk1%do1%dt", this.d);
            intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 70);
            StackTraceElement stackTraceElement = e2.getStackTrace()[0];
            intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_ex", InstallationTracker.class.toString() + "###" + e2.getMessage() + "###" + stackTraceElement.getClassName() + "@" + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber());
            this.b.startService(intent);
            return null;
        }
    }

    private boolean b(String str) {
        try {
            this.b.getPackageManager().getPackageInfo(str, 1);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    private void a(String str, String str2) {
        this.e.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 99);
        this.e.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type", this.h);
        this.e.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow", this.j);
        this.e.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_result", str2);
        this.e.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer", str);
        this.e.putExtra("1%dns#ge1%dk1%do1%dt", this.d);
        this.b.startService(this.e);
    }
}
