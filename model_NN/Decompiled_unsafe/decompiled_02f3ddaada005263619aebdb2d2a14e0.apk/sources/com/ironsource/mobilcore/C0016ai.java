package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.view.MotionEvent;

@SuppressLint({"NewApi"})
/* renamed from: com.ironsource.mobilcore.ai  reason: case insensitive filesystem */
final class C0016ai extends ak {
    C0016ai(Activity activity, int i) {
        super(activity, i);
    }

    public final void a(boolean z) {
        a(this.u, 0, z);
    }

    public final void b(boolean z) {
        a(0, 0, z);
    }

    public final void a(int i) {
        this.p = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{-16777216, 0});
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int i7 = (int) this.c;
        this.s.layout(0, 0, i5, this.u);
        f(i7);
        if (m) {
            this.t.layout(0, 0, i5, i6);
        } else {
            this.t.layout(0, i7, i5, i6 + i7);
        }
    }

    private void f(int i) {
        if (this.l && this.u != 0) {
            int i2 = this.u;
            float f = (((float) i2) - ((float) i)) / ((float) i2);
            if (!m) {
                this.s.offsetTopAndBottom(((int) ((((float) i2) * (-f)) * 0.25f)) - this.s.getTop());
                this.s.setVisibility(i == 0 ? 4 : 0);
            } else if (i > 0) {
                this.s.setTranslationY((float) ((int) (((float) i2) * (-f) * 0.25f)));
            } else {
                this.s.setTranslationY((float) (-i2));
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas, int i) {
        this.p.setBounds(0, i - this.q, getWidth(), i);
        this.p.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void b(Canvas canvas, int i) {
        int width = getWidth();
        float f = ((float) i) / ((float) this.u);
        this.n.setBounds(0, 0, width, i);
        this.n.setAlpha((int) ((1.0f - f) * 185.0f));
        this.n.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.i.a(0, 0, this.u / 3, 0, 1000);
    }

    /* access modifiers changed from: protected */
    public final void b(int i) {
        if (m) {
            this.t.setTranslationY((float) i);
            f(i);
            invalidate();
            return;
        }
        this.t.offsetTopAndBottom(i - this.t.getTop());
        f(i);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final boolean a(MotionEvent motionEvent) {
        return motionEvent.getY() > this.c;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        return (!this.w && this.f <= ((float) this.z)) || (this.w && this.f >= this.c);
    }

    /* access modifiers changed from: protected */
    public final boolean a(float f) {
        return (!this.w && this.f <= ((float) this.z) && f > 0.0f) || (this.w && this.f >= this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* access modifiers changed from: protected */
    public final void b(float f) {
        c(Math.min(Math.max(this.c + f, 0.0f), (float) this.u));
    }

    /* access modifiers changed from: protected */
    public final void b(MotionEvent motionEvent) {
        int i = (int) this.c;
        if (this.d) {
            this.j.computeCurrentVelocity(1000, (float) this.k);
            int xVelocity = (int) this.j.getXVelocity();
            this.h = motionEvent.getY();
            a(this.j.getYVelocity() > 0.0f ? this.u : 0, xVelocity, true);
        } else if (this.w && motionEvent.getY() > ((float) i)) {
            b(true);
        }
    }
}
