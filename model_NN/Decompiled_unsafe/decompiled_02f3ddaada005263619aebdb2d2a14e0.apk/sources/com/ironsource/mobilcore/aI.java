package com.ironsource.mobilcore;

import android.support.v4.view.accessibility.AccessibilityEventCompat;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

class aI {
    aI() {
    }

    public static boolean a(String str, File file) {
        try {
            ZipInputStream zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(file)));
            String substring = file.getName().substring(0, file.getName().length() - 4);
            File file2 = new File(str + "/" + substring);
            if (!file2.exists()) {
                file2.mkdirs();
            }
            while (true) {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry != null) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byte[] bArr = new byte[AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END];
                    if (!nextEntry.isDirectory()) {
                        File file3 = new File(str + "/" + substring + "/" + nextEntry.getName());
                        if (!file3.getParentFile().exists()) {
                            file3.getParentFile().mkdirs();
                        }
                        File file4 = new File(str + "/" + substring, nextEntry.getName());
                        file4.getParentFile().mkdirs();
                        FileOutputStream fileOutputStream = new FileOutputStream(file4);
                        while (true) {
                            int read = zipInputStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            byteArrayOutputStream.write(bArr, 0, read);
                            fileOutputStream.write(byteArrayOutputStream.toByteArray());
                            byteArrayOutputStream.reset();
                        }
                        fileOutputStream.close();
                        byteArrayOutputStream.close();
                        zipInputStream.closeEntry();
                    }
                } else {
                    zipInputStream.close();
                    return true;
                }
            }
        } catch (IOException e) {
            av.a(MobileCore.b, aI.class.getName(), e);
            return false;
        }
    }
}
