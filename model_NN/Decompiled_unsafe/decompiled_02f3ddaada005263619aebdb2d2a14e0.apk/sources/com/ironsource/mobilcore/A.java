package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.ironsource.mobilcore.J;
import com.ironsource.mobilcore.ax;

@SuppressLint({"ViewConstructor"})
class A extends B {
    /* access modifiers changed from: private */
    public J b;

    public A(Context context, Activity activity, String str) {
        super(context, activity);
        this.b = new J(this.a, str);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        setWebChromeClient(new ax(this, new a()));
        setWebViewClient(new b(this, (byte) 0));
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.b.c();
        super.b();
    }

    class b extends WebViewClient {
        private b() {
        }

        /* synthetic */ b(A a2, byte b) {
            this();
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            return A.this.b.a(webView, str, (J.a) null);
        }

        public final void onReceivedError(WebView webView, int i, String str, String str2) {
            av.a(A.this.getContext(), A.class.getName(), "Web offerwall: Received error. errorCode:" + i + " , failingUrl:" + str2);
            super.onReceivedError(webView, i, str, str2);
        }
    }

    class a implements ax.a {
        public a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ironsource.mobilcore.J.a(java.lang.String, java.lang.String, java.lang.String, boolean):void
         arg types: [java.lang.String, java.lang.String, java.lang.String, int]
         candidates:
          com.ironsource.mobilcore.J.a(java.lang.String, java.lang.String, org.json.JSONObject, boolean):void
          com.ironsource.mobilcore.J.a(java.lang.String, java.lang.String, java.lang.String, boolean):void */
        public final void init(String str, String str2, String str3) {
            A.this.b.a(str, str2, str3, false);
        }

        public final String getMobileParams() {
            return av.j(A.this.getContext());
        }

        public final void reportImpressions(int i, int i2) {
            A.this.b.a(i, i2);
        }
    }
}
