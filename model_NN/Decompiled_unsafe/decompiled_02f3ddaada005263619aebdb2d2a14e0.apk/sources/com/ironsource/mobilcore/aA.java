package com.ironsource.mobilcore;

import android.content.Context;
import android.text.TextUtils;
import org.json.JSONException;
import org.json.JSONObject;

final class aA {
    private Context a;

    public aA(Context context) {
        this.a = context;
    }

    public final C0036u a(JSONObject jSONObject, ao aoVar) throws JSONException {
        C0036u auVar;
        String string = jSONObject.getString("type");
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        if (string.equals("lineButton")) {
            auVar = new G(this.a, aoVar);
        } else if (string.equals("widgetButton")) {
            auVar = new az(this.a, aoVar);
        } else if (string.equals("sliderHeader")) {
            auVar = new al(this.a, aoVar);
        } else if (string.equals("groupHeader")) {
            auVar = new C0040y(this.a, aoVar);
        } else if (string.equals("separator")) {
            auVar = new N(this.a, aoVar);
        } else if (string.equals("ironsourceRateUs")) {
            auVar = new K(this.a, aoVar);
        } else if (string.equals("ironsourceShare")) {
            auVar = new O(this.a, aoVar);
        } else if (string.equals("ironsourceSearch")) {
            auVar = new M(this.a, aoVar);
        } else if (string.equals("ironsourceOfferWallOpener")) {
            auVar = new I(this.a, aoVar);
        } else if (string.equals("ironsourceFeedback")) {
            auVar = new C0039x(this.a, aoVar);
        } else if (string.equals("ironsourceInlineApps")) {
            auVar = new C(this.a, aoVar);
        } else if (string.equals("ironsourceSocialWidget")) {
            auVar = new at(this.a, aoVar);
        } else {
            auVar = string.equals("ironsourceUrl") ? new au(this.a, aoVar) : null;
        }
        if (auVar == null) {
            return null;
        }
        auVar.a(jSONObject);
        return auVar;
    }
}
