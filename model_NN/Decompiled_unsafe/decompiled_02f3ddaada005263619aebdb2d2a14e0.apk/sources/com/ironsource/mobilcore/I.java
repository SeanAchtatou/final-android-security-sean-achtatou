package com.ironsource.mobilcore;

import android.content.Context;
import android.view.View;

final class I extends F {
    public I(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "ironsourceOfferWallOpener";
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        super.a(view);
        MobileCore.showOfferWall(this.e, null);
    }
}
