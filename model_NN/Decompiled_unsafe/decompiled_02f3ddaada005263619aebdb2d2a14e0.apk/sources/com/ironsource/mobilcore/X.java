package com.ironsource.mobilcore;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;

final class X extends C0015ah {
    X(Activity activity, int i) {
        super(activity, i, C0009ab.LEFT);
    }

    public final void a(int i) {
        this.p = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[]{-16777216, 0});
        invalidate();
    }
}
