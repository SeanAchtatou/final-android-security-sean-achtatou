package com.ironsource.mobilcore;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ironsource.mobilcore.ao;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.ironsource.mobilcore.v  reason: case insensitive filesystem */
abstract class C0037v extends C0036u {
    /* access modifiers changed from: private */
    public Drawable a;
    /* access modifiers changed from: private */
    public Drawable b;
    protected String i;
    protected String j;
    protected String k;
    protected String l;
    protected TextView m;
    protected TextView n;
    protected TextView o;
    protected ImageView p;
    /* access modifiers changed from: private */
    public int q;

    /* access modifiers changed from: protected */
    public abstract boolean d();

    static /* synthetic */ void c(C0037v vVar) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f, 1, 0.5f, 1, 0.5f);
        ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.0f, 2.0f, 1.0f, 2.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(new AccelerateInterpolator());
        scaleAnimation2.setInterpolator(new BounceInterpolator());
        scaleAnimation.setDuration(300);
        scaleAnimation2.setStartOffset(300);
        scaleAnimation2.setDuration(1000);
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(scaleAnimation2);
        animationSet.setFillEnabled(true);
        animationSet.setAnimationListener(new Animation.AnimationListener() {
            public final void onAnimationStart(Animation animation) {
            }

            public final void onAnimationRepeat(Animation animation) {
            }

            public final void onAnimationEnd(Animation animation) {
                if (C0037v.this.q < 3) {
                    C0037v.c(C0037v.this);
                    C0037v.e(C0037v.this);
                }
            }
        });
        vVar.p.startAnimation(animationSet);
    }

    static /* synthetic */ int e(C0037v vVar) {
        int i2 = vVar.q;
        vVar.q = i2 + 1;
        return i2;
    }

    public C0037v(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String a(MCEWidgetTextProperties mCEWidgetTextProperties) {
        switch (mCEWidgetTextProperties) {
            case MAIN_TEXT:
                return this.i;
            case SECONDARY_TEXT:
                return this.j;
            case BADGE_TEXT:
                return this.k;
            default:
                return null;
        }
    }

    public final void a(MCEWidgetTextProperties mCEWidgetTextProperties, String str) {
        if (d()) {
            switch (mCEWidgetTextProperties) {
                case MAIN_TEXT:
                    this.i = str;
                    if (this.m != null) {
                        this.m.setText(this.i);
                        this.m.setBackgroundColor(0);
                        break;
                    }
                    break;
                case SECONDARY_TEXT:
                    this.j = str;
                    if (this.n != null) {
                        this.n.setText(this.j);
                        this.n.setBackgroundColor(0);
                        break;
                    }
                    break;
                case BADGE_TEXT:
                    this.k = str;
                    if (this.o != null) {
                        this.o.setText(this.k);
                        this.o.setBackgroundColor(0);
                        break;
                    }
                    break;
            }
            if (this instanceof C0041z) {
                ((C0041z) this).p();
            }
        }
    }

    public final void b(int i2) {
        if (d() && this.p != null) {
            this.p.setImageResource(i2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        k();
        super.a(view);
    }

    /* access modifiers changed from: protected */
    public final void a(C0038w... wVarArr) {
        C0038w wVar = new C0038w("mainText", TextUtils.isEmpty(this.i) ? "" : this.i);
        if (wVarArr == null) {
            super.a(wVar);
            return;
        }
        C0038w[] wVarArr2 = new C0038w[(wVarArr.length + 1)];
        System.arraycopy(wVarArr, 0, wVarArr2, 0, wVarArr.length);
        if (!TextUtils.isEmpty(this.i)) {
            wVarArr2[wVarArr2.length - 1] = wVar;
        }
        super.a(wVarArr2);
    }

    /* access modifiers changed from: protected */
    public final void a(JSONObject jSONObject, boolean z) throws JSONException {
        this.i = jSONObject.optString("text", "");
        this.j = jSONObject.optString("secondaryText", "");
        this.k = jSONObject.optString("badgeText", "");
        this.l = jSONObject.optString("base64Icon", "");
        if (this.l != null && this.l == "null") {
            this.l = null;
        }
        super.a(jSONObject, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
      com.ironsource.mobilcore.b.a(android.content.Context, float):int */
    /* access modifiers changed from: protected */
    public final void n() {
        this.o = new TextView(this.c);
        this.o.setBackgroundColor(0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(11);
        layoutParams.addRule(15);
        layoutParams.leftMargin = C0017b.a(this.c, 5.0f);
        this.o.setLayoutParams(layoutParams);
        this.o.setId(j());
        this.o.setGravity(1);
        this.o.setTextColor(this.d.b().a(true));
        this.o.setTypeface(null, 1);
        this.o.setTextSize(2, 15.0f);
        C0017b.a(this.o, this.d.p());
        int a2 = C0017b.a(this.c, 8.0f);
        int a3 = C0017b.a(this.c, 4.0f);
        this.o.setPadding(a2, a3, a2, a3);
    }

    /* access modifiers changed from: protected */
    public final void a(Drawable drawable, final ao.b bVar, final TextView... textViewArr) {
        this.a = drawable;
        this.b = this.d.s();
        C0017b.a(this.g, this.a);
        this.d.a(true, bVar, textViewArr);
        this.g.setOnTouchListener(new View.OnTouchListener() {
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    C0017b.a(C0037v.this.g, C0037v.this.b);
                    C0037v.this.d.a(false, bVar, textViewArr);
                } else if (motionEvent.getAction() == 1 || motionEvent.getAction() == 3) {
                    C0017b.a(C0037v.this.g, C0037v.this.a);
                    C0037v.this.d.a(true, bVar, textViewArr);
                }
                return false;
            }
        });
    }

    public final void o() {
        this.q = 1;
        C0017b.a(this.g, this.b);
        if (this.p != null) {
            this.p.post(new Runnable() {
                public final void run() {
                    C0037v.c(C0037v.this);
                }
            });
        }
    }
}
