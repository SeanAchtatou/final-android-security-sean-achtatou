package com.ironsource.mobilcore;

import android.content.Context;
import org.json.JSONException;
import org.json.JSONObject;

final class az extends C0033r {
    public az(Context context, ao aoVar) {
        super(context, aoVar);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return true;
    }

    public final String e() {
        return "widgetButton";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject, true);
    }
}
