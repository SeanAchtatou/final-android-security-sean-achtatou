package com.ironsource.mobilcore;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

class OfferwallManager$2 extends BroadcastReceiver {
    private /* synthetic */ aF a;

    OfferwallManager$2(aF aFVar) {
        this.a = aFVar;
    }

    public void onReceive(Context context, Intent intent) {
        boolean a2 = C0017b.a(this.a.a);
        C0031p.a("offerwall , mConnectivityReceiver | called , hasInternetConnection:" + a2, 55);
        if (a2) {
            this.a.j();
        }
    }
}
