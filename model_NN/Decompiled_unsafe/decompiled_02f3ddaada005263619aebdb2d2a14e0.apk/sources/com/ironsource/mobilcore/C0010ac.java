package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.view.MotionEvent;

@SuppressLint({"NewApi"})
/* renamed from: com.ironsource.mobilcore.ac  reason: case insensitive filesystem */
final class C0010ac extends V {
    C0010ac(Activity activity, int i) {
        super(activity, i);
    }

    public final void a(boolean z) {
        a(-this.u, 0, z);
    }

    public final void b(boolean z) {
        a(0, 0, z);
    }

    public final void a(int i) {
        this.p = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{-16777216, 0});
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int i7 = (int) this.c;
        this.s.layout(i5 - this.u, 0, i5, i6);
        f(i7);
        if (m) {
            this.t.layout(0, 0, i5, i6);
        } else {
            this.t.layout(i7, 0, i5 + i7, i6);
        }
    }

    private void f(int i) {
        if (this.l && this.u != 0) {
            int i2 = this.u;
            float f = (((float) i2) + ((float) i)) / ((float) i2);
            if (!m) {
                this.s.offsetLeftAndRight((((int) ((((float) i2) * f) * 0.25f)) + getWidth()) - this.s.getRight());
                this.s.setVisibility(i == 0 ? 4 : 0);
            } else if (i != 0) {
                this.s.setTranslationX((float) ((int) (((float) i2) * f * 0.25f)));
            } else {
                this.s.setTranslationX((float) (-i2));
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas, int i) {
        int height = getHeight();
        int width = getWidth() + i;
        this.p.setBounds(width, 0, this.q + width, height);
        this.p.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void b(Canvas canvas, int i) {
        int height = getHeight();
        int width = getWidth();
        float abs = ((float) Math.abs(i)) / ((float) this.u);
        this.n.setBounds(width + i, 0, width, height);
        this.n.setAlpha((int) (185.0f * (1.0f - abs)));
        this.n.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.i.a(0, 0, (-this.u) / 3, 0, 1000);
    }

    /* access modifiers changed from: protected */
    public final void b(int i) {
        if (m) {
            this.t.setTranslationX((float) i);
            f(i);
            invalidate();
            return;
        }
        this.t.offsetLeftAndRight(i - this.t.getLeft());
        f(i);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final boolean a(MotionEvent motionEvent) {
        return motionEvent.getX() < ((float) getWidth()) + this.c;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        int width = getWidth();
        int i = (int) this.e;
        return (!this.w && i >= width - this.z) || (this.w && ((float) i) <= ((float) width) + this.c);
    }

    /* access modifiers changed from: protected */
    public final boolean a(float f) {
        int width = getWidth();
        int i = (int) this.e;
        return (!this.w && i >= width - this.z && f < 0.0f) || (this.w && ((float) i) <= ((float) width) + this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    public final void b(float f) {
        c(Math.max(Math.min(this.c + f, 0.0f), (float) (-this.u)));
    }

    /* access modifiers changed from: protected */
    public final void b(MotionEvent motionEvent) {
        int i = (int) this.c;
        int width = getWidth();
        if (this.d) {
            this.j.computeCurrentVelocity(1000, (float) this.k);
            int xVelocity = (int) this.j.getXVelocity();
            this.g = motionEvent.getX();
            a(this.j.getXVelocity() > 0.0f ? 0 : -this.u, xVelocity, true);
        } else if (this.w && motionEvent.getX() < ((float) (i + width))) {
            b(true);
        }
    }
}
