package com.ironsource.mobilcore;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.ironsource.mobilcore.t  reason: case insensitive filesystem */
abstract class C0035t extends C0037v {
    public C0035t(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public String e() {
        return "ironsourceSocialWidget";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject, true);
    }

    /* access modifiers changed from: protected */
    public final boolean f() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g = new LinearLayout(this.c);
        ((LinearLayout) this.g).setOrientation(0);
        ((LinearLayout) this.g).setGravity(17);
        this.g.setPadding(this.d.h(), this.d.j(), this.d.h(), this.d.j());
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.p = new ImageView(this.c);
        ((ViewGroup) this.g).addView(this.p);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        Bitmap a = C0017b.a(this.c, this.l);
        if (a != null) {
            this.p.setImageBitmap(a);
        }
    }
}
