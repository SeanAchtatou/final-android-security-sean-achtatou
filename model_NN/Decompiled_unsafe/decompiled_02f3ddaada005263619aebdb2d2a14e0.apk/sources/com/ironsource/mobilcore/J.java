package com.ironsource.mobilcore;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import com.ironsource.mobilcore.Utils;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class J {
    private String a;
    private String b;
    private String c;
    private String d;
    private JSONObject e;
    /* access modifiers changed from: private */
    public aE f;
    private aE g;
    /* access modifiers changed from: private */
    public ArrayList<Integer> h = new ArrayList<>();
    private Context i;
    private boolean j = false;
    private boolean k;
    private boolean l = false;
    /* access modifiers changed from: private */
    public boolean m;
    private long n;
    private long o;
    private boolean p;

    public J(Context context, String str) {
        this.a = str;
        this.i = context;
    }

    public final boolean a() {
        return this.j;
    }

    public final boolean b() {
        return this.k;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.J.a(java.lang.String, java.lang.String, org.json.JSONObject, boolean):void
     arg types: [java.lang.String, java.lang.String, org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.J.a(java.lang.String, java.lang.String, java.lang.String, boolean):void
      com.ironsource.mobilcore.J.a(java.lang.String, java.lang.String, org.json.JSONObject, boolean):void */
    public final void a(String str, String str2, String str3, boolean z) {
        try {
            a(str, str2, new JSONObject(str3), false);
        } catch (JSONException e2) {
            C0031p.a("Failed to parse offers JSON: " + str3 + e2.getLocalizedMessage(), 55);
        }
    }

    public final void a(String str, String str2, JSONObject jSONObject, boolean z) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || jSONObject == null || jSONObject.length() == 0) {
            String str3 = "";
            if (jSONObject != null) {
                str3 = jSONObject.toString();
            }
            C0031p.a("Called MCOfferWallReportHelper init() without valid params. flowName:" + str + " , flowName:" + str2 + " , offersJsonStr:" + str3, 2);
            return;
        }
        this.c = str;
        this.d = str2;
        this.e = jSONObject;
        this.m = z;
        a(this.e);
        if (z) {
            a(0, this.f.size() - 1);
        }
        this.k = true;
        this.o = System.currentTimeMillis() - aF.a().m();
        this.n = System.currentTimeMillis();
        this.p = false;
    }

    public final void c() {
        if (this.j || !this.k || this.g == null) {
            this.j = false;
        } else {
            a(30, this.g);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0144  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.webkit.WebView r12, java.lang.String r13, final com.ironsource.mobilcore.J.a r14) {
        /*
            r11 = this;
            if (r12 == 0) goto L_0x0008
            boolean r0 = android.text.TextUtils.isEmpty(r13)
            if (r0 == 0) goto L_0x000a
        L_0x0008:
            r0 = 0
        L_0x0009:
            return r0
        L_0x000a:
            java.lang.String r0 = r11.b
            if (r0 != 0) goto L_0x0010
            r11.b = r13
        L_0x0010:
            com.ironsource.mobilcore.aE r0 = r11.f
            if (r0 != 0) goto L_0x0019
            org.json.JSONObject r0 = r11.e
            r11.a(r0)
        L_0x0019:
            java.lang.String r0 = "iron://decline"
            boolean r0 = r13.contains(r0)
            if (r0 == 0) goto L_0x0036
            r0 = 29
            com.ironsource.mobilcore.aE r1 = r11.g
            r11.a(r0, r1)
            r0 = 1
            r11.j = r0
            r0 = 0
            r11.l = r0
            if (r14 == 0) goto L_0x0034
            r0 = 1
            r14.a(r0)
        L_0x0034:
            r0 = 1
            goto L_0x0009
        L_0x0036:
            java.lang.String r0 = "//play.google.com"
            boolean r0 = r13.contains(r0)
            if (r0 == 0) goto L_0x0059
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "market://details?id="
            r0.<init>(r1)
            java.lang.String r1 = "id="
            int r1 = r13.indexOf(r1)
            int r1 = r1 + 3
            java.lang.String r1 = r13.substring(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r13 = r0.toString()
        L_0x0059:
            com.ironsource.mobilcore.aE r0 = r11.f
            java.lang.String r1 = r11.b
            com.ironsource.mobilcore.aD r6 = r0.c(r1)
            if (r6 != 0) goto L_0x0068
            r0 = 0
            r11.l = r0
            r0 = 1
            goto L_0x0009
        L_0x0068:
            boolean r0 = r11.l
            if (r0 != 0) goto L_0x00ac
            java.lang.String r2 = r6.f()
            boolean r0 = r11.p
            if (r0 != 0) goto L_0x010b
            long r0 = java.lang.System.currentTimeMillis()
            long r3 = r11.n
            long r0 = r0 - r3
            r11.n = r0
            r1 = 0
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0103 }
            r0.<init>(r2)     // Catch:{ JSONException -> 0x0103 }
            java.lang.String r1 = "TimerOWtoAd"
            long r2 = r11.n     // Catch:{ JSONException -> 0x0290 }
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x0290 }
            java.lang.String r1 = "TimerAdFetchToPresented"
            long r2 = r11.o     // Catch:{ JSONException -> 0x0290 }
            r0.put(r1, r2)     // Catch:{ JSONException -> 0x0290 }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ JSONException -> 0x0290 }
            r11.n = r1     // Catch:{ JSONException -> 0x0290 }
            r1 = 1
            r11.p = r1     // Catch:{ JSONException -> 0x0290 }
        L_0x009a:
            android.content.Context r1 = r11.i
            java.lang.String r2 = r11.c
            java.lang.String r3 = r11.d
            java.lang.String r4 = "S"
            java.lang.String r0 = r0.toString()
            com.ironsource.mobilcore.av.a(r1, r2, r3, r4, r0)
        L_0x00a9:
            r0 = 1
            r11.l = r0
        L_0x00ac:
            if (r14 == 0) goto L_0x00b1
            r14.a()
        L_0x00b1:
            java.lang.String r0 = "market://"
            boolean r0 = r13.startsWith(r0)
            if (r0 == 0) goto L_0x0144
            java.lang.String r3 = com.ironsource.mobilcore.av.e(r13)
            android.content.Intent r0 = new android.content.Intent     // Catch:{ ActivityNotFoundException -> 0x011b }
            java.lang.String r1 = "android.intent.action.VIEW"
            android.net.Uri r2 = android.net.Uri.parse(r13)     // Catch:{ ActivityNotFoundException -> 0x011b }
            r0.<init>(r1, r2)     // Catch:{ ActivityNotFoundException -> 0x011b }
            r1 = 268435456(0x10000000, float:2.5243549E-29)
            r0.setFlags(r1)     // Catch:{ ActivityNotFoundException -> 0x011b }
            android.content.Context r1 = r12.getContext()     // Catch:{ ActivityNotFoundException -> 0x011b }
            r1.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x011b }
        L_0x00d4:
            android.content.Context r0 = r11.i
            boolean r0 = com.ironsource.mobilcore.av.c(r0, r3)
            if (r0 != 0) goto L_0x0134
            android.content.Context r0 = r11.i
            java.lang.String r1 = r11.a
            java.lang.String r2 = r11.d
            r4 = 0
            r5 = 3
            java.lang.String r6 = r6.f()
            r7 = 1
            java.lang.String r8 = r11.c
            r9 = -1
            com.ironsource.mobilcore.av.a(r0, r1, r2, r3, r4, r5, r6, r7, r8, r9)
        L_0x00ef:
            java.lang.String r0 = r11.b
            r11.a(r0)
            r0 = 0
            r11.b = r0
            r0 = 0
            r11.l = r0
            if (r14 == 0) goto L_0x0100
            r0 = 0
            r14.a(r0)
        L_0x0100:
            r0 = 1
            goto L_0x0009
        L_0x0103:
            r0 = move-exception
            r10 = r0
            r0 = r1
            r1 = r10
        L_0x0107:
            r1.printStackTrace()
            goto L_0x009a
        L_0x010b:
            android.content.Context r0 = r11.i
            java.lang.String r1 = r11.c
            java.lang.String r2 = r11.d
            java.lang.String r3 = "S"
            java.lang.String r4 = r6.f()
            com.ironsource.mobilcore.av.a(r0, r1, r2, r3, r4)
            goto L_0x00a9
        L_0x011b:
            r0 = move-exception
            android.content.Context r1 = r11.i
            java.lang.String r2 = "You must have Android Play Store"
            r4 = 1
            android.widget.Toast r1 = android.widget.Toast.makeText(r1, r2, r4)
            r1.show()
            android.content.Context r1 = r11.i
            java.lang.Class<com.ironsource.mobilcore.J> r2 = com.ironsource.mobilcore.J.class
            java.lang.String r2 = r2.getName()
            com.ironsource.mobilcore.av.a(r1, r2, r0)
            goto L_0x00d4
        L_0x0134:
            android.content.Context r0 = r11.i
            java.lang.String r1 = r11.c
            java.lang.String r2 = r11.d
            java.lang.String r3 = "AI"
            java.lang.String r4 = r6.f()
            com.ironsource.mobilcore.av.a(r0, r1, r2, r3, r4)
            goto L_0x00ef
        L_0x0144:
            java.lang.String r0 = r6.c()
            java.lang.String r1 = "ApkDownload"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0253
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "calling hanfleApk "
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r13)
            java.lang.String r0 = r0.toString()
            r1 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r1)
            android.content.Context r0 = r11.i
            java.lang.String r1 = r6.h()
            boolean r0 = com.ironsource.mobilcore.av.c(r0, r1)
            if (r0 != 0) goto L_0x0231
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "Url overriding got url: "
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r13)
            java.lang.String r0 = r0.toString()
            r1 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r1)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "Got ApkDownload type with url: "
            r0.<init>(r1)
            java.lang.String r1 = r11.b
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r1)
            r6.b()
            java.lang.String r2 = ""
            java.lang.String r3 = ""
            java.lang.String r2 = r6.a()     // Catch:{ Exception -> 0x01dc }
            java.lang.String r3 = r6.e()     // Catch:{ Exception -> 0x01dc }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01dc }
            java.lang.String r1 = "Utils/imgpath = "
            r0.<init>(r1)     // Catch:{ Exception -> 0x01dc }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01dc }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01dc }
            r1 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r1)     // Catch:{ Exception -> 0x01dc }
        L_0x01bd:
            com.ironsource.mobilcore.k r0 = com.ironsource.mobilcore.C0026k.a()
            boolean r0 = r0.a(r2)
            if (r0 == 0) goto L_0x01f6
            android.content.Context r0 = r11.i
            java.lang.String r1 = "Already downloading this offer"
            r2 = 1
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r2)
            r0.show()
            if (r14 == 0) goto L_0x01d9
            r0 = 0
            r14.a(r0)
        L_0x01d9:
            r0 = 1
            goto L_0x0009
        L_0x01dc:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r4 = "appImgPath error: "
            r1.<init>(r4)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r1 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r1)
            goto L_0x01bd
        L_0x01f6:
            android.content.Context r0 = r11.i
            java.lang.String r1 = "The app will be downloaded to your device shortly"
            r4 = 1
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r4)
            r0.show()
            com.ironsource.mobilcore.k r0 = com.ironsource.mobilcore.C0026k.a()
            java.lang.String r1 = r11.b
            java.lang.String r4 = r6.f()
            java.lang.String r5 = r11.c
            r6 = -1
            r8 = 1
            java.lang.String r9 = r11.d
            r0.a(r1, r2, r3, r4, r5, r6, r8, r9)
            java.lang.String r0 = r11.b
            r11.a(r0)
            r0 = 0
            r11.b = r0
            java.util.Timer r0 = new java.util.Timer
            r0.<init>()
            com.ironsource.mobilcore.J$1 r1 = new com.ironsource.mobilcore.J$1
            r1.<init>(r11, r14)
            r2 = 2000(0x7d0, double:9.88E-321)
            r0.schedule(r1, r2)
            r0 = 0
            r11.l = r0
            goto L_0x01d9
        L_0x0231:
            android.content.Context r0 = r11.i
            java.lang.String r1 = "Application already installed"
            r2 = 1
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r2)
            r0.show()
            if (r14 == 0) goto L_0x0243
            r0 = 0
            r14.a(r0)
        L_0x0243:
            android.content.Context r0 = r11.i
            java.lang.String r1 = r11.c
            java.lang.String r2 = r11.d
            java.lang.String r3 = "AI"
            java.lang.String r4 = r6.f()
            com.ironsource.mobilcore.av.a(r0, r1, r2, r3, r4)
            goto L_0x01d9
        L_0x0253:
            com.ironsource.mobilcore.aE r0 = r11.f
            java.lang.String r1 = r11.b
            com.ironsource.mobilcore.aD r0 = r0.c(r1)
            java.lang.String r0 = r0.c()
            java.lang.String r1 = "web"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0278
            if (r14 == 0) goto L_0x026d
            r0 = 0
            r14.a(r0)
        L_0x026d:
            java.lang.String r0 = r11.b
            r11.a(r0)
            r0 = 0
            r11.l = r0
            r0 = 0
            goto L_0x0009
        L_0x0278:
            java.lang.String r0 = r6.c()
            java.lang.String r1 = "CPC"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x028d
            if (r14 == 0) goto L_0x028a
            r0 = 0
            r14.a(r0)
        L_0x028a:
            r0 = 0
            goto L_0x0009
        L_0x028d:
            r0 = 0
            goto L_0x0009
        L_0x0290:
            r1 = move-exception
            goto L_0x0107
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.J.a(android.webkit.WebView, java.lang.String, com.ironsource.mobilcore.J$a):boolean");
    }

    public interface a {
        final /* synthetic */ Utils.b a;

        default a(Utils.b bVar) {
            this.a = bVar;
        }

        default void a() {
            this.a.a.setVisibility(0);
        }

        default void a(boolean z) {
            this.a.a.setVisibility(8);
            if (Utils.this.l) {
                Utils.this.a(false);
            } else if (z) {
                Utils.this.p.dismiss();
            }
        }
    }

    public final void a(int i2, int i3) {
        if (i2 >= 0 && i3 >= 0 && i2 <= i3) {
            while (i2 <= i3) {
                this.h.add(Integer.valueOf(i2));
                this.g.add(this.f.get(i2));
                i2++;
            }
            AnonymousClass2 r0 = new AsyncTask<Void, Void, aE>() {
                /* access modifiers changed from: protected */
                public final /* synthetic */ Object doInBackground(Object[] objArr) {
                    return a();
                }

                /* access modifiers changed from: protected */
                public final /* synthetic */ void onPostExecute(Object obj) {
                    J.this.a(27, (aE) obj);
                }

                private aE a() {
                    aE aEVar;
                    HttpClient a2 = av.a();
                    if (J.this.m) {
                        aEVar = (aE) J.this.f.clone();
                    } else {
                        aEVar = new aE();
                        Iterator it = J.this.h.iterator();
                        while (it.hasNext()) {
                            int intValue = ((Integer) it.next()).intValue();
                            if (intValue < J.this.f.size()) {
                                aEVar.add(J.this.f.get(intValue));
                            }
                        }
                        J.this.h.clear();
                    }
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= aEVar.size()) {
                            return aEVar;
                        }
                        aD aDVar = (aD) aEVar.get(i2);
                        try {
                            HttpResponse execute = a2.execute(new HttpGet(aDVar.g()));
                            int statusCode = execute.getStatusLine().getStatusCode();
                            execute.getEntity().consumeContent();
                            aDVar.a(statusCode);
                        } catch (Exception e) {
                            aDVar.a(-1);
                            C0031p.a("Error Impression: " + e.getLocalizedMessage(), 55);
                        }
                        i = i2 + 1;
                    }
                }
            };
            if (Build.VERSION.SDK_INT >= 11) {
                r0.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            } else {
                r0.execute(new Void[0]);
            }
        }
    }

    private void a(JSONObject jSONObject) {
        this.f = new aE();
        this.g = new aE();
        if (jSONObject != null) {
            this.f.a(jSONObject);
        }
    }

    private void a(String str) {
        this.g.a(str);
    }

    /* access modifiers changed from: private */
    public void a(int i2, aE aEVar) {
        Intent intent = new Intent(this.i, MobileCoreReport.class);
        intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", i2);
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow", this.c);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type", this.d);
        try {
            intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offers", new JSONArray(aEVar.toString()).toString());
        } catch (Exception e2) {
            av.a(this.i, J.class.getName(), e2);
        }
        intent.putExtra("1%dns#ge1%dk1%do1%dt", this.a);
        this.i.startService(intent);
    }
}
