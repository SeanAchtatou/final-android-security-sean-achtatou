package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.view.View;

class aB {
    private static Intent f;
    private View a;
    protected Activity b;
    private SharedPreferences c;
    private String d;
    private Intent e;

    @SuppressLint({"InlinedApi"})
    protected aB(Activity activity) {
        try {
            this.b = activity;
            if (Build.VERSION.SDK_INT >= 9) {
                this.c = this.b.getApplicationContext().getSharedPreferences("1%dss#gfs#ge1%dr1%dps#g_s#gds#ge1%drs#gas#ghs#gSs#g_s#ge1%dr1%dos#gCs#ge1%dls#gis#gb1%do1%dm", 4);
            } else {
                this.c = this.b.getApplicationContext().getSharedPreferences("1%dss#gfs#ge1%dr1%dps#g_s#gds#ge1%drs#gas#ghs#gSs#g_s#ge1%dr1%dos#gCs#ge1%dls#gis#gb1%do1%dm", 0);
            }
            this.d = C0017b.n(this.c.getString("1%dns#ge1%dk1%do1%dts#g_1%dss#gfs#ge1%dr1%dp", ""));
            this.e = new Intent(this.b, MobileCoreReport.class);
            this.e.putExtra("1%dns#ge1%dk1%do1%dt", this.d);
            Intent intent = new Intent(this.b, MobileCoreReport.class);
            f = intent;
            intent.putExtra("1%dns#ge1%dk1%do1%dt", this.d);
            f.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 70);
        } catch (Exception e2) {
            StackTraceElement stackTraceElement = e2.getStackTrace()[0];
            f.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_ex", aB.class.toString() + "###" + e2.getMessage() + "###" + stackTraceElement.getClassName() + "@" + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber());
            this.b.startService(f);
        }
        this.e = new Intent(this.b, MobileCoreReport.class);
        this.e.putExtra("1%dns#ge1%dk1%do1%dt", this.d);
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        this.a = view;
    }

    /* access modifiers changed from: protected */
    public final AlertDialog b() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.b);
            builder.setInverseBackgroundForced(true);
            builder.setView(this.a);
            builder.setTitle("");
            builder.setCancelable(true);
            AlertDialog create = builder.create();
            create.getWindow().addFlags(1);
            create.show();
            return create;
        } catch (Exception e2) {
            Intent intent = new Intent(this.b, MobileCoreReport.class);
            intent.putExtra("1%dns#ge1%dk1%do1%dt", this.d);
            intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 70);
            StackTraceElement stackTraceElement = e2.getStackTrace()[0];
            intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_ex", aB.class.toString() + "###" + e2.getMessage() + "###" + stackTraceElement.getClassName() + "@" + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber());
            this.b.startService(intent);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, boolean z) {
        SharedPreferences.Editor edit = this.c.edit();
        edit.putBoolean(str, z);
        edit.commit();
    }
}
