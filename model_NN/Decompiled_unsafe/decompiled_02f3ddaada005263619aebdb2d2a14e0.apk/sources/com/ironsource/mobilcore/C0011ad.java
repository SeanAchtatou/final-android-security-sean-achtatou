package com.ironsource.mobilcore;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;

/* renamed from: com.ironsource.mobilcore.ad  reason: case insensitive filesystem */
final class C0011ad extends C0015ah {
    C0011ad(Activity activity, int i) {
        super(activity, i, C0009ab.RIGHT);
    }

    public final void a(int i) {
        this.p = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{-16777216, 0});
        invalidate();
    }
}
