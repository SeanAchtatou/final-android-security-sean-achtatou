package com.ironsource.mobilcore;

import android.content.Context;

class aG extends Thread {
    private a a;
    private Context b;
    private String[] c;

    public aG(a aVar, Context context, String... strArr) {
        this.a = aVar;
        this.b = context;
        this.c = strArr;
        setPriority(10);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x011a A[SYNTHETIC, Splitter:B:34:0x011a] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x011f A[Catch:{ IOException -> 0x012e }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0124 A[Catch:{ IOException -> 0x012e }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0129 A[Catch:{ IOException -> 0x012e }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01df A[SYNTHETIC, Splitter:B:51:0x01df] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01e4 A[Catch:{ IOException -> 0x01f8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01e9 A[Catch:{ IOException -> 0x01f8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01ee A[Catch:{ IOException -> 0x01f8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0030 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void start() {
        /*
            r13 = this;
            java.lang.String[] r8 = r13.c
            int r9 = r8.length
            r0 = 0
            r7 = r0
        L_0x0005:
            if (r7 >= r9) goto L_0x01f2
            r0 = r8[r7]
            r5 = 0
            r3 = 0
            r2 = 0
            r1 = 0
            java.lang.Class<com.ironsource.mobilcore.MobileCore> r4 = com.ironsource.mobilcore.MobileCore.class
            java.io.InputStream r4 = r4.getResourceAsStream(r0)     // Catch:{ Exception -> 0x033c, all -> 0x01db }
            if (r4 != 0) goto L_0x003b
            java.lang.String r0 = "ReadFileAsyncTask / ReadFile | is is null"
            r3 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r3)     // Catch:{ Exception -> 0x0340, all -> 0x0321 }
        L_0x001c:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x028e }
        L_0x0021:
            if (r5 == 0) goto L_0x0026
            r5.close()     // Catch:{ IOException -> 0x028e }
        L_0x0026:
            if (r2 == 0) goto L_0x002b
            r2.close()     // Catch:{ IOException -> 0x028e }
        L_0x002b:
            if (r4 == 0) goto L_0x0030
            r2.close()     // Catch:{ IOException -> 0x028e }
        L_0x0030:
            java.lang.String r0 = "finished loading agreement"
            r1 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r1)
            int r0 = r7 + 1
            r7 = r0
            goto L_0x0005
        L_0x003b:
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0340, all -> 0x0321 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0340, all -> 0x0321 }
            r6.<init>(r4)     // Catch:{ Exception -> 0x0340, all -> 0x0321 }
            r3.<init>(r6)     // Catch:{ Exception -> 0x0340, all -> 0x0321 }
            android.content.Context r2 = r13.b     // Catch:{ Exception -> 0x0345, all -> 0x0326 }
            java.io.File r2 = r2.getFilesDir()     // Catch:{ Exception -> 0x0345, all -> 0x0326 }
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x0345, all -> 0x0326 }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0345, all -> 0x0326 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0345, all -> 0x0326 }
            r10.<init>()     // Catch:{ Exception -> 0x0345, all -> 0x0326 }
            java.lang.StringBuilder r2 = r10.append(r2)     // Catch:{ Exception -> 0x0345, all -> 0x0326 }
            java.lang.String r10 = "/"
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ Exception -> 0x0345, all -> 0x0326 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0345, all -> 0x0326 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0345, all -> 0x0326 }
            r2 = 0
            r6.<init>(r0, r2)     // Catch:{ Exception -> 0x0345, all -> 0x0326 }
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x034b, all -> 0x032c }
            java.io.OutputStreamWriter r0 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x034b, all -> 0x032c }
            r0.<init>(r6)     // Catch:{ Exception -> 0x034b, all -> 0x032c }
            r2.<init>(r0)     // Catch:{ Exception -> 0x034b, all -> 0x032c }
        L_0x0076:
            java.lang.String r0 = r3.readLine()     // Catch:{ Exception -> 0x0080, all -> 0x0332 }
            if (r0 == 0) goto L_0x01c4
            r2.write(r0)     // Catch:{ Exception -> 0x0080, all -> 0x0332 }
            goto L_0x0076
        L_0x0080:
            r0 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r6
        L_0x0085:
            android.content.Intent r5 = new android.content.Intent     // Catch:{ all -> 0x0339 }
            android.content.Context r6 = r13.b     // Catch:{ all -> 0x0339 }
            java.lang.Class<com.ironsource.mobilcore.MobileCoreReport> r10 = com.ironsource.mobilcore.MobileCoreReport.class
            r5.<init>(r6, r10)     // Catch:{ all -> 0x0339 }
            java.lang.StackTraceElement[] r6 = r0.getStackTrace()     // Catch:{ all -> 0x0339 }
            r10 = 0
            r6 = r6[r10]     // Catch:{ all -> 0x0339 }
            java.lang.String r10 = "com.ironsource.mobilcore.MobileCoreReport_extra_ex"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x0339 }
            r11.<init>()     // Catch:{ all -> 0x0339 }
            java.lang.Class<com.ironsource.mobilcore.aG> r12 = com.ironsource.mobilcore.aG.class
            java.lang.String r12 = r12.getName()     // Catch:{ all -> 0x0339 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x0339 }
            java.lang.String r12 = "###"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x0339 }
            java.lang.String r12 = r0.getMessage()     // Catch:{ all -> 0x0339 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x0339 }
            java.lang.String r12 = "###"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x0339 }
            java.lang.String r12 = r6.getClassName()     // Catch:{ all -> 0x0339 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x0339 }
            java.lang.String r12 = ":"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x0339 }
            int r6 = r6.getLineNumber()     // Catch:{ all -> 0x0339 }
            java.lang.StringBuilder r6 = r11.append(r6)     // Catch:{ all -> 0x0339 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0339 }
            r5.putExtra(r10, r6)     // Catch:{ all -> 0x0339 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0339 }
            java.lang.String r10 = "caught ex "
            r6.<init>(r10)     // Catch:{ all -> 0x0339 }
            java.lang.Class<com.ironsource.mobilcore.MobileCore> r10 = com.ironsource.mobilcore.MobileCore.class
            java.lang.String r10 = r10.getName()     // Catch:{ all -> 0x0339 }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ all -> 0x0339 }
            java.lang.String r10 = "###"
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ all -> 0x0339 }
            java.lang.String r10 = r0.getMessage()     // Catch:{ all -> 0x0339 }
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ all -> 0x0339 }
            java.lang.String r10 = "###"
            java.lang.StringBuilder r6 = r6.append(r10)     // Catch:{ all -> 0x0339 }
            java.lang.String r0 = android.util.Log.getStackTraceString(r0)     // Catch:{ all -> 0x0339 }
            java.lang.StringBuilder r0 = r6.append(r0)     // Catch:{ all -> 0x0339 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0339 }
            r6 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r6)     // Catch:{ all -> 0x0339 }
            java.lang.String r0 = "caught ex in MobileCore sent to MobileCore"
            r6 = 3
            com.ironsource.mobilcore.C0031p.a(r0, r6)     // Catch:{ all -> 0x0339 }
            android.content.Context r0 = r13.b     // Catch:{ all -> 0x0339 }
            r0.startService(r5)     // Catch:{ all -> 0x0339 }
            if (r1 == 0) goto L_0x011d
            r1.close()     // Catch:{ IOException -> 0x012e }
        L_0x011d:
            if (r4 == 0) goto L_0x0122
            r4.close()     // Catch:{ IOException -> 0x012e }
        L_0x0122:
            if (r2 == 0) goto L_0x0127
            r2.close()     // Catch:{ IOException -> 0x012e }
        L_0x0127:
            if (r3 == 0) goto L_0x0030
            r2.close()     // Catch:{ IOException -> 0x012e }
            goto L_0x0030
        L_0x012e:
            r0 = move-exception
            android.content.Intent r1 = new android.content.Intent
            android.content.Context r2 = r13.b
            java.lang.Class<com.ironsource.mobilcore.MobileCoreReport> r3 = com.ironsource.mobilcore.MobileCoreReport.class
            r1.<init>(r2, r3)
            java.lang.StackTraceElement[] r2 = r0.getStackTrace()
            r3 = 0
            r2 = r2[r3]
            java.lang.String r3 = "com.ironsource.mobilcore.MobileCoreReport_extra_ex"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.Class<com.ironsource.mobilcore.aG> r5 = com.ironsource.mobilcore.aG.class
            java.lang.String r5 = r5.getName()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "###"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r0.getMessage()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "###"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r2.getClassName()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ":"
            java.lang.StringBuilder r4 = r4.append(r5)
            int r2 = r2.getLineNumber()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            r1.putExtra(r3, r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "caught ex "
            r2.<init>(r3)
            java.lang.Class<com.ironsource.mobilcore.MobileCore> r3 = com.ironsource.mobilcore.MobileCore.class
            java.lang.String r3 = r3.getName()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "###"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r0.getMessage()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "###"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = android.util.Log.getStackTraceString(r0)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r2 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r2)
            java.lang.String r0 = "caught ex in MobileCore sent to MobileCore"
            r2 = 3
            com.ironsource.mobilcore.C0031p.a(r0, r2)
            android.content.Context r0 = r13.b
        L_0x01bf:
            r0.startService(r1)
            goto L_0x0030
        L_0x01c4:
            r2.flush()     // Catch:{ Exception -> 0x0080, all -> 0x0332 }
            r6.flush()     // Catch:{ Exception -> 0x0080, all -> 0x0332 }
            r2.close()     // Catch:{ Exception -> 0x0080, all -> 0x0332 }
            r6.close()     // Catch:{ Exception -> 0x0080, all -> 0x0332 }
            r3.close()     // Catch:{ Exception -> 0x0080, all -> 0x0332 }
            r4.close()     // Catch:{ Exception -> 0x0080, all -> 0x0332 }
            r1 = r2
            r5 = r6
            r2 = r3
            goto L_0x001c
        L_0x01db:
            r0 = move-exception
            r4 = r5
        L_0x01dd:
            if (r1 == 0) goto L_0x01e2
            r1.close()     // Catch:{ IOException -> 0x01f8 }
        L_0x01e2:
            if (r4 == 0) goto L_0x01e7
            r4.close()     // Catch:{ IOException -> 0x01f8 }
        L_0x01e7:
            if (r2 == 0) goto L_0x01ec
            r2.close()     // Catch:{ IOException -> 0x01f8 }
        L_0x01ec:
            if (r3 == 0) goto L_0x01f1
            r2.close()     // Catch:{ IOException -> 0x01f8 }
        L_0x01f1:
            throw r0
        L_0x01f2:
            com.ironsource.mobilcore.aG$a r0 = r13.a
            r0.a()
            return
        L_0x01f8:
            r1 = move-exception
            android.content.Intent r2 = new android.content.Intent
            android.content.Context r3 = r13.b
            java.lang.Class<com.ironsource.mobilcore.MobileCoreReport> r4 = com.ironsource.mobilcore.MobileCoreReport.class
            r2.<init>(r3, r4)
            java.lang.StackTraceElement[] r3 = r1.getStackTrace()
            r4 = 0
            r3 = r3[r4]
            java.lang.String r4 = "com.ironsource.mobilcore.MobileCoreReport_extra_ex"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.Class<com.ironsource.mobilcore.aG> r6 = com.ironsource.mobilcore.aG.class
            java.lang.String r6 = r6.getName()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = "###"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = r1.getMessage()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = "###"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = r3.getClassName()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = ":"
            java.lang.StringBuilder r5 = r5.append(r6)
            int r3 = r3.getLineNumber()
            java.lang.StringBuilder r3 = r5.append(r3)
            java.lang.String r3 = r3.toString()
            r2.putExtra(r4, r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "caught ex "
            r3.<init>(r4)
            java.lang.Class<com.ironsource.mobilcore.MobileCore> r4 = com.ironsource.mobilcore.MobileCore.class
            java.lang.String r4 = r4.getName()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "###"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r1.getMessage()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "###"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r1 = android.util.Log.getStackTraceString(r1)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r3 = 55
            com.ironsource.mobilcore.C0031p.a(r1, r3)
            java.lang.String r1 = "caught ex in MobileCore sent to MobileCore"
            r3 = 3
            com.ironsource.mobilcore.C0031p.a(r1, r3)
            android.content.Context r1 = r13.b
            r1.startService(r2)
            goto L_0x01f1
        L_0x028e:
            r0 = move-exception
            android.content.Intent r1 = new android.content.Intent
            android.content.Context r2 = r13.b
            java.lang.Class<com.ironsource.mobilcore.MobileCoreReport> r3 = com.ironsource.mobilcore.MobileCoreReport.class
            r1.<init>(r2, r3)
            java.lang.StackTraceElement[] r2 = r0.getStackTrace()
            r3 = 0
            r2 = r2[r3]
            java.lang.String r3 = "com.ironsource.mobilcore.MobileCoreReport_extra_ex"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.Class<com.ironsource.mobilcore.aG> r5 = com.ironsource.mobilcore.aG.class
            java.lang.String r5 = r5.getName()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "###"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r0.getMessage()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "###"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r2.getClassName()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ":"
            java.lang.StringBuilder r4 = r4.append(r5)
            int r2 = r2.getLineNumber()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            r1.putExtra(r3, r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "caught ex "
            r2.<init>(r3)
            java.lang.Class<com.ironsource.mobilcore.MobileCore> r3 = com.ironsource.mobilcore.MobileCore.class
            java.lang.String r3 = r3.getName()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "###"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r0.getMessage()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "###"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = android.util.Log.getStackTraceString(r0)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r2 = 55
            com.ironsource.mobilcore.C0031p.a(r0, r2)
            java.lang.String r0 = "caught ex in MobileCore sent to MobileCore"
            r2 = 3
            com.ironsource.mobilcore.C0031p.a(r0, r2)
            android.content.Context r0 = r13.b
            goto L_0x01bf
        L_0x0321:
            r0 = move-exception
            r3 = r4
            r4 = r5
            goto L_0x01dd
        L_0x0326:
            r0 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x01dd
        L_0x032c:
            r0 = move-exception
            r2 = r3
            r3 = r4
            r4 = r6
            goto L_0x01dd
        L_0x0332:
            r0 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r6
            goto L_0x01dd
        L_0x0339:
            r0 = move-exception
            goto L_0x01dd
        L_0x033c:
            r0 = move-exception
            r4 = r5
            goto L_0x0085
        L_0x0340:
            r0 = move-exception
            r3 = r4
            r4 = r5
            goto L_0x0085
        L_0x0345:
            r0 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
            goto L_0x0085
        L_0x034b:
            r0 = move-exception
            r2 = r3
            r3 = r4
            r4 = r6
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.aG.start():void");
    }

    public interface a {
        final /* synthetic */ C0030o a;

        default a(C0030o oVar) {
            this.a = oVar;
        }

        /* JADX WARNING: Removed duplicated region for block: B:15:0x0055 A[SYNTHETIC, Splitter:B:15:0x0055] */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x005a A[Catch:{ IOException -> 0x009d }] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0085 A[SYNTHETIC, Splitter:B:32:0x0085] */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x008a A[Catch:{ IOException -> 0x008e }] */
        /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        default void a() {
            /*
                r5 = this;
                r2 = 0
                java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00ae, all -> 0x0081 }
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00ae, all -> 0x0081 }
                r0.<init>()     // Catch:{ IOException -> 0x00ae, all -> 0x0081 }
                com.ironsource.mobilcore.o r3 = r5.a     // Catch:{ IOException -> 0x00ae, all -> 0x0081 }
                android.app.Activity r3 = r3.c     // Catch:{ IOException -> 0x00ae, all -> 0x0081 }
                java.io.File r3 = r3.getFilesDir()     // Catch:{ IOException -> 0x00ae, all -> 0x0081 }
                java.lang.String r3 = r3.getPath()     // Catch:{ IOException -> 0x00ae, all -> 0x0081 }
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ IOException -> 0x00ae, all -> 0x0081 }
                java.lang.String r3 = "/agreement.html"
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ IOException -> 0x00ae, all -> 0x0081 }
                java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00ae, all -> 0x0081 }
                r1.<init>(r0)     // Catch:{ IOException -> 0x00ae, all -> 0x0081 }
                java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ IOException -> 0x00b1 }
                java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x00b1 }
                r0.<init>(r1)     // Catch:{ IOException -> 0x00b1 }
                r3.<init>(r0)     // Catch:{ IOException -> 0x00b1 }
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0044, all -> 0x00ab }
                int r2 = r1.available()     // Catch:{ IOException -> 0x0044, all -> 0x00ab }
                r0.<init>(r2)     // Catch:{ IOException -> 0x0044, all -> 0x00ab }
            L_0x003a:
                java.lang.String r2 = r3.readLine()     // Catch:{ IOException -> 0x0044, all -> 0x00ab }
                if (r2 == 0) goto L_0x005e
                r0.append(r2)     // Catch:{ IOException -> 0x0044, all -> 0x00ab }
                goto L_0x003a
            L_0x0044:
                r0 = move-exception
                r2 = r3
            L_0x0046:
                android.content.Context r3 = com.ironsource.mobilcore.MobileCore.b     // Catch:{ all -> 0x00a9 }
                java.lang.Class r4 = r5.getClass()     // Catch:{ all -> 0x00a9 }
                java.lang.String r4 = r4.getName()     // Catch:{ all -> 0x00a9 }
                com.ironsource.mobilcore.av.a(r3, r4, r0)     // Catch:{ all -> 0x00a9 }
                if (r2 == 0) goto L_0x0058
                r2.close()     // Catch:{ IOException -> 0x009d }
            L_0x0058:
                if (r1 == 0) goto L_0x005d
                r1.close()     // Catch:{ IOException -> 0x009d }
            L_0x005d:
                return
            L_0x005e:
                com.ironsource.mobilcore.o r2 = r5.a     // Catch:{ IOException -> 0x0044, all -> 0x00ab }
                java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0044, all -> 0x00ab }
                java.lang.String unused = r2.f = r0     // Catch:{ IOException -> 0x0044, all -> 0x00ab }
                if (r3 == 0) goto L_0x006c
                r3.close()     // Catch:{ IOException -> 0x0072 }
            L_0x006c:
                if (r1 == 0) goto L_0x005d
                r1.close()     // Catch:{ IOException -> 0x0072 }
                goto L_0x005d
            L_0x0072:
                r0 = move-exception
                android.content.Context r1 = com.ironsource.mobilcore.MobileCore.b
                java.lang.Class r2 = r5.getClass()
                java.lang.String r2 = r2.getName()
            L_0x007d:
                com.ironsource.mobilcore.av.a(r1, r2, r0)
                goto L_0x005d
            L_0x0081:
                r0 = move-exception
                r1 = r2
            L_0x0083:
                if (r2 == 0) goto L_0x0088
                r2.close()     // Catch:{ IOException -> 0x008e }
            L_0x0088:
                if (r1 == 0) goto L_0x008d
                r1.close()     // Catch:{ IOException -> 0x008e }
            L_0x008d:
                throw r0
            L_0x008e:
                r1 = move-exception
                android.content.Context r2 = com.ironsource.mobilcore.MobileCore.b
                java.lang.Class r3 = r5.getClass()
                java.lang.String r3 = r3.getName()
                com.ironsource.mobilcore.av.a(r2, r3, r1)
                goto L_0x008d
            L_0x009d:
                r0 = move-exception
                android.content.Context r1 = com.ironsource.mobilcore.MobileCore.b
                java.lang.Class r2 = r5.getClass()
                java.lang.String r2 = r2.getName()
                goto L_0x007d
            L_0x00a9:
                r0 = move-exception
                goto L_0x0083
            L_0x00ab:
                r0 = move-exception
                r2 = r3
                goto L_0x0083
            L_0x00ae:
                r0 = move-exception
                r1 = r2
                goto L_0x0046
            L_0x00b1:
                r0 = move-exception
                goto L_0x0046
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.aG.a.a():void");
        }
    }
}
