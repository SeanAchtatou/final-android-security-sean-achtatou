package com.ironsource.mobilcore;

import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

final class G extends C0037v implements C0041z {
    private LinearLayout a;

    public G(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "lineButton";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject, true);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g = new RelativeLayout(this.c);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        this.p = new ImageView(this.c);
        this.p.setId(j());
        n();
        this.m = new TextView(this.c);
        this.m.setBackgroundColor(0);
        this.m.setId(j());
        this.m.setSingleLine();
        this.m.setEllipsize(TextUtils.TruncateAt.END);
        this.m.setTextSize(2, 22.0f);
        this.n = new TextView(this.c);
        this.n.setBackgroundColor(0);
        this.n.setSingleLine();
        this.n.setEllipsize(TextUtils.TruncateAt.END);
        this.n.setTextSize(2, 12.0f);
        this.a = new LinearLayout(this.c);
        this.a.setOrientation(1);
        a(null, this.d.b(), this.m, this.n, this.o);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.m.setText(this.i);
        if (this.o != null) {
            this.o.setText(this.k);
        }
        if (this.n != null) {
            this.n.setText(this.j);
        }
        if (!TextUtils.isEmpty(this.l)) {
            this.p.setImageBitmap(C0017b.a(this.c, this.l));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
      com.ironsource.mobilcore.b.a(android.content.Context, float):int */
    public final void p() {
        boolean z;
        ViewGroup viewGroup = (ViewGroup) this.g;
        viewGroup.removeAllViews();
        this.a.removeAllViews();
        boolean z2 = !TextUtils.isEmpty(this.k);
        if (!TextUtils.isEmpty(this.j)) {
            z = true;
        } else {
            z = false;
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(15);
        layoutParams.rightMargin = C0017b.a(this.c, 9.0f);
        this.p.setLayoutParams(layoutParams);
        viewGroup.addView(this.p);
        viewGroup.addView(this.o);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(1, this.p.getId());
        layoutParams2.addRule(15);
        if (z2) {
            this.o.setVisibility(0);
            layoutParams2.addRule(0, this.o.getId());
        } else {
            this.o.setVisibility(8);
        }
        if (!z) {
            layoutParams2.addRule(15);
        }
        this.m.setLayoutParams(layoutParams2);
        if (z) {
            this.n.setVisibility(0);
            this.n.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams3.addRule(1, this.p.getId());
            layoutParams3.addRule(15);
            if (z2) {
                layoutParams3.addRule(0, this.o.getId());
            }
            this.a.setLayoutParams(layoutParams3);
            this.a.addView(this.m);
            this.a.addView(this.n);
            viewGroup.addView(this.a);
            viewGroup.setPadding(this.d.h(), 0, this.d.h(), 0);
            return;
        }
        this.n.setVisibility(8);
        viewGroup.addView(this.m);
        viewGroup.setPadding(this.d.h(), this.d.i(), this.d.h(), this.d.i());
    }
}
