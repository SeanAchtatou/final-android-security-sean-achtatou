package com.ironsource.mobilcore;

import android.app.Activity;
import android.graphics.Canvas;
import android.view.View;

/* renamed from: com.ironsource.mobilcore.ah  reason: case insensitive filesystem */
abstract class C0015ah extends Y {
    private C0009ab a;

    C0015ah(Activity activity, int i, C0009ab abVar) {
        super(activity, i);
        this.a = abVar;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        int width = getWidth();
        int height = getHeight();
        int i = this.u;
        int i2 = this.q;
        switch (this.a) {
            case LEFT:
                this.p.setBounds(i - i2, 0, i, height);
                break;
            case TOP:
                this.p.setBounds(0, i - i2, width, i);
                break;
            case RIGHT:
                this.p.setBounds(width - i, 0, (width - i) + i2, height);
                break;
            case BOTTOM:
                this.p.setBounds(0, height - i, width, (height - i) + i2);
                break;
        }
        this.p.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        switch (this.a) {
            case LEFT:
                this.s.layout(0, 0, this.u, i6);
                this.t.layout(this.u, 0, i5, i6);
                return;
            case TOP:
                this.s.layout(0, 0, i5, this.u);
                this.t.layout(0, this.u, i5, i6);
                return;
            case RIGHT:
                this.s.layout(i5 - this.u, 0, i5, i6);
                this.t.layout(0, 0, i5 - this.u, i6);
                return;
            case BOTTOM:
                this.s.layout(0, i6 - this.u, i5, i6);
                this.t.layout(0, 0, i5, i6 - this.u);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        if (mode == 1073741824 && mode2 == 1073741824) {
            int size = View.MeasureSpec.getSize(i);
            int size2 = View.MeasureSpec.getSize(i2);
            if (!this.v) {
                this.u = (int) (((float) size2) * 0.25f);
            }
            switch (this.a) {
                case LEFT:
                case RIGHT:
                    int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(size2, 1073741824);
                    int i3 = this.u;
                    int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i3, 1073741824);
                    this.t.measure(View.MeasureSpec.makeMeasureSpec(size - i3, 1073741824), makeMeasureSpec);
                    this.s.measure(makeMeasureSpec2, makeMeasureSpec);
                    break;
                case TOP:
                case BOTTOM:
                    int makeMeasureSpec3 = View.MeasureSpec.makeMeasureSpec(size, 1073741824);
                    int i4 = this.u;
                    int makeMeasureSpec4 = View.MeasureSpec.makeMeasureSpec(i4, 1073741824);
                    this.t.measure(makeMeasureSpec3, View.MeasureSpec.makeMeasureSpec(size2 - i4, 1073741824));
                    this.s.measure(makeMeasureSpec3, makeMeasureSpec4);
                    break;
            }
            setMeasuredDimension(size, size2);
            return;
        }
        throw new IllegalStateException("Must measure with an exact size");
    }

    public final void c(boolean z) {
    }

    public final void a(boolean z) {
    }

    public final void b(boolean z) {
    }

    public final boolean c() {
        return true;
    }

    public final void c(int i) {
        this.u = i;
        this.v = true;
        requestLayout();
        invalidate();
    }

    public final void d() {
    }
}
