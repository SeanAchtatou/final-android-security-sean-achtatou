package com.ironsource.mobilcore;

import java.util.ArrayList;

/* renamed from: com.ironsource.mobilcore.j  reason: case insensitive filesystem */
final class C0025j extends ArrayList<C0019d> {
    private static final long serialVersionUID = 1;

    C0025j() {
    }

    public final boolean contains(Object obj) {
        if (obj instanceof C0019d) {
            C0019d dVar = (C0019d) obj;
            for (int i = 0; i < size(); i++) {
                if (dVar.a.equals(((C0019d) get(i)).a)) {
                    return true;
                }
            }
            return false;
        }
        C0031p.a("DownloadList cannot contain an object that isn't a Download", 55);
        return false;
    }

    public final boolean a(String str) {
        for (int i = 0; i < size(); i++) {
            if (str.equals(((C0019d) get(i)).a)) {
                return true;
            }
        }
        return false;
    }

    public final boolean b(String str) {
        int i = 0;
        while (true) {
            if (i >= size()) {
                i = -1;
                break;
            } else if (str.equals(((C0019d) get(i)).a)) {
                break;
            } else {
                i++;
            }
        }
        if (i < 0 || i >= size()) {
            return false;
        }
        remove(i);
        return true;
    }
}
