package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.provider.Browser;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.ironsource.mobilcore.CallbackResponse;
import com.ironsource.mobilcore.H;
import com.ironsource.mobilcore.J;
import com.ironsource.mobilcore.ax;
import java.util.ArrayList;
import java.util.HashMap;

class Utils implements ax.a {
    protected boolean a;
    int b = -1;
    private String c;
    /* access modifiers changed from: private */
    public Context d;
    /* access modifiers changed from: private */
    public Activity e;
    /* access modifiers changed from: private */
    public WebView f;
    private View g;
    /* access modifiers changed from: private */
    public CallbackResponse h;
    /* access modifiers changed from: private */
    public J i;
    private boolean j;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public OrientationEventListener n;
    /* access modifiers changed from: private */
    public String o;
    /* access modifiers changed from: private */
    public Dialog p;
    private Window q;
    private WindowManager r;
    private OfferwallBroadcastReceiver s;
    private boolean t = false;

    static /* synthetic */ RelativeLayout.LayoutParams k(Utils utils) {
        ((WindowManager) utils.d.getSystemService("window")).getDefaultDisplay().getMetrics(new DisplayMetrics());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        return layoutParams;
    }

    public Utils(String str, Context context, WebView webView, CallbackResponse callbackResponse) {
        this.d = context.getApplicationContext();
        this.b = this.d.getResources().getConfiguration().orientation;
        this.c = str;
        this.f = webView;
        this.h = callbackResponse;
        this.i = new J(this.d, this.c);
        this.r = (WindowManager) this.d.getSystemService("window");
    }

    public void setConfiguration(boolean z, boolean z2) {
        this.l = z2;
        this.j = z;
    }

    public void setActivity(Activity activity) {
        this.q = activity.getWindow();
        this.e = activity;
    }

    public void setCallBackListener(CallbackResponse callbackResponse) {
        this.h = callbackResponse;
    }

    private static String a(String str) {
        return "content://" + str + ".settings/favorites?notify=false";
    }

    /* access modifiers changed from: protected */
    public final void a(J j2) {
        this.i = j2;
    }

    public void setFlow(String str) {
        this.m = str;
    }

    public void setFlowName(String str) {
        this.o = str;
    }

    public void openReport(String str, String str2) {
        av.a(this.d, this.m, this.o, str, str2);
    }

    public void setSharedBooleanPrefs(String str, boolean z) {
        SharedPreferences.Editor edit = av.b().edit();
        edit.putBoolean(str, z);
        edit.commit();
    }

    public void setSharedStringPrefs(String str, String str2) {
        SharedPreferences.Editor edit = av.b().edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public void setSharedStringPrefs(String str, int i2) {
        SharedPreferences.Editor edit = av.b().edit();
        edit.putInt(str, i2);
        edit.commit();
    }

    public boolean getPreferenceBoolean(String str, boolean z) {
        return av.b().getBoolean(str, z);
    }

    public String getPreferenceString(String str, String str2) {
        return av.b().getString(str, str2);
    }

    public int getPreferenceSInt(String str, int i2) {
        return av.b().getInt(str, i2);
    }

    public boolean isDownloadReady() {
        return MobileCore.a(this.d) == 9;
    }

    public void initReset() {
    }

    public void initOfferwallFlow() {
        if (!getPreferenceBoolean("com.ironsource.mobilecore.prefs_offerwall_fetch", false)) {
            aF.a().j();
        }
    }

    class a implements DialogInterface.OnDismissListener {
        private a() {
        }

        /* synthetic */ a(Utils utils, byte b) {
            this();
        }

        public final void onDismiss(DialogInterface dialogInterface) {
            CallbackResponse.TYPE type;
            C0031p.a("onDismiss", 55);
            aF a2 = aF.a();
            a2.a(false);
            a2.c(false);
            a2.b();
            Utils.this.n.disable();
            if (Utils.this.h != null) {
                if (Utils.this.i.b()) {
                    type = Utils.this.i.a() ? CallbackResponse.TYPE.OFFERWALL_QUIT : CallbackResponse.TYPE.OFFERWALL_BACK;
                } else {
                    type = CallbackResponse.TYPE.OFFERWALL_BACK;
                }
                Utils.this.h.onConfirmation(type);
            }
            Utils.this.i.c();
            if (Utils.this.f != null) {
                aF.a().k();
                aF.a().l();
            }
        }
    }

    class b extends WebViewClient {
        RelativeLayout a;

        b(RelativeLayout relativeLayout) {
            this.a = relativeLayout;
        }

        public final void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            sslErrorHandler.proceed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ironsource.mobilcore.J.a(java.lang.String, java.lang.String, org.json.JSONObject, boolean):void
         arg types: [java.lang.String, java.lang.String, org.json.JSONObject, int]
         candidates:
          com.ironsource.mobilcore.J.a(java.lang.String, java.lang.String, java.lang.String, boolean):void
          com.ironsource.mobilcore.J.a(java.lang.String, java.lang.String, org.json.JSONObject, boolean):void */
        public final void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            if (!Utils.this.k || Utils.this.a) {
                if (Utils.this.a) {
                    Utils.this.a = false;
                    webView.loadUrl("javascript:loadContent()");
                    if (Utils.this.k) {
                        return;
                    }
                }
                boolean unused = Utils.this.k = true;
                Utils.this.i.a(Utils.this.m, Utils.this.o, aF.a().g(), false);
                webView.loadUrl("javascript:loadContent()");
                this.a.setVisibility(8);
                webView.setVisibility(0);
            }
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            return Utils.this.i.a(webView, str, new J.a(this));
        }

        public final void onReceivedError(WebView webView, int i, String str, String str2) {
            Toast.makeText(Utils.this.d, "There was an error", 1).show();
            C0031p.a("error description: " + str, 2);
            C0031p.a("error code: " + i, 2);
            if (TextUtils.isEmpty(str2)) {
                str2 = "noUrlReturend";
            }
            av.a(Utils.this.d, Utils.class.getName(), "Offerwall webview error: errorCode = " + i + " url = " + str2);
            this.a.setVisibility(8);
            if (Utils.this.p != null) {
                Utils.this.p.dismiss();
            }
            aF.a().c(false);
            super.onReceivedError(webView, i, str, str2);
        }
    }

    @SuppressLint({"NewApi", "SetJavaScriptEnabled"})
    public void showOfferWall(String str) {
        ViewGroup relativeLayout;
        C0031p.a("showOfferWall | showing offerwall.", 55);
        C0031p.a("url is: " + str, 55);
        if (this.l) {
            relativeLayout = new H(this.d, new H.a() {
                public final boolean a() {
                    Utils.this.a(true);
                    return false;
                }
            });
        } else {
            relativeLayout = new RelativeLayout(this.d);
        }
        ay a2 = aF.a().a(this.h);
        RelativeLayout relativeLayout2 = new RelativeLayout(this.d);
        ImageView imageView = new ImageView(this.d);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        imageView.setLayoutParams(layoutParams);
        final AnimationDrawable i2 = aF.a().i();
        if (i2 != null) {
            i2.setOneShot(false);
            C0017b.a(imageView, i2);
            i2.stop();
            imageView.post(new Runnable(this) {
                public final void run() {
                    i2.start();
                }
            });
        }
        relativeLayout2.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        relativeLayout2.setBackgroundColor(805306368);
        relativeLayout2.setFocusable(true);
        relativeLayout2.setClickable(true);
        relativeLayout2.addView(imageView);
        relativeLayout.addView(a2);
        relativeLayout.addView(relativeLayout2);
        a2.setVisibility(8);
        imageView.bringToFront();
        aF.a().a(this.i);
        a2.setWebViewClient(new b(relativeLayout2));
        a2.loadUrl(str);
        this.n = new OrientationEventListener(this.d.getApplicationContext(), 2, a2, relativeLayout2) {
            private /* synthetic */ ay a;
            private /* synthetic */ RelativeLayout b;

            {
                this.a = r5;
                this.b = r6;
            }

            public final void onOrientationChanged(int i) {
                if (Utils.this.d.getResources().getConfiguration().orientation != Utils.this.b) {
                    if (!av.h(Utils.this.d)) {
                        Utils.this.n.disable();
                        return;
                    }
                    C0031p.a("on orientation changed", 55);
                    Utils.this.b = Utils.this.d.getResources().getConfiguration().orientation;
                    this.a.setLayoutParams(Utils.k(Utils.this));
                    this.a.postInvalidate();
                    if (this.a != null && !this.b.isShown()) {
                        Utils.this.a = true;
                        this.a.reload();
                    }
                }
            }
        };
        this.n.enable();
        if (this.l) {
            WindowManager.LayoutParams layoutParams2 = new WindowManager.LayoutParams();
            layoutParams2.height = -1;
            layoutParams2.width = -1;
            layoutParams2.alpha = 1.0f;
            layoutParams2.format = 1;
            layoutParams2.type = 2002;
            this.g = relativeLayout;
            this.s = new OfferwallBroadcastReceiver((byte) 0);
            this.s.a(this);
            this.g.setBackgroundColor(Color.parseColor("#363636"));
            this.r.addView(this.g, layoutParams2);
            this.t = false;
            this.d.registerReceiver(this.s, new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
            return;
        }
        this.p = new Dialog(this.e, 16973835);
        this.p.getWindow().requestFeature(1);
        this.p.getWindow().setBackgroundDrawableResource(17170445);
        this.p.setOnDismissListener(new a(this, (byte) 0));
        this.p.setContentView(relativeLayout);
        Dialog dialog = this.p;
        WindowManager.LayoutParams attributes = this.q.getAttributes();
        WindowManager.LayoutParams attributes2 = dialog.getWindow().getAttributes();
        if (Build.VERSION.SDK_INT >= 11) {
            attributes2.systemUiVisibility = attributes.systemUiVisibility;
            if (this.q.hasFeature(1)) {
                int i3 = Build.VERSION.SDK_INT;
                attributes2.systemUiVisibility = 1;
            }
            if ((this.e.getWindow().getAttributes().flags & AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END) == 1024) {
                attributes2.flags = AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END;
            }
        } else if ((this.q.getAttributes().flags & AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END) == 1024) {
            attributes2.flags = AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END;
        }
        attributes2.width = -1;
        attributes2.height = -1;
        this.p.getWindow().setAttributes(attributes2);
        this.p.show();
        aF.a().c(true);
    }

    static class OfferwallBroadcastReceiver extends BroadcastReceiver {
        private Utils a;

        private OfferwallBroadcastReceiver() {
        }

        /* synthetic */ OfferwallBroadcastReceiver(byte b) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            this.a.a(true);
        }

        public final void a(Utils utils) {
            this.a = utils;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        if (z) {
            this.i.c();
        }
        if (this.g.getParent() != null) {
            this.r.removeView(this.g);
        }
        if (!this.t) {
            this.d.unregisterReceiver(this.s);
            this.t = true;
        }
    }

    public boolean checkIcon(String str, String str2) {
        String str3;
        C0031p.a("DROPICON: checkIcon", 55);
        HashMap hashMap = new HashMap();
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        PackageManager packageManager = this.d.getPackageManager();
        for (ResolveInfo next : packageManager.queryIntentActivities(intent, 0)) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            String str4 = next.activityInfo.packageName;
            packageManager.getPreferredActivities(arrayList, arrayList2, str4);
            if (arrayList.size() > 0) {
                if (next.activityInfo.packageName.equals("com.motorola.blur.home")) {
                    str3 = "com.android.launcher";
                } else {
                    str3 = str4;
                }
                C0031p.a("packageName " + str3 + " is preferred", 55);
                hashMap.put(str3, true);
            } else {
                if (next.activityInfo.packageName.equals("com.motorola.blur.home")) {
                    str4 = "com.android.launcher";
                }
                C0031p.a("packageName " + str4 + " is not preferred", 55);
                hashMap.put(str4, false);
            }
        }
        for (String str5 : hashMap.keySet()) {
            boolean a2 = a(Uri.parse(a(str5)), str5, str);
            if ((a2 && ((Boolean) hashMap.get(str5)).booleanValue()) || (a2 && hashMap.size() <= 1)) {
                return true;
            }
        }
        return false;
    }

    private boolean a(Uri uri, String str, String str2) {
        Cursor cursor;
        Cursor cursor2;
        C0031p.a("DROPICON: hasIcon", 55);
        try {
            cursor = this.d.getContentResolver().query(uri, null, null, null, null);
        } catch (Exception e2) {
            C0031p.a("sec ex in query1 " + e2.getMessage(), 2);
            cursor = null;
        }
        if (cursor == null) {
            try {
                cursor2 = this.d.getContentResolver().query(Uri.parse(a(str.substring(0, str.lastIndexOf(".")))), null, null, null, null);
            } catch (Exception e3) {
                cursor2 = null;
            }
            if (cursor2 != null) {
                cursor2.moveToFirst();
                do {
                    int columnIndex = cursor2.getColumnIndex("intent");
                    if (columnIndex >= 0) {
                        C0031p.a(cursor2.getString(columnIndex), 55);
                        if (cursor2.getString(columnIndex) != null && cursor2.getString(columnIndex).contains(str2)) {
                            C0031p.a("found search icon in package" + str, 55);
                            cursor2.close();
                            return true;
                        }
                    }
                } while (cursor2.moveToNext());
                cursor2.close();
            }
        } else {
            cursor.moveToFirst();
            do {
                int columnIndex2 = cursor.getColumnIndex("intent");
                if (columnIndex2 >= 0 && cursor.getString(columnIndex2) != null && cursor.getString(columnIndex2).contains(str2)) {
                    C0031p.a("found search icon in package" + str, 55);
                    cursor.close();
                    return true;
                }
            } while (cursor.moveToNext());
            cursor.close();
        }
        C0031p.a("no icon found in package" + str, 55);
        return false;
    }

    public void processUserAgreementAction(String str) {
        C0031p.a("DROPICON: icon offer  , setUserAction() | called. action:" + str, 55);
        C0030o.a().b(this.e, str);
    }

    public String getUserAgreementAction() {
        String b2 = C0030o.a().b();
        C0031p.a("DROPICON: icon offer  , getUserAgreementAction() | called. action to return:" + b2, 55);
        return "'" + b2 + "'";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
     arg types: [java.lang.String, long]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void} */
    public void addBookmark(String str, String str2, String str3) {
        boolean z;
        C0031p.a("DROPICON: icon offer , addBookmark() | called. title:" + str + " , url:" + str2, 55);
        aD aDVar = new aD("", str, 0, str3, "Bookmark", str2, 0.0d, "", "", null, "");
        Context context = this.d;
        int checkCallingOrSelfPermission = context.checkCallingOrSelfPermission("com.android.browser.permission.WRITE_HISTORY_BOOKMARKS");
        int checkCallingOrSelfPermission2 = context.checkCallingOrSelfPermission("com.android.browser.permission.READ_HISTORY_BOOKMARKS");
        if (checkCallingOrSelfPermission == 0 && checkCallingOrSelfPermission2 == 0) {
            long currentTimeMillis = System.currentTimeMillis();
            Cursor query = context.getContentResolver().query(Browser.BOOKMARKS_URI, new String[]{"_id"}, "url=? AND bookmark=1", new String[]{str2}, null);
            if (query == null || !query.moveToNext()) {
                Cursor query2 = context.getContentResolver().query(Browser.BOOKMARKS_URI, null, null, null, null);
                boolean z2 = query2.getColumnIndex("folder") >= 0;
                query2.close();
                ContentValues contentValues = new ContentValues();
                contentValues.put("title", str);
                contentValues.put("url", str2);
                contentValues.put("bookmark", (Integer) 1);
                contentValues.put("created", Long.valueOf(currentTimeMillis));
                contentValues.put("date", (Long) 0L);
                contentValues.put("visits", (Integer) 0);
                if (z2) {
                    contentValues.put("folder", (Integer) 0);
                }
                context.getContentResolver().insert(Browser.BOOKMARKS_URI, contentValues);
            } else {
                String[] strArr = {String.valueOf(query.getLong(query.getColumnIndex("_id")))};
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("created", Long.valueOf(currentTimeMillis));
                context.getContentResolver().update(Browser.BOOKMARKS_URI, contentValues2, "_id=?", strArr);
                query.close();
            }
            z = true;
        } else {
            C0031p.a("missing bookmarks permissions. canReadBookmarks:" + checkCallingOrSelfPermission2 + " , canWriteBookmarks:" + checkCallingOrSelfPermission, 3);
            z = false;
        }
        if (z) {
            openReport("+", aDVar.f());
        } else {
            openReport("-", aDVar.f());
        }
    }

    public void setHomepage(String str) {
        C0031p.a("DROPICON: setHomepage", 55);
        C0031p.a("icon offer , setHomepage() | called. url:" + str, 55);
    }

    public void showDialog(final String str, final String str2, final String str3) {
        C0031p.a("DROPICON: in showDialog " + System.currentTimeMillis(), 55);
        MobileCore.a.post(new Runnable() {
            public final void run() {
                new C0018c(Utils.this.e, str, str2, new CallbackResponse() {
                    public final void onConfirmation(CallbackResponse.TYPE type) {
                        if (Utils.this.h != null) {
                            Utils.this.h.onConfirmation(type);
                        }
                        C0031p.a("DROPICON: in onConfirmation in JS showDialog", 55);
                        C0031p.a("DROPICON: javascript:" + str3 + "('" + type.toString() + "')", 55);
                        Utils.this.f.loadUrl("javascript:" + str3 + "('" + type.toString() + "')");
                    }
                }).a();
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    @SuppressLint({"NewApi"})
    public void createShortcutToURL(String str, String str2, String str3) {
        byte[] decode;
        boolean z;
        int i2;
        C0031p.a("DROPICON: icon offer  , createShortcutToURL() | called", 55);
        if (TextUtils.isEmpty(str)) {
            str = "iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDowMzgwMTE3NDA3MjA2ODExQTRFMEQzRDI2RDUyMTgzQyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpERjdBREM4RDU3RkMxMUUyOUQ0RUZFRDM2M0NBOEQwQyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpERjdBREM4QzU3RkMxMUUyOUQ0RUZFRDM2M0NBOEQwQyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkJGNDM0RUJERjM1N0UyMTE4OTI4RjA1ODM3NDdDQzg0IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjAzODAxMTc0MDcyMDY4MTFBNEUwRDNEMjZENTIxODNDIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+uA2vnwAAERFJREFUeNrsXQmQFOUVfj3nzs619wLLggvLjQICarwPKMFEiMYLcyiVqpQHKJ6JsaxoYiyiRjGKWrEMZcSYiqgpiYWKQPAoUUBY7mO5Flhgd9mdmd05e6Y77/XM7HbPTs90z/bM7mzmFX9tzwzT3fO+97/7/5vheR4K1H+kK7CgAEABgAL1HxkU/j8jjlmxMRVHVey9AvWQD8dpHHtxrMWxDgeb7kuMAiN8A45ncdQXeKyKGnH8GscHmaogPY4/xU5QYL56Ip69H+OhPpMZQF98NP7CGwbY2MrBHg9Aa5CHcMF7lepyBqDSzMBEB8AVlTqwSpX7s7HZoBiAm3C8F3+xw83Du00RCEQKjFZCRSjvt9XqYUoJI377ZhyrlABgxHeO4Fdroszn4G9HIlAQeHVErF9YhyA4u7X8SRx1iYY5mRc0J878LlQ7bx8LQyTH3Oc4kgIezAwPeo4nKcE36X1e+Az/QIST3pRexwD+A52OBiNYN55hIILHQZ4BFo91OXa6VyLvRk80gS3KZeLpXBwfpQNgVvzgv60s6v7s6p0wGhN9iAc/6jeXl4eTngic6tJ+xpFEDrXpocahhxIrAxbUExETAwYDk7XfRnZzXQsL84cZxbxNC8C58YNtHST9nMbSzYMuCNDp5eBwaxiOd+bGsBCgzQgsDTHV2vUwqtIAdqsOOHNs9mhIDa6wGIDJSgKxim6l5dNG/XB4El0Az3cWvag2FsIcDBgiAYgLgQFV1MQKI9SUIxhFCIa+72A0+yQ/tlIJAKb4QYDLXDpp4hiDDDSf5WEnTkMuD6w4CcYOvNcdLSDYk3OrjDCsHO2HGe1QhvYjwV03qkpFhDMAgIkwEO7Uw/YTLHjY/PWdSGAazrA4ABxGBqYON4LBjrZJr+1vSgkAyysHwBjSg6tDB9tPhVHf5kavj0SDeh7q7zqnHoajLq8sZsBu0oEFDSt5ST4Uvy4UglNdnKBmGjsisK0lDB0BdTqQBOmLIyHBkE8daoCSUg5YUyT7ACiZAWbWAC2tetjbRv83+8q9vkQP14w0weW1JigrktcLpL+dNNCw1qD3M2NIz+w/6g7D+iYWNjSFoM2v/J5J9rehgMEpgAkVBqiqjEDQGO6bd5YkENuHYxwdXP/lcXnGcwbwtJlhd0v2pZ0kbzoy8PYJZphQbtRUzXxxPAT/3OeHY57MhGdSlR4cFUEI6uSBWH1ZbfxwP47ximdAKNL7pHpOB2avDTad4HKiakahernnfAtMSsF4I04EM0q7GVUPOS5kQPkYg8mwBtELC6I6SvTo6P9dOcIkjE+OBGHFTj94Qup0PAkg02KAmcNNELR2YeDHaaeCWE4KQGnYAbua9KhXs69qiJE/n1QEN4+zQDLX3IaG0VmEOh//mhS6i34EgRjsCvACKJLwv84Ml9aY4KXvffDViZDqGOM7FEib0QqTR0Sgw+DRBoBADAArb4bQWSds6uBil8sulVt08NhFxb2knoAoRb1faVHOdDGRcaZRXQzgQ8N6Bn30TpHE2zAyfvwiK6w+pIe/NvhVxytk8Dcd0kF9aRmYyt3gZYJ9AyAUCUENVwENTSacDbmJnmrtOnj6MjtUFUsNbImZEVIJRo3yOcU4c8h7IiBOopfkFzns148ugmFWPfxxk1fyvlJqREE1uu0wZYQZTuraUv7flD/H2VkDW44S83Pje5+DDHnuSinzSdDp/REO7ZifCMSYUj0MsepAPKfI6C+93CZ8ngkRz4h3xMOMAdjXmrtAqhoZ8PSlNnQbdRKVMbbMAA4Tk/XrE+ij0MUVaza69hOokgx9AD4dDwdEV0QRMvqpS2yC7hcbWWKIGqknw3oQg63tGGztbA2jaxlRlcuy4jVHJ1xzarURFk0rztpvNwwEAO6dZhGiWrFaILWjJDG5qZlFoxkUPJfD7kivnBMx89xKI1yFruYNY8yC3k8nDHUIwiEXghdTvdeih7SjlYK3UK4ByL4KuniYCWaNNEsYdo4jPfM/RqY/v9kLBzrCaXXx92dCwnhhc5cAxKMX2BAU+Z9ehHqI7uGwq6cucS/OgoZWFs76tTWIuv5WPXdPLZZEvCNRQlPp3NNeDm5d7YJffeZOy/xk4kRSfN377fDk113o5fEp1dEQm04yK++aor0q6lcAbhprhgqRx1OJx8UpKlSbT7Mw+1/tqgOlZCmIN3b4YN6HHdCaQqIr0SaJvaBLMdqdUG4YHACQhzO/vqj7NQVW1cXyt/NNcwgWoOS3B7RTAWSof4wgtPjkzzncJnVPb59oGRwAUOhvM/VcvrqYAUZG+EnVLFzjzigoSkdH0XD/7GNXr9SEWE06zT03NgO9onPSGPK8AGDWSJPE8JbIpJZZ1Bd3feaRpAy0pt1tYfgd2gTZGMEqvbfZonvPSwDI5RxV0qNLKa8vp/mXb/PB/vZw1u9p5W4/bDnNynpFYltwZa0ZtKrd9wsA4uKIkOcpSv5rPEEeXt3my8k90fxa+q1X9vNSkRoqQ+OslRrqFwCmVhklxtcsk9l8Z68fvDmsK5Ohp4ArGdlNUlZNqzLmLwDjy/SSlIMcrToQyPm9fXAw+TVN+uiI07gyQ34CYETlKfZ+imR+xxkMuPadDeccgA3HgikDxzjV2LRhXc5zQcXG3gYuGW1rYdXPLAySLhxqRH2tE+IFUimUnFNDja6I4HHZk2Rgo6qS744P8hIAY4L7IJd2oDyMGnpophV+OkEaJC2ZboW30Lt5aatX1bkoqTclSa5InCU1GaKeG59vKkifcEWdTPTVEVQe8ZJfnsj8ON0xyYJuozq/3S1z7UTXk9HAFc05AErvWU0Vbv6YopSf35Dm80SSqwUzoH1hKOcAJObrOZlJLPa709EIe+qfUetQ57PbZSpwkYQeKi02Gcg5AKEE1S5X66+xK2eaO01y1BNUl8Crlbm2OF1EbfZ8Ps6AxKSXXBJskoq071cnUrd/fKkifU1l0SFWnYzw9NzrGZ82WdmcAxAI85IWF7mFf5R3r7Aou7139gTgqCf5iRo7wvDuPuUB3SU1phT33nNMrSx5CQDJ0BF3z82nSjX8aLRZ0TmpIWrhGhd8iFGsL2a96byrDvjhl5+6hd4fxQa93iyrfgKidPhhtzZBYr8U5RswyBpbGr20n432bCaLx+6YbIG3dvkV6Vp3kIc/fNOFAzL2z6mLWlyfFpM3JNX528+E83MGCFGu6Ob5FH43gXSdwlmQOMsyofunF8sGhi7RPYZRhe45m8cA7GhjMdDq0dntAXmWPXWxXSiQZ5soQ7tAJpijGeoWFYS+aQ7LOg95AQD122wU9diQjpazBUNtOlh6hT2r90MZ2eWzHLJFFlrEIfb51x0LanbtfitJUjMVxytz627ESPaBGdas3Aflpt6Y45QtsJCgt4nujdYwy1XO8gqAZnTjxP55F05xMqRy9PBMKzyoMQjUmbFirhMuHy7velIfkljb0GoaLXcO6Ne+oLd3+wWD1gNKTztgMqKM52uzHbKpAjVEa83+c2Op0CknR6QWxZ1wTR7t2xN16f2J7I2TyPBV+4Pdr6kDoqkztXcxr74INt5WjmqJCuPqr2k1RoFce0uZUD9IZaeahOCu57uvbvehwGTyWwdYHCCmd3FK/6DG2N2cS8UQ0rO0GEOOqJX95VlOWIIqaeUeP3zUGBBUhRzRfJlYYUDQLOjpFEla4JMR8fgISjsryhzSGrKGFlbz359ylWTNa2dyAgIxf9k1Dkl1bIhV32uVTCo65AoLvjk1WpFXRXUHp0kndDpPQRezUmFag3hOqka8WI/SHPev86TsJU1FJ++ujh+qWyWZK6I+/ue/64LfXmjv3lLmtDciTPdhNmVZ0dElBmH0yT3mo2uIxS6xK8DB77/uzJj5A9oIi+nrkyy8ss3by/8+ggzJxeYe1PZ4sEPKfJpJT3zVBae82buBfgUgMexfg3r2L1u9khoB2YQDHawkFaBpcpCnDoyIkDUVSzmlRx77ohMaXdntzOg3AGiPh09vLoPF51t7gfD0pi5JBjMc80hIz3dp1CPKCykQDvYj4ykIFJ+1uZODhzZ4VK8/yBsAyPd+fbYTSs16WDi5uBcI1E5y3zp3r84IUg+UBiZVQeopk9WblFI+hRJPPUcnOiO9dDvtH7EIr61Vvj+tFsg1828ZZ4FHLrCCXtRSQCAQvfx9jw0gBixZ74GfjDXDgvEWMImaokhf+2O7X1GzFCXrzLEWR/J+yJkStpkTtiqgVfHR7xCArMzGRZRueL3BK9iinKrh9BNVI38Xx+LzbXDn5OTLfKIg8BIQiFkU+m84HkQQioVcfWJbC0l0oA/rBjpR11M74r8xlgj0w2aoOZkBZGyfvMQB19Wlbg9ZONkqLD9K7IqjNsVlW7vgH3t9MBfPQSqs2tq37uQD7WH47GgQ1jcFs7LwY8AAQOrhxaucMGNI+uYoijR3tsmrAFpK9NZuH/wdB6URplYa4TwMskY79WBPE922+CKCUd3REoYtp0NZdS0HDAAUyb5yTQnUl6a/DLmBi9a5FPn8JK970YjSoFRGfJZRq7uBidoBsgHUxxPfrmag7llnSKu4MyRacb58lhOqLOmZfwKN6T3IfC+pggyvSV8Nh1Mkv5h8BCBDohUwf77C2WtRQzJqRdVwz+cuVVuHDSbSHIC5dWZ4Cg2uQUHnqicYlfwTnf+/u4Ib0gUtRSq29iUX875pNoUBEQeL17uFPRkGM6VzbVMC0B6ICGU7yp+nWhVIgc/DM+1w6ziLotiB7umhje6UHk++Exl9yif5w30syNAJQpGIsI432WI6miFPo8q5eoSy/h2K/B//0i2kGwYrkddFaWwlGWyDUqZRbZRmA22epI9NhxKcGcuudsJ5FcpXDC79rhPWatjWMZCINoulQo6awE6VEfbHwn4qio8rMwpu5gi78lMs3+6F9w/4Bx3jKebwstENANWGG6q9ILpAndMAb84pVVzmI1q51wdv7vQOOuYLW2EGuYxbVVQDQNK/al65sHQ/Hl2SRkrlK33U6IcXt3QNOsZ3hrg+V+tU1wNomq3Y5e1l8UkCkgkBJbv+sKlzUDyDhlQNFYoor0QbgGtRKs0oHf3Mtx7hs3un2XsBIUT9sRmx9XQIfoMeTyTPn9hKPUI+5DbVE7TOKWUcCT/zbafwNxGEuKRQSvnOT9qFmUHtJgyTf9IewJsniQ9m8SlGfUpFyIFw0MXCL9ac7W7rI96bY9sGUyyhG6BgkHQTs0m/02bfuZi3fc4FJYJAZcIFq9slPZU8SCtX0R1Sokv/qTu5v2YHSTnLRSWcypahyMCoiHVbWGKQkukXB2HBBCvcurpNKHqnIvqhtFyVTDPx3igAEW0VN8T+ag1KnNlkOOkvFfTZSPalPGE3GFYJAK3xg/FlBmGvTKUz4bnNXbJF71RmPg6I2OjrYs8BoOK94OZGn80WM/BMt7GPMzf6bR5iz3zrLsqTA8Dx0G8FmVHSbr1WJQBQ/+K1dDC/3oIAKM/ZsBr+yjjTwnnuwP5wlKQOvktJHPB5/GAR6nWHqfDQ7UyJePfAdEdS3qYC4BM++uBJYSPVFXPLgCnwUjURz4h3lbEOb+RpM/FWCQAsfvmB+IsbxxTDqvkVaXvqC9RDxKv35lUIvBMBsoTMXS+gUjzQmR5C/Ej8BdVsX9jigdWH/MI2kixXeMCtmMi1pnXN14+2wIMzHInbLDwHoodjKwWAOp+WkSkosLdPtBzH/ZTRSPZhKr1CX1iM4yaE6FCBj6qpEaJP0V4kx/x0M0BMVPKag2M2jkk4qmGArK4ZQES97LSmazeOtTGDmzaIUgpAgbJEBdemAEABgAL1I/1PgAEAqRCLVAg9gdwAAAAASUVORK5CYII=";
        }
        if (Build.VERSION.SDK_INT <= 8) {
            decode = C0007a.a(str, 0);
        } else {
            decode = Base64.decode(str, 0);
        }
        C0031p.a("DROPICON: fitIconToDensity", 55);
        int i3 = this.d.getResources().getDisplayMetrics().densityDpi;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDensity = i3;
        options.inTargetDensity = i3;
        Bitmap decodeByteArray = BitmapFactory.decodeByteArray(decode, 0, decode.length, options);
        decodeByteArray.setDensity(i3);
        Context context = this.d;
        if ((context.getResources().getConfiguration().screenLayout & 15) == 4) {
            z = true;
        } else {
            z = false;
        }
        boolean z2 = z || ((context.getResources().getConfiguration().screenLayout & 15) == 3);
        switch (i3) {
            case 120:
                if (!z2) {
                    i2 = 36;
                    break;
                } else {
                    i2 = 48;
                    break;
                }
            case 160:
                if (!z2) {
                    i2 = 48;
                    break;
                }
            case 213:
                i2 = 72;
                break;
            case 240:
                if (!z2) {
                    i2 = 72;
                    break;
                } else {
                    i2 = 96;
                    break;
                }
            case 320:
                if (!z2) {
                    i2 = 96;
                    break;
                } else {
                    i2 = 144;
                    break;
                }
            case 480:
                i2 = 144;
                break;
            default:
                i2 = 72;
                break;
        }
        C0031p.a("bitmap props h: " + decodeByteArray.getHeight() + " w: " + decodeByteArray.getWidth() + " density" + decodeByteArray.getDensity(), 55);
        Bitmap createScaledBitmap = Bitmap.createScaledBitmap(decodeByteArray, i2, i2, true);
        C0031p.a("*****  currentBitmap density:" + createScaledBitmap.getDensity() + " height: " + createScaledBitmap.getHeight() + " width: " + createScaledBitmap.getWidth(), 55);
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
        Intent intent2 = new Intent();
        intent2.putExtra("duplicate", false);
        intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
        intent2.putExtra("android.intent.extra.shortcut.NAME", str3);
        intent2.putExtra("android.intent.extra.shortcut.ICON", createScaledBitmap);
        intent2.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        this.d.sendBroadcast(intent2);
        C0031p.a("DROPICON: icon offer, DROPPED!", 55);
    }

    public String getCRCD() {
        C0031p.a("DROPICON: getCRCD", 55);
        return "'" + C0030o.a().c() + "'";
    }

    public boolean getPermissionToDownloadOfferWallAssets() {
        return this.j;
    }

    public void downloadResourceFile(String str) {
        aF.a().a(str);
    }

    public void downloadAndStoreOfferWallFile(String str, String str2) {
        aH.a(str, new C0024i(aF.a().f(), str2, null));
    }

    public void downloadAndStoreOfferWallFeedFile(String str, String str2) {
    }

    public String getFilesPath() {
        String str = "'" + aF.a().e() + "'";
        C0031p.a("path before return to js: " + str, 55);
        return str;
    }

    public String getMobileParams() {
        return av.j(this.d);
    }

    public void setDataToReportOnFeed(String str) {
        C0031p.a(str, 55);
        aF.a().b(str);
    }

    public void notifyOfferWallRequstedAllAssets() {
    }

    public String getOfferwallJson() {
        return aF.a().h();
    }

    public void reportImpressions(int i2, int i3) {
        this.i.a(i2, i3);
    }
}
