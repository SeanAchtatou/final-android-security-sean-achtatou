package com.ironsource.mobilcore;

import java.util.HashMap;

/* renamed from: com.ironsource.mobilcore.m  reason: case insensitive filesystem */
final class C0028m extends C0017b {
    private String q;
    private String r;
    private String s;
    private String t;

    public C0028m() {
    }

    public C0028m(C0017b bVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c;
        this.d = bVar.d;
        this.e = bVar.e;
        this.f = bVar.f;
        this.g = bVar.g;
        this.h = bVar.h;
        this.i = bVar.i;
        this.j = bVar.j;
        this.k = bVar.k;
        this.l = bVar.l;
        this.m = bVar.m;
        this.n = bVar.n;
        this.o = bVar.o;
        this.p = bVar.p;
    }

    public final void p(String str) {
        this.q = str;
    }

    public final void q(String str) {
        this.r = str;
    }

    public final void r(String str) {
        this.s = str;
    }

    public final void s(String str) {
        this.t = str;
    }

    /* access modifiers changed from: protected */
    public final HashMap<String, String> a(HashMap<String, String> hashMap) {
        return super.a(a(a(a(a(hashMap, "Component", this.q), "Event", this.r), "Action", this.s), "AdditionalParams", this.t));
    }
}
