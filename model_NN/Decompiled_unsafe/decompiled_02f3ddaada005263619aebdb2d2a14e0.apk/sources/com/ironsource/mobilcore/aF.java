package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import com.ironsource.mobilcore.B;
import com.ironsource.mobilcore.av;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class aF {
    private static aF u;
    private ay A;
    private aw B;
    private BroadcastReceiver C = new OfferwallManager$2(this);
    /* access modifiers changed from: private */
    public Context a;
    private String b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    private boolean e = false;
    private ArrayList<b> f;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public Handler h;
    /* access modifiers changed from: private */
    public Runnable i;
    private String j = "";
    private String k;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public a m;
    private String n;
    /* access modifiers changed from: private */
    public JSONObject o;
    /* access modifiers changed from: private */
    public JSONObject p;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public boolean r = false;
    /* access modifiers changed from: private */
    public boolean s = true;
    private Runnable t = new Runnable() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ironsource.mobilcore.aF.a(com.ironsource.mobilcore.aF, boolean):boolean
         arg types: [com.ironsource.mobilcore.aF, int]
         candidates:
          com.ironsource.mobilcore.aF.a(com.ironsource.mobilcore.aF, com.ironsource.mobilcore.aF$a):com.ironsource.mobilcore.aF$a
          com.ironsource.mobilcore.aF.a(com.ironsource.mobilcore.aF, org.json.JSONObject):org.json.JSONObject
          com.ironsource.mobilcore.aF.a(java.io.File, java.io.File):boolean
          com.ironsource.mobilcore.aF.a(android.app.Activity, java.lang.String):void
          com.ironsource.mobilcore.aF.a(org.json.JSONObject, java.lang.String):void
          com.ironsource.mobilcore.aF.a(com.ironsource.mobilcore.aF, boolean):boolean */
        public final void run() {
            boolean unused = aF.this.s = true;
        }
    };
    private long v;
    private boolean w = true;
    private boolean x = false;
    private long y;
    private AnimationDrawable z;

    public interface b {
        void a();

        boolean b();
    }

    static /* synthetic */ void i(aF aFVar) {
        aFVar.v = System.currentTimeMillis();
        aFVar.p();
        if (aFVar.y != -1) {
            String str = aFVar.w ? "OfferWallTimer" : "OfferWallTimerReFetch";
            long currentTimeMillis = System.currentTimeMillis();
            Intent intent = new Intent(aFVar.a, MobileCoreReport.class);
            intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_timer", currentTimeMillis - aFVar.y);
            intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type", str);
            intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 99);
            intent.putExtra("1%dns#ge1%dk1%do1%dt", aFVar.b);
            aFVar.a.startService(intent);
        }
    }

    private aF() {
    }

    public static synchronized aF a() {
        aF aFVar;
        synchronized (aF.class) {
            if (u == null) {
                u = new aF();
            }
            aFVar = u;
        }
        return aFVar;
    }

    public final void b() {
        this.A = o();
    }

    public final ay a(CallbackResponse callbackResponse) {
        if (this.A == null || this.A.getParent() != null) {
            this.A = o();
        }
        if (this.B != null) {
            this.B.a().setCallBackListener(callbackResponse);
        }
        return this.A;
    }

    @SuppressLint({"NewApi", "SetJavaScriptEnabled"})
    private ay o() {
        ay ayVar = new ay(this.a);
        ((WindowManager) this.a.getSystemService("window")).getDefaultDisplay().getMetrics(new DisplayMetrics());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        ayVar.setLayoutParams(layoutParams);
        ayVar.setBackgroundColor(0);
        if (Build.VERSION.SDK_INT >= 15) {
            ayVar.setLayerType(1, null);
        }
        this.B = new aw(this.b, this.a, "", ayVar);
        ayVar.clearCache(true);
        ayVar.getSettings().setJavaScriptEnabled(true);
        ayVar.getSettings().setSupportMultipleWindows(false);
        ayVar.getSettings().setNeedInitialFocus(false);
        ayVar.getSettings().setDomStorageEnabled(true);
        ayVar.setWebChromeClient(this.B);
        ayVar.getSettings().setSupportZoom(false);
        ayVar.getSettings().setDomStorageEnabled(true);
        ayVar.setInitialScale(100);
        ayVar.setHorizontalScrollBarEnabled(false);
        return ayVar;
    }

    public final void a(J j2) {
        if (this.B != null && j2 != null) {
            this.B.a().a(j2);
        }
    }

    public final boolean c() {
        return this.q;
    }

    public final void a(boolean z2) {
        this.q = z2;
        C0031p.a("Offerwall | setIsShowingOfferwall | isShowing: " + z2, 55);
        if (z2) {
            new Handler().postDelayed(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.ironsource.mobilcore.aF.b(com.ironsource.mobilcore.aF, boolean):boolean
                 arg types: [com.ironsource.mobilcore.aF, int]
                 candidates:
                  com.ironsource.mobilcore.aF.b(com.ironsource.mobilcore.aF, org.json.JSONObject):org.json.JSONObject
                  com.ironsource.mobilcore.aF.b(java.io.File, java.io.File):boolean
                  com.ironsource.mobilcore.aF.b(com.ironsource.mobilcore.aF, boolean):boolean */
                public final void run() {
                    boolean unused = aF.this.q = false;
                }
            }, 2000);
        }
    }

    public static boolean d() {
        return av.b().getBoolean("com.ironsource.mobilecore.prefs_offerwall_fetch", false);
    }

    public static boolean b(boolean z2) {
        return av.b().edit().putBoolean("com.ironsource.mobilecore.prefs_offerwall_fetch", true).commit();
    }

    public final String e() {
        return this.c;
    }

    public final String f() {
        return this.d;
    }

    public final void a(Activity activity, String str) {
        if (!this.x) {
            this.x = true;
            this.a = activity.getApplicationContext();
            this.b = str;
            this.f = new ArrayList<>();
            File file = new File(this.a.getFilesDir().getPath(), "temp_offerwall_assets");
            if (!file.exists()) {
                file.mkdirs();
            }
            this.d = file.getAbsolutePath();
            File file2 = new File(this.a.getFilesDir().getPath(), "offerwall_assets");
            if (!file2.exists()) {
                file2.mkdirs();
            }
            this.c = file2.getAbsolutePath();
            if (c("preloader.zip")) {
                try {
                    s();
                } catch (Exception e2) {
                    av.a(MobileCore.b, getClass().getName(), e2);
                }
            }
            try {
                this.p = new JSONObject(q());
            } catch (JSONException e3) {
                this.p = null;
            }
            boolean a2 = C0017b.a(this.a);
            C0031p.a("offerwall , init | called , hasInternetConnection:" + a2, 55);
            if (a2) {
                j();
            } else {
                this.e = true;
                this.a.registerReceiver(this.C, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            }
            b();
        }
    }

    private synchronized void p() {
        Iterator<b> it = this.f.iterator();
        while (it.hasNext()) {
            b next = it.next();
            next.a();
            if (next.b()) {
                it.remove();
            }
        }
    }

    public final synchronized void a(b bVar) {
        if (!this.f.contains(bVar)) {
            this.f.add(bVar);
        }
    }

    public final synchronized void b(b bVar) {
        this.f.remove(bVar);
    }

    /* access modifiers changed from: private */
    public void b(Activity activity, CallbackResponse callbackResponse, boolean z2, boolean z3) {
        if (d()) {
            String str = this.c;
            C0031p.a("Offerwall | runFlowHtmlFromPath | mWasInit: " + this.x, 55);
            if (!this.x) {
                a(false);
                return;
            }
            ay ayVar = new ay(this.a);
            aw awVar = new aw(this.b, activity, "http://wwww.dummyurl.com", ayVar, callbackResponse);
            awVar.a().setConfiguration(false, z3);
            awVar.a().setActivity(activity);
            av.a(ayVar, awVar);
            String str2 = "file://" + str + "/offerwall_flow.html";
            C0031p.a("Offerwall | runFlowHtmlFromPath | localUrl: " + str2, 55);
            ayVar.loadUrl(str2);
            return;
        }
        av.a(new A(activity, activity, this.b), "http://offerwall.mobilecore.com/?client=1", new B.a(this, callbackResponse));
        Object[] objArr = new Object[1];
        objArr[0] = z2 ? "force" : "notForce";
        String format = MessageFormat.format("web_{0}", objArr);
        Intent intent = new Intent(this.a, MobileCoreReport.class);
        intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 99);
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow", "offerwall");
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type", format);
        intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 27);
        try {
            intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offers", new JSONArray(new aE().toString()).toString());
        } catch (Exception e2) {
            av.a(this.a, aF.class.getName(), e2);
        }
        intent.putExtra("1%dns#ge1%dk1%do1%dt", this.b);
        this.a.startService(intent);
    }

    public final void a(Activity activity, CallbackResponse callbackResponse, boolean z2, boolean z3) {
        if (z3 && (!this.s || this.r)) {
            return;
        }
        if (this.l) {
            this.m = new a(this, activity, callbackResponse, z2, z3);
        } else {
            b(activity, callbackResponse, z2, z3);
        }
    }

    public interface a {
        final /* synthetic */ Activity a;
        final /* synthetic */ CallbackResponse b;
        final /* synthetic */ boolean c;
        final /* synthetic */ boolean d;
        final /* synthetic */ aF e;

        default a(aF aFVar, Activity activity, CallbackResponse callbackResponse, boolean z, boolean z2) {
            this.e = aFVar;
            this.a = activity;
            this.b = callbackResponse;
            this.c = z;
            this.d = z2;
        }

        default void a() {
            this.e.b(this.a, this.b, this.c, this.d);
            a unused = this.e.m = null;
        }
    }

    public final JSONObject g() {
        return this.p;
    }

    private String q() {
        String str = this.c + "/" + av.b().getString("currentFeedJsonFileName", "offerwall_feed_json.json");
        String str2 = "";
        if (!TextUtils.isEmpty(str)) {
            try {
                FileInputStream fileInputStream = new FileInputStream(str);
                str2 = a(fileInputStream);
                fileInputStream.close();
                return str2;
            } catch (Exception e2) {
                return str2;
            }
        } else {
            C0031p.a("getOfferwallJson | called when offerwall json file name was not set", 55);
            return str2;
        }
    }

    /* access modifiers changed from: private */
    public boolean r() {
        FileWriter fileWriter;
        boolean z2 = false;
        try {
            fileWriter = new FileWriter(new File(this.d, this.n));
            try {
                fileWriter.write(this.o.toString());
                z2 = true;
                try {
                    fileWriter.flush();
                    fileWriter.close();
                } catch (IOException e2) {
                    C0031p.a("Error: failed to write offerwall json" + e2.getLocalizedMessage(), 2);
                }
            } catch (IOException e3) {
                e = e3;
                try {
                    av.a(MobileCore.b, getClass().getName(), e);
                    try {
                        fileWriter.flush();
                        fileWriter.close();
                    } catch (IOException e4) {
                        C0031p.a("Error: failed to write offerwall json" + e4.getLocalizedMessage(), 2);
                    }
                    return z2;
                } catch (Throwable th) {
                    th = th;
                    try {
                        fileWriter.flush();
                        fileWriter.close();
                    } catch (IOException e5) {
                        C0031p.a("Error: failed to write offerwall json" + e5.getLocalizedMessage(), 2);
                    }
                    throw th;
                }
            }
        } catch (IOException e6) {
            e = e6;
            fileWriter = null;
            av.a(MobileCore.b, getClass().getName(), e);
            fileWriter.flush();
            fileWriter.close();
            return z2;
        } catch (Throwable th2) {
            th = th2;
            fileWriter = null;
            fileWriter.flush();
            fileWriter.close();
            throw th;
        }
        return z2;
    }

    public final String h() {
        return "'" + this.p.toString() + "'";
    }

    private void s() throws Exception {
        List<File> a2 = a(new File(this.a.getFilesDir().getPath() + "/preloader/"));
        if (a2 == null) {
            throw new Exception("No Files");
        }
        this.z = new AnimationDrawable();
        for (File path : a2) {
            this.z.addFrame(new BitmapDrawable(this.a.getResources(), C0017b.a(this.a, BitmapFactory.decodeFile(path.getPath()))), 100);
        }
    }

    private boolean c(String str) {
        try {
            InputStream resourceAsStream = MobileCore.class.getResourceAsStream("fallback/" + str);
            File file = new File(this.a.getFilesDir().getPath() + "/", str);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END];
            while (true) {
                int read = resourceAsStream.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    resourceAsStream.close();
                    return aI.a(this.a.getFilesDir().getPath() + "/", file);
                }
            }
        } catch (Exception e2) {
            av.a(MobileCore.b, getClass().getName(), e2);
            return false;
        }
    }

    public final AnimationDrawable i() {
        return this.z;
    }

    private List<File> a(File file) {
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        if (listFiles == null || listFiles.length <= 0) {
            C0031p.a("OfferwallManager | getListFiles | Error: no files", 55);
            c("preloader.zip");
            return null;
        }
        for (File file2 : listFiles) {
            if (file2.isDirectory()) {
                arrayList.addAll(a(file2));
            } else {
                arrayList.add(file2);
            }
        }
        return arrayList;
    }

    @SuppressLint({"NewApi"})
    public final void j() {
        C0031p.a("offerwall , loadOfferwallFromWeb() | called", 55);
        if (this.e) {
            MobileCore.b.unregisterReceiver(this.C);
            this.e = false;
        }
        this.y = System.currentTimeMillis();
        Context context = this.a;
        ay ayVar = new ay(this.a);
        aw awVar = new aw(this.b, context, "http://wwww.dummyurl.com", ayVar, null);
        awVar.a().setConfiguration(true, false);
        av.a(ayVar, awVar);
        aH.a(av.a(this.a, "offerWall"), new C0021f(ayVar, this.d, "offerwall_flow.html", null));
    }

    class c implements av.c {
        private ArrayList<String> b = new ArrayList<>();

        public c() {
        }

        /* access modifiers changed from: private */
        public boolean a() {
            C0031p.a("ResourceLoaderListener | saving feed", 55);
            if (aF.this.o == null) {
                av.a(MobileCore.b, getClass().getName(), "Got to updateAndSaveFeed after swapping (mPendingOfferwallJson is null)");
                return false;
            }
            JSONArray optJSONArray = aF.this.o.optJSONArray("ads");
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                Iterator<String> keys = optJSONObject.keys();
                while (keys.hasNext()) {
                    try {
                        String next = keys.next();
                        String string = optJSONObject.getString(next);
                        int i2 = 0;
                        while (true) {
                            if (i2 >= this.b.size()) {
                                break;
                            } else if (string.contains(this.b.get(i2))) {
                                optJSONObject.put(next, "");
                                break;
                            } else {
                                i2++;
                            }
                        }
                    } catch (JSONException e) {
                    }
                }
            }
            return aF.this.r();
        }

        public final void a(String str, boolean z) {
            C0031p.a("ResourceLoaderListener | resourceComplete " + str + " " + (z ? "OK" : "NEY"), 55);
            if (!z) {
                this.b.add(str);
            }
        }

        public final void a(boolean z) {
            C0031p.a("ResourceLoaderListener | allComplete " + (z ? "OK" : "NEY"), 55);
            aF.this.h.removeCallbacks(aF.this.i);
            if (!z || aF.this.r) {
                aF.this.g = false;
            } else {
                new AsyncTask<Void, Void, Boolean>() {
                    /* access modifiers changed from: protected */
                    public final /* synthetic */ Object doInBackground(Object[] objArr) {
                        boolean z;
                        if (c.this.a()) {
                            boolean unused = aF.this.l = true;
                            JSONObject unused2 = aF.this.p = aF.this.o;
                            JSONObject unused3 = aF.this.o = null;
                            z = aF.this.a(new File(aF.this.d), new File(aF.this.c));
                            boolean unused4 = aF.this.l = false;
                        } else {
                            z = false;
                        }
                        return Boolean.valueOf(z);
                    }

                    /* access modifiers changed from: protected */
                    public final /* synthetic */ void onPostExecute(Object obj) {
                        Boolean bool = (Boolean) obj;
                        super.onPostExecute(bool);
                        aF.this.g = false;
                        if (bool.booleanValue()) {
                            aF aFVar = aF.this;
                            if (!aF.d()) {
                                aF aFVar2 = aF.this;
                                aF.b(true);
                            }
                            aF.i(aF.this);
                            if (aF.this.m != null) {
                                aF.this.m.a();
                            }
                        }
                    }
                }.execute(new Void[0]);
            }
        }
    }

    public final void a(String str) {
        this.j = str;
        this.g = true;
        aH.a(str, new C0022g(new c()));
        this.h = new Handler();
        this.i = new Runnable() {
            public final void run() {
                aF.this.g = false;
            }
        };
        this.h.postDelayed(this.i, 30000);
    }

    public final void k() {
        if (!this.g) {
            File file = new File(this.d);
            av.a(file);
            file.mkdir();
            a(this.j);
        }
    }

    public final void l() {
        this.s = false;
        MobileCore.a.removeCallbacks(this.t);
        MobileCore.a.postDelayed(this.t, 5000);
    }

    /* access modifiers changed from: private */
    public boolean a(File file, File file2) {
        boolean b2;
        boolean z2 = true;
        for (File file3 : file.listFiles()) {
            File file4 = new File(file2.getAbsolutePath(), file3.getName());
            if (file3.isDirectory()) {
                if (!file4.exists()) {
                    file4.mkdirs();
                }
                b2 = a(file3, file4);
            } else {
                if (file4.exists()) {
                    file4.delete();
                }
                b2 = b(file3, file4);
            }
            z2 &= b2;
        }
        return z2;
    }

    private boolean b(File file, File file2) {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            byte[] bArr = new byte[AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read > 0) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileInputStream.close();
                    fileOutputStream.close();
                    C0031p.a("File copied.", 55);
                    return true;
                }
            }
        } catch (Exception e2) {
            av.a(MobileCore.b, getClass().getName(), e2);
            return false;
        }
    }

    private static String a(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return sb.toString();
            }
            sb.append(readLine);
        }
    }

    /* access modifiers changed from: protected */
    public final long m() {
        return this.v;
    }

    /* access modifiers changed from: protected */
    public final void c(boolean z2) {
        this.r = z2;
    }

    /* access modifiers changed from: protected */
    public final void b(String str) {
        this.k = str;
    }

    /* access modifiers changed from: protected */
    public final String n() {
        return this.k;
    }

    public final void a(JSONObject jSONObject, String str) {
        this.o = jSONObject;
        this.n = str;
    }
}
