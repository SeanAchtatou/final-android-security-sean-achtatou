package com.ironsource.mobilcore;

import android.app.Activity;

public interface MCISliderAPI {
    public static final String ACTION_WIDGET_CALLBACK = "com.ironsource.mcslidermenu.MCWidgetCallbackAction";
    public static final String EXTRA_CALLBACK_ID = "callbackId";

    void closeSliderMenu(boolean z);

    String getWidgetTextProperty(String str, MCEWidgetTextProperties mCEWidgetTextProperties);

    void hideWidget(String str);

    boolean isSliderMenuOpen();

    void openSliderMenu(boolean z);

    void setContentViewWithSlider(Activity activity, int i);

    void setContentViewWithSlider(Activity activity, int i, int i2);

    void setEmphasizedWidget(String str);

    void setWidgetIconResource(String str, int i);

    void setWidgetTextProperty(String str, MCEWidgetTextProperties mCEWidgetTextProperties, String str2);

    void showWidget(String str);

    void toggleSliderMenu(boolean z);
}
