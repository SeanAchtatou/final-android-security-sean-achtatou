package com.ironsource.mobilcore;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ironsource.mobilcore.C0034s;

final class D extends C0034s {
    public D(Context context, ao aoVar, C0034s.a aVar) {
        super(context, aoVar, aVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g = new RelativeLayout(this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
      com.ironsource.mobilcore.b.a(android.content.Context, float):int */
    /* access modifiers changed from: protected */
    public final void b() {
        this.p = new ImageView(this.c);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 1;
        layoutParams.bottomMargin = C0017b.a(this.c, 15.0f);
        this.p.setLayoutParams(layoutParams);
        this.m = new TextView(this.c);
        this.m.setBackgroundColor(0);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 1;
        this.m.setLayoutParams(layoutParams2);
        this.m.setGravity(1);
        this.m.setTypeface(null, 1);
        this.m.setTextColor(this.d.b().a(true));
        this.m.setSingleLine();
        this.m.setEllipsize(TextUtils.TruncateAt.END);
        this.m.setTextSize(2, 16.0f);
        LinearLayout linearLayout = new LinearLayout(this.c);
        linearLayout.setId(j());
        linearLayout.setOrientation(1);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(13);
        linearLayout.setLayoutParams(layoutParams3);
        linearLayout.setPadding(this.d.h(), this.d.h(), this.d.h(), this.d.h());
        View view = new View(this.c);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(C0017b.a(this.c, (float) this.d.g()), -2);
        layoutParams4.addRule(11);
        layoutParams4.addRule(6, linearLayout.getId());
        layoutParams4.addRule(8, linearLayout.getId());
        view.setLayoutParams(layoutParams4);
        view.setBackgroundColor(this.d.e());
        ViewGroup viewGroup = (ViewGroup) this.g;
        viewGroup.setClickable(true);
        viewGroup.setFocusable(true);
        viewGroup.addView(linearLayout);
        viewGroup.addView(view);
        linearLayout.addView(this.p);
        linearLayout.addView(this.m);
        a(this.d.o(), this.d.b(), this.m);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        Bitmap decodeFile;
        Bitmap createScaledBitmap;
        this.m.setText(this.i);
        if (!TextUtils.isEmpty(this.a) && (decodeFile = BitmapFactory.decodeFile(this.a, null)) != null && (createScaledBitmap = Bitmap.createScaledBitmap(decodeFile, 72, 72, true)) != null) {
            this.p.setImageBitmap(C0017b.a(this.c, createScaledBitmap));
        }
    }
}
