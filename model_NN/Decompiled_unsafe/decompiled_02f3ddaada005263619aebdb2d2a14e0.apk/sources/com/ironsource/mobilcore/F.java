package com.ironsource.mobilcore;

import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

abstract class F extends C0037v {
    public F(Context context, ao aoVar) {
        super(context, aoVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject, true);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return true;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g = new RelativeLayout(this.c);
        this.g.setPadding(this.d.h(), this.d.i(), this.d.h(), this.d.i());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
      com.ironsource.mobilcore.b.a(android.content.Context, float):int */
    /* access modifiers changed from: protected */
    public final void b() {
        ViewGroup viewGroup = (ViewGroup) this.g;
        this.p = new ImageView(this.c);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(15);
        layoutParams.rightMargin = C0017b.a(this.c, 9.0f);
        this.p.setLayoutParams(layoutParams);
        this.p.setId(j());
        viewGroup.addView(this.p);
        this.m = new TextView(this.c);
        this.m.setBackgroundColor(0);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(1, this.p.getId());
        layoutParams2.addRule(15);
        this.m.setLayoutParams(layoutParams2);
        this.m.setGravity(3);
        this.m.setTypeface(null, 1);
        this.m.setTextSize(2, 16.0f);
        viewGroup.addView(this.m);
        a(this.d.q(), this.d.b(), this.m);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.m.setText(this.i.toUpperCase());
        if (!TextUtils.isEmpty(this.l)) {
            this.p.setImageBitmap(C0017b.a(this.c, this.l));
        }
    }
}
