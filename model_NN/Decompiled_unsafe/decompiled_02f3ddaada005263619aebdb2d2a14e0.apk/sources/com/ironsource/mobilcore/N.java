package com.ironsource.mobilcore;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import org.json.JSONException;
import org.json.JSONObject;

final class N extends C0037v {
    private int a;

    public N(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "groupHeader";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) throws JSONException {
        this.a = this.d.f();
        super.a(jSONObject, true);
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g = new RelativeLayout(this.c);
        this.g.setEnabled(false);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        View view = new View(this.c);
        view.setLayoutParams(new RelativeLayout.LayoutParams(-1, C0017b.a(this.c, (float) this.a)));
        C0017b.a(view, this.d.r());
        ((ViewGroup) this.g).addView(view);
    }

    /* access modifiers changed from: protected */
    public final void c() {
    }
}
