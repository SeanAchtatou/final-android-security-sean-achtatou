package com.ironsource.mobilcore;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import org.json.JSONException;
import org.json.JSONObject;

final class aq extends C0035t {
    private Intent a;

    public aq(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "ironsourceSocialWidgetShare";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        this.a = av.a(jSONObject);
        super.a(jSONObject);
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        if (this.a != null) {
            this.e.startActivity(this.a);
        }
        super.a(view);
    }
}
