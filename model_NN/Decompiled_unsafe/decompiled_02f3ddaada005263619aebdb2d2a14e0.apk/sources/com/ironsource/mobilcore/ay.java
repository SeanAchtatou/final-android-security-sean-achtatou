package com.ironsource.mobilcore;

import android.content.Context;
import android.view.KeyEvent;
import android.webkit.WebView;

final class ay extends WebView {
    private boolean a;

    public ay(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        this.a = true;
        C0031p.a("MCWebView | onDetachedFromWindow() ", 55);
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        C0031p.a("MCWebView | onAttachedWindow", 55);
    }

    public final void loadUrl(String str) {
        if (!this.a) {
            super.loadUrl(str);
        }
    }

    public final void reload() {
        if (!this.a) {
            super.reload();
        }
    }

    public final boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        if (i == 4) {
            C0031p.a("got back key", 55);
        }
        return super.onKeyPreIme(i, keyEvent);
    }
}
