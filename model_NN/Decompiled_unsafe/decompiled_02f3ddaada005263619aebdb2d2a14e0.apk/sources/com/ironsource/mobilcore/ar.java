package com.ironsource.mobilcore;

import android.content.Context;
import android.view.View;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import org.json.JSONException;
import org.json.JSONObject;

final class ar extends C0035t {
    public ar(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "ironsourceSocialWidgetGooglePlus";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.av.a(android.app.Activity, java.lang.String, boolean):void
     arg types: [android.app.Activity, java.lang.String, int]
     candidates:
      com.ironsource.mobilcore.av.a(android.content.Context, java.lang.String, java.lang.Exception):void
      com.ironsource.mobilcore.av.a(android.content.Context, java.lang.String, java.lang.String):void
      com.ironsource.mobilcore.av.a(com.ironsource.mobilcore.B, java.lang.String, com.ironsource.mobilcore.B$a):void
      com.ironsource.mobilcore.av.a(android.app.Activity, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(View view) {
        try {
            av.a(this.e, MessageFormat.format("https://plus.google.com/share?url={0}", av.d(av.i(this.c))), true);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        super.a(view);
    }
}
