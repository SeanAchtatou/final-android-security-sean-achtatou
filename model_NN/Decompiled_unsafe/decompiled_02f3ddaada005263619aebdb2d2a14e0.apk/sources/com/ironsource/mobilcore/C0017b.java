package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.ironsource.mobilcore.b  reason: case insensitive filesystem */
class C0017b {
    protected String a;
    protected String b;
    protected String c;
    protected String d;
    protected String e;
    protected String f;
    protected String g;
    protected String h;
    protected String i;
    protected String j;
    protected JSONArray k = null;
    protected String l;
    protected String m;
    protected String n;
    protected String o;
    protected String p;
    private String q;
    private JSONObject r;
    private String s;
    private JSONArray t;

    protected static String m(String str) {
        String str2 = "";
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (str.charAt(i2) < 'k') {
                str2 = str2.concat(String.valueOf(str.charAt(i2))).concat("g#s");
            } else if (str.charAt(i2) >= 'k') {
                str2 = str2.concat(String.valueOf(str.charAt(i2))).concat("d%1");
            }
        }
        return p(str2);
    }

    protected static boolean a(Context context, int i2) {
        for (NetworkInfo networkInfo : ((ConnectivityManager) context.getSystemService("connectivity")).getAllNetworkInfo()) {
            C0031p.a("network detected: " + networkInfo.getTypeName(), 55);
            if (i2 == 1) {
                if (networkInfo.getTypeName().equalsIgnoreCase("MOBILE") && networkInfo.isAvailable()) {
                    return true;
                }
            } else if (i2 != 0) {
                C0031p.a("NetworkUtils/checkConnectivity | error: connection requested is neither wifi nor cellular.", 55);
            } else if (networkInfo.getTypeName().equalsIgnoreCase("WIFI") && networkInfo.isAvailable()) {
                return true;
            }
        }
        return false;
    }

    protected static String n(String str) {
        return p(str).replaceAll("g#s", "").replaceAll("d%1", "");
    }

    private static String p(String str) {
        char[] charArray = str.toCharArray();
        int i2 = 0;
        for (int length = charArray.length - 1; i2 < length; length--) {
            char c2 = charArray[i2];
            charArray[i2] = charArray[length];
            charArray[length] = c2;
            i2++;
        }
        return new String(charArray);
    }

    protected static boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public HashMap<String, String> a(HashMap<String, String> hashMap) {
        HashMap<String, String> a2 = a(a(a(a(a(a(a(a(a(a(a(a(a(a(a(a(a(a(a(a(hashMap, "Carrier", this.c), "BV", this.s), "Flow", this.d), "Target", this.i), "Step", this.j), "UID", this.q), "RS", this.e), "Err", this.f), "IRVER", this.g), "TK", this.h), "FirstRun", this.a), "RV", this.b), "FlowName", this.l), "Platform", this.m), "Orientation", null), "curConnection", null), "Offline", this.o), "CarrierVer", this.n), "Timer", this.p), "DomainAccounts", null);
        if (this.r != null) {
            a2 = a(a2, this.r);
        }
        if (this.t != null) {
            return a(a2, this.t);
        }
        return a2;
    }

    public static Bitmap a(Context context, String str) {
        Bitmap o2 = o(str);
        if (o2 == null) {
            return null;
        }
        return a(context, o2);
    }

    @SuppressLint({"InlinedApi"})
    public static Bitmap o(String str) {
        byte[] decode;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            if (Build.VERSION.SDK_INT <= 8) {
                decode = C0007a.a(str, 0);
            } else {
                decode = Base64.decode(str, 0);
            }
            return BitmapFactory.decodeByteArray(decode, 0, decode.length);
        } catch (Exception e2) {
            return null;
        }
    }

    private HashMap<String, String> a(HashMap<String, String> hashMap, JSONArray jSONArray) {
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            try {
                hashMap = a(hashMap, jSONArray.getJSONObject(i2));
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return hashMap;
    }

    private static HashMap<String, String> a(HashMap<String, String> hashMap, JSONObject jSONObject) {
        JSONArray names = jSONObject.names();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= names.length()) {
                return hashMap;
            }
            try {
                String str = (String) names.get(i3);
                Object obj = jSONObject.get(str);
                hashMap.put(str, String.valueOf(obj));
                C0031p.a("BaseReport | Adding " + str + ":" + obj, 55);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            i2 = i3 + 1;
        }
    }

    public static int a(Context context, float f2) {
        return (int) (context.getResources().getDisplayMetrics().density * f2);
    }

    public final void a(JSONArray jSONArray) {
        this.t = jSONArray;
    }

    public final void a(JSONObject jSONObject) {
        this.r = jSONObject;
    }

    public static void a(View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    protected static HashMap<String, String> a(HashMap<String, String> hashMap, String str, String str2) {
        if (str2 != null) {
            hashMap.put(str, str2);
        }
        return hashMap;
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject(a(new HashMap()));
        if (this.k != null) {
            try {
                jSONObject.put("Offers", this.k);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return jSONObject;
    }

    @SuppressLint({"InlinedApi"})
    public static Bitmap a(Context context, Bitmap bitmap) {
        float f2 = 320.0f / ((float) context.getResources().getDisplayMetrics().densityDpi);
        return Bitmap.createScaledBitmap(bitmap, (int) (((float) bitmap.getWidth()) / f2), (int) (((float) bitmap.getHeight()) / f2), true);
    }

    public final void a(int i2) {
        this.s = String.valueOf(i2);
    }

    public final void b(JSONObject jSONObject) {
        if (this.k == null) {
            this.k = new JSONArray();
        }
        this.k.put(jSONObject);
    }

    public final void b(JSONArray jSONArray) {
        this.k = jSONArray;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void b(String str) {
        this.n = str;
    }

    public final void c(String str) {
        this.q = str;
    }

    public final void d(String str) {
        this.d = str;
    }

    public final void e(String str) {
        this.e = str;
    }

    public final void f(String str) {
        this.f = str;
    }

    public final void g(String str) {
        this.g = str;
    }

    public final void h(String str) {
        this.h = str;
    }

    public final void a(boolean z) {
        this.a = String.valueOf(z);
    }

    public final void i(String str) {
        this.b = str;
    }

    public final void j(String str) {
        this.l = str;
    }

    public final void k(String str) {
        this.m = str;
    }

    public final void b(boolean z) {
        this.o = "true";
    }

    public final void l(String str) {
        this.p = str;
    }
}
