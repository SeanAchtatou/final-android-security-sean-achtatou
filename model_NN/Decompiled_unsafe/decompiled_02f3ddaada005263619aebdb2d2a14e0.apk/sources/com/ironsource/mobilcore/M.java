package com.ironsource.mobilcore;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.text.MessageFormat;
import org.json.JSONException;
import org.json.JSONObject;

final class M extends C0037v {
    private String a;
    private EditText b;
    private ImageView q;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.av.a(android.app.Activity, java.lang.String, boolean):void
     arg types: [android.app.Activity, java.lang.String, int]
     candidates:
      com.ironsource.mobilcore.av.a(android.content.Context, java.lang.String, java.lang.Exception):void
      com.ironsource.mobilcore.av.a(android.content.Context, java.lang.String, java.lang.String):void
      com.ironsource.mobilcore.av.a(com.ironsource.mobilcore.B, java.lang.String, com.ironsource.mobilcore.B$a):void
      com.ironsource.mobilcore.av.a(android.app.Activity, java.lang.String, boolean):void */
    static /* synthetic */ void a(M m) {
        String obj = m.b.getText().toString();
        if (!TextUtils.isEmpty(m.a) && !TextUtils.isEmpty(obj)) {
            String format = MessageFormat.format(m.a, obj);
            m.b.setText("");
            m.k();
            av.a(m.e, format, true);
            C0031p.a(m.c, "slider", "widget", "search_performed", new C0038w[0]);
        }
    }

    public M(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "ironsourceSearch";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) throws JSONException {
        this.a = jSONObject.optString("baseSearchUrl", "http://search.spearmint-browser.com/results.php?s={0}&a=mcsldr");
        super.a(jSONObject, true);
        this.b.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i != 3) {
                    return false;
                }
                M.a(M.this);
                return false;
            }
        });
        this.q.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                M.a(M.this);
            }
        });
    }

    /* access modifiers changed from: protected */
    public final boolean d() {
        return false;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g = new RelativeLayout(this.c);
        this.g.setPadding(this.d.h(), this.d.h(), this.d.h(), this.d.h());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
      com.ironsource.mobilcore.b.a(android.content.Context, float):int */
    /* access modifiers changed from: protected */
    public final void b() {
        this.q = new ImageView(this.c);
        this.q.setId(j());
        int a2 = C0017b.a(this.c, 5.0f);
        this.q.setPadding(a2 << 1, a2, a2, a2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(15);
        layoutParams.addRule(11);
        this.q.setLayoutParams(layoutParams);
        this.q.setImageDrawable(this.d.l());
        this.b = new EditText(this.c);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(0, this.q.getId());
        this.b.setLayoutParams(layoutParams2);
        this.b.setSingleLine();
        this.b.setImeOptions(3);
        this.b.setHintTextColor(this.d.c());
        this.b.setTextColor(this.d.d());
        C0017b.a(this.b, (Drawable) null);
        this.b.setPadding(0, 0, 0, 0);
        RelativeLayout relativeLayout = new RelativeLayout(this.c);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        relativeLayout.addView(this.q);
        relativeLayout.addView(this.b);
        C0017b.a(relativeLayout, this.d.t());
        ((ViewGroup) this.g).addView(relativeLayout);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (!TextUtils.isEmpty(this.i)) {
            this.b.setHint(this.i);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
    }
}
