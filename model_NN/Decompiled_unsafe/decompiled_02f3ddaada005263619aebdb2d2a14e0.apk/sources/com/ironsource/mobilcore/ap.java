package com.ironsource.mobilcore;

import android.content.Context;
import android.view.View;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import org.json.JSONException;
import org.json.JSONObject;

final class ap extends C0035t {
    private String a;
    private String b;
    private String q;
    private String r;
    private String s;

    public ap(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "ironsourceSocialWidgetFacebookWallPost";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        this.a = jSONObject.optString("facebookAppId", "");
        this.b = jSONObject.optString("facebookRedirectURI", "");
        this.q = jSONObject.optString("appPictureUrl", "");
        this.r = jSONObject.optString("appName", "");
        this.s = jSONObject.optString("postText", "");
        super.a(jSONObject);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.av.a(android.app.Activity, java.lang.String, boolean):void
     arg types: [android.app.Activity, java.lang.String, int]
     candidates:
      com.ironsource.mobilcore.av.a(android.content.Context, java.lang.String, java.lang.Exception):void
      com.ironsource.mobilcore.av.a(android.content.Context, java.lang.String, java.lang.String):void
      com.ironsource.mobilcore.av.a(com.ironsource.mobilcore.B, java.lang.String, com.ironsource.mobilcore.B$a):void
      com.ironsource.mobilcore.av.a(android.app.Activity, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(View view) {
        try {
            av.a(this.e, MessageFormat.format("https://m.facebook.com/dialog/feed?app_id={0}&link={1}&picture={2}&name={3}&description={4}&redirect_uri={5}", av.d(this.a), av.d(av.i(this.c)), av.d(this.q), av.d(this.r), av.d(this.s), av.d(this.b)), true);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        super.a(view);
    }
}
