package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import com.applovin.sdk.bootstrap.SdkBoostrapTasks;
import com.ironsource.mobilcore.av;
import java.io.File;
import java.net.URI;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class aH {
    aH() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public static String a(String str, av.b bVar) {
        boolean z;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String str2 = av.a(str) + av.f(str);
        C0031p.a("ResourceManager | preloadMediaResource : going to Download file name: " + str2 + " from: " + str, 55);
        String f = aF.a().f();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("id", str2);
            jSONObject.put("type", "img");
            jSONObject.put("location", str);
            jSONObject.put("filename", str2);
            jSONObject.put("mandatory", false);
            jSONObject.put("update-feed-json", true);
            bVar.a(jSONObject);
            z = a(str, new C0023h(f, str2, bVar));
        } catch (JSONException e) {
            C0031p.a("ResourceManager | ` : failed to preload resource: " + str2 + " from: " + str, 55);
            av.a(MobileCore.b, aH.class.getName(), e);
            z = false;
        }
        if (z) {
            return "file://" + aF.a().e() + "/" + str2;
        }
        return null;
    }

    @SuppressLint({"NewApi"})
    public static boolean a(final String str, final av.a aVar) {
        C0031p.a(str, 55);
        AnonymousClass1 r0 = new AsyncTask<Void, Void, Boolean>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object doInBackground(Object[] objArr) {
                return a();
            }

            private Boolean a() {
                if (TextUtils.isEmpty(str)) {
                    C0031p.a("ResourceManager | Trying to download empty URL!", 2);
                    return false;
                }
                C0031p.a("ResourceManager | Downloading: " + str, 55);
                try {
                    HttpResponse execute = av.a().execute(new HttpGet(new URI(str.replace(" ", "%20"))));
                    if (execute.getStatusLine().getStatusCode() != 200) {
                        aVar.a(execute.getStatusLine().getStatusCode());
                        return false;
                    }
                    HttpEntity entity = execute.getEntity();
                    aVar.a(entity);
                    entity.consumeContent();
                    return true;
                } catch (Exception e) {
                    av.a(MobileCore.b, aH.class.getName(), e);
                }
            }
        };
        if (Build.VERSION.SDK_INT >= 11) {
            r0.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            return true;
        }
        r0.execute(new Void[0]);
        return true;
    }

    public static void a(JSONObject jSONObject, av.c cVar) {
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("data");
            av.b bVar = new av.b(cVar);
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                String optString = jSONObject2.optString("id");
                String optString2 = jSONObject2.optString(SdkBoostrapTasks.SDK_VERSION_FILE, null);
                String optString3 = jSONObject2.optString("filename");
                if (!av.b().contains(optString) || !new File(aF.a().e(), optString3).exists() || !av.b().getString(optString, "").equals(optString2) || TextUtils.isEmpty(optString2)) {
                    bVar.a(jSONObject2);
                }
            }
            bVar.a();
        } catch (JSONException e) {
            av.a(MobileCore.b, aH.class.getName(), e);
        }
    }
}
