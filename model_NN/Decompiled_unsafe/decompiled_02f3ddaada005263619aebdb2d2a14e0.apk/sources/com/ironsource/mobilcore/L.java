package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.ScrollView;

public final class L extends ScrollView {
    /* access modifiers changed from: private */
    public b a = null;
    private Handler b = new a();

    public interface b {
        void d();
    }

    public L(Context context) {
        super(context);
    }

    public final void a(b bVar) {
        this.a = bVar;
    }

    /* access modifiers changed from: protected */
    public final void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        Message obtain = Message.obtain();
        obtain.arg1 = i4;
        obtain.arg2 = i2;
        obtain.what = 1;
        this.b.sendMessageDelayed(obtain, 500);
    }

    @SuppressLint({"HandlerLeak"})
    class a extends Handler {
        a() {
        }

        public final void handleMessage(Message message) {
            super.handleMessage(message);
            if (!hasMessages(1) && L.this.a != null) {
                L.this.a.d();
            }
        }
    }
}
