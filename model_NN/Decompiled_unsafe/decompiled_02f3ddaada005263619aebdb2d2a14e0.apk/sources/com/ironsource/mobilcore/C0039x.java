package com.ironsource.mobilcore;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.ironsource.mobilcore.x  reason: case insensitive filesystem */
final class C0039x extends F {
    private String a;
    private String b;

    public C0039x(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "ironsourceFeedback";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      com.ironsource.mobilcore.v.a(com.ironsource.mobilcore.MCEWidgetTextProperties, java.lang.String):void
      com.ironsource.mobilcore.v.a(org.json.JSONObject, boolean):void */
    public final void a(JSONObject jSONObject) throws JSONException {
        this.a = jSONObject.optString("mailRecipient", "");
        this.b = jSONObject.optString("mailSubject", "");
        super.a(jSONObject, true);
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        super.a(view);
        if (!TextUtils.isEmpty(this.a) && !TextUtils.isEmpty(this.b)) {
            Intent intent = new Intent("android.intent.action.SENDTO");
            intent.setData(Uri.parse("mailto:" + Uri.encode(this.a) + "?subject=" + Uri.encode(this.b)));
            this.e.startActivity(Intent.createChooser(intent, ""));
        }
    }
}
