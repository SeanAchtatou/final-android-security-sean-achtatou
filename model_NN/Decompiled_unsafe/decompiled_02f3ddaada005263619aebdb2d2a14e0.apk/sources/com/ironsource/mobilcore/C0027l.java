package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import com.tapjoy.TapjoyConstants;
import java.io.File;

/* renamed from: com.ironsource.mobilcore.l  reason: case insensitive filesystem */
class C0027l {
    private NotificationManager a = null;
    private final int b = Build.VERSION.SDK_INT;
    private Notification.Builder c;
    private Notification d;
    private Context e;
    private int f;
    private DownloadManager g;
    private String h;
    private String i;
    private C0025j j;
    private String k;

    @SuppressLint({"NewApi"})
    public C0027l(Context context, Intent intent) {
        C0031p.a("dmService called", 55);
        if (this.e == null) {
            this.e = context;
            this.a = (NotificationManager) context.getSystemService("notification");
        }
        if (this.j == null) {
            this.j = new C0025j();
        }
        this.h = intent.getStringExtra("1%dns#ge1%dk1%do1%dt");
        if (intent.getStringExtra("extra_service_type").equals("download")) {
            C0031p.a("got download intent", 55);
            String stringExtra = intent.getStringExtra("com.ironsource.mobilcore.extra_download_url");
            String stringExtra2 = intent.getStringExtra("com.ironsource.mobilcore.extra_download_filename");
            String stringExtra3 = intent.getStringExtra("com.ironsource.mobilcore.extra_download_appname");
            String stringExtra4 = intent.getStringExtra("com.ironsource.mobilcore.extra_download_app_img_name");
            String stringExtra5 = intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer");
            this.i = intent.getStringExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type");
            this.k = intent.getStringExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow");
            boolean booleanExtra = intent.getBooleanExtra("com.ironsource.mobilcore.extra_is_apk_download", false);
            if (!this.j.a(stringExtra3)) {
                this.j.add(new C0019d(stringExtra3, 0));
                C0031p.a("DownloadManagerService | adding " + stringExtra3, 55);
                if (this.b >= 14) {
                    String str = this.i;
                    String str2 = this.k;
                    a(stringExtra, stringExtra2, stringExtra4, stringExtra3, stringExtra5, booleanExtra);
                } else if (this.b < 9) {
                    C0031p.a("downloading for api < GingerBread", 55);
                    String str3 = this.i;
                    String str4 = this.k;
                    a(stringExtra, stringExtra2, stringExtra4, stringExtra3, stringExtra5, booleanExtra);
                } else if (av.b(this.e, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                    C0031p.a("WRITE_EXTERNAL_STORAGE permission available. Using DownloadManager", 55);
                    C0031p.a("got api: " + this.b, 55);
                    long longExtra = intent.getLongExtra("com.ironsource.mobilcore.extra_download_queue_id", -1);
                    C0031p.a("got queue_id: " + longExtra, 55);
                    String str5 = Environment.getExternalStorageDirectory() + "/mc_data";
                    if (longExtra == -1) {
                        C0031p.a("no_queue_id", 55);
                        this.g = (DownloadManager) context.getSystemService("download");
                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(stringExtra));
                        File file = new File(str5);
                        if (!file.exists()) {
                            file.mkdirs();
                        }
                        String str6 = file + File.separator + stringExtra2;
                        C0031p.a("deleting file " + str6, 55);
                        C0031p.a("file deletion was " + new File(str6).delete(), 55);
                        request.setDestinationInExternalPublicDir("/mc_data", stringExtra2);
                        request.setAllowedNetworkTypes(3);
                        request.setAllowedOverRoaming(false);
                        request.setVisibleInDownloadsUi(true);
                        try {
                            request.setShowRunningNotification(true);
                        } catch (Exception e2) {
                            C0031p.a("downloadManager error: " + e2.getMessage(), 55);
                        }
                        request.setTitle("Downloading " + stringExtra3);
                        request.setDescription("Download In Progress...");
                        long enqueue = this.g.enqueue(request);
                        C0031p.a("queue_id from Download Manager is: " + enqueue, 55);
                        String str7 = this.i;
                        a(stringExtra, stringExtra3, stringExtra4, stringExtra5, stringExtra2, enqueue, this.k);
                        return;
                    }
                    C0031p.a("queue_id != Consts.NO_QUEUE_ID trying installation", 55);
                    if (this.g == null) {
                        this.g = (DownloadManager) this.e.getSystemService("download");
                    }
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(longExtra);
                    Cursor query2 = this.g.query(query);
                    int i2 = -3;
                    if (query2 != null && query2.moveToFirst()) {
                        i2 = query2.getColumnIndex("status");
                    }
                    long j2 = (long) query2.getInt(i2);
                    C0031p.a("Download Status = " + j2, 55);
                    if (j2 == 16) {
                        C0031p.a("DownloadManagerService | status fail: removing file if exists and reporting fail.", 55);
                        this.j.b(stringExtra3);
                        C0026k.a().b(stringExtra3);
                        String c2 = av.c(stringExtra);
                        C0031p.a("Deleting file: " + c2, 55);
                        this.e.deleteFile(c2);
                        a(stringExtra5, "S-");
                    } else if (j2 == 8) {
                        C0031p.a("DownloadManagerService | removing " + stringExtra3, 55);
                        this.j.b(stringExtra3);
                        C0026k.a().b(stringExtra3);
                        String c3 = av.c(stringExtra);
                        av.a(this.e, this.h, this.i, av.a(this.e, this.h, str5, c3), c3, 4, stringExtra5, 1, this.k, -1);
                        C0031p.a("installQueuedFile start", 55);
                        if (this.g == null) {
                            this.g = (DownloadManager) this.e.getSystemService("download");
                        }
                        DownloadManager.Query query3 = new DownloadManager.Query();
                        query3.setFilterById(longExtra);
                        Cursor query4 = this.g.query(query3);
                        if (query4 != null && query4.moveToFirst()) {
                            int i3 = query4.getInt(query4.getColumnIndex("_id"));
                            if (((long) i3) == longExtra) {
                                C0031p.a("download succesfull " + i3, 55);
                            }
                            try {
                                int columnIndex = query4.getColumnIndex("local_uri");
                                C0031p.a("urlcollon " + columnIndex, 55);
                                String string = query4.getString(columnIndex);
                                String a2 = a(string);
                                C0031p.a("installQueuedFile | attempting removeFilePrefix(" + string + ")", 55);
                                Intent intent2 = new Intent("android.intent.action.VIEW");
                                C0031p.a("fileUri: " + a2, 55);
                                intent2.setDataAndType(Uri.fromFile(new File(a2)), "application/vnd.android.package-archive");
                                intent2.addFlags(268435456);
                                this.e.startActivity(intent2);
                            } catch (Exception e3) {
                                C0031p.a("download error: " + e3.getMessage(), 55);
                                C0031p.a("download failed. cursor reason integer: " + query4.getInt(query4.getColumnIndex("reason")), 55);
                                Intent intent3 = new Intent(this.e, MobileCoreReport.class);
                                intent3.putExtra("1%dns#ge1%dk1%do1%dt", this.h);
                                intent3.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 70);
                                if (query4.getInt(query4.getColumnIndex("reason")) == 1007) {
                                    C0031p.a("download error: No SD card mounted", 55);
                                    intent3.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_ex", "Couldn't download: No SD card mounted.");
                                } else {
                                    StackTraceElement stackTraceElement = e3.getStackTrace()[0];
                                    intent3.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_ex", InstallationTracker.class.toString() + "###" + e3.getMessage() + "###" + stackTraceElement.getClassName() + "@" + stackTraceElement.getFileName() + ":" + stackTraceElement.getLineNumber());
                                }
                                this.e.startService(intent3);
                            }
                        }
                    } else {
                        C0031p.a("Download still in progress not installing yet. trying again in 1", 55);
                        String str8 = this.i;
                        a(stringExtra, stringExtra3, stringExtra4, stringExtra5, stringExtra2, longExtra, this.k);
                    }
                } else {
                    C0031p.a("WRITE_EXTERNAL_STORAGE permission unavailable. Using custom DownloadManager", 55);
                    String str9 = this.i;
                    String str10 = this.k;
                    a(stringExtra, stringExtra2, stringExtra4, stringExtra3, stringExtra5, booleanExtra);
                }
            } else {
                C0031p.a("DownloadManagerService | already downloading " + stringExtra3, 55);
            }
        } else if (intent.getStringExtra("extra_service_type").equals("com.ironsource.mobilcore.extra_service_close_notification")) {
            C0031p.a("got service type close notification", 55);
            int intExtra = intent.getIntExtra("com.ironsource.mobilcore.extra_close_notification_num", -1);
            if (intExtra >= 0) {
                this.a.cancel(intExtra);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void a(String str, String str2, String str3, String str4, String str5, long j2, String str6) {
        C0031p.a("recallDownloadService started", 55);
        Intent intent = new Intent(this.e, MobileCoreReport.class);
        intent.putExtra("extra_service_type", "download");
        intent.putExtra("com.ironsource.mobilcore.extra_download_url", str);
        intent.putExtra("com.ironsource.mobilcore.extra_download_appname", str2);
        intent.putExtra("com.ironsource.mobilcore.extra_download_app_img_name", str3);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer", str4);
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow", str6);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type", this.i);
        intent.putExtra("com.ironsource.mobilcore.extra_download_queue_id", j2);
        intent.setAction("assisst_download_manager");
        intent.putExtra("1%dns#ge1%dk1%do1%dt", this.h);
        intent.putExtra("com.ironsource.mobilcore.extra_download_filename", str5);
        intent.putExtra("com.ironsource.mobilcore.extra_is_apk_download", true);
        ((AlarmManager) this.e.getSystemService("alarm")).set(0, System.currentTimeMillis() + TapjoyConstants.THROTTLE_GET_TAP_POINTS_INTERVAL, PendingIntent.getService(this.e, (int) System.currentTimeMillis(), intent, 1073741824));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0242  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x03df  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r16, java.lang.String r17, java.lang.String r18, java.lang.String r19, java.lang.String r20, boolean r21) {
        /*
            r15 = this;
            java.lang.String r1 = "started download"
            r2 = 55
            com.ironsource.mobilcore.C0031p.a(r1, r2)
            java.lang.String r1 = a(r18)
            java.lang.String r2 = com.ironsource.mobilcore.av.b(r1)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "using appImage = "
            r3.<init>(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r3 = r3.toString()
            r4 = 55
            com.ironsource.mobilcore.C0031p.a(r3, r4)
            java.io.File r3 = new java.io.File
            r4 = 0
            int r5 = r1.length()
            int r6 = r2.length()
            int r5 = r5 - r6
            java.lang.String r1 = r1.substring(r4, r5)
            r3.<init>(r1, r2)
            java.lang.String r1 = r3.getAbsolutePath()
            android.graphics.Bitmap r6 = android.graphics.BitmapFactory.decodeFile(r1)
            java.lang.String r1 = "Download In Progress..."
            r0 = r19
            r15.a(r0, r1, r6)
            java.io.File r7 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            android.content.Context r2 = r15.e
            java.io.File r2 = r2.getFilesDir()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "/"
            java.lang.StringBuilder r1 = r1.append(r2)
            r0 = r17
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r1 = r1.toString()
            r7.<init>(r1)
            r3 = 0
            r2 = 0
            r4 = 0
            r1 = 0
            r15.f = r1
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x012f }
            r0 = r16
            r1.<init>(r0)     // Catch:{ Exception -> 0x012f }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Exception -> 0x012f }
            java.lang.String r8 = "Range"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f }
            java.lang.String r10 = "bytes="
            r9.<init>(r10)     // Catch:{ Exception -> 0x012f }
            r10 = 0
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x012f }
            java.lang.String r10 = "-"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x012f }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x012f }
            r1.setRequestProperty(r8, r9)     // Catch:{ Exception -> 0x012f }
            r1.connect()     // Catch:{ Exception -> 0x012f }
            int r8 = r1.getContentLength()     // Catch:{ Exception -> 0x012f }
            long r8 = (long) r8     // Catch:{ Exception -> 0x012f }
            java.io.InputStream r2 = r1.getInputStream()     // Catch:{ Exception -> 0x012f }
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r10 = new byte[r1]     // Catch:{ Exception -> 0x012f }
            r11 = 0
            r13 = 0
            int r1 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r1 == 0) goto L_0x01cb
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x012f }
            r11 = 1
            r1.<init>(r7, r11)     // Catch:{ Exception -> 0x012f }
            r3 = r1
        L_0x00b6:
            r3.close()     // Catch:{ Exception -> 0x012f }
            android.content.Context r1 = r15.e     // Catch:{ Exception -> 0x012f }
            r7 = 1
            r0 = r17
            java.io.FileOutputStream r3 = r1.openFileOutput(r0, r7)     // Catch:{ Exception -> 0x012f }
        L_0x00c2:
            int r1 = r2.read(r10)     // Catch:{ Exception -> 0x012f }
            r7 = -1
            if (r1 == r7) goto L_0x0312
            long r11 = (long) r1     // Catch:{ Exception -> 0x012f }
            long r4 = r4 + r11
            r11 = 0
            int r7 = (r8 > r11 ? 1 : (r8 == r11 ? 0 : -1))
            if (r7 <= 0) goto L_0x012a
            r11 = 100
            long r11 = r11 * r4
            long r11 = r11 / r8
            int r7 = (int) r11     // Catch:{ Exception -> 0x012f }
            int r11 = r15.f     // Catch:{ Exception -> 0x012f }
            if (r11 == r7) goto L_0x012a
            r15.f = r7     // Catch:{ Exception -> 0x012f }
            int r7 = r15.f     // Catch:{ Exception -> 0x012f }
            int r11 = r15.b     // Catch:{ Exception -> 0x012f }
            r12 = 16
            if (r11 < r12) goto L_0x02dd
            r11 = 100
            if (r7 >= r11) goto L_0x01d3
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            r11 = 17301633(0x1080081, float:2.4979616E-38)
            r7.setSmallIcon(r11)     // Catch:{ Exception -> 0x012f }
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            r11 = 100
            int r12 = r15.f     // Catch:{ Exception -> 0x012f }
            r13 = 0
            r7.setProgress(r11, r12, r13)     // Catch:{ Exception -> 0x012f }
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            r11 = 0
            r7.setAutoCancel(r11)     // Catch:{ Exception -> 0x012f }
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            r11 = 1
            r7.setOngoing(r11)     // Catch:{ Exception -> 0x012f }
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            android.content.Context r11 = r15.e     // Catch:{ Exception -> 0x012f }
            r12 = 0
            android.content.Intent r13 = new android.content.Intent     // Catch:{ Exception -> 0x012f }
            r13.<init>()     // Catch:{ Exception -> 0x012f }
            r14 = 0
            android.app.PendingIntent r11 = android.app.PendingIntent.getActivity(r11, r12, r13, r14)     // Catch:{ Exception -> 0x012f }
            r7.setContentIntent(r11)     // Catch:{ Exception -> 0x012f }
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            android.app.Notification r7 = r7.build()     // Catch:{ Exception -> 0x012f }
            r15.d = r7     // Catch:{ Exception -> 0x012f }
            android.app.NotificationManager r7 = r15.a     // Catch:{ Exception -> 0x012f }
            r11 = 495307123(0x1d85c973, float:3.54131E-21)
            android.app.Notification r12 = r15.d     // Catch:{ Exception -> 0x012f }
            r7.notify(r11, r12)     // Catch:{ Exception -> 0x012f }
        L_0x012a:
            r7 = 0
            r3.write(r10, r7, r1)     // Catch:{ Exception -> 0x012f }
            goto L_0x00c2
        L_0x012f:
            r1 = move-exception
            java.lang.String r4 = "exception: download failed."
            r5 = 2
            com.ironsource.mobilcore.C0031p.a(r4, r5)     // Catch:{ all -> 0x0231 }
            java.lang.String r4 = "Download Error."
            r0 = r19
            r15.a(r0, r4, r6)     // Catch:{ all -> 0x0231 }
            r21 = 0
            android.content.Intent r4 = new android.content.Intent     // Catch:{ all -> 0x0231 }
            android.content.Context r5 = r15.e     // Catch:{ all -> 0x0231 }
            java.lang.Class<com.ironsource.mobilcore.MobileCoreReport> r6 = com.ironsource.mobilcore.MobileCoreReport.class
            r4.<init>(r5, r6)     // Catch:{ all -> 0x0231 }
            java.lang.String r5 = "s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr"
            r6 = 70
            r4.putExtra(r5, r6)     // Catch:{ all -> 0x0231 }
            java.lang.StackTraceElement[] r5 = r1.getStackTrace()     // Catch:{ all -> 0x0231 }
            r6 = 0
            r5 = r5[r6]     // Catch:{ all -> 0x0231 }
            java.lang.String r6 = "com.ironsource.mobilcore.MobileCoreReport_extra_ex"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0231 }
            r7.<init>()     // Catch:{ all -> 0x0231 }
            java.lang.Class<com.ironsource.mobilcore.l> r8 = com.ironsource.mobilcore.C0027l.class
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x0231 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0231 }
            java.lang.String r8 = "###"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0231 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0231 }
            java.lang.StringBuilder r1 = r7.append(r1)     // Catch:{ all -> 0x0231 }
            java.lang.String r7 = "###"
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0231 }
            java.lang.String r7 = r5.getClassName()     // Catch:{ all -> 0x0231 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0231 }
            java.lang.String r7 = "@"
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0231 }
            java.lang.String r7 = r5.getFileName()     // Catch:{ all -> 0x0231 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0231 }
            java.lang.String r7 = ":"
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0231 }
            int r5 = r5.getLineNumber()     // Catch:{ all -> 0x0231 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x0231 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0231 }
            r4.putExtra(r6, r1)     // Catch:{ all -> 0x0231 }
            if (r3 == 0) goto L_0x01ae
            r3.flush()     // Catch:{ Exception -> 0x0477 }
            r3.close()     // Catch:{ Exception -> 0x0477 }
        L_0x01ae:
            if (r2 == 0) goto L_0x01b3
            r2.close()     // Catch:{ Exception -> 0x0477 }
        L_0x01b3:
            java.lang.String r1 = "S-"
            r0 = r20
            r15.a(r0, r1)
            com.ironsource.mobilcore.j r1 = r15.j
            r0 = r19
            r1.b(r0)
            com.ironsource.mobilcore.k r1 = com.ironsource.mobilcore.C0026k.a()
        L_0x01c5:
            r0 = r19
            r1.b(r0)
            return
        L_0x01cb:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x012f }
            r1.<init>(r7)     // Catch:{ Exception -> 0x012f }
            r3 = r1
            goto L_0x00b6
        L_0x01d3:
            r11 = 100
            if (r7 != r11) goto L_0x012a
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            r11 = 17301634(0x1080082, float:2.497962E-38)
            r7.setSmallIcon(r11)     // Catch:{ Exception -> 0x012f }
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f }
            r11.<init>()     // Catch:{ Exception -> 0x012f }
            r0 = r19
            java.lang.StringBuilder r11 = r11.append(r0)     // Catch:{ Exception -> 0x012f }
            java.lang.String r12 = " Download"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x012f }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x012f }
            android.app.Notification$Builder r7 = r7.setContentTitle(r11)     // Catch:{ Exception -> 0x012f }
            java.lang.String r11 = "Download Complete."
            r7.setContentText(r11)     // Catch:{ Exception -> 0x012f }
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            r11 = 1
            r7.setAutoCancel(r11)     // Catch:{ Exception -> 0x012f }
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            r11 = 0
            r7.setOngoing(r11)     // Catch:{ Exception -> 0x012f }
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            android.content.Context r11 = r15.e     // Catch:{ Exception -> 0x012f }
            r12 = 0
            android.content.Intent r13 = new android.content.Intent     // Catch:{ Exception -> 0x012f }
            r13.<init>()     // Catch:{ Exception -> 0x012f }
            r14 = 0
            android.app.PendingIntent r11 = android.app.PendingIntent.getActivity(r11, r12, r13, r14)     // Catch:{ Exception -> 0x012f }
            r7.setContentIntent(r11)     // Catch:{ Exception -> 0x012f }
            android.app.Notification$Builder r7 = r15.c     // Catch:{ Exception -> 0x012f }
            android.app.Notification r7 = r7.build()     // Catch:{ Exception -> 0x012f }
            r15.d = r7     // Catch:{ Exception -> 0x012f }
            android.app.NotificationManager r7 = r15.a     // Catch:{ Exception -> 0x012f }
            r11 = 495307123(0x1d85c973, float:3.54131E-21)
            android.app.Notification r12 = r15.d     // Catch:{ Exception -> 0x012f }
            r7.notify(r11, r12)     // Catch:{ Exception -> 0x012f }
            goto L_0x012a
        L_0x0231:
            r1 = move-exception
            r11 = r1
            if (r3 == 0) goto L_0x023b
            r3.flush()     // Catch:{ Exception -> 0x03e8 }
            r3.close()     // Catch:{ Exception -> 0x03e8 }
        L_0x023b:
            if (r2 == 0) goto L_0x0240
            r2.close()     // Catch:{ Exception -> 0x03e8 }
        L_0x0240:
            if (r21 == 0) goto L_0x03df
            java.lang.String r5 = com.ironsource.mobilcore.av.c(r16)
            android.content.Context r1 = r15.e
            java.io.File r1 = r1.getFilesDir()
            java.lang.String r1 = r1.getPath()
            android.content.Context r2 = r15.e
            java.lang.String r3 = r15.h
            java.lang.String r4 = com.ironsource.mobilcore.av.a(r2, r3, r1, r5)
            android.content.Context r1 = r15.e
            boolean r1 = com.ironsource.mobilcore.av.c(r1, r4)
            if (r1 != 0) goto L_0x046e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "attempting installation of "
            r1.<init>(r2)
            r0 = r17
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r1 = r1.toString()
            r2 = 55
            com.ironsource.mobilcore.C0031p.a(r1, r2)
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.<init>(r2)
            java.io.File r2 = new java.io.File
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            android.content.Context r6 = r15.e
            java.io.File r6 = r6.getFilesDir()
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r6 = "/"
            java.lang.StringBuilder r3 = r3.append(r6)
            r0 = r17
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            android.net.Uri r2 = android.net.Uri.fromFile(r2)
            java.lang.String r3 = "application/vnd.android.package-archive"
            r1.setDataAndType(r2, r3)
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r2)
            android.content.Context r2 = r15.e
            r2.startActivity(r1)
            android.content.Context r1 = r15.e
            java.lang.String r2 = r15.h
            java.lang.String r3 = r15.i
            r6 = 4
            r8 = 1
            java.lang.String r9 = r15.k
            r10 = -1
            r7 = r20
            com.ironsource.mobilcore.av.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10)
        L_0x02c4:
            android.app.NotificationManager r1 = r15.a
            r2 = 495307123(0x1d85c973, float:3.54131E-21)
            r1.cancel(r2)
        L_0x02cc:
            com.ironsource.mobilcore.j r1 = r15.j
            r0 = r19
            r1.b(r0)
            com.ironsource.mobilcore.k r1 = com.ironsource.mobilcore.C0026k.a()
            r0 = r19
            r1.b(r0)
            throw r11
        L_0x02dd:
            int r11 = r15.b     // Catch:{ Exception -> 0x012f }
            r12 = 9
            if (r11 < r12) goto L_0x012a
            r11 = 100
            if (r7 >= r11) goto L_0x0304
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f }
            java.lang.String r12 = "Download In Progress... - "
            r11.<init>(r12)     // Catch:{ Exception -> 0x012f }
            java.lang.StringBuilder r7 = r11.append(r7)     // Catch:{ Exception -> 0x012f }
            java.lang.String r11 = "%"
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ Exception -> 0x012f }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x012f }
            r11 = 0
            r0 = r19
            r15.a(r0, r7, r11)     // Catch:{ Exception -> 0x012f }
            goto L_0x012a
        L_0x0304:
            r11 = 100
            if (r7 != r11) goto L_0x012a
            java.lang.String r7 = "Download Complete."
            r11 = 0
            r0 = r19
            r15.a(r0, r7, r11)     // Catch:{ Exception -> 0x012f }
            goto L_0x012a
        L_0x0312:
            r3.close()     // Catch:{ Exception -> 0x012f }
            java.lang.String r1 = "download finished"
            r4 = 55
            com.ironsource.mobilcore.C0031p.a(r1, r4)     // Catch:{ Exception -> 0x012f }
            java.lang.String r1 = "Download Complete."
            r0 = r19
            r15.a(r0, r1, r6)     // Catch:{ Exception -> 0x012f }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012f }
            java.lang.String r4 = "DownloadManagerService | removing "
            r1.<init>(r4)     // Catch:{ Exception -> 0x012f }
            r0 = r19
            java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ Exception -> 0x012f }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x012f }
            r4 = 55
            com.ironsource.mobilcore.C0031p.a(r1, r4)     // Catch:{ Exception -> 0x012f }
            if (r3 == 0) goto L_0x0341
            r3.flush()     // Catch:{ Exception -> 0x0506 }
            r3.close()     // Catch:{ Exception -> 0x0506 }
        L_0x0341:
            if (r2 == 0) goto L_0x0346
            r2.close()     // Catch:{ Exception -> 0x0506 }
        L_0x0346:
            if (r21 == 0) goto L_0x04fd
            java.lang.String r5 = com.ironsource.mobilcore.av.c(r16)
            android.content.Context r1 = r15.e
            java.io.File r1 = r1.getFilesDir()
            java.lang.String r1 = r1.getPath()
            android.content.Context r2 = r15.e
            java.lang.String r3 = r15.h
            java.lang.String r4 = com.ironsource.mobilcore.av.a(r2, r3, r1, r5)
            android.content.Context r1 = r15.e
            boolean r1 = com.ironsource.mobilcore.av.c(r1, r4)
            if (r1 != 0) goto L_0x058c
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "attempting installation of "
            r1.<init>(r2)
            r0 = r17
            java.lang.StringBuilder r1 = r1.append(r0)
            java.lang.String r1 = r1.toString()
            r2 = 55
            com.ironsource.mobilcore.C0031p.a(r1, r2)
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.VIEW"
            r1.<init>(r2)
            java.io.File r2 = new java.io.File
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            android.content.Context r6 = r15.e
            java.io.File r6 = r6.getFilesDir()
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r6 = "/"
            java.lang.StringBuilder r3 = r3.append(r6)
            r0 = r17
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            android.net.Uri r2 = android.net.Uri.fromFile(r2)
            java.lang.String r3 = "application/vnd.android.package-archive"
            r1.setDataAndType(r2, r3)
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r2)
            android.content.Context r2 = r15.e
            r2.startActivity(r1)
            android.content.Context r1 = r15.e
            java.lang.String r2 = r15.h
            java.lang.String r3 = r15.i
            r6 = 4
            r8 = 1
            java.lang.String r9 = r15.k
            r10 = -1
            r7 = r20
            com.ironsource.mobilcore.av.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10)
        L_0x03ca:
            android.app.NotificationManager r1 = r15.a
            r2 = 495307123(0x1d85c973, float:3.54131E-21)
            r1.cancel(r2)
        L_0x03d2:
            com.ironsource.mobilcore.j r1 = r15.j
            r0 = r19
            r1.b(r0)
            com.ironsource.mobilcore.k r1 = com.ironsource.mobilcore.C0026k.a()
            goto L_0x01c5
        L_0x03df:
            java.lang.String r1 = "S-"
            r0 = r20
            r15.a(r0, r1)
            goto L_0x02cc
        L_0x03e8:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "caught exception sent to MobileCore. "
            r2.<init>(r3)
            java.lang.String r3 = r1.getMessage()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r3 = 2
            com.ironsource.mobilcore.C0031p.a(r2, r3)
            android.content.Intent r2 = new android.content.Intent
            android.content.Context r3 = r15.e
            java.lang.Class<com.ironsource.mobilcore.MobileCoreReport> r4 = com.ironsource.mobilcore.MobileCoreReport.class
            r2.<init>(r3, r4)
            java.lang.String r3 = "s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr"
            r4 = 70
            r2.putExtra(r3, r4)
            java.lang.StackTraceElement[] r3 = r1.getStackTrace()
            r4 = 0
            r3 = r3[r4]
            java.lang.String r4 = "com.ironsource.mobilcore.MobileCoreReport_extra_ex"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.Class<com.ironsource.mobilcore.l> r6 = com.ironsource.mobilcore.C0027l.class
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = "###"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r1 = r1.getMessage()
            java.lang.StringBuilder r1 = r5.append(r1)
            java.lang.String r5 = "###"
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = r3.getClassName()
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = "@"
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = r3.getFileName()
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = ":"
            java.lang.StringBuilder r1 = r1.append(r5)
            int r3 = r3.getLineNumber()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r2.putExtra(r4, r1)
            android.content.Context r1 = r15.e
            r1.startService(r2)
            goto L_0x0240
        L_0x046e:
            java.lang.String r1 = "AI"
            r0 = r20
            r15.a(r0, r1)
            goto L_0x02c4
        L_0x0477:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "caught exception sent to MobileCore. "
            r2.<init>(r3)
            java.lang.String r3 = r1.getMessage()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r3 = 2
            com.ironsource.mobilcore.C0031p.a(r2, r3)
            android.content.Intent r2 = new android.content.Intent
            android.content.Context r3 = r15.e
            java.lang.Class<com.ironsource.mobilcore.MobileCoreReport> r4 = com.ironsource.mobilcore.MobileCoreReport.class
            r2.<init>(r3, r4)
            java.lang.String r3 = "s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr"
            r4 = 70
            r2.putExtra(r3, r4)
            java.lang.StackTraceElement[] r3 = r1.getStackTrace()
            r4 = 0
            r3 = r3[r4]
            java.lang.String r4 = "com.ironsource.mobilcore.MobileCoreReport_extra_ex"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.Class<com.ironsource.mobilcore.l> r6 = com.ironsource.mobilcore.C0027l.class
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = "###"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r1 = r1.getMessage()
            java.lang.StringBuilder r1 = r5.append(r1)
            java.lang.String r5 = "###"
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = r3.getClassName()
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = "@"
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = r3.getFileName()
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = ":"
            java.lang.StringBuilder r1 = r1.append(r5)
            int r3 = r3.getLineNumber()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r2.putExtra(r4, r1)
            android.content.Context r1 = r15.e
            r1.startService(r2)
            goto L_0x01b3
        L_0x04fd:
            java.lang.String r1 = "S-"
            r0 = r20
            r15.a(r0, r1)
            goto L_0x03d2
        L_0x0506:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "caught exception sent to MobileCore. "
            r2.<init>(r3)
            java.lang.String r3 = r1.getMessage()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r3 = 2
            com.ironsource.mobilcore.C0031p.a(r2, r3)
            android.content.Intent r2 = new android.content.Intent
            android.content.Context r3 = r15.e
            java.lang.Class<com.ironsource.mobilcore.MobileCoreReport> r4 = com.ironsource.mobilcore.MobileCoreReport.class
            r2.<init>(r3, r4)
            java.lang.String r3 = "s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr"
            r4 = 70
            r2.putExtra(r3, r4)
            java.lang.StackTraceElement[] r3 = r1.getStackTrace()
            r4 = 0
            r3 = r3[r4]
            java.lang.String r4 = "com.ironsource.mobilcore.MobileCoreReport_extra_ex"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.Class<com.ironsource.mobilcore.l> r6 = com.ironsource.mobilcore.C0027l.class
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = "###"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r1 = r1.getMessage()
            java.lang.StringBuilder r1 = r5.append(r1)
            java.lang.String r5 = "###"
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = r3.getClassName()
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = "@"
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = r3.getFileName()
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r5 = ":"
            java.lang.StringBuilder r1 = r1.append(r5)
            int r3 = r3.getLineNumber()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r2.putExtra(r4, r1)
            android.content.Context r1 = r15.e
            r1.startService(r2)
            goto L_0x0346
        L_0x058c:
            java.lang.String r1 = "AI"
            r0 = r20
            r15.a(r0, r1)
            goto L_0x03ca
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mobilcore.C0027l.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean):void");
    }

    private static String a(String str) {
        C0031p.a("removing prefix from: " + str, 55);
        if (str.startsWith("file://")) {
            return str.substring(8);
        }
        C0031p.a("Prefix cutting failed: no file:/// prefix found." + str, 55);
        return str;
    }

    @SuppressLint({"NewApi"})
    private void a(String str, String str2, Bitmap bitmap) {
        if (this.b >= 14) {
            this.c = new Notification.Builder(this.e).setContentTitle(str).setContentText(str2).setSmallIcon(17301634);
            if (bitmap != null) {
                this.c.setLargeIcon(bitmap);
            }
            this.c.setAutoCancel(true);
            this.c.setOngoing(false);
            this.c.setContentIntent(PendingIntent.getActivity(this.e, 0, new Intent(), 0));
            this.d = this.c.build();
            this.a.notify(495307123, this.d);
            return;
        }
        C0031p.a("< 2.3 notification.", 55);
        PendingIntent activity = PendingIntent.getActivity(this.e, 0, new Intent(), 0);
        if (this.a == null) {
            this.a = (NotificationManager) this.e.getSystemService("notification");
        }
        Notification notification = new Notification(17301634, str, System.currentTimeMillis());
        if ("Download Complete.".equals(str2)) {
            notification.flags &= 2;
            notification.flags |= 16;
        } else {
            notification.flags &= 16;
            notification.flags |= 2;
        }
        notification.setLatestEventInfo(this.e, str, str2, activity);
        this.a.notify(495307123, notification);
    }

    private void a(String str, String str2) {
        Intent intent = new Intent(this.e, MobileCoreReport.class);
        intent.putExtra("1%dns#ge1%dk1%do1%dt", this.h);
        intent.putExtra("s#ge1%dp1%dys#gT1%dt1%dr1%do1%dps#ge1%dr", 99);
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_flow", this.k);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_flow_type", this.i);
        intent.putExtra("com.ironsource.mobilecore.MobileCoreReport_extra_result", str2);
        intent.putExtra("com.ironsource.mobilcore.MobileCoreReport_extra_offer", str);
        intent.putExtra("1%dns#ge1%dk1%do1%dt", this.h);
        this.e.startService(intent);
    }
}
