package com.ironsource.mobilcore;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;

final class aj extends C0015ah {
    aj(Activity activity, int i) {
        super(activity, i, C0009ab.TOP);
    }

    public final void a(int i) {
        this.p = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{-16777216, 0});
        invalidate();
    }
}
