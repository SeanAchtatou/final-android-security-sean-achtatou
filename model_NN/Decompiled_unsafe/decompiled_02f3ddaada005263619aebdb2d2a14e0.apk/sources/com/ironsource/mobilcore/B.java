package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ironsource.mobilcore.CallbackResponse;

@SuppressLint({"SetJavaScriptEnabled", "ViewConstructor"})
class B extends WebView {
    protected Activity a;
    /* access modifiers changed from: private */
    public RelativeLayout b;
    private View c;
    private a d;

    static /* synthetic */ int a(B b2, View view) {
        Rect rect = new Rect();
        view.getWindowVisibleDisplayFrame(rect);
        return rect.top;
    }

    public B(Context context, Activity activity) {
        super(context);
        this.a = activity;
        getSettings().setJavaScriptEnabled(true);
        a();
    }

    public final void a(a aVar) {
        this.d = aVar;
    }

    public final void c() {
        if (this.a != null) {
            final ViewGroup viewGroup = (ViewGroup) this.a.getWindow().getDecorView();
            this.c = (ViewGroup) viewGroup.getChildAt(0);
            ((InputMethodManager) getContext().getSystemService("input_method")).hideSoftInputFromWindow(this.c.getWindowToken(), 0);
            this.c.postDelayed(new Runnable() {
                public final void run() {
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
                    viewGroup.removeAllViews();
                    viewGroup.setPadding(0, B.a(B.this, viewGroup), 0, 0);
                    viewGroup.addView(B.this.d(), layoutParams);
                }
            }, 500);
            requestFocus();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        setWebViewClient(new b(this, (byte) 0));
    }

    /* access modifiers changed from: protected */
    public void b() {
        ViewGroup viewGroup = (ViewGroup) this.a.getWindow().getDecorView();
        viewGroup.removeAllViews();
        viewGroup.setPadding(0, 0, 0, 0);
        viewGroup.addView(this.c, -1, -1);
        if (this.d != null) {
            this.d.a();
        }
    }

    class b extends WebViewClient {
        private b(B b) {
        }

        /* synthetic */ b(B b, byte b2) {
            this(b);
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            return false;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            if (canGoBack()) {
                goBack();
            } else {
                b();
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, float):int
     arg types: [android.app.Activity, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
      com.ironsource.mobilcore.b.a(android.content.Context, float):int */
    /* access modifiers changed from: private */
    @SuppressLint({"NewApi"})
    public View d() {
        PackageManager packageManager = this.a.getPackageManager();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        LinearLayout linearLayout = new LinearLayout(this.a);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.rightMargin = C0017b.a((Context) this.a, 5.0f);
        this.b = new RelativeLayout(this.a);
        this.b.setId(2);
        this.b.setLayoutParams(layoutParams2);
        this.b.setPadding(12, 9, 0, 9);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(15);
        Bitmap a2 = C0017b.a(this.a, C0017b.o("iVBORw0KGgoAAAANSUhEUgAAAAsAAAAYCAYAAAAs7gcTAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB90FHAwJN9A+JSQAAAGaSURBVDjLdZO9ihVBFIS/6tRIV4QVwUBW8RXMNJBFRFADM8N75gEM/AmERbngE0zPEwjCFUREIxNzExEDwUB2YTHYDY26DLZ7mTs7nmTgTHV1VZ3TYlLDMABs2f5q+y+AJAPfxEz1ff9a0gPp6LdtgDtr4Jwztjcl7TUgYGB/sVhsaob1fUrplm0kqX7vAW81YT2fUtqtgKrAvyPiIkBq4IhA0kvbHmmVpJ2G0ZhV0u5YaynloOu6jdZII9alJFzrKDF1OedjPynnzDAMp4CHNdMm9iAi3ozNp4jAdq5m3ORJetJuPdbc9/1ZSX8qgnrgMCLOTGNNkp41YCOw/Xhusgk4PW1KOvwf+PkEaODVHFh10z7Y3q5SVH3eB1ZrBodhoJRyTtL+aHIG9iLiwgnmOsUVcBdwTUXAtu1PXdetyaCUclXS9/G4bf+KiEsnmOuhd8Dt1m/aI2K1tnXL5RLbj0YGWzI7s8x1+b+klK6NBgRwA/g8lUEp5YqkH5NkfkbE5dkHm3P+CNwcbSDA9VSf/tqjBZ5OJBp48Q8fl9kHP5yZfQAAAABJRU5ErkJggg=="));
        ImageView imageView = new ImageView(this.a);
        imageView.setImageBitmap(a2);
        imageView.setLayoutParams(layoutParams3);
        imageView.setId(1);
        Drawable drawable = null;
        try {
            drawable = packageManager.getApplicationIcon(this.a.getApplicationContext().getPackageName());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(15);
        layoutParams4.addRule(1, 1);
        layoutParams4.leftMargin = C0017b.a((Context) this.a, 2.0f);
        ImageView imageView2 = new ImageView(this.a);
        imageView2.setLayoutParams(layoutParams4);
        imageView2.setImageDrawable(drawable);
        imageView2.setOnTouchListener(new View.OnTouchListener() {
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case 0:
                        B.this.b.setBackgroundColor(-3355444);
                        break;
                    case 1:
                    case 3:
                        B.this.b.setBackgroundColor(0);
                        break;
                }
                return false;
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                B.this.b();
            }
        });
        TextView textView = new TextView(this.a);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams5.addRule(15);
        layoutParams5.addRule(1, 2);
        textView.setLayoutParams(layoutParams5);
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.END);
        textView.setTypeface(null, 1);
        textView.setBackgroundColor(0);
        textView.setTextSize(2, 18.0f);
        textView.setBackgroundColor(0);
        ApplicationInfo applicationInfo = null;
        try {
            applicationInfo = packageManager.getApplicationInfo(this.a.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        textView.setText(((String) packageManager.getApplicationLabel(applicationInfo)).toUpperCase().toUpperCase());
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout relativeLayout = new RelativeLayout(this.a);
        relativeLayout.setLayoutParams(layoutParams6);
        relativeLayout.setPadding(0, 0, 12, 0);
        this.b.addView(imageView);
        this.b.addView(imageView2);
        relativeLayout.addView(this.b);
        relativeLayout.addView(textView);
        linearLayout.addView(relativeLayout);
        linearLayout.addView(this);
        return linearLayout;
    }

    public interface a {
        final /* synthetic */ CallbackResponse a;

        default a(aF aFVar, CallbackResponse callbackResponse) {
            this.a = callbackResponse;
        }

        default void a() {
            if (this.a != null) {
                this.a.onConfirmation(CallbackResponse.TYPE.OFFERWALL_BACK);
            }
        }
    }
}
