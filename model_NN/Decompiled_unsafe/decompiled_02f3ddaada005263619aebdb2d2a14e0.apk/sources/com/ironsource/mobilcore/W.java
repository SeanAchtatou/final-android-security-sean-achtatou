package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.view.MotionEvent;

final class W extends V {
    W(Activity activity, int i) {
        super(activity, i);
    }

    public final void a(boolean z) {
        a(this.u, 0, z);
    }

    public final void b(boolean z) {
        a(0, 0, z);
    }

    public final void a(int i) {
        this.p = new GradientDrawable(GradientDrawable.Orientation.RIGHT_LEFT, new int[]{-16777216, 0});
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int i7 = (int) this.c;
        this.s.layout(0, 0, this.u, i6);
        f(i7);
        if (m) {
            this.t.layout(0, 0, i5, i6);
        } else {
            this.t.layout(i7, 0, i5 + i7, i6);
        }
    }

    @SuppressLint({"NewApi"})
    private void f(int i) {
        if (this.l && this.u != 0) {
            int i2 = this.u;
            float f = (((float) i2) - ((float) i)) / ((float) i2);
            if (!m) {
                this.s.offsetLeftAndRight(((int) ((((float) i2) * (-f)) * 0.25f)) - this.s.getLeft());
                this.s.setVisibility(i == 0 ? 4 : 0);
            } else if (i > 0) {
                this.s.setTranslationX((float) ((int) (((float) i2) * (-f) * 0.25f)));
            } else {
                this.s.setTranslationX((float) (-i2));
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas, int i) {
        this.p.setBounds(i - this.q, 0, i, getHeight());
        this.p.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void b(Canvas canvas, int i) {
        int height = getHeight();
        float f = ((float) i) / ((float) this.u);
        this.n.setBounds(0, 0, i, height);
        this.n.setAlpha((int) ((1.0f - f) * 185.0f));
        this.n.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        int i = this.u / 3;
        this.i.b(i, 0, -i, 0, 1000);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    public final void b(int i) {
        if (m) {
            this.t.setTranslationX((float) i);
            f(i);
            invalidate();
            return;
        }
        this.t.offsetLeftAndRight(i - this.t.getLeft());
        f(i);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final boolean a(MotionEvent motionEvent) {
        return motionEvent.getX() > this.c;
    }

    /* access modifiers changed from: protected */
    public final boolean b() {
        return (!this.w && this.e <= ((float) this.z)) || (this.w && this.e >= this.c);
    }

    /* access modifiers changed from: protected */
    public final boolean a(float f) {
        return (!this.w && this.e <= ((float) this.z) && f > 0.0f) || (this.w && this.e >= this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* access modifiers changed from: protected */
    public final void b(float f) {
        c(Math.min(Math.max(this.c + f, 0.0f), (float) this.u));
    }

    /* access modifiers changed from: protected */
    public final void b(MotionEvent motionEvent) {
        int i = (int) this.c;
        if (this.d) {
            this.j.computeCurrentVelocity(1000, (float) this.k);
            int xVelocity = (int) this.j.getXVelocity();
            this.g = motionEvent.getX();
            a(this.j.getXVelocity() > 0.0f ? this.u : 0, xVelocity, true);
        } else if (this.w && motionEvent.getX() > ((float) i)) {
            b(true);
        }
    }
}
