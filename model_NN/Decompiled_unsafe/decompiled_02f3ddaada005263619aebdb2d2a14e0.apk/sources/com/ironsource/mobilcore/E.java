package com.ironsource.mobilcore;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ironsource.mobilcore.C0034s;
import org.json.JSONObject;

final class E extends C0034s {
    private String q;

    public E(Context context, ao aoVar, C0034s.a aVar) {
        super(context, aoVar, aVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.g = new RelativeLayout(this.c);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ironsource.mobilcore.b.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.ironsource.mobilcore.b.a(android.content.Context, android.graphics.Bitmap):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONArray):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(java.util.HashMap<java.lang.String, java.lang.String>, org.json.JSONObject):java.util.HashMap<java.lang.String, java.lang.String>
      com.ironsource.mobilcore.b.a(android.view.View, android.graphics.drawable.Drawable):void
      com.ironsource.mobilcore.b.a(android.content.Context, int):boolean
      com.ironsource.mobilcore.b.a(android.content.Context, float):int */
    /* access modifiers changed from: protected */
    public final void b() {
        this.p = new ImageView(this.c);
        this.p.setId(j());
        this.p.setScaleType(ImageView.ScaleType.CENTER_CROP);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        this.p.setLayoutParams(layoutParams);
        this.m = new TextView(this.c);
        this.m.setBackgroundColor(Color.parseColor("#90000000"));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(8, this.p.getId());
        this.m.setLayoutParams(layoutParams2);
        int a = C0017b.a(this.c, 5.0f);
        this.m.setPadding(a, a, a, a);
        this.m.setGravity(1);
        this.m.setTypeface(null, 1);
        this.m.setTextColor(-1);
        this.m.setSingleLine();
        this.m.setEllipsize(TextUtils.TruncateAt.END);
        this.m.setTextSize(2, 16.0f);
        View view = new View(this.c);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(C0017b.a(this.c, (float) this.d.g()), -2);
        layoutParams3.addRule(11);
        layoutParams3.addRule(6, this.p.getId());
        layoutParams3.addRule(8, this.p.getId());
        view.setLayoutParams(layoutParams3);
        view.setBackgroundColor(this.d.e());
        ViewGroup viewGroup = (ViewGroup) this.g;
        viewGroup.setClickable(true);
        viewGroup.setFocusable(true);
        viewGroup.addView(this.p);
        viewGroup.addView(this.m);
        viewGroup.addView(view);
        a(this.d.o(), this.d.b(), new TextView[0]);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        String str;
        this.m.setText(this.i);
        if (this.q == null) {
            str = this.a;
        } else if (this.q == "") {
            this.b = false;
            return;
        } else {
            str = this.q;
        }
        if (!TextUtils.isEmpty(str)) {
            Bitmap decodeFile = BitmapFactory.decodeFile(str, null);
            if (decodeFile != null) {
                this.p.setImageBitmap(C0017b.a(this.c, decodeFile));
                return;
            }
            this.b = false;
            return;
        }
        this.b = false;
    }

    /* access modifiers changed from: protected */
    public final void c(JSONObject jSONObject) {
        super.c(jSONObject);
        this.q = jSONObject.optString("cover_img", null);
    }
}
