package com.ironsource.mobilcore;

import com.ironsource.mobilcore.av;
import java.io.File;
import org.apache.http.HttpEntity;

/* renamed from: com.ironsource.mobilcore.i  reason: case insensitive filesystem */
public final class C0024i extends C0023h {
    C0024i(String str, String str2, av.b bVar) {
        super(str, str2, bVar);
    }

    public final boolean a(HttpEntity httpEntity) {
        File b = super.b(httpEntity);
        boolean a = aI.a(this.c, b);
        if (this.d != null) {
            this.d.a(this.b, b != null);
        }
        return a;
    }
}
