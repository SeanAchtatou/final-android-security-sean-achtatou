package com.ironsource.mobilcore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import com.ironsource.mobilcore.C0036u;

abstract class Y extends ViewGroup {
    static final boolean m = (Build.VERSION.SDK_INT >= 12);
    protected int A;
    protected boolean B;
    private int a;
    private a b;
    private Activity c;
    protected Drawable n;
    protected boolean o;
    protected Drawable p;
    protected int q;
    protected final Rect r;
    protected S s;
    protected S t;
    protected int u;
    protected boolean v;
    protected boolean w;
    protected int x;
    protected int y;
    protected int z;

    public abstract void a(int i);

    public abstract void a(boolean z2);

    public abstract void b(boolean z2);

    public abstract void c(int i);

    public abstract void c(boolean z2);

    public abstract boolean c();

    public abstract void d();

    public interface a {
        final /* synthetic */ int a;
        final /* synthetic */ am b;

        default a(am amVar, int i) {
            this.b = amVar;
            this.a = i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ironsource.mobilcore.am.a(com.ironsource.mobilcore.am, boolean):boolean
         arg types: [com.ironsource.mobilcore.am, int]
         candidates:
          com.ironsource.mobilcore.am.a(com.ironsource.mobilcore.am, int):int
          com.ironsource.mobilcore.am.a(android.view.ViewGroup, java.lang.String):com.ironsource.mobilcore.u
          com.ironsource.mobilcore.am.a(com.ironsource.mobilcore.am, com.ironsource.mobilcore.u$a):void
          com.ironsource.mobilcore.am.a(com.ironsource.mobilcore.am, java.lang.Exception):void
          com.ironsource.mobilcore.am.a(com.ironsource.mobilcore.am, java.lang.String):void
          com.ironsource.mobilcore.am.a(android.content.Context, java.lang.String):void
          com.ironsource.mobilcore.am.a(com.ironsource.mobilcore.am, boolean):boolean */
        default void a(int i) {
            int i2;
            String str;
            C0038w wVar;
            C0036u uVar;
            if (i == 2) {
                am.g();
            } else if (i == 0 || i == 8) {
                ((InputMethodManager) this.b.a.getSystemService("input_method")).hideSoftInputFromWindow(this.b.s.getWindowToken(), 0);
                if (!this.b.j && !TextUtils.isEmpty(this.b.k) && this.b.h.containsKey(this.b.k) && (uVar = (C0036u) this.b.h.get(this.b.k)) != null && (uVar instanceof C0037v)) {
                    ((C0037v) uVar).o();
                    boolean unused = this.b.j = true;
                }
                if (i == 8) {
                    C0031p.a("onDrawerStateChange | Clearing download in progress flags when slider opens", 55);
                    am.a(this.b, C0036u.a.VISIBLE);
                    if (av.b().getBoolean("sliderOpenedByUser", false)) {
                        Context a2 = this.b.a;
                        C0038w[] wVarArr = new C0038w[1];
                        Context a3 = this.b.a;
                        if (a3 == null) {
                            wVar = null;
                        } else {
                            switch (a3.getResources().getConfiguration().orientation) {
                                case 1:
                                    str = "orientation_portrait";
                                    break;
                                case 2:
                                    str = "orientation_landscape";
                                    break;
                                case 3:
                                    str = "orientation_square";
                                    break;
                                default:
                                    str = "orientation_undefined";
                                    break;
                            }
                            wVar = new C0038w("orientation", str);
                        }
                        wVarArr[0] = wVar;
                        C0031p.a(a2, "slider", "slider_features", "opened", wVarArr);
                    }
                    i2 = 32;
                } else if (i == 0) {
                    am.a(this.b, C0036u.a.HIDDEN);
                    if (av.b().getBoolean("sliderOpenedByUser", false)) {
                        C0031p.a(this.b.a, "slider", "slider_features", "closed", new C0038w[0]);
                    }
                    i2 = this.a;
                } else {
                    i2 = -1;
                }
                if (this.b.b != null && i2 != -1) {
                    this.b.b.getWindow().setSoftInputMode(i2);
                }
            }
        }
    }

    public static Y a(Activity activity, int i) {
        U q2;
        switch (C0009ab.LEFT) {
            case LEFT:
                q2 = new W(activity, 1);
                break;
            case RIGHT:
                q2 = new C0010ac(activity, 1);
                break;
            case TOP:
                q2 = new C0016ai(activity, 1);
                break;
            case BOTTOM:
                q2 = new Q(activity, 1);
                break;
            default:
                throw new IllegalArgumentException("position must be one of LEFT, TOP, RIGHT or BOTTOM");
        }
        q2.setId(4660);
        switch (1) {
            case 0:
                ViewGroup viewGroup = (ViewGroup) activity.findViewById(16908290);
                viewGroup.removeAllViews();
                viewGroup.addView(q2, -1, -1);
                break;
            case 1:
                ViewGroup viewGroup2 = (ViewGroup) activity.getWindow().getDecorView();
                ViewGroup viewGroup3 = (ViewGroup) viewGroup2.getChildAt(0);
                viewGroup2.removeAllViews();
                viewGroup2.addView(q2, -1, -1);
                q2.t.addView(viewGroup3, viewGroup3.getLayoutParams());
                break;
            default:
                throw new RuntimeException("Unknown menu mode: " + 1);
        }
        return q2;
    }

    Y(Activity activity, int i) {
        this(activity);
        this.c = activity;
        this.a = i;
    }

    private Y(Context context) {
        this(context, (AttributeSet) null);
    }

    private Y(Context context, AttributeSet attributeSet) {
        super(context, null);
        this.r = new Rect();
        this.a = 0;
        this.x = 0;
        this.A = 1;
        this.B = true;
        a(context, (AttributeSet) null);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"NewApi"})
    public void a(Context context, AttributeSet attributeSet) {
        boolean z2 = false;
        setWillNotDraw(false);
        setFocusable(false);
        if (this.u != -1) {
            z2 = true;
        }
        this.v = z2;
        this.o = true;
        this.p = null;
        if (this.p == null) {
            a(-16777216);
        }
        this.q = d(6);
        this.y = d(24);
        this.s = new S(context);
        this.s.setId(4661);
        addView(this.s);
        this.t = new Z(context);
        C0017b.a(this.t, new ColorDrawable(Color.parseColor("#000000")));
        addView(this.t);
        this.n = new T(-16777216);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
    }

    /* access modifiers changed from: protected */
    public final int d(int i) {
        return (int) ((getResources().getDisplayMetrics().density * ((float) i)) + 0.5f);
    }

    public final void a(a aVar) {
        this.b = aVar;
    }

    public final void a(View view) {
        boolean z2;
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        this.s.removeAllViews();
        this.s.addView(view, layoutParams);
        if ((this.c.getWindow().getAttributes().flags & AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END) != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (!z2 && this.a == 1) {
            Rect rect = new Rect();
            this.c.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
            int i = rect.top;
            if (i != 0) {
                this.s.setPadding(0, i, 0, 0);
                C0031p.a("MCSliderDrawerMenuDrawer.addMenuContainerPaddingIfNeeded(), statusBarHeight=" + i + ", setting it as a paddingTop for mMenuContainer", 55);
            }
        }
    }

    public final void b(View view) {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        switch (this.a) {
            case 0:
                this.t.removeAllViews();
                this.t.addView(view, layoutParams);
                return;
            case 1:
                this.c.setContentView(view, layoutParams);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final void e(int i) {
        if (i != this.x) {
            int i2 = this.x;
            this.x = i;
            if (this.b != null) {
                this.b.a(i);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean fitSystemWindows(Rect rect) {
        if (this.a == 1) {
            this.s.setPadding(0, rect.top, 0, 0);
        }
        return super.fitSystemWindows(rect);
    }
}
