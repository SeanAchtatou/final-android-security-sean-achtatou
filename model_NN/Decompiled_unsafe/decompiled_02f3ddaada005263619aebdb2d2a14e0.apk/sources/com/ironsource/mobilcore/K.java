package com.ironsource.mobilcore;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

final class K extends F {
    public K(Context context, ao aoVar) {
        super(context, aoVar);
    }

    public final String e() {
        return "ironsourceRateUs";
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        super.a(view);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("market://details?id=" + this.c.getPackageName()));
        this.e.startActivity(intent);
    }
}
