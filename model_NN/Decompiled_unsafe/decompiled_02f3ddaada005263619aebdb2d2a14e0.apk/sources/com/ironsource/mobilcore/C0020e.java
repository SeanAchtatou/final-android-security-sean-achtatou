package com.ironsource.mobilcore;

import android.content.Context;
import com.ironsource.mobilcore.av;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.ironsource.mobilcore.e  reason: case insensitive filesystem */
public final class C0020e extends C0023h {
    C0020e(String str, String str2, av.b bVar) {
        super(str, str2, bVar);
    }

    private boolean a(JSONObject jSONObject) {
        try {
            a(jSONObject, jSONObject.optJSONArray("extra"));
            JSONArray optJSONArray = jSONObject.optJSONArray("ads");
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                optJSONObject.putOpt("img", aH.a(optJSONObject.optString("img"), this.d));
                optJSONObject.putOpt("cover_img", aH.a(optJSONObject.optString("cover_img"), this.d));
                a(optJSONObject, optJSONObject.optJSONArray("extra"));
            }
            return true;
        } catch (Exception e) {
            if (this.d == null) {
                return false;
            }
            this.d.a(this.b, e);
            return false;
        }
    }

    private void a(JSONObject jSONObject, JSONArray jSONArray) {
        boolean z;
        if (jSONArray != null) {
            try {
                JSONArray jSONArray2 = new JSONArray();
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject optJSONObject = jSONArray.optJSONObject(i);
                    String string = optJSONObject.getString("value");
                    String string2 = optJSONObject.getString("name");
                    String string3 = optJSONObject.getString("type");
                    if ("img".equalsIgnoreCase(string3) || "audio".equalsIgnoreCase(string3) || "video".equalsIgnoreCase(string3) || "file".equalsIgnoreCase(string3)) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if (z) {
                        jSONObject.putOpt(string2, aH.a(string, this.d));
                    } else if ("report".equalsIgnoreCase(string3)) {
                        jSONArray2.put(optJSONObject);
                    } else {
                        jSONObject.putOpt(string2, string);
                    }
                }
                jSONObject.remove("extra");
                jSONObject.put("extra", jSONArray2);
            } catch (JSONException e) {
                if (this.d != null) {
                    this.d.a(this.b, e);
                }
            }
        }
    }

    public final boolean a(HttpEntity httpEntity) {
        boolean z;
        String str;
        try {
            JSONObject jSONObject = new JSONObject(EntityUtils.toString(httpEntity));
            if (jSONObject.optString("id", null) == null) {
                av.a(MobileCore.b, MobileCore.class.getName(), "Feed is missing field 'id'");
            }
            JSONArray optJSONArray = jSONObject.optJSONArray("ads");
            if (optJSONArray == null || optJSONArray.length() <= 0) {
                Context context = MobileCore.b;
                String name = MobileCore.class.getName();
                Object[] objArr = new Object[1];
                if (optJSONArray == null) {
                    str = "missing";
                } else {
                    str = "empty";
                }
                objArr[0] = str;
                av.a(context, name, String.format("Feed ads are invalid (%s)", objArr));
                z = false;
            } else {
                z = true;
            }
            if (!z) {
                return false;
            }
            boolean a = a(jSONObject);
            aF.a().a(jSONObject, this.b);
            if (this.d != null) {
                this.d.a(this.b, a);
            }
            return true;
        } catch (Exception e) {
            if (this.d == null) {
                return false;
            }
            this.d.a(this.b, e);
            return false;
        }
    }
}
