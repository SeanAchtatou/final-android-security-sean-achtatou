package com.ironsource.mobilcore;

import android.content.Context;
import android.view.KeyEvent;
import android.widget.LinearLayout;

final class H extends LinearLayout {
    private a a;

    public interface a {
        boolean a();
    }

    public H(Context context, a aVar) {
        super(context);
        this.a = aVar;
    }

    public final boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() != 4 || this.a == null) {
            return super.dispatchKeyEvent(keyEvent);
        }
        return this.a.a();
    }
}
