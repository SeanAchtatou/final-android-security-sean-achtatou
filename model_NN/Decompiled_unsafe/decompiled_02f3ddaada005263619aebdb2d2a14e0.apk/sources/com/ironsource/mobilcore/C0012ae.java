package com.ironsource.mobilcore;

import android.content.Context;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;

/* renamed from: com.ironsource.mobilcore.ae  reason: case insensitive filesystem */
final class C0012ae {
    private static final float[] o = new float[101];
    private static float q = 8.0f;
    private static float r;
    private int a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private long h;
    private int i;
    private float j;
    private float k;
    private float l;
    private boolean m;
    private Interpolator n;
    private final float p;

    static {
        float f2;
        float f3;
        Math.log(0.75d);
        Math.log(0.9d);
        float f4 = 0.0f;
        int i2 = 0;
        while (i2 <= 100) {
            float f5 = ((float) i2) / 100.0f;
            float f6 = 1.0f;
            float f7 = f4;
            while (true) {
                f2 = ((f6 - f7) / 2.0f) + f7;
                f3 = 3.0f * f2 * (1.0f - f2);
                float f8 = ((((1.0f - f2) * 0.4f) + (0.6f * f2)) * f3) + (f2 * f2 * f2);
                if (((double) Math.abs(f8 - f5)) < 1.0E-5d) {
                    break;
                } else if (f8 > f5) {
                    f6 = f2;
                } else {
                    f7 = f2;
                }
            }
            o[i2] = (f2 * f2 * f2) + f3;
            i2++;
            f4 = f7;
        }
        o[100] = 1.0f;
        r = 1.0f;
        r = 1.0f / a(1.0f);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public C0012ae(Context context, Interpolator interpolator) {
        this(context, interpolator, context.getApplicationInfo().targetSdkVersion >= 11);
    }

    private C0012ae(Context context, Interpolator interpolator, boolean z) {
        this.m = true;
        this.n = interpolator;
        this.p = context.getResources().getDisplayMetrics().density * 160.0f;
        float scrollFriction = ViewConfiguration.getScrollFriction() * 386.0878f * this.p;
    }

    public final boolean a() {
        return this.m;
    }

    public final int b() {
        return this.f;
    }

    public final int c() {
        return this.d;
    }

    public final boolean d() {
        float interpolation;
        if (this.m) {
            return false;
        }
        int currentAnimationTimeMillis = (int) (AnimationUtils.currentAnimationTimeMillis() - this.h);
        if (currentAnimationTimeMillis < this.i) {
            switch (this.a) {
                case 0:
                    float f2 = ((float) currentAnimationTimeMillis) * this.j;
                    if (this.n == null) {
                        interpolation = a(f2);
                    } else {
                        interpolation = this.n.getInterpolation(f2);
                    }
                    this.f = this.b + Math.round(this.k * interpolation);
                    this.g = Math.round(interpolation * this.l) + this.c;
                    break;
                case 1:
                    float f3 = ((float) currentAnimationTimeMillis) / ((float) this.i);
                    int i2 = (int) (100.0f * f3);
                    float f4 = ((float) i2) / 100.0f;
                    float f5 = o[i2];
                    float f6 = (((f3 - f4) / ((((float) (i2 + 1)) / 100.0f) - f4)) * (o[i2 + 1] - f5)) + f5;
                    this.f = this.b + Math.round(((float) (this.d - this.b)) * f6);
                    this.f = Math.min(this.f, 0);
                    this.f = Math.max(this.f, 0);
                    this.g = Math.round(f6 * ((float) (this.e - this.c))) + this.c;
                    this.g = Math.min(this.g, 0);
                    this.g = Math.max(this.g, 0);
                    if (this.f == this.d && this.g == this.e) {
                        this.m = true;
                        break;
                    }
                case 2:
                    float interpolation2 = new BounceInterpolator().getInterpolation(((float) currentAnimationTimeMillis) * this.j);
                    this.f = this.b + Math.round(this.k * interpolation2);
                    this.g = Math.round(interpolation2 * this.l) + this.c;
                    break;
            }
        } else {
            this.f = this.d;
            this.g = this.e;
            this.m = true;
        }
        return true;
    }

    public final void a(int i2, int i3, int i4, int i5, int i6) {
        this.a = 0;
        this.m = false;
        this.i = i6;
        this.h = AnimationUtils.currentAnimationTimeMillis();
        this.b = i2;
        this.c = i3;
        this.d = i2 + i4;
        this.e = i3 + i5;
        this.k = (float) i4;
        this.l = (float) i5;
        this.j = 1.0f / ((float) this.i);
    }

    public final void b(int i2, int i3, int i4, int i5, int i6) {
        a(i2, 0, i4, 0, 1000);
        this.a = 2;
    }

    private static float a(float f2) {
        float exp;
        float f3 = q * f2;
        if (f3 < 1.0f) {
            exp = f3 - (1.0f - ((float) Math.exp((double) (-f3))));
        } else {
            exp = ((1.0f - ((float) Math.exp((double) (1.0f - f3)))) * 0.63212055f) + 0.36787945f;
        }
        return exp * r;
    }

    public final void e() {
        this.f = this.d;
        this.g = this.e;
        this.m = true;
    }
}
