package com.bckalz.android.quickaction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.bckalz.iphone5s.R;

public class NewQAAdapter extends BaseAdapter {
    private String[] data;
    private LayoutInflater mInflater;

    public NewQAAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }

    public void setData(String[] data2) {
        this.data = data2;
    }

    public int getCount() {
        return this.data.length;
    }

    public Object getItem(int item) {
        return this.data[item];
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.list, (ViewGroup) null);
            holder = new ViewHolder();
            holder.mTitleText = (TextView) convertView.findViewById(R.id.t_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.mTitleText.setText(this.data[position]);
        return convertView;
    }

    static class ViewHolder {
        TextView mTitleText;

        ViewHolder() {
        }
    }
}
