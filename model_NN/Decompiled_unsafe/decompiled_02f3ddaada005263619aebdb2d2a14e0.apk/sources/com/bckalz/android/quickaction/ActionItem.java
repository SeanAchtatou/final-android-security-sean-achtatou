package com.bckalz.android.quickaction;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public class ActionItem {
    private Drawable icon;
    private boolean selected;
    private Bitmap thumb;
    private String title;

    public ActionItem() {
    }

    public ActionItem(Drawable icon2) {
        this.icon = icon2;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setIcon(Drawable icon2) {
        this.icon = icon2;
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public void setSelected(boolean selected2) {
        this.selected = selected2;
    }

    public boolean isSelected() {
        return this.selected;
    }

    public void setThumb(Bitmap thumb2) {
        this.thumb = thumb2;
    }

    public Bitmap getThumb() {
        return this.thumb;
    }
}
