package com.bckalz.android.quickaction;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.bckalz.iphone5s.R;

public class QuickAction extends PopupWindows {
    public static final int ANIM_AUTO = 4;
    public static final int ANIM_GROW_FROM_CENTER = 3;
    public static final int ANIM_GROW_FROM_LEFT = 1;
    public static final int ANIM_GROW_FROM_RIGHT = 2;
    private int animStyle = 4;
    private boolean animateTrack = true;
    private LayoutInflater inflater;
    private ImageView mArrowDown;
    private ImageView mArrowUp;
    private int mChildPos = 0;
    /* access modifiers changed from: private */
    public OnActionItemClickListener mListener;
    private ViewGroup mTrack;
    private Animation mTrackAnim;

    public interface OnActionItemClickListener {
        void onItemClick(int i);
    }

    public QuickAction(Context context) {
        super(context);
        this.inflater = (LayoutInflater) context.getSystemService("layout_inflater");
        this.mTrackAnim = AnimationUtils.loadAnimation(context, R.anim.rail);
        this.mTrackAnim.setInterpolator(new Interpolator() {
            public float getInterpolation(float t) {
                float inner = (1.55f * t) - 1.1f;
                return 1.2f - (inner * inner);
            }
        });
        setRootViewId(R.layout.quickaction);
    }

    public void setRootViewId(int id) {
        this.mRootView = (ViewGroup) this.inflater.inflate(id, (ViewGroup) null);
        this.mTrack = (ViewGroup) this.mRootView.findViewById(R.id.tracks);
        this.mArrowDown = (ImageView) this.mRootView.findViewById(R.id.arrow_down);
        this.mArrowUp = (ImageView) this.mRootView.findViewById(R.id.arrow_up);
        setContentView(this.mRootView);
    }

    public void animateTrack(boolean animateTrack2) {
        this.animateTrack = animateTrack2;
    }

    public void setAnimStyle(int animStyle2) {
        this.animStyle = animStyle2;
    }

    public void addActionItem(ActionItem action) {
        String title = action.getTitle();
        Drawable icon = action.getIcon();
        View container = this.inflater.inflate((int) R.layout.action_item, (ViewGroup) null);
        ImageView img = (ImageView) container.findViewById(R.id.iv_icon);
        TextView text = (TextView) container.findViewById(R.id.tv_title);
        if (icon != null) {
            img.setImageDrawable(icon);
        } else {
            img.setVisibility(8);
        }
        if (title != null) {
            text.setText(title);
        } else {
            text.setVisibility(8);
        }
        final int pos = this.mChildPos;
        container.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (QuickAction.this.mListener != null) {
                    QuickAction.this.mListener.onItemClick(pos);
                }
                QuickAction.this.dismiss();
            }
        });
        container.setFocusable(true);
        container.setClickable(true);
        this.mTrack.addView(container, this.mChildPos + 1);
        this.mChildPos++;
    }

    public void setOnActionItemClickListener(OnActionItemClickListener listener) {
        this.mListener = listener;
    }

    public void show(View anchor) {
        preShow();
        int[] location = new int[2];
        anchor.getLocationOnScreen(location);
        Rect anchorRect = new Rect(location[0], location[1], location[0] + anchor.getWidth(), location[1] + anchor.getHeight());
        this.mRootView.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        this.mRootView.measure(-2, -2);
        int rootWidth = this.mRootView.getMeasuredWidth();
        int rootHeight = this.mRootView.getMeasuredHeight();
        int screenWidth = this.mWindowManager.getDefaultDisplay().getWidth();
        int xPos = (screenWidth - rootWidth) / 2;
        int yPos = anchorRect.top - rootHeight;
        boolean onTop = true;
        if (rootHeight > anchor.getTop()) {
            yPos = anchorRect.bottom;
            onTop = false;
        }
        showArrow(onTop ? R.id.arrow_down : R.id.arrow_up, anchorRect.centerX());
        setAnimationStyle(screenWidth, anchorRect.centerX(), onTop);
        this.mWindow.showAtLocation(anchor, 0, xPos, yPos);
        if (this.animateTrack) {
            this.mTrack.startAnimation(this.mTrackAnim);
        }
    }

    private void setAnimationStyle(int screenWidth, int requestedX, boolean onTop) {
        int i = R.style.Animations_PopUpMenu_Center;
        int i2 = R.style.Animations_PopUpMenu_Left;
        int arrowPos = requestedX - (this.mArrowUp.getMeasuredWidth() / 2);
        switch (this.animStyle) {
            case 1:
                PopupWindow popupWindow = this.mWindow;
                if (!onTop) {
                    i2 = 2131230741;
                }
                popupWindow.setAnimationStyle(i2);
                return;
            case 2:
                this.mWindow.setAnimationStyle(onTop ? R.style.Animations_PopUpMenu_Right : 2131230742);
                return;
            case 3:
                this.mWindow.setAnimationStyle(onTop ? 2131230747 : 2131230743);
                return;
            case 4:
                if (arrowPos <= screenWidth / 4) {
                    PopupWindow popupWindow2 = this.mWindow;
                    if (!onTop) {
                        i2 = 2131230741;
                    }
                    popupWindow2.setAnimationStyle(i2);
                    return;
                } else if (arrowPos <= screenWidth / 4 || arrowPos >= (screenWidth / 4) * 3) {
                    PopupWindow popupWindow3 = this.mWindow;
                    if (onTop) {
                    }
                    popupWindow3.setAnimationStyle(R.style.Animations_PopDownMenu_Right);
                    return;
                } else {
                    PopupWindow popupWindow4 = this.mWindow;
                    if (!onTop) {
                        i = 2131230743;
                    }
                    popupWindow4.setAnimationStyle(i);
                    return;
                }
            default:
                return;
        }
    }

    private void showArrow(int whichArrow, int requestedX) {
        View showArrow = whichArrow == R.id.arrow_up ? this.mArrowUp : this.mArrowDown;
        View hideArrow = whichArrow == R.id.arrow_up ? this.mArrowDown : this.mArrowUp;
        int arrowWidth = this.mArrowUp.getMeasuredWidth();
        showArrow.setVisibility(0);
        ((ViewGroup.MarginLayoutParams) showArrow.getLayoutParams()).leftMargin = requestedX - (arrowWidth / 2);
        hideArrow.setVisibility(4);
    }
}
