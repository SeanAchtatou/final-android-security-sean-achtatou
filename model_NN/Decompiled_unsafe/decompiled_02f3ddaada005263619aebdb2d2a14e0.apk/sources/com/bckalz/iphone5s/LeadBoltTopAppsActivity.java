package com.bckalz.iphone5s;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LeadBoltTopAppsActivity extends Activity {
    private WebView webView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.webview);
        this.webView = (WebView) findViewById(R.id.webview_compontent);
        this.webView.setWebViewClient(new MyWebViewClient(this, null));
        openURL();
    }

    private void openURL() {
        this.webView.loadUrl("http://ad.leadboltads.net/show_app_wall?section_id=676300483");
    }

    private class MyWebViewClient extends WebViewClient {
        private MyWebViewClient() {
        }

        /* synthetic */ MyWebViewClient(LeadBoltTopAppsActivity leadBoltTopAppsActivity, MyWebViewClient myWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
