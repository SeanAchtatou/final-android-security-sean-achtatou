package com.bckalz.iphone5s;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class AdmobBannerActivity extends Activity implements AdListener {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.banner_advert);
        AdView adView = new AdView(this, AdSize.IAB_MRECT, "a14fbab1296ccaa");
        ((LinearLayout) findViewById(R.id.linearLayout)).addView(adView);
        AdRequest request = new AdRequest();
        adView.setGravity(17);
        adView.loadAd(request);
    }

    public void onDismissScreen(Ad arg0) {
        Log.d("MY_LOG", "onDismissScreen");
    }

    public void onLeaveApplication(Ad arg0) {
        Log.d("MY_LOG", "onLeaveApplication");
    }

    public void onPresentScreen(Ad arg0) {
        Log.d("MY_LOG", "onPresentScreen");
    }

    public void onReceiveAd(Ad arg0) {
        Log.d("MY_LOG", "Did Receive Ad");
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode errorCode) {
        Log.d("MY_LOG", "failed to receive ad (" + errorCode + ")");
    }
}
