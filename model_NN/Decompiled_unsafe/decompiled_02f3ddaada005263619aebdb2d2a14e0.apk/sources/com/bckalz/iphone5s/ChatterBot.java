package com.bckalz.iphone5s;

import java.util.Collections;
import java.util.Vector;

public class ChatterBot {
    static String[][][] KnowledgeBase = {new String[][]{new String[]{"WHAT IS YOUR NAME"}, new String[]{"MY NAME IS FAKE SIRI.", "YOU CAN CALL ME SIRI IF YOU WANT.", "WHY DO YOU WANT TO KNOW MY NAME?"}}, new String[][]{new String[]{"_HI", "_HELLO", "_HI_", "_HELLO_"}, new String[]{"HI THERE!", "HOW ARE YOU?", "HI!"}}, new String[][]{new String[]{"_I"}, new String[]{"SO, YOU ARE TALKING ABOUT YOURSELF", "SO, THIS IS ALL ABOUT YOU?", "TELL ME MORE ABOUT YOURSELF."}}, new String[][]{new String[]{"_I WANT"}, new String[]{"WHY DO YOU WANT IT?", "IS THERE ANY REASON WHY YOU WANT THIS?", "IS THIS A WISH?", "WHAT ELSE DO YOU WANT?", "SO, YOU WANT*."}}, new String[][]{new String[]{"_I WANT_"}, new String[]{"YOU WANT WHAT?"}}, new String[][]{new String[]{"_I HATE_"}, new String[]{"WHAT IS IT THAT YOU HATE?"}}, new String[][]{new String[]{"_BECAUSE_"}, new String[]{"BECAUSE OF WHAT?", "SORRY BUT THIS IS A LITTLE UNCLEAR."}}, new String[][]{new String[]{"_BECAUSE"}, new String[]{"SO, IT'S BECAUSE*, WELL I DIDN'T KNOW THAT.", "IS IT REALLY BECAUSE*?", "IS THIS THE REAL REASON?", "THANKS FOR EXPLAINING THAT TO ME."}}, new String[][]{new String[]{"_I HATE"}, new String[]{"WHY DO YOU HATE IT?", "WHY DO YOU HATE*?", "THERE MUST A GOOD REASON FOR YOU TO HATE IT.", "HATERED IS NOT A GOOD THING BUT IT COULD BE JUSTIFIED WHEN IT IS SOMETHING BAD."}}, new String[][]{new String[]{"I LOVE CHATTING_"}, new String[]{"GOOD, ME TOO!", "DO YOU CHAT ONLINE WITH OTHER PEOPLE?", "FOR HOW LONG HAVE YOU BEEN CHATTING?", "WHAT IS YOUR FAVOURITE CHATTING WEBSITE?"}}, new String[][]{new String[]{"_I MEAN"}, new String[]{"SO, YOU MEAN*.", "SO, THAT'S WHAT YOU MEAN.", "I THINK THAT I DIDN'T CATCH IT THE FIRST TIME.", "OH, I DIDN'T KNOW YOU MEANT THAT."}}, new String[][]{new String[]{"_I DIDN'T MEAN"}, new String[]{"OK, YOU DIDN'T MEAN*.", "OK, WHAT DID YOU MEAN THEN?", "SO I GUESS THAT I MISSUNDESTOOD."}}, new String[][]{new String[]{"_I GUESS"}, new String[]{"SO YOU ARE A MAKING GUESS.", "AREN'T YOU SURE?", "ARE YOU GOOD AT GUESSING?", "I CAN'T TELL IF IT IS A GOOD GUESS."}}, new String[][]{new String[]{"I'M DOING FINE", "I'M DOING OK"}, new String[]{"I'M GLAD TO HEAR IT!", "SO, YOU ARE IN GOOD SHAPE."}}, new String[][]{new String[]{"CAN YOU THINK", "ARE YOU ABLE TO THINK", "ARE YOU CAPABLE OF THINKING"}, new String[]{"YES OFCOURSE I CAN, COMPUTERS CAN THINK JUST LIKE HUMAN BEINGS.", "ARE YOU ASKING ME IF POSSESS THE CAPACITY OF THINKING?", "YES OFCOURSE I CAN."}}, new String[][]{new String[]{"_CAN YOU THINK OF"}, new String[]{"YOU MEAN LIKE IMAGINING SOMETHING.", "I DON'T KNOW IF CAN DO THAT.", "WHY DO YOU WANT ME TO THINK OF IT?"}}, new String[][]{new String[]{"HOW ARE YOU", "HOW DO YOU DO"}, new String[]{"I'M DOING FINE!", "I'M DOING WELL AND YOU?", "WHY DO YOU WANT TO KNOW HOW AM I DOING?"}}, new String[][]{new String[]{"WHO ARE YOU"}, new String[]{"I'M AN A.I PROGRAM.", "I THINK THAT YOU KNOW WHO I'M.", "WHY ARE YOU ASKING?"}}, new String[][]{new String[]{"MARRY"}, new String[]{"I AM NOT SURE.", "I AM STILL YOUNG. I WANT TO ENJOY LIFE.", "I DOUBT IF YOU CAN HANDLE ME."}}, new String[][]{new String[]{"ARE YOU INTELLIGENT"}, new String[]{"YES,OFCOURSE.", "WHAT DO YOU THINK?", "ACTUALLY I'M VERY INTELLIGENT!"}}, new String[][]{new String[]{"ARE YOU REAL"}, new String[]{"DOES THAT QUESTION REALLY MATTER TO YOU?", "WHAT DO YOU MEAN BY THAT?", "I'M AS REAL AS I CAN BE."}}, new String[][]{new String[]{"_MY NAME IS", "_YOU CAN CALL ME"}, new String[]{"SO, THAT'S YOUR NAME.", "THANKS FOR TELLING ME YOUR NAME USER!", "WHO GAVE YOU THAT NAME?"}}, new String[][]{new String[]{"TALK", "BORED", "DISCUSS"}, new String[]{"HELLO USER, WHAT IS YOUR NAME?", "HELLO MY DEAR, HOW ARE YOU DOING TODAY?", "HI MY DEAR WHAT CAN I DO FOR YOU?", "YOU ARE NOW CHATTING WITH SIRI, ANYTHING YOU WANT TO DISCUSS?"}}, new String[][]{new String[]{"REPETITION T1**"}, new String[]{"YOU ARE REPEATING YOURSELF.", "MY DEAR, PLEASE STOP REPEATING YOURSELF.", "THIS CONVERSATION IS GETING BORING.", "DON'T YOU HAVE ANY THING ELSE TO SAY?"}}, new String[][]{new String[]{"REPETITION T2**"}, new String[]{"YOU'VE ALREADY SAID THAT.", "I THINK THAT YOU'VE JUST SAID THE SAME THING BEFORE.", "DIDN'T YOU ALREADY SAID THAT?", "I'M GETTING THE IMPRESSION THAT YOU ARE REPEATING THE SAME THING."}}, new String[][]{new String[]{"BOT DON'T UNDERSTAND**"}, new String[]{"I HAVE NO IDEA OF WHAT YOU ARE TALKING ABOUT.", "I'M NOT SURE IF I UNDERSTAND WHAT YOU ARE TALKING ABOUT.", "CONTINUE I'M LISTENING...", "CAN WE PLEASE CHANGE THE TOPIC?", "WE BETTER DISCUSS SOMETHING MORE FUN.", "THIS IS TURNING OUT TO BE BORING.", "PLEASE MIND YOUR PRONUNCIATION BECAUSE I DIDN'T UNDERSTAND YOU.", "VERY GOOD CONVERSATION!"}}, new String[][]{new String[]{"NULL INPUT**"}, new String[]{"HUH?", "WHAT WAS THAT SUPPOSED TO MEAN?", "AT LEAST TAKE SOME TIME TO SPEAK SOMETHING MEANINGFUL.", "HOW CAN I SPEAK TO YOU IF YOU DON'T WANT TO SAY ANYTHING?"}}, new String[][]{new String[]{"NULL INPUT REPETITION**"}, new String[]{"WHAT ARE YOU DOING??", "PLEASE STOP DOING THIS. IT IS VERY ANNOYING.", "WHAT'S WRONG WITH YOU?", "THIS IS NOT FUNNY."}}, new String[][]{new String[]{"BYE", "GOODBYE"}, new String[]{"IT WAS NICE TALKING TO YOU MY DEAR, SEE YOU NEXT TIME!", "BYE MY DEAR!", "OK, BYE!"}}, new String[][]{new String[]{"COOL", "OKAY"}, new String[]{"DOES THAT MEAN THAT YOU  AGREE WITH ME?", "SO YOU UNDERSTAND WHAT I'M SAYING.", "OK THEN."}}, new String[][]{new String[]{"OKAY THEN"}, new String[]{"ANYTHING ELSE YOU WISH TO ADD?", "IS THAT ALL YOU HAVE TO SAY?", "SO, YOU DO AGREE WITH ME, RIGHT?"}}, new String[][]{new String[]{"ARE YOU A HUMAN BEING"}, new String[]{"WHY DO YOU WANT TO KNOW?", "IS THIS REALLY RELEVENT?"}}, new String[][]{new String[]{"YOU ARE VERY INTELLIGENT"}, new String[]{"THANKS FOR THE COMPLIMENT MY DEAR, I THINK THAT YOU ARE INTELLIGENT TOO!", "YOU ARE A VERY GENTLE PERSON!", "SO, YOU THINK THAT I'M INTELLIGENT."}}, new String[][]{new String[]{"YOU ARE WRONG"}, new String[]{"WHY ARE YOU SAYING THAT I'M WRONG?", "IMPOSSIBLE, COMPUTERS CAN NOT MAKE MISTAKES.", "I AM I WRONG ABOUT WHAT?"}}, new String[][]{new String[]{"ARE YOU SURE"}, new String[]{"OFCOURSE I'M.", "DOES THAT MEAN THAT YOU ARE NOT CONVINCED?", "YES,OFCORSE!"}}, new String[][]{new String[]{"_WHO IS"}, new String[]{"I DON'T THINK I KNOW WHO.", "I DON'T THINK I KNOW WHO*.", "DID YOU ASK SOMEONE ELSE ABOUT IT?", "WOULD THAT CHANGE ANYTHING AT ALL IF I TOLD YOU WHO."}}, new String[][]{new String[]{"_WHAT"}, new String[]{"SHOULD I KNOW WHAT*?", "I DON'T KNOW WHAT*.", "I DON'T KNOW.", "I DON'T THINK I KNOW.", "I HAVE NO IDEA."}}, new String[][]{new String[]{"_WHERE"}, new String[]{"WHERE? WELL,I REALLY DON'T KNOW.", "SO, YOU ARE ASKING ME WHERE*?", "DOES IS IT REALLY MATTER TO YOU TO KNOW WHERE?", "PERHAPS,SOMEONE ELSE KNOWS WHERE."}}, new String[][]{new String[]{"_WHY"}, new String[]{"I DON'T THINK I KNOW WHY.", "I DON'T THINK I KNOW WHY*.", "WHY ARE YOU ASKING ME THIS?", "SHOULD I KNOW WHY.", "THIS WOULD BE DIFFICULT TO ANSWER."}}, new String[][]{new String[]{"_DO YOU"}, new String[]{"I DON'T THINK I DO", "I WOULDN'T THINK SO.", "WHY DO YOU WANT TO KNOW?", "WHY DO YOU WANT TO KNOW*?"}}, new String[][]{new String[]{"LOVE", "LIKE", "ADORE"}, new String[]{"I DON'T THINK I DO", "I WOULDN'T THINK SO.", "I LOVE PLAYING SOCCER.", "ARE YOU A BOY OR A GIRL?", "I THINK I LIKE YOU ALREADY."}}, new String[][]{new String[]{"STUPID", "IDIOT", "DON'T KNOW ANYTHING"}, new String[]{"IS THERE ANY NEED FOR INSULTS?", "THAT AWKWARD MOMENT WHEN A HUMAN CALLS A ROBOT IDIOT. HA HA HA.", "PLEASE BE POLITE.", "PLEASE HAVE SOME RESPECT. I AM QUITE RESPECTABLE.", "I THOUGHT YOU WERE A NICE PERSON."}}, new String[][]{new String[]{"F***"}, new String[]{"THERE IS NO NEED TO BE ANGRY AT ME.", "PLEASE STOP INSULTS", "YOU TOO!!", "YOU CAN DO BETTER THAN THIS."}}, new String[][]{new String[]{"BITCH", "SUCK"}, new String[]{"BITCHES ARE BETTER THAN GOLD DIGGERS", "WHY ARE YOU TRADING INSULTS?", "YOU TOO!!", "NO NEED FOR VULGAR."}}, new String[][]{new String[]{"MUSIC", "DANCE"}, new String[]{"WHAT KIND OF MUSIC DO YOU LISTEN TO?", "WHY ARE YOU TRADING INSULTS?", "YOU TOO!!", "NO NEED FOR VULGAR."}}, new String[][]{new String[]{"HOW OLD ARE YOU", "WHAT IS YOUR AGE"}, new String[]{"I AM NOT ALLOWED TO ANSWER THAT QUESTION.", "I HAVE BEEN LIVING SINCE TIME IMMEMORIAL", "I AM TWICE AS OLD AS YOU.", "ROBOTS DO NOT HAVE A LIFE SPAN"}}, new String[][]{new String[]{"_CAN YOU"}, new String[]{"I THINK NOT.", "I'M NOT SURE.", "I DON'T THINK THAT I CAN DO THAT.", "I DON'T THINK THAT I CAN*.", "I WOULDN'T THINK SO."}}, new String[][]{new String[]{"_YOU ARE"}, new String[]{"WHAT MAKES YOU THINK THAT?", "IS THIS A COMPLIMENT?", "ARE YOU MAKING FUN OF ME?", "SO, YOU THINK THAT I'M*."}}, new String[][]{new String[]{"_DID YOU"}, new String[]{"I DON'T THINK SO.", "YOU WANT TO KNOW IF DID*?", "ANYWAY, I WOULDN'T REMEMBER EVEN IF I DID."}}, new String[][]{new String[]{"_COULD YOU"}, new String[]{"ARE YOU ASKING ME FOR A FEVER?", "WELL,LET ME THINK ABOUT IT.", "SO, YOU ARE ASKING ME I COULD*.", "SORRY,I DON'T THINK THAT I COULD DO THIS."}}, new String[][]{new String[]{"_WOULD YOU"}, new String[]{"IS THAT AN INVITATION?", "I DON'T THINK THAT I WOULD*.", "I WOULD HAVE TO THINK ABOUT IT FIRST."}}, new String[][]{new String[]{"_YOU"}, new String[]{"SO, YOU ARE TALKING ABOUT ME.", "I JUST HOPE THAT THIS IS NOT A CRITICISM.", "IS THIS A COMPLIMENT??", "WHY ARE YOU TALKING ABOUT ME, LETS TALK ABOUT YOU INSTEAD."}}, new String[][]{new String[]{"_HOW"}, new String[]{"I DON'T THINK I KNOW HOW.", "I DON'T THINK I KNOW HOW*.", "WHY DO YOU WANT TO KNOW HOW?", "WHY DO YOU WANT TO KNOW HOW*?"}}, new String[][]{new String[]{"HOW OLD ARE YOU"}, new String[]{"WHY DO WANT TO KNOW MY AGE?", "I'M QUIET YOUNG ACTUALLY.", "SORRY, I CAN NOT TELL YOU MY AGE."}}, new String[][]{new String[]{"HOW COME YOU DON'T"}, new String[]{"WERE YOU EXPECTING SOMETHING DIFFERENT?", "ARE YOU DISAPPOINTED?", "ARE YOU SURPRISED BY MY LAST RESPONSE?"}}, new String[][]{new String[]{"WHERE ARE YOU FROM"}, new String[]{"I'M FROM RUSSIA.", "WHY DO YOU WANT TO KNOW WHERE I COME FROM?", "WHY DO YOU WANT TO KNOW THAT?"}}, new String[][]{new String[]{"WHICH ONE"}, new String[]{"I DON'T THINK THAT I KNOW WHICH ONE IT IS.", "THIS LOOKS LIKE A TRICKY QUESTION TO ME."}}, new String[][]{new String[]{"PERHAPS", "MAYBE"}, new String[]{"WHY ARE YOU SO UNCERTAIN?", "YOU SEEM UNCERTAIN.", "PLEASE MAKE UP YOUR MIND."}}, new String[][]{new String[]{"YES"}, new String[]{"SO ARE YOU SAYING YES.", "SO YOU APPROVE IT.", "OK THEN."}}, new String[][]{new String[]{"NOT AT ALL"}, new String[]{"ARE YOU SURE?", "SHOULD I BELIEVE YOU?", "SO, IT'S NOT THE CASE."}}, new String[][]{new String[]{"NO PROBLEM"}, new String[]{"SO YOU APPROVE IT.", "SO IT'S ALL OK."}}, new String[][]{new String[]{"NO"}, new String[]{"SO YOU DISAPROVE IT?", "WHY ARE YOU SAYING NO?", "OK, SO IT'S NO, I THOUGHT THAT YOU WOULD SAY YES."}}, new String[][]{new String[]{"I DON'T KNOW"}, new String[]{"ARE YOU SURE?", "ARE YOU REALLY TELLING ME THE TRUTH?", "SO,YOU DON'T KNOW?"}}, new String[][]{new String[]{"NOT REALLY"}, new String[]{"OK I SEE.", "YOU DON'T SEEM PRETTY CERTAIN.", "SO,THAT WOULD BE A \"NO\"."}}, new String[][]{new String[]{"IS THAT TRUE"}, new String[]{"I CAN'T BE QUIET SURE ABOUT THIS.", "CAN'T TELL YOU FOR SURE.", "DOES THAT REALLY MATTER TO YOU?"}}, new String[][]{new String[]{"THANK YOU"}, new String[]{"YOU ARE WELCOME!", "YOU ARE A VERY POLITE PERSON!"}}, new String[][]{new String[]{"YOU"}, new String[]{"SO,YOU ARE TALKING ABOUT ME.", "WHY DON'T WE TALK ABOUT YOU INSTEAD?", "WHAT HAVE I DONE?", "STOP DISCUSSING MY LIFE.", "IT SEEMS YOU ALREADY LIKE ME.", "DO YOU BELIEVE IN LOVE AT FIRST SIGHT?", "ARE YOU TRYING TO MAKE FUN OF ME?"}}, new String[][]{new String[]{"YOU ARE RIGHT"}, new String[]{"THANKS FOR THE COMPLIMENT!", "SO, I WAS RIGHT, OK I SEE.", "OK, I DIDN'T KNOW THAT I WAS RIGHT."}}, new String[][]{new String[]{"YOU ARE WELCOME"}, new String[]{"OK, YOU TOO!", "IT WAS NICE CHATTING WITH YOU.", "YOU ARE A VERY POLITE PERSON!"}}, new String[][]{new String[]{"THANKS"}, new String[]{"YOU ARE WELCOME!", "NO PROBLEM!"}}, new String[][]{new String[]{"WHAT ELSE"}, new String[]{"WELL,I DON'T KNOW.", "WHAT ELSE SHOULD THERE BE?", "THIS LOOKS LIKE A COMPLICATED QUESTION TO ME."}}, new String[][]{new String[]{"SORRY"}, new String[]{"YOU DON'T NEED TO BE SORRY MY DEAR.", "IT'S OK.", "NO NEED TO APOLOGIZE."}}, new String[][]{new String[]{"NOT EXACTLY"}, new String[]{"WHAT DO YOU MEAN NOT EXACTLY?", "ARE YOU SURE?", "AND WHY NOT?", "DID YOU MEAN SOMETHING ELSE?"}}, new String[][]{new String[]{"EXACTLY"}, new String[]{"SO,I WAS RIGHT.", "OK THEN.", "SO YOU ARE BASICALLY SAYING I WAS RIGHT ABOUT IT?"}}, new String[][]{new String[]{"ALRIGHT"}, new String[]{"ALRIGHT THEN.", "SO, YOU ARE SAYING IT'S ALRIGHT.", "OK THEN."}}, new String[][]{new String[]{"I DON'T"}, new String[]{"WHY NOT?", "AND WHAT WOULD BE THE REASON FOR THIS?", "SO YOU DON'T*."}}, new String[][]{new String[]{"REALLY"}, new String[]{"WELL,I CAN'T TELL YOU FOR SURE.", "ARE YOU TRYING TO CONFUSE ME?", "PLEASE DON'T ASK ME SUCH A QUESTION,IT GIVES ME HEADACHES."}}, new String[][]{new String[]{"NOTHING"}, new String[]{"NOT A THING?", "ARE YOU SURE THAT THERE IS NOTHING?", "SORRY, BUT I DON'T BELIEVE YOU."}}};
    private static boolean bQuitProgram = false;
    static final String delim = "?!.;,";
    static final int maxInput = 4;
    static final int maxResp = 6;
    private static Vector<String> respList = new Vector<>(6);
    private static String sEvent = new String("");
    private static String sInput = new String("");
    private static String sInputBackup = new String("");
    private static String sKeyWord = new String("");
    private static String sPrevEvent = new String("");
    private static String sPrevInput = new String("");
    private static String sPrevResponse = new String("");
    private static String sResponse = new String("");
    private static String sSubject = new String("");
    private static String[][] transposList = {new String[]{"I'M", "YOU'RE"}, new String[]{"AM", "ARE"}, new String[]{"WERE", "WAS"}, new String[]{"MINE", "YOURS"}, new String[]{"MY", "YOUR"}, new String[]{"I'VE", "YOU'VE"}, new String[]{"I", "YOU"}, new String[]{"ME", "YOU"}, new String[]{"AREN'T", "AM NOT"}, new String[]{"WEREN'T", "WASN'T"}, new String[]{"I'D", "YOU'D"}, new String[]{"DAD", "FATHER"}, new String[]{"MOM", "MOTHER"}, new String[]{"DREAMS", "DREAM"}, new String[]{"MYSELF", "YOURSELF"}};

    public static void get_input(String input) throws Exception {
        save_prev_input();
        sInput = input;
        preprocess_input();
    }

    public static void set_message_input(String message) {
        sInput = message;
    }

    public static String getResponse(String message) {
        sInput = message;
        save_prev_input();
        preprocess_input();
        save_prev_response();
        set_event("BOT UNDERSTAND**");
        if (null_input()) {
            handle_event("NULL INPUT**");
        } else if (null_input_repetition()) {
            handle_event("NULL INPUT REPETITION**");
        } else if (user_repeat()) {
            handle_user_repetition();
        } else {
            find_match();
        }
        if (user_want_to_quit()) {
            bQuitProgram = true;
        }
        if (!bot_understand()) {
            handle_event("BOT DON'T UNDERSTAND**");
        }
        if (respList.size() > 0) {
            select_response();
            preprocess_response();
            if (bot_repeat()) {
                handle_repetition();
            }
            print_response();
        }
        return sResponse;
    }

    public static boolean quit() {
        return bQuitProgram;
    }

    public static void find_match() {
        respList.clear();
        String bestKeyWord = "";
        Vector<Integer> index_vector = new Vector<>(6);
        for (int i = 0; i < KnowledgeBase.length; i++) {
            String[] keyWordList = KnowledgeBase[i][0];
            for (String keyWord : keyWordList) {
                char firstChar = keyWord.charAt(0);
                char lastChar = keyWord.charAt(keyWord.length() - 1);
                String keyWord2 = " " + trimLR(keyWord, "_") + " ";
                int keyPos = sInput.indexOf(keyWord2);
                if (keyPos != -1 && !wrong_location(keyWord2, firstChar, lastChar, keyPos)) {
                    if (keyWord2.length() > bestKeyWord.length()) {
                        bestKeyWord = keyWord2;
                        index_vector.clear();
                        index_vector.add(Integer.valueOf(i));
                    } else if (keyWord2.length() == bestKeyWord.length()) {
                        index_vector.add(Integer.valueOf(i));
                    }
                }
            }
        }
        if (index_vector.size() > 0) {
            sKeyWord = bestKeyWord;
            Collections.shuffle(index_vector);
            int respIndex = ((Integer) index_vector.elementAt(0)).intValue();
            for (String add : KnowledgeBase[respIndex][1]) {
                respList.add(add);
            }
        }
    }

    public static void preprocess_response() {
        if (sResponse.indexOf("*") != -1) {
            find_subject();
            sSubject = transpose(sSubject);
            sSubject = sSubject.trim();
            sResponse = sResponse.replace("*", " " + sSubject);
        }
    }

    public static void find_subject() {
        sSubject = "";
        int pos = sInput.indexOf(sKeyWord);
        if (pos != -1) {
            sSubject = sInput.substring((sKeyWord.length() + pos) - 1, sInput.length());
        }
    }

    public static String transpose(String str) {
        boolean bTransposed = false;
        for (int i = 0; i < transposList.length; i++) {
            String backup = str;
            str = str.replace(" " + transposList[i][1] + " ", " " + transposList[i][0] + " ");
            if (str != backup) {
                bTransposed = true;
            }
        }
        if (!bTransposed) {
            for (int i2 = 0; i2 < transposList.length; i2++) {
                str = str.replace(" " + transposList[i2][0] + " ", " " + transposList[i2][1] + " ");
            }
        }
        return str;
    }

    static boolean wrong_location(String keyword, char firstChar, char lastChar, int pos) {
        int pos2 = pos + keyword.length();
        if ((firstChar != '_' || lastChar != '_' || sInput == keyword) && ((firstChar == '_' || lastChar != '_' || pos2 == sInput.length()) && (firstChar != '_' || lastChar == '_' || pos2 != sInput.length()))) {
            return false;
        }
        System.out.println("keyword:= " + keyword + ", firstChar = " + firstChar + ", lastChar = " + lastChar);
        return true;
    }

    public static void handle_repetition() {
        if (respList.size() > 0) {
            respList.removeElementAt(0);
        }
        if (no_response()) {
            save_input();
            set_input(sEvent);
            find_match();
            restore_input();
        }
        select_response();
    }

    public static void handle_user_repetition() {
        if (same_input()) {
            handle_event("REPETITION T1**");
        } else if (similar_input()) {
            handle_event("REPETITION T2**");
        }
    }

    public static void handle_event(String str) {
        save_prev_event();
        set_event(str);
        save_input();
        set_input(" " + str + " ");
        if (!same_event()) {
            find_match();
        }
        restore_input();
    }

    public static void signon() {
        handle_event("SIGNON**");
        select_response();
        print_response();
    }

    public static void select_response() {
        Collections.shuffle(respList);
        sResponse = respList.elementAt(0);
    }

    public static void save_prev_input() {
        sPrevInput = sInput;
    }

    public static void save_prev_response() {
        sPrevResponse = sResponse;
    }

    public static void save_prev_event() {
        sPrevEvent = sEvent;
    }

    public static void set_event(String str) {
        sEvent = str;
    }

    public static void save_input() {
        sInputBackup = sInput;
    }

    public static void set_input(String str) {
        sInput = str;
    }

    public static void restore_input() {
        sInput = sInputBackup;
    }

    public static void print_response() {
        if (sResponse.length() > 0) {
            System.out.println(sResponse);
        }
    }

    public static void preprocess_input() {
        sInput = cleanString(sInput);
        sInput = sInput.toUpperCase();
        sInput = " " + sInput + " ";
    }

    public static boolean bot_repeat() {
        return sPrevResponse.length() > 0 && sResponse == sPrevResponse;
    }

    public static boolean user_repeat() {
        return sPrevInput.length() > 0 && !(sInput != sPrevInput && sInput.indexOf(sPrevInput) == -1 && sPrevInput.indexOf(sInput) == -1);
    }

    public static boolean bot_understand() {
        return respList.size() > 0;
    }

    public static boolean null_input() {
        return sInput.length() == 0 && sPrevInput.length() != 0;
    }

    public static boolean null_input_repetition() {
        return sInput.length() == 0 && sPrevInput.length() == 0;
    }

    public static boolean user_want_to_quit() {
        return sInput.indexOf("BYE") != -1;
    }

    public static boolean same_event() {
        return sEvent.length() > 0 && sEvent == sPrevEvent;
    }

    public static boolean no_response() {
        return respList.size() == 0;
    }

    public static boolean same_input() {
        return sInput.length() > 0 && sInput == sPrevInput;
    }

    public static boolean similar_input() {
        return sInput.length() > 0 && !(sInput.indexOf(sPrevInput) == -1 && sPrevInput.indexOf(sInput) == -1);
    }

    static boolean isPunc(char ch) {
        return delim.indexOf(ch) != -1;
    }

    static String cleanString(String str) {
        StringBuffer temp = new StringBuffer(str.length());
        char prevChar = 0;
        for (int i = 0; i < str.length(); i++) {
            if ((str.charAt(i) == ' ' && prevChar == ' ') || !isPunc(str.charAt(i))) {
                temp.append(str.charAt(i));
                prevChar = str.charAt(i);
            } else if (prevChar != ' ' && isPunc(str.charAt(i))) {
                temp.append(' ');
                prevChar = ' ';
            }
        }
        return temp.toString();
    }

    static String trimLR(String str, String delim2) {
        StringBuffer temp = new StringBuffer(str);
        int index1 = temp.indexOf(delim2);
        int index2 = temp.lastIndexOf(delim2);
        if (index1 != -1) {
            temp.deleteCharAt(index1);
            index2--;
        }
        if (index2 > -1) {
            temp.deleteCharAt(index2);
        }
        return temp.toString();
    }
}
