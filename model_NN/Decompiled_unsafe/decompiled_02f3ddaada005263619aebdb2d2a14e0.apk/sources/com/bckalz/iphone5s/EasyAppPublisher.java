package com.bckalz.iphone5s;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tapjoy.TapjoyAwardPointsNotifier;
import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyDisplayAdNotifier;
import com.tapjoy.TapjoyDisplayAdSize;
import com.tapjoy.TapjoyEarnedPointsNotifier;
import com.tapjoy.TapjoyFeaturedAppNotifier;
import com.tapjoy.TapjoyFeaturedAppObject;
import com.tapjoy.TapjoyLog;
import com.tapjoy.TapjoyNotifier;
import com.tapjoy.TapjoySpendPointsNotifier;
import com.tapjoy.TapjoyVideoNotifier;

public class EasyAppPublisher extends Activity implements View.OnClickListener, TapjoyNotifier, TapjoyFeaturedAppNotifier, TapjoySpendPointsNotifier, TapjoyDisplayAdNotifier, TapjoyAwardPointsNotifier, TapjoyEarnedPointsNotifier, TapjoyVideoNotifier {
    LinearLayout adLinearLayout;
    View adView;
    String currency_name;
    String displayText = "";
    boolean earnedPoints = false;
    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            EasyAppPublisher.this.updateResultsInUi();
        }
    };
    int point_total;
    TextView pointsTextView;
    RelativeLayout relativeLayout;
    TextView supportText;
    TextView tapjoySDKVersionView;
    boolean update_display_ad = false;
    boolean update_text = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.tapjoy);
        TapjoyLog.enableLogging(true);
        TapjoyConnect.requestTapjoyConnect(getApplicationContext(), "48a5522d-093c-4d17-9e98-f5e9f48b663f", "AGxvp7laIdgrG1MBlf0L");
        TapjoyConnect.getTapjoyConnectInstance().initVideoAd(this);
        TapjoyConnect.getTapjoyConnectInstance().setEarnedPointsNotifier(this);
        this.relativeLayout = (RelativeLayout) findViewById(R.id.RelativeLayout01);
        this.adLinearLayout = (LinearLayout) findViewById(R.id.AdLinearLayout);
        ((Button) findViewById(R.id.OffersButton)).setOnClickListener(this);
        ((Button) findViewById(R.id.GetFeaturedApp)).setOnClickListener(this);
        ((Button) findViewById(R.id.DisplayAd)).setOnClickListener(this);
        this.supportText = (TextView) findViewById(R.id.complete_offer);
        ((Button) findViewById(R.id.sign_up)).setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v instanceof Button) {
            switch (((Button) v).getId()) {
                case R.id.sign_up /*2131099687*/:
                    startActivity(new Intent(this, LeadBoltCaptureForm.class));
                    return;
                case R.id.OffersButton /*2131099688*/:
                    TapjoyConnect.getTapjoyConnectInstance().showOffers();
                    return;
                case R.id.GetFeaturedApp /*2131099689*/:
                    TapjoyConnect.getTapjoyConnectInstance().getFeaturedApp(this);
                    return;
                case R.id.DisplayAd /*2131099690*/:
                    TapjoyConnect.getTapjoyConnectInstance().enableBannerAdAutoRefresh(true);
                    TapjoyConnect.getTapjoyConnectInstance().setBannerAdSize(TapjoyDisplayAdSize.TJC_AD_BANNERSIZE_640X100);
                    TapjoyConnect.getTapjoyConnectInstance().getDisplayAd(this);
                    this.supportText.setVisibility(0);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        TapjoyConnect.getTapjoyConnectInstance().enableBannerAdAutoRefresh(false);
    }

    /* access modifiers changed from: private */
    public void updateResultsInUi() {
        if (this.update_display_ad) {
            this.adLinearLayout.removeAllViews();
            this.adLinearLayout.addView(this.adView);
            this.update_display_ad = false;
        }
        if (this.pointsTextView != null && this.update_text) {
            this.pointsTextView.setText(this.displayText);
            this.update_text = false;
        }
    }

    public void getUpdatePoints(String currencyName, int pointTotal) {
        Log.i("EASY_APP", "currencyName: " + currencyName);
        Log.i("EASY_APP", "pointTotal: " + pointTotal);
        this.currency_name = currencyName;
        this.point_total = pointTotal;
        this.update_text = true;
        if (this.earnedPoints) {
            this.displayText = String.valueOf(this.displayText) + "\n" + currencyName + ": " + pointTotal;
            this.earnedPoints = false;
        } else {
            this.displayText = String.valueOf(currencyName) + ": " + pointTotal;
        }
        this.mHandler.post(this.mUpdateResults);
    }

    public void getUpdatePointsFailed(String error) {
        Log.i("EASY_APP", "getTapPoints error: " + error);
        this.update_text = true;
        this.displayText = "Unable to retrieve tap points from server.";
        this.mHandler.post(this.mUpdateResults);
    }

    public void getFeaturedAppResponse(TapjoyFeaturedAppObject featuredApObject) {
        Log.i("EASY_APP", "Displaying Full Screen Ad..");
        TapjoyConnect.getTapjoyConnectInstance().showFeaturedAppFullScreenAd();
    }

    public void getFeaturedAppResponseFailed(String error) {
        Log.i("EASY_APP", "No Full Screen Ad to display: " + error);
        this.update_text = true;
        this.displayText = "No Full Screen Ad to display.";
        this.mHandler.post(this.mUpdateResults);
    }

    public void getSpendPointsResponse(String currencyName, int pointTotal) {
        Log.i("EASY_APP", "currencyName: " + currencyName);
        Log.i("EASY_APP", "pointTotal: " + pointTotal);
        this.update_text = true;
        this.displayText = String.valueOf(currencyName) + ": " + pointTotal;
        this.mHandler.post(this.mUpdateResults);
    }

    public void getSpendPointsResponseFailed(String error) {
        Log.i("EASY_APP", "spendTapPoints error: " + error);
        this.update_text = true;
        this.displayText = "Spend Tap Points: " + error;
        this.mHandler.post(this.mUpdateResults);
    }

    public void getDisplayAdResponse(View view) {
        this.adView = view;
        int ad_width = this.adView.getLayoutParams().width;
        int ad_height = this.adView.getLayoutParams().height;
        Log.i("EASY_APP", "adView dimensions: " + ad_width + "x" + ad_height);
        int desired_width = this.adLinearLayout.getMeasuredWidth();
        if (desired_width > ad_width) {
            desired_width = ad_width;
        }
        this.adView.setLayoutParams(new ViewGroup.LayoutParams(desired_width, (desired_width * ad_height) / ad_width));
        Log.i("EASY_APP", "adLinearLayout dimensions: " + this.adLinearLayout.getMeasuredWidth() + "x" + this.adLinearLayout.getMeasuredHeight());
        this.update_display_ad = true;
        this.mHandler.post(this.mUpdateResults);
    }

    public void getDisplayAdResponseFailed(String error) {
        Log.i("EASY_APP", "getDisplayAd error: " + error);
        this.update_text = true;
        this.displayText = "Banner Ads: " + error;
        this.mHandler.post(this.mUpdateResults);
    }

    public void getAwardPointsResponse(String currencyName, int pointTotal) {
        this.update_text = true;
        this.displayText = String.valueOf(currencyName) + ": " + pointTotal;
        this.mHandler.post(this.mUpdateResults);
    }

    public void getAwardPointsResponseFailed(String error) {
        this.update_text = true;
        this.displayText = "Award Points: " + error;
        this.mHandler.post(this.mUpdateResults);
    }

    public void earnedTapPoints(int amount) {
        this.earnedPoints = true;
        this.update_text = true;
        this.displayText = "You've just earned " + amount + " Tap Points!";
        this.mHandler.post(this.mUpdateResults);
    }

    public void videoReady() {
        Log.i("EASY_APP", "VIDEO READY");
        this.update_text = true;
        this.displayText = "VIDEO READY!";
        this.mHandler.post(this.mUpdateResults);
    }

    public void videoError(int statusCode) {
        Log.i("EASY_APP", "VIDEO ERROR: " + statusCode);
        switch (statusCode) {
            case 1:
                this.displayText = "VIDEO ERROR: No SD card or external media storage mounted on device";
                break;
            case 2:
                this.displayText = "VIDEO ERROR: Network error on init videos";
                break;
            case 3:
                this.displayText = "VIDEO ERROR: Error playing video";
                break;
        }
        this.update_text = true;
        this.mHandler.post(this.mUpdateResults);
    }

    public void videoComplete() {
        Log.i("EASY_APP", "VIDEO COMPLETE");
    }
}
