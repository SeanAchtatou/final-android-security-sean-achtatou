package com.bckalz.iphone5s;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class AddAdapter extends BaseAdapter {
    public static final int ITEM_BILLIARDS = 1;
    public static final int ITEM_DRUM_KIT = 3;
    public static final int ITEM_FAKE_IPAD = 0;
    public static final int ITEM_FAKE_NOKIA = 2;
    public static final int ITEM_FUNNY_ACCENTS = 5;
    public static final int ITEM_TORCH = 4;
    public static final int ITEM_WHIP_APP = 6;
    private final LayoutInflater mInflater;
    private final ArrayList<ListItem> mItems = new ArrayList<>();

    public class ListItem {
        public final int actionTag;
        public final Drawable image;
        public final CharSequence text;

        public ListItem(Resources res, int textResourceId, int imageResourceId, int actionTag2) {
            this.text = res.getString(textResourceId);
            if (imageResourceId != -1) {
                this.image = res.getDrawable(imageResourceId);
            } else {
                this.image = null;
            }
            this.actionTag = actionTag2;
        }
    }

    public AddAdapter(ShowAppsDialogActivity launcher) {
        this.mInflater = (LayoutInflater) launcher.getSystemService("layout_inflater");
        Resources res = launcher.getResources();
        this.mItems.add(new ListItem(res, R.string.fake_ipad, R.drawable.ic_ipad, 0));
        this.mItems.add(new ListItem(res, R.string.shoot_billiards, R.drawable.ic_billiards, 1));
        this.mItems.add(new ListItem(res, R.string.fake_nokia, R.drawable.ic_nokia, 2));
        this.mItems.add(new ListItem(res, R.string.drum_kit, R.drawable.ic_drum, 3));
        this.mItems.add(new ListItem(res, R.string.bright_torch, R.drawable.ic_light_bulb, 4));
        this.mItems.add(new ListItem(res, R.string.funny_accents, R.drawable.ic_accents, 5));
        this.mItems.add(new ListItem(res, R.string.whip_app, R.drawable.ic_whip, 6));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        ListItem item = (ListItem) getItem(position);
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.add_list_item, parent, false);
        }
        TextView textView = (TextView) convertView;
        textView.setTag(item);
        textView.setText(item.text);
        textView.setCompoundDrawablesWithIntrinsicBounds(item.image, (Drawable) null, (Drawable) null, (Drawable) null);
        return convertView;
    }

    public int getCount() {
        return this.mItems.size();
    }

    public Object getItem(int position) {
        return this.mItems.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }
}
