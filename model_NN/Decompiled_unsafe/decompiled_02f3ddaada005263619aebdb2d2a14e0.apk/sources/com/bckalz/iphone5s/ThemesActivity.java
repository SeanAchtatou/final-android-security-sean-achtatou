package com.bckalz.iphone5s;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class ThemesActivity extends Activity {
    Button btnSetTheme;
    /* access modifiers changed from: private */
    public int selectedImage;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.themes);
        this.btnSetTheme = (Button) findViewById(R.id.btnTheme);
        Gallery gallery = (Gallery) findViewById(R.id.gallery);
        gallery.setAdapter((SpinnerAdapter) new ImageAdapter(this));
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Toast.makeText(ThemesActivity.this, new StringBuilder().append(position).toString(), 0).show();
                ThemesActivity.this.selectedImage = position;
            }
        });
        this.btnSetTheme.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ThemesActivity.this.createIntent(ThemesActivity.this.selectedImage);
            }
        });
    }

    /* access modifiers changed from: private */
    public void createIntent(int result) {
        Bundle bundle = new Bundle();
        bundle.putInt("CATEGORY", result);
        Intent intent = new Intent(getApplicationContext(), ViewPagerActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, 0);
    }

    class ImageAdapter extends BaseAdapter {
        private Context mContext;
        int mGalleryItemBackground;
        private Integer[] mImageIds = {Integer.valueOf((int) R.drawable.background_1), Integer.valueOf((int) R.drawable.background_2), Integer.valueOf((int) R.drawable.background_3), Integer.valueOf((int) R.drawable.background_4), Integer.valueOf((int) R.drawable.background_5), Integer.valueOf((int) R.drawable.background_6)};

        public ImageAdapter(Context c) {
            this.mContext = c;
            TypedArray attr = this.mContext.obtainStyledAttributes(R.styleable.HelloGallery);
            this.mGalleryItemBackground = attr.getResourceId(0, 0);
            attr.recycle();
        }

        public int getCount() {
            return this.mImageIds.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView = new ImageView(this.mContext);
            imageView.setImageResource(this.mImageIds[position].intValue());
            imageView.setLayoutParams(new Gallery.LayoutParams(300, 200));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setBackgroundResource(this.mGalleryItemBackground);
            return imageView;
        }
    }
}
