package com.bckalz.iphone5s;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.telephony.TelephonyManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InstructionsActivity extends Activity {
    Button back;
    ImageView iconBattery;
    ImageView iconNetwork;
    TextView netWorkName;
    TextView time;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.instructions);
        getWindow().setFeatureInt(7, R.layout.window_title);
        getWindow().setFlags(AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END, AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END);
        this.iconNetwork = (ImageView) findViewById(R.id.icon_network);
        this.iconBattery = (ImageView) findViewById(R.id.icon_battery);
        this.netWorkName = (TextView) findViewById(R.id.network_title);
        this.time = (TextView) findViewById(R.id.time_text);
        this.time.setText(getPresentTime());
        this.netWorkName.setText(getPhoneServiceProvider());
        this.iconBattery.setImageResource(R.drawable.battery_green_100);
        this.iconNetwork.setImageResource(R.drawable.signal_blue_100);
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
    }

    private CharSequence getPhoneServiceProvider() {
        return ((TelephonyManager) getSystemService("phone")).getNetworkOperatorName();
    }

    private CharSequence getPresentTime() {
        return new SimpleDateFormat("hh:mm a").format(new Date());
    }
}
