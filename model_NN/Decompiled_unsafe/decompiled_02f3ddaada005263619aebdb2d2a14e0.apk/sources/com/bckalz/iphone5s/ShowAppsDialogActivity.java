package com.bckalz.iphone5s;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class ShowAppsDialogActivity extends Activity {
    Button btnHotApps;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.show_app);
        LinearLayout layout = (LinearLayout) findViewById(R.id.linearLayout);
        AdView adView = new AdView(this, AdSize.IAB_MRECT, "a14fbab1296ccaa");
        layout.addView(adView);
        AdRequest request = new AdRequest();
        adView.setGravity(17);
        adView.loadAd(request);
        final AddAdapter mAdapter = new AddAdapter(this);
        showHottestAppsDialog(mAdapter);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        this.btnHotApps = new Button(this);
        this.btnHotApps.setText("Hottest Apps");
        this.btnHotApps.setLayoutParams(layoutParams);
        layout.addView(this.btnHotApps);
        this.btnHotApps.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowAppsDialogActivity.this.showHottestAppsDialog(mAdapter);
            }
        });
    }

    /* access modifiers changed from: private */
    public void showHottestAppsDialog(AddAdapter mAdapter) {
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setIcon((int) R.drawable.ic_tray_collapse);
        alt_bld.setTitle((int) R.string.menu_item_add_item);
        alt_bld.setAdapter(mAdapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    ShowAppsDialogActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.fakeipad3")));
                }
                if (item == 1) {
                    ShowAppsDialogActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.shootbilliardballs")));
                }
                if (item == 2) {
                    ShowAppsDialogActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.fakenokian9")));
                }
                if (item == 3) {
                    ShowAppsDialogActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.fingerdrum")));
                }
                if (item == 4) {
                    ShowAppsDialogActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.flashlight")));
                }
                if (item == 5) {
                    ShowAppsDialogActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.funnyenglishaccents")));
                }
                if (item == 6) {
                    ShowAppsDialogActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.whipthem")));
                }
            }
        });
        alt_bld.create().show();
    }
}
