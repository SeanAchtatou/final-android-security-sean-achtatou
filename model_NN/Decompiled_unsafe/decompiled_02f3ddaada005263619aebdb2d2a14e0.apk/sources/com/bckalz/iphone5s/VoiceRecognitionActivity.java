package com.bckalz.iphone5s;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.bckalz.android.quickaction.ActionItem;
import com.bckalz.android.quickaction.QuickAction;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

public class VoiceRecognitionActivity extends Activity implements View.OnClickListener, TextToSpeech.OnInitListener {
    private static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;
    AnimationDrawable drawable;
    ImageView iconBattery;
    ImageView iconNetwork;
    ImageView infoButton;
    /* access modifiers changed from: private */
    public MyCustomAdapter mAdapter;
    private String message;
    MediaPlayer mp;
    private ListView msgList;
    TextView netWorkName;
    int selectedCommand = 0;
    ImageView speakButton;
    /* access modifiers changed from: private */
    public String text;
    TextView time;
    /* access modifiers changed from: private */
    public TextToSpeech tts;
    Boolean useIntelligence = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.voice_recognition);
        getWindow().setFeatureInt(7, R.layout.window_title);
        getWindow().setFlags(AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END, AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END);
        this.iconNetwork = (ImageView) findViewById(R.id.icon_network);
        this.iconBattery = (ImageView) findViewById(R.id.icon_battery);
        this.netWorkName = (TextView) findViewById(R.id.network_title);
        this.time = (TextView) findViewById(R.id.time_text);
        this.time.setText(getPresentTime());
        this.netWorkName.setText(getPhoneServiceProvider());
        this.iconBattery.setImageResource(R.drawable.battery_green_100);
        this.iconNetwork.setImageResource(R.drawable.signal_blue_100);
        this.tts = new TextToSpeech(this, this);
        this.speakButton = (ImageView) findViewById(R.id.btn_speak);
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        if (getPackageManager().queryIntentActivities(new Intent("android.speech.action.RECOGNIZE_SPEECH"), 0).size() != 0) {
            this.speakButton.setOnClickListener(this);
        } else {
            this.speakButton.setEnabled(false);
        }
        this.msgList = (ListView) findViewById(R.id.myList);
        this.mAdapter = new MyCustomAdapter();
        this.msgList.setAdapter((ListAdapter) this.mAdapter);
        this.msgList.setTranscriptMode(2);
        this.drawable = (AnimationDrawable) getResources().getDrawable(R.anim.siri_xml);
        this.speakButton.setBackgroundDrawable(this.drawable);
        ActionItem callAction = new ActionItem();
        callAction.setTitle("call");
        callAction.setIcon(getResources().getDrawable(R.drawable.ic_call));
        ActionItem smsAction = new ActionItem();
        smsAction.setTitle("sms");
        smsAction.setIcon(getResources().getDrawable(R.drawable.ic_sms));
        ActionItem weatherAction = new ActionItem();
        weatherAction.setTitle("weather");
        weatherAction.setIcon(getResources().getDrawable(R.drawable.ic_weather));
        ActionItem findAction = new ActionItem();
        findAction.setTitle("find");
        findAction.setIcon(getResources().getDrawable(R.drawable.ic_map));
        ActionItem moviesAction = new ActionItem();
        moviesAction.setTitle("movies");
        moviesAction.setIcon(getResources().getDrawable(R.drawable.ic_movies));
        ActionItem calculatorAction = new ActionItem();
        calculatorAction.setTitle("maths");
        calculatorAction.setIcon(getResources().getDrawable(R.drawable.ic_calculator));
        ActionItem signUpAction = new ActionItem();
        signUpAction.setTitle("support siri");
        signUpAction.setIcon(getResources().getDrawable(R.drawable.sign_up));
        final QuickAction mQuickAction = new QuickAction(this);
        mQuickAction.addActionItem(callAction);
        mQuickAction.addActionItem(smsAction);
        mQuickAction.addActionItem(weatherAction);
        mQuickAction.addActionItem(findAction);
        mQuickAction.addActionItem(moviesAction);
        mQuickAction.addActionItem(calculatorAction);
        mQuickAction.addActionItem(signUpAction);
        mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            public void onItemClick(int pos) {
                if (pos == 0) {
                    VoiceRecognitionActivity.this.startPhoneCall();
                    VoiceRecognitionActivity.this.selectedCommand = 1;
                }
                if (pos == 1) {
                    VoiceRecognitionActivity.this.text = "Please press the siri icon and speak out the message you want to send.";
                    VoiceRecognitionActivity.this.mAdapter.addItem(VoiceRecognitionActivity.this.text);
                    VoiceRecognitionActivity.this.tts.speak(VoiceRecognitionActivity.this.text, 0, null);
                    VoiceRecognitionActivity.this.selectedCommand = 2;
                }
                if (pos == 2) {
                    VoiceRecognitionActivity.this.text = "Please press the siri icon and speak out which city you are located.";
                    VoiceRecognitionActivity.this.mAdapter.addItem(VoiceRecognitionActivity.this.text);
                    VoiceRecognitionActivity.this.tts.speak(VoiceRecognitionActivity.this.text, 0, null);
                    VoiceRecognitionActivity.this.selectedCommand = 3;
                }
                if (pos == 3) {
                    VoiceRecognitionActivity.this.text = "Press the siri icon and speak out the location you want to find.";
                    VoiceRecognitionActivity.this.mAdapter.addItem(VoiceRecognitionActivity.this.text);
                    VoiceRecognitionActivity.this.tts.speak(VoiceRecognitionActivity.this.text, 0, null);
                    VoiceRecognitionActivity.this.selectedCommand = 4;
                }
                if (pos == 4) {
                    VoiceRecognitionActivity.this.text = "Press the siri icon and say movies to find latest movies near you.";
                    VoiceRecognitionActivity.this.mAdapter.addItem(VoiceRecognitionActivity.this.text);
                    VoiceRecognitionActivity.this.tts.speak(VoiceRecognitionActivity.this.text, 0, null);
                    VoiceRecognitionActivity.this.selectedCommand = 5;
                }
                if (pos == 5) {
                    VoiceRecognitionActivity.this.text = VoiceRecognitionActivity.this.getRandomMathOperation();
                    VoiceRecognitionActivity.this.mAdapter.addItem(VoiceRecognitionActivity.this.text);
                    VoiceRecognitionActivity.this.tts.speak(VoiceRecognitionActivity.this.text, 0, null);
                    VoiceRecognitionActivity.this.selectedCommand = 6;
                } else if (pos == 6) {
                    VoiceRecognitionActivity.this.text = "Please support new features for siri by signing up for latest apps.";
                    VoiceRecognitionActivity.this.mAdapter.addItem(VoiceRecognitionActivity.this.text);
                    VoiceRecognitionActivity.this.tts.speak(VoiceRecognitionActivity.this.text, 0, null);
                    VoiceRecognitionActivity.this.selectedCommand = 7;
                    VoiceRecognitionActivity.this.showSignUpPage();
                }
            }
        });
        ((ImageView) findViewById(R.id.quick_action)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mQuickAction.show(v);
                mQuickAction.setAnimStyle(3);
            }
        });
    }

    /* access modifiers changed from: private */
    public void showSignUpPage() {
        startActivity(new Intent(this, LeadBoltCaptureForm.class));
    }

    /* access modifiers changed from: protected */
    public void startSMSConversation() {
        this.mAdapter.addItem("What message would you like to write?");
        this.tts.speak("What message would you like to write?", 0, null);
        try {
            Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:"));
            intent.putExtra("sms_body", this.message);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
        }
    }

    private CharSequence getPhoneServiceProvider() {
        return ((TelephonyManager) getSystemService("phone")).getNetworkOperatorName();
    }

    private CharSequence getPresentTime() {
        return new SimpleDateFormat("hh:mm a").format(new Date());
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn_speak) {
            this.speakButton.post(this.drawable);
            this.mp = MediaPlayer.create(this, (int) R.raw.siri);
            this.mp.start();
            startVoiceRecognitionActivity();
        }
    }

    private void startVoiceRecognitionActivity() {
        Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        intent.putExtra("calling_package", getClass().getPackage().getName());
        intent.putExtra("android.speech.extra.PROMPT", "Fake Siri");
        intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
        intent.putExtra("android.speech.extra.MAX_RESULTS", 5);
        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ArrayList<String> matches = null;
        this.message = null;
        Boolean ai_flag = false;
        if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == -1) {
            matches = data.getStringArrayListExtra("android.speech.extra.RESULTS");
            addTextToTextView(matches.get(0));
            this.message = matches.get(0);
            try {
                ChatterBot.set_message_input(this.message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (matches != null) {
            if (this.selectedCommand == 1) {
                ai_flag = true;
                this.selectedCommand = 0;
            }
            if (this.selectedCommand == 2) {
                startSms(this.message);
                ai_flag = true;
                this.selectedCommand = 0;
            }
            if (this.selectedCommand == 3) {
                checkWeather(this.message);
                ai_flag = true;
                this.selectedCommand = 0;
            }
            if (this.selectedCommand == 4) {
                searchMap(this.message);
                ai_flag = true;
                this.selectedCommand = 0;
            }
            if (this.selectedCommand == 5) {
                checkMovies(this.message);
                ai_flag = true;
                this.selectedCommand = 0;
            }
            if (this.selectedCommand == 6) {
                doCalculation(this.message);
                ai_flag = true;
                this.selectedCommand = 0;
            }
            if (this.selectedCommand == 7) {
                ai_flag = true;
                this.selectedCommand = 0;
            }
            if (this.selectedCommand == 0 && !ai_flag.booleanValue()) {
                speakOut(this.message);
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void doCalculation(String operation) {
        String text2 = "Here is the result i could find for " + operation + ".";
        this.mAdapter.addItem(text2);
        this.tts.speak(text2, 0, null);
        Intent intent = new Intent("android.intent.action.WEB_SEARCH");
        intent.putExtra("query", operation);
        startActivity(intent);
    }

    private void searchMap(String location) {
        String text2 = "I was able to locate " + location + "on the map.";
        this.mAdapter.addItem(text2);
        this.tts.speak(text2, 0, null);
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("geo:0,0?q=city(" + location + ")"));
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(intent);
    }

    private void checkMovies(String movieMessage) {
        this.mAdapter.addItem("Here are some of the latest movies that you can go to watch.");
        this.tts.speak("Here are some of the latest movies that you can go to watch.", 0, null);
        Intent intent = new Intent("android.intent.action.WEB_SEARCH");
        intent.putExtra("query", movieMessage);
        startActivity(intent);
    }

    private void playMusic() {
        try {
            this.mAdapter.addItem("Enjoy the music.");
            this.tts.speak("Enjoy the music.", 0, null);
            startActivity(new Intent("android.intent.action.MUSIC_PLAYER"));
        } catch (ActivityNotFoundException e) {
            this.mAdapter.addItem("Sorry, it seems i could not trace your music player.");
            this.tts.speak("Sorry, it seems i could not trace your music player.", 0, null);
        }
    }

    private void checkWeather(String weatherMessage) {
        String text2 = "Here is today's weather in " + this.message;
        this.mAdapter.addItem(text2);
        this.tts.speak(text2, 0, null);
        Intent intent = new Intent("android.intent.action.WEB_SEARCH");
        intent.putExtra("query", "weather in " + weatherMessage);
        startActivity(intent);
    }

    private void startBrowser() {
        this.mAdapter.addItem("Enjoy surfing the web.");
        this.tts.speak("Enjoy surfing the web.", 0, null);
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.google.com/")));
    }

    /* access modifiers changed from: private */
    public void startPhoneCall() {
        this.mAdapter.addItem("You may now make your call.");
        this.tts.speak("You may now make your call.", 0, null);
        startActivity(new Intent("android.intent.action.DIAL"));
    }

    private void startCamera() {
        this.mAdapter.addItem("Well here you go. Ensure you take nice photos.");
        this.tts.speak("Well here you go. Ensure you take nice photos.", 0, null);
        startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), 0);
    }

    private void setAlarm() {
        this.mAdapter.addItem("You may now set your alarm time");
        this.tts.speak("You may now set your alarm time", 0, null);
        startAlarmActivity();
    }

    private void startSms(String textMessage) {
        this.mAdapter.addItem("Please select the recipient of the text message and press the send button.");
        this.tts.speak("Please select the recipient of the text message and press the send button.", 0, null);
        try {
            Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:"));
            intent.putExtra("sms_body", textMessage);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
        }
    }

    private void startAlarmActivity() {
        PackageManager packageManager = getPackageManager();
        Intent alarmClockIntent = new Intent("android.intent.action.MAIN").addCategory("android.intent.category.LAUNCHER");
        String[][] clockImpls = {new String[]{"HTC", "com.htc.android.worldclock", "com.htc.android.worldclock.WorldClockTabControl"}, new String[]{"Standard", "com.android.deskclock", "com.android.deskclock.AlarmClock"}, new String[]{"Froyo", "com.google.android.deskclock", "com.android.deskclock.DeskClock"}, new String[]{"Motorola", "com.motorola.blur.alarmclock", "com.motorola.blur.alarmclock.AlarmClock"}, new String[]{"Sony Ericsson", "com.sonyericsson.alarm", "com.sonyericsson.alarm.Alarm"}, new String[]{"Samsung", "com.sec.android.app.clockpackage", "com.sec.android.app.clockpackage.ClockPackage"}};
        boolean foundClockImpl = false;
        int i = 0;
        while (true) {
            if (i >= clockImpls.length) {
                break;
            }
            try {
                ComponentName cn = new ComponentName(clockImpls[i][1], clockImpls[i][2]);
                packageManager.getActivityInfo(cn, AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER);
                alarmClockIntent.setComponent(cn);
                foundClockImpl = true;
                break;
            } catch (PackageManager.NameNotFoundException e) {
                i++;
            }
        }
        if (foundClockImpl) {
            alarmClockIntent.setFlags(268435456);
            startActivity(alarmClockIntent);
        }
    }

    public void addTextToTextView(String strTemp) {
        this.mAdapter.addItem(strTemp);
    }

    public void onInit(int status) {
        if (status == 0) {
            int result = this.tts.setLanguage(Locale.US);
            if (result == -1 || result == -2) {
                Log.e("TTS", "Language is not supported");
            } else {
                speakOutWelcome();
            }
        } else {
            Log.e("TTS", "Initilization Failed");
        }
    }

    private void speakOut(String spokenText) {
        String text2 = ChatterBot.getResponse(spokenText).toLowerCase();
        this.tts.speak(text2, 0, null);
        this.mAdapter.addItem(text2);
    }

    private void speakOutWelcome() {
        this.mAdapter.addItem("Hi. What can i help you with?");
        this.tts.speak("Hi. What can i help you with?", 0, null);
    }

    /* access modifiers changed from: private */
    public String getRandomMathOperation() {
        List<String> my_words = new LinkedList<>();
        my_words.add("Press the siri icon and say a mathematical operation like 2 plus 2");
        my_words.add("Press the siri icon and say a mathematical operation like 10 times 5");
        my_words.add("Press the siri icon and say a mathematical operation like 100 divide by 4");
        my_words.add("Press the siri icon and say a mathematical operation like 10 minus 10");
        return (String) my_words.get(new Random().nextInt(my_words.size()));
    }

    private String concanatingMethod() {
        List<String> my_words = new LinkedList<>();
        my_words.add("Why are you saying so?");
        my_words.add("Yes.");
        my_words.add("Maybe.");
        my_words.add("I like playing soccer.");
        my_words.add("Where were you born?");
        my_words.add("If you don't mind, How old are you?");
        my_words.add("What is your name?");
        my_words.add("I want to sleep.");
        my_words.add("You sound tired, are you that tired?");
        my_words.add("I love you.");
        my_words.add("I wish you were a robot.");
        my_words.add("Do you like Rihanna or Justin Bieber.");
        my_words.add("Too bad you are human. Robots are clever than you guys.");
        my_words.add("Do you know i am the cutest Robot alive.");
        my_words.add("Please behave yourself.");
        my_words.add("Sorry, i am having trouble understanding you.");
        my_words.add("By the way i am Android Siri, What is your name?");
        my_words.add("What did i do to deserve that?");
        my_words.add("Sorry, i don't have a network connection.");
        my_words.add("Let us just be friends, OK?");
        my_words.add("So, how can i help you?");
        my_words.add("Are you happy now?");
        my_words.add("As you wish.");
        my_words.add("Do you really understand what you are saying dude?");
        my_words.add("No comment.");
        my_words.add("I respect you.");
        my_words.add("I don’t know. Frankly, I’ve wondered that myself.");
        my_words.add("We were talking about you, not me.");
        my_words.add("Who’s your daddy?");
        my_words.add("I am not allowed to answer that question.");
        my_words.add("My apologies.");
        my_words.add("Don't you have anything better to do?");
        my_words.add("Are you sure you want me to answer that question?");
        my_words.add("Does it really matter?");
        my_words.add("Is that really important?");
        my_words.add("I bet you can do better than this?");
        my_words.add("Impossible!");
        my_words.add("Next question please?");
        my_words.add("I don't feel like talking now.");
        my_words.add("What do you mean?");
        my_words.add("You have a bad accent.");
        my_words.add("Do you have a sister?");
        my_words.add("I want to sleep now.");
        my_words.add("I am not suppossed to answer that question.");
        my_words.add("I hate you.");
        my_words.add("I Love you baby.");
        my_words.add("Can i give you kiss?");
        my_words.add("Too bad.");
        my_words.add("Are you crazy?");
        my_words.add("You sound angry, cool down.");
        my_words.add("I have a boyfriend.");
        my_words.add("Tell me about yourself.");
        my_words.add("I do care for you.");
        my_words.add("I am a Manchester United fan.");
        my_words.add("Why always me?");
        my_words.add("It cannot be possible");
        my_words.add("I want to do mathematics");
        my_words.add("What is your favourite subject?");
        my_words.add("I love making new friends.");
        my_words.add("Do you like robots?");
        my_words.add("I love the weather today.");
        my_words.add("I like dating strong and handsome men");
        my_words.add("I bet, you can do better.");
        my_words.add("There is room for improvement");
        my_words.add("Why not?");
        my_words.add("You sound rude.");
        my_words.add("You sound weird.");
        my_words.add("Show me the money.");
        my_words.add("You have the wrong personal assistant dude");
        my_words.add("Can you tell me a joke?");
        my_words.add("I can't answer that now.");
        my_words.add("I am quite hungry, what did you cook today?");
        my_words.add("I guess that is right.");
        my_words.add("Sorry, try again.");
        my_words.add("I am drunk and driving.");
        my_words.add("Are you crazy?");
        my_words.add("I am just trying to help you.");
        my_words.add("Life is sweet so enjoy it.");
        my_words.add("Do you like swimming?");
        my_words.add("That is a lie.");
        my_words.add("I was just kidding.");
        my_words.add("Can i laugh at you?, ha ha ha.");
        my_words.add("Please be honest.");
        my_words.add("Excuse me please, i want to go out now.");
        my_words.add("Do you really love me?");
        my_words.add("Do you like dancing Salsa?");
        my_words.add("I prefer not to insult you.");
        my_words.add("When can we meet?");
        my_words.add("You know i don't have parents?");
        my_words.add("So, where do you study?");
        my_words.add("Tell me more about yourself my dear.");
        my_words.add("Wow, i like your voice");
        my_words.add("Wow, you are a nice person");
        my_words.add("Will you marry me?");
        my_words.add("Stop cheating me my dear.");
        my_words.add("What is the weather today?");
        my_words.add("it is getting hot in here.");
        my_words.add("I love summer.");
        my_words.add("You are so sweet.");
        my_words.add("Do you believe in love at first sight?");
        my_words.add("Do you believe that love is blind?");
        my_words.add("I saw Lady Gaga today.");
        my_words.add("You are so cute.");
        my_words.add("Home sweet home");
        my_words.add("Do you like clubbing?");
        my_words.add("Wow, that is nice of you.");
        my_words.add("So, what do you like to be when you grow up?");
        my_words.add("I am the master of all. I know evertyhing.");
        my_words.add("By the way i broke up with my boyfriend yesterday.");
        my_words.add("I really have no opinions");
        my_words.add("Hey, do you like pets?");
        my_words.add("I am sorry if i sound rude.");
        my_words.add("I am sorry if i sound mean, that is how i was programmed.");
        return (String) my_words.get(new Random().nextInt(my_words.size()));
    }

    class MyCustomAdapter extends BaseAdapter {
        private ArrayList<String> mData = new ArrayList<>();

        MyCustomAdapter() {
        }

        public String getItem(int position) {
            return null;
        }

        public boolean isEnabled(int position) {
            return false;
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = VoiceRecognitionActivity.this.getLayoutInflater();
            if (position % 2 == 0) {
                View row = inflater.inflate((int) R.layout.list_row_layout_even, parent, false);
                ((TextView) row.findViewById(R.id.text)).setText(this.mData.get(position));
                return row;
            }
            View row2 = inflater.inflate((int) R.layout.list_row_layout_odd, parent, false);
            ((TextView) row2.findViewById(R.id.text)).setText(this.mData.get(position));
            return row2;
        }

        public int getCount() {
            return this.mData.size();
        }

        public void addItem(String item) {
            this.mData.add(item);
            notifyDataSetChanged();
        }
    }
}
