package com.bckalz.iphone5s;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import com.Leadbolt.AdController;

public class LeadBoltCaptureForm extends Activity {
    /* access modifiers changed from: private */
    public AdController myController;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout layout = new LinearLayout(this);
        setContentView(layout);
        layout.post(new Runnable() {
            private String MY_LB_SECTION_ID = "688245730";

            public void run() {
                LeadBoltCaptureForm.this.myController = new AdController(this, this.MY_LB_SECTION_ID);
                LeadBoltCaptureForm.this.myController.loadAd();
            }
        });
    }

    public void onDestroy() {
        this.myController.destroyAd();
        super.onDestroy();
    }
}
