package com.bckalz.iphone5s;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.Leadbolt.AdController;

public class LeadboltAdvancedOverlays extends Activity {
    /* access modifiers changed from: private */
    public AdController myController;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout layout = new LinearLayout(this);
        TextView txt = new TextView(this);
        txt.setText("Advertisements from our sponsors support the future development of this app. So please check out some of the offers from our sponsors. Thank you all for your support!!");
        txt.setTextSize(20.0f);
        txt.setTextColor(-16777216);
        setContentView(layout);
        layout.setBackgroundResource(R.drawable.background_1);
        layout.addView(txt);
        layout.post(new Runnable() {
            private String MY_LB_SECTION_ID = "688245730";

            public void run() {
                LeadboltAdvancedOverlays.this.myController = new AdController(this, this.MY_LB_SECTION_ID);
                LeadboltAdvancedOverlays.this.myController.loadAd();
            }
        });
    }

    public void onDestroy() {
        this.myController.destroyAd();
        super.onDestroy();
    }
}
