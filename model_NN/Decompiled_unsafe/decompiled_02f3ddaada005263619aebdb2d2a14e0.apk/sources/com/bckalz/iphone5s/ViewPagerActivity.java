package com.bckalz.iphone5s;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.apperhand.device.android.AndroidSDKProvider;
import com.applovin.sdk.AppLovinSdk;
import com.ironsource.mobilcore.CallbackResponse;
import com.ironsource.mobilcore.MobileCore;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ViewPagerActivity extends Activity {
    int batteryLevel;
    private CallbackResponse callbackResponse;
    Context context;
    private int defaultBackground = 0;
    ImageView iconBattery;
    ImageView iconNetwork;
    LinearLayout linearLayout;
    MyPagerAdapter mAdapter;
    ViewPager mPager;
    TextView netWorkName;
    private SharedPreferences preferences = null;
    int selectedResult;
    TextView time;
    View v;
    View view;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.main);
        AppLovinSdk.initializeSdk(this);
        AndroidSDKProvider.initSDK(this);
        MobileCore.init(this, "7WTNGCO0PEBBY8PQPHDD2V5QHOEU3", MobileCore.LOG_TYPE.PRODUCTION);
        MobileCore.showOfferWall(this, this.callbackResponse);
        getWindow().setFeatureInt(7, R.layout.window_title);
        getWindow().setFlags(AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END, AccessibilityEventCompat.TYPE_TOUCH_EXPLORATION_GESTURE_END);
        this.iconNetwork = (ImageView) findViewById(R.id.icon_network);
        this.iconBattery = (ImageView) findViewById(R.id.icon_battery);
        this.netWorkName = (TextView) findViewById(R.id.network_title);
        this.iconBattery.setImageResource(R.drawable.battery_green_100);
        this.time = (TextView) findViewById(R.id.time_text);
        this.time.setText(getPresentTime());
        this.netWorkName.setText(getPhoneServiceProvider());
        this.iconNetwork.setImageResource(R.drawable.signal_blue_100);
        Resources res = getResources();
        this.preferences = getSharedPreferences("Share", 0);
        if (savedInstanceState != null) {
            this.selectedResult = this.preferences.getInt("SELECTED_BACKGROUND", 0);
            setBackgroundResource(res);
        } else {
            this.selectedResult = getCategory(this.v);
        }
        setBackgroundResource(res);
        this.mAdapter = new MyPagerAdapter(this, null);
        this.mPager = (ViewPager) findViewById(R.id.pager);
        this.mPager.setAdapter(this.mAdapter);
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(this.mPager);
        indicator.setRadius(4.0f * getResources().getDisplayMetrics().density);
    }

    private CharSequence getPhoneServiceProvider() {
        String carrierName = ((TelephonyManager) getSystemService("phone")).getNetworkOperatorName();
        if (carrierName.length() > 12) {
            return carrierName.subSequence(0, 13);
        }
        return carrierName;
    }

    private CharSequence getPresentTime() {
        return new SimpleDateFormat("hh:mm a").format(new Date());
    }

    private int getCategory(View v2) {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return this.defaultBackground;
        }
        return bundle.getInt("CATEGORY");
    }

    private void setBackgroundResource(Resources res) {
        Drawable drawable = res.getDrawable(getBackGround(this.selectedResult));
        this.linearLayout = (LinearLayout) findViewById(R.id.main_menu_layout);
        this.linearLayout.setBackgroundDrawable(drawable);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putInt("SELECTED_BACKGROUND", this.selectedResult);
        editor.commit();
        super.onPause();
    }

    private int getBackGround(int result) {
        if (result == 0) {
            return R.drawable.background_1;
        }
        if (result == 1) {
            return R.drawable.background_2;
        }
        if (result == 2) {
            return R.drawable.background_3;
        }
        if (result == 3) {
            return R.drawable.background_4;
        }
        if (result == 4) {
            return R.drawable.background_5;
        }
        if (result == 5) {
            return R.drawable.background_6;
        }
        return this.defaultBackground;
    }

    public void onMessagesClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        try {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setType("vnd.android-dir/mms-sms");
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(this, AdmobBannerActivity.class));
        }
    }

    public void onContactsClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        Intent i = new Intent("android.intent.action.PICK");
        i.setType("vnd.android.cursor.dir/contact");
        startActivity(i);
    }

    public void onCalenderClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        try {
            Intent i = new Intent();
            new ComponentName("com.google.android.calendar", "com.android.calendar.LaunchActivity");
            i.setComponent(new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity"));
            startActivity(i);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(this, AdmobBannerActivity.class));
        }
    }

    public void onSafariClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.apple.com/")));
    }

    public void onClockClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startAlarmActivity();
    }

    public void onCameraClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), 0);
    }

    public void onGoogleTalkClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        try {
            Intent i = new Intent("android.intent.action.MAIN");
            i.setComponent(new ComponentName("com.google.android.talk", "com.google.android.talk.SigningInActivity"));
            startActivity(i);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(this, AdmobBannerActivity.class));
        }
    }

    public void onPandoraClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltBannerSlot.class));
    }

    public void onYelpClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltBannerActivity.class));
    }

    public void onPulseClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltCaptureForm.class));
    }

    public void onEvernoteClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadboltAdvert.class));
    }

    public void onAirHornClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.airhorn")));
    }

    public void onSettingsClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        Intent intent = new Intent("android.settings.SETTINGS");
        intent.setFlags(1073741824);
        startActivity(intent);
    }

    public void onMapsClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?saddr=20.344,34.34&daddr=20.5666,45.345")));
    }

    public void onFacebookClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        try {
            Intent intent = new Intent("android.intent.category.LAUNCHER");
            intent.setClassName("com.facebook.katana", "com.facebook.katana.LoginActivity");
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.facebook.com/")));
        }
    }

    public void onSearchClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.google.com/")));
    }

    public void onHomeClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        Intent startMain = new Intent("android.intent.action.MAIN");
        startMain.addCategory("android.intent.category.HOME");
        startMain.setFlags(268435456);
        startActivity(startMain);
    }

    public void onPhoneClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        try {
            startActivity(new Intent("android.intent.action.DIAL"));
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(this, AdmobBannerActivity.class));
        }
    }

    public void onVideosClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        try {
            Intent intent = new Intent("android.intent.action.MEDIA_SEARCH");
            startActivity(intent);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(this, LeadBoltBannerSlot.class));
        }
    }

    public void onTwitterClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://m.twitter.com")));
    }

    public void onSkypeClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        if (isPackageExists("com.skype.raider")) {
            Intent skype = getPackageManager().getLaunchIntentForPackage("com.skype.raider");
            skype.setData(Uri.parse("tel:65465446"));
            startActivity(skype);
            return;
        }
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://skype.com")));
    }

    public void onStocksClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadboltSplashActivity.class));
    }

    public void onWeatherClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, AdmobBannerActivity.class));
    }

    public void onYoutubeClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("vnd.youtube:1FJHYqE0RDg")));
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://m.youtube.com")));
        }
    }

    public void onCalculatorClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        try {
            Intent i = new Intent();
            i.setAction("android.intent.action.MAIN");
            i.addCategory("android.intent.category.LAUNCHER");
            i.setFlags(270532608);
            i.setComponent(new ComponentName("com.android.calculator2", "com.android.calculator2.Calculator"));
            startActivity(i);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(this, AdmobBannerActivity.class));
        }
    }

    public void onMailClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        try {
            Intent mailClient = new Intent("android.intent.action.VIEW");
            mailClient.setClassName("com.google.android.gm", "com.google.android.gm.ConversationListActivity");
            startActivity(mailClient);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(this, AdmobBannerActivity.class));
        }
    }

    public void onThemesClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, ThemesActivity.class));
    }

    public void onPhotosClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        Intent intentBrowseFiles = new Intent("android.intent.action.GET_CONTENT");
        intentBrowseFiles.setType("image/*");
        startActivity(intentBrowseFiles);
    }

    public void onItunesClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("audio/*");
        startActivity(Intent.createChooser(intent, "Select music"));
    }

    public void onVoiceMemosClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        try {
            startActivityForResult(new Intent("android.provider.MediaStore.RECORD_SOUND"), 1);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(this, LeadBoltBannerActivity.class));
        }
    }

    public void onFaceTimeClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadboltAdvert.class));
    }

    public void onNotesClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadboltAdvert.class));
    }

    public void onRemaindersClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltTopAppsActivity.class));
    }

    public void onCydiaClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, AdmobBannerActivity.class));
    }

    public void onGraphClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltTopAppsActivity.class));
    }

    public void oniCloudClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, AdmobBannerActivity.class));
    }

    public void onNewsClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, AdmobBannerActivity.class));
    }

    public void onStockClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltTopAppsActivity.class));
    }

    public void onKindleClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltBannerActivity.class));
    }

    public void onAppstoreClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltBannerSlot.class));
    }

    public void onGamesClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltBannerActivity.class));
    }

    public void onIpodClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, AdmobBannerActivity.class));
    }

    public void onDoodleJumpClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltTopAppsActivity.class));
    }

    public void onAngryBirdsClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadboltAdvancedOverlays.class));
    }

    public void onMusicClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltTopAppsActivity.class));
    }

    public void onFourSquareClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltBannerSlot.class));
    }

    public void onGoogleClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadboltAdvancedOverlays.class));
    }

    public void onEbayClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://m.ebay.com")));
    }

    public void onCompassClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, AdmobBannerActivity.class));
    }

    public void onShareClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        shareApp();
    }

    public void onDownloadsClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, ShowAppsDialogActivity.class));
    }

    public void onRateClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        Toast toast = Toast.makeText(this, "Thank you for your support!", 2000);
        toast.setGravity(80, -30, 50);
        toast.show();
        rateUs();
    }

    public void onSiriClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, VoiceRecognitionActivity.class));
    }

    public void oniBooksClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, AdmobBannerActivity.class));
    }

    public void onGarageBandClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltBannerSlot.class));
    }

    public void onFlixterClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltTopAppsActivity.class));
    }

    public void oniPodClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, AdmobBannerActivity.class));
    }

    public void onPhotoBoothClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, AdmobBannerActivity.class));
    }

    public void onSupportClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, EasyAppPublisher.class));
    }

    public void onAccentsClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.funnyenglishaccents")));
    }

    private void startAlarmActivity() {
        PackageManager packageManager = getPackageManager();
        Intent alarmClockIntent = new Intent("android.intent.action.MAIN").addCategory("android.intent.category.LAUNCHER");
        String[][] clockImpls = {new String[]{"HTC", "com.htc.android.worldclock", "com.htc.android.worldclock.WorldClockTabControl"}, new String[]{"Standard", "com.android.deskclock", "com.android.deskclock.AlarmClock"}, new String[]{"Froyo", "com.google.android.deskclock", "com.android.deskclock.DeskClock"}, new String[]{"Motorola", "com.motorola.blur.alarmclock", "com.motorola.blur.alarmclock.AlarmClock"}, new String[]{"Sony Ericsson", "com.sonyericsson.alarm", "com.sonyericsson.alarm.Alarm"}, new String[]{"Samsung", "com.sec.android.app.clockpackage", "com.sec.android.app.clockpackage.ClockPackage"}};
        boolean foundClockImpl = false;
        int i = 0;
        while (true) {
            if (i >= clockImpls.length) {
                break;
            }
            try {
                ComponentName cn = new ComponentName(clockImpls[i][1], clockImpls[i][2]);
                packageManager.getActivityInfo(cn, AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER);
                alarmClockIntent.setComponent(cn);
                foundClockImpl = true;
                break;
            } catch (PackageManager.NameNotFoundException e) {
                i++;
            }
        }
        if (foundClockImpl) {
            alarmClockIntent.setFlags(268435456);
            startActivity(alarmClockIntent);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unbindDrawables(findViewById(R.id.main_menu_layout));
        System.gc();
    }

    private void unbindDrawables(View view2) {
        if (view2.getBackground() != null) {
            view2.getBackground().setCallback(null);
        }
        if (view2 instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view2).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view2).getChildAt(i));
            }
            ((ViewGroup) view2).removeAllViews();
        }
    }

    private void shareApp() {
        Intent shareIntent = new Intent("android.intent.action.SEND");
        shareIntent.setType("text/plain");
        shareIntent.putExtra("android.intent.extra.SUBJECT", new String((String) getText(R.string.menu_share_subject)));
        shareIntent.putExtra("android.intent.extra.TEXT", new String((String) getText(R.string.menu_share_body)));
        startActivity(Intent.createChooser(shareIntent, getText(R.string.menu_share_intent)));
    }

    public void onShazamClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        try {
            Intent shazamIntent = new Intent("com.shazam.android");
            shazamIntent.setComponent(new ComponentName("com.shazam.android", "com.shazam.android.Splash"));
            startActivity(shazamIntent);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(this, AdmobBannerActivity.class));
        }
    }

    public void onEbuddyClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, LeadBoltTopAppsActivity.class));
    }

    public void onInstagramClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, AdmobBannerActivity.class));
    }

    public void onBilliardClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.shootbilliardballs")));
    }

    public void on4SquareClick(View v2) {
        v2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.image_click));
        startActivity(new Intent(this, AdmobBannerActivity.class));
    }

    private void rateUs() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.iphone4s")));
    }

    public void onDrumsClick(View v2) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://market.android.com/details?id=com.mobisoft.android.fingerdrum")));
    }

    public boolean isPackageExists(String targetPackage) {
        for (ApplicationInfo packageInfo : getPackageManager().getInstalledApplications(0)) {
            if (packageInfo.packageName.equals(targetPackage)) {
                return true;
            }
        }
        return false;
    }

    private class MyPagerAdapter extends PagerAdapter {
        private MyPagerAdapter() {
        }

        /* synthetic */ MyPagerAdapter(ViewPagerActivity viewPagerActivity, MyPagerAdapter myPagerAdapter) {
            this();
        }

        public int getCount() {
            return 4;
        }

        public Object instantiateItem(View collection, int position) {
            LayoutInflater inflater = (LayoutInflater) collection.getContext().getSystemService("layout_inflater");
            int resId = 0;
            switch (position) {
                case 0:
                    resId = R.layout.layout_view_1;
                    break;
                case 1:
                    resId = R.layout.layout_view_2;
                    break;
                case 2:
                    resId = R.layout.layout_view_3;
                    break;
                case 3:
                    resId = R.layout.layout_view_4;
                    break;
            }
            View view = inflater.inflate(resId, (ViewGroup) null);
            ((ViewPager) collection).addView(view, 0);
            return view;
        }

        public void destroyItem(View arg0, int arg1, Object arg2) {
            ((ViewPager) arg0).removeView((View) arg2);
        }

        public void finishUpdate(View arg0) {
        }

        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == ((View) arg1);
        }

        public void restoreState(Parcelable arg0, ClassLoader arg1) {
        }

        public Parcelable saveState() {
            return null;
        }

        public void startUpdate(View arg0) {
        }
    }

    public void onBackPressed() {
        MobileCore.showOfferWall(this, new CallbackResponse() {
            public void onConfirmation(CallbackResponse.TYPE arg0) {
                ViewPagerActivity.this.finish();
            }
        });
    }
}
