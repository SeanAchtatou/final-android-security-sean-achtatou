package com.google.ads;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.WebView;
import java.util.HashMap;

public final class ab implements j {

    public enum b {
        AD("ad"),
        APP("app");
        
        public String c;

        private b(String str) {
            this.c = str;
        }
    }

    private static class c implements DialogInterface.OnClickListener {
        private d a;

        public c(d dVar) {
            this.a = dVar;
        }

        public final void onClick(DialogInterface dialogInterface, int i) {
            HashMap hashMap = new HashMap();
            hashMap.put(AdActivity.URL_PARAM, "market://details?id=com.google.android.apps.plus");
            AdActivity.launchAdActivity(this.a, new e("intent", hashMap));
        }
    }

    private static class a implements DialogInterface.OnClickListener {
        public final void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("a");
        if (str != null) {
            if (str.equals("resize")) {
                a.a(webView, "(G_resizeIframe(" + hashMap.get(AdActivity.URL_PARAM) + "))");
                return;
            } else if (str.equals("state")) {
                z.a(dVar.e(), webView, hashMap.get(AdActivity.URL_PARAM));
                return;
            }
        }
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.google.android.apps.plus", "com.google.android.apps.circles.platform.PlusOneActivity"));
        Activity e = dVar.e();
        if (e == null) {
            com.google.ads.util.a.e("Activity was null when responding to +1 action");
        } else if (aa.a(intent, e.getApplicationContext())) {
            AdActivity.launchAdActivity(dVar, new e("plusone", hashMap));
        } else if (!aa.a(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.google.android.apps.plus")), e.getApplicationContext())) {
        } else {
            if (TextUtils.isEmpty(hashMap.get("d")) || TextUtils.isEmpty(hashMap.get(AdActivity.ORIENTATION_PARAM)) || TextUtils.isEmpty(hashMap.get("c"))) {
                HashMap hashMap2 = new HashMap();
                hashMap2.put(AdActivity.URL_PARAM, "market://details?id=com.google.android.apps.plus");
                AdActivity.launchAdActivity(dVar, new e("intent", hashMap2));
                return;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(e);
            builder.setMessage(hashMap.get("d")).setPositiveButton(hashMap.get(AdActivity.ORIENTATION_PARAM), new c(dVar)).setNegativeButton(hashMap.get("c"), new a());
            builder.create().show();
        }
    }
}
