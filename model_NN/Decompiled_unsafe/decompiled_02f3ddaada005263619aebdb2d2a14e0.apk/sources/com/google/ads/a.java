package com.google.ads;

import android.net.Uri;
import android.webkit.WebView;
import com.google.ads.util.AdUtil;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public final class a {
    public static final Map<String, j> a;
    public static final Map<String, j> b;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("/invalidRequest", new p());
        hashMap.put("/loadAdURL", new q());
        a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("/open", new t());
        hashMap2.put("/canOpenURLs", new k());
        hashMap2.put("/close", new m());
        hashMap2.put("/evalInOpener", new n());
        hashMap2.put("/log", new s());
        hashMap2.put("/click", new l());
        hashMap2.put("/httpTrack", new o());
        hashMap2.put("/touch", new u());
        hashMap2.put("/video", new v());
        hashMap2.put("/plusOne", new ab());
        b = Collections.unmodifiableMap(hashMap2);
    }

    private a() {
    }

    static void a(d dVar, Map<String, j> map, Uri uri, WebView webView) {
        String str;
        HashMap<String, String> b2 = AdUtil.b(uri);
        if (b2 == null) {
            com.google.ads.util.a.e("An error occurred while parsing the message parameters.");
            return;
        }
        if (c(uri)) {
            String host = uri.getHost();
            if (host == null) {
                com.google.ads.util.a.e("An error occurred while parsing the AMSG parameters.");
                str = null;
            } else if (host.equals("launch")) {
                b2.put("a", "intent");
                b2.put(AdActivity.URL_PARAM, b2.get("url"));
                b2.remove("url");
                str = "/open";
            } else if (host.equals("closecanvas")) {
                str = "/close";
            } else if (host.equals("log")) {
                str = "/log";
            } else {
                com.google.ads.util.a.e("An error occurred while parsing the AMSG: " + uri.toString());
                str = null;
            }
        } else if (b(uri)) {
            str = uri.getPath();
        } else {
            com.google.ads.util.a.e("Message was neither a GMSG nor an AMSG.");
            str = null;
        }
        if (str == null) {
            com.google.ads.util.a.e("An error occurred while parsing the message.");
            return;
        }
        j jVar = map.get(str);
        if (jVar == null) {
            com.google.ads.util.a.e("No AdResponse found, <message: " + str + ">");
        } else {
            jVar.a(dVar, b2, webView);
        }
    }

    public static boolean a(Uri uri) {
        if (uri == null || !uri.isHierarchical()) {
            return false;
        }
        if (b(uri) || c(uri)) {
            return true;
        }
        return false;
    }

    private static boolean b(Uri uri) {
        String authority;
        String scheme = uri.getScheme();
        if (scheme == null || !scheme.equals("gmsg") || (authority = uri.getAuthority()) == null || !authority.equals("mobileads.google.com")) {
            return false;
        }
        return true;
    }

    private static boolean c(Uri uri) {
        String scheme = uri.getScheme();
        if (scheme == null || !scheme.equals("admob")) {
            return false;
        }
        return true;
    }

    public static void a(WebView webView, String str, String str2) {
        if (str2 != null) {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "', " + str2 + ");");
        } else {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "');");
        }
    }

    public static void a(WebView webView, String str) {
        com.google.ads.util.a.d("Sending JS to a WebView: " + str);
        webView.loadUrl("javascript:" + str);
    }

    public static void a(WebView webView, Map<String, Boolean> map) {
        a(webView, "openableURLs", new JSONObject(map).toString());
    }

    public static void a(WebView webView) {
        a(webView, "onshow", "{'version': 'afma-sdk-a-v4.3.1'}");
    }

    public static void b(WebView webView) {
        a(webView, "onhide", null);
    }
}
