package com.Leadbolt;

public interface AdListener {
    void onAdClicked();

    void onAdClosed();

    void onAdCompleted();

    void onAdFailed();

    void onAdLoaded();

    void onAdProgress();
}
