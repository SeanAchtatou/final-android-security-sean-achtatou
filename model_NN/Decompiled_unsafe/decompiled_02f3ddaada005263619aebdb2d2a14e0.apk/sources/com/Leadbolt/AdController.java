package com.Leadbolt;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.provider.Browser;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.TimerTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

public class AdController {
    public static final String LB_LOG = "LBAdController";
    private static final String SDK_LEVEL = "01";
    private static final String SDK_VERSION = "3";
    private final int LB_MAX_POLL;
    private final int LB_SET_MANUAL_AFTER;
    private final int MAX_APP_ICONS;
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public boolean adDestroyed;
    private String adDisplayInterval;
    /* access modifiers changed from: private */
    public boolean adLoaded;
    /* access modifiers changed from: private */
    public OfferPolling adPolling;
    private int additionalDockingMargin;
    private AlarmManager am;
    private String appId;
    private boolean asynchTask;
    private Button backBtn;
    private Button closeBtn;
    /* access modifiers changed from: private */
    public boolean completed;
    private Context context;
    private boolean dataretrieve;
    private String domain;
    private View footer;
    private TextView footerTitle;
    private Button fwdBtn;
    private Button homeBtn;
    /* access modifiers changed from: private */
    public boolean homeLoaded;
    private boolean initialized;
    private RelativeLayout layout;
    private boolean linkClicked;
    /* access modifiers changed from: private */
    public AdListener listener;
    /* access modifiers changed from: private */
    public boolean loadAd;
    /* access modifiers changed from: private */
    public boolean loadIcon;
    /* access modifiers changed from: private */
    public Runnable loadProgress;
    /* access modifiers changed from: private */
    public String loadUrl;
    /* access modifiers changed from: private */
    public boolean loading;
    /* access modifiers changed from: private */
    public ProgressDialog loadingDialog;
    private RelativeLayout.LayoutParams lpC;
    private WebView mainView;
    private ViewGroup mainViewParent;
    private View mask;
    private ViewGroup.MarginLayoutParams mlpC;
    /* access modifiers changed from: private */
    public boolean nativeOpen;
    private String notificationLaunchType;
    private boolean onRequest;
    private boolean onTimer;
    private PendingIntent pendingAlarmIntent;
    private Button pollBtn;
    /* access modifiers changed from: private */
    public int pollCount;
    /* access modifiers changed from: private */
    public int pollManual;
    /* access modifiers changed from: private */
    public int pollMax;
    /* access modifiers changed from: private */
    public Handler pollingHandler;
    private boolean pollingInitialized;
    /* access modifiers changed from: private */
    public Handler progressHandler;
    /* access modifiers changed from: private */
    public int progressInterval;
    private Button refreshBtn;
    private LBRequest req;
    private boolean requestInProgress;
    /* access modifiers changed from: private */
    public JSONObject results;
    private int sHeight;
    private int sWidth;
    /* access modifiers changed from: private */
    public String sectionid;
    private String subid;
    private TextView title;
    /* access modifiers changed from: private */
    public TelephonyManager tm;
    private List<NameValuePair> tokens;
    private View toolbar;
    private boolean useLocation;
    /* access modifiers changed from: private */
    public boolean useNotification;
    /* access modifiers changed from: private */
    public WebView webview;

    public AdController(Activity act, String sid) {
        this(act, sid, new RelativeLayout(act));
    }

    public AdController(Activity act, String sid, WebView w) {
        this.LB_MAX_POLL = 500;
        this.LB_SET_MANUAL_AFTER = 10;
        this.MAX_APP_ICONS = 5;
        this.requestInProgress = false;
        this.completed = false;
        this.homeLoaded = false;
        this.linkClicked = false;
        this.pollingInitialized = false;
        this.useLocation = false;
        this.dataretrieve = true;
        this.nativeOpen = false;
        this.initialized = false;
        this.useNotification = false;
        this.additionalDockingMargin = 0;
        this.asynchTask = false;
        this.loadAd = true;
        this.onRequest = false;
        this.onTimer = false;
        this.listener = null;
        this.progressInterval = 0;
        this.adLoaded = false;
        this.adDestroyed = false;
        this.pollCount = 0;
        this.pollMax = 0;
        this.pollManual = 0;
        this.loadIcon = false;
        this.activity = act;
        this.sectionid = sid;
        this.mainView = w;
        this.layout = new RelativeLayout(this.activity);
    }

    public AdController(Context ctx, String sid) {
        this.LB_MAX_POLL = 500;
        this.LB_SET_MANUAL_AFTER = 10;
        this.MAX_APP_ICONS = 5;
        this.requestInProgress = false;
        this.completed = false;
        this.homeLoaded = false;
        this.linkClicked = false;
        this.pollingInitialized = false;
        this.useLocation = false;
        this.dataretrieve = true;
        this.nativeOpen = false;
        this.initialized = false;
        this.useNotification = false;
        this.additionalDockingMargin = 0;
        this.asynchTask = false;
        this.loadAd = true;
        this.onRequest = false;
        this.onTimer = false;
        this.listener = null;
        this.progressInterval = 0;
        this.adLoaded = false;
        this.adDestroyed = false;
        this.pollCount = 0;
        this.pollMax = 0;
        this.pollManual = 0;
        this.loadIcon = false;
        this.context = ctx;
        this.sectionid = sid;
    }

    public AdController(Activity act, String sid, AdListener lt) {
        this(act, sid, new RelativeLayout(act));
        this.listener = lt;
    }

    public AdController(Activity act, String sid, RelativeLayout ly) {
        this.LB_MAX_POLL = 500;
        this.LB_SET_MANUAL_AFTER = 10;
        this.MAX_APP_ICONS = 5;
        this.requestInProgress = false;
        this.completed = false;
        this.homeLoaded = false;
        this.linkClicked = false;
        this.pollingInitialized = false;
        this.useLocation = false;
        this.dataretrieve = true;
        this.nativeOpen = false;
        this.initialized = false;
        this.useNotification = false;
        this.additionalDockingMargin = 0;
        this.asynchTask = false;
        this.loadAd = true;
        this.onRequest = false;
        this.onTimer = false;
        this.listener = null;
        this.progressInterval = 0;
        this.adLoaded = false;
        this.adDestroyed = false;
        this.pollCount = 0;
        this.pollMax = 0;
        this.pollManual = 0;
        this.loadIcon = false;
        this.activity = act;
        this.sectionid = sid;
        this.layout = ly == null ? new RelativeLayout(act) : ly;
        this.mainView = null;
    }

    public void setUseLocation(boolean uL) {
        this.useLocation = uL;
        AdLog.i(LB_LOG, "setUseLocation: " + uL);
    }

    public void setLayout(RelativeLayout ly) {
        this.layout = ly;
    }

    public void setAdditionalDockingMargin(int newOffset) {
        this.additionalDockingMargin = newOffset;
        AdLog.i(LB_LOG, "setAdditionalDockingMargin: " + newOffset);
    }

    public void setAsynchTask(boolean asynch) {
        this.asynchTask = asynch;
    }

    public void setSubId(String sbid) {
        this.subid = sbid;
    }

    public void setTokens(List<NameValuePair> tks) {
        this.tokens = tks;
    }

    public void setOnProgressInterval(int pI) {
        this.progressInterval = pI;
    }

    public void destroyAd() {
        AdLog.i(LB_LOG, "destroyAd called");
        this.adDestroyed = true;
        closeUnlocker();
    }

    private void initialize() {
        AdLog.i(LB_LOG, "initializing...");
        if (this.activity != null) {
            if (this.webview == null) {
                this.webview = new WebView(this.activity);
                this.webview.getSettings().setJavaScriptEnabled(true);
                this.webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                this.webview.addJavascriptInterface(new LBJSInterface(this, null), "LBOUT");
                if (Build.VERSION.SDK_INT >= 8) {
                    this.webview.getSettings().setPluginState(WebSettings.PluginState.ON);
                }
            }
            this.webview.setWebChromeClient(new WebChromeClient() {
                public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                    new AlertDialog.Builder(AdController.this.activity).setTitle("Alert").setMessage(message).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            result.confirm();
                        }
                    }).setCancelable(false).create().show();
                    return true;
                }
            });
            this.webview.setWebViewClient(new WebViewClient() {
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    view.setPadding(0, 0, 0, 0);
                    view.setInitialScale(100);
                    view.setVerticalScrollBarEnabled(false);
                    view.setHorizontalScrollBarEnabled(false);
                    if (url.equals(AdController.this.loadUrl)) {
                        AdLog.i(AdController.LB_LOG, "Home loaded - loading = " + AdController.this.loading);
                        if (!AdController.this.loading) {
                            try {
                                if (AdController.this.results.get("useclickwindow").equals("1")) {
                                    AdLog.i(AdController.LB_LOG, "Going to use ClickWindow details");
                                    AdController.this.homeLoaded = true;
                                    AdController.this.loading = false;
                                    AdController.this.linkClicked();
                                    return;
                                }
                                AdLog.i(AdController.LB_LOG, "Normal window to be used");
                                AdController.this.loadAd();
                            } catch (Exception e) {
                                AdLog.e(AdController.LB_LOG, "Exception - " + e.getMessage());
                                AdController.this.loadAd();
                            }
                        }
                    } else {
                        AdLog.d(AdController.LB_LOG, "Link clicked!!");
                        if (AdController.this.loading) {
                            return;
                        }
                        if (AdController.this.nativeOpen || url.startsWith("market://") || url.contains("&usenative=1") || url.startsWith("http://market.android.com") || url.startsWith("https://market.android.com")) {
                            try {
                                AdLog.i(AdController.LB_LOG, "Opening URL natively");
                                view.stopLoading();
                                try {
                                    view.loadUrl(AdController.this.results.getString("clickhelpurl"));
                                } catch (Exception e2) {
                                }
                                AdController.this.activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                            } catch (Exception e3) {
                                AdController.this.closeUnlocker();
                            }
                        } else {
                            if (AdController.this.loadingDialog == null || !AdController.this.loadingDialog.isShowing()) {
                                AdController.this.loadingDialog = ProgressDialog.show(AdController.this.activity, "", "Loading....Please wait!", true);
                            }
                            AdController.this.linkClicked();
                        }
                    }
                }

                public void onPageFinished(WebView view, String url) {
                    if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                        AdController.this.loadingDialog.dismiss();
                    }
                    if (url.equals(AdController.this.loadUrl)) {
                        AdController.this.webview.setBackgroundColor(-1);
                        view.loadUrl("javascript:window.LBOUT.processHTML(document.getElementsByTagName('body')[0].getAttribute('ad_count'))");
                    }
                    AdController.this.loading = false;
                    AdController.this.webview.requestFocus(130);
                    try {
                        view.loadUrl("javascript:(function() { sdkNetworkCountry = '" + AdController.this.tm.getNetworkCountryIso() + "';" + "sdkNetworkOperator = '" + AdController.this.tm.getNetworkOperator() + "';" + "sdkNetworkOperatorName = '" + AdController.this.tm.getNetworkOperatorName() + "';" + "sdkPhoneNumber = '" + AdController.this.tm.getLine1Number() + "';" + "})()");
                    } catch (Exception e) {
                    }
                    if (url.contains("#app_close")) {
                        try {
                            Thread.sleep(1000);
                            AdController.this.closeUnlocker();
                        } catch (Exception e2) {
                        }
                    }
                }
            });
            try {
                NetworkInfo netInfo = ((ConnectivityManager) this.activity.getSystemService("connectivity")).getActiveNetworkInfo();
                if (netInfo != null) {
                    Boolean con = new Boolean(netInfo.isConnected());
                    boolean makeRequest = true;
                    try {
                        String displayinterval = this.activity.getSharedPreferences("Preference", 2).getString("SD_" + this.sectionid, "0");
                        if (!displayinterval.equals("0")) {
                            int curTimestamp = (int) (Calendar.getInstance().getTimeInMillis() / 1000);
                            if (displayinterval.equals("-1") || curTimestamp < new Integer(displayinterval).intValue()) {
                                makeRequest = false;
                            }
                        }
                    } catch (Exception e) {
                        AdLog.printStackTrace(LB_LOG, e);
                    }
                    if (con.booleanValue() && makeRequest) {
                        if (Build.VERSION.SDK_INT > 8 || this.asynchTask) {
                            AdLog.d(LB_LOG, "Request will not be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                            if (this.asynchTask) {
                                AdLog.d(LB_LOG, "AsynchTask variable set");
                            }
                            this.req = new LBRequest(this, null);
                            this.req.execute("");
                            return;
                        }
                        AdLog.d(LB_LOG, "Request to be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                        makeLBRequest();
                        if (this.loadAd) {
                            displayAd();
                        } else if (this.useNotification) {
                            setNotification();
                        } else if (this.loadIcon) {
                            displayIcon();
                        }
                    }
                } else {
                    AdLog.e(LB_LOG, "No Internet connection detected. No Ads loaded");
                    if (this.listener != null) {
                        try {
                            AdLog.i(LB_LOG, "onAdFailed triggered");
                            this.listener.onAdFailed();
                            this.adLoaded = true;
                        } catch (Exception e2) {
                            AdLog.i(LB_LOG, "Error while calling onAdFailed");
                            AdLog.printStackTrace(LB_LOG, e2);
                        }
                    }
                }
            } catch (Exception e3) {
                AdLog.printStackTrace(LB_LOG, e3);
                AdLog.e(LB_LOG, "Error Message No wifi - " + e3.getMessage());
                AdLog.e(LB_LOG, "No WIFI, 3G or Edge connection detected");
                if (this.listener != null) {
                    try {
                        AdLog.i(LB_LOG, "onAdFailed triggered");
                        this.listener.onAdFailed();
                        this.adLoaded = true;
                    } catch (Exception ex) {
                        AdLog.i(LB_LOG, "Error while calling onAdFailed");
                        AdLog.printStackTrace(LB_LOG, ex);
                    }
                }
            }
        } else if (((ConnectivityManager) this.context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
            if (Build.VERSION.SDK_INT > 8 || this.asynchTask) {
                if (this.loadIcon) {
                    AdLog.d(LB_LOG, "loadIcon Request will not be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                } else {
                    AdLog.d(LB_LOG, "Notification Request will not be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                }
                if (this.asynchTask) {
                    AdLog.d(LB_LOG, "AsynchTask variable set");
                }
                new LBRequest(this, null).execute("");
                return;
            }
            if (this.loadIcon) {
                AdLog.d(LB_LOG, "loadIcon Request will be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
            } else {
                AdLog.d(LB_LOG, "Notification Request will be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
            }
            makeLBRequest();
            if (this.loadIcon) {
                displayIcon();
            } else {
                setNotification();
            }
        } else if (!this.loadIcon) {
            setAlarmFromCookie();
        }
    }

    public void loadNotification() {
        AdLog.i(LB_LOG, "loadNotification called");
        if (!this.initialized) {
            this.useNotification = true;
            this.loadAd = false;
            this.notificationLaunchType = "App";
            this.onTimer = false;
            this.onRequest = false;
            initialize();
        }
    }

    /* access modifiers changed from: private */
    public void setNotification() {
        AdLog.d(LB_LOG, "setNotification called");
        if (this.results == null) {
            AdLog.e(LB_LOG, "Results are null - no notification will be loaded");
            return;
        }
        try {
            if (this.onRequest) {
                loadNotificationDetails();
            } else if (this.onTimer) {
                loadNotificationTimerDetails();
            } else if (this.results.getString("show").equals("1")) {
                String notificationtype = this.results.getString("notificationtype");
                if (notificationtype.equals("Immediate")) {
                    AdLog.i(LB_LOG, "Immediate notification to be fired");
                    loadNotificationOnRequest("App");
                } else if (notificationtype.equals("Recurring")) {
                    AdLog.i(LB_LOG, "Recurring notification to be created");
                    loadNotificationOnTimer();
                }
            } else {
                AdLog.i(LB_LOG, "Notification not be set for this user - DeviceId = " + this.tm.getDeviceId());
            }
        } catch (Exception e) {
            AdLog.printStackTrace(LB_LOG, e);
        }
    }

    private void loadNotificationOnTimer() {
        AdLog.i(LB_LOG, "loadNotificationOnTimer called");
        if (!this.initialized) {
            this.useNotification = true;
            this.loadAd = false;
            this.onRequest = true;
            this.onTimer = false;
            initialize();
            return;
        }
        loadNotificationTimerDetails();
    }

    private void loadNotificationTimerDetails() {
        if (this.results == null) {
            AdLog.e(LB_LOG, "Notification will not be loaded - no internet connection");
            return;
        }
        try {
            if (this.results.getString("show").equals("1")) {
                setAlarm();
            }
        } catch (Exception e) {
            AdLog.printStackTrace(LB_LOG, e);
        }
    }

    private void setAlarmFromCookie() {
        Context ctx;
        if (this.activity != null) {
            ctx = this.activity;
        } else {
            ctx = this.context;
        }
        SharedPreferences pref = ctx.getSharedPreferences("Preference", 2);
        SharedPreferences.Editor editor = pref.edit();
        long timesAttempted = (long) Integer.valueOf(pref.getString("SD_ALARM_ATTEMPTED_" + this.sectionid, "0")).intValue();
        long now = System.currentTimeMillis();
        long newStartTimeTime = now + 10000;
        if (timesAttempted > 25) {
            long alarminterval = pref.getLong("SD_ALARM_INTERVAL_" + this.sectionid, 0);
            AdLog.i(LB_LOG, "No internet, already tried 5 times, set it to timer " + alarminterval + "s");
            AdLog.i(LB_LOG, "Times attempted = " + timesAttempted);
            newStartTimeTime = now + (1000 * alarminterval);
            editor.putLong("SD_ALARM_TIME_" + this.sectionid, newStartTimeTime);
            editor.putString("SD_ALARM_ATTEMPTED_" + this.sectionid, "0");
            editor.commit();
        } else if (timesAttempted % 5 != 0 || timesAttempted <= 0) {
            AdLog.i(LB_LOG, "No internet, retry alarm in 10s");
            editor.putString("SD_ALARM_ATTEMPTED_" + this.sectionid, new StringBuilder().append(1 + timesAttempted).toString());
            editor.commit();
        } else {
            newStartTimeTime = now + 600000;
            AdLog.i(LB_LOG, "No internet, retry alarm in 10 mins");
            editor.putString("SD_ALARM_ATTEMPTED_" + this.sectionid, new StringBuilder().append(1 + timesAttempted).toString());
            editor.commit();
        }
        this.am = (AlarmManager) ctx.getSystemService("alarm");
        Intent intent = new Intent(ctx, AdNotification.class);
        intent.putExtra("sectionid", this.sectionid);
        this.pendingAlarmIntent = PendingIntent.getBroadcast(ctx, 0, intent, 134217728);
        try {
            this.am.set(0, newStartTimeTime, this.pendingAlarmIntent);
        } catch (Exception ex) {
            AdLog.printStackTrace(LB_LOG, ex);
        }
    }

    private void setAlarm() {
        Context ctx;
        int resultcookie;
        AdLog.i(LB_LOG, "setAlarm called");
        if (this.activity != null) {
            ctx = this.activity;
        } else {
            ctx = this.context;
        }
        SharedPreferences pref = ctx.getSharedPreferences("Preference", 2);
        SharedPreferences.Editor editor = pref.edit();
        if (this.results.length() > 0) {
            AdLog.i(LB_LOG, "internet connection found....initialize alarm from settings - Result Length=" + this.results.length());
            try {
                int iterationcounter = Integer.valueOf(pref.getString("SD_ITERATION_COUNTER_" + this.sectionid, "0")).intValue();
                try {
                    resultcookie = Integer.valueOf(this.results.getString("notificationcookie")).intValue();
                } catch (Exception e) {
                    resultcookie = 0;
                }
                AdLog.d(LB_LOG, "Values: ck=" + resultcookie + ", ic=" + iterationcounter + ", nLT=" + this.notificationLaunchType);
                this.am = (AlarmManager) ctx.getSystemService("alarm");
                Intent intent = new Intent(ctx.getApplicationContext(), AdNotification.class);
                intent.putExtra("sectionid", this.sectionid);
                this.pendingAlarmIntent = PendingIntent.getBroadcast(ctx.getApplicationContext(), 0, intent, 134217728);
                if (resultcookie == 1 || ((resultcookie == 0 && iterationcounter == 0) || this.notificationLaunchType.equals("Alarm"))) {
                    AdLog.i(LB_LOG, "alarm will be initialized - ck is " + resultcookie + ", ic is " + iterationcounter + ", nLT is " + this.notificationLaunchType);
                    Calendar cal = Calendar.getInstance();
                    long now = System.currentTimeMillis();
                    try {
                        int startat = Integer.parseInt(this.results.getString("notificationstart"));
                        cal.add(13, startat);
                        AdLog.d(LB_LOG, "Alarm initialized - Scheduled at " + startat + ", current time = " + now);
                        AdLog.d(LB_LOG, "----------------------------------------");
                        this.am.set(0, cal.getTimeInMillis(), this.pendingAlarmIntent);
                        editor.putLong("SD_ALARM_TIME_" + this.sectionid, now);
                        editor.putLong("SD_WAKE_TIME_" + this.sectionid, cal.getTimeInMillis());
                        editor.putLong("SD_ALARM_INTERVAL_" + this.sectionid, (long) startat);
                        editor.commit();
                    } catch (Exception e2) {
                        AdLog.e(LB_LOG, "Error caused while setting Alarm (if case): " + e2.getMessage());
                        AdLog.printStackTrace(LB_LOG, e2);
                    }
                } else {
                    long newstartat = pref.getLong("SD_WAKE_TIME_" + this.sectionid, 0);
                    Calendar cal2 = Calendar.getInstance();
                    long now2 = System.currentTimeMillis();
                    try {
                        cal2.setTimeInMillis(newstartat);
                        AdLog.d(LB_LOG, "Alarm reset - Scheduled at " + newstartat + ", current time = " + now2);
                        AdLog.d(LB_LOG, "----------------------------------------");
                        this.am.set(0, cal2.getTimeInMillis(), this.pendingAlarmIntent);
                        editor.putLong("SD_ALARM_TIME_" + this.sectionid, now2);
                        editor.putLong("SD_WAKE_TIME_" + this.sectionid, newstartat);
                        editor.commit();
                    } catch (Exception e3) {
                        AdLog.e(LB_LOG, "Error caused while setting Alarm (else case): " + e3.getMessage());
                        AdLog.printStackTrace(LB_LOG, e3);
                    }
                }
            } catch (Exception e4) {
            }
        } else {
            setAlarmFromCookie();
        }
    }

    public void loadNotificationOnRequest(String launchtype) {
        if (launchtype.equals("App") || launchtype.equals("Alarm")) {
            AdLog.i(LB_LOG, "loadNotificationOnRequest called");
            this.notificationLaunchType = launchtype;
            if (!this.initialized) {
                this.useNotification = true;
                this.loadAd = false;
                this.onRequest = true;
                this.onTimer = false;
                initialize();
                return;
            }
            loadNotificationDetails();
            return;
        }
        AdLog.e(LB_LOG, "Illegal use of loadNotificationOnRequest. LaunchType used = " + launchtype);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0276, code lost:
        if (r0.notificationLaunchType.equals(r23) != false) goto L_0x0278;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadNotificationDetails() {
        /*
            r25 = this;
            java.lang.String r21 = "LBAdController"
            java.lang.String r22 = "loadNotificationDetails called"
            com.Leadbolt.AdLog.i(r21, r22)
            r0 = r25
            org.json.JSONObject r0 = r0.results
            r21 = r0
            if (r21 != 0) goto L_0x0017
            java.lang.String r21 = "LBAdController"
            java.lang.String r22 = "Notification will not be loaded - no internet connection"
            com.Leadbolt.AdLog.e(r21, r22)
        L_0x0016:
            return
        L_0x0017:
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "show"
            java.lang.String r21 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = "1"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x0210 }
            if (r21 == 0) goto L_0x01c0
            java.lang.String r21 = "LBAdController"
            java.lang.String r22 = "notification to be fired"
            com.Leadbolt.AdLog.i(r21, r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            if (r21 == 0) goto L_0x0200
            r0 = r25
            android.app.Activity r6 = r0.activity     // Catch:{ Exception -> 0x0210 }
        L_0x003e:
            java.lang.String r21 = "notification"
            r0 = r21
            java.lang.Object r15 = r6.getSystemService(r0)     // Catch:{ Exception -> 0x0210 }
            android.app.NotificationManager r15 = (android.app.NotificationManager) r15     // Catch:{ Exception -> 0x0210 }
            r21 = 5
            r0 = r21
            int[] r10 = new int[r0]     // Catch:{ Exception -> 0x0210 }
            r10 = {17301620, 17301547, 17301516, 17301514, 17301618} // fill-array
            r9 = 0
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0206, all -> 0x020c }
            r21 = r0
            java.lang.String r22 = "notificationicon"
            int r9 = r21.getInt(r22)     // Catch:{ Exception -> 0x0206, all -> 0x020c }
            r9 = r10[r9]     // Catch:{ Exception -> 0x0210 }
        L_0x0060:
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "notificationtext"
            java.lang.String r17 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r11 = ""
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x02a6 }
            r21 = r0
            java.lang.String r22 = "notificationinstruction"
            java.lang.String r11 = r21.getString(r22)     // Catch:{ Exception -> 0x02a6 }
        L_0x007a:
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "notificationtext"
            java.lang.String r5 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "notificationdescription"
            java.lang.String r4 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x02a3 }
            r21 = r0
            java.lang.String r22 = "notificationdisplay"
            java.lang.String r18 = r21.getString(r22)     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r21 = ""
            r0 = r18
            r1 = r21
            boolean r21 = r0.equals(r1)     // Catch:{ Exception -> 0x02a3 }
            if (r21 != 0) goto L_0x0258
            java.lang.String r21 = "notificationtext"
            r0 = r18
            r1 = r21
            boolean r21 = r0.equals(r1)     // Catch:{ Exception -> 0x02a3 }
            if (r21 == 0) goto L_0x00c5
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x02a3 }
            r21 = r0
            r0 = r21
            r1 = r18
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Exception -> 0x02a3 }
            r4 = r11
        L_0x00c5:
            long r19 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0210 }
            android.content.Intent r12 = new android.content.Intent     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = "android.intent.action.VIEW"
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r22 = r0
            java.lang.String r23 = "notificationurl"
            java.lang.String r22 = r22.getString(r23)     // Catch:{ Exception -> 0x0210 }
            android.net.Uri r22 = android.net.Uri.parse(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r21
            r1 = r22
            r12.<init>(r0, r1)     // Catch:{ Exception -> 0x0210 }
            r21 = 0
            r22 = 0
            r0 = r21
            r1 = r22
            android.app.PendingIntent r3 = android.app.PendingIntent.getActivity(r6, r0, r12, r1)     // Catch:{ Exception -> 0x0210 }
            android.app.Notification r14 = new android.app.Notification     // Catch:{ Exception -> 0x0210 }
            r0 = r17
            r1 = r19
            r14.<init>(r9, r0, r1)     // Catch:{ Exception -> 0x0210 }
            int r0 = r14.flags     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            r21 = r21 | 16
            r0 = r21
            r14.flags = r0     // Catch:{ Exception -> 0x0210 }
            r14.setLatestEventInfo(r6, r5, r4, r3)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = "Preference"
            r22 = 2
            r0 = r21
            r1 = r22
            android.content.SharedPreferences r16 = r6.getSharedPreferences(r0, r1)     // Catch:{ Exception -> 0x0210 }
            android.content.SharedPreferences$Editor r8 = r16.edit()     // Catch:{ Exception -> 0x0210 }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = "SD_NOTIFICATION_ID_"
            r21.<init>(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x0210 }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x0210 }
            r22 = 0
            r0 = r16
            r1 = r21
            r2 = r22
            int r13 = r0.getInt(r1, r2)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0210 }
            java.lang.String r23 = "Stored Pref - "
            r22.<init>(r23)     // Catch:{ Exception -> 0x0210 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r13)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0210 }
            com.Leadbolt.AdLog.d(r21, r22)     // Catch:{ Exception -> 0x0210 }
            if (r13 != 0) goto L_0x0151
            r13 = 10001(0x2711, float:1.4014E-41)
        L_0x0151:
            r21 = 1001(0x3e9, float:1.403E-42)
            r0 = r21
            r15.notify(r0, r14)     // Catch:{ Exception -> 0x0210 }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a0 }
            java.lang.String r22 = "SD_NOTIFICATION_FIRED_"
            r21.<init>(r22)     // Catch:{ Exception -> 0x02a0 }
            r0 = r25
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x02a0 }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ Exception -> 0x02a0 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x02a0 }
            r0 = r21
            r1 = r19
            r8.putLong(r0, r1)     // Catch:{ Exception -> 0x02a0 }
            r8.commit()     // Catch:{ Exception -> 0x02a0 }
        L_0x0177:
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0210 }
            java.lang.String r23 = "New Notification created with ID - "
            r22.<init>(r23)     // Catch:{ Exception -> 0x0210 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r13)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0210 }
            com.Leadbolt.AdLog.d(r21, r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x029d }
            r21 = r0
            java.lang.String r22 = "notificationmultiple"
            java.lang.String r21 = r21.getString(r22)     // Catch:{ Exception -> 0x029d }
            java.lang.String r22 = "1"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x029d }
            if (r21 == 0) goto L_0x01c0
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x029d }
            java.lang.String r22 = "SD_NOTIFICATION_ID_"
            r21.<init>(r22)     // Catch:{ Exception -> 0x029d }
            r0 = r25
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x029d }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ Exception -> 0x029d }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x029d }
            int r13 = r13 + 1
            r0 = r21
            r8.putInt(r0, r13)     // Catch:{ Exception -> 0x029d }
            r8.commit()     // Catch:{ Exception -> 0x029d }
        L_0x01c0:
            r25.incrementIterationCounter()
            r0 = r25
            boolean r0 = r0.onTimer     // Catch:{ Exception -> 0x01de }
            r21 = r0
            if (r21 != 0) goto L_0x01d9
            r0 = r25
            java.lang.String r0 = r0.notificationLaunchType     // Catch:{ Exception -> 0x01de }
            r21 = r0
            java.lang.String r22 = "Alarm"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x01de }
            if (r21 == 0) goto L_0x0016
        L_0x01d9:
            r25.setAlarm()     // Catch:{ Exception -> 0x01de }
            goto L_0x0016
        L_0x01de:
            r7 = move-exception
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder
            java.lang.String r23 = "Error while setting Alarm - "
            r22.<init>(r23)
            java.lang.String r23 = r7.getMessage()
            java.lang.StringBuilder r22 = r22.append(r23)
            java.lang.String r22 = r22.toString()
            com.Leadbolt.AdLog.e(r21, r22)
            java.lang.String r21 = "LBAdController"
            r0 = r21
            com.Leadbolt.AdLog.printStackTrace(r0, r7)
            goto L_0x0016
        L_0x0200:
            r0 = r25
            android.content.Context r6 = r0.context     // Catch:{ Exception -> 0x0210 }
            goto L_0x003e
        L_0x0206:
            r7 = move-exception
            r9 = 0
            r9 = r10[r9]     // Catch:{ Exception -> 0x0210 }
            goto L_0x0060
        L_0x020c:
            r21 = move-exception
            r9 = r10[r9]     // Catch:{ Exception -> 0x0210 }
            throw r21     // Catch:{ Exception -> 0x0210 }
        L_0x0210:
            r7 = move-exception
            java.lang.String r21 = "LBAdController"
            r0 = r21
            com.Leadbolt.AdLog.printStackTrace(r0, r7)     // Catch:{ all -> 0x025e }
            r25.incrementIterationCounter()
            r0 = r25
            boolean r0 = r0.onTimer     // Catch:{ Exception -> 0x0236 }
            r21 = r0
            if (r21 != 0) goto L_0x0231
            r0 = r25
            java.lang.String r0 = r0.notificationLaunchType     // Catch:{ Exception -> 0x0236 }
            r21 = r0
            java.lang.String r22 = "Alarm"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x0236 }
            if (r21 == 0) goto L_0x0016
        L_0x0231:
            r25.setAlarm()     // Catch:{ Exception -> 0x0236 }
            goto L_0x0016
        L_0x0236:
            r7 = move-exception
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder
            java.lang.String r23 = "Error while setting Alarm - "
            r22.<init>(r23)
            java.lang.String r23 = r7.getMessage()
            java.lang.StringBuilder r22 = r22.append(r23)
            java.lang.String r22 = r22.toString()
            com.Leadbolt.AdLog.e(r21, r22)
            java.lang.String r21 = "LBAdController"
            r0 = r21
            com.Leadbolt.AdLog.printStackTrace(r0, r7)
            goto L_0x0016
        L_0x0258:
            java.lang.String r5 = "You have 1 new message"
            java.lang.String r4 = "Tap to View"
            goto L_0x00c5
        L_0x025e:
            r21 = move-exception
            r25.incrementIterationCounter()
            r0 = r25
            boolean r0 = r0.onTimer     // Catch:{ Exception -> 0x027c }
            r22 = r0
            if (r22 != 0) goto L_0x0278
            r0 = r25
            java.lang.String r0 = r0.notificationLaunchType     // Catch:{ Exception -> 0x027c }
            r22 = r0
            java.lang.String r23 = "Alarm"
            boolean r22 = r22.equals(r23)     // Catch:{ Exception -> 0x027c }
            if (r22 == 0) goto L_0x027b
        L_0x0278:
            r25.setAlarm()     // Catch:{ Exception -> 0x027c }
        L_0x027b:
            throw r21
        L_0x027c:
            r7 = move-exception
            java.lang.String r22 = "LBAdController"
            java.lang.StringBuilder r23 = new java.lang.StringBuilder
            java.lang.String r24 = "Error while setting Alarm - "
            r23.<init>(r24)
            java.lang.String r24 = r7.getMessage()
            java.lang.StringBuilder r23 = r23.append(r24)
            java.lang.String r23 = r23.toString()
            com.Leadbolt.AdLog.e(r22, r23)
            java.lang.String r22 = "LBAdController"
            r0 = r22
            com.Leadbolt.AdLog.printStackTrace(r0, r7)
            goto L_0x027b
        L_0x029d:
            r21 = move-exception
            goto L_0x01c0
        L_0x02a0:
            r21 = move-exception
            goto L_0x0177
        L_0x02a3:
            r21 = move-exception
            goto L_0x00c5
        L_0x02a6:
            r21 = move-exception
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Leadbolt.AdController.loadNotificationDetails():void");
    }

    private void incrementIterationCounter() {
        AdLog.i(LB_LOG, "increment counter called");
        SharedPreferences pref = (this.activity != null ? this.activity : this.context).getSharedPreferences("Preference", 2);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("SD_ITERATION_COUNTER_" + this.sectionid, new StringBuilder().append(Integer.valueOf(pref.getString("SD_ITERATION_COUNTER_" + this.sectionid, "0")).intValue() + 1).toString());
        editor.commit();
    }

    public void loadAd() {
        AdLog.i(LB_LOG, "loadAd called");
        if (!this.initialized) {
            initialize();
            this.loadAd = true;
        } else {
            displayAd();
        }
        if (this.listener != null && this.progressInterval > 0) {
            if (this.loadProgress == null) {
                this.loadProgress = new Runnable() {
                    public void run() {
                        try {
                            if (!AdController.this.adLoaded && !AdController.this.adDestroyed) {
                                AdLog.i(AdController.LB_LOG, "onAdProgress triggered");
                                AdController.this.listener.onAdProgress();
                                AdController.this.progressHandler.postDelayed(AdController.this.loadProgress, (long) (AdController.this.progressInterval * 1000));
                            }
                        } catch (Exception e) {
                            AdLog.e(AdController.LB_LOG, "error when onAdProgress triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e);
                        }
                    }
                };
            }
            if (this.progressHandler == null) {
                this.progressHandler = new Handler();
                this.progressHandler.postDelayed(this.loadProgress, (long) (this.progressInterval * 1000));
            }
        }
    }

    public void loadIcon() {
        AdLog.i(LB_LOG, "loadIcon called");
        if (!this.initialized) {
            this.loadAd = false;
            this.loadIcon = true;
            initialize();
            return;
        }
        displayIcon();
    }

    /* access modifiers changed from: private */
    public void displayIcon() {
        String adText;
        AdLog.i(LB_LOG, "displayIcon called");
        if (this.results == null) {
            AdLog.e(LB_LOG, "Results are null - no icon will be loaded");
            return;
        }
        try {
            if (this.results.get("show").equals("1") && this.context != null) {
                AdLog.i(LB_LOG, "going to display icon");
                try {
                    adText = this.results.getString("adname");
                } catch (Exception e) {
                    AdLog.printStackTrace(LB_LOG, e);
                }
                SharedPreferences pref = this.context.getSharedPreferences("Preference", 2);
                int displayCount = pref.getInt("SD_ICON_DISPLAY_" + this.sectionid, 0);
                if (displayCount < 5) {
                    AdLog.i(LB_LOG, "MAX count not passed so display icon");
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(String.valueOf(this.results.getString("adurl")) + this.sectionid));
                    Intent addIntent = new Intent();
                    addIntent.putExtra("android.intent.extra.shortcut.INTENT", intent);
                    addIntent.putExtra("android.intent.extra.shortcut.NAME", adText);
                    try {
                        Bitmap bmp = BitmapFactory.decodeStream(((HttpURLConnection) new URL(this.results.getString("adiconurl")).openConnection()).getInputStream());
                        Display display = ((WindowManager) this.context.getSystemService("window")).getDefaultDisplay();
                        DisplayMetrics metrics = new DisplayMetrics();
                        display.getMetrics(metrics);
                        bmp.setDensity((int) (160.0f * metrics.density));
                        Bitmap bmp2 = Bitmap.createScaledBitmap(bmp, 72, 72, true);
                        if (bmp2 != null) {
                            AdLog.i(LB_LOG, "bitmap not null");
                            addIntent.putExtra("android.intent.extra.shortcut.ICON", bmp2);
                        } else {
                            AdLog.i(LB_LOG, "bitmap null");
                            addIntent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this.context, 17301593));
                        }
                    } catch (Exception e2) {
                        AdLog.i(LB_LOG, "exception in getting icon");
                        AdLog.printStackTrace(LB_LOG, e2);
                        addIntent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this.context, 17301593));
                    }
                    addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
                    this.context.sendBroadcast(addIntent);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putInt("SD_ICON_DISPLAY_" + this.sectionid, displayCount + 1);
                    editor.commit();
                } else {
                    AdLog.d(LB_LOG, "DisplayCount = " + displayCount + ", MAX_APP_ICONS = " + 5);
                }
                ContentValues cv = new ContentValues();
                cv.put("title", adText);
                cv.put("url", String.valueOf(this.results.getString("adurl")) + this.sectionid);
                cv.put("bookmark", (Integer) 1);
                try {
                    this.context.getContentResolver().insert(Browser.BOOKMARKS_URI, cv);
                    AdLog.d(LB_LOG, "bookmark insert successfully");
                } catch (Exception e3) {
                    AdLog.e(LB_LOG, "bookmark insert error");
                }
            }
        } catch (Exception e4) {
            AdLog.printStackTrace(LB_LOG, e4);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x084f, code lost:
        closeUnlocker();
        r1.loading = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x085c, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x085d, code lost:
        closeUnlocker();
        android.util.Log.d("AdController", "JSONException - " + r14.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x090a, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:?, code lost:
        com.Leadbolt.AdLog.i(com.Leadbolt.AdController.LB_LOG, "Error while calling onAdFailed");
        com.Leadbolt.AdLog.printStackTrace(com.Leadbolt.AdController.LB_LOG, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:25:0x015f, B:128:0x08f4] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x085c A[ExcHandler: JSONException (r14v1 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:25:0x015f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void displayAd() {
        /*
            r50 = this;
            r0 = r50
            org.json.JSONObject r0 = r0.results
            r45 = r0
            if (r45 != 0) goto L_0x0010
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "Results are null - no ad will be loaded"
            com.Leadbolt.AdLog.e(r45, r46)
        L_0x000f:
            return
        L_0x0010:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x004d }
            r45 = r0
            java.lang.String r46 = "useclickwindow"
            boolean r45 = r45.isNull(r46)     // Catch:{ Exception -> 0x004d }
            if (r45 != 0) goto L_0x006d
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x004d }
            r45 = r0
            java.lang.String r46 = "useclickwindow"
            java.lang.String r45 = r45.getString(r46)     // Catch:{ Exception -> 0x004d }
            java.lang.String r46 = "1"
            boolean r45 = r45.equals(r46)     // Catch:{ Exception -> 0x004d }
            if (r45 == 0) goto L_0x006d
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ Exception -> 0x004d }
            r45 = 0
            r0 = r45
            r1 = r50
            r1.linkClicked = r0     // Catch:{ Exception -> 0x004d }
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "Going to use click window - cancel out of here..."
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ Exception -> 0x004d }
            r50.linkClicked()     // Catch:{ Exception -> 0x004d }
            goto L_0x000f
        L_0x004d:
            r14 = move-exception
            java.lang.String r45 = "LBAdController"
            r0 = r45
            com.Leadbolt.AdLog.printStackTrace(r0, r14)
            java.lang.String r45 = "LBAdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder
            java.lang.String r47 = "Exception when using ClickWindow - "
            r46.<init>(r47)
            java.lang.String r47 = r14.getMessage()
            java.lang.StringBuilder r46 = r46.append(r47)
            java.lang.String r46 = r46.toString()
            com.Leadbolt.AdLog.e(r45, r46)
        L_0x006d:
            r0 = r50
            boolean r0 = r0.homeLoaded
            r45 = r0
            if (r45 != 0) goto L_0x000f
            r45 = 1
            r0 = r45
            r1 = r50
            r1.homeLoaded = r0
            r45 = 0
            r0 = r45
            r1 = r50
            r1.linkClicked = r0
            r45 = 1
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "show"
            java.lang.Object r45 = r45.get(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r46 = "1"
            boolean r45 = r45.equals(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            if (r45 == 0) goto L_0x08dd
            r0 = r50
            boolean r0 = r0.completed     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            if (r45 != 0) goto L_0x08d0
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.webkit.WebView r0 = r0.webview     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.toolbar     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.footer     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.backBtn     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.fwdBtn     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.homeBtn     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.refreshBtn     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r0 = r50
            android.widget.Button r0 = r0.pollBtn     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            if (r45 == 0) goto L_0x015f
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
            r45 = r0
            r45.removeAllViews()     // Catch:{ Exception -> 0x0924, JSONException -> 0x085c }
        L_0x015f:
            android.view.View r45 = new android.view.View     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r46 = r0
            r45.<init>(r46)     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r0 = r45
            r1 = r50
            r1.mask = r0     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r45 = r0
            r46 = -1
            r45.setMinimumHeight(r46)     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r45 = r0
            r46 = -1
            r45.setMinimumWidth(r46)     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r45 = r0
            r46 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r45.setBackgroundColor(r46)     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r45 = r0
            java.lang.String r46 = "maskalpha"
            double r45 = r45.getDouble(r46)     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r47 = 4643176031446892544(0x406fe00000000000, double:255.0)
            double r5 = r45 * r47
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r45 = r0
            android.graphics.drawable.Drawable r45 = r45.getBackground()     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            int r0 = (int) r5     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r46 = r0
            r45.setAlpha(r46)     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r45 = r0
            com.Leadbolt.AdController$4 r46 = new com.Leadbolt.AdController$4     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r0 = r46
            r1 = r50
            r0.<init>()     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
            r45.setOnClickListener(r46)     // Catch:{ Exception -> 0x0921, JSONException -> 0x085c }
        L_0x01c6:
            android.view.ViewGroup$MarginLayoutParams r30 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = -1
            r46 = -1
            r0 = r30
            r1 = r45
            r2 = r46
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = 0
            r46 = 0
            r47 = 0
            r48 = 0
            r0 = r30
            r1 = r45
            r2 = r46
            r3 = r47
            r4 = r48
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.widget.RelativeLayout$LayoutParams r29 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r29.<init>(r30)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "windowy"
            int r44 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r13 = "Middle"
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x091e, JSONException -> 0x085c }
            r45 = r0
            java.lang.String r46 = "windowdockingy"
            java.lang.String r13 = r45.getString(r46)     // Catch:{ Exception -> 0x091e, JSONException -> 0x085c }
        L_0x0209:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "titlevisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            if (r45 != 0) goto L_0x0266
            r0 = r50
            android.webkit.WebView r0 = r0.webview     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r46 = 0
            r45.setBackgroundColor(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            if (r45 <= 0) goto L_0x0266
            java.lang.String r45 = "Middle"
            r0 = r45
            boolean r45 = r13.equals(r0)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            if (r45 != 0) goto L_0x0266
            java.lang.String r45 = "LBAdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r47 = "Additional Docking is set, adjusting banner by "
            r46.<init>(r47)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r47 = r0
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r47 = "px"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r46 = r46.toString()     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r45 = "Top"
            r0 = r45
            boolean r45 = r13.equals(r0)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            if (r45 == 0) goto L_0x0836
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            int r44 = r44 + r45
        L_0x0266:
            android.view.ViewGroup$MarginLayoutParams r43 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "windowwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            java.lang.String r47 = "windowheight"
            int r46 = r46.getInt(r47)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r43
            r1 = r45
            r2 = r46
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "windowx"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = 0
            r47 = 0
            r0 = r43
            r1 = r45
            r2 = r44
            r3 = r46
            r4 = r47
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.widget.RelativeLayout$LayoutParams r42 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r42.<init>(r43)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "titlex"
            int r38 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "titley"
            int r39 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "titlewidth"
            int r37 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "titleheight"
            int r36 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.view.ViewGroup$MarginLayoutParams r32 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r32
            r1 = r37
            r2 = r36
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = 0
            r46 = 0
            r0 = r32
            r1 = r38
            r2 = r39
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.widget.RelativeLayout$LayoutParams r31 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r31.<init>(r32)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.view.View r45 = new android.view.View     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r45
            r1 = r50
            r1.toolbar = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "titlecolor"
            java.lang.String r12 = r45.getString(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r45 = ""
            r0 = r45
            boolean r45 = r12.equals(r0)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            if (r45 != 0) goto L_0x0325
            if (r12 != 0) goto L_0x0327
        L_0x0325:
            java.lang.String r12 = "#E6E6E6"
        L_0x0327:
            r0 = r50
            android.view.View r0 = r0.toolbar     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r12)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45.setBackgroundColor(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.widget.TextView r45 = new android.widget.TextView     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r45
            r1 = r50
            r1.title = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            java.lang.String r47 = "titletext"
            java.lang.String r46 = r46.getString(r47)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45.setText(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "titletextcolor"
            java.lang.String r9 = r45.getString(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r45 = ""
            r0 = r45
            boolean r45 = r9.equals(r0)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            if (r45 != 0) goto L_0x0372
            if (r9 != 0) goto L_0x0374
        L_0x0372:
            java.lang.String r9 = "#E6E6E6"
        L_0x0374:
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r9)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45.setTextColor(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "titletextheight"
            int r33 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.view.ViewGroup$MarginLayoutParams r41 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "titletextwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r41
            r1 = r45
            r2 = r33
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            int r34 = r38 + 20
            int r45 = r36 - r33
            int r45 = r45 / 2
            int r45 = r45 + r39
            int r35 = r45 + 4
            r45 = 0
            r46 = 0
            r0 = r41
            r1 = r34
            r2 = r35
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.widget.RelativeLayout$LayoutParams r40 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r40.<init>(r41)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "footerx"
            int r23 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "footery"
            int r24 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "footerheight"
            int r21 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.view.ViewGroup$MarginLayoutParams r17 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "footerwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r17
            r1 = r45
            r2 = r21
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = 0
            r46 = 0
            r0 = r17
            r1 = r23
            r2 = r24
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.widget.RelativeLayout$LayoutParams r16 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r16.<init>(r17)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.view.View r45 = new android.view.View     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r45
            r1 = r50
            r1.footer = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "footercolor"
            java.lang.String r15 = r45.getString(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r45 = ""
            r0 = r45
            boolean r45 = r15.equals(r0)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            if (r45 != 0) goto L_0x043e
            if (r15 != 0) goto L_0x0440
        L_0x043e:
            java.lang.String r15 = "#E6E6E6"
        L_0x0440:
            r0 = r50
            android.view.View r0 = r0.footer     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r15)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45.setBackgroundColor(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.widget.TextView r45 = new android.widget.TextView     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r45
            r1 = r50
            r1.footerTitle = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            java.lang.String r47 = "footertext"
            java.lang.String r46 = r46.getString(r47)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45.setText(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r46 = 1092616192(0x41200000, float:10.0)
            r45.setTextSize(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "footertextheight"
            int r22 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "footertextcolor"
            java.lang.String r20 = r45.getString(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r45 = ""
            r0 = r20
            r1 = r45
            boolean r45 = r0.equals(r1)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            if (r45 != 0) goto L_0x04a4
            if (r20 != 0) goto L_0x04a6
        L_0x04a4:
            java.lang.String r20 = "#E6E6E6"
        L_0x04a6:
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r20)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45.setTextColor(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.view.ViewGroup$MarginLayoutParams r28 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "footertextwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r28
            r1 = r45
            r2 = r22
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            int r18 = r23 + 20
            int r45 = r21 - r22
            int r45 = r45 / 2
            int r19 = r24 + r45
            r45 = 0
            r46 = 0
            r0 = r28
            r1 = r18
            r2 = r19
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.widget.RelativeLayout$LayoutParams r27 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r27.<init>(r28)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            int r45 = r36 + -5
            r46 = 0
            int r8 = java.lang.Math.max(r45, r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            int r45 = r8 / 2
            r0 = r45
            float r7 = (float) r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.widget.Button r45 = new android.widget.Button     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r45
            r1 = r50
            r1.closeBtn = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "X"
            r45.setText(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r45
            r0.setTextSize(r7)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r9)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45.setTextColor(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r46 = 0
            r47 = 0
            r48 = 0
            r49 = 0
            r45.setPadding(r46, r47, r48, r49)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r12)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45.setBackgroundColor(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            com.Leadbolt.AdController$5 r46 = new com.Leadbolt.AdController$5     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r46
            r1 = r50
            r0.<init>()     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45.setOnClickListener(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.view.ViewGroup$MarginLayoutParams r45 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = 55
            r0 = r45
            r1 = r46
            r0.<init>(r1, r8)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r45
            r1 = r50
            r1.mlpC = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            int r45 = r37 + r38
            int r45 = r45 + -55
            int r10 = r45 + -5
            int r45 = r36 - r8
            int r45 = r45 / 2
            int r45 = r45 + r39
            int r11 = r45 + 2
            r0 = r50
            android.view.ViewGroup$MarginLayoutParams r0 = r0.mlpC     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r46 = 0
            r47 = 0
            r0 = r45
            r1 = r46
            r2 = r47
            r0.setMargins(r10, r11, r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.widget.RelativeLayout$LayoutParams r45 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.view.ViewGroup$MarginLayoutParams r0 = r0.mlpC     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r45
            r1 = r50
            r1.lpC = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            if (r45 == 0) goto L_0x05d4
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            android.view.ViewParent r45 = r45.getParent()     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.view.ViewGroup r45 = (android.view.ViewGroup) r45     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r45
            r1 = r50
            r1.mainViewParent = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.view.ViewGroup r0 = r0.mainViewParent     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r45.removeView(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r45.addView(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
        L_0x05d4:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "maskvisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x0617
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r29
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            if (r45 == 0) goto L_0x0617
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            com.Leadbolt.AdController$6 r46 = new com.Leadbolt.AdController$6     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r46
            r1 = r50
            r0.<init>()     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45.setOnTouchListener(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
        L_0x0617:
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            android.webkit.WebView r0 = r0.webview     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r42
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "titlevisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x06a7
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.toolbar     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r31
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "titletext"
            java.lang.String r45 = r45.getString(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r46 = ""
            boolean r45 = r45.equals(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            if (r45 != 0) goto L_0x067e
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r40
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
        L_0x067e:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "showclose"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x06a7
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r50
            android.widget.RelativeLayout$LayoutParams r0 = r0.lpC     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r47 = r0
            r45.addView(r46, r47)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
        L_0x06a7:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "footervisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x06f9
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.footer     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r16
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "footertext"
            java.lang.String r45 = r45.getString(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r46 = ""
            boolean r45 = r45.equals(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            if (r45 != 0) goto L_0x06f9
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r27
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
        L_0x06f9:
            java.lang.StringBuilder r45 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            r46 = r0
            java.lang.String r47 = "adurl"
            java.lang.String r46 = r46.getString(r47)     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            java.lang.String r46 = java.lang.String.valueOf(r46)     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            r45.<init>(r46)     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            r0 = r50
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            r46 = r0
            java.lang.StringBuilder r45 = r45.append(r46)     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            java.lang.String r45 = r45.toString()     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            r0 = r45
            r1 = r50
            r1.loadUrl = r0     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            r0 = r50
            android.webkit.WebView r0 = r0.webview     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            java.lang.String r0 = r0.loadUrl     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            r46 = r0
            r45.loadUrl(r46)     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
            r45 = 1
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ Exception -> 0x084e, JSONException -> 0x085c }
        L_0x0739:
            android.view.ViewGroup$MarginLayoutParams r25 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            int r0 = r0.sWidth     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            int r0 = r0.sHeight     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r25
            r1 = r45
            r2 = r46
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = 0
            r46 = 0
            r47 = 0
            r48 = 0
            r0 = r25
            r1 = r45
            r2 = r46
            r3 = r47
            r4 = r48
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.widget.RelativeLayout$LayoutParams r26 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r26
            r1 = r25
            r0.<init>(r1)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x091b, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x091b, JSONException -> 0x085c }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r26
            r0.addContentView(r1, r2)     // Catch:{ Exception -> 0x091b, JSONException -> 0x085c }
        L_0x0783:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            java.lang.String r46 = "pollenable"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x087a
            r0 = r50
            boolean r0 = r0.pollingInitialized     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            if (r45 != 0) goto L_0x087a
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            int r0 = r0.pollMax     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 > r1) goto L_0x087a
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            int r0 = r0.pollManual     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 >= r1) goto L_0x087a
            r45 = 1
            r0 = r45
            r1 = r50
            r1.pollingInitialized = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            com.Leadbolt.AdController$OfferPolling r45 = new com.Leadbolt.AdController$OfferPolling     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r45
            r1 = r50
            r0.<init>()     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r45
            r1 = r50
            r1.adPolling = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            android.os.Handler r45 = new android.os.Handler     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45.<init>()     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r45
            r1 = r50
            r1.pollingHandler = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r45 = "LBAdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            java.lang.String r47 = "Polling initialized every "
            r46.<init>(r47)     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            r47 = r0
            java.lang.String r48 = "pollinterval"
            int r47 = r47.getInt(r48)     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            java.lang.String r47 = "s"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            java.lang.String r46 = r46.toString()     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            com.Leadbolt.AdLog.d(r45, r46)     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            r0 = r50
            android.os.Handler r0 = r0.pollingHandler     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            r45 = r0
            r0 = r50
            com.Leadbolt.AdController$OfferPolling r0 = r0.adPolling     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            r46 = r0
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            r47 = r0
            java.lang.String r48 = "pollinterval"
            int r47 = r47.getInt(r48)     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            r0 = r47
            int r0 = r0 * 1000
            r47 = r0
            r0 = r47
            long r0 = (long) r0     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            r47 = r0
            r45.postDelayed(r46, r47)     // Catch:{ Exception -> 0x0833, JSONException -> 0x085c }
            goto L_0x000f
        L_0x0833:
            r45 = move-exception
            goto L_0x000f
        L_0x0836:
            java.lang.String r45 = "Bottom"
            r0 = r45
            boolean r45 = r13.equals(r0)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            if (r45 == 0) goto L_0x0266
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            int r44 = r44 - r45
            if (r44 >= 0) goto L_0x0266
            r44 = 0
            goto L_0x0266
        L_0x084e:
            r14 = move-exception
            r50.closeUnlocker()     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            goto L_0x0739
        L_0x085c:
            r14 = move-exception
            r50.closeUnlocker()
            java.lang.String r45 = "AdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder
            java.lang.String r47 = "JSONException - "
            r46.<init>(r47)
            java.lang.String r47 = r14.getMessage()
            java.lang.StringBuilder r46 = r46.append(r47)
            java.lang.String r46 = r46.toString()
            android.util.Log.d(r45, r46)
            goto L_0x000f
        L_0x087a:
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            int r0 = r0.pollMax     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 <= r1) goto L_0x000f
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            r0 = r50
            int r0 = r0.pollManual     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 < r1) goto L_0x000f
            r45 = 0
            r0 = r45
            r1 = r50
            r1.pollingInitialized = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r50.showManualPoll()     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            goto L_0x000f
        L_0x08ab:
            r14 = move-exception
            r50.closeUnlocker()
            java.lang.String r45 = "LBAdController"
            r0 = r45
            com.Leadbolt.AdLog.printStackTrace(r0, r14)
            java.lang.String r45 = "AdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder
            java.lang.String r47 = "Exception - "
            r46.<init>(r47)
            java.lang.String r47 = r14.getMessage()
            java.lang.StringBuilder r46 = r46.append(r47)
            java.lang.String r46 = r46.toString()
            android.util.Log.d(r45, r46)
            goto L_0x000f
        L_0x08d0:
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r50.closeUnlocker()     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            goto L_0x000f
        L_0x08dd:
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r50.closeUnlocker()     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r0 = r50
            com.Leadbolt.AdListener r0 = r0.listener     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            r45 = r0
            if (r45 == 0) goto L_0x000f
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "onAdFailed triggered"
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ Exception -> 0x090a, JSONException -> 0x085c }
            r0 = r50
            com.Leadbolt.AdListener r0 = r0.listener     // Catch:{ Exception -> 0x090a, JSONException -> 0x085c }
            r45 = r0
            r45.onAdFailed()     // Catch:{ Exception -> 0x090a, JSONException -> 0x085c }
            r45 = 1
            r0 = r45
            r1 = r50
            r1.adLoaded = r0     // Catch:{ Exception -> 0x090a, JSONException -> 0x085c }
            goto L_0x000f
        L_0x090a:
            r14 = move-exception
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "Error while calling onAdFailed"
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            java.lang.String r45 = "LBAdController"
            r0 = r45
            com.Leadbolt.AdLog.printStackTrace(r0, r14)     // Catch:{ JSONException -> 0x085c, Exception -> 0x08ab }
            goto L_0x000f
        L_0x091b:
            r45 = move-exception
            goto L_0x0783
        L_0x091e:
            r45 = move-exception
            goto L_0x0209
        L_0x0921:
            r45 = move-exception
            goto L_0x01c6
        L_0x0924:
            r45 = move-exception
            goto L_0x015f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Leadbolt.AdController.displayAd():void");
    }

    /* access modifiers changed from: private */
    public void linkClicked() {
        int fTitlex;
        AdLog.i(LB_LOG, "linkClicked called");
        if (!this.linkClicked) {
            AdLog.i(LB_LOG, "Loading window...");
            this.homeLoaded = false;
            this.linkClicked = true;
            if (!this.loading) {
                AdLog.i(LB_LOG, "remove the views if required first...");
                try {
                    this.layout.removeView(this.webview);
                    this.layout.removeView(this.toolbar);
                    this.layout.removeView(this.title);
                    this.layout.removeView(this.footer);
                    this.layout.removeView(this.footerTitle);
                    this.layout.removeView(this.backBtn);
                    this.layout.removeView(this.fwdBtn);
                    this.layout.removeView(this.closeBtn);
                    this.layout.removeView(this.homeBtn);
                    this.layout.removeView(this.refreshBtn);
                    if (this.pollBtn != null) {
                        this.layout.removeAllViews();
                    }
                } catch (Exception e) {
                }
                int windowx = this.results.getInt("clickwindowx");
                int windowy = this.results.getInt("clickwindowy");
                ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(this.results.getInt("clickwindowwidth"), this.results.getInt("clickwindowheight"));
                marginLayoutParams.setMargins(windowx, windowy, 0, 0);
                RelativeLayout.LayoutParams wL = new RelativeLayout.LayoutParams(marginLayoutParams);
                int toolbarwidth = this.results.getInt("clicktitlewidth");
                int toolbarheight = this.results.getInt("clicktitleheight");
                int toolbarx = this.results.getInt("clicktitlex");
                int toolbary = this.results.getInt("clicktitley");
                ViewGroup.MarginLayoutParams marginLayoutParams2 = new ViewGroup.MarginLayoutParams(toolbarwidth, toolbarheight);
                marginLayoutParams2.setMargins(toolbarx, toolbary, 0, 0);
                RelativeLayout.LayoutParams tL = new RelativeLayout.LayoutParams(marginLayoutParams2);
                try {
                    this.toolbar.invalidate();
                } catch (Exception e2) {
                }
                try {
                    this.toolbar = new View(this.activity);
                    String clr = this.results.getString("clicktitlecolor");
                    if (clr.equals("") || clr == null) {
                        clr = "#E6E6E6";
                    }
                    this.toolbar.setBackgroundColor(Color.parseColor(clr));
                    try {
                        this.title.invalidate();
                    } catch (Exception e3) {
                    }
                    this.title = new TextView(this.activity);
                    this.title.setText(this.results.getString("clicktitletext"));
                    String clk = this.results.getString("clicktitletextcolor");
                    if (clk.equals("") || clk == null) {
                        clk = "#000000";
                    }
                    this.title.setTextColor(Color.parseColor(clk));
                    ViewGroup.MarginLayoutParams marginLayoutParams3 = new ViewGroup.MarginLayoutParams(this.results.getInt("clicktitletextwidth"), toolbarheight - 2);
                    marginLayoutParams3.setMargins(toolbarx + 20, toolbary + 8, 0, 0);
                    RelativeLayout.LayoutParams tvl = new RelativeLayout.LayoutParams(marginLayoutParams3);
                    int footerx = this.results.getInt("clickfooterx");
                    int footery = this.results.getInt("clickfootery");
                    int footerheight = this.results.getInt("clickfooterheight");
                    ViewGroup.MarginLayoutParams marginLayoutParams4 = new ViewGroup.MarginLayoutParams(this.results.getInt("clickfooterwidth"), footerheight);
                    marginLayoutParams4.setMargins(footerx, footery, 0, 0);
                    RelativeLayout.LayoutParams fL = new RelativeLayout.LayoutParams(marginLayoutParams4);
                    this.footer = new View(this.activity);
                    String fClr = this.results.getString("clickfootercolor");
                    if (fClr.equals("") || fClr == null) {
                        fClr = "#E6E6E6";
                    }
                    this.footer.setBackgroundColor(Color.parseColor(fClr));
                    this.footerTitle = new TextView(this.activity);
                    this.footerTitle.setText(this.results.getString("clickfootertext"));
                    this.footerTitle.setTextSize(10.0f);
                    String fClk = this.results.getString("clickfootertextcolor");
                    if (fClk.equals("") || fClk == null) {
                        fClk = "#000000";
                    }
                    this.footerTitle.setTextColor(Color.parseColor(fClk));
                    ViewGroup.MarginLayoutParams marginLayoutParams5 = new ViewGroup.MarginLayoutParams(this.results.getInt("clickfootertextwidth"), this.results.getInt("clickfootertextheight"));
                    int fTitlex2 = footerx;
                    if (this.results.getInt("shownavigation") == 1) {
                        fTitlex = fTitlex2 + 70;
                    } else {
                        fTitlex = fTitlex2 + 20;
                    }
                    marginLayoutParams5.setMargins(fTitlex, footery + 5, 0, 0);
                    RelativeLayout.LayoutParams fvl = new RelativeLayout.LayoutParams(marginLayoutParams5);
                    int titleBtnHeight = Math.max(toolbarheight - 5, 0);
                    float titleBtnFont = (float) (titleBtnHeight / 2);
                    if (titleBtnFont > 10.0f) {
                        titleBtnFont = 10.0f;
                    }
                    int navBtnHeight = Math.max(footerheight - 5, 0);
                    float navBtnFont = (float) (navBtnHeight / 2);
                    if (navBtnFont > 10.0f) {
                        navBtnFont = 10.0f;
                    }
                    if (this.homeBtn != null) {
                        this.homeBtn.invalidate();
                    }
                    this.homeBtn = new Button(this.activity);
                    this.homeBtn.setText("Back");
                    this.homeBtn.setTextSize(navBtnFont);
                    this.homeBtn.setTextColor(Color.parseColor(clk));
                    this.homeBtn.setPadding(0, 0, 0, 0);
                    this.homeBtn.setBackgroundColor(Color.parseColor(fClr));
                    this.homeBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.loadAd();
                        }
                    });
                    if (this.closeBtn != null) {
                        this.closeBtn.invalidate();
                    }
                    this.closeBtn = new Button(this.activity);
                    this.closeBtn.setText("X");
                    this.closeBtn.setTextSize(titleBtnFont);
                    this.closeBtn.setTextColor(Color.parseColor(clk));
                    this.closeBtn.setPadding(0, 0, 0, 0);
                    this.closeBtn.setBackgroundColor(Color.parseColor(clr));
                    this.closeBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.closeUnlocker();
                            if (AdController.this.listener != null) {
                                try {
                                    AdLog.i(AdController.LB_LOG, "onAdClosed triggered");
                                    AdController.this.listener.onAdClosed();
                                } catch (Exception e) {
                                    AdLog.e(AdController.LB_LOG, "error when onAdClosed triggered");
                                    AdLog.printStackTrace(AdController.LB_LOG, e);
                                }
                            }
                        }
                    });
                    this.fwdBtn = new Button(this.activity);
                    this.fwdBtn.setText(">");
                    this.fwdBtn.setTextSize(navBtnFont);
                    this.fwdBtn.setTextColor(Color.parseColor(clk));
                    this.fwdBtn.setPadding(0, 0, 0, 0);
                    this.fwdBtn.setBackgroundColor(Color.parseColor(fClr));
                    this.fwdBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.goForward();
                        }
                    });
                    if (this.backBtn != null) {
                        this.backBtn.invalidate();
                    }
                    this.backBtn = new Button(this.activity);
                    this.backBtn.setText("<");
                    this.backBtn.setTextSize(navBtnFont);
                    this.backBtn.setTextColor(Color.parseColor(clk));
                    this.backBtn.setPadding(0, 0, 0, 0);
                    this.backBtn.setBackgroundColor(Color.parseColor(fClr));
                    this.backBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.goBack();
                        }
                    });
                    ViewGroup.MarginLayoutParams marginLayoutParams6 = new ViewGroup.MarginLayoutParams(30, navBtnHeight);
                    int btny = ((this.results.getInt("clickfooterheight") - navBtnHeight) / 2) + footery + 3;
                    marginLayoutParams6.setMargins(footerx + 5, btny, 0, 0);
                    RelativeLayout.LayoutParams bB = new RelativeLayout.LayoutParams(marginLayoutParams6);
                    marginLayoutParams6.setMargins(footerx + 5 + 30, btny, 0, 0);
                    RelativeLayout.LayoutParams fB = new RelativeLayout.LayoutParams(marginLayoutParams6);
                    this.layout.addView(this.webview, wL);
                    if (this.listener != null) {
                        try {
                            AdLog.i(LB_LOG, "onAdClicked triggered");
                            this.listener.onAdClicked();
                        } catch (Exception e4) {
                            AdLog.e(LB_LOG, "error when onAdClicked triggered");
                            AdLog.printStackTrace(LB_LOG, e4);
                        }
                    }
                    if (this.results.getInt("clicktitlevisible") == 1) {
                        this.layout.addView(this.toolbar, tL);
                        if (!this.results.getString("clicktitletext").equals("")) {
                            this.layout.addView(this.title, tvl);
                        }
                        if (this.results.getInt("showclose") == 1) {
                            this.mlpC = new ViewGroup.MarginLayoutParams(55, titleBtnHeight);
                            this.mlpC.setMargins(((toolbarwidth + toolbarx) - 55) - 5, ((toolbarheight - titleBtnHeight) / 2) + toolbary + 2, 0, 0);
                            this.lpC = new RelativeLayout.LayoutParams(this.mlpC);
                            this.layout.addView(this.closeBtn, this.lpC);
                        }
                    }
                    if (this.results.getInt("clickfootervisible") == 1) {
                        this.layout.addView(this.footer, fL);
                        if (!this.results.getString("clickfootertext").equals("")) {
                            this.layout.addView(this.footerTitle, fvl);
                        }
                        if (this.results.getInt("shownavigation") == 1) {
                            this.layout.addView(this.backBtn, bB);
                            this.layout.addView(this.fwdBtn, fB);
                        }
                        if (this.results.getInt("showback") == 1) {
                            ViewGroup.MarginLayoutParams marginLayoutParams7 = new ViewGroup.MarginLayoutParams(55, navBtnHeight);
                            marginLayoutParams7.setMargins(((this.results.getInt("clickfooterwidth") + footerx) - 55) - 5, ((footerheight - navBtnHeight) / 2) + footery + 2, 0, 0);
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginLayoutParams7);
                            this.layout.addView(this.homeBtn, layoutParams);
                        }
                    }
                    if (this.results.get("useclickwindow").equals("1")) {
                        try {
                            this.loadUrl = String.valueOf(this.results.getString("adurl")) + this.sectionid;
                            this.webview.loadUrl(this.loadUrl);
                            this.loading = true;
                        } catch (Exception e5) {
                            closeUnlocker();
                            this.loading = false;
                        }
                        try {
                            ViewGroup.MarginLayoutParams marginLayoutParams8 = new ViewGroup.MarginLayoutParams(this.sWidth, this.sHeight);
                            marginLayoutParams8.setMargins(0, 0, 0, 0);
                            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(marginLayoutParams8);
                            try {
                                this.activity.addContentView(this.layout, layoutParams2);
                            } catch (Exception e6) {
                            }
                        } catch (Exception e7) {
                        }
                    }
                    AdLog.i(LB_LOG, "pE = " + this.results.getInt("pollenable") + ", pI = " + this.pollingInitialized + ", pC = " + this.pollCount + ", pM = " + this.pollMax + ", pMl = " + this.pollManual);
                    boolean iP = false;
                    if (this.results.getInt("pollenable") != 1 || this.pollingInitialized || this.pollCount <= 0) {
                        if (this.pollCount > this.pollMax || this.pollCount >= this.pollManual) {
                            if (this.pollCount > this.pollMax && this.pollCount >= this.pollManual) {
                                iP = false;
                            }
                        } else {
                            iP = true;
                        }
                    } else {
                        iP = true;
                    }
                    if (iP) {
                        AdLog.i(LB_LOG, "Polling to be initialized in linkClicked");
                        this.pollingInitialized = true;
                        this.adPolling = new OfferPolling();
                        this.pollingHandler = new Handler();
                        try {
                            AdLog.d(LB_LOG, "Polling initialized every " + this.results.getInt("pollinterval") + "s");
                            this.pollingHandler.postDelayed(this.adPolling, (long) (this.results.getInt("pollinterval") * 1000));
                        } catch (Exception e8) {
                        }
                    } else {
                        AdLog.i(LB_LOG, "Manual Polling in linkClicked");
                        this.pollingInitialized = false;
                        showManualPoll();
                    }
                } catch (JSONException ex) {
                    AdLog.printStackTrace(LB_LOG, ex);
                    AdLog.e(LB_LOG, "JSON Exception - " + ex.getMessage());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void showCloseButton() {
        try {
            this.layout.removeView(this.closeBtn);
        } catch (Exception e) {
        }
        int titleBtnHeight = 0;
        String clr = "#E6E6E6";
        String clk = "#000000";
        if (this.homeLoaded) {
            try {
                titleBtnHeight = Math.max(this.results.getInt("titlewidth") - 5, 0);
                clr = this.results.getString("titlecolor");
                clk = this.results.getString("titletextcolor");
            } catch (Exception e2) {
            }
        } else if (this.linkClicked) {
            try {
                titleBtnHeight = Math.max(this.results.getInt("clicktitlewidth") - 5, 0);
                clr = this.results.getString("clicktitlecolor");
                clk = this.results.getString("clicktitletextcolor");
            } catch (Exception e3) {
            }
        }
        if (clr.equals("") || clr == null) {
            clr = "#E6E6E6";
        }
        if (clk.equals("") || clk == null) {
            clk = "#000000";
        }
        float titleBtnFont = (float) (titleBtnHeight / 2);
        if (titleBtnFont > 10.0f) {
            titleBtnFont = 10.0f;
        }
        if (this.closeBtn != null) {
            this.closeBtn.invalidate();
        }
        this.closeBtn = new Button(this.activity);
        this.closeBtn.setText("X");
        this.closeBtn.setTextSize(titleBtnFont);
        this.closeBtn.setTextColor(Color.parseColor(clk));
        this.closeBtn.setPadding(0, 0, 0, 0);
        this.closeBtn.setBackgroundColor(Color.parseColor(clr));
        this.closeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AdController.this.closeUnlocker();
                if (AdController.this.listener != null) {
                    try {
                        AdLog.i(AdController.LB_LOG, "onAdClosed triggered");
                        AdController.this.listener.onAdClosed();
                    } catch (Exception e) {
                        AdLog.e(AdController.LB_LOG, "error when onAdClosed triggered");
                        AdLog.printStackTrace(AdController.LB_LOG, e);
                    }
                }
            }
        });
        this.mlpC = new ViewGroup.MarginLayoutParams(55, titleBtnHeight);
        if (this.homeLoaded) {
            try {
                this.mlpC.setMargins(((this.results.getInt("titlewidth") + this.results.getInt("titlex")) - 55) - 5, this.results.getInt("titley") + (Math.round((float) (this.results.getInt("titleheight") - titleBtnHeight)) / 2) + 2, 0, 0);
                this.lpC = new RelativeLayout.LayoutParams(this.mlpC);
            } catch (JSONException e4) {
            }
        } else if (this.linkClicked) {
            try {
                this.mlpC.setMargins(((this.results.getInt("clicktitlewidth") + this.results.getInt("clicktitlex")) - 55) - 5, this.results.getInt("clicktitley") + Math.round((float) ((this.results.getInt("clicktitleheight") - titleBtnHeight) / 2)) + 2, 0, 0);
            } catch (JSONException e5) {
            }
        }
        try {
            this.lpC = new RelativeLayout.LayoutParams(this.mlpC);
            if ((this.homeLoaded && this.results.getInt("titlevisible") == 1) || (this.linkClicked && this.results.getInt("clicktitlevisible") == 1)) {
                this.layout.addView(this.closeBtn, this.lpC);
            }
        } catch (Exception e6) {
        }
    }

    /* access modifiers changed from: private */
    public void showManualPoll() {
        this.pollingInitialized = false;
        if (this.adPolling != null) {
            this.adPolling.cancel();
        }
        this.pollBtn = new Button(this.activity);
        this.pollBtn.setText("Refresh");
        int pollBtnHeight = 0;
        String clr = "#E6E6E6";
        String clk = "#000000";
        if (this.linkClicked) {
            try {
                pollBtnHeight = Math.max(this.results.getInt("clickfooterheight") - 5, 0);
                clr = this.results.getString("clickfootercolor");
                clk = this.results.getString("clicktitletextcolor");
                if (clr.equals("") || clr == null) {
                    clr = "#E6E6E6";
                }
                if (clk.equals("") || clk == null) {
                    clk = "#000000";
                }
            } catch (Exception e) {
            }
        }
        float pollBtnFont = (float) (pollBtnHeight / 2);
        if (pollBtnFont > 10.0f) {
            pollBtnFont = 10.0f;
        }
        this.pollBtn.setPadding(0, 0, 0, 0);
        this.pollBtn.setTextSize(pollBtnFont);
        this.pollBtn.setTextColor(Color.parseColor(clk));
        this.pollBtn.setBackgroundColor(Color.parseColor(clr));
        this.pollBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AdController.this.loadingDialog = ProgressDialog.show(AdController.this.activity, "", "Checking....Please Wait!", true);
                if (AdController.this.adPolling == null) {
                    AdController.this.adPolling = new OfferPolling();
                }
                AdController.this.pollingHandler = new Handler();
                try {
                    AdLog.i(AdController.LB_LOG, "Manually Polling");
                    AdController.this.pollingHandler.post(AdController.this.adPolling);
                } catch (Exception e) {
                    AdLog.e(AdController.LB_LOG, "Error in manual polling - " + e.getMessage());
                    AdLog.printStackTrace(AdController.LB_LOG, e);
                }
            }
        });
        if (this.linkClicked) {
            try {
                int footerx = this.results.getInt("clickfooterx");
                int footery = this.results.getInt("clickfootery");
                int footerheight = this.results.getInt("clickfooterheight");
                ViewGroup.MarginLayoutParams hB = new ViewGroup.MarginLayoutParams(55, pollBtnHeight);
                hB.setMargins((this.results.getInt("clickfooterwidth") + footerx) - 120, ((footerheight - pollBtnHeight) / 2) + footery + 2, 0, 0);
                this.layout.addView(this.pollBtn, new RelativeLayout.LayoutParams(hB));
            } catch (Exception e2) {
                AdLog.e(LB_LOG, "Error (add Manual Poll btn before click): " + e2.getMessage());
                AdLog.printStackTrace(LB_LOG, e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void closeUnlocker() {
        this.adDestroyed = true;
        if (this.req != null) {
            this.req.cancel(true);
        }
        AdLog.i(LB_LOG, "closeUnlocker called");
        this.homeLoaded = false;
        this.linkClicked = false;
        this.pollingInitialized = false;
        try {
            this.layout.removeAllViews();
            if (this.adPolling != null) {
                this.adPolling.cancel();
            }
            if (this.mainView != null) {
                this.mainView.setOnTouchListener(null);
            }
        } catch (Exception e) {
            AdLog.printStackTrace(LB_LOG, e);
            AdLog.e(LB_LOG, "CloseUnlocker error - " + e.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void goBack() {
        if (this.webview.canGoBack()) {
            this.webview.goBack();
        }
    }

    /* access modifiers changed from: private */
    public void goForward() {
        if (this.webview.canGoForward()) {
            this.webview.goForward();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(13:149|150|151|152|(3:154|155|156)|157|158|159|(1:163)|164|165|166|(2:168|219)(1:218)) */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x08fb, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:?, code lost:
        com.Leadbolt.AdLog.printStackTrace(com.Leadbolt.AdController.LB_LOG, r14);
        r17 = false;
        r1.results = new org.json.JSONObject();
        r1.initialized = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x091e, code lost:
        if (r0.listener != null) goto L_0x0920;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:?, code lost:
        com.Leadbolt.AdLog.i(com.Leadbolt.AdController.LB_LOG, "onAdFailed triggered");
        r0.listener.onAdFailed();
        r1.adLoaded = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:?, code lost:
        com.Leadbolt.AdLog.d(com.Leadbolt.AdController.LB_LOG, "Results - " + r0.results.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x096c, code lost:
        r1.initialized = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0974, code lost:
        r1.requestInProgress = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x0982, code lost:
        if (r0.useNotification != false) goto L_0x0984;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x0984, code lost:
        r12 = r37.edit();
        r12.putLong("SD_NOTIFICATION_REQUESTED_" + r0.sectionid, java.lang.System.currentTimeMillis());
        r12.commit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x09eb, code lost:
        r55 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:?, code lost:
        com.Leadbolt.AdLog.d(com.Leadbolt.AdController.LB_LOG, "Results - " + r0.results.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x0a20, code lost:
        r1.initialized = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x0a28, code lost:
        r1.requestInProgress = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x0a36, code lost:
        if (r0.useNotification != false) goto L_0x0a38;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x0a38, code lost:
        r12 = r37.edit();
        r12.putLong("SD_NOTIFICATION_REQUESTED_" + r0.sectionid, java.lang.System.currentTimeMillis());
        r12.commit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0a5f, code lost:
        throw r55;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0a80, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:?, code lost:
        com.Leadbolt.AdLog.i(com.Leadbolt.AdController.LB_LOG, "Error while calling onAdFailed");
        com.Leadbolt.AdLog.printStackTrace(com.Leadbolt.AdController.LB_LOG, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x0a92, code lost:
        r1.initialized = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x0a9d, code lost:
        r1.initialized = false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void makeLBRequest() {
        /*
            r62 = this;
            r0 = r62
            boolean r0 = r0.requestInProgress
            r55 = r0
            if (r55 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            r55 = 1
            r0 = r55
            r1 = r62
            r1.requestInProgress = r0
            java.util.Random r38 = new java.util.Random
            r38.<init>()
            r55 = 151(0x97, float:2.12E-43)
            r0 = r38
            r1 = r55
            int r55 = r0.nextInt(r1)
            r0 = r55
            int r0 = r0 + 250
            r39 = r0
            r0 = r39
            long r0 = (long) r0
            r55 = r0
            java.lang.Thread.sleep(r55)     // Catch:{ Exception -> 0x0ab1 }
        L_0x002e:
            r0 = r62
            android.app.Activity r0 = r0.activity
            r55 = r0
            if (r55 != 0) goto L_0x0676
            r0 = r62
            android.content.Context r0 = r0.context
            r49 = r0
        L_0x003c:
            java.lang.String r55 = "Preference"
            r56 = 2
            r0 = r49
            r1 = r55
            r2 = r56
            android.content.SharedPreferences r37 = r0.getSharedPreferences(r1, r2)
            r0 = r62
            android.app.Activity r0 = r0.activity
            r55 = r0
            if (r55 == 0) goto L_0x0107
            android.util.DisplayMetrics r10 = new android.util.DisplayMetrics
            r10.<init>()
            r0 = r62
            android.app.Activity r0 = r0.activity
            r55 = r0
            android.view.WindowManager r55 = r55.getWindowManager()
            android.view.Display r55 = r55.getDefaultDisplay()
            r0 = r55
            r0.getMetrics(r10)
            android.graphics.Rect r40 = new android.graphics.Rect
            r40.<init>()
            r0 = r62
            android.app.Activity r0 = r0.activity
            r55 = r0
            android.view.Window r53 = r55.getWindow()
            android.view.View r55 = r53.getDecorView()
            r0 = r55
            r1 = r40
            r0.getWindowVisibleDisplayFrame(r1)
            r0 = r40
            int r0 = r0.top
            r45 = r0
            r55 = 16908290(0x1020002, float:2.3877235E-38)
            r0 = r53
            r1 = r55
            android.view.View r55 = r0.findViewById(r1)
            int r7 = r55.getTop()
            r0 = r45
            if (r7 <= r0) goto L_0x067e
            int r47 = r7 - r45
        L_0x009f:
            int r0 = r10.widthPixels
            r55 = r0
            r0 = r55
            r1 = r62
            r1.sWidth = r0
            int r0 = r10.heightPixels
            r55 = r0
            int r55 = r55 - r45
            int r55 = r55 - r47
            r0 = r55
            r1 = r62
            r1.sHeight = r0
            java.lang.String r55 = "LBAdController"
            java.lang.StringBuilder r56 = new java.lang.StringBuilder
            java.lang.String r57 = "Device Width = "
            r56.<init>(r57)
            r0 = r62
            int r0 = r0.sWidth
            r57 = r0
            java.lang.StringBuilder r56 = r56.append(r57)
            java.lang.String r57 = ", Height = "
            java.lang.StringBuilder r56 = r56.append(r57)
            r0 = r62
            int r0 = r0.sHeight
            r57 = r0
            java.lang.StringBuilder r56 = r56.append(r57)
            java.lang.String r56 = r56.toString()
            com.Leadbolt.AdLog.d(r55, r56)
            java.lang.String r55 = "LBAdController"
            java.lang.StringBuilder r56 = new java.lang.StringBuilder
            java.lang.String r57 = "SBH = "
            r56.<init>(r57)
            r0 = r56
            r1 = r45
            java.lang.StringBuilder r56 = r0.append(r1)
            java.lang.String r57 = ", TBH = "
            java.lang.StringBuilder r56 = r56.append(r57)
            r0 = r56
            r1 = r47
            java.lang.StringBuilder r56 = r0.append(r1)
            java.lang.String r56 = r56.toString()
            com.Leadbolt.AdLog.d(r55, r56)
        L_0x0107:
            org.apache.http.params.BasicHttpParams r35 = new org.apache.http.params.BasicHttpParams
            r35.<init>()
            java.lang.String r55 = "http.protocol.version"
            org.apache.http.HttpVersion r56 = org.apache.http.HttpVersion.HTTP_1_1
            r0 = r35
            r1 = r55
            r2 = r56
            r0.setParameter(r1, r2)
            org.apache.http.impl.client.DefaultHttpClient r19 = new org.apache.http.impl.client.DefaultHttpClient
            r0 = r19
            r1 = r35
            r0.<init>(r1)
            java.util.Calendar r4 = java.util.Calendar.getInstance()
            r0 = r62
            android.app.Activity r0 = r0.activity
            r55 = r0
            if (r55 == 0) goto L_0x0682
            r0 = r62
            android.app.Activity r0 = r0.activity
            r55 = r0
            android.content.Context r55 = r55.getBaseContext()
            java.lang.String r56 = "phone"
            java.lang.Object r55 = r55.getSystemService(r56)
            android.telephony.TelephonyManager r55 = (android.telephony.TelephonyManager) r55
            r0 = r55
            r1 = r62
            r1.tm = r0
        L_0x0146:
            android.content.ContentResolver r55 = r49.getContentResolver()
            java.lang.String r56 = "android_id"
            java.lang.String r8 = android.provider.Settings.Secure.getString(r55, r56)
            r55 = 2
            r0 = r55
            java.lang.String[] r0 = new java.lang.String[r0]
            r51 = r0
            r55 = 0
            java.lang.String r56 = "http://ad.leadboltapps.net"
            r51[r55] = r56
            r55 = 1
            java.lang.String r56 = "http://ad.leadbolt.net"
            r51[r55] = r56
            r17 = 0
            r21 = 0
        L_0x0168:
            r0 = r51
            int r0 = r0.length
            r55 = r0
            r0 = r21
            r1 = r55
            if (r0 >= r1) goto L_0x0008
            if (r17 != 0) goto L_0x0008
            r55 = r51[r21]
            r0 = r55
            r1 = r62
            r1.domain = r0
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            r0 = r62
            java.lang.String r0 = r0.domain
            r56 = r0
            java.lang.String r56 = java.lang.String.valueOf(r56)
            r55.<init>(r56)
            java.lang.String r56 = "/show_app.conf?"
            java.lang.StringBuilder r55 = r55.append(r56)
            java.lang.String r50 = r55.toString()
            r0 = r62
            boolean r0 = r0.useNotification
            r55 = r0
            if (r55 == 0) goto L_0x0698
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            r0 = r62
            java.lang.String r0 = r0.domain
            r56 = r0
            java.lang.String r56 = java.lang.String.valueOf(r56)
            r55.<init>(r56)
            java.lang.String r56 = "/show_notification?"
            java.lang.StringBuilder r55 = r55.append(r56)
            java.lang.String r50 = r55.toString()
        L_0x01b7:
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            java.lang.String r56 = "&section_id="
            r55.<init>(r56)
            r0 = r62
            java.lang.String r0 = r0.sectionid
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)
            java.lang.String r56 = "&app_id="
            java.lang.StringBuilder r55 = r55.append(r56)
            r0 = r62
            java.lang.String r0 = r0.appId
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)
            java.lang.String r18 = r55.toString()
            r0 = r62
            boolean r0 = r0.useNotification
            r55 = r0
            if (r55 == 0) goto L_0x02a1
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            java.lang.String r56 = "SD_ITERATION_COUNTER_"
            r55.<init>(r56)
            r0 = r62
            java.lang.String r0 = r0.sectionid
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)
            java.lang.String r55 = r55.toString()
            java.lang.String r56 = "0"
            r0 = r37
            r1 = r55
            r2 = r56
            java.lang.String r55 = r0.getString(r1, r2)
            java.lang.Integer r55 = java.lang.Integer.valueOf(r55)
            int r24 = r55.intValue()
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            java.lang.String r56 = java.lang.String.valueOf(r18)
            r55.<init>(r56)
            java.lang.String r56 = "&iteration_counter="
            java.lang.StringBuilder r55 = r55.append(r56)
            r0 = r55
            r1 = r24
            java.lang.StringBuilder r55 = r0.append(r1)
            java.lang.String r18 = r55.toString()
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            java.lang.String r56 = java.lang.String.valueOf(r18)
            r55.<init>(r56)
            java.lang.String r56 = "&launch_type="
            java.lang.StringBuilder r55 = r55.append(r56)
            r0 = r62
            java.lang.String r0 = r0.notificationLaunchType
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)
            java.lang.String r18 = r55.toString()
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            java.lang.String r56 = "SD_NOTIFICATION_FIRED_"
            r55.<init>(r56)
            r0 = r62
            java.lang.String r0 = r0.sectionid
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)
            java.lang.String r55 = r55.toString()
            r56 = -1
            r0 = r37
            r1 = r55
            r2 = r56
            long r15 = r0.getLong(r1, r2)
            r55 = -1
            int r55 = (r15 > r55 ? 1 : (r15 == r55 ? 0 : -1))
            if (r55 != 0) goto L_0x06bb
            r23 = -1
        L_0x026e:
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            java.lang.String r56 = java.lang.String.valueOf(r18)
            r55.<init>(r56)
            java.lang.String r56 = "&notification_fired="
            java.lang.StringBuilder r55 = r55.append(r56)
            r0 = r55
            r1 = r23
            java.lang.StringBuilder r55 = r0.append(r1)
            java.lang.String r18 = r55.toString()
            java.lang.String r55 = "LBAdController"
            java.lang.StringBuilder r56 = new java.lang.StringBuilder
            java.lang.String r57 = "NotificationFired = "
            r56.<init>(r57)
            r0 = r56
            r1 = r23
            java.lang.StringBuilder r56 = r0.append(r1)
            java.lang.String r56 = r56.toString()
            com.Leadbolt.AdLog.d(r55, r56)
        L_0x02a1:
            r0 = r62
            boolean r0 = r0.loadIcon
            r55 = r0
            if (r55 == 0) goto L_0x02e3
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            java.lang.String r56 = "SD_ICON_DISPLAY_"
            r55.<init>(r56)
            r0 = r62
            java.lang.String r0 = r0.sectionid
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)
            java.lang.String r55 = r55.toString()
            r56 = 0
            r0 = r37
            r1 = r55
            r2 = r56
            int r9 = r0.getInt(r1, r2)
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            java.lang.String r56 = java.lang.String.valueOf(r18)
            r55.<init>(r56)
            java.lang.String r56 = "&icon_displayed_count="
            java.lang.StringBuilder r55 = r55.append(r56)
            r0 = r55
            java.lang.StringBuilder r55 = r0.append(r9)
            java.lang.String r18 = r55.toString()
        L_0x02e3:
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            java.lang.String r56 = java.lang.String.valueOf(r50)
            r55.<init>(r56)
            java.lang.String r56 = "&get="
            java.lang.StringBuilder r55 = r55.append(r56)
            java.lang.String r56 = r18.trim()
            java.lang.String r56 = com.Leadbolt.AdEncryption.encrypt(r56)
            java.lang.String r56 = r56.trim()
            java.lang.StringBuilder r55 = r55.append(r56)
            java.lang.String r50 = r55.toString()
            java.lang.String r55 = "LBAdController"
            java.lang.StringBuilder r56 = new java.lang.StringBuilder
            java.lang.String r57 = "Get = "
            r56.<init>(r57)
            java.lang.String r57 = r18.trim()
            java.lang.String r57 = com.Leadbolt.AdEncryption.encrypt(r57)
            java.lang.StringBuilder r56 = r56.append(r57)
            java.lang.String r56 = r56.toString()
            com.Leadbolt.AdLog.d(r55, r56)
            org.apache.http.client.methods.HttpPost r20 = new org.apache.http.client.methods.HttpPost
            r0 = r20
            r1 = r50
            r0.<init>(r1)
            java.util.ArrayList r31 = new java.util.ArrayList
            r55 = 2
            r0 = r31
            r1 = r55
            r0.<init>(r1)
            r0 = r62
            java.lang.String r0 = r0.subid
            r55 = r0
            if (r55 == 0) goto L_0x0352
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r56 = "subid"
            r0 = r62
            java.lang.String r0 = r0.subid
            r57 = r0
            r55.<init>(r56, r57)
            r0 = r31
            r1 = r55
            r0.add(r1)
        L_0x0352:
            r0 = r62
            java.util.List<org.apache.http.NameValuePair> r0 = r0.tokens
            r55 = r0
            if (r55 == 0) goto L_0x039a
            java.lang.String r46 = ""
            r25 = 0
        L_0x035e:
            r0 = r62
            java.util.List<org.apache.http.NameValuePair> r0 = r0.tokens     // Catch:{ Exception -> 0x0726 }
            r55 = r0
            int r55 = r55.size()     // Catch:{ Exception -> 0x0726 }
            r0 = r25
            r1 = r55
            if (r0 < r1) goto L_0x06d5
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0726 }
            java.lang.String r56 = "tokens"
            r0 = r55
            r1 = r56
            r2 = r46
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0726 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x0726 }
            java.lang.String r55 = "LBAdController"
            java.lang.StringBuilder r56 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0726 }
            java.lang.String r57 = "Token Str = "
            r56.<init>(r57)     // Catch:{ Exception -> 0x0726 }
            r0 = r56
            r1 = r46
            java.lang.StringBuilder r56 = r0.append(r1)     // Catch:{ Exception -> 0x0726 }
            java.lang.String r56 = r56.toString()     // Catch:{ Exception -> 0x0726 }
            com.Leadbolt.AdLog.i(r55, r56)     // Catch:{ Exception -> 0x0726 }
        L_0x039a:
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref1"
            r0 = r55
            r1 = r56
            r0.<init>(r1, r8)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref2"
            java.lang.String r57 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x07a2 }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref3"
            java.lang.String r57 = "Android"
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref4"
            java.lang.String r57 = r62.getLocalIpAddress()     // Catch:{ Exception -> 0x07a2 }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref5"
            java.lang.StringBuilder r57 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07a2 }
            r57.<init>()     // Catch:{ Exception -> 0x07a2 }
            r58 = 15
            r0 = r58
            int r58 = r4.get(r0)     // Catch:{ Exception -> 0x07a2 }
            java.lang.StringBuilder r57 = r57.append(r58)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r57 = r57.toString()     // Catch:{ Exception -> 0x07a2 }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref6"
            java.lang.StringBuilder r57 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07a2 }
            r57.<init>()     // Catch:{ Exception -> 0x07a2 }
            long r58 = r4.getTimeInMillis()     // Catch:{ Exception -> 0x07a2 }
            r60 = 1000(0x3e8, double:4.94E-321)
            long r58 = r58 / r60
            r0 = r58
            int r0 = (int) r0     // Catch:{ Exception -> 0x07a2 }
            r58 = r0
            java.lang.StringBuilder r57 = r57.append(r58)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r57 = r57.toString()     // Catch:{ Exception -> 0x07a2 }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref7"
            java.lang.StringBuilder r57 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07a2 }
            r57.<init>()     // Catch:{ Exception -> 0x07a2 }
            r0 = r62
            int r0 = r0.sWidth     // Catch:{ Exception -> 0x07a2 }
            r58 = r0
            java.lang.StringBuilder r57 = r57.append(r58)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r57 = r57.toString()     // Catch:{ Exception -> 0x07a2 }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref8"
            java.lang.StringBuilder r57 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07a2 }
            r57.<init>()     // Catch:{ Exception -> 0x07a2 }
            r0 = r62
            int r0 = r0.sHeight     // Catch:{ Exception -> 0x07a2 }
            r58 = r0
            java.lang.StringBuilder r57 = r57.append(r58)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r57 = r57.toString()     // Catch:{ Exception -> 0x07a2 }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            r0 = r62
            boolean r0 = r0.useLocation     // Catch:{ Exception -> 0x07a2 }
            r55 = r0
            if (r55 == 0) goto L_0x04c1
            java.lang.String r55 = "location"
            r0 = r49
            r1 = r55
            java.lang.Object r27 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x0abf }
            android.location.LocationManager r27 = (android.location.LocationManager) r27     // Catch:{ Exception -> 0x0abf }
            java.lang.String r55 = "gps"
            r0 = r27
            r1 = r55
            android.location.Location r28 = r0.getLastKnownLocation(r1)     // Catch:{ Exception -> 0x0abf }
            double r55 = r28.getLongitude()     // Catch:{ Exception -> 0x0abf }
            java.lang.String r29 = java.lang.String.valueOf(r55)     // Catch:{ Exception -> 0x0abf }
            double r55 = r28.getLatitude()     // Catch:{ Exception -> 0x0abf }
            java.lang.String r26 = java.lang.String.valueOf(r55)     // Catch:{ Exception -> 0x0abf }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0abf }
            java.lang.String r56 = "ref9"
            r0 = r55
            r1 = r56
            r2 = r26
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0abf }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x0abf }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0abf }
            java.lang.String r56 = "ref10"
            r0 = r55
            r1 = r56
            r2 = r29
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0abf }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x0abf }
        L_0x04c1:
            r0 = r62
            boolean r0 = r0.dataretrieve     // Catch:{ Exception -> 0x07a2 }
            r55 = r0
            if (r55 == 0) goto L_0x0511
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0abc }
            java.lang.String r56 = "ref11"
            r0 = r62
            android.telephony.TelephonyManager r0 = r0.tm     // Catch:{ Exception -> 0x0abc }
            r57 = r0
            java.lang.String r57 = r57.getNetworkCountryIso()     // Catch:{ Exception -> 0x0abc }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x0abc }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x0abc }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0abc }
            java.lang.String r56 = "ref12"
            r0 = r62
            android.telephony.TelephonyManager r0 = r0.tm     // Catch:{ Exception -> 0x0abc }
            r57 = r0
            java.lang.String r57 = r57.getNetworkOperator()     // Catch:{ Exception -> 0x0abc }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x0abc }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x0abc }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0abc }
            java.lang.String r56 = "ref13"
            r0 = r62
            android.telephony.TelephonyManager r0 = r0.tm     // Catch:{ Exception -> 0x0abc }
            r57 = r0
            java.lang.String r57 = r57.getNetworkOperatorName()     // Catch:{ Exception -> 0x0abc }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x0abc }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x0abc }
        L_0x0511:
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref15"
            java.lang.String r57 = "3"
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref16"
            java.lang.String r57 = "01"
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref17"
            r0 = r62
            android.telephony.TelephonyManager r0 = r0.tm     // Catch:{ Exception -> 0x07a2 }
            r57 = r0
            java.lang.String r57 = r57.getDeviceId()     // Catch:{ Exception -> 0x07a2 }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref18"
            java.lang.String r57 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x07a2 }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref19"
            java.lang.String r57 = android.os.Build.MODEL     // Catch:{ Exception -> 0x07a2 }
            r55.<init>(r56, r57)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r55 = "connectivity"
            r0 = r49
            r1 = r55
            java.lang.Object r5 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x07a2 }
            android.net.ConnectivityManager r5 = (android.net.ConnectivityManager) r5     // Catch:{ Exception -> 0x07a2 }
            r55 = 0
            r0 = r55
            android.net.NetworkInfo r55 = r5.getNetworkInfo(r0)     // Catch:{ Exception -> 0x07a2 }
            android.net.NetworkInfo$State r30 = r55.getState()     // Catch:{ Exception -> 0x07a2 }
            r55 = 1
            r0 = r55
            android.net.NetworkInfo r55 = r5.getNetworkInfo(r0)     // Catch:{ Exception -> 0x07a2 }
            android.net.NetworkInfo$State r52 = r55.getState()     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r32 = ""
            android.net.NetworkInfo$State r55 = android.net.NetworkInfo.State.CONNECTED     // Catch:{ Exception -> 0x07a2 }
            r0 = r52
            r1 = r55
            if (r0 == r1) goto L_0x059f
            android.net.NetworkInfo$State r55 = android.net.NetworkInfo.State.CONNECTING     // Catch:{ Exception -> 0x07a2 }
            r0 = r52
            r1 = r55
            if (r0 != r1) goto L_0x0737
        L_0x059f:
            java.lang.String r32 = "wifi"
        L_0x05a1:
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref20"
            r0 = r55
            r1 = r56
            r2 = r32
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            r0 = r62
            android.telephony.TelephonyManager r0 = r0.tm     // Catch:{ Exception -> 0x07a2 }
            r55 = r0
            int r43 = r55.getSimState()     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r44 = ""
            switch(r43) {
                case 0: goto L_0x075f;
                case 1: goto L_0x074b;
                case 2: goto L_0x0753;
                case 3: goto L_0x0757;
                case 4: goto L_0x074f;
                case 5: goto L_0x075b;
                default: goto L_0x05c4;
            }     // Catch:{ Exception -> 0x07a2 }
        L_0x05c4:
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref21"
            r0 = r55
            r1 = r56
            r2 = r44
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x07a2 }
            r0 = r31
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "LBAdController"
            java.lang.StringBuilder r57 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r55 = "r20 - "
            r0 = r57
            r1 = r55
            r0.<init>(r1)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r55 = "wifi"
            r0 = r32
            r1 = r55
            boolean r55 = r0.equals(r1)     // Catch:{ Exception -> 0x07a2 }
            if (r55 == 0) goto L_0x0763
            java.lang.String r55 = "w"
        L_0x05f3:
            r0 = r57
            r1 = r55
            java.lang.StringBuilder r55 = r0.append(r1)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r57 = ", r21 - "
            r0 = r55
            r1 = r57
            java.lang.StringBuilder r55 = r0.append(r1)     // Catch:{ Exception -> 0x07a2 }
            r0 = r55
            r1 = r44
            java.lang.StringBuilder r55 = r0.append(r1)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x07a2 }
            r0 = r56
            r1 = r55
            com.Leadbolt.AdLog.d(r0, r1)     // Catch:{ Exception -> 0x07a2 }
            java.util.ArrayList r34 = new java.util.ArrayList     // Catch:{ Exception -> 0x07a2 }
            r55 = 2
            r0 = r34
            r1 = r55
            r0.<init>(r1)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r41 = ""
            r54 = 0
        L_0x0627:
            int r55 = r31.size()     // Catch:{ Exception -> 0x07a2 }
            r0 = r54
            r1 = r55
            if (r0 < r1) goto L_0x0767
            r55 = 0
            int r56 = r41.length()     // Catch:{ Exception -> 0x07a2 }
            int r56 = r56 + -1
            r0 = r41
            r1 = r55
            r2 = r56
            java.lang.String r55 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r41 = com.Leadbolt.AdEncryption.encrypt(r55)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.message.BasicNameValuePair r55 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "ref"
            r0 = r55
            r1 = r56
            r2 = r41
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x07a2 }
            r0 = r34
            r1 = r55
            r0.add(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.client.entity.UrlEncodedFormEntity r55 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x07a2 }
            r0 = r55
            r1 = r34
            r0.<init>(r1)     // Catch:{ Exception -> 0x07a2 }
            r0 = r20
            r1 = r55
            r0.setEntity(r1)     // Catch:{ Exception -> 0x07a2 }
        L_0x066b:
            r6 = 0
        L_0x066c:
            r55 = 10
            r0 = r55
            if (r6 < r0) goto L_0x07ac
        L_0x0672:
            int r21 = r21 + 1
            goto L_0x0168
        L_0x0676:
            r0 = r62
            android.app.Activity r0 = r0.activity
            r49 = r0
            goto L_0x003c
        L_0x067e:
            r47 = 0
            goto L_0x009f
        L_0x0682:
            r0 = r62
            android.content.Context r0 = r0.context
            r55 = r0
            java.lang.String r56 = "phone"
            java.lang.Object r55 = r55.getSystemService(r56)
            android.telephony.TelephonyManager r55 = (android.telephony.TelephonyManager) r55
            r0 = r55
            r1 = r62
            r1.tm = r0
            goto L_0x0146
        L_0x0698:
            r0 = r62
            boolean r0 = r0.loadIcon
            r55 = r0
            if (r55 == 0) goto L_0x01b7
            java.lang.StringBuilder r55 = new java.lang.StringBuilder
            r0 = r62
            java.lang.String r0 = r0.domain
            r56 = r0
            java.lang.String r56 = java.lang.String.valueOf(r56)
            r55.<init>(r56)
            java.lang.String r56 = "/show_app_icon.conf?"
            java.lang.StringBuilder r55 = r55.append(r56)
            java.lang.String r50 = r55.toString()
            goto L_0x01b7
        L_0x06bb:
            long r55 = java.lang.System.currentTimeMillis()
            r57 = 1000(0x3e8, double:4.94E-321)
            long r55 = r55 / r57
            r0 = r55
            int r0 = (int) r0
            r55 = r0
            r56 = 1000(0x3e8, double:4.94E-321)
            long r56 = r15 / r56
            r0 = r56
            int r0 = (int) r0
            r56 = r0
            int r23 = r55 - r56
            goto L_0x026e
        L_0x06d5:
            r0 = r62
            java.util.List<org.apache.http.NameValuePair> r0 = r0.tokens     // Catch:{ Exception -> 0x0726 }
            r55 = r0
            r0 = r55
            r1 = r25
            java.lang.Object r48 = r0.get(r1)     // Catch:{ Exception -> 0x0726 }
            org.apache.http.message.BasicNameValuePair r48 = (org.apache.http.message.BasicNameValuePair) r48     // Catch:{ Exception -> 0x0726 }
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0726 }
            java.lang.String r56 = java.lang.String.valueOf(r46)     // Catch:{ Exception -> 0x0726 }
            r55.<init>(r56)     // Catch:{ Exception -> 0x0726 }
            java.lang.String r56 = r48.getName()     // Catch:{ Exception -> 0x0726 }
            byte[] r56 = r56.getBytes()     // Catch:{ Exception -> 0x0726 }
            r57 = 0
            java.lang.String r56 = android.util.Base64.encodeToString(r56, r57)     // Catch:{ Exception -> 0x0726 }
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x0726 }
            java.lang.String r56 = ":"
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x0726 }
            java.lang.String r56 = r48.getValue()     // Catch:{ Exception -> 0x0726 }
            byte[] r56 = r56.getBytes()     // Catch:{ Exception -> 0x0726 }
            r57 = 0
            java.lang.String r56 = android.util.Base64.encodeToString(r56, r57)     // Catch:{ Exception -> 0x0726 }
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x0726 }
            java.lang.String r56 = ","
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x0726 }
            java.lang.String r46 = r55.toString()     // Catch:{ Exception -> 0x0726 }
            int r25 = r25 + 1
            goto L_0x035e
        L_0x0726:
            r11 = move-exception
            java.lang.String r55 = "LBAdController"
            java.lang.String r56 = "Error while adding tokens"
            com.Leadbolt.AdLog.e(r55, r56)
            java.lang.String r55 = "LBAdController"
            r0 = r55
            com.Leadbolt.AdLog.printStackTrace(r0, r11)
            goto L_0x039a
        L_0x0737:
            android.net.NetworkInfo$State r55 = android.net.NetworkInfo.State.CONNECTED     // Catch:{ Exception -> 0x07a2 }
            r0 = r30
            r1 = r55
            if (r0 == r1) goto L_0x0747
            android.net.NetworkInfo$State r55 = android.net.NetworkInfo.State.CONNECTING     // Catch:{ Exception -> 0x07a2 }
            r0 = r30
            r1 = r55
            if (r0 != r1) goto L_0x05a1
        L_0x0747:
            java.lang.String r32 = "carrier"
            goto L_0x05a1
        L_0x074b:
            java.lang.String r44 = "no_sim"
            goto L_0x05c4
        L_0x074f:
            java.lang.String r44 = "sim_carrier_locked"
            goto L_0x05c4
        L_0x0753:
            java.lang.String r44 = "sim_user_locked"
            goto L_0x05c4
        L_0x0757:
            java.lang.String r44 = "sim_puk_locked"
            goto L_0x05c4
        L_0x075b:
            java.lang.String r44 = "sim_ok"
            goto L_0x05c4
        L_0x075f:
            java.lang.String r44 = "sim_unknown"
            goto L_0x05c4
        L_0x0763:
            java.lang.String r55 = "c"
            goto L_0x05f3
        L_0x0767:
            r0 = r31
            r1 = r54
            java.lang.Object r48 = r0.get(r1)     // Catch:{ Exception -> 0x07a2 }
            org.apache.http.NameValuePair r48 = (org.apache.http.NameValuePair) r48     // Catch:{ Exception -> 0x07a2 }
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = java.lang.String.valueOf(r41)     // Catch:{ Exception -> 0x07a2 }
            r55.<init>(r56)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = r48.getName()     // Catch:{ Exception -> 0x07a2 }
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "="
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = r48.getValue()     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = java.net.URLEncoder.encode(r56)     // Catch:{ Exception -> 0x07a2 }
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r56 = "&"
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x07a2 }
            java.lang.String r41 = r55.toString()     // Catch:{ Exception -> 0x07a2 }
            int r54 = r54 + 1
            goto L_0x0627
        L_0x07a2:
            r11 = move-exception
            java.lang.String r55 = "LBAdController"
            r0 = r55
            com.Leadbolt.AdLog.printStackTrace(r0, r11)
            goto L_0x066b
        L_0x07ac:
            if (r17 != 0) goto L_0x0672
            r36 = 0
            org.apache.http.HttpResponse r42 = r19.execute(r20)     // Catch:{ Exception -> 0x08fb }
            org.apache.http.StatusLine r55 = r42.getStatusLine()     // Catch:{ Exception -> 0x08fb }
            int r55 = r55.getStatusCode()     // Catch:{ Exception -> 0x08fb }
            r56 = 200(0xc8, float:2.8E-43)
            r0 = r55
            r1 = r56
            if (r0 != r1) goto L_0x0867
            r17 = 1
            org.apache.http.HttpEntity r13 = r42.getEntity()     // Catch:{ Exception -> 0x08fb }
            if (r13 == 0) goto L_0x0867
            java.io.InputStream r22 = r13.getContent()     // Catch:{ Exception -> 0x08fb }
            java.lang.String r46 = convertStreamToString(r22)     // Catch:{ Exception -> 0x08fb }
            org.json.JSONObject r55 = new org.json.JSONObject     // Catch:{ Exception -> 0x08fb }
            r0 = r55
            r1 = r46
            r0.<init>(r1)     // Catch:{ Exception -> 0x08fb }
            r0 = r55
            r1 = r62
            r1.results = r0     // Catch:{ Exception -> 0x08fb }
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0ab9 }
            r55 = r0
            java.lang.String r56 = "usenative"
            java.lang.Object r55 = r55.get(r56)     // Catch:{ JSONException -> 0x0ab9 }
            java.lang.String r56 = "1"
            boolean r55 = r55.equals(r56)     // Catch:{ JSONException -> 0x0ab9 }
            if (r55 == 0) goto L_0x07ff
            r55 = 1
            r0 = r55
            r1 = r62
            r1.nativeOpen = r0     // Catch:{ JSONException -> 0x0ab9 }
        L_0x07ff:
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x08e8 }
            r55 = r0
            java.lang.String r56 = "pollmaxcount"
            int r55 = r55.getInt(r56)     // Catch:{ Exception -> 0x08e8 }
            r0 = r55
            r1 = r62
            r1.pollMax = r0     // Catch:{ Exception -> 0x08e8 }
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x08e8 }
            r55 = r0
            java.lang.String r56 = "pollmanualafter"
            int r33 = r55.getInt(r56)     // Catch:{ Exception -> 0x08e8 }
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x08e8 }
            r55 = r0
            java.lang.String r56 = "pollinterval"
            int r36 = r55.getInt(r56)     // Catch:{ Exception -> 0x08e8 }
            if (r36 <= 0) goto L_0x08de
            int r55 = r33 * 60
            int r55 = r55 / r36
            r0 = r55
            r1 = r62
            r1.pollManual = r0     // Catch:{ Exception -> 0x08e8 }
        L_0x0835:
            android.content.SharedPreferences$Editor r12 = r37.edit()     // Catch:{ Exception -> 0x08fb }
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x09b0 }
            java.lang.String r56 = "SD_"
            r55.<init>(r56)     // Catch:{ Exception -> 0x09b0 }
            r0 = r62
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x09b0 }
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x09b0 }
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x09b0 }
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x09b0 }
            r56 = r0
            java.lang.String r57 = "displayinterval"
            java.lang.String r56 = r56.getString(r57)     // Catch:{ Exception -> 0x09b0 }
            r0 = r55
            r1 = r56
            r12.putString(r0, r1)     // Catch:{ Exception -> 0x09b0 }
        L_0x0861:
            r12.commit()     // Catch:{ Exception -> 0x08fb }
            r22.close()     // Catch:{ Exception -> 0x08fb }
        L_0x0867:
            java.lang.String r55 = "LBAdController"
            java.lang.StringBuilder r56 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0aa6 }
            java.lang.String r57 = "Results - "
            r56.<init>(r57)     // Catch:{ Exception -> 0x0aa6 }
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0aa6 }
            r57 = r0
            java.lang.String r57 = r57.toString()     // Catch:{ Exception -> 0x0aa6 }
            java.lang.StringBuilder r56 = r56.append(r57)     // Catch:{ Exception -> 0x0aa6 }
            java.lang.String r56 = r56.toString()     // Catch:{ Exception -> 0x0aa6 }
            com.Leadbolt.AdLog.d(r55, r56)     // Catch:{ Exception -> 0x0aa6 }
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0aa6 }
            r55 = r0
            if (r55 == 0) goto L_0x08a3
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0aa6 }
            r55 = r0
            java.lang.String r56 = "show"
            java.lang.String r55 = r55.getString(r56)     // Catch:{ Exception -> 0x0aa6 }
            if (r55 == 0) goto L_0x08a3
            r55 = 1
            r0 = r55
            r1 = r62
            r1.initialized = r0     // Catch:{ Exception -> 0x0aa6 }
        L_0x08a3:
            r55 = 0
            r0 = r55
            r1 = r62
            r1.requestInProgress = r0
            r0 = r62
            boolean r0 = r0.useNotification     // Catch:{ Exception -> 0x0ab4 }
            r55 = r0
            if (r55 == 0) goto L_0x08da
            android.content.SharedPreferences$Editor r12 = r37.edit()     // Catch:{ Exception -> 0x0ab4 }
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0ab4 }
            java.lang.String r56 = "SD_NOTIFICATION_REQUESTED_"
            r55.<init>(r56)     // Catch:{ Exception -> 0x0ab4 }
            r0 = r62
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x0ab4 }
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x0ab4 }
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x0ab4 }
            long r56 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0ab4 }
            r0 = r55
            r1 = r56
            r12.putLong(r0, r1)     // Catch:{ Exception -> 0x0ab4 }
            r12.commit()     // Catch:{ Exception -> 0x0ab4 }
        L_0x08da:
            int r6 = r6 + 1
            goto L_0x066c
        L_0x08de:
            r55 = 10
            r0 = r55
            r1 = r62
            r1.pollManual = r0     // Catch:{ Exception -> 0x08e8 }
            goto L_0x0835
        L_0x08e8:
            r11 = move-exception
            r55 = 500(0x1f4, float:7.0E-43)
            r0 = r55
            r1 = r62
            r1.pollMax = r0     // Catch:{ Exception -> 0x08fb }
            r55 = 10
            r0 = r55
            r1 = r62
            r1.pollManual = r0     // Catch:{ Exception -> 0x08fb }
            goto L_0x0835
        L_0x08fb:
            r14 = move-exception
            java.lang.String r55 = "LBAdController"
            r0 = r55
            com.Leadbolt.AdLog.printStackTrace(r0, r14)     // Catch:{ all -> 0x09eb }
            r17 = 0
            org.json.JSONObject r55 = new org.json.JSONObject     // Catch:{ all -> 0x09eb }
            r55.<init>()     // Catch:{ all -> 0x09eb }
            r0 = r55
            r1 = r62
            r1.results = r0     // Catch:{ all -> 0x09eb }
            r55 = 1
            r0 = r55
            r1 = r62
            r1.initialized = r0     // Catch:{ all -> 0x09eb }
            r0 = r62
            com.Leadbolt.AdListener r0 = r0.listener     // Catch:{ all -> 0x09eb }
            r55 = r0
            if (r55 == 0) goto L_0x0938
            java.lang.String r55 = "LBAdController"
            java.lang.String r56 = "onAdFailed triggered"
            com.Leadbolt.AdLog.i(r55, r56)     // Catch:{ Exception -> 0x0a80 }
            r0 = r62
            com.Leadbolt.AdListener r0 = r0.listener     // Catch:{ Exception -> 0x0a80 }
            r55 = r0
            r55.onAdFailed()     // Catch:{ Exception -> 0x0a80 }
            r55 = 1
            r0 = r55
            r1 = r62
            r1.adLoaded = r0     // Catch:{ Exception -> 0x0a80 }
        L_0x0938:
            java.lang.String r55 = "LBAdController"
            java.lang.StringBuilder r56 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a91 }
            java.lang.String r57 = "Results - "
            r56.<init>(r57)     // Catch:{ Exception -> 0x0a91 }
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a91 }
            r57 = r0
            java.lang.String r57 = r57.toString()     // Catch:{ Exception -> 0x0a91 }
            java.lang.StringBuilder r56 = r56.append(r57)     // Catch:{ Exception -> 0x0a91 }
            java.lang.String r56 = r56.toString()     // Catch:{ Exception -> 0x0a91 }
            com.Leadbolt.AdLog.d(r55, r56)     // Catch:{ Exception -> 0x0a91 }
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a91 }
            r55 = r0
            if (r55 == 0) goto L_0x0974
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a91 }
            r55 = r0
            java.lang.String r56 = "show"
            java.lang.String r55 = r55.getString(r56)     // Catch:{ Exception -> 0x0a91 }
            if (r55 == 0) goto L_0x0974
            r55 = 1
            r0 = r55
            r1 = r62
            r1.initialized = r0     // Catch:{ Exception -> 0x0a91 }
        L_0x0974:
            r55 = 0
            r0 = r55
            r1 = r62
            r1.requestInProgress = r0
            r0 = r62
            boolean r0 = r0.useNotification     // Catch:{ Exception -> 0x09ad }
            r55 = r0
            if (r55 == 0) goto L_0x08da
            android.content.SharedPreferences$Editor r12 = r37.edit()     // Catch:{ Exception -> 0x09ad }
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x09ad }
            java.lang.String r56 = "SD_NOTIFICATION_REQUESTED_"
            r55.<init>(r56)     // Catch:{ Exception -> 0x09ad }
            r0 = r62
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x09ad }
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x09ad }
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x09ad }
            long r56 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x09ad }
            r0 = r55
            r1 = r56
            r12.putLong(r0, r1)     // Catch:{ Exception -> 0x09ad }
            r12.commit()     // Catch:{ Exception -> 0x09ad }
            goto L_0x08da
        L_0x09ad:
            r55 = move-exception
            goto L_0x08da
        L_0x09b0:
            r11 = move-exception
            r0 = r62
            java.lang.String r0 = r0.adDisplayInterval     // Catch:{ Exception -> 0x08fb }
            r55 = r0
            if (r55 == 0) goto L_0x0a60
            r0 = r62
            java.lang.String r0 = r0.adDisplayInterval     // Catch:{ Exception -> 0x08fb }
            r55 = r0
            java.lang.String r56 = "0"
            boolean r55 = r55.equals(r56)     // Catch:{ Exception -> 0x08fb }
            if (r55 != 0) goto L_0x0a60
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x08fb }
            java.lang.String r56 = "SD_"
            r55.<init>(r56)     // Catch:{ Exception -> 0x08fb }
            r0 = r62
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x08fb }
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x08fb }
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x08fb }
            r0 = r62
            java.lang.String r0 = r0.adDisplayInterval     // Catch:{ Exception -> 0x08fb }
            r56 = r0
            r0 = r55
            r1 = r56
            r12.putString(r0, r1)     // Catch:{ Exception -> 0x08fb }
            goto L_0x0861
        L_0x09eb:
            r55 = move-exception
            java.lang.String r56 = "LBAdController"
            java.lang.StringBuilder r57 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a9c }
            java.lang.String r58 = "Results - "
            r57.<init>(r58)     // Catch:{ Exception -> 0x0a9c }
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a9c }
            r58 = r0
            java.lang.String r58 = r58.toString()     // Catch:{ Exception -> 0x0a9c }
            java.lang.StringBuilder r57 = r57.append(r58)     // Catch:{ Exception -> 0x0a9c }
            java.lang.String r57 = r57.toString()     // Catch:{ Exception -> 0x0a9c }
            com.Leadbolt.AdLog.d(r56, r57)     // Catch:{ Exception -> 0x0a9c }
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a9c }
            r56 = r0
            if (r56 == 0) goto L_0x0a28
            r0 = r62
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a9c }
            r56 = r0
            java.lang.String r57 = "show"
            java.lang.String r56 = r56.getString(r57)     // Catch:{ Exception -> 0x0a9c }
            if (r56 == 0) goto L_0x0a28
            r56 = 1
            r0 = r56
            r1 = r62
            r1.initialized = r0     // Catch:{ Exception -> 0x0a9c }
        L_0x0a28:
            r56 = 0
            r0 = r56
            r1 = r62
            r1.requestInProgress = r0
            r0 = r62
            boolean r0 = r0.useNotification     // Catch:{ Exception -> 0x0ab7 }
            r56 = r0
            if (r56 == 0) goto L_0x0a5f
            android.content.SharedPreferences$Editor r12 = r37.edit()     // Catch:{ Exception -> 0x0ab7 }
            java.lang.StringBuilder r56 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0ab7 }
            java.lang.String r57 = "SD_NOTIFICATION_REQUESTED_"
            r56.<init>(r57)     // Catch:{ Exception -> 0x0ab7 }
            r0 = r62
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x0ab7 }
            r57 = r0
            java.lang.StringBuilder r56 = r56.append(r57)     // Catch:{ Exception -> 0x0ab7 }
            java.lang.String r56 = r56.toString()     // Catch:{ Exception -> 0x0ab7 }
            long r57 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0ab7 }
            r0 = r56
            r1 = r57
            r12.putLong(r0, r1)     // Catch:{ Exception -> 0x0ab7 }
            r12.commit()     // Catch:{ Exception -> 0x0ab7 }
        L_0x0a5f:
            throw r55
        L_0x0a60:
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x08fb }
            java.lang.String r56 = "SD_"
            r55.<init>(r56)     // Catch:{ Exception -> 0x08fb }
            r0 = r62
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x08fb }
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x08fb }
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x08fb }
            java.lang.String r56 = "0"
            r0 = r55
            r1 = r56
            r12.putString(r0, r1)     // Catch:{ Exception -> 0x08fb }
            goto L_0x0861
        L_0x0a80:
            r11 = move-exception
            java.lang.String r55 = "LBAdController"
            java.lang.String r56 = "Error while calling onAdFailed"
            com.Leadbolt.AdLog.i(r55, r56)     // Catch:{ all -> 0x09eb }
            java.lang.String r55 = "LBAdController"
            r0 = r55
            com.Leadbolt.AdLog.printStackTrace(r0, r11)     // Catch:{ all -> 0x09eb }
            goto L_0x0938
        L_0x0a91:
            r11 = move-exception
            r55 = 0
            r0 = r55
            r1 = r62
            r1.initialized = r0
            goto L_0x0974
        L_0x0a9c:
            r11 = move-exception
            r56 = 0
            r0 = r56
            r1 = r62
            r1.initialized = r0
            goto L_0x0a28
        L_0x0aa6:
            r11 = move-exception
            r55 = 0
            r0 = r55
            r1 = r62
            r1.initialized = r0
            goto L_0x08a3
        L_0x0ab1:
            r55 = move-exception
            goto L_0x002e
        L_0x0ab4:
            r55 = move-exception
            goto L_0x08da
        L_0x0ab7:
            r56 = move-exception
            goto L_0x0a5f
        L_0x0ab9:
            r55 = move-exception
            goto L_0x07ff
        L_0x0abc:
            r55 = move-exception
            goto L_0x0511
        L_0x0abf:
            r55 = move-exception
            goto L_0x04c1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Leadbolt.AdController.makeLBRequest():void");
    }

    private class LBRequest extends AsyncTask<String, Void, String> {
        private LBRequest() {
        }

        /* synthetic */ LBRequest(AdController adController, LBRequest lBRequest) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            AdController.this.makeLBRequest();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String res) {
            super.onPostExecute((Object) res);
            if (AdController.this.loadAd) {
                try {
                    if (AdController.this.results.getInt("timeopen") > 0) {
                        int time = AdController.this.results.getInt("timeopen") * 1000;
                        AdLog.i(AdController.LB_LOG, "Tease Time used - ad will load after " + time + "ms");
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                AdLog.i(AdController.LB_LOG, "Tease Time passed - loading Ad");
                                AdController.this.displayAd();
                            }
                        }, (long) time);
                        return;
                    }
                    AdController.this.displayAd();
                } catch (Exception e) {
                    AdController.this.displayAd();
                }
            } else if (AdController.this.useNotification) {
                AdController.this.setNotification();
            } else if (AdController.this.loadIcon) {
                AdController.this.displayIcon();
            }
        }
    }

    class OfferPolling extends TimerTask {
        OfferPolling() {
        }

        public void run() {
            HttpEntity entity;
            String success = "0";
            try {
                HttpResponse response = new DefaultHttpClient().execute(new HttpPost(String.valueOf(AdController.this.results.getString("pollurl")) + AdController.this.sectionid));
                if (response.getStatusLine().getStatusCode() == 200 && (entity = response.getEntity()) != null) {
                    InputStream instream = entity.getContent();
                    success = AdController.convertStreamToString(instream);
                    instream.close();
                }
                if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                    AdController.this.loadingDialog.dismiss();
                }
                AdController adController = AdController.this;
                adController.pollCount = adController.pollCount + 1;
                if (success.contains("0")) {
                    try {
                        if (AdController.this.pollCount > AdController.this.pollMax || AdController.this.pollCount >= AdController.this.pollManual) {
                            AdController.this.showManualPoll();
                        } else {
                            AdController.this.pollingHandler.postDelayed(AdController.this.adPolling, (long) (AdController.this.results.getInt("pollinterval") * 1000));
                        }
                    } catch (Exception e) {
                    }
                } else if (success.contains("1")) {
                    AdController.this.closeUnlocker();
                    AdController.this.completed = true;
                    if (AdController.this.listener != null) {
                        try {
                            AdLog.i(AdController.LB_LOG, "onAdCompleted triggered");
                            AdController.this.listener.onAdCompleted();
                        } catch (Exception e2) {
                            AdLog.e(AdController.LB_LOG, "error when onAdCompleted triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e2);
                        }
                    }
                } else if (success.contains("2")) {
                    if (AdController.this.adPolling != null) {
                        AdController.this.adPolling.cancel();
                    }
                    try {
                        AdController.this.results.put("showclose", "1");
                        AdController.this.results.put("showclickclose", "1");
                    } catch (JSONException e3) {
                    }
                    AdController.this.showCloseButton();
                }
            } catch (IOException e4) {
                if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                    AdController.this.loadingDialog.dismiss();
                }
                AdController adController2 = AdController.this;
                adController2.pollCount = adController2.pollCount + 1;
                if (success.contains("0")) {
                    try {
                        if (AdController.this.pollCount > AdController.this.pollMax || AdController.this.pollCount >= AdController.this.pollManual) {
                            AdController.this.showManualPoll();
                        } else {
                            AdController.this.pollingHandler.postDelayed(AdController.this.adPolling, (long) (AdController.this.results.getInt("pollinterval") * 1000));
                        }
                    } catch (Exception e5) {
                    }
                } else if (success.contains("1")) {
                    AdController.this.closeUnlocker();
                    AdController.this.completed = true;
                    if (AdController.this.listener != null) {
                        try {
                            AdLog.i(AdController.LB_LOG, "onAdCompleted triggered");
                            AdController.this.listener.onAdCompleted();
                        } catch (Exception e6) {
                            AdLog.e(AdController.LB_LOG, "error when onAdCompleted triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e6);
                        }
                    }
                } else if (success.contains("2")) {
                    if (AdController.this.adPolling != null) {
                        AdController.this.adPolling.cancel();
                    }
                    try {
                        AdController.this.results.put("showclose", "1");
                        AdController.this.results.put("showclickclose", "1");
                    } catch (JSONException e7) {
                    }
                    AdController.this.showCloseButton();
                }
            } catch (JSONException e8) {
                if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                    AdController.this.loadingDialog.dismiss();
                }
                AdController adController3 = AdController.this;
                adController3.pollCount = adController3.pollCount + 1;
                if (success.contains("0")) {
                    try {
                        if (AdController.this.pollCount > AdController.this.pollMax || AdController.this.pollCount >= AdController.this.pollManual) {
                            AdController.this.showManualPoll();
                        } else {
                            AdController.this.pollingHandler.postDelayed(AdController.this.adPolling, (long) (AdController.this.results.getInt("pollinterval") * 1000));
                        }
                    } catch (Exception e9) {
                    }
                } else if (success.contains("1")) {
                    AdController.this.closeUnlocker();
                    AdController.this.completed = true;
                    if (AdController.this.listener != null) {
                        try {
                            AdLog.i(AdController.LB_LOG, "onAdCompleted triggered");
                            AdController.this.listener.onAdCompleted();
                        } catch (Exception e10) {
                            AdLog.e(AdController.LB_LOG, "error when onAdCompleted triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e10);
                        }
                    }
                } else if (success.contains("2")) {
                    if (AdController.this.adPolling != null) {
                        AdController.this.adPolling.cancel();
                    }
                    try {
                        AdController.this.results.put("showclose", "1");
                        AdController.this.results.put("showclickclose", "1");
                    } catch (JSONException e11) {
                    }
                    AdController.this.showCloseButton();
                }
            } catch (Exception e12) {
                if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                    AdController.this.loadingDialog.dismiss();
                }
                AdController adController4 = AdController.this;
                adController4.pollCount = adController4.pollCount + 1;
                if (success.contains("0")) {
                    try {
                        if (AdController.this.pollCount > AdController.this.pollMax || AdController.this.pollCount >= AdController.this.pollManual) {
                            AdController.this.showManualPoll();
                        } else {
                            AdController.this.pollingHandler.postDelayed(AdController.this.adPolling, (long) (AdController.this.results.getInt("pollinterval") * 1000));
                        }
                    } catch (Exception e13) {
                    }
                } else if (success.contains("1")) {
                    AdController.this.closeUnlocker();
                    AdController.this.completed = true;
                    if (AdController.this.listener != null) {
                        try {
                            AdLog.i(AdController.LB_LOG, "onAdCompleted triggered");
                            AdController.this.listener.onAdCompleted();
                        } catch (Exception e14) {
                            AdLog.e(AdController.LB_LOG, "error when onAdCompleted triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e14);
                        }
                    }
                } else if (success.contains("2")) {
                    if (AdController.this.adPolling != null) {
                        AdController.this.adPolling.cancel();
                    }
                    try {
                        AdController.this.results.put("showclose", "1");
                        AdController.this.results.put("showclickclose", "1");
                    } catch (JSONException e15) {
                    }
                    AdController.this.showCloseButton();
                }
            } catch (Throwable th) {
                if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                    AdController.this.loadingDialog.dismiss();
                }
                AdController adController5 = AdController.this;
                adController5.pollCount = adController5.pollCount + 1;
                if (success.contains("0")) {
                    try {
                        if (AdController.this.pollCount > AdController.this.pollMax || AdController.this.pollCount >= AdController.this.pollManual) {
                            AdController.this.showManualPoll();
                        } else {
                            AdController.this.pollingHandler.postDelayed(AdController.this.adPolling, (long) (AdController.this.results.getInt("pollinterval") * 1000));
                        }
                    } catch (Exception e16) {
                    }
                } else if (success.contains("1")) {
                    AdController.this.closeUnlocker();
                    AdController.this.completed = true;
                    if (AdController.this.listener != null) {
                        try {
                            AdLog.i(AdController.LB_LOG, "onAdCompleted triggered");
                            AdController.this.listener.onAdCompleted();
                        } catch (Exception e17) {
                            AdLog.e(AdController.LB_LOG, "error when onAdCompleted triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e17);
                        }
                    }
                } else if (success.contains("2")) {
                    if (AdController.this.adPolling != null) {
                        AdController.this.adPolling.cancel();
                    }
                    try {
                        AdController.this.results.put("showclose", "1");
                        AdController.this.results.put("showclickclose", "1");
                    } catch (JSONException e18) {
                    }
                    AdController.this.showCloseButton();
                }
                throw th;
            }
        }
    }

    /* access modifiers changed from: private */
    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        AdLog.printStackTrace(LB_LOG, e);
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                AdLog.printStackTrace(LB_LOG, e2);
                try {
                    is.close();
                } catch (IOException e3) {
                    AdLog.printStackTrace(LB_LOG, e3);
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    AdLog.printStackTrace(LB_LOG, e4);
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    private String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            AdLog.printStackTrace(LB_LOG, ex);
        }
        return null;
    }

    public boolean onBackPressed() {
        if (!this.linkClicked) {
            return false;
        }
        loadAd();
        return true;
    }

    private class LBJSInterface {
        private LBJSInterface() {
        }

        /* synthetic */ LBJSInterface(AdController adController, LBJSInterface lBJSInterface) {
            this();
        }

        public void processHTML(String content) {
            if (content != null && content.equals("0")) {
                AdController.this.adDestroyed = true;
                if (AdController.this.listener != null) {
                    try {
                        AdLog.i(AdController.LB_LOG, "onAdFailed triggered");
                        AdController.this.listener.onAdFailed();
                    } catch (Exception e) {
                        AdLog.i(AdController.LB_LOG, "Error while calling onAdFailed");
                        AdLog.printStackTrace(AdController.LB_LOG, e);
                    }
                }
            }
            if (AdController.this.listener != null && !AdController.this.adDestroyed) {
                try {
                    AdLog.i(AdController.LB_LOG, "onAdLoaded triggered");
                    AdController.this.listener.onAdLoaded();
                    AdController.this.adLoaded = true;
                } catch (Exception e2) {
                    AdLog.i(AdController.LB_LOG, "Error while calling onAdLoaded");
                    AdLog.printStackTrace(AdController.LB_LOG, e2);
                }
            }
        }
    }
}
