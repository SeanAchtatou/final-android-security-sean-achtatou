package com.apperhand.common.dto;

import java.util.Locale;

public class ApplicationDetails extends BaseDTO {
    private static final long serialVersionUID = 6231432081614762071L;
    private String abTestId;
    private String androidId;
    private int appVersion;
    private String applicationId;
    private Build build;
    private String developerId;
    private String deviceId;
    private DisplayMetrics displayMetrics;
    private Locale locale;
    private String packageId;
    private String protocolVersion;
    private String sourceIp;
    private String userAgent;

    public String getAbTestId() {
        return this.abTestId;
    }

    public String getAndroidId() {
        return this.androidId;
    }

    public int getAppVersion() {
        return this.appVersion;
    }

    public String getApplicationId() {
        return this.applicationId;
    }

    public Build getBuild() {
        return this.build;
    }

    public String getDeveloperId() {
        return this.developerId;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public DisplayMetrics getDisplayMetrics() {
        return this.displayMetrics;
    }

    public Locale getLocale() {
        return this.locale;
    }

    public String getPackageId() {
        return this.packageId;
    }

    public String getProtocolVersion() {
        return this.protocolVersion;
    }

    public String getSourceIp() {
        return this.sourceIp;
    }

    public String getUserAgent() {
        return this.userAgent;
    }

    public void setAbTestId(String str) {
        this.abTestId = str;
    }

    public void setAndroidId(String str) {
        this.androidId = str;
    }

    public void setAppVersion(int i) {
        this.appVersion = i;
    }

    public void setApplicationId(String str) {
        this.applicationId = str;
    }

    public void setBuild(Build build2) {
        this.build = build2;
    }

    public void setDeveloperId(String str) {
        this.developerId = str;
    }

    public void setDeviceId(String str) {
        this.deviceId = str;
    }

    public void setDisplayMetrics(DisplayMetrics displayMetrics2) {
        this.displayMetrics = displayMetrics2;
    }

    public void setLocale(Locale locale2) {
        this.locale = locale2;
    }

    public void setPackageId(String str) {
        this.packageId = str;
    }

    public void setProtocolVersion(String str) {
        this.protocolVersion = str;
    }

    public void setSourceIp(String str) {
        this.sourceIp = str;
    }

    public void setUserAgent(String str) {
        this.userAgent = str;
    }

    public String toString() {
        return "ApplicationDetails [applicationId=" + this.applicationId + ", developerId=" + this.developerId + ", sourceIp=" + this.sourceIp + ", userAgent=" + this.userAgent + ", deviceId=" + this.deviceId + ", locale=" + this.locale + ", protocolVersion=" + this.protocolVersion + ", displayMetrics=" + this.displayMetrics + ", build=" + this.build + ", abTestId=" + this.abTestId + ", packageId=" + this.packageId + ", androidId=" + this.androidId + ", appVersion=" + this.appVersion + "]";
    }
}
