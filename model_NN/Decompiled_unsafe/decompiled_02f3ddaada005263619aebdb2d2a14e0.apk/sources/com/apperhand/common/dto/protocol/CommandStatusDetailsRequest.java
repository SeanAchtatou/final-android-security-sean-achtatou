package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.CommandStatus;
import java.util.List;

public class CommandStatusDetailsRequest extends BaseRequest {
    private static final long serialVersionUID = 5346676359806508659L;
    private List<CommandStatus> statuses;

    public List<CommandStatus> getStatuses() {
        return this.statuses;
    }

    public void setStatuses(List<CommandStatus> list) {
        this.statuses = list;
    }

    public String toString() {
        return "CommandStatusRequest [statuses=" + this.statuses + ", toString()=" + super.toString() + "]";
    }
}
