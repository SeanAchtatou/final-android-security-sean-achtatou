package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.BaseDTO;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseResponse extends BaseDTO {
    private static final long serialVersionUID = -1535279639837531044L;
    protected String abTest;
    protected Map<String, String> parameters = new HashMap();
    protected boolean validResponse = true;

    public String getAbTest() {
        return this.abTest;
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public boolean isValidResponse() {
        return this.validResponse;
    }

    public void setAbTest(String str) {
        this.abTest = str;
    }

    public void setParameters(Map<String, String> map) {
        this.parameters = map;
    }

    public void setValidResponse(boolean z) {
        this.validResponse = z;
    }

    public String toString() {
        return "BaseResponse [parameters=" + this.parameters + ", abTests=" + this.abTest + ", validResponse=" + this.validResponse + "]";
    }
}
