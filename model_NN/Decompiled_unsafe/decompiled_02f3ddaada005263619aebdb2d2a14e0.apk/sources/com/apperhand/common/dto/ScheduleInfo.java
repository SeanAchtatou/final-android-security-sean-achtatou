package com.apperhand.common.dto;

public class ScheduleInfo extends BaseDTO {
    private static final long serialVersionUID = -8761009878083698329L;
    private long commandsDetailsInterval;
    private long infoInterval;

    public long getCommandsDetailsInterval() {
        return this.commandsDetailsInterval;
    }

    public long getInfoInterval() {
        return this.infoInterval;
    }

    public void setCommandsDetailsInterval(long j) {
        this.commandsDetailsInterval = j;
    }

    public void setInfoInterval(long j) {
        this.infoInterval = j;
    }

    public String toString() {
        return "ScheduleInfo [infoInterval=" + this.infoInterval + ", commandsDetailsInterval=" + this.commandsDetailsInterval + "]";
    }
}
