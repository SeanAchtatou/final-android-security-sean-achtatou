package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.ShortcutsDetails;
import java.util.List;

public class ShortcutsDetailsResponse extends BaseResponse {
    public static final long serialVersionUID = -4596240154822595562L;
    public List<ShortcutsDetails> shortcutsList;

    public List<ShortcutsDetails> getShortcutList() {
        return this.shortcutsList;
    }

    public void setShortcutList(List<ShortcutsDetails> list) {
        this.shortcutsList = list;
    }

    public String toString() {
        return "ShortcutResponse [shortcutList=" + this.shortcutsList + ", toString()=" + super.toString() + "]";
    }
}
