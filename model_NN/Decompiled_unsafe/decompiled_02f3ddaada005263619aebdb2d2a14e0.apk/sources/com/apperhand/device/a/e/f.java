package com.apperhand.device.a.e;

public class f extends Exception {
    private a a;
    private boolean b;

    public enum a {
        GENERAL_ERROR(1, "general error"),
        SHORTCUT_ERROR(10, "shortcut error"),
        BOOKMARK_ERROR(20, "bookmark error"),
        HISTORY_ERROR(30, "history error");
        
        private long e;
        private String f;

        private a(long j, String str) {
            this.e = j;
            this.f = str;
        }
    }

    public f() {
        this.b = true;
    }

    public f(a aVar, String str) {
        this(aVar, str, null);
    }

    public f(a aVar, String str, Throwable th) {
        this(aVar, str, th, true);
    }

    public f(a aVar, String str, Throwable th, boolean z) {
        super(str, th);
        this.b = true;
        this.a = aVar;
        this.b = z;
    }

    public f(a aVar, Throwable th) {
        this(aVar, null, th);
    }

    public boolean a() {
        return this.b;
    }
}
