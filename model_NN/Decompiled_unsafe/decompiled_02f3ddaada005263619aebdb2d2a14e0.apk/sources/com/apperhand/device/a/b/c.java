package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.BookmarksDetailsResponse;
import com.apperhand.common.dto.protocol.HomepageDetailsResponse;
import com.apperhand.common.dto.protocol.ShortcutsDetailsResponse;
import com.apperhand.common.dto.protocol.TerminateResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.b;
import com.apperhand.device.a.e.c;

public class c {
    public static b a(BaseResponse baseResponse, b bVar, a aVar) {
        if (baseResponse instanceof BookmarksDetailsResponse) {
            Command command = new Command(Command.Commands.BOOKMARKS);
            return new a(bVar, aVar, command.getId(), command.getCommand());
        } else if (baseResponse instanceof ShortcutsDetailsResponse) {
            Command command2 = new Command(Command.Commands.SHORTCUTS);
            return new e(bVar, aVar, command2.getId(), command2.getCommand());
        } else if (baseResponse instanceof HomepageDetailsResponse) {
            Command command3 = new Command(Command.Commands.HOMEPAGE);
            return new d(bVar, aVar, command3.getId(), command3.getCommand());
        } else if (baseResponse instanceof TerminateResponse) {
            Command command4 = new Command(Command.Commands.TERMINATE);
            return new f(bVar, aVar, command4.getId(), command4.getCommand());
        } else {
            aVar.a().a(c.a.DEBUG, String.format("Uknown response [response = %s] !!!", baseResponse));
            return null;
        }
    }
}
