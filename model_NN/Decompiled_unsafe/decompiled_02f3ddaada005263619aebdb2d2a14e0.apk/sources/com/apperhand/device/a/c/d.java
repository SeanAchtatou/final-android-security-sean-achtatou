package com.apperhand.device.a.c;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.ScheduleInfo;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusDetailsRequest;
import com.apperhand.common.dto.protocol.CommandsDetailsResponse;
import com.apperhand.common.dto.protocol.ExternalCommandsDetailsRequest;
import com.apperhand.device.a.a;
import com.apperhand.device.a.b.b;
import com.apperhand.device.a.e.c;
import com.apperhand.device.a.e.e;
import com.apperhand.device.a.e.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class d extends a {
    private String f;
    private List<b> g = null;

    public d(com.apperhand.device.a.b bVar, a aVar, String str, Command command) {
        super(bVar, aVar, str, command.getCommand());
        Map<String, Object> parameters = command.getParameters();
        if (parameters != null) {
            this.f = (String) parameters.get("externalData");
        }
        this.g = new ArrayList();
    }

    private CommandsDetailsResponse a(ExternalCommandsDetailsRequest externalCommandsDetailsRequest) {
        try {
            return (CommandsDetailsResponse) this.d.b().a(externalCommandsDetailsRequest, Command.Commands.EXTERNAL_COMMANDS_DETAILS, null, CommandsDetailsResponse.class);
        } catch (f e) {
            this.d.a().a(c.a.DEBUG, "Unable to handle external commands details command!!!!", e);
            throw e;
        }
    }

    private void a(Map<String, String> map) {
        if (map != null && map.size() > 0) {
            for (String next : map.keySet()) {
                this.d.i().b(next, map.get(next));
            }
        }
    }

    private List<CommandStatus> b(Map<String, Object> map) {
        ArrayList arrayList = new ArrayList();
        if (this.g != null) {
            Iterator<b> it = this.g.iterator();
            while (it.hasNext()) {
                b next = it.next();
                CommandStatus b = next != null ? next.b(map) : null;
                if (b != null) {
                    arrayList.add(b);
                }
            }
        }
        return arrayList;
    }

    private ExternalCommandsDetailsRequest e() {
        ExternalCommandsDetailsRequest externalCommandsDetailsRequest = new ExternalCommandsDetailsRequest();
        externalCommandsDetailsRequest.setApplicationDetails(this.d.j());
        externalCommandsDetailsRequest.setAbTestId(this.e.b());
        externalCommandsDetailsRequest.setData(this.f);
        ScheduleInfo scheduleInfo = new ScheduleInfo();
        scheduleInfo.setCommandsDetailsInterval(this.d.h().c());
        scheduleInfo.setInfoInterval(this.d.h().f());
        externalCommandsDetailsRequest.setScheduleInfo(scheduleInfo);
        externalCommandsDetailsRequest.setSupportLauncher(this.d.d().c());
        externalCommandsDetailsRequest.setInitiationType("intent");
        return externalCommandsDetailsRequest;
    }

    /* access modifiers changed from: protected */
    public Map<String, Object> a(BaseResponse baseResponse) {
        Map<String, Object> map;
        CommandsDetailsResponse commandsDetailsResponse = (CommandsDetailsResponse) baseResponse;
        this.e.a(e.a(commandsDetailsResponse));
        a(commandsDetailsResponse.getGeneralParameters());
        this.d.i().a(commandsDetailsResponse.getIdentifiers());
        HashMap hashMap = new HashMap();
        List<BaseResponse> responses = commandsDetailsResponse.getResponses();
        if (responses == null || responses.size() == 0) {
            return hashMap;
        }
        this.g.clear();
        for (BaseResponse next : responses) {
            if (next != null) {
                b a = com.apperhand.device.a.b.c.a(next, this.e, this.d);
                this.g.add(a);
                try {
                    map = a.a(next);
                } catch (Throwable th) {
                    a.a(th);
                    map = null;
                }
                if (map != null) {
                    hashMap.putAll(map);
                }
            }
        }
        return hashMap;
    }

    public void a(BaseResponse baseResponse, Map<String, Object> map) {
        CommandStatusDetailsRequest b = b();
        List<CommandStatus> b2 = b(map);
        if (b2 != null && b2.size() != 0) {
            b.setStatuses(b2);
            a(b);
        }
    }

    /* access modifiers changed from: protected */
    public CommandStatusDetailsRequest b() {
        return super.b();
    }

    /* access modifiers changed from: protected */
    public BaseResponse d() {
        return a(e());
    }
}
