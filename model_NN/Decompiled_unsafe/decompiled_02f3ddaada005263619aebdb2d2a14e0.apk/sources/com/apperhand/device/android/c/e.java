package com.apperhand.device.android.c;

import java.io.ByteArrayOutputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.http.HttpResponse;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.message.AbstractHttpMessage;

public class e {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.apperhand.device.android.c.e.a(java.lang.String, org.apache.http.message.AbstractHttpMessage, org.apache.http.HttpResponse):java.lang.StringBuilder
     arg types: [java.lang.String, org.apache.http.client.methods.HttpPost, org.apache.http.HttpResponse]
     candidates:
      com.apperhand.device.android.c.e.a(java.lang.String, byte[], java.util.List<org.apache.http.Header>):java.lang.String
      com.apperhand.device.android.c.e.a(java.lang.String, org.apache.http.message.AbstractHttpMessage, org.apache.http.HttpResponse):java.lang.StringBuilder */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:20:0x00a9=Splitter:B:20:0x00a9, B:55:0x013f=Splitter:B:55:0x013f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r9, byte[] r10, java.util.List<org.apache.http.Header> r11) {
        /*
            r2 = 60000(0xea60, float:8.4078E-41)
            r1 = 0
            r4 = 0
            org.apache.http.params.BasicHttpParams r0 = new org.apache.http.params.BasicHttpParams
            r0.<init>()
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r2)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r2)
            org.apache.http.impl.client.DefaultHttpClient r5 = new org.apache.http.impl.client.DefaultHttpClient
            r5.<init>(r0)
            com.apperhand.device.android.c.e$1 r0 = new com.apperhand.device.android.c.e$1
            r0.<init>()
            r5.setHttpRequestRetryHandler(r0)
            org.apache.http.client.methods.HttpPost r6 = new org.apache.http.client.methods.HttpPost
            r6.<init>(r9)
            if (r11 == 0) goto L_0x0038
            java.util.Iterator r2 = r11.iterator()
        L_0x0028:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0038
            java.lang.Object r0 = r2.next()
            org.apache.http.Header r0 = (org.apache.http.Header) r0
            r6.setHeader(r0)
            goto L_0x0028
        L_0x0038:
            org.apache.http.params.HttpParams r0 = r6.getParams()
            java.lang.String r2 = "http.protocol.expect-continue"
            r0.setBooleanParameter(r2, r4)
            java.lang.String r0 = "Content-Type"
            java.lang.String r2 = "application/json"
            r6.setHeader(r0, r2)
            java.lang.String r0 = "Accept-Encoding"
            java.lang.String r2 = "gzip"
            r6.setHeader(r0, r2)
            java.lang.String r0 = "Accept"
            java.lang.String r2 = "application/json"
            r6.setHeader(r0, r2)
            org.apache.http.entity.AbstractHttpEntity r0 = a(r10)     // Catch:{ IOException -> 0x00de }
            r6.setEntity(r0)     // Catch:{ IOException -> 0x00de }
        L_0x005d:
            org.apache.http.HttpResponse r3 = r5.execute(r6)     // Catch:{ IOException -> 0x0185, RuntimeException -> 0x0182 }
            org.apache.http.HttpEntity r0 = r3.getEntity()     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            org.apache.http.StatusLine r2 = r3.getStatusLine()     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            int r2 = r2.getStatusCode()     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            r7 = 200(0xc8, float:2.8E-43)
            if (r2 == r7) goto L_0x00e9
            if (r0 == 0) goto L_0x0076
            r0.consumeContent()     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
        L_0x0076:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            r0.<init>()     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            java.lang.String r1 = "Status code is "
            java.lang.StringBuilder r1 = r0.append(r1)     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            org.apache.http.StatusLine r2 = r3.getStatusLine()     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            int r2 = r2.getStatusCode()     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            java.lang.String r2 = ", "
            r1.append(r2)     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            java.lang.StringBuilder r1 = a(r9, r6, r3)     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            r0.append(r1)     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            com.apperhand.device.a.e.f r1 = new com.apperhand.device.a.e.f     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            com.apperhand.device.a.e.f$a r2 = com.apperhand.device.a.e.f.a.GENERAL_ERROR     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            r4 = 0
            r7 = 0
            r1.<init>(r2, r0, r4, r7)     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            throw r1     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
        L_0x00a7:
            r0 = move-exception
            r1 = r3
        L_0x00a9:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d5 }
            r2.<init>()     // Catch:{ all -> 0x00d5 }
            java.lang.String r3 = "Error execute Exception "
            java.lang.StringBuilder r3 = r2.append(r3)     // Catch:{ all -> 0x00d5 }
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x00d5 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00d5 }
            java.lang.String r4 = ", "
            r3.append(r4)     // Catch:{ all -> 0x00d5 }
            java.lang.StringBuilder r1 = a(r9, r6, r1)     // Catch:{ all -> 0x00d5 }
            r2.append(r1)     // Catch:{ all -> 0x00d5 }
            com.apperhand.device.a.e.f r1 = new com.apperhand.device.a.e.f     // Catch:{ all -> 0x00d5 }
            com.apperhand.device.a.e.f$a r3 = com.apperhand.device.a.e.f.a.GENERAL_ERROR     // Catch:{ all -> 0x00d5 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00d5 }
            r4 = 0
            r1.<init>(r3, r2, r0, r4)     // Catch:{ all -> 0x00d5 }
            throw r1     // Catch:{ all -> 0x00d5 }
        L_0x00d5:
            r0 = move-exception
            org.apache.http.conn.ClientConnectionManager r1 = r5.getConnectionManager()
            r1.shutdown()
            throw r0
        L_0x00de:
            r0 = move-exception
            org.apache.http.entity.ByteArrayEntity r0 = new org.apache.http.entity.ByteArrayEntity
            r0.<init>(r10)
            r6.setEntity(r0)
            goto L_0x005d
        L_0x00e9:
            if (r0 == 0) goto L_0x018d
            java.io.InputStream r2 = r0.getContent()     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
            org.apache.http.Header r0 = r0.getContentEncoding()     // Catch:{ all -> 0x0188 }
            if (r0 == 0) goto L_0x0113
            if (r2 == 0) goto L_0x0113
            org.apache.http.HeaderElement[] r7 = r0.getElements()     // Catch:{ all -> 0x0188 }
            r0 = r4
        L_0x00fc:
            int r4 = r7.length     // Catch:{ all -> 0x0188 }
            if (r0 >= r4) goto L_0x0113
            r4 = r7[r0]     // Catch:{ all -> 0x0188 }
            java.lang.String r4 = r4.getName()     // Catch:{ all -> 0x0188 }
            java.lang.String r8 = "gzip"
            boolean r4 = r4.equalsIgnoreCase(r8)     // Catch:{ all -> 0x0188 }
            if (r4 == 0) goto L_0x016e
            java.util.zip.GZIPInputStream r0 = new java.util.zip.GZIPInputStream     // Catch:{ all -> 0x0188 }
            r0.<init>(r2)     // Catch:{ all -> 0x0188 }
            r2 = r0
        L_0x0113:
            if (r2 == 0) goto L_0x018b
            java.io.StringWriter r0 = new java.io.StringWriter     // Catch:{ all -> 0x0136 }
            r0.<init>()     // Catch:{ all -> 0x0136 }
            r1 = 1024(0x400, float:1.435E-42)
            char[] r1 = new char[r1]     // Catch:{ all -> 0x0136 }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ all -> 0x0136 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ all -> 0x0136 }
            java.lang.String r8 = "UTF-8"
            r7.<init>(r2, r8)     // Catch:{ all -> 0x0136 }
            r4.<init>(r7)     // Catch:{ all -> 0x0136 }
        L_0x012a:
            int r7 = r4.read(r1)     // Catch:{ all -> 0x0136 }
            r8 = -1
            if (r7 == r8) goto L_0x0171
            r8 = 0
            r0.write(r1, r8, r7)     // Catch:{ all -> 0x0136 }
            goto L_0x012a
        L_0x0136:
            r0 = move-exception
            r1 = r2
        L_0x0138:
            if (r1 == 0) goto L_0x013d
            r1.close()     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
        L_0x013d:
            throw r0     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
        L_0x013e:
            r0 = move-exception
        L_0x013f:
            r6.abort()     // Catch:{ all -> 0x00d5 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d5 }
            r1.<init>()     // Catch:{ all -> 0x00d5 }
            java.lang.String r2 = "Error execute Exception "
            java.lang.StringBuilder r2 = r1.append(r2)     // Catch:{ all -> 0x00d5 }
            java.lang.String r4 = r0.getMessage()     // Catch:{ all -> 0x00d5 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x00d5 }
            java.lang.String r4 = ", "
            r2.append(r4)     // Catch:{ all -> 0x00d5 }
            java.lang.StringBuilder r2 = a(r9, r6, r3)     // Catch:{ all -> 0x00d5 }
            r1.append(r2)     // Catch:{ all -> 0x00d5 }
            com.apperhand.device.a.e.f r2 = new com.apperhand.device.a.e.f     // Catch:{ all -> 0x00d5 }
            com.apperhand.device.a.e.f$a r3 = com.apperhand.device.a.e.f.a.GENERAL_ERROR     // Catch:{ all -> 0x00d5 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00d5 }
            r4 = 0
            r2.<init>(r3, r1, r0, r4)     // Catch:{ all -> 0x00d5 }
            throw r2     // Catch:{ all -> 0x00d5 }
        L_0x016e:
            int r0 = r0 + 1
            goto L_0x00fc
        L_0x0171:
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0136 }
        L_0x0175:
            if (r2 == 0) goto L_0x017a
            r2.close()     // Catch:{ IOException -> 0x00a7, RuntimeException -> 0x013e }
        L_0x017a:
            org.apache.http.conn.ClientConnectionManager r1 = r5.getConnectionManager()
            r1.shutdown()
            return r0
        L_0x0182:
            r0 = move-exception
            r3 = r1
            goto L_0x013f
        L_0x0185:
            r0 = move-exception
            goto L_0x00a9
        L_0x0188:
            r0 = move-exception
            r1 = r2
            goto L_0x0138
        L_0x018b:
            r0 = r1
            goto L_0x0175
        L_0x018d:
            r0 = r1
            goto L_0x017a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.apperhand.device.android.c.e.a(java.lang.String, byte[], java.util.List):java.lang.String");
    }

    private static StringBuilder a(String str, AbstractHttpMessage abstractHttpMessage, HttpResponse httpResponse) {
        StringBuilder sb = new StringBuilder();
        if (str != null) {
            sb.append("address = [").append(str).append("],");
        }
        if (!(abstractHttpMessage == null || abstractHttpMessage.getAllHeaders() == null)) {
            sb.append("Headers = [").append(abstractHttpMessage.getAllHeaders()).append("],");
        }
        if (!(httpResponse == null || httpResponse.getStatusLine() == null)) {
            sb.append("statusLine = [").append(httpResponse.getStatusLine()).append("]");
        }
        return sb;
    }

    public static AbstractHttpEntity a(byte[] bArr) {
        if (bArr.length < 512) {
            return new ByteArrayEntity(bArr);
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        gZIPOutputStream.write(bArr);
        gZIPOutputStream.close();
        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(byteArrayOutputStream.toByteArray());
        byteArrayEntity.setContentEncoding("gzip");
        return byteArrayEntity;
    }
}
