package com.apperhand.device.android.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.google.mygson.JsonDeserializationContext;
import com.google.mygson.JsonDeserializer;
import com.google.mygson.JsonElement;
import com.google.mygson.JsonParseException;
import com.google.mygson.JsonSerializationContext;
import com.google.mygson.JsonSerializer;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class a implements JsonDeserializer<BaseResponse>, JsonSerializer<BaseResponse> {
    private static final Map<Command.Commands, String> a = new HashMap(4);

    static {
        a.put(Command.Commands.BOOKMARKS, "BookmarksDetailsResponse");
        a.put(Command.Commands.HOMEPAGE, "HomepageDetailsResponse");
        a.put(Command.Commands.SHORTCUTS, "ShortcutsDetailsResponse");
        a.put(Command.Commands.TERMINATE, "TerminateResponse");
    }

    /* renamed from: a */
    public BaseResponse deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        String str = a.get(Command.getCommandByName(jsonElement.getAsJsonObject().get("command").getAsString()));
        if (str == null) {
            return null;
        }
        try {
            return (BaseResponse) jsonDeserializationContext.deserialize(jsonElement, Class.forName(BaseResponse.class.getPackage().getName() + "." + str));
        } catch (ClassNotFoundException e) {
            throw new JsonParseException("Unknown element type: " + str, e);
        }
    }

    /* renamed from: a */
    public JsonElement serialize(BaseResponse baseResponse, Type type, JsonSerializationContext jsonSerializationContext) {
        return null;
    }
}
