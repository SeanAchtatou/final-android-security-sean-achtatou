package com.apperhand.device.android.a;

import android.content.Context;
import com.apperhand.common.dto.Homepage;
import com.apperhand.device.a.a.c;
import com.apperhand.device.android.a.a.a;
import java.util.List;

public class e implements c {
    private Context a;

    public e(Context context) {
        this.a = context;
    }

    public boolean a(Homepage homepage) {
        boolean z;
        boolean z2 = false;
        try {
            List<a> a2 = a.C0002a.a(this.a);
            if (a2 != null && a2.size() > 0) {
                for (a a3 : a2) {
                    try {
                        z = a3.a(this.a, homepage) ? true : z2;
                    } catch (Throwable th) {
                        z = z2;
                    }
                    z2 = z;
                }
            }
        } catch (Throwable th2) {
        }
        return z2;
    }
}
