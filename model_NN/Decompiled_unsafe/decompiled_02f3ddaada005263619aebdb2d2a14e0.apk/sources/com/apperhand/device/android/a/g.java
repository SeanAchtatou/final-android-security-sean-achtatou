package com.apperhand.device.android.a;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import android.util.Log;
import com.apperhand.common.dto.Action;
import com.apperhand.common.dto.AssetInformation;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandInformation;
import com.apperhand.common.dto.Shortcut;
import com.apperhand.device.a.a.e;
import com.apperhand.device.a.a.f;
import com.apperhand.device.a.e.c;
import com.apperhand.device.a.e.f;
import com.apperhand.device.android.c.c;
import com.apperhand.device.android.c.d;
import com.tapjoy.TapjoyConstants;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class g implements f {
    private Context a;
    private ResolveInfo b = null;
    private String c = null;
    private StringBuilder d = new StringBuilder();
    private boolean e = true;

    private enum a {
        NONE,
        READ,
        WRITE,
        BOTH
    }

    public g(Context context) {
        this.a = context;
        a();
    }

    private Intent a(Shortcut shortcut, String str) {
        Intent b2 = b(shortcut);
        if (b2 == null) {
            return null;
        }
        Intent intent = new Intent();
        intent.putExtra("android.intent.extra.shortcut.INTENT", b2);
        intent.putExtra("android.intent.extra.shortcut.NAME", shortcut.getName());
        byte[] icon = shortcut.getIcon();
        if (icon == null) {
            byte[] bArr = new byte[0];
            intent.putExtra("android.intent.extra.shortcut.ICON", BitmapFactory.decodeByteArray(bArr, 0, bArr.length));
        } else {
            intent.putExtra("android.intent.extra.shortcut.ICON", BitmapFactory.decodeByteArray(icon, 0, icon.length));
        }
        intent.setAction(str);
        return intent;
    }

    private Intent a(String str, String str2) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        intent.addFlags(268435456);
        intent.addFlags(67108864);
        a(intent, str2);
        return intent;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(android.content.pm.ProviderInfo r11) {
        /*
            r10 = this;
            r6 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "content://"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r11.authority
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "/sqlite_master"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.net.Uri r1 = android.net.Uri.parse(r0)
            android.content.Context r0 = r10.a
            android.content.ContentResolver r0 = r0.getContentResolver()
            r2 = 0
            java.lang.String r3 = "type = 'table'"
            r4 = 0
            r5 = 0
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c1 }
            if (r7 != 0) goto L_0x0038
            if (r7 == 0) goto L_0x0036
            r7.close()
        L_0x0036:
            r0 = r6
        L_0x0037:
            return r0
        L_0x0038:
            java.lang.String r1 = "tbl_name"
            int r8 = r7.getColumnIndex(r1)     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
        L_0x003e:
            boolean r1 = r7.moveToNext()     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            if (r1 == 0) goto L_0x00bb
            java.lang.String r1 = r7.getString(r8)     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            r2.<init>()     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.String r3 = "content://"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.String r3 = r11.authority     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.String r9 = r1.toString()     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            android.net.Uri r1 = android.net.Uri.parse(r9)     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.String r2 = "intent"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            r3 = -1
            if (r2 == r3) goto L_0x0098
            r1.close()     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            r0.<init>()     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.String r1 = "?notify=false"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            if (r7 == 0) goto L_0x0037
            r7.close()
            goto L_0x0037
        L_0x0098:
            r1.close()     // Catch:{ Throwable -> 0x009c, all -> 0x00c9 }
            goto L_0x003e
        L_0x009c:
            r0 = move-exception
            r1 = r7
        L_0x009e:
            java.lang.StringBuilder r2 = r10.d     // Catch:{ all -> 0x00cb }
            r3 = 91
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00cb }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00cb }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x00cb }
            java.lang.String r2 = "]"
            r0.append(r2)     // Catch:{ all -> 0x00cb }
            if (r1 == 0) goto L_0x00b8
            r1.close()
        L_0x00b8:
            r0 = r6
            goto L_0x0037
        L_0x00bb:
            if (r7 == 0) goto L_0x00b8
            r7.close()
            goto L_0x00b8
        L_0x00c1:
            r0 = move-exception
            r7 = r6
        L_0x00c3:
            if (r7 == 0) goto L_0x00c8
            r7.close()
        L_0x00c8:
            throw r0
        L_0x00c9:
            r0 = move-exception
            goto L_0x00c3
        L_0x00cb:
            r0 = move-exception
            r7 = r1
            goto L_0x00c3
        L_0x00ce:
            r0 = move-exception
            r1 = r6
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.apperhand.device.android.a.g.a(android.content.pm.ProviderInfo):java.lang.String");
    }

    private List<Shortcut> a(Cursor cursor, List<String> list) {
        if (cursor == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int columnIndex = cursor.getColumnIndex("_id");
        int columnIndex2 = cursor.getColumnIndex("title");
        int columnIndex3 = cursor.getColumnIndex("intent");
        int columnIndex4 = cursor.getColumnIndex("screen");
        while (cursor.moveToNext()) {
            Shortcut shortcut = new Shortcut();
            shortcut.setId(cursor.getLong(columnIndex));
            shortcut.setName(cursor.getString(columnIndex2));
            String string = cursor.getString(columnIndex3);
            if (string != null) {
                try {
                    Intent parseUri = Intent.parseUri(string, 0);
                    shortcut.setLink(parseUri.getDataString());
                    shortcut.setPackageName(parseUri.getPackage());
                    shortcut.setIntentString(parseUri.toUri(0));
                    shortcut.setIdentifier(null);
                    Bundle extras = parseUri.getExtras();
                    if (extras != null) {
                        for (String next : list) {
                            if (extras.containsKey(next)) {
                                shortcut.setIdentifier(parseUri.getExtras().getString(next));
                            }
                        }
                    }
                    shortcut.setScreen(cursor.getInt(columnIndex4));
                    if (shortcut.getLink() != null || shortcut.getPackageName() != null) {
                        arrayList.add(shortcut);
                    }
                } catch (URISyntaxException e2) {
                }
            }
        }
        return arrayList;
    }

    private void a(Intent intent, String str) {
        if (str != null) {
            intent.putExtra(str, str);
        }
    }

    private Intent b(Shortcut shortcut) {
        switch (shortcut.getAction()) {
            case URL:
                return a(shortcut.getLink(), shortcut.getIdentifier());
            case MARKET:
                return a(shortcut.getLink(), shortcut.getIdentifier());
            case APPLICATION:
                if (b(shortcut.getPackageName())) {
                    return d(shortcut);
                }
                throw new com.apperhand.device.a.e.f(f.a.SHORTCUT_ERROR, "Application not exists");
            case MARKET_OR_APPLICATION:
                return b(shortcut.getPackageName()) ? d(shortcut) : a(shortcut.getLink(), shortcut.getIdentifier());
            case QUICK_SEARCH:
                Intent intent = new Intent(this.a, b.a(Action.QUICK_SEARCH));
                intent.setAction("android.intent.action.MAIN");
                intent.setPackage(this.a.getPackageName());
                intent.addFlags(268435456);
                a(intent, shortcut.getIdentifier());
                return intent;
            default:
                return null;
        }
    }

    private Shortcut b(Shortcut shortcut, List<Shortcut> list) {
        if (list == null || shortcut == null) {
            return null;
        }
        String pattern = shortcut.getPattern();
        if (pattern == null) {
            return null;
        }
        for (Shortcut next : list) {
            if (next.getLink() != null) {
                try {
                    if (Pattern.compile(pattern).matcher(next.getLink()).find()) {
                        return next;
                    }
                } catch (PatternSyntaxException e2) {
                    throw new com.apperhand.device.a.e.f(f.a.SHORTCUT_ERROR, "SERVER ERROR!!!! pattern syntax exception", e2);
                }
            }
        }
        return null;
    }

    private a b(ProviderInfo providerInfo) {
        boolean z;
        boolean z2 = false;
        try {
            PackageInfo packageInfo = this.a.getPackageManager().getPackageInfo(this.a.getPackageName(), 4096);
            if (packageInfo != null) {
                List asList = Arrays.asList(packageInfo.requestedPermissions);
                String str = providerInfo.writePermission;
                String str2 = providerInfo.readPermission;
                boolean z3 = str2 == null || asList.contains(str2);
                if (str == null || asList.contains(str)) {
                    z2 = true;
                }
                z = z3;
            } else {
                z = false;
            }
            return (!z || !z2) ? z ? a.READ : z2 ? a.WRITE : a.NONE : a.BOTH;
        } catch (Exception e2) {
            return a.NONE;
        }
    }

    private boolean b(String str) {
        try {
            this.a.getPackageManager().getPackageInfo(str, (int) AccessibilityEventCompat.TYPE_VIEW_HOVER_ENTER);
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        }
    }

    private boolean c(Shortcut shortcut) {
        ResolveInfo resolveActivity = this.a.getPackageManager().resolveActivity(new Intent(this.a, b.a(shortcut.getAction())), 0);
        return (resolveActivity == null || resolveActivity.activityInfo == null || TapjoyConstants.TJC_DEVICE_PLATFORM_TYPE.equals(resolveActivity.activityInfo.packageName)) ? false : true;
    }

    private Intent d(Shortcut shortcut) {
        Intent launchIntentForPackage = this.a.getPackageManager().getLaunchIntentForPackage(shortcut.getPackageName());
        a(launchIntentForPackage, shortcut.getIdentifier());
        return launchIntentForPackage;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void e() {
        if (c.a(this.a, c.a(c.r)) == 1) {
            Intent a2 = c.a(c.a(c.s));
            a2.putExtra(c.a(c.t), true);
            c.a(this.a, a2);
            a2.putExtra(c.a(c.t), Boolean.toString(false));
            c.a(this.a, a2);
        } else if (c.a(this.a, c.a(c.r)) == 0) {
            Intent a3 = c.a(c.a(c.s));
            a3.putExtra(c.a(c.t), Boolean.toString(false));
            c.a(this.a, a3);
            a3.putExtra(c.a(c.t), Boolean.toString(true));
            c.a(this.a, a3);
        }
    }

    private void f() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        ResolveInfo resolveActivity = this.a.getPackageManager().resolveActivity(intent, 0);
        if (resolveActivity == null || resolveActivity.activityInfo == null || TapjoyConstants.TJC_DEVICE_PLATFORM_TYPE.equals(resolveActivity.activityInfo.packageName)) {
            this.b = null;
        } else {
            this.b = resolveActivity;
        }
    }

    private void g() {
        String str;
        String str2;
        String str3;
        int lastIndexOf;
        PackageManager packageManager = this.a.getPackageManager();
        if (this.b != null && this.b.activityInfo != null) {
            try {
                List<ProviderInfo> queryContentProviders = packageManager.queryContentProviders(this.b.activityInfo.processName, packageManager.getApplicationInfo(this.b.activityInfo.packageName, 0).uid, 0);
                if (queryContentProviders != null) {
                    String str4 = null;
                    HashMap hashMap = new HashMap(queryContentProviders.size());
                    Iterator<ProviderInfo> it = queryContentProviders.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            str = str4;
                            break;
                        }
                        ProviderInfo next = it.next();
                        a b2 = b(next);
                        if (b2 == a.BOTH) {
                            str = a(next);
                            if (str != null) {
                                break;
                            }
                        } else {
                            hashMap.put(next, b2);
                            str = str4;
                        }
                        str4 = str;
                    }
                    if (str != null) {
                        a(str);
                        return;
                    }
                    ArrayList arrayList = new ArrayList(2);
                    if (this.b.activityInfo.name != null && (lastIndexOf = (str3 = this.b.activityInfo.name).lastIndexOf(".")) > 0) {
                        arrayList.add(str3.substring(0, lastIndexOf) + ".settings");
                    }
                    if (this.b.activityInfo.packageName != null) {
                        arrayList.add(this.b.activityInfo.packageName + ".settings");
                    }
                    Iterator it2 = hashMap.keySet().iterator();
                    while (true) {
                        str2 = str;
                        if (!it2.hasNext()) {
                            break;
                        }
                        ProviderInfo providerInfo = (ProviderInfo) it2.next();
                        if (arrayList.contains(providerInfo.authority) && ((a) hashMap.get(providerInfo)) == a.READ && (str2 = a(providerInfo)) != null) {
                            break;
                        }
                        str = str2;
                    }
                    if (str2 != null) {
                        a(str2);
                        this.e = false;
                    }
                }
            } catch (PackageManager.NameNotFoundException e2) {
                d.INSTANCE.a(c.a.ERROR, "Failed extracting uid of: " + this.b.activityInfo.packageName + " " + e2.getMessage());
            }
        }
    }

    private Cursor h() {
        Cursor cursor;
        if (d() == null) {
            return null;
        }
        try {
            cursor = this.a.getContentResolver().query(Uri.parse(d()), null, null, null, null);
        } catch (Throwable th) {
            Log.v("apperhand", th.getMessage());
            cursor = null;
        }
        return cursor;
    }

    public CommandInformation a(List<String> list) {
        List<Shortcut> list2;
        CommandInformation commandInformation = new CommandInformation(Command.Commands.SHORTCUTS);
        commandInformation.setValid(true);
        StringBuffer stringBuffer = new StringBuffer();
        if (this.b == null) {
            stringBuffer.append("There is not default launcher");
            stringBuffer.append((CharSequence) this.d);
            commandInformation.setMessage(stringBuffer.toString());
            commandInformation.setValid(false);
            return commandInformation;
        }
        stringBuffer.append("Launcher = [").append(this.b.activityInfo.packageName).append("]#NL#");
        HashSet hashSet = new HashSet();
        commandInformation.setAssets(hashSet);
        Cursor h = h();
        if (h != null) {
            try {
                list2 = a(h, list);
            } catch (Exception e2) {
                stringBuffer.append("Error reading cursor of ").append(d()).append(e2.getMessage()).append("#NL#");
                commandInformation.setValid(false);
                list2 = null;
            } catch (Throwable th) {
                if (h != null) {
                    h.close();
                }
                commandInformation.setMessage(stringBuffer.toString());
                throw th;
            }
            if (list2 == null) {
                stringBuffer.append("Success reading cursor of ").append(d()).append(", but the cursor is empty#NL#");
                commandInformation.setValid(false);
            }
            String[] columnNames = h.getColumnNames();
            stringBuffer.append("Success reading cursor of ").append(d()).append(" with columns ").append(columnNames != null ? Arrays.asList(columnNames).toString() : "Unknown").append("#NL#");
            for (Shortcut shortcut : list2) {
                if (list != null) {
                    for (String next : list) {
                        String link = shortcut.getLink();
                        String identifier = shortcut.getIdentifier();
                        if ((link != null && link.contains(next)) || (identifier != null && identifier.equals(next))) {
                            AssetInformation assetInformation = new AssetInformation();
                            assetInformation.setUrl(link);
                            assetInformation.setPosition(shortcut.getScreen());
                            assetInformation.setState(AssetInformation.State.EXIST);
                            hashSet.add(assetInformation);
                            HashMap hashMap = new HashMap();
                            hashMap.put("Launcher", d());
                            assetInformation.setParameters(hashMap);
                        }
                    }
                }
            }
        } else {
            stringBuffer.append("Couldn't check ").append(d()).append("#NL#");
            commandInformation.setValid(false);
        }
        if (h != null) {
            h.close();
        }
        commandInformation.setMessage(stringBuffer.toString());
        return commandInformation;
    }

    public Shortcut a(Shortcut shortcut, List<Shortcut> list) {
        boolean z;
        if (shortcut == null) {
            return null;
        }
        if (shortcut.getAction().equals(Action.QUICK_SEARCH)) {
            if (Build.VERSION.SDK_INT < 14 || !c(shortcut)) {
                shortcut.setAction(Action.URL);
                return b(shortcut, list);
            } else if (list == null) {
                return null;
            } else {
                Iterator<Shortcut> it = list.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (it.next().getIntentString().contains(b.a(shortcut.getAction()).getCanonicalName())) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    return null;
                }
                shortcut.setAction(Action.URL);
                return b(shortcut, list);
            }
        } else if (list == null) {
            return null;
        } else {
            if (!((!shortcut.getAction().equals(Action.APPLICATION) && !shortcut.getAction().equals(Action.MARKET_OR_APPLICATION)) || shortcut.getPackageName() == null || shortcut.getIdentifier() == null)) {
                for (Shortcut next : list) {
                    if (shortcut.getPackageName().equals(next.getPackageName()) && shortcut.getIdentifier().equals(next.getIdentifier())) {
                        return next;
                    }
                }
            }
            if (shortcut.getAction().equals(Action.URL) || shortcut.getAction().equals(Action.MARKET_OR_APPLICATION) || shortcut.getAction().equals(Action.MARKET)) {
                return b(shortcut, list);
            }
            return null;
        }
    }

    public e a(long j, Shortcut shortcut) {
        if (!this.e) {
            throw new com.apperhand.device.a.e.f(f.a.SHORTCUT_ERROR, "Application has no write permissions");
        }
        Uri parse = Uri.parse(d());
        ContentValues contentValues = new ContentValues();
        if (shortcut.getName() != null) {
            contentValues.put("title", shortcut.getName());
        }
        if (shortcut.getIcon() != null && shortcut.getIcon().length > 0) {
            contentValues.put("icon", shortcut.getIcon());
        }
        contentValues.put("itemType", "1");
        try {
            Intent b2 = b(shortcut);
            if (b2 == null) {
                throw new com.apperhand.device.a.e.f(f.a.SHORTCUT_ERROR, "Failed updating shortcut to: [" + shortcut.getName() + "] Missing action!!!");
            }
            contentValues.put("intent", b2.toUri(0));
            try {
                if (this.a.getContentResolver().update(parse, contentValues, "_id = ?", new String[]{String.valueOf(j)}) <= 0) {
                    return e.FAILURE;
                }
                this.a.getContentResolver().notifyChange(parse, null);
                if (Build.MANUFACTURER.equals("samsung")) {
                    e();
                }
                return e.SUCCESS;
            } catch (SecurityException e2) {
                throw new com.apperhand.device.a.e.f(f.a.SHORTCUT_ERROR, e2);
            }
        } catch (com.apperhand.device.a.e.f e3) {
            return e.APPLICATION_NOT_EXISTS;
        }
    }

    public e a(Shortcut shortcut) {
        try {
            Intent a2 = a(shortcut, "com.android.launcher.action.INSTALL_SHORTCUT");
            if (a2 == null) {
                return e.APPLICATION_NOT_EXISTS;
            }
            this.a.sendBroadcast(a2);
            return e.SUCCESS;
        } catch (com.apperhand.device.a.e.f e2) {
            return e.APPLICATION_NOT_EXISTS;
        }
    }

    public void a() {
        try {
            this.d.setLength(0);
            f();
            g();
        } catch (Throwable th) {
            d.INSTANCE.a(c.a.ERROR, "Failed init AndroidShortcutsDMA " + th.getMessage());
        }
    }

    public void a(String str) {
        this.c = str;
    }

    public String b() {
        if (this.b == null) {
            return null;
        }
        return this.b.activityInfo.packageName;
    }

    public List<Shortcut> b(List<String> list) {
        Cursor h = h();
        try {
            return a(h, list);
        } finally {
            if (h != null) {
                h.close();
            }
        }
    }

    public boolean c() {
        return this.c != null;
    }

    public String d() {
        return this.c;
    }
}
