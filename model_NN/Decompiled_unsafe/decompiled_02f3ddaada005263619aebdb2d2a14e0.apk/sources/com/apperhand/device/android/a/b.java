package com.apperhand.device.android.a;

import com.apperhand.common.dto.Action;
import com.apperhand.device.android.AndroidSearchActivity;
import java.util.HashMap;
import java.util.Map;

public class b {
    private static final Map<Action, Class<?>> a = new HashMap<Action, Class<?>>(1) {
        {
            put(Action.QUICK_SEARCH, AndroidSearchActivity.class);
        }
    };

    public static Class<?> a(Action action) {
        return a.get(action);
    }
}
