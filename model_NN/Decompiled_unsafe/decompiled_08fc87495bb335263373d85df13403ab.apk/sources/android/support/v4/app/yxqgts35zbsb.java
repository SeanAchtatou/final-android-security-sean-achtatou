package android.support.v4.app;

import android.os.Bundle;
import android.support.v4.a.cehyzt7dw;
import android.support.v4.a.uin6g3d5rqgcbs;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;

final class yxqgts35zbsb implements uin6g3d5rqgcbs {
    boolean ay6ebym1yp0qgk;
    boolean b5zlaptmyxarl;
    final /* synthetic */ kld4qxthnxo5uo bpogj6;
    ty7df019s cehyzt7dw;
    Object e8kxjqktk9t;
    boolean ef5tn1cvshg414;
    boolean fxug2rdnfo;
    boolean iux03f6yieb;
    boolean lg71ytkvzw;
    boolean mhtc4dliin7r;
    yxqgts35zbsb oc9mgl157cp;
    final Bundle ozpoxuz523b2;
    final int ttmhx7;
    cehyzt7dw uin6g3d5rqgcbs;
    boolean usuayu88rw4;

    /* access modifiers changed from: package-private */
    public void cehyzt7dw() {
        if (this.ef5tn1cvshg414) {
            if (kld4qxthnxo5uo.ttmhx7) {
                Log.v("LoaderManager", "  Finished Retaining: " + this);
            }
            this.ef5tn1cvshg414 = false;
            if (this.lg71ytkvzw != this.b5zlaptmyxarl && !this.lg71ytkvzw) {
                usuayu88rw4();
            }
        }
        if (this.lg71ytkvzw && this.usuayu88rw4 && !this.iux03f6yieb) {
            ttmhx7(this.uin6g3d5rqgcbs, this.e8kxjqktk9t);
        }
    }

    /* access modifiers changed from: package-private */
    public void fxug2rdnfo() {
        String str;
        if (kld4qxthnxo5uo.ttmhx7) {
            Log.v("LoaderManager", "  Destroying: " + this);
        }
        this.ay6ebym1yp0qgk = true;
        boolean z = this.fxug2rdnfo;
        this.fxug2rdnfo = false;
        if (this.cehyzt7dw != null && this.uin6g3d5rqgcbs != null && this.usuayu88rw4 && z) {
            if (kld4qxthnxo5uo.ttmhx7) {
                Log.v("LoaderManager", "  Reseting: " + this);
            }
            if (this.bpogj6.usuayu88rw4 != null) {
                String str2 = this.bpogj6.usuayu88rw4.ozpoxuz523b2.mqnmk83l0o;
                this.bpogj6.usuayu88rw4.ozpoxuz523b2.mqnmk83l0o = "onLoaderReset";
                str = str2;
            } else {
                str = null;
            }
            try {
                this.cehyzt7dw.ttmhx7(this.uin6g3d5rqgcbs);
            } finally {
                if (this.bpogj6.usuayu88rw4 != null) {
                    this.bpogj6.usuayu88rw4.ozpoxuz523b2.mqnmk83l0o = str;
                }
            }
        }
        this.cehyzt7dw = null;
        this.e8kxjqktk9t = null;
        this.usuayu88rw4 = false;
        if (this.uin6g3d5rqgcbs != null) {
            if (this.mhtc4dliin7r) {
                this.mhtc4dliin7r = false;
                this.uin6g3d5rqgcbs.ttmhx7((uin6g3d5rqgcbs) this);
            }
            this.uin6g3d5rqgcbs.usuayu88rw4();
        }
        if (this.oc9mgl157cp != null) {
            this.oc9mgl157cp.fxug2rdnfo();
        }
    }

    /* access modifiers changed from: package-private */
    public void ozpoxuz523b2() {
        if (kld4qxthnxo5uo.ttmhx7) {
            Log.v("LoaderManager", "  Retaining: " + this);
        }
        this.ef5tn1cvshg414 = true;
        this.b5zlaptmyxarl = this.lg71ytkvzw;
        this.lg71ytkvzw = false;
        this.cehyzt7dw = null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        sb.append("LoaderInfo{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" #");
        sb.append(this.ttmhx7);
        sb.append(" : ");
        android.support.v4.c.uin6g3d5rqgcbs.ttmhx7(this.uin6g3d5rqgcbs, sb);
        sb.append("}}");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7() {
        if (this.ef5tn1cvshg414 && this.b5zlaptmyxarl) {
            this.lg71ytkvzw = true;
        } else if (!this.lg71ytkvzw) {
            this.lg71ytkvzw = true;
            if (kld4qxthnxo5uo.ttmhx7) {
                Log.v("LoaderManager", "  Starting: " + this);
            }
            if (this.uin6g3d5rqgcbs == null && this.cehyzt7dw != null) {
                this.uin6g3d5rqgcbs = this.cehyzt7dw.ttmhx7(this.ttmhx7, this.ozpoxuz523b2);
            }
            if (this.uin6g3d5rqgcbs == null) {
                return;
            }
            if (!this.uin6g3d5rqgcbs.getClass().isMemberClass() || Modifier.isStatic(this.uin6g3d5rqgcbs.getClass().getModifiers())) {
                if (!this.mhtc4dliin7r) {
                    this.uin6g3d5rqgcbs.ttmhx7(this.ttmhx7, this);
                    this.mhtc4dliin7r = true;
                }
                this.uin6g3d5rqgcbs.ttmhx7();
                return;
            }
            throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + this.uin6g3d5rqgcbs);
        }
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7(cehyzt7dw cehyzt7dw2, Object obj) {
        String str;
        if (this.cehyzt7dw != null) {
            if (this.bpogj6.usuayu88rw4 != null) {
                String str2 = this.bpogj6.usuayu88rw4.ozpoxuz523b2.mqnmk83l0o;
                this.bpogj6.usuayu88rw4.ozpoxuz523b2.mqnmk83l0o = "onLoadFinished";
                str = str2;
            } else {
                str = null;
            }
            try {
                if (kld4qxthnxo5uo.ttmhx7) {
                    Log.v("LoaderManager", "  onLoadFinished in " + cehyzt7dw2 + ": " + cehyzt7dw2.ttmhx7(obj));
                }
                this.cehyzt7dw.ttmhx7(cehyzt7dw2, obj);
                this.fxug2rdnfo = true;
            } finally {
                if (this.bpogj6.usuayu88rw4 != null) {
                    this.bpogj6.usuayu88rw4.ozpoxuz523b2.mqnmk83l0o = str;
                }
            }
        }
    }

    public void ttmhx7(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.ttmhx7);
        printWriter.print(" mArgs=");
        printWriter.println(this.ozpoxuz523b2);
        printWriter.print(str);
        printWriter.print("mCallbacks=");
        printWriter.println(this.cehyzt7dw);
        printWriter.print(str);
        printWriter.print("mLoader=");
        printWriter.println(this.uin6g3d5rqgcbs);
        if (this.uin6g3d5rqgcbs != null) {
            this.uin6g3d5rqgcbs.ttmhx7(str + "  ", fileDescriptor, printWriter, strArr);
        }
        if (this.usuayu88rw4 || this.fxug2rdnfo) {
            printWriter.print(str);
            printWriter.print("mHaveData=");
            printWriter.print(this.usuayu88rw4);
            printWriter.print("  mDeliveredData=");
            printWriter.println(this.fxug2rdnfo);
            printWriter.print(str);
            printWriter.print("mData=");
            printWriter.println(this.e8kxjqktk9t);
        }
        printWriter.print(str);
        printWriter.print("mStarted=");
        printWriter.print(this.lg71ytkvzw);
        printWriter.print(" mReportNextStart=");
        printWriter.print(this.iux03f6yieb);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.ay6ebym1yp0qgk);
        printWriter.print(str);
        printWriter.print("mRetaining=");
        printWriter.print(this.ef5tn1cvshg414);
        printWriter.print(" mRetainingStarted=");
        printWriter.print(this.b5zlaptmyxarl);
        printWriter.print(" mListenerRegistered=");
        printWriter.println(this.mhtc4dliin7r);
        if (this.oc9mgl157cp != null) {
            printWriter.print(str);
            printWriter.println("Pending Loader ");
            printWriter.print(this.oc9mgl157cp);
            printWriter.println(":");
            this.oc9mgl157cp.ttmhx7(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }

    /* access modifiers changed from: package-private */
    public void uin6g3d5rqgcbs() {
        if (this.lg71ytkvzw && this.iux03f6yieb) {
            this.iux03f6yieb = false;
            if (this.usuayu88rw4) {
                ttmhx7(this.uin6g3d5rqgcbs, this.e8kxjqktk9t);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void usuayu88rw4() {
        if (kld4qxthnxo5uo.ttmhx7) {
            Log.v("LoaderManager", "  Stopping: " + this);
        }
        this.lg71ytkvzw = false;
        if (!this.ef5tn1cvshg414 && this.uin6g3d5rqgcbs != null && this.mhtc4dliin7r) {
            this.mhtc4dliin7r = false;
            this.uin6g3d5rqgcbs.ttmhx7((uin6g3d5rqgcbs) this);
            this.uin6g3d5rqgcbs.cehyzt7dw();
        }
    }
}
