package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.c.ay6ebym1yp0qgk;
import android.support.v4.c.uin6g3d5rqgcbs;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Fragment implements ComponentCallbacks, View.OnCreateContextMenuListener {
    private static final ay6ebym1yp0qgk olqqn4x4 = new ay6ebym1yp0qgk();
    static final Object ttmhx7 = new Object();
    Boolean a5uhwh1;
    String aecbla89ntoa8;
    int ay6ebym1yp0qgk;
    Fragment b5zlaptmyxarl;
    ViewGroup b6p1j7hoons8;
    int bjcn50q4e9;
    boolean bpogj6;
    boolean ca2ssr26fefu;
    View cehyzt7dw;
    kld4qxthnxo5uo ck0x6f;
    rulrdod1midre cpgyvt8o4r3;
    int e8kxjqktk9t = -1;
    Bundle ef5tn1cvshg414;
    int eyli1ymagd3o;
    boolean flawb66z00q;
    boolean ftlyjgoncub6q;
    Object fx9gujks = ttmhx7;
    SparseArray fxug2rdnfo;
    Boolean hajwjku;
    Object i422h07p3ed = null;
    boolean ijavw7l1x;
    int iux03f6yieb = -1;
    Object j72htm5 = ttmhx7;
    boolean jaztd5t99d;
    boolean k3jokks5k5;
    boolean kld4qxthnxo5uo;
    String lg71ytkvzw;
    rulrdod1midre lqwegpi5;
    boolean mhtc4dliin7r;
    oc9mgl157cp mqnmk83l0o;
    ug2s4y o9ph3xbm2yk6g = null;
    boolean oc9mgl157cp;
    Fragment ol99ycz2wbkd;
    View oziax9tu6k;
    int ozpoxuz523b2 = 0;
    boolean rob6sujr97;
    int rulrdod1midre;
    boolean s6o869vduri;
    int sgnd7s4;
    View ty7df019s;
    boolean ug2s4y;
    int uin6g3d5rqgcbs;
    Bundle usuayu88rw4;
    ug2s4y w3g96lv8 = null;
    Object w5mzcxwa3kamml = null;
    boolean wg4f90m80dyc0s = true;
    boolean xbcow1jyae;
    Object xf1q4c = null;
    boolean yxqgts35zbsb = true;
    Object zk18i66egwxe = ttmhx7;
    boolean zs1ge47fq1dgv5;

    public class SavedState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new mhtc4dliin7r();
        final Bundle ttmhx7;

        SavedState(Parcel parcel, ClassLoader classLoader) {
            this.ttmhx7 = parcel.readBundle();
            if (classLoader != null && this.ttmhx7 != null) {
                this.ttmhx7.setClassLoader(classLoader);
            }
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeBundle(this.ttmhx7);
        }
    }

    static boolean ozpoxuz523b2(Context context, String str) {
        try {
            Class<?> cls = (Class) olqqn4x4.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                olqqn4x4.put(str, cls);
            }
            return Fragment.class.isAssignableFrom(cls);
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public static Fragment ttmhx7(Context context, String str) {
        return ttmhx7(context, str, (Bundle) null);
    }

    public static Fragment ttmhx7(Context context, String str, Bundle bundle) {
        try {
            Class<?> cls = (Class) olqqn4x4.get(str);
            if (cls == null) {
                cls = context.getClassLoader().loadClass(str);
                olqqn4x4.put(str, cls);
            }
            Fragment fragment = (Fragment) cls.newInstance();
            if (bundle != null) {
                bundle.setClassLoader(fragment.getClass().getClassLoader());
                fragment.ef5tn1cvshg414 = bundle;
            }
            return fragment;
        } catch (ClassNotFoundException e) {
            throw new ay6ebym1yp0qgk("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e);
        } catch (InstantiationException e2) {
            throw new ay6ebym1yp0qgk("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e2);
        } catch (IllegalAccessException e3) {
            throw new ay6ebym1yp0qgk("Unable to instantiate fragment " + str + ": make sure class name exists, is public, and has an" + " empty constructor that is public", e3);
        }
    }

    /* access modifiers changed from: package-private */
    public void aecbla89ntoa8() {
        this.lqwegpi5 = new rulrdod1midre();
        this.lqwegpi5.ttmhx7(this.mqnmk83l0o, new iux03f6yieb(this), this);
    }

    public void ay6ebym1yp0qgk() {
        this.jaztd5t99d = true;
    }

    public void b5zlaptmyxarl() {
        this.jaztd5t99d = true;
    }

    /* access modifiers changed from: package-private */
    public void bjcn50q4e9() {
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.k3jokks5k5();
        }
        this.jaztd5t99d = false;
        oc9mgl157cp();
        if (!this.jaztd5t99d) {
            throw new rob6sujr97("Fragment " + this + " did not call through to super.onDestroy()");
        }
    }

    /* access modifiers changed from: package-private */
    public void bpogj6() {
        this.e8kxjqktk9t = -1;
        this.lg71ytkvzw = null;
        this.mhtc4dliin7r = false;
        this.oc9mgl157cp = false;
        this.bpogj6 = false;
        this.ca2ssr26fefu = false;
        this.flawb66z00q = false;
        this.k3jokks5k5 = false;
        this.rulrdod1midre = 0;
        this.cpgyvt8o4r3 = null;
        this.lqwegpi5 = null;
        this.mqnmk83l0o = null;
        this.eyli1ymagd3o = 0;
        this.sgnd7s4 = 0;
        this.aecbla89ntoa8 = null;
        this.zs1ge47fq1dgv5 = false;
        this.xbcow1jyae = false;
        this.ijavw7l1x = false;
        this.ck0x6f = null;
        this.ug2s4y = false;
        this.rob6sujr97 = false;
    }

    public void ca2ssr26fefu() {
        this.jaztd5t99d = true;
    }

    public final Resources cehyzt7dw() {
        if (this.mqnmk83l0o != null) {
            return this.mqnmk83l0o.getResources();
        }
        throw new IllegalStateException("Fragment " + this + " not attached to Activity");
    }

    public void cehyzt7dw(Bundle bundle) {
        this.jaztd5t99d = true;
    }

    /* access modifiers changed from: package-private */
    public boolean cehyzt7dw(Menu menu) {
        boolean z = false;
        if (this.zs1ge47fq1dgv5) {
            return false;
        }
        if (this.s6o869vduri && this.wg4f90m80dyc0s) {
            z = true;
            ttmhx7(menu);
        }
        return this.lqwegpi5 != null ? z | this.lqwegpi5.ttmhx7(menu) : z;
    }

    /* access modifiers changed from: package-private */
    public boolean cehyzt7dw(MenuItem menuItem) {
        if (!this.zs1ge47fq1dgv5) {
            if (!this.s6o869vduri || !this.wg4f90m80dyc0s || !ttmhx7(menuItem)) {
                return this.lqwegpi5 != null && this.lqwegpi5.ttmhx7(menuItem);
            }
            return true;
        }
    }

    public Object cpgyvt8o4r3() {
        return this.xf1q4c;
    }

    /* access modifiers changed from: package-private */
    public void e8kxjqktk9t(Bundle bundle) {
        Parcelable parcelable;
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.ef5tn1cvshg414();
        }
        this.jaztd5t99d = false;
        cehyzt7dw(bundle);
        if (!this.jaztd5t99d) {
            throw new rob6sujr97("Fragment " + this + " did not call through to super.onCreate()");
        } else if (bundle != null && (parcelable = bundle.getParcelable("android:support:fragments")) != null) {
            if (this.lqwegpi5 == null) {
                aecbla89ntoa8();
            }
            this.lqwegpi5.ttmhx7(parcelable, (ArrayList) null);
            this.lqwegpi5.b5zlaptmyxarl();
        }
    }

    public final boolean e8kxjqktk9t() {
        return this.zs1ge47fq1dgv5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, boolean, boolean):android.support.v4.app.kld4qxthnxo5uo
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, boolean, boolean):android.support.v4.app.kld4qxthnxo5uo */
    public void ef5tn1cvshg414() {
        this.jaztd5t99d = true;
        if (!this.ug2s4y) {
            this.ug2s4y = true;
            if (!this.rob6sujr97) {
                this.rob6sujr97 = true;
                this.ck0x6f = this.mqnmk83l0o.ttmhx7(this.lg71ytkvzw, this.ug2s4y, false);
            }
            if (this.ck0x6f != null) {
                this.ck0x6f.ozpoxuz523b2();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ef5tn1cvshg414(Bundle bundle) {
        Parcelable lg71ytkvzw2;
        fxug2rdnfo(bundle);
        if (this.lqwegpi5 != null && (lg71ytkvzw2 = this.lqwegpi5.lg71ytkvzw()) != null) {
            bundle.putParcelable("android:support:fragments", lg71ytkvzw2);
        }
    }

    public final boolean equals(Object obj) {
        return super.equals(obj);
    }

    public boolean eyli1ymagd3o() {
        if (this.a5uhwh1 == null) {
            return true;
        }
        return this.a5uhwh1.booleanValue();
    }

    public void flawb66z00q() {
    }

    /* access modifiers changed from: package-private */
    public void ftlyjgoncub6q() {
        onLowMemory();
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.rulrdod1midre();
        }
    }

    public void fxug2rdnfo(Bundle bundle) {
    }

    public final boolean fxug2rdnfo() {
        return this.xbcow1jyae;
    }

    public final int hashCode() {
        return super.hashCode();
    }

    /* access modifiers changed from: package-private */
    public void ijavw7l1x() {
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.oc9mgl157cp();
        }
        this.jaztd5t99d = false;
        iux03f6yieb();
        if (!this.jaztd5t99d) {
            throw new rob6sujr97("Fragment " + this + " did not call through to super.onPause()");
        }
    }

    public void iux03f6yieb() {
        this.jaztd5t99d = true;
    }

    /* access modifiers changed from: package-private */
    public void jaztd5t99d() {
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.flawb66z00q();
        }
        this.jaztd5t99d = false;
        mhtc4dliin7r();
        if (!this.jaztd5t99d) {
            throw new rob6sujr97("Fragment " + this + " did not call through to super.onDestroyView()");
        } else if (this.ck0x6f != null) {
            this.ck0x6f.fxug2rdnfo();
        }
    }

    public Object k3jokks5k5() {
        return this.w5mzcxwa3kamml;
    }

    public View lg71ytkvzw() {
        return this.oziax9tu6k;
    }

    /* access modifiers changed from: package-private */
    public void lg71ytkvzw(Bundle bundle) {
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.ef5tn1cvshg414();
        }
        this.jaztd5t99d = false;
        uin6g3d5rqgcbs(bundle);
        if (!this.jaztd5t99d) {
            throw new rob6sujr97("Fragment " + this + " did not call through to super.onActivityCreated()");
        } else if (this.lqwegpi5 != null) {
            this.lqwegpi5.iux03f6yieb();
        }
    }

    public Object lqwegpi5() {
        return this.i422h07p3ed;
    }

    public void mhtc4dliin7r() {
        this.jaztd5t99d = true;
    }

    public Object mqnmk83l0o() {
        return this.zk18i66egwxe == ttmhx7 ? cpgyvt8o4r3() : this.zk18i66egwxe;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, boolean, boolean):android.support.v4.app.kld4qxthnxo5uo
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, boolean, boolean):android.support.v4.app.kld4qxthnxo5uo */
    public void oc9mgl157cp() {
        this.jaztd5t99d = true;
        if (!this.rob6sujr97) {
            this.rob6sujr97 = true;
            this.ck0x6f = this.mqnmk83l0o.ttmhx7(this.lg71ytkvzw, this.ug2s4y, false);
        }
        if (this.ck0x6f != null) {
            this.ck0x6f.lg71ytkvzw();
        }
    }

    public Object ol99ycz2wbkd() {
        return this.fx9gujks == ttmhx7 ? lqwegpi5() : this.fx9gujks;
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.jaztd5t99d = true;
    }

    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        ozpoxuz523b2().onCreateContextMenu(contextMenu, view, contextMenuInfo);
    }

    public void onLowMemory() {
        this.jaztd5t99d = true;
    }

    public final oc9mgl157cp ozpoxuz523b2() {
        return this.mqnmk83l0o;
    }

    public LayoutInflater ozpoxuz523b2(Bundle bundle) {
        LayoutInflater cloneInContext = this.mqnmk83l0o.getLayoutInflater().cloneInContext(this.mqnmk83l0o);
        uin6g3d5rqgcbs();
        cloneInContext.setFactory(this.lqwegpi5.cpgyvt8o4r3());
        return cloneInContext;
    }

    /* access modifiers changed from: package-private */
    public View ozpoxuz523b2(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.ef5tn1cvshg414();
        }
        return ttmhx7(layoutInflater, viewGroup, bundle);
    }

    public void ozpoxuz523b2(Menu menu) {
    }

    /* access modifiers changed from: package-private */
    public boolean ozpoxuz523b2(Menu menu, MenuInflater menuInflater) {
        boolean z = false;
        if (this.zs1ge47fq1dgv5) {
            return false;
        }
        if (this.s6o869vduri && this.wg4f90m80dyc0s) {
            z = true;
            ttmhx7(menu, menuInflater);
        }
        return this.lqwegpi5 != null ? z | this.lqwegpi5.ttmhx7(menu, menuInflater) : z;
    }

    public boolean ozpoxuz523b2(MenuItem menuItem) {
        return false;
    }

    public Object rulrdod1midre() {
        return this.j72htm5 == ttmhx7 ? k3jokks5k5() : this.j72htm5;
    }

    /* access modifiers changed from: package-private */
    public void s6o869vduri() {
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.bpogj6();
        }
        this.jaztd5t99d = false;
        ay6ebym1yp0qgk();
        if (!this.jaztd5t99d) {
            throw new rob6sujr97("Fragment " + this + " did not call through to super.onStop()");
        }
    }

    public boolean sgnd7s4() {
        if (this.hajwjku == null) {
            return true;
        }
        return this.hajwjku.booleanValue();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        uin6g3d5rqgcbs.ttmhx7(this, sb);
        if (this.e8kxjqktk9t >= 0) {
            sb.append(" #");
            sb.append(this.e8kxjqktk9t);
        }
        if (this.eyli1ymagd3o != 0) {
            sb.append(" id=0x");
            sb.append(Integer.toHexString(this.eyli1ymagd3o));
        }
        if (this.aecbla89ntoa8 != null) {
            sb.append(" ");
            sb.append(this.aecbla89ntoa8);
        }
        sb.append('}');
        return sb.toString();
    }

    public View ttmhx7(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return null;
    }

    public Animation ttmhx7(int i, boolean z, int i2) {
        return null;
    }

    public void ttmhx7(int i, int i2, Intent intent) {
    }

    /* access modifiers changed from: package-private */
    public final void ttmhx7(int i, Fragment fragment) {
        this.e8kxjqktk9t = i;
        if (fragment != null) {
            this.lg71ytkvzw = fragment.lg71ytkvzw + ":" + this.e8kxjqktk9t;
        } else {
            this.lg71ytkvzw = "android:fragment:" + this.e8kxjqktk9t;
        }
    }

    public void ttmhx7(Activity activity) {
        this.jaztd5t99d = true;
    }

    public void ttmhx7(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        this.jaztd5t99d = true;
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7(Configuration configuration) {
        onConfigurationChanged(configuration);
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.ttmhx7(configuration);
        }
    }

    /* access modifiers changed from: package-private */
    public final void ttmhx7(Bundle bundle) {
        if (this.fxug2rdnfo != null) {
            this.ty7df019s.restoreHierarchyState(this.fxug2rdnfo);
            this.fxug2rdnfo = null;
        }
        this.jaztd5t99d = false;
        usuayu88rw4(bundle);
        if (!this.jaztd5t99d) {
            throw new rob6sujr97("Fragment " + this + " did not call through to super.onViewStateRestored()");
        }
    }

    public void ttmhx7(Menu menu) {
    }

    public void ttmhx7(Menu menu, MenuInflater menuInflater) {
    }

    public void ttmhx7(View view, Bundle bundle) {
    }

    public void ttmhx7(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mFragmentId=#");
        printWriter.print(Integer.toHexString(this.eyli1ymagd3o));
        printWriter.print(" mContainerId=#");
        printWriter.print(Integer.toHexString(this.sgnd7s4));
        printWriter.print(" mTag=");
        printWriter.println(this.aecbla89ntoa8);
        printWriter.print(str);
        printWriter.print("mState=");
        printWriter.print(this.ozpoxuz523b2);
        printWriter.print(" mIndex=");
        printWriter.print(this.e8kxjqktk9t);
        printWriter.print(" mWho=");
        printWriter.print(this.lg71ytkvzw);
        printWriter.print(" mBackStackNesting=");
        printWriter.println(this.rulrdod1midre);
        printWriter.print(str);
        printWriter.print("mAdded=");
        printWriter.print(this.mhtc4dliin7r);
        printWriter.print(" mRemoving=");
        printWriter.print(this.oc9mgl157cp);
        printWriter.print(" mResumed=");
        printWriter.print(this.bpogj6);
        printWriter.print(" mFromLayout=");
        printWriter.print(this.ca2ssr26fefu);
        printWriter.print(" mInLayout=");
        printWriter.println(this.flawb66z00q);
        printWriter.print(str);
        printWriter.print("mHidden=");
        printWriter.print(this.zs1ge47fq1dgv5);
        printWriter.print(" mDetached=");
        printWriter.print(this.xbcow1jyae);
        printWriter.print(" mMenuVisible=");
        printWriter.print(this.wg4f90m80dyc0s);
        printWriter.print(" mHasMenu=");
        printWriter.println(this.s6o869vduri);
        printWriter.print(str);
        printWriter.print("mRetainInstance=");
        printWriter.print(this.ftlyjgoncub6q);
        printWriter.print(" mRetaining=");
        printWriter.print(this.ijavw7l1x);
        printWriter.print(" mUserVisibleHint=");
        printWriter.println(this.yxqgts35zbsb);
        if (this.cpgyvt8o4r3 != null) {
            printWriter.print(str);
            printWriter.print("mFragmentManager=");
            printWriter.println(this.cpgyvt8o4r3);
        }
        if (this.mqnmk83l0o != null) {
            printWriter.print(str);
            printWriter.print("mActivity=");
            printWriter.println(this.mqnmk83l0o);
        }
        if (this.ol99ycz2wbkd != null) {
            printWriter.print(str);
            printWriter.print("mParentFragment=");
            printWriter.println(this.ol99ycz2wbkd);
        }
        if (this.ef5tn1cvshg414 != null) {
            printWriter.print(str);
            printWriter.print("mArguments=");
            printWriter.println(this.ef5tn1cvshg414);
        }
        if (this.usuayu88rw4 != null) {
            printWriter.print(str);
            printWriter.print("mSavedFragmentState=");
            printWriter.println(this.usuayu88rw4);
        }
        if (this.fxug2rdnfo != null) {
            printWriter.print(str);
            printWriter.print("mSavedViewState=");
            printWriter.println(this.fxug2rdnfo);
        }
        if (this.b5zlaptmyxarl != null) {
            printWriter.print(str);
            printWriter.print("mTarget=");
            printWriter.print(this.b5zlaptmyxarl);
            printWriter.print(" mTargetRequestCode=");
            printWriter.println(this.ay6ebym1yp0qgk);
        }
        if (this.bjcn50q4e9 != 0) {
            printWriter.print(str);
            printWriter.print("mNextAnim=");
            printWriter.println(this.bjcn50q4e9);
        }
        if (this.b6p1j7hoons8 != null) {
            printWriter.print(str);
            printWriter.print("mContainer=");
            printWriter.println(this.b6p1j7hoons8);
        }
        if (this.oziax9tu6k != null) {
            printWriter.print(str);
            printWriter.print("mView=");
            printWriter.println(this.oziax9tu6k);
        }
        if (this.ty7df019s != null) {
            printWriter.print(str);
            printWriter.print("mInnerView=");
            printWriter.println(this.oziax9tu6k);
        }
        if (this.cehyzt7dw != null) {
            printWriter.print(str);
            printWriter.print("mAnimatingAway=");
            printWriter.println(this.cehyzt7dw);
            printWriter.print(str);
            printWriter.print("mStateAfterAnimating=");
            printWriter.println(this.uin6g3d5rqgcbs);
        }
        if (this.ck0x6f != null) {
            printWriter.print(str);
            printWriter.println("Loader Manager:");
            this.ck0x6f.ttmhx7(str + "  ", fileDescriptor, printWriter, strArr);
        }
        if (this.lqwegpi5 != null) {
            printWriter.print(str);
            printWriter.println("Child " + this.lqwegpi5 + ":");
            this.lqwegpi5.ttmhx7(str + "  ", fileDescriptor, printWriter, strArr);
        }
    }

    public void ttmhx7(boolean z) {
    }

    /* access modifiers changed from: package-private */
    public final boolean ttmhx7() {
        return this.rulrdod1midre > 0;
    }

    public boolean ttmhx7(MenuItem menuItem) {
        return false;
    }

    public final flawb66z00q uin6g3d5rqgcbs() {
        if (this.lqwegpi5 == null) {
            aecbla89ntoa8();
            if (this.ozpoxuz523b2 >= 5) {
                this.lqwegpi5.mhtc4dliin7r();
            } else if (this.ozpoxuz523b2 >= 4) {
                this.lqwegpi5.ay6ebym1yp0qgk();
            } else if (this.ozpoxuz523b2 >= 2) {
                this.lqwegpi5.iux03f6yieb();
            } else if (this.ozpoxuz523b2 >= 1) {
                this.lqwegpi5.b5zlaptmyxarl();
            }
        }
        return this.lqwegpi5;
    }

    public void uin6g3d5rqgcbs(Bundle bundle) {
        this.jaztd5t99d = true;
    }

    /* access modifiers changed from: package-private */
    public void uin6g3d5rqgcbs(Menu menu) {
        if (!this.zs1ge47fq1dgv5) {
            if (this.s6o869vduri && this.wg4f90m80dyc0s) {
                ozpoxuz523b2(menu);
            }
            if (this.lqwegpi5 != null) {
                this.lqwegpi5.ozpoxuz523b2(menu);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean uin6g3d5rqgcbs(MenuItem menuItem) {
        if (!this.zs1ge47fq1dgv5) {
            if (ozpoxuz523b2(menuItem)) {
                return true;
            }
            return this.lqwegpi5 != null && this.lqwegpi5.ozpoxuz523b2(menuItem);
        }
    }

    public void usuayu88rw4(Bundle bundle) {
        this.jaztd5t99d = true;
    }

    public final boolean usuayu88rw4() {
        return this.mqnmk83l0o != null && this.mhtc4dliin7r;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, boolean, boolean):android.support.v4.app.kld4qxthnxo5uo
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, boolean, boolean):android.support.v4.app.kld4qxthnxo5uo */
    /* access modifiers changed from: package-private */
    public void wg4f90m80dyc0s() {
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.ca2ssr26fefu();
        }
        if (this.ug2s4y) {
            this.ug2s4y = false;
            if (!this.rob6sujr97) {
                this.rob6sujr97 = true;
                this.ck0x6f = this.mqnmk83l0o.ttmhx7(this.lg71ytkvzw, this.ug2s4y, false);
            }
            if (this.ck0x6f == null) {
                return;
            }
            if (!this.mqnmk83l0o.lg71ytkvzw) {
                this.ck0x6f.cehyzt7dw();
            } else {
                this.ck0x6f.uin6g3d5rqgcbs();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void xbcow1jyae() {
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.ef5tn1cvshg414();
            this.lqwegpi5.usuayu88rw4();
        }
        this.jaztd5t99d = false;
        b5zlaptmyxarl();
        if (!this.jaztd5t99d) {
            throw new rob6sujr97("Fragment " + this + " did not call through to super.onResume()");
        } else if (this.lqwegpi5 != null) {
            this.lqwegpi5.mhtc4dliin7r();
            this.lqwegpi5.usuayu88rw4();
        }
    }

    /* access modifiers changed from: package-private */
    public void zs1ge47fq1dgv5() {
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.ef5tn1cvshg414();
            this.lqwegpi5.usuayu88rw4();
        }
        this.jaztd5t99d = false;
        ef5tn1cvshg414();
        if (!this.jaztd5t99d) {
            throw new rob6sujr97("Fragment " + this + " did not call through to super.onStart()");
        }
        if (this.lqwegpi5 != null) {
            this.lqwegpi5.ay6ebym1yp0qgk();
        }
        if (this.ck0x6f != null) {
            this.ck0x6f.e8kxjqktk9t();
        }
    }
}
