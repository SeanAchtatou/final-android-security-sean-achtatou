package android.support.v4.media.session;

import android.os.Build;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.v4.media.MediaDescriptionCompat;

public class MediaSessionCompat {

    public final class QueueItem implements Parcelable {
        public static final Parcelable.Creator CREATOR = new ozpoxuz523b2();
        private final long ozpoxuz523b2;
        private final MediaDescriptionCompat ttmhx7;

        private QueueItem(Parcel parcel) {
            this.ttmhx7 = (MediaDescriptionCompat) MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
            this.ozpoxuz523b2 = parcel.readLong();
        }

        public int describeContents() {
            return 0;
        }

        public String toString() {
            return "MediaSession.QueueItem {Description=" + this.ttmhx7 + ", Id=" + this.ozpoxuz523b2 + " }";
        }

        public void writeToParcel(Parcel parcel, int i) {
            this.ttmhx7.writeToParcel(parcel, i);
            parcel.writeLong(this.ozpoxuz523b2);
        }
    }

    final class ResultReceiverWrapper implements Parcelable {
        public static final Parcelable.Creator CREATOR = new cehyzt7dw();
        private ResultReceiver ttmhx7;

        ResultReceiverWrapper(Parcel parcel) {
            this.ttmhx7 = (ResultReceiver) ResultReceiver.CREATOR.createFromParcel(parcel);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            this.ttmhx7.writeToParcel(parcel, i);
        }
    }

    public final class Token implements Parcelable {
        public static final Parcelable.Creator CREATOR = new uin6g3d5rqgcbs();
        private final Object ttmhx7;

        Token(Object obj) {
            this.ttmhx7 = obj;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            if (Build.VERSION.SDK_INT >= 21) {
                parcel.writeParcelable((Parcelable) this.ttmhx7, i);
            } else {
                parcel.writeStrongBinder((IBinder) this.ttmhx7);
            }
        }
    }
}
