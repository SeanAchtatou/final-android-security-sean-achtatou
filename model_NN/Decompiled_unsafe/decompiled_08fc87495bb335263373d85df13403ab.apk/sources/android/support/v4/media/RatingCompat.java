package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

public final class RatingCompat implements Parcelable {
    public static final Parcelable.Creator CREATOR = new fxug2rdnfo();
    private final float ozpoxuz523b2;
    private final int ttmhx7;

    private RatingCompat(int i, float f) {
        this.ttmhx7 = i;
        this.ozpoxuz523b2 = f;
    }

    /* synthetic */ RatingCompat(int i, float f, fxug2rdnfo fxug2rdnfo) {
        this(i, f);
    }

    public int describeContents() {
        return this.ttmhx7;
    }

    public String toString() {
        return "Rating:style=" + this.ttmhx7 + " rating=" + (this.ozpoxuz523b2 < 0.0f ? "unrated" : String.valueOf(this.ozpoxuz523b2));
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.ttmhx7);
        parcel.writeFloat(this.ozpoxuz523b2);
    }
}
