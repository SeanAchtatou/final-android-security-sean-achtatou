package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable;

final class fxug2rdnfo implements Parcelable.Creator {
    fxug2rdnfo() {
    }

    /* renamed from: ttmhx7 */
    public PlaybackStateCompat createFromParcel(Parcel parcel) {
        return new PlaybackStateCompat(parcel, null);
    }

    /* renamed from: ttmhx7 */
    public PlaybackStateCompat[] newArray(int i) {
        return new PlaybackStateCompat[i];
    }
}
