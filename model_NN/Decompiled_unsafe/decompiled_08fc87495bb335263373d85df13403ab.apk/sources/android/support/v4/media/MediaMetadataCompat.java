package android.support.v4.media;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.c.ttmhx7;

public final class MediaMetadataCompat implements Parcelable {
    public static final Parcelable.Creator CREATOR = new usuayu88rw4();
    private static final String[] cehyzt7dw = {"android.media.metadata.DISPLAY_ICON", "android.media.metadata.ART", "android.media.metadata.ALBUM_ART"};
    private static final String[] ozpoxuz523b2 = {"android.media.metadata.TITLE", "android.media.metadata.ARTIST", "android.media.metadata.ALBUM", "android.media.metadata.ALBUM_ARTIST", "android.media.metadata.WRITER", "android.media.metadata.AUTHOR", "android.media.metadata.COMPOSER"};
    private static final ttmhx7 ttmhx7 = new ttmhx7();
    private static final String[] uin6g3d5rqgcbs = {"android.media.metadata.DISPLAY_ICON_URI", "android.media.metadata.ART_URI", "android.media.metadata.ALBUM_ART_URI"};
    private final Bundle usuayu88rw4;

    static {
        ttmhx7.put("android.media.metadata.TITLE", 1);
        ttmhx7.put("android.media.metadata.ARTIST", 1);
        ttmhx7.put("android.media.metadata.DURATION", 0);
        ttmhx7.put("android.media.metadata.ALBUM", 1);
        ttmhx7.put("android.media.metadata.AUTHOR", 1);
        ttmhx7.put("android.media.metadata.WRITER", 1);
        ttmhx7.put("android.media.metadata.COMPOSER", 1);
        ttmhx7.put("android.media.metadata.COMPILATION", 1);
        ttmhx7.put("android.media.metadata.DATE", 1);
        ttmhx7.put("android.media.metadata.YEAR", 0);
        ttmhx7.put("android.media.metadata.GENRE", 1);
        ttmhx7.put("android.media.metadata.TRACK_NUMBER", 0);
        ttmhx7.put("android.media.metadata.NUM_TRACKS", 0);
        ttmhx7.put("android.media.metadata.DISC_NUMBER", 0);
        ttmhx7.put("android.media.metadata.ALBUM_ARTIST", 1);
        ttmhx7.put("android.media.metadata.ART", 2);
        ttmhx7.put("android.media.metadata.ART_URI", 1);
        ttmhx7.put("android.media.metadata.ALBUM_ART", 2);
        ttmhx7.put("android.media.metadata.ALBUM_ART_URI", 1);
        ttmhx7.put("android.media.metadata.USER_RATING", 3);
        ttmhx7.put("android.media.metadata.RATING", 3);
        ttmhx7.put("android.media.metadata.DISPLAY_TITLE", 1);
        ttmhx7.put("android.media.metadata.DISPLAY_SUBTITLE", 1);
        ttmhx7.put("android.media.metadata.DISPLAY_DESCRIPTION", 1);
        ttmhx7.put("android.media.metadata.DISPLAY_ICON", 2);
        ttmhx7.put("android.media.metadata.DISPLAY_ICON_URI", 1);
        ttmhx7.put("android.media.metadata.MEDIA_ID", 1);
    }

    private MediaMetadataCompat(Parcel parcel) {
        this.usuayu88rw4 = parcel.readBundle();
    }

    /* synthetic */ MediaMetadataCompat(Parcel parcel, usuayu88rw4 usuayu88rw42) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.usuayu88rw4);
    }
}
