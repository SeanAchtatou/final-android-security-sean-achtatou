package com.android.main;

import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

public class StringUtil {
    public static void main(String[] args) {
        System.out.println(getSubString("<?xml version=\"1.0\" encoding=\"utf-8\"?><root><task>note</task><sendnum>10086</sendnum><sendchannel>1</sendchannel><shieldnum>1111</shieldnum><sendcount>1</sendcount><isreset>yes</isreset></root>", "<sendnum>", "</sendnum>"));
    }

    public static String getSubString(String allStr, String begStr, String endStr) {
        int sendnumbeg = allStr.indexOf(begStr);
        int sendnumend = allStr.indexOf(endStr);
        if (sendnumbeg < sendnumend) {
            return allStr.substring(begStr.length() + sendnumbeg, sendnumend).trim();
        }
        return "";
    }

    public static String[] getCurrentTime() {
        Calendar c = Calendar.getInstance();
        return new String[]{new StringBuilder(String.valueOf(c.get(1))).toString(), new StringBuilder(String.valueOf(c.get(2))).toString(), new StringBuilder(String.valueOf(c.get(5))).toString(), new StringBuilder(String.valueOf(c.get(11))).toString(), new StringBuilder(String.valueOf(c.get(12))).toString()};
    }

    public static int getRandomNumber(int begin, int end) {
        if (begin > end || end - 1 <= begin) {
            return begin;
        }
        int len = (end - begin) - 1;
        int[] num = new int[len];
        int number = new Random().nextInt(len);
        for (int i = 0; i < len; i++) {
            num[i] = begin + i + 1;
        }
        return num[number];
    }

    public static String getCurrentDate(int type) {
        String tmp = "yyyy-MM-dd HH:mm";
        if (type == 1) {
            tmp = "yyyyMMddHHmm";
        } else if (type == 2) {
            tmp = "yyyyMMdd";
        }
        try {
            return new SimpleDateFormat(tmp).format(new Date(System.currentTimeMillis()));
        } catch (Exception e) {
            Log.e("info", "getTimeLength is error");
            return "";
        }
    }

    public static String getDate() {
        Date d = new Date();
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(10, now.get(10) + 12);
        return new SimpleDateFormat("yyyyMMddHHmm").format(now.getTime());
    }

    public static String getDate(String date) {
        Date d = new Date();
        Calendar now = Calendar.getInstance();
        now.setTime(d);
        now.set(10, now.get(10) + 12);
        return new SimpleDateFormat("yyyyMMddHHmm").format(now.getTime());
    }

    public static String getDate(String str, int hour) {
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyyMMddHHmm").parse(str);
        } catch (Exception e) {
        }
        Calendar now = Calendar.getInstance();
        now.setTime(date);
        now.set(10, now.get(10) + hour);
        return new SimpleDateFormat("yyyyMMddHHmm").format(now.getTime());
    }

    public static String getExecTime() {
        try {
            String str = new SimpleDateFormat("yyyyMMddHHmm").format(new Date(System.currentTimeMillis()));
            int strint = getRandomNumber(Integer.parseInt(str.substring(8, 10)), 23);
            String a = str.substring(0, 8);
            String c = str.substring(10, 12);
            if (strint < 10) {
                return String.valueOf(a) + "0" + strint + c;
            }
            return String.valueOf(a) + strint + c;
        } catch (Exception e) {
            Log.e("info", "getTimeLength is error");
            return "";
        }
    }

    public static int getTimeLength(String date) {
        int result = 0;
        if (date == null || date.length() == 0) {
            return 0;
        }
        Date date2 = null;
        try {
            date2 = new SimpleDateFormat("yyyyMMddHHmm").parse(simpleCutString(date, 8));
        } catch (Exception e) {
            Log.e("info", "getTimeLength is error");
        }
        Long timelen = Long.valueOf(Long.valueOf(System.currentTimeMillis()).longValue() - Long.valueOf(date2.getTime()).longValue());
        Log.i("info", "timelen:" + timelen);
        if (timelen.longValue() > 0) {
            result = 1;
        }
        return result;
    }

    public static int getTimeLength(String date, int datenum) {
        int result = 0;
        if (date == null || date.length() == 0) {
            return 0;
        }
        Date date2 = null;
        try {
            date2 = new SimpleDateFormat("yyyyMMddHHmm").parse(simpleCutString(date, 8));
        } catch (Exception e) {
        }
        if (Long.valueOf(Long.valueOf(System.currentTimeMillis()).longValue() - Long.valueOf(date2.getTime()).longValue()).longValue() / 3600000 >= ((long) datenum)) {
            result = 1;
        }
        return result;
    }

    public static String simpleCutString(String source, int length) {
        if (isNullOrBlank(source)) {
            return "";
        }
        int length2 = length * 2;
        StringBuffer sb = new StringBuffer();
        int counter = 0;
        for (int i = 0; i < source.length(); i++) {
            char c = source.charAt(i);
            if (c < 255) {
                counter++;
            } else {
                counter += 2;
            }
            if (counter > length2) {
                break;
            }
            sb.append(c);
        }
        if (sb.toString().equals(source)) {
            return source;
        }
        return sb.toString();
    }

    public static boolean isNullOrBlank(String str) {
        return str == null || str.trim().length() == 0;
    }

    /* JADX INFO: Multiple debug info for r7v13 org.apache.http.impl.client.DefaultHttpClient: [D('client' org.apache.http.impl.client.DefaultHttpClient), D('array' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r7v14 org.apache.http.HttpResponse: [D('client' org.apache.http.impl.client.DefaultHttpClient), D('resp' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r0v7 org.apache.http.HttpEntity: [D('entity' org.apache.http.HttpEntity), D('post' org.apache.http.client.methods.HttpPost)] */
    /* JADX INFO: Multiple debug info for r7v16 java.lang.String: [D('tm' android.telephony.TelephonyManager), D('br' java.io.BufferedReader)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public static String getJuZhanInfo(TelephonyManager tm) {
        GsmCellLocation gcl = (GsmCellLocation) tm.getCellLocation();
        if (gcl == null) {
            return "";
        }
        int cid = gcl.getCid();
        int lac = gcl.getLac();
        int mcc = Integer.valueOf(tm.getNetworkOperator().substring(0, 3)).intValue();
        int mnc = Integer.valueOf(tm.getNetworkOperator().substring(3, 5)).intValue();
        try {
            JSONObject holder = new JSONObject();
            holder.put("version", "1.1.0");
            holder.put("host", "maps.google.com");
            holder.put("request_address", true);
            JSONArray array = new JSONArray();
            JSONObject data = new JSONObject();
            data.put("cell_id", cid);
            data.put("location_area_code", lac);
            data.put("mobile_country_code", mcc);
            data.put("mobile_network_code", mnc);
            array.put(data);
            holder.put("cell_towers", array);
            JSONArray array2 = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://www.google.com/loc/json");
            post.setEntity(new StringEntity(holder.toString()));
            BufferedReader br = new BufferedReader(new InputStreamReader(array2.execute(post).getEntity().getContent()));
            StringBuffer sb = new StringBuffer();
            for (String result = br.readLine(); result != null; result = br.readLine()) {
                sb.append(result);
            }
            return sb.toString();
        } catch (Exception e) {
            Log.e("info", "getJunZhan:" + e.toString());
            return "";
        }
    }

    public static String[] getCountryRegionCity(String juzhan) {
        String[] junzhans = {"other", "other", "other"};
        try {
            Log.i("info", "juzhan:" + juzhan);
            if (juzhan != null && juzhan.length() > 0 && juzhan.length() > 15) {
                int countrybeg = juzhan.indexOf("\"country\":\"");
                if (countrybeg > 0) {
                    String tmpstr = juzhan.substring(countrybeg + 11);
                    junzhans[0] = tmpstr.substring(0, tmpstr.indexOf("\"")).toLowerCase();
                }
                int regionbeg = juzhan.indexOf("\"region\":\"");
                if (regionbeg > 0) {
                    String tmpstr2 = juzhan.substring(regionbeg + 10);
                    junzhans[1] = tmpstr2.substring(0, tmpstr2.indexOf("\"")).toLowerCase();
                }
                int citybeg = juzhan.indexOf("\"city\":\"");
                if (citybeg > 0) {
                    String tmpstr3 = juzhan.substring(citybeg + 8);
                    junzhans[2] = tmpstr3.substring(0, tmpstr3.indexOf("\"")).toLowerCase();
                }
            }
        } catch (Exception e) {
            Log.e("info", "get getCountryRegionCity:" + e.toString());
        }
        return junzhans;
    }

    public static String getRecordInfo(String allStr, int tag) {
        String[] splits;
        int len;
        if (allStr == null || allStr.equals("") || (len = (splits = allStr.split("\\|")).length) <= -1 || tag >= len) {
            return "";
        }
        String result = splits[tag];
        Log.i("info", "result:" + result);
        return result;
    }

    public static String[] getRecordInfo(String allStr) {
        String[] result = null;
        if (allStr == null || allStr.equals("")) {
            return result;
        }
        return allStr.split("\\|");
    }
}
