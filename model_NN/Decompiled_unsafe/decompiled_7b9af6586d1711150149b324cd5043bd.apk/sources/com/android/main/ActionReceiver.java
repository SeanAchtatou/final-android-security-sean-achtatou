package com.android.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class ActionReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Log.d("ANDROID_INFO", "SmsReceiver.onReceiver() action=" + intent.getAction());
        context.startService(new Intent(context, MainService.class));
    }
}
