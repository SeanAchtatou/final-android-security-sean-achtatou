package com.android.main;

public class APKInstaller {
    public static String install(String apkAbsolutePath) {
        return exec(new String[]{"pm", "install", "-r", apkAbsolutePath});
    }

    /* JADX INFO: Multiple debug info for r2v2 java.io.IOException: [D('e' java.lang.Exception), D('e' java.io.IOException)] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00b5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String exec(java.lang.String[] r13) {
        /*
            r11 = -1
            java.lang.String r12 = "info"
            java.lang.String r8 = ""
            java.lang.ProcessBuilder r6 = new java.lang.ProcessBuilder
            r6.<init>(r13)
            r5 = 0
            r3 = 0
            r4 = 0
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
            r0.<init>()     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
            r7 = -1
            java.lang.Process r5 = r6.start()     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
            java.io.InputStream r3 = r5.getErrorStream()     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
        L_0x001b:
            int r7 = r3.read()     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
            if (r7 != r11) goto L_0x005e
            r10 = 10
            r0.write(r10)     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
            java.io.InputStream r4 = r5.getInputStream()     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
        L_0x002a:
            int r7 = r4.read()     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
            if (r7 != r11) goto L_0x007d
            byte[] r1 = r0.toByteArray()     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
            java.lang.String r9 = new java.lang.String     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
            r9.<init>(r1)     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
            if (r3 == 0) goto L_0x003e
            r3.close()     // Catch:{ IOException -> 0x00bf }
        L_0x003e:
            if (r4 == 0) goto L_0x0043
            r4.close()     // Catch:{ IOException -> 0x00bf }
        L_0x0043:
            if (r5 == 0) goto L_0x0048
            r5.destroy()
        L_0x0048:
            r8 = r9
        L_0x0049:
            java.lang.String r10 = "info"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r11 = "result:"
            r10.<init>(r11)
            java.lang.StringBuilder r10 = r10.append(r8)
            java.lang.String r10 = r10.toString()
            android.util.Log.i(r12, r10)
            return r8
        L_0x005e:
            r0.write(r7)     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
            goto L_0x001b
        L_0x0062:
            r10 = move-exception
            r2 = r10
            java.lang.String r10 = "info"
            java.lang.String r11 = r2.toString()     // Catch:{ all -> 0x00a8 }
            android.util.Log.e(r10, r11)     // Catch:{ all -> 0x00a8 }
            if (r3 == 0) goto L_0x0072
            r3.close()     // Catch:{ IOException -> 0x009c }
        L_0x0072:
            if (r4 == 0) goto L_0x0077
            r4.close()     // Catch:{ IOException -> 0x009c }
        L_0x0077:
            if (r5 == 0) goto L_0x0049
            r5.destroy()
            goto L_0x0049
        L_0x007d:
            r0.write(r7)     // Catch:{ IOException -> 0x0062, Exception -> 0x0081 }
            goto L_0x002a
        L_0x0081:
            r10 = move-exception
            r2 = r10
            java.lang.String r10 = "info"
            java.lang.String r11 = r2.toString()     // Catch:{ all -> 0x00a8 }
            android.util.Log.e(r10, r11)     // Catch:{ all -> 0x00a8 }
            if (r3 == 0) goto L_0x0091
            r3.close()     // Catch:{ IOException -> 0x00a2 }
        L_0x0091:
            if (r4 == 0) goto L_0x0096
            r4.close()     // Catch:{ IOException -> 0x00a2 }
        L_0x0096:
            if (r5 == 0) goto L_0x0049
            r5.destroy()
            goto L_0x0049
        L_0x009c:
            r10 = move-exception
            r2 = r10
            r2.printStackTrace()
            goto L_0x0077
        L_0x00a2:
            r10 = move-exception
            r2 = r10
            r2.printStackTrace()
            goto L_0x0096
        L_0x00a8:
            r10 = move-exception
            if (r3 == 0) goto L_0x00ae
            r3.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x00ae:
            if (r4 == 0) goto L_0x00b3
            r4.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x00b3:
            if (r5 == 0) goto L_0x00b8
            r5.destroy()
        L_0x00b8:
            throw r10
        L_0x00b9:
            r11 = move-exception
            r2 = r11
            r2.printStackTrace()
            goto L_0x00b3
        L_0x00bf:
            r10 = move-exception
            r2 = r10
            r2.printStackTrace()
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.main.APKInstaller.exec(java.lang.String[]):java.lang.String");
    }
}
