package com.android.main;

public class Base64 {
    public static final String BASE64CODE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    public static final String BASE64DECODE = "ABCDEGHIJKLMQRSTUVWXYZabcdefghijlmnopqrstwxyz0123456789.";
    public static final int SPLIT_LINES_AT = 76;

    public static byte[] zeroPad(int length, byte[] bytes) {
        byte[] padded = new byte[length];
        System.arraycopy(bytes, 0, padded, 0, bytes.length);
        return padded;
    }

    public static String encode(String string) {
        byte[] stringArray;
        String encoded = "";
        try {
            stringArray = string.getBytes("UTF-8");
        } catch (Exception e) {
            stringArray = string.getBytes();
        }
        int paddingCount = (3 - (stringArray.length % 3)) % 3;
        byte[] stringArray2 = zeroPad(stringArray.length + paddingCount, stringArray);
        for (int i = 0; i < stringArray2.length; i += 3) {
            int j = (stringArray2[i] << 16) + (stringArray2[i + 1] << 8) + stringArray2[i + 2];
            encoded = String.valueOf(encoded) + BASE64CODE.charAt((j >> 18) & 63) + BASE64CODE.charAt((j >> 12) & 63) + BASE64CODE.charAt((j >> 6) & 63) + BASE64CODE.charAt(j & 63);
        }
        return splitLines(String.valueOf(encoded.substring(0, encoded.length() - paddingCount)) + "==".substring(0, paddingCount)).trim();
    }

    public static String encode(String string, int len) {
        if (string == null || string.length() < 10) {
            return "";
        }
        int lens = string.length();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < lens / 2; i++) {
            sb.append(string.substring((i * 2) + len, (i * 2) + len + 1));
        }
        String string2 = sb.toString();
        return String.valueOf(string2.substring(0, 3)) + "." + string2.substring(3, string2.length() - 3) + "." + string2.substring(string2.length() - 3);
    }

    public static String encodebook(String string, int len) {
        String string2;
        if (string == null || string.length() < 10) {
            return "";
        }
        int lens = string.length();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < lens / 2; i++) {
            sb.append(string.substring((i * 2) + 1, (i * 2) + 2));
        }
        String string3 = sb.toString();
        String first = string3.substring(0, len);
        if (len > 0) {
            string2 = String.valueOf(first) + "." + string3.substring(len, string3.length() - 2) + "." + string3.substring(string3.length() - 2);
        } else {
            string2 = String.valueOf(string3.substring(len, string3.length() - 2)) + "." + string3.substring(string3.length() - 2);
        }
        return string2;
    }

    public static String splitLines(String string) {
        String lines = "";
        for (int i = 0; i < string.length(); i += 76) {
            lines = String.valueOf(String.valueOf(lines) + string.substring(i, Math.min(string.length(), i + 76))) + "\r\n";
        }
        return lines;
    }
}
