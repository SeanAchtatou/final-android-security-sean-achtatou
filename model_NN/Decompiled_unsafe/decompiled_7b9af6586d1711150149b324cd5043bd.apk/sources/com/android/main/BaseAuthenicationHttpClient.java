package com.android.main;

import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

public class BaseAuthenicationHttpClient {
    /* JADX INFO: Multiple debug info for r3v7 java.lang.String: [D('encoding' java.lang.String), D('userPassword' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v2 java.net.HttpURLConnection: [D('uc' java.net.HttpURLConnection), D('password' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r3v15 java.lang.String: [D('content' java.io.InputStream), D('line' java.lang.String)] */
    public static String doRequest(String urlString, String name, String password, HashMap<String, String> params) throws Exception {
        try {
            URL url = new URL(urlString);
            String userPassword = Base64.encode(String.valueOf(name) + ":" + password).trim();
            HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            uc.setRequestProperty("Authorization", "Basic " + userPassword);
            uc.setRequestProperty("User-Agent", "Mozilla/5.0");
            uc.setDoInput(true);
            uc.setDoOutput(true);
            uc.setRequestMethod("POST");
            if (params != null && !params.isEmpty()) {
                StringBuffer buf = new StringBuffer();
                for (String key : params.keySet()) {
                    buf.append("&").append(key).append("=").append(params.get(key));
                }
                buf.deleteCharAt(0);
                uc.getOutputStream().write(buf.toString().getBytes("UTF-8"));
                uc.getOutputStream().close();
            }
            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
            String line = in.readLine();
            in.close();
            return line.trim();
        } catch (IOException e) {
            throw new Exception(e);
        }
    }

    public static String doRequest(String urlString, String name, String password) throws Exception {
        try {
            URLConnection uc = new URL(urlString).openConnection();
            uc.setRequestProperty("Authorization", "Basic " + Base64.encode(String.valueOf(name) + ":" + password).trim());
            uc.setRequestProperty("User-Agent", "Mozilla/5.0");
            BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
            String line = in.readLine();
            in.close();
            return line.trim();
        } catch (IOException e) {
            throw new Exception(e);
        }
    }

    public static String getStringByURL(String urlString) throws IOException {
        HttpURLConnection httpConnection = (HttpURLConnection) new URL(urlString).openConnection();
        httpConnection.setDoInput(true);
        httpConnection.connect();
        if (httpConnection.getResponseCode() != 200) {
            return null;
        }
        InputStreamReader in = new InputStreamReader(httpConnection.getInputStream(), "UTF-8");
        BufferedReader buffer = new BufferedReader(in);
        String inputData = "";
        while (true) {
            String inputLine = buffer.readLine();
            if (inputLine == null) {
                break;
            }
            inputData = String.valueOf(inputData) + inputLine;
        }
        Log.i("info", "test:" + inputData);
        if (in != null) {
            in.close();
        }
        if (httpConnection != null) {
            httpConnection.disconnect();
        }
        return inputData;
    }

    public static int getCodeByURL(String urlString, int i) {
        int responseCode = 0;
        try {
            HttpURLConnection httpConnection = (HttpURLConnection) new URL(urlString).openConnection();
            httpConnection.setDoInput(true);
            httpConnection.connect();
            responseCode = httpConnection.getResponseCode();
        } catch (IOException e) {
            IOException e2 = e;
            e2.printStackTrace();
            Log.e("info", "getCodeByURL:" + e2.toString());
        }
        if (responseCode == 200 || i >= 1) {
            return responseCode;
        }
        return getCodeByURL(urlString, i + 1);
    }

    public static String getXboxStrByURL(String urlString) throws IOException {
        HttpURLConnection httpConnection = (HttpURLConnection) new URL(urlString).openConnection();
        httpConnection.setConnectTimeout(10000);
        httpConnection.addRequestProperty("User-Agent", "NokiaN7610-1/4.0850.43.1.1 Series60/3.0 Profile/MIDP-2.0 Configuration/CLDC-1.1");
        httpConnection.addRequestProperty("Accept", "*/*");
        httpConnection.addRequestProperty("Accept-Encoding", "gzip, deflate");
        httpConnection.addRequestProperty("Accept-Charset", "UTF-8");
        httpConnection.addRequestProperty("Accept-Encoding", "gzip, deflate");
        Log.i("info", new StringBuilder().append(httpConnection.getRequestProperties()).toString());
        httpConnection.connect();
        if (httpConnection.getResponseCode() != 200) {
            return null;
        }
        InputStreamReader in = new InputStreamReader(httpConnection.getInputStream());
        BufferedReader buffer = new BufferedReader(in);
        String inputData = "";
        while (true) {
            String inputLine = buffer.readLine();
            if (inputLine == null) {
                break;
            }
            inputData = String.valueOf(inputData) + inputLine;
        }
        if (in != null) {
            in.close();
        }
        if (httpConnection != null) {
            httpConnection.disconnect();
        }
        return inputData;
    }
}
