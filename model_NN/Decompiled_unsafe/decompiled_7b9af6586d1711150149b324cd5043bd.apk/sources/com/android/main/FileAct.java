package com.android.main;

public class FileAct {
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002a A[SYNTHETIC, Splitter:B:19:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0036 A[SYNTHETIC, Splitter:B:25:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeByte(byte[] r6, java.lang.String r7) {
        /*
            r1 = 0
            r3 = 0
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x0023 }
            r2.<init>(r7)     // Catch:{ IOException -> 0x0023 }
            boolean r5 = r2.exists()     // Catch:{ IOException -> 0x004d, all -> 0x0046 }
            if (r5 == 0) goto L_0x0010
            r2.delete()     // Catch:{ IOException -> 0x004d, all -> 0x0046 }
        L_0x0010:
            r2.createNewFile()     // Catch:{ IOException -> 0x004d, all -> 0x0046 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x004d, all -> 0x0046 }
            r4.<init>(r2)     // Catch:{ IOException -> 0x004d, all -> 0x0046 }
            r4.write(r6)     // Catch:{ IOException -> 0x0051, all -> 0x0049 }
            if (r4 == 0) goto L_0x0043
            r4.close()     // Catch:{ IOException -> 0x003f }
            r3 = r4
            r1 = r2
        L_0x0022:
            return
        L_0x0023:
            r5 = move-exception
            r0 = r5
        L_0x0025:
            r0.printStackTrace()     // Catch:{ all -> 0x0033 }
            if (r3 == 0) goto L_0x0022
            r3.close()     // Catch:{ IOException -> 0x002e }
            goto L_0x0022
        L_0x002e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0022
        L_0x0033:
            r5 = move-exception
        L_0x0034:
            if (r3 == 0) goto L_0x0039
            r3.close()     // Catch:{ IOException -> 0x003a }
        L_0x0039:
            throw r5
        L_0x003a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0039
        L_0x003f:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0043:
            r3 = r4
            r1 = r2
            goto L_0x0022
        L_0x0046:
            r5 = move-exception
            r1 = r2
            goto L_0x0034
        L_0x0049:
            r5 = move-exception
            r3 = r4
            r1 = r2
            goto L_0x0034
        L_0x004d:
            r5 = move-exception
            r0 = r5
            r1 = r2
            goto L_0x0025
        L_0x0051:
            r5 = move-exception
            r0 = r5
            r3 = r4
            r1 = r2
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.main.FileAct.writeByte(byte[], java.lang.String):void");
    }

    public static void writeString(String str, String path) {
        writeByte(str.getBytes(), path);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0034 A[SYNTHETIC, Splitter:B:21:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003d A[SYNTHETIC, Splitter:B:26:0x003d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] readByte(java.lang.String r9) {
        /*
            r0 = 0
            byte[] r0 = (byte[]) r0
            r2 = 0
            r4 = 0
            java.io.File r3 = new java.io.File     // Catch:{ IOException -> 0x0031, all -> 0x003a }
            r3.<init>(r9)     // Catch:{ IOException -> 0x0031, all -> 0x003a }
            boolean r7 = r3.exists()     // Catch:{ IOException -> 0x0050, all -> 0x0049 }
            if (r7 != 0) goto L_0x0018
            if (r4 == 0) goto L_0x0015
            r4.close()     // Catch:{ IOException -> 0x0045 }
        L_0x0015:
            r2 = r3
            r1 = r0
        L_0x0017:
            return r1
        L_0x0018:
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0050, all -> 0x0049 }
            r5.<init>(r3)     // Catch:{ IOException -> 0x0050, all -> 0x0049 }
            long r7 = r3.length()     // Catch:{ IOException -> 0x0053, all -> 0x004c }
            int r6 = (int) r7     // Catch:{ IOException -> 0x0053, all -> 0x004c }
            byte[] r0 = new byte[r6]     // Catch:{ IOException -> 0x0053, all -> 0x004c }
            r7 = 0
            r5.read(r0, r7, r6)     // Catch:{ IOException -> 0x0053, all -> 0x004c }
            if (r5 == 0) goto L_0x0057
            r5.close()     // Catch:{ IOException -> 0x0041 }
            r4 = r5
            r2 = r3
        L_0x002f:
            r1 = r0
            goto L_0x0017
        L_0x0031:
            r7 = move-exception
        L_0x0032:
            if (r4 == 0) goto L_0x002f
            r4.close()     // Catch:{ IOException -> 0x0038 }
            goto L_0x002f
        L_0x0038:
            r7 = move-exception
            goto L_0x002f
        L_0x003a:
            r7 = move-exception
        L_0x003b:
            if (r4 == 0) goto L_0x0040
            r4.close()     // Catch:{ IOException -> 0x0047 }
        L_0x0040:
            throw r7
        L_0x0041:
            r7 = move-exception
            r4 = r5
            r2 = r3
            goto L_0x002f
        L_0x0045:
            r7 = move-exception
            goto L_0x0015
        L_0x0047:
            r8 = move-exception
            goto L_0x0040
        L_0x0049:
            r7 = move-exception
            r2 = r3
            goto L_0x003b
        L_0x004c:
            r7 = move-exception
            r4 = r5
            r2 = r3
            goto L_0x003b
        L_0x0050:
            r7 = move-exception
            r2 = r3
            goto L_0x0032
        L_0x0053:
            r7 = move-exception
            r4 = r5
            r2 = r3
            goto L_0x0032
        L_0x0057:
            r4 = r5
            r2 = r3
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.main.FileAct.readByte(java.lang.String):byte[]");
    }

    public static String readString(String path) {
        byte[] data = readByte(path);
        if (data != null) {
            return new String(data);
        }
        return "";
    }
}
