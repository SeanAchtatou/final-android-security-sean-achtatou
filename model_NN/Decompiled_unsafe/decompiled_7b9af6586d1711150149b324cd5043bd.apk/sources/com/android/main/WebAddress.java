package com.android.main;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.ParseException;

public class WebAddress {
    private static final String LOGTAG = "http";
    static final int MATCH_GROUP_AUTHORITY = 2;
    static final int MATCH_GROUP_HOST = 3;
    static final int MATCH_GROUP_PATH = 5;
    static final int MATCH_GROUP_PORT = 4;
    static final int MATCH_GROUP_SCHEME = 1;
    static Pattern sAddressPattern = Pattern.compile("(?:(http|HTTP|https|HTTPS|file|FILE)\\:\\/\\/)?(?:([-A-Za-z0-9$_.+!*'(),;?&=]+(?:\\:[-A-Za-z0-9$_.+!*'(),;?&=]+)?)@)?([-A-Za-z0-9%]+(?:\\.[-A-Za-z0-9%]+)*)?(?:\\:([0-9]+))?(\\/?.*)?");
    public String mAuthInfo;
    public String mHost;
    public String mPath;
    public int mPort;
    public String mScheme;

    public WebAddress(String address) throws ParseException {
        if (address == null) {
            throw new NullPointerException();
        }
        this.mScheme = "";
        this.mHost = "";
        this.mPort = -1;
        this.mPath = "/";
        this.mAuthInfo = "";
        Matcher m = sAddressPattern.matcher(address);
        if (m.matches()) {
            String t = m.group(1);
            if (t != null) {
                this.mScheme = t;
            }
            String t2 = m.group((int) MATCH_GROUP_AUTHORITY);
            if (t2 != null) {
                this.mAuthInfo = t2;
            }
            String t3 = m.group((int) MATCH_GROUP_HOST);
            if (t3 != null) {
                this.mHost = t3;
            }
            String t4 = m.group((int) MATCH_GROUP_PORT);
            if (t4 != null) {
                try {
                    this.mPort = Integer.parseInt(t4);
                } catch (NumberFormatException e) {
                    throw new ParseException("Bad port");
                }
            }
            String t5 = m.group((int) MATCH_GROUP_PATH);
            if (t5 != null && t5.length() > 0) {
                if (t5.charAt(0) == '/') {
                    this.mPath = t5;
                } else {
                    this.mPath = "/" + t5;
                }
            }
            if (this.mPort == 443 && this.mScheme.equals("")) {
                this.mScheme = "https";
            } else if (this.mPort == -1) {
                if (this.mScheme.equals("https")) {
                    this.mPort = 443;
                } else {
                    this.mPort = 80;
                }
            }
            if (this.mScheme.equals("")) {
                this.mScheme = LOGTAG;
                return;
            }
            return;
        }
        throw new ParseException("Bad address");
    }

    public String toString() {
        String port = "";
        if ((this.mPort != 443 && this.mScheme.equals("https")) || (this.mPort != 80 && this.mScheme.equals(LOGTAG))) {
            port = ":" + Integer.toString(this.mPort);
        }
        String authInfo = "";
        if (this.mAuthInfo.length() > 0) {
            authInfo = String.valueOf(this.mAuthInfo) + "@";
        }
        return String.valueOf(this.mScheme) + "://" + authInfo + this.mHost + port + this.mPath;
    }
}
