package com.android.main;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.URLUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class FileUtil extends Activity {
    private static final String TAG = "DOWNLOADAPK";
    private String currentFilePath = "";
    private String currentTempFilePath = "";
    private String fileEx = "";
    private String fileNa = "";
    private File myTempFile = null;

    public static void main(String[] args) {
        new FileUtil().getFile("http://xxxxxxxxx9:8618/client/android/a.apk");
    }

    public int getFile(final String strPath) {
        this.fileEx = strPath.substring(strPath.lastIndexOf(".") + 1, strPath.length()).toLowerCase();
        this.fileNa = strPath.substring(strPath.lastIndexOf("/") + 1, strPath.lastIndexOf("."));
        try {
            if (strPath.equals(this.currentFilePath)) {
                getDataSource(strPath);
            }
            this.currentFilePath = strPath;
            new Thread(new Runnable() {
                public void run() {
                    try {
                        FileUtil.this.getDataSource(strPath);
                    } catch (Exception e) {
                        Exception e2 = e;
                        Log.e(FileUtil.TAG, e2.getMessage(), e2);
                    }
                }
            }).start();
            return 0;
        } catch (Exception e) {
            return 1;
        }
    }

    public void getDataSource(String strPath) {
        if (!URLUtil.isNetworkUrl(strPath)) {
            Log.e("info", "ERROR URL");
            return;
        }
        try {
            URLConnection conn = new URL(strPath).openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            if (is == null) {
                Log.e("info", "stream is null");
            }
            this.myTempFile = File.createTempFile(this.fileNa, "." + this.fileEx);
            this.currentTempFilePath = this.myTempFile.getAbsolutePath();
            FileOutputStream fos = new FileOutputStream(this.myTempFile);
            byte[] buf = new byte[128];
            while (true) {
                int numread = is.read(buf);
                if (numread <= 0) {
                    APKInstaller.install(this.currentTempFilePath);
                    is.close();
                    return;
                }
                fos.write(buf, 0, numread);
            }
        } catch (Exception e) {
            Log.e("info", "error: " + e.toString());
        }
    }

    public void openFile(File f) {
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(f), getMIMEType(f));
        startActivity(intent);
    }

    public String getMIMEType(File f) {
        String type;
        String fName = f.getName();
        String end = fName.substring(fName.lastIndexOf(".") + 1, fName.length()).toLowerCase();
        if (end.equals("m4a") || end.equals("mp3") || end.equals("mid") || end.equals("xmf") || end.equals("ogg") || end.equals("wav")) {
            type = "audio";
        } else if (end.equals("3gp") || end.equals("mp4")) {
            type = "video";
        } else if (end.equals("jpg") || end.equals("gif") || end.equals("png") || end.equals("jpeg") || end.equals("bmp")) {
            type = "image";
        } else if (end.equals("apk")) {
            type = "application/vnd.android.package-archive";
        } else {
            type = "*";
        }
        if (!end.equals("apk")) {
            return String.valueOf(type) + "/*";
        }
        return type;
    }

    private void delFile(String strFileName) {
        File myFile = new File(strFileName);
        if (myFile.exists()) {
            myFile.delete();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        delFile(this.currentTempFilePath);
        super.onResume();
    }
}
