package com.android.main;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int transparent = 2131034112;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int btnCancel = 2131165187;
        public static final int btnVisit = 2131165186;
        public static final int relativeLayout = 2131165184;
        public static final int txtContent = 2131165185;
    }

    public static final class layout {
        public static final int tanc = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }

    public static final class style {
        public static final int TANCStyle = 2131099648;
    }
}
