package com.paojiao.help.util;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.paojiao.help.R;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class Util {
    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap.Config config;
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (drawable.getOpacity() != -1) {
            config = Bitmap.Config.ARGB_8888;
        } else {
            config = Bitmap.Config.RGB_565;
        }
        Bitmap bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, config);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static String getStringByURL(String urlString) throws IOException {
        HttpURLConnection httpConnection = (HttpURLConnection) new URL(urlString).openConnection();
        httpConnection.setDoInput(true);
        httpConnection.connect();
        if (httpConnection.getResponseCode() != 200) {
            return null;
        }
        InputStreamReader in = new InputStreamReader(httpConnection.getInputStream());
        BufferedReader buffer = new BufferedReader(in);
        String inputData = "";
        while (true) {
            String inputLine = buffer.readLine();
            if (inputLine == null) {
                break;
            }
            inputData = String.valueOf(inputData) + inputLine;
        }
        if (in != null) {
            in.close();
        }
        if (httpConnection != null) {
            httpConnection.disconnect();
        }
        return inputData;
    }

    public static void MainSetStart(Activity mActivity) {
        DataDefine.settings = mActivity.getSharedPreferences(DataDefine.PREFS_NAME, 0);
        if (DataDefine.settings.getBoolean(DataDefine.PREFS_ITME_IS_FIRST_TIME_TO_OPEN_SET, true)) {
            SharedPreferences.Editor edit = DataDefine.settings.edit();
            edit.putBoolean(DataDefine.PREFS_ITME_IS_FIRST_TIME_TO_OPEN_SET, false);
            edit.commit();
            mActivity.showDialog(101);
            return;
        }
        StartThreadDoToServer(mActivity);
    }

    public static void openSoftwareDo(Activity mActivity) {
        StartThreadDoToServer(mActivity);
    }

    public static void StartThreadDoToServer(Activity mActivity) {
        boolean needToUpdate = DataDefine.settings.getBoolean(DataDefine.PREFS_ITME_NEED_TO_UPDATE, false);
        if (DataDefine.settings.getBoolean(DataDefine.PREFS_ITME_IS_FIRST_TIME_TO_CONNECT_SERVER, true)) {
            DataDefine.urlFirstConnectToServer = String.valueOf(mActivity.getString(R.string.url_first_connect_to_server)) + "id=" + mActivity.getString(R.string.search_channel) + "&imei=" + ((TelephonyManager) mActivity.getSystemService("phone")).getDeviceId() + "&uid=" + makeuid() + "&type=3" + "&fid=android&ua=" + Build.MODEL.replace(" ", "_");
            new Thread() {
                public void run() {
                    Util.firstConnectToServer();
                }
            }.start();
        }
        if (needToUpdate) {
            mActivity.showDialog(100);
        }
        long currentTime = System.currentTimeMillis();
        if (DataDefine.settings.getLong(DataDefine.PREFS_ITEM_NEXT_UPDATE_TIME_NAME, currentTime) <= currentTime) {
            DataDefine.urlUpdateTxt = String.valueOf(mActivity.getString(R.string.url_update)) + "xml/and21_" + mActivity.getString(R.string.search_channel) + ".txt?" + getUrlEnding(mActivity);
            try {
                DataDefine.currentVersionName = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0).versionName;
                DataDefine.urlUpdateApk = mActivity.getString(R.string.url_update);
                new Thread() {
                    public void run() {
                        if (Util.correctDoWithUpdate()) {
                            long nextUpdateTime = System.currentTimeMillis() + ((long) (DataDefine.settings.getInt(DataDefine.PREFS_ITEM_UPDATE_TIME_LIMIT_NAME, 15) * DataDefine.ONE_DAY_TIME));
                            SharedPreferences.Editor edit = DataDefine.settings.edit();
                            edit.putLong(DataDefine.PREFS_ITEM_NEXT_UPDATE_TIME_NAME, nextUpdateTime);
                            edit.commit();
                        }
                    }
                }.start();
            } catch (PackageManager.NameNotFoundException e) {
            }
        }
    }

    public static String getUrlEnding(Activity mActivity) {
        if (DataDefine.deviceId == null) {
            DataDefine.deviceId = ((TelephonyManager) mActivity.getSystemService("phone")).getDeviceId();
            DataDefine.channel = mActivity.getString(R.string.search_channel);
        }
        return "fid=android&imei=" + DataDefine.deviceId + "&id=" + DataDefine.channel;
    }

    public static String getUrlEnding() {
        return "fid=android&imei=" + DataDefine.deviceId + "&id=" + DataDefine.channel;
    }

    public static boolean firstConnectToServer() {
        try {
            SharedPreferences.Editor edit = DataDefine.settings.edit();
            edit.putBoolean(DataDefine.PREFS_ITME_NEED_TO_UPDATE, false);
            edit.commit();
            if (!"OK".equals(getStringByURL(DataDefine.urlFirstConnectToServer))) {
                return false;
            }
            edit.putBoolean(DataDefine.PREFS_ITME_IS_FIRST_TIME_TO_CONNECT_SERVER, false);
            edit.commit();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void nextUpdate() {
        SharedPreferences.Editor edit = DataDefine.settings.edit();
        edit.putBoolean(DataDefine.PREFS_ITME_NEED_TO_UPDATE, false);
        edit.commit();
    }

    public static boolean correctDoWithUpdate() {
        return false;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String toUtf8String(java.lang.String r9) {
        /*
            if (r9 == 0) goto L_0x000a
            java.lang.String r7 = ""
            boolean r7 = r9.equals(r7)
            if (r7 == 0) goto L_0x000c
        L_0x000a:
            r7 = 0
        L_0x000b:
            return r7
        L_0x000c:
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            r3 = 0
        L_0x0012:
            int r7 = r9.length()     // Catch:{ Exception -> 0x005e }
            if (r3 < r7) goto L_0x001d
        L_0x0018:
            java.lang.String r7 = r6.toString()
            goto L_0x000b
        L_0x001d:
            char r1 = r9.charAt(r3)     // Catch:{ Exception -> 0x005e }
            if (r1 < 0) goto L_0x002d
            r7 = 255(0xff, float:3.57E-43)
            if (r1 > r7) goto L_0x002d
            r6.append(r1)     // Catch:{ Exception -> 0x005e }
        L_0x002a:
            int r3 = r3 + 1
            goto L_0x0012
        L_0x002d:
            java.lang.String r7 = java.lang.Character.toString(r1)     // Catch:{ Exception -> 0x005e }
            java.lang.String r8 = "utf-8"
            byte[] r0 = r7.getBytes(r8)     // Catch:{ Exception -> 0x005e }
            r4 = 0
        L_0x0038:
            int r7 = r0.length     // Catch:{ Exception -> 0x005e }
            if (r4 >= r7) goto L_0x002a
            byte r5 = r0[r4]     // Catch:{ Exception -> 0x005e }
            if (r5 >= 0) goto L_0x0041
            int r5 = r5 + 256
        L_0x0041:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005e }
            java.lang.String r8 = "%"
            r7.<init>(r8)     // Catch:{ Exception -> 0x005e }
            java.lang.String r8 = java.lang.Integer.toHexString(r5)     // Catch:{ Exception -> 0x005e }
            java.lang.String r8 = r8.toUpperCase()     // Catch:{ Exception -> 0x005e }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x005e }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x005e }
            r6.append(r7)     // Catch:{ Exception -> 0x005e }
            int r4 = r4 + 1
            goto L_0x0038
        L_0x005e:
            r7 = move-exception
            r2 = r7
            r2.printStackTrace()
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paojiao.help.util.Util.toUtf8String(java.lang.String):java.lang.String");
    }

    public static String getRandomString(String[] stringArray) {
        return stringArray[(int) (0.0d + (((double) ((stringArray.length - 1) - 0)) * Math.random()))];
    }

    /* JADX INFO: Multiple debug info for r1v2 java.lang.String: [D('chars' java.lang.String[]), D('c2' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r2v13 java.lang.String[]: [D('chars' java.lang.String[]), D('tempint' int)] */
    /* JADX INFO: Multiple debug info for r2v14 java.lang.String: [D('c3' java.lang.String), D('chars' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r5v3 java.lang.String: [D('chars' java.lang.String[]), D('c6' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v13 java.lang.String: [D('chars' java.lang.String[]), D('c9' java.lang.String)] */
    private static String makeuid() {
        Random r = new Random();
        int c1 = r.nextInt(9);
        String c2 = new String[]{"f", "g", "h", "i", "j", "k", "l", "m", "n", "o"}[r.nextInt(9)];
        String c3 = new String[]{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"}[c1];
        int c4 = r.nextInt(9);
        String[] chars = {"f", "g", "h", "i", "j", "k", "l", "m", "n", "o"};
        int c5 = 0;
        for (int i = 0; i < chars.length; i++) {
            if (c2 == chars[i]) {
                c5 = i;
            }
        }
        String c6 = new String[]{"k", "l", "m", "n", "o", "p", "q", "r", "s", "t"}[c4];
        int c7 = r.nextInt(9);
        if (c7 == 0) {
            c7 = 1;
        }
        return String.valueOf(c1) + c2 + c3 + c4 + c5 + c6 + c7 + 8 + new String[]{"r", "s", "t", "u", "v", "w", "x", "y", "z"}[c7 - 1];
    }
}
