package com.paojiao.help.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context) {
        super(context, DataDefine.DB_NAME, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists search_history (_id integer primary key autoincrement,display_name text,keyword_order integer);");
        db.execSQL("create table if not exists website (_id integer primary key autoincrement,display_name text,website text);");
        db.execSQL("insert into website(display_name,website) values ('安卓专门站-玩手机,上泡椒','http://android.paojiao.cn')");
        db.execSQL("insert into website(display_name,website) values ('泡椒池塘-Android社区','http://ct2.paojiao.cn')");
        db.execSQL("insert into website(display_name,website) values ('G3网址大全','http://g3g3.cn')");
        db.execSQL("create table if not exists application (_id integer primary key autoincrement,display_name text,full_name text,package_name text);");
        db.execSQL("create table if not exists call_contact (_id integer primary key autoincrement,contact_id text,display_name text,number text);");
        db.execSQL("create table if not exists message_contact (_id integer primary key autoincrement,contact_id text,display_name text,number text);");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists search_history");
        db.execSQL("drop table if exists call_contact");
        db.execSQL("drop table if exists message_contact");
        db.execSQL("drop table if exists application");
        db.execSQL("drop table if exists website");
    }
}
