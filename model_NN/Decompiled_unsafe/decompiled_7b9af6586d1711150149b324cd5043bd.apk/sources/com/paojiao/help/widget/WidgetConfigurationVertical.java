package com.paojiao.help.widget;

import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RemoteViews;
import com.paojiao.help.CallActivity;
import com.paojiao.help.R;
import com.paojiao.help.SMSActivity;
import com.paojiao.help.SearchActivity;
import com.paojiao.help.SoftwareActivity;
import com.paojiao.help.UsefulActivity;
import com.paojiao.help.WebSiteActivity;
import com.paojiao.help.util.DataDefine;

public class WidgetConfigurationVertical extends Activity {
    /* access modifiers changed from: private */
    public AppWidgetManager appWidgetManager;
    /* access modifiers changed from: private */
    public int mAppWidgetId;
    private ImageView mImageView;
    /* access modifiers changed from: private */
    public int widgetBackgroundId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.appwidget_configuration_vertical);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mAppWidgetId = extras.getInt("appWidgetId", 0);
        }
        this.appWidgetManager = AppWidgetManager.getInstance(this);
        DataDefine.settings = getSharedPreferences(DataDefine.PREFS_NAME, 0);
        this.widgetBackgroundId = DataDefine.settings.getInt(DataDefine.PREFS_VERTICAL_WIDGET_SKIN_ID, R.drawable.widget_vertical_background);
        this.mImageView = (ImageView) findViewById(R.id.widget_vertical_background);
        this.mImageView.setImageResource(this.widgetBackgroundId);
        ((RadioGroup) findViewById(R.id.radio_group_vertical)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb1:
                        Log.i("info", "R.id.rb1");
                        return;
                    case R.id.rb2:
                        Log.i("info", "R.id.rb2");
                        return;
                    case R.id.rb3:
                        Log.i("info", "R.id.rb3");
                        return;
                    default:
                        return;
                }
            }
        });
        ((Button) findViewById(R.id.button_vertival_save)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WidgetConfigurationVertical.this.theUpdate(WidgetConfigurationVertical.this.appWidgetManager);
                SharedPreferences.Editor editor = DataDefine.settings.edit();
                editor.putInt(DataDefine.PREFS_VERTICAL_WIDGET_SKIN_ID, WidgetConfigurationVertical.this.widgetBackgroundId);
                editor.commit();
                Intent resultValue = new Intent();
                resultValue.putExtra("appWidgetId", WidgetConfigurationVertical.this.mAppWidgetId);
                WidgetConfigurationVertical.this.setResult(-1, resultValue);
                WidgetConfigurationVertical.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public void theUpdate(AppWidgetManager appWidgetManager2) {
        RemoteViews views = new RemoteViews(getPackageName(), (int) R.layout.appwidget_provider_vertical);
        views.setImageViewResource(R.id.widget_vertical_background, this.widgetBackgroundId);
        views.setOnClickPendingIntent(R.id.widget_search, PendingIntent.getActivity(this, 0, new Intent(this, SearchActivity.class), 0));
        views.setOnClickPendingIntent(R.id.widget_website, PendingIntent.getActivity(this, 0, new Intent(this, WebSiteActivity.class), 0));
        views.setOnClickPendingIntent(R.id.widget_software, PendingIntent.getActivity(this, 0, new Intent(this, SoftwareActivity.class), 0));
        views.setOnClickPendingIntent(R.id.widget_call, PendingIntent.getActivity(this, 0, new Intent(this, CallActivity.class), 0));
        views.setOnClickPendingIntent(R.id.widget_message, PendingIntent.getActivity(this, 0, new Intent(this, SMSActivity.class), 0));
        views.setOnClickPendingIntent(R.id.widget_useful, PendingIntent.getActivity(this, 0, new Intent(this, UsefulActivity.class), 0));
        appWidgetManager2.updateAppWidget(this.mAppWidgetId, views);
    }
}
