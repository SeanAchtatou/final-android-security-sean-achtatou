package com.paojiao.help.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import com.paojiao.help.CallActivity;
import com.paojiao.help.R;
import com.paojiao.help.SMSActivity;
import com.paojiao.help.SearchActivity;
import com.paojiao.help.SoftwareActivity;
import com.paojiao.help.UsefulActivity;
import com.paojiao.help.WebSiteActivity;
import com.paojiao.help.util.DataDefine;

public class WidgetProviderVertical extends AppWidgetProvider {
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        DataDefine.settings = context.getSharedPreferences(DataDefine.PREFS_NAME, 0);
        int widgetBackgroundId = DataDefine.settings.getInt(DataDefine.PREFS_VERTICAL_WIDGET_SKIN_ID, R.drawable.widget_vertical_background);
        for (int appWidgetId : appWidgetIds) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.appwidget_provider_vertical);
            remoteViews.setOnClickPendingIntent(R.id.widget_search, PendingIntent.getActivity(context, 0, new Intent(context, SearchActivity.class), 0));
            remoteViews.setOnClickPendingIntent(R.id.widget_website, PendingIntent.getActivity(context, 0, new Intent(context, WebSiteActivity.class), 0));
            remoteViews.setOnClickPendingIntent(R.id.widget_software, PendingIntent.getActivity(context, 0, new Intent(context, SoftwareActivity.class), 0));
            remoteViews.setOnClickPendingIntent(R.id.widget_call, PendingIntent.getActivity(context, 0, new Intent(context, CallActivity.class), 0));
            remoteViews.setOnClickPendingIntent(R.id.widget_message, PendingIntent.getActivity(context, 0, new Intent(context, SMSActivity.class), 0));
            remoteViews.setOnClickPendingIntent(R.id.widget_useful, PendingIntent.getActivity(context, 0, new Intent(context, UsefulActivity.class), 0));
            remoteViews.setImageViewResource(R.id.widget_vertical_background, widgetBackgroundId);
            appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
        }
    }
}
