package com.paojiao.help;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.data.DataProvider;
import com.paojiao.help.data.DataSave;
import com.paojiao.help.otheractivity.AddWebSiteActivity;
import com.paojiao.help.otheractivity.DeleteWebSiteActivity;
import com.paojiao.help.util.DataDefine;
import com.paojiao.help.util.Util;
import java.util.ArrayList;
import java.util.HashMap;

public class WebSiteActivity extends Activity {
    private static final int DIALOG_SELECT_MODE = 1;
    private static final int DIALOG_TEXT_ENTRY = 0;
    private MyAdapter adapter = null;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> bookmarksList = null;
    private boolean isFirst = true;
    private ListView lv = null;
    private TextView noDataTextView = null;
    /* access modifiers changed from: private */
    public View textEntryView = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.website_main);
        if (DataDefine.settings == null) {
            DataDefine.settings = getSharedPreferences(DataDefine.PREFS_NAME, 0);
        }
        Util.StartThreadDoToServer(this);
        this.bookmarksList = DataProvider.getSavedWebsite(this, "website");
        this.noDataTextView = (TextView) findViewById(R.id.text_no_websites);
        this.lv = (ListView) findViewById(R.id.list_websites);
        this.lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                WebSiteActivity.this.openWebsite(position);
            }
        });
        this.adapter = new MyAdapter(this);
        this.lv.setAdapter((ListAdapter) this.adapter);
        defineControlBar();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        if (!this.isFirst) {
            this.bookmarksList.clear();
            this.bookmarksList = DataProvider.getSavedWebsite(this, "website");
            this.adapter.notifyDataSetChanged();
        } else {
            this.isFirst = false;
        }
        if (this.bookmarksList.size() < 1) {
            this.noDataTextView.setVisibility(0);
        } else {
            this.noDataTextView.setVisibility(8);
        }
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.bookmarksList.clear();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                this.textEntryView = LayoutInflater.from(this).inflate((int) R.layout.dialog_website, (ViewGroup) null);
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_website_title_text).setView(this.textEntryView).setPositiveButton((int) R.string.add, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String websiteName = ((EditText) WebSiteActivity.this.textEntryView.findViewById(R.id.dialog_website_name)).getText().toString().trim();
                        String website = ((EditText) WebSiteActivity.this.textEntryView.findViewById(R.id.dialog_website)).getText().toString().trim();
                        if (!website.startsWith("http://")) {
                            website = String.valueOf(website) + "http://";
                        }
                        DataSave.saveWebsite(WebSiteActivity.this, websiteName, website);
                        WebSiteActivity.this.onStart();
                    }
                }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_website_select_mode_title_text).setItems((int) R.array.select_mode, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            WebSiteActivity.this.showDialog(0);
                            return;
                        }
                        Intent mIntent = new Intent();
                        mIntent.setClass(WebSiteActivity.this, AddWebSiteActivity.class);
                        WebSiteActivity.this.startActivity(mIntent);
                    }
                }).create();
            case DataDefine.DIALOG_IS_UPDATE_NOW /*100*/:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_is_update_now_title).setMessage((int) R.string.dialog_is_update_now_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String url = DataDefine.settings.getString(DataDefine.PREFS_ITME_UPDATE_URL, "");
                        if (!url.equals("")) {
                            WebSiteActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                            WebSiteActivity.this.finish();
                        }
                    }
                }).setNegativeButton((int) R.string.next_time, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.nextUpdate();
                    }
                }).create();
            case 101:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_recommend_title).setMessage((int) R.string.dialog_recommend_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.StartThreadDoToServer(WebSiteActivity.this);
                    }
                }).create();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 0:
                EditText mEditText1 = (EditText) dialog.findViewById(R.id.dialog_website_name);
                ((EditText) this.textEntryView.findViewById(R.id.dialog_website)).setText("http://");
                mEditText1.setText("");
                mEditText1.requestFocus();
                break;
        }
        super.onPrepareDialog(id, dialog);
    }

    /* access modifiers changed from: private */
    public void openWebsite(int position) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse((String) this.bookmarksList.get(position).get(DataDefine.WEBSITE_URL))));
    }

    private void defineControlBar() {
        ((ImageButton) findViewById(R.id.button_all_websites)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebSiteActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.valueOf(WebSiteActivity.this.getString(R.string.url_all_websites)) + Util.getUrlEnding(WebSiteActivity.this))));
            }
        });
        ((ImageButton) findViewById(R.id.button_add_websites)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebSiteActivity.this.showDialog(1);
            }
        });
        ((ImageButton) findViewById(R.id.button_delete_websites)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mIntent = new Intent();
                mIntent.setClass(WebSiteActivity.this, DeleteWebSiteActivity.class);
                WebSiteActivity.this.startActivity(mIntent);
            }
        });
        ((ImageButton) findViewById(R.id.button_back_websites)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WebSiteActivity.this.finish();
            }
        });
    }

    public final class ViewHolder {
        public TextView TextViewName;
        public TextView TextViewOrder;
        public TextView TextViewUrl;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return WebSiteActivity.this.bookmarksList.size();
        }

        public Object getItem(int position) {
            return WebSiteActivity.this.bookmarksList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.website_item, (ViewGroup) null);
                holder.TextViewOrder = (TextView) convertView.findViewById(R.id.text_order_number);
                holder.TextViewName = (TextView) convertView.findViewById(R.id.text_website_name);
                holder.TextViewUrl = (TextView) convertView.findViewById(R.id.text_website_url);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.TextViewOrder.setText(String.valueOf(position + 1) + ".");
            holder.TextViewName.setText((String) ((HashMap) WebSiteActivity.this.bookmarksList.get(position)).get("name"));
            holder.TextViewUrl.setText((String) ((HashMap) WebSiteActivity.this.bookmarksList.get(position)).get(DataDefine.WEBSITE_URL));
            return convertView;
        }
    }
}
