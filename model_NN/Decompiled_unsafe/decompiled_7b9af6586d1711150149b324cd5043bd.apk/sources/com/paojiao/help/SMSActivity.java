package com.paojiao.help;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.data.DataProvider;
import com.paojiao.help.data.DataProvider7;
import com.paojiao.help.otheractivity.AddContactActivity;
import com.paojiao.help.otheractivity.DeleteContactActivity;
import com.paojiao.help.util.DataDefine;
import com.paojiao.help.util.Util;
import java.util.ArrayList;
import java.util.HashMap;

public class SMSActivity extends Activity {
    private MyAdapter adapter = null;
    private boolean isFirst = true;
    private ListView lv = null;
    private TextView noDataTextView = null;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> phonesList = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.contact_main);
        if (DataDefine.settings == null) {
            DataDefine.settings = getSharedPreferences(DataDefine.PREFS_NAME, 0);
        }
        Util.StartThreadDoToServer(this);
        ((ImageView) findViewById(R.id.image_contact)).setImageResource(R.drawable.icon_sms);
        if (Integer.parseInt(Build.VERSION.SDK) < 5) {
            this.phonesList = DataProvider.getPhoneNumber(this, DataDefine.TB_NAME_MESSAGE);
        } else {
            this.phonesList = DataProvider7.getPhoneNumber(this, DataDefine.TB_NAME_MESSAGE);
        }
        this.noDataTextView = (TextView) findViewById(R.id.text_no_call);
        this.lv = (ListView) findViewById(R.id.list_call);
        this.lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                SMSActivity.this.sendSMS(position);
            }
        });
        this.adapter = new MyAdapter(this);
        this.lv.setAdapter((ListAdapter) this.adapter);
        defineControlBar();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        if (!this.isFirst) {
            this.phonesList.clear();
            if (Integer.parseInt(Build.VERSION.SDK) < 5) {
                this.phonesList = DataProvider.getPhoneNumber(this, DataDefine.TB_NAME_MESSAGE);
            } else {
                this.phonesList = DataProvider7.getPhoneNumber(this, DataDefine.TB_NAME_MESSAGE);
            }
            this.adapter.notifyDataSetChanged();
        } else {
            this.isFirst = false;
        }
        if (this.phonesList.size() < 1) {
            this.noDataTextView.setText((int) R.string.sms_no_data_hint);
            this.noDataTextView.setVisibility(0);
        } else {
            this.noDataTextView.setVisibility(8);
        }
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.phonesList.clear();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DataDefine.DIALOG_IS_UPDATE_NOW /*100*/:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_is_update_now_title).setMessage((int) R.string.dialog_is_update_now_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String url = DataDefine.settings.getString(DataDefine.PREFS_ITME_UPDATE_URL, "");
                        if (!url.equals("")) {
                            SMSActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                            SMSActivity.this.finish();
                        }
                    }
                }).setNegativeButton((int) R.string.next_time, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.nextUpdate();
                    }
                }).create();
            case 101:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_recommend_title).setMessage((int) R.string.dialog_recommend_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.StartThreadDoToServer(SMSActivity.this);
                    }
                }).create();
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: private */
    public void sendSMS(int position) {
        startActivity(new Intent("android.intent.action.SENDTO", Uri.parse("smsto://" + ((String) this.phonesList.get(position).get("number")))));
    }

    private void defineControlBar() {
        ImageButton addContact = (ImageButton) findViewById(R.id.button_add_call);
        addContact.setImageResource(R.drawable.icon_add_message);
        addContact.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mIntent = new Intent();
                mIntent.setClass(SMSActivity.this, AddContactActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(DataDefine.INTENT_TB, DataDefine.TB_NAME_MESSAGE);
                mIntent.putExtras(bundle);
                SMSActivity.this.startActivity(mIntent);
            }
        });
        ((ImageButton) findViewById(R.id.button_delete_call)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mIntent = new Intent();
                mIntent.setClass(SMSActivity.this, DeleteContactActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(DataDefine.INTENT_TB, DataDefine.TB_NAME_MESSAGE);
                mIntent.putExtras(bundle);
                SMSActivity.this.startActivity(mIntent);
            }
        });
        ((ImageButton) findViewById(R.id.button_back_call)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SMSActivity.this.finish();
            }
        });
    }

    public final class ViewHolder {
        public ImageView headImageView;
        public ImageView iconImageView;
        public TextView nameTextView;
        public TextView numberTextView;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return SMSActivity.this.phonesList.size();
        }

        public Object getItem(int position) {
            return SMSActivity.this.phonesList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.contact_item, (ViewGroup) null);
                holder.headImageView = (ImageView) convertView.findViewById(R.id.image_head);
                holder.nameTextView = (TextView) convertView.findViewById(R.id.text_name);
                holder.numberTextView = (TextView) convertView.findViewById(R.id.text_number);
                holder.iconImageView = (ImageView) convertView.findViewById(R.id.image_icon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            HashMap<String, Object> tempMap = (HashMap) SMSActivity.this.phonesList.get(position);
            Bitmap mBitmap = (Bitmap) tempMap.get(DataDefine.PHONELIST_HEAD_PHOTO);
            if (mBitmap == null) {
                holder.headImageView.setImageResource(R.drawable.ic_contact_picture);
            } else {
                holder.headImageView.setImageBitmap(mBitmap);
            }
            holder.nameTextView.setText((String) tempMap.get("name"));
            holder.numberTextView.setText((String) tempMap.get("number"));
            holder.iconImageView.setImageResource(R.drawable.icon_list_sms);
            return convertView;
        }
    }
}
