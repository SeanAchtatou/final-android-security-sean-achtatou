package com.paojiao.help;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.util.DataDefine;
import com.paojiao.help.util.Util;

public class MainActivity extends Activity {
    private static final int DIALOG_HELP = 2;
    private static final int DIALOG_SEARCH = 1;
    private static final int DIALOG_UPDATE = 0;
    private MyAdapter adapter = null;
    /* access modifiers changed from: private */
    public String searchEngine = "default";
    /* access modifiers changed from: private */
    public String[] searchEngineArray = null;
    private int searchEngineId = -1;
    /* access modifiers changed from: private */
    public int updateTimeLimit = 15;
    /* access modifiers changed from: private */
    public int[] updateTimeLimitArray = null;
    private int updateTimeLimitId = -1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        Util.MainSetStart(this);
        this.updateTimeLimit = DataDefine.settings.getInt(DataDefine.PREFS_ITEM_UPDATE_TIME_LIMIT_NAME, 15);
        this.searchEngine = DataDefine.settings.getString(DataDefine.PREFS_ITEM_SEARCH_ENGINE_NAME, "default");
        this.updateTimeLimitArray = getResources().getIntArray(R.array.select_update_time_limit_url);
        this.searchEngineArray = getResources().getStringArray(R.array.select_search_engine_url);
        int i = 0;
        while (true) {
            if (i >= this.updateTimeLimitArray.length) {
                break;
            } else if (this.updateTimeLimitArray[i] == this.updateTimeLimit) {
                this.updateTimeLimitId = i;
                break;
            } else {
                i++;
            }
        }
        int i2 = 0;
        while (true) {
            if (i2 >= this.searchEngineArray.length) {
                break;
            } else if (this.searchEngineArray[i2].equals(this.searchEngine)) {
                this.searchEngineId = i2;
                break;
            } else {
                i2++;
            }
        }
        this.adapter = new MyAdapter(this);
        ListView mListView = (ListView) findViewById(R.id.list_set);
        mListView.setAdapter((ListAdapter) this.adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                switch (position) {
                    case 0:
                        MainActivity.this.showDialog(1);
                        return;
                    case 1:
                        MainActivity.this.showDialog(0);
                        return;
                    case MainActivity.DIALOG_HELP /*2*/:
                        Intent sendIntent = new Intent("android.intent.action.SENDTO", Uri.parse("sms://"));
                        sendIntent.putExtra("address", "");
                        sendIntent.putExtra("sms_body", MainActivity.this.getString(R.string.sms_recommend_content));
                        MainActivity.this.startActivity(sendIntent);
                        return;
                    case 3:
                        MainActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.valueOf(MainActivity.this.getString(R.string.url_official)) + Util.getUrlEnding(MainActivity.this))));
                        return;
                    case 4:
                        MainActivity.this.showDialog(MainActivity.DIALOG_HELP);
                        return;
                    default:
                        return;
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_select_update_time_limit_title).setSingleChoiceItems((int) R.array.select_update_time_limit, this.updateTimeLimitId, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.updateTimeLimit = MainActivity.this.updateTimeLimitArray[which];
                    }
                }).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        SharedPreferences.Editor edit = DataDefine.settings.edit();
                        edit.putInt(DataDefine.PREFS_ITEM_UPDATE_TIME_LIMIT_NAME, MainActivity.this.updateTimeLimit);
                        edit.putLong(DataDefine.PREFS_ITEM_NEXT_UPDATE_TIME_NAME, System.currentTimeMillis() + ((long) (MainActivity.this.updateTimeLimit * DataDefine.ONE_DAY_TIME)));
                        edit.commit();
                    }
                }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_select_search_engine_title).setSingleChoiceItems((int) R.array.select_search_engine, this.searchEngineId, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.searchEngine = MainActivity.this.searchEngineArray[which];
                    }
                }).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        SharedPreferences.Editor edit = DataDefine.settings.edit();
                        edit.putString(DataDefine.PREFS_ITEM_SEARCH_ENGINE_NAME, MainActivity.this.searchEngine);
                        edit.commit();
                    }
                }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            case DIALOG_HELP /*2*/:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_help_title).setMessage((int) R.string.dialog_help_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            case DataDefine.DIALOG_IS_UPDATE_NOW /*100*/:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_is_update_now_title).setMessage((int) R.string.dialog_is_update_now_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String url = DataDefine.settings.getString(DataDefine.PREFS_ITME_UPDATE_URL, "");
                        if (!url.equals("")) {
                            MainActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                            MainActivity.this.finish();
                        }
                    }
                }).setNegativeButton((int) R.string.next_time, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.nextUpdate();
                    }
                }).create();
            case 101:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_recommend_title).setMessage((int) R.string.dialog_recommend_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.StartThreadDoToServer(MainActivity.this);
                    }
                }).create();
            default:
                return super.onCreateDialog(id);
        }
    }

    public final class ViewHolder {
        public ImageView imageViewIcon;
        public TextView textViewName;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return 5;
        }

        public Object getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.set_item, (ViewGroup) null);
                holder.textViewName = (TextView) convertView.findViewById(R.id.text_set_name);
                holder.imageViewIcon = (ImageView) convertView.findViewById(R.id.image_set_icon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            switch (position) {
                case 0:
                    holder.textViewName.setText((int) R.string.list_set_search);
                    holder.imageViewIcon.setImageResource(R.drawable.icon_list_more);
                    break;
                case 1:
                    holder.textViewName.setText((int) R.string.list_set_update);
                    holder.imageViewIcon.setImageResource(R.drawable.icon_list_more);
                    break;
                case MainActivity.DIALOG_HELP /*2*/:
                    holder.textViewName.setText((int) R.string.list_set_recommend);
                    break;
                case 3:
                    holder.textViewName.setText((int) R.string.list_set_official);
                    break;
                case 4:
                    holder.textViewName.setText((int) R.string.list_set_help);
                    break;
            }
            return convertView;
        }
    }
}
