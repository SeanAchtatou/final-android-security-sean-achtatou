package com.paojiao.help;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.data.DataDelete;
import com.paojiao.help.data.DataProvider;
import com.paojiao.help.data.DataSave;
import com.paojiao.help.util.DBHelper;
import com.paojiao.help.util.DataDefine;
import com.paojiao.help.util.Util;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class SearchActivity extends Activity {
    public static final int MENU_DELETE_ALL = 0;
    public EditText edit = null;
    public ArrayList<HashMap<String, Object>> keyWordList = null;
    public MyAdapter myAdapter = null;
    public String searchEngine = null;
    public String searchURL = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        requestWindowFeature(2);
        setContentView((int) R.layout.search);
        if (DataDefine.settings == null) {
            DataDefine.settings = getSharedPreferences(DataDefine.PREFS_NAME, 0);
        }
        Util.StartThreadDoToServer(this);
        if (DataDefine.settings.getBoolean(DataDefine.PREFS_ITME_IS_FIRST_TIME_TO_OPEN_SEARCH, true)) {
            SharedPreferences.Editor edit2 = DataDefine.settings.edit();
            edit2.putBoolean(DataDefine.PREFS_ITME_IS_FIRST_TIME_TO_OPEN_SEARCH, false);
            edit2.commit();
            showDialog(101);
        }
        this.edit = (EditText) findViewById(R.id.edittext_search);
        this.edit.setHint(Util.getRandomString(getResources().getStringArray(R.array.several_search_key_word)));
        this.edit.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode != 66 || event.getAction() != 1) {
                    return false;
                }
                SearchActivity.this.startSearch();
                return false;
            }
        });
        ((Button) findViewById(R.id.clearable_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchActivity.this.edit.setText("");
            }
        });
        DataDefine.deviceId = ((TelephonyManager) getSystemService("phone")).getDeviceId();
        this.searchURL = getString(R.string.url_search);
        DataDefine.channel = getString(R.string.search_channel);
        ((Button) findViewById(R.id.button_search)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SearchActivity.this.startSearch();
            }
        });
        ListView keyWordListView = (ListView) findViewById(R.id.history_search_word);
        this.keyWordList = DataProvider.getAllSearchKeyWord(this);
        this.myAdapter = new MyAdapter(this);
        keyWordListView.setAdapter((ListAdapter) this.myAdapter);
        keyWordListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                SearchActivity.this.edit.setText((String) SearchActivity.this.keyWordList.get(position).get(DataDefine.KEYWORDLIST_WORD));
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        this.searchEngine = DataDefine.settings.getString(DataDefine.PREFS_ITEM_SEARCH_ENGINE_NAME, "default");
        this.keyWordList = DataProvider.getAllSearchKeyWord(this);
        this.myAdapter.notifyDataSetChanged();
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DataDefine.DIALOG_IS_UPDATE_NOW /*100*/:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_is_update_now_title).setMessage((int) R.string.dialog_is_update_now_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String url = DataDefine.settings.getString(DataDefine.PREFS_ITME_UPDATE_URL, "");
                        if (!url.equals("")) {
                            SearchActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                            SearchActivity.this.finish();
                        }
                    }
                }).setNegativeButton((int) R.string.next_time, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.nextUpdate();
                    }
                }).create();
            case 101:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_search_help_title).setMessage((int) R.string.dialog_search_help_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.StartThreadDoToServer(SearchActivity.this);
                    }
                }).create();
            default:
                return super.onCreateDialog(id);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, (int) R.string.menu_search_delete_all_word);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                DataDelete.deleteDataOfAllSearchHistory(this);
                this.keyWordList.clear();
                this.myAdapter.notifyDataSetChanged();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: private */
    public void startSearch() {
        String searchContent = this.edit.getText().toString();
        if (searchContent.length() < 1) {
            searchContent = (String) this.edit.getHint();
        }
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.valueOf(this.searchURL) + Util.toUtf8String(searchContent) + "/" + DataDefine.deviceId + "/" + DataDefine.channel + "/" + this.searchEngine + "/android")));
        saveKeyWord(searchContent);
    }

    private void saveKeyWord(String searchContent) {
        int id = -1;
        int order = 0;
        int listSize = this.keyWordList.size();
        if (listSize > 0) {
            order = ((Integer) this.keyWordList.get(0).get(DataDefine.KEYWORDLIST_ORDER)).intValue() + 1;
            Iterator<HashMap<String, Object>> it = this.keyWordList.iterator();
            while (true) {
                if (it.hasNext()) {
                    HashMap<String, Object> tempMap = it.next();
                    if (searchContent.equals(tempMap.get(DataDefine.KEYWORDLIST_WORD))) {
                        id = ((Integer) tempMap.get("id")).intValue();
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        if (id < 0 && listSize >= 30) {
            SQLiteDatabase db = new DBHelper(this).getWritableDatabase();
            db.delete(DataDefine.TB_NAME_SEARCH_HISTORY, "_id=?", new String[]{String.valueOf(this.keyWordList.get(listSize - 1).get("id"))});
            db.close();
        }
        DataSave.saveSearchKeyWord(this, searchContent, order, id);
    }

    public final class ViewHolder {
        public TextView textViewName;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return SearchActivity.this.keyWordList.size();
        }

        public Object getItem(int position) {
            return SearchActivity.this.keyWordList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* Debug info: failed to restart local var, previous not found, register: 4 */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.search_history_item, (ViewGroup) null);
                holder.textViewName = (TextView) convertView.findViewById(R.id.text_history_name);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.textViewName.setText((String) SearchActivity.this.keyWordList.get(position).get(DataDefine.KEYWORDLIST_WORD));
            return convertView;
        }
    }
}
