package com.paojiao.help.data;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.Browser;
import android.provider.ContactsContract;
import com.paojiao.help.bean.ApplicationInfo;
import com.paojiao.help.otheractivity.AddContactActivity;
import com.paojiao.help.util.DBHelper;
import com.paojiao.help.util.DataDefine;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class DataProvider7 {
    public static Cursor getCursorFromContacts(Context mContext) {
        return mContext.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, new String[]{DataDefine.TB_ID, DataDefine.TB_DISPLAY_NAME, "photo_id"}, "has_phone_number > 0", null, "display_name DESC");
    }

    public static ArrayList<HashMap<String, Object>> getAllPhoneNumberFromContacts(Context mContext, Cursor mCursor) {
        int size = mCursor.getCount();
        ArrayList<HashMap<String, Object>> phonesList = new ArrayList<>();
        String[] projectionPhone = {"data1"};
        if (mCursor.moveToFirst()) {
            AddContactActivity.getContactNum = 1;
            do {
                String contactId = mCursor.getString(0);
                String contactName = mCursor.getString(1);
                String contactHeadId = mCursor.getString(2);
                if (contactId != null && contactId.length() > 0) {
                    Cursor phones = mContext.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projectionPhone, "contact_id = " + contactId, null, null);
                    if (phones.moveToFirst()) {
                        do {
                            HashMap<String, Object> phoneMap = new HashMap<>();
                            phoneMap.put("contact_id", contactId);
                            if (contactName == null || contactName.length() <= 0) {
                                phoneMap.put("name", DataDefine.NULL);
                            } else {
                                phoneMap.put("name", contactName);
                            }
                            String contactNumber = phones.getString(0);
                            if (contactNumber == null || contactNumber.length() <= 0) {
                                phoneMap.put("number", DataDefine.NULL);
                            } else {
                                phoneMap.put("number", contactNumber);
                            }
                            if (contactHeadId != null && contactHeadId.length() > 0) {
                                phoneMap.put(DataDefine.PHONELIST_HEAD_PHOTO, getContactHead(contactHeadId, mContext));
                            }
                            phonesList.add(phoneMap);
                        } while (phones.moveToNext());
                    }
                    phones.close();
                }
                AddContactActivity.getContactNum++;
                if (size < AddContactActivity.getContactNum) {
                    break;
                }
            } while (mCursor.moveToNext());
        }
        mCursor.close();
        return phonesList;
    }

    public static String getHeadPhotoId(String contactId, Context mContext) {
        Cursor c = mContext.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, new String[]{"photo_id"}, "_id = " + contactId, null, null);
        if (c.moveToFirst()) {
            return c.getString(0);
        }
        return "";
    }

    public static ArrayList<HashMap<String, Object>> getPhoneNumber(Context mContext, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ArrayList<HashMap<String, Object>> phonesList = new ArrayList<>();
        Cursor c = null;
        try {
            c = db.query(dbTable, null, null, null, null, null, null);
            if (c.moveToFirst()) {
                do {
                    if (!isDeleted(mContext, c.getString(1), c.getString(3))) {
                        HashMap<String, Object> phoneMap = new HashMap<>();
                        phoneMap.put("id", c.getString(0));
                        phoneMap.put("name", c.getString(2));
                        phoneMap.put("number", c.getString(3));
                        phoneMap.put(DataDefine.PHONELIST_HEAD_PHOTO, getContactHead(getHeadPhotoId(c.getString(1), mContext), mContext));
                        phonesList.add(phoneMap);
                    } else {
                        DataDelete.deleteSingleData(db, dbTable, c.getString(0));
                    }
                } while (c.moveToNext());
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (SQLiteException e) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
            throw th;
        }
        return phonesList;
    }

    public static Bitmap getContactHead(String photoId, Context mContext) {
        byte[] data;
        Bitmap mBitmap = null;
        String[] projection = {"data15"};
        if (photoId != null && photoId.length() > 0) {
            Cursor cur = mContext.getContentResolver().query(ContactsContract.Data.CONTENT_URI, projection, "_id=" + photoId, null, null);
            if (cur.moveToFirst() && (data = cur.getBlob(0)) != null) {
                mBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(data));
            }
            cur.close();
        }
        return mBitmap;
    }

    public static boolean isDeleted(Context mContext, String contactId, String phoneNumber) {
        boolean isDeleted = true;
        Cursor phones = mContext.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"data1"}, "contact_id = " + contactId, null, null);
        if (phones.moveToFirst()) {
            while (true) {
                if (!phoneNumber.equals(phones.getString(0))) {
                    if (!phones.moveToNext()) {
                        break;
                    }
                } else {
                    isDeleted = false;
                    break;
                }
            }
        }
        phones.close();
        return isDeleted;
    }

    public static ArrayList<HashMap<String, Object>> getBookmarksFromBrowser(Context mContext) {
        String[] bookmarksArray = {"title", "url"};
        ArrayList<HashMap<String, Object>> bookmarksList = new ArrayList<>();
        Cursor c = null;
        try {
            Cursor c2 = mContext.getContentResolver().query(Browser.BOOKMARKS_URI, bookmarksArray, null, null, "title ASC");
            c2.moveToFirst();
            if (c2.getCount() > 0) {
                do {
                    HashMap<String, Object> bookmarkMap = new HashMap<>();
                    bookmarkMap.put("name", c2.getString(0));
                    bookmarkMap.put(DataDefine.WEBSITE_URL, c2.getString(1));
                    bookmarksList.add(bookmarkMap);
                } while (c2.moveToNext());
            }
            if (c2 != null && !c2.isClosed()) {
                c2.close();
            }
        } catch (SQLiteException e) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
        } catch (Throwable th) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            throw th;
        }
        return bookmarksList;
    }

    public static ArrayList<HashMap<String, Object>> getSavedWebsite(Context mContext, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ArrayList<HashMap<String, Object>> websitesList = new ArrayList<>();
        Cursor c = null;
        try {
            Cursor c2 = db.query(dbTable, null, null, null, null, null, null);
            c2.moveToFirst();
            if (c2.getCount() > 0) {
                do {
                    HashMap<String, Object> bookmarkMap = new HashMap<>();
                    bookmarkMap.put("id", c2.getString(0));
                    bookmarkMap.put("name", c2.getString(1));
                    bookmarkMap.put(DataDefine.WEBSITE_URL, c2.getString(2));
                    websitesList.add(bookmarkMap);
                } while (c2.moveToNext());
            }
            if (c2 != null && !c2.isClosed()) {
                c2.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (SQLiteException e) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
            throw th;
        }
        return websitesList;
    }

    public static ArrayList<ApplicationInfo> getAllApplications(Context mContext) {
        PackageManager manager = mContext.getPackageManager();
        Intent mIntent = new Intent("android.intent.action.MAIN", (Uri) null);
        mIntent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> apps = manager.queryIntentActivities(mIntent, 0);
        Collections.sort(apps, new ResolveInfo.DisplayNameComparator(manager));
        if (apps == null) {
            return null;
        }
        int count = apps.size();
        ArrayList<ApplicationInfo> mApplications = new ArrayList<>(count);
        mApplications.clear();
        for (int i = 0; i < count; i++) {
            ApplicationInfo application = new ApplicationInfo();
            ResolveInfo info = apps.get(i);
            application.name = (String) info.loadLabel(manager);
            application.fullName = info.activityInfo.name;
            application.packageName = info.activityInfo.packageName;
            application.icon = info.activityInfo.loadIcon(manager);
            mApplications.add(application);
        }
        return mApplications;
    }

    public static String getSize(long size) {
        if (size > ((long) 1024)) {
            long tempLong = size / ((long) 1024);
            if (tempLong > ((long) 1024)) {
                String tempString = String.valueOf(tempLong / ((long) 1024));
                if (tempString.length() > 4) {
                    tempString = tempString.substring(0, 3);
                }
                return String.valueOf(tempString) + "MB";
            }
            String tempString2 = String.valueOf(tempLong);
            if (tempString2.length() > 4) {
                tempString2 = tempString2.substring(0, 3);
            }
            return String.valueOf(tempString2) + "KB";
        }
        String tempString3 = String.valueOf(size);
        if (tempString3.length() > 4) {
            tempString3 = tempString3.substring(0, 3);
        }
        return String.valueOf(tempString3) + "B";
    }

    public static ArrayList<ApplicationInfo> getSavedApplications(Context mContext, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ArrayList<ApplicationInfo> appList = new ArrayList<>();
        Cursor c = null;
        PackageManager manager = mContext.getPackageManager();
        try {
            c = db.query(dbTable, null, null, null, null, null, null);
            if (c.moveToFirst()) {
                do {
                    ApplicationInfo appInfo = new ApplicationInfo();
                    appInfo.id = c.getString(0);
                    appInfo.name = c.getString(1);
                    appInfo.fullName = c.getString(2);
                    appInfo.packageName = c.getString(3);
                    try {
                        appInfo.icon = manager.getActivityIcon(appInfo.getIntent());
                        appList.add(appInfo);
                    } catch (PackageManager.NameNotFoundException e) {
                        DataDelete.deleteSingleData(db, dbTable, appInfo.id);
                    }
                } while (c.moveToNext());
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (SQLiteException e2) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
            throw th;
        }
        return appList;
    }
}
