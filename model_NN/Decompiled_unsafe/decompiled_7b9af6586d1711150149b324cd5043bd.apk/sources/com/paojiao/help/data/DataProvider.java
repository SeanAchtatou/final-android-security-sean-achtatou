package com.paojiao.help.data;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.Browser;
import android.provider.Contacts;
import android.util.Log;
import com.paojiao.help.bean.ApplicationInfo;
import com.paojiao.help.otheractivity.AddContactActivity;
import com.paojiao.help.otheractivity.AddSoftwareActivity;
import com.paojiao.help.util.DBHelper;
import com.paojiao.help.util.DataDefine;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class DataProvider {
    public static ArrayList<HashMap<String, Object>> getAllSearchKeyWord(Context mContext) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ArrayList<HashMap<String, Object>> keyWordList = new ArrayList<>();
        Cursor c = null;
        try {
            Cursor c2 = db.query(DataDefine.TB_NAME_SEARCH_HISTORY, null, null, null, null, null, "keyword_order DESC");
            if (c2.moveToFirst()) {
                do {
                    HashMap<String, Object> phoneMap = new HashMap<>();
                    phoneMap.put("id", Integer.valueOf(c2.getInt(0)));
                    phoneMap.put(DataDefine.KEYWORDLIST_WORD, c2.getString(1));
                    phoneMap.put(DataDefine.KEYWORDLIST_ORDER, Integer.valueOf(c2.getInt(2)));
                    keyWordList.add(phoneMap);
                } while (c2.moveToNext());
            }
            if (c2 != null && !c2.isClosed()) {
                c2.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (SQLiteException e) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
            throw th;
        }
        return keyWordList;
    }

    public static Cursor getCursorFromContacts(Context mContext) {
        String[] phonesArray = {DataDefine.TB_ID, "name", "number"};
        Cursor c = null;
        try {
            return mContext.getContentResolver().query(Contacts.Phones.CONTENT_URI, phonesArray, "number is not null", null, "name ASC");
        } catch (SQLiteException e) {
            if (c == null || c.isClosed()) {
                return c;
            }
            c.close();
            return c;
        }
    }

    public static ArrayList<HashMap<String, Object>> getAllPhoneNumberFromContacts(Cursor c) {
        int size = c.getCount();
        ArrayList<HashMap<String, Object>> phonesList = new ArrayList<>();
        try {
            if (c.moveToFirst()) {
                AddContactActivity.getContactNum = 1;
                do {
                    HashMap<String, Object> phoneMap = new HashMap<>();
                    phoneMap.put("contact_id", c.getString(0));
                    phoneMap.put("name", c.getString(1));
                    phoneMap.put("number", c.getString(2));
                    phonesList.add(phoneMap);
                    AddContactActivity.getContactNum++;
                    if (size < AddContactActivity.getContactNum) {
                        break;
                    }
                } while (c.moveToNext());
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
        } catch (SQLiteException e) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
        } catch (Throwable th) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            throw th;
        }
        return phonesList;
    }

    public static ArrayList<HashMap<String, Object>> getPhoneNumber(Context mContext, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ArrayList<HashMap<String, Object>> phonesList = new ArrayList<>();
        Cursor c = null;
        try {
            Cursor c2 = db.query(dbTable, null, null, null, null, null, null);
            Log.i("info", "c.size:" + c2.getCount());
            if (c2.moveToFirst()) {
                do {
                    Log.i("info", "c.getString(0):" + c2.getString(0));
                    Log.i("info", "c.getString(2):" + c2.getString(2));
                    Log.i("info", "c.getString(3):" + c2.getString(3));
                    HashMap<String, Object> phoneMap = new HashMap<>();
                    phoneMap.put("id", c2.getString(0));
                    phoneMap.put("name", c2.getString(2));
                    phoneMap.put("number", c2.getString(3));
                    phonesList.add(phoneMap);
                } while (c2.moveToNext());
            }
            if (c2 != null && !c2.isClosed()) {
                c2.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (SQLiteException e) {
            Log.i("info", e.getMessage());
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
            throw th;
        }
        Log.i("info", "phonesList.size:" + phonesList.size());
        return phonesList;
    }

    public static ArrayList<HashMap<String, Object>> getBookmarksFromBrowser(Context mContext) {
        String[] bookmarksArray = {"title", "url"};
        ArrayList<HashMap<String, Object>> bookmarksList = new ArrayList<>();
        Cursor c = null;
        try {
            Cursor c2 = mContext.getContentResolver().query(Browser.BOOKMARKS_URI, bookmarksArray, null, null, "title ASC");
            c2.moveToFirst();
            if (c2.getCount() > 0) {
                do {
                    HashMap<String, Object> bookmarkMap = new HashMap<>();
                    bookmarkMap.put("name", c2.getString(0));
                    bookmarkMap.put(DataDefine.WEBSITE_URL, c2.getString(1));
                    bookmarksList.add(bookmarkMap);
                } while (c2.moveToNext());
            }
            if (c2 != null && !c2.isClosed()) {
                c2.close();
            }
        } catch (SQLiteException e) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
        } catch (Throwable th) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            throw th;
        }
        return bookmarksList;
    }

    public static ArrayList<HashMap<String, Object>> getSavedWebsite(Context mContext, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ArrayList<HashMap<String, Object>> websitesList = new ArrayList<>();
        Cursor c = null;
        try {
            Cursor c2 = db.query(dbTable, null, null, null, null, null, null);
            c2.moveToFirst();
            if (c2.getCount() > 0) {
                do {
                    HashMap<String, Object> bookmarkMap = new HashMap<>();
                    bookmarkMap.put("id", c2.getString(0));
                    bookmarkMap.put("name", c2.getString(1));
                    bookmarkMap.put(DataDefine.WEBSITE_URL, c2.getString(2));
                    websitesList.add(bookmarkMap);
                } while (c2.moveToNext());
            }
            if (c2 != null && !c2.isClosed()) {
                c2.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (SQLiteException e) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
            throw th;
        }
        return websitesList;
    }

    public static List<ResolveInfo> getAllApplicationsNum(Context mContext) {
        PackageManager manager = mContext.getPackageManager();
        Intent mIntent = new Intent("android.intent.action.MAIN", (Uri) null);
        mIntent.addCategory("android.intent.category.LAUNCHER");
        return manager.queryIntentActivities(mIntent, 0);
    }

    public static ArrayList<ApplicationInfo> getAllApplications(List<ResolveInfo> apps, PackageManager manager) {
        Collections.sort(apps, new ResolveInfo.DisplayNameComparator(manager));
        if (apps == null) {
            return null;
        }
        int count = apps.size();
        ArrayList<ApplicationInfo> mApplications = new ArrayList<>(count);
        mApplications.clear();
        while (AddSoftwareActivity.getAppNum < count) {
            ApplicationInfo application = new ApplicationInfo();
            ResolveInfo info = apps.get(AddSoftwareActivity.getAppNum);
            application.name = (String) info.loadLabel(manager);
            application.fullName = info.activityInfo.name;
            application.packageName = info.activityInfo.packageName;
            application.icon = info.activityInfo.loadIcon(manager);
            mApplications.add(application);
            AddSoftwareActivity.getAppNum++;
        }
        return mApplications;
    }

    public static String getSize(long size) {
        if (size > ((long) 1024)) {
            long tempLong = size / ((long) 1024);
            if (tempLong > ((long) 1024)) {
                String tempString = String.valueOf(tempLong / ((long) 1024));
                if (tempString.length() > 4) {
                    tempString = tempString.substring(0, 3);
                }
                return String.valueOf(tempString) + "MB";
            }
            String tempString2 = String.valueOf(tempLong);
            if (tempString2.length() > 4) {
                tempString2 = tempString2.substring(0, 3);
            }
            return String.valueOf(tempString2) + "KB";
        }
        String tempString3 = String.valueOf(size);
        if (tempString3.length() > 4) {
            tempString3 = tempString3.substring(0, 3);
        }
        return String.valueOf(tempString3) + "B";
    }

    public static ArrayList<ApplicationInfo> getSavedApplications(Context mContext, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ArrayList<ApplicationInfo> appList = new ArrayList<>();
        Cursor c = null;
        PackageManager manager = mContext.getPackageManager();
        try {
            c = db.query(dbTable, null, null, null, null, null, null);
            if (c.moveToFirst()) {
                do {
                    ApplicationInfo appInfo = new ApplicationInfo();
                    appInfo.id = c.getString(0);
                    appInfo.name = c.getString(1);
                    appInfo.fullName = c.getString(2);
                    appInfo.packageName = c.getString(3);
                    try {
                        appInfo.icon = manager.getActivityIcon(appInfo.getIntent());
                        appList.add(appInfo);
                    } catch (PackageManager.NameNotFoundException e) {
                        DataDelete.deleteSingleData(db, dbTable, appInfo.id);
                    }
                } while (c.moveToNext());
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (SQLiteException e2) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
        } catch (Throwable th) {
            if (c != null && !c.isClosed()) {
                c.close();
            }
            if (db.isOpen()) {
                db.close();
            }
            throw th;
        }
        return appList;
    }
}
