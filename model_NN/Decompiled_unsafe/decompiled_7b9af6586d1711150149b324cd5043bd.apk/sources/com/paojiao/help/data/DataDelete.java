package com.paojiao.help.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.paojiao.help.bean.ApplicationInfo;
import com.paojiao.help.util.DBHelper;
import com.paojiao.help.util.DataDefine;
import java.util.ArrayList;
import java.util.HashMap;

public class DataDelete {
    public static void deleteSingleData(SQLiteDatabase db, String dbTable, String id) {
        db.delete(dbTable, "_id = " + id, null);
    }

    public static void deleteDataOfAllSearchHistory(Context mContext) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        db.delete(DataDefine.TB_NAME_SEARCH_HISTORY, null, null);
        db.close();
    }

    public static void deleteDataOfContact(Context mContext, ArrayList<HashMap<String, Object>> phonesList, boolean[] mCheckBoxs, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        int length = mCheckBoxs.length;
        for (int i = 0; i < length; i++) {
            if (mCheckBoxs[i]) {
                db.delete(dbTable, "_id = " + phonesList.get(i).get("id"), null);
            }
            values.clear();
        }
        db.close();
    }

    public static void deleteDataOfWebsite(Context mContext, ArrayList<HashMap<String, Object>> bookmarksList, boolean[] mCheckBoxs, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        int length = mCheckBoxs.length;
        for (int i = 0; i < length; i++) {
            if (mCheckBoxs[i]) {
                db.delete(dbTable, "_id = " + bookmarksList.get(i).get("id"), null);
            }
            values.clear();
        }
        db.close();
    }

    public static void deleteDataOfApp(Context mContext, ArrayList<ApplicationInfo> appList, boolean[] mCheckBoxs, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        int length = mCheckBoxs.length;
        for (int i = 0; i < length; i++) {
            if (mCheckBoxs[i]) {
                db.delete(dbTable, "_id = " + appList.get(i).id, null);
            }
            values.clear();
        }
        db.close();
    }
}
