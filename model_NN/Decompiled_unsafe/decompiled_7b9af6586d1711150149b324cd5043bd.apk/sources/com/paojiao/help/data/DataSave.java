package com.paojiao.help.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.paojiao.help.bean.ApplicationInfo;
import com.paojiao.help.util.DBHelper;
import com.paojiao.help.util.DataDefine;
import java.util.ArrayList;
import java.util.HashMap;

public class DataSave {
    public static void saveSearchKeyWord(Context mContext, String keyWord, int order, int id) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataDefine.TB_DISPLAY_NAME, keyWord);
        values.put(DataDefine.TB_SEARCH_ORDER, Integer.valueOf(order));
        if (id < 0) {
            db.insert(DataDefine.TB_NAME_SEARCH_HISTORY, DataDefine.TB_ID, values);
        } else {
            db.update(DataDefine.TB_NAME_SEARCH_HISTORY, values, "_id=?", new String[]{String.valueOf(id)});
        }
        db.close();
    }

    public static void savePhoneNumber(Context mContext, ArrayList<HashMap<String, Object>> phonesList, boolean[] mCheckBoxs, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        int length = mCheckBoxs.length;
        for (int i = 0; i < length; i++) {
            if (mCheckBoxs[i]) {
                HashMap<String, Object> hashMap = phonesList.get(i);
                values.put("contact_id", (String) hashMap.get("contact_id"));
                values.put(DataDefine.TB_DISPLAY_NAME, (String) hashMap.get("name"));
                values.put("number", (String) hashMap.get("number"));
                db.insert(dbTable, DataDefine.TB_ID, values);
            }
            values.clear();
        }
        db.close();
    }

    public static void saveWebsites(Context mContext, ArrayList<HashMap<String, Object>> bookmarksList, boolean[] mCheckBoxs, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        int length = mCheckBoxs.length;
        for (int i = 0; i < length; i++) {
            if (mCheckBoxs[i]) {
                HashMap<String, Object> hashMap = bookmarksList.get(i);
                values.put(DataDefine.TB_DISPLAY_NAME, (String) hashMap.get("name"));
                values.put("website", (String) hashMap.get(DataDefine.WEBSITE_URL));
                db.insert(dbTable, DataDefine.TB_ID, values);
            }
            values.clear();
        }
        db.close();
    }

    public static void saveWebsite(Context mContext, String websiteName, String websiteURL) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DataDefine.TB_DISPLAY_NAME, websiteName);
        values.put("website", websiteURL);
        db.insert("website", DataDefine.TB_ID, values);
        db.close();
    }

    public static void saveAppData(Context mContext, ArrayList<ApplicationInfo> appList, boolean[] mCheckBoxs, String dbTable) {
        SQLiteDatabase db = new DBHelper(mContext).getWritableDatabase();
        ContentValues values = new ContentValues();
        int length = mCheckBoxs.length;
        for (int i = 0; i < length; i++) {
            if (mCheckBoxs[i]) {
                ApplicationInfo appInfo = appList.get(i);
                values.put(DataDefine.TB_DISPLAY_NAME, appInfo.name);
                values.put(DataDefine.TB_FULL_NAME, appInfo.fullName);
                values.put(DataDefine.TB_PACKAGE_NAME, appInfo.packageName);
                db.insert(dbTable, DataDefine.TB_ID, values);
            }
            values.clear();
        }
        db.close();
    }
}
