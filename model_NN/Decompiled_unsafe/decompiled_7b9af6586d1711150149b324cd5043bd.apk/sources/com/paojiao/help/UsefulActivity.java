package com.paojiao.help;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.util.DataDefine;
import com.paojiao.help.util.Util;

public class UsefulActivity extends Activity {
    private MyAdapter adapter = null;
    public String deviceId = null;
    /* access modifiers changed from: private */
    public String[] itemName = null;
    /* access modifiers changed from: private */
    public String[] itemValue = null;
    public String searchChannel = null;
    /* access modifiers changed from: private */
    public String urlFront = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.itemName = getResources().getStringArray(R.array.select_useful_item_name);
        this.itemValue = getResources().getStringArray(R.array.select_useful_item_value);
        setContentView((int) R.layout.main);
        if (DataDefine.settings == null) {
            DataDefine.settings = getSharedPreferences(DataDefine.PREFS_NAME, 0);
        }
        Util.StartThreadDoToServer(this);
        this.deviceId = ((TelephonyManager) getSystemService("phone")).getDeviceId();
        this.searchChannel = getString(R.string.search_channel);
        this.urlFront = getString(R.string.url_useful_front);
        this.adapter = new MyAdapter(this);
        ListView mListView = (ListView) findViewById(R.id.list_set);
        mListView.setAdapter((ListAdapter) this.adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == 8) {
                    UsefulActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(UsefulActivity.this.getString(R.string.url_android_software_download_list))));
                    UsefulActivity.this.finish();
                    return;
                }
                UsefulActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.valueOf(UsefulActivity.this.urlFront) + UsefulActivity.this.itemValue[position] + "/" + UsefulActivity.this.deviceId + "/" + UsefulActivity.this.searchChannel + "/android")));
                UsefulActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DataDefine.DIALOG_IS_UPDATE_NOW /*100*/:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_is_update_now_title).setMessage((int) R.string.dialog_is_update_now_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String url = DataDefine.settings.getString(DataDefine.PREFS_ITME_UPDATE_URL, "");
                        if (!url.equals("")) {
                            UsefulActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                            UsefulActivity.this.finish();
                        }
                    }
                }).setNegativeButton((int) R.string.next_time, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.nextUpdate();
                    }
                }).create();
            case 101:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_recommend_title).setMessage((int) R.string.dialog_recommend_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.StartThreadDoToServer(UsefulActivity.this);
                    }
                }).create();
            default:
                return null;
        }
    }

    public final class ViewHolder {
        public ImageView imageViewIcon;
        public TextView textViewName;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return UsefulActivity.this.itemName.length;
        }

        public Object getItem(int position) {
            return UsefulActivity.this.itemValue[position];
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.useful_item, (ViewGroup) null);
                holder.textViewName = (TextView) convertView.findViewById(R.id.text_useful_name);
                holder.imageViewIcon = (ImageView) convertView.findViewById(R.id.image_useful_icon);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            switch (position) {
                case 0:
                    holder.textViewName.setText(UsefulActivity.this.itemName[0]);
                    holder.imageViewIcon.setImageResource(R.drawable.wanggou);
                    break;
                case DataDefine.DB_VERSION /*1*/:
                    holder.textViewName.setText(UsefulActivity.this.itemName[1]);
                    holder.imageViewIcon.setImageResource(R.drawable.tianqi);
                    break;
                case 2:
                    holder.textViewName.setText(UsefulActivity.this.itemName[2]);
                    holder.imageViewIcon.setImageResource(R.drawable.huafei);
                    break;
                case 3:
                    holder.textViewName.setText(UsefulActivity.this.itemName[3]);
                    holder.imageViewIcon.setImageResource(R.drawable.qbi);
                    break;
                case 4:
                    holder.textViewName.setText(UsefulActivity.this.itemName[4]);
                    holder.imageViewIcon.setImageResource(R.drawable.caipiao);
                    break;
                case 5:
                    holder.textViewName.setText(UsefulActivity.this.itemName[5]);
                    holder.imageViewIcon.setImageResource(R.drawable.feiji);
                    break;
                case 6:
                    holder.textViewName.setText(UsefulActivity.this.itemName[6]);
                    holder.imageViewIcon.setImageResource(R.drawable.longli);
                    break;
                case 7:
                    holder.textViewName.setText(UsefulActivity.this.itemName[7]);
                    holder.imageViewIcon.setImageResource(R.drawable.gupiao);
                    break;
                case 8:
                    holder.textViewName.setText(UsefulActivity.this.itemName[8]);
                    holder.imageViewIcon.setImageResource(R.drawable.xiazai);
                    break;
            }
            return convertView;
        }
    }
}
