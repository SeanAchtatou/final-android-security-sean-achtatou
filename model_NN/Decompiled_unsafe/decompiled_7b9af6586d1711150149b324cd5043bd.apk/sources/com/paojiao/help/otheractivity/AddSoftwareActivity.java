package com.paojiao.help.otheractivity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.R;
import com.paojiao.help.bean.ApplicationInfo;
import com.paojiao.help.data.DataProvider;
import com.paojiao.help.data.DataSave;
import com.paojiao.help.util.DataDefine;
import java.util.ArrayList;
import java.util.List;

public class AddSoftwareActivity extends Activity {
    private static final int MESSAGE_FOR_LOAD = 3;
    private static final int MESSAGE_FOR_PROGRESS = 2;
    private static final int STATE_DONE = 0;
    private static final int STATE_RUNNING = 1;
    public static int getAppNum = STATE_DONE;
    /* access modifiers changed from: private */
    public MyAdapter adapter = null;
    /* access modifiers changed from: private */
    public ArrayList<ApplicationInfo> appList = null;
    /* access modifiers changed from: private */
    public List<ResolveInfo> apps = null;
    /* access modifiers changed from: private */
    public String dbTable = null;
    /* access modifiers changed from: private */
    public boolean[] mCheckBoxs = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOff = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOn = null;
    /* access modifiers changed from: private */
    public int mState = STATE_DONE;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private ProgressThread progressThread = null;
    /* access modifiers changed from: private */
    public int totalAppNum;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.list_view_and_button);
        this.dbTable = getIntent().getExtras().getString(DataDefine.INTENT_TB);
        this.apps = DataProvider.getAllApplicationsNum(this);
        if (this.apps != null) {
            this.totalAppNum = this.apps.size();
            showDialog(101);
            new Thread() {
                public void run() {
                    MyHandler myHandler = new MyHandler(Looper.getMainLooper());
                    AddSoftwareActivity.this.appList = DataProvider.getAllApplications(AddSoftwareActivity.this.apps, AddSoftwareActivity.this.getPackageManager());
                    AddSoftwareActivity.this.mCheckBoxs = new boolean[AddSoftwareActivity.this.appList.size()];
                    AddSoftwareActivity.this.apps = null;
                    myHandler.sendMessage(myHandler.obtainMessage(AddSoftwareActivity.MESSAGE_FOR_LOAD));
                }
            }.start();
        }
        this.mCheckOn = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_on);
        this.mCheckOff = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_off);
        ListView lv = (ListView) findViewById(R.id.list);
        this.adapter = new MyAdapter(this);
        lv.setAdapter((ListAdapter) this.adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                AddSoftwareActivity.this.mCheckBoxs[position] = !AddSoftwareActivity.this.mCheckBoxs[position];
                ImageView mImageView = (ImageView) view.findViewById(R.id.image_check);
                if (AddSoftwareActivity.this.mCheckBoxs[position]) {
                    mImageView.setImageBitmap(AddSoftwareActivity.this.mCheckOn);
                } else {
                    mImageView.setImageBitmap(AddSoftwareActivity.this.mCheckOff);
                }
            }
        });
        Button mButton = (Button) findViewById(R.id.button);
        mButton.setText(getString(R.string.add));
        mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AddSoftwareActivity.this.appList != null) {
                    DataSave.saveAppData(AddSoftwareActivity.this, AddSoftwareActivity.this.appList, AddSoftwareActivity.this.mCheckBoxs, AddSoftwareActivity.this.dbTable);
                }
                AddSoftwareActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 101:
                this.progressDialog = new ProgressDialog(this);
                this.progressDialog.setProgressStyle(1);
                this.progressDialog.setMessage(getString(R.string.dialog_progress_app));
                this.progressDialog.setMax(this.totalAppNum);
                this.progressDialog.setButton(getText(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        AddSoftwareActivity.getAppNum = AddSoftwareActivity.this.totalAppNum;
                        AddSoftwareActivity.this.mState = AddSoftwareActivity.STATE_DONE;
                    }
                });
                this.progressThread = new ProgressThread(this, null);
                this.progressThread.start();
                return this.progressDialog;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.appList != null) {
            this.appList.clear();
        }
        if (this.mCheckOn != null && !this.mCheckOn.isRecycled()) {
            this.mCheckOn.recycle();
        }
        if (this.mCheckOff != null && !this.mCheckOff.isRecycled()) {
            this.mCheckOff.recycle();
        }
        super.onDestroy();
    }

    private class ProgressThread extends Thread {
        private ProgressThread() {
        }

        /* synthetic */ ProgressThread(AddSoftwareActivity addSoftwareActivity, ProgressThread progressThread) {
            this();
        }

        public void run() {
            AddSoftwareActivity.this.mState = 1;
            MyHandler myHandler = new MyHandler(Looper.getMainLooper());
            while (AddSoftwareActivity.this.mState == 1) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    Log.e("ERROR", "Thread Interrupted");
                }
                myHandler.sendMessage(myHandler.obtainMessage(AddSoftwareActivity.MESSAGE_FOR_PROGRESS));
            }
        }
    }

    private class MyHandler extends Handler {
        public MyHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case AddSoftwareActivity.MESSAGE_FOR_PROGRESS /*2*/:
                    if (AddSoftwareActivity.this.totalAppNum <= AddSoftwareActivity.getAppNum) {
                        AddSoftwareActivity.this.progressDialog.setMessage(AddSoftwareActivity.this.getString(R.string.dialog_progress_app));
                        AddSoftwareActivity.this.mState = AddSoftwareActivity.STATE_DONE;
                        AddSoftwareActivity.this.progressDialog.dismiss();
                        return;
                    }
                    AddSoftwareActivity.this.progressDialog.setProgress(AddSoftwareActivity.getAppNum);
                    return;
                case AddSoftwareActivity.MESSAGE_FOR_LOAD /*3*/:
                    AddSoftwareActivity.this.progressDialog.dismiss();
                    AddSoftwareActivity.this.adapter.notifyDataSetChanged();
                    AddSoftwareActivity.getAppNum = AddSoftwareActivity.STATE_DONE;
                    return;
                default:
                    return;
            }
        }
    }

    public final class ViewHolder {
        public ImageView checkImageView;
        public ImageView iconImageView;
        public TextView nameTextView;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return AddSoftwareActivity.this.appList != null ? AddSoftwareActivity.this.appList.size() : AddSoftwareActivity.STATE_DONE;
        }

        public Object getItem(int position) {
            return AddSoftwareActivity.this.appList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.software_item_select, (ViewGroup) null);
                holder.iconImageView = (ImageView) convertView.findViewById(R.id.image_software_icon);
                holder.nameTextView = (TextView) convertView.findViewById(R.id.text_software_name);
                holder.checkImageView = (ImageView) convertView.findViewById(R.id.image_check);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.iconImageView.setImageDrawable(((ApplicationInfo) AddSoftwareActivity.this.appList.get(position)).icon);
            holder.nameTextView.setText(((ApplicationInfo) AddSoftwareActivity.this.appList.get(position)).name);
            if (AddSoftwareActivity.this.mCheckBoxs[position]) {
                holder.checkImageView.setImageBitmap(AddSoftwareActivity.this.mCheckOn);
            } else {
                holder.checkImageView.setImageBitmap(AddSoftwareActivity.this.mCheckOff);
            }
            return convertView;
        }
    }
}
