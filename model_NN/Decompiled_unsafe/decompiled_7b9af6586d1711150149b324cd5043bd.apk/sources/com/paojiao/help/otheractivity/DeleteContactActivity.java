package com.paojiao.help.otheractivity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.R;
import com.paojiao.help.data.DataDelete;
import com.paojiao.help.data.DataProvider;
import com.paojiao.help.data.DataProvider7;
import com.paojiao.help.util.DataDefine;
import java.util.ArrayList;
import java.util.HashMap;

public class DeleteContactActivity extends Activity {
    private MyAdapter adapter = null;
    /* access modifiers changed from: private */
    public String dbTable = null;
    /* access modifiers changed from: private */
    public boolean[] mCheckBoxs = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOff = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOn = null;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> phonesList = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dbTable = getIntent().getExtras().getString(DataDefine.INTENT_TB);
        if (Integer.parseInt(Build.VERSION.SDK) < 5) {
            this.phonesList = DataProvider.getPhoneNumber(this, this.dbTable);
        } else {
            this.phonesList = DataProvider7.getPhoneNumber(this, this.dbTable);
        }
        this.mCheckBoxs = new boolean[this.phonesList.size()];
        this.mCheckOn = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_on);
        this.mCheckOff = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_off);
        setContentView((int) R.layout.list_view_and_button);
        ListView lv = (ListView) findViewById(R.id.list);
        this.adapter = new MyAdapter(this);
        lv.setAdapter((ListAdapter) this.adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                DeleteContactActivity.this.mCheckBoxs[position] = !DeleteContactActivity.this.mCheckBoxs[position];
                ImageView mImageView = (ImageView) view.findViewById(R.id.image_check);
                if (DeleteContactActivity.this.mCheckBoxs[position]) {
                    mImageView.setImageBitmap(DeleteContactActivity.this.mCheckOn);
                } else {
                    mImageView.setImageBitmap(DeleteContactActivity.this.mCheckOff);
                }
            }
        });
        Button mButton = (Button) findViewById(R.id.button);
        mButton.setText(getString(R.string.delete));
        mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DataDelete.deleteDataOfContact(DeleteContactActivity.this, DeleteContactActivity.this.phonesList, DeleteContactActivity.this.mCheckBoxs, DeleteContactActivity.this.dbTable);
                DeleteContactActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.phonesList.clear();
        this.mCheckOn.recycle();
        this.mCheckOff.recycle();
        super.onDestroy();
    }

    public final class ViewHolder {
        public ImageView checkImage;
        public ImageView headImage;
        public TextView nameTextView;
        public TextView numberTextView;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return DeleteContactActivity.this.phonesList.size();
        }

        public Object getItem(int position) {
            return DeleteContactActivity.this.phonesList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.contact_item_select, (ViewGroup) null);
                holder.nameTextView = (TextView) convertView.findViewById(R.id.text_contact_name);
                holder.numberTextView = (TextView) convertView.findViewById(R.id.text_contact_number);
                holder.checkImage = (ImageView) convertView.findViewById(R.id.image_check);
                holder.headImage = (ImageView) convertView.findViewById(R.id.image_contact_head);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            HashMap<String, Object> mHashMap = (HashMap) DeleteContactActivity.this.phonesList.get(position);
            holder.nameTextView.setText((String) mHashMap.get("name"));
            holder.numberTextView.setText((String) mHashMap.get("number"));
            Bitmap mBitmap = (Bitmap) mHashMap.get(DataDefine.PHONELIST_HEAD_PHOTO);
            if (mBitmap == null) {
                holder.headImage.setImageResource(R.drawable.ic_contact_picture);
            } else {
                holder.headImage.setImageBitmap(mBitmap);
            }
            if (DeleteContactActivity.this.mCheckBoxs[position]) {
                holder.checkImage.setImageBitmap(DeleteContactActivity.this.mCheckOn);
            } else {
                holder.checkImage.setImageBitmap(DeleteContactActivity.this.mCheckOff);
            }
            return convertView;
        }
    }
}
