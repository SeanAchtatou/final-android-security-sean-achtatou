package com.paojiao.help.otheractivity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.R;
import com.paojiao.help.data.DataProvider;
import com.paojiao.help.data.DataProvider7;
import com.paojiao.help.data.DataSave;
import com.paojiao.help.util.DataDefine;
import java.util.ArrayList;
import java.util.HashMap;

public class AddContactActivity extends Activity {
    private static final int MESSAGE_FOR_LOAD = 3;
    private static final int MESSAGE_FOR_PROGRESS = 2;
    private static final int STATE_DONE = 0;
    private static final int STATE_RUNNING = 1;
    public static int getContactNum = STATE_DONE;
    /* access modifiers changed from: private */
    public MyAdapter adapter = null;
    /* access modifiers changed from: private */
    public Cursor c = null;
    /* access modifiers changed from: private */
    public String dbTable = null;
    /* access modifiers changed from: private */
    public boolean[] mCheckBoxs = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOff = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOn = null;
    /* access modifiers changed from: private */
    public int mState = STATE_DONE;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> phonesList = null;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    private ProgressThread progressThread = null;
    /* access modifiers changed from: private */
    public int skdVersion = 7;
    /* access modifiers changed from: private */
    public int totalContactNum;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.list_view_and_button);
        this.dbTable = getIntent().getExtras().getString(DataDefine.INTENT_TB);
        this.skdVersion = Integer.parseInt(Build.VERSION.SDK);
        if (this.skdVersion < 5) {
            this.c = DataProvider.getCursorFromContacts(this);
        } else {
            this.c = DataProvider7.getCursorFromContacts(this);
        }
        if (this.c != null) {
            this.totalContactNum = this.c.getCount();
            showDialog(101);
            if (this.totalContactNum > 0) {
                new Thread() {
                    public void run() {
                        MyHandler myHandler = new MyHandler(Looper.getMainLooper());
                        if (AddContactActivity.this.skdVersion < 5) {
                            AddContactActivity.this.phonesList = DataProvider.getAllPhoneNumberFromContacts(AddContactActivity.this.c);
                        } else {
                            AddContactActivity.this.phonesList = DataProvider7.getAllPhoneNumberFromContacts(AddContactActivity.this, AddContactActivity.this.c);
                        }
                        AddContactActivity.this.mCheckBoxs = new boolean[AddContactActivity.this.phonesList.size()];
                        myHandler.sendMessage(myHandler.obtainMessage(AddContactActivity.MESSAGE_FOR_LOAD));
                    }
                }.start();
            }
        }
        this.mCheckOn = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_on);
        this.mCheckOff = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_off);
        ListView lv = (ListView) findViewById(R.id.list);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                AddContactActivity.this.mCheckBoxs[position] = !AddContactActivity.this.mCheckBoxs[position];
                ImageView mImageView = (ImageView) view.findViewById(R.id.image_check);
                if (AddContactActivity.this.mCheckBoxs[position]) {
                    mImageView.setImageBitmap(AddContactActivity.this.mCheckOn);
                } else {
                    mImageView.setImageBitmap(AddContactActivity.this.mCheckOff);
                }
            }
        });
        Button mButton = (Button) findViewById(R.id.button);
        mButton.setText(getString(R.string.add));
        mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AddContactActivity.this.phonesList != null) {
                    DataSave.savePhoneNumber(AddContactActivity.this, AddContactActivity.this.phonesList, AddContactActivity.this.mCheckBoxs, AddContactActivity.this.dbTable);
                }
                AddContactActivity.this.finish();
            }
        });
        this.adapter = new MyAdapter(this);
        lv.setAdapter((ListAdapter) this.adapter);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.phonesList != null) {
            this.phonesList.clear();
        }
        if (this.mCheckOn != null && !this.mCheckOn.isRecycled()) {
            this.mCheckOn.recycle();
        }
        if (this.mCheckOff != null && !this.mCheckOff.isRecycled()) {
            this.mCheckOff.recycle();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 101:
                this.progressDialog = new ProgressDialog(this);
                this.progressDialog.setProgressStyle(1);
                this.progressDialog.setMessage(getString(R.string.dialog_contact_app));
                this.progressDialog.setMax(this.totalContactNum);
                this.progressDialog.setButton(getText(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        AddContactActivity.getContactNum = AddContactActivity.this.totalContactNum;
                        AddContactActivity.this.mState = AddContactActivity.STATE_DONE;
                    }
                });
                this.progressThread = new ProgressThread(this, null);
                this.progressThread.start();
                return this.progressDialog;
            default:
                return null;
        }
    }

    private class ProgressThread extends Thread {
        private ProgressThread() {
        }

        /* synthetic */ ProgressThread(AddContactActivity addContactActivity, ProgressThread progressThread) {
            this();
        }

        public void run() {
            AddContactActivity.this.mState = 1;
            MyHandler myHandler = new MyHandler(Looper.getMainLooper());
            while (AddContactActivity.this.mState == 1) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    Log.e("ERROR", "Thread Interrupted");
                }
                myHandler.sendMessage(myHandler.obtainMessage(AddContactActivity.MESSAGE_FOR_PROGRESS));
            }
        }
    }

    private class MyHandler extends Handler {
        public MyHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case AddContactActivity.MESSAGE_FOR_PROGRESS /*2*/:
                    if (AddContactActivity.this.totalContactNum <= AddContactActivity.getContactNum) {
                        AddContactActivity.this.mState = AddContactActivity.STATE_DONE;
                        AddContactActivity.this.progressDialog.setProgress(AddContactActivity.this.totalContactNum);
                        return;
                    }
                    AddContactActivity.this.progressDialog.setProgress(AddContactActivity.getContactNum);
                    return;
                case AddContactActivity.MESSAGE_FOR_LOAD /*3*/:
                    AddContactActivity.this.progressDialog.dismiss();
                    AddContactActivity.this.adapter.notifyDataSetChanged();
                    AddContactActivity.getContactNum = AddContactActivity.STATE_DONE;
                    return;
                default:
                    return;
            }
        }
    }

    public final class ViewHolder {
        public ImageView checkImage;
        public ImageView headImage;
        public TextView nameTextView;
        public TextView numberTextView;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return AddContactActivity.this.phonesList != null ? AddContactActivity.this.phonesList.size() : AddContactActivity.STATE_DONE;
        }

        public Object getItem(int position) {
            return AddContactActivity.this.phonesList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.contact_item_select, (ViewGroup) null);
                holder.nameTextView = (TextView) convertView.findViewById(R.id.text_contact_name);
                holder.numberTextView = (TextView) convertView.findViewById(R.id.text_contact_number);
                holder.checkImage = (ImageView) convertView.findViewById(R.id.image_check);
                holder.headImage = (ImageView) convertView.findViewById(R.id.image_contact_head);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            HashMap<String, Object> mHashMap = (HashMap) AddContactActivity.this.phonesList.get(position);
            holder.nameTextView.setText((String) mHashMap.get("name"));
            holder.numberTextView.setText((String) mHashMap.get("number"));
            Bitmap mBitmap = (Bitmap) mHashMap.get(DataDefine.PHONELIST_HEAD_PHOTO);
            if (mBitmap == null) {
                holder.headImage.setImageResource(R.drawable.ic_contact_picture);
            } else {
                holder.headImage.setImageBitmap(mBitmap);
            }
            if (AddContactActivity.this.mCheckBoxs[position]) {
                holder.checkImage.setImageBitmap(AddContactActivity.this.mCheckOn);
            } else {
                holder.checkImage.setImageBitmap(AddContactActivity.this.mCheckOff);
            }
            return convertView;
        }
    }
}
