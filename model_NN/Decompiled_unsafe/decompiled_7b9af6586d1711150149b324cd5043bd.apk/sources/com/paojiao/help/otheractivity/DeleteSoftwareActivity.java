package com.paojiao.help.otheractivity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.R;
import com.paojiao.help.bean.ApplicationInfo;
import com.paojiao.help.data.DataDelete;
import com.paojiao.help.data.DataProvider;
import com.paojiao.help.util.DataDefine;
import java.util.ArrayList;

public class DeleteSoftwareActivity extends Activity {
    private MyAdapter adapter = null;
    /* access modifiers changed from: private */
    public ArrayList<ApplicationInfo> appList = null;
    /* access modifiers changed from: private */
    public String dbTable = null;
    /* access modifiers changed from: private */
    public boolean[] mCheckBoxs = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOff = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOn = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.dbTable = getIntent().getExtras().getString(DataDefine.INTENT_TB);
        this.appList = DataProvider.getSavedApplications(this, this.dbTable);
        this.mCheckBoxs = new boolean[this.appList.size()];
        this.mCheckOn = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_on);
        this.mCheckOff = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_off);
        setContentView((int) R.layout.list_view_and_button);
        ListView lv = (ListView) findViewById(R.id.list);
        this.adapter = new MyAdapter(this);
        lv.setAdapter((ListAdapter) this.adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                DeleteSoftwareActivity.this.mCheckBoxs[position] = !DeleteSoftwareActivity.this.mCheckBoxs[position];
                ImageView mImageView = (ImageView) view.findViewById(R.id.image_check);
                if (DeleteSoftwareActivity.this.mCheckBoxs[position]) {
                    mImageView.setImageBitmap(DeleteSoftwareActivity.this.mCheckOn);
                } else {
                    mImageView.setImageBitmap(DeleteSoftwareActivity.this.mCheckOff);
                }
            }
        });
        Button mButton = (Button) findViewById(R.id.button);
        mButton.setText(getString(R.string.delete));
        mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DataDelete.deleteDataOfApp(DeleteSoftwareActivity.this, DeleteSoftwareActivity.this.appList, DeleteSoftwareActivity.this.mCheckBoxs, DeleteSoftwareActivity.this.dbTable);
                DeleteSoftwareActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.appList.clear();
        this.mCheckOn.recycle();
        this.mCheckOff.recycle();
        super.onDestroy();
    }

    public final class ViewHolder {
        public ImageView checkImageView;
        public ImageView iconImageView;
        public TextView nameTextView;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return DeleteSoftwareActivity.this.appList.size();
        }

        public Object getItem(int position) {
            return DeleteSoftwareActivity.this.appList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.software_item_select, (ViewGroup) null);
                holder.iconImageView = (ImageView) convertView.findViewById(R.id.image_software_icon);
                holder.nameTextView = (TextView) convertView.findViewById(R.id.text_software_name);
                holder.checkImageView = (ImageView) convertView.findViewById(R.id.image_check);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.iconImageView.setImageDrawable(((ApplicationInfo) DeleteSoftwareActivity.this.appList.get(position)).icon);
            holder.nameTextView.setText(((ApplicationInfo) DeleteSoftwareActivity.this.appList.get(position)).name);
            if (DeleteSoftwareActivity.this.mCheckBoxs[position]) {
                holder.checkImageView.setImageBitmap(DeleteSoftwareActivity.this.mCheckOn);
            } else {
                holder.checkImageView.setImageBitmap(DeleteSoftwareActivity.this.mCheckOff);
            }
            return convertView;
        }
    }
}
