package com.paojiao.help.otheractivity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.R;
import com.paojiao.help.data.DataDelete;
import com.paojiao.help.data.DataProvider;
import com.paojiao.help.util.DataDefine;
import java.util.ArrayList;
import java.util.HashMap;

public class DeleteWebSiteActivity extends Activity {
    private MyAdapter adapter = null;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> bookmarksList = null;
    /* access modifiers changed from: private */
    public boolean[] mCheckBoxs = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOff = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOn = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.bookmarksList = DataProvider.getSavedWebsite(this, "website");
        this.mCheckBoxs = new boolean[this.bookmarksList.size()];
        this.mCheckOn = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_on);
        this.mCheckOff = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_off);
        setContentView((int) R.layout.list_view_and_button);
        ListView lv = (ListView) findViewById(R.id.list);
        this.adapter = new MyAdapter(this);
        lv.setAdapter((ListAdapter) this.adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                DeleteWebSiteActivity.this.mCheckBoxs[position] = !DeleteWebSiteActivity.this.mCheckBoxs[position];
                ImageView mImageView = (ImageView) view.findViewById(R.id.image_check);
                if (DeleteWebSiteActivity.this.mCheckBoxs[position]) {
                    mImageView.setImageBitmap(DeleteWebSiteActivity.this.mCheckOn);
                } else {
                    mImageView.setImageBitmap(DeleteWebSiteActivity.this.mCheckOff);
                }
            }
        });
        Button mButton = (Button) findViewById(R.id.button);
        mButton.setText(getString(R.string.delete));
        mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DataDelete.deleteDataOfWebsite(DeleteWebSiteActivity.this, DeleteWebSiteActivity.this.bookmarksList, DeleteWebSiteActivity.this.mCheckBoxs, "website");
                DeleteWebSiteActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.bookmarksList.clear();
        this.mCheckOn.recycle();
        this.mCheckOff.recycle();
        super.onDestroy();
    }

    public final class ViewHolder {
        public ImageView mImageView;
        public TextView nameTextView;
        public TextView orderTextView;
        public TextView urlTextView;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return DeleteWebSiteActivity.this.bookmarksList.size();
        }

        public Object getItem(int position) {
            return DeleteWebSiteActivity.this.bookmarksList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.website_item_select, (ViewGroup) null);
                holder.orderTextView = (TextView) convertView.findViewById(R.id.text_order_number);
                holder.nameTextView = (TextView) convertView.findViewById(R.id.text_website_name);
                holder.urlTextView = (TextView) convertView.findViewById(R.id.text_website_url);
                holder.mImageView = (ImageView) convertView.findViewById(R.id.image_check);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.orderTextView.setText(String.valueOf(position + 1) + ".");
            holder.nameTextView.setText((String) ((HashMap) DeleteWebSiteActivity.this.bookmarksList.get(position)).get("name"));
            holder.urlTextView.setText((String) ((HashMap) DeleteWebSiteActivity.this.bookmarksList.get(position)).get(DataDefine.WEBSITE_URL));
            if (DeleteWebSiteActivity.this.mCheckBoxs[position]) {
                holder.mImageView.setImageBitmap(DeleteWebSiteActivity.this.mCheckOn);
            } else {
                holder.mImageView.setImageBitmap(DeleteWebSiteActivity.this.mCheckOff);
            }
            return convertView;
        }
    }
}
