package com.paojiao.help.otheractivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.R;
import com.paojiao.help.data.DataProvider;
import com.paojiao.help.data.DataSave;
import com.paojiao.help.util.DataDefine;
import java.util.ArrayList;
import java.util.HashMap;

public class AddWebSiteActivity extends Activity {
    /* access modifiers changed from: private */
    public MyAdapter adapter = null;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> bookmarksList = null;
    /* access modifiers changed from: private */
    public boolean[] mCheckBoxs = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOff = null;
    /* access modifiers changed from: private */
    public Bitmap mCheckOn = null;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.list_view_and_button);
        setProgressDialog();
        this.mProgressDialog.show();
        new Thread() {
            public void run() {
                MyHandler myHandler = new MyHandler(Looper.getMainLooper());
                AddWebSiteActivity.this.bookmarksList = DataProvider.getBookmarksFromBrowser(AddWebSiteActivity.this);
                AddWebSiteActivity.this.mCheckBoxs = new boolean[AddWebSiteActivity.this.bookmarksList.size()];
                myHandler.removeMessages(0);
                myHandler.sendMessage(myHandler.obtainMessage(0));
            }
        }.start();
        this.mCheckOn = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_on);
        this.mCheckOff = BitmapFactory.decodeResource(getResources(), R.drawable.btn_check_off);
        ListView lv = (ListView) findViewById(R.id.list);
        this.adapter = new MyAdapter(this);
        lv.setAdapter((ListAdapter) this.adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                AddWebSiteActivity.this.mCheckBoxs[position] = !AddWebSiteActivity.this.mCheckBoxs[position];
                ImageView mImageView = (ImageView) view.findViewById(R.id.image_check);
                if (AddWebSiteActivity.this.mCheckBoxs[position]) {
                    mImageView.setImageBitmap(AddWebSiteActivity.this.mCheckOn);
                } else {
                    mImageView.setImageBitmap(AddWebSiteActivity.this.mCheckOff);
                }
            }
        });
        Button mButton = (Button) findViewById(R.id.button);
        mButton.setText(getString(R.string.add));
        mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (AddWebSiteActivity.this.bookmarksList != null) {
                    DataSave.saveWebsites(AddWebSiteActivity.this, AddWebSiteActivity.this.bookmarksList, AddWebSiteActivity.this.mCheckBoxs, "website");
                }
                AddWebSiteActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.bookmarksList != null) {
            this.bookmarksList.clear();
        }
        if (this.mCheckOn != null && !this.mCheckOn.isRecycled()) {
            this.mCheckOn.recycle();
        }
        if (this.mCheckOff != null && !this.mCheckOff.isRecycled()) {
            this.mCheckOff.recycle();
        }
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.cancel();
            this.mProgressDialog = null;
        }
        super.onDestroy();
    }

    private void setProgressDialog() {
        this.mProgressDialog = new ProgressDialog(this);
        this.mProgressDialog.setMessage(getString(R.string.dialog_wait_hint));
        this.mProgressDialog.setIndeterminate(true);
        this.mProgressDialog.setCancelable(true);
    }

    class MyHandler extends Handler {
        public MyHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    AddWebSiteActivity.this.adapter.notifyDataSetChanged();
                    if (AddWebSiteActivity.this.mProgressDialog != null && AddWebSiteActivity.this.mProgressDialog.isShowing()) {
                        AddWebSiteActivity.this.mProgressDialog.cancel();
                        break;
                    }
            }
            super.handleMessage(msg);
        }
    }

    public final class ViewHolder {
        public ImageView mImageView;
        public TextView nameTextView;
        public TextView orderTextView;
        public TextView urlTextView;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            if (AddWebSiteActivity.this.bookmarksList != null) {
                return AddWebSiteActivity.this.bookmarksList.size();
            }
            return 0;
        }

        public Object getItem(int position) {
            return AddWebSiteActivity.this.bookmarksList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.website_item_select, (ViewGroup) null);
                holder.orderTextView = (TextView) convertView.findViewById(R.id.text_order_number);
                holder.nameTextView = (TextView) convertView.findViewById(R.id.text_website_name);
                holder.urlTextView = (TextView) convertView.findViewById(R.id.text_website_url);
                holder.mImageView = (ImageView) convertView.findViewById(R.id.image_check);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.orderTextView.setText(String.valueOf(position + 1) + ".");
            holder.nameTextView.setText((String) ((HashMap) AddWebSiteActivity.this.bookmarksList.get(position)).get("name"));
            holder.urlTextView.setText((String) ((HashMap) AddWebSiteActivity.this.bookmarksList.get(position)).get(DataDefine.WEBSITE_URL));
            if (AddWebSiteActivity.this.mCheckBoxs[position]) {
                holder.mImageView.setImageBitmap(AddWebSiteActivity.this.mCheckOn);
            } else {
                holder.mImageView.setImageBitmap(AddWebSiteActivity.this.mCheckOff);
            }
            return convertView;
        }
    }
}
