package com.paojiao.help.bean;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.drawable.Drawable;

public class ApplicationInfo {
    public String fullName;
    public Drawable icon;
    public String id;
    public Intent intent;
    public int launchFlags;
    public String name;
    public String packageName;

    public Intent getIntent() {
        if (this.intent == null) {
            this.intent = new Intent("android.intent.action.MAIN");
            this.intent.addCategory("android.intent.category.LAUNCHER");
            this.intent.setComponent(new ComponentName(this.packageName, this.fullName));
            this.intent.setFlags(this.launchFlags);
        }
        return this.intent;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApplicationInfo)) {
            return false;
        }
        ApplicationInfo that = (ApplicationInfo) o;
        return this.name.equals(that.name) && this.intent.getComponent().getClassName().equals(that.intent.getComponent().getClassName());
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.name != null) {
            result = this.name.hashCode();
        } else {
            result = 0;
        }
        String name2 = this.intent.getComponent().getClassName();
        int i2 = result * 31;
        if (name2 != null) {
            i = name2.hashCode();
        }
        return i2 + i;
    }
}
