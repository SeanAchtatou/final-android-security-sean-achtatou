package com.paojiao.help;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paojiao.help.bean.ApplicationInfo;
import com.paojiao.help.data.DataProvider;
import com.paojiao.help.otheractivity.AddSoftwareActivity;
import com.paojiao.help.otheractivity.DeleteSoftwareActivity;
import com.paojiao.help.util.DataDefine;
import com.paojiao.help.util.Util;
import java.util.ArrayList;

public class SoftwareActivity extends Activity {
    private MyAdapter adapter = null;
    /* access modifiers changed from: private */
    public ArrayList<ApplicationInfo> appList = null;
    private boolean isFirst = true;
    private ListView lv = null;
    private TextView noDataTextView = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.software_main);
        if (DataDefine.settings == null) {
            DataDefine.settings = getSharedPreferences(DataDefine.PREFS_NAME, 0);
        }
        Util.StartThreadDoToServer(this);
        this.appList = DataProvider.getSavedApplications(this, DataDefine.TB_NAME_APP);
        this.noDataTextView = (TextView) findViewById(R.id.text_no_software);
        this.lv = (ListView) findViewById(R.id.list_software);
        this.lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                SoftwareActivity.this.sentIntent(position);
            }
        });
        this.adapter = new MyAdapter(this);
        this.lv.setAdapter((ListAdapter) this.adapter);
        ImageView iconDownload = (ImageView) findViewById(R.id.image_icon_software);
        iconDownload.setImageResource(R.drawable.icon_software1);
        iconDownload.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SoftwareActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(SoftwareActivity.this.getString(R.string.url_android_software_download))));
                SoftwareActivity.this.finish();
            }
        });
        defineControlBar();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        if (!this.isFirst) {
            this.appList.clear();
            this.appList = DataProvider.getSavedApplications(this, DataDefine.TB_NAME_APP);
            this.adapter.notifyDataSetChanged();
        } else {
            this.isFirst = false;
        }
        if (this.appList.size() < 1) {
            this.noDataTextView.setVisibility(0);
        } else {
            this.noDataTextView.setVisibility(8);
        }
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.appList.clear();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DataDefine.DIALOG_IS_UPDATE_NOW /*100*/:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_is_update_now_title).setMessage((int) R.string.dialog_is_update_now_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String url = DataDefine.settings.getString(DataDefine.PREFS_ITME_UPDATE_URL, "");
                        if (!url.equals("")) {
                            SoftwareActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                            SoftwareActivity.this.finish();
                        }
                    }
                }).setNegativeButton((int) R.string.next_time, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.nextUpdate();
                    }
                }).create();
            case 101:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_recommend_title).setMessage((int) R.string.dialog_recommend_content).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Util.StartThreadDoToServer(SoftwareActivity.this);
                    }
                }).create();
            default:
                return super.onCreateDialog(id);
        }
    }

    public void sentIntent(int position) {
        startActivity(new Intent(this.appList.get(position).getIntent()));
    }

    private void defineControlBar() {
        ((ImageButton) findViewById(R.id.button_add_software)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mIntent = new Intent();
                mIntent.setClass(SoftwareActivity.this, AddSoftwareActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putCharSequence(DataDefine.INTENT_TB, DataDefine.TB_NAME_APP);
                mIntent.putExtras(mBundle);
                SoftwareActivity.this.startActivity(mIntent);
            }
        });
        ((ImageButton) findViewById(R.id.button_delete_software)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent mIntent = new Intent();
                mIntent.setClass(SoftwareActivity.this, DeleteSoftwareActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putCharSequence(DataDefine.INTENT_TB, DataDefine.TB_NAME_APP);
                mIntent.putExtras(mBundle);
                SoftwareActivity.this.startActivity(mIntent);
            }
        });
        ((ImageButton) findViewById(R.id.button_back_software)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoftwareActivity.this.finish();
            }
        });
    }

    public final class ViewHolder {
        public ImageView iconImageView;
        public TextView nameTextView;

        public ViewHolder() {
        }
    }

    public class MyAdapter extends BaseAdapter {
        private LayoutInflater mInflater;

        public MyAdapter(Context context) {
            this.mInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return SoftwareActivity.this.appList.size();
        }

        public Object getItem(int position) {
            return SoftwareActivity.this.appList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* Debug info: failed to restart local var, previous not found, register: 4 */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.software_item, (ViewGroup) null);
                holder.iconImageView = (ImageView) convertView.findViewById(R.id.image_icon);
                holder.nameTextView = (TextView) convertView.findViewById(R.id.text_name);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.iconImageView.setImageDrawable(((ApplicationInfo) SoftwareActivity.this.appList.get(position)).icon);
            holder.nameTextView.setText(((ApplicationInfo) SoftwareActivity.this.appList.get(position)).name);
            return convertView;
        }
    }
}
