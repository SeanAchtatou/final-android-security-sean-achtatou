package com.paojiao.help;

public final class R {

    public static final class array {
        public static final int select_mode = 2131034112;
        public static final int select_search_engine = 2131034113;
        public static final int select_search_engine_url = 2131034114;
        public static final int select_update_time_limit = 2131034115;
        public static final int select_update_time_limit_url = 2131034116;
        public static final int select_useful_item_name = 2131034117;
        public static final int select_useful_item_value = 2131034118;
        public static final int several_search_key_word = 2131034119;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int words_white_color = 2131165184;
    }

    public static final class dimen {
        public static final int configuration_button_padding = 2131099667;
        public static final int configuration_show_margin_right = 2131099668;
        public static final int configuration_text_margin = 2131099665;
        public static final int configuration_text_size = 2131099666;
        public static final int list_item_contact_head_image_height = 2131099661;
        public static final int list_item_contact_head_image_width = 2131099660;
        public static final int list_item_first_view_right_margin = 2131099652;
        public static final int list_item_padding = 2131099650;
        public static final int list_item_right_icon_padding_right = 2131099657;
        public static final int list_item_search_history_padding = 2131099651;
        public static final int list_item_search_history_size = 2131099655;
        public static final int list_item_set_padding = 2131099662;
        public static final int list_item_software_icon_size = 2131099658;
        public static final int list_item_software_margin = 2131099659;
        public static final int list_item_text_order_number_size = 2131099653;
        public static final int list_item_two_line_down_size = 2131099656;
        public static final int list_item_two_line_up_size = 2131099654;
        public static final int list_item_website_add_or_delete_padding = 2131099664;
        public static final int list_item_website_main_padding = 2131099663;
        public static final int no_data_hint_padding = 2131099649;
        public static final int no_data_hint_size = 2131099648;
    }

    public static final class drawable {
        public static final int all_websites_button = 2130837504;
        public static final int btn_check_off = 2130837505;
        public static final int btn_check_on = 2130837506;
        public static final int button_control_bar = 2130837507;
        public static final int button_icon_website = 2130837508;
        public static final int button_icon_website_press = 2130837509;
        public static final int button_no_press = 2130837510;
        public static final int button_press = 2130837511;
        public static final int caipiao = 2130837512;
        public static final int clearable_edit_text_image = 2130837513;
        public static final int control_bar = 2130837514;
        public static final int feiji = 2130837515;
        public static final int gupiao = 2130837516;
        public static final int history_key_word_icon = 2130837517;
        public static final int huafei = 2130837518;
        public static final int ic_contact_picture = 2130837519;
        public static final int icon = 2130837520;
        public static final int icon_add_call = 2130837521;
        public static final int icon_add_message = 2130837522;
        public static final int icon_add_software = 2130837523;
        public static final int icon_add_website = 2130837524;
        public static final int icon_back = 2130837525;
        public static final int icon_call = 2130837526;
        public static final int icon_call_no_contact = 2130837527;
        public static final int icon_delete = 2130837528;
        public static final int icon_list_call = 2130837529;
        public static final int icon_list_more = 2130837530;
        public static final int icon_list_sms = 2130837531;
        public static final int icon_sms = 2130837532;
        public static final int icon_software = 2130837533;
        public static final int icon_software1 = 2130837534;
        public static final int longli = 2130837535;
        public static final int qbi = 2130837536;
        public static final int quickcontact_badge_unpressed = 2130837537;
        public static final int search_button_clear_default = 2130837538;
        public static final int search_button_clear_pressed = 2130837539;
        public static final int search_icon_search = 2130837540;
        public static final int tianqi = 2130837541;
        public static final int wanggou = 2130837542;
        public static final int websites = 2130837543;
        public static final int widget1 = 2130837544;
        public static final int widget1_press = 2130837545;
        public static final int widget1_vertical = 2130837546;
        public static final int widget1_vertical_press = 2130837547;
        public static final int widget1_vertical_xml = 2130837548;
        public static final int widget1_xml = 2130837549;
        public static final int widget2 = 2130837550;
        public static final int widget2_press = 2130837551;
        public static final int widget2_vertical = 2130837552;
        public static final int widget2_vertical_press = 2130837553;
        public static final int widget2_vertical_xml = 2130837554;
        public static final int widget2_xml = 2130837555;
        public static final int widget3 = 2130837556;
        public static final int widget3_press = 2130837557;
        public static final int widget3_vertical = 2130837558;
        public static final int widget3_vertical_press = 2130837559;
        public static final int widget3_vertical_xml = 2130837560;
        public static final int widget3_xml = 2130837561;
        public static final int widget4 = 2130837562;
        public static final int widget4_press = 2130837563;
        public static final int widget4_vertical = 2130837564;
        public static final int widget4_vertical_press = 2130837565;
        public static final int widget4_vertical_xml = 2130837566;
        public static final int widget4_xml = 2130837567;
        public static final int widget5 = 2130837568;
        public static final int widget5_press = 2130837569;
        public static final int widget5_vertical = 2130837570;
        public static final int widget5_vertical_press = 2130837571;
        public static final int widget5_vertical_xml = 2130837572;
        public static final int widget5_xml = 2130837573;
        public static final int widget6 = 2130837574;
        public static final int widget6_press = 2130837575;
        public static final int widget6_vertical = 2130837576;
        public static final int widget6_vertical_press = 2130837577;
        public static final int widget6_vertical_xml = 2130837578;
        public static final int widget6_xml = 2130837579;
        public static final int widget_horizontal_background = 2130837580;
        public static final int widget_vertical_background = 2130837581;
        public static final int widget_vertical_background1 = 2130837582;
        public static final int xiazai = 2130837583;
    }

    public static final class id {
        public static final int button = 2131296294;
        public static final int button_add_call = 2131296286;
        public static final int button_add_software = 2131296309;
        public static final int button_add_websites = 2131296320;
        public static final int button_all_websites = 2131296317;
        public static final int button_back_call = 2131296288;
        public static final int button_back_software = 2131296311;
        public static final int button_back_websites = 2131296322;
        public static final int button_delete_call = 2131296287;
        public static final int button_delete_software = 2131296310;
        public static final int button_delete_websites = 2131296321;
        public static final int button_horizontal_save = 2131296263;
        public static final int button_search = 2131296298;
        public static final int button_vertival_save = 2131296265;
        public static final int clearable_button = 2131296297;
        public static final int clearable_edit_text = 2131296273;
        public static final int clearable_image_view = 2131296274;
        public static final int dialog_website = 2131296292;
        public static final int dialog_website_name = 2131296290;
        public static final int edittext_search = 2131296296;
        public static final int history_search_word = 2131296299;
        public static final int image_check = 2131296282;
        public static final int image_contact = 2131296283;
        public static final int image_contact_head = 2131296279;
        public static final int image_head = 2131296275;
        public static final int image_history_icon = 2131296300;
        public static final int image_icon = 2131296278;
        public static final int image_icon_software = 2131296306;
        public static final int image_set_icon = 2131296303;
        public static final int image_software_icon = 2131296304;
        public static final int image_useful_icon = 2131296312;
        public static final int list = 2131296293;
        public static final int list_call = 2131296285;
        public static final int list_set = 2131296295;
        public static final int list_software = 2131296308;
        public static final int list_websites = 2131296319;
        public static final int password_view = 2131296291;
        public static final int radio_group_horizontal = 2131296259;
        public static final int radio_group_vertical = 2131296264;
        public static final int rb1 = 2131296260;
        public static final int rb2 = 2131296261;
        public static final int rb3 = 2131296262;
        public static final int text_contact_name = 2131296280;
        public static final int text_contact_number = 2131296281;
        public static final int text_history_name = 2131296301;
        public static final int text_name = 2131296276;
        public static final int text_no_call = 2131296284;
        public static final int text_no_software = 2131296307;
        public static final int text_no_websites = 2131296318;
        public static final int text_number = 2131296277;
        public static final int text_order_number = 2131296314;
        public static final int text_set_name = 2131296302;
        public static final int text_software_name = 2131296305;
        public static final int text_useful_name = 2131296313;
        public static final int text_view_effect = 2131296256;
        public static final int text_view_select_effect = 2131296258;
        public static final int text_website_name = 2131296315;
        public static final int text_website_url = 2131296316;
        public static final int username_view = 2131296289;
        public static final int widget_call = 2131296270;
        public static final int widget_horizontal_background = 2131296257;
        public static final int widget_message = 2131296271;
        public static final int widget_search = 2131296267;
        public static final int widget_software = 2131296269;
        public static final int widget_useful = 2131296272;
        public static final int widget_vertical_background = 2131296266;
        public static final int widget_website = 2131296268;
    }

    public static final class layout {
        public static final int appwidget_configuration_horizontal = 2130903040;
        public static final int appwidget_configuration_vertical = 2130903041;
        public static final int appwidget_provider_horizontal = 2130903042;
        public static final int appwidget_provider_vertical = 2130903043;
        public static final int clearable_edit_text = 2130903044;
        public static final int contact_item = 2130903045;
        public static final int contact_item_select = 2130903046;
        public static final int contact_main = 2130903047;
        public static final int dialog_website = 2130903048;
        public static final int list_view_and_button = 2130903049;
        public static final int main = 2130903050;
        public static final int search = 2130903051;
        public static final int search_history_item = 2130903052;
        public static final int set_item = 2130903053;
        public static final int software_item = 2130903054;
        public static final int software_item_select = 2130903055;
        public static final int software_main = 2130903056;
        public static final int useful_item = 2130903057;
        public static final int website_item = 2130903058;
        public static final int website_item_select = 2130903059;
        public static final int website_main = 2130903060;
    }

    public static final class string {
        public static final int activity_configuration_name = 2131230734;
        public static final int activity_contact_add_name = 2131230731;
        public static final int activity_contact_delete_name = 2131230732;
        public static final int activity_contact_name = 2131230730;
        public static final int activity_search_title_name = 2131230723;
        public static final int activity_software_add_name = 2131230728;
        public static final int activity_software_delete_name = 2131230729;
        public static final int activity_software_name = 2131230727;
        public static final int activity_useful_name = 2131230733;
        public static final int activity_website_add_name = 2131230725;
        public static final int activity_website_delete_name = 2131230726;
        public static final int activity_website_name = 2131230724;
        public static final int add = 2131230737;
        public static final int app_name = 2131230720;
        public static final int button_call_contact = 2131230751;
        public static final int button_search = 2131230748;
        public static final int button_sms_contact = 2131230752;
        public static final int button_software = 2131230750;
        public static final int button_tools = 2131230753;
        public static final int button_website = 2131230749;
        public static final int call_no_data_hint = 2131230744;
        public static final int cancel = 2131230739;
        public static final int delete = 2131230738;
        public static final int dialog_contact_app = 2131230769;
        public static final int dialog_help_content = 2131230775;
        public static final int dialog_help_title = 2131230774;
        public static final int dialog_is_update_now_content = 2131230777;
        public static final int dialog_is_update_now_title = 2131230776;
        public static final int dialog_progress_app = 2131230768;
        public static final int dialog_recommend_content = 2131230779;
        public static final int dialog_recommend_title = 2131230778;
        public static final int dialog_search_help_content = 2131230773;
        public static final int dialog_search_help_title = 2131230772;
        public static final int dialog_select_search_engine_title = 2131230770;
        public static final int dialog_select_update_time_limit_title = 2131230771;
        public static final int dialog_wait_hint = 2131230747;
        public static final int dialog_website_name_text = 2131230754;
        public static final int dialog_website_select_mode_title_text = 2131230757;
        public static final int dialog_website_text = 2131230755;
        public static final int dialog_website_title_text = 2131230756;
        public static final int list_set_help = 2131230785;
        public static final int list_set_official = 2131230784;
        public static final int list_set_recommend = 2131230782;
        public static final int list_set_search = 2131230781;
        public static final int list_set_update = 2131230780;
        public static final int menu_search_delete_all_word = 2131230759;
        public static final int next_time = 2131230740;
        public static final int ok = 2131230741;
        public static final int save = 2131230742;
        public static final int search_channel = 2131230786;
        public static final int search_history_title = 2131230758;
        public static final int sms_no_data_hint = 2131230745;
        public static final int sms_recommend_content = 2131230783;
        public static final int software_no_data_hint = 2131230746;
        public static final int text_configuration_select = 2131230735;
        public static final int text_configuration_show = 2131230736;
        public static final int url_all_websites = 2131230761;
        public static final int url_android_software_download = 2131230766;
        public static final int url_android_software_download_list = 2131230767;
        public static final int url_first_connect_to_server = 2131230764;
        public static final int url_official = 2131230763;
        public static final int url_search = 2131230760;
        public static final int url_update = 2131230762;
        public static final int url_useful_front = 2131230765;
        public static final int website_no_data_hint = 2131230743;
        public static final int widget_name_horizontal = 2131230722;
        public static final int widget_name_vertical = 2131230721;
    }

    public static final class xml {
        public static final int widget_provider_info_horizontal = 2130968576;
        public static final int widget_provider_info_vertical = 2130968577;
    }
}
