package com.shoujiduoduo.b.f;

import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.CollectData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import java.util.Collection;

/* compiled from: UserRingListMgrImpl */
public class f implements c {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1394a;
    private boolean b;
    private b c;
    private a d;
    private d e;

    public void a() {
        this.c = new b();
        this.e = new d();
        this.d = new a();
        this.c.c();
        this.e.c();
        this.f1394a = true;
        this.b = false;
        c.a().a(b.OBSERVER_USER_RING, new c.a<w>() {
            public void a() {
                ((w) this.f1284a).a();
            }
        });
        e();
    }

    public void b() {
        this.c.d();
        this.e.d();
    }

    private void e() {
        this.c.b();
        this.e.b();
        this.d.b();
        this.f1394a = false;
        this.b = true;
    }

    public boolean c() {
        return this.c.a() && this.e.a() && this.d.a();
    }

    public boolean a(RingData ringData, String str) {
        if (str.equals("favorite_ring_list")) {
            return this.c.a(ringData);
        }
        if (str.equals("make_ring_list")) {
            if (ringData instanceof MakeRingData) {
                return this.e.a((MakeRingData) ringData);
            }
            a.c("UserRingListMgrImpl", "add make ring, but data is not makeringdata type");
        }
        return false;
    }

    public RingData a(String str) {
        if (this.e == null || this.e.size() <= 0) {
            return null;
        }
        return this.e.b(str);
    }

    public boolean a(String str, String str2) {
        if (str2.equals("favorite_ring_list")) {
            return this.c.a(str);
        }
        if (str2.equals("make_ring_list")) {
            return this.e.a(str);
        }
        return false;
    }

    public boolean a(String str, int i) {
        if (str.equals("favorite_ring_list")) {
            return this.c.a(i);
        }
        if (str.equals("make_ring_list")) {
            return this.e.a(i);
        }
        return false;
    }

    public DDList b(String str) {
        if (str.equals("favorite_ring_list")) {
            return this.c;
        }
        if (str.equals("make_ring_list")) {
            return this.e;
        }
        if (str.equals("collect_ring_list")) {
            return this.d;
        }
        return null;
    }

    public boolean a(CollectData collectData) {
        return this.d.a(collectData);
    }

    public DDList d() {
        return this.d;
    }

    public boolean a(String str, Collection<Integer> collection) {
        if (str.equals("make_ring_list")) {
            return this.e.a(collection);
        }
        if (str.equals("favorite_ring_list")) {
            return this.c.a(collection);
        }
        return false;
    }

    public boolean a(Collection<Integer> collection) {
        return this.d.a(collection);
    }
}
