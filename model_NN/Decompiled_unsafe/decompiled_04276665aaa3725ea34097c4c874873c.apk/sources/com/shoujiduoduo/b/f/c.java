package com.shoujiduoduo.b.f;

import com.shoujiduoduo.a.b.a;
import com.shoujiduoduo.base.bean.CollectData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.RingData;
import java.util.Collection;

/* compiled from: IUserListMgr */
public interface c extends a {
    RingData a(String str);

    boolean a(CollectData collectData);

    boolean a(RingData ringData, String str);

    boolean a(String str, int i);

    boolean a(String str, String str2);

    boolean a(String str, Collection<Integer> collection);

    boolean a(Collection<Integer> collection);

    DDList b(String str);

    boolean c();

    DDList d();
}
