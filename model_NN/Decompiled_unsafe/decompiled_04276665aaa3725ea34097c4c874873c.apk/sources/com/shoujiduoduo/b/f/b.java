package com.shoujiduoduo.b.f;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.m;
import com.shoujiduoduo.util.r;
import com.shoujiduoduo.util.u;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Element;

/* compiled from: FavoriteRingList */
public class b implements DDList {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1375a = m.b(3);
    private a b = new a(RingDDApp.c(), "duoduo.ringtone.database", null, 3);
    /* access modifiers changed from: private */
    public ArrayList<RingData> c = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<RingData> d = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public String f = "";
    private v g = new v() {
        public void a(int i, boolean z, String str, String str2) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user is login, success:" + z);
            if (z) {
                String f = com.shoujiduoduo.a.b.b.g().f();
                if (!TextUtils.isEmpty(f)) {
                    b.this.c(f);
                }
            }
        }

        public void a(int i) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user is logout");
            ArrayList unused = b.this.d = b.this.c;
            c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1284a).a(b.this, 0);
                }
            });
        }

        public void b(int i) {
        }

        public void a(String str, boolean z) {
        }

        public void a(String str) {
        }
    };

    public boolean a() {
        return this.e;
    }

    public void b() {
        com.shoujiduoduo.base.a.a.b("FavoriteRingList", "begin init favorite ring data");
        final boolean g2 = com.shoujiduoduo.a.b.b.g().g();
        final String f2 = com.shoujiduoduo.a.b.b.g().f();
        i.a(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.util.ArrayList, java.util.ArrayList, boolean):java.util.ArrayList
             arg types: [com.shoujiduoduo.b.f.b, java.util.ArrayList<T>, java.util.ArrayList, int]
             candidates:
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, org.w3c.dom.Element, java.lang.String, int):void
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, org.w3c.dom.Element, java.lang.String, java.lang.String):void
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.util.ArrayList, java.util.ArrayList, boolean):java.util.ArrayList */
            public void run() {
                final ArrayList a2 = b.this.f();
                ArrayList<T> arrayList = null;
                if (g2) {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user is in login state, get online ring from cache");
                    ListContent a3 = b.this.b(f2);
                    if (a3 != null) {
                        arrayList = a3.data;
                        b.this.a(a3.fadeRidMap, a2);
                    }
                }
                final ArrayList a4 = b.this.a((ArrayList) arrayList, a2, true);
                c.a().a(new c.b() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, boolean):boolean
                     arg types: [com.shoujiduoduo.b.f.b, int]
                     candidates:
                      com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.lang.String):com.shoujiduoduo.base.bean.ListContent
                      com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.util.ArrayList):java.util.ArrayList
                      com.shoujiduoduo.b.f.b.a(java.util.HashMap<java.lang.String, java.lang.String>, java.util.ArrayList<com.shoujiduoduo.base.bean.RingData>):void
                      com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, boolean):boolean */
                    public void a() {
                        if (a2 != null) {
                            ArrayList unused = b.this.c = a2;
                        }
                        if (a4 != null) {
                            ArrayList unused2 = b.this.d = a4;
                        }
                        boolean unused3 = b.this.e = true;
                        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, new c.a<w>() {
                            public void a() {
                                ((w) this.f1284a).a(0, null, "user_favorite");
                            }
                        });
                    }
                });
                if (g2) {
                    b.this.c(f2);
                }
            }
        });
        com.shoujiduoduo.base.a.a.b("FavoriteRingList", "end init favorite ring data");
    }

    public void c() {
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.g);
    }

    public void d() {
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.g);
    }

    public String getBaseURL() {
        return null;
    }

    public String getListId() {
        return "user_favorite";
    }

    /* access modifiers changed from: private */
    public ListContent<RingData> b(String str) {
        String str2 = m.a(2) + str + ".xml";
        File file = new File(str2);
        if (file.exists()) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user online ring cache exists, cache file:" + str2);
            try {
                ListContent<RingData> g2 = j.g(new FileInputStream(file));
                if (g2 != null) {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user online ring cache size:" + g2.data.size());
                    this.f = g2.sig;
                    return g2;
                }
                com.shoujiduoduo.base.a.a.c("FavoriteRingList", "parse user online ring cache failed!!");
            } catch (ArrayIndexOutOfBoundsException e2) {
                e2.printStackTrace();
            } catch (FileNotFoundException e3) {
                e3.printStackTrace();
            }
        } else {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user online ring cache not exists");
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void c(final String str) {
        i.a(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.util.ArrayList, java.util.ArrayList, boolean):java.util.ArrayList
             arg types: [com.shoujiduoduo.b.f.b, java.util.ArrayList<T>, java.util.ArrayList, int]
             candidates:
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, org.w3c.dom.Element, java.lang.String, int):void
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, org.w3c.dom.Element, java.lang.String, java.lang.String):void
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.util.ArrayList, java.util.ArrayList, boolean):java.util.ArrayList */
            public void run() {
                com.shoujiduoduo.base.a.a.a("FavoriteRingList", "get user online ring  begin");
                String e = u.e(b.this.f);
                if (ai.c(e)) {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "get user online ring error, return null");
                } else if ("no change".equals(e)) {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user favorite return no change");
                } else {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "save online ring to cache");
                    r.a(m.a(2) + str + ".xml", e.getBytes());
                    try {
                        ListContent<RingData> g = j.g(new ByteArrayInputStream(e.getBytes()));
                        if (g != null) {
                            synchronized ("FavoriteRingList") {
                                com.shoujiduoduo.base.a.a.a("FavoriteRingList", "merge user local ring with online data");
                                if (g.data.size() > 0) {
                                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "online ring num: " + g.data.size());
                                    b.this.a(g.fadeRidMap, b.this.c);
                                    final ArrayList a2 = b.this.a((ArrayList) g.data, b.this.c, false);
                                    c.a().a(new c.b() {
                                        public void a() {
                                            ArrayList unused = b.this.d = a2;
                                            c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c.a<g>() {
                                                public void a() {
                                                    ((g) this.f1284a).a(b.this, 0);
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "online ring num: 0");
                                }
                            }
                            return;
                        }
                        com.shoujiduoduo.base.a.a.c("FavoriteRingList", "Get user favorite rings failed!!");
                    } catch (ArrayIndexOutOfBoundsException e2) {
                        com.shoujiduoduo.base.a.a.c("FavoriteRingList", "Get user favorite rings failed!!, crash");
                        e2.printStackTrace();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(HashMap<String, String> hashMap, ArrayList<RingData> arrayList) {
        boolean z;
        if (hashMap != null && arrayList != null) {
            boolean z2 = false;
            if (hashMap.size() > 0 && arrayList.size() > 0) {
                Iterator<RingData> it = arrayList.iterator();
                while (true) {
                    z = z2;
                    if (!it.hasNext()) {
                        break;
                    }
                    RingData next = it.next();
                    if (hashMap.containsKey(next.rid)) {
                        String str = hashMap.get(next.rid);
                        com.shoujiduoduo.base.a.a.a("FavoriteRingList", "replace rid:" + next.rid + " to " + str);
                        next.rid = str;
                        z2 = true;
                    } else {
                        z2 = z;
                    }
                }
                z2 = z;
            }
            if (z2) {
                a(arrayList);
            }
        }
    }

    /* access modifiers changed from: private */
    public ArrayList<RingData> f() {
        com.shoujiduoduo.base.a.a.a("FavoriteRingList", "read Local ringlist begin");
        File file = new File(f1375a);
        if (file.exists()) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user favorite ring file exists, file:" + f1375a);
            try {
                ListContent<RingData> f2 = j.f(new FileInputStream(file));
                if (f2 != null) {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user local favorite ring size:" + f2.data.size());
                    com.shoujiduoduo.util.g.a(RingDDApp.c(), "COLLECT_RING_NUM_NEW", new HashMap(), (long) f2.data.size());
                    return f2.data;
                }
                com.shoujiduoduo.base.a.a.c("FavoriteRingList", "parse user local favorite ring failed!!");
                return null;
            } catch (ArrayIndexOutOfBoundsException e2) {
                e2.printStackTrace();
                return null;
            } catch (FileNotFoundException e3) {
                e3.printStackTrace();
                return null;
            }
        } else {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user favorite ring file not exists");
            return null;
        }
    }

    /* access modifiers changed from: private */
    public ArrayList<RingData> a(ArrayList<RingData> arrayList, ArrayList<RingData> arrayList2, boolean z) {
        com.shoujiduoduo.base.a.a.a("FavoriteRingList", "mergeRingdata begin, bFromCache:" + z);
        if (arrayList == null || arrayList.size() == 0) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "online list is null or size is 0, return localRinglist");
            return arrayList2;
        } else if (arrayList2 == null || arrayList2.size() == 0) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "localRinglist is null or size is 0, return online list");
            return arrayList;
        } else {
            HashMap hashMap = new HashMap();
            ArrayList<RingData> arrayList3 = new ArrayList<>();
            ArrayList arrayList4 = new ArrayList();
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "local ring size:" + arrayList2.size());
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "online ring size:" + arrayList.size());
            Iterator<RingData> it = arrayList.iterator();
            while (it.hasNext()) {
                RingData next = it.next();
                hashMap.put("" + next.getRid(), next);
            }
            arrayList3.addAll(arrayList);
            Iterator<RingData> it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                RingData next2 = it2.next();
                if (!hashMap.containsKey("" + next2.getRid())) {
                    arrayList3.add(next2);
                    arrayList4.add(next2);
                }
            }
            if (arrayList4.size() > 0 && !z) {
                StringBuilder sb = new StringBuilder();
                Iterator it3 = arrayList4.iterator();
                while (it3.hasNext()) {
                    sb.append(((RingData) it3.next()).rid + "|");
                }
                final String substring = sb.substring(0, sb.length() - 1);
                com.shoujiduoduo.base.a.a.a("FavoriteRingList", "本地有新增铃声，需要同步至服务器");
                com.shoujiduoduo.base.a.a.a("FavoriteRingList", "add user favorite, size:" + arrayList4.size() + ", rid:" + substring);
                i.a(new Runnable() {
                    public void run() {
                        String f = u.f(substring);
                        if (ai.c(f)) {
                            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "sync to server error, return null");
                        } else {
                            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "sync to server, return:" + f);
                        }
                    }
                });
            }
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "merge ring size:" + arrayList3.size());
            return arrayList3;
        }
    }

    public boolean a(String str) {
        synchronized ("FavoriteRingList") {
            for (int i = 0; i < this.d.size(); i++) {
                if (str.equalsIgnoreCase(this.d.get(i).rid)) {
                    return true;
                }
            }
            return false;
        }
    }

    private void a(final ArrayList<RingData> arrayList) {
        i.a(new Runnable() {
            /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r9 = this;
                    java.lang.String r2 = "FavoriteRingList"
                    monitor-enter(r2)
                    javax.xml.parsers.DocumentBuilderFactory r0 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    javax.xml.parsers.DocumentBuilder r0 = r0.newDocumentBuilder()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    org.w3c.dom.Document r3 = r0.newDocument()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r0 = "list"
                    org.w3c.dom.Element r4 = r3.createElement(r0)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r0 = "num"
                    java.util.ArrayList r1 = r2     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    int r1 = r1.size()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r4.setAttribute(r0, r1)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r3.appendChild(r4)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r0 = 0
                    r1 = r0
                L_0x0029:
                    java.util.ArrayList r0 = r2     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    int r0 = r0.size()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    if (r1 >= r0) goto L_0x0188
                    java.util.ArrayList r0 = r2     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.Object r0 = r0.get(r1)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.base.bean.RingData r0 = (com.shoujiduoduo.base.bean.RingData) r0     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r5 = "ring"
                    org.w3c.dom.Element r5 = r3.createElement(r5)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r6 = "name"
                    java.lang.String r7 = r0.name     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r5.setAttribute(r6, r7)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r6 = "artist"
                    java.lang.String r7 = r0.artist     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r5.setAttribute(r6, r7)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "uid"
                    java.lang.String r8 = r0.uid     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "duration"
                    int r8 = r0.duration     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "score"
                    int r8 = r0.score     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "playcnt"
                    int r8 = r0.playcnt     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r6 = "rid"
                    java.lang.String r7 = r0.rid     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r5.setAttribute(r6, r7)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "bdurl"
                    java.lang.String r8 = r0.baiduURL     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "hbr"
                    int r8 = r0.getHighAACBitrate()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "hurl"
                    java.lang.String r8 = r0.getHighAACURL()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "lbr"
                    int r8 = r0.getLowAACBitrate()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "lurl"
                    java.lang.String r8 = r0.getLowAACURL()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "mp3br"
                    int r8 = r0.getMp3Bitrate()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "mp3url"
                    java.lang.String r8 = r0.getMp3URL()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "isnew"
                    int r8 = r0.isNew     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "cid"
                    java.lang.String r8 = r0.cid     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "valid"
                    java.lang.String r8 = r0.valid     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "hasmedia"
                    int r8 = r0.hasmedia     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "singerId"
                    java.lang.String r8 = r0.singerId     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "ctcid"
                    java.lang.String r8 = r0.ctcid     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "ctvalid"
                    java.lang.String r8 = r0.ctvalid     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "cthasmedia"
                    int r8 = r0.cthasmedia     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "ctvip"
                    int r8 = r0.ctVip     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "wavurl"
                    java.lang.String r8 = r0.ctWavUrl     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "cuvip"
                    int r8 = r0.cuvip     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "cuftp"
                    java.lang.String r8 = r0.cuftp     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "cucid"
                    java.lang.String r8 = r0.cucid     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "cusid"
                    java.lang.String r8 = r0.cusid     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "cuurl"
                    java.lang.String r8 = r0.cuurl     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "cuvalid"
                    java.lang.String r8 = r0.cuvalid     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "hasshow"
                    int r8 = r0.hasshow     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "ishot"
                    int r8 = r0.isHot     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "head_url"
                    java.lang.String r8 = r0.userHead     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "comment_num"
                    int r8 = r0.commentNum     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r7 = "date"
                    java.lang.String r0 = r0.date     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r6.a(r5, r7, r0)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r4.appendChild(r5)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    int r0 = r1 + 1
                    r1 = r0
                    goto L_0x0029
                L_0x0188:
                    javax.xml.transform.TransformerFactory r0 = javax.xml.transform.TransformerFactory.newInstance()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    javax.xml.transform.Transformer r0 = r0.newTransformer()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r1 = "encoding"
                    java.lang.String r4 = "utf-8"
                    r0.setOutputProperty(r1, r4)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r1 = "indent"
                    java.lang.String r4 = "yes"
                    r0.setOutputProperty(r1, r4)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r1 = "standalone"
                    java.lang.String r4 = "yes"
                    r0.setOutputProperty(r1, r4)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r1 = "method"
                    java.lang.String r4 = "xml"
                    r0.setOutputProperty(r1, r4)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    javax.xml.transform.dom.DOMSource r1 = new javax.xml.transform.dom.DOMSource     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    org.w3c.dom.Element r3 = r3.getDocumentElement()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r1.<init>(r3)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.io.File r3 = new java.io.File     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r4 = com.shoujiduoduo.b.f.b.f1375a     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r3.<init>(r4)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    boolean r4 = r3.exists()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    if (r4 != 0) goto L_0x01c7
                    r3.createNewFile()     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                L_0x01c7:
                    java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r4.<init>(r3)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    javax.xml.transform.stream.StreamResult r3 = new javax.xml.transform.stream.StreamResult     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r3.<init>(r4)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    r0.transform(r1, r3)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    java.lang.String r0 = "FavoriteRingList"
                    java.lang.String r1 = "write local favorite ring file success"
                    com.shoujiduoduo.base.a.a.a(r0, r1)     // Catch:{ ParserConfigurationException -> 0x01dd, TransformerConfigurationException -> 0x01ed, FileNotFoundException -> 0x01f2, TransformerException -> 0x01f7, Exception -> 0x01fc }
                    monitor-exit(r2)     // Catch:{ all -> 0x01ea }
                L_0x01dc:
                    return
                L_0x01dd:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ all -> 0x01ea }
                L_0x01e1:
                    java.lang.String r0 = "FavoriteRingList"
                    java.lang.String r1 = "write favorite fing file error"
                    com.shoujiduoduo.base.a.a.a(r0, r1)     // Catch:{ all -> 0x01ea }
                    monitor-exit(r2)     // Catch:{ all -> 0x01ea }
                    goto L_0x01dc
                L_0x01ea:
                    r0 = move-exception
                    monitor-exit(r2)     // Catch:{ all -> 0x01ea }
                    throw r0
                L_0x01ed:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ all -> 0x01ea }
                    goto L_0x01e1
                L_0x01f2:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ all -> 0x01ea }
                    goto L_0x01e1
                L_0x01f7:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ all -> 0x01ea }
                    goto L_0x01e1
                L_0x01fc:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ all -> 0x01ea }
                    goto L_0x01e1
                */
                throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.b.AnonymousClass5.run():void");
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(Element element, String str, String str2) {
        if (!ai.c(str2)) {
            element.setAttribute(str, str2);
        }
    }

    /* access modifiers changed from: private */
    public void a(Element element, String str, int i) {
        if (i != 0) {
            element.setAttribute(str, "" + i);
        }
    }

    public boolean a(List<RingData> list) {
        boolean z;
        synchronized ("FavoriteRingList") {
            if (list != null) {
                if (list.size() != 0) {
                    StringBuilder sb = new StringBuilder();
                    for (RingData ringData : list) {
                        sb.append(ringData.rid);
                        sb.append("|");
                    }
                    if (this.d == this.c) {
                        this.d.removeAll(list);
                    } else {
                        this.d.removeAll(list);
                        this.c.removeAll(list);
                    }
                    if (sb.toString().endsWith("|")) {
                        sb.deleteCharAt(sb.length() - 1);
                    }
                    e(sb.toString());
                    a(this.c);
                    g();
                    z = true;
                }
            }
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r0 = a((java.util.List<com.shoujiduoduo.base.bean.RingData>) r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.util.Collection<java.lang.Integer> r7) {
        /*
            r6 = this;
            java.lang.String r1 = "FavoriteRingList"
            monitor-enter(r1)
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x003b }
            r2.<init>()     // Catch:{ all -> 0x003b }
            java.util.Iterator r3 = r7.iterator()     // Catch:{ all -> 0x003b }
        L_0x000c:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x003e
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x003b }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x003b }
            int r4 = r0.intValue()     // Catch:{ all -> 0x003b }
            if (r4 < 0) goto L_0x002a
            int r4 = r0.intValue()     // Catch:{ all -> 0x003b }
            java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> r5 = r6.d     // Catch:{ all -> 0x003b }
            int r5 = r5.size()     // Catch:{ all -> 0x003b }
            if (r4 <= r5) goto L_0x002d
        L_0x002a:
            r0 = 0
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
        L_0x002c:
            return r0
        L_0x002d:
            java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> r4 = r6.d     // Catch:{ all -> 0x003b }
            int r0 = r0.intValue()     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r4.get(r0)     // Catch:{ all -> 0x003b }
            r2.add(r0)     // Catch:{ all -> 0x003b }
            goto L_0x000c
        L_0x003b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            throw r0
        L_0x003e:
            boolean r0 = r6.a(r2)     // Catch:{ all -> 0x003b }
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.b.a(java.util.Collection):boolean");
    }

    public boolean a(int i) {
        boolean z;
        synchronized ("FavoriteRingList") {
            if (i >= this.d.size() || i < 0) {
                z = false;
            } else {
                String str = this.d.get(i).rid;
                if (this.d != this.c) {
                    this.d.remove(i);
                }
                d(str);
                e(str);
                g();
                z = true;
            }
        }
        return z;
    }

    private void d(String str) {
        com.shoujiduoduo.base.a.a.a("FavoriteRingList", "delete from local data, rid:" + str);
        Iterator<RingData> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            RingData next = it.next();
            if (next.rid.equals(str)) {
                com.shoujiduoduo.base.a.a.a("FavoriteRingList", "found in local, delete success");
                this.c.remove(next);
                a(this.c);
                break;
            }
        }
        this.b.a(str);
    }

    private void e(final String str) {
        if (com.shoujiduoduo.a.b.b.g().g()) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "delete from online data, rid:" + str);
            i.a(new Runnable() {
                public void run() {
                    u.g(str);
                }
            });
        }
    }

    public boolean a(RingData ringData) {
        boolean add;
        synchronized ("FavoriteRingList") {
            Iterator<RingData> it = this.d.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().rid.equals(ringData.rid)) {
                        add = true;
                        break;
                    }
                } else {
                    add = this.d.add(ringData);
                    b(ringData);
                    c(ringData);
                    g();
                    break;
                }
            }
        }
        return add;
    }

    private void b(RingData ringData) {
        com.shoujiduoduo.base.a.a.a("FavoriteRingList", "append to local data, name:" + ringData.name);
        if (this.d != this.c) {
            this.c.add(ringData);
        }
        this.b.a(ringData);
        a(this.c);
    }

    private void c(final RingData ringData) {
        if (com.shoujiduoduo.a.b.b.g().g()) {
            i.a(new Runnable() {
                public void run() {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "append to online data, name:" + ringData.name);
                    u.f(ringData.rid);
                }
            });
        }
    }

    private void g() {
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c.a<g>() {
            public void a() {
                ((g) this.f1284a).a(b.this, 0);
            }
        });
    }

    /* compiled from: FavoriteRingList */
    private class a extends SQLiteOpenHelper {
        private final String b = "UserRingDB";

        public a(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
            super(context, str, cursorFactory, i);
        }

        public void a(String str) {
            SQLiteDatabase sQLiteDatabase = null;
            try {
                sQLiteDatabase = getWritableDatabase();
                sQLiteDatabase.execSQL("delete from user_ring_table where rid='" + str + "'");
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (Throwable th) {
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
                throw th;
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x00d4, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x00d5, code lost:
            r3 = r1;
            r1 = r0;
            r0 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x00da, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x00ce, code lost:
            if (r0 != null) goto L_0x00d0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x00d0, code lost:
            r0.close();
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x00da  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x00cd A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0001] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.shoujiduoduo.base.bean.RingData r5) {
            /*
                r4 = this;
                r0 = 0
                android.database.sqlite.SQLiteDatabase r0 = r4.getWritableDatabase()     // Catch:{ Exception -> 0x00cd, all -> 0x00d4 }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                r1.<init>()     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = "insert into user_ring_table (rid, name, artist, duration, score, play_count, cailing_id, cailing_valid_date, has_media, singer_id, price)VALUES ("
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.rid     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.name     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.artist     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.duration     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.score     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.playcnt     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.cid     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.valid     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.hasmedia     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.singerId     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.price     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ");"
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                r0.execSQL(r1)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                if (r0 == 0) goto L_0x00cc
                r0.close()
            L_0x00cc:
                return
            L_0x00cd:
                r1 = move-exception
                if (r0 == 0) goto L_0x00cc
                r0.close()
                goto L_0x00cc
            L_0x00d4:
                r1 = move-exception
                r3 = r1
                r1 = r0
                r0 = r3
            L_0x00d8:
                if (r1 == 0) goto L_0x00dd
                r1.close()
            L_0x00dd:
                throw r0
            L_0x00de:
                r1 = move-exception
                r3 = r1
                r1 = r0
                r0 = r3
                goto L_0x00d8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.b.a.a(com.shoujiduoduo.base.bean.RingData):void");
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    /* renamed from: b */
    public RingData get(int i) {
        if (i < 0 || i >= this.d.size()) {
            return null;
        }
        return this.d.get(i);
    }

    public int size() {
        return this.d.size();
    }

    public boolean isRetrieving() {
        return false;
    }

    public void retrieveData() {
    }

    public void refreshData() {
    }

    public boolean hasMoreData() {
        return false;
    }

    public ListType.LIST_TYPE getListType() {
        return ListType.LIST_TYPE.list_user_favorite;
    }

    public void reloadData() {
    }
}
