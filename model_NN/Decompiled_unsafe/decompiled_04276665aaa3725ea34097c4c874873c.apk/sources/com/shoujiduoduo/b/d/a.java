package com.shoujiduoduo.b.d;

import android.util.Xml;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.t;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.r;
import com.shoujiduoduo.util.u;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

/* compiled from: HotKeyword */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<b> f1356a;
    /* access modifiers changed from: private */
    public C0039a b = new C0039a("hotkey.tmp");
    private boolean c;

    /* access modifiers changed from: private */
    public ArrayList<b> a(InputStream inputStream) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
            if (parse == null) {
                com.shoujiduoduo.base.a.a.a("HotKeyword", "parse error");
                return null;
            }
            Element documentElement = parse.getDocumentElement();
            if (documentElement == null) {
                com.shoujiduoduo.base.a.a.a("HotKeyword", "cannot find root node");
                return null;
            }
            NodeList elementsByTagName = documentElement.getElementsByTagName("key");
            if (elementsByTagName == null) {
                com.shoujiduoduo.base.a.a.a("HotKeyword", "cannot find node named \"key\"");
                return null;
            }
            ArrayList<b> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                b bVar = new b();
                bVar.f1359a = g.a(attributes, "txt");
                bVar.b = g.a(attributes, "new", 0);
                bVar.c = g.a(attributes, "trend", 0);
                arrayList.add(bVar);
            }
            return arrayList;
        } catch (IOException e) {
            com.shoujiduoduo.base.a.a.a(e);
            return null;
        } catch (SAXException e2) {
            com.shoujiduoduo.base.a.a.a(e2);
            return null;
        } catch (ParserConfigurationException e3) {
            com.shoujiduoduo.base.a.a.a(e3);
            return null;
        } catch (ArrayIndexOutOfBoundsException e4) {
            com.shoujiduoduo.base.a.a.a(e4);
            return null;
        } catch (DOMException e5) {
            com.shoujiduoduo.base.a.a.a(e5);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean d() {
        String g = u.g();
        if (g != null) {
            this.f1356a = a(new ByteArrayInputStream(g.getBytes()));
            if (this.f1356a != null && this.f1356a.size() > 0) {
                com.shoujiduoduo.base.a.a.a("HotKeyword", this.f1356a.size() + " keywords.");
                this.b.a(this.f1356a);
                this.c = true;
                c.a().a(b.OBSERVER_SEARCH_HOT_WORD, new c.a<t>() {
                    public void a() {
                        ((t) this.f1284a).a();
                    }
                });
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean e() {
        this.f1356a = this.b.a();
        if (this.f1356a == null || this.f1356a.size() <= 0) {
            com.shoujiduoduo.base.a.a.e("HotKeyword", "cache is not valid");
            return false;
        }
        com.shoujiduoduo.base.a.a.a("HotKeyword", this.f1356a.size() + " keywords. read from cache.");
        this.c = true;
        c.a().a(b.OBSERVER_SEARCH_HOT_WORD, new c.a<t>() {
            public void a() {
                ((t) this.f1284a).a();
            }
        });
        return true;
    }

    public void a() {
        if (this.f1356a == null) {
            i.a(new Runnable() {
                public void run() {
                    if (a.this.b.a(21600000)) {
                        if (!a.this.d()) {
                            boolean unused = a.this.e();
                        }
                    } else if (!a.this.e()) {
                        boolean unused2 = a.this.d();
                    }
                }
            });
        }
    }

    public boolean b() {
        return this.c;
    }

    public ArrayList<b> c() {
        return this.f1356a;
    }

    /* renamed from: com.shoujiduoduo.b.d.a$a  reason: collision with other inner class name */
    /* compiled from: HotKeyword */
    class C0039a extends q<ArrayList<b>> {
        C0039a(String str) {
            super(str);
        }

        public ArrayList<b> a() {
            try {
                return a.this.a(new FileInputStream(c + this.b));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }

        public void a(ArrayList<b> arrayList) {
            if (arrayList != null) {
                XmlSerializer newSerializer = Xml.newSerializer();
                StringWriter stringWriter = new StringWriter();
                try {
                    newSerializer.setOutput(stringWriter);
                    newSerializer.startDocument("UTF-8", true);
                    newSerializer.startTag("", "root");
                    newSerializer.attribute("", "num", String.valueOf(arrayList.size()));
                    for (int i = 0; i < arrayList.size(); i++) {
                        b bVar = arrayList.get(i);
                        newSerializer.startTag("", "key");
                        newSerializer.attribute("", "txt", bVar.f1359a);
                        newSerializer.attribute("", "new", "" + bVar.b);
                        newSerializer.attribute("", "trend", "" + bVar.c);
                        newSerializer.endTag("", "key");
                    }
                    newSerializer.endTag("", "root");
                    newSerializer.endDocument();
                    r.b(c + this.b, stringWriter.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
