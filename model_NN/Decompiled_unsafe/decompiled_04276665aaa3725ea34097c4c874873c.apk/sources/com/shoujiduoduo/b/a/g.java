package com.shoujiduoduo.b.a;

import android.text.TextUtils;
import cn.banshenggua.aichang.utils.Constants;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.m;
import com.shoujiduoduo.util.r;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.u;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/* compiled from: SearchAdData */
public class g {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static String f1299a = (m.a(2) + "search_ad.tmp");
    private ArrayList<a> b = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean c;
    private int d;

    public void a() {
        f();
    }

    public void b() {
        if (this.b != null) {
            this.b.clear();
        }
    }

    public boolean c() {
        com.shoujiduoduo.base.a.a.a("SearchAdData", "isDataReady:" + this.c);
        return this.c;
    }

    public a d() {
        if (!this.c) {
            return null;
        }
        try {
            int size = this.b.size();
            if (size > 0) {
                a aVar = this.b.get(this.d % size);
                this.d++;
                return aVar;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return null;
    }

    /* compiled from: SearchAdData */
    public class a {

        /* renamed from: a  reason: collision with root package name */
        public String f1301a = "";
        public String b = "";
        public String c = "";
        public String d = "";
        public String e = "";
        public String f = "";

        public a() {
        }

        public String toString() {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("app:" + this.f1301a);
            stringBuffer.append(", packagename:" + this.b);
            stringBuffer.append(", des:" + this.c);
            stringBuffer.append(", icon:" + this.d);
            stringBuffer.append(", weight:" + this.e);
            stringBuffer.append(", download:" + this.f);
            return stringBuffer.toString();
        }

        /* renamed from: a */
        public a clone() {
            a aVar = new a();
            aVar.f1301a = this.f1301a;
            aVar.b = this.b;
            aVar.c = this.c;
            aVar.d = this.d;
            aVar.e = this.e;
            aVar.f = this.f;
            return aVar;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, long):long */
    private void f() {
        long a2 = af.a(RingDDApp.c(), "update_search_ad_time", 0L);
        if (a2 != 0) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "timeLastUpdate = " + a2);
            com.shoujiduoduo.base.a.a.a("SearchAdData", "current time = " + System.currentTimeMillis());
            if (System.currentTimeMillis() - a2 > 3600000) {
                com.shoujiduoduo.base.a.a.a("SearchAdData", "cache out of data, download new data");
                h();
            } else if (!g()) {
                com.shoujiduoduo.base.a.a.a("SearchAdData", "cache is available, but read failed, download new data");
                h();
            }
        } else {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "no cache, read from net");
            h();
        }
    }

    /* access modifiers changed from: private */
    public boolean g() {
        NodeList elementsByTagName;
        if (!new File(f1299a).exists()) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "cache file not exist, 不显示广告");
            return false;
        }
        try {
            Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new FileInputStream(f1299a)).getDocumentElement();
            if (documentElement == null || (elementsByTagName = documentElement.getElementsByTagName(Constants.ITEM)) == null) {
                return false;
            }
            this.b.clear();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                a aVar = new a();
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                aVar.f1301a = com.shoujiduoduo.util.g.a(attributes, PushConstants.EXTRA_APPLICATION_PENDING_INTENT);
                aVar.b = com.shoujiduoduo.util.g.a(attributes, "packagename");
                aVar.c = com.shoujiduoduo.util.g.a(attributes, "des");
                aVar.d = com.shoujiduoduo.util.g.a(attributes, "icon");
                aVar.e = com.shoujiduoduo.util.g.a(attributes, "weight");
                aVar.f = com.shoujiduoduo.util.g.a(attributes, "download");
                this.b.add(aVar);
                for (int i2 = 1; i2 < s.a(aVar.e, 1); i2++) {
                    this.b.add(aVar.clone());
                }
            }
            com.shoujiduoduo.base.a.a.a("SearchAdData", "read success, list size:" + this.b.size());
            this.c = true;
            return true;
        } catch (IOException e) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "load cache exception");
            com.shoujiduoduo.base.a.a.a(e);
            return false;
        } catch (SAXException e2) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "load cache exception");
            com.shoujiduoduo.base.a.a.a(e2);
            return false;
        } catch (ParserConfigurationException e3) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "load cache exception");
            com.shoujiduoduo.base.a.a.a(e3);
            return false;
        } catch (DOMException e4) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "load cache exception");
            com.shoujiduoduo.base.a.a.a(e4);
            return false;
        } catch (Exception e5) {
            com.shoujiduoduo.base.a.a.a("SearchAdData", "load cache exception");
            com.shoujiduoduo.base.a.a.a(e5);
            return false;
        }
    }

    private void h() {
        i.a(new Runnable() {
            public void run() {
                String d = u.d();
                if (!ai.c(d) && !TextUtils.isEmpty(d.trim())) {
                    r.b(g.f1299a, d.trim());
                    com.shoujiduoduo.base.a.a.a("SearchAdData", "loadFromNetwork, write to local cache file");
                    com.shoujiduoduo.base.a.a.a("SearchAdData", "content:" + d);
                    af.b(RingDDApp.c(), "update_search_ad_time", System.currentTimeMillis());
                }
                if (g.this.g()) {
                    c.a().a(b.OBSERVER_SEARCH_AD, new c.a<com.shoujiduoduo.a.c.s>() {
                        public void a() {
                            ((com.shoujiduoduo.a.c.s) this.f1284a).a();
                        }
                    });
                    boolean unused = g.this.c = true;
                    return;
                }
                boolean unused2 = g.this.c = false;
            }
        });
    }
}
