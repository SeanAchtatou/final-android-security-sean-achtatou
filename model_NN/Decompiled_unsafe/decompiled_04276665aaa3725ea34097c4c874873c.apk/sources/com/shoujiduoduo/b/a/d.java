package com.shoujiduoduo.b.a;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.DuoduoSoftData;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/* compiled from: DuoduoFamilyData */
public class d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<DuoduoSoftData> f1291a;
    /* access modifiers changed from: private */
    public c b;
    /* access modifiers changed from: private */
    public g c = null;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    private String f;
    /* access modifiers changed from: private */
    public Handler g = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.b.a.d.a(com.shoujiduoduo.b.a.d, boolean):boolean
         arg types: [com.shoujiduoduo.b.a.d, int]
         candidates:
          com.shoujiduoduo.b.a.d.a(com.shoujiduoduo.b.a.d, java.util.ArrayList):java.util.ArrayList
          com.shoujiduoduo.b.a.d.a(com.shoujiduoduo.b.a.d, boolean):boolean */
        public void handleMessage(Message message) {
            a.a("DuoduoFamilyData", "Thread ID: " + Thread.currentThread().getName());
            switch (message.what) {
                case 0:
                case 2:
                    ArrayList unused = d.this.f1291a = (ArrayList) message.obj;
                    boolean unused2 = d.this.d = false;
                    boolean unused3 = d.this.e = false;
                    break;
                case 1:
                    boolean unused4 = d.this.d = false;
                    boolean unused5 = d.this.e = true;
                    break;
            }
            if (d.this.c != null) {
                d.this.c.a(null, message.what);
            }
        }
    };

    public d() {
        new d("duoduo_family.tmp");
    }

    public d(String str) {
        this.b = new c(str);
        this.f = str;
    }

    public void a(g gVar) {
        this.c = gVar;
    }

    public static ArrayList<DuoduoSoftData> a(InputStream inputStream) {
        Element documentElement;
        NodeList elementsByTagName;
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
            if (parse == null || (documentElement = parse.getDocumentElement()) == null || (elementsByTagName = documentElement.getElementsByTagName("software")) == null) {
                return null;
            }
            ArrayList<DuoduoSoftData> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                a.a("DuoduoFamilyData", "soft " + i);
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                DuoduoSoftData duoduoSoftData = new DuoduoSoftData();
                Node firstChild = elementsByTagName.item(i).getFirstChild();
                if (firstChild != null) {
                    duoduoSoftData.mSoftIntro = firstChild.getNodeValue();
                } else {
                    duoduoSoftData.mSoftIntro = "";
                }
                duoduoSoftData.mSoftName = com.shoujiduoduo.util.g.a(attributes, SelectCountryActivity.EXTRA_COUNTRY_NAME);
                duoduoSoftData.mPicURL = com.shoujiduoduo.util.g.a(attributes, "pic");
                duoduoSoftData.mSoftURL = com.shoujiduoduo.util.g.a(attributes, "url");
                arrayList.add(duoduoSoftData);
            }
            return arrayList;
        } catch (IOException | Exception | ParserConfigurationException | DOMException | SAXException e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean c() {
        return false;
    }

    /* access modifiers changed from: private */
    public boolean d() {
        ArrayList<DuoduoSoftData> a2 = this.b.a();
        if (a2 == null) {
            return false;
        }
        a.a("DuoduoFamilyData", a2.size() + " softwares in family. read from cache.");
        this.g.sendMessage(this.g.obtainMessage(2, a2));
        return true;
    }

    public void a() {
        if (this.f1291a == null) {
            this.d = true;
            this.e = false;
            new Thread() {
                public void run() {
                    if (d.this.b.a(LogBuilder.MAX_INTERVAL)) {
                        if (!d.this.c() && !d.this.d()) {
                            d.this.g.sendEmptyMessage(1);
                        }
                    } else if (!d.this.d() && !d.this.c()) {
                        d.this.g.sendEmptyMessage(1);
                    }
                }
            }.start();
        }
    }

    public int b() {
        if (this.f1291a == null) {
            return 0;
        }
        return this.f1291a.size();
    }

    public DuoduoSoftData a(int i) {
        if (this.f1291a == null || i < 0 || i >= this.f1291a.size()) {
            return null;
        }
        return this.f1291a.get(i);
    }
}
