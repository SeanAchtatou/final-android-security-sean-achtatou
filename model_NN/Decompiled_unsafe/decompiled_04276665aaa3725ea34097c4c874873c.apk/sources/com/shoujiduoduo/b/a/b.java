package com.shoujiduoduo.b.a;

import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.m;
import java.util.ArrayList;

/* compiled from: BannerAdData */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static String f1289a = (m.a(2) + "banner_ad.tmp");
    private ArrayList<a> b = new ArrayList<>();
    private boolean c;
    private int d;

    /* compiled from: BannerAdData */
    public class a {

        /* renamed from: a  reason: collision with root package name */
        public String f1290a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
    }

    public void a() {
        f();
    }

    public boolean b() {
        com.shoujiduoduo.base.a.a.a("BannerAdData", "isDataReady:" + this.c);
        return this.c;
    }

    public void c() {
        if (this.b != null) {
            this.b.clear();
        }
    }

    public ArrayList<a> d() {
        return this.b;
    }

    public int e() {
        return this.d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, long):long */
    private void f() {
        long a2 = af.a(RingDDApp.c(), "update_banner_ad_time", 0L);
        if (a2 != 0) {
            com.shoujiduoduo.base.a.a.a("BannerAdData", "timeLastUpdate = " + a2);
            com.shoujiduoduo.base.a.a.a("BannerAdData", "current time = " + System.currentTimeMillis());
            g();
            return;
        }
        com.shoujiduoduo.base.a.a.a("BannerAdData", "no cache, read from net");
        g();
    }

    private void g() {
    }
}
