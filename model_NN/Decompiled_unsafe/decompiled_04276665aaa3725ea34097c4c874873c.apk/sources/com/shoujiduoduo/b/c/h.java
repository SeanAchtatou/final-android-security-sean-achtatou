package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.MessageData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.u;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/* compiled from: MessageList */
public class h implements DDList {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<MessageData> f1330a = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<MessageData> b = new ArrayList<>();
    private String c;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = true;
    /* access modifiers changed from: private */
    public boolean f = true;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public i j;
    /* access modifiers changed from: private */
    public a k = a.old_refresh;
    private com.shoujiduoduo.a.c.a l = new com.shoujiduoduo.a.c.a() {
        public void a() {
            if (h.this.b.size() > 0) {
                com.shoujiduoduo.base.a.a.a("MessageList", "app退出时，如果刷新的数据与老数据尚未衔接，则复写老数据, 更新缓存文件");
                ListContent listContent = new ListContent();
                listContent.hasMore = h.this.e;
                listContent.data = h.this.b;
                h.this.j.a(listContent);
            }
        }

        public void b() {
        }
    };
    private Handler m = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ListContent listContent = (ListContent) message.obj;
                    if (listContent != null) {
                        com.shoujiduoduo.base.a.a.a("MessageList", "list data size = " + listContent.data.size() + ", hasmore:" + listContent.hasMore);
                        if (listContent.data.size() > 0) {
                            com.shoujiduoduo.base.a.a.a("MessageList", "数据时间范围，begin:" + ((MessageData) listContent.data.get(listContent.data.size() - 1)).date + ", end:" + ((MessageData) listContent.data.get(0)).date);
                        }
                        if (h.this.i) {
                            h.this.f1330a.clear();
                            h.this.b.clear();
                        }
                        if (h.this.f1330a == null) {
                            ArrayList unused = h.this.f1330a = listContent.data;
                        } else {
                            h.this.f1330a.addAll(listContent.data);
                            if (h.this.f1330a.size() > 0) {
                                af.c(RingDDApp.c(), "concern_feeds_newest_time", ((MessageData) h.this.f1330a.get(0)).date);
                            }
                        }
                        boolean unused2 = h.this.f = listContent.hasMore;
                        listContent.data = h.this.f1330a;
                        if (h.this.h && h.this.f1330a.size() > 0) {
                            h.this.j.a(listContent);
                            boolean unused3 = h.this.h = false;
                        }
                    }
                    boolean unused4 = h.this.d = false;
                    boolean unused5 = h.this.g = false;
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                    boolean unused6 = h.this.d = false;
                    boolean unused7 = h.this.g = true;
                    break;
                case 5:
                    ListContent listContent2 = (ListContent) message.obj;
                    if (listContent2 != null) {
                        com.shoujiduoduo.base.a.a.a("MessageList", "get new data size = " + listContent2.data.size() + ", hasmore:" + listContent2.hasMore);
                        if (listContent2.data.size() > 0) {
                            com.shoujiduoduo.base.a.a.a("MessageList", "数据时间范围，begin:" + ((MessageData) listContent2.data.get(listContent2.data.size() - 1)).date + ", end:" + ((MessageData) listContent2.data.get(0)).date);
                            af.c(RingDDApp.c(), "concern_feeds_newest_time", ((MessageData) listContent2.data.get(0)).date);
                        }
                        if (h.this.b.size() <= 0) {
                            h.this.b.addAll(listContent2.data);
                            if (listContent2.hasMore) {
                                a unused8 = h.this.k = a.new_refresh;
                            } else {
                                a unused9 = h.this.k = a.old_refresh;
                                h.this.f1330a.addAll(0, h.this.b);
                                h.this.b.clear();
                                com.shoujiduoduo.base.a.a.a("MessageList", "hasmore is false, mNewRefreshData 与mOldRefreshData 合并");
                                com.shoujiduoduo.base.a.a.a("MessageList", "合并后大小， Old list size:" + h.this.f1330a.size());
                                if (h.this.f1330a.size() > 0) {
                                    ListContent listContent3 = new ListContent();
                                    listContent3.hasMore = h.this.f;
                                    listContent3.data = h.this.f1330a;
                                    h.this.j.a(listContent3);
                                    boolean unused10 = h.this.h = false;
                                    com.shoujiduoduo.base.a.a.a("MessageList", "write cache, cache size:" + h.this.f1330a.size());
                                }
                            }
                        } else if (listContent2.hasMore) {
                            h.this.f1330a.clear();
                            h.this.f1330a.addAll(h.this.b);
                            h.this.b.clear();
                            boolean unused11 = h.this.f = h.this.e;
                            a unused12 = h.this.k = a.old_refresh;
                            com.shoujiduoduo.base.a.a.a("MessageList", "之前更新了最新数据，但是数据没有和老的数据衔接，则抛弃老的数据, 更新一次缓存文件");
                            if (h.this.f1330a.size() > 0) {
                                ListContent listContent4 = new ListContent();
                                listContent4.hasMore = h.this.f;
                                listContent4.data = h.this.f1330a;
                                h.this.j.a(listContent4);
                            }
                        } else {
                            h.this.b.addAll(0, listContent2.data);
                            a unused13 = h.this.k = a.new_refresh;
                            com.shoujiduoduo.base.a.a.a("MessageList", "无更多数据，更新mNewRefreshData, 将新数据放在mNewRefreshData头部");
                        }
                        boolean unused14 = h.this.e = listContent2.hasMore;
                    }
                    boolean unused15 = h.this.d = false;
                    boolean unused16 = h.this.g = false;
                    break;
                case 6:
                    ListContent listContent5 = (ListContent) message.obj;
                    if (listContent5 != null) {
                        com.shoujiduoduo.base.a.a.a("MessageList", "get new data more size = " + listContent5.data.size() + ", hasmore:" + listContent5.hasMore);
                        if (listContent5.data.size() > 0) {
                            com.shoujiduoduo.base.a.a.a("MessageList", "数据时间范围，begin:" + ((MessageData) listContent5.data.get(listContent5.data.size() - 1)).date + ", end:" + ((MessageData) listContent5.data.get(0)).date);
                        }
                        h.this.b.addAll(listContent5.data);
                        if (listContent5.hasMore) {
                            a unused17 = h.this.k = a.new_refresh;
                            com.shoujiduoduo.base.a.a.a("MessageList", "hasmore is true, 数据添加至mNewRefreshData");
                        } else {
                            a unused18 = h.this.k = a.old_refresh;
                            h.this.f1330a.addAll(0, h.this.b);
                            h.this.b.clear();
                            com.shoujiduoduo.base.a.a.a("MessageList", "hasmore is false, mNewRefreshData 与mOldRefreshData 合并");
                            com.shoujiduoduo.base.a.a.a("MessageList", "合并后大小， mOldRefreshData size:" + h.this.f1330a.size());
                            if (h.this.f1330a.size() > 0) {
                                ListContent listContent6 = new ListContent();
                                listContent6.hasMore = h.this.f;
                                listContent6.data = h.this.f1330a;
                                h.this.j.a(listContent6);
                                boolean unused19 = h.this.h = false;
                                com.shoujiduoduo.base.a.a.a("MessageList", "write cache, cache size:" + h.this.f1330a.size());
                            }
                        }
                        boolean unused20 = h.this.e = listContent5.hasMore;
                    }
                    boolean unused21 = h.this.d = false;
                    boolean unused22 = h.this.g = false;
                    break;
            }
            final int i = message.what;
            c.a().a(b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1284a).a(h.this, i);
                }
            });
        }
    };

    /* compiled from: MessageList */
    private enum a {
        new_refresh,
        old_refresh
    }

    public h(String str) {
        this.j = new i("concern_feeds_" + str + ".tmp");
        this.c = str;
        c.a().a(b.OBSERVER_APP, this.l);
    }

    public Object get(int i2) {
        if (this.k == a.new_refresh) {
            if (this.b.size() > 0) {
                return this.b.get(i2);
            }
        } else if (this.f1330a.size() > 0) {
            return this.f1330a.get(i2);
        }
        return null;
    }

    public String getBaseURL() {
        return null;
    }

    public String getListId() {
        return "concern_feeds_" + this.c;
    }

    public ListType.LIST_TYPE getListType() {
        return ListType.LIST_TYPE.list_concern_feeds;
    }

    public int size() {
        if (this.k == a.new_refresh) {
            return this.b.size();
        }
        return this.f1330a.size();
    }

    public boolean isRetrieving() {
        return this.d;
    }

    public void retrieveData() {
        if (this.k == a.old_refresh) {
            com.shoujiduoduo.base.a.a.a("MessageList", "retrieveData, old refresh type");
            if (this.f1330a == null || this.f1330a.size() == 0) {
                this.d = true;
                this.g = false;
                this.i = false;
                i.a(new Runnable() {
                    public void run() {
                        com.shoujiduoduo.base.a.a.a("MessageList", "retrieveData, old refresh type, getListData");
                        h.this.c();
                    }
                });
            } else if (this.f) {
                this.d = true;
                this.g = false;
                this.i = false;
                i.a(new Runnable() {
                    public void run() {
                        com.shoujiduoduo.base.a.a.a("MessageList", "retrieveData, old refresh type, getListDataMore");
                        h.this.d();
                    }
                });
            }
        } else {
            com.shoujiduoduo.base.a.a.a("MessageList", "retrieveData, new refresh type");
            if (this.b == null || this.b.size() == 0) {
                this.d = true;
                this.g = false;
                this.i = false;
                i.a(new Runnable() {
                    public void run() {
                        com.shoujiduoduo.base.a.a.a("MessageList", "retrieveData, new refresh type, getNewListData");
                        h.this.a();
                    }
                });
            } else if (this.e) {
                this.d = true;
                this.g = false;
                this.i = false;
                i.a(new Runnable() {
                    public void run() {
                        com.shoujiduoduo.base.a.a.a("MessageList", "retrieveData, new refresh type, getNewListDataMore");
                        h.this.b();
                    }
                });
            }
        }
    }

    public void refreshData() {
        this.d = true;
        this.g = false;
        this.i = false;
        i.a(new Runnable() {
            public void run() {
                h.this.a();
            }
        });
    }

    /* access modifiers changed from: private */
    public void a() {
        String str = "";
        if (this.b.size() > 0) {
            str = this.b.get(0).date;
            com.shoujiduoduo.base.a.a.b("MessageList", "getNewListData, tBegin from new refresh data:" + str);
        } else if (this.f1330a.size() > 0) {
            str = this.f1330a.get(0).date;
            com.shoujiduoduo.base.a.a.b("MessageList", "getNewListData, tBegin from old refresh data:" + str);
        }
        String a2 = a(str, "");
        if (ai.c(a2)) {
            com.shoujiduoduo.base.a.a.a("MessageList", "RingList: httpGetRingList Failed!");
            this.m.sendEmptyMessage(4);
            return;
        }
        ListContent<MessageData> c2 = j.c(new ByteArrayInputStream(a2.getBytes()));
        if (c2 == null) {
            com.shoujiduoduo.base.a.a.a("MessageList", "RingList:parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
            this.m.sendEmptyMessage(4);
        } else if (c2.data.size() > 0) {
            a(c2.data);
            this.m.sendMessage(this.m.obtainMessage(5, c2));
            Iterator<T> it = c2.data.iterator();
            while (it.hasNext()) {
                com.shoujiduoduo.base.a.a.a("MessageList", ((MessageData) it.next()).date);
            }
        } else {
            com.shoujiduoduo.base.a.a.a("MessageList", "refresh data size is 0, no new data");
            this.m.sendEmptyMessage(3);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        String str;
        if (this.f1330a == null || this.f1330a.size() <= 0) {
            str = "";
        } else {
            str = this.f1330a.get(0).date;
        }
        String str2 = "";
        if (this.b != null && this.b.size() > 0) {
            str2 = this.b.get(this.b.size() - 1).date;
        }
        com.shoujiduoduo.base.a.a.b("MessageList", "getNewListDataMore, tBegin:" + str + ", tEnd:" + str2);
        String a2 = a(str, str2);
        if (ai.c(a2)) {
            com.shoujiduoduo.base.a.a.a("MessageList", "RingList: httpGetRingList Failed!");
            this.m.sendEmptyMessage(4);
            return;
        }
        ListContent<MessageData> c2 = j.c(new ByteArrayInputStream(a2.getBytes()));
        if (c2 != null) {
            if (c2.data.size() > 0) {
                a(c2.data);
            }
            this.m.sendMessage(this.m.obtainMessage(6, c2));
            Iterator<T> it = c2.data.iterator();
            while (it.hasNext()) {
                com.shoujiduoduo.base.a.a.a("MessageList", ((MessageData) it.next()).date);
            }
            return;
        }
        com.shoujiduoduo.base.a.a.a("MessageList", "RingList:parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.m.sendEmptyMessage(4);
    }

    /* access modifiers changed from: private */
    public void c() {
        if (!this.j.a(345600000)) {
            com.shoujiduoduo.base.a.a.a("MessageList", "CollectList: cache is available! Use Cache!");
            ListContent<MessageData> a2 = this.j.a();
            if (!(a2 == null || a2.data == null || a2.data.size() <= 0)) {
                com.shoujiduoduo.base.a.a.a("MessageList", "RingList: Read RingList Cache Success!");
                this.m.sendMessage(this.m.obtainMessage(0, a2));
                return;
            }
        }
        com.shoujiduoduo.base.a.a.a("MessageList", "cache is out of date or read cache failed!");
        com.shoujiduoduo.base.a.a.b("MessageList", "get data, tBegin:, tEnd:");
        String a3 = a("", "");
        if (ai.c(a3)) {
            com.shoujiduoduo.base.a.a.a("MessageList", "RingList: httpGetRingList Failed!");
            this.m.sendEmptyMessage(1);
            return;
        }
        ListContent<MessageData> c2 = j.c(new ByteArrayInputStream(a3.getBytes()));
        if (c2 != null) {
            com.shoujiduoduo.base.a.a.a("MessageList", "list data size = " + c2.data.size());
            this.h = true;
            if (c2.data.size() > 0) {
                a(c2.data);
            }
            Iterator<T> it = c2.data.iterator();
            while (it.hasNext()) {
                com.shoujiduoduo.base.a.a.a("MessageList", ((MessageData) it.next()).date);
            }
            this.m.sendMessage(this.m.obtainMessage(0, c2));
            return;
        }
        com.shoujiduoduo.base.a.a.a("MessageList", "RingList:parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.m.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void d() {
        ListContent<MessageData> listContent;
        com.shoujiduoduo.base.a.a.b("MessageList", "retrieving more data, current old refresh list size = " + this.f1330a.size());
        MessageData messageData = this.f1330a.get(this.f1330a.size() - 1);
        com.shoujiduoduo.base.a.a.a("MessageList", "get more data, tend:" + messageData.date);
        String a2 = a("", messageData.date);
        if (ai.c(a2)) {
            this.m.sendEmptyMessage(2);
            return;
        }
        try {
            listContent = j.c(new ByteArrayInputStream(a2.getBytes()));
        } catch (ArrayIndexOutOfBoundsException e2) {
            listContent = null;
        }
        if (listContent != null) {
            com.shoujiduoduo.base.a.a.a("MessageList", "list data size = " + listContent.data.size());
            this.h = true;
            if (listContent.data.size() > 0) {
                a(listContent.data);
            }
            Iterator<T> it = listContent.data.iterator();
            while (it.hasNext()) {
                com.shoujiduoduo.base.a.a.a("MessageList", ((MessageData) it.next()).date);
            }
            this.m.sendMessage(this.m.obtainMessage(0, listContent));
            return;
        }
        this.m.sendEmptyMessage(2);
    }

    private String a(String str, String str2) {
        return u.a("getfeeds", "&pagesize=25&tb=" + str + "&te=" + str2 + "&uid=" + com.shoujiduoduo.a.b.b.g().f());
    }

    private void a(ArrayList<MessageData> arrayList) {
        Collections.sort(arrayList, new Comparator<MessageData>() {
            /* renamed from: a */
            public int compare(MessageData messageData, MessageData messageData2) {
                try {
                    return -messageData.date.compareTo(messageData2.date);
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
            }
        });
    }

    public void reloadData() {
        com.shoujiduoduo.base.a.a.a("MessageList", "reloaddata");
        this.d = true;
        this.g = false;
        this.i = true;
        i.a(new Runnable() {
            public void run() {
                h.this.c();
            }
        });
    }

    public boolean hasMoreData() {
        if (this.k == a.old_refresh) {
            return this.f;
        }
        return this.e;
    }
}
