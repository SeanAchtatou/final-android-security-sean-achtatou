package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.u;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/* compiled from: ConcernRingList */
public class f implements DDList {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<RingData> f1319a = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<RingData> b = new ArrayList<>();
    private String c;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = true;
    /* access modifiers changed from: private */
    public boolean f = true;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public k j;
    /* access modifiers changed from: private */
    public a k = a.old_refresh;
    private b l;
    private com.shoujiduoduo.a.c.a m = new com.shoujiduoduo.a.c.a() {
        public void a() {
            if (f.this.b.size() > 0) {
                com.shoujiduoduo.base.a.a.a("ConcernRingList", "app退出时，如果刷新的数据与老数据尚未衔接，则复写老数据, 更新缓存文件");
                ListContent listContent = new ListContent();
                listContent.hasMore = f.this.e;
                listContent.data = f.this.b;
                f.this.j.a(listContent);
            }
        }

        public void b() {
        }
    };
    private Handler n = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ListContent listContent = (ListContent) message.obj;
                    if (listContent != null) {
                        com.shoujiduoduo.base.a.a.a("ConcernRingList", "list data size = " + listContent.data.size() + ", hasmore:" + listContent.hasMore);
                        if (f.this.h) {
                            f.this.f1319a.clear();
                            f.this.b.clear();
                        }
                        if (f.this.f1319a == null) {
                            ArrayList unused = f.this.f1319a = listContent.data;
                        } else {
                            f.this.f1319a.addAll(listContent.data);
                        }
                        boolean unused2 = f.this.f = listContent.hasMore;
                        listContent.data = f.this.f1319a;
                        if (f.this.i && f.this.f1319a.size() > 0) {
                            f.this.j.a(listContent);
                            boolean unused3 = f.this.i = false;
                        }
                    }
                    boolean unused4 = f.this.d = false;
                    boolean unused5 = f.this.g = false;
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                    boolean unused6 = f.this.d = false;
                    boolean unused7 = f.this.g = true;
                    break;
                case 5:
                    ListContent listContent2 = (ListContent) message.obj;
                    if (listContent2 != null) {
                        com.shoujiduoduo.base.a.a.a("ConcernRingList", "get new data size = " + listContent2.data.size() + ", hasmore:" + listContent2.hasMore);
                        if (listContent2.data.size() > 0) {
                            com.shoujiduoduo.base.a.a.a("ConcernRingList", "begin:" + ((RingData) listContent2.data.get(listContent2.data.size() - 1)).date + ", end:" + ((RingData) listContent2.data.get(0)).date);
                        }
                        if (f.this.b.size() <= 0) {
                            f.this.b.addAll(listContent2.data);
                            if (listContent2.hasMore) {
                                a unused8 = f.this.k = a.new_refresh;
                            } else {
                                a unused9 = f.this.k = a.old_refresh;
                                f.this.f1319a.addAll(0, f.this.b);
                                f.this.b.clear();
                                com.shoujiduoduo.base.a.a.a("ConcernRingList", "hasmore is false, mNewRefreshData 与mOldRefreshData 合并");
                                com.shoujiduoduo.base.a.a.a("ConcernRingList", "合并后大小， Old list size:" + f.this.f1319a.size());
                                if (f.this.f1319a.size() > 0) {
                                    ListContent listContent3 = new ListContent();
                                    listContent3.hasMore = f.this.f;
                                    listContent3.data = f.this.f1319a;
                                    f.this.j.a(listContent3);
                                    boolean unused10 = f.this.i = false;
                                    com.shoujiduoduo.base.a.a.a("ConcernRingList", "write cache, cache size:" + f.this.f1319a.size());
                                }
                            }
                        } else if (listContent2.hasMore) {
                            f.this.f1319a.clear();
                            f.this.f1319a.addAll(f.this.b);
                            f.this.b.clear();
                            boolean unused11 = f.this.f = f.this.e;
                            a unused12 = f.this.k = a.old_refresh;
                            com.shoujiduoduo.base.a.a.a("ConcernRingList", "之前更新了最新数据，但是数据没有和老的数据衔接，则抛弃老的数据, 更新一次缓存文件");
                            if (f.this.f1319a.size() > 0) {
                                ListContent listContent4 = new ListContent();
                                listContent4.hasMore = f.this.f;
                                listContent4.data = f.this.f1319a;
                                f.this.j.a(listContent4);
                            }
                        } else {
                            f.this.b.addAll(0, listContent2.data);
                            a unused13 = f.this.k = a.new_refresh;
                            com.shoujiduoduo.base.a.a.a("ConcernRingList", "无更多数据，更新mNewRefreshData, 将新数据放在mNewRefreshData头部");
                        }
                        boolean unused14 = f.this.e = listContent2.hasMore;
                    }
                    boolean unused15 = f.this.d = false;
                    boolean unused16 = f.this.g = false;
                    break;
                case 6:
                    ListContent listContent5 = (ListContent) message.obj;
                    if (listContent5 != null) {
                        com.shoujiduoduo.base.a.a.a("ConcernRingList", "get new data more size = " + listContent5.data.size() + ", hasmore:" + listContent5.hasMore);
                        if (listContent5.data.size() > 0) {
                            com.shoujiduoduo.base.a.a.a("ConcernRingList", "begin:" + ((RingData) listContent5.data.get(listContent5.data.size() - 1)).date + ", end:" + ((RingData) listContent5.data.get(0)).date);
                        }
                        f.this.b.addAll(listContent5.data);
                        if (listContent5.hasMore) {
                            a unused17 = f.this.k = a.new_refresh;
                            com.shoujiduoduo.base.a.a.a("ConcernRingList", "hasmore is true, 数据添加至mNewRefreshData");
                        } else {
                            a unused18 = f.this.k = a.old_refresh;
                            f.this.f1319a.addAll(0, f.this.b);
                            f.this.b.clear();
                            com.shoujiduoduo.base.a.a.a("ConcernRingList", "hasmore is false, mNewRefreshData 与mOldRefreshData 合并");
                            com.shoujiduoduo.base.a.a.a("ConcernRingList", "合并后大小， mOldRefreshData size:" + f.this.f1319a.size());
                            if (f.this.f1319a.size() > 0) {
                                ListContent listContent6 = new ListContent();
                                listContent6.hasMore = f.this.f;
                                listContent6.data = f.this.f1319a;
                                f.this.j.a(listContent6);
                                boolean unused19 = f.this.i = false;
                                com.shoujiduoduo.base.a.a.a("ConcernRingList", "write cache, cache size:" + f.this.f1319a.size());
                            }
                        }
                        boolean unused20 = f.this.e = listContent5.hasMore;
                    }
                    boolean unused21 = f.this.d = false;
                    boolean unused22 = f.this.g = false;
                    break;
            }
            final int i = message.what;
            c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1284a).a(f.this, i);
                }
            });
        }
    };

    /* compiled from: ConcernRingList */
    private enum a {
        new_refresh,
        old_refresh
    }

    /* compiled from: ConcernRingList */
    public enum b {
        concern_work,
        message_feeds
    }

    public f(String str, b bVar) {
        this.j = new k("concern_ring_" + str + ".tmp");
        this.c = str;
        this.l = bVar;
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_APP, this.m);
    }

    public Object get(int i2) {
        if (this.k == a.new_refresh) {
            if (this.b.size() > 0) {
                return this.b.get(i2);
            }
        } else if (this.f1319a.size() > 0) {
            return this.f1319a.get(i2);
        }
        return null;
    }

    public String getBaseURL() {
        return null;
    }

    public String getListId() {
        return "concern_ring_" + this.c;
    }

    public ListType.LIST_TYPE getListType() {
        return ListType.LIST_TYPE.list_ring_concern;
    }

    public int size() {
        if (this.k == a.new_refresh) {
            return this.b.size();
        }
        return this.f1319a.size();
    }

    public boolean isRetrieving() {
        return this.d;
    }

    public void retrieveData() {
        if (this.k == a.old_refresh) {
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "retrieveData, old refresh type");
            if (this.f1319a == null || this.f1319a.size() == 0) {
                this.d = true;
                this.g = false;
                this.h = false;
                i.a(new Runnable() {
                    public void run() {
                        com.shoujiduoduo.base.a.a.a("ConcernRingList", "retrieveData, old refresh type, getListData");
                        f.this.c();
                    }
                });
            } else if (this.f) {
                this.d = true;
                this.g = false;
                this.h = false;
                i.a(new Runnable() {
                    public void run() {
                        com.shoujiduoduo.base.a.a.a("ConcernRingList", "retrieveData, old refresh type, getListDataMore");
                        f.this.d();
                    }
                });
            }
        } else {
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "retrieveData, new refresh type");
            if (this.b == null || this.b.size() == 0) {
                this.d = true;
                this.g = false;
                this.h = false;
                i.a(new Runnable() {
                    public void run() {
                        com.shoujiduoduo.base.a.a.a("ConcernRingList", "retrieveData, new refresh type, getNewListData");
                        f.this.a();
                    }
                });
            } else if (this.e) {
                this.d = true;
                this.g = false;
                this.h = false;
                i.a(new Runnable() {
                    public void run() {
                        com.shoujiduoduo.base.a.a.a("ConcernRingList", "retrieveData, new refresh type, getNewListDataMore");
                        f.this.b();
                    }
                });
            }
        }
    }

    public void refreshData() {
        this.d = true;
        this.g = false;
        this.h = false;
        i.a(new Runnable() {
            public void run() {
                f.this.a();
            }
        });
    }

    /* access modifiers changed from: private */
    public void a() {
        String str = "";
        if (this.b.size() > 0) {
            str = this.b.get(0).date;
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "getNewListData, tBegin from new refresh data:" + str);
        } else if (this.f1319a.size() > 0) {
            str = this.f1319a.get(0).date;
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "getNewListData, tBegin from old refresh data:" + str);
        }
        String a2 = a(str, "");
        if (ai.c(a2)) {
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "RingList: httpGetRingList Failed!");
            this.n.sendEmptyMessage(4);
            return;
        }
        ListContent<RingData> a3 = j.a(new ByteArrayInputStream(a2.getBytes()));
        if (a3 == null) {
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "RingList:parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
            this.n.sendEmptyMessage(4);
        } else if (a3.data.size() > 0) {
            this.n.sendMessage(this.n.obtainMessage(5, a3));
        } else {
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "refresh data size is 0, no new data");
            this.n.sendEmptyMessage(3);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        String str;
        if (this.f1319a == null || this.f1319a.size() <= 0) {
            str = "";
        } else {
            str = this.f1319a.get(0).date;
        }
        String str2 = "";
        if (this.b != null && this.b.size() > 0) {
            str2 = this.b.get(this.b.size() - 1).date;
        }
        com.shoujiduoduo.base.a.a.a("ConcernRingList", "getNewListDataMore, tBegin:" + str + ", tEnd:" + str2);
        String a2 = a(str, str2);
        if (ai.c(a2)) {
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "RingList: httpGetRingList Failed!");
            this.n.sendEmptyMessage(4);
            return;
        }
        ListContent<RingData> a3 = j.a(new ByteArrayInputStream(a2.getBytes()));
        if (a3 != null) {
            this.n.sendMessage(this.n.obtainMessage(6, a3));
            return;
        }
        com.shoujiduoduo.base.a.a.a("ConcernRingList", "RingList:parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.n.sendEmptyMessage(4);
    }

    /* access modifiers changed from: private */
    public void c() {
        if (!this.j.a(345600000)) {
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "CollectList: cache is available! Use Cache!");
            ListContent<RingData> b2 = this.j.b();
            if (!(b2 == null || b2.data == null || b2.data.size() <= 0)) {
                com.shoujiduoduo.base.a.a.a("ConcernRingList", "RingList: Read RingList Cache Success!");
                this.n.sendMessage(this.n.obtainMessage(0, b2));
                return;
            }
        }
        com.shoujiduoduo.base.a.a.a("ConcernRingList", "cache is out of date or read cache failed!");
        com.shoujiduoduo.base.a.a.a("ConcernRingList", "get data, tBegin:, tEnd:");
        String a2 = a("", "");
        if (ai.c(a2)) {
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "RingList: httpGetRingList Failed!");
            this.n.sendEmptyMessage(1);
            return;
        }
        ListContent<RingData> a3 = j.a(new ByteArrayInputStream(a2.getBytes()));
        if (a3 != null) {
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "list data size = " + a3.data.size());
            this.i = true;
            this.n.sendMessage(this.n.obtainMessage(0, a3));
            return;
        }
        com.shoujiduoduo.base.a.a.a("ConcernRingList", "RingList:parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.n.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void d() {
        ListContent<RingData> listContent;
        com.shoujiduoduo.base.a.a.a("ConcernRingList", "retrieving more data, list size = " + this.f1319a.size());
        RingData ringData = this.f1319a.get(this.f1319a.size() - 1);
        com.shoujiduoduo.base.a.a.a("ConcernRingList", "get more data, tend:" + ringData.date);
        String a2 = a("", ringData.date);
        if (ai.c(a2)) {
            this.n.sendEmptyMessage(2);
            return;
        }
        try {
            listContent = j.a(new ByteArrayInputStream(a2.getBytes()));
        } catch (ArrayIndexOutOfBoundsException e2) {
            listContent = null;
        }
        if (listContent != null) {
            com.shoujiduoduo.base.a.a.a("ConcernRingList", "list data size = " + listContent.data.size());
            this.i = true;
            this.n.sendMessage(this.n.obtainMessage(0, listContent));
            return;
        }
        this.n.sendEmptyMessage(2);
    }

    private String a(String str, String str2) {
        return u.a("concerned", "&pagesize=25&tb=" + str + "&te=" + str2 + "&uid=" + com.shoujiduoduo.a.b.b.g().f());
    }

    public void reloadData() {
        com.shoujiduoduo.base.a.a.a("ConcernRingList", "reloaddata");
        this.d = true;
        this.g = false;
        this.h = true;
        if (this.j != null) {
            this.j.a();
        }
        i.a(new Runnable() {
            public void run() {
                f.this.c();
            }
        });
    }

    public boolean hasMoreData() {
        if (this.k == a.old_refresh) {
            return this.f;
        }
        return this.e;
    }
}
