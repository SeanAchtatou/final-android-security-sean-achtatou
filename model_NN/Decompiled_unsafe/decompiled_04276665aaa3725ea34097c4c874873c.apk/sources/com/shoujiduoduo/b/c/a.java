package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.bean.ArtistData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.u;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/* compiled from: ArtistList */
public class a implements DDList {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public b f1306a = null;
    private String b;
    /* access modifiers changed from: private */
    public boolean c = true;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public int f = -1;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public ArrayList<ArtistData> h = new ArrayList<>();
    private Handler i = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ListContent listContent = (ListContent) message.obj;
                    if (listContent != null) {
                        com.shoujiduoduo.base.a.a.a("RingList", "new obtained list data size = " + listContent.data.size());
                        if (a.this.h == null) {
                            ArrayList unused = a.this.h = listContent.data;
                        } else {
                            a.this.h.addAll(listContent.data);
                        }
                        boolean unused2 = a.this.c = listContent.hasMore;
                        listContent.data = a.this.h;
                        if (a.this.f >= 0) {
                            listContent.page = a.this.f;
                        }
                        if (a.this.g && a.this.h.size() > 0) {
                            a.this.f1306a.a(listContent);
                            boolean unused3 = a.this.g = false;
                        }
                    }
                    boolean unused4 = a.this.d = false;
                    boolean unused5 = a.this.e = false;
                    break;
                case 1:
                case 2:
                    boolean unused6 = a.this.d = false;
                    boolean unused7 = a.this.e = true;
                    break;
            }
            final int i = message.what;
            c.a().a(b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1284a).a(a.this, i);
                }
            });
        }
    };

    public a(String str) {
        this.f1306a = new b("artist_" + str + ".tmp");
        this.b = str;
    }

    public String getBaseURL() {
        return null;
    }

    public String getListId() {
        return "artist";
    }

    public void retrieveData() {
        if (this.h == null || this.h.size() == 0) {
            this.d = true;
            this.e = false;
            i.a(new Runnable() {
                public void run() {
                    a.this.a();
                }
            });
        } else if (this.c) {
            this.d = true;
            this.e = false;
            i.a(new Runnable() {
                public void run() {
                    a.this.b();
                }
            });
        }
    }

    public void refreshData() {
    }

    /* access modifiers changed from: private */
    public void a() {
        if (!this.f1306a.a(14400000)) {
            com.shoujiduoduo.base.a.a.a("ArtistList", "CollectList: cache is available! Use Cache!");
            ListContent<ArtistData> a2 = this.f1306a.a();
            if (!(a2 == null || a2.data == null || a2.data.size() <= 0)) {
                com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: Read RingList Cache Success!");
                this.f = a2.page;
                this.i.sendMessage(this.i.obtainMessage(0, a2));
                return;
            }
        }
        com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: cache is out of date or read cache failed!");
        String b2 = b(0);
        if (ai.c(b2)) {
            com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: httpGetRingList Failed!");
            this.i.sendEmptyMessage(1);
            return;
        }
        ListContent<ArtistData> d2 = j.d(new ByteArrayInputStream(b2.getBytes()));
        if (d2 != null) {
            com.shoujiduoduo.base.a.a.a("ArtistList", "list data size = " + d2.data.size());
            com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.g = true;
            this.f = 0;
            this.i.sendMessage(this.i.obtainMessage(0, d2));
            return;
        }
        com.shoujiduoduo.base.a.a.a("ArtistList", "RingList:parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.i.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void b() {
        int i2;
        ListContent<ArtistData> listContent;
        com.shoujiduoduo.base.a.a.a("ArtistList", "retrieving more data, list size = " + this.h.size());
        if (this.f < 0) {
            i2 = this.h.size() / 25;
            com.shoujiduoduo.base.a.a.a("ArtistList", "没有cache current page 记录，通过list size 计算页数， 下一页，page：" + i2);
        } else {
            i2 = this.f + 1;
            com.shoujiduoduo.base.a.a.a("ArtistList", "有 cache current page 记录，下一页，page：" + i2);
        }
        String b2 = b(i2);
        if (ai.c(b2)) {
            this.i.sendEmptyMessage(2);
            return;
        }
        try {
            listContent = j.d(new ByteArrayInputStream(b2.getBytes()));
        } catch (ArrayIndexOutOfBoundsException e2) {
            listContent = null;
        }
        if (listContent != null) {
            com.shoujiduoduo.base.a.a.a("RingList", "list data size = " + listContent.data.size());
            this.g = true;
            this.f = i2;
            this.i.sendMessage(this.i.obtainMessage(0, listContent));
            return;
        }
        this.i.sendEmptyMessage(2);
    }

    private String b(int i2) {
        return u.a("hotartist", "&page=" + i2 + "&pagesize=" + 25 + "&listid=" + this.b);
    }

    public boolean isRetrieving() {
        return this.d;
    }

    /* renamed from: a */
    public ArtistData get(int i2) {
        if (i2 < 0 || i2 >= this.h.size()) {
            return null;
        }
        return this.h.get(i2);
    }

    public int size() {
        return this.h.size();
    }

    public boolean hasMoreData() {
        return this.c;
    }

    public ListType.LIST_TYPE getListType() {
        return ListType.LIST_TYPE.list_artist;
    }

    public void reloadData() {
    }
}
