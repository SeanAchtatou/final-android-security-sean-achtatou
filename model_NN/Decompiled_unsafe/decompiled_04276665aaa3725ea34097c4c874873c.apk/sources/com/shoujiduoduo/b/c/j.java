package com.shoujiduoduo.b.c;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.cmsc.cmmusic.common.data.GetUserInfoRsp;
import com.igexin.download.Downloads;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.aj;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.u;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/* compiled from: RingList */
public class j implements DDList {

    /* renamed from: a  reason: collision with root package name */
    private long f1341a = 14400000;
    private int b = 0;
    private boolean c = false;
    private String d = "";
    /* access modifiers changed from: private */
    public int e;
    private aj f;
    private String g = "";
    private boolean h = true;
    /* access modifiers changed from: private */
    public boolean i = true;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public ListType.LIST_TYPE n;
    /* access modifiers changed from: private */
    public k o = null;
    /* access modifiers changed from: private */
    public String p;
    private String q;
    private String r;
    /* access modifiers changed from: private */
    public String s = null;
    /* access modifiers changed from: private */
    public ArrayList<RingData> t = new ArrayList<>();
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v = -1;
    private Handler w = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.b.c.j.b(com.shoujiduoduo.b.c.j, boolean):boolean
         arg types: [com.shoujiduoduo.b.c.j, int]
         candidates:
          com.shoujiduoduo.b.c.j.b(com.shoujiduoduo.b.c.j, java.lang.String):java.lang.String
          com.shoujiduoduo.b.c.j.b(com.shoujiduoduo.b.c.j, boolean):boolean */
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ListContent listContent = (ListContent) message.obj;
                    if (listContent != null) {
                        a.a("RingList", "new obtained list data size = " + listContent.data.size() + ", listid:" + j.this.e);
                        if (j.this.m) {
                            j.this.t.clear();
                            j.this.t.addAll(listContent.data);
                        } else if (j.this.t == null) {
                            ArrayList unused = j.this.t = listContent.data;
                        } else {
                            j.this.t.addAll(listContent.data);
                        }
                        String unused2 = j.this.p = listContent.area;
                        boolean unused3 = j.this.i = listContent.hasMore;
                        String unused4 = j.this.s = listContent.baseURL;
                        listContent.data = j.this.t;
                        if (j.this.v >= 0) {
                            listContent.page = j.this.v;
                        }
                        if (j.this.l && j.this.t.size() > 0) {
                            if (j.this.o != null) {
                                if (j.this.u <= 0) {
                                    j.this.o.a(listContent);
                                } else if (j.this.t.size() <= j.this.u) {
                                    j.this.o.a(listContent);
                                } else {
                                    a.a("RingList", "超过最大缓存限制，不缓存了， maxsize：" + j.this.u + ", datasize:" + j.this.t.size());
                                }
                            }
                            boolean unused5 = j.this.l = false;
                        }
                    } else {
                        j.this.t.clear();
                    }
                    boolean unused6 = j.this.j = false;
                    boolean unused7 = j.this.k = false;
                    break;
                case 1:
                case 2:
                    boolean unused8 = j.this.j = false;
                    boolean unused9 = j.this.k = true;
                    break;
            }
            final int i = message.what;
            c.a().a(b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1284a).a(j.this, i);
                }
            });
        }
    };

    public j(ListType.LIST_TYPE list_type, String str, String str2) {
        this.q = str;
        this.g = str2;
        this.n = list_type;
        this.o = new k(this.q.hashCode() + ".tmp");
    }

    public j(ListType.LIST_TYPE list_type) {
        this.n = list_type;
    }

    public j(ListType.LIST_TYPE list_type, String str, boolean z) {
        this.n = list_type;
        this.r = str;
        this.h = z;
        if (this.h) {
            a(list_type, str, false, "");
        }
    }

    public j(ListType.LIST_TYPE list_type, String str, boolean z, String str2) {
        this.n = list_type;
        this.e = s.a(str, 0);
        this.c = z;
        this.d = str2;
        a(list_type, str, z, str2);
    }

    private void a(ListType.LIST_TYPE list_type, String str, boolean z, String str2) {
        if (list_type == ListType.LIST_TYPE.list_ring_normal) {
            if (!TextUtils.isEmpty(str2)) {
                if (z) {
                    this.b = new Random().nextInt(100) + 1;
                    this.o = new k("list_" + str + "_" + str2 + "_" + this.b + ".tmp");
                    return;
                }
                this.o = new k("list_" + str + "_" + str2 + ".tmp");
            } else if (z) {
                this.b = new Random().nextInt(100) + 1;
                this.o = new k("list_" + str + "_" + this.b + ".tmp");
            } else {
                this.o = new k("list_" + str + ".tmp");
            }
        } else if (list_type == ListType.LIST_TYPE.list_ring_cmcc) {
            this.o = new k("cmcc_cailing.tmp");
        } else if (list_type == ListType.LIST_TYPE.list_ring_ctcc) {
            this.o = new k("ctcc_cailing.tmp");
        } else if (list_type == ListType.LIST_TYPE.list_ring_cucc) {
            this.o = new k("cucc_cailing.tmp");
        } else if (list_type == ListType.LIST_TYPE.list_ring_collect) {
            this.o = new k("collect_" + str + ".tmp");
        } else if (list_type == ListType.LIST_TYPE.list_ring_artist) {
            this.o = new k("artist_" + str + ".tmp");
        } else if (list_type == ListType.LIST_TYPE.list_ring_user_upload) {
            this.o = new k("user_" + str + ".tmp");
        } else {
            this.o = new k(list_type.toString() + "_" + str + ".tmp");
        }
    }

    public void a(long j2) {
        if (j2 > 0) {
            this.f1341a = j2;
        }
    }

    public String getBaseURL() {
        return this.s;
    }

    public ListType.LIST_TYPE a() {
        return this.n;
    }

    public String getListId() {
        switch (this.n) {
            case list_ring_normal:
            case list_ring_collect:
                return "" + this.e;
            case list_ring_user_upload:
                return "user_ring_" + this.r;
            case list_ring_search:
                return "search_" + this.g;
            case list_ring_cmcc:
                return "cmcc_cailing";
            case list_ring_ctcc:
                return "ctcc_cailing";
            case list_ring_cucc:
                return "cucc_cailing";
            case sys_alarm:
                return "sys_ring_alram";
            case sys_notify:
                return "sys_ring_notify";
            case sys_ringtone:
                return "sys_ring_ringtone";
            default:
                return "unknown";
        }
    }

    public void reloadData() {
        this.j = true;
        this.k = false;
        this.m = true;
        i.a(new Runnable() {
            public void run() {
                switch (AnonymousClass7.f1346a[j.this.n.ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 11:
                        j.this.b();
                        return;
                    case 5:
                        j.this.d();
                        return;
                    case 6:
                        j.this.f();
                        return;
                    case 7:
                        j.this.e();
                        return;
                    case 8:
                    case 9:
                    case 10:
                        j.this.g();
                        return;
                    default:
                        return;
                }
            }
        });
    }

    public void retrieveData() {
        if (this.t == null || this.t.size() == 0) {
            this.j = true;
            this.k = false;
            this.m = false;
            i.a(new Runnable() {
                public void run() {
                    switch (AnonymousClass7.f1346a[j.this.n.ordinal()]) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 11:
                            j.this.b();
                            return;
                        case 5:
                            j.this.d();
                            return;
                        case 6:
                            j.this.f();
                            return;
                        case 7:
                            j.this.e();
                            return;
                        case 8:
                        case 9:
                        case 10:
                            j.this.g();
                            return;
                        default:
                            return;
                    }
                }
            });
        } else if (this.i) {
            this.j = true;
            this.k = false;
            this.m = false;
            i.a(new Runnable() {
                public void run() {
                    j.this.c();
                }
            });
        }
    }

    public void refreshData() {
    }

    /* access modifiers changed from: private */
    public void b() {
        if (this.o != null && !this.o.a(this.f1341a)) {
            a.a("RingList", "RingList: cache is available! Use Cache!, listid:" + this.e);
            ListContent<RingData> b2 = this.o.b();
            if (!(b2 == null || b2.data == null || b2.data.size() <= 0)) {
                a.a("RingList", "RingList: Read RingList Cache Success!");
                this.v = b2.page;
                this.w.sendMessage(this.w.obtainMessage(0, b2));
                return;
            }
        }
        a.a("RingList", "RingList: cache is out of date or read cache failed!");
        String b3 = b(0);
        if (ai.c(b3)) {
            a.a("RingList", "RingList: httpGetRingList Failed!, listid:" + getListId());
            this.w.sendEmptyMessage(1);
            return;
        }
        ListContent<RingData> f2 = com.shoujiduoduo.util.j.f(new ByteArrayInputStream(b3.getBytes()));
        if (f2 != null) {
            a.a("RingList", "list data size = " + f2.data.size() + ", listid:" + getListId());
            a.a("RingList", "RingList: send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.l = true;
            this.v = 0;
            this.w.sendMessage(this.w.obtainMessage(0, f2));
            return;
        }
        a.a("RingList", "RingList: but parse content FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.w.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void c() {
        int i2;
        ListContent<RingData> listContent;
        a.a("RingList", "retrieving more data, list size = " + this.t.size());
        if (this.v < 0) {
            i2 = this.t.size() / 25;
            a.a("RingList", "没有cache current page 记录，通过list size 计算页数， 下一页，page：" + i2);
        } else {
            i2 = this.v + 1;
            a.a("RingList", "有 cache current page 记录，下一页，page：" + i2);
        }
        String b2 = b(i2);
        if (ai.c(b2)) {
            this.w.sendEmptyMessage(2);
            return;
        }
        try {
            listContent = com.shoujiduoduo.util.j.f(new ByteArrayInputStream(b2.getBytes()));
        } catch (ArrayIndexOutOfBoundsException e2) {
            listContent = null;
        }
        if (listContent != null) {
            a.a("RingList", "list data size = " + listContent.data.size());
            this.l = true;
            this.v = i2;
            this.w.sendMessage(this.w.obtainMessage(0, listContent));
            return;
        }
        this.w.sendEmptyMessage(2);
    }

    /* access modifiers changed from: private */
    public void d() {
        c.b bVar;
        a.a("RingList", "彩铃，查询个人铃音库, type:" + this.n.toString());
        if (com.shoujiduoduo.util.c.b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c("RingList", "main activity is destroyed");
            return;
        }
        if (this.o != null && !this.o.a(this.f1341a) && af.a((Context) null, "NeedUpdateCaiLingLib", 1) == 0) {
            a.a("RingList", "RingList: cache is available! Use Cache!");
            ListContent<RingData> b2 = this.o.b();
            if (b2 != null) {
                a.a("RingList", "RingList: Read RingList Cache Success!");
                this.w.sendMessage(this.w.obtainMessage(0, b2));
                return;
            }
        }
        a.a("RingList", "RingList: cache is out of date or cache failed!");
        c.b c2 = com.shoujiduoduo.util.c.b.a().c("");
        if (c2 == null) {
            a.e("RingList", "查询彩铃功能请求失败");
            this.w.sendEmptyMessage(1);
        } else if (c2.a().equals("000000")) {
            a.a("RingList", "彩铃功能已开通");
            if (this.n == ListType.LIST_TYPE.list_ring_cmcc) {
                bVar = com.shoujiduoduo.util.c.b.a().f();
            } else {
                bVar = null;
            }
            if (com.shoujiduoduo.util.c.b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                a.c("RingList", "main activity is destroyed 2");
            } else if (bVar == null) {
                a.c("RingList", "查询失败，cRsp == null");
                this.w.sendEmptyMessage(1);
            } else if (bVar.a() != null && bVar.a().equals(GetUserInfoRsp.NON_MEM_ERROR_CODE)) {
                a.a("RingList", "查询失败，用户非彩铃用户，提示开通咪咕特级会员");
                com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                    public void a() {
                        ((com.shoujiduoduo.a.c.c) this.f1284a).a(false, g.b.f2309a);
                    }
                });
                this.w.sendEmptyMessage(1);
            } else if (bVar.a() == null || ((!bVar.a().equals("000000") && !bVar.a().equals("0000")) || !(bVar instanceof c.z))) {
                this.w.sendEmptyMessage(1);
                a.c("RingList", "查询失败，res：" + bVar.b());
            } else {
                a.a("RingList", "查询成功");
                List<c.ae> d2 = ((c.z) bVar).d();
                if (d2 == null || d2.size() == 0) {
                    a.a("RingList", "userToneInfo == null");
                    this.w.sendMessage(this.w.obtainMessage(0, null));
                    return;
                }
                ListContent listContent = new ListContent();
                ArrayList<T> arrayList = new ArrayList<>();
                for (int i2 = 0; i2 < d2.size(); i2++) {
                    RingData ringData = new RingData();
                    ringData.name = d2.get(i2).c();
                    ringData.artist = d2.get(i2).d();
                    if (this.n == ListType.LIST_TYPE.list_ring_cmcc) {
                        ringData.price = s.a(d2.get(i2).a(), Downloads.STATUS_SUCCESS);
                        ringData.cid = d2.get(i2).b();
                        a.a("RingList", "用户铃音库：name:" + ringData.name + " 彩铃id：" + ringData.cid);
                        String e2 = d2.get(i2).e();
                        try {
                            if (e2.indexOf(" ") > 0) {
                                ringData.valid = e2.substring(0, e2.indexOf(" "));
                            } else {
                                ringData.valid = e2;
                            }
                        } catch (Exception e3) {
                            ringData.valid = "";
                            e3.printStackTrace();
                        }
                        ringData.hasmedia = 0;
                        if (com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                            a.c("RingList", "main activity is destroyed 3");
                            return;
                        }
                        ringData.rid = com.shoujiduoduo.util.c.a.a((Context) null).a(ringData.cid);
                    }
                    arrayList.add(ringData);
                }
                listContent.data = arrayList;
                listContent.hasMore = false;
                this.l = true;
                af.b((Context) null, "NeedUpdateCaiLingLib", 0);
                this.w.sendMessage(this.w.obtainMessage(0, listContent));
            }
        } else {
            com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                public void a() {
                    ((com.shoujiduoduo.a.c.c) this.f1284a).a(false, g.b.f2309a);
                }
            });
            this.w.sendEmptyMessage(1);
            a.e("RingList", "彩铃功能尚未开通");
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        a.a("RingList", "彩铃，查询个人铃音库, type:" + this.n.toString());
        if (com.shoujiduoduo.util.e.a.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c("RingList", "main activity is destroyed");
            return;
        }
        if (this.o != null && !this.o.a(this.f1341a) && af.a((Context) null, "NeedUpdateCaiLingLib", 1) == 0) {
            a.a("RingList", "RingList: cache is available! Use Cache!");
            ListContent<RingData> b2 = this.o.b();
            if (b2 != null) {
                a.a("RingList", "RingList: Read RingList Cache Success!");
                this.w.sendMessage(this.w.obtainMessage(0, b2));
                return;
            }
        }
        a.a("RingList", "RingList: cache is out of date or cache failed!");
        c.b d2 = com.shoujiduoduo.util.e.a.a().d();
        if (d2 == null) {
            a.c("RingList", "查询失败，cRsp == null");
            this.w.sendEmptyMessage(1);
        } else if ((d2 instanceof c.u) && d2.a().equals("000000")) {
            a.a("RingList", "查询成功");
            c.ab[] abVarArr = ((c.u) d2).d;
            if (abVarArr == null || abVarArr.length == 0) {
                a.a("RingList", "userToneInfo == null");
                this.w.sendMessage(this.w.obtainMessage(0, null));
                return;
            }
            ListContent listContent = new ListContent();
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i2 = 0; i2 < abVarArr.length; i2++) {
                RingData ringData = new RingData();
                ringData.name = abVarArr[i2].b;
                ringData.artist = abVarArr[i2].d;
                if (this.n == ListType.LIST_TYPE.list_ring_cucc) {
                    ringData.cucid = abVarArr[i2].f2222a;
                    a.a("RingList", "用户铃音库：name:" + ringData.name + " 彩铃id：" + ringData.cucid);
                    String str = abVarArr[i2].g;
                    try {
                        if (str.indexOf(" ") > 0) {
                            ringData.cuvalid = str.substring(0, str.indexOf(" "));
                        } else {
                            ringData.cuvalid = str;
                        }
                    } catch (Exception e2) {
                        ringData.cuvalid = "";
                        e2.printStackTrace();
                    }
                    if (com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                        a.c("RingList", "main activity is destroyed 3");
                        return;
                    }
                    ringData.rid = com.shoujiduoduo.util.c.a.a((Context) null).a(ringData.cucid);
                }
                arrayList.add(ringData);
            }
            listContent.data = arrayList;
            listContent.hasMore = false;
            this.l = true;
            af.b((Context) null, "NeedUpdateCaiLingLib", 0);
            this.w.sendMessage(this.w.obtainMessage(0, listContent));
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        c.b bVar;
        a.a("RingList", "彩铃，查询个人铃音库, type:" + this.n.toString());
        if (com.shoujiduoduo.util.d.b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c("RingList", "main activity is destroyed");
            return;
        }
        if (this.o != null && !this.o.a(this.f1341a) && af.a((Context) null, "NeedUpdateCaiLingLib", 1) == 0) {
            a.a("RingList", "RingList: cache is available! Use Cache!");
            ListContent<RingData> b2 = this.o.b();
            if (b2 != null) {
                a.a("RingList", "RingList: Read RingList Cache Success!");
                this.w.sendMessage(this.w.obtainMessage(0, b2));
                return;
            }
        }
        a.a("RingList", "RingList: cache is out of date or cache failed!");
        String a2 = af.a(RingDDApp.c(), "pref_phone_num", "");
        if (this.n == ListType.LIST_TYPE.list_ring_ctcc) {
            bVar = com.shoujiduoduo.util.d.b.a().c(a2);
        } else {
            bVar = null;
        }
        if (com.shoujiduoduo.util.d.b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c("RingList", "main activity is destroyed 2");
        } else if (bVar == null) {
            a.c("RingList", "查询失败，cRsp == null");
            this.w.sendEmptyMessage(1);
        } else if (bVar.a() == null || ((!bVar.a().equals("000000") && !bVar.a().equals("0000")) || !(bVar instanceof c.z))) {
            this.w.sendEmptyMessage(1);
            a.c("RingList", "查询失败，res：" + bVar.b());
        } else {
            a.a("RingList", "查询成功");
            List<c.ae> d2 = ((c.z) bVar).d();
            if (d2 == null || d2.size() == 0) {
                a.a("RingList", "userToneInfo == null");
                this.w.sendMessage(this.w.obtainMessage(0, null));
                return;
            }
            ListContent listContent = new ListContent();
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i2 = 0; i2 < d2.size(); i2++) {
                RingData ringData = new RingData();
                ringData.name = d2.get(i2).c();
                ringData.artist = d2.get(i2).d();
                if (this.n == ListType.LIST_TYPE.list_ring_ctcc) {
                    ringData.ctcid = d2.get(i2).b();
                    a.a("RingList", "用户铃音库：name:" + ringData.name + " 彩铃id：" + ringData.ctcid);
                    String e2 = d2.get(i2).e();
                    try {
                        if (e2.indexOf(" ") > 0) {
                            ringData.ctvalid = e2.substring(0, e2.indexOf(" "));
                        } else {
                            ringData.ctvalid = e2;
                        }
                    } catch (Exception e3) {
                        ringData.ctvalid = "";
                        e3.printStackTrace();
                    }
                    ringData.cthasmedia = 0;
                    if (com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                        a.c("RingList", "main activity is destroyed 3");
                        return;
                    }
                    ringData.rid = com.shoujiduoduo.util.c.a.a((Context) null).a(ringData.ctcid);
                }
                arrayList.add(ringData);
            }
            listContent.data = arrayList;
            listContent.hasMore = false;
            this.l = true;
            af.b((Context) null, "NeedUpdateCaiLingLib", 0);
            this.w.sendMessage(this.w.obtainMessage(0, listContent));
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        int i2;
        switch (this.n) {
            case sys_alarm:
                i2 = 4;
                break;
            case sys_notify:
                i2 = 2;
                break;
            case sys_ringtone:
                i2 = 1;
                break;
            default:
                i2 = 0;
                break;
        }
        if (this.f == null) {
            this.f = new aj(RingDDApp.c());
        }
        List<aj.a> a2 = this.f.a(i2, true);
        if (a2 == null) {
            this.w.sendEmptyMessage(1);
            a.c("RingList", "get system ring error");
            return;
        }
        ArrayList<T> arrayList = new ArrayList<>();
        for (aj.a next : a2) {
            RingData ringData = new RingData();
            ringData.localPath = next.f2194a;
            ringData.name = next.b;
            ringData.duration = next.c / 1000 == 0 ? 1 : next.c / 1000;
            arrayList.add(ringData);
        }
        if (arrayList.size() > 0) {
            ListContent listContent = new ListContent();
            listContent.data = arrayList;
            this.w.sendMessage(this.w.obtainMessage(0, listContent));
            return;
        }
        this.w.sendEmptyMessage(1);
        a.c("RingList", "get system ring error");
    }

    private String b(int i2) {
        switch (this.n) {
            case list_ring_normal:
                String str = "&listid=" + this.e + "&page=" + i2 + "&pagesize=" + 25;
                if (!TextUtils.isEmpty(this.d)) {
                    if (this.c) {
                        str = str + "&area=" + this.d + "&ranid=" + this.b;
                    } else {
                        str = str + "&area=" + this.d;
                    }
                } else if (this.c) {
                    str = str + "&ranid=" + this.b;
                }
                return u.d(str);
            case list_ring_collect:
                return u.a("getlist&iscol=1", "&page=" + i2 + "&pagesize=" + 25 + "&listid=" + this.e);
            case list_ring_user_upload:
                return u.a("getuserringlist", "&uid=" + com.shoujiduoduo.a.b.b.g().c().getUid() + "&tuid=" + this.r + "&page=" + i2 + "&pagesize=" + 25);
            case list_ring_search:
                return u.a(this.q, this.g, i2, 25);
            case list_ring_cmcc:
            case list_ring_ctcc:
            case list_ring_cucc:
            case sys_alarm:
            case sys_notify:
            case sys_ringtone:
            default:
                String str2 = "&listid=" + this.e + "&page=" + i2 + "&pagesize=" + 25;
                if (this.c) {
                    str2 = str2 + "&ranid=" + this.b;
                }
                return u.d(str2);
            case list_ring_artist:
                return u.a("getlist", "&page=" + i2 + "&pagesize=" + 25 + "&artistid=" + this.e);
        }
    }

    public boolean hasMoreData() {
        return this.i;
    }

    public boolean isRetrieving() {
        return this.j;
    }

    /* renamed from: a */
    public RingData get(int i2) {
        if (i2 < 0 || i2 >= this.t.size()) {
            return null;
        }
        return this.t.get(i2);
    }

    public int size() {
        return this.t.size();
    }

    public ListType.LIST_TYPE getListType() {
        return this.n;
    }
}
