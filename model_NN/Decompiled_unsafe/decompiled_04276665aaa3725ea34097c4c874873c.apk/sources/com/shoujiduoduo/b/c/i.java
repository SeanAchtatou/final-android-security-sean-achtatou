package com.shoujiduoduo.b.c;

import com.a.a.e;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.MessageData;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.r;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/* compiled from: MessageListCache */
public class i extends q<ListContent<MessageData>> {
    public i(String str) {
        super(str);
    }

    public void a(ListContent<MessageData> listContent) {
        ArrayList<T> arrayList = listContent.data;
        if (arrayList != null && arrayList.size() > 0) {
            try {
                r.b(c + this.b, "{\"hasmore\":" + (listContent.hasMore ? "1" : "0") + MiPushClient.ACCEPT_TIME_SEPARATOR + "\"list\":" + new e().a(arrayList) + "}");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public ListContent<MessageData> a() {
        try {
            return j.c(new FileInputStream(c + this.b));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
