package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.CommentData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.u;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

/* compiled from: CommentList */
public class e implements DDList {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f1314a = false;
    /* access modifiers changed from: private */
    public boolean b = true;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public boolean d = false;
    private int e = -1;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public ArrayList<CommentData> g = new ArrayList<>();
    private String h;
    private int i;
    private Handler j = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    ListContent listContent = (ListContent) message.obj;
                    if (listContent != null) {
                        a.a("RingList", "new obtained list data size = " + listContent.data.size());
                        if (e.this.f1314a) {
                            e.this.g.clear();
                            e.this.g.addAll(listContent.data);
                        } else if (e.this.g == null) {
                            ArrayList unused = e.this.g = listContent.data;
                        } else {
                            e.this.g.addAll(listContent.data);
                        }
                        boolean unused2 = e.this.b = listContent.hasMore;
                        listContent.data = e.this.g;
                        if (e.this.f && e.this.g.size() > 0) {
                            boolean unused3 = e.this.f = false;
                        }
                    }
                    boolean unused4 = e.this.c = false;
                    boolean unused5 = e.this.d = false;
                    break;
                case 1:
                case 2:
                    boolean unused6 = e.this.c = false;
                    boolean unused7 = e.this.d = true;
                    break;
            }
            final int i = message.what;
            c.a().a(b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1284a).a(e.this, i);
                }
            });
        }
    };

    public e(String str) {
        this.h = str;
    }

    public Object get(int i2) {
        if (i2 >= this.g.size() || i2 < 0) {
            return null;
        }
        return this.g.get(i2);
    }

    public String getBaseURL() {
        return null;
    }

    public String getListId() {
        return "comment_list";
    }

    public ListType.LIST_TYPE getListType() {
        return ListType.LIST_TYPE.list_comment;
    }

    public int size() {
        return this.g.size();
    }

    public boolean isRetrieving() {
        return this.c;
    }

    public void retrieveData() {
        if (this.g == null || this.g.size() == 0) {
            this.c = true;
            this.d = false;
            this.f1314a = false;
            i.a(new Runnable() {
                public void run() {
                    e.this.a();
                }
            });
        } else if (this.b) {
            this.c = true;
            this.d = false;
            this.f1314a = false;
            i.a(new Runnable() {
                public void run() {
                    e.this.b();
                }
            });
        }
    }

    public void refreshData() {
    }

    public void reloadData() {
        this.c = true;
        this.d = false;
        this.f1314a = true;
        i.a(new Runnable() {
            public void run() {
                e.this.a();
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.j.a(java.io.InputStream, boolean):com.shoujiduoduo.base.bean.ListContent<com.shoujiduoduo.base.bean.CommentData>
     arg types: [java.io.ByteArrayInputStream, int]
     candidates:
      com.shoujiduoduo.util.j.a(org.json.JSONObject, com.shoujiduoduo.base.bean.CommentData):void
      com.shoujiduoduo.util.j.a(org.json.JSONObject, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.util.j.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.ArtistData):void
      com.shoujiduoduo.util.j.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.CollectData):void
      com.shoujiduoduo.util.j.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.util.j.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.UserData):void
      com.shoujiduoduo.util.j.a(java.io.InputStream, boolean):com.shoujiduoduo.base.bean.ListContent<com.shoujiduoduo.base.bean.CommentData> */
    /* access modifiers changed from: private */
    public void a() {
        a.a("CommentList", "getListData");
        String a2 = a(0);
        if (ai.c(a2)) {
            a.a("CommentList", "RingList: httpGetRingList Failed!");
            this.j.sendEmptyMessage(1);
            return;
        }
        ListContent<CommentData> a3 = j.a((InputStream) new ByteArrayInputStream(a2.getBytes()), false);
        if (a3 != null) {
            this.i = a3.hotCommentNum;
            a.a("CommentList", "list data size = " + a3.data.size() + ", hotCommentNum:" + this.i);
            a.a("CommentList", "UserList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.f = true;
            this.e = 0;
            this.j.sendMessage(this.j.obtainMessage(0, a3));
            return;
        }
        a.a("CommentList", "RingList: but parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.j.sendEmptyMessage(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.j.a(java.io.InputStream, boolean):com.shoujiduoduo.base.bean.ListContent<com.shoujiduoduo.base.bean.CommentData>
     arg types: [java.io.ByteArrayInputStream, int]
     candidates:
      com.shoujiduoduo.util.j.a(org.json.JSONObject, com.shoujiduoduo.base.bean.CommentData):void
      com.shoujiduoduo.util.j.a(org.json.JSONObject, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.util.j.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.ArtistData):void
      com.shoujiduoduo.util.j.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.CollectData):void
      com.shoujiduoduo.util.j.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.util.j.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.UserData):void
      com.shoujiduoduo.util.j.a(java.io.InputStream, boolean):com.shoujiduoduo.base.bean.ListContent<com.shoujiduoduo.base.bean.CommentData> */
    /* access modifiers changed from: private */
    public void b() {
        int i2;
        ListContent<CommentData> listContent;
        a.a("CommentList", "retrieving more data, list size = " + (this.g.size() - this.i));
        if (this.e < 0) {
            i2 = (this.g.size() - this.i) / 25;
            a.a("CommentList", "没有cache current page 记录，通过list size 计算页数， 下一页，page：" + i2);
        } else {
            i2 = this.e + 1;
            a.a("CommentList", "有 cache current page 记录，下一页，page：" + i2);
        }
        String a2 = a(i2);
        if (ai.c(a2)) {
            this.j.sendEmptyMessage(2);
            return;
        }
        try {
            listContent = j.a((InputStream) new ByteArrayInputStream(a2.getBytes()), true);
        } catch (ArrayIndexOutOfBoundsException e2) {
            listContent = null;
        }
        if (listContent != null) {
            a.a("RingList", "list data size = " + listContent.data.size());
            this.f = true;
            this.e = i2;
            this.j.sendMessage(this.j.obtainMessage(0, listContent));
            return;
        }
        this.j.sendEmptyMessage(2);
    }

    private String a(int i2) {
        return u.a("getcommentlist", "&page=" + i2 + "&pagesize=" + 25 + "&uid=" + com.shoujiduoduo.a.b.b.g().c().getUid() + "&rid=" + this.h);
    }

    public boolean hasMoreData() {
        return this.b;
    }
}
