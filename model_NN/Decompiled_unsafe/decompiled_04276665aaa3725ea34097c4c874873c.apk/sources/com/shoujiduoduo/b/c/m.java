package com.shoujiduoduo.b.c;

import android.util.Xml;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.base.bean.TopListData;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.r;
import com.shoujiduoduo.util.u;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

/* compiled from: TopListMgrImpl */
public class m implements g {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<TopListData> f1348a;
    /* access modifiers changed from: private */
    public a b;
    /* access modifiers changed from: private */
    public boolean c;
    private HashSet<String> d;

    public void a() {
        this.b = new a("toplist.tmp");
        this.d = new HashSet<>();
        this.d.add("list");
        this.d.add("collect");
        this.d.add("artist");
        this.d.add("html");
        e();
    }

    public void b() {
    }

    public boolean c() {
        return this.c;
    }

    public ArrayList<TopListData> d() {
        if (this.c) {
            return this.f1348a;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public ArrayList<TopListData> a(InputStream inputStream) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
            if (parse == null) {
                return null;
            }
            Element documentElement = parse.getDocumentElement();
            if (documentElement == null) {
                com.shoujiduoduo.base.a.a.c("TopListMgrImpl", "cannot find root node");
                return null;
            }
            NodeList elementsByTagName = documentElement.getElementsByTagName(Constants.ITEM);
            if (elementsByTagName == null) {
                com.shoujiduoduo.base.a.a.c("TopListMgrImpl", "cannot find node named \"item\"");
                return null;
            }
            ArrayList<TopListData> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                TopListData topListData = new TopListData();
                topListData.name = g.a(attributes, SelectCountryActivity.EXTRA_COUNTRY_NAME);
                topListData.type = g.a(attributes, "type", "");
                topListData.id = g.a(attributes, "id", 0);
                topListData.url = g.a(attributes, "url");
                if (!this.d.contains(topListData.type)) {
                    com.shoujiduoduo.base.a.a.e("TopListMgrImpl", "not support top list type:" + topListData.type);
                } else if (!"html".equals(topListData.type)) {
                    arrayList.add(topListData);
                } else if (com.shoujiduoduo.util.a.d()) {
                    arrayList.add(topListData);
                }
            }
            return arrayList;
        } catch (IOException e) {
            com.shoujiduoduo.base.a.a.a(e);
            return null;
        } catch (SAXException e2) {
            com.shoujiduoduo.base.a.a.a(e2);
            return null;
        } catch (ParserConfigurationException e3) {
            com.shoujiduoduo.base.a.a.a(e3);
            return null;
        } catch (ArrayIndexOutOfBoundsException e4) {
            com.shoujiduoduo.base.a.a.a(e4);
            return null;
        } catch (DOMException e5) {
            com.shoujiduoduo.base.a.a.a(e5);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean f() {
        String str = "";
        if (g.s().toString().contains("cu")) {
            str = "&cucc=1";
        }
        String a2 = u.a("gettabs", str);
        if (a2 != null) {
            this.f1348a = a(new ByteArrayInputStream(a2.getBytes()));
            if (this.f1348a == null || this.f1348a.size() <= 0) {
                com.shoujiduoduo.base.a.a.c("TopListMgrImpl", "parse net data error");
            } else {
                com.shoujiduoduo.base.a.a.a("TopListMgrImpl", this.f1348a.size() + " keywords.");
                this.b.a(this.f1348a);
                c.a().a(b.OBSERVER_TOP_LIST, new c.a<com.shoujiduoduo.a.c.u>() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.b.c.m.a(com.shoujiduoduo.b.c.m, boolean):boolean
                     arg types: [com.shoujiduoduo.b.c.m, int]
                     candidates:
                      com.shoujiduoduo.b.c.m.a(com.shoujiduoduo.b.c.m, java.io.InputStream):java.util.ArrayList
                      com.shoujiduoduo.b.c.m.a(com.shoujiduoduo.b.c.m, boolean):boolean */
                    public void a() {
                        boolean unused = m.this.c = true;
                        ((com.shoujiduoduo.a.c.u) this.f1284a).a(1);
                    }
                });
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean g() {
        this.f1348a = this.b.a();
        if (this.f1348a == null || this.f1348a.size() <= 0) {
            com.shoujiduoduo.base.a.a.a("TopListMgrImpl", "cache is not valid");
            return false;
        }
        com.shoujiduoduo.base.a.a.a("TopListMgrImpl", this.f1348a.size() + " list. read from cache.");
        c.a().a(b.OBSERVER_TOP_LIST, new c.a<com.shoujiduoduo.a.c.u>() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.b.c.m.a(com.shoujiduoduo.b.c.m, boolean):boolean
             arg types: [com.shoujiduoduo.b.c.m, int]
             candidates:
              com.shoujiduoduo.b.c.m.a(com.shoujiduoduo.b.c.m, java.io.InputStream):java.util.ArrayList
              com.shoujiduoduo.b.c.m.a(com.shoujiduoduo.b.c.m, boolean):boolean */
            public void a() {
                boolean unused = m.this.c = true;
                ((com.shoujiduoduo.a.c.u) this.f1284a).a(1);
            }
        });
        return true;
    }

    public void e() {
        if (this.f1348a == null) {
            i.a(new Runnable() {
                public void run() {
                    if (m.this.b.a(21600000)) {
                        if (!m.this.f() && !m.this.g()) {
                            c.a().a(b.OBSERVER_TOP_LIST, new c.a<com.shoujiduoduo.a.c.u>() {
                                public void a() {
                                    ((com.shoujiduoduo.a.c.u) this.f1284a).a(2);
                                }
                            });
                        }
                    } else if (!m.this.g() && !m.this.f()) {
                        c.a().a(b.OBSERVER_TOP_LIST, new c.a<com.shoujiduoduo.a.c.u>() {
                            public void a() {
                                ((com.shoujiduoduo.a.c.u) this.f1284a).a(2);
                            }
                        });
                    }
                }
            });
        }
    }

    /* compiled from: TopListMgrImpl */
    class a extends q<ArrayList<TopListData>> {
        a(String str) {
            super(str);
        }

        public ArrayList<TopListData> a() {
            try {
                return m.this.a(new FileInputStream(c + this.b));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }

        public void a(ArrayList<TopListData> arrayList) {
            if (arrayList != null && arrayList.size() != 0) {
                XmlSerializer newSerializer = Xml.newSerializer();
                StringWriter stringWriter = new StringWriter();
                try {
                    newSerializer.setOutput(stringWriter);
                    newSerializer.startDocument("UTF-8", true);
                    newSerializer.startTag("", "root");
                    newSerializer.attribute("", "num", String.valueOf(arrayList.size()));
                    for (int i = 0; i < arrayList.size(); i++) {
                        TopListData topListData = arrayList.get(i);
                        newSerializer.startTag("", Constants.ITEM);
                        newSerializer.attribute("", SelectCountryActivity.EXTRA_COUNTRY_NAME, topListData.name);
                        newSerializer.attribute("", "type", "" + topListData.type);
                        newSerializer.attribute("", "id", "" + topListData.id);
                        newSerializer.attribute("", "url", "" + topListData.url);
                        newSerializer.endTag("", Constants.ITEM);
                    }
                    newSerializer.endTag("", "root");
                    newSerializer.endDocument();
                    r.b(c + this.b, stringWriter.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
