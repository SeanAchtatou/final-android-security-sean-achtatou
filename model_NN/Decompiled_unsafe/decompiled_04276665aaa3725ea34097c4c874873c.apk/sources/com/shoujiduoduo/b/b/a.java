package com.shoujiduoduo.b.b;

import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.m;
import com.shoujiduoduo.util.r;
import com.shoujiduoduo.util.u;
import java.util.ArrayList;

/* compiled from: CategoryData */
public class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static String f1302a = (m.a(2) + "category.tmp");
    private ArrayList<C0038a> b = new ArrayList<>();
    private boolean c;

    /* renamed from: com.shoujiduoduo.b.b.a$a  reason: collision with other inner class name */
    /* compiled from: CategoryData */
    public static class C0038a {

        /* renamed from: a  reason: collision with root package name */
        public String f1304a;
        public String b;
        public String c;
        public String d;
    }

    public void a() {
        f();
    }

    public void b() {
        if (this.b != null) {
            this.b.clear();
        }
    }

    public boolean c() {
        return this.c;
    }

    public ArrayList<C0038a> d() {
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, long):long */
    private void f() {
        boolean a2 = a(null);
        if (!a2) {
            com.shoujiduoduo.base.a.a.a("CategoryData", "no cache or cache read filed, load default");
            h();
        } else {
            com.shoujiduoduo.base.a.a.a("CategoryData", "has cache, load cache success");
        }
        long a3 = af.a(RingDDApp.c(), "update_category_time", 0L);
        if (a3 != 0) {
            com.shoujiduoduo.base.a.a.a("CategoryData", "timeLastUpdate = " + a3);
            com.shoujiduoduo.base.a.a.a("CategoryData", "current time = " + System.currentTimeMillis());
            if (System.currentTimeMillis() - a3 > LogBuilder.MAX_INTERVAL) {
                com.shoujiduoduo.base.a.a.a("CategoryData", "cache out of data, download new data");
                g();
            } else if (!a2) {
                com.shoujiduoduo.base.a.a.a("CategoryData", "read cache failed, current data is default, so download new data");
                g();
            }
        } else {
            com.shoujiduoduo.base.a.a.a("CategoryData", "no cache, read from net");
            g();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return false;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(java.lang.String r9) {
        /*
            r8 = this;
            r1 = 1
            r0 = 0
            javax.xml.parsers.DocumentBuilderFactory r2 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            javax.xml.parsers.DocumentBuilder r2 = r2.newDocumentBuilder()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            boolean r3 = android.text.TextUtils.isEmpty(r9)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            if (r3 == 0) goto L_0x0022
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.String r4 = com.shoujiduoduo.b.b.a.f1302a     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            org.w3c.dom.Document r2 = r2.parse(r3)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
        L_0x001b:
            org.w3c.dom.Element r2 = r2.getDocumentElement()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            if (r2 != 0) goto L_0x0031
        L_0x0021:
            return r0
        L_0x0022:
            java.io.StringReader r3 = new java.io.StringReader     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r3.<init>(r9)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            org.xml.sax.InputSource r4 = new org.xml.sax.InputSource     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            org.w3c.dom.Document r2 = r2.parse(r4)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            goto L_0x001b
        L_0x0031:
            java.lang.String r3 = "list"
            org.w3c.dom.NodeList r3 = r2.getElementsByTagName(r3)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            if (r3 == 0) goto L_0x0021
            java.util.ArrayList<com.shoujiduoduo.b.b.a$a> r2 = r8.b     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r2.clear()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r2 = r0
        L_0x003f:
            int r4 = r3.getLength()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            if (r2 >= r4) goto L_0x00a0
            com.shoujiduoduo.b.b.a$a r4 = new com.shoujiduoduo.b.b.a$a     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r4.<init>()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            org.w3c.dom.Node r5 = r3.item(r2)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            org.w3c.dom.NamedNodeMap r5 = r5.getAttributes()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.String r6 = "id"
            java.lang.String r6 = com.shoujiduoduo.util.g.a(r5, r6)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r4.d = r6     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.String r6 = "name"
            java.lang.String r6 = com.shoujiduoduo.util.g.a(r5, r6)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r4.f1304a = r6     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r6.<init>()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.String r7 = "picprefix"
            java.lang.String r7 = com.shoujiduoduo.util.g.a(r5, r7)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.String r7 = "_normal.png"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.String r6 = r6.toString()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r4.b = r6     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r6.<init>()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.String r7 = "picprefix"
            java.lang.String r5 = com.shoujiduoduo.util.g.a(r5, r7)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.String r6 = "_pressed.png"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r4.c = r5     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.util.ArrayList<com.shoujiduoduo.b.b.a$a> r5 = r8.b     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r5.add(r4)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            int r2 = r2 + 1
            goto L_0x003f
        L_0x00a0:
            java.util.ArrayList<com.shoujiduoduo.b.b.a$a> r2 = r8.b     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            int r2 = r2.size()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            if (r2 <= 0) goto L_0x00da
            r2 = 1
            r8.c = r2     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            com.shoujiduoduo.a.a.c r2 = com.shoujiduoduo.a.a.c.a()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            com.shoujiduoduo.a.a.b r3 = com.shoujiduoduo.a.a.b.OBSERVER_CATEGORY     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            com.shoujiduoduo.b.b.a$1 r4 = new com.shoujiduoduo.b.b.a$1     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r4.<init>()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r2.a(r3, r4)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
        L_0x00b9:
            java.lang.String r2 = "CategoryData"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r3.<init>()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.String r4 = "read success, list size:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.util.ArrayList<com.shoujiduoduo.b.b.a$a> r4 = r8.b     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            int r4 = r4.size()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            com.shoujiduoduo.base.a.a.a(r2, r3)     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            r0 = r1
            goto L_0x0021
        L_0x00da:
            r2 = 0
            r8.c = r2     // Catch:{ IOException -> 0x00de, SAXException -> 0x00ec, ParserConfigurationException -> 0x00e9, DOMException -> 0x00e6, Exception -> 0x00e3, all -> 0x00e1 }
            goto L_0x00b9
        L_0x00de:
            r1 = move-exception
            goto L_0x0021
        L_0x00e1:
            r0 = move-exception
            throw r0
        L_0x00e3:
            r1 = move-exception
            goto L_0x0021
        L_0x00e6:
            r1 = move-exception
            goto L_0x0021
        L_0x00e9:
            r1 = move-exception
            goto L_0x0021
        L_0x00ec:
            r1 = move-exception
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.b.a.a(java.lang.String):boolean");
    }

    private void g() {
        i.a(new Runnable() {
            public void run() {
                String c = u.c();
                if (!ai.c(c)) {
                    r.b(a.f1302a, c);
                    com.shoujiduoduo.base.a.a.a("CategoryData", "loadFromNetwork, write to local cache file");
                    af.b(RingDDApp.c(), "update_category_time", System.currentTimeMillis());
                }
            }
        });
    }

    private void h() {
        if (a("<?xml version=\"1.0\" encoding=\"utf-8\"?><root><list id=\"2\" name=\"流行金曲\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Fpop_songs\"/><list id=\"3\" name=\"影视广告\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Fmovie_and_ads\"/><list id=\"4\" name=\"动漫游戏\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Fcomic_and_game\"/><list id=\"5\" name=\"信息短信\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Fsms\"/><list id=\"6\" name=\"DJ舞曲\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Fdance_music\"/><list id=\"7\" name=\"幽默搞笑\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Falternative\"/><list id=\"101\" name=\"纯音乐\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Fmusic\"/><list id=\"102\" name=\"欧美热歌\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Foumei\"/><list id=\"103\" name=\"日韩风潮\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Frihan\"/><list id=\"8\" name=\"用户上传\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Fuser_upload\"/><list id=\"9\" name=\"其他另类\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Fothers\"/><list id=\"104\" name=\"彩铃专区\" picprefix=\"http://bcs.pubbcsapp.com/duoduo-ring/pic%2Fcailing\"/></root>")) {
            com.shoujiduoduo.base.a.a.a("CategoryData", "loadDefault, load success");
        } else {
            com.shoujiduoduo.base.a.a.a("CategoryData", "loadDefault, load failed");
        }
    }
}
