package com.shoujiduoduo.b.e;

import android.text.TextUtils;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.u;
import java.util.HashMap;

/* compiled from: UserInfoMgrImpl */
public class b implements a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public UserInfo f1363a = new UserInfo();
    private v b = new v() {
        public void a(int i, boolean z, String str, String str2) {
            a.a("UserInfoMgrImpl", "onLogin, type:" + i);
            if (i != 1) {
                b.this.j();
            }
        }

        public void a(int i) {
        }

        public void b(int i) {
        }

        public void a(String str, boolean z) {
        }

        public void a(String str) {
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.b.e.b.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.shoujiduoduo.b.e.b.a(com.shoujiduoduo.b.e.b, int):void
      com.shoujiduoduo.b.e.b.a(java.lang.String, boolean):void */
    public void a() {
        int a2 = af.a(RingDDApp.c(), "user_loginStatus", 0);
        if (a2 != 0) {
            a.a("UserInfoMgrImpl", "user in login status， 加载登录信息");
            String a3 = af.a(RingDDApp.c(), "user_name", "");
            a.b("UserInfoMgrImpl", "user_name:" + a3);
            String a4 = af.a(RingDDApp.c(), "user_headpic", "");
            a.b("UserInfoMgrImpl", "user_headpic:" + a4);
            String a5 = af.a(RingDDApp.c(), "user_uid", "");
            a.b("UserInfoMgrImpl", "user_uid:" + a5);
            int a6 = af.a(RingDDApp.c(), "user_loginType", 0);
            a.b("UserInfoMgrImpl", "user_loginType:" + a6);
            int a7 = af.a(RingDDApp.c(), "user_vip_type", 0);
            a.b("UserInfoMgrImpl", "user_vip_type:" + a7);
            String a8 = af.a(RingDDApp.c(), "user_phone_num", "");
            a.b("UserInfoMgrImpl", "user_phone_num:" + a8);
            int a9 = af.a(RingDDApp.c(), "user_is_superuser", 0);
            a.b("UserInfoMgrImpl", "user_is_superuser:" + a9);
            int a10 = af.a(RingDDApp.c(), "user_fans_num", 0);
            a.b("UserInfoMgrImpl", "user_fans_num:" + a10);
            int a11 = af.a(RingDDApp.c(), "user_follow_num", 0);
            a.b("UserInfoMgrImpl", "user_follow_num:" + a11);
            String a12 = af.a(RingDDApp.c(), "user_followings", "");
            this.f1363a.setUid(a5);
            this.f1363a.setHeadPic(a4);
            this.f1363a.setUserName(a3);
            this.f1363a.setLoginType(a6);
            this.f1363a.setLoginStatus(a2);
            this.f1363a.setVipType(a7);
            this.f1363a.setPhoneNum(a8);
            this.f1363a.setIsSuperuser(a9);
            this.f1363a.setFansNum(a10);
            this.f1363a.setFollowNum(a11);
            this.f1363a.setFollowings(a12);
            if (!TextUtils.isEmpty(a8)) {
                c(a8, true);
            } else {
                j();
            }
            if (!TextUtils.isEmpty(a5)) {
                String str = "unknown";
                if (a5.indexOf("_") > 0) {
                    str = a5.substring(0, a5.indexOf("_"));
                }
                HashMap hashMap = new HashMap();
                hashMap.put("platform", str);
                com.umeng.a.b.a(RingDDApp.c(), "USER_ACCOUNT_INFO", hashMap);
            }
        } else {
            a.a("UserInfoMgrImpl", "user is not in  login status");
            String a13 = af.a(RingDDApp.c(), "user_phone_num", "");
            a.b("UserInfoMgrImpl", "user_phone_num:" + a13);
            this.f1363a.setPhoneNum(a13);
            a(a13, false);
        }
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.b);
    }

    public void b() {
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.b);
    }

    public void b(final String str) {
        String str2 = af.a(RingDDApp.c(), "user_followings", "") + "|" + str;
        this.f1363a.setFollowings(str2);
        af.c(RingDDApp.c(), "user_followings", str2);
        this.f1363a.setFollowNum(this.f1363a.getFollowNum() + 1);
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
            public void a() {
                ((v) this.f1284a).a(str, true);
            }
        });
    }

    public void a(final String str) {
        String a2 = af.a(RingDDApp.c(), "user_followings", "");
        if (a2.contains(str)) {
            String replace = a2.replace(str, "");
            this.f1363a.setFollowings(replace);
            af.c(RingDDApp.c(), "user_followings", replace);
            this.f1363a.setFollowNum(this.f1363a.getFollowNum() > 0 ? this.f1363a.getFollowNum() - 1 : 0);
            c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                public void a() {
                    ((v) this.f1284a).a(str, false);
                }
            });
        }
    }

    public String i() {
        return this.f1363a.getFollowings();
    }

    private void a(String str, final boolean z) {
        if (!TextUtils.isEmpty(str)) {
            c(str, z);
        } else if (g.u()) {
            a.a("UserInfoMgrImpl", "can show cu cailing");
            if (g.e() && !g.f()) {
                com.shoujiduoduo.util.e.a.a().a(new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        if (bVar != null && (bVar instanceof c.j)) {
                            com.shoujiduoduo.util.e.a.a().b(((c.j) bVar).f2239a, new com.shoujiduoduo.util.b.b() {
                                public void a(c.b bVar) {
                                    super.a(bVar);
                                    if (bVar != null && (bVar instanceof c.t)) {
                                        c.t tVar = (c.t) bVar;
                                        b.this.f1363a.setPhoneNum(tVar.f2250a);
                                        af.c(RingDDApp.c(), "user_phone_num", tVar.f2250a);
                                        b.this.b(tVar.f2250a, z);
                                    }
                                }

                                public void b(c.b bVar) {
                                    super.b(bVar);
                                    a.e("UserInfoMgrImpl", "get user mobile num failed");
                                }
                            });
                        }
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        a.e("UserInfoMgrImpl", "get uniKey failed");
                    }
                });
            }
        } else {
            a.a("UserInfoMgrImpl", "unknown cailing type");
        }
    }

    /* access modifiers changed from: private */
    public void b(final String str, final boolean z) {
        com.shoujiduoduo.util.e.a.a().h(str, new com.shoujiduoduo.util.b.b() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.util.e.a.a(boolean, java.lang.String, java.lang.String):void
             arg types: [int, java.lang.String, java.lang.String]
             candidates:
              com.shoujiduoduo.util.e.a.a(com.shoujiduoduo.util.e.a, java.lang.String, java.lang.String):com.shoujiduoduo.util.b.c$b
              com.shoujiduoduo.util.e.a.a(java.lang.String, com.shoujiduoduo.util.b.c$b, java.lang.String):void
              com.shoujiduoduo.util.e.a.a(java.lang.String, java.lang.String, java.lang.String):void
              com.shoujiduoduo.util.e.a.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.b):void
              com.shoujiduoduo.util.e.a.a(boolean, java.lang.String, java.lang.String):void */
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar != null && (bVar instanceof c.ah)) {
                    c.ah ahVar = (c.ah) bVar;
                    a.a("UserInfoMgrImpl", "user location, provinceid:" + ahVar.f2228a + ", province name:" + ahVar.d);
                    if (com.shoujiduoduo.util.e.a.a().c(ahVar.f2228a)) {
                        a.a("UserInfoMgrImpl", "in qualified area, support cucc");
                        if (com.shoujiduoduo.util.e.a.a().a(str)) {
                            a.a("UserInfoMgrImpl", "当前手机号有token， phone:" + str);
                            com.shoujiduoduo.util.e.a.a().f(new com.shoujiduoduo.util.b.b() {
                                public void a(c.b bVar) {
                                    super.a(bVar);
                                    if (bVar instanceof c.f) {
                                        c.f fVar = (c.f) bVar;
                                        if (fVar.e()) {
                                            a.a("UserInfoMgrImpl", "联通vip 开通状态");
                                            if (z) {
                                                b.this.a(3);
                                            }
                                        } else {
                                            a.a("UserInfoMgrImpl", "联通vip 未开通");
                                            if (z) {
                                                b.this.a(0);
                                            }
                                        }
                                        if (fVar.f2235a.a().equals("40307") || fVar.f2235a.a().equals("40308")) {
                                            a.a("UserInfoMgrImpl", "token 失效");
                                            com.shoujiduoduo.util.e.a.a().a(str, "");
                                        }
                                    }
                                }

                                public void b(c.b bVar) {
                                    if (bVar.a().equals("40307") || bVar.a().equals("40308")) {
                                        a.a("UserInfoMgrImpl", "token 失效");
                                        com.shoujiduoduo.util.e.a.a().a(str, "");
                                    }
                                    super.b(bVar);
                                }
                            });
                            return;
                        }
                        a.a("UserInfoMgrImpl", "当前手机号没有token， 不做vip查询");
                        return;
                    }
                    com.shoujiduoduo.util.e.a.a().a(false, "", "");
                    a.a("UserInfoMgrImpl", "not in qualified area, not support cucc");
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                a.a("UserInfoMgrImpl", "get UserLocation failed");
            }
        });
    }

    /* access modifiers changed from: private */
    public void j() {
        final String f = com.shoujiduoduo.a.b.b.g().f();
        if (!ai.c(f)) {
            i.a(new Runnable() {
                public void run() {
                    final String a2 = u.a("query3rd", "&uid=" + f);
                    if (a2 == null || !g.f(a2)) {
                        a.e("UserInfoMgrImpl", "未查询到当前第三方账号关联的手机号");
                    } else {
                        com.shoujiduoduo.a.a.c.a().a(new c.b() {
                            public void a() {
                                b.this.f1363a.setPhoneNum(a2);
                                b.this.c(a2, true);
                            }
                        });
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void c(String str, final boolean z) {
        switch (g.g(str)) {
            case f2309a:
                return;
            case cu:
                b(str, z);
                return;
            case ct:
                com.shoujiduoduo.util.d.b.a().a(str, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        a.a("UserInfoMgrImpl", "查询会员状态成功");
                        if (bVar != null && (bVar instanceof c.e)) {
                            c.e eVar = (c.e) bVar;
                            a.a("UserInfoMgrImpl", "code:" + eVar.a() + " msg:" + eVar.b());
                            int i = (eVar.e() || eVar.f()) ? 2 : 0;
                            if (z) {
                                b.this.a(i);
                            }
                        }
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        a.c("UserInfoMgrImpl", "查询会员状态失败, code:" + bVar.a() + ", msg:" + bVar.b());
                        if (z) {
                            b.this.a(0);
                        }
                    }
                });
                return;
            default:
                a.e("UserInfoMgrImpl", "unknown phone type");
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a(final int i) {
        this.f1363a.setVipType(i);
        af.b(RingDDApp.c(), "user_vip_type", i);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new c.a<x>() {
            public void a() {
                ((x) this.f1284a).a(i);
            }
        });
    }

    public UserInfo c() {
        return this.f1363a;
    }

    public void a(final UserInfo userInfo) {
        this.f1363a = userInfo;
        af.c(RingDDApp.c(), "user_name", this.f1363a.getUserName());
        af.c(RingDDApp.c(), "user_headpic", this.f1363a.getHeadPic());
        af.c(RingDDApp.c(), "user_uid", this.f1363a.getUid());
        af.b(RingDDApp.c(), "user_loginType", this.f1363a.getLoginType());
        af.b(RingDDApp.c(), "user_loginStatus", this.f1363a.getLoginStatus());
        af.b(RingDDApp.c(), "user_vip_type", this.f1363a.getVipType());
        af.c(RingDDApp.c(), "user_phone_num", this.f1363a.getPhoneNum());
        af.b(RingDDApp.c(), "user_is_superuser", this.f1363a.isSuperUser() ? 1 : 0);
        af.b(RingDDApp.c(), "user_fans_num", this.f1363a.getFansNum());
        af.b(RingDDApp.c(), "user_follow_num", this.f1363a.getFollowNum());
        af.c(RingDDApp.c(), "user_followings", this.f1363a.getFollowings());
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
            public void a() {
                ((v) this.f1284a).b(userInfo.getLoginType());
            }
        });
    }

    public String f() {
        return this.f1363a.getUid();
    }

    public boolean g() {
        return this.f1363a.isLogin();
    }

    public boolean h() {
        return this.f1363a.isVip();
    }

    public int d() {
        return this.f1363a.getLoginType();
    }

    public int e() {
        return this.f1363a.getVipType();
    }
}
