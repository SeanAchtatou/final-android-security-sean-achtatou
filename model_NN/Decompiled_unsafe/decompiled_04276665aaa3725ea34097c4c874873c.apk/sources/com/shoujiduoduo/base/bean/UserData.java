package com.shoujiduoduo.base.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class UserData implements Parcelable {
    public static final Parcelable.Creator<UserData> CREATOR = new Parcelable.Creator<UserData>() {
        public UserData createFromParcel(Parcel parcel) {
            return new UserData(parcel);
        }

        public UserData[] newArray(int i) {
            return new UserData[i];
        }
    };
    public String bgurl;
    public String ddid;
    public boolean followed;
    public int followerNum;
    public int followingNum;
    public String followings;
    public String headUrl;
    public String intro;
    public int isSuperUser;
    public int ringsNum;
    public String sex;
    public String uid;
    public String userName;

    public UserData() {
    }

    protected UserData(Parcel parcel) {
        this.userName = parcel.readString();
        this.headUrl = parcel.readString();
        this.uid = parcel.readString();
        this.sex = parcel.readString();
        this.bgurl = parcel.readString();
        this.intro = parcel.readString();
        this.ddid = parcel.readString();
        this.followed = parcel.readByte() != 0;
        this.ringsNum = parcel.readInt();
        this.followerNum = parcel.readInt();
        this.followingNum = parcel.readInt();
        this.isSuperUser = parcel.readInt();
        this.followings = parcel.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.userName);
        parcel.writeString(this.headUrl);
        parcel.writeString(this.uid);
        parcel.writeString(this.sex);
        parcel.writeString(this.bgurl);
        parcel.writeString(this.intro);
        parcel.writeString(this.ddid);
        parcel.writeByte((byte) (this.followed ? 1 : 0));
        parcel.writeInt(this.ringsNum);
        parcel.writeInt(this.followerNum);
        parcel.writeInt(this.followingNum);
        parcel.writeInt(this.isSuperUser);
        parcel.writeString(this.followings);
    }
}
