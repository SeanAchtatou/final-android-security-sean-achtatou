package com.shoujiduoduo.base.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class MessageData implements Parcelable {
    public static final Parcelable.Creator<MessageData> CREATOR = new Parcelable.Creator<MessageData>() {
        public MessageData createFromParcel(Parcel parcel) {
            return new MessageData(parcel);
        }

        public MessageData[] newArray(int i) {
            return new MessageData[i];
        }
    };
    public String artist;
    public String cid;
    public String comment;
    public String createtime;
    public String date;
    public String ddid;
    public String desc;
    public int duration;
    public String feedtype;
    public String hbr;
    public String head_url;
    public String hurl;
    public String lbr;
    public String lurl;
    public String name;
    public String rid;
    public String ruid;
    public String tcid;
    public String tcomment;
    public String tuid;
    public String uid;
    public int upvote;

    public MessageData() {
        this.name = "";
        this.uid = "";
        this.artist = "";
        this.duration = 0;
        this.head_url = "";
        this.hurl = "";
        this.lurl = "";
        this.lbr = "";
        this.hbr = "";
        this.date = "";
        this.rid = "";
        this.desc = "";
        this.feedtype = "";
        this.ruid = "";
        this.tuid = "";
        this.ddid = "";
        this.tcid = "";
        this.cid = "";
        this.comment = "";
        this.tcomment = "";
        this.createtime = "";
        this.upvote = 0;
    }

    protected MessageData(Parcel parcel) {
        this.name = parcel.readString();
        this.uid = parcel.readString();
        this.artist = parcel.readString();
        this.duration = parcel.readInt();
        this.head_url = parcel.readString();
        this.hurl = parcel.readString();
        this.lurl = parcel.readString();
        this.lbr = parcel.readString();
        this.hbr = parcel.readString();
        this.date = parcel.readString();
        this.rid = parcel.readString();
        this.desc = parcel.readString();
        this.feedtype = parcel.readString();
        this.ruid = parcel.readString();
        this.tuid = parcel.readString();
        this.ddid = parcel.readString();
        this.tcid = parcel.readString();
        this.cid = parcel.readString();
        this.comment = parcel.readString();
        this.tcomment = parcel.readString();
        this.createtime = parcel.readString();
        this.upvote = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.uid);
        parcel.writeString(this.artist);
        parcel.writeInt(this.duration);
        parcel.writeString(this.head_url);
        parcel.writeString(this.hurl);
        parcel.writeString(this.lurl);
        parcel.writeString(this.lbr);
        parcel.writeString(this.hbr);
        parcel.writeString(this.date);
        parcel.writeString(this.rid);
        parcel.writeString(this.desc);
        parcel.writeString(this.feedtype);
        parcel.writeString(this.ruid);
        parcel.writeString(this.tuid);
        parcel.writeString(this.ddid);
        parcel.writeString(this.tcid);
        parcel.writeString(this.cid);
        parcel.writeString(this.comment);
        parcel.writeString(this.tcomment);
        parcel.writeString(this.createtime);
        parcel.writeInt(this.upvote);
    }
}
