package com.shoujiduoduo.base.bean;

public class RingAntiStealData {
    private int mBitrate;
    private String mFormat;
    private String mUrl;

    public RingAntiStealData(String str, String str2, int i) {
        this.mUrl = str;
        this.mFormat = str2;
        this.mBitrate = i;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public String getFormat() {
        return this.mFormat;
    }

    public int getBitrate() {
        return this.mBitrate;
    }
}
