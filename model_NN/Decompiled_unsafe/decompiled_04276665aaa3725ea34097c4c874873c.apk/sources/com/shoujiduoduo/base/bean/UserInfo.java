package com.shoujiduoduo.base.bean;

import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ai;
import com.umeng.socialize.PlatformConfig;

public class UserInfo {
    public static final int cailing_type_cmcc = 1;
    public static final int cailing_type_ctcc = 2;
    public static final int cailing_type_cucc = 3;
    public static final int login_status_login = 1;
    public static final int login_status_logout = 2;
    public static final int login_status_not_login = 3;
    public static final int login_type_phone = 1;
    public static final int login_type_qq = 2;
    public static final int login_type_renren = 4;
    public static final int login_type_weibo = 3;
    public static final int login_type_weixin = 5;
    private static final String user_followings = "user_followings";
    public static final int vip_type_cmcc = 1;
    public static final int vip_type_ctcc = 2;
    public static final int vip_type_cucc = 3;
    public static final int vip_type_none = 0;
    private int cailingType;
    private int fansNum;
    private int followNum;
    private String followings = "";
    private String headPic = "";
    private int isSuperuser;
    private int loginStatus;
    private int loginType;
    private String nickName = "";
    private String phoneNum = "";
    private String uid = "";
    private String userName = "";
    private int vipType;

    public void setFollowings(String str) {
        this.followings = str;
    }

    public String getFollowings() {
        return this.followings;
    }

    public void setFansNum(int i) {
        this.fansNum = i;
    }

    public void setFollowNum(int i) {
        this.followNum = i;
    }

    public int getFansNum() {
        return this.fansNum;
    }

    public int getFollowNum() {
        return this.followNum;
    }

    public String getUid() {
        return this.uid;
    }

    public void setUid(String str) {
        this.uid = str;
    }

    public String getUserName() {
        if (ai.c(this.userName)) {
            return this.phoneNum;
        }
        return this.userName;
    }

    public void setUserName(String str) {
        this.userName = str;
    }

    public String getHeadPic() {
        return this.headPic;
    }

    public void setHeadPic(String str) {
        this.headPic = str;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String str) {
        this.nickName = str;
    }

    public int getLoginType() {
        return this.loginType;
    }

    public String getLoginTypeStr() {
        switch (this.loginType) {
            case 1:
                return "phone";
            case 2:
                return "qq";
            case 3:
                return Constants.WEIBO;
            case 4:
                return PlatformConfig.Renren.Name;
            case 5:
                return "weixin";
            default:
                return "";
        }
    }

    public void setLoginType(int i) {
        this.loginType = i;
    }

    public int getVipType() {
        return this.vipType;
    }

    public void setVipType(int i) {
        this.vipType = i;
    }

    public int getLoginStatus() {
        return this.loginStatus;
    }

    public void setLoginStatus(int i) {
        this.loginStatus = i;
    }

    public int getCailingType() {
        return this.cailingType;
    }

    public void setCailingType(int i) {
        this.cailingType = i;
    }

    public boolean isLogin() {
        return this.loginStatus == 1;
    }

    public void setIsSuperuser(int i) {
        this.isSuperuser = i;
    }

    public boolean isSuperUser() {
        return this.isSuperuser == 1;
    }

    public boolean isVip() {
        return this.vipType != 0 && isLogin();
    }

    public boolean isCailingUser() {
        return this.cailingType != 0;
    }

    public void setPhoneNum(String str) {
        af.c(RingDDApp.c(), "pref_phone_num", str);
        this.phoneNum = str;
    }

    public String getPhoneNum() {
        return this.phoneNum;
    }
}
