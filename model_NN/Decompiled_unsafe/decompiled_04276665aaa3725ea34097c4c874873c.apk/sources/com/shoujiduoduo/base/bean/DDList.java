package com.shoujiduoduo.base.bean;

import com.shoujiduoduo.base.bean.ListType;

public interface DDList {
    public static final int MESSAGE_FAIL_GET_NEW_DATA = 4;
    public static final int MESSAGE_FAIL_RETRIEVE_DATA = 1;
    public static final int MESSAGE_FAIL_RETRIEVE_MORE_DATA = 2;
    public static final int MESSAGE_NO_NEW_DATA = 3;
    public static final int MESSAGE_SUCCESS_GET_NEW_DATA = 5;
    public static final int MESSAGE_SUCCESS_GET_NEW_DATA_MORE = 6;
    public static final int MESSAGE_SUCCESS_RETRIVE_DATA = 0;

    Object get(int i);

    String getBaseURL();

    String getListId();

    ListType.LIST_TYPE getListType();

    boolean hasMoreData();

    boolean isRetrieving();

    void refreshData();

    void reloadData();

    void retrieveData();

    int size();
}
