package com.shoujiduoduo.base.bean;

import java.util.ArrayList;
import java.util.HashMap;

public class ListContent<T> {
    public String area = "";
    public String baseURL = "";
    public ArrayList<T> data;
    public HashMap<String, String> fadeRidMap;
    public boolean hasMore = false;
    public int hotCommentNum;
    public int page;
    public String sig = "";
}
