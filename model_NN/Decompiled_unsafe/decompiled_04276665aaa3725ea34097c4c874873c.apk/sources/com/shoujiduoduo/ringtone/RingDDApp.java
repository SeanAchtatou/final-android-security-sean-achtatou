package com.shoujiduoduo.ringtone;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Process;
import cn.banshenggua.aichang.sdk.ACContext;
import cn.banshenggua.aichang.sdk.ACException;
import com.d.a.b.a.l;
import com.d.a.b.d;
import com.d.a.b.e;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.NetworkStateUtil;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.r;
import com.shoujiduoduo.util.y;
import com.umeng.a.b;
import com.umeng.socialize.PlatformConfig;
import java.util.HashMap;

public class RingDDApp extends Application {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1419a;
    /* access modifiers changed from: private */
    public static volatile boolean b;
    private static RingDDApp c = null;
    private static long d = Thread.currentThread().getId();
    private static Handler e = new Handler();
    private static boolean g;
    private static boolean i = false;
    private HashMap<String, Object> f;
    private BroadcastReceiver h = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("com.shoujiduoduo.ringtone.exitapp".equals(intent.getAction())) {
                a.a("RingDDApp", "ExitAppReceiver received");
                RingDDApp.g();
            }
        }
    };

    public void onCreate() {
        super.onCreate();
        a.a("RingDDApp", "**********************************************************");
        a.a("RingDDApp", "\n\r\n\r");
        a.a("RingDDApp", "App Instance is created!, Main ThreadID = " + Thread.currentThread().getId());
        a.a("RingDDApp", "\n\r\n\r");
        a.a("RingDDApp", "**********************************************************");
        c = this;
        try {
            ACContext.initContext(this);
        } catch (ACException e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
        }
        if (!g.c(this)) {
            a.a("RingDDApp", "若由其他非主进程的进程启动，不需要进行多次初始化");
        } else {
            i();
        }
    }

    private void i() {
        g.b(this);
        NetworkStateUtil.a(getApplicationContext());
        af.a(this);
        g.b bVar = g.b.none;
        try {
            g.s();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        r.a(this);
        ad.a().b();
        this.f = new HashMap<>();
        i.a(new Runnable() {
            public void run() {
                y.a();
            }
        });
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.shoujiduoduo.ringtone.exitapp");
        registerReceiver(this.h, intentFilter);
        h();
        k();
        j();
        a.a("RingDDApp", "app version:" + g.o());
        a.a("RingDDApp", "app install src:" + g.n());
        a.a("RingDDApp", "device info:" + g.i());
        a.a("RingDDApp", "os version:" + g.j());
        a.a("RingDDApp", "density:" + getResources().getDisplayMetrics().density);
        a.a("RingDDApp", "Build.BRAND:" + Build.BRAND);
        a.a("RingDDApp", "Build.MANUFACTURER:" + Build.MANUFACTURER);
        a.a("RingDDApp", "Build.MODEL:" + Build.MODEL);
        a.a("RingDDApp", "Build.DEVICE:" + Build.DEVICE);
        a.a("RingDDApp", "Build.BOARD:" + Build.BOARD);
        a.a("RingDDApp", "Build.DISPLAY:" + Build.DISPLAY);
        a.a("RingDDApp", "Build.PRODUCT:" + Build.PRODUCT);
        a.a("RingDDApp", "Build.BOOTLOADER:" + Build.BOOTLOADER);
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        android.support.multidex.a.a(this);
    }

    private void j() {
        String a2 = af.a(this, "pref_load_cmcc_sunshine_sdk_start", "");
        String a3 = af.a(this, "pref_load_cmcc_sunshine_sdk_end", "");
        if (a2.equals("1") && !a3.equals("1")) {
            a(true);
            HashMap hashMap = new HashMap();
            hashMap.put("phone", Build.BRAND + "-" + Build.MODEL + "-" + Build.VERSION.SDK_INT);
            a.a("RingDDApp", "sunshine sdk crash last time:" + Build.BRAND + "-" + Build.MODEL + "-" + Build.VERSION.RELEASE);
            b.a(this, "cmcc_sunshine_303_sdk_crash", hashMap);
        }
    }

    public boolean a() {
        return g;
    }

    public void a(boolean z) {
        g = z;
    }

    public static RingDDApp b() {
        return c;
    }

    public static Context c() {
        return c;
    }

    public static long d() {
        return d;
    }

    public static Handler e() {
        return e;
    }

    public static boolean f() {
        return b;
    }

    public static void g() {
        if (!f1419a) {
            f1419a = true;
            NetworkStateUtil.b(c.getApplicationContext());
            c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_APP, new c.a<com.shoujiduoduo.a.c.a>() {
                public void a() {
                    try {
                        ((com.shoujiduoduo.a.c.a) this.f1284a).a();
                    } catch (Throwable th) {
                    }
                }
            });
            c.a().b(new c.b() {
                public void a() {
                    boolean unused = RingDDApp.b = true;
                    c.a().b(new c.b() {
                        public void a() {
                            c.a().b();
                            try {
                                com.shoujiduoduo.a.b.b.a();
                            } catch (Throwable th) {
                            }
                            c.a().a(500, new c.b() {
                                public void a() {
                                    i.a();
                                    b.c(RingDDApp.c());
                                    Process.killProcess(Process.myPid());
                                    System.exit(0);
                                }
                            });
                        }
                    });
                }
            });
        }
    }

    public void onTerminate() {
        a.a("RingDDApp", "App onTerminate.");
        unregisterReceiver(this.h);
        super.onTerminate();
    }

    public void a(String str, Object obj) {
        this.f.put(str, obj);
    }

    public Object a(String str) {
        if (this.f.containsKey(str)) {
            return this.f.get(str);
        }
        return null;
    }

    public static void h() {
        if (!i) {
            i = true;
            d.a().a(new e.a(c()).b(3).a().a(new com.d.a.a.a.b.c()).a(l.LIFO).c());
        }
    }

    private void k() {
        PlatformConfig.setWeixin("wxb4cd572ca73fd239", "48bccd48df1afa300a52bd75611a6710");
        PlatformConfig.setSinaWeibo("1981517408", "001b0b305f9cbb4f307866ea81cd473c");
        PlatformConfig.setQQZone("100382066", "2305e02edac0c67f822f453d92be2a66");
    }
}
