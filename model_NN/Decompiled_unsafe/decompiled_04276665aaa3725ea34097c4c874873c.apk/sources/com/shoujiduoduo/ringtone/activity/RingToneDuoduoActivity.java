package com.shoujiduoduo.ringtone.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.banshenggua.aichang.utils.Constants;
import com.cmsc.cmmusic.init.InitCmmInterface;
import com.igexin.sdk.PushManager;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.f;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.service.DuoDuoIntentService;
import com.shoujiduoduo.service.GTPushService;
import com.shoujiduoduo.ui.adwall.AdWallFrag;
import com.shoujiduoduo.ui.adwall.MoreOptionFrag;
import com.shoujiduoduo.ui.aichang.AiChangFrag;
import com.shoujiduoduo.ui.category.CategoryFrag;
import com.shoujiduoduo.ui.home.HomepageFrag;
import com.shoujiduoduo.ui.home.MusicAlbumActivity;
import com.shoujiduoduo.ui.makering.MakeRingActivity;
import com.shoujiduoduo.ui.mine.MyRingtoneFrag;
import com.shoujiduoduo.ui.search.SearchActivity;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.ui.utils.MyViewPager;
import com.shoujiduoduo.ui.utils.j;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ae;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.al;
import com.shoujiduoduo.util.c.c;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.n;
import com.shoujiduoduo.util.o;
import com.shoujiduoduo.util.widget.MyRadioButton;
import com.shoujiduoduo.util.widget.WebViewActivity;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;
import com.umeng.socialize.UMShareAPI;
import java.io.File;
import java.util.ArrayList;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class RingToneDuoduoActivity extends BaseFragmentActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private static final String o = ("start_time:" + g.n());
    private static RingToneDuoduoActivity r;
    private static boolean s;
    private ServiceConnection A = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            com.shoujiduoduo.base.a.a.a("ServiceConnection", "ServiceConnection: onServiceConnected.");
            PlayerService unused = RingToneDuoduoActivity.this.j = ((PlayerService.b) iBinder).a();
            ab.a().a(RingToneDuoduoActivity.this.j);
            com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "ServiceConnection: mPlayService = " + RingToneDuoduoActivity.this.j);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            com.shoujiduoduo.base.a.a.a("ServiceConnection", "ServiceConnection: onServiceDisconnected.");
        }
    };
    /* access modifiers changed from: private */
    @SuppressLint({"HandlerLeak"})
    public Handler B = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case Constants.CLEARIMGING:
                    new AlertDialog.Builder(RingToneDuoduoActivity.this).setTitle("提示").setMessage("目前检测您的网络连接可能有问题，请联系铃声多多客服解决一下，客服QQ：2262193174, errorContent:" + ((String) message.obj)).setPositiveButton((int) R.string.ok, (DialogInterface.OnClickListener) null).show();
                    return;
                case 1100:
                    RingToneDuoduoActivity.this.a((String) message.obj);
                    return;
                case 1101:
                    Uri fromFile = Uri.fromFile(new File((String) message.obj));
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
                    RingToneDuoduoActivity.this.startActivity(intent);
                    return;
                case 1102:
                    Toast.makeText(RingToneDuoduoActivity.this, (int) R.string.down_update_apk_error, 0).show();
                    return;
                case 1130:
                    if (RingToneDuoduoActivity.this.k != null) {
                        RingToneDuoduoActivity.this.k.cancel();
                    }
                    Toast.makeText(RingToneDuoduoActivity.this, (int) R.string.clean_cache_suc, 0).show();
                    return;
                case 1131:
                    Intent intent2 = new Intent(RingToneDuoduoActivity.this, SearchActivity.class);
                    intent2.putExtra("from", "push");
                    intent2.putExtra("key", (String) message.obj);
                    RingToneDuoduoActivity.this.startActivity(intent2);
                    return;
                case 1133:
                    String str = (String) message.obj;
                    Intent intent3 = new Intent(RingToneDuoduoActivity.this, WebViewActivity.class);
                    intent3.putExtra("url", str);
                    com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "url:" + str);
                    RingToneDuoduoActivity.this.startActivity(intent3);
                    return;
                case 1134:
                    RingToneDuoduoActivity.this.g();
                    return;
                case 1135:
                    b bVar = (b) message.obj;
                    Intent intent4 = new Intent(RingToneDuoduoActivity.this, MusicAlbumActivity.class);
                    intent4.putExtra("url", bVar.f1440a);
                    intent4.putExtra("title", bVar.b);
                    intent4.putExtra("type", MusicAlbumActivity.a.create_album);
                    com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "title:" + bVar.b + ", url:" + bVar.f1440a);
                    RingToneDuoduoActivity.this.startActivity(intent4);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public MyRadioButton f1422a;
    /* access modifiers changed from: private */
    public ImageView b;
    private TextView c;
    private Drawable d;
    private RelativeLayout e;
    private RelativeLayout f;
    private TextView g;
    private TextView h;
    private LinearLayout i;
    /* access modifiers changed from: private */
    public PlayerService j;
    /* access modifiers changed from: private */
    public ProgressDialog k;
    private MyViewPager l;
    /* access modifiers changed from: private */
    public ArrayList<Fragment> m = new ArrayList<>();
    /* access modifiers changed from: private */
    public RadioGroup n;
    private a p;
    private boolean q;
    private boolean t;
    /* access modifiers changed from: private */
    public boolean u;
    private com.shoujiduoduo.ui.settings.a v;
    private f w = new f() {
        public void a(boolean z) {
            if (z) {
                RingToneDuoduoActivity.this.g();
            }
        }
    };
    private v x = new v() {
        public void b(int i) {
        }

        public void a(String str, boolean z) {
        }

        public void a(String str) {
            if (!RingToneDuoduoActivity.this.u) {
                RingToneDuoduoActivity.this.b.setVisibility(0);
            }
        }

        public void a(int i) {
            RingToneDuoduoActivity.this.f1422a.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, j.b(R.drawable.btn_navi_myring), (Drawable) null, (Drawable) null);
        }

        public void a(int i, boolean z, String str, String str2) {
        }
    };
    private x y = new x() {
        public void a(int i) {
            Drawable b;
            if (com.shoujiduoduo.a.b.b.g().h()) {
                b = j.b(R.drawable.btn_navi_myring_vip);
            } else {
                b = j.b(R.drawable.btn_navi_myring);
            }
            RingToneDuoduoActivity.this.f1422a.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, b, (Drawable) null, (Drawable) null);
        }
    };
    private com.shoujiduoduo.a.c.a z = new com.shoujiduoduo.a.c.a() {
        public void a() {
            RingToneDuoduoActivity.this.d();
            RingToneDuoduoActivity.this.finish();
        }

        public void b() {
            com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "Sd card error");
            new b.a(RingToneDuoduoActivity.this).a((int) R.string.sdcard_not_access).a((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    RingToneDuoduoActivity.this.finish();
                    RingDDApp.g();
                }
            }).a().show();
        }
    };

    public static RingToneDuoduoActivity a() {
        return r;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.af.b(android.content.Context, java.lang.String, int):boolean
     arg types: [com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.af.b(android.content.Context, java.lang.String, long):boolean
      com.shoujiduoduo.util.af.b(android.content.Context, java.lang.String, java.lang.String):boolean
      com.shoujiduoduo.util.af.b(android.content.Context, java.lang.String, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, int):int
     arg types: [com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, long):long
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.af.a(android.content.Context, java.lang.String, int):int */
    public void onCreate(Bundle bundle) {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onCreate");
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_main);
        com.jaeger.library.a.a(this, j.a(R.color.bkg_green), 0);
        r = this;
        c.a();
        n.a().a(this.B);
        o.a(getApplicationContext());
        ae.a(getApplicationContext());
        com.shoujiduoduo.util.c.a.a(getApplicationContext());
        af.b((Context) this, o, af.a((Context) this, o, 0) + 1);
        b();
        this.t = com.shoujiduoduo.util.a.f();
        if (this.t) {
            com.shoujiduoduo.a.b.b.c().h();
        }
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onCreate 1");
        setVolumeControlStream(3);
        Intent intent = new Intent(this, PlayerService.class);
        startService(intent);
        bindService(intent, this.A, 1);
        this.q = true;
        c();
        this.B.sendEmptyMessageDelayed(1134, 2000);
        this.v = new com.shoujiduoduo.ui.settings.a(this, R.style.DuoDuoDialog);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CONFIG, this.w);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.y);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_APP, this.z);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.x);
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onCreate:end");
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_MusicAlbum:
                com.umeng.a.b.b(this, "CLICK_MUSIC_ALBUM");
                Intent intent = new Intent(this, MusicAlbumActivity.class);
                intent.putExtra("type", MusicAlbumActivity.a.my_album);
                intent.putExtra("title", "音乐相册");
                startActivity(intent);
                return;
            case R.id.tv_record:
                if (this.j != null && this.j.l()) {
                    this.j.m();
                }
                Intent intent2 = new Intent();
                intent2.setClass(this, MakeRingActivity.class);
                intent2.setFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
                startActivity(intent2);
                return;
            default:
                return;
        }
    }

    private void b() {
        this.c = (TextView) findViewById(R.id.header_text);
        this.e = (RelativeLayout) findViewById(R.id.header);
        this.f = (RelativeLayout) findViewById(R.id.search_layout);
        this.g = (TextView) findViewById(R.id.tv_MusicAlbum);
        this.h = (TextView) findViewById(R.id.tv_record);
        this.i = (LinearLayout) findViewById(R.id.search_entrance);
        this.b = (ImageView) findViewById(R.id.iv_my_red_point);
        this.f1422a = (MyRadioButton) findViewById(R.id.buttonMyRingtone);
        if (com.shoujiduoduo.a.b.b.g().h() && com.shoujiduoduo.a.b.b.g().g()) {
            this.f1422a.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, j.b(R.drawable.btn_navi_myring_vip), (Drawable) null, (Drawable) null);
        }
        f();
        MyRadioButton myRadioButton = (MyRadioButton) findViewById(R.id.buttonMoreOptions);
        myRadioButton.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.d, (Drawable) null, (Drawable) null);
        myRadioButton.setText(e());
        this.f.findViewById(R.id.tv_record).setOnClickListener(this);
        this.f.findViewById(R.id.tv_MusicAlbum).setOnClickListener(this);
        ((LinearLayout) this.f.findViewById(R.id.search_entrance)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                RingToneDuoduoActivity.this.startActivity(new Intent(RingToneDuoduoActivity.this, SearchActivity.class));
            }
        });
        this.m.add(new HomepageFrag());
        this.m.add(new CategoryFrag());
        this.m.add(new MyRingtoneFrag());
        if (com.shoujiduoduo.util.a.b()) {
            this.m.add(new AdWallFrag());
        } else if (com.shoujiduoduo.util.a.c()) {
            this.m.add(new AiChangFrag());
        } else {
            this.m.add(new MoreOptionFrag());
        }
        this.l = (MyViewPager) findViewById(R.id.vPager);
        this.l.setOffscreenPageLimit(4);
        this.l.setCanScroll(true);
        this.l.setAdapter(new FragmentAdapter(getSupportFragmentManager()));
        this.n = (RadioGroup) findViewById(R.id.navi_group);
        this.n.setOnCheckedChangeListener(this);
        this.n.check(R.id.buttonHomepage);
        this.l.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrolled(int i, float f, int i2) {
            }

            public void onPageSelected(int i) {
                switch (i) {
                    case 0:
                        RingToneDuoduoActivity.this.n.check(R.id.buttonHomepage);
                        return;
                    case 1:
                        RingToneDuoduoActivity.this.n.check(R.id.buttonCategory);
                        return;
                    case 2:
                        RingToneDuoduoActivity.this.n.check(R.id.buttonMyRingtone);
                        return;
                    case 3:
                        RingToneDuoduoActivity.this.n.check(R.id.buttonMoreOptions);
                        return;
                    default:
                        return;
                }
            }

            public void onPageScrollStateChanged(int i) {
            }
        });
    }

    public class FragmentAdapter extends FragmentPagerAdapter {
        FragmentAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int i) {
            return (Fragment) RingToneDuoduoActivity.this.m.get(i);
        }

        public int getCount() {
            return RingToneDuoduoActivity.this.m.size();
        }
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i2) {
        switch (i2) {
            case R.id.buttonHomepage:
                this.l.setCurrentItem(0, false);
                this.e.setVisibility(0);
                this.f.setVisibility(0);
                this.i.setVisibility(0);
                this.h.setVisibility(0);
                this.g.setVisibility(0);
                this.c.setVisibility(4);
                return;
            case R.id.buttonCategory:
                this.l.setCurrentItem(1, false);
                this.e.setVisibility(0);
                this.f.setVisibility(0);
                this.i.setVisibility(0);
                this.h.setVisibility(0);
                this.g.setVisibility(0);
                this.c.setVisibility(4);
                return;
            case R.id.buttonMyRingtone:
                this.l.setCurrentItem(2, false);
                this.e.setVisibility(0);
                this.f.setVisibility(0);
                this.i.setVisibility(4);
                this.h.setVisibility(0);
                this.g.setVisibility(4);
                this.c.setVisibility(0);
                this.c.setText("我的");
                this.b.setVisibility(8);
                this.u = true;
                return;
            case R.id.buttonMoreOptions:
                this.l.setCurrentItem(3, false);
                if (com.shoujiduoduo.util.a.b()) {
                    this.e.setVisibility(8);
                    return;
                } else if (com.shoujiduoduo.util.a.c()) {
                    this.e.setVisibility(8);
                    return;
                } else {
                    this.e.setVisibility(0);
                    this.f.setVisibility(4);
                    this.c.setVisibility(0);
                    this.c.setText("更多");
                    return;
                }
            default:
                return;
        }
    }

    private void c() {
        com.shoujiduoduo.a.a.c.a().b(new c.b() {
            public void a() {
                Intent intent = RingToneDuoduoActivity.this.getIntent();
                if (intent != null) {
                    String stringExtra = intent.getStringExtra("action");
                    com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "action = " + stringExtra);
                    if (stringExtra == null) {
                        return;
                    }
                    if (stringExtra.equalsIgnoreCase("search")) {
                        final String stringExtra2 = intent.getStringExtra("para");
                        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "push task: search, keyword = " + stringExtra2);
                        if (stringExtra2 != null) {
                            new Thread() {
                                public void run() {
                                    try {
                                        RingToneDuoduoActivity.this.B.sendMessageDelayed(RingToneDuoduoActivity.this.B.obtainMessage(1131, stringExtra2), 1000);
                                    } catch (Exception e) {
                                    }
                                }
                            }.start();
                        }
                    } else if (stringExtra.equalsIgnoreCase(cn.banshenggua.aichang.player.PlayerService.ACTION_PLAY)) {
                        final String stringExtra3 = intent.getStringExtra("para");
                        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "push task: play, keyword = " + stringExtra3);
                        if (stringExtra3 != null) {
                            new Thread() {
                                public void run() {
                                    try {
                                        RingToneDuoduoActivity.this.B.sendMessageDelayed(RingToneDuoduoActivity.this.B.obtainMessage(1132, stringExtra3), 1000);
                                    } catch (Exception e) {
                                    }
                                }
                            }.start();
                        }
                    } else if (stringExtra.equalsIgnoreCase("webview")) {
                        RingToneDuoduoActivity.this.B.sendMessageDelayed(RingToneDuoduoActivity.this.B.obtainMessage(1133, intent.getStringExtra("para")), 1000);
                    } else if (stringExtra.equalsIgnoreCase("music_album")) {
                        b bVar = new b();
                        bVar.f1440a = intent.getStringExtra("para");
                        bVar.b = intent.getStringExtra("title");
                        RingToneDuoduoActivity.this.B.sendMessageDelayed(RingToneDuoduoActivity.this.B.obtainMessage(1135, bVar), 1000);
                    } else if (!stringExtra.equalsIgnoreCase("ad") && !stringExtra.equalsIgnoreCase("update")) {
                        com.shoujiduoduo.base.a.a.c("RingToneDuoduoActivity", "not support action");
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        UMShareAPI.get(this).onActivityResult(i2, i3, intent);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onStart");
        super.onStart();
        this.p = new a();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("install_apk_from_start_ad");
        registerReceiver(this.p, intentFilter);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onStop");
        unregisterReceiver(this.p);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onDestroy");
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.y);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.x);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_APP, this.z);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CONFIG, this.w);
        if (com.shoujiduoduo.util.c.c.a() != null) {
            com.shoujiduoduo.util.c.c.a().b();
        }
        if (ab.a() != null) {
            ab.a().c();
        }
        ae.a(this).b();
        if (this.B != null) {
            this.B.removeCallbacksAndMessages(null);
        }
        if (com.shoujiduoduo.util.c.b.a().e() && ad.a().b("cm_sunshine_sdk_enable")) {
            try {
                InitCmmInterface.exitApp(this);
            } catch (Throwable th) {
            }
        }
        if (this == r) {
            r = null;
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.q) {
            unbindService(this.A);
            this.q = false;
        }
        this.A = null;
        stopService(new Intent(this, PlayerService.class));
        ab.a().a(null);
        this.j = null;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onResume");
        super.onResume();
        PushManager.getInstance().initialize(getApplicationContext(), GTPushService.class);
        PushManager.getInstance().registerPushIntentService(getApplicationContext(), DuoDuoIntentService.class);
        PushManager.getInstance().setSilentTime(getApplicationContext(), 22, 9);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onPause");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onRestart");
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onNewIntent");
        super.onNewIntent(intent);
        setIntent(intent);
        c();
    }

    private class a extends BroadcastReceiver {
        private a() {
        }

        public void onReceive(Context context, Intent intent) {
            com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "receive broadcast");
            if (intent.getAction().equals("install_apk_from_start_ad")) {
                final String stringExtra = intent.getStringExtra("PackagePath");
                String stringExtra2 = intent.getStringExtra("PackageName");
                if (!TextUtils.isEmpty(stringExtra2) && !TextUtils.isEmpty(stringExtra)) {
                    new b.a(RingToneDuoduoActivity.this).b((int) R.string.hint).a(stringExtra2 + RingToneDuoduoActivity.this.getResources().getString(R.string.start_ad_down_apk_hint)).a((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            g.b(stringExtra);
                            dialogInterface.dismiss();
                        }
                    }).b((int) R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).a().show();
                }
            }
        }
    }

    private String e() {
        if (com.shoujiduoduo.util.a.b()) {
            return ad.a().a("navi_ad_title");
        }
        if (com.shoujiduoduo.util.a.c()) {
            return ad.a().a("aichang_title");
        }
        return "更多";
    }

    private void f() {
        if (com.shoujiduoduo.util.a.b()) {
            if (com.shoujiduoduo.util.a.a().equals("shop")) {
                this.d = getResources().getDrawable(R.drawable.btn_navi_shop);
            } else if (com.shoujiduoduo.util.a.a().equals("news")) {
                this.d = getResources().getDrawable(R.drawable.btn_navi_discover);
            } else if (com.shoujiduoduo.util.a.a().equals("video")) {
                this.d = getResources().getDrawable(R.drawable.btn_navi_aichang);
            } else {
                this.d = getResources().getDrawable(R.drawable.btn_navi_shop);
            }
        } else if (com.shoujiduoduo.util.a.c()) {
            this.d = getResources().getDrawable(R.drawable.btn_navi_aichang);
        } else {
            this.d = getResources().getDrawable(R.drawable.btn_navi_more);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        this.v.show();
        return true;
    }

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        public String f1440a;
        public String b;

        private b() {
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        if (str != null && str.length() > 0) {
            new b.a(this).b((int) R.string.update_hint).a((int) R.string.has_update_hint).a((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    d.a("新版本已开始下载");
                    new al(RingToneDuoduoActivity.this, ad.a().a("update_url")).execute(new Void[0]);
                }
            }).b((int) R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).a().show();
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        String a2;
        if (!s) {
            s = true;
            String a3 = ad.a().a("update_version");
            com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "onConfigListener: update version: " + a3);
            com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "onConfigListener: cur version: " + g.q());
            if (a3.compareToIgnoreCase(g.q()) > 0 && (a2 = ad.a().a("update_url")) != null && a2.length() > 0) {
                Message obtainMessage = this.B.obtainMessage(1100, a2);
                com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "onConfigListener: update url: " + a2);
                this.B.sendMessage(obtainMessage);
            }
        }
    }
}
