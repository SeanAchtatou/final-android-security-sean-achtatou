package com.shoujiduoduo.a.a;

import com.shoujiduoduo.a.c.a;
import com.shoujiduoduo.a.c.c;
import com.shoujiduoduo.a.c.d;
import com.shoujiduoduo.a.c.e;
import com.shoujiduoduo.a.c.f;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.a.c.k;
import com.shoujiduoduo.a.c.l;
import com.shoujiduoduo.a.c.m;
import com.shoujiduoduo.a.c.n;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.a.c.q;
import com.shoujiduoduo.a.c.r;
import com.shoujiduoduo.a.c.s;
import com.shoujiduoduo.a.c.t;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.a.c.x;

/* compiled from: MessageID */
public enum b {
    OBSERVER_ID_RESERVE {
        public Class<? extends a> a() {
            return null;
        }
    },
    OBSERVER_APP {
        /* access modifiers changed from: package-private */
        public Class<? extends a> a() {
            return a.class;
        }
    },
    OBSERVER_PLAY_STATUS {
        public Class<? extends a> a() {
            return o.class;
        }
    },
    OBSERVER_RING_CHANGE {
        public Class<? extends a> a() {
            return p.class;
        }
    },
    OBSERVER_RING_UPLOAD {
        public Class<? extends a> a() {
            return q.class;
        }
    },
    OBSERVER_LIST_DATA {
        public Class<? extends a> a() {
            return g.class;
        }
    },
    OBSERVER_LIST_AREA {
        public Class<? extends a> a() {
            return l.class;
        }
    },
    OBSERVER_CAILING {
        public Class<? extends a> a() {
            return c.class;
        }
    },
    OBSERVER_USER_RING {
        public Class<? extends a> a() {
            return w.class;
        }
    },
    OBSERVER_USER_CENTER {
        public Class<? extends a> a() {
            return v.class;
        }
    },
    OBSERVER_CATEGORY {
        public Class<? extends a> a() {
            return d.class;
        }
    },
    OBSERVER_SEARCH_AD {
        public Class<? extends a> a() {
            return s.class;
        }
    },
    OBSERVER_BANNER_AD {
        public Class<? extends a> a() {
            return com.shoujiduoduo.a.c.b.class;
        }
    },
    OBSERVER_SEARCH_HOT_WORD {
        public Class<? extends a> a() {
            return t.class;
        }
    },
    OBSERVER_MAKE_RING {
        public Class<? extends a> a() {
            return n.class;
        }
    },
    OBSERVER_TOP_LIST {
        public Class<? extends a> a() {
            return u.class;
        }
    },
    OBSERVER_SCENE {
        public Class<? extends a> a() {
            return r.class;
        }
    },
    OBSERVER_CHANGE_BATCH {
        public Class<? extends a> a() {
            return m.class;
        }
    },
    OBSERVER_VIP {
        public Class<? extends a> a() {
            return x.class;
        }
    },
    OBSERVER_FEED_AD {
        public Class<? extends a> a() {
            return k.class;
        }
    },
    OBSERVER_CONFIG {
        public Class<? extends a> a() {
            return f.class;
        }
    },
    OBSERVER_COMMENT {
        public Class<? extends a> a() {
            return e.class;
        }
    };

    /* access modifiers changed from: package-private */
    public abstract Class<? extends a> a();
}
