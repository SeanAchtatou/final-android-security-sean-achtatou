package com.shoujiduoduo.a.a;

import android.os.Handler;
import com.shoujiduoduo.a.a.d;
import com.shoujiduoduo.ringtone.RingDDApp;
import java.util.ArrayList;

/* compiled from: MessageManager */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    static final c f1283a = new c();
    static final long b = RingDDApp.d();
    static final Handler c = RingDDApp.e();
    static boolean d;
    static ArrayList<ArrayList<a>> e = new ArrayList<>(b.values().length);

    /* compiled from: MessageManager */
    public static abstract class a<T extends a> implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        protected T f1284a;
        public b b = b.OBSERVER_ID_RESERVE;
        public boolean c = false;

        public abstract void a();

        public void run() {
            if (!c.d) {
                int ordinal = this.b.ordinal();
                ArrayList arrayList = c.e.get(ordinal);
                d.a a2 = d.a(ordinal, arrayList.size());
                while (a2.b < a2.c) {
                    this.f1284a = (a) arrayList.get(a2.b);
                    a();
                    a2.b++;
                }
                this.f1284a = null;
                d.a();
            }
            b();
        }

        /* access modifiers changed from: protected */
        public final void b() {
            if (this.c) {
                synchronized (this) {
                    notify();
                }
            }
        }
    }

    /* compiled from: MessageManager */
    public static abstract class b extends a<a> {
        public abstract void a();

        public final void run() {
            a();
            b();
        }
    }

    public static c a() {
        return f1283a;
    }

    public void a(b bVar, a aVar) {
        com.shoujiduoduo.base.a.a.a(bVar.a(), aVar);
        com.shoujiduoduo.base.a.a.a();
        ArrayList arrayList = e.get(bVar.ordinal());
        if (!arrayList.contains(aVar)) {
            arrayList.add(aVar);
            d.a(bVar.ordinal());
            return;
        }
        com.shoujiduoduo.base.a.a.c("MessageManager", "已经attach过了");
    }

    public void b(b bVar, a aVar) {
        com.shoujiduoduo.base.a.a.a(bVar.a(), aVar);
        com.shoujiduoduo.base.a.a.a();
        ArrayList arrayList = e.get(bVar.ordinal());
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (((a) arrayList.get(i)) == aVar) {
                arrayList.remove(aVar);
                d.b(bVar.ordinal(), i);
                return;
            }
        }
    }

    public <T extends a> void a(b bVar, a<T> aVar) {
        if (!RingDDApp.f()) {
            aVar.b = bVar;
            a(c, aVar);
        }
    }

    public <T extends a> void b(b bVar, a<T> aVar) {
        if (!RingDDApp.f()) {
            aVar.b = bVar;
            a(c, 0, aVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.a.a.c.a(android.os.Handler, com.shoujiduoduo.a.a.c$a):void
     arg types: [android.os.Handler, com.shoujiduoduo.a.a.c$b]
     candidates:
      com.shoujiduoduo.a.a.c.a(int, com.shoujiduoduo.a.a.c$b):void
      com.shoujiduoduo.a.a.c.a(android.os.Handler, com.shoujiduoduo.a.a.c$b):void
      com.shoujiduoduo.a.a.c.a(com.shoujiduoduo.a.a.b, com.shoujiduoduo.a.a.a):void
      com.shoujiduoduo.a.a.c.a(com.shoujiduoduo.a.a.b, com.shoujiduoduo.a.a.c$a):void
      com.shoujiduoduo.a.a.c.a(android.os.Handler, com.shoujiduoduo.a.a.c$a):void */
    public void a(b bVar) {
        a(c, (a) bVar);
    }

    public void a(int i, b bVar) {
        a(c, i, bVar);
    }

    public void b(b bVar) {
        a(c, bVar);
    }

    public <T extends a> void a(Handler handler, a<T> aVar) {
        long currentTimeMillis = System.currentTimeMillis();
        if (handler.getLooper().getThread().getId() == Thread.currentThread().getId()) {
            aVar.run();
        } else {
            aVar.c = true;
            try {
                synchronized (aVar) {
                    handler.post(aVar);
                    aVar.wait();
                }
                aVar.c = false;
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
        if (currentTimeMillis2 > 150 && Thread.currentThread().getId() == RingDDApp.d()) {
            com.shoujiduoduo.base.a.a.e("MessageManager", "同步消息执行超时，time=" + currentTimeMillis2);
        }
    }

    public <T extends a> void a(Handler handler, int i, a<T> aVar) {
        handler.postDelayed(aVar, (long) i);
    }

    public void a(Handler handler, b bVar) {
        a(handler, 0, bVar);
    }

    public void b() {
        d = true;
    }

    c() {
    }

    static {
        for (int i = 0; i < b.values().length; i++) {
            e.add(new ArrayList());
        }
    }
}
