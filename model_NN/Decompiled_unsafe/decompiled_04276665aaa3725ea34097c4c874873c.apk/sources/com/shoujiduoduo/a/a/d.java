package com.shoujiduoduo.a.a;

import java.util.ArrayList;

/* compiled from: ProcessingNotifyStack */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static int f1285a = 0;
    private static ArrayList<a> b = new ArrayList<>(2);

    /* compiled from: ProcessingNotifyStack */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        public int f1286a;
        public int b;
        public int c;
    }

    public static a a(int i, int i2) {
        a aVar;
        if (f1285a == b.size()) {
            aVar = new a();
            b.add(aVar);
            com.shoujiduoduo.base.a.a.e("MessageManager", "同步通知嵌套达到" + (f1285a + 1) + "层");
        } else {
            aVar = b.get(f1285a);
        }
        aVar.f1286a = i;
        aVar.b = 0;
        aVar.c = i2;
        f1285a++;
        return aVar;
    }

    public static void a() {
        f1285a--;
    }

    public static void a(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < f1285a) {
                a aVar = b.get(i3);
                if (aVar.f1286a == i) {
                    aVar.c++;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public static void b(int i, int i2) {
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 < f1285a) {
                a aVar = b.get(i4);
                if (aVar.f1286a == i) {
                    aVar.c--;
                    if (i2 <= aVar.b) {
                        aVar.b--;
                    }
                }
                i3 = i4 + 1;
            } else {
                return;
            }
        }
    }

    static {
        for (int i = 0; i < 2; i++) {
            b.add(new a());
        }
    }
}
