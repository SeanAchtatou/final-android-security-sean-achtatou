package com.shoujiduoduo.ui.makering;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.ao;
import com.shoujiduoduo.util.m;
import com.shoujiduoduo.util.r;
import com.shoujiduoduo.util.widget.IndexListView;
import com.shoujiduoduo.util.widget.b;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class MakeRingChooseMusicFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public IndexListView f1714a;
    private Thread b;
    /* access modifiers changed from: private */
    public ArrayList<c> c;
    /* access modifiers changed from: private */
    public f d;
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public LayoutInflater f;
    /* access modifiers changed from: private */
    public b g;
    /* access modifiers changed from: private */
    public View h;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public d j;
    private HashSet<String> k;
    /* access modifiers changed from: private */
    public ProgressDialog l = null;

    public interface d {
        void a(boolean z);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = new ArrayList<>();
        this.d = new f();
        this.g = new b();
        this.k = new HashSet<>();
        this.k.add(m.a(3));
        this.k.add(m.a(2));
        this.k.add(m.a(8) + "CailingDD/cache/");
        this.k.add(m.a(8) + "CailingDD/recordings/");
        this.k.add(m.a(8) + "Wallpaper/Ring/cache/");
        this.k.add(m.a(0) + "CailingDuoduo/cache/");
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.j = (d) activity;
        } catch (ClassCastException e2) {
            e2.printStackTrace();
            throw new ClassCastException(activity.toString() + " must implement OnDecodeAudioListener");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f = layoutInflater;
        View inflate = layoutInflater.inflate((int) R.layout.fragment_makering_choose_music, viewGroup, false);
        this.f1714a = (IndexListView) inflate.findViewById(R.id.lv_choose_music_list);
        this.f1714a.setFastScrollEnabled(true);
        this.f1714a.setOnItemClickListener(new a());
        this.f1714a.setVisibility(4);
        this.h = inflate.findViewById(R.id.layout_scan_loading);
        if (!this.i) {
            this.b = new e();
            this.b.start();
        } else {
            this.g.a(this.c);
            this.f1714a.setAdapter((ListAdapter) this.g);
            this.h.setVisibility(4);
            this.f1714a.setVisibility(0);
        }
        return inflate;
    }

    private class a implements AdapterView.OnItemClickListener {
        private a() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            c cVar = (c) MakeRingChooseMusicFragment.this.c.get(i);
            com.shoujiduoduo.base.a.a.b("MakeRingChooseMusicFragment", "choose music click item, path:" + cVar.d);
            ao.a().a(cVar.d, MakeRingChooseMusicFragment.this.d);
            com.umeng.a.b.b(MakeRingChooseMusicFragment.this.getActivity(), "EDIT_RING");
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.b != null && this.b.isAlive()) {
            this.e = true;
        }
        if (this.d != null) {
            this.d.removeCallbacksAndMessages(null);
        }
    }

    private class c {

        /* renamed from: a  reason: collision with root package name */
        String f1717a;
        String b;
        int c;
        String d;
        String e;

        private c() {
            this.f1717a = "";
            this.b = "";
            this.c = 0;
            this.d = "";
            this.e = "";
        }
    }

    private class f extends Handler {
        private f() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.a(com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment, int]
         candidates:
          com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.a(com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment, android.app.ProgressDialog):android.app.ProgressDialog
          com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.a(com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment, java.lang.String):boolean
          com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.a(com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment, boolean):boolean */
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    boolean unused = MakeRingChooseMusicFragment.this.i = true;
                    MakeRingChooseMusicFragment.this.g.a(MakeRingChooseMusicFragment.this.c);
                    MakeRingChooseMusicFragment.this.f1714a.setAdapter((ListAdapter) MakeRingChooseMusicFragment.this.g);
                    MakeRingChooseMusicFragment.this.h.setVisibility(4);
                    MakeRingChooseMusicFragment.this.f1714a.setVisibility(0);
                    break;
                case 2:
                    Toast.makeText(MakeRingChooseMusicFragment.this.getActivity(), (int) R.string.search_local_music_error, 1).show();
                    new b.a(MakeRingChooseMusicFragment.this.getActivity()).b("提示").a("若无法获取本地音乐列表，请尝试通过手机的“权限管理”功能，设置铃声多多的存储权限。如何设置请百度 “机型 + 权限管理”").a("确定", (DialogInterface.OnClickListener) null).a().show();
                    MakeRingChooseMusicFragment.this.h.setVisibility(4);
                    break;
                case 11:
                    if (MakeRingChooseMusicFragment.this.l != null) {
                        MakeRingChooseMusicFragment.this.l.dismiss();
                    }
                    Toast.makeText(MakeRingChooseMusicFragment.this.getActivity(), (int) R.string.decode_error_hint, 1).show();
                    break;
                case 12:
                    MakeRingChooseMusicFragment.this.j.a(true);
                    if (MakeRingChooseMusicFragment.this.l != null) {
                        MakeRingChooseMusicFragment.this.l.dismiss();
                        break;
                    }
                    break;
                case 13:
                    if (MakeRingChooseMusicFragment.this.l == null) {
                        ProgressDialog unused2 = MakeRingChooseMusicFragment.this.l = new ProgressDialog(MakeRingChooseMusicFragment.this.getActivity());
                        MakeRingChooseMusicFragment.this.l.setMessage(MakeRingChooseMusicFragment.this.getResources().getString(R.string.decode_music_hint));
                        MakeRingChooseMusicFragment.this.l.setIndeterminate(false);
                        MakeRingChooseMusicFragment.this.l.setCancelable(true);
                    }
                    MakeRingChooseMusicFragment.this.l.show();
                    break;
            }
            super.handleMessage(message);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(String str) {
        File file = new File(str);
        String b2 = r.b(str);
        if (!file.exists() || b2 == null) {
            return false;
        }
        if (file.length() <= 128 || file.isHidden() || !b2.equalsIgnoreCase("mp3")) {
            com.shoujiduoduo.base.a.a.a("MakeRingChooseMusicFragment", "格式过滤：" + str);
            return false;
        }
        if (!this.k.contains(r.e(str) + "/")) {
            return true;
        }
        com.shoujiduoduo.base.a.a.a("MakeRingChooseMusicFragment", "过滤音频， path：" + str);
        return false;
    }

    private class e extends Thread {
        private e() {
        }

        /* JADX WARNING: CFG modification limit reached, blocks count: 130 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r13 = this;
                r7 = 2
                r12 = 0
                r11 = 1
                r6 = 0
                java.lang.String r0 = "MakeRingChooseMusicFragment"
                java.lang.String r1 = "begin scan"
                com.shoujiduoduo.base.a.a.a(r0, r1)
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment r0 = com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.this
                java.util.ArrayList r0 = r0.c
                r0.clear()
                long r8 = java.lang.System.currentTimeMillis()
                android.content.Context r0 = com.shoujiduoduo.ringtone.RingDDApp.c()     // Catch:{ SecurityException -> 0x0052, Exception -> 0x005f }
                android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ SecurityException -> 0x0052, Exception -> 0x005f }
                android.net.Uri r1 = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI     // Catch:{ SecurityException -> 0x0052, Exception -> 0x005f }
                r2 = 5
                java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SecurityException -> 0x0052, Exception -> 0x005f }
                r3 = 0
                java.lang.String r4 = "artist"
                r2[r3] = r4     // Catch:{ SecurityException -> 0x0052, Exception -> 0x005f }
                r3 = 1
                java.lang.String r4 = "title"
                r2[r3] = r4     // Catch:{ SecurityException -> 0x0052, Exception -> 0x005f }
                r3 = 2
                java.lang.String r4 = "duration"
                r2[r3] = r4     // Catch:{ SecurityException -> 0x0052, Exception -> 0x005f }
                r3 = 3
                java.lang.String r4 = "mime_type"
                r2[r3] = r4     // Catch:{ SecurityException -> 0x0052, Exception -> 0x005f }
                r3 = 4
                java.lang.String r4 = "_data"
                r2[r3] = r4     // Catch:{ SecurityException -> 0x0052, Exception -> 0x005f }
                r3 = 0
                r4 = 0
                java.lang.String r5 = "title"
                android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ SecurityException -> 0x0052, Exception -> 0x005f }
            L_0x0046:
                if (r0 != 0) goto L_0x0065
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment r0 = com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.this
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment$f r0 = r0.d
                r0.sendEmptyMessage(r7)
            L_0x0051:
                return
            L_0x0052:
                r0 = move-exception
                r0.printStackTrace()
                java.lang.String r0 = "MakeRingChooseMusicFragment"
                java.lang.String r1 = "读取系统Media信息失败，没有获取权限"
                com.shoujiduoduo.base.a.a.c(r0, r1)
                r0 = r6
                goto L_0x0046
            L_0x005f:
                r0 = move-exception
                r0.printStackTrace()
                r0 = r6
                goto L_0x0046
            L_0x0065:
                int r1 = r0.getCount()
                if (r1 > 0) goto L_0x00c9
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment r1 = com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.this
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment$f r1 = r1.d
                r1.sendEmptyMessage(r11)
                r0.close()
                goto L_0x0051
            L_0x0078:
                java.lang.String r1 = "artist"
                int r1 = r0.getColumnIndex(r1)
                java.lang.String r1 = r0.getString(r1)
                java.lang.String r2 = "title"
                int r2 = r0.getColumnIndex(r2)
                java.lang.String r2 = r0.getString(r2)
                java.lang.String r3 = "duration"
                int r3 = r0.getColumnIndex(r3)
                int r3 = r0.getInt(r3)
                java.lang.String r4 = "mime_type"
                int r4 = r0.getColumnIndex(r4)
                r0.getString(r4)
                java.lang.String r4 = "_data"
                int r4 = r0.getColumnIndex(r4)
                java.lang.String r4 = r0.getString(r4)
                java.lang.String r5 = "MakeRingChooseMusicFragment"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r10 = "path:"
                java.lang.StringBuilder r7 = r7.append(r10)
                java.lang.StringBuilder r7 = r7.append(r4)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r5, r7)
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment r5 = com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.this
                boolean r5 = r5.a(r4)
                if (r5 != 0) goto L_0x0113
            L_0x00c9:
                boolean r1 = r0.moveToNext()
                if (r1 == 0) goto L_0x00d7
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment r1 = com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.this
                boolean r1 = r1.e
                if (r1 == 0) goto L_0x0078
            L_0x00d7:
                r0.close()
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment r0 = com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.this
                java.util.ArrayList r0 = r0.c
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment$e$1 r1 = new com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment$e$1
                r1.<init>()
                java.util.Collections.sort(r0, r1)
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment r0 = com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.this
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment$f r0 = r0.d
                r0.sendEmptyMessage(r11)
                java.lang.String r0 = "MakeRingChooseMusicFragment"
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "first scan thread time:"
                java.lang.StringBuilder r1 = r1.append(r2)
                long r2 = java.lang.System.currentTimeMillis()
                long r2 = r2 - r8
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r1 = r1.toString()
                com.shoujiduoduo.base.a.a.b(r0, r1)
                super.run()
                goto L_0x0051
            L_0x0113:
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment$c r5 = new com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment$c
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment r7 = com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.this
                r5.<init>()
                r5.f1717a = r2
                r5.c = r3
                r5.b = r1
                r5.d = r4
                if (r2 == 0) goto L_0x0134
                int r1 = r2.length()
                if (r1 <= 0) goto L_0x0134
                char r1 = r2.charAt(r12)
                java.lang.String r1 = com.shoujiduoduo.util.aa.a(r1)
                r5.e = r1
            L_0x0134:
                com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment r1 = com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.this
                java.util.ArrayList r1 = r1.c
                r1.add(r5)
                goto L_0x00c9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment.e.run():void");
        }
    }

    private class g {

        /* renamed from: a  reason: collision with root package name */
        TextView f1721a;
        TextView b;
        TextView c;
        TextView d;

        private g() {
        }
    }

    private class b extends BaseAdapter implements SectionIndexer {
        private final String b;
        private HashMap<String, Integer> c;
        private ArrayList<c> d;

        private b() {
            this.b = "ABCDEFGHIJKLMNOPQRSTUVWXYZ#";
        }

        public void a(ArrayList<c> arrayList) {
            this.d = arrayList;
            this.c = new HashMap<>();
            for (int i = 0; i < this.d.size(); i++) {
                String a2 = a(i);
                if (!this.c.containsKey(a2)) {
                    this.c.put(a2, Integer.valueOf(i));
                }
            }
            int i2 = 0;
            for (int i3 = 0; i3 < "ABCDEFGHIJKLMNOPQRSTUVWXYZ#".length(); i3++) {
                if (this.c.containsKey(String.valueOf("ABCDEFGHIJKLMNOPQRSTUVWXYZ#".charAt(i3)))) {
                    i2 = this.c.get(String.valueOf("ABCDEFGHIJKLMNOPQRSTUVWXYZ#".charAt(i3))).intValue();
                } else {
                    this.c.put(String.valueOf(String.valueOf("ABCDEFGHIJKLMNOPQRSTUVWXYZ#".charAt(i3))), Integer.valueOf(i2));
                }
            }
        }

        public int getCount() {
            return this.d.size();
        }

        public Object getItem(int i) {
            return this.d.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        private String a(int i) {
            String str = this.d.get(i).e.length() > 0 ? this.d.get(i).e.charAt(0) + "" : "";
            if (str.equals("") || !ai.a(str.charAt(0))) {
                str = "#";
            }
            return str.toUpperCase();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            g gVar;
            String str;
            if (view == null) {
                view = MakeRingChooseMusicFragment.this.f.inflate((int) R.layout.listitem_local_song, viewGroup, false);
                gVar = new g();
                gVar.f1721a = (TextView) view.findViewById(R.id.tv_local_song_item_alpha);
                gVar.c = (TextView) view.findViewById(R.id.tv_local_song_title);
                gVar.d = (TextView) view.findViewById(R.id.tv_local_song_path);
                gVar.b = (TextView) view.findViewById(R.id.tv_local_song_index);
                view.setTag(gVar);
            } else {
                gVar = (g) view.getTag();
            }
            c cVar = this.d.get(i);
            gVar.f1721a.setText(a(i));
            gVar.c.setText(cVar.f1717a);
            String str2 = cVar.d;
            if (cVar.d.contains(m.a(0))) {
                str = cVar.d.replace(m.a(0), "");
            } else {
                str = str2;
            }
            gVar.d.setText(str);
            gVar.b.setText(String.valueOf(i + 1));
            String a2 = a(i);
            if (!(i + -1 >= 0 ? a(i - 1) : " ").equals(a2)) {
                gVar.f1721a.setVisibility(0);
                gVar.f1721a.setText(a2);
            } else {
                gVar.f1721a.setVisibility(8);
            }
            return view;
        }

        public Object[] getSections() {
            getClass();
            String[] strArr = new String["ABCDEFGHIJKLMNOPQRSTUVWXYZ#".length()];
            int i = 0;
            while (true) {
                getClass();
                if (i >= "ABCDEFGHIJKLMNOPQRSTUVWXYZ#".length()) {
                    return strArr;
                }
                getClass();
                strArr[i] = String.valueOf("ABCDEFGHIJKLMNOPQRSTUVWXYZ#".charAt(i));
                i++;
            }
        }

        public int getPositionForSection(int i) {
            try {
                return this.c.get(getSections()[i]).intValue();
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        }

        public int getSectionForPosition(int i) {
            return 0;
        }
    }
}
