package com.shoujiduoduo.ui.makering;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.n;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.makering.MakeRingChooseMusicFragment;
import com.shoujiduoduo.ui.makering.MakeRingRecordFragment;
import com.shoujiduoduo.ui.makering.MakeRingSaveFragment;
import com.shoujiduoduo.ui.makering.MakeRingTypeFragment;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.util.ao;

public class MakeRingActivity extends BaseFragmentActivity implements MakeRingChooseMusicFragment.d, MakeRingRecordFragment.b, MakeRingSaveFragment.b, MakeRingTypeFragment.a {

    /* renamed from: a  reason: collision with root package name */
    private ImageButton f1706a;
    private TextView b;
    /* access modifiers changed from: private */
    public a c;
    private MakeRingTypeFragment d;
    private MakeRingChooseMusicFragment e;
    /* access modifiers changed from: private */
    public MakeRingRecordFragment f;
    private MakeRingSaveFragment g;

    public enum a {
        choose_type,
        choose_song,
        record_edit,
        song_edit,
        save_ring,
        upload_ring
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_make_ring);
        com.jaeger.library.a.a(this, getResources().getColor(R.color.bkg_green), 0);
        this.c = a.choose_type;
        this.f1706a = (ImageButton) findViewById(R.id.backButton);
        this.b = (TextView) findViewById(R.id.header_text);
        this.b.setText((int) R.string.ringtone_diy);
        this.f1706a.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean unused = MakeRingActivity.this.d();
            }
        });
        a(a.choose_type);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            return d();
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public boolean d() {
        switch (this.c) {
            case choose_type:
                finish();
                break;
            case record_edit:
            case song_edit:
                if (this.f != null && !this.f.a()) {
                    if (!this.f.c()) {
                        a(a.choose_type);
                        break;
                    } else {
                        new AlertDialog.Builder(this).setMessage(this.c == a.record_edit ? R.string.record_quit_confirm : R.string.edit_quit_confirm).setIcon(17301543).setTitle((int) R.string.hint).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ao.a().h();
                                com.shoujiduoduo.player.a.b().j();
                                MakeRingActivity.this.f.b();
                                if (MakeRingActivity.this.c.equals(a.song_edit)) {
                                    a unused = MakeRingActivity.this.c = a.choose_song;
                                    MakeRingActivity.this.getSupportFragmentManager().popBackStack();
                                    return;
                                }
                                MakeRingActivity.this.a(a.choose_type);
                            }
                        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        }).show();
                        break;
                    }
                }
            case save_ring:
                if (this.g.a()) {
                    finish();
                    break;
                } else {
                    new AlertDialog.Builder(this).setMessage((int) R.string.record_quit_confirm).setIcon(17301543).setTitle((int) R.string.hint).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ao.a().h();
                            com.shoujiduoduo.player.a.b().j();
                            MakeRingActivity.this.a(a.choose_type);
                        }
                    }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    }).show();
                    break;
                }
            case choose_song:
                a(a.choose_type);
                break;
        }
        return true;
    }

    private void e() {
        finish();
        c.a().b(b.OBSERVER_MAKE_RING, new c.a<n>() {
            public void a() {
                ((n) this.f1284a).a();
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(a aVar) {
        this.c = aVar;
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        switch (aVar) {
            case choose_type:
                this.b.setText((int) R.string.ringtone_diy);
                if (this.d == null) {
                    this.d = new MakeRingTypeFragment();
                }
                if (this.f != null) {
                    this.f.a(MakeRingRecordFragment.d.record);
                }
                beginTransaction.replace(R.id.details, this.d);
                break;
            case record_edit:
                this.b.setText((int) R.string.record);
                if (this.f == null) {
                    this.f = new MakeRingRecordFragment();
                }
                beginTransaction.replace(R.id.details, this.f);
                break;
            case song_edit:
                this.b.setText((int) R.string.edit);
                if (this.f == null) {
                    this.f = new MakeRingRecordFragment();
                }
                this.f.a(MakeRingRecordFragment.d.song_edit);
                beginTransaction.replace(R.id.details, this.f);
                break;
            case save_ring:
                this.b.setText((int) R.string.ring_save);
                if (this.g == null) {
                    this.g = new MakeRingSaveFragment();
                }
                beginTransaction.replace(R.id.details, this.g);
                break;
            case choose_song:
                this.b.setText((int) R.string.choose_song);
                if (this.e == null) {
                    this.e = new MakeRingChooseMusicFragment();
                }
                beginTransaction.replace(R.id.details, this.e);
                break;
            case upload_ring:
                this.b.setText((int) R.string.ring_upload);
                break;
        }
        beginTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        beginTransaction.addToBackStack(this.b.getText().toString());
        beginTransaction.commitAllowingStateLoss();
    }

    public void a(String str) {
        if (str.equals("record")) {
            a(a.record_edit);
        } else if (str.equals("edit")) {
            a(a.choose_song);
        }
    }

    public void a(boolean z) {
        if (z) {
            a(a.song_edit);
        }
    }

    public void a() {
        a(a.choose_type);
    }

    public void b() {
        e();
    }

    public void c() {
        a(a.save_ring);
    }

    public void b(String str) {
        this.b.setText(str);
    }
}
