package com.shoujiduoduo.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;
import com.jaeger.library.a;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.mine.FavoriteRingFragment;
import com.shoujiduoduo.ui.mine.MakeRingFragment;
import com.shoujiduoduo.ui.mine.UserCollectFragment;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.SlidingActivity;
import com.shoujiduoduo.ui.utils.j;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.ai;
import com.sina.weibo.sdk.component.WidgetRequestParam;

public class RingListActivity extends SlidingActivity {
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Fragment fragment;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_ringlist);
        a.a(this, j.a(R.color.bkg_green), 0);
        Intent intent = getIntent();
        if (intent != null) {
            String stringExtra = intent.getStringExtra("list_id");
            ((TextView) findViewById(R.id.title)).setText(intent.getStringExtra("title"));
            if ("user_favorite".equals(stringExtra)) {
                fragment = new FavoriteRingFragment();
            } else if ("user_collect".equals(stringExtra)) {
                fragment = new UserCollectFragment();
            } else if ("user_make".equals(stringExtra)) {
                fragment = new MakeRingFragment();
            } else {
                Fragment dDListFragment = new DDListFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putString("adapter_type", "ring_list_adapter");
                String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "feed_ad_list_id");
                boolean f = com.shoujiduoduo.util.a.f();
                if ((ai.b(a2) || a2.contains(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY)) && f) {
                    bundle2.putBoolean("support_feed_ad", true);
                }
                dDListFragment.setArguments(bundle2);
                ((DDListFragment) dDListFragment).a(new com.shoujiduoduo.b.c.j(ListType.LIST_TYPE.list_ring_normal, stringExtra, false, ""));
                fragment = dDListFragment;
            }
            FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
            beginTransaction.add((int) R.id.list_frag_layout, fragment);
            beginTransaction.commit();
        }
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                RingListActivity.this.finish();
            }
        });
    }

    public void a() {
    }

    public void b() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        PlayerService b = ab.a().b();
        if (b != null && b.l()) {
            b.m();
        }
    }
}
