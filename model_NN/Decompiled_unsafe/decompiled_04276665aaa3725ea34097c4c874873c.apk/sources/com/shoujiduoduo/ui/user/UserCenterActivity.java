package com.shoujiduoduo.ui.user;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.CailingManageActivity;
import com.shoujiduoduo.ui.cailing.e;
import com.shoujiduoduo.ui.cailing.f;
import com.shoujiduoduo.ui.settings.ChangeSkinActivity;
import com.shoujiduoduo.ui.utils.SlidingActivity;
import com.shoujiduoduo.ui.utils.j;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.b.a;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UserCenterActivity extends SlidingActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final String f1967a = "UserCenterActivity";
    private Button b;
    private TextView c;
    /* access modifiers changed from: private */
    public TextView d;
    private RelativeLayout e;
    private ListView f;
    /* access modifiers changed from: private */
    public ArrayList<Map<String, Object>> g;
    private TextView h;
    private g.b i;
    /* access modifiers changed from: private */
    public Handler j = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 1) {
                UserCenterActivity.this.d();
                final String str = (String) message.obj;
                b.a().c(new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "cmcc sdk init succeed");
                        UserCenterActivity.this.f(str);
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        UserCenterActivity.this.e();
                        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "cmcc sdk init failed, try sms code init");
                        UserCenterActivity.this.b(str);
                    }
                });
            }
        }
    };
    private ProgressDialog k = null;
    /* access modifiers changed from: private */
    public ProgressDialog l = null;
    private v m = new v() {
        public void b(int i) {
            UserCenterActivity.this.f();
        }

        public void a(String str, boolean z) {
        }

        public void a(String str) {
        }

        public void a(int i) {
        }

        public void a(int i, boolean z, String str, String str2) {
        }
    };
    private x n = new x() {
        public void a(int i) {
            UserCenterActivity.this.f();
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_user_center);
        com.jaeger.library.a.a(this, j.a(R.color.bkg_green), 0);
        this.j = new Handler();
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.btn_manage_cailing).setOnClickListener(this);
        this.f = (ListView) findViewById(R.id.right_list);
        this.e = (RelativeLayout) findViewById(R.id.open_vip);
        this.e.setOnClickListener(this);
        this.h = (TextView) findViewById(R.id.cost_hint);
        this.g = i();
        this.f.setAdapter((ListAdapter) new a());
        this.c = (TextView) findViewById(R.id.title);
        this.d = (TextView) findViewById(R.id.cailing_name);
        this.b = (Button) findViewById(R.id.btn_change_phone);
        findViewById(R.id.user_logout).setOnClickListener(this);
        switch (g.s()) {
            case f2309a:
                this.h.setText((int) R.string.six_yuan_per_month);
                break;
            case cu:
                this.h.setText((int) R.string.five_yuan_per_month);
                break;
            case ct:
                this.h.setText((int) R.string.six_yuan_per_month);
                break;
            default:
                com.shoujiduoduo.base.a.a.e("UserCenterActivity", "unknown service type ");
                break;
        }
        f();
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.n);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.m);
    }

    public void a() {
    }

    public void b() {
        finish();
    }

    /* access modifiers changed from: private */
    public void f() {
        if (com.shoujiduoduo.a.b.b.g().h()) {
            this.c.setText("我的VIP特权");
            this.d.setText("正在查询彩铃...");
            com.shoujiduoduo.util.b.a.a(g.s(), new a.b() {
                public void a(a.C0050a aVar) {
                    UserCenterActivity.this.d.setText(aVar.c + "-" + aVar.b);
                }

                public void a(String str, String str2) {
                    UserCenterActivity.this.d.setText("当前彩铃查询失败");
                }
            });
            this.e.setVisibility(4);
            return;
        }
        this.c.setText("开通VIP会员");
        this.d.setText("尚未设置，开通VIP可设置彩铃");
        this.d.setTextColor(getResources().getColor(R.color.text_orange));
        this.b.setVisibility(8);
        this.e.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void g() {
        com.umeng.socialize.c.a aVar = null;
        switch (com.shoujiduoduo.a.b.b.g().d()) {
            case 2:
                aVar = com.umeng.socialize.c.a.QQ;
                break;
            case 3:
                aVar = com.umeng.socialize.c.a.SINA;
                break;
            case 5:
                aVar = com.umeng.socialize.c.a.WEIXIN;
                break;
        }
        if (aVar != null) {
            ak.a().a(this, aVar);
        }
        com.umeng.a.b.b(RingDDApp.c(), "USER_LOGOUT");
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        c2.setLoginStatus(0);
        com.shoujiduoduo.a.b.b.g().a(c2);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
            public void a() {
                ((v) this.f1284a).a(com.shoujiduoduo.a.b.b.g().d());
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        new e(this, R.style.DuoDuoDialog, str, this.i, new e.a() {
            public void a(String str) {
                UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                if (!c.isLogin()) {
                    c.setUserName(str);
                    c.setUid("phone_" + str);
                }
                c.setPhoneNum(str);
                c.setLoginStatus(1);
                com.shoujiduoduo.a.b.b.g().a(c);
                UserCenterActivity.this.c(str);
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void h() {
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        if (!ai.c(c2.getPhoneNum())) {
            this.i = g.g(c2.getPhoneNum());
            c(c2.getPhoneNum());
            return;
        }
        switch (g.s()) {
            case f2309a:
                this.i = g.b.f2309a;
                c("");
                return;
            case cu:
                this.i = g.b.cu;
                b("");
                return;
            case ct:
                this.i = g.b.ct;
                String h2 = g.h();
                if (!TextUtils.isEmpty(h2)) {
                    a("请稍候...");
                    com.shoujiduoduo.util.d.b.a().b(h2, new com.shoujiduoduo.util.b.b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            UserCenterActivity.this.e();
                            if (bVar == null || !(bVar instanceof c.i)) {
                                UserCenterActivity.this.b("");
                                return;
                            }
                            UserCenterActivity.this.b(((c.i) bVar).d());
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            UserCenterActivity.this.e();
                            UserCenterActivity.this.b("");
                        }
                    });
                    return;
                }
                b("");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        a("请稍候...");
        if (ai.c(str)) {
            e(str);
            return;
        }
        switch (g.g(str)) {
            case f2309a:
                e(str);
                return;
            case cu:
                if (com.shoujiduoduo.util.e.a.a().a(str)) {
                    com.shoujiduoduo.base.a.a.a("UserCenterActivity", "当前手机号有token， phone:" + str);
                    g(str);
                    return;
                }
                b(str);
                return;
            case ct:
                d(str);
                return;
            default:
                e();
                com.shoujiduoduo.base.a.a.c("UserCenterActivity", "unknown phone type");
                return;
        }
    }

    private void d(final String str) {
        com.shoujiduoduo.util.d.b.a().a(str, new com.shoujiduoduo.util.b.b() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
             arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
             candidates:
              com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, android.app.ProgressDialog):android.app.ProgressDialog
              com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
              com.shoujiduoduo.ui.user.UserCenterActivity.a(java.lang.String, boolean):void
              com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
             arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
             candidates:
              com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
              com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
             arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
             candidates:
              com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
              com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
            public void a(c.b bVar) {
                super.a(bVar);
                UserCenterActivity.this.e();
                if (bVar != null && (bVar instanceof c.e)) {
                    c.e eVar = (c.e) bVar;
                    if (eVar.d() && (eVar.e() || eVar.f())) {
                        UserCenterActivity.this.c(true);
                        d.a("当前手机号已经是多多VIP会员啦");
                    } else if (eVar.d() && !eVar.e() && !eVar.f()) {
                        UserCenterActivity.this.b(false);
                    } else if (!eVar.d() && (eVar.e() || eVar.f())) {
                        UserCenterActivity.this.c(true);
                        if (com.shoujiduoduo.util.d.b.a().a(str).equals(b.d.wait_open)) {
                            d.a("正在为您开通彩铃业务，请耐心等待一会儿...");
                        } else {
                            UserCenterActivity.this.a(true);
                        }
                    } else if (!eVar.d() && !eVar.e() && !eVar.f()) {
                        if (com.shoujiduoduo.util.d.b.a().a(str).equals(b.d.wait_open)) {
                            d.a("正在为您开通彩铃业务，请耐心等待一会儿...");
                        } else {
                            UserCenterActivity.this.a(false);
                        }
                    }
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                UserCenterActivity.this.e();
            }
        });
    }

    private void e(final String str) {
        com.shoujiduoduo.util.c.b.a().a(new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                com.shoujiduoduo.base.a.a.a("UserCenterActivity", "cmcc sdk is inited");
                if (bVar instanceof c.n) {
                    c.n nVar = (c.n) bVar;
                    if (nVar.f2243a == c.n.a.sms_code || nVar.f2243a == c.n.a.all) {
                        UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                        if (!c.isLogin()) {
                            c.setUserName(nVar.d);
                            c.setUid("phone_" + nVar.d);
                        }
                        c.setPhoneNum(nVar.d);
                        c.setLoginStatus(1);
                        com.shoujiduoduo.a.b.b.g().a(c);
                        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                            public void a() {
                                ((v) this.f1284a).a(1, true, "", "");
                            }
                        });
                    }
                }
                UserCenterActivity.this.f(str);
            }

            public void b(c.b bVar) {
                super.b(bVar);
                com.shoujiduoduo.base.a.a.a("UserCenterActivity", "cmcc sdk is not inited");
                UserCenterActivity.this.c();
                Message message = new Message();
                message.what = 1;
                message.obj = str;
                UserCenterActivity.this.j.sendMessageDelayed(message, 3000);
            }
        });
    }

    /* access modifiers changed from: private */
    public void f(String str) {
        com.shoujiduoduo.util.c.b.a().e(new com.shoujiduoduo.util.b.b() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
             arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
             candidates:
              com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, android.app.ProgressDialog):android.app.ProgressDialog
              com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
              com.shoujiduoduo.ui.user.UserCenterActivity.a(java.lang.String, boolean):void
              com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
             arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
             candidates:
              com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
              com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
            public void a(c.b bVar) {
                super.a(bVar);
                UserCenterActivity.this.e();
                if (bVar instanceof c.d) {
                    c.d dVar = (c.d) bVar;
                    if (dVar.e() && dVar.d()) {
                        UserCenterActivity.this.c(true);
                        d.a("当前手机号已经是多多VIP会员啦");
                    } else if (dVar.e() && !dVar.d()) {
                        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "vip开通，彩铃关闭");
                        UserCenterActivity.this.c(true);
                    } else if (dVar.e() || !dVar.d()) {
                        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "均关闭");
                        UserCenterActivity.this.b(true);
                    } else {
                        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "vip关闭，彩铃开通");
                        UserCenterActivity.this.b(false);
                    }
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                UserCenterActivity.this.e();
                com.shoujiduoduo.base.a.a.a("UserCenterActivity", "查询状态失败");
                d.a("查询状态失败");
            }
        });
    }

    private void g(final String str) {
        a("请稍候...");
        com.shoujiduoduo.util.e.a.a().h(str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar != null && (bVar instanceof c.ah)) {
                    c.ah ahVar = (c.ah) bVar;
                    com.shoujiduoduo.base.a.a.a("UserCenterActivity", "user location, provinceid:" + ahVar.f2228a + ", province name:" + ahVar.d);
                    UserCenterActivity.this.a(str, com.shoujiduoduo.util.e.a.a().c(ahVar.f2228a));
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                UserCenterActivity.this.e();
                new b.a(UserCenterActivity.this).b("设置彩铃").a("获取当前手机号信息失败，请稍候再试试。").a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(final String str, final boolean z) {
        com.shoujiduoduo.util.e.a.a().f(new com.shoujiduoduo.util.b.b() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
             arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
             candidates:
              com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, android.app.ProgressDialog):android.app.ProgressDialog
              com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
              com.shoujiduoduo.ui.user.UserCenterActivity.a(java.lang.String, boolean):void
              com.shoujiduoduo.ui.user.UserCenterActivity.a(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
             arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
             candidates:
              com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
              com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
             arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
             candidates:
              com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
              com.shoujiduoduo.ui.user.UserCenterActivity.c(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar instanceof c.f) {
                    c.f fVar = (c.f) bVar;
                    if (fVar.d() && fVar.e()) {
                        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "彩铃与会员均开");
                        UserCenterActivity.this.e();
                        UserCenterActivity.this.c(true);
                        d.a("当前手机号已经是多多VIP会员啦");
                    } else if (fVar.d() && !fVar.e()) {
                        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "彩铃开，会员关闭");
                        UserCenterActivity.this.e();
                        if (z || fVar.i()) {
                            UserCenterActivity.this.b(false);
                        } else {
                            new b.a(UserCenterActivity.this).b("设置彩铃").a("抱歉，您所在的当前区域不支持铃声多多的彩铃功能！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                        }
                    } else if (!fVar.d() && fVar.e()) {
                        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "彩铃关，会员开");
                        UserCenterActivity.this.c(true);
                        if (fVar.j()) {
                            com.shoujiduoduo.base.a.a.a("UserCenterActivity", "属于免彩铃功能费范围，先 帮用户自动开通彩铃功能， net type:" + fVar.g() + ", location:" + fVar.f());
                            com.shoujiduoduo.util.e.a.a().e("&phone=" + str, new com.shoujiduoduo.util.b.b() {
                                public void a(c.b bVar) {
                                    super.a(bVar);
                                    UserCenterActivity.this.e();
                                    com.shoujiduoduo.base.a.a.a("UserCenterActivity", "成功开通彩铃基础功能");
                                    d.a("当前手机号已经是多多VIP会员啦");
                                }

                                public void b(c.b bVar) {
                                    super.b(bVar);
                                    UserCenterActivity.this.e();
                                    if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
                                        d.a("当前手机号已经是多多VIP会员啦");
                                    } else {
                                        new b.a(UserCenterActivity.this).b("设置彩铃").a("为您免费开通彩铃功能失败， 原因：" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                                    }
                                }
                            });
                        } else {
                            com.shoujiduoduo.base.a.a.a("UserCenterActivity", "不属于免彩铃功能费范围， 提示开通彩铃");
                            UserCenterActivity.this.e();
                            UserCenterActivity.this.a(false);
                        }
                    } else if (!fVar.d() && !fVar.e()) {
                        com.shoujiduoduo.base.a.a.a("UserCenterActivity", "彩铃、会员均关闭");
                        if (!z && !fVar.i()) {
                            new b.a(UserCenterActivity.this).b("设置彩铃").a("抱歉，您所在的当前区域不支持铃声多多的彩铃功能！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                        } else if (fVar.j()) {
                            com.shoujiduoduo.base.a.a.a("UserCenterActivity", "属于免彩铃功能费范围， 帮用户自动开通彩铃，net type:" + fVar.g() + ", location:" + fVar.f());
                            com.shoujiduoduo.util.e.a.a().e("&phone=" + str, new com.shoujiduoduo.util.b.b() {
                                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                 method: com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
                                 arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
                                 candidates:
                                  com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
                                  com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
                                public void a(c.b bVar) {
                                    super.a(bVar);
                                    UserCenterActivity.this.e();
                                    com.shoujiduoduo.base.a.a.a("UserCenterActivity", "成功开通彩铃基础功能, 提示开通会员");
                                    UserCenterActivity.this.b(false);
                                }

                                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                 method: com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
                                 arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
                                 candidates:
                                  com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
                                  com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
                                public void b(c.b bVar) {
                                    super.b(bVar);
                                    UserCenterActivity.this.e();
                                    if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
                                        UserCenterActivity.this.b(false);
                                    } else {
                                        new b.a(UserCenterActivity.this).b("设置彩铃").a("为您免费开通彩铃功能失败， 原因：" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                                    }
                                }
                            });
                        } else {
                            com.shoujiduoduo.base.a.a.a("UserCenterActivity", "不属于免彩铃功能费范围， 提示开通会员");
                            UserCenterActivity.this.e();
                            UserCenterActivity.this.a(true);
                        }
                    }
                    if (fVar.f2235a.a().equals("40307") || fVar.f2235a.a().equals("40308")) {
                        com.shoujiduoduo.util.e.a.a().a(str, "");
                        UserCenterActivity.this.e();
                        UserCenterActivity.this.b(str);
                    }
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                UserCenterActivity.this.e();
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(final boolean z) {
        new f(this, R.style.DuoDuoDialog, this.i, new f.a() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void
             arg types: [com.shoujiduoduo.ui.user.UserCenterActivity, int]
             candidates:
              com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, java.lang.String):void
              com.shoujiduoduo.ui.user.UserCenterActivity.b(com.shoujiduoduo.ui.user.UserCenterActivity, boolean):void */
            public void a(f.a.C0045a aVar) {
                if (!z && aVar.equals(f.a.C0045a.open)) {
                    UserCenterActivity.this.b(false);
                }
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        new com.shoujiduoduo.ui.cailing.d(this, this.i, null, "user_center", false, z, null).show();
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        final int i2;
        int i3 = 0;
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        if (this.i.equals(g.b.cu)) {
            i2 = 3;
        } else if (this.i.equals(g.b.ct)) {
            i2 = 2;
        } else if (this.i.equals(g.b.f2309a)) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        if (z) {
            i3 = i2;
        }
        c2.setVipType(i3);
        c2.setLoginStatus(1);
        com.shoujiduoduo.a.b.b.g().a(c2);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new c.a<x>() {
            public void a() {
                ((x) this.f1284a).a(i2);
            }
        });
    }

    private ArrayList<Map<String, Object>> i() {
        ArrayList<Map<String, Object>> arrayList = new ArrayList<>();
        HashMap hashMap = new HashMap();
        hashMap.put("icon", Integer.valueOf((int) R.drawable.vip_free));
        hashMap.put("title", "20万首彩铃免费换");
        hashMap.put("description", "换彩铃不用花钱啦");
        hashMap.put("arrow", Integer.valueOf((int) R.drawable.arraw_right));
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("icon", Integer.valueOf((int) R.drawable.vip_noad));
        hashMap2.put("title", "永久去除应用内广告");
        hashMap2.put("description", "无广告，真干净");
        hashMap2.put("arrow", Integer.valueOf((int) R.drawable.arraw_right));
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("icon", Integer.valueOf((int) R.drawable.vip_personal));
        hashMap3.put("title", "私人定制炫酷启动画面");
        hashMap3.put("description", "小清新、文艺范、女汉子...");
        hashMap3.put("arrow", Integer.valueOf((int) R.drawable.arraw_right));
        arrayList.add(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("icon", Integer.valueOf((int) R.drawable.vip_service));
        hashMap4.put("title", "客服MM一对一为您服务");
        hashMap4.put("description", "客服QQ：3219210390");
        hashMap4.put("arrow", Integer.valueOf((int) R.drawable.arraw_right));
        arrayList.add(hashMap4);
        return arrayList;
    }

    public void onClick(View view) {
        String str;
        switch (view.getId()) {
            case R.id.back:
                finish();
                return;
            case R.id.open_vip:
                h();
                return;
            case R.id.user_logout:
                if (com.shoujiduoduo.a.b.b.g().h()) {
                    str = "退出登录后将不再享受VIP特权，确定退出登录吗？";
                } else {
                    str = "确定退出当前账号吗？";
                }
                new b.a(this).a(str).a("退出", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        UserCenterActivity.this.g();
                        dialogInterface.dismiss();
                        UserCenterActivity.this.finish();
                    }
                }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                return;
            case R.id.btn_manage_cailing:
                startActivity(new Intent(this, CailingManageActivity.class));
                return;
            default:
                return;
        }
    }

    private class a extends BaseAdapter {
        private a() {
        }

        public int getCount() {
            return UserCenterActivity.this.g.size();
        }

        public Object getItem(int i) {
            return UserCenterActivity.this.g.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(UserCenterActivity.this).inflate((int) R.layout.listitem_vip_rights, viewGroup, false);
            }
            Map map = (Map) UserCenterActivity.this.g.get(i);
            TextView textView = (TextView) view.findViewById(R.id.title);
            Button button = (Button) view.findViewById(R.id.btn_action);
            ((ImageView) view.findViewById(R.id.icon)).setImageResource(((Integer) map.get("icon")).intValue());
            textView.setText((String) map.get("title"));
            ((TextView) view.findViewById(R.id.description)).setText((String) map.get("description"));
            if (i == 0) {
                textView.setTextColor(UserCenterActivity.this.getResources().getColor(R.color.text_orange));
                button.setText("换一首");
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (com.shoujiduoduo.a.b.b.g().h()) {
                            Intent intent = new Intent(RingDDApp.c(), RingListActivity.class);
                            String str = "";
                            switch (g.s()) {
                                case f2309a:
                                    str = "20";
                                    break;
                                case cu:
                                    str = "26";
                                    break;
                                case ct:
                                    str = Constants.VIA_REPORT_TYPE_QQFAVORITES;
                                    break;
                            }
                            intent.putExtra("list_id", str);
                            intent.putExtra("title", "VIP彩铃");
                            UserCenterActivity.this.startActivity(intent);
                            return;
                        }
                        UserCenterActivity.this.h();
                    }
                });
            } else if (i == 1) {
                button.setVisibility(4);
            } else if (i == 2) {
                button.setText("来一张");
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (com.shoujiduoduo.a.b.b.g().h()) {
                            UserCenterActivity.this.startActivity(new Intent(UserCenterActivity.this, ChangeSkinActivity.class));
                            return;
                        }
                        UserCenterActivity.this.h();
                    }
                });
            } else {
                button.setVisibility(4);
            }
            return view;
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.k == null) {
            this.k = new ProgressDialog(this);
            this.k.setMessage("中国移动提示您：首次使用彩铃功能需要发送一条免费短信，该过程自动完成，如弹出警告，请选择允许。");
            this.k.setIndeterminate(false);
            this.k.setCancelable(true);
            this.k.setCanceledOnTouchOutside(false);
            this.k.setButton(-1, "确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            this.k.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.k != null) {
            this.k.dismiss();
            this.k = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(final String str) {
        this.j.post(new Runnable() {
            public void run() {
                if (UserCenterActivity.this.l == null) {
                    ProgressDialog unused = UserCenterActivity.this.l = new ProgressDialog(UserCenterActivity.this);
                    UserCenterActivity.this.l.setMessage(str);
                    UserCenterActivity.this.l.setIndeterminate(false);
                    UserCenterActivity.this.l.setCancelable(true);
                    if (!UserCenterActivity.this.isFinishing()) {
                        UserCenterActivity.this.l.show();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.n);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.m);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.j.post(new Runnable() {
            public void run() {
                if (UserCenterActivity.this.l != null) {
                    UserCenterActivity.this.l.dismiss();
                    ProgressDialog unused = UserCenterActivity.this.l = (ProgressDialog) null;
                }
            }
        });
    }
}
