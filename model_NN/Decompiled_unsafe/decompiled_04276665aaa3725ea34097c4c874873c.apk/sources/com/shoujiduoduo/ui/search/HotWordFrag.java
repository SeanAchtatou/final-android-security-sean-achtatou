package com.shoujiduoduo.ui.search;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.t;
import com.shoujiduoduo.ringtone.R;

public class HotWordFrag extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private GridView f1874a;
    /* access modifiers changed from: private */
    public a b;
    /* access modifiers changed from: private */
    public b c;
    private t d = new t() {
        public void a() {
            HotWordFrag.this.b.notifyDataSetChanged();
        }
    };

    public interface b {
        void a(String str);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.c = (b) activity;
        } catch (ClassCastException e) {
            e.printStackTrace();
            throw new ClassCastException(activity.toString() + "must implements IHotKeyWordListener");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.fragment_search_hotword, viewGroup, false);
        this.f1874a = (GridView) inflate.findViewById(R.id.hotKeywordGrid);
        this.b = new a();
        this.f1874a.setAdapter((ListAdapter) this.b);
        this.f1874a.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                com.shoujiduoduo.b.d.b bVar = com.shoujiduoduo.a.b.b.e().c().get(i);
                if (bVar != null) {
                    HotWordFrag.this.c.a(bVar.f1359a);
                }
            }
        });
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_SEARCH_HOT_WORD, this.d);
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_SEARCH_HOT_WORD, this.d);
    }

    public void onDetach() {
        super.onDetach();
    }

    class a extends BaseAdapter {
        private LayoutInflater b;

        a() {
            this.b = LayoutInflater.from(HotWordFrag.this.getActivity());
        }

        public int getCount() {
            if (com.shoujiduoduo.a.b.b.e().c() != null) {
                return com.shoujiduoduo.a.b.b.e().c().size();
            }
            return 0;
        }

        public Object getItem(int i) {
            if (com.shoujiduoduo.a.b.b.e().c() != null) {
                return com.shoujiduoduo.a.b.b.e().c().get(i);
            }
            return 0;
        }

        public long getItemId(int i) {
            return 0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = this.b.inflate((int) R.layout.hot_keyword_item, viewGroup, false);
            }
            com.shoujiduoduo.b.d.b bVar = com.shoujiduoduo.a.b.b.e().c().get(i);
            ((TextView) view.findViewById(R.id.hot_keyword_ranklist_item_sn)).setText(String.valueOf(i + 1));
            ((TextView) view.findViewById(R.id.hot_keyword_ranklist_item)).setText(bVar.f1359a);
            ImageView imageView = (ImageView) view.findViewById(R.id.trend_icon);
            if (bVar.c > 0) {
                imageView.setImageResource(R.drawable.search_up);
                imageView.setVisibility(0);
            } else if (bVar.c < 0) {
                imageView.setImageResource(R.drawable.search_down);
                imageView.setVisibility(0);
            } else {
                imageView.setVisibility(8);
            }
            ImageView imageView2 = (ImageView) view.findViewById(R.id.new_icon);
            if (bVar.b > 0) {
                imageView2.setVisibility(0);
            } else {
                imageView2.setVisibility(4);
            }
            return view;
        }
    }
}
