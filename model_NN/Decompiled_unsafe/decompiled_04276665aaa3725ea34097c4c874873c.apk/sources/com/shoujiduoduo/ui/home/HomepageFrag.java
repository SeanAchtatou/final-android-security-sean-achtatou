package com.shoujiduoduo.ui.home;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.l;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.b.c.f;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.TopListData;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.HtmlFragment;
import com.shoujiduoduo.ui.utils.j;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.widget.f;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.b.a.a.c;
import net.lucode.hackware.magicindicator.b.a.a.d;

public class HomepageFrag extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ViewPager f1686a;
    /* access modifiers changed from: private */
    public List<Fragment> b = new ArrayList();
    private ArrayList<TopListData> c;
    private RelativeLayout d;
    private RelativeLayout e;
    private RelativeLayout f;
    private boolean g;
    private u h = new u() {
        public void a(int i) {
            if (i == 1) {
                com.shoujiduoduo.base.a.a.a("HomepageFrag", "top list data is ready, init view pager and show data");
                HomepageFrag.this.c();
                HomepageFrag.this.a();
                return;
            }
            com.shoujiduoduo.base.a.a.a("HomepageFrag", "top list data load error, show failed view");
            HomepageFrag.this.b();
        }
    };
    private l i = new l() {
        public void a(String str, String str2) {
            HomepageFrag.this.a(str, str2);
        }
    };
    private net.lucode.hackware.magicindicator.b.a.a.a j = new net.lucode.hackware.magicindicator.b.a.a.a() {
        public int a() {
            if (b.f().c() && HomepageFrag.this.b.size() > 0) {
                return b.f().d().size();
            }
            com.shoujiduoduo.base.a.a.c("HomepageFrag", "getCount() return 0");
            return 0;
        }

        public d a(Context context, final int i) {
            if (!b.f().c() || HomepageFrag.this.b.size() <= 0) {
                return null;
            }
            f fVar = new f(context);
            fVar.setText(b.f().d().get(i).name);
            fVar.setTextSize(17.0f);
            fVar.setMinScale(0.8f);
            fVar.setNormalColor(j.a(R.color.text_black));
            fVar.setSelectedColor(j.a(R.color.text_green));
            fVar.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    HomepageFrag.this.f1686a.setCurrentItem(i);
                }
            });
            return fVar;
        }

        public c a(Context context) {
            return null;
        }
    };
    private View.OnClickListener k = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("HomepageFrag", "retry load top list data");
            HomepageFrag.this.d();
            b.f().e();
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.homepage, viewGroup, false);
        this.f1686a = (ViewPager) inflate.findViewById(R.id.vPager);
        this.f1686a.setOffscreenPageLimit(6);
        this.f1686a.setAdapter(new a(getChildFragmentManager()));
        this.d = (RelativeLayout) inflate.findViewById(R.id.failed_view);
        this.d.setOnClickListener(this.k);
        this.e = (RelativeLayout) inflate.findViewById(R.id.loading_view);
        ((AnimationDrawable) ((ImageView) this.e.findViewById(R.id.loading)).getBackground()).start();
        this.f = (RelativeLayout) inflate.findViewById(R.id.home_lists);
        MagicIndicator magicIndicator = (MagicIndicator) inflate.findViewById(R.id.magic_indicator);
        net.lucode.hackware.magicindicator.b.a.a aVar = new net.lucode.hackware.magicindicator.b.a.a(getContext());
        aVar.setAdapter(this.j);
        magicIndicator.setNavigator(aVar);
        magicIndicator.setBackgroundColor(j.a(R.color.white));
        net.lucode.hackware.magicindicator.d.a(magicIndicator, this.f1686a);
        this.g = com.shoujiduoduo.util.a.f();
        if (b.f().c()) {
            com.shoujiduoduo.base.a.a.a("HomepageFrag", "top list data is  ready");
            c();
            a();
        } else {
            d();
            com.shoujiduoduo.base.a.a.a("HomepageFrag", "top list dat is not ready,just wait");
        }
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_AREA, this.i);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_TOP_LIST, this.h);
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_AREA, this.i);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_TOP_LIST, this.h);
    }

    /* access modifiers changed from: private */
    public void a() {
        com.shoujiduoduo.b.c.j jVar;
        this.c = b.f().d();
        UserInfo c2 = b.g().c();
        if (c2.isLogin() && c2.getFollowNum() > 0) {
            TopListData topListData = new TopListData();
            topListData.type = TopListData.concern_type;
            topListData.name = "关注";
            this.c.add(1, topListData);
            com.shoujiduoduo.base.a.a.a("HomepageFrag", "add concern");
        }
        Iterator<TopListData> it = this.c.iterator();
        while (it.hasNext()) {
            TopListData next = it.next();
            com.shoujiduoduo.base.a.a.a("HomepageFrag", "listname:" + next.name + ", id:" + next.id + ", type:" + next.type);
            if (next.type.equals(TopListData.list_type)) {
                if (next.id == 20) {
                    if (g.t()) {
                        next.name = "彩铃榜";
                        jVar = new com.shoujiduoduo.b.c.j(ListType.LIST_TYPE.list_ring_normal, "20", false, "");
                    } else {
                        next.name = "分享榜";
                        jVar = new com.shoujiduoduo.b.c.j(ListType.LIST_TYPE.list_ring_normal, Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, false, "");
                    }
                } else if (next.id == 24) {
                    String a2 = af.a(getContext(), "user_area", "");
                    if (a2.equals("")) {
                        com.shoujiduoduo.base.a.a.a("HomepageFrag", "全国榜");
                        next.name = "全国榜";
                        jVar = new com.shoujiduoduo.b.c.j(ListType.LIST_TYPE.list_ring_normal, "24", false, "");
                    } else {
                        next.name = a2 + "榜";
                        com.shoujiduoduo.base.a.a.a("HomepageFrag", "用户选择的地域榜:" + a2);
                        jVar = new com.shoujiduoduo.b.c.j(ListType.LIST_TYPE.list_ring_normal, "25", false, a2);
                    }
                } else {
                    jVar = new com.shoujiduoduo.b.c.j(ListType.LIST_TYPE.list_ring_normal, "" + next.id, false, "");
                }
                DDListFragment dDListFragment = new DDListFragment();
                Bundle bundle = new Bundle();
                if (next.id == 24 || next.id == 25) {
                    bundle.putBoolean("support_area", true);
                }
                if (this.g) {
                    bundle.putBoolean("support_feed_ad", true);
                }
                bundle.putBoolean("support_lazy_load", true);
                bundle.putString("adapter_type", "ring_list_adapter");
                dDListFragment.setArguments(bundle);
                dDListFragment.a(jVar);
                this.b.add(dDListFragment);
            } else if (next.type.equals(TopListData.concern_type)) {
                DDListFragment dDListFragment2 = new DDListFragment();
                com.shoujiduoduo.b.c.f fVar = new com.shoujiduoduo.b.c.f(b.g().f(), f.b.concern_work);
                Bundle bundle2 = new Bundle();
                if (this.g) {
                    bundle2.putBoolean("support_feed_ad", true);
                }
                bundle2.putString("adapter_type", "ring_list_adapter");
                bundle2.putBoolean("support_lazy_load", true);
                bundle2.putBoolean("support_pull_refresh", true);
                dDListFragment2.setArguments(bundle2);
                dDListFragment2.a(fVar);
                this.b.add(dDListFragment2);
            } else if (next.type.equals(TopListData.collect_type)) {
                DDListFragment dDListFragment3 = new DDListFragment();
                com.shoujiduoduo.b.c.c cVar = new com.shoujiduoduo.b.c.c("collect");
                Bundle bundle3 = new Bundle();
                bundle3.putString("adapter_type", "collect_list_adapter");
                bundle3.putBoolean("support_lazy_load", true);
                dDListFragment3.setArguments(bundle3);
                dDListFragment3.a(cVar);
                this.b.add(dDListFragment3);
            } else if (next.type.equals(TopListData.artist_type)) {
                DDListFragment dDListFragment4 = new DDListFragment();
                Bundle bundle4 = new Bundle();
                bundle4.putString("adapter_type", "artist_list_adapter");
                bundle4.putBoolean("support_lazy_load", true);
                dDListFragment4.setArguments(bundle4);
                dDListFragment4.a(new com.shoujiduoduo.b.c.a("artist"));
                this.b.add(dDListFragment4);
            } else if (next.type.equals(TopListData.html_type)) {
                HtmlFragment htmlFragment = new HtmlFragment();
                Bundle bundle5 = new Bundle();
                bundle5.putString("url", next.url);
                htmlFragment.setArguments(bundle5);
                this.b.add(htmlFragment);
            } else {
                com.shoujiduoduo.base.a.a.e("HomepageFrag", "不支持的列表类型，跳过吧。");
            }
        }
        com.shoujiduoduo.base.a.a.a("HomepageFrag", "fragment size:" + this.b.size());
        this.f1686a.getAdapter().notifyDataSetChanged();
        this.j.b();
        this.f1686a.setCurrentItem(0);
    }

    /* access modifiers changed from: private */
    public void b() {
        this.d.setVisibility(0);
        this.e.setVisibility(4);
        this.f.setVisibility(4);
    }

    /* access modifiers changed from: private */
    public void c() {
        this.d.setVisibility(4);
        this.e.setVisibility(4);
        this.f.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void d() {
        this.d.setVisibility(4);
        this.e.setVisibility(0);
        this.f.setVisibility(4);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x004c A[EDGE_INSN: B:20:0x004c->B:8:0x004c ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r7, java.lang.String r8) {
        /*
            r6 = this;
            r3 = 0
            com.shoujiduoduo.b.c.g r0 = com.shoujiduoduo.a.b.b.f()
            java.util.ArrayList r0 = r0.d()
            r6.c = r0
            android.content.Context r0 = r6.getContext()
            java.lang.String r1 = "user_area"
            com.shoujiduoduo.util.af.c(r0, r1, r7)
            java.util.ArrayList<com.shoujiduoduo.base.bean.TopListData> r0 = r6.c
            java.util.Iterator r1 = r0.iterator()
        L_0x001a:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x004c
            java.lang.Object r0 = r1.next()
            com.shoujiduoduo.base.bean.TopListData r0 = (com.shoujiduoduo.base.bean.TopListData) r0
            int r2 = r0.id
            r4 = 24
            if (r2 == r4) goto L_0x0032
            int r2 = r0.id
            r4 = 25
            if (r2 != r4) goto L_0x001a
        L_0x0032:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r2 = "榜"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.name = r1
            net.lucode.hackware.magicindicator.b.a.a.a r0 = r6.j
            r0.b()
        L_0x004c:
            r2 = r3
        L_0x004d:
            java.util.List<android.support.v4.app.Fragment> r0 = r6.b
            int r0 = r0.size()
            if (r2 >= r0) goto L_0x0085
            java.util.List<android.support.v4.app.Fragment> r0 = r6.b
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.app.Fragment r0 = (android.support.v4.app.Fragment) r0
            boolean r1 = r0 instanceof com.shoujiduoduo.ui.utils.DDListFragment
            if (r1 == 0) goto L_0x0081
            r1 = r0
            com.shoujiduoduo.ui.utils.DDListFragment r1 = (com.shoujiduoduo.ui.utils.DDListFragment) r1
            java.lang.String r1 = r1.b()
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x0081
            com.shoujiduoduo.b.c.j r1 = new com.shoujiduoduo.b.c.j
            com.shoujiduoduo.base.bean.ListType$LIST_TYPE r4 = com.shoujiduoduo.base.bean.ListType.LIST_TYPE.list_ring_normal
            java.lang.String r5 = "25"
            r1.<init>(r4, r5, r3, r7)
            com.shoujiduoduo.ui.utils.DDListFragment r0 = (com.shoujiduoduo.ui.utils.DDListFragment) r0
            r0.a(r1)
            net.lucode.hackware.magicindicator.b.a.a.a r0 = r6.j
            r0.b()
        L_0x0081:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x004d
        L_0x0085:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.home.HomepageFrag.a(java.lang.String, java.lang.String):void");
    }

    private class a extends FragmentPagerAdapter {
        public CharSequence getPageTitle(int i) {
            if (b.f().c()) {
                return b.f().d().get(i).name;
            }
            return "";
        }

        a(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int i) {
            if (b.f().c() && HomepageFrag.this.b.size() > 0) {
                return (Fragment) HomepageFrag.this.b.get(i % HomepageFrag.this.b.size());
            }
            com.shoujiduoduo.base.a.a.c("HomepageFrag", "return null fragment 2");
            return null;
        }

        public int getCount() {
            if (b.f().c() && HomepageFrag.this.b.size() > 0) {
                return b.f().d().size();
            }
            com.shoujiduoduo.base.a.a.c("HomepageFrag", "getCount() return 0");
            return 0;
        }
    }
}
