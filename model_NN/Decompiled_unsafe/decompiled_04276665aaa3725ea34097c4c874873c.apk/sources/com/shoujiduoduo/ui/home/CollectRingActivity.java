package com.shoujiduoduo.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.a.b.d;
import com.jaeger.library.a;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.b.c.j;
import com.shoujiduoduo.base.bean.CollectData;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.h;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.am;
import com.shoujiduoduo.util.s;
import java.text.DecimalFormat;

public class CollectRingActivity extends BaseFragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    private TextView f1667a;
    private DDListFragment b;
    private Button c;
    private Button d;
    /* access modifiers changed from: private */
    public CollectData e;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_collect_ring);
        a.a(this, getResources().getColor(R.color.bkg_green), 0);
        this.c = (Button) findViewById(R.id.fav_btn);
        this.d = (Button) findViewById(R.id.share_btn);
        this.f1667a = (TextView) findViewById(R.id.header_title);
        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CollectRingActivity.this.finish();
            }
        });
        Intent intent = getIntent();
        if (intent != null) {
            this.e = (CollectData) RingDDApp.b().a(intent.getStringExtra("parakey"));
            if (this.e != null) {
                this.f1667a.setText(this.e.title);
                a();
                this.b.a(new j(ListType.LIST_TYPE.list_ring_collect, this.e.id, false, ""));
                b();
                return;
            }
            com.shoujiduoduo.base.a.a.c("CollectListActivity", "wrong collect data prarm");
            return;
        }
        com.shoujiduoduo.base.a.a.c("CollectListActivity", "wrong intent null");
    }

    private void a() {
        this.b = new DDListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("adapter_type", "ring_list_adapter");
        this.b.setArguments(bundle);
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.list_layout, this.b);
        beginTransaction.commitAllowingStateLoss();
    }

    private void b() {
        TextView textView = (TextView) findViewById(R.id.fav_num);
        d.a().a(this.e.pic, (ImageView) findViewById(R.id.pic), h.a().h());
        ((TextView) findViewById(R.id.content)).setText(this.e.content);
        int a2 = s.a(this.e.favNum, 1000);
        StringBuilder sb = new StringBuilder();
        if (a2 > 10000) {
            sb.append(new DecimalFormat("#.00").format((double) (((float) a2) / 10000.0f)));
            sb.append("万");
        } else {
            sb.append(a2);
        }
        textView.setText(sb.toString());
        this.c.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                b.b().a(CollectRingActivity.this.e);
                com.shoujiduoduo.util.widget.d.a("添加收藏成功", 0);
                am.a(CollectRingActivity.this.e.id, 12, "&from=collect&listType=" + ListType.LIST_TYPE.list_ring_collect);
            }
        });
        this.d.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        PlayerService b2 = ab.a().b();
        if (b2 != null && b2.l()) {
            b2.m();
        }
        super.onDestroy();
    }
}
