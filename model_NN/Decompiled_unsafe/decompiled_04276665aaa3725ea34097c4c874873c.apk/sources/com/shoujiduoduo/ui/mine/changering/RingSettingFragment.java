package com.shoujiduoduo.ui.mine.changering;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.e;
import com.shoujiduoduo.ui.cailing.f;
import com.shoujiduoduo.ui.utils.m;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.aj;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.widget.CircleProgressBar;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Timer;

public class RingSettingFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ListView f1829a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public ArrayList<e> e;
    /* access modifiers changed from: private */
    public c f;
    /* access modifiers changed from: private */
    public g g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public boolean j;
    private Timer k;
    /* access modifiers changed from: private */
    public b l;
    /* access modifiers changed from: private */
    public a m;
    private p n = new p() {
        public void a(int i, RingData ringData) {
            if (RingSettingFragment.this.j) {
                RingSettingFragment.this.g.sendMessage(RingSettingFragment.this.g.obtainMessage(10, Integer.valueOf(i)));
            }
        }
    };
    private o o = new o() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.changering.RingSettingFragment.b(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.changering.RingSettingFragment, int]
         candidates:
          com.shoujiduoduo.ui.mine.changering.RingSettingFragment.b(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, int):int
          com.shoujiduoduo.ui.mine.changering.RingSettingFragment.b(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, java.lang.String):void
          com.shoujiduoduo.ui.mine.changering.RingSettingFragment.b(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, boolean):boolean */
        public void a(String str, int i) {
            if (RingSettingFragment.this.j) {
                boolean unused = RingSettingFragment.this.i = true;
                RingSettingFragment.this.f.notifyDataSetChanged();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.changering.RingSettingFragment.b(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.changering.RingSettingFragment, int]
         candidates:
          com.shoujiduoduo.ui.mine.changering.RingSettingFragment.b(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, int):int
          com.shoujiduoduo.ui.mine.changering.RingSettingFragment.b(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, java.lang.String):void
          com.shoujiduoduo.ui.mine.changering.RingSettingFragment.b(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, boolean):boolean */
        public void b(String str, int i) {
            if (RingSettingFragment.this.j) {
                boolean unused = RingSettingFragment.this.i = false;
                RingSettingFragment.this.f.notifyDataSetChanged();
            }
        }

        public void a(String str, int i, int i2) {
            if (RingSettingFragment.this.j) {
                RingSettingFragment.this.f.notifyDataSetChanged();
            }
        }
    };
    private com.shoujiduoduo.util.b.b p = new com.shoujiduoduo.util.b.b() {
        public void a(c.b bVar) {
            super.a(bVar);
            if (bVar != null && (bVar instanceof c.z)) {
                c.z zVar = (c.z) bVar;
                if (zVar.a() == null || zVar.d() == null || zVar.d().size() <= 0) {
                    com.shoujiduoduo.base.a.a.c("RingSettingFragment", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
                    RingSettingFragment.this.e();
                    return;
                }
                String unused = RingSettingFragment.this.d = zVar.d().get(0).b();
                String unused2 = RingSettingFragment.this.b = zVar.d().get(0).c();
                String unused3 = RingSettingFragment.this.c = zVar.d().get(0).d();
                b unused4 = RingSettingFragment.this.l = b.success;
                RingData ringData = new RingData();
                ringData.cid = RingSettingFragment.this.d;
                ringData.hasmedia = 0;
                ringData.name = RingSettingFragment.this.b + "-" + RingSettingFragment.this.c;
                ringData.duration = 48;
                if (RingSettingFragment.this.e.size() > 3) {
                    ((e) RingSettingFragment.this.e.get(3)).c = 48000;
                    ((e) RingSettingFragment.this.e.get(3)).b = RingSettingFragment.this.b + "-" + RingSettingFragment.this.c;
                    ((e) RingSettingFragment.this.e.get(3)).f1854a = ringData;
                }
                RingSettingFragment.this.f.notifyDataSetChanged();
            }
        }

        public void b(c.b bVar) {
            super.b(bVar);
            com.shoujiduoduo.base.a.a.c("RingSettingFragment", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
            RingSettingFragment.this.e();
        }
    };
    private com.shoujiduoduo.util.b.b q = new com.shoujiduoduo.util.b.b() {
        public void a(c.b bVar) {
            super.a(bVar);
            if (bVar != null && (bVar instanceof c.z)) {
                c.z zVar = (c.z) bVar;
                if (zVar.a() == null || zVar.d() == null || zVar.d().size() <= 0) {
                    com.shoujiduoduo.base.a.a.c("RingSettingFragment", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
                    RingSettingFragment.this.e();
                    return;
                }
                String unused = RingSettingFragment.this.d = zVar.d().get(0).b();
                com.shoujiduoduo.util.d.b.a().e(com.shoujiduoduo.a.b.b.g().c().getPhoneNum(), new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        boolean z;
                        super.a(bVar);
                        if (bVar instanceof c.z) {
                            Iterator<c.ae> it = ((c.z) bVar).d().iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    z = false;
                                    break;
                                }
                                c.ae next = it.next();
                                if (RingSettingFragment.this.d.equals(next.b())) {
                                    String unused = RingSettingFragment.this.c = next.d();
                                    String unused2 = RingSettingFragment.this.b = next.c();
                                    z = true;
                                    break;
                                }
                            }
                            if (z) {
                                b unused3 = RingSettingFragment.this.l = b.success;
                                RingData ringData = new RingData();
                                ringData.ctcid = RingSettingFragment.this.d;
                                ringData.cthasmedia = 0;
                                ringData.name = RingSettingFragment.this.b + "-" + RingSettingFragment.this.c;
                                ringData.duration = 48;
                                if (RingSettingFragment.this.e.size() > 3) {
                                    ((e) RingSettingFragment.this.e.get(3)).c = 48000;
                                    ((e) RingSettingFragment.this.e.get(3)).b = RingSettingFragment.this.b + "-" + RingSettingFragment.this.c;
                                    ((e) RingSettingFragment.this.e.get(3)).f1854a = ringData;
                                }
                                RingSettingFragment.this.f.notifyDataSetChanged();
                            }
                        }
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        com.shoujiduoduo.base.a.a.c("RingSettingFragment", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
                        RingSettingFragment.this.e();
                    }
                });
            }
        }

        public void b(c.b bVar) {
            super.b(bVar);
            com.shoujiduoduo.base.a.a.c("RingSettingFragment", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
            RingSettingFragment.this.e();
        }
    };
    private com.shoujiduoduo.util.b.b r = new com.shoujiduoduo.util.b.b() {
        public void a(c.b bVar) {
            super.a(bVar);
            if (bVar != null && (bVar instanceof c.s)) {
                c.s sVar = (c.s) bVar;
                if (sVar.d != null) {
                    int i = 0;
                    while (true) {
                        if (i >= sVar.d.length) {
                            break;
                        } else if (sVar.d[i].c.equals("0")) {
                            String unused = RingSettingFragment.this.d = sVar.d[i].d;
                            com.shoujiduoduo.base.a.a.a("RingSettingFragment", "default cucc cailing id:" + RingSettingFragment.this.d);
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                if (!ai.c(RingSettingFragment.this.d)) {
                    com.shoujiduoduo.util.e.a.a().i(new com.shoujiduoduo.util.b.b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            if (bVar instanceof c.u) {
                                c.ab[] abVarArr = ((c.u) bVar).d;
                                if (abVarArr == null || abVarArr.length == 0) {
                                    com.shoujiduoduo.base.a.a.a("RingSettingFragment", "userToneInfo == null");
                                    return;
                                }
                                for (int i = 0; i < abVarArr.length; i++) {
                                    if (RingSettingFragment.this.d.equals(abVarArr[i].f2222a)) {
                                        String unused = RingSettingFragment.this.b = abVarArr[i].b;
                                        String unused2 = RingSettingFragment.this.c = abVarArr[i].d;
                                        b unused3 = RingSettingFragment.this.l = b.success;
                                        RingData ringData = new RingData();
                                        ringData.cucid = RingSettingFragment.this.d;
                                        ringData.name = RingSettingFragment.this.b + "-" + RingSettingFragment.this.c;
                                        ringData.duration = 48;
                                        if (RingSettingFragment.this.e.size() > 3) {
                                            ((e) RingSettingFragment.this.e.get(3)).c = 48000;
                                            ((e) RingSettingFragment.this.e.get(3)).b = RingSettingFragment.this.b + "-" + RingSettingFragment.this.c;
                                            ((e) RingSettingFragment.this.e.get(3)).f1854a = ringData;
                                        }
                                        RingSettingFragment.this.f.notifyDataSetChanged();
                                        return;
                                    }
                                }
                            }
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            RingSettingFragment.this.e();
                        }
                    });
                } else {
                    RingSettingFragment.this.e();
                }
            }
        }

        public void b(c.b bVar) {
            super.b(bVar);
            RingSettingFragment.this.e();
        }
    };

    public interface a {
        void a(int i);
    }

    private enum b {
        uninit,
        querying,
        success,
        fail
    }

    public enum f {
        ringtone,
        notification,
        alarm,
        cailing
    }

    public void onCreate(Bundle bundle) {
        this.g = new g();
        super.onCreate(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        com.shoujiduoduo.base.a.a.a("RingSettingFragment", "onCreateView");
        this.j = false;
        View inflate = layoutInflater.inflate((int) R.layout.my_ringtone_setting, viewGroup, false);
        this.f1829a = (ListView) inflate.findViewById(R.id.my_ringtone_setting_list);
        this.f = new c();
        i.a(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.mine.changering.RingSettingFragment.a(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, boolean):boolean
             arg types: [com.shoujiduoduo.ui.mine.changering.RingSettingFragment, int]
             candidates:
              com.shoujiduoduo.ui.mine.changering.RingSettingFragment.a(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, com.shoujiduoduo.ui.mine.changering.RingSettingFragment$b):com.shoujiduoduo.ui.mine.changering.RingSettingFragment$b
              com.shoujiduoduo.ui.mine.changering.RingSettingFragment.a(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, int):void
              com.shoujiduoduo.ui.mine.changering.RingSettingFragment.a(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, java.lang.String):void
              com.shoujiduoduo.ui.mine.changering.RingSettingFragment.a(java.lang.String, com.shoujiduoduo.util.g$b):void
              com.shoujiduoduo.ui.mine.changering.RingSettingFragment.a(com.shoujiduoduo.ui.mine.changering.RingSettingFragment, boolean):boolean */
            public void run() {
                if (RingSettingFragment.this.getActivity() != null) {
                    RingSettingFragment.this.c();
                    boolean unused = RingSettingFragment.this.j = true;
                    RingSettingFragment.this.g.sendEmptyMessage(5);
                }
            }
        });
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.n);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.o);
        return inflate;
    }

    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.base.a.a.a("RingSettingFragment", "onDestroy");
    }

    public void onDestroyView() {
        com.shoujiduoduo.base.a.a.a("RingSettingFragment", "onDestroyView");
        super.onDestroyView();
        if (this.g != null) {
            this.g.removeCallbacksAndMessages(null);
        }
        if (this.k != null) {
            this.k.cancel();
        }
        PlayerService b2 = ab.a().b();
        if (b2 != null && b2.l()) {
            b2.m();
        }
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.n);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.o);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.m = (a) activity;
        } catch (ClassCastException e2) {
            throw new ClassCastException(activity.toString() + " must implents ChangRingListener");
        }
    }

    private class d implements View.OnClickListener {
        private int b;

        public d(int i) {
            this.b = i;
        }

        public void onClick(View view) {
            if (this.b == 0 || this.b == 1 || this.b == 2) {
                RingSettingFragment.this.m.a(this.b);
                return;
            }
            switch (com.shoujiduoduo.util.g.s()) {
                case f2309a:
                    if (com.shoujiduoduo.util.c.b.a().c().equals(b.a.success)) {
                        RingSettingFragment.this.m.a(3);
                        return;
                    }
                    switch (RingSettingFragment.this.l) {
                        case uninit:
                            RingSettingFragment.this.b();
                            return;
                        case fail:
                            RingSettingFragment.this.a();
                            return;
                        case querying:
                        default:
                            return;
                        case success:
                            RingSettingFragment.this.m.a(3);
                            return;
                    }
                case cu:
                    switch (RingSettingFragment.this.l) {
                        case uninit:
                        case fail:
                            String phoneNum = com.shoujiduoduo.a.b.b.g().c().getPhoneNum();
                            if (TextUtils.isEmpty(phoneNum) || !com.shoujiduoduo.util.e.a.a().a(phoneNum)) {
                                new com.shoujiduoduo.ui.cailing.e(RingSettingFragment.this.getActivity(), R.style.DuoDuoDialog, "", g.b.cu, new e.a() {
                                    public void a(String str) {
                                        RingSettingFragment.this.a(str);
                                    }
                                }).show();
                                return;
                            } else {
                                RingSettingFragment.this.a(phoneNum);
                                return;
                            }
                        case querying:
                        default:
                            return;
                        case success:
                            RingSettingFragment.this.m.a(3);
                            return;
                    }
                case ct:
                    switch (RingSettingFragment.this.l) {
                        case uninit:
                        case fail:
                            String phoneNum2 = com.shoujiduoduo.a.b.b.g().c().getPhoneNum();
                            if (!TextUtils.isEmpty(phoneNum2)) {
                                RingSettingFragment.this.b(phoneNum2);
                                return;
                            } else {
                                new com.shoujiduoduo.ui.cailing.e(RingSettingFragment.this.getActivity(), R.style.DuoDuoDialog, "", g.b.ct, new e.a() {
                                    public void a(String str) {
                                        RingSettingFragment.this.b(str);
                                    }
                                }).show();
                                return;
                            }
                        case querying:
                        default:
                            return;
                        case success:
                            RingSettingFragment.this.m.a(3);
                            return;
                    }
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        com.shoujiduoduo.ui.utils.i.a(getActivity());
        com.shoujiduoduo.util.c.b.a().i("", new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                com.shoujiduoduo.ui.utils.i.a();
                b unused = RingSettingFragment.this.l = b.success;
                RingSettingFragment.this.d();
            }

            public void b(c.b bVar) {
                super.b(bVar);
                com.shoujiduoduo.ui.utils.i.a();
                b unused = RingSettingFragment.this.l = b.fail;
                new com.shoujiduoduo.ui.cailing.f(RingSettingFragment.this.getActivity(), R.style.DuoDuoDialog, g.b.ct, new f.a() {
                    public void a(f.a.C0045a aVar) {
                        if (aVar == f.a.C0045a.open) {
                            b unused = RingSettingFragment.this.l = b.success;
                            RingSettingFragment.this.d();
                        }
                    }
                }).show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        com.shoujiduoduo.ui.utils.i.a(getActivity());
        com.shoujiduoduo.util.e.a.a().e(new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                b unused = RingSettingFragment.this.l = b.success;
                RingSettingFragment.this.d();
            }

            public void b(c.b bVar) {
                super.b(bVar);
                com.shoujiduoduo.ui.utils.i.a();
                b unused = RingSettingFragment.this.l = b.fail;
                new com.shoujiduoduo.ui.cailing.f(RingSettingFragment.this.getActivity(), R.style.DuoDuoDialog, g.b.cu, new f.a() {
                    public void a(f.a.C0045a aVar) {
                        if (aVar == f.a.C0045a.open) {
                            b unused = RingSettingFragment.this.l = b.success;
                            RingSettingFragment.this.d();
                        }
                    }
                }).show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(final String str) {
        com.shoujiduoduo.ui.utils.i.a(getActivity());
        com.shoujiduoduo.util.d.b.a().g(str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                com.shoujiduoduo.ui.utils.i.a();
                b unused = RingSettingFragment.this.l = b.success;
                RingSettingFragment.this.d();
            }

            public void b(c.b bVar) {
                super.b(bVar);
                com.shoujiduoduo.ui.utils.i.a();
                b unused = RingSettingFragment.this.l = b.fail;
                if (com.shoujiduoduo.util.d.b.a().a(str).equals(b.d.wait_open)) {
                    com.shoujiduoduo.util.widget.d.a("正在为您开通业务，请耐心等待一会儿.");
                } else {
                    new com.shoujiduoduo.ui.cailing.f(RingSettingFragment.this.getActivity(), R.style.DuoDuoDialog, g.b.ct, new f.a() {
                        public void a(f.a.C0045a aVar) {
                            if (aVar == f.a.C0045a.open) {
                                b unused = RingSettingFragment.this.l = b.success;
                                RingSettingFragment.this.d();
                            }
                        }
                    }).show();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void b() {
        com.shoujiduoduo.base.a.a.a("RingSettingFragment", "准备初始化移动sdk");
        com.shoujiduoduo.ui.utils.i.a(getActivity(), "正在初始化，请稍候...");
        com.shoujiduoduo.ui.utils.i.b(getActivity());
        this.g.sendEmptyMessageDelayed(1, 4000);
    }

    private class g extends Handler {
        private g() {
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 1:
                    com.shoujiduoduo.ui.utils.i.b();
                    com.shoujiduoduo.util.c.b.a().c(new com.shoujiduoduo.util.b.b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            com.shoujiduoduo.ui.utils.i.a();
                            RingSettingFragment.this.m.a(3);
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            com.shoujiduoduo.ui.utils.i.a();
                            RingSettingFragment.this.a("", g.b.f2309a);
                        }
                    });
                    return;
                case 4:
                    RingSettingFragment.this.f.notifyDataSetChanged();
                    return;
                case 5:
                    RingSettingFragment.this.f1829a.setAdapter((ListAdapter) RingSettingFragment.this.f);
                    RingSettingFragment.this.d();
                    return;
                case 10:
                    int intValue = ((Integer) message.obj).intValue();
                    switch (intValue) {
                        case 1:
                        case 2:
                        case 4:
                            RingSettingFragment.this.a(intValue);
                            break;
                        case 16:
                            RingSettingFragment.this.d();
                            break;
                    }
                    RingSettingFragment.this.f.notifyDataSetChanged();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, g.b bVar) {
        if (getActivity() != null && !getActivity().isFinishing()) {
            new com.shoujiduoduo.ui.cailing.e(getActivity(), R.style.DuoDuoDialog, str, bVar, new e.a() {
                public void a(String str) {
                    b unused = RingSettingFragment.this.l = b.success;
                    UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                    if (!c.isLogin()) {
                        c.setUserName(str);
                        c.setUid("phone_" + str);
                    }
                    c.setPhoneNum(str);
                    c.setLoginStatus(1);
                    com.shoujiduoduo.a.b.b.g().a(c);
                    com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                        public void a() {
                            ((v) this.f1284a).a(1, true, "", "");
                        }
                    });
                    RingSettingFragment.this.m.a(3);
                }
            }).show();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        this.e = new ArrayList<>();
        aj.b a2 = new aj(getActivity()).a();
        if (a2.f2195a != null) {
            this.e.add(new e(a2.f2195a.b, a2.f2195a.c, b(a2), f.ringtone));
        }
        if (a2.b != null) {
            this.e.add(new e(a2.b.b, a2.b.c, c(a2), f.notification));
        }
        if (a2.c != null) {
            this.e.add(new e(a2.c.b, a2.c.c, a(a2), f.alarm));
        }
        switch (com.shoujiduoduo.util.g.s()) {
            case f2309a:
                if (!com.shoujiduoduo.util.g.t()) {
                    return;
                }
                if (com.shoujiduoduo.util.c.b.a().c().equals(b.a.success)) {
                    this.l = b.querying;
                    this.e.add(new e(RingDDApp.c().getString(R.string.query_coloring_hint), 0, null, f.cailing));
                    return;
                }
                this.l = b.uninit;
                this.e.add(new e(RingDDApp.c().getString(R.string.set_coloring_hint), 0, null, f.cailing));
                return;
            case cu:
                if (!com.shoujiduoduo.util.g.u()) {
                    return;
                }
                if (!ai.c(com.shoujiduoduo.a.b.b.g().c().getPhoneNum())) {
                    this.l = b.querying;
                    this.e.add(new e(RingDDApp.c().getString(R.string.query_coloring_hint), 0, null, f.cailing));
                    return;
                }
                this.l = b.uninit;
                this.e.add(new e(RingDDApp.c().getString(R.string.set_coloring_hint), 0, null, f.cailing));
                return;
            case ct:
                if (!com.shoujiduoduo.util.g.v()) {
                    return;
                }
                if (!ai.c(com.shoujiduoduo.a.b.b.g().c().getPhoneNum())) {
                    this.l = b.querying;
                    this.e.add(new e(RingDDApp.c().getString(R.string.query_coloring_hint), 0, null, f.cailing));
                    return;
                }
                this.l = b.uninit;
                this.e.add(new e(RingDDApp.c().getString(R.string.set_coloring_hint), 0, null, f.cailing));
                return;
            default:
                return;
        }
    }

    private RingData a(aj.b bVar) {
        RingData ringData = new RingData();
        ringData.localPath = bVar.c.f2194a;
        ringData.name = bVar.c.b;
        ringData.duration = bVar.c.c / 1000;
        return ringData;
    }

    private RingData b(aj.b bVar) {
        RingData ringData = new RingData();
        ringData.localPath = bVar.f2195a.f2194a;
        ringData.name = bVar.f2195a.b;
        ringData.duration = bVar.f2195a.c / 1000;
        return ringData;
    }

    private RingData c(aj.b bVar) {
        RingData ringData = new RingData();
        ringData.localPath = bVar.b.f2194a;
        ringData.name = bVar.b.b;
        ringData.duration = bVar.b.c / 1000;
        return ringData;
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        aj.b a2 = new aj(getActivity()).a();
        switch (i2) {
            case 1:
                this.e.get(0).c = a2.f2195a.c;
                this.e.get(0).b = a2.f2195a.b;
                this.e.get(0).f1854a = b(a2);
                break;
            case 2:
                this.e.get(1).c = a2.b.c;
                this.e.get(1).b = a2.b.b;
                this.e.get(1).f1854a = c(a2);
                break;
            case 4:
                this.e.get(2).c = a2.c.c;
                this.e.get(2).b = a2.c.b;
                this.e.get(2).f1854a = a(a2);
                break;
        }
        this.f.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void d() {
        this.l = b.querying;
        this.f.notifyDataSetChanged();
        switch (com.shoujiduoduo.util.g.s()) {
            case f2309a:
                if (!com.shoujiduoduo.util.g.t()) {
                    return;
                }
                if (com.shoujiduoduo.util.c.b.a().c().equals(b.a.success)) {
                    com.shoujiduoduo.util.c.b.a().h("", this.p);
                    return;
                } else {
                    this.l = b.uninit;
                    return;
                }
            case cu:
                if (com.shoujiduoduo.util.g.u()) {
                    com.shoujiduoduo.util.e.a.a().h(this.r);
                    return;
                }
                return;
            case ct:
                if (com.shoujiduoduo.util.g.v()) {
                    com.shoujiduoduo.util.d.b.a().f(com.shoujiduoduo.a.b.b.g().c().getPhoneNum(), this.q);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        this.l = b.fail;
        if (this.e.size() > 3) {
            this.e.get(3).b = RingDDApp.c().getString(R.string.set_coloring_error);
        }
        this.f.notifyDataSetChanged();
        com.shoujiduoduo.util.widget.d.a("当前彩铃查询失败，请稍后再试");
    }

    private class e {

        /* renamed from: a  reason: collision with root package name */
        public RingData f1854a;
        public String b;
        public int c;
        public f d;

        public e(String str, int i, RingData ringData, f fVar) {
            this.b = str;
            this.c = i;
            this.f1854a = ringData;
            this.d = fVar;
        }
    }

    private class c extends BaseAdapter {
        private c() {
        }

        public int getCount() {
            return RingSettingFragment.this.e.size();
        }

        public Object getItem(int i) {
            return RingSettingFragment.this.e.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(final int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = ((LayoutInflater) RingSettingFragment.this.getActivity().getSystemService("layout_inflater")).inflate((int) R.layout.listitem_current_ring, viewGroup, false);
            }
            TextView textView = (TextView) view.findViewById(R.id.tv_ring_type);
            TextView textView2 = (TextView) view.findViewById(R.id.tv_duradion);
            ImageView imageView = (ImageView) view.findViewById(R.id.iv_duration);
            ((Button) view.findViewById(R.id.btn_change_ring)).setOnClickListener(new d(i));
            ((TextView) view.findViewById(R.id.tv_ring_name)).setText(((e) RingSettingFragment.this.e.get(i)).b);
            if (((e) RingSettingFragment.this.e.get(i)).c == 0) {
                textView2.setVisibility(8);
                imageView.setVisibility(8);
            } else {
                int i2 = ((e) RingSettingFragment.this.e.get(i)).c / 1000;
                StringBuilder append = new StringBuilder().append("时长:");
                if (i2 == 0) {
                    i2 = 1;
                }
                textView2.setText(append.append(i2).append("秒").toString());
            }
            switch (i) {
                case 0:
                    textView.setText("来电铃声");
                    break;
                case 1:
                    textView.setText("短信铃声");
                    break;
                case 2:
                    textView.setText("闹钟铃声");
                    break;
                case 3:
                    textView.setText("当前彩铃");
                    break;
            }
            ImageView imageView2 = (ImageView) view.findViewById(R.id.ringitem_play);
            imageView2.setOnClickListener(new View.OnClickListener() {
                /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
                public void onClick(View view) {
                    int unused = RingSettingFragment.this.h = i;
                    switch (((e) RingSettingFragment.this.e.get(i)).d) {
                        case ringtone:
                        case notification:
                        case alarm:
                            RingData ringData = ((e) RingSettingFragment.this.e.get(i)).f1854a;
                            if (ringData == null) {
                                return;
                            }
                            if (TextUtils.isEmpty(ringData.localPath)) {
                                com.shoujiduoduo.util.widget.d.a((int) R.string.no_ring_hint);
                                return;
                            }
                            break;
                        case cailing:
                            switch (RingSettingFragment.this.l) {
                                case uninit:
                                    com.shoujiduoduo.util.widget.d.a("请先设置一首彩铃吧");
                                    return;
                                case fail:
                                    com.shoujiduoduo.util.widget.d.a("未查询到您的当前彩铃");
                                    return;
                                case querying:
                                    com.shoujiduoduo.util.widget.d.a("正在查询彩铃状态，请稍候...");
                                    return;
                                case success:
                                default:
                                    if (((e) RingSettingFragment.this.e.get(i)).f1854a == null) {
                                        com.shoujiduoduo.base.a.a.c("RingSettingFragment", "cailing data error");
                                        return;
                                    }
                                    break;
                            }
                    }
                    PlayerService b2 = ab.a().b();
                    if (b2 != null) {
                        b2.a(((e) RingSettingFragment.this.e.get(i)).f1854a, "");
                        RingSettingFragment.this.f.notifyDataSetChanged();
                        return;
                    }
                    com.shoujiduoduo.base.a.a.c("RingSettingFragment", "PlayerService is unavailable!");
                }
            });
            ImageView imageView3 = (ImageView) view.findViewById(R.id.ringitem_pause);
            imageView3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    PlayerService b = ab.a().b();
                    if (b != null) {
                        b.j();
                    }
                }
            });
            ImageView imageView4 = (ImageView) view.findViewById(R.id.ringitem_failed);
            imageView4.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    PlayerService b2 = ab.a().b();
                    if (b2 != null) {
                        b2.a(((e) RingSettingFragment.this.e.get(i)).f1854a, "");
                        RingSettingFragment.this.f.notifyDataSetChanged();
                        return;
                    }
                    com.shoujiduoduo.base.a.a.c("RingSettingFragment", "PlayerService is unavailable!");
                }
            });
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.ringitem_download_progress);
            progressBar.setVisibility(4);
            CircleProgressBar circleProgressBar = (CircleProgressBar) m.a(view, R.id.play_progress_bar);
            imageView2.setVisibility(0);
            imageView3.setVisibility(4);
            imageView4.setVisibility(4);
            PlayerService b = ab.a().b();
            if (i != RingSettingFragment.this.h || b == null) {
                imageView2.setVisibility(0);
            } else if (RingSettingFragment.this.i) {
                switch (b.a()) {
                    case 1:
                        progressBar.setVisibility(0);
                        imageView2.setVisibility(4);
                        break;
                    case 2:
                        imageView3.setVisibility(0);
                        imageView2.setVisibility(4);
                        break;
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        imageView2.setVisibility(0);
                        break;
                }
            } else {
                imageView2.setVisibility(0);
            }
            return view;
        }
    }
}
