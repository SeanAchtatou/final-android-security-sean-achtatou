package com.shoujiduoduo.ui.mine;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.m;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.widget.CircleProgressBar;

/* compiled from: FavoriteRingListAdapter */
class b extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f1802a;
    /* access modifiers changed from: private */
    public int b = -1;
    /* access modifiers changed from: private */
    public Activity c;
    /* access modifiers changed from: private */
    public boolean d;
    private o e = new o() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.b.a(com.shoujiduoduo.ui.mine.b, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.b, int]
         candidates:
          com.shoujiduoduo.ui.mine.b.a(com.shoujiduoduo.ui.mine.b, int):int
          com.shoujiduoduo.ui.mine.b.a(android.view.View, int):void
          com.shoujiduoduo.ui.mine.b.a(com.shoujiduoduo.ui.mine.b, boolean):boolean */
        public void a(String str, int i) {
            boolean unused = b.this.d = true;
            int unused2 = b.this.b = i;
            b.this.notifyDataSetChanged();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.b.a(com.shoujiduoduo.ui.mine.b, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.b, int]
         candidates:
          com.shoujiduoduo.ui.mine.b.a(com.shoujiduoduo.ui.mine.b, int):int
          com.shoujiduoduo.ui.mine.b.a(android.view.View, int):void
          com.shoujiduoduo.ui.mine.b.a(com.shoujiduoduo.ui.mine.b, boolean):boolean */
        public void b(String str, int i) {
            boolean unused = b.this.d = false;
            int unused2 = b.this.b = -1;
            b.this.notifyDataSetChanged();
        }

        public void a(String str, int i, int i2) {
            b.this.notifyDataSetChanged();
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener f = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialogInterface, int i) {
            switch (i) {
                case -2:
                default:
                    return;
                case -1:
                    DDList b = com.shoujiduoduo.a.b.b.b().b("favorite_ring_list");
                    PlayerService b2 = ab.a().b();
                    if (b2 != null && b.get(b.this.b) != null) {
                        if (b2.f() == ((RingData) b.get(b.this.b)).getRid()) {
                            b2.e();
                        }
                        int a2 = b.this.b;
                        int unused = b.this.b = -1;
                        com.shoujiduoduo.a.b.b.b().a("favorite_ring_list", a2);
                        b.this.notifyDataSetChanged();
                        return;
                    }
                    return;
            }
        }
    };
    private View.OnClickListener g = new View.OnClickListener() {
        public void onClick(View view) {
            a.a("CollectRingListAdapter", "RingtoneDuoduo: click share button!");
            ak.a().a(b.this.c, (RingData) com.shoujiduoduo.a.b.b.b().b("favorite_ring_list").get(b.this.b), "myring");
        }
    };
    private View.OnClickListener h = new View.OnClickListener() {
        public void onClick(View view) {
            DDList b = com.shoujiduoduo.a.b.b.b().b("favorite_ring_list");
            if (b.this.b >= 0 && b.this.b < b.size()) {
                new AlertDialog.Builder(b.this.c).setTitle((int) R.string.hint).setMessage((int) R.string.delete_ring_confirm).setIcon(17301543).setPositiveButton((int) R.string.ok, b.this.f).setNegativeButton((int) R.string.cancel, b.this.f).show();
            }
        }
    };
    private View.OnClickListener i = new View.OnClickListener() {
        public void onClick(View view) {
            a.a("CollectRingListAdapter", "MyRingtoneScene:clickApplyButton:SetRingTone:getInstance.");
            new com.shoujiduoduo.ui.settings.b(b.this.c, R.style.DuoDuoDialog, (RingData) com.shoujiduoduo.a.b.b.b().b("favorite_ring_list").get(b.this.b), "user_collect_ring", ListType.LIST_TYPE.list_user_favorite.toString()).show();
        }
    };
    private View.OnClickListener j = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = ab.a().b();
            if (b != null) {
                if (b.a() == 3) {
                    b.n();
                } else if (b.i() == 1) {
                    g.c("play from MyRingtoneScene.ListAdapter, currentSong is null!");
                }
            }
        }
    };
    private View.OnClickListener k = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = ab.a().b();
            if (b != null) {
                b.j();
            }
        }
    };
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            if (b.this.b >= 0) {
                PlayerService b = ab.a().b();
                DDList b2 = com.shoujiduoduo.a.b.b.b().b("favorite_ring_list");
                if (b != null) {
                    b.a(b2, b.this.b);
                }
            }
        }
    };

    public b(Activity activity) {
        this.c = activity;
        this.f1802a = LayoutInflater.from(this.c);
    }

    public void a() {
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.e);
    }

    public void b() {
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.e);
    }

    public void a(int i2) {
        this.b = i2;
    }

    private void a(View view, int i2) {
        RingData ringData = (RingData) com.shoujiduoduo.a.b.b.b().b("favorite_ring_list").get(i2);
        TextView textView = (TextView) m.a(view, R.id.tv_duradion);
        ((ImageView) m.a(view, R.id.iv_play_times)).setVisibility(8);
        ((TextView) m.a(view, R.id.item_song_name)).setText(ringData.name);
        ((TextView) m.a(view, R.id.item_artist)).setText(ringData.artist);
        textView.setText(String.format("%02d:%02d", Integer.valueOf(ringData.duration / 60), Integer.valueOf(ringData.duration % 60)));
        if (ringData.duration == 0) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
    }

    public int getCount() {
        DDList b2 = com.shoujiduoduo.a.b.b.b().b("favorite_ring_list");
        if (b2 != null) {
            return b2.size();
        }
        return 0;
    }

    public Object getItem(int i2) {
        DDList b2 = com.shoujiduoduo.a.b.b.b().b("favorite_ring_list");
        if (i2 < b2.size()) {
            return b2.get(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        DDList b2 = com.shoujiduoduo.a.b.b.b().b("favorite_ring_list");
        if (b2 != null && i2 < b2.size()) {
            if (view == null) {
                view = this.f1802a.inflate((int) R.layout.listitem_ring, viewGroup, false);
            }
            a(view, i2);
            ProgressBar progressBar = (ProgressBar) m.a(view, R.id.ringitem_download_progress);
            CircleProgressBar circleProgressBar = (CircleProgressBar) m.a(view, R.id.play_progress_bar);
            TextView textView = (TextView) m.a(view, R.id.ringitem_serial_number);
            ImageButton imageButton = (ImageButton) m.a(view, R.id.ringitem_play);
            ImageButton imageButton2 = (ImageButton) m.a(view, R.id.ringitem_pause);
            ImageButton imageButton3 = (ImageButton) m.a(view, R.id.ringitem_failed);
            imageButton.setOnClickListener(this.j);
            imageButton2.setOnClickListener(this.k);
            imageButton3.setOnClickListener(this.l);
            String str = "";
            PlayerService b3 = ab.a().b();
            if (b3 != null) {
                str = b3.b();
                this.b = b3.c();
            }
            if (i2 == this.b && str.equals(b2.getListId())) {
                RingData ringData = (RingData) b2.get(i2);
                Button button = (Button) m.a(view, R.id.ring_item_button0);
                Button button2 = (Button) m.a(view, R.id.ring_item_button1);
                Button button3 = (Button) m.a(view, R.id.ring_item_button2);
                if (ringData.cid.equals("") || (!ringData.cid.equals("") && ringData.hasmedia != 0)) {
                    button.setVisibility(0);
                } else {
                    button.setVisibility(8);
                }
                button.setOnClickListener(this.g);
                button2.setText((int) R.string.delete);
                button2.setVisibility(0);
                button2.setOnClickListener(this.h);
                button3.setVisibility(0);
                button3.setOnClickListener(this.i);
                textView.setVisibility(4);
                progressBar.setVisibility(4);
                imageButton.setVisibility(4);
                imageButton2.setVisibility(4);
                imageButton3.setVisibility(4);
                PlayerService b4 = ab.a().b();
                if (this.d && b4 != null && b4.f() == ((RingData) b2.get(i2)).getRid()) {
                    switch (b4.a()) {
                        case 1:
                            progressBar.setVisibility(0);
                            circleProgressBar.setVisibility(4);
                            break;
                        case 2:
                            imageButton2.setVisibility(0);
                            circleProgressBar.setVisibility(0);
                            break;
                        case 3:
                        case 4:
                        case 5:
                            imageButton.setVisibility(0);
                            circleProgressBar.setVisibility(0);
                            break;
                        case 6:
                            imageButton3.setVisibility(0);
                            circleProgressBar.setVisibility(4);
                            break;
                    }
                } else {
                    imageButton.setVisibility(0);
                }
            } else {
                ((Button) m.a(view, R.id.ring_item_button0)).setVisibility(8);
                ((Button) m.a(view, R.id.ring_item_button1)).setVisibility(8);
                ((Button) m.a(view, R.id.ring_item_button2)).setVisibility(8);
                textView.setText(Integer.toString(i2 + 1));
                circleProgressBar.setVisibility(4);
                textView.setVisibility(0);
                progressBar.setVisibility(4);
                imageButton.setVisibility(4);
                imageButton2.setVisibility(4);
                imageButton3.setVisibility(4);
            }
        }
        return view;
    }
}
