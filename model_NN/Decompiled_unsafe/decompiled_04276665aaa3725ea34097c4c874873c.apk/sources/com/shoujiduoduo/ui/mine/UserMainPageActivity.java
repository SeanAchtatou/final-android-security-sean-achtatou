package com.shoujiduoduo.ui.mine;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.a.a.e;
import com.a.a.p;
import com.d.a.b.d;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.b.c.j;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.HttpJsonRes;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.user.UserInfoEditActivity;
import com.shoujiduoduo.ui.user.UserLoginActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.SlidingActivity;
import com.shoujiduoduo.ui.utils.h;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.u;

public class UserMainPageActivity extends SlidingActivity implements View.OnClickListener {
    private AbsListView.OnScrollListener A = new AbsListView.OnScrollListener() {
        public void onScrollStateChanged(AbsListView absListView, int i) {
        }

        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            if (!UserMainPageActivity.this.q.getGlobalVisibleRect(UserMainPageActivity.this.z) || i > 0) {
                UserMainPageActivity.this.w.setBackgroundColor(Color.parseColor("#a0000000"));
                UserMainPageActivity.this.p.setVisibility(0);
                if (!UserMainPageActivity.this.j) {
                    UserMainPageActivity.this.o.setVisibility(0);
                    return;
                }
                return;
            }
            UserMainPageActivity.this.w.setBackgroundColor(0);
            UserMainPageActivity.this.p.setVisibility(8);
            UserMainPageActivity.this.o.setVisibility(8);
        }
    };
    /* access modifiers changed from: private */
    public boolean B;

    /* renamed from: a  reason: collision with root package name */
    private TextView f1793a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private ImageView f;
    private ImageView g;
    private ImageView h;
    /* access modifiers changed from: private */
    public UserData i;
    /* access modifiers changed from: private */
    public boolean j;
    /* access modifiers changed from: private */
    public String k;
    private int l;
    private int m;
    /* access modifiers changed from: private */
    public Button n;
    /* access modifiers changed from: private */
    public Button o;
    /* access modifiers changed from: private */
    public TextView p;
    /* access modifiers changed from: private */
    public View q;
    private final String r = "关注TA";
    private final String s = "取消关注";
    private final String t = "已关注";
    private final String u = "编辑资料";
    private RelativeLayout v;
    /* access modifiers changed from: private */
    public RelativeLayout w;
    private int x;
    private int y;
    /* access modifiers changed from: private */
    public Rect z = new Rect();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_user_mainpage);
        this.w = (RelativeLayout) findViewById(R.id.topbanner);
        findViewById(R.id.back).setOnClickListener(this);
        this.p = (TextView) findViewById(R.id.tv_top_username);
        this.o = (Button) findViewById(R.id.btn_top_follow);
        this.o.setOnClickListener(this);
        View c2 = c();
        DDListFragment dDListFragment = new DDListFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putString("adapter_type", "ring_list_adapter");
        bundle2.putString("userlist_tuid", this.k);
        dDListFragment.setArguments(bundle2);
        dDListFragment.a(c2);
        dDListFragment.a(this.A);
        j jVar = new j(ListType.LIST_TYPE.list_ring_user_upload, this.k, !b.g().f().equals(this.k));
        jVar.a(600000L);
        dDListFragment.a(jVar);
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.list_frag_layout, dDListFragment);
        beginTransaction.commit();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View c() {
        View inflate = LayoutInflater.from(this).inflate((int) R.layout.listitem_mainpage_userinfo, (ViewGroup) null, false);
        inflate.findViewById(R.id.tv_fans_hint).setOnClickListener(this);
        inflate.findViewById(R.id.tv_follow_hint).setOnClickListener(this);
        inflate.findViewById(R.id.fans_layout).setOnClickListener(this);
        inflate.findViewById(R.id.follow_layout).setOnClickListener(this);
        this.q = inflate.findViewById(R.id.scroll_hint);
        Button button = (Button) inflate.findViewById(R.id.btn_destroy_user);
        button.setOnClickListener(this);
        if (b.g().c().isSuperUser()) {
            button.setVisibility(0);
        } else {
            button.setVisibility(8);
        }
        this.f1793a = (TextView) inflate.findViewById(R.id.user_fans);
        this.f1793a.setOnClickListener(this);
        this.b = (TextView) inflate.findViewById(R.id.user_follow);
        this.b.setOnClickListener(this);
        this.c = (TextView) inflate.findViewById(R.id.user_name);
        this.d = (TextView) inflate.findViewById(R.id.user_id);
        this.g = (ImageView) inflate.findViewById(R.id.user_head);
        this.e = (TextView) inflate.findViewById(R.id.tv_user_intro);
        this.n = (Button) inflate.findViewById(R.id.btn_follow);
        this.n.setOnClickListener(this);
        this.h = (ImageView) inflate.findViewById(R.id.iv_bkg);
        this.f = (ImageView) inflate.findViewById(R.id.iv_sex);
        this.v = (RelativeLayout) inflate.findViewById(R.id.user_info_layout);
        Intent intent = getIntent();
        if (intent != null) {
            this.k = intent.getStringExtra("tuid");
            String f2 = b.g().f();
            if (!ai.c(f2) && f2.equals(this.k)) {
                this.j = true;
            }
            a(this.k, this.j);
            if (this.j) {
                this.n.setBackgroundResource(R.drawable.btn_bkg_yellow);
                this.n.setText("编辑资料");
            } else {
                this.n.setBackgroundResource(R.drawable.btn_bkg_wine_red);
                this.n.setText("关注TA");
            }
            this.l = intent.getIntExtra("fansNum", 0);
            this.m = intent.getIntExtra("followNum", 0);
            this.f1793a.setText("" + this.l);
            this.b.setText("" + this.m);
        }
        return inflate;
    }

    public void a() {
    }

    public void b() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        a.a("UserMainPageActivity", "onDestroy");
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2) {
            this.x = this.w.getHeight();
            this.y = this.v.getHeight();
        }
    }

    private void a(String str, boolean z2) {
        UserInfo c2 = b.g().c();
        StringBuilder sb = new StringBuilder();
        sb.append("&uid=").append(c2.getUid()).append("&tuid=").append(str);
        if (z2) {
            sb.append("&username=").append(u.i(c2.getUserName()));
            sb.append("&headurl=").append(u.i(c2.getHeadPic()));
        }
        u.a("getuserinfo", sb.toString(), new u.a() {
            public void a(String str) {
                a.a("UserMainPageActivity", "userinfo:" + str);
                UserData b = com.shoujiduoduo.util.j.b(str);
                if (b != null) {
                    UserData unused = UserMainPageActivity.this.i = b;
                    UserMainPageActivity.this.a(b);
                    return;
                }
                a.a("UserMainPageActivity", "user 解析失败");
            }

            public void a(String str, String str2) {
                a.a("UserMainPageActivity", "user 信息获取失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(UserData userData) {
        this.m = userData.followingNum;
        this.l = userData.followerNum;
        a.a("UserMainPageActivity", "fansNum:" + this.l + ", followNum:" + this.m);
        this.c.setText(userData.userName);
        this.p.setText(this.i.userName);
        if (userData.followerNum >= 0) {
            this.f1793a.setText("" + userData.followerNum);
        }
        if (userData.followingNum >= 0) {
            this.b.setText("" + userData.followingNum);
        }
        if (!ai.c(userData.headUrl)) {
            d.a().a(userData.headUrl, this.g, h.a().d());
        }
        if (!ai.c(userData.ddid)) {
            this.d.setText("多多ID: " + userData.ddid);
        } else {
            this.d.setVisibility(4);
        }
        if (!ai.c(userData.bgurl)) {
            d.a().a(userData.bgurl, this.h, h.a().k());
        } else {
            this.h.setImageResource(R.drawable.main_page_bkg);
        }
        if (!ai.c(userData.intro)) {
            this.e.setText(userData.intro);
        }
        if (!ai.c(userData.sex)) {
            String str = userData.sex;
            char c2 = 65535;
            switch (str.hashCode()) {
                case 22899:
                    if (str.equals("女")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case 30007:
                    if (str.equals("男")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 657289:
                    if (str.equals("保密")) {
                        c2 = 2;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    this.f.setImageResource(R.drawable.icon_boy);
                    break;
                case 1:
                    this.f.setImageResource(R.drawable.icon_girl);
                    break;
                case 2:
                    this.f.setImageResource(R.drawable.icon_sex_secket);
                    break;
                default:
                    this.f.setVisibility(8);
                    break;
            }
        }
        if (this.j) {
            return;
        }
        if (userData.followed) {
            this.n.setText("已关注");
            this.o.setText("已关注");
            return;
        }
        this.n.setText("关注TA");
        this.o.setText("关注TA");
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                return;
            case R.id.follow_layout:
            case R.id.user_follow:
            case R.id.tv_follow_hint:
                Intent intent = new Intent(RingDDApp.c(), FollowAndFansActivity.class);
                intent.putExtra("type", "follow");
                intent.putExtra("tuid", this.k);
                intent.putExtra("fansNum", this.l);
                intent.putExtra("followNum", this.m);
                startActivity(intent);
                return;
            case R.id.user_fans:
            case R.id.tv_fans_hint:
            case R.id.fans_layout:
                Intent intent2 = new Intent(RingDDApp.c(), FollowAndFansActivity.class);
                intent2.putExtra("type", "fans");
                intent2.putExtra("tuid", this.k);
                intent2.putExtra("fansNum", this.l);
                intent2.putExtra("followNum", this.m);
                startActivity(intent2);
                return;
            case R.id.btn_follow:
            case R.id.btn_top_follow:
                if (this.j) {
                    e();
                    return;
                } else {
                    d();
                    return;
                }
            case R.id.btn_destroy_user:
                new AlertDialog.Builder(this).setMessage("确定屏蔽该用户？ 屏蔽后，不能上传，不能评论").setNegativeButton("再想想", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        u.a("blacklist", "&uid=" + b.g().f() + "&tuid=" + UserMainPageActivity.this.k + "&destroyuser=1", new u.a() {
                            public void a(String str) {
                                a.a("UserMainPageActivity", "blacklist user:" + str);
                                com.shoujiduoduo.util.widget.d.a("操作成功");
                            }

                            public void a(String str, String str2) {
                                a.a("UserMainPageActivity", "blacklist user error");
                                com.shoujiduoduo.util.widget.d.a("操作失败");
                            }
                        });
                    }
                }).show();
                return;
            default:
                return;
        }
    }

    private void d() {
        UserInfo c2 = b.g().c();
        if (c2.isLogin()) {
            String charSequence = this.n.getText().toString();
            UserInfo c3 = b.g().c();
            String str = "&uid=" + c2.getUid() + "&tuid=" + this.k + "&username=" + s.a(c3.getUserName()) + "&headurl=" + s.a(c3.getHeadPic());
            if (this.B) {
                a.a("UserMainPageActivity", "isRequesting, return");
                return;
            }
            this.B = true;
            if ("关注TA".equals(charSequence)) {
                u.a("follow", str, new u.a() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, boolean):boolean
                     arg types: [com.shoujiduoduo.ui.mine.UserMainPageActivity, int]
                     candidates:
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(java.lang.String, boolean):void
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, boolean):boolean */
                    public void a(String str) {
                        try {
                            HttpJsonRes httpJsonRes = (HttpJsonRes) new e().a(str, HttpJsonRes.class);
                            if (httpJsonRes.getResult().equals("success")) {
                                UserMainPageActivity.this.n.setText("取消关注");
                                UserMainPageActivity.this.o.setText("取消关注");
                                com.shoujiduoduo.util.widget.d.a("关注成功");
                                b.g().b(UserMainPageActivity.this.k);
                            } else {
                                com.shoujiduoduo.util.widget.d.a(httpJsonRes.getMsg());
                            }
                        } catch (p e) {
                            e.printStackTrace();
                        }
                        boolean unused = UserMainPageActivity.this.B = false;
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, boolean):boolean
                     arg types: [com.shoujiduoduo.ui.mine.UserMainPageActivity, int]
                     candidates:
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(java.lang.String, boolean):void
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, boolean):boolean */
                    public void a(String str, String str2) {
                        boolean unused = UserMainPageActivity.this.B = false;
                        com.shoujiduoduo.util.widget.d.a("关注失败");
                    }
                });
            } else {
                u.a("unfollow", str, new u.a() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, boolean):boolean
                     arg types: [com.shoujiduoduo.ui.mine.UserMainPageActivity, int]
                     candidates:
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(java.lang.String, boolean):void
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, boolean):boolean */
                    public void a(String str) {
                        UserMainPageActivity.this.n.setText("关注TA");
                        UserMainPageActivity.this.o.setText("关注TA");
                        com.shoujiduoduo.util.widget.d.a("取消关注成功");
                        b.g().a(UserMainPageActivity.this.k);
                        boolean unused = UserMainPageActivity.this.B = false;
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, boolean):boolean
                     arg types: [com.shoujiduoduo.ui.mine.UserMainPageActivity, int]
                     candidates:
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(java.lang.String, boolean):void
                      com.shoujiduoduo.ui.mine.UserMainPageActivity.a(com.shoujiduoduo.ui.mine.UserMainPageActivity, boolean):boolean */
                    public void a(String str, String str2) {
                        com.shoujiduoduo.util.widget.d.a("取消失败");
                        boolean unused = UserMainPageActivity.this.B = false;
                    }
                });
            }
        } else {
            startActivity(new Intent(this, UserLoginActivity.class));
        }
    }

    private void e() {
        if (this.i != null) {
            Intent intent = new Intent(RingDDApp.c(), UserInfoEditActivity.class);
            intent.putExtra("userdata", this.i);
            startActivityForResult(intent, 1);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        char c2;
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1 && i3 == -1) {
            UserData userData = (UserData) intent.getParcelableExtra("new_user_data");
            if (!ai.c(userData.userName)) {
                this.c.setText(userData.userName);
            }
            if (!ai.c(userData.bgurl)) {
                d.a().a(userData.bgurl, this.h, h.a().k());
            }
            if (!ai.c(userData.headUrl)) {
                d.a().a(userData.headUrl, this.g, h.a().d());
            }
            if (!ai.c(userData.intro)) {
                this.e.setText(userData.intro);
            }
            if (!ai.c(userData.sex)) {
                String str = userData.sex;
                switch (str.hashCode()) {
                    case 22899:
                        if (str.equals("女")) {
                            c2 = 1;
                            break;
                        }
                        c2 = 65535;
                        break;
                    case 30007:
                        if (str.equals("男")) {
                            c2 = 0;
                            break;
                        }
                        c2 = 65535;
                        break;
                    case 657289:
                        if (str.equals("保密")) {
                            c2 = 2;
                            break;
                        }
                        c2 = 65535;
                        break;
                    default:
                        c2 = 65535;
                        break;
                }
                switch (c2) {
                    case 0:
                        this.f.setImageResource(R.drawable.icon_boy);
                        return;
                    case 1:
                        this.f.setImageResource(R.drawable.icon_girl);
                        return;
                    case 2:
                        this.f.setImageResource(R.drawable.icon_sex_secket);
                        return;
                    default:
                        this.f.setVisibility(8);
                        return;
                }
            }
        }
    }
}
