package com.shoujiduoduo.ui.mine;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.m;
import com.shoujiduoduo.util.af;
import java.util.ArrayList;
import java.util.List;

/* compiled from: EditModeAdapter */
public class a extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f1801a;
    private DDList b;
    private List<Boolean> c = new ArrayList();
    private String d;

    public a(Context context, DDList dDList, String str) {
        this.f1801a = context;
        this.b = dDList;
        this.d = str;
        for (int i = 0; i < dDList.size(); i++) {
            this.c.add(false);
        }
    }

    public void a(DDList dDList) {
        this.b = dDList;
        if (dDList.size() > 0) {
            this.c.clear();
            this.c = null;
            this.c = new ArrayList();
            for (int i = 0; i < dDList.size(); i++) {
                this.c.add(false);
            }
        }
    }

    public List<Boolean> a() {
        return this.c;
    }

    public List<Integer> b() {
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        int i = 0;
        while (i < this.c.size()) {
            if (this.c.get(i).booleanValue()) {
                if (arrayList2 == null) {
                    arrayList = new ArrayList();
                } else {
                    arrayList = arrayList2;
                }
                arrayList.add(Integer.valueOf(i));
            } else {
                arrayList = arrayList2;
            }
            i++;
            arrayList2 = arrayList;
        }
        return arrayList2;
    }

    public int getCount() {
        if (this.b != null) {
            return this.b.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (i < this.b.size()) {
            return this.b.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (this.b != null && i < this.b.size()) {
            if (view == null) {
                view = LayoutInflater.from(this.f1801a).inflate((int) R.layout.ring_item_delete_mode, viewGroup, false);
            }
            ((CheckBox) m.a(view, R.id.checkbox)).setChecked(this.c.get(i).booleanValue());
            if (this.d.equals("favorite_ring_list")) {
                a(view, i);
            } else if (this.d.equals("make_ring_list")) {
                b(view, i);
            }
        }
        return view;
    }

    private void a(View view, int i) {
        RingData ringData = (RingData) b.b().b("favorite_ring_list").get(i);
        TextView textView = (TextView) m.a(view, R.id.item_duration);
        ((TextView) m.a(view, R.id.item_song_name)).setText(ringData.name);
        ((TextView) m.a(view, R.id.item_artist)).setText(ringData.artist);
        textView.setText(String.format("%02d:%02d", Integer.valueOf(ringData.duration / 60), Integer.valueOf(ringData.duration % 60)));
        if (ringData.duration == 0) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
        af.a(this.f1801a, "user_ring_phone_select", "ringtoneduoduo_not_set");
        af.a(this.f1801a, "user_ring_alarm_select", "ringtoneduoduo_not_set");
        af.a(this.f1801a, "user_ring_notification_select", "ringtoneduoduo_not_set");
    }

    private void b(View view, int i) {
        String string;
        MakeRingData makeRingData = (MakeRingData) b.b().b("make_ring_list").get(i);
        if (makeRingData != null) {
            TextView textView = (TextView) m.a(view, R.id.item_duration);
            ((TextView) m.a(view, R.id.item_song_name)).setText(makeRingData.name);
            ((TextView) m.a(view, R.id.item_artist)).setText(makeRingData.makeDate);
            if (makeRingData.upload != 0) {
                string = this.f1801a.getResources().getString(R.string.upload_suc);
            } else if (makeRingData.percent == -1) {
                string = this.f1801a.getResources().getString(R.string.upload_error);
            } else if (makeRingData.percent == 0) {
                string = "";
            } else {
                string = this.f1801a.getResources().getString(R.string.uploading) + String.valueOf(makeRingData.percent) + "%";
            }
            if (!b.g().g()) {
                textView.setText(String.format("%02d:%02d", Integer.valueOf(makeRingData.duration / 60), Integer.valueOf(makeRingData.duration % 60)));
            } else {
                textView.setText(String.format("%02d:%02d %s", Integer.valueOf(makeRingData.duration / 60), Integer.valueOf(makeRingData.duration % 60), string));
            }
            if (makeRingData.duration == 0) {
                textView.setVisibility(4);
            } else {
                textView.setVisibility(0);
            }
        }
    }
}
