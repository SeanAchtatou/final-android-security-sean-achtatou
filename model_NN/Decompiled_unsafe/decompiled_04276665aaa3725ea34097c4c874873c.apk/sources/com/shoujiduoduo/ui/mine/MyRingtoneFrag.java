package com.shoujiduoduo.ui.mine;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.baoyz.widget.PullRefreshLayout;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListContent;
import com.shoujiduoduo.base.bean.MessageData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.mine.changering.CurrentRingSettingActivity;
import com.shoujiduoduo.ui.settings.SettingActivity;
import com.shoujiduoduo.ui.user.RingListActivity;
import com.shoujiduoduo.ui.user.UserCenterActivity;
import com.shoujiduoduo.ui.user.UserLoginActivity;
import com.shoujiduoduo.ui.utils.TestBtnBkg;
import com.shoujiduoduo.ui.utils.m;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.j;
import com.shoujiduoduo.util.u;
import com.shoujiduoduo.util.widget.MyButton;
import com.shoujiduoduo.util.widget.WebViewActivity;
import com.shoujiduoduo.util.widget.d;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MyRingtoneFrag extends Fragment implements View.OnClickListener, PullRefreshLayout.a {

    /* renamed from: a  reason: collision with root package name */
    private ListView f1772a;
    /* access modifiers changed from: private */
    public PullRefreshLayout b;
    /* access modifiers changed from: private */
    public ArrayList<b> c = new ArrayList<>();
    /* access modifiers changed from: private */
    public c d;
    /* access modifiers changed from: private */
    public LayoutInflater e;
    /* access modifiers changed from: private */
    public UserData f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;
    /* access modifiers changed from: private */
    public boolean j;
    private AdapterView.OnItemClickListener k = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            switch (AnonymousClass9.f1780a[((b) MyRingtoneFrag.this.c.get(i)).f1782a.ordinal()]) {
                case 1:
                    com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "open user center");
                    MyRingtoneFrag.this.d();
                    return;
                case 2:
                    MyRingtoneFrag.this.startActivity(new Intent(RingDDApp.c(), com.shoujiduoduo.a.b.b.g().g() ? UserCenterActivity.class : UserLoginActivity.class));
                    return;
                case 3:
                    Intent intent = new Intent(RingDDApp.c(), RingListActivity.class);
                    intent.putExtra("list_id", "user_favorite");
                    intent.putExtra("title", "我的铃声收藏");
                    MyRingtoneFrag.this.startActivity(intent);
                    return;
                case 4:
                    MyRingtoneFrag.this.startActivity(new Intent(RingDDApp.c(), CurrentRingSettingActivity.class));
                    return;
                case 5:
                    MyRingtoneFrag.this.startActivity(new Intent(RingDDApp.c(), TestBtnBkg.class));
                    return;
                case 6:
                    Intent intent2 = new Intent(RingDDApp.c(), RingListActivity.class);
                    intent2.putExtra("list_id", "user_collect");
                    intent2.putExtra("title", "我的精选集");
                    MyRingtoneFrag.this.startActivity(intent2);
                    return;
                case 7:
                    Intent intent3 = new Intent(RingDDApp.c(), RingListActivity.class);
                    intent3.putExtra("list_id", "user_make");
                    intent3.putExtra("title", "我的作品");
                    MyRingtoneFrag.this.startActivity(intent3);
                    return;
                case 8:
                    Intent intent4 = new Intent(RingDDApp.c(), WebViewActivity.class);
                    intent4.putExtra("url", MyRingtoneFrag.this.e());
                    intent4.putExtra("title", "我的快秀");
                    MyRingtoneFrag.this.startActivity(intent4);
                    return;
                case 9:
                    MyRingtoneFrag.this.startActivity(new Intent(RingDDApp.c(), SettingActivity.class));
                    return;
                case 10:
                    if (com.shoujiduoduo.a.b.b.g().g()) {
                        Intent intent5 = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                        intent5.putExtra("tuid", com.shoujiduoduo.a.b.b.g().c().getUid());
                        MyRingtoneFrag.this.startActivity(intent5);
                        return;
                    }
                    MyRingtoneFrag.this.startActivity(new Intent(MyRingtoneFrag.this.getActivity(), UserLoginActivity.class));
                    return;
                default:
                    return;
            }
        }
    };
    private g l = new g() {
        public void a(DDList dDList, int i) {
            MyRingtoneFrag.this.b(dDList.getListId());
        }
    };
    private x m = new x() {
        public void a(int i) {
            com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "onVipStateChange");
            MyRingtoneFrag.this.d.notifyDataSetChanged();
        }
    };
    private v n = new v() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.MyRingtoneFrag.b(com.shoujiduoduo.ui.mine.MyRingtoneFrag, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.MyRingtoneFrag, int]
         candidates:
          com.shoujiduoduo.ui.mine.MyRingtoneFrag.b(com.shoujiduoduo.ui.mine.MyRingtoneFrag, com.shoujiduoduo.base.bean.UserData):void
          com.shoujiduoduo.ui.mine.MyRingtoneFrag.b(com.shoujiduoduo.ui.mine.MyRingtoneFrag, boolean):boolean */
        public void a(int i, boolean z, String str, String str2) {
            if (z) {
                com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "onLogin");
                if (!MyRingtoneFrag.this.j) {
                    boolean unused = MyRingtoneFrag.this.j = true;
                    MyRingtoneFrag.this.a(com.shoujiduoduo.a.b.b.g().f(), true, false);
                    MyRingtoneFrag.this.b();
                }
                MyRingtoneFrag.this.d.notifyDataSetChanged();
            }
        }

        public void a(int i) {
            com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "onLogout");
            MyRingtoneFrag.this.d.notifyDataSetChanged();
        }

        public void b(int i) {
            MyRingtoneFrag.this.d.notifyDataSetChanged();
        }

        public void a(String str, boolean z) {
            MyRingtoneFrag.this.d.notifyDataSetChanged();
        }

        public void a(String str) {
        }
    };
    private w o = new w() {
        public void a() {
        }

        public void a(int i, List<RingData> list, String str) {
            MyRingtoneFrag.this.b(str);
        }
    };

    private enum a {
        user_center,
        vip_center,
        ring_favorite,
        current_ring,
        notification,
        collect,
        work,
        kuaixiu,
        setting,
        main_page
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.fragment_mine, viewGroup, false);
        this.e = layoutInflater;
        f();
        this.f1772a = (ListView) inflate.findViewById(R.id.lv_mine_item);
        this.d = new c();
        this.f1772a.setAdapter((ListAdapter) this.d);
        this.f1772a.setOnItemClickListener(this.k);
        this.b = (PullRefreshLayout) inflate.findViewById(R.id.swipeRefreshLayout);
        this.b.setOnRefreshListener(this);
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        if (c2.isLogin()) {
            a(c2.getUid(), true, false);
            b();
        }
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.m);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.n);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.o);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.l);
        return inflate;
    }

    public void d_() {
        com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "onRefresh");
        if (!com.shoujiduoduo.a.b.b.g().g()) {
            com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "onrefresh, plesae login");
            this.b.setRefreshing(false);
            this.j = false;
            d.a("请先登录!");
        } else if (!this.j) {
            com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "get refresh data");
            this.j = true;
            a(com.shoujiduoduo.a.b.b.g().f(), true, true);
            b();
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        final String str = "&pagesize=25&tb=" + af.a(RingDDApp.c(), "concern_feeds_newest_time", "") + "&te=" + "&uid=" + com.shoujiduoduo.a.b.b.g().f();
        u.a("getfeeds", str, new u.a() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.mine.MyRingtoneFrag.a(com.shoujiduoduo.ui.mine.MyRingtoneFrag, boolean):boolean
             arg types: [com.shoujiduoduo.ui.mine.MyRingtoneFrag, int]
             candidates:
              com.shoujiduoduo.ui.mine.MyRingtoneFrag.a(com.shoujiduoduo.ui.mine.MyRingtoneFrag, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
              com.shoujiduoduo.ui.mine.MyRingtoneFrag.a(com.shoujiduoduo.ui.mine.MyRingtoneFrag, java.lang.String):void
              com.shoujiduoduo.ui.mine.MyRingtoneFrag.a(com.shoujiduoduo.ui.mine.MyRingtoneFrag, boolean):boolean */
            public void a(String str) {
                if (ai.c(str)) {
                    com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "RingList: httpGetRingList Failed!");
                    return;
                }
                ListContent<MessageData> c = j.c(new ByteArrayInputStream(str.getBytes()));
                if (c != null && c.data.size() > 0) {
                    boolean unused = MyRingtoneFrag.this.g = true;
                    MyRingtoneFrag.this.d.notifyDataSetChanged();
                    com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                        public void a() {
                            ((v) this.f1284a).a("");
                        }
                    });
                } else if (c == null) {
                    com.umeng.a.b.a(RingDDApp.c(), "parseMessageContent error, param:" + str);
                }
            }

            public void a(String str, String str2) {
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(String str, boolean z, final boolean z2) {
        this.j = true;
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        StringBuilder sb = new StringBuilder();
        sb.append("&uid=").append(c2.getUid()).append("&tuid=").append(str);
        if (z) {
            sb.append("&username=").append(u.i(c2.getUserName()));
            sb.append("&headurl=").append(u.i(c2.getHeadPic()));
        }
        u.a("getuserinfo", sb.toString(), new u.a() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.mine.MyRingtoneFrag.b(com.shoujiduoduo.ui.mine.MyRingtoneFrag, boolean):boolean
             arg types: [com.shoujiduoduo.ui.mine.MyRingtoneFrag, int]
             candidates:
              com.shoujiduoduo.ui.mine.MyRingtoneFrag.b(com.shoujiduoduo.ui.mine.MyRingtoneFrag, com.shoujiduoduo.base.bean.UserData):void
              com.shoujiduoduo.ui.mine.MyRingtoneFrag.b(com.shoujiduoduo.ui.mine.MyRingtoneFrag, boolean):boolean */
            public void a(String str) {
                com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "userinfo:" + str);
                UserData b2 = j.b(str);
                if (b2 != null) {
                    boolean unused = MyRingtoneFrag.this.j = false;
                    UserData unused2 = MyRingtoneFrag.this.f = b2;
                    MyRingtoneFrag.this.a(MyRingtoneFrag.this.f);
                    MyRingtoneFrag.this.b.setRefreshing(false);
                    MyRingtoneFrag.this.d.notifyDataSetChanged();
                    if (z2) {
                        d.a("刷新成功!");
                        return;
                    }
                    return;
                }
                com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "user 解析失败");
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.mine.MyRingtoneFrag.b(com.shoujiduoduo.ui.mine.MyRingtoneFrag, boolean):boolean
             arg types: [com.shoujiduoduo.ui.mine.MyRingtoneFrag, int]
             candidates:
              com.shoujiduoduo.ui.mine.MyRingtoneFrag.b(com.shoujiduoduo.ui.mine.MyRingtoneFrag, com.shoujiduoduo.base.bean.UserData):void
              com.shoujiduoduo.ui.mine.MyRingtoneFrag.b(com.shoujiduoduo.ui.mine.MyRingtoneFrag, boolean):boolean */
            public void a(String str, String str2) {
                MyRingtoneFrag.this.b.setRefreshing(false);
                boolean unused = MyRingtoneFrag.this.j = false;
                com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "user 信息获取失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(UserData userData) {
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        if (userData.followerNum > c2.getFansNum()) {
            this.h = true;
        }
        if (userData.followingNum > c2.getFollowNum()) {
            this.i = true;
        }
        if (this.i || this.h || !c2.getFollowings().equals(userData.followings)) {
            c2.setFollowNum(userData.followingNum);
            c2.setFansNum(userData.followerNum);
            c2.setFollowings(userData.followings);
            c2.setIsSuperuser(userData.isSuperUser);
            com.shoujiduoduo.a.b.b.g().a(c2);
            com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                public void a() {
                    ((v) this.f1284a).a("");
                }
            });
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.follow_layout:
                this.i = false;
                this.d.notifyDataSetChanged();
                a("follow");
                return;
            case R.id.iv_userhead:
            case R.id.btn_mainpage:
                d();
                return;
            case R.id.fans_layout:
                this.h = false;
                this.d.notifyDataSetChanged();
                a("fans");
                return;
            case R.id.notification_layout:
                this.g = false;
                this.d.notifyDataSetChanged();
                c();
                return;
            default:
                return;
        }
    }

    private void c() {
        if (!com.shoujiduoduo.a.b.b.g().g()) {
            startActivity(new Intent(getActivity(), UserLoginActivity.class));
        } else if (this.f != null) {
            Intent intent = new Intent(RingDDApp.c(), MessageActivity.class);
            intent.putExtra("type", 16843169);
            intent.putExtra("tuid", this.f.uid);
            intent.putExtra("fansNum", this.f.followerNum);
            intent.putExtra("followNum", this.f.followingNum);
            startActivity(intent);
        } else {
            com.shoujiduoduo.base.a.a.e("MyRingtoneFrag", "mUserData is null");
        }
    }

    private void a(String str) {
        if (!com.shoujiduoduo.a.b.b.g().g()) {
            startActivity(new Intent(getActivity(), UserLoginActivity.class));
        } else if (this.f != null) {
            Intent intent = new Intent(RingDDApp.c(), FollowAndFansActivity.class);
            intent.putExtra("type", str);
            intent.putExtra("tuid", this.f.uid);
            intent.putExtra("fansNum", this.f.followerNum);
            intent.putExtra("followNum", this.f.followingNum);
            startActivity(intent);
        } else {
            com.shoujiduoduo.base.a.a.e("MyRingtoneFrag", "muserdata is null");
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (com.shoujiduoduo.a.b.b.g().g()) {
            Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
            intent.putExtra("tuid", com.shoujiduoduo.a.b.b.g().c().getUid());
            startActivity(intent);
            return;
        }
        startActivity(new Intent(getActivity(), UserLoginActivity.class));
    }

    /* renamed from: com.shoujiduoduo.ui.mine.MyRingtoneFrag$9  reason: invalid class name */
    static /* synthetic */ class AnonymousClass9 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f1780a = new int[a.values().length];

        static {
            try {
                f1780a[a.user_center.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1780a[a.vip_center.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1780a[a.ring_favorite.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1780a[a.current_ring.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f1780a[a.notification.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f1780a[a.collect.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f1780a[a.work.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f1780a[a.kuaixiu.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f1780a[a.setting.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                f1780a[a.main_page.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
        }
    }

    /* access modifiers changed from: private */
    public String e() {
        String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "personal_kuaixiu_url");
        if (ai.b(a2)) {
            a2 = "http://musicalbum.shoujiduoduo.com/malbum/serv/user.php?ddsrc=upload_ring_ar&owner=1";
        }
        com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "personal kuaixiu url:" + h.b(a2));
        return h.b(a2);
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "onDataUpdate, listId:" + str);
        if ("user_favorite".equals(str)) {
            Iterator<b> it = this.c.iterator();
            while (it.hasNext()) {
                b next = it.next();
                if (next.f1782a == a.ring_favorite) {
                    next.f = "" + com.shoujiduoduo.a.b.b.b().b("favorite_ring_list").size();
                    com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "user favorite ring size:" + next.f);
                }
            }
            this.d.notifyDataSetChanged();
        } else if ("user_collect".equals(str)) {
            Iterator<b> it2 = this.c.iterator();
            while (it2.hasNext()) {
                b next2 = it2.next();
                if (next2.f1782a == a.collect) {
                    next2.f = "" + com.shoujiduoduo.a.b.b.b().b("collect_ring_list").size();
                    com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "user collect ring size:" + next2.f);
                }
            }
            this.d.notifyDataSetChanged();
        } else if ("user_make".equals(str)) {
            Iterator<b> it3 = this.c.iterator();
            while (it3.hasNext()) {
                b next3 = it3.next();
                if (next3.f1782a == a.work) {
                    next3.f = "" + com.shoujiduoduo.a.b.b.b().b("make_ring_list").size();
                    com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "user make ring size:" + next3.f);
                }
            }
            this.d.notifyDataSetChanged();
        }
    }

    private class b {

        /* renamed from: a  reason: collision with root package name */
        a f1782a;
        String b;
        String c;
        int d;
        boolean e = true;
        String f;

        b(String str, int i, a aVar) {
            this.b = str;
            this.d = i;
            this.f1782a = aVar;
        }

        public void a(String str) {
            this.c = str;
        }

        public void a(boolean z) {
            this.e = z;
        }

        public void a(int i) {
            this.f = "" + i;
        }

        public void b(String str) {
            this.f = str;
        }
    }

    private void f() {
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        if (c2.isLogin()) {
            b bVar = new b(c2.getUserName(), R.drawable.user_head, a.user_center);
            bVar.a(true);
            bVar.b("个人主页");
            if (!c2.isVip()) {
                bVar.a("已登录，尚未开通VIP");
            }
            this.c.add(bVar);
        } else {
            b bVar2 = new b("点击登录", R.drawable.user_head, a.user_center);
            bVar2.a(false);
            bVar2.b("个人主页");
            bVar2.a("登录开启云端收藏铃声");
            this.c.add(bVar2);
        }
        if (!c2.isVip() || !c2.isLogin()) {
            b bVar3 = new b("开通VIP", R.drawable.icon_vip_not_open, a.vip_center);
            bVar3.a("立即开通享有特权");
            this.c.add(bVar3);
        } else {
            b bVar4 = new b("我的VIP特权", R.drawable.icon_vip_opened, a.vip_center);
            bVar4.a("已享VIP特权");
            this.c.add(bVar4);
        }
        int size = com.shoujiduoduo.a.b.b.b().b("favorite_ring_list").size();
        int size2 = com.shoujiduoduo.a.b.b.b().b("collect_ring_list").size();
        int size3 = com.shoujiduoduo.a.b.b.b().b("make_ring_list").size();
        b bVar5 = new b("我的铃声收藏", R.drawable.icon_my_favorite, a.ring_favorite);
        bVar5.a(size);
        this.c.add(bVar5);
        this.c.add(new b("我的当前铃声", R.drawable.icon_my_current_ring, a.current_ring));
        if (size2 > 0) {
            b bVar6 = new b("我的精选集", R.drawable.icon_my_collect, a.collect);
            bVar6.a(size2);
            this.c.add(bVar6);
        }
        b bVar7 = new b("我的作品", R.drawable.icon_my_works, a.work);
        bVar7.a(size3);
        this.c.add(bVar7);
        this.c.add(new b("我的快秀", R.drawable.icon_my_kuaixiu, a.kuaixiu));
        this.c.add(new b("设置", R.drawable.icon_setting, a.setting));
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.m);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.n);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.o);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.l);
    }

    private class c extends BaseAdapter {
        private int b;
        private int c;

        private c() {
            this.b = 0;
            this.c = 1;
        }

        public int getCount() {
            return MyRingtoneFrag.this.c.size();
        }

        public Object getItem(int i) {
            return MyRingtoneFrag.this.c.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (getItemViewType(i) == this.b) {
                if (view == null) {
                    view = MyRingtoneFrag.this.e.inflate((int) R.layout.listitem_user, viewGroup, false);
                }
                ImageView imageView = (ImageView) view.findViewById(R.id.iv_userhead);
                TextView textView = (TextView) view.findViewById(R.id.tv_title);
                TextView textView2 = (TextView) view.findViewById(R.id.tv_des);
                MyButton myButton = (MyButton) view.findViewById(R.id.btn_mainpage);
                LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.divider_line);
                LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.divider_gray);
                TextView textView3 = (TextView) m.a(view, R.id.tv_message_num);
                TextView textView4 = (TextView) m.a(view, R.id.tv_follow_num);
                TextView textView5 = (TextView) m.a(view, R.id.tv_fans_num);
                ((LinearLayout) m.a(view, R.id.notification_layout)).setOnClickListener(MyRingtoneFrag.this);
                ((LinearLayout) m.a(view, R.id.fans_layout)).setOnClickListener(MyRingtoneFrag.this);
                ((LinearLayout) m.a(view, R.id.follow_layout)).setOnClickListener(MyRingtoneFrag.this);
                myButton.setOnClickListener(MyRingtoneFrag.this);
                ((ImageView) m.a(view, R.id.iv_red_point_notification)).setVisibility(MyRingtoneFrag.this.g ? 0 : 4);
                ((ImageView) m.a(view, R.id.iv_red_point_fans)).setVisibility(MyRingtoneFrag.this.h ? 0 : 4);
                ((ImageView) m.a(view, R.id.iv_red_point_follow)).setVisibility(MyRingtoneFrag.this.i ? 0 : 4);
                b bVar = (b) MyRingtoneFrag.this.c.get(i);
                linearLayout.setVisibility(4);
                linearLayout2.setVisibility(0);
                UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
                if (c2.isLogin()) {
                    myButton.setVisibility(0);
                    textView.setText(c2.getUserName());
                    if (!ai.c(c2.getHeadPic())) {
                        com.d.a.b.d.a().a(c2.getHeadPic(), imageView, com.shoujiduoduo.ui.utils.h.a().d());
                    } else {
                        imageView.setImageResource(bVar.d);
                    }
                    if (c2.isVip()) {
                        textView2.setVisibility(4);
                        view.findViewById(R.id.iv_vip_show).setVisibility(0);
                    } else {
                        textView2.setVisibility(0);
                        textView2.setText("已登录，尚未开通VIP");
                        view.findViewById(R.id.iv_vip_show).setVisibility(4);
                    }
                    textView5.setText("" + com.shoujiduoduo.a.b.b.g().c().getFansNum());
                    textView4.setText("" + com.shoujiduoduo.a.b.b.g().c().getFollowNum());
                } else {
                    myButton.setVisibility(4);
                    textView.setText("点击登录");
                    textView2.setText("登录开启云端收藏");
                    textView2.setVisibility(0);
                    imageView.setImageResource(bVar.d);
                    view.findViewById(R.id.iv_vip_show).setVisibility(4);
                }
            } else {
                if (view == null) {
                    view = MyRingtoneFrag.this.e.inflate((int) R.layout.listitem_mine, viewGroup, false);
                }
                ImageView imageView2 = (ImageView) view.findViewById(R.id.iv_icon);
                TextView textView6 = (TextView) view.findViewById(R.id.tv_title);
                TextView textView7 = (TextView) view.findViewById(R.id.tv_des);
                TextView textView8 = (TextView) view.findViewById(R.id.tv_number);
                LinearLayout linearLayout3 = (LinearLayout) view.findViewById(R.id.divider_line);
                LinearLayout linearLayout4 = (LinearLayout) view.findViewById(R.id.divider_gray);
                ImageView imageView3 = (ImageView) view.findViewById(R.id.iv_right_arrow);
                b bVar2 = (b) MyRingtoneFrag.this.c.get(i);
                if (bVar2.f1782a == a.vip_center || bVar2.f1782a == a.kuaixiu) {
                    linearLayout3.setVisibility(4);
                    linearLayout4.setVisibility(0);
                } else {
                    linearLayout3.setVisibility(0);
                    linearLayout4.setVisibility(8);
                }
                if (bVar2.f1782a == a.vip_center) {
                    imageView3.setVisibility(0);
                    UserInfo c3 = com.shoujiduoduo.a.b.b.g().c();
                    if (!c3.isLogin() || !c3.isVip()) {
                        textView6.setText("开通VIP会员");
                        textView7.setText("立即开通享特权");
                        imageView2.setImageResource(R.drawable.icon_vip_not_open);
                    } else {
                        textView6.setText("我的VIP特权");
                        textView7.setText("已享VIP特权");
                        imageView2.setImageResource(R.drawable.icon_vip_opened);
                    }
                } else {
                    textView6.setText(bVar2.b);
                    if (!ai.c(bVar2.c)) {
                        textView7.setText(bVar2.c);
                    } else {
                        textView7.setVisibility(8);
                    }
                    imageView2.setImageResource(bVar2.d);
                }
                if (bVar2.f1782a == a.ring_favorite) {
                    bVar2.f = "" + com.shoujiduoduo.a.b.b.b().b("favorite_ring_list").size();
                } else if (bVar2.f1782a == a.work) {
                    bVar2.f = "" + com.shoujiduoduo.a.b.b.b().b("make_ring_list").size();
                } else if (bVar2.f1782a == a.collect) {
                    bVar2.f = "" + com.shoujiduoduo.a.b.b.b().b("collect_ring_list").size();
                }
                if (!ai.c(bVar2.f)) {
                    textView8.setText(bVar2.f);
                } else {
                    textView8.setVisibility(4);
                }
            }
            return view;
        }

        public int getItemViewType(int i) {
            if (i == 0) {
                return this.b;
            }
            return this.c;
        }

        public int getViewTypeCount() {
            return 2;
        }
    }
}
