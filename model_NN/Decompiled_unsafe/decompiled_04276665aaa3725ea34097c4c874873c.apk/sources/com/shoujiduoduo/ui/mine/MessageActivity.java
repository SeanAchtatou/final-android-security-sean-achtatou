package com.shoujiduoduo.ui.mine;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import com.shoujiduoduo.b.c.h;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.SlidingActivity;

public class MessageActivity extends SlidingActivity {

    /* renamed from: a  reason: collision with root package name */
    private String f1770a;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_message);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MessageActivity.this.finish();
            }
        });
        Intent intent = getIntent();
        if (intent != null) {
            this.f1770a = intent.getStringExtra("tuid");
        }
        DDListFragment dDListFragment = new DDListFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putString("adapter_type", "concern_feeds_adapter");
        bundle2.putBoolean("support_pull_refresh", true);
        dDListFragment.setArguments(bundle2);
        dDListFragment.a(new h(this.f1770a));
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.list_frag_layout, dDListFragment);
        beginTransaction.commitAllowingStateLoss();
    }

    public void a() {
    }

    public void b() {
        finish();
    }
}
