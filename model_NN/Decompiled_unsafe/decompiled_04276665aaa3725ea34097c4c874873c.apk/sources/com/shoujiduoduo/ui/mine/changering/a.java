package com.shoujiduoduo.ui.mine.changering;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.b.c.j;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.ui.utils.m;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.aj;
import com.shoujiduoduo.util.l;
import java.io.File;

/* compiled from: SystemRingListAdapter */
public class a extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public j f1858a;
    /* access modifiers changed from: private */
    public int b = -1;
    /* access modifiers changed from: private */
    public boolean d;
    /* access modifiers changed from: private */
    public Context e;
    private Handler f;
    private o g = new o() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.changering.a, int]
         candidates:
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, int):int
          com.shoujiduoduo.ui.mine.changering.a.a(android.view.View, int):void
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean */
        public void a(String str, int i) {
            if (a.this.f1858a != null && a.this.f1858a.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("SystemRingListAdapter", "onSetPlay, listid:" + str);
                if (str.equals(a.this.f1858a.getListId())) {
                    boolean unused = a.this.d = true;
                    int unused2 = a.this.b = i;
                } else {
                    boolean unused3 = a.this.d = false;
                }
                a.this.notifyDataSetChanged();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.changering.a, int]
         candidates:
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, int):int
          com.shoujiduoduo.ui.mine.changering.a.a(android.view.View, int):void
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean */
        public void b(String str, int i) {
            if (a.this.f1858a != null && a.this.f1858a.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("SystemRingListAdapter", "onCanclePlay, listId:" + str);
                boolean unused = a.this.d = false;
                int unused2 = a.this.b = i;
                a.this.notifyDataSetChanged();
            }
        }

        public void a(String str, int i, int i2) {
            if (a.this.f1858a != null && a.this.f1858a.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("SystemRingListAdapter", "onStatusChange, listid:" + str);
                a.this.notifyDataSetChanged();
            }
        }
    };
    private View.OnClickListener h = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = ab.a().b();
            if (b != null) {
                if (b.a() == 3) {
                    b.n();
                } else {
                    b.i();
                }
            }
        }
    };
    private View.OnClickListener i = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = ab.a().b();
            if (b != null) {
                b.j();
            }
        }
    };
    private View.OnClickListener j = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = ab.a().b();
            if (b != null) {
                b.a(a.this.f1858a, a.this.b);
            }
        }
    };
    private View.OnClickListener k = new View.OnClickListener() {
        public void onClick(View view) {
            if (a.this.f1858a.get(a.this.b) != null) {
                final int i = 0;
                switch (AnonymousClass8.f1866a[a.this.f1858a.a().ordinal()]) {
                    case 1:
                        i = 4;
                        break;
                    case 2:
                        i = 2;
                        break;
                    case 3:
                        i = 1;
                        break;
                }
                aj ajVar = new aj(RingDDApp.c());
                File file = new File(a.this.f1858a.get(a.this.b).localPath);
                if (file.exists()) {
                    ajVar.a(i, Uri.fromFile(file), a.this.f1858a.get(a.this.b).localPath);
                    String str = "";
                    String str2 = a.this.f1858a.get(a.this.b).name;
                    String string = a.this.e.getResources().getString(R.string.set_ring_hint2);
                    switch (i) {
                        case 1:
                            str = str2 + string + a.this.e.getResources().getString(R.string.set_ring_incoming_call);
                            break;
                        case 2:
                            str = str2 + string + a.this.e.getResources().getString(R.string.set_ring_message);
                            break;
                        case 4:
                            str = str2 + string + a.this.e.getResources().getString(R.string.set_ring_alarm);
                            break;
                    }
                    com.shoujiduoduo.util.widget.d.a(str);
                    c.a().b(b.OBSERVER_RING_CHANGE, new c.a<p>() {
                        public void a() {
                            ((p) this.f1284a).a(i, new RingData());
                        }
                    });
                }
            }
        }
    };

    public a(Context context) {
        this.e = context;
        this.f = new Handler();
    }

    public void a() {
        c.a().a(b.OBSERVER_PLAY_STATUS, this.g);
    }

    public void b() {
        c.a().b(b.OBSERVER_PLAY_STATUS, this.g);
    }

    public int getCount() {
        if (this.f1858a == null) {
            return 0;
        }
        if (this.c == d.a.LIST_CONTENT) {
            return this.f1858a.size();
        }
        return 1;
    }

    public Object getItem(int i2) {
        if (this.f1858a != null && i2 >= 0 && i2 < this.f1858a.size()) {
            return this.f1858a.get(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private void a(View view, int i2) {
        RingData a2 = this.f1858a.get(i2);
        TextView textView = (TextView) m.a(view, R.id.item_duration);
        ((TextView) m.a(view, R.id.item_song_name)).setText(a2.name);
        textView.setText(String.format("%02d:%02d", Integer.valueOf(a2.duration / 60), Integer.valueOf(a2.duration % 60)));
        if (a2.duration == 0) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
    }

    /* renamed from: com.shoujiduoduo.ui.mine.changering.a$8  reason: invalid class name */
    /* compiled from: SystemRingListAdapter */
    static /* synthetic */ class AnonymousClass8 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f1866a = new int[ListType.LIST_TYPE.values().length];

        static {
            try {
                f1866a[ListType.LIST_TYPE.sys_alarm.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1866a[ListType.LIST_TYPE.sys_notify.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1866a[ListType.LIST_TYPE.sys_ringtone.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (this.f1858a == null) {
            return null;
        }
        switch (getItemViewType(i2)) {
            case 0:
                if (i2 >= this.f1858a.size()) {
                    return view;
                }
                if (view == null) {
                    view = LayoutInflater.from(this.e).inflate((int) R.layout.system_ring_item, viewGroup, false);
                }
                a(view, i2);
                ProgressBar progressBar = (ProgressBar) m.a(view, R.id.ringitem_download_progress);
                TextView textView = (TextView) m.a(view, R.id.ringitem_serial_number);
                ImageButton imageButton = (ImageButton) m.a(view, R.id.ringitem_play);
                imageButton.setOnClickListener(this.h);
                ImageButton imageButton2 = (ImageButton) m.a(view, R.id.ringitem_pause);
                imageButton2.setOnClickListener(this.i);
                ImageButton imageButton3 = (ImageButton) m.a(view, R.id.ringitem_failed);
                imageButton3.setOnClickListener(this.j);
                PlayerService b2 = ab.a().b();
                if (i2 != this.b || !this.d) {
                    ((Button) m.a(view, R.id.ring_item_set)).setVisibility(8);
                    textView.setText(Integer.toString(i2 + 1));
                    textView.setVisibility(0);
                    progressBar.setVisibility(4);
                    imageButton.setVisibility(4);
                    imageButton2.setVisibility(4);
                    imageButton3.setVisibility(4);
                    return view;
                }
                Button button = (Button) m.a(view, R.id.ring_item_set);
                button.setVisibility(0);
                button.setOnClickListener(this.k);
                textView.setVisibility(4);
                imageButton.setVisibility(4);
                imageButton2.setVisibility(4);
                imageButton3.setVisibility(4);
                int i3 = 5;
                if (b2 != null) {
                    i3 = b2.a();
                }
                switch (i3) {
                    case 1:
                        progressBar.setVisibility(0);
                        return view;
                    case 2:
                        imageButton2.setVisibility(0);
                        return view;
                    case 3:
                    case 4:
                    case 5:
                        imageButton.setVisibility(0);
                        return view;
                    case 6:
                        imageButton3.setVisibility(0);
                        return view;
                    default:
                        return view;
                }
            case 1:
            default:
                return view;
            case 2:
                View inflate = LayoutInflater.from(this.e).inflate((int) R.layout.list_loading, viewGroup, false);
                if (l.b > 0) {
                    AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams) inflate.getLayoutParams();
                    layoutParams.width = l.b;
                    layoutParams.height = l.b;
                    inflate.setLayoutParams(layoutParams);
                }
                final AnimationDrawable animationDrawable = (AnimationDrawable) ((ImageView) inflate.findViewById(R.id.loading)).getBackground();
                this.f.post(new Runnable() {
                    public void run() {
                        animationDrawable.start();
                    }
                });
                return inflate;
            case 3:
                View inflate2 = LayoutInflater.from(this.e).inflate((int) R.layout.list_failed, viewGroup, false);
                if (l.b > 0) {
                    AbsListView.LayoutParams layoutParams2 = (AbsListView.LayoutParams) inflate2.getLayoutParams();
                    layoutParams2.width = l.b;
                    layoutParams2.height = l.b;
                    inflate2.setLayoutParams(layoutParams2);
                }
                inflate2.findViewById(R.id.network_fail_layout).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        a.this.a(d.a.LIST_LOADING);
                        a.this.f1858a.retrieveData();
                    }
                });
                return inflate2;
        }
    }

    public void a(boolean z) {
    }

    public void a(DDList dDList) {
        if (this.f1858a != dDList) {
            this.f1858a = null;
            this.f1858a = (j) dDList;
            this.d = false;
            notifyDataSetChanged();
        }
    }
}
