package com.shoujiduoduo.ui.mine;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.shoujiduoduo.b.f.e;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.af;

/* compiled from: RingUploadDialog */
public class d extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1867a;
    /* access modifiers changed from: private */
    public MakeRingData b;
    private EditText c;
    /* access modifiers changed from: private */
    public EditText d;
    private ImageButton e;
    private Button f;
    /* access modifiers changed from: private */
    public RadioGroup g;
    /* access modifiers changed from: private */
    public RadioGroup h;
    private RadioButton[] i = new RadioButton[10];
    /* access modifiers changed from: private */
    public e j;
    /* access modifiers changed from: private */
    public int k;

    public d(Context context, int i2, RingData ringData) {
        super(context, i2);
        this.f1867a = context;
        this.b = (MakeRingData) ringData;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_ring_upload);
        this.j = new e();
        String a2 = af.a(this.f1867a, "user_upload_name", "");
        this.c = (EditText) findViewById(R.id.et_upload_name);
        this.c.setText(this.b.name);
        this.d = (EditText) findViewById(R.id.et_user_name);
        this.d.setText(a2);
        this.g = (RadioGroup) findViewById(R.id.group1);
        this.h = (RadioGroup) findViewById(R.id.group2);
        this.i[0] = (RadioButton) findViewById(R.id.liuxingjinqu);
        this.i[1] = (RadioButton) findViewById(R.id.xinxiduanxi);
        this.i[2] = (RadioButton) findViewById(R.id.yingshiguanggao);
        this.i[3] = (RadioButton) findViewById(R.id.djwuqu);
        this.i[4] = (RadioButton) findViewById(R.id.dongmanyouxi);
        this.i[5] = (RadioButton) findViewById(R.id.gaoxiaolinglei);
        this.i[6] = (RadioButton) findViewById(R.id.rihanchaoliu);
        this.i[7] = (RadioButton) findViewById(R.id.chunyinyue);
        this.i[8] = (RadioButton) findViewById(R.id.oumeiliuxing);
        this.i[9] = (RadioButton) findViewById(R.id.yueyuliuxing);
        for (int i2 = 0; i2 <= 9; i2++) {
            this.i[i2].setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    if (z) {
                        if (compoundButton.getParent() == d.this.g) {
                            d.this.h.clearCheck();
                        } else {
                            d.this.g.clearCheck();
                        }
                        if (compoundButton.getId() == R.id.liuxingjinqu) {
                            int unused = d.this.k = 1;
                        } else if (compoundButton.getId() == R.id.xinxiduanxi) {
                            int unused2 = d.this.k = 2;
                        } else if (compoundButton.getId() == R.id.yingshiguanggao) {
                            int unused3 = d.this.k = 3;
                        } else if (compoundButton.getId() == R.id.djwuqu) {
                            int unused4 = d.this.k = 4;
                        } else if (compoundButton.getId() == R.id.dongmanyouxi) {
                            int unused5 = d.this.k = 5;
                        } else if (compoundButton.getId() == R.id.gaoxiaolinglei) {
                            int unused6 = d.this.k = 6;
                        } else if (compoundButton.getId() == R.id.rihanchaoliu) {
                            int unused7 = d.this.k = 31;
                        } else if (compoundButton.getId() == R.id.chunyinyue) {
                            int unused8 = d.this.k = 32;
                        } else if (compoundButton.getId() == R.id.oumeiliuxing) {
                            int unused9 = d.this.k = 33;
                        } else if (compoundButton.getId() == R.id.yueyuliuxing) {
                            int unused10 = d.this.k = 34;
                        }
                    }
                }
            });
        }
        this.e = (ImageButton) findViewById(R.id.upload_close);
        this.e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                d.this.dismiss();
            }
        });
        this.f = (Button) findViewById(R.id.btn_upload);
        this.f.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (d.this.k == 0) {
                    Toast.makeText(d.this.f1867a, (int) R.string.pick_category, 1).show();
                    return;
                }
                String obj = d.this.d.getText().toString();
                if (!TextUtils.isEmpty(obj.trim())) {
                    af.c(d.this.f1867a, "user_upload_name", obj.trim());
                    d.this.j.a(d.this.b, d.this.k);
                    com.shoujiduoduo.util.widget.d.a((int) R.string.ring_is_uploading);
                    d.this.dismiss();
                    return;
                }
                Toast.makeText(d.this.f1867a, (int) R.string.input_user_name_hint, 1).show();
            }
        });
    }
}
