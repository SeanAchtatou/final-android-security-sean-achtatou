package com.shoujiduoduo.ui.mine.changering;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.shoujiduoduo.b.c.j;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.g;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.List;
import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.b.a.a.c;
import net.lucode.hackware.magicindicator.b.a.a.d;

public class ChangeRingFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ViewPager f1823a;
    /* access modifiers changed from: private */
    public String[] b = {"", "系统自带"};
    private j[] c = new j[2];
    /* access modifiers changed from: private */
    public List<DDListFragment> d = new ArrayList();
    private net.lucode.hackware.magicindicator.b.a.a.a e = new net.lucode.hackware.magicindicator.b.a.a.a() {
        public int a() {
            return 2;
        }

        public d a(Context context, final int i) {
            if (ChangeRingFragment.this.d == null || ChangeRingFragment.this.d.size() <= 0) {
                return null;
            }
            net.lucode.hackware.magicindicator.b.a.d.a aVar = new net.lucode.hackware.magicindicator.b.a.d.a(context);
            aVar.setNormalColor(com.shoujiduoduo.ui.utils.j.a(R.color.text_black));
            aVar.setSelectedColor(com.shoujiduoduo.ui.utils.j.a(R.color.text_green));
            aVar.setText(ChangeRingFragment.this.b[i]);
            aVar.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    ChangeRingFragment.this.f1823a.setCurrentItem(i);
                }
            });
            return aVar;
        }

        public c a(Context context) {
            net.lucode.hackware.magicindicator.b.a.b.a aVar = new net.lucode.hackware.magicindicator.b.a.b.a(context);
            aVar.setMode(1);
            aVar.setColors(Integer.valueOf(com.shoujiduoduo.ui.utils.j.a(R.color.text_green)));
            return aVar;
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onCreateView");
        View inflate = layoutInflater.inflate((int) R.layout.fragment_change_ring, viewGroup, false);
        PlayerService.a(true);
        this.f1823a = (ViewPager) inflate.findViewById(R.id.vPager);
        this.f1823a.setOffscreenPageLimit(2);
        this.f1823a.setAdapter(new a(getActivity().getSupportFragmentManager()));
        MagicIndicator magicIndicator = (MagicIndicator) inflate.findViewById(R.id.magic_indicator);
        net.lucode.hackware.magicindicator.b.a.a aVar = new net.lucode.hackware.magicindicator.b.a.a(getContext());
        aVar.setAdapter(this.e);
        aVar.setAdjustMode(true);
        magicIndicator.setNavigator(aVar);
        magicIndicator.setBackgroundColor(com.shoujiduoduo.ui.utils.j.a(R.color.white));
        net.lucode.hackware.magicindicator.d.a(magicIndicator, this.f1823a);
        int i = getArguments().getInt("type", 0);
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "change type:" + i);
        switch (i) {
            case 0:
                this.b[0] = getResources().getString(R.string.hot_ringtone);
                this.c[0] = new j(ListType.LIST_TYPE.list_ring_normal, "1", false, "");
                this.c[1] = new j(ListType.LIST_TYPE.sys_ringtone);
                break;
            case 1:
                this.b[0] = getResources().getString(R.string.hot_notification);
                this.c[0] = new j(ListType.LIST_TYPE.list_ring_normal, "5", false, "");
                this.c[1] = new j(ListType.LIST_TYPE.sys_notify);
                break;
            case 2:
                this.b[0] = getResources().getString(R.string.hot_alarm);
                this.c[0] = new j(ListType.LIST_TYPE.list_ring_search, "闹钟", "input");
                this.c[1] = new j(ListType.LIST_TYPE.sys_alarm);
                break;
            case 3:
                this.b[0] = getResources().getString(R.string.hot_coloring);
                this.b[1] = getResources().getString(R.string.manage_coloring);
                if (!g.t()) {
                    if (!g.v()) {
                        if (g.u()) {
                            this.c[0] = new j(ListType.LIST_TYPE.list_ring_normal, "26", false, "");
                            this.c[1] = new j(ListType.LIST_TYPE.list_ring_cucc, "", false, "");
                            break;
                        }
                    } else {
                        this.c[0] = new j(ListType.LIST_TYPE.list_ring_normal, Constants.VIA_REPORT_TYPE_QQFAVORITES, false, "");
                        this.c[1] = new j(ListType.LIST_TYPE.list_ring_ctcc, "", false, "");
                        break;
                    }
                } else {
                    this.c[0] = new j(ListType.LIST_TYPE.list_ring_normal, "20", false, "");
                    this.c[1] = new j(ListType.LIST_TYPE.list_ring_cmcc, "", false, "");
                    break;
                }
                break;
        }
        DDListFragment dDListFragment = new DDListFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putString("adapter_type", "ring_list_adapter");
        dDListFragment.setArguments(bundle2);
        dDListFragment.a(this.c[0]);
        this.d.add(dDListFragment);
        if (i == 3) {
            DDListFragment dDListFragment2 = new DDListFragment();
            Bundle bundle3 = new Bundle();
            bundle3.putString("adapter_type", "cailing_list_adapter");
            bundle3.putBoolean("support_lazy_load", true);
            dDListFragment2.setArguments(bundle3);
            dDListFragment2.a(this.c[1]);
            this.d.add(dDListFragment2);
        } else {
            DDListFragment dDListFragment3 = new DDListFragment();
            Bundle bundle4 = new Bundle();
            bundle4.putString("adapter_type", "system_ring_list_adapter");
            bundle4.putBoolean("support_lazy_load", true);
            dDListFragment3.setArguments(bundle4);
            dDListFragment3.a(this.c[1]);
            this.d.add(dDListFragment3);
        }
        this.f1823a.setCurrentItem(0);
        this.f1823a.getAdapter().notifyDataSetChanged();
        this.e.b();
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onDestroyView");
        PlayerService.a(false);
        PlayerService b2 = ab.a().b();
        if (b2 != null && b2.l()) {
            b2.m();
        }
    }

    public void onDestroy() {
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onDestroy");
        super.onDestroy();
    }

    public void onDetach() {
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onDetach");
        super.onDetach();
    }

    public void onPause() {
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onPause");
        super.onPause();
    }

    public void onStop() {
        com.shoujiduoduo.base.a.a.a("ChangeRingFragment", "onStop");
        super.onStop();
    }

    private class a extends FragmentPagerAdapter {
        public a(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public CharSequence getPageTitle(int i) {
            return ChangeRingFragment.this.b[i];
        }

        public Fragment getItem(int i) {
            if (ChangeRingFragment.this.d != null && ChangeRingFragment.this.d.size() > 0) {
                return (Fragment) ChangeRingFragment.this.d.get(i % ChangeRingFragment.this.d.size());
            }
            com.shoujiduoduo.base.a.a.c("ChangeRingFragment", "return null fragment 2");
            return null;
        }

        public int getCount() {
            if (ChangeRingFragment.this.d == null || ChangeRingFragment.this.d.size() <= 0) {
                return 0;
            }
            return ChangeRingFragment.this.d.size();
        }
    }
}
