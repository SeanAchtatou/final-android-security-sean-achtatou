package com.shoujiduoduo.ui.mine;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.CollectData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.home.CollectRingActivity;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;
import java.util.List;

public class UserCollectFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ListView f1784a;
    /* access modifiers changed from: private */
    public e b;
    private View c;
    private Button d;
    /* access modifiers changed from: private */
    public Button e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    private LinearLayout i;
    private View.OnKeyListener j = new View.OnKeyListener() {
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (i != 4 || keyEvent.getAction() != 0 || !UserCollectFragment.this.g) {
                return false;
            }
            a.a("UserCollectFragment", "edit mode, key back");
            UserCollectFragment.this.a(false);
            return true;
        }
    };
    private View.OnClickListener k = new View.OnClickListener() {
        public void onClick(View view) {
            UserCollectFragment.this.a(false);
            int unused = UserCollectFragment.this.f = 0;
            UserCollectFragment.this.e.setText("删除");
        }
    };
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            final List<Integer> b = UserCollectFragment.this.b.b();
            if (b == null || b.size() == 0) {
                d.a("请选择要删除的精选集", 0);
            } else {
                new b.a(UserCollectFragment.this.getActivity()).b((int) R.string.hint).a("确定删除选中的精选集吗？").a((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (com.shoujiduoduo.a.b.b.b().a(b)) {
                            d.a("已删除" + b.size() + "个精选集", 0);
                            UserCollectFragment.this.a(false);
                        } else {
                            d.a("删除精选集失败", 0);
                        }
                        dialogInterface.dismiss();
                    }
                }).b((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).a().show();
            }
        }
    };
    private g m = new g() {
        public void a(DDList dDList, int i) {
            a.a("UserCollectFragment", "data update");
            if (dDList != null && dDList.getListId().equals("user_collect")) {
                UserCollectFragment.this.b.notifyDataSetChanged();
                UserCollectFragment.this.a();
            }
        }
    };
    private w n = new w() {
        public void a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.UserCollectFragment.a(com.shoujiduoduo.ui.mine.UserCollectFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.UserCollectFragment, int]
         candidates:
          com.shoujiduoduo.ui.mine.UserCollectFragment.a(com.shoujiduoduo.ui.mine.UserCollectFragment, int):int
          com.shoujiduoduo.ui.mine.UserCollectFragment.a(com.shoujiduoduo.ui.mine.UserCollectFragment, boolean):boolean */
        public void a(int i, List<RingData> list, String str) {
            if (str.equals("user_collect")) {
                a.a("UserCollectFragment", "data is ready");
                UserCollectFragment.this.a();
                UserCollectFragment.this.f1784a.setAdapter((ListAdapter) UserCollectFragment.this.b);
                boolean unused = UserCollectFragment.this.h = true;
            }
        }
    };
    private AdapterView.OnItemClickListener o = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (UserCollectFragment.this.g) {
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
                checkBox.toggle();
                UserCollectFragment.this.b.a().set(i, Boolean.valueOf(checkBox.isChecked()));
                if (checkBox.isChecked()) {
                    UserCollectFragment.f(UserCollectFragment.this);
                } else {
                    UserCollectFragment.g(UserCollectFragment.this);
                }
                if (UserCollectFragment.this.f > 0) {
                    UserCollectFragment.this.e.setText("删除(" + UserCollectFragment.this.f + ")");
                } else {
                    UserCollectFragment.this.e.setText("删除");
                }
            } else {
                RingDDApp.b().a("collectdata", (CollectData) com.shoujiduoduo.a.b.b.b().d().get(i));
                Intent intent = new Intent(UserCollectFragment.this.getActivity(), CollectRingActivity.class);
                intent.putExtra("parakey", "collectdata");
                UserCollectFragment.this.getActivity().startActivity(intent);
            }
        }
    };

    static /* synthetic */ int f(UserCollectFragment userCollectFragment) {
        int i2 = userCollectFragment.f;
        userCollectFragment.f = i2 + 1;
        return i2;
    }

    static /* synthetic */ int g(UserCollectFragment userCollectFragment) {
        int i2 = userCollectFragment.f;
        userCollectFragment.f = i2 - 1;
        return i2;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a.a("UserCollectFragment", "onCreate");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        a.a("UserCollectFragment", "onCreateView");
        View inflate = layoutInflater.inflate((int) R.layout.my_ringtone_collect, viewGroup, false);
        this.f1784a = (ListView) inflate.findViewById(R.id.my_ringtone_collect);
        this.f1784a.setOnItemClickListener(this.o);
        this.b = new e(getActivity(), com.shoujiduoduo.a.b.b.b().d());
        this.i = (LinearLayout) inflate.findViewById(R.id.no_data);
        if (com.shoujiduoduo.a.b.b.b().c()) {
            a.a("UserCollectFragment", "data is ready 1");
            this.f1784a.setAdapter((ListAdapter) this.b);
            a();
            this.h = true;
        } else {
            a.a("UserCollectFragment", "data is not ready");
        }
        this.c = (LinearLayout) inflate.findViewById(R.id.del_confirm);
        this.d = (Button) this.c.findViewById(R.id.cancel);
        this.d.setOnClickListener(this.k);
        this.e = (Button) this.c.findViewById(R.id.delete);
        this.e.setOnClickListener(this.l);
        this.c.setVisibility(4);
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.n);
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.m);
        return inflate;
    }

    public void onDestroyView() {
        this.h = false;
        this.g = false;
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.n);
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.m);
        super.onDestroyView();
        a.a("UserCollectFragment", "onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        a.a("UserCollectFragment", "onDestroy");
    }

    public void onResume() {
        a.a("UserCollectFragment", "onResume");
        super.onResume();
        getView().setFocusable(true);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(this.j);
    }

    public void a(boolean z) {
        if (this.g != z && this.h) {
            this.g = z;
            this.c.setVisibility(z ? 0 : 8);
            this.b.a(com.shoujiduoduo.a.b.b.b().d());
            this.b.a(z);
            if (z) {
                this.f = 0;
                this.e.setText("删除");
            }
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (com.shoujiduoduo.a.b.b.b().d().size() > 0) {
            this.i.setVisibility(8);
        } else {
            this.i.setVisibility(0);
        }
    }
}
