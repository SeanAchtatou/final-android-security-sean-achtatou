package com.shoujiduoduo.ui.adwall;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.HtmlFragment;
import com.shoujiduoduo.ui.utils.LazyFragment;
import com.shoujiduoduo.ui.utils.j;
import com.shoujiduoduo.util.ad;
import java.util.ArrayList;
import java.util.Iterator;
import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.b.a.a.c;
import net.lucode.hackware.magicindicator.b.a.a.d;

public class AdWallFrag extends LazyFragment {
    /* access modifiers changed from: private */
    public ViewPager b;
    /* access modifiers changed from: private */
    public ArrayList<Fragment> c = new ArrayList<>();
    private RelativeLayout d;
    private RelativeLayout e;
    private LinearLayout f;
    private MagicIndicator g;
    /* access modifiers changed from: private */
    public ArrayList<ad.c> h;
    private net.lucode.hackware.magicindicator.b.a.a.a i = new net.lucode.hackware.magicindicator.b.a.a.a() {
        public int a() {
            if (AdWallFrag.this.c == null || AdWallFrag.this.c.size() <= 0) {
                return 0;
            }
            return AdWallFrag.this.c.size();
        }

        public d a(Context context, final int i) {
            if (AdWallFrag.this.c == null || AdWallFrag.this.h == null || AdWallFrag.this.c.size() <= 0) {
                return null;
            }
            net.lucode.hackware.magicindicator.b.a.d.a aVar = new net.lucode.hackware.magicindicator.b.a.d.a(context);
            aVar.setNormalColor(j.a(R.color.text_black));
            aVar.setSelectedColor(j.a(R.color.text_green));
            aVar.setText(((ad.c) AdWallFrag.this.h.get(i)).f2186a);
            aVar.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    AdWallFrag.this.b.setCurrentItem(i);
                }
            });
            return aVar;
        }

        public c a(Context context) {
            net.lucode.hackware.magicindicator.b.a.b.a aVar = new net.lucode.hackware.magicindicator.b.a.b.a(context);
            aVar.setMode(1);
            aVar.setColors(Integer.valueOf(j.a(R.color.text_green)));
            return aVar;
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.wall_ad_view, viewGroup, false);
        this.b = (ViewPager) inflate.findViewById(R.id.ad_vPager);
        this.b.setOffscreenPageLimit(5);
        this.b.setAdapter(new a(getChildFragmentManager()));
        this.d = (RelativeLayout) inflate.findViewById(R.id.ad_failed_view);
        this.e = (RelativeLayout) inflate.findViewById(R.id.ad_loading_view);
        this.f = (LinearLayout) inflate.findViewById(R.id.ad_home_lists);
        this.g = (MagicIndicator) inflate.findViewById(R.id.magic_indicator);
        net.lucode.hackware.magicindicator.b.a.a aVar = new net.lucode.hackware.magicindicator.b.a.a(getActivity());
        aVar.setAdapter(this.i);
        aVar.setAdjustMode(true);
        this.g.setNavigator(aVar);
        this.g.setBackgroundColor(j.a(R.color.white));
        net.lucode.hackware.magicindicator.d.a(this.g, this.b);
        this.h = ad.a().d();
        c();
        b();
        this.i.b();
        return inflate;
    }

    private class a extends FragmentPagerAdapter {
        public CharSequence getPageTitle(int i) {
            if (AdWallFrag.this.h == null || AdWallFrag.this.h.size() <= 0) {
                return "";
            }
            return ((ad.c) AdWallFrag.this.h.get(i)).f2186a;
        }

        public a(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int i) {
            if (AdWallFrag.this.c != null && AdWallFrag.this.c.size() > 0) {
                return (Fragment) AdWallFrag.this.c.get(i % AdWallFrag.this.c.size());
            }
            com.shoujiduoduo.base.a.a.c("AdWallFrag", "return null fragment 2");
            return null;
        }

        public int getCount() {
            if (AdWallFrag.this.c == null || AdWallFrag.this.c.size() <= 0) {
                return 0;
            }
            return AdWallFrag.this.c.size();
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    private void b() {
        this.h.add(new ad.c("美女", "https://cpu.baidu.com/1034/c5228eac"));
        this.h.add(new ad.c("搞笑", "https://cpu.baidu.com/1025/c9e0f226"));
        Iterator<ad.c> it = this.h.iterator();
        while (it.hasNext()) {
            HtmlFragment htmlFragment = new HtmlFragment();
            Bundle bundle = new Bundle();
            bundle.putString("url", it.next().b);
            htmlFragment.setArguments(bundle);
            this.c.add(htmlFragment);
        }
        if (this.h.size() == 1) {
            this.g.setVisibility(8);
        }
        this.b.getAdapter().notifyDataSetChanged();
        this.b.setCurrentItem(0);
    }

    private void c() {
        this.d.setVisibility(4);
        this.e.setVisibility(4);
        this.f.setVisibility(0);
    }
}
