package com.shoujiduoduo.ui.aichang;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cn.banshenggua.aichang.entry.RoomsEntryFragment;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.LazyFragment;

public class AiChangFrag extends LazyFragment {
    private boolean b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.b = false;
        return layoutInflater.inflate((int) R.layout.ac_view_layout, viewGroup, false);
    }

    public void onDestroyView() {
        this.b = false;
        super.onDestroyView();
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (!this.b) {
            this.b = true;
            a.a("AiChangFrag", "lazyLoad");
            RoomsEntryFragment newInstance = RoomsEntryFragment.newInstance();
            FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
            beginTransaction.replace(R.id.aichang_layout, newInstance);
            beginTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            beginTransaction.commitAllowingStateLoss();
            newInstance.getFragmentManager().executePendingTransactions();
        }
    }
}
