package com.shoujiduoduo.ui.settings;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.igexin.download.Downloads;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.CropImageView;
import com.shoujiduoduo.util.i;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.File;

public class CropImageActivity extends Activity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    Uri f1912a;
    Uri b;
    int c;
    int d;
    boolean e;
    int f;
    int g;
    String h;
    CropImageView i;
    RelativeLayout j;
    View k;
    View l;
    LinearLayout m;
    ImageButton n;
    ImageButton o;
    ImageButton p;
    ImageButton q;

    private void a(Intent intent) {
        if (intent != null) {
            this.f1912a = intent.getData();
            this.c = intent.getIntExtra("outputX", (int) (((float) this.f) * 0.8f));
            this.d = intent.getIntExtra("outputY", (int) (((float) this.g) * 0.8f));
            this.e = intent.getBooleanExtra("rotateEnable", false);
            this.h = intent.getStringExtra("save_path");
            a.a("CropImageActivity", "outWidth=" + this.c + " , outHeight=" + this.d);
            a(this.f1912a);
        }
    }

    /* JADX INFO: finally extract failed */
    private void a(Uri uri) {
        Cursor cursor;
        String str;
        Bitmap bitmap = null;
        if (uri != null) {
            int i2 = (int) (((double) this.f) * 1.1d);
            int i3 = (int) (((double) this.g) * 1.1d);
            if ("content".equalsIgnoreCase(uri.getScheme())) {
                ContentResolver contentResolver = getContentResolver();
                if (contentResolver != null) {
                    try {
                        cursor = contentResolver.query(uri, new String[]{Downloads._DATA}, null, null, null);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        cursor = null;
                    }
                    if (cursor != null) {
                        try {
                            int columnIndexOrThrow = cursor.getColumnIndexOrThrow(Downloads._DATA);
                            cursor.moveToFirst();
                            str = cursor.getString(columnIndexOrThrow);
                            cursor.close();
                        } catch (Exception e3) {
                            e3.printStackTrace();
                            cursor.close();
                            str = null;
                        } catch (Throwable th) {
                            cursor.close();
                            throw th;
                        }
                        bitmap = a(str, i2, i3);
                        if (bitmap == null) {
                            bitmap = a(str, this.c, this.d);
                        }
                    }
                }
            } else {
                bitmap = a(uri.getPath(), i2, i3);
                if (bitmap == null) {
                    bitmap = a(uri.getPath(), this.c, this.d);
                }
            }
            if (bitmap != null) {
                a.c("CropImageActivity", bitmap.getWidth() + " * " + bitmap.getHeight() + " size===" + ((((float) (bitmap.getRowBytes() * bitmap.getHeight())) / 1024.0f) / 1024.0f) + "MB");
                this.i.a(this.c, this.d, bitmap.getWidth(), bitmap.getHeight());
                this.i.setImageBitmap(bitmap);
                return;
            }
            this.k.setEnabled(false);
            this.i.setErrorHint(getResources().getString(R.string.skin_error_hint));
        }
    }

    /* access modifiers changed from: private */
    public Uri a() {
        File a2;
        byte[] cropImage = this.i.getCropImage();
        if (cropImage == null || (a2 = a(cropImage, this.h)) == null || !a2.exists()) {
            return null;
        }
        a.a("CropImageActivity", "saveCropBitmap-->" + this.h);
        return Uri.fromFile(a2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0026 A[SYNTHETIC, Splitter:B:18:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0032 A[SYNTHETIC, Splitter:B:24:0x0032] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.File a(byte[] r5, java.lang.String r6) {
        /*
            r4 = this;
            r2 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x001e }
            r0.<init>(r6)     // Catch:{ Exception -> 0x001e }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x003e }
            r1.<init>(r0)     // Catch:{ Exception -> 0x003e }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x003e }
            r3.<init>(r1)     // Catch:{ Exception -> 0x003e }
            r3.write(r5)     // Catch:{ Exception -> 0x0040, all -> 0x003b }
            if (r3 == 0) goto L_0x0018
            r3.close()     // Catch:{ IOException -> 0x0019 }
        L_0x0018:
            return r0
        L_0x0019:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0018
        L_0x001e:
            r0 = move-exception
            r1 = r0
            r0 = r2
        L_0x0021:
            r1.printStackTrace()     // Catch:{ all -> 0x002f }
            if (r2 == 0) goto L_0x0018
            r2.close()     // Catch:{ IOException -> 0x002a }
            goto L_0x0018
        L_0x002a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0018
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ IOException -> 0x0036 }
        L_0x0035:
            throw r0
        L_0x0036:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0035
        L_0x003b:
            r0 = move-exception
            r2 = r3
            goto L_0x0030
        L_0x003e:
            r1 = move-exception
            goto L_0x0021
        L_0x0040:
            r1 = move-exception
            r2 = r3
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.settings.CropImageActivity.a(byte[], java.lang.String):java.io.File");
    }

    public Bitmap a(String str, int i2, int i3) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            options.inSampleSize = a(options, i2, i3);
            if (options.outWidth > options.outHeight) {
                options.inSampleSize = a(options, i3, i2);
            }
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(str, options);
        } catch (Exception e2) {
            System.gc();
            return null;
        }
    }

    public int a(BitmapFactory.Options options, int i2, int i3) {
        int i4 = options.outHeight;
        int i5 = options.outWidth;
        int i6 = 1;
        if (i4 > i3 || i5 > i2) {
            if (i5 > i4) {
                i6 = Math.round(((float) i4) / ((float) i3));
            } else {
                i6 = Math.round(((float) i5) / ((float) i2));
            }
        }
        a.a("CropImageActivity", i5 + MiPushClient.ACCEPT_TIME_SEPARATOR + i4 + "--->" + i2 + MiPushClient.ACCEPT_TIME_SEPARATOR + i3 + " calculateInSampleSize-->" + i6);
        return i6;
    }

    private void b() {
        this.i = (CropImageView) findViewById(R.id.cropimg_imageview);
        this.j = (RelativeLayout) findViewById(R.id.cropimg_progresslayout);
        this.m = (LinearLayout) findViewById(R.id.croping_rotatelayout);
        this.k = findViewById(R.id.cropimg_okbtn);
        this.l = findViewById(R.id.cropimg_cancelbtn);
        this.k.setOnClickListener(this);
        this.l.setOnClickListener(this);
        this.n = (ImageButton) findViewById(R.id.cropimg_zoomout);
        this.o = (ImageButton) findViewById(R.id.cropimg_zoomin);
        this.p = (ImageButton) findViewById(R.id.cropimg_rotateleft);
        this.q = (ImageButton) findViewById(R.id.cropimg_rotateright);
        this.n.setOnClickListener(this);
        this.o.setOnClickListener(this);
        this.p.setOnClickListener(this);
        this.q.setOnClickListener(this);
        try {
            this.k.setBackgroundResource(R.drawable.xml_btn_cropimg_save);
            this.l.setBackgroundResource(R.drawable.xml_btn_cropimg_cancel);
            this.n.setImageResource(R.drawable.btn_crop_zoomout);
            this.o.setImageResource(R.drawable.btn_crop_zoomin);
            this.p.setImageResource(R.drawable.btn_crop_rotate_left);
            this.q.setImageResource(R.drawable.btn_crop_rotate_right);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        if (Build.VERSION.SDK_INT < 11) {
            getWindow().setFlags(1024, 1024);
        }
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_cropimage);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.f = displayMetrics.widthPixels;
        this.g = displayMetrics.heightPixels;
        b();
        a(getIntent());
        if (Build.VERSION.SDK_INT >= 11) {
            this.i.setSystemUiVisibility(4);
        }
        if (this.e) {
            this.m.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.k.setEnabled(true);
        this.j.setVisibility(8);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        a(intent);
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    @TargetApi(11)
    public void onDestroy() {
        this.i.setImageBitmap(null);
        if (Build.VERSION.SDK_INT >= 11) {
            this.i.setSystemUiVisibility(0);
        }
        super.onDestroy();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cropimg_okbtn:
                this.k.setEnabled(false);
                c();
                return;
            case R.id.cropimg_cancelbtn:
                setResult(0);
                finish();
                return;
            case R.id.croping_rotatelayout:
            default:
                return;
            case R.id.cropimg_zoomout:
                b(true);
                return;
            case R.id.cropimg_zoomin:
                b(false);
                return;
            case R.id.cropimg_rotateleft:
                a(true);
                return;
            case R.id.cropimg_rotateright:
                a(false);
                return;
        }
    }

    private void a(boolean z) {
        this.i.setImageRotate(z);
    }

    private void b(boolean z) {
        this.i.setImageZoom(z);
    }

    private void c() {
        this.j.setVisibility(0);
        i.a(new Runnable() {
            public void run() {
                CropImageActivity.this.b = CropImageActivity.this.a();
                c.a().a(new c.b() {
                    public void a() {
                        if (CropImageActivity.this.b != null) {
                            Intent intent = new Intent();
                            intent.setData(CropImageActivity.this.b);
                            CropImageActivity.this.setResult(-1, intent);
                        } else {
                            Toast.makeText(CropImageActivity.this, "背景图片保存时出错！", 0).show();
                            CropImageActivity.this.setResult(0);
                        }
                        CropImageActivity.this.finish();
                    }
                });
            }
        });
    }
}
