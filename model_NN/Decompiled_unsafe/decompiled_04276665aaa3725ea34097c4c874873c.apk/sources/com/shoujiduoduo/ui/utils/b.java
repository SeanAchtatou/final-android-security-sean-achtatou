package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.shoujiduoduo.base.bean.ArtistData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.l;
import java.text.DecimalFormat;

/* compiled from: ArtistListAdapter */
public class b extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public com.shoujiduoduo.b.c.a f2055a;
    private Context b;
    private Handler d = new Handler();

    public b(Context context) {
        this.b = context;
    }

    public int getCount() {
        if (this.f2055a == null) {
            return 0;
        }
        if (this.c == d.a.LIST_CONTENT) {
            return this.f2055a.size();
        }
        return 1;
    }

    public Object getItem(int i) {
        if (this.f2055a != null) {
            return this.f2055a.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* compiled from: ArtistListAdapter */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        ImageView f2058a;
        TextView b;
        TextView c;
        TextView d;
        TextView e;

        private a() {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        a aVar;
        if (this.f2055a == null) {
            com.shoujiduoduo.base.a.a.a("ArtistListAdapter", "return null");
            return null;
        }
        switch (getItemViewType(i)) {
            case 0:
                if (view == null) {
                    view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_artist, viewGroup, false);
                    a aVar2 = new a();
                    aVar2.f2058a = (ImageView) view.findViewById(R.id.pic);
                    aVar2.b = (TextView) view.findViewById(R.id.title);
                    aVar2.c = (TextView) view.findViewById(R.id.content);
                    aVar2.d = (TextView) view.findViewById(R.id.sale);
                    aVar2.e = (TextView) view.findViewById(R.id.sn);
                    if (g.l()) {
                        aVar2.c.setLines(1);
                        aVar2.e.setTextSize(2, 12.0f);
                    }
                    view.setTag(aVar2);
                    aVar = aVar2;
                } else {
                    aVar = (a) view.getTag();
                }
                ArtistData a2 = this.f2055a.get(i);
                com.d.a.b.d.a().a(a2.pic, aVar.f2058a, h.a().g());
                aVar.b.setText(a2.name);
                aVar.c.setText(a2.work);
                aVar.e.setText("" + (i + 1));
                int i2 = a2.sale;
                StringBuilder sb = new StringBuilder();
                sb.append("彩铃销售:");
                if (i2 > 10000) {
                    sb.append(new DecimalFormat("#.00").format((double) (((float) i2) / 10000.0f)));
                    sb.append("万");
                } else {
                    sb.append(i2);
                }
                aVar.d.setText(sb.toString());
                return view;
            case 1:
            default:
                return view;
            case 2:
                View inflate = LayoutInflater.from(this.b).inflate((int) R.layout.list_loading, viewGroup, false);
                if (l.b > 0) {
                    AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams) inflate.getLayoutParams();
                    layoutParams.width = l.b;
                    layoutParams.height = l.b;
                    inflate.setLayoutParams(layoutParams);
                }
                final AnimationDrawable animationDrawable = (AnimationDrawable) ((ImageView) inflate.findViewById(R.id.loading)).getBackground();
                this.d.post(new Runnable() {
                    public void run() {
                        animationDrawable.start();
                    }
                });
                return inflate;
            case 3:
                View inflate2 = LayoutInflater.from(this.b).inflate((int) R.layout.list_failed, viewGroup, false);
                if (l.b > 0) {
                    AbsListView.LayoutParams layoutParams2 = (AbsListView.LayoutParams) inflate2.getLayoutParams();
                    layoutParams2.width = l.b;
                    layoutParams2.height = l.b;
                    inflate2.setLayoutParams(layoutParams2);
                }
                inflate2.findViewById(R.id.network_fail_layout).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        b.this.a(d.a.LIST_LOADING);
                        b.this.f2055a.retrieveData();
                    }
                });
                return inflate2;
        }
    }

    public void a(boolean z) {
    }

    public void a(DDList dDList) {
        this.f2055a = (com.shoujiduoduo.b.c.a) dDList;
    }

    public void a() {
    }

    public void b() {
    }
}
