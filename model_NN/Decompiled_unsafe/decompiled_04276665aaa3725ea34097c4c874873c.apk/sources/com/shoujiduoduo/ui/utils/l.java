package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.a.a.e;
import com.a.a.p;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.b.c.n;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.HttpJsonRes;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.u;

/* compiled from: UserListAdapter */
public class l extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public n f2141a;
    private Context b;
    private String d;
    private boolean e;
    /* access modifiers changed from: private */
    public boolean f;
    private Handler g = new Handler();
    private n.a h;

    public l(Context context) {
        this.b = context;
    }

    public void a(String str) {
        this.d = str;
        this.e = this.d != null && this.d.equals(b.g().f());
    }

    public void a(boolean z) {
    }

    public void a(DDList dDList) {
        this.f2141a = (n) dDList;
        this.h = this.f2141a.a();
    }

    public void a() {
    }

    public void b() {
    }

    public int getCount() {
        if (this.f2141a == null) {
            return 0;
        }
        if (this.c == d.a.LIST_CONTENT) {
            return this.f2141a.size();
        }
        return 1;
    }

    public Object getItem(int i) {
        if (this.f2141a != null) {
            return this.f2141a.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        switch (getItemViewType(i)) {
            case 0:
                if (view == null) {
                    view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_fans_follow, viewGroup, false);
                }
                TextView textView = (TextView) m.a(view, R.id.user_des);
                Button button = (Button) m.a(view, R.id.btn_follow);
                UserData userData = (UserData) this.f2141a.get(i);
                com.d.a.b.d.a().a(userData.headUrl, (ImageView) m.a(view, R.id.user_head), h.a().e());
                ((TextView) m.a(view, R.id.user_name)).setText(userData.userName);
                textView.setText(userData.intro);
                if (ai.c(userData.intro)) {
                    textView.setVisibility(8);
                }
                final String str = userData.uid;
                if (this.h == n.a.fans) {
                    if (b.g().i().contains(userData.uid)) {
                        button.setText("取消关注");
                    } else {
                        button.setText("关注");
                    }
                } else if (this.e) {
                    button.setText("取消关注");
                } else if (b.g().i().contains(userData.uid)) {
                    button.setText("取消关注");
                } else {
                    button.setText("关注");
                }
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(final View view) {
                        String charSequence = ((Button) view).getText().toString();
                        UserInfo c = b.g().c();
                        if (!l.this.f) {
                            boolean unused = l.this.f = true;
                            if ("关注".equals(charSequence)) {
                                u.a("follow", "&uid=" + c.getUid() + "&tuid=" + str, new u.a() {
                                    public void a(String str) {
                                        try {
                                            HttpJsonRes httpJsonRes = (HttpJsonRes) new e().a(str, HttpJsonRes.class);
                                            if (httpJsonRes.getResult().equals("success")) {
                                                ((Button) view).setText("取消关注");
                                                com.shoujiduoduo.util.widget.d.a("关注成功");
                                                b.g().b(str);
                                            } else {
                                                com.shoujiduoduo.util.widget.d.a(httpJsonRes.getMsg());
                                            }
                                        } catch (p e) {
                                            e.printStackTrace();
                                        }
                                        boolean unused = l.this.f = false;
                                    }

                                    public void a(String str, String str2) {
                                        com.shoujiduoduo.util.widget.d.a("关注失败");
                                        boolean unused = l.this.f = false;
                                    }
                                });
                            } else {
                                u.a("unfollow", "&uid=" + c.getUid() + "&tuid=" + str, new u.a() {
                                    public void a(String str) {
                                        ((Button) view).setText("关注");
                                        com.shoujiduoduo.util.widget.d.a("取消关注成功");
                                        boolean unused = l.this.f = false;
                                        b.g().a(str);
                                    }

                                    public void a(String str, String str2) {
                                        com.shoujiduoduo.util.widget.d.a("取消失败");
                                        boolean unused = l.this.f = false;
                                    }
                                });
                            }
                        }
                    }
                });
                break;
            case 2:
                view = LayoutInflater.from(this.b).inflate((int) R.layout.list_loading, viewGroup, false);
                if (com.shoujiduoduo.util.l.b > 0) {
                    AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams) view.getLayoutParams();
                    layoutParams.width = com.shoujiduoduo.util.l.b;
                    layoutParams.height = com.shoujiduoduo.util.l.b;
                    view.setLayoutParams(layoutParams);
                }
                final AnimationDrawable animationDrawable = (AnimationDrawable) ((ImageView) view.findViewById(R.id.loading)).getBackground();
                this.g.post(new Runnable() {
                    public void run() {
                        animationDrawable.start();
                    }
                });
                break;
            case 3:
                view = LayoutInflater.from(this.b).inflate((int) R.layout.list_failed, viewGroup, false);
                if (com.shoujiduoduo.util.l.b > 0) {
                    AbsListView.LayoutParams layoutParams2 = (AbsListView.LayoutParams) view.getLayoutParams();
                    layoutParams2.width = com.shoujiduoduo.util.l.b;
                    layoutParams2.height = com.shoujiduoduo.util.l.b;
                    view.setLayoutParams(layoutParams2);
                }
                view.findViewById(R.id.network_fail_layout).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        l.this.a(d.a.LIST_LOADING);
                        l.this.f2141a.retrieveData();
                    }
                });
                break;
        }
        return view;
    }
}
