package com.shoujiduoduo.ui.utils;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import cn.banshenggua.aichang.utils.PreferencesUtils;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.u;

public class TestBtnBkg extends Activity implements View.OnClickListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.test_btn_bkg);
        findViewById(R.id.get_feeds).setOnClickListener(this);
        findViewById(R.id.my_concern).setOnClickListener(this);
    }

    public void onClick(View view) {
        UserInfo c = b.g().c();
        switch (view.getId()) {
            case R.id.my_concern:
                a.a("TestBtnBkg", "method:my_concern");
                u.a("concerned", "&uid=" + c.getUid() + "&te=" + "" + "&tb=" + "" + "&direction=" + PreferencesUtils.FORWARD + "&pagesize=25", new u.a() {
                    public void a(String str) {
                        a.a("TestBtnBkg", "success, res:" + str);
                    }

                    public void a(String str, String str2) {
                        a.a("TestBtnBkg", "fail");
                    }
                });
                return;
            case R.id.get_feeds:
                a.a("TestBtnBkg", "method:get_feeds");
                u.a("getfeeds", "&uid=" + c.getUid() + "&te=" + "" + "&tb=" + "" + "&direction=" + PreferencesUtils.FORWARD + "&pagesize=25", new u.a() {
                    public void a(String str) {
                        a.a("TestBtnBkg", "success, res:" + str);
                    }

                    public void a(String str, String str2) {
                        a.a("TestBtnBkg", "fail");
                    }
                });
                return;
            default:
                return;
        }
    }
}
