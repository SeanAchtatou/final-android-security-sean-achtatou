package com.shoujiduoduo.ui.utils;

import android.app.Activity;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.umeng.a.b;

public class BaseActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        b.b(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        b.a(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        RingDDApp.b().a("activity_on_stop_time", Long.valueOf(System.currentTimeMillis()));
    }
}
