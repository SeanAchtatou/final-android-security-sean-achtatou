package com.shoujiduoduo.ui.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.b.a.e;
import com.shoujiduoduo.b.a.g;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.ui.cailing.c;
import com.shoujiduoduo.ui.cailing.d;
import com.shoujiduoduo.ui.cailing.e;
import com.shoujiduoduo.ui.cailing.f;
import com.shoujiduoduo.ui.home.MusicAlbumActivity;
import com.shoujiduoduo.ui.mine.UserMainPageActivity;
import com.shoujiduoduo.ui.user.CommentActivity;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.al;
import com.shoujiduoduo.util.am;
import com.shoujiduoduo.util.an;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.widget.CircleProgressBar;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: RingListAdapter */
public class k extends d {
    private View.OnClickListener A = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "RingtoneDuoduo: click weixiu button!");
            Intent intent = new Intent(RingDDApp.c(), MusicAlbumActivity.class);
            intent.putExtra("musicid", ((RingData) k.this.f2090a.get(k.this.b)).rid);
            intent.putExtra("title", "快秀");
            intent.putExtra("type", MusicAlbumActivity.a.ring_story);
            RingToneDuoduoActivity.a().startActivity(intent);
        }
    };
    private View.OnClickListener B = new View.OnClickListener() {
        public void onClick(View view) {
            RingData ringData;
            if (k.this.b >= 0 && (ringData = (RingData) k.this.f2090a.get(k.this.b)) != null && !ai.c(ringData.uid)) {
                Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                intent.putExtra("tuid", ringData.uid);
                k.this.g.startActivity(intent);
                am.a(ringData.rid, 14, "&from=" + k.this.f2090a.getListId() + "&listType=" + k.this.f2090a.getListType() + "&uid=" + b.g().c().getUid() + "&tuid=" + ringData.uid);
            }
        }
    };
    private View.OnClickListener C = new View.OnClickListener() {
        public void onClick(View view) {
            RingData ringData = (RingData) k.this.f2090a.get(k.this.b);
            Intent intent = new Intent(RingDDApp.c(), CommentActivity.class);
            intent.putExtra("tuid", ringData.uid);
            intent.putExtra(SelectCountryActivity.EXTRA_COUNTRY_NAME, ringData.name);
            intent.putExtra("rid", ringData.rid);
            RingDDApp.b().a("current_list", k.this.f2090a);
            k.this.g.startActivity(intent);
            am.a(ringData.rid, 15, "&from=" + k.this.f2090a.getListId() + "&listType=" + k.this.f2090a.getListType() + "&uid=" + b.g().c().getUid() + "&tuid=" + ringData.uid);
        }
    };
    private View.OnClickListener D = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "RingtoneDuoduo: CategoryScene: click apply button!");
            RingData ringData = (RingData) k.this.f2090a.get(k.this.b);
            if (ringData != null) {
                b.b().a(ringData, "favorite_ring_list");
                new com.shoujiduoduo.ui.settings.b(k.this.g, R.style.DuoDuoDialog, ringData, k.this.f2090a.getListId(), k.this.f2090a.getListType().toString()).show();
            }
        }
    };
    private View.OnClickListener E = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "RingtoneDuoduo: click collect button!");
            RingData ringData = (RingData) k.this.f2090a.get(k.this.b);
            if (ringData != null) {
                b.b().a(ringData, "favorite_ring_list");
                d.a((int) R.string.add_favorite_suc, 0);
                am.a(ringData.rid, 0, "&from=" + k.this.f2090a.getListId() + "&listType=" + k.this.f2090a.getListType() + "&cucid=" + ringData.cucid + "&uid=" + b.g().c().getUid() + "&tuid=" + ringData.uid);
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public DDList f2090a;
    /* access modifiers changed from: private */
    public int b = -1;
    private String d;
    private LayoutInflater e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public Context g;
    private boolean h;
    private int i;
    private int j;
    /* access modifiers changed from: private */
    public boolean k;
    private Map<Integer, e.a> l;
    /* access modifiers changed from: private */
    public RingData m;
    private Timer n;
    private a o;
    private Handler p;
    private com.shoujiduoduo.a.c.k q = new com.shoujiduoduo.a.c.k() {
        public void a() {
            k.this.notifyDataSetChanged();
        }
    };
    private o r = new o() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.k, int]
         candidates:
          com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, int):int
          com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b):android.view.View
          com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData):com.shoujiduoduo.base.bean.RingData
          com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
          com.shoujiduoduo.ui.utils.k.a(java.lang.String, com.shoujiduoduo.base.bean.RingData):void
          com.shoujiduoduo.ui.utils.k.a(boolean, com.shoujiduoduo.util.g$b):void
          com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.k, int]
         candidates:
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData):void
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, boolean):boolean */
        public void a(String str, int i) {
            if (k.this.f2090a != null && k.this.f2090a.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "onSetPlay, listid:" + str);
                boolean unused = k.this.f = true;
                int unused2 = k.this.b = i;
                boolean unused3 = k.this.k = true;
                k.this.notifyDataSetChanged();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.k, int]
         candidates:
          com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, int):int
          com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b):android.view.View
          com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData):com.shoujiduoduo.base.bean.RingData
          com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
          com.shoujiduoduo.ui.utils.k.a(java.lang.String, com.shoujiduoduo.base.bean.RingData):void
          com.shoujiduoduo.ui.utils.k.a(boolean, com.shoujiduoduo.util.g$b):void
          com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.k, int]
         candidates:
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData):void
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, boolean):boolean */
        public void b(String str, int i) {
            if (k.this.f2090a != null && k.this.f2090a.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "onCanclePlay, listId:" + str);
                boolean unused = k.this.f = false;
                int unused2 = k.this.b = i;
                boolean unused3 = k.this.k = true;
                k.this.notifyDataSetChanged();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.k, int]
         candidates:
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData):void
          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, boolean):boolean */
        public void a(String str, int i, int i2) {
            if (k.this.f2090a != null && k.this.f2090a.getListId().equals(str)) {
                boolean unused = k.this.k = true;
                k.this.notifyDataSetChanged();
            }
        }
    };
    private View.OnClickListener s = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = ab.a().b();
            if (b != null) {
                if (b.a() == 3) {
                    b.n();
                } else {
                    b.i();
                }
            }
        }
    };
    private View.OnClickListener t = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = ab.a().b();
            if (b != null) {
                b.j();
            }
        }
    };
    private View.OnClickListener u = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = ab.a().b();
            if (b != null) {
                b.a(k.this.f2090a, k.this.b);
            }
        }
    };
    private View.OnClickListener v = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "RingtoneDuoduo: click Cailing button!");
            RingData ringData = (RingData) k.this.f2090a.get(k.this.b);
            RingData unused = k.this.m = ringData;
            switch (g.s()) {
                case f2309a:
                    k.this.b(ringData);
                    return;
                case cu:
                    k.this.d(ringData);
                    return;
                case ct:
                    if (ringData.ctVip == 1 || ringData.ctVip == 2) {
                        k.this.e(ringData);
                        return;
                    } else {
                        c.a(k.this.g).a(view, ringData, k.this.f2090a.getListId(), k.this.f2090a.getListType());
                        return;
                    }
                default:
                    d.a("不支持的运营商类型!");
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Handler w = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "init chinamobile sdk");
            k.this.d();
            com.shoujiduoduo.util.c.b.a().c(new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc sdk init succeed");
                    k.this.c(k.this.m);
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    k.this.f();
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc sdk init failed, try sms code init");
                    k.this.b(k.this.m, "", g.b.f2309a);
                }
            });
        }
    };
    private ProgressDialog x = null;
    private ProgressDialog y = null;
    private View.OnClickListener z = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "RingtoneDuoduo: click share button!");
            ak.a().a((Activity) k.this.g, (RingData) k.this.f2090a.get(k.this.b), k.this.f2090a.getListId());
        }
    };

    public k(Context context) {
        this.g = context;
        this.e = LayoutInflater.from(this.g);
        this.l = new HashMap();
        this.n = new Timer();
        this.o = new a();
        this.p = new Handler();
    }

    public void a() {
        this.i = s.a(com.umeng.b.a.a().a(RingDDApp.c(), "feed_ad_gap"), 10);
        this.j = s.a(com.umeng.b.a.a().a(RingDDApp.c(), "feed_ad_start_pos"), 6);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.r);
        if (this.h) {
            com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, this.q);
        }
        this.n.schedule(this.o, 0, 250);
    }

    /* compiled from: RingListAdapter */
    private class a extends TimerTask {
        /* access modifiers changed from: private */
        public CircleProgressBar b;

        private a() {
        }

        public void a(CircleProgressBar circleProgressBar) {
            this.b = circleProgressBar;
        }

        public void run() {
            com.shoujiduoduo.a.a.c.a().a(new c.b() {
                public void a() {
                    if (a.this.b != null && k.this.f) {
                        a.this.b.setMax(100);
                        PlayerService b = ab.a().b();
                        if (b != null) {
                            switch (b.a()) {
                                case 1:
                                case 3:
                                case 6:
                                default:
                                    return;
                                case 2:
                                    int g = b.g();
                                    int h = b.h();
                                    if (g > 0) {
                                        a.this.b.setProgress((int) ((100.0d * ((double) h)) / ((double) g)));
                                        return;
                                    }
                                    return;
                                case 4:
                                    a.this.b.setProgress(100);
                                    return;
                                case 5:
                                    a.this.b.setProgress(0);
                                    return;
                            }
                        }
                    }
                }
            });
        }
    }

    public void b() {
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.r);
        if (this.h) {
            com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, this.q);
        }
        if (this.n != null) {
            this.n.cancel();
        }
    }

    public void a(boolean z2) {
        this.h = z2;
    }

    public void a(String str) {
        this.d = str;
    }

    public void a(DDList dDList) {
        if (this.f2090a != dDList) {
            this.f2090a = null;
            this.f2090a = dDList;
            this.f = false;
            this.k = false;
            notifyDataSetChanged();
        }
    }

    public int getItemViewType(int i2) {
        switch (this.c) {
            case LIST_CONTENT:
                if (!this.h || i2 + 1 < this.j || ((i2 + 1) - this.j) % this.i != 0) {
                    return 0;
                }
                return 1;
            case LIST_LOADING:
                return 2;
            case LIST_FAILED:
                return 3;
            default:
                return -1;
        }
    }

    public int getViewTypeCount() {
        if (this.h) {
            return 4;
        }
        return 3;
    }

    public int getCount() {
        if (this.f2090a == null) {
            return 0;
        }
        if (this.c != d.a.LIST_CONTENT) {
            return 1;
        }
        if (!this.h) {
            return this.f2090a.size();
        }
        if (this.f2090a.size() > this.j) {
            return this.f2090a.size() + ((this.f2090a.size() - this.j) / (this.i - 1));
        }
        return this.f2090a.size();
    }

    public Object getItem(int i2) {
        if (this.f2090a == null || i2 < 0 || i2 >= this.f2090a.size()) {
            return null;
        }
        return this.f2090a.get(i2);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private boolean a(RingData ringData) {
        if (!ringData.cid.equals("") && g.t()) {
            return true;
        }
        if (g.v()) {
            b.c b2 = com.shoujiduoduo.util.d.b.a().b(com.shoujiduoduo.a.b.b.g().c().getPhoneNum());
            boolean z2 = b2 == null || !b2.f2288a || b2.b;
            if (!ringData.ctcid.equals("")) {
                return true;
            }
            if (ringData.ctVip != 2 || !z2) {
                return false;
            }
            return true;
        } else if (!g.u() || !com.shoujiduoduo.util.e.a.a().b()) {
            return false;
        } else {
            if (ringData.cuvip != 2) {
                return false;
            }
            return true;
        }
    }

    private void a(View view, int i2, boolean z2) {
        String str;
        RingData ringData = (RingData) this.f2090a.get(i2);
        TextView textView = (TextView) m.a(view, R.id.item_song_name);
        TextView textView2 = (TextView) m.a(view, R.id.item_artist);
        TextView textView3 = (TextView) m.a(view, R.id.tv_duradion);
        TextView textView4 = (TextView) m.a(view, R.id.tv_play_times);
        ImageView imageView = (ImageView) m.a(view, R.id.iv_cailing);
        imageView.setVisibility(8);
        ImageView imageView2 = (ImageView) m.a(view, R.id.iv_new);
        imageView2.setVisibility(8);
        String str2 = ringData.name;
        if (a(ringData)) {
            imageView.setVisibility(0);
        }
        if (z2) {
            TextView textView5 = (TextView) m.a(view, R.id.tv_upload_time);
            try {
                Date parse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(ringData.date);
                textView5.setText(g.b(parse));
                ((TextView) m.a(view, R.id.upload_time)).setText(g.b(parse));
            } catch (ParseException e2) {
                e2.printStackTrace();
            }
        }
        ImageView imageView3 = (ImageView) m.a(view, R.id.iv_comment_small);
        TextView textView6 = (TextView) m.a(view, R.id.tv_comment_num);
        TextView textView7 = (TextView) m.a(view, R.id.tv_comment_num_small);
        ((ImageView) m.a(view, R.id.iv_comment)).setOnClickListener(this.C);
        textView6.setOnClickListener(this.C);
        if (ringData.commentNum > 0) {
            textView6.setText("" + ringData.commentNum);
            imageView3.setVisibility(0);
            textView7.setVisibility(0);
            textView7.setText(a(ringData.commentNum));
        } else {
            imageView3.setVisibility(8);
            textView7.setVisibility(8);
            textView6.setText("评论");
        }
        if (ringData.isNew != 0) {
            imageView2.setVisibility(0);
        }
        if (ringData.isHot != 0) {
            Drawable drawable = this.g.getResources().getDrawable(R.drawable.icon_hot);
            drawable.setBounds(0, 0, (int) this.g.getResources().getDimension(R.dimen.hot_icon_width), (int) this.g.getResources().getDimension(R.dimen.hot_icon_height));
            an anVar = new an(drawable);
            SpannableString spannableString = new SpannableString(str2 + "  ");
            spannableString.setSpan(anVar, spannableString.length() - 1, spannableString.length(), 17);
            textView.setText(spannableString);
        } else {
            textView.setText(str2);
        }
        textView2.setText(ringData.artist);
        if (ringData.duration > 60) {
            str = "" + (ringData.duration / 60) + "分" + (ringData.duration % 60) + "秒";
        } else {
            str = "" + ringData.duration + "秒";
        }
        textView3.setText(str);
        textView3.setVisibility(ringData.duration == 0 ? 4 : 0);
        textView4.setText(b(ringData.playcnt));
    }

    private String a(int i2) {
        if (i2 <= 10000) {
            return "" + i2;
        }
        return "" + String.format("%.1f", Double.valueOf(((double) i2) / 10000.0d)) + "万";
    }

    private String b(int i2) {
        if (i2 > 100000000) {
            return "" + String.format("%.1f", Double.valueOf(((double) i2) / 1.0E8d)) + "亿";
        } else if (i2 > 10000) {
            return ("" + (i2 / 10000)) + "万";
        } else if (i2 < 100) {
            return "少于100";
        } else {
            return "" + i2;
        }
    }

    /* access modifiers changed from: private */
    public void b(final RingData ringData) {
        e();
        com.shoujiduoduo.util.c.b.a().a(new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc sdk is inited");
                if (bVar instanceof c.n) {
                    c.n nVar = (c.n) bVar;
                    if (nVar.f2243a == c.n.a.sms_code || nVar.f2243a == c.n.a.all) {
                        UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "sdk 已初始化，是sms_code 方式。isLogin:" + c.isLogin() + ", mobile:" + nVar.d);
                        if (!c.isLogin()) {
                            c.setUserName(nVar.d);
                            c.setUid("phone_" + nVar.d);
                        }
                        c.setPhoneNum(nVar.d);
                        c.setLoginStatus(1);
                        com.shoujiduoduo.a.b.b.g().a(c);
                        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                            public void a() {
                                ((v) this.f1284a).a(1, true, "", "");
                            }
                        });
                    } else {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "sdk 已初始化，但不是 sms_code 方式。");
                    }
                }
                k.this.c(ringData);
            }

            public void b(c.b bVar) {
                super.b(bVar);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc sdk is not inited");
                k.this.c();
                k.this.w.sendEmptyMessageDelayed(1, 3000);
            }
        });
    }

    /* access modifiers changed from: private */
    public void c(final RingData ringData) {
        com.shoujiduoduo.util.c.b.a().e(new com.shoujiduoduo.util.b.b() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, com.shoujiduoduo.util.g$b):void
             arg types: [com.shoujiduoduo.ui.utils.k, int, com.shoujiduoduo.util.g$b]
             candidates:
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b):android.view.View
              com.shoujiduoduo.ui.utils.k.a(int, android.view.View, boolean):void
              com.shoujiduoduo.ui.utils.k.a(android.view.View, int, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, com.shoujiduoduo.util.g$b):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.ui.utils.k, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.k, int]
             candidates:
              com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
              com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.ui.utils.k, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, int]
             candidates:
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, int]
             candidates:
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void */
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar instanceof c.d) {
                    c.d dVar = (c.d) bVar;
                    if (dVar.e() && dVar.d()) {
                        k.this.f();
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "均开通，直接订购");
                        k.this.a(true, g.b.f2309a);
                        k.this.b(true);
                    } else if (dVar.e() && !dVar.d()) {
                        k.this.f();
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "vip开通，彩铃关闭");
                        k.this.a(ringData, g.b.f2309a, "", true);
                    } else if (dVar.e() || !dVar.d()) {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "均关闭");
                        k.this.f();
                        k.this.a(ringData, "", g.b.f2309a, true);
                    } else {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "vip关闭，彩铃开通");
                        k.this.f();
                        k.this.a(ringData, "", g.b.f2309a, false);
                    }
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                k.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "查询状态失败");
                com.shoujiduoduo.util.widget.d.a("查询状态失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public void d(RingData ringData) {
        UserInfo c = com.shoujiduoduo.a.b.b.g().c();
        if (TextUtils.isEmpty(c.getPhoneNum())) {
            b(ringData, "", g.b.cu);
            return;
        }
        String phoneNum = c.getPhoneNum();
        if (com.shoujiduoduo.util.e.a.a().a(phoneNum)) {
            a(ringData, phoneNum, g.b.cu);
        } else {
            b(ringData, phoneNum, g.b.cu);
        }
    }

    /* access modifiers changed from: private */
    public void e(final RingData ringData) {
        final UserInfo c = com.shoujiduoduo.a.b.b.g().c();
        if (TextUtils.isEmpty(c.getPhoneNum()) || !c.isLogin()) {
            String h2 = g.h();
            if (!TextUtils.isEmpty(h2)) {
                b("请稍候...");
                com.shoujiduoduo.base.a.a.a("fuck", "RingListAdapter 调用 findMdnByImsi");
                com.shoujiduoduo.util.d.b.a().b(h2, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        if (bVar == null || !(bVar instanceof c.i)) {
                            k.this.f();
                            k.this.b(ringData, "", g.b.ct);
                            return;
                        }
                        k.this.f();
                        String d = ((c.i) bVar).d();
                        if (!c.isLogin()) {
                            c.setUserName(d);
                            c.setUid("phone_" + d);
                        }
                        c.setPhoneNum(d);
                        c.setLoginStatus(1);
                        com.shoujiduoduo.a.b.b.g().a(c);
                        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                            public void a() {
                                ((v) this.f1284a).a(1, true, "", "");
                            }
                        });
                        k.this.a(ringData, c.getPhoneNum(), g.b.ct);
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        k.this.f();
                        k.this.b(ringData, "", g.b.ct);
                    }
                });
                return;
            }
            b(ringData, "", g.b.ct);
            return;
        }
        a(ringData, c.getPhoneNum(), g.b.ct);
    }

    /* access modifiers changed from: private */
    public void a(RingData ringData, String str, g.b bVar) {
        if (bVar.equals(g.b.cu)) {
            a(ringData, str);
        } else if (bVar.equals(g.b.ct)) {
            b(ringData, str);
        } else if (bVar.equals(g.b.f2309a)) {
            c(ringData);
        }
    }

    private void a(final RingData ringData, final String str) {
        b("请稍候...");
        com.shoujiduoduo.util.e.a.a().h(str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar != null && (bVar instanceof c.ah)) {
                    c.ah ahVar = (c.ah) bVar;
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "user location, provinceid:" + ahVar.f2228a + ", province name:" + ahVar.d);
                    k.this.a(ringData, str, com.shoujiduoduo.util.e.a.a().c(ahVar.f2228a));
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                k.this.f();
                new b.a(k.this.g).b("设置彩铃").a("获取当前手机号信息失败，请稍候再试试。").a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(final RingData ringData, final String str, final boolean z2) {
        com.shoujiduoduo.util.e.a.a().f(new com.shoujiduoduo.util.b.b() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, com.shoujiduoduo.util.g$b):void
             arg types: [com.shoujiduoduo.ui.utils.k, int, com.shoujiduoduo.util.g$b]
             candidates:
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b):android.view.View
              com.shoujiduoduo.ui.utils.k.a(int, android.view.View, boolean):void
              com.shoujiduoduo.ui.utils.k.a(android.view.View, int, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, com.shoujiduoduo.util.g$b):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, int]
             candidates:
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, int]
             candidates:
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void */
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar == null || !(bVar instanceof c.f)) {
                    k.this.f();
                    new b.a(k.this.g).b("设置彩铃").a(bVar != null ? bVar.b() : "设置失败").a("确定", (DialogInterface.OnClickListener) null).a().show();
                    com.shoujiduoduo.base.a.a.c("RingListAdapter", "checkCailingAndVip failed");
                    return;
                }
                c.f fVar = (c.f) bVar;
                if (fVar.d() && fVar.e()) {
                    k.this.f();
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃与会员均开通，直接订购");
                    k.this.a(true, g.b.cu);
                    new b.a(k.this.g).b("设置彩铃(免费)").a(k.this.a(ringData, g.b.cu)).a("确定", new DialogInterface.OnClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
                         arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, int]
                         candidates:
                          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b):void
                          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void */
                        public void onClick(DialogInterface dialogInterface, int i) {
                            k.this.b("请稍候...");
                            k.this.c(ringData, str, false);
                            dialogInterface.dismiss();
                        }
                    }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                } else if (fVar.d() && !fVar.e()) {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃开通，会员关闭，提示开通会员");
                    k.this.f();
                    if (z2 || fVar.i()) {
                        k.this.a(ringData, str, g.b.cu, false);
                    } else {
                        new b.a(k.this.g).b("设置彩铃").a("抱歉，您所在的当前区域不支持铃声多多的彩铃功能！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                    }
                } else if (!fVar.d() && fVar.e()) {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃关闭，会员开通");
                    k.this.a(true, g.b.cu);
                    if (fVar.j()) {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "属于免彩铃功能费范围，先 帮用户自动开通彩铃功能， net type:" + fVar.g() + ", location:" + fVar.f());
                        com.shoujiduoduo.util.e.a.a().e("&phone=" + str, new com.shoujiduoduo.util.b.b() {
                            public void a(c.b bVar) {
                                super.a(bVar);
                                k.this.f();
                                com.shoujiduoduo.base.a.a.a("RingListAdapter", "成功开通彩铃基础功能");
                                new b.a(k.this.g).b("设置彩铃(免费)").a(k.this.a(ringData, g.b.cu)).a("确定", new DialogInterface.OnClickListener() {
                                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                     method: com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
                                     arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, int]
                                     candidates:
                                      com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b):void
                                      com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void */
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        k.this.b("请稍候...");
                                        k.this.c(ringData, str, true);
                                        dialogInterface.dismiss();
                                    }
                                }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                            }

                            public void b(c.b bVar) {
                                super.b(bVar);
                                k.this.f();
                                if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
                                    new b.a(k.this.g).b("设置彩铃(免费)").a(k.this.a(ringData, g.b.cu)).a("确定", new DialogInterface.OnClickListener() {
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
                                         arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, int]
                                         candidates:
                                          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b):void
                                          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void */
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            k.this.b("请稍候...");
                                            k.this.c(ringData, str, true);
                                            dialogInterface.dismiss();
                                        }
                                    }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                                } else {
                                    new b.a(k.this.g).b("设置彩铃").a("为您免费开通彩铃功能失败， 原因：" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                                }
                            }
                        });
                    } else {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "不属于免彩铃功能费范围， 提示开通彩铃");
                        k.this.f();
                        k.this.a(ringData, g.b.cu, str, true);
                    }
                } else if (!fVar.d() && !fVar.e()) {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃会员均关闭");
                    if (!z2 && !fVar.i()) {
                        new b.a(k.this.g).b("设置彩铃").a("抱歉，您所在的当前区域不支持铃声多多的彩铃功能！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                    } else if (fVar.j()) {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "属于免彩铃功能费范围， 帮用户自动开通彩铃，net type:" + fVar.g() + ", location:" + fVar.f());
                        com.shoujiduoduo.util.e.a.a().e("&phone=" + str, new com.shoujiduoduo.util.b.b() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void
                             arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, int]
                             candidates:
                              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void
                              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
                              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void */
                            public void a(c.b bVar) {
                                super.a(bVar);
                                k.this.f();
                                com.shoujiduoduo.base.a.a.a("RingListAdapter", "成功开通彩铃基础功能, 提示开通会员");
                                k.this.a((RingData) k.this.f2090a.get(k.this.b), str, g.b.cu, false);
                            }

                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void
                             arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, int]
                             candidates:
                              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void
                              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
                              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void */
                            public void b(c.b bVar) {
                                super.b(bVar);
                                k.this.f();
                                if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
                                    k.this.a((RingData) k.this.f2090a.get(k.this.b), str, g.b.cu, false);
                                } else {
                                    new b.a(k.this.g).b("设置彩铃").a("为您免费开通彩铃功能失败， 原因：" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                                }
                            }
                        });
                    } else {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "不属于免彩铃功能费范围， 提示开通会员");
                        k.this.f();
                        k.this.a(ringData, g.b.cu, str, false);
                    }
                }
                if (fVar.f2235a.a().equals("40307") || fVar.f2235a.a().equals("40308")) {
                    com.shoujiduoduo.util.e.a.a().a(str, "");
                    k.this.f();
                    k.this.b(ringData, str, g.b.cu);
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
            }
        });
    }

    private void b(final RingData ringData, final String str) {
        b("请稍候...");
        com.shoujiduoduo.util.d.b.a().a(str, new com.shoujiduoduo.util.b.b() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, com.shoujiduoduo.util.g$b):void
             arg types: [com.shoujiduoduo.ui.utils.k, int, com.shoujiduoduo.util.g$b]
             candidates:
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b):android.view.View
              com.shoujiduoduo.ui.utils.k.a(int, android.view.View, boolean):void
              com.shoujiduoduo.ui.utils.k.a(android.view.View, int, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, com.shoujiduoduo.util.g$b):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, int]
             candidates:
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, int]
             candidates:
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void */
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar == null || !(bVar instanceof c.e)) {
                    k.this.f();
                    new b.a(k.this.g).b("设置彩铃").a(bVar != null ? bVar.b() : "设置失败").a("确定", (DialogInterface.OnClickListener) null).a().show();
                    com.shoujiduoduo.base.a.a.c("RingListAdapter", "checkCailingAndVip failed");
                    return;
                }
                c.e eVar = (c.e) bVar;
                if (eVar.d() && (eVar.e() || eVar.f())) {
                    k.this.f();
                    k.this.a(true, g.b.ct);
                    new b.a(k.this.g).b("设置彩铃(免费)").a(k.this.a(ringData, g.b.ct)).a("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            k.this.b("请稍候...");
                            k.this.d(ringData, str, false);
                            dialogInterface.dismiss();
                        }
                    }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                } else if (eVar.d() && !eVar.e() && !eVar.f()) {
                    k.this.f();
                    k.this.a(ringData, str, g.b.ct, false);
                } else if (!eVar.d() && (eVar.e() || eVar.f())) {
                    k.this.a(true, g.b.ct);
                    k.this.f();
                    if (com.shoujiduoduo.util.d.b.a().a(str).equals(b.d.wait_open)) {
                        com.shoujiduoduo.util.widget.d.a("正在为您开通彩铃业务，请耐心等待一会儿...");
                    } else {
                        k.this.a(ringData, g.b.ct, str, true);
                    }
                } else if (!eVar.d() && !eVar.e() && !eVar.f()) {
                    k.this.f();
                    if (com.shoujiduoduo.util.d.b.a().a(str).equals(b.d.wait_open)) {
                        com.shoujiduoduo.util.widget.d.a("正在为您开通彩铃业务，请耐心等待一会儿...");
                    } else {
                        k.this.a(ringData, g.b.ct, str, false);
                    }
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                k.this.f();
                new b.a(k.this.g).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void c(final RingData ringData, final String str) {
        com.shoujiduoduo.util.d.b.a().g(str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                k.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "基础业务开通状态");
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, int]
             candidates:
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void */
            public void b(c.b bVar) {
                super.b(bVar);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃基础业务尚未开通");
                k.this.f();
                if (com.shoujiduoduo.util.d.b.a().a(str).equals(b.d.wait_open)) {
                    com.shoujiduoduo.util.widget.d.a("正在为您开通业务，请耐心等待一会儿.");
                } else {
                    k.this.a(ringData, g.b.ct, str, false);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, g.b bVar) {
        final int i2;
        int i3 = 0;
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "setVipState, isVip:" + z2 + ", type:" + bVar.toString());
        UserInfo c = com.shoujiduoduo.a.b.b.g().c();
        if (bVar.equals(g.b.cu)) {
            i2 = 3;
        } else if (bVar.equals(g.b.ct)) {
            i2 = 2;
        } else if (bVar.equals(g.b.f2309a)) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        if (z2) {
            i3 = i2;
        }
        c.setVipType(i3);
        if (!c.isLogin()) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "user is not login, phoneNum:" + c.getPhoneNum());
            if (bVar != g.b.f2309a || !ai.c(c.getPhoneNum())) {
                c.setUserName(c.getPhoneNum());
                c.setUid("phone_" + c.getPhoneNum());
            } else {
                c.setUserName("多多VIP");
                c.setUid("phone_" + com.shoujiduoduo.util.c.b.a().b());
            }
            c.setLoginStatus(1);
            com.shoujiduoduo.a.b.b.g().a(c);
            com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                public void a() {
                    ((v) this.f1284a).a(1, true, "", "");
                }
            });
        } else {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "user is login, update userinfo");
            com.shoujiduoduo.a.b.b.g().a(c);
        }
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new c.a<x>() {
            public void a() {
                ((x) this.f1284a).a(i2);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(final RingData ringData, String str, g.b bVar) {
        new com.shoujiduoduo.ui.cailing.e(this.g, R.style.DuoDuoDialog, str, bVar, new e.a() {
            public void a(String str) {
                UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                if (!c.isLogin()) {
                    c.setUserName(str);
                    c.setUid("phone_" + str);
                }
                c.setPhoneNum(str);
                c.setLoginStatus(1);
                com.shoujiduoduo.a.b.b.g().a(c);
                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                    public void a() {
                        ((v) this.f1284a).a(1, true, "", "");
                    }
                });
                k.this.a(ringData, str, g.g(str));
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void a(final RingData ringData, final String str, final g.b bVar, boolean z2) {
        new com.shoujiduoduo.ui.cailing.d(this.g, bVar, ringData, "ringlist", false, z2, new d.c() {
            public void a(boolean z) {
                if (z) {
                    new b.a(k.this.g).a("多多VIP会员业务已成功受理，正在为您开通。 是否设置 《" + ringData.name + "》 为您的当前彩铃？").a("确定", new DialogInterface.OnClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
                         arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, int]
                         candidates:
                          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b):void
                          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void */
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.ui.utils.k, boolean):void
                         arg types: [com.shoujiduoduo.ui.utils.k, int]
                         candidates:
                          com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
                          com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData):void
                          com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.ui.utils.k, boolean):void */
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            if (bVar.equals(g.b.cu)) {
                                k.this.c(ringData, str, true);
                            } else if (bVar.equals(g.b.ct)) {
                                k.this.d(ringData, str, true);
                            } else if (bVar.equals(g.b.f2309a)) {
                                k.this.b(true);
                            }
                        }
                    }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                }
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void a(RingData ringData, g.b bVar, String str, boolean z2) {
        final boolean z3 = z2;
        final RingData ringData2 = ringData;
        final g.b bVar2 = bVar;
        final String str2 = str;
        new f(this.g, R.style.DuoDuoDialog, bVar, new f.a() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, int]
             candidates:
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.g$b, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.k.a(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b, boolean):void */
            public void a(f.a.C0045a aVar) {
                if (!aVar.equals(b.d.open)) {
                    return;
                }
                if (z3) {
                    new b.a(k.this.g).a("多多会员业务已成功受理，正在为您开通。 是否设置 《" + ringData2.name + "》 为您的当前彩铃？").a("确定", new DialogInterface.OnClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
                         arg types: [com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, int]
                         candidates:
                          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.g$b):void
                          com.shoujiduoduo.ui.utils.k.b(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void */
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.ui.utils.k, boolean):void
                         arg types: [com.shoujiduoduo.ui.utils.k, int]
                         candidates:
                          com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
                          com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.ui.utils.k, com.shoujiduoduo.base.bean.RingData):void
                          com.shoujiduoduo.ui.utils.k.c(com.shoujiduoduo.ui.utils.k, boolean):void */
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            if (bVar2.equals(g.b.cu)) {
                                k.this.c(ringData2, str2, true);
                            } else if (bVar2.equals(g.b.ct)) {
                                k.this.d(ringData2, str2, true);
                            } else if (bVar2.equals(g.b.f2309a)) {
                                k.this.b(true);
                            }
                        }
                    }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                } else {
                    k.this.a((RingData) k.this.f2090a.get(k.this.b), str2, bVar2, false);
                }
            }
        }).show();
    }

    private void b(final RingData ringData, final String str, boolean z2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("&rid=").append(ringData.rid).append("&from=").append(this.f2090a.getListId()).append("&phone=").append(str);
        com.shoujiduoduo.util.d.b.a().a(ringData.name, str, ringData.ctWavUrl, sb.toString() + ("&info=" + ai.a("ringname:" + ringData.name)), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar != null && (bVar instanceof c.aa)) {
                    c.aa aaVar = (c.aa) bVar;
                    sb.append("&info=").append(ai.a("audioId:" + aaVar.f2221a + ", ringname:" + ringData.name));
                    com.shoujiduoduo.util.d.b.a().b(aaVar.f2221a, str, sb.toString(), new com.shoujiduoduo.util.b.b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            k.this.f();
                            new b.a(k.this.g).b("设置彩铃").a("已成功设置彩铃，预计十分钟生效，稍候请留意短信提醒").a("确定", (DialogInterface.OnClickListener) null).a().show();
                        }

                        public void b(c.b bVar) {
                            String bVar2;
                            super.b(bVar);
                            k.this.f();
                            if (bVar.a().equals("3023")) {
                                bVar2 = com.umeng.b.a.a().a(RingDDApp.c(), "ctcc_audio_check_hint");
                                if (ai.b(bVar2)) {
                                    bVar2 = "您设置的彩铃已提交电信审核，审核通过后即可生效。可能需要1-2个小时，部分省份需24小时，请耐心等待一下。";
                                }
                            } else {
                                bVar2 = bVar.toString();
                            }
                            new b.a(k.this.g).b("设置彩铃").a(bVar2).a("确定", (DialogInterface.OnClickListener) null).a().show();
                        }
                    });
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                k.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "diy_clip_upload onFailure:" + bVar.toString());
                new b.a(k.this.g).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        com.shoujiduoduo.ui.cailing.a aVar = new com.shoujiduoduo.ui.cailing.a(this.g, R.style.DuoDuoDialog, g.b.f2309a, z2);
        aVar.a(this.m, this.f2090a.getListId(), this.f2090a.getListType());
        aVar.show();
    }

    /* access modifiers changed from: private */
    public void c(final RingData ringData, String str, boolean z2) {
        StringBuilder sb = new StringBuilder();
        sb.append("&rid=").append(ringData.rid).append("&from=").append(this.f2090a.getListId()).append("&cucid=").append(ringData.cucid).append("&phone=").append(str).append("&info=").append(ai.a("ringname:" + ringData.name + ", cusid:" + ringData.cusid));
        com.shoujiduoduo.util.e.a.a().a(ringData.cucid, ringData.cusid, sb.toString(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "0元会员订购彩铃成功");
                k.this.a(ringData.cucid, ringData);
                am.a(ringData.rid, 6, "&from=" + k.this.f2090a.getListId() + "&listType=" + k.this.f2090a.getListType().toString() + "&cucid=" + ringData.cucid);
            }

            public void b(c.b bVar) {
                super.b(bVar);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "0元会员订购彩铃失败， msg:" + bVar.toString());
                if (bVar.a().equals("400033")) {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "set default");
                    k.this.a(ringData.cucid, ringData);
                    am.a(ringData.rid, 6, "&from=" + k.this.f2090a.getListId() + "&listType=" + k.this.f2090a.getListType() + "&cucid=" + ringData.cucid);
                    return;
                }
                k.this.f();
                new b.a(k.this.g).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void d(final RingData ringData, final String str, final boolean z2) {
        if (ringData.ctVip == 2) {
            b(ringData, str, z2);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("&ctcid=").append(ringData.ctcid).append("&from=").append(this.f2090a.getListId()).append("&phone=").append(str);
        com.shoujiduoduo.util.d.b.a().a(str, ringData.ctcid, sb.toString(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                k.this.d(ringData, str);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "vipOrder onSuccess:" + bVar.toString());
                af.b(k.this.g, "NeedUpdateCaiLingLib", 1);
                com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                    public void a() {
                        ((com.shoujiduoduo.a.c.c) this.f1284a).a(g.b.ct);
                    }
                });
            }

            public void b(c.b bVar) {
                k.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "vipOrder onFailure:" + bVar.toString());
                if (bVar.a().equals("0536") || bVar.a().equals("0538") || bVar.a().equals("0531")) {
                    k.this.d(ringData, str);
                } else if (bVar.a().equals("0703")) {
                    k.this.c(ringData, str);
                } else if ((bVar.a().equals("0574") || bVar.a().equals("0015") || bVar.a().equals("9001")) && z2) {
                    com.shoujiduoduo.util.widget.d.a("正在为您开通会员业务，请稍等一会儿... ", 1);
                } else if (bVar.a().equals("0556")) {
                    new b.a(k.this.g).b("设置彩铃").a("铃音数量已经达到最大限制, 请到我的彩铃里删除部分彩铃后再购买").a("确定", (DialogInterface.OnClickListener) null).a().show();
                } else {
                    new b.a(k.this.g).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                }
                super.b(bVar);
            }
        });
    }

    /* access modifiers changed from: private */
    public void d(final RingData ringData, String str) {
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "setDefaultCtccCailing, id:" + ringData.ctcid);
        com.shoujiduoduo.util.d.b.a().c(str, ringData.ctcid, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                k.this.f();
                new b.a(k.this.g).b("设置彩铃").a("已成功设置为您的当前彩铃.赶快试试吧！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                af.c(k.this.g, "DEFAULT_CAILING_ID", ringData.ctcid);
                com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, new c.a<p>() {
                    public void a() {
                        ((p) this.f1284a).a(16, ringData);
                    }
                });
                super.a(bVar);
            }

            public void b(c.b bVar) {
                k.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "setDefaultCtccCailing, onFailure, " + bVar.toString());
                new b.a(k.this.g).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                super.b(bVar);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(final String str, final RingData ringData) {
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "设置默认铃音");
        com.shoujiduoduo.util.e.a.a().h(new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                String str;
                boolean z = false;
                super.a(bVar);
                if (bVar != null && (bVar instanceof c.s)) {
                    c.s sVar = (c.s) bVar;
                    if (sVar.d != null) {
                        int i = 0;
                        while (true) {
                            if (i >= sVar.d.length) {
                                break;
                            } else if (sVar.d[i].c.equals("0")) {
                                z = true;
                                str = sVar.d[i].f2231a;
                                break;
                            } else {
                                i++;
                            }
                        }
                    }
                    str = "";
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "是否有timetype=0的默认铃声：" + z);
                    k.this.a(z, str, str, ringData);
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                k.this.f();
                com.shoujiduoduo.base.a.a.c("RingListAdapter", "查询用户铃音设置失败");
                new b.a(k.this.g).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, String str, final String str2, final RingData ringData) {
        com.shoujiduoduo.util.e.a.a().a(str2, z2, str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                k.this.f();
                new b.a(k.this.g).b("设置彩铃").a("已成功设置为您的当前彩铃.赶快试试吧！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                af.c(k.this.g, "DEFAULT_CAILING_ID", str2);
                com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, new c.a<p>() {
                    public void a() {
                        ((p) this.f1284a).a(16, ringData);
                    }
                });
                af.b(k.this.g, "NeedUpdateCaiLingLib", 1);
                com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                    public void a() {
                        ((com.shoujiduoduo.a.c.c) this.f1284a).a(g.b.cu);
                    }
                });
            }

            public void b(c.b bVar) {
                super.b(bVar);
                k.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "setDefaultCtccCailing, onFailure, " + bVar.toString());
                new b.a(k.this.g).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    public View a(RingData ringData, g.b bVar) {
        View inflate = LayoutInflater.from(this.g).inflate((int) R.layout.layout_cailing_info, (ViewGroup) null, false);
        ((ListView) inflate.findViewById(R.id.cailing_info_list)).setAdapter((ListAdapter) new SimpleAdapter(this.g, b(ringData, bVar), R.layout.listitem_cailing_info, new String[]{"cailing_info_des", "divider", "cailing_info_content"}, new int[]{R.id.cailing_info_des, R.id.devider, R.id.cailing_info_content}));
        return inflate;
    }

    private ArrayList<Map<String, Object>> b(RingData ringData, g.b bVar) {
        ArrayList<Map<String, Object>> arrayList = new ArrayList<>();
        HashMap hashMap = new HashMap();
        hashMap.put("cailing_info_des", "歌名");
        hashMap.put("divider", ":");
        hashMap.put("cailing_info_content", ringData.name);
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("cailing_info_des", "歌手");
        hashMap2.put("divider", ":");
        hashMap2.put("cailing_info_content", ringData.artist);
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("cailing_info_des", "有效期");
        hashMap3.put("divider", ":");
        String str = "";
        switch (bVar) {
            case f2309a:
                str = ringData.valid;
                break;
            case cu:
                if (ringData.cuvip != 1) {
                    str = ringData.cuvalid;
                    break;
                } else {
                    str = g();
                    break;
                }
            case ct:
                if (ringData.ctVip != 2) {
                    str = ringData.ctvalid;
                    break;
                } else {
                    str = g();
                    break;
                }
        }
        hashMap3.put("cailing_info_content", str);
        arrayList.add(hashMap3);
        return arrayList;
    }

    @SuppressLint({"SimpleDateFormat"})
    private String g() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        return simpleDateFormat.format(new Date(date.getYear() + 1, date.getMonth(), date.getDate()));
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.x == null) {
            this.x = new ProgressDialog(this.g);
            this.x.setMessage("您好，初始化彩铃功能需要发送一条免费短信，该过程自动完成，如弹出警告，请选择允许。");
            this.x.setIndeterminate(false);
            this.x.setCancelable(true);
            this.x.setCanceledOnTouchOutside(false);
            this.x.setButton(-1, "确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            this.x.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.x != null) {
            this.x.dismiss();
            this.x = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        b("请稍候...");
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (this.y == null) {
            this.y = new ProgressDialog(this.g);
            this.y.setMessage(str);
            this.y.setIndeterminate(false);
            this.y.setCancelable(true);
            this.y.setCanceledOnTouchOutside(false);
            this.y.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.y != null) {
            this.y.dismiss();
            this.y = null;
        }
    }

    private int c(int i2) {
        if (!this.h || i2 + 1 < this.j) {
            return i2;
        }
        return i2 - ((((i2 + 1) - this.j) / this.i) + 1);
    }

    private int h() {
        String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "feed_ad_layout_type");
        if (a2.equals("1")) {
            return R.layout.listitem_feed_ad;
        }
        if (a2.equals("2") || a2.equals("3")) {
        }
        return R.layout.listitem_feed_ad2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        int i3 = R.layout.listitem_concern_ring;
        if (this.f2090a == null) {
            return null;
        }
        switch (getItemViewType(i2)) {
            case 0:
                boolean equals = this.f2090a.getListType().equals(ListType.LIST_TYPE.list_ring_concern);
                if (view == null) {
                    LayoutInflater layoutInflater = this.e;
                    if (!equals) {
                        i3 = R.layout.listitem_ring;
                    }
                    view = layoutInflater.inflate(i3, viewGroup, false);
                    view.setTag(R.id.list_item_tag_key, "ring_tag");
                } else {
                    Object tag = view.getTag(R.id.list_item_tag_key);
                    if (tag != null && !tag.toString().equals("ring_tag")) {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "View type is ring , but tag is not ring tag");
                        LayoutInflater layoutInflater2 = this.e;
                        if (!equals) {
                            i3 = R.layout.listitem_ring;
                        }
                        view = layoutInflater2.inflate(i3, viewGroup, false);
                        view.setTag(R.id.list_item_tag_key, "ring_tag");
                    }
                }
                if (c(i2) >= this.f2090a.size()) {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "fuck, 越界了");
                    return view;
                }
                b(i2, view, equals);
                return view;
            case 1:
                boolean equals2 = this.f2090a.getListType().equals(ListType.LIST_TYPE.list_ring_concern);
                if (view == null) {
                    view = this.e.inflate(h(), viewGroup, false);
                    view.setTag(R.id.list_item_tag_key, "ad_tag");
                } else {
                    Object tag2 = view.getTag(R.id.list_item_tag_key);
                    if (tag2 != null && !tag2.toString().equals("ad_tag")) {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "View type is ad , but tag is not ad tag");
                        view = this.e.inflate(h(), viewGroup, false);
                        view.setTag(R.id.list_item_tag_key, "ad_tag");
                    }
                }
                a(i2, view, equals2);
                return view;
            case 2:
                View inflate = this.e.inflate((int) R.layout.list_loading, viewGroup, false);
                inflate.setTag(R.id.list_item_tag_key, "loading_tag");
                if (l.b > 0) {
                    AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams) inflate.getLayoutParams();
                    layoutParams.width = l.b;
                    layoutParams.height = l.b;
                    inflate.setLayoutParams(layoutParams);
                }
                final AnimationDrawable animationDrawable = (AnimationDrawable) ((ImageView) inflate.findViewById(R.id.loading)).getBackground();
                this.p.post(new Runnable() {
                    public void run() {
                        animationDrawable.start();
                    }
                });
                return inflate;
            case 3:
                View inflate2 = this.e.inflate((int) R.layout.list_failed, viewGroup, false);
                inflate2.setTag(R.id.list_item_tag_key, "failed_tag");
                if (l.b > 0) {
                    AbsListView.LayoutParams layoutParams2 = (AbsListView.LayoutParams) inflate2.getLayoutParams();
                    layoutParams2.width = l.b;
                    layoutParams2.height = l.b;
                    inflate2.setLayoutParams(layoutParams2);
                }
                inflate2.findViewById(R.id.network_fail_layout).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        k.this.a(d.a.LIST_LOADING);
                        k.this.f2090a.retrieveData();
                    }
                });
                return inflate2;
            default:
                return view;
        }
    }

    private void a(int i2, View view, boolean z2) {
        final e.a i3;
        TextView textView = (TextView) view.findViewById(R.id.sn);
        TextView textView2 = (TextView) view.findViewById(R.id.title);
        TextView textView3 = (TextView) view.findViewById(R.id.content);
        ImageView imageView = (ImageView) view.findViewById(R.id.pic);
        ImageView imageView2 = (ImageView) view.findViewById(R.id.ad_icon);
        if (com.umeng.b.a.a().a(RingDDApp.c(), "feed_ad_layout_type").equals("3")) {
            imageView.setVisibility(8);
        }
        textView.setText("" + (i2 + 1));
        if (z2) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
        if (!this.k) {
            if (this.l.containsKey(Integer.valueOf(i2))) {
                i3 = this.l.get(Integer.valueOf(i2));
                i3.c();
                if (i3.d()) {
                    this.l.remove(Integer.valueOf(i2));
                }
            } else {
                i3 = com.shoujiduoduo.a.b.b.c().i();
                if (i3 != null) {
                    i3.c();
                    if (!i3.d()) {
                        this.l.put(Integer.valueOf(i2), i3);
                    }
                }
            }
            if (i3 == null || i3.a() != 4) {
                imageView2.setVisibility(4);
            } else {
                imageView2.setVisibility(0);
            }
            if (i3 != null) {
                textView2.setText(i3.e());
                textView3.setText(i3.f());
                com.d.a.b.d.a().a(i3.g(), imageView, h.a().k());
                i3.a(view);
                view.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        i3.b(view);
                    }
                });
            } else {
                com.shoujiduoduo.base.a.a.e("RingListAdapter", "can not get valid feed ad, pos:" + (i2 + 1));
                if (com.shoujiduoduo.a.b.b.c().f()) {
                    final g.a g2 = com.shoujiduoduo.a.b.b.c().g();
                    if (g2 != null) {
                        textView2.setText(g2.f1301a);
                        textView3.setText(g2.c);
                        com.d.a.b.d.a().a(g2.d, imageView, h.a().j());
                        view.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                HashMap hashMap = new HashMap();
                                hashMap.put("package", g2.b);
                                hashMap.put(PushConstants.EXTRA_APPLICATION_PENDING_INTENT, g2.f1301a);
                                if (!ad.a().a("app_install_mode").equals("market")) {
                                    com.shoujiduoduo.util.x.a(g2.f, g2.f1301a);
                                } else if (!com.shoujiduoduo.util.x.a(g2.b)) {
                                    com.shoujiduoduo.util.x.a(g2.f, g2.f1301a);
                                }
                                com.umeng.a.b.a(RingDDApp.c(), "duoduo_app_ad_click", hashMap);
                            }
                        });
                    }
                } else {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "检索广告数据尚未获取");
                    textView2.setText("儿歌多多");
                    textView3.setText("多多团队出品，最好的儿歌故事类应用");
                    imageView.setImageResource(R.drawable.child_story_logo);
                    view.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "erge_down_url");
                            if (!ai.c(a2) && !com.shoujiduoduo.util.g.a("com.duoduo.child.story")) {
                                com.shoujiduoduo.util.widget.d.a("开始下载儿歌多多...");
                                new al(k.this.g, a2).execute(new Void[0]);
                            }
                        }
                    });
                }
            }
        }
        this.k = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x01fc  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0207  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x02e2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r24, android.view.View r25, boolean r26) {
        /*
            r23 = this;
            int r3 = r23.c(r24)
            r0 = r23
            r1 = r25
            r2 = r26
            r0.a(r1, r3, r2)
            r3 = 2131493660(0x7f0c031c, float:1.8610806E38)
            r0 = r25
            android.view.View r3 = com.shoujiduoduo.ui.utils.m.a(r0, r3)
            android.widget.ProgressBar r3 = (android.widget.ProgressBar) r3
            r4 = 2131493657(0x7f0c0319, float:1.86108E38)
            r0 = r25
            android.view.View r4 = com.shoujiduoduo.ui.utils.m.a(r0, r4)
            com.shoujiduoduo.util.widget.CircleProgressBar r4 = (com.shoujiduoduo.util.widget.CircleProgressBar) r4
            r5 = 2131493656(0x7f0c0318, float:1.8610798E38)
            r0 = r25
            android.view.View r5 = com.shoujiduoduo.ui.utils.m.a(r0, r5)
            android.widget.TextView r5 = (android.widget.TextView) r5
            if (r26 == 0) goto L_0x0070
            r6 = 4
            r5.setVisibility(r6)
            r6 = 2131493658(0x7f0c031a, float:1.8610802E38)
            r0 = r25
            android.view.View r6 = com.shoujiduoduo.ui.utils.m.a(r0, r6)
            android.widget.ImageView r6 = (android.widget.ImageView) r6
            r0 = r23
            com.shoujiduoduo.base.bean.DDList r7 = r0.f2090a
            int r8 = r23.c(r24)
            java.lang.Object r7 = r7.get(r8)
            com.shoujiduoduo.base.bean.RingData r7 = (com.shoujiduoduo.base.bean.RingData) r7
            java.lang.String r8 = r7.userHead
            boolean r8 = com.shoujiduoduo.util.ai.c(r8)
            if (r8 != 0) goto L_0x02b4
            com.d.a.b.d r8 = com.d.a.b.d.a()
            java.lang.String r9 = r7.userHead
            com.shoujiduoduo.ui.utils.h r10 = com.shoujiduoduo.ui.utils.h.a()
            com.d.a.b.c r10 = r10.e()
            r8.a(r9, r6, r10)
        L_0x0066:
            com.shoujiduoduo.ui.utils.k$32 r8 = new com.shoujiduoduo.ui.utils.k$32
            r0 = r23
            r8.<init>(r7)
            r6.setOnClickListener(r8)
        L_0x0070:
            r6 = 2131493661(0x7f0c031d, float:1.8610808E38)
            r0 = r25
            android.view.View r6 = com.shoujiduoduo.ui.utils.m.a(r0, r6)
            android.widget.ImageButton r6 = (android.widget.ImageButton) r6
            r7 = 2131493662(0x7f0c031e, float:1.861081E38)
            r0 = r25
            android.view.View r7 = com.shoujiduoduo.ui.utils.m.a(r0, r7)
            android.widget.ImageButton r7 = (android.widget.ImageButton) r7
            r8 = 2131493663(0x7f0c031f, float:1.8610813E38)
            r0 = r25
            android.view.View r8 = com.shoujiduoduo.ui.utils.m.a(r0, r8)
            android.widget.ImageButton r8 = (android.widget.ImageButton) r8
            r0 = r23
            android.view.View$OnClickListener r9 = r0.s
            r6.setOnClickListener(r9)
            r0 = r23
            android.view.View$OnClickListener r9 = r0.t
            r7.setOnClickListener(r9)
            r0 = r23
            android.view.View$OnClickListener r9 = r0.u
            r8.setOnClickListener(r9)
            java.lang.String r9 = ""
            com.shoujiduoduo.util.ab r10 = com.shoujiduoduo.util.ab.a()
            com.shoujiduoduo.player.PlayerService r20 = r10.b()
            if (r20 == 0) goto L_0x00be
            java.lang.String r9 = r20.b()
            int r10 = r20.c()
            r0 = r23
            r0.b = r10
        L_0x00be:
            r0 = r23
            com.shoujiduoduo.base.bean.DDList r10 = r0.f2090a
            java.lang.String r10 = r10.getListId()
            boolean r9 = r9.equals(r10)
            if (r9 == 0) goto L_0x03a1
            int r9 = r23.c(r24)
            r0 = r23
            int r10 = r0.b
            if (r9 != r10) goto L_0x03a1
            r0 = r23
            boolean r9 = r0.f
            if (r9 == 0) goto L_0x03a1
            r0 = r23
            com.shoujiduoduo.base.bean.DDList r9 = r0.f2090a
            int r10 = r23.c(r24)
            java.lang.Object r9 = r9.get(r10)
            com.shoujiduoduo.base.bean.RingData r9 = (com.shoujiduoduo.base.bean.RingData) r9
            r10 = 2131493679(0x7f0c032f, float:1.8610845E38)
            r0 = r25
            android.view.View r10 = com.shoujiduoduo.ui.utils.m.a(r0, r10)
            com.shoujiduoduo.util.widget.MyButton r10 = (com.shoujiduoduo.util.widget.MyButton) r10
            r11 = 2131493678(0x7f0c032e, float:1.8610843E38)
            r0 = r25
            android.view.View r11 = com.shoujiduoduo.ui.utils.m.a(r0, r11)
            com.shoujiduoduo.util.widget.MyButton r11 = (com.shoujiduoduo.util.widget.MyButton) r11
            r12 = 2131493677(0x7f0c032d, float:1.861084E38)
            r0 = r25
            android.view.View r12 = com.shoujiduoduo.ui.utils.m.a(r0, r12)
            com.shoujiduoduo.util.widget.MyButton r12 = (com.shoujiduoduo.util.widget.MyButton) r12
            r13 = 2131493682(0x7f0c0332, float:1.8610851E38)
            r0 = r25
            android.view.View r13 = com.shoujiduoduo.ui.utils.m.a(r0, r13)
            com.shoujiduoduo.util.widget.MyButton r13 = (com.shoujiduoduo.util.widget.MyButton) r13
            r14 = 2131493680(0x7f0c0330, float:1.8610847E38)
            r0 = r25
            android.view.View r14 = com.shoujiduoduo.ui.utils.m.a(r0, r14)
            com.shoujiduoduo.util.widget.MyButton r14 = (com.shoujiduoduo.util.widget.MyButton) r14
            r15 = 2131493681(0x7f0c0331, float:1.861085E38)
            r0 = r25
            android.view.View r15 = com.shoujiduoduo.ui.utils.m.a(r0, r15)
            com.shoujiduoduo.util.widget.MyButton r15 = (com.shoujiduoduo.util.widget.MyButton) r15
            r16 = 2131493669(0x7f0c0325, float:1.8610825E38)
            r0 = r25
            r1 = r16
            android.view.View r16 = com.shoujiduoduo.ui.utils.m.a(r0, r1)
            android.widget.TextView r16 = (android.widget.TextView) r16
            boolean r17 = r9.hasAACUrl()
            if (r17 == 0) goto L_0x02bc
            r17 = 0
        L_0x0141:
            r0 = r17
            r10.setVisibility(r0)
            int r0 = r9.hasshow
            r17 = r0
            r18 = 1
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x02c0
            r17 = 0
        L_0x0154:
            r0 = r17
            r14.setVisibility(r0)
            r17 = 0
            r0 = r17
            r11.setVisibility(r0)
            r17 = 0
            r0 = r17
            r12.setVisibility(r0)
            r0 = r23
            boolean r17 = r0.a(r9)
            if (r17 == 0) goto L_0x02c4
            r17 = 0
        L_0x0171:
            r0 = r17
            r13.setVisibility(r0)
            r17 = 2131493659(0x7f0c031b, float:1.8610804E38)
            r0 = r25
            r1 = r17
            android.view.View r17 = com.shoujiduoduo.ui.utils.m.a(r0, r1)
            android.widget.ImageView r17 = (android.widget.ImageView) r17
            if (r26 == 0) goto L_0x02c8
            r18 = 8
            r0 = r16
            r1 = r18
            r0.setVisibility(r1)
            r18 = 0
            r17.setVisibility(r18)
        L_0x0193:
            java.lang.String r0 = r9.uid
            r17 = r0
            boolean r17 = com.shoujiduoduo.util.ai.c(r17)
            if (r17 == 0) goto L_0x02cf
            r17 = 8
            r0 = r17
            r15.setVisibility(r0)
            r17 = 0
            r18 = r17
        L_0x01a8:
            r17 = 2131493043(0x7f0c00b3, float:1.8609555E38)
            r0 = r25
            r1 = r17
            android.view.View r17 = com.shoujiduoduo.ui.utils.m.a(r0, r1)
            android.widget.RelativeLayout r17 = (android.widget.RelativeLayout) r17
            if (r18 == 0) goto L_0x036a
            r0 = r23
            java.lang.String r0 = r0.d
            r16 = r0
            if (r16 == 0) goto L_0x02dc
            r0 = r23
            java.lang.String r0 = r0.d
            r16 = r0
            java.lang.String r0 = r9.uid
            r18 = r0
            r0 = r16
            r1 = r18
            boolean r16 = r0.equals(r1)
            if (r16 == 0) goto L_0x02dc
            r16 = 1
            r19 = r16
        L_0x01d7:
            r16 = 0
            r0 = r17
            r1 = r16
            r0.setVisibility(r1)
            r16 = 2131493684(0x7f0c0334, float:1.8610855E38)
            r0 = r17
            r1 = r16
            android.view.View r16 = r0.findViewById(r1)
            android.widget.TextView r16 = (android.widget.TextView) r16
            r0 = r23
            android.view.View$OnClickListener r0 = r0.B
            r18 = r0
            r0 = r16
            r1 = r18
            r0.setOnClickListener(r1)
            if (r19 == 0) goto L_0x02e2
            r18 = 4
            r0 = r16
            r1 = r18
            r0.setVisibility(r1)
        L_0x0205:
            if (r26 != 0) goto L_0x024b
            r18 = 2131493052(0x7f0c00bc, float:1.8609573E38)
            android.view.View r18 = r17.findViewById(r18)
            com.shoujiduoduo.util.widget.MyButton r18 = (com.shoujiduoduo.util.widget.MyButton) r18
            com.shoujiduoduo.ui.utils.k$34 r21 = new com.shoujiduoduo.ui.utils.k$34
            r0 = r21
            r1 = r23
            r2 = r16
            r0.<init>(r9, r2)
            r0 = r18
            r1 = r21
            r0.setOnClickListener(r1)
            if (r19 == 0) goto L_0x02f8
            r16 = 4
            r0 = r18
            r1 = r16
            r0.setVisibility(r1)
        L_0x022d:
            r16 = 2131493658(0x7f0c031a, float:1.8610802E38)
            r0 = r17
            r1 = r16
            android.view.View r16 = r0.findViewById(r1)
            android.widget.ImageView r16 = (android.widget.ImageView) r16
            r0 = r23
            android.view.View$OnClickListener r0 = r0.B
            r17 = r0
            r16.setOnClickListener(r17)
            if (r19 == 0) goto L_0x0338
            r9 = 4
            r0 = r16
            r0.setVisibility(r9)
        L_0x024b:
            r0 = r23
            android.view.View$OnClickListener r9 = r0.z
            r10.setOnClickListener(r9)
            r9 = 2130837981(0x7f0201dd, float:1.7280931E38)
            r10 = 0
            r16 = 0
            r17 = 0
            r0 = r16
            r1 = r17
            r11.setCompoundDrawablesWithIntrinsicBounds(r9, r10, r0, r1)
            r9 = 2131099782(0x7f060086, float:1.7811927E38)
            r11.setText(r9)
            r0 = r23
            android.view.View$OnClickListener r9 = r0.E
            r11.setOnClickListener(r9)
            r0 = r23
            android.view.View$OnClickListener r9 = r0.D
            r12.setOnClickListener(r9)
            r0 = r23
            android.view.View$OnClickListener r9 = r0.v
            r13.setOnClickListener(r9)
            r0 = r23
            android.view.View$OnClickListener r9 = r0.A
            r14.setOnClickListener(r9)
            r0 = r23
            android.view.View$OnClickListener r9 = r0.B
            r15.setOnClickListener(r9)
            r9 = 4
            r5.setVisibility(r9)
            r5 = 4
            r3.setVisibility(r5)
            r5 = 4
            r6.setVisibility(r5)
            r5 = 4
            r7.setVisibility(r5)
            r5 = 4
            r8.setVisibility(r5)
            r5 = 4
            r4.setVisibility(r5)
            r0 = r23
            com.shoujiduoduo.ui.utils.k$a r5 = r0.o
            r5.a(r4)
            r5 = 5
            if (r20 == 0) goto L_0x02b0
            int r5 = r20.a()
        L_0x02b0:
            switch(r5) {
                case 1: goto L_0x0383;
                case 2: goto L_0x038d;
                case 3: goto L_0x0379;
                case 4: goto L_0x0379;
                case 5: goto L_0x0379;
                case 6: goto L_0x0397;
                default: goto L_0x02b3;
            }
        L_0x02b3:
            return
        L_0x02b4:
            r8 = 2130837931(0x7f0201ab, float:1.728083E38)
            r6.setImageResource(r8)
            goto L_0x0066
        L_0x02bc:
            r17 = 8
            goto L_0x0141
        L_0x02c0:
            r17 = 8
            goto L_0x0154
        L_0x02c4:
            r17 = 8
            goto L_0x0171
        L_0x02c8:
            r17 = 0
            r16.setVisibility(r17)
            goto L_0x0193
        L_0x02cf:
            r17 = 8
            r0 = r17
            r15.setVisibility(r0)
            r17 = 1
            r18 = r17
            goto L_0x01a8
        L_0x02dc:
            r16 = 0
            r19 = r16
            goto L_0x01d7
        L_0x02e2:
            r18 = 0
            r0 = r16
            r1 = r18
            r0.setVisibility(r1)
            java.lang.String r0 = r9.artist
            r18 = r0
            r0 = r16
            r1 = r18
            r0.setText(r1)
            goto L_0x0205
        L_0x02f8:
            com.shoujiduoduo.b.e.a r21 = com.shoujiduoduo.a.b.b.g()
            com.shoujiduoduo.base.bean.UserInfo r21 = r21.c()
            java.lang.String r21 = r21.getFollowings()
            if (r21 == 0) goto L_0x0324
            java.lang.String r0 = r9.uid
            r22 = r0
            boolean r21 = r21.contains(r22)
            if (r21 == 0) goto L_0x0324
            r21 = 8
            r0 = r18
            r1 = r21
            r0.setVisibility(r1)
            r18 = 0
            r0 = r16
            r1 = r18
            r0.setVisibility(r1)
            goto L_0x022d
        L_0x0324:
            r21 = 0
            r0 = r18
            r1 = r21
            r0.setVisibility(r1)
            r18 = 8
            r0 = r16
            r1 = r18
            r0.setVisibility(r1)
            goto L_0x022d
        L_0x0338:
            r17 = 0
            r16.setVisibility(r17)
            java.lang.String r0 = r9.userHead
            r17 = r0
            boolean r17 = com.shoujiduoduo.util.ai.c(r17)
            if (r17 != 0) goto L_0x0360
            com.d.a.b.d r17 = com.d.a.b.d.a()
            java.lang.String r9 = r9.userHead
            com.shoujiduoduo.ui.utils.h r18 = com.shoujiduoduo.ui.utils.h.a()
            com.d.a.b.c r18 = r18.e()
            r0 = r17
            r1 = r16
            r2 = r18
            r0.a(r9, r1, r2)
            goto L_0x024b
        L_0x0360:
            r9 = 2130837931(0x7f0201ab, float:1.728083E38)
            r0 = r16
            r0.setImageResource(r9)
            goto L_0x024b
        L_0x036a:
            r9 = 8
            r0 = r17
            r0.setVisibility(r9)
            r9 = 0
            r0 = r16
            r0.setVisibility(r9)
            goto L_0x024b
        L_0x0379:
            r3 = 0
            r6.setVisibility(r3)
            r3 = 0
            r4.setVisibility(r3)
            goto L_0x02b3
        L_0x0383:
            r5 = 0
            r3.setVisibility(r5)
            r3 = 4
            r4.setVisibility(r3)
            goto L_0x02b3
        L_0x038d:
            r3 = 0
            r7.setVisibility(r3)
            r3 = 0
            r4.setVisibility(r3)
            goto L_0x02b3
        L_0x0397:
            r3 = 0
            r8.setVisibility(r3)
            r3 = 4
            r4.setVisibility(r3)
            goto L_0x02b3
        L_0x03a1:
            r9 = 2131493679(0x7f0c032f, float:1.8610845E38)
            r0 = r25
            android.view.View r9 = com.shoujiduoduo.ui.utils.m.a(r0, r9)
            com.shoujiduoduo.util.widget.MyButton r9 = (com.shoujiduoduo.util.widget.MyButton) r9
            r10 = 2131493678(0x7f0c032e, float:1.8610843E38)
            r0 = r25
            android.view.View r10 = com.shoujiduoduo.ui.utils.m.a(r0, r10)
            com.shoujiduoduo.util.widget.MyButton r10 = (com.shoujiduoduo.util.widget.MyButton) r10
            r11 = 2131493677(0x7f0c032d, float:1.861084E38)
            r0 = r25
            android.view.View r11 = com.shoujiduoduo.ui.utils.m.a(r0, r11)
            com.shoujiduoduo.util.widget.MyButton r11 = (com.shoujiduoduo.util.widget.MyButton) r11
            r12 = 2131493682(0x7f0c0332, float:1.8610851E38)
            r0 = r25
            android.view.View r12 = com.shoujiduoduo.ui.utils.m.a(r0, r12)
            com.shoujiduoduo.util.widget.MyButton r12 = (com.shoujiduoduo.util.widget.MyButton) r12
            r13 = 2131493680(0x7f0c0330, float:1.8610847E38)
            r0 = r25
            android.view.View r13 = com.shoujiduoduo.ui.utils.m.a(r0, r13)
            com.shoujiduoduo.util.widget.MyButton r13 = (com.shoujiduoduo.util.widget.MyButton) r13
            r14 = 2131493681(0x7f0c0331, float:1.861085E38)
            r0 = r25
            android.view.View r14 = com.shoujiduoduo.ui.utils.m.a(r0, r14)
            com.shoujiduoduo.util.widget.MyButton r14 = (com.shoujiduoduo.util.widget.MyButton) r14
            r15 = 2131493043(0x7f0c00b3, float:1.8609555E38)
            r0 = r25
            android.view.View r15 = com.shoujiduoduo.ui.utils.m.a(r0, r15)
            android.widget.RelativeLayout r15 = (android.widget.RelativeLayout) r15
            r16 = 2131493669(0x7f0c0325, float:1.8610825E38)
            r0 = r25
            r1 = r16
            android.view.View r16 = com.shoujiduoduo.ui.utils.m.a(r0, r1)
            android.widget.TextView r16 = (android.widget.TextView) r16
            r17 = 0
            r16.setVisibility(r17)
            r16 = 8
            r15.setVisibility(r16)
            r15 = 2131493659(0x7f0c031b, float:1.8610804E38)
            r0 = r25
            android.view.View r15 = com.shoujiduoduo.ui.utils.m.a(r0, r15)
            android.widget.ImageView r15 = (android.widget.ImageView) r15
            if (r26 == 0) goto L_0x0417
            r16 = 4
            r15.setVisibility(r16)
        L_0x0417:
            r15 = 8
            r9.setVisibility(r15)
            r9 = 8
            r10.setVisibility(r9)
            r9 = 8
            r11.setVisibility(r9)
            r9 = 8
            r12.setVisibility(r9)
            r9 = 8
            r13.setVisibility(r9)
            r9 = 8
            r14.setVisibility(r9)
            int r9 = r24 + 1
            java.lang.String r9 = java.lang.Integer.toString(r9)
            r5.setText(r9)
            r9 = 0
            r5.setVisibility(r9)
            r5 = 4
            r3.setVisibility(r5)
            r3 = 4
            r4.setVisibility(r3)
            r3 = 4
            r6.setVisibility(r3)
            r3 = 4
            r7.setVisibility(r3)
            r3 = 4
            r8.setVisibility(r3)
            goto L_0x02b3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.utils.k.b(int, android.view.View, boolean):void");
    }
}
