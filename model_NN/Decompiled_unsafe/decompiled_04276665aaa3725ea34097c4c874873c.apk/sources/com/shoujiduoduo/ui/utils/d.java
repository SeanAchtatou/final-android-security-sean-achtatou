package com.shoujiduoduo.ui.utils;

import android.widget.BaseAdapter;
import com.shoujiduoduo.base.bean.DDList;

/* compiled from: BaseListAdapter */
public abstract class d extends BaseAdapter {
    public volatile a c = a.LIST_LOADING;

    /* compiled from: BaseListAdapter */
    public enum a {
        LIST_CONTENT,
        LIST_LOADING,
        LIST_FAILED
    }

    public abstract void a();

    public abstract void a(DDList dDList);

    public abstract void a(boolean z);

    public abstract void b();

    public void a(a aVar) {
        this.c = aVar;
        notifyDataSetChanged();
    }

    public int getViewTypeCount() {
        return 3;
    }

    public int getItemViewType(int i) {
        switch (this.c) {
            case LIST_CONTENT:
                return 0;
            case LIST_LOADING:
                return 2;
            case LIST_FAILED:
                return 3;
            default:
                return -1;
        }
    }
}
