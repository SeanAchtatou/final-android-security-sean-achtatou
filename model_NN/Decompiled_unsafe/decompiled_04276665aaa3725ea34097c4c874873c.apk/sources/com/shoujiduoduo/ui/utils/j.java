package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import com.shoujiduoduo.ringtone.RingDDApp;

/* compiled from: ResUtils */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private static Context f2089a = RingDDApp.c();

    public static int a(int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return f2089a.getResources().getColor(i, f2089a.getTheme());
        }
        return f2089a.getResources().getColor(i);
    }

    public static Drawable b(int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return f2089a.getResources().getDrawable(i, f2089a.getTheme());
        }
        return f2089a.getResources().getDrawable(i);
    }
}
