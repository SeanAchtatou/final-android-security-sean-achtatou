package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.b.c.e;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.CommentData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.mine.UserMainPageActivity;
import com.shoujiduoduo.ui.utils.TextViewFixTouchConsume;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.u;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/* compiled from: CommentListAdapter */
public class f extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public e f2068a;
    /* access modifiers changed from: private */
    public Context b;
    private Handler d;
    private com.shoujiduoduo.a.c.e e = new com.shoujiduoduo.a.c.e() {
        public void a(CommentData commentData) {
        }

        public void a() {
            if (f.this.f2068a != null) {
                f.this.f2068a.reloadData();
                f.this.a(d.a.LIST_LOADING);
            }
        }
    };

    public f(Context context) {
        this.b = context;
        this.d = new Handler();
    }

    public void a(boolean z) {
    }

    public void a(DDList dDList) {
        this.f2068a = (e) dDList;
    }

    public void a() {
        c.a().a(b.OBSERVER_COMMENT, this.e);
    }

    public void b() {
        c.a().b(b.OBSERVER_COMMENT, this.e);
    }

    public int getCount() {
        if (this.f2068a == null) {
            return 0;
        }
        if (this.c == d.a.LIST_CONTENT) {
            return this.f2068a.size();
        }
        return 1;
    }

    public Object getItem(int i) {
        if (this.f2068a != null) {
            return this.f2068a.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* access modifiers changed from: private */
    public boolean a(CommentData commentData) {
        return af.a(RingDDApp.c(), "upvote_comment_list", "").contains(commentData.cid);
    }

    /* access modifiers changed from: private */
    public void b(CommentData commentData) {
        String str;
        String a2 = af.a(RingDDApp.c(), "upvote_comment_list", "");
        if (a2.equals("")) {
            str = commentData.cid;
        } else {
            str = a2 + "|" + commentData.cid;
        }
        af.c(RingDDApp.c(), "upvote_comment_list", str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        switch (getItemViewType(i)) {
            case 0:
                if (view == null) {
                    view = LayoutInflater.from(this.b).inflate((int) R.layout.listitem_comment, viewGroup, false);
                }
                ImageView imageView = (ImageView) m.a(view, R.id.user_head);
                TextView textView = (TextView) m.a(view, R.id.user_name);
                TextView textView2 = (TextView) m.a(view, R.id.create_time);
                TextView textView3 = (TextView) m.a(view, R.id.vote_num);
                TextViewFixTouchConsume textViewFixTouchConsume = (TextViewFixTouchConsume) m.a(view, R.id.comment);
                TextViewFixTouchConsume textViewFixTouchConsume2 = (TextViewFixTouchConsume) m.a(view, R.id.tcomment);
                TextView textView4 = (TextView) m.a(view, R.id.comment_catetory);
                ImageView imageView2 = (ImageView) m.a(view, R.id.tv_upvote);
                final CommentData commentData = (CommentData) this.f2068a.get(i);
                if (!ai.c(commentData.head_url)) {
                    com.d.a.b.d.a().a(commentData.head_url, imageView, h.a().e());
                }
                imageView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                        intent.putExtra("tuid", commentData.uid);
                        f.this.b.startActivity(intent);
                    }
                });
                textView.setText(commentData.name);
                try {
                    textView2.setText(g.a(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(commentData.createtime)));
                } catch (ParseException e2) {
                    e2.printStackTrace();
                    textView2.setText(commentData.createtime);
                }
                if (commentData.upvote > 0) {
                    textView3.setText("" + commentData.upvote);
                } else {
                    textView3.setText("");
                }
                if (a(commentData)) {
                    imageView2.setImageResource(R.drawable.icon_upvote_pressed);
                    textView3.setTextColor(RingDDApp.c().getResources().getColor(R.color.text_green));
                } else {
                    imageView2.setImageResource(R.drawable.icon_upvote_normal);
                    textView3.setTextColor(RingDDApp.c().getResources().getColor(R.color.text_gray));
                }
                m.a(view, R.id.vote_layout).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (!f.this.a(commentData)) {
                            a.a("CommentListAdapter", "顶评论， comment：" + commentData.comment + ", cid:" + commentData.cid);
                            commentData.upvote++;
                            f.this.b(commentData);
                            f.this.notifyDataSetChanged();
                            u.a("upvote", "&rid=" + commentData.rid + "&uid=" + com.shoujiduoduo.a.b.b.g().c().getUid() + "&cid=" + commentData.cid, new u.a() {
                                public void a(String str) {
                                    a.a("CommentListAdapter", "vote comment success:" + str);
                                }

                                public void a(String str, String str2) {
                                    a.a("CommentListAdapter", "vote comment error");
                                }
                            });
                        }
                    }
                });
                if (!ai.c(commentData.tcid)) {
                    textViewFixTouchConsume.setMovementMethod(TextViewFixTouchConsume.a.a());
                    textViewFixTouchConsume.setFocusable(false);
                    textViewFixTouchConsume2.setMovementMethod(TextViewFixTouchConsume.a.a());
                    textViewFixTouchConsume2.setFocusable(false);
                    SpannableString spannableString = new SpannableString("回复@" + commentData.tname + ":" + commentData.comment);
                    SpannableString spannableString2 = new SpannableString("@" + commentData.tname + ":" + commentData.tcomment);
                    final int color = this.b.getResources().getColor(R.color.text_blue);
                    AnonymousClass4 r3 = new ClickableSpan() {
                        public void updateDrawState(TextPaint textPaint) {
                            super.updateDrawState(textPaint);
                            textPaint.setColor(color);
                            textPaint.setUnderlineText(false);
                        }

                        public void onClick(View view) {
                            Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                            intent.putExtra("tuid", commentData.tuid);
                            f.this.b.startActivity(intent);
                        }
                    };
                    spannableString.setSpan(r3, 2, ("@" + commentData.tname).length() + 2, 17);
                    spannableString2.setSpan(r3, 0, ("@" + commentData.tname).length(), 17);
                    textViewFixTouchConsume2.setText(spannableString2);
                    textViewFixTouchConsume2.setVisibility(0);
                    textViewFixTouchConsume.setText(spannableString);
                } else {
                    textViewFixTouchConsume.setText(commentData.comment);
                    textViewFixTouchConsume2.setVisibility(8);
                }
                if (ai.c(commentData.catetoryHint)) {
                    textView4.setVisibility(8);
                    break;
                } else {
                    textView4.setText(commentData.catetoryHint);
                    textView4.setVisibility(0);
                    break;
                }
            case 2:
                view = LayoutInflater.from(this.b).inflate((int) R.layout.list_loading, viewGroup, false);
                if (l.b > 0) {
                    AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams) view.getLayoutParams();
                    layoutParams.width = l.b;
                    layoutParams.height = l.b;
                    view.setLayoutParams(layoutParams);
                }
                final AnimationDrawable animationDrawable = (AnimationDrawable) ((ImageView) view.findViewById(R.id.loading)).getBackground();
                this.d.post(new Runnable() {
                    public void run() {
                        animationDrawable.start();
                    }
                });
                break;
            case 3:
                view = LayoutInflater.from(this.b).inflate((int) R.layout.list_failed, viewGroup, false);
                if (l.b > 0) {
                    AbsListView.LayoutParams layoutParams2 = (AbsListView.LayoutParams) view.getLayoutParams();
                    layoutParams2.width = l.b;
                    layoutParams2.height = l.b;
                    view.setLayoutParams(layoutParams2);
                }
                view.findViewById(R.id.network_fail_layout).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        f.this.a(d.a.LIST_LOADING);
                        f.this.f2068a.retrieveData();
                    }
                });
                break;
        }
        return view;
    }
}
