package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.d.a.b.d;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.b.a.g;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.x;
import com.umeng.a.b;
import java.util.HashMap;

/* compiled from: AdHeaderViewHolder */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private View f2053a;
    private RelativeLayout b;
    private TextView c;
    private TextView d;
    private ImageView e;
    private Button f;
    /* access modifiers changed from: private */
    public g.a g;
    private Context h;

    public a(Context context) {
        this.h = context;
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void b() {
        this.f2053a = LayoutInflater.from(this.h).inflate((int) R.layout.listitem_header_search_ad, (ViewGroup) null, false);
        this.b = (RelativeLayout) this.f2053a.findViewById(R.id.header_layout);
        this.c = (TextView) this.f2053a.findViewById(R.id.app_name);
        this.d = (TextView) this.f2053a.findViewById(R.id.des);
        this.e = (ImageView) this.f2053a.findViewById(R.id.ad_icon);
        this.f = (Button) this.f2053a.findViewById(R.id.btn_install);
        this.f.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                HashMap hashMap = new HashMap();
                hashMap.put("package", a.this.g.b);
                hashMap.put(PushConstants.EXTRA_APPLICATION_PENDING_INTENT, a.this.g.f1301a);
                if (!ad.a().a("app_install_mode").equals("market")) {
                    x.a(a.this.g.f, a.this.g.f1301a);
                } else if (!x.a(a.this.g.b)) {
                    x.a(a.this.g.f, a.this.g.f1301a);
                }
                b.a(RingDDApp.c(), "duoduo_app_ad_click", hashMap);
            }
        });
    }

    public View a() {
        return this.f2053a;
    }

    public void a(boolean z) {
        int i = 8;
        if (com.shoujiduoduo.a.b.b.g().h()) {
            this.b.setVisibility(8);
            return;
        }
        RelativeLayout relativeLayout = this.b;
        if (z) {
            i = 0;
        }
        relativeLayout.setVisibility(i);
    }

    public void a(g.a aVar) {
        this.g = aVar;
        this.c.setText(aVar.f1301a);
        this.d.setText(aVar.c);
        if (aVar.b.equals("com.duoduo.child.story")) {
            this.e.setImageResource(R.drawable.child_story_logo);
        } else if (aVar.b.equals("com.shoujiduoduo.wallpaper")) {
            this.e.setImageResource(R.drawable.wallpaper_logo);
        } else {
            d.a().a(aVar.d, this.e, h.a().j());
        }
    }
}
