package com.shoujiduoduo.ui.utils;

import android.support.v4.app.Fragment;

public abstract class LazyFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f2043a;

    /* access modifiers changed from: protected */
    public abstract void a();

    public void setUserVisibleHint(boolean z) {
        super.setUserVisibleHint(z);
        if (getUserVisibleHint()) {
            this.f2043a = true;
            d();
            return;
        }
        this.f2043a = false;
        e();
    }

    /* access modifiers changed from: protected */
    public void d() {
        a();
    }

    /* access modifiers changed from: protected */
    public void e() {
    }
}
