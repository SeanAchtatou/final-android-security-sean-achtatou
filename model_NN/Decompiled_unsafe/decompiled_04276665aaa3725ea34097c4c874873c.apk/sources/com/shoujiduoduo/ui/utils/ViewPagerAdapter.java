package com.shoujiduoduo.ui.utils;

import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List<View> f2052a;

    public int getCount() {
        if (this.f2052a == null) {
            return 0;
        }
        return this.f2052a.size();
    }

    public void destroyItem(View view, int i, Object obj) {
        ((ViewPager) view).removeView(this.f2052a.get(i));
    }

    public Object instantiateItem(View view, int i) {
        ((ViewPager) view).addView(this.f2052a.get(i), 0);
        return this.f2052a.get(i);
    }

    public void finishUpdate(View view) {
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
    }

    public Parcelable saveState() {
        return null;
    }

    public void startUpdate(View view) {
    }
}
