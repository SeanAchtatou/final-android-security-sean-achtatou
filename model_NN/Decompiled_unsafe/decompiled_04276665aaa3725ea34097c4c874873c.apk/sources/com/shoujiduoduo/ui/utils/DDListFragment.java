package com.shoujiduoduo.ui.utils;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.baoyz.widget.PullRefreshLayout;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.m;
import com.shoujiduoduo.base.bean.CommentData;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.MessageData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.home.ArtistRingActivity;
import com.shoujiduoduo.ui.home.CollectRingActivity;
import com.shoujiduoduo.ui.mine.UserMainPageActivity;
import com.shoujiduoduo.ui.user.CommentActivity;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.s;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;

public class DDListFragment extends LazyFragment implements PullRefreshLayout.a {
    /* access modifiers changed from: private */
    public DDList b;
    private RelativeLayout c;
    /* access modifiers changed from: private */
    public PullRefreshLayout d;
    /* access modifiers changed from: private */
    public d e;
    private ListView f;
    /* access modifiers changed from: private */
    public boolean g;
    private Button h;
    private Button i;
    private View j;
    private View k;
    private boolean l;
    private boolean m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    /* access modifiers changed from: private */
    public AbsListView.OnScrollListener s;
    private int t;
    private int u;
    private com.shoujiduoduo.a.c.g v = new com.shoujiduoduo.a.c.g() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.DDListFragment, int]
         candidates:
          com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, int):int
          com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, com.shoujiduoduo.ui.utils.DDListFragment$e):void
          com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, boolean):boolean */
        public void a(DDList dDList, int i) {
            if (DDListFragment.this.b != null && dDList.getListId().equals(DDListFragment.this.b.getListId())) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "onDataUpdate in, id:" + DDListFragment.this.b.getListId());
                if (DDListFragment.this.g) {
                    boolean unused = DDListFragment.this.g = false;
                    DDListFragment.this.d.setRefreshing(false);
                }
                switch (i) {
                    case 0:
                    case 5:
                    case 6:
                        com.shoujiduoduo.base.a.a.a("DDListFragment", "show content now! listid:" + dDList.getListId());
                        DDListFragment.this.a(d.a.LIST_CONTENT);
                        return;
                    case 1:
                        com.shoujiduoduo.base.a.a.a("DDListFragment", "show failed now. listid:" + dDList.getListId());
                        DDListFragment.this.a(d.a.LIST_FAILED);
                        return;
                    case 2:
                        com.shoujiduoduo.base.a.a.a("DDListFragment", "more data ready. notify the adapter to update. listid:" + dDList.getListId());
                        com.shoujiduoduo.base.a.a.a("DDListFragment", "FooterState: set failed onDataUpdate.");
                        DDListFragment.this.a(e.RETRIEVE_FAILED);
                        DDListFragment.this.e.notifyDataSetChanged();
                        return;
                    case 3:
                        com.shoujiduoduo.util.widget.d.a("已经是最新啦");
                        return;
                    case 4:
                        com.shoujiduoduo.util.widget.d.a("获取失败");
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private AbsListView.OnScrollListener w = new AbsListView.OnScrollListener() {

        /* renamed from: a  reason: collision with root package name */
        boolean f2033a = false;

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (DDListFragment.this.s != null) {
                DDListFragment.this.s.onScrollStateChanged(absListView, i);
            }
            if (this.f2033a && i == 0) {
                if (DDListFragment.this.b != null) {
                    if (DDListFragment.this.b.hasMoreData()) {
                        if (!DDListFragment.this.b.isRetrieving()) {
                            DDListFragment.this.b.retrieveData();
                            DDListFragment.this.a(e.RETRIEVE);
                        }
                    } else if (DDListFragment.this.b.size() > 1) {
                        DDListFragment.this.a(e.TOTAL);
                    } else {
                        DDListFragment.this.a(e.INVISIBLE);
                    }
                }
                this.f2033a = false;
            }
        }

        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            if (DDListFragment.this.s != null) {
                DDListFragment.this.s.onScroll(absListView, i, i2, i3);
            }
            if (i + i2 == i3 && i3 > 0 && i2 < i3) {
                this.f2033a = true;
            }
        }
    };

    private enum e {
        RETRIEVE,
        TOTAL,
        RETRIEVE_FAILED,
        INVISIBLE
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String str;
        View inflate = layoutInflater.inflate((int) R.layout.ring_list_panel, viewGroup, false);
        this.c = (RelativeLayout) inflate.findViewById(R.id.content_view);
        this.d = (PullRefreshLayout) inflate.findViewById(R.id.swipeRefreshLayout);
        this.d.setOnRefreshListener(this);
        this.f = (ListView) this.c.findViewById(R.id.list_view);
        this.j = layoutInflater.inflate((int) R.layout.get_more_rings, (ViewGroup) null, false);
        if (this.j != null) {
            this.f.addFooterView(this.j);
            this.j.setVisibility(4);
        }
        this.f.setOnScrollListener(this.w);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.l = arguments.getBoolean("support_area", false);
            if (this.l) {
                g();
            }
            this.m = arguments.getBoolean("support_batch", false);
            if (this.m) {
                c();
            }
            this.n = arguments.getBoolean("support_feed_ad", false);
            this.o = arguments.getBoolean("support_lazy_load", false);
            if (!this.o) {
                setUserVisibleHint(true);
            }
            this.p = arguments.getBoolean("support_pull_refresh", false);
            this.d.setEnabled(this.p);
            String string = arguments.getString("userlist_tuid");
            if (string == null) {
                str = "";
            } else {
                str = string;
            }
            String string2 = arguments.getString("adapter_type");
            if ("ring_list_adapter".equals(string2)) {
                this.e = new k(getActivity());
                if (!ai.c(str)) {
                    ((k) this.e).a(str);
                }
                this.f.setOnItemClickListener(new f());
            } else if ("cailing_list_adapter".equals(string2)) {
                this.e = new com.shoujiduoduo.ui.cailing.b(getActivity());
                this.f.setOnItemClickListener(new f());
            } else if ("system_ring_list_adapter".equals(string2)) {
                this.e = new com.shoujiduoduo.ui.mine.changering.a(getActivity());
                this.f.setOnItemClickListener(new f());
            } else if ("collect_list_adapter".equals(string2)) {
                this.e = new e(getActivity());
                this.f.setOnItemClickListener(new b());
            } else if ("artist_list_adapter".equals(string2)) {
                this.e = new b(getActivity());
                this.f.setOnItemClickListener(new a());
            } else if ("user_list_adapter".equals(string2)) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "user list adapter");
                this.e = new l(getActivity());
                if (!ai.c(str)) {
                    ((l) this.e).a(str);
                }
                this.f.setOnItemClickListener(new g());
            } else if ("comment_list_adapter".equals(string2)) {
                this.e = new f(getActivity());
                this.f.setOnItemClickListener(new c());
            } else if ("concern_feeds_adapter".equals(string2)) {
                this.e = new g(getActivity());
                this.f.setOnItemClickListener(new d());
            } else {
                com.shoujiduoduo.base.a.a.c("DDListFragment", "not support adapter type");
            }
        }
        if (this.e != null) {
            if (this.n) {
                this.e.a(true);
            }
            this.e.a();
        }
        this.t = s.a(com.umeng.b.a.a().a(RingDDApp.c(), "feed_ad_gap"), 10);
        this.u = s.a(com.umeng.b.a.a().a(RingDDApp.c(), "feed_ad_start_pos"), 6);
        this.q = true;
        this.r = false;
        if (this.k != null) {
            a(this.k);
        }
        a();
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.v);
        return inflate;
    }

    public void d_() {
        if (!this.g) {
            com.shoujiduoduo.base.a.a.a("DDListFragment", "onRefresh, begin refresh data");
            this.g = true;
            this.b.refreshData();
            return;
        }
        com.shoujiduoduo.base.a.a.a("DDListFragment", "onRefresh, is refreshing, do nothing");
    }

    public void onDestroy() {
        com.shoujiduoduo.base.a.a.a("DDListFragment", "onDestroy, list id:" + (this.b == null ? "no id" : this.b.getListId()));
        super.onDestroy();
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.base.a.a.a("DDListFragment", "onDestroyView, id:" + (this.b == null ? "no id" : this.b.getListId()));
        if (this.e != null) {
            this.e.b();
        }
        this.q = false;
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.v);
    }

    public void onDetach() {
        super.onDetach();
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.q && this.f2043a && this.b != null && this.e != null && !this.r) {
            com.shoujiduoduo.base.a.a.a("DDListFragment", "lazyLoad, loadListData");
            f();
            this.r = true;
        }
    }

    public void a(AbsListView.OnScrollListener onScrollListener) {
        this.s = onScrollListener;
    }

    public void a(View view) {
        if (!this.q) {
            this.k = view;
        } else if (this.k == null || this.f.getHeaderViewsCount() == 0) {
            this.k = view;
            this.f.addHeaderView(this.k);
        }
    }

    public class g implements AdapterView.OnItemClickListener {
        public g() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            com.shoujiduoduo.base.a.a.a("DDListFragment", "onItemClick, UserClickListener");
            if (DDListFragment.this.b != null && j >= 0) {
                Intent intent = new Intent(DDListFragment.this.getActivity(), UserMainPageActivity.class);
                intent.putExtra("tuid", ((UserData) DDListFragment.this.b.get(i)).uid);
                intent.putExtra("fansNum", ((UserData) DDListFragment.this.b.get(i)).followerNum);
                intent.putExtra("followNum", ((UserData) DDListFragment.this.b.get(i)).followingNum);
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    public class c implements AdapterView.OnItemClickListener {
        public c() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long j) {
            com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, new c.a<com.shoujiduoduo.a.c.e>() {
                public void a() {
                    ((com.shoujiduoduo.a.c.e) this.f1284a).a((CommentData) DDListFragment.this.b.get(i));
                }
            });
        }
    }

    public class d implements AdapterView.OnItemClickListener {
        public d() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            MessageData messageData = (MessageData) DDListFragment.this.b.get(i);
            if (messageData.feedtype.equals("comment")) {
                Intent intent = new Intent(RingDDApp.c(), CommentActivity.class);
                intent.putExtra("tuid", messageData.ruid);
                intent.putExtra("rid", messageData.rid);
                RingData a2 = com.shoujiduoduo.a.b.b.b().a(messageData.rid);
                if (a2 != null) {
                    intent.putExtra(SelectCountryActivity.EXTRA_COUNTRY_NAME, a2.name);
                }
                intent.putExtra("from", "feeds_replay");
                intent.putExtra("current_comment", messageData);
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    public class a implements AdapterView.OnItemClickListener {
        public a() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (DDListFragment.this.b != null && j >= 0) {
                RingDDApp.b().a("artistdata", DDListFragment.this.b.get(i));
                Intent intent = new Intent(DDListFragment.this.getActivity(), ArtistRingActivity.class);
                intent.putExtra("parakey", "artistdata");
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    public class b implements AdapterView.OnItemClickListener {
        public b() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (DDListFragment.this.b != null && j >= 0) {
                RingDDApp.b().a("collectdata", DDListFragment.this.b.get(i));
                Intent intent = new Intent(DDListFragment.this.getActivity(), CollectRingActivity.class);
                intent.putExtra("parakey", "collectdata");
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    /* access modifiers changed from: private */
    public int a(int i2) {
        if (this.n && i2 + 1 >= this.u) {
            return (((i2 + 1) - this.u) / this.t) + 1;
        }
        return 0;
    }

    public class f implements AdapterView.OnItemClickListener {
        public f() {
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void onItemClick(android.widget.AdapterView<?> r5, android.view.View r6, int r7, long r8) {
            /*
                r4 = this;
                com.shoujiduoduo.ui.utils.DDListFragment r0 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.DDList r0 = r0.b
                if (r0 == 0) goto L_0x000e
                r0 = 0
                int r0 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
                if (r0 >= 0) goto L_0x000f
            L_0x000e:
                return
            L_0x000f:
                int r0 = (int) r8
                android.widget.Adapter r1 = r5.getAdapter()
                int r1 = r1.getItemViewType(r7)
                if (r1 != 0) goto L_0x0035
                com.shoujiduoduo.util.ab r1 = com.shoujiduoduo.util.ab.a()
                com.shoujiduoduo.player.PlayerService r1 = r1.b()
                if (r1 == 0) goto L_0x000e
                com.shoujiduoduo.ui.utils.DDListFragment r2 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.DDList r2 = r2.b
                com.shoujiduoduo.ui.utils.DDListFragment r3 = com.shoujiduoduo.ui.utils.DDListFragment.this
                int r3 = r3.a(r0)
                int r0 = r0 - r3
                r1.a(r2, r0)
                goto L_0x000e
            L_0x0035:
                com.shoujiduoduo.util.ab r1 = com.shoujiduoduo.util.ab.a()
                com.shoujiduoduo.player.PlayerService r1 = r1.b()
                if (r1 == 0) goto L_0x000e
                com.shoujiduoduo.ui.utils.DDListFragment r2 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.DDList r2 = r2.b
                r1.a(r2, r0)
                goto L_0x000e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.utils.DDListFragment.f.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
        }
    }

    public String b() {
        if (this.b != null) {
            return this.b.getListId();
        }
        return "";
    }

    public void a(DDList dDList) {
        if (dDList != this.b) {
            this.b = null;
            this.b = dDList;
            if (this.q) {
                this.r = false;
                a();
            }
        }
    }

    private void f() {
        this.f.setAdapter((ListAdapter) this.e);
        if (this.b != null) {
            this.e.a(this.b);
            if (this.b.size() == 0) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "loadListData: show loading panel, id:" + this.b.getListId());
                a(d.a.LIST_LOADING);
                if (!this.b.isRetrieving()) {
                    this.b.retrieveData();
                    return;
                }
                return;
            }
            com.shoujiduoduo.base.a.a.a("DDListFragment", "setRingList: Show list content, id:" + this.b.getListId());
            a(d.a.LIST_CONTENT);
            return;
        }
        this.e.a((DDList) null);
        this.e.notifyDataSetChanged();
    }

    private void g() {
        this.i = (Button) this.c.findViewById(R.id.changeArea);
        this.i.setVisibility(0);
        this.i.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                new com.shoujiduoduo.ui.home.a(DDListFragment.this.getActivity(), R.style.DuoDuoDialog, DDListFragment.this.b.getListId()).show();
            }
        });
    }

    public void c() {
        this.h = (Button) this.c.findViewById(R.id.changeBatch);
        this.h.setVisibility(0);
        this.h.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                com.umeng.a.b.b(RingDDApp.c(), "HOT_LIST_CHANGE_BATCH");
                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CHANGE_BATCH, new c.a<m>() {
                    public void a() {
                        ((m) this.f1284a).a(DDListFragment.this.b.getListType(), DDListFragment.this.b.getListId());
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(e eVar) {
        String str;
        if (this.j != null) {
            ImageView imageView = (ImageView) this.j.findViewById(R.id.more_data_loading);
            TextView textView = (TextView) this.j.findViewById(R.id.get_more_text);
            switch (eVar) {
                case RETRIEVE:
                    imageView.setVisibility(0);
                    ((AnimationDrawable) imageView.getBackground()).start();
                    textView.setText((int) R.string.ringlist_retrieving);
                    this.j.setVisibility(0);
                    return;
                case TOTAL:
                    imageView.setVisibility(8);
                    if (this.f.getCount() > (this.f.getHeaderViewsCount() > 0 ? 2 : 1)) {
                        String string = RingDDApp.c().getResources().getString(R.string.total);
                        if (this.b.getListType().equals(ListType.LIST_TYPE.list_artist)) {
                            str = "个歌手";
                        } else if (this.b.getListType().equals(ListType.LIST_TYPE.list_collect)) {
                            str = "个精选集";
                        } else if (this.b.getListType().equals(ListType.LIST_TYPE.list_user)) {
                            str = "个用户";
                        } else if (this.b.getListType().equals(ListType.LIST_TYPE.list_comment)) {
                            str = "个评论";
                        } else if (this.b.getListType().equals(ListType.LIST_TYPE.list_concern_feeds)) {
                            str = "条消息";
                        } else {
                            str = "首铃声";
                        }
                        int count = this.f.getCount();
                        if (this.f.getHeaderViewsCount() > 0) {
                            count -= this.f.getHeaderViewsCount();
                        }
                        textView.setText(string + (count - 1) + str);
                    }
                    if (this.b.getListType().equals(ListType.LIST_TYPE.list_comment)) {
                        this.j.setVisibility(8);
                        return;
                    } else {
                        this.j.setVisibility(0);
                        return;
                    }
                case RETRIEVE_FAILED:
                    imageView.setVisibility(8);
                    textView.setText((int) R.string.ringlist_retrieve_error);
                    this.j.setVisibility(0);
                    return;
                case INVISIBLE:
                    this.j.setVisibility(8);
                    return;
                default:
                    return;
            }
        }
    }

    public void a(d.a aVar) {
        if (this.e != null) {
            this.e.a(aVar);
        }
    }
}
