package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.cmsc.cmmusic.common.CMMusicCallback;
import com.cmsc.cmmusic.common.RingbackManagerInterface;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.u;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;

/* compiled from: MemberOpenDialog */
public class f extends Dialog implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1630a;
    private ImageButton b;
    private ImageView c;
    private Button d;
    private g.b e;
    private RelativeLayout f;
    private EditText g;
    private EditText h;
    private ImageButton i;
    /* access modifiers changed from: private */
    public a j;
    private TextView k;
    /* access modifiers changed from: private */
    public ContentObserver l;
    private TextView m;
    /* access modifiers changed from: private */
    public Handler n = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 112) {
                OrderResult orderResult = (OrderResult) message.obj;
                f.this.a();
                f.this.dismiss();
                if (orderResult == null) {
                    return;
                }
                if (orderResult.getResCode().equals("000000") || orderResult.getResCode().equals("0")) {
                    new AlertDialog.Builder(f.this.f1630a).setTitle("开通彩铃").setMessage("彩铃业务已成功申请开通,稍候收到短信即可正常使用").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                    if (f.this.j != null) {
                        f.this.j.a(a.C0045a.open);
                    }
                    c.a().b(b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                        public void a() {
                            ((com.shoujiduoduo.a.c.c) this.f1284a).a(true, g.b.f2309a);
                        }
                    });
                    u.b("cm:sun_open_cailing", "success, resCode:" + orderResult.getResCode() + ", resMsg:" + orderResult.getResMsg());
                } else if (orderResult.getResCode().equals("000001") || orderResult.getResCode().equals("P000002") || orderResult.getResCode().equals("P000001")) {
                    new AlertDialog.Builder(f.this.f1630a).setTitle("开通彩铃").setMessage("彩铃业务已成功申请开通,稍候收到短信即可正常使用").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                    if (f.this.j != null) {
                        f.this.j.a(a.C0045a.waiting);
                    }
                    c.a().b(b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                        public void a() {
                            ((com.shoujiduoduo.a.c.c) this.f1284a).a(true, g.b.f2309a);
                        }
                    });
                    u.b("cm:sun_open_cailing", "success, resCode:" + orderResult.getResCode() + ", resMsg:" + orderResult.getResMsg());
                } else {
                    String resMsg = orderResult.getResMsg();
                    if (ai.c(resMsg)) {
                        resMsg = "未成功开通彩铃业务！";
                    }
                    new AlertDialog.Builder(f.this.f1630a).setTitle("开通彩铃").setMessage(resMsg).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                    if (f.this.j != null) {
                        f.this.j.a(a.C0045a.close);
                    }
                    u.b("cm:sun_open_cailing", "fail, resCode:" + orderResult.getResCode() + ", resMsg:" + orderResult.getResMsg());
                }
            }
        }
    };
    private ProgressDialog o = null;

    /* compiled from: MemberOpenDialog */
    public interface a {

        /* renamed from: com.shoujiduoduo.ui.cailing.f$a$a  reason: collision with other inner class name */
        /* compiled from: MemberOpenDialog */
        public enum C0045a {
            open,
            waiting,
            close
        }

        void a(C0045a aVar);
    }

    public f(Context context, int i2, g.b bVar, a aVar) {
        super(context, i2);
        this.f1630a = context;
        this.e = bVar;
        this.j = aVar;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_open_member);
        this.k = (TextView) findViewById(R.id.cailing_des);
        this.b = (ImageButton) findViewById(R.id.open_migu_close);
        this.b.setOnClickListener(this);
        setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                if (f.this.l != null) {
                    f.this.f1630a.getContentResolver().unregisterContentObserver(f.this.l);
                }
            }
        });
        this.d = (Button) findViewById(R.id.open_migu_member);
        this.d.setOnClickListener(this);
        TextView textView = (TextView) findViewById(R.id.cailing_des);
        this.c = (ImageView) findViewById(R.id.member_icon);
        this.f = (RelativeLayout) findViewById(R.id.random_key_auth_layout);
        this.m = (TextView) findViewById(R.id.title);
        this.h = (EditText) findViewById(R.id.et_phone_code);
        this.g = (EditText) findViewById(R.id.et_phone_no);
        String b2 = g.b();
        if (!TextUtils.isEmpty(b2)) {
            this.g.setText(b2);
        } else if (!TextUtils.isEmpty(com.shoujiduoduo.a.b.b.g().c().getPhoneNum())) {
            this.g.setText(com.shoujiduoduo.a.b.b.g().c().getPhoneNum());
        }
        if (this.e == g.b.f2309a) {
            this.c.setImageResource(R.drawable.icon_cmcc);
            this.f.setVisibility(8);
            textView.setText((int) R.string.cmcc_member_open_des);
            this.g.setHint((int) R.string.cmcc_num);
        } else if (this.e == g.b.ct) {
            this.c.setImageResource(R.drawable.icon_ctcc);
            this.f.setVisibility(0);
            textView.setText((int) R.string.ctcc_member_open_des);
            this.g.setHint((int) R.string.ctcc_num);
        } else if (this.e == g.b.cu) {
            this.m.setText("开通炫铃业务");
            this.c.setImageResource(R.drawable.icon_cucc);
            this.k.setText((int) R.string.cucc_open_cailing_hint);
            this.f.setVisibility(8);
        }
        this.i = (ImageButton) findViewById(R.id.btn_get_code);
        this.i.setOnClickListener(this);
        this.l = new ag(this.f1630a, new Handler(), this.h, "118100", 6);
        this.f1630a.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.l);
        setCanceledOnTouchOutside(true);
    }

    private void b() {
        a("请稍候...");
        if (com.shoujiduoduo.util.c.b.a().d() == c.n.a.sms_code) {
            com.shoujiduoduo.util.c.b.a().c(new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    f.this.c();
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    f.this.c();
                }
            });
        } else {
            c();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        com.shoujiduoduo.util.c.b.a().a(RingToneDuoduoActivity.a(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                f.this.a();
                RingbackManagerInterface.openRingback(f.this.f1630a, new CMMusicCallback<OrderResult>() {
                    /* renamed from: a */
                    public void operationResult(OrderResult orderResult) {
                        Message message = new Message();
                        message.what = 112;
                        message.obj = orderResult;
                        f.this.n.sendMessage(message);
                    }
                });
            }

            public void b(c.b bVar) {
                super.b(bVar);
                f.this.a();
                d.a("初始化SDK失败");
            }
        });
    }

    private void d() {
        com.shoujiduoduo.util.c.b.a().c("", new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                f.this.a();
                f.this.dismiss();
                new AlertDialog.Builder(f.this.f1630a).setTitle("开通彩铃").setMessage("彩铃业务已成功申请开通,稍候收到短信即可正常使用").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                if (f.this.j != null) {
                    f.this.j.a(a.C0045a.open);
                }
                com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                    public void a() {
                        ((com.shoujiduoduo.a.c.c) this.f1284a).a(true, g.b.f2309a);
                    }
                });
            }

            public void b(c.b bVar) {
                f.this.a();
                f.this.dismiss();
                if (bVar.a().equals("000001")) {
                    new AlertDialog.Builder(f.this.f1630a).setTitle("开通彩铃").setMessage("彩铃业务已成功申请开通,稍候收到短信即可正常使用").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                    if (f.this.j != null) {
                        f.this.j.a(a.C0045a.waiting);
                    }
                    com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                        public void a() {
                            ((com.shoujiduoduo.a.c.c) this.f1284a).a(true, g.b.f2309a);
                        }
                    });
                    return;
                }
                new AlertDialog.Builder(f.this.f1630a).setTitle("开通彩铃").setMessage(bVar.b()).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                if (f.this.j != null) {
                    f.this.j.a(a.C0045a.close);
                }
            }
        });
    }

    private void e() {
        com.shoujiduoduo.util.e.a.a().e("&phone=" + com.shoujiduoduo.a.b.b.g().c().getPhoneNum(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                f.this.a();
                new b.a(f.this.f1630a).b("开通彩铃").a("开通彩铃业务已成功受理，正在为您开通，稍候会短信通知结果").a("确认", (DialogInterface.OnClickListener) null).a().show();
                if (f.this.j != null) {
                    f.this.j.a(a.C0045a.open);
                }
                f.this.dismiss();
            }

            public void b(c.b bVar) {
                super.b(bVar);
                f.this.a();
                if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
                    new b.a(f.this.f1630a).b("开通彩铃").a("开通彩铃业务已成功受理，正在为您开通，稍候会短信通知结果").a("确认", (DialogInterface.OnClickListener) null).a().show();
                    if (f.this.j != null) {
                        f.this.j.a(a.C0045a.open);
                    }
                } else {
                    new b.a(f.this.f1630a).b("开通彩铃").a(bVar.b()).a("确认", (DialogInterface.OnClickListener) null).a().show();
                    if (f.this.j != null) {
                        f.this.j.a(a.C0045a.close);
                    }
                }
                f.this.dismiss();
            }
        });
    }

    private void f() {
        String obj = this.g.getText().toString();
        String obj2 = this.h.getText().toString();
        if (obj == null || !g.f(obj)) {
            this.g.setError("请输入正确的手机号");
        } else if (obj2 == null || obj2.length() != 6) {
            this.h.setError("请输入正确的验证码");
        } else {
            com.shoujiduoduo.util.d.b.a().d(obj, obj2, new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    f.this.a();
                    new b.a(f.this.f1630a).b("开通彩铃").a("开通彩铃业务已成功受理，正在为您开通，稍候会短信通知结果").a("确认", (DialogInterface.OnClickListener) null).a().show();
                    if (f.this.j != null) {
                        f.this.j.a(a.C0045a.open);
                    }
                    f.this.dismiss();
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    f.this.a();
                    if (bVar.a().equals("0002") || bVar.a().equals("9028") || bVar.a().equals("0764") || bVar.a().equals("02000000")) {
                        new b.a(f.this.f1630a).b("开通彩铃").a("开通彩铃基础业务已成功受理，正在为您开通，稍候会短信通知结果").a("确认", (DialogInterface.OnClickListener) null).a().show();
                        if (f.this.j != null) {
                            f.this.j.a(a.C0045a.waiting);
                        }
                    } else if (bVar.a().equals("0501")) {
                        new b.a(f.this.f1630a).b("开通彩铃").a("该手机号的彩铃业务已经是开通状态").a("确认", (DialogInterface.OnClickListener) null).a().show();
                        if (f.this.j != null) {
                            f.this.j.a(a.C0045a.open);
                        }
                    } else {
                        new b.a(f.this.f1630a).b("开通彩铃").a(bVar.b()).a("确认", (DialogInterface.OnClickListener) null).a().show();
                        if (f.this.j != null) {
                            f.this.j.a(a.C0045a.close);
                        }
                    }
                    f.this.dismiss();
                }
            });
        }
    }

    private void b(String str) {
        com.shoujiduoduo.util.d.b.a().d(str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                d.a("验证码短信已发出，请注意查收");
                f.this.a();
            }

            public void b(c.b bVar) {
                super.b(bVar);
                d.a("获取短信验证码失败，请重试");
                f.this.a();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (this.o == null) {
            this.o = new ProgressDialog(this.f1630a);
            this.o.setMessage(str);
            this.o.setIndeterminate(false);
            this.o.setCancelable(false);
            this.o.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.o != null) {
            this.o.dismiss();
            this.o = null;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_get_code:
                String obj = this.g.getText().toString();
                if (obj == null || !g.f(this.g.getText().toString())) {
                    Toast.makeText(this.f1630a, "请输入正确的手机号", 1).show();
                    return;
                } else if (this.e == g.b.ct) {
                    a("请稍候...");
                    b(obj);
                    return;
                } else {
                    return;
                }
            case R.id.open_migu_close:
                dismiss();
                return;
            case R.id.open_migu_member:
                a("请稍候...");
                if (this.e == g.b.f2309a) {
                    if (ad.a().b("cm_sunshine_sdk_enable")) {
                        b();
                        return;
                    } else {
                        d();
                        return;
                    }
                } else if (this.e == g.b.ct) {
                    f();
                    return;
                } else if (this.e == g.b.cu) {
                    e();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
