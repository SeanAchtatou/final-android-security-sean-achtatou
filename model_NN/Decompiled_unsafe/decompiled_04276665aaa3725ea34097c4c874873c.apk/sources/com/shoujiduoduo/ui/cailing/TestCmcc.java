package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import com.cmsc.cmmusic.common.RingbackManagerInterface;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.e;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.widget.d;
import java.util.List;

public class TestCmcc extends BaseActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private String f1487a = "810027210086";
    private EditText b;
    /* access modifiers changed from: private */
    public EditText c;
    private ContentObserver d;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_test_cmcc);
        this.b = (EditText) findViewById(R.id.music_id);
        this.b.setText(this.f1487a);
        this.c = (EditText) findViewById(R.id.phone_num);
        findViewById(R.id.query_vip_state).setOnClickListener(this);
        findViewById(R.id.open_vip).setOnClickListener(this);
        findViewById(R.id.vip_order).setOnClickListener(this);
        findViewById(R.id.close_vip).setOnClickListener(this);
        findViewById(R.id.check_sdk_init_status).setOnClickListener(this);
        findViewById(R.id.get_sms_code).setOnClickListener(this);
        findViewById(R.id.check_base_cailing_status).setOnClickListener(this);
        findViewById(R.id.query_caililng_and_vip).setOnClickListener(this);
        findViewById(R.id.query_cailing_url).setOnClickListener(this);
        findViewById(R.id.query_ring_box).setOnClickListener(this);
        findViewById(R.id.query_default_ring).setOnClickListener(this);
        findViewById(R.id.phone_sms_init).setOnClickListener(this);
        findViewById(R.id.query_ring_circle).setOnClickListener(this);
        this.d = new ag(this, new Handler(), (EditText) findViewById(R.id.random_key), "10658830");
        getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.d);
    }

    public void onClick(View view) {
        String obj = this.c.getText().toString();
        if (ai.c(obj)) {
            d.a("请输入查询的手机号");
            return;
        }
        switch (view.getId()) {
            case R.id.phone_sms_init:
                a(obj, g.b.f2309a);
                return;
            case R.id.check_sdk_init_status:
                b.a().a(new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        TestCmcc.this.a(bVar, "SDK wap/sms初始化成功");
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        b.a().b(new com.shoujiduoduo.util.b.b() {
                            public void a(c.b bVar) {
                                super.a(bVar);
                                TestCmcc.this.a(bVar, "sdk 通过手机号验证码初始化成功");
                            }

                            public void b(c.b bVar) {
                                super.b(bVar);
                                TestCmcc.this.a(bVar);
                            }
                        });
                    }
                });
                return;
            case R.id.check_base_cailing_status:
                b.a().i(this.c.getText().toString(), new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        TestCmcc.this.a(bVar);
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        TestCmcc.this.a(bVar);
                    }
                });
                return;
            case R.id.query_vip_state:
                b.a().d(this.c.getText().toString(), new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        TestCmcc.this.a(bVar);
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        TestCmcc.this.a(bVar);
                    }
                });
                return;
            case R.id.query_caililng_and_vip:
                b.a().e(this.c.getText().toString(), new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        c.d dVar = (c.d) bVar;
                        TestCmcc.this.a(bVar, "cailing is open:" + dVar.d() + ", is vip open:" + dVar.e());
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        TestCmcc.this.a(bVar);
                    }
                });
                return;
            case R.id.vip_order:
            case R.id.open_vip:
            case R.id.get_sms_code:
            case R.id.query_cailing_url:
            default:
                return;
            case R.id.close_vip:
                new AlertDialog.Builder(this).setMessage("确定关闭包月吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.a().f(new com.shoujiduoduo.util.b.b() {
                            public void a(c.b bVar) {
                                super.a(bVar);
                                TestCmcc.this.a(bVar);
                            }

                            public void b(c.b bVar) {
                                super.b(bVar);
                                TestCmcc.this.a(bVar);
                            }
                        });
                    }
                }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                return;
            case R.id.query_ring_box:
                b.a().g(new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        List<c.ae> d = ((c.z) bVar).d();
                        if (d != null) {
                            StringBuilder sb = new StringBuilder();
                            for (c.ae next : d) {
                                sb.append(next.d()).append("-").append(next.c());
                                sb.append(" | ");
                            }
                            TestCmcc.this.a(bVar, sb.toString());
                            return;
                        }
                        TestCmcc.this.a(bVar, "查询不到彩铃库");
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        TestCmcc.this.a(bVar);
                    }
                });
                return;
            case R.id.query_default_ring:
                b.a().h(obj, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        c.z zVar = (c.z) bVar;
                        if (zVar.a() == null || zVar.d() == null || zVar.d().size() <= 0) {
                            TestCmcc.this.a(bVar, "查询失败");
                            return;
                        }
                        String b = zVar.d().get(0).b();
                        String c = zVar.d().get(0).c();
                        TestCmcc.this.a(bVar, "彩铃id:" + b + " \r\n ringname:" + c + "\r\n singername:" + zVar.d().get(0).d());
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        TestCmcc.this.a(bVar);
                    }
                });
                return;
            case R.id.query_ring_circle:
                a.a("testcmcc", "ring_cilcle:" + RingbackManagerInterface.queryRingbackGears(RingDDApp.c()));
                return;
        }
    }

    private void a(String str, g.b bVar) {
        new e(this, R.style.DuoDuoDialog, str, bVar, new e.a() {
            public void a(String str) {
                UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                if (!c.isLogin()) {
                    c.setUserName(str);
                    c.setUid("phone_" + str);
                }
                c.setPhoneNum(str);
                c.setLoginStatus(1);
                com.shoujiduoduo.a.b.b.g().a(c);
                TestCmcc.this.c.setText(str);
                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                    public void a() {
                        ((v) this.f1284a).a(1, true, "", "");
                    }
                });
                d.a("初始化成功，可以进行功能调用了。。。");
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar) {
        new AlertDialog.Builder(this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar, String str) {
        new AlertDialog.Builder(this).setMessage(bVar.toString() + " , " + str).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }
}
