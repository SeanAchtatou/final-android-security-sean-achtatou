package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.b.c.j;
import com.shoujiduoduo.base.bean.DDList;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.e;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.ui.utils.m;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.am;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.l;

/* compiled from: CailingListAdapter */
public class b extends d {

    /* renamed from: a  reason: collision with root package name */
    boolean f1568a = false;
    public com.shoujiduoduo.util.b.b b = new com.shoujiduoduo.util.b.b() {
        public void a(c.b bVar) {
            b.this.c();
            String unused = b.this.f = b.this.e;
            com.shoujiduoduo.util.widget.d.a("已设置为默认彩铃");
            af.c(b.this.j, "DEFAULT_CAILING_ID", b.this.f);
            b.this.notifyDataSetChanged();
            com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, new c.a<p>() {
                public void a() {
                    RingData a2 = b.this.d.get(b.this.g);
                    if (a2 != null) {
                        ((p) this.f1284a).a(16, a2);
                    }
                }
            });
        }

        public void b(c.b bVar) {
            String str;
            b.this.c();
            if (bVar.a().equals("999018") || bVar.a().equals("999019")) {
                new e(b.this.j, R.style.DuoDuoDialog, af.a(RingDDApp.c(), "user_phone_num", ""), g.b.f2309a, new e.a() {
                    public void a(String str) {
                        com.shoujiduoduo.util.c.b.a().f(b.this.d.get(b.this.g).cid, b.this.b);
                    }
                }).show();
                return;
            }
            if (bVar != null) {
                str = bVar.b();
            } else {
                str = "对不起，彩铃设置失败。";
            }
            Toast.makeText(b.this.j, str, 1).show();
        }
    };
    /* access modifiers changed from: private */
    public j d;
    /* access modifiers changed from: private */
    public String e = "";
    /* access modifiers changed from: private */
    public String f = "";
    /* access modifiers changed from: private */
    public int g = -1;
    private LayoutInflater h;
    /* access modifiers changed from: private */
    public g.b i;
    /* access modifiers changed from: private */
    public Context j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public boolean m;
    private Handler n;
    private o o = new o() {
        public void a(String str, int i) {
            if (b.this.d != null && b.this.d.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "onSetPlay, listid:" + str);
                if (str.equals(b.this.d.getListId())) {
                    b.this.f1568a = true;
                    int unused = b.this.g = i;
                } else {
                    b.this.f1568a = false;
                }
                b.this.notifyDataSetChanged();
            }
        }

        public void b(String str, int i) {
            if (b.this.d != null && b.this.d.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "onCanclePlay, listId:" + str);
                b.this.f1568a = false;
                int unused = b.this.g = i;
                b.this.notifyDataSetChanged();
            }
        }

        public void a(String str, int i, int i2) {
            if (b.this.d != null && b.this.d.getListId().equals(str)) {
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "onStatusChange, listid:" + str);
                b.this.notifyDataSetChanged();
            }
        }
    };
    private View.OnClickListener p = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = ab.a().b();
            if (b == null) {
                return;
            }
            if (b.a() == 3) {
                b.n();
            } else {
                b.i();
            }
        }
    };
    private View.OnClickListener q = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = ab.a().b();
            if (b != null) {
                b.j();
            }
        }
    };
    private View.OnClickListener r = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b = ab.a().b();
            if (b != null) {
                b.a(b.this.d, b.this.g);
            }
        }
    };
    private View.OnClickListener s = new View.OnClickListener() {
        public void onClick(View view) {
            new AlertDialog.Builder(b.this.j).setTitle("提示").setMessage("删除后将无法使用该彩铃，确定删除吗？").setPositiveButton("确认", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    com.shoujiduoduo.base.a.a.a("CailingListAdapter", "delete cailing");
                    b.this.a("正在删除...");
                    RingData a2 = b.this.d.get(b.this.g);
                    PlayerService b = ab.a().b();
                    if (b != null) {
                        b.e();
                    }
                    if (b.this.i == g.b.f2309a) {
                        com.shoujiduoduo.util.c.b.a().g(a2.cid, b.this.t);
                    } else if (b.this.i == g.b.ct) {
                        com.shoujiduoduo.util.d.b.a().e(af.a(RingDDApp.c(), "pref_phone_num", ""), a2.ctcid, b.this.t);
                    } else if (b.this.i == g.b.cu) {
                        com.shoujiduoduo.util.e.a.a().f(a2.cucid, b.this.t);
                    }
                }
            }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
        }
    };
    /* access modifiers changed from: private */
    public com.shoujiduoduo.util.b.b t = new com.shoujiduoduo.util.b.b() {
        public void a(c.b bVar) {
            super.a(bVar);
            com.shoujiduoduo.a.a.c.a().b(new c.b() {
                public void a() {
                    b.this.c();
                }
            });
            com.shoujiduoduo.base.a.a.a("CailingListAdapter", "删除铃声成功，设置需要获取铃音库标识true");
            int unused = b.this.g = -1;
            af.b(RingDDApp.c(), "NeedUpdateCaiLingLib", 1);
            com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                public void a() {
                    ((com.shoujiduoduo.a.c.c) this.f1284a).b(g.s());
                }
            });
        }

        public void b(c.b bVar) {
            super.b(bVar);
            com.shoujiduoduo.a.a.c.a().b(new c.b() {
                public void a() {
                    b.this.c();
                    new AlertDialog.Builder(b.this.j).setTitle("").setMessage("操作失败，请稍后再试").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                }
            });
        }
    };
    private View.OnClickListener u = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("CailingListAdapter", "set default cailing");
            b.this.a("正在设置...");
            RingData a2 = b.this.d.get(b.this.g);
            if (a2 != null) {
                if (b.this.i.equals(g.b.f2309a)) {
                    String unused = b.this.e = a2.cid;
                    com.shoujiduoduo.util.c.b.a().f(a2.cid, b.this.b);
                } else if (b.this.i.equals(g.b.ct)) {
                    String unused2 = b.this.e = a2.ctcid;
                    com.shoujiduoduo.util.d.b.a().c(af.a(RingDDApp.c(), "pref_phone_num"), a2.ctcid, b.this.b);
                } else {
                    String unused3 = b.this.e = a2.cucid;
                    com.shoujiduoduo.util.e.a.a().a(a2.cucid, b.this.k, b.this.l, b.this.b);
                }
            }
        }
    };
    private View.OnClickListener v = new View.OnClickListener() {
        public void onClick(View view) {
            ListType.LIST_TYPE list_type;
            com.shoujiduoduo.base.a.a.a("CailingListAdapter", "give cailing");
            RingData a2 = b.this.d.get(b.this.g);
            if (a2 != null) {
                Bundle bundle = new Bundle();
                Intent intent = new Intent(b.this.j, GiveCailingActivity.class);
                bundle.putParcelable("ringdata", a2);
                intent.putExtras(bundle);
                intent.putExtra("listid", "cailingmanage");
                if (g.t()) {
                    intent.putExtra("operator_type", 0);
                    list_type = ListType.LIST_TYPE.list_ring_cmcc;
                } else if (g.v()) {
                    intent.putExtra("operator_type", 1);
                    list_type = ListType.LIST_TYPE.list_ring_ctcc;
                } else {
                    list_type = ListType.LIST_TYPE.list_ring_cmcc;
                }
                intent.putExtra("listtype", list_type.toString());
                b.this.j.startActivity(intent);
                am.a(a2.rid, 7, "&cucid=" + a2.cucid);
            }
        }
    };
    private p w = new p() {
        public void a(int i, RingData ringData) {
            if (i == 16 && b.this.d != null) {
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "default cailing change, refresh list");
                b.this.notifyDataSetChanged();
            }
        }
    };
    private com.shoujiduoduo.a.c.c x = new com.shoujiduoduo.a.c.c() {
        public void a(boolean z, g.b bVar) {
            if (b.this.d != null && b.this.d.getListId().equals("cmcc_cailing")) {
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "on Cailing Status change, open:" + z);
                boolean unused = b.this.m = z;
                if (z) {
                    b.this.a(d.a.LIST_LOADING);
                    b.this.d.retrieveData();
                    return;
                }
                b.this.a(d.a.LIST_FAILED);
                b.this.notifyDataSetChanged();
            }
        }

        public void a(g.b bVar) {
            if (b.this.d != null) {
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "onOrderCailing");
                b.this.a(d.a.LIST_LOADING);
                b.this.d.reloadData();
            }
        }

        public void b(g.b bVar) {
            if (b.this.d != null) {
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "onDeleteCailing");
                b.this.a(d.a.LIST_LOADING);
                b.this.d.reloadData();
            }
        }
    };
    private x y = new x() {
        public void a(int i) {
            if (b.this.d != null) {
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "vipType:" + i);
                if ((i == 1 && !b.this.d.getListType().equals(ListType.LIST_TYPE.list_ring_cmcc)) || ((i == 3 && !b.this.d.getListType().equals(ListType.LIST_TYPE.list_ring_cucc)) || (i == 2 && !b.this.d.getListType().equals(ListType.LIST_TYPE.list_ring_ctcc)))) {
                    switch (i) {
                        case 1:
                            j unused = b.this.d = new j(ListType.LIST_TYPE.list_ring_cmcc, "", false, "");
                            break;
                        case 2:
                            j unused2 = b.this.d = new j(ListType.LIST_TYPE.list_ring_ctcc, "", false, "");
                            break;
                        case 3:
                            j unused3 = b.this.d = new j(ListType.LIST_TYPE.list_ring_cucc, "", false, "");
                            break;
                    }
                    com.shoujiduoduo.base.a.a.a("CailingListAdapter", "vipType:" + i + ", cur list id:" + b.this.d.getListId());
                    b.this.c = d.a.LIST_LOADING;
                    b.this.notifyDataSetChanged();
                    b.this.d.reloadData();
                }
            }
        }
    };
    private ProgressDialog z = null;

    public b(Context context) {
        this.j = context;
        this.h = LayoutInflater.from(context);
        this.n = new Handler();
    }

    public void a() {
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.o);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, this.x);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.y);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.w);
    }

    public void b() {
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.o);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, this.x);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.y);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.w);
    }

    private void d() {
        com.shoujiduoduo.base.a.a.a("CailingListAdapter", "begin queryUserRingBox");
        if (this.i.equals(g.b.f2309a)) {
            com.shoujiduoduo.util.c.b.a().h("", new a());
        } else if (this.i.equals(g.b.ct)) {
            com.shoujiduoduo.util.d.b.a().f(af.a(RingDDApp.c(), "pref_phone_num"), new a());
        } else if (this.i.equals(g.b.cu)) {
            com.shoujiduoduo.util.e.a.a().h(new a());
        }
    }

    /* compiled from: CailingListAdapter */
    private class a extends com.shoujiduoduo.util.b.b {
        private a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.cailing.b.a(com.shoujiduoduo.ui.cailing.b, boolean):boolean
         arg types: [com.shoujiduoduo.ui.cailing.b, int]
         candidates:
          com.shoujiduoduo.ui.cailing.b.a(com.shoujiduoduo.ui.cailing.b, int):int
          com.shoujiduoduo.ui.cailing.b.a(com.shoujiduoduo.ui.cailing.b, com.shoujiduoduo.b.c.j):com.shoujiduoduo.b.c.j
          com.shoujiduoduo.ui.cailing.b.a(com.shoujiduoduo.ui.cailing.b, java.lang.String):java.lang.String
          com.shoujiduoduo.ui.cailing.b.a(android.view.View, int):void
          com.shoujiduoduo.ui.cailing.b.a(com.shoujiduoduo.ui.cailing.b, boolean):boolean */
        public void a(c.b bVar) {
            if (b.this.i.equals(g.b.cu)) {
                if (bVar != null && (bVar instanceof c.s)) {
                    c.s sVar = (c.s) bVar;
                    if (sVar.d != null) {
                        for (int i = 0; i < sVar.d.length; i++) {
                            if (sVar.d[i].c.equals("0")) {
                                boolean unused = b.this.k = true;
                                String unused2 = b.this.l = sVar.d[i].f2231a;
                                String unused3 = b.this.f = sVar.d[i].d;
                                af.c(b.this.j, "DEFAULT_CAILING_ID", b.this.f);
                                b.this.notifyDataSetChanged();
                                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "default cucc cailing id:" + b.this.f);
                                return;
                            }
                        }
                    }
                }
            } else if (bVar != null && (bVar instanceof c.z)) {
                c.z zVar = (c.z) bVar;
                if (zVar.a() == null || zVar.d() == null || zVar.d().size() <= 0) {
                    com.shoujiduoduo.base.a.a.c("CailingListAdapter", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
                    return;
                }
                String unused4 = b.this.f = zVar.d().get(0).b();
                af.c(b.this.j, "DEFAULT_CAILING_ID", b.this.f);
                b.this.notifyDataSetChanged();
                com.shoujiduoduo.base.a.a.a("CailingListAdapter", "default cailing id:" + b.this.f);
            }
        }

        public void b(c.b bVar) {
            com.shoujiduoduo.base.a.a.c("CailingListAdapter", "查询默认彩铃失败，code:" + bVar.a() + " msg:" + bVar.b());
        }
    }

    public int getCount() {
        if (this.d == null) {
            return 0;
        }
        if (this.c == d.a.LIST_CONTENT) {
            return this.d.size();
        }
        return 1;
    }

    public Object getItem(int i2) {
        if (this.d != null && i2 >= 0 && i2 < this.d.size()) {
            return this.d.get(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private void a(View view, int i2) {
        RingData a2 = this.d.get(i2);
        TextView textView = (TextView) m.a(view, R.id.cailing_item_valid_date);
        TextView textView2 = (TextView) m.a(view, R.id.cailing_item_default_tip);
        ((TextView) m.a(view, R.id.cailing_item_song_name)).setText(a2.name);
        ((TextView) m.a(view, R.id.cailing_item_artist)).setText(a2.artist);
        String str = "";
        String str2 = "";
        if (this.i.equals(g.b.f2309a)) {
            str = a2.valid;
            str2 = a2.cid;
        } else if (this.i.equals(g.b.ct)) {
            str = a2.ctvalid;
            str2 = a2.ctcid;
        } else if (this.i.equals(g.b.cu)) {
            str = a2.cuvalid;
            str2 = a2.cucid;
        }
        textView.setText(String.format("有效期:" + str, new Object[0]));
        if (str.equals("")) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
        textView2.setText("当前彩铃");
        this.f = af.a(RingDDApp.c(), "DEFAULT_CAILING_ID", "");
        if (str2.equals(this.f)) {
            textView2.setVisibility(0);
        } else {
            textView2.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (this.d == null) {
            return null;
        }
        switch (getItemViewType(i2)) {
            case 0:
                if (i2 >= this.d.size()) {
                    return view;
                }
                if (view == null) {
                    view = this.h.inflate((int) R.layout.listitem_cailing_manage, viewGroup, false);
                }
                a(view, i2);
                ProgressBar progressBar = (ProgressBar) m.a(view, R.id.cailing_item_download_progress);
                TextView textView = (TextView) m.a(view, R.id.cailing_item_serial_number);
                ImageButton imageButton = (ImageButton) m.a(view, R.id.cailing_item_play);
                ImageButton imageButton2 = (ImageButton) m.a(view, R.id.cailing_item_pause);
                ImageButton imageButton3 = (ImageButton) m.a(view, R.id.cailing_item_failed);
                imageButton3.setOnClickListener(this.r);
                imageButton.setOnClickListener(this.p);
                imageButton2.setOnClickListener(this.q);
                if (i2 != this.g || !this.f1568a) {
                    ((Button) m.a(view, R.id.cailing_item_set_default)).setVisibility(8);
                    ((Button) m.a(view, R.id.cailing_item_give)).setVisibility(8);
                    ((Button) m.a(view, R.id.cailing_item_delete)).setVisibility(8);
                    textView.setText(Integer.toString(i2 + 1));
                    textView.setVisibility(0);
                    progressBar.setVisibility(4);
                    imageButton.setVisibility(4);
                    imageButton2.setVisibility(4);
                    imageButton3.setVisibility(4);
                    return view;
                }
                Button button = (Button) m.a(view, R.id.cailing_item_set_default);
                Button button2 = (Button) m.a(view, R.id.cailing_item_give);
                Button button3 = (Button) m.a(view, R.id.cailing_item_delete);
                button.setVisibility(0);
                button2.setVisibility(8);
                button3.setVisibility(0);
                button.setOnClickListener(this.u);
                button2.setOnClickListener(this.v);
                button3.setOnClickListener(this.s);
                textView.setVisibility(4);
                progressBar.setVisibility(4);
                imageButton.setVisibility(4);
                imageButton2.setVisibility(4);
                imageButton3.setVisibility(4);
                PlayerService b2 = ab.a().b();
                if (b2 == null) {
                    return view;
                }
                switch (b2.a()) {
                    case 1:
                        progressBar.setVisibility(0);
                        return view;
                    case 2:
                        imageButton2.setVisibility(0);
                        return view;
                    case 3:
                    case 4:
                    case 5:
                        imageButton.setVisibility(0);
                        return view;
                    case 6:
                        imageButton3.setVisibility(0);
                        return view;
                    default:
                        return view;
                }
            case 1:
            default:
                return view;
            case 2:
                View inflate = this.h.inflate((int) R.layout.list_loading, viewGroup, false);
                if (l.b > 0) {
                    AbsListView.LayoutParams layoutParams = (AbsListView.LayoutParams) inflate.getLayoutParams();
                    layoutParams.width = l.b;
                    layoutParams.height = l.b;
                    inflate.setLayoutParams(layoutParams);
                }
                final AnimationDrawable animationDrawable = (AnimationDrawable) ((ImageView) inflate.findViewById(R.id.loading)).getBackground();
                this.n.post(new Runnable() {
                    public void run() {
                        animationDrawable.start();
                    }
                });
                return inflate;
            case 3:
                View inflate2 = this.h.inflate((int) R.layout.list_failed, viewGroup, false);
                if (l.b > 0) {
                    AbsListView.LayoutParams layoutParams2 = (AbsListView.LayoutParams) inflate2.getLayoutParams();
                    layoutParams2.width = l.b;
                    layoutParams2.height = l.b;
                    inflate2.setLayoutParams(layoutParams2);
                }
                inflate2.findViewById(R.id.network_fail_layout).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        b.this.a(d.a.LIST_LOADING);
                        b.this.d.retrieveData();
                    }
                });
                return inflate2;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (this.z == null) {
            this.z = new ProgressDialog(this.j);
            this.z.setMessage(str);
            this.z.setIndeterminate(false);
            this.z.setCancelable(false);
            this.z.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.z != null) {
            this.z.dismiss();
            this.z = null;
        }
    }

    public void a(boolean z2) {
    }

    public void a(DDList dDList) {
        if (this.d != dDList) {
            this.d = (j) dDList;
            notifyDataSetChanged();
        }
        if (dDList.getListId().equals("cmcc_cailing")) {
            this.i = g.b.f2309a;
        } else if (dDList.getListId().equals("ctcc_cailing")) {
            this.i = g.b.ct;
        } else if (dDList.getListId().equals("cucc_cailing")) {
            this.i = g.b.cu;
        }
        d();
    }
}
