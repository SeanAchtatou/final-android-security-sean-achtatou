package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.cmsc.cmmusic.common.CMMusicCallback;
import com.cmsc.cmmusic.common.RingbackManagerInterface;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.Result;
import com.igexin.download.Downloads;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.base.bean.ListType;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.u;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;

/* compiled from: BuyCailingDialog */
public class a extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1555a;
    private ImageButton b;
    private Button c;
    private ListView d;
    /* access modifiers changed from: private */
    public RingData e;
    private String f;
    private ListType.LIST_TYPE g;
    /* access modifiers changed from: private */
    public g.b h;
    /* access modifiers changed from: private */
    public EditText i;
    private EditText j;
    private ImageButton k;
    private RelativeLayout l;
    private TextView m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public Handler o = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 111) {
                a.this.a((OrderResult) message.obj);
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog p = null;

    public a(Context context, int i2, g.b bVar, boolean z) {
        super(context, i2);
        this.f1555a = context;
        this.h = bVar;
        this.n = z;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        PlayerService.a(true);
        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "onStart, threadId:" + Thread.currentThread().getId());
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        PlayerService.a(false);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_buy_cailing);
        this.b = (ImageButton) findViewById(R.id.buy_cailing_close);
        this.b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                a.this.dismiss();
            }
        });
        setCanceledOnTouchOutside(true);
        this.d = (ListView) findViewById(R.id.cailing_info_list);
        this.d.setAdapter((ListAdapter) new C0042a());
        this.d.setEnabled(false);
        this.c = (Button) findViewById(R.id.buy_cailing);
        this.c.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (a.this.h == g.b.f2309a) {
                    a.this.a("请稍候...");
                    a.this.b();
                    return;
                }
                d.a("当前手机卡运营商类型错误！");
            }
        });
        this.j = (EditText) findViewById(R.id.et_phone_code);
        this.i = (EditText) findViewById(R.id.et_phone_no);
        this.m = (TextView) findViewById(R.id.buy_cailing_tips);
        this.l = (RelativeLayout) findViewById(R.id.random_key_auth_layout);
        if (this.h == g.b.f2309a) {
            this.l.setVisibility(8);
            this.m.setText((int) R.string.buy_cailing_hint_cmcc);
        } else if (this.h == g.b.ct) {
            this.l.setVisibility(0);
            this.m.setText((int) R.string.buy_cailing_hint_ctcc);
            this.i.setHint((int) R.string.ctcc_num);
        } else {
            this.l.setVisibility(0);
            this.m.setText((int) R.string.buy_cailing_hint_cmcc);
            this.m.setVisibility(8);
            this.i.setHint((int) R.string.cmcc_num);
        }
        this.k = (ImageButton) findViewById(R.id.btn_get_code);
        this.k.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String obj = a.this.i.getText().toString();
                if (!g.f(a.this.i.getText().toString())) {
                    Toast.makeText(a.this.f1555a, "请输入正确的手机号", 1).show();
                } else if (a.this.h != g.b.ct) {
                    a.this.b(obj);
                }
            }
        });
        String phoneNum = b.g().c().getPhoneNum();
        this.i.setText(phoneNum);
        if (!ai.c(phoneNum) && !com.shoujiduoduo.util.c.b.a().a(phoneNum).equals("")) {
            this.k.setVisibility(8);
            this.j.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        com.shoujiduoduo.util.c.b.a().a(str, new com.shoujiduoduo.util.b.b() {
            public void b(c.b bVar) {
                super.b(bVar);
                d.a("成功发送验证码失败，请重试");
            }

            public void a(c.b bVar) {
                super.a(bVar);
                d.a("成功发送验证码，请查收");
            }
        });
    }

    /* access modifiers changed from: private */
    public void b() {
        a("请稍候...");
        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "id1:" + Thread.currentThread().getId());
        com.shoujiduoduo.util.c.b.a().a(RingToneDuoduoActivity.a(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                com.shoujiduoduo.util.c.b.a().b(a.this.e.cid, "", new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        BizInfo bizInfo;
                        super.a(bVar);
                        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "getringbadkPolicy success");
                        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "id2:" + Thread.currentThread().getId());
                        c.l lVar = (c.l) bVar;
                        if (a.this.n) {
                            bizInfo = lVar.d;
                        } else {
                            bizInfo = lVar.f2241a;
                        }
                        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "bizCode:" + bizInfo.getBizCode() + ",bizType:" + bizInfo.getBizType() + ", salePrice:" + bizInfo.getSalePrice() + ", monLevel:" + lVar.g + ", hode2:" + bizInfo.getHold2());
                        try {
                            RingbackManagerInterface.buyRingback(a.this.f1555a, a.this.e.cid, bizInfo.getBizCode(), bizInfo.getBizType(), bizInfo.getSalePrice(), lVar.g, bizInfo.getHold2(), new CMMusicCallback<OrderResult>() {
                                /* renamed from: a */
                                public void operationResult(OrderResult orderResult) {
                                    Message message = new Message();
                                    message.what = 111;
                                    message.obj = orderResult;
                                    a.this.o.sendMessage(message);
                                }
                            });
                        } catch (Throwable th) {
                            com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "buyRingback crash");
                            th.printStackTrace();
                            a.this.a();
                            new AlertDialog.Builder(a.this.f1555a).setTitle("订购彩铃").setMessage("订购失败, 原因：SDK初始化失败").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                        }
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        a.this.a();
                        new AlertDialog.Builder(a.this.f1555a).setTitle("订购彩铃").setMessage("订购失败，原因：" + bVar.toString()).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                    }
                });
            }

            public void b(c.b bVar) {
                super.b(bVar);
                a.this.a();
                d.a("初始化SDK失败！");
            }
        });
    }

    private String c() {
        if (this.e == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("&rid=").append(this.e.rid).append("&from=").append(this.f);
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.cailing.a.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, android.app.ProgressDialog):android.app.ProgressDialog
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, com.cmsc.cmmusic.common.data.Result):void
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, java.lang.String):void
      com.shoujiduoduo.ui.cailing.a.a(boolean, boolean):void */
    /* access modifiers changed from: private */
    public void a(Result result) {
        if (result == null || result.getResCode() == null) {
            a();
            d.a("订购没有成功！");
            return;
        }
        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "cmcc order result:" + result.toString());
        if (result.getResCode().equals("000000") || result.getResCode().equals("0")) {
            com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "订购成功");
            a(true, false);
            dismiss();
            u.a("cm:sun_buy_cailing", "success, resCode:" + result.getResCode() + ", resMsg:" + result.getResMsg(), c());
        } else if (result.getResCode().equals("302011")) {
            com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "重复订购");
            a(false, false);
            dismiss();
            u.a("cm:sun_buy_cailing", "success, resCode:" + result.getResCode() + ", resMsg:" + result.getResMsg(), c());
        } else if (result.getResCode().equals("100002")) {
            a(false, true);
            dismiss();
            u.a("cm:sun_buy_cailing", "success, resCode:" + result.getResCode() + ", resMsg:" + result.getResMsg(), c());
        } else {
            com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "订购失败");
            a();
            String resMsg = result.getResMsg();
            if (result.getResCode().equals("303023")) {
                resMsg = "中国移动提醒您，您的彩铃库已达到上限，建议您去\"彩铃管理\"清理部分不用的彩铃后，再重新购买。";
            }
            if (ai.c(resMsg)) {
                resMsg = "订购未成功!";
            }
            try {
                new AlertDialog.Builder(this.f1555a).setTitle("订购彩铃").setMessage(resMsg).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
            } catch (Exception e2) {
                com.umeng.a.b.a(this.f1555a, "AlertDialog Failed!");
            }
            u.a("cm:sun_buy_cailing", "fail, resCode:" + result.getResCode() + ", resMsg:" + result.getResMsg(), c());
        }
    }

    private void a(final boolean z, final boolean z2) {
        com.shoujiduoduo.util.c.b.a().f(this.e.cid, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                a.this.a();
                try {
                    new AlertDialog.Builder(a.this.f1555a).setTitle("订购彩铃").setMessage("恭喜，订购成功，已帮您设置为默认彩铃").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                } catch (Exception e) {
                }
                af.c(a.this.f1555a, "DEFAULT_CAILING_ID", a.this.e.cid);
                com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, new c.a<p>() {
                    public void a() {
                        ((p) this.f1284a).a(16, a.this.e);
                    }
                });
                if (z) {
                    af.b(a.this.f1555a, "NeedUpdateCaiLingLib", 1);
                    com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                        public void a() {
                            ((com.shoujiduoduo.a.c.c) this.f1284a).a(g.b.f2309a);
                        }
                    });
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                a.this.a();
                if (z2) {
                    new b.a(a.this.f1555a).b("设置彩铃").a("设置未成功, 请打开“我的”->“彩铃”，删除几首不用的彩铃后再试试。彩铃有最大数量限制。").a("确定", (DialogInterface.OnClickListener) null).a().show();
                } else {
                    new b.a(a.this.f1555a).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                }
            }
        });
    }

    public void a(RingData ringData, String str, ListType.LIST_TYPE list_type) {
        this.e = ringData;
        this.f = str;
        this.g = list_type;
    }

    /* access modifiers changed from: package-private */
    public void a(final String str) {
        this.o.post(new Runnable() {
            public void run() {
                if (a.this.p == null) {
                    ProgressDialog unused = a.this.p = new ProgressDialog(a.this.f1555a);
                    a.this.p.setMessage(str);
                    a.this.p.setIndeterminate(false);
                    a.this.p.setCancelable(false);
                    a.this.p.show();
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.o.post(new Runnable() {
            public void run() {
                if (a.this.p != null) {
                    a.this.p.dismiss();
                    ProgressDialog unused = a.this.p = (ProgressDialog) null;
                }
            }
        });
    }

    /* renamed from: com.shoujiduoduo.ui.cailing.a$a  reason: collision with other inner class name */
    /* compiled from: BuyCailingDialog */
    private class C0042a extends BaseAdapter {
        private C0042a() {
        }

        public int getCount() {
            return 4;
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            String str;
            View inflate = a.this.getLayoutInflater().inflate((int) R.layout.listitem_buy_cailing, viewGroup, false);
            TextView textView = (TextView) inflate.findViewById(R.id.cailing_info_des);
            TextView textView2 = (TextView) inflate.findViewById(R.id.cailing_info_content);
            switch (i) {
                case 0:
                    textView.setText("歌曲名");
                    textView2.setText(a.this.e.name);
                    break;
                case 1:
                    textView.setText("歌   手");
                    textView2.setText(a.this.e.artist);
                    break;
                case 2:
                    textView.setText("有效期");
                    String str2 = "";
                    if (a.this.h == g.b.f2309a) {
                        str2 = a.this.e.valid;
                    } else if (a.this.h == g.b.ct) {
                        str2 = a.this.e.ctvalid;
                    }
                    if (TextUtils.isEmpty(str2)) {
                        str2 = "2017-06-30";
                    }
                    textView2.setText(str2);
                    break;
                case 3:
                    textView.setText("彩铃价格");
                    int i2 = Downloads.STATUS_SUCCESS;
                    if (a.this.h == g.b.f2309a) {
                        i2 = a.this.e.price;
                    } else if (a.this.h == g.b.ct) {
                        i2 = a.this.e.ctprice;
                    }
                    if (i2 == 0 || a.this.n) {
                        str = "免费";
                    } else {
                        str = String.valueOf(((double) ((float) i2)) / 100.0d) + "元";
                    }
                    textView2.setText(str);
                    break;
            }
            return inflate;
        }
    }
}
