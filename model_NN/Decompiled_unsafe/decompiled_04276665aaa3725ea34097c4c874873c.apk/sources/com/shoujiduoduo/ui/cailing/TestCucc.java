package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.cailing.e;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.b.b;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.e.a;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.widget.d;

public class TestCucc extends BaseActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private String f1529a = "18561951252";
    private String b = "";
    private String c = "810027210086";
    private EditText d;
    /* access modifiers changed from: private */
    public EditText e;
    private EditText f;
    private ContentObserver g;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_test_cucc);
        this.d = (EditText) findViewById(R.id.music_id);
        this.d.setText(this.c);
        this.e = (EditText) findViewById(R.id.phone_num);
        this.e.setText(this.f1529a);
        this.f = (EditText) findViewById(R.id.random_key);
        findViewById(R.id.query_vip_state).setOnClickListener(this);
        findViewById(R.id.open_vip).setOnClickListener(this);
        findViewById(R.id.vip_order).setOnClickListener(this);
        findViewById(R.id.sms_random_key).setOnClickListener(this);
        findViewById(R.id.close_vip).setOnClickListener(this);
        findViewById(R.id.check_base_cailing_status).setOnClickListener(this);
        findViewById(R.id.sms_get_token).setOnClickListener(this);
        findViewById(R.id.open_cailing).setOnClickListener(this);
        findViewById(R.id.close_cailing).setOnClickListener(this);
        findViewById(R.id.query_subed_product).setOnClickListener(this);
        findViewById(R.id.query_tone_set).setOnClickListener(this);
        findViewById(R.id.query_user_tone).setOnClickListener(this);
        findViewById(R.id.query_ring_by_id).setOnClickListener(this);
        findViewById(R.id.query_cucc_and_area).setOnClickListener(this);
        findViewById(R.id.query_cucc_info).setOnClickListener(this);
        findViewById(R.id.check_token).setOnClickListener(this);
        findViewById(R.id.get_mobile_num).setOnClickListener(this);
        findViewById(R.id.buy_cucc_cailing).setOnClickListener(this);
        findViewById(R.id.del_cucc_cailing).setOnClickListener(this);
        findViewById(R.id.qry_user_box).setOnClickListener(this);
        findViewById(R.id.qry_box_mem).setOnClickListener(this);
        findViewById(R.id.phone_sms_login).setOnClickListener(this);
        this.g = new ag(this, new Handler(), (EditText) findViewById(R.id.random_key), "1065515888");
        getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.g);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        getContentResolver().unregisterContentObserver(this.g);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar) {
        new AlertDialog.Builder(this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar, String str) {
        new AlertDialog.Builder(this).setMessage(bVar.toString() + " , " + str).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
    }

    public void onClick(View view) {
        this.f1529a = this.e.getText().toString();
        if (ai.c(this.f1529a)) {
            d.a("请输入手机号");
        } else if (!a.a().a(this.f1529a)) {
            a(this.f1529a, g.b.cu);
        } else {
            switch (view.getId()) {
                case R.id.check_base_cailing_status:
                    if (!TextUtils.isEmpty(this.e.getText().toString())) {
                        this.f1529a = this.e.getText().toString();
                    }
                    a.a().e(new b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            if (bVar instanceof c.af) {
                                com.shoujiduoduo.base.a.a.a("testcucc", "openCheck onSuccess:" + bVar.toString());
                                TestCucc.this.a(bVar, "userStatus:" + ((c.af) bVar).f2226a + ",\n是否开通了彩铃：" + ((c.af) bVar).d());
                                return;
                            }
                            com.shoujiduoduo.base.a.a.a("testcucc", SocketMessage.MSG_ERROR_KEY);
                            TestCucc.this.a(bVar, SocketMessage.MSG_ERROR_KEY);
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            com.shoujiduoduo.base.a.a.a("testcucc", "openCheck onFailure:" + bVar.toString());
                            TestCucc.this.a(bVar);
                        }
                    });
                    return;
                case R.id.query_vip_state:
                case R.id.query_caililng_and_vip:
                case R.id.get_sms_code:
                case R.id.query_cailing_url:
                case R.id.query_ring_box:
                case R.id.query_default_ring:
                case R.id.query_ring_circle:
                case R.id.find_mdn_by_imsi:
                case R.id.diy_clip_upload:
                case R.id.set_diy_ring:
                case R.id.get_diy_ring:
                case R.id.query_diy_ring_status:
                case R.id.sms_random_key_common:
                case R.id.query_ring_info:
                case R.id.query_zhenling_url:
                case R.id.emp_operate_layout:
                case R.id.emp_launch:
                case R.id.emp_open_vip:
                case R.id.emp_close_vip:
                case R.id.emp_one_key_open:
                case R.id.query3rd_phone:
                case R.id.query3rd_uid:
                default:
                    return;
                case R.id.vip_order:
                    a.a().a(true, "一首很火的来电铃声", a.b(this.f1529a), this.f1529a, "diy/20150914/", "6725908.wav", "晓辰枫", "", new b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            com.shoujiduoduo.base.a.a.a("testcucc", "vipOrder onSuccess:" + bVar.toString());
                            new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        }

                        public void b(c.b bVar) {
                            com.shoujiduoduo.base.a.a.a("testcucc", "vipOrder onFailure:" + bVar.toString());
                            new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                            super.b(bVar);
                        }
                    });
                    return;
                case R.id.open_vip:
                    new AlertDialog.Builder(this).setMessage("确定要开通包月吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            a.a().d("", new b() {
                                public void a(c.b bVar) {
                                    super.a(bVar);
                                    com.shoujiduoduo.base.a.a.a("testcucc", "openVip onSuccess:" + bVar.toString());
                                    TestCucc.this.a(bVar);
                                }

                                public void b(c.b bVar) {
                                    com.shoujiduoduo.base.a.a.a("testcucc", "openVip onFailure:" + bVar.toString());
                                    super.b(bVar);
                                    TestCucc.this.a(bVar);
                                }
                            });
                        }
                    }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                    return;
                case R.id.close_vip:
                    new AlertDialog.Builder(this).setMessage("确定关闭包月吗？").setNegativeButton("取消", (DialogInterface.OnClickListener) null).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            a.a().d(new b() {
                                public void a(c.b bVar) {
                                    com.shoujiduoduo.base.a.a.a("testcucc", "closeVip onFailure:" + bVar.toString());
                                    new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                    super.a(bVar);
                                }

                                public void b(c.b bVar) {
                                    com.shoujiduoduo.base.a.a.a("testcucc", "closeVip onFailure:" + bVar.toString());
                                    new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                                    super.b(bVar);
                                }
                            });
                        }
                    }).show();
                    return;
                case R.id.sms_random_key:
                    if (!TextUtils.isEmpty(this.e.getText().toString())) {
                        this.f1529a = this.e.getText().toString();
                    }
                    a.a().c(this.f1529a, new b() {
                        public void a(c.b bVar) {
                            com.shoujiduoduo.base.a.a.a("testcucc", "sendAuthRandomKey onSuccess:" + bVar.toString());
                            new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                            super.a(bVar);
                        }

                        public void b(c.b bVar) {
                            com.shoujiduoduo.base.a.a.a("testcucc", "sendAuthRandomKey onFailure:" + bVar.toString());
                            new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                            super.b(bVar);
                        }
                    });
                    return;
                case R.id.phone_sms_login:
                    a(this.f1529a, g.b.cu);
                    return;
                case R.id.query_subed_product:
                    a.a().c(new b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            com.shoujiduoduo.base.a.a.a("testcucc", "qrySubedProducts onSuccess:" + bVar.toString());
                            new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString() + ", \n是否开通vip：" + ((c.r) bVar).f2248a).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            com.shoujiduoduo.base.a.a.a("testcucc", "qrySubedProducts onFailure:" + bVar.toString());
                            new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        }
                    });
                    return;
                case R.id.query_tone_set:
                    a.a().h(new b() {
                        public void a(c.b bVar) {
                            String str;
                            String str2;
                            boolean z = false;
                            super.a(bVar);
                            com.shoujiduoduo.base.a.a.a("testcucc", "qryToneSet onSuccess:" + bVar.toString());
                            c.s sVar = (c.s) bVar;
                            if (sVar.d != null) {
                                int i = 0;
                                while (true) {
                                    if (i >= sVar.d.length) {
                                        break;
                                    } else if (sVar.d[i].c.equals("0")) {
                                        String str3 = sVar.d[i].f2231a;
                                        str = sVar.d[i].d;
                                        str2 = str3;
                                        z = true;
                                        break;
                                    } else {
                                        i++;
                                    }
                                }
                            }
                            str = "";
                            str2 = "";
                            TestCucc.this.a(bVar, "hasDefaultRing:" + z + ", settingid:" + str2 + ", defaultCailingId:" + str);
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            com.shoujiduoduo.base.a.a.a("testcucc", "qryToneSet onSuccess:" + bVar.toString());
                            TestCucc.this.a(bVar);
                        }
                    });
                    return;
                case R.id.query_user_tone:
                    a.a().i(new b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            com.shoujiduoduo.base.a.a.a("testcucc", "qryUserTone onSuccess:" + bVar.toString());
                            if (bVar instanceof c.u) {
                                c.ab[] abVarArr = ((c.u) bVar).d;
                                if (abVarArr == null || abVarArr.length == 0) {
                                    com.shoujiduoduo.base.a.a.a("testcucc", "userToneInfo == null");
                                    return;
                                }
                                StringBuilder sb = new StringBuilder();
                                for (int i = 0; i < abVarArr.length; i++) {
                                    sb.append(abVarArr[i].d).append("-").append(abVarArr[i].b);
                                    sb.append("|");
                                }
                                TestCucc.this.a(bVar, sb.toString());
                                return;
                            }
                            TestCucc.this.a(bVar, SocketMessage.MSG_ERROR_KEY);
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            com.shoujiduoduo.base.a.a.a("testcucc", "qryUserTone onSuccess:" + bVar.toString());
                            TestCucc.this.a(bVar);
                        }
                    });
                    return;
                case R.id.query_ring_by_id:
                    a.a().g(this.d.getText().toString(), new b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            com.shoujiduoduo.base.a.a.a("testcucc", "qryRingById onSuccess:" + bVar.toString());
                            new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            com.shoujiduoduo.base.a.a.a("testcucc", "qryRingById onSuccess:" + bVar.toString());
                            new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                        }
                    });
                    return;
                case R.id.query_cucc_and_area:
                    a.a().h(this.f1529a, new b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            com.shoujiduoduo.base.a.a.a("testcucc", "qryUserLocation onFailure:" + bVar.toString());
                            c.ah ahVar = (c.ah) bVar;
                            TestCucc.this.a(bVar, "provinceId:" + ahVar.f2228a + ", provinceName:" + ahVar.d);
                        }

                        public void b(c.b bVar) {
                            com.shoujiduoduo.base.a.a.a("testcucc", "qryUserLocation onFailure:" + bVar.toString());
                            super.b(bVar);
                            TestCucc.this.a(bVar);
                        }
                    });
                    return;
                case R.id.query_cucc_info:
                    this.f1529a = this.e.getText().toString();
                    a.a().i(this.f1529a, new b() {
                        public void a(c.b bVar) {
                            com.shoujiduoduo.base.a.a.a("testcucc", "qryUserInfo onFailure:" + bVar.toString());
                            super.a(bVar);
                            TestCucc.this.a(bVar, "网络类型：" + ((c.ai) bVar).f2229a);
                        }

                        public void b(c.b bVar) {
                            com.shoujiduoduo.base.a.a.a("testcucc", "qryUserInfo onFailure:" + bVar.toString());
                            super.b(bVar);
                            TestCucc.this.a(bVar);
                        }
                    });
                    return;
                case R.id.buy_cucc_cailing:
                    String obj = this.d.getText().toString();
                    if (TextUtils.isEmpty(obj)) {
                        obj = "9178900048603256445007";
                    }
                    a.a().b(obj, "", new b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            TestCucc.this.a(bVar);
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            TestCucc.this.a(bVar);
                        }
                    });
                    return;
                case R.id.del_cucc_cailing:
                    if (TextUtils.isEmpty(this.d.getText().toString())) {
                    }
                    a.a().a("xxx", a.b(this.f1529a), this.f1529a, "", new b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            TestCucc.this.a(bVar);
                        }

                        public void b(c.b bVar) {
                            TestCucc.this.a(bVar);
                            super.b(bVar);
                        }
                    });
                    return;
                case R.id.qry_user_box:
                    a.a().b(new b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            TestCucc.this.a(bVar);
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            TestCucc.this.a(bVar);
                        }
                    });
                    return;
                case R.id.qry_box_mem:
                    a.a().a("907550290062", new b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            TestCucc.this.a(bVar);
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            TestCucc.this.a(bVar);
                        }
                    });
                    return;
                case R.id.open_cailing:
                    if (!TextUtils.isEmpty(this.e.getText().toString())) {
                        this.f1529a = this.e.getText().toString();
                    }
                    new AlertDialog.Builder(this).setMessage("确定要开通彩铃业务吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            a.a().e("", new b() {
                                public void a(c.b bVar) {
                                    super.a(bVar);
                                    com.shoujiduoduo.base.a.a.a("testcucc", "openAccount onSuccess:" + bVar.toString());
                                    TestCucc.this.a(bVar);
                                }

                                public void b(c.b bVar) {
                                    super.b(bVar);
                                    com.shoujiduoduo.base.a.a.a("testcucc", "openAccount onFailure:" + bVar.toString());
                                    TestCucc.this.a(bVar);
                                }
                            });
                        }
                    }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                    return;
                case R.id.close_cailing:
                    if (!TextUtils.isEmpty(this.e.getText().toString())) {
                        this.f1529a = this.e.getText().toString();
                    }
                    new AlertDialog.Builder(this).setMessage("确定要关闭彩铃业务吗？").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            a.a().g(new b() {
                                public void a(c.b bVar) {
                                    super.a(bVar);
                                    com.shoujiduoduo.base.a.a.a("testcucc", "closeAccount onSuccess:" + bVar.toString());
                                    TestCucc.this.a(bVar);
                                }

                                public void b(c.b bVar) {
                                    super.b(bVar);
                                    com.shoujiduoduo.base.a.a.a("testcucc", "closeAccount onFailure:" + bVar.toString());
                                    TestCucc.this.a(bVar);
                                }
                            });
                        }
                    }).setNegativeButton("取消", (DialogInterface.OnClickListener) null).show();
                    return;
                case R.id.sms_get_token:
                    if (!TextUtils.isEmpty(this.f.getText().toString())) {
                        this.b = this.f.getText().toString();
                    }
                    if (!TextUtils.isEmpty(this.e.getText().toString())) {
                        this.f1529a = this.e.getText().toString();
                    }
                    a.a().a(this.f1529a, this.b, new b() {
                        public void a(c.b bVar) {
                            if (bVar instanceof c.a) {
                                c.a aVar = (c.a) bVar;
                                com.shoujiduoduo.base.a.a.a("testcucc", "getAuthToken onSuccess:" + bVar.toString() + ", token:" + aVar.f2220a);
                                new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString() + ", token:" + aVar.f2220a).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                            } else {
                                new AlertDialog.Builder(TestCucc.this).setMessage("返回结果不对").setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                            }
                            super.a(bVar);
                        }

                        public void b(c.b bVar) {
                            com.shoujiduoduo.base.a.a.a("testcucc", "getAuthToken onFailure:" + bVar.toString());
                            new AlertDialog.Builder(TestCucc.this).setMessage(bVar.toString()).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                            super.b(bVar);
                        }
                    });
                    return;
                case R.id.check_token:
                    new AlertDialog.Builder(this).setMessage("当前号码是否有token:" + a.a().a(this.f1529a)).setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                    return;
                case R.id.get_mobile_num:
                    a.a().a(new b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            TestCucc.this.a(bVar);
                            a.a().b(((c.j) bVar).f2239a, new b() {
                                public void a(c.b bVar) {
                                    super.a(bVar);
                                    c.t tVar = (c.t) bVar;
                                    TestCucc.this.a(bVar, "imsi:" + tVar.d + ", mobile:" + tVar.f2250a + ", apn:" + tVar.e + ", ratetype:" + tVar.f);
                                }

                                public void b(c.b bVar) {
                                    super.b(bVar);
                                    TestCucc.this.a(bVar);
                                }
                            });
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            TestCucc.this.a(bVar);
                        }
                    });
                    return;
            }
        }
    }

    private void a(String str, g.b bVar) {
        new e(this, R.style.DuoDuoDialog, str, bVar, new e.a() {
            public void a(String str) {
                UserInfo c = com.shoujiduoduo.a.b.b.g().c();
                if (!c.isLogin()) {
                    c.setUserName(str);
                    c.setUid("phone_" + str);
                }
                c.setPhoneNum(str);
                c.setLoginStatus(1);
                com.shoujiduoduo.a.b.b.g().a(c);
                TestCucc.this.e.setText(str);
                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                    public void a() {
                        ((v) this.f1284a).a(1, true, "", "");
                    }
                });
                d.a("初始化成功，可以进行功能调用了。。。");
            }
        }).show();
    }
}
