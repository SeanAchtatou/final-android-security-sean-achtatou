package com.shoujiduoduo.ui.cailing;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.CMMusicCallback;
import com.cmsc.cmmusic.common.RingbackManagerInterface;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.ffcs.inapppaylib.EMPHelper;
import com.ffcs.inapppaylib.bean.Constants;
import com.ffcs.inapppaylib.bean.response.BaseResponse;
import com.ffcs.inapppaylib.bean.response.IValidatableResponse;
import com.ffcs.inapppaylib.bean.response.VerifyResponse;
import com.ffcs.inapppaylib.impl.OnVCodeListener;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.u;
import com.shoujiduoduo.util.widget.b;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: DuoduoVipDialog */
public class d extends Dialog implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public EditText f1599a;
    private String b;
    private String c;
    private EditText d;
    /* access modifiers changed from: private */
    public Button e;
    private TextView f;
    private TextView g;
    private TextView h;
    private RelativeLayout i;
    /* access modifiers changed from: private */
    public Context j;
    /* access modifiers changed from: private */
    public C0044d k;
    private ListView l;
    /* access modifiers changed from: private */
    public a m;
    private g.b n;
    /* access modifiers changed from: private */
    public ArrayList<Map<String, Object>> o;
    /* access modifiers changed from: private */
    public c p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public RingData r;
    /* access modifiers changed from: private */
    public ContentObserver s;
    private boolean t;
    /* access modifiers changed from: private */
    public boolean u;
    private EMPHelper v;
    /* access modifiers changed from: private */
    public String w = "";
    /* access modifiers changed from: private */
    public boolean x = false;
    /* access modifiers changed from: private */
    public ProgressDialog y = null;

    /* compiled from: DuoduoVipDialog */
    public interface c {
        void a(boolean z);
    }

    public d(Context context, g.b bVar, RingData ringData, String str, boolean z, boolean z2, c cVar) {
        super(context, R.style.duoduo_dialog_theme);
        this.j = context;
        this.q = str;
        this.r = ringData;
        if (this.r == null) {
            this.r = new RingData();
        }
        this.t = z;
        this.k = new C0044d(60000, 1000);
        this.u = z2;
        this.n = bVar;
        this.m = new a(this);
        if (this.n.equals(g.b.ct)) {
            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "init ctcc emp helper");
            this.v = EMPHelper.getInstance(this.j);
            this.v.init("1000010404421", "5297", "x4lRWHcgRqfH", this.m, 15000);
        }
        this.o = d();
        this.p = cVar;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_duoduo_vip);
        this.l = (ListView) findViewById(R.id.vip_right_list);
        this.i = (RelativeLayout) findViewById(R.id.phone_auth_layout);
        this.i.setVisibility(0);
        this.f1599a = (EditText) this.i.findViewById(R.id.et_phone_no);
        this.d = (EditText) this.i.findViewById(R.id.et_random_key);
        this.e = (Button) this.i.findViewById(R.id.reget_sms_code);
        this.e.setOnClickListener(this);
        this.f = (TextView) findViewById(R.id.open_tips);
        this.g = (TextView) findViewById(R.id.bottom_tips);
        this.h = (TextView) findViewById(R.id.cost_hint);
        if (this.u) {
            this.f.setText((int) R.string.open_cailing_and_update_vip);
            this.g.setText((int) R.string.open_cailing_hint);
        }
        if (this.n.equals(g.b.cu)) {
            this.g.setText(Html.fromHtml("<p>1.<font color=#ff0000>“铃声多多”</font>炫铃包月为包月计费，资费为<font color=#ff0000>5元/月</font>，用户订购即扣费。<br/>2.订购包月业务后，如月末不退订，次月1日将自动扣除包月费用。<br/>3.若您取消炫铃功能，将不能正常使用炫铃包月业务<br/>4.联通沃3G预付费20元卡不支持开通此业务<br/>5.使用炫铃过程中产生流量费按照您手机套餐内资费标准收取<br/>6.暂不支持山东用户进行订购，敬请期待"));
            this.f1599a.setHint((int) R.string.cucc_num);
        } else if (this.n.equals(g.b.ct)) {
            this.f1599a.setHint((int) R.string.ctcc_num);
        } else if (this.n.equals(g.b.f2309a)) {
            this.f1599a.setHint("中国移动号码");
            this.i.setVisibility(8);
        }
        String phoneNum = com.shoujiduoduo.a.b.b.g().c().getPhoneNum();
        this.b = phoneNum;
        this.f1599a.setText(phoneNum);
        if (this.n.equals(g.b.ct)) {
            this.h.setText((int) R.string.six_yuan_per_month);
            this.t = true;
            if (!ai.c(phoneNum)) {
                this.k.start();
                com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "start to get feed id");
                this.c = phoneNum;
                this.v.getTradeId(phoneNum, "90213829", "135000000000000214309");
            }
        } else if (!this.n.equals(g.b.cu) && this.n.equals(g.b.f2309a)) {
            this.h.setText((int) R.string.six_yuan_per_month);
        }
        a(this.t);
        this.l.setAdapter((ListAdapter) new b());
        findViewById(R.id.close).setOnClickListener(this);
        findViewById(R.id.open).setOnClickListener(this);
        setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                String obj = d.this.f1599a.getText().toString();
                StringBuilder sb = new StringBuilder();
                sb.append("&ctcid=").append(d.this.r.ctcid).append("&rid=").append(d.this.r.rid).append("&from=").append(d.this.q).append("&phone=").append(obj);
                u.a(d.this.j(), "close_back", sb.toString());
            }
        });
        setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                d.this.j.getContentResolver().unregisterContentObserver(d.this.s);
                if (d.this.k != null) {
                    d.this.k.cancel();
                }
            }
        });
        this.s = new ag(this.j, new Handler(), this.d, "1065987320001", 4);
        this.j.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.s);
    }

    private void a(boolean z) {
        if (z) {
            this.d.setVisibility(0);
            this.e.setVisibility(0);
            return;
        }
        this.d.setVisibility(8);
        this.e.setVisibility(8);
    }

    /* compiled from: DuoduoVipDialog */
    private static class a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<d> f1617a;

        public a(d dVar) {
            this.f1617a = new WeakReference<>(dVar);
        }

        public void handleMessage(Message message) {
            String str;
            d dVar = this.f1617a.get();
            if (dVar != null) {
                switch (message.what) {
                    case 10:
                        if (dVar.p != null) {
                            dVar.p.a(true);
                        }
                        dVar.dismiss();
                        return;
                    case 11:
                        if (dVar.p != null) {
                            dVar.p.a(false);
                        }
                        dVar.dismiss();
                        return;
                    case 12:
                    default:
                        return;
                    case Constants.RESULT_PAY_SUCCESS:
                        com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "pay success");
                        u.a(dVar.j(), "success", dVar.b());
                        dVar.i();
                        dVar.c();
                        dVar.a();
                        if (TextUtils.isEmpty(dVar.r.rid)) {
                            new b.a(dVar.j).a((int) R.string.open_vip_success).b((int) R.string.hint).a((int) R.string.ok, (DialogInterface.OnClickListener) null).a().show();
                        } else if (dVar.p != null) {
                            dVar.p.a(true);
                        }
                        dVar.dismiss();
                        return;
                    case Constants.RESULT_PAY_FAILURE:
                        dVar.a();
                        BaseResponse baseResponse = (BaseResponse) message.obj;
                        if (baseResponse != null) {
                            str = baseResponse.getRes_code() + ":" + baseResponse.getRes_message();
                        } else {
                            str = "";
                        }
                        u.a(dVar.j(), "fail," + str, dVar.b());
                        com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "pay failure," + str);
                        new b.a(dVar.j).a("未成功开通会员. 原因:" + str).b((int) R.string.hint).a((int) R.string.ok, (DialogInterface.OnClickListener) null).a().show();
                        if (dVar.p != null) {
                            dVar.p.a(false);
                        }
                        dVar.dismiss();
                        return;
                    case Constants.RESULT_VALIDATE_FAILURE:
                        BaseResponse baseResponse2 = (BaseResponse) message.obj;
                        if (baseResponse2 != null) {
                            String str2 = baseResponse2.getRes_code() + ":" + baseResponse2.getRes_message();
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "RESULT_VALIDATE_FAILURE, " + baseResponse2.getRes_code() + ":" + baseResponse2.getRes_message());
                            com.shoujiduoduo.util.widget.d.a(str2);
                            u.a(dVar.j(), "validate fail," + str2, dVar.b());
                            return;
                        }
                        return;
                    case 900:
                        com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "get trade id success");
                        IValidatableResponse iValidatableResponse = (IValidatableResponse) message.obj;
                        if (iValidatableResponse != null) {
                            String unused = dVar.w = iValidatableResponse.getTrade_id();
                            boolean unused2 = dVar.x = iValidatableResponse.getLowTariff();
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "tradeid:" + dVar.w + ", lowTariff:" + dVar.x);
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", iValidatableResponse.getRes_code() + ":" + iValidatableResponse.getRes_message());
                        } else {
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "get trade id, response is null");
                        }
                        com.shoujiduoduo.util.widget.d.a("成功获取验证码");
                        return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public String b() {
        return "&ctcid=" + this.r.ctcid + "&rid=" + this.r.rid + "&cucid=" + this.r.cucid + "&from=" + this.q + "&phone=" + this.b;
    }

    /* renamed from: com.shoujiduoduo.ui.cailing.d$d  reason: collision with other inner class name */
    /* compiled from: DuoduoVipDialog */
    private class C0044d extends CountDownTimer {
        public C0044d(long j, long j2) {
            super(j, j2);
        }

        public void onTick(long j) {
            d.this.e.setClickable(false);
            d.this.e.setText((j / 1000) + "秒");
        }

        public void onFinish() {
            d.this.e.setClickable(true);
            d.this.e.setText("重新获取");
        }
    }

    /* compiled from: DuoduoVipDialog */
    private class b extends BaseAdapter {
        private b() {
        }

        public int getCount() {
            if (d.this.o != null) {
                return d.this.o.size();
            }
            return 0;
        }

        public Object getItem(int i) {
            if (d.this.o != null) {
                return d.this.o.get(i);
            }
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(d.this.j).inflate((int) R.layout.listitem_vip_rights_small, viewGroup, false);
            }
            Map map = (Map) d.this.o.get(i);
            TextView textView = (TextView) view.findViewById(R.id.title);
            ((ImageView) view.findViewById(R.id.icon)).setImageResource(((Integer) map.get("icon")).intValue());
            textView.setText((String) map.get("title"));
            ((TextView) view.findViewById(R.id.description)).setText((String) map.get("description"));
            if (i == 0) {
                textView.setTextColor(d.this.j.getResources().getColor(R.color.text_orange));
            }
            return view;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        String str;
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        String h2 = g.h();
        String bVar = g.s().toString();
        String str2 = "";
        switch (c2.getLoginType()) {
            case 1:
                str2 = "phone";
                break;
            case 2:
                str2 = "qq";
                break;
            case 3:
                str2 = cn.banshenggua.aichang.utils.Constants.WEIBO;
                break;
            case 5:
                str2 = "weixin";
                break;
        }
        switch (this.n) {
            case f2309a:
                str = "cm_open_vip";
                break;
            case ct:
                str = "ct_open_diy_sdk";
                break;
            case cu:
                str = "cu_open_vip";
                break;
            default:
                str = "unknown";
                break;
        }
        final StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("&newimsi=").append(h2).append("&phone=").append(this.b).append("&st=").append(bVar).append("&uid=").append(c2.getUid()).append("&3rd=").append(str2).append("&rid=").append(this.r.rid).append("&viptype=").append(str);
        if (this.n == g.b.f2309a) {
            stringBuffer.append("&cm_uid=").append(com.shoujiduoduo.util.c.b.a().b());
        }
        i.a(new Runnable() {
            public void run() {
                com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "res:" + u.a("openvip", stringBuffer.toString()));
            }
        });
    }

    private ArrayList<Map<String, Object>> d() {
        ArrayList<Map<String, Object>> arrayList = new ArrayList<>();
        HashMap hashMap = new HashMap();
        hashMap.put("icon", Integer.valueOf((int) R.drawable.vip_free_s));
        hashMap.put("title", this.n == g.b.f2309a ? "百万彩铃免费用" : "所有彩铃免费用");
        hashMap.put("description", "换彩铃不用花钱啦");
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("icon", Integer.valueOf((int) R.drawable.vip_noad_s));
        hashMap2.put("title", "永久去除应用内广告");
        hashMap2.put("description", "无广告，真干净");
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("icon", Integer.valueOf((int) R.drawable.vip_personal_s));
        hashMap3.put("title", "私人定制炫酷启动画面");
        hashMap3.put("description", "小清新、文艺范、女汉子...");
        arrayList.add(hashMap3);
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public void a(final String str) {
        this.m.post(new Runnable() {
            public void run() {
                if (d.this.y == null) {
                    ProgressDialog unused = d.this.y = new ProgressDialog(d.this.j);
                    d.this.y.setMessage(str);
                    d.this.y.setIndeterminate(false);
                    d.this.y.setCancelable(true);
                    d.this.y.setCanceledOnTouchOutside(false);
                    d.this.y.show();
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.m.post(new Runnable() {
            public void run() {
                if (d.this.y != null) {
                    d.this.y.dismiss();
                    ProgressDialog unused = d.this.y = (ProgressDialog) null;
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void e() {
        com.shoujiduoduo.util.e.a.a().d(b(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                d.this.i();
                d.this.c();
                d.this.a();
                d.this.m.sendMessage(d.this.m.obtainMessage(10));
            }

            public void b(c.b bVar) {
                String str;
                super.b(bVar);
                d.this.a();
                if (bVar.a().equals("2100")) {
                    str = "当前区域尚未开通包月业务，请耐心等待，本次开通不扣费.";
                } else {
                    str = "未成功开通会员. 原因:" + bVar.b();
                }
                new b.a(d.this.j).a(str).b((int) R.string.hint).a((int) R.string.ok, (DialogInterface.OnClickListener) null).a().show();
                d.this.m.sendMessage(d.this.m.obtainMessage(11));
            }
        });
    }

    private void b(String str) {
        try {
            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "confirmPayment, tradeId:" + this.w + ", code:" + str + ", lowTariff:" + this.x);
            this.v.confirmPayment(this.w, str, this.x);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        a("请稍候...");
        com.shoujiduoduo.util.c.b.a().a(RingToneDuoduoActivity.a(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                RingbackManagerInterface.openRingback(d.this.j, new CMMusicCallback<OrderResult>() {
                    /* renamed from: a */
                    public void operationResult(OrderResult orderResult) {
                        if (orderResult == null) {
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "开通彩铃，cancel");
                            d.this.a();
                        } else if (orderResult.getResCode().equals("0") || orderResult.getResCode().equals("000000") || orderResult.getResCode().equals("000001") || orderResult.getResCode().equals("P000002") || orderResult.getResCode().equals("P301034") || orderResult.getResCode().equals("P000001")) {
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "open cailing , rescode:" + orderResult.getResCode());
                            d.this.g();
                            u.a("cm:sun_open_cailing", "success, resCode:" + orderResult.getResCode() + ", resMsg:" + orderResult.getResMsg(), d.this.b());
                        } else if (orderResult.getResCode().equals("CT0004")) {
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "付费界面取消订购， res:" + orderResult.toString());
                            d.this.a();
                            com.shoujiduoduo.util.widget.d.a("彩铃基础功能开通失败！");
                            u.a("cm:sun_open_cailing", "fail, resCode:" + orderResult.getResCode() + ", resMsg:" + orderResult.getResMsg(), d.this.b());
                        } else {
                            d.this.a();
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "开通失败， res ：" + orderResult.toString());
                            new b.a(d.this.j).a("开通失败," + orderResult.toString()).b((int) R.string.hint).a((int) R.string.ok, (DialogInterface.OnClickListener) null).a().show();
                            com.shoujiduoduo.util.widget.d.a("彩铃基础功能开通失败！");
                            u.a("cm:sun_open_cailing", "fail, resCode:" + orderResult.getResCode() + ", resMsg:" + orderResult.getResMsg(), d.this.b());
                        }
                    }
                });
            }

            public void b(c.b bVar) {
                super.b(bVar);
                d.this.a();
                com.shoujiduoduo.util.widget.d.a("初始化SDK失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public void g() {
        com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "cmccRealOpenVip");
        com.shoujiduoduo.util.c.b.a().b(b(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                c.k kVar = (c.k) bVar;
                com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "getCrbtMonthPolicy, res:salePrice:" + kVar.f2240a.c + ", mobile:" + kVar.d);
                RingbackManagerInterface.openRingbackMonth(d.this.j, "600927020000006624", kVar.f2240a.c, "", new CMMusicCallback<OrderResult>() {
                    /* renamed from: a */
                    public void operationResult(OrderResult orderResult) {
                        if (orderResult != null) {
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "openRingbackMonth, res:" + orderResult.toString());
                            if (orderResult.getResCode().equals("000000") || orderResult.getResCode().equals("0")) {
                                d.this.a();
                                d.this.i();
                                d.this.c();
                                d.this.m.sendMessage(d.this.m.obtainMessage(10));
                                u.a("cm:sun_open_vip", "success, resCode:" + orderResult.getResCode() + ", resMsg:" + orderResult.getResMsg(), d.this.b());
                            } else if (orderResult.getResCode().equals("CT0004")) {
                                d.this.a();
                                u.a("cm:sun_open_vip", "fail, resCode:" + orderResult.getResCode() + ", resMsg:" + orderResult.getResMsg(), d.this.b());
                                com.shoujiduoduo.util.widget.d.a("取消开通包月会员业务！");
                                d.this.m.sendMessage(d.this.m.obtainMessage(11));
                            } else {
                                d.this.a();
                                if (orderResult.getResCode().equals("P1000")) {
                                    String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "cmcc_open_delay_msg");
                                    if (ai.b(a2)) {
                                        a2 = "已为您提交会员开通申请，请按照短信提示操作，回复“是”确认订购。目前河北、山东用户开通后稍有延迟";
                                    }
                                    new b.a(d.this.j).a(a2).b((int) R.string.hint).a((int) R.string.ok, (DialogInterface.OnClickListener) null).a().show();
                                } else {
                                    new b.a(d.this.j).a("开通失败, " + orderResult.toString()).b((int) R.string.hint).a((int) R.string.ok, (DialogInterface.OnClickListener) null).a().show();
                                }
                                u.a("cm:sun_open_vip", "fail, resCode:" + orderResult.getResCode() + ", resMsg:" + orderResult.getResMsg(), d.this.b());
                                d.this.m.sendMessage(d.this.m.obtainMessage(11));
                            }
                        } else {
                            com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "开通包月， 取消");
                        }
                    }
                });
            }

            public void b(c.b bVar) {
                d.this.a();
                com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "getCrbtMonthPolicy, error， res:" + bVar.toString());
                com.shoujiduoduo.util.widget.d.a("获取包月信息失败！");
                super.b(bVar);
            }
        });
    }

    /* access modifiers changed from: private */
    public void h() {
        a("请稍候...");
        com.shoujiduoduo.util.c.b.a().a(RingToneDuoduoActivity.a(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                d.this.g();
            }

            public void b(c.b bVar) {
                super.b(bVar);
                com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "SDK 初始化失败");
                d.this.a();
                com.shoujiduoduo.util.widget.d.a("初始化SDK失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public void i() {
        UserInfo c2 = com.shoujiduoduo.a.b.b.g().c();
        final int i2 = 0;
        switch (this.n) {
            case f2309a:
                i2 = 1;
                break;
            case ct:
                i2 = 2;
                break;
            case cu:
                i2 = 3;
                break;
        }
        c2.setVipType(i2);
        c2.setPhoneNum(this.b);
        if (!c2.isLogin()) {
            if (this.n != g.b.f2309a || !ai.c(this.b)) {
                c2.setUserName(this.b);
                c2.setUid("phone_" + this.b);
            } else {
                c2.setUserName("多多VIP");
                c2.setUid("phone_" + com.shoujiduoduo.util.c.b.a().b());
            }
            c2.setLoginStatus(1);
            com.shoujiduoduo.a.b.b.g().a(c2);
            com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                public void a() {
                    ((v) this.f1284a).a(1, true, "", "");
                }
            });
        } else {
            com.shoujiduoduo.a.b.b.g().a(c2);
        }
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new c.a<x>() {
            public void a() {
                ((x) this.f1284a).a(i2);
            }
        });
    }

    /* access modifiers changed from: private */
    public String j() {
        if (this.n.equals(g.b.ct)) {
            return "ct:ct_open_diy_sdk";
        }
        if (this.n.equals(g.b.cu)) {
            return "cu:cu_open_vip";
        }
        if (this.n.equals(g.b.f2309a)) {
            return "cm:cm_open_vip";
        }
        return "";
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reget_sms_code:
                this.b = this.f1599a.getText().toString();
                if (!g.f(this.b)) {
                    com.shoujiduoduo.util.widget.d.a("请输入正确的手机号", 0);
                    return;
                }
                this.n = g.g(this.b);
                if (this.n == g.b.none) {
                    com.shoujiduoduo.util.widget.d.a("未知的手机号类型，无法判断运营商，请确认手机号输入正确！");
                    com.shoujiduoduo.base.a.a.c("DuoduoVipDialog", "unknown phone type :" + this.b);
                    return;
                }
                this.e.setClickable(false);
                if (this.n.equals(g.b.ct)) {
                    if (this.b.equals(this.c)) {
                        com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "refresh vcode id from btn");
                        this.v.refreshVCode(this.w, "90213829", new OnVCodeListener() {
                            public void onRefreshVcoBtnSuccess(VerifyResponse verifyResponse) {
                                com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "refresh code success");
                                d.this.a(verifyResponse, true);
                            }

                            public void onRefreshVcoBtnFailure(VerifyResponse verifyResponse) {
                                com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "refresh code fail");
                                d.this.a(verifyResponse, false);
                            }
                        });
                        return;
                    }
                    com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "get trade id from btn");
                    this.c = this.b;
                    this.v.getTradeId(this.b, "90213829", "135000000000000214309");
                    this.k.start();
                    return;
                } else if (this.n.equals(g.b.f2309a)) {
                    com.shoujiduoduo.util.c.b.a().a(this.b, new com.shoujiduoduo.util.b.b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            d.this.e.setClickable(true);
                            C0044d unused = d.this.k = new C0044d(60000, 1000);
                            d.this.k.start();
                            com.shoujiduoduo.util.widget.d.a("成功发送验证码,请注意查收");
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            d.this.e.setClickable(true);
                            com.shoujiduoduo.util.widget.d.a("获取验证码失败");
                        }
                    });
                    return;
                } else {
                    return;
                }
            case R.id.open:
                com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "click open vip button");
                if (this.n.equals(g.b.cu)) {
                    com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "operator type : cu");
                    TextView textView = new TextView(this.j);
                    textView.setLineSpacing(10.0f, 1.0f);
                    textView.setText(Html.fromHtml("即将为您开通铃声多多包月，标准资费<font color=#ff0000>5元/月</font>，资费由运营商收取"));
                    new b.a(this.j).b("开通铃声多多包月").a(textView).b("取消", (DialogInterface.OnClickListener) null).a("确认开通", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            d.this.a("请稍候...");
                            d.this.e();
                        }
                    }).a().show();
                    return;
                } else if (this.n.equals(g.b.ct)) {
                    com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "operator type : ct");
                    String obj = this.d.getText().toString();
                    if (TextUtils.isEmpty(obj)) {
                        com.shoujiduoduo.util.widget.d.a("请输入正确的验证码");
                        return;
                    }
                    a("请稍候...");
                    b(obj);
                    return;
                } else if (this.n.equals(g.b.f2309a)) {
                    com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "operator type : cm");
                    if (com.shoujiduoduo.util.c.b.a().d() == c.n.a.sms_code) {
                        a("请稍候...");
                        com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "用户当前通过手机号验证码初始化，开通会员时再通过SDK 初始化 user 一次");
                        com.shoujiduoduo.util.c.b.a().c(new com.shoujiduoduo.util.b.b() {
                            public void a(c.b bVar) {
                                super.a(bVar);
                                d.this.a();
                                com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "user 初始化成功，准备开通");
                                d.this.k();
                            }

                            public void b(c.b bVar) {
                                super.b(bVar);
                                d.this.a();
                                com.shoujiduoduo.base.a.a.a("DuoduoVipDialog", "user 初始化失败，再尝试开一把试试看看");
                                d.this.k();
                            }
                        });
                        return;
                    }
                    k();
                    return;
                } else {
                    com.shoujiduoduo.base.a.a.e("DuoduoVipDialog", "不支持的运营商类型");
                    com.shoujiduoduo.util.widget.d.a("不支持的运营商类型!");
                    return;
                }
            case R.id.close:
                String obj2 = this.f1599a.getText().toString();
                StringBuilder sb = new StringBuilder();
                sb.append("&ctcid=").append(this.r.ctcid).append("&rid=").append(this.r.rid).append("&from=").append(this.q).append("&phone=").append(obj2);
                u.a(j(), "close", sb.toString());
                dismiss();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        if (!"false".equals(com.umeng.b.a.a().a(RingDDApp.c(), "cmcc_open_vip_twice_confirm"))) {
            TextView textView = new TextView(this.j);
            textView.setLineSpacing(10.0f, 1.0f);
            textView.setText(Html.fromHtml("即将为您开通铃声多多包月，标准资费<font color=#ff0000>6元/月</font>，资费由运营商收取"));
            new b.a(this.j).b("开通铃声多多包月").a(textView).b("取消", (DialogInterface.OnClickListener) null).a("确认开通", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    d.this.a("请稍候...");
                    if (d.this.u) {
                        d.this.f();
                    } else {
                        d.this.h();
                    }
                }
            }).a().show();
        } else if (this.u) {
            f();
        } else {
            h();
        }
    }

    /* access modifiers changed from: private */
    public void a(VerifyResponse verifyResponse, boolean z) {
        this.e.setClickable(true);
        if (z) {
            this.k = new C0044d(60000, 1000);
            this.k.start();
        } else if (verifyResponse.getRes_code() == -1 || verifyResponse.getRes_code() == -3) {
            com.shoujiduoduo.util.widget.d.a("网络异常, 验证码刷新失败");
        }
    }
}
