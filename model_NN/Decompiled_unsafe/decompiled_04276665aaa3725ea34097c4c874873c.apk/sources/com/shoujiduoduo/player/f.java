package com.shoujiduoduo.player;

import android.os.Handler;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: Recorder */
public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    protected int f1413a = 0;
    public a b = a.none;
    protected ArrayList<d> c = new ArrayList<>();
    protected ArrayList<b> d = new ArrayList<>();
    protected ArrayList<c> e = new ArrayList<>();
    protected Handler f = new Handler();

    /* compiled from: Recorder */
    public enum a {
        none,
        Start,
        Pause,
        Stop,
        Complete,
        Error
    }

    /* compiled from: Recorder */
    public interface b {
        void a(f fVar, long j);
    }

    /* compiled from: Recorder */
    public interface c {
        void a(f fVar, a aVar);
    }

    /* compiled from: Recorder */
    public interface d {
        void a(f fVar, short[] sArr);
    }

    public int f() {
        return 0;
    }

    public a l() {
        return this.b;
    }

    public void a(d dVar) {
        this.c.add(dVar);
    }

    public void b(d dVar) {
        if (this.c.contains(dVar)) {
            this.c.remove(dVar);
        }
    }

    public void a(b bVar) {
        this.d.add(bVar);
    }

    public void b(b bVar) {
        if (this.d.contains(bVar)) {
            this.d.remove(bVar);
        }
    }

    public void a(c cVar) {
        this.e.add(cVar);
    }

    public void b(c cVar) {
        if (this.e.contains(cVar)) {
            this.e.remove(cVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a(final f fVar, final short[] sArr) {
        Iterator<d> it = this.c.iterator();
        while (it.hasNext()) {
            final d next = it.next();
            if (next != null) {
                this.f.post(new Runnable() {
                    public void run() {
                        next.a(fVar, sArr);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(final f fVar) {
        Iterator<b> it = this.d.iterator();
        while (it.hasNext()) {
            final b next = it.next();
            if (next != null) {
                this.f.post(new Runnable() {
                    public void run() {
                        next.a(fVar, (long) f.this.f1413a);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(final f fVar, final a aVar) {
        this.b = aVar;
        Iterator<c> it = this.e.iterator();
        while (it.hasNext()) {
            final c next = it.next();
            if (next != null) {
                this.f.post(new Runnable() {
                    public void run() {
                        next.a(fVar, aVar);
                    }
                });
            }
        }
    }
}
