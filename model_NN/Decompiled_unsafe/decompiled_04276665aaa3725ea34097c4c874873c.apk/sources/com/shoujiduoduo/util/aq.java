package com.shoujiduoduo.util;

import com.shoujiduoduo.base.a.a;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

/* compiled from: http */
public class aq {
    public static byte[] a(String str, int i, int i2) {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpParams params = defaultHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, i);
            HttpConnectionParams.setSoTimeout(params, i2);
            HttpResponse execute = defaultHttpClient.execute(new HttpGet(str));
            int statusCode = execute.getStatusLine().getStatusCode();
            a.a("http", "httpGet: http client status = " + statusCode);
            if (statusCode < 200 || statusCode >= 300) {
                a.a("http", "httpGet: Request failed, status code:" + statusCode);
                return null;
            }
            HttpEntity entity = execute.getEntity();
            if (entity == null) {
                a.a("http", "httpGet: Fail to getEntity");
                return null;
            }
            InputStream content = entity.getContent();
            if (content == null) {
                return null;
            }
            byte[] bArr = new byte[2048];
            BufferedInputStream bufferedInputStream = new BufferedInputStream(content);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = bufferedInputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            bufferedInputStream.close();
            content.close();
            a.a("http", "httpGet: content = " + byteArrayOutputStream.toString());
            if (byteArrayOutputStream.toByteArray() != null) {
                return byteArrayOutputStream.toByteArray();
            }
            return "ok".getBytes();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            a.c("http", "httpGet: ClientProtocolException catched!");
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            a.c("http", "httpGet: IOException catched!");
            return null;
        }
    }

    public static byte[] b(String str, int i, int i2) {
        boolean z = false;
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpParams params = defaultHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, i);
            HttpConnectionParams.setSoTimeout(params, i2);
            HttpGet httpGet = new HttpGet(str);
            httpGet.addHeader("Accept-Encoding", "gzip");
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            int statusCode = execute.getStatusLine().getStatusCode();
            a.a("http", "httpGetGZIP: http client status = " + statusCode);
            if (statusCode < 200 || statusCode >= 300) {
                a.a("http", "httpGetGZIP: Request failed, status code:" + statusCode);
                return null;
            }
            Header firstHeader = execute.getFirstHeader("Content-Encoding");
            if (firstHeader != null) {
                if (!firstHeader.getValue().equalsIgnoreCase("gzip")) {
                    a.a("http", "httpGetGZIP: Request failed, response encoding is " + firstHeader.getValue());
                } else {
                    z = true;
                }
            }
            HttpEntity entity = execute.getEntity();
            if (entity == null) {
                a.a("http", "httpGetGZIP: Fail to getEntity");
                return null;
            }
            InputStream content = entity.getContent();
            if (content == null) {
                return null;
            }
            byte[] bArr = new byte[2048];
            InputStream gZIPInputStream = z ? new GZIPInputStream(content) : new BufferedInputStream(content);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = gZIPInputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            gZIPInputStream.close();
            content.close();
            if (byteArrayOutputStream.toByteArray() != null) {
                return byteArrayOutputStream.toByteArray();
            }
            return "ok".getBytes();
        } catch (ClientProtocolException e) {
            a.a(e);
            e.printStackTrace();
            a.c("http", "httpGetGZIP: ClientProtocolException catched!");
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            a.a(e2);
            a.c("http", "httpGetGZIP: IOException catched!");
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ee  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x00e2=Splitter:B:28:0x00e2, B:23:0x00d8=Splitter:B:23:0x00d8, B:11:0x006e=Splitter:B:11:0x006e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r7, org.json.JSONObject r8) {
        /*
            r1 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0101, UnsupportedEncodingException -> 0x00d7, IOException -> 0x00e1 }
            r0.<init>(r7)     // Catch:{ MalformedURLException -> 0x0101, UnsupportedEncodingException -> 0x00d7, IOException -> 0x00e1 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ MalformedURLException -> 0x0101, UnsupportedEncodingException -> 0x00d7, IOException -> 0x00e1 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0101, UnsupportedEncodingException -> 0x00d7, IOException -> 0x00e1 }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r1 = 1
            r0.setDoInput(r1)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r1 = 0
            r0.setUseCaches(r1)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r1 = 1
            r0.setInstanceFollowRedirects(r1)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r1 = "Content-Type"
            java.lang.String r2 = "application/json"
            r0.setRequestProperty(r1, r2)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r0.connect()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r2 = r8.toString()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            byte[] r2 = r2.getBytes()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r1.write(r2)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r1.flush()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r1.close()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r1.<init>(r2)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r3 = ""
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
        L_0x0055:
            java.lang.String r3 = r1.readLine()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            if (r3 == 0) goto L_0x0079
            java.lang.String r4 = new java.lang.String     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            byte[] r3 = r3.getBytes()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r5 = "utf-8"
            r4.<init>(r3, r5)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r2.append(r4)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            goto L_0x0055
        L_0x006a:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x006e:
            r0.printStackTrace()     // Catch:{ all -> 0x00eb }
            if (r1 == 0) goto L_0x0076
            r1.disconnect()
        L_0x0076:
            java.lang.String r0 = ""
        L_0x0078:
            return r0
        L_0x0079:
            java.lang.String r3 = "http"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r4.<init>()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r5 = "response code:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            int r5 = r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r4 = r4.toString()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            com.shoujiduoduo.base.a.a.a(r3, r4)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r3 = "http"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r4.<init>()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r5 = "response msg:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r5 = r0.getResponseMessage()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r4 = r4.toString()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            com.shoujiduoduo.base.a.a.a(r3, r4)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r3 = "http"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r4.<init>()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r5 = "post response:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r4 = r4.toString()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            com.shoujiduoduo.base.a.a.a(r3, r4)     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            r1.close()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            java.lang.String r1 = r2.toString()     // Catch:{ MalformedURLException -> 0x006a, UnsupportedEncodingException -> 0x00fc, IOException -> 0x00f7, all -> 0x00f2 }
            if (r0 == 0) goto L_0x00d5
            r0.disconnect()
        L_0x00d5:
            r0 = r1
            goto L_0x0078
        L_0x00d7:
            r0 = move-exception
        L_0x00d8:
            r0.printStackTrace()     // Catch:{ all -> 0x00eb }
            if (r1 == 0) goto L_0x0076
            r1.disconnect()
            goto L_0x0076
        L_0x00e1:
            r0 = move-exception
        L_0x00e2:
            r0.printStackTrace()     // Catch:{ all -> 0x00eb }
            if (r1 == 0) goto L_0x0076
            r1.disconnect()
            goto L_0x0076
        L_0x00eb:
            r0 = move-exception
        L_0x00ec:
            if (r1 == 0) goto L_0x00f1
            r1.disconnect()
        L_0x00f1:
            throw r0
        L_0x00f2:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00ec
        L_0x00f7:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00e2
        L_0x00fc:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x00d8
        L_0x0101:
            r0 = move-exception
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.aq.a(java.lang.String, org.json.JSONObject):java.lang.String");
    }

    public static boolean a(String str, List<NameValuePair> list, int i, int i2) {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpParams params = defaultHttpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, i);
            HttpConnectionParams.setSoTimeout(params, i2);
            HttpPost httpPost = new HttpPost(str);
            httpPost.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode != 200) {
                a.c("http", "httpPost: Request failed, status code:" + statusCode);
                return false;
            }
            HttpEntity entity = execute.getEntity();
            if (entity != null) {
                a.a("http", "post response entity:" + EntityUtils.toString(entity));
            } else {
                a.c("http", "entity is null");
            }
            return true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            a.c("http", "httpPost: ClientProtocolException catched!");
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            a.c("http", "httpPost: IOException catched!");
            return false;
        }
    }

    public static byte[] a(String str) {
        a.a("http", str);
        return a(str, 20000, 20000);
    }

    public static byte[] b(String str) {
        return b(str, 20000, 20000);
    }

    public static boolean a(String str, List<NameValuePair> list) {
        return a(str, list, 20000, 20000);
    }
}
