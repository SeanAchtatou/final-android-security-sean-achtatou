package com.shoujiduoduo.util;

import com.shoujiduoduo.base.a.a;

public class NativeDES {

    /* renamed from: a  reason: collision with root package name */
    private static String f2167a = "NativeDES";
    private static boolean b = z.a("url_encode");

    public native String Encrypt(String str);

    static {
        a.a(f2167a, "load url_encode lib, res:" + b);
    }

    public static boolean a() {
        return b;
    }
}
