package com.shoujiduoduo.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

/* compiled from: BcsUtils */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private static String f2257a = "http://bj.bcebos.com";

    /* compiled from: BcsUtils */
    public enum a {
        recordRing,
        editRing,
        userHead,
        userBkg
    }

    public static String a(String str, a aVar) {
        String str2 = "http://" + ad.a().a("bcs_domain_name");
        switch (aVar) {
            case recordRing:
                return str2 + "/duoduo-ring-user-upload/record-ring/" + str;
            case editRing:
                return str2 + "/duoduo-ring-user-upload/edit-ring/" + str;
            case userBkg:
                return str2 + "/duoduo-ring-userprofile/bg_pic/" + a(str);
            case userHead:
                return str2 + "/duoduo-ring-userprofile/head_pic/" + a(str);
            default:
                return str2;
        }
    }

    private static String a(String str) {
        try {
            String a2 = r.a(str);
            return a2.substring(a2.length() - 2) + "/" + str;
        } catch (Exception e) {
            e.printStackTrace();
            return "00/" + str;
        }
    }

    public static boolean a(String str, String str2, a aVar) {
        String str3;
        com.shoujiduoduo.base.a.a.a("BcsUtils", "putObject in");
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(a(str2, aVar)).openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod(HttpProxyConstants.PUT);
            File file = new File(str);
            if (!file.exists()) {
                return false;
            }
            httpURLConnection.setRequestProperty("Content-length", "" + file.length());
            httpURLConnection.setRequestProperty("Accept", "*/*");
            httpURLConnection.setRequestProperty("Content-Type", "binary/octet-stream");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Charset", "UTF-8");
            com.shoujiduoduo.base.a.a.a("BcsUtils", "output before");
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
            bufferedOutputStream.write(v.a(file));
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            com.shoujiduoduo.base.a.a.a("BcsUtils", "output flush");
            int responseCode = httpURLConnection.getResponseCode();
            com.shoujiduoduo.base.a.a.a("BcsUtils", "responseCode-----" + responseCode);
            if (200 == responseCode) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = httpURLConnection.getInputStream().read(bArr, 0, 1024);
                    if (read != -1) {
                        byteArrayOutputStream.write(bArr, 0, read);
                    } else {
                        com.shoujiduoduo.base.a.a.a("BcsUtils", "responseBody------------\r\n" + new String(byteArrayOutputStream.toByteArray(), "UTF-8"));
                        return true;
                    }
                }
            } else {
                str3 = "";
                com.shoujiduoduo.base.a.a.a("cm httppostcore return:", str3);
                return false;
            }
        } catch (Exception e) {
            Exception exc = e;
            str3 = "";
            exc.printStackTrace();
            com.shoujiduoduo.base.a.a.c("BcsUtils", "bcs upload exception");
        }
    }
}
