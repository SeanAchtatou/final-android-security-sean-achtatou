package com.shoujiduoduo.util.a;

import com.igexin.download.Downloads;
import com.shoujiduoduo.util.a.d;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/* compiled from: CheapMP3 */
public class c extends d {
    private static int[] r = {0, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, Downloads.STATUS_RUNNING, 224, 256, 320, 0};
    private static int[] s = {0, 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 144, 160, 0};
    private static int[] t = {44100, 48000, 32000, 0};
    private static int[] u = {22050, 24000, 16000, 0};

    /* renamed from: a  reason: collision with root package name */
    private int f2175a;
    private int[] b;
    private int[] h;
    private int[] i;
    private int j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;

    public static d.a a() {
        return new d.a() {
            public d a() {
                return new c();
            }

            public String[] b() {
                return new String[]{"mp3"};
            }
        };
    }

    public int b() {
        return this.f2175a;
    }

    public int c() {
        return 1152;
    }

    public int[] d() {
        return this.i;
    }

    public int e() {
        return this.l;
    }

    public int f() {
        return this.m;
    }

    public int a_(int i2) {
        if (i2 <= 0) {
            return 0;
        }
        if (i2 >= this.f2175a) {
            return this.j;
        }
        return this.b[i2];
    }

    public void a(File file) throws FileNotFoundException, IOException {
        int i2;
        boolean z;
        int i3;
        int i4;
        int i5;
        super.a(file);
        this.f2175a = 0;
        this.n = 64;
        this.b = new int[this.n];
        this.h = new int[this.n];
        this.i = new int[this.n];
        this.o = 0;
        this.p = 255;
        this.q = 0;
        this.j = (int) this.g.length();
        FileInputStream fileInputStream = new FileInputStream(this.g);
        int i6 = 0;
        int i7 = 0;
        byte[] bArr = new byte[12];
        while (i6 < this.j - 12) {
            while (i7 < 12) {
                i7 += fileInputStream.read(bArr, i7, 12 - i7);
            }
            int i8 = 0;
            while (true) {
                i2 = i8;
                if (i2 < 12 && bArr[i2] != -1) {
                    i8 = i2 + 1;
                }
            }
            if (this.f != null && !this.f.a((((double) i6) * 1.0d) / ((double) this.j))) {
                break;
            } else if (i2 > 0) {
                for (int i9 = 0; i9 < 12 - i2; i9++) {
                    bArr[i9] = bArr[i2 + i9];
                }
                i6 += i2;
                i7 = 12 - i2;
            } else {
                if (bArr[1] == -6 || bArr[1] == -5) {
                    z = true;
                } else if (bArr[1] == -14 || bArr[1] == -13) {
                    z = true;
                } else {
                    for (int i10 = 0; i10 < 11; i10++) {
                        bArr[i10] = bArr[1 + i10];
                    }
                    i6++;
                    i7 = 11;
                }
                if (z) {
                    i3 = r[(bArr[2] & 240) >> 4];
                    i4 = t[(bArr[2] & 12) >> 2];
                } else {
                    i3 = s[(bArr[2] & 240) >> 4];
                    i4 = u[(bArr[2] & 12) >> 2];
                }
                if (i3 == 0 || i4 == 0) {
                    for (int i11 = 0; i11 < 10; i11++) {
                        bArr[i11] = bArr[2 + i11];
                    }
                    i6 += 2;
                    i7 = 10;
                } else {
                    this.l = i4;
                    int i12 = (((i3 * 144) * 1000) / i4) + ((bArr[2] & 2) >> 1);
                    if ((bArr[3] & 192) == 192) {
                        this.m = 1;
                        if (z) {
                            i5 = ((bArr[10] & 1) << 7) + ((bArr[11] & 254) >> 1);
                        } else {
                            i5 = ((bArr[9] & 3) << 6) + ((bArr[10] & 252) >> 2);
                        }
                    } else {
                        this.m = 2;
                        if (z) {
                            i5 = ((bArr[9] & Byte.MAX_VALUE) << 1) + ((bArr[10] & 128) >> 7);
                        } else {
                            i5 = 0;
                        }
                    }
                    this.o = i3 + this.o;
                    this.b[this.f2175a] = i6;
                    this.h[this.f2175a] = i12;
                    this.i[this.f2175a] = i5;
                    if (i5 < this.p) {
                        this.p = i5;
                    }
                    if (i5 > this.q) {
                        this.q = i5;
                    }
                    this.f2175a++;
                    if (this.f2175a == this.n) {
                        this.k = this.o / this.f2175a;
                        int i13 = ((((this.j / this.k) * i4) / 144000) * 11) / 10;
                        if (i13 < this.n * 2) {
                            i13 = this.n * 2;
                        }
                        int[] iArr = new int[i13];
                        int[] iArr2 = new int[i13];
                        int[] iArr3 = new int[i13];
                        for (int i14 = 0; i14 < this.f2175a; i14++) {
                            iArr[i14] = this.b[i14];
                            iArr2[i14] = this.h[i14];
                            iArr3[i14] = this.i[i14];
                        }
                        this.b = iArr;
                        this.h = iArr2;
                        this.i = iArr3;
                        this.n = i13;
                    }
                    fileInputStream.skip((long) (i12 - 12));
                    i6 += i12;
                    i7 = 0;
                }
            }
        }
        if (this.f2175a > 0) {
            this.k = this.o / this.f2175a;
        } else {
            this.k = 0;
        }
    }

    public void a(File file, int i2, int i3) throws IOException {
        file.createNewFile();
        FileInputStream fileInputStream = new FileInputStream(this.g);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        int i4 = 0;
        for (int i5 = 0; i5 < i3; i5++) {
            if (this.h[i2 + i5] > i4) {
                i4 = this.h[i2 + i5];
            }
        }
        byte[] bArr = new byte[i4];
        int i6 = 0;
        int i7 = 0;
        while (i6 < i3) {
            int i8 = this.b[i2 + i6] - i7;
            int i9 = this.h[i2 + i6];
            if (i8 > 0) {
                fileInputStream.skip((long) i8);
                i7 += i8;
            }
            fileInputStream.read(bArr, 0, i9);
            fileOutputStream.write(bArr, 0, i9);
            i6++;
            i7 += i9;
        }
        fileInputStream.close();
        fileOutputStream.close();
    }
}
