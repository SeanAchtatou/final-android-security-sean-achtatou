package com.shoujiduoduo.util;

import android.text.TextUtils;
import com.shoujiduoduo.ringtone.RingDDApp;

/* compiled from: AdUtils */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static Boolean f2171a;
    private static String b;
    private static Boolean c;

    public static String a() {
        if (b != null) {
            return b;
        }
        String a2 = ad.a().a("navi_ad_type");
        if ("video".equals(a2)) {
            b = "video";
        } else if ("shop".equals(a2)) {
            b = "shop";
        } else if ("news".equals(a2)) {
            b = "news";
        } else {
            b = "shop";
        }
        return b;
    }

    public static boolean b() {
        if (c != null) {
            return c.booleanValue();
        }
        c = Boolean.valueOf(ad.a().b("navi_ad_enable"));
        return c.booleanValue();
    }

    public static boolean c() {
        if (f2171a != null) {
            return f2171a.booleanValue();
        }
        if (ad.a().b("aichang_enable")) {
            f2171a = true;
            return true;
        }
        f2171a = false;
        return false;
    }

    public static boolean d() {
        return true;
    }

    public static boolean a(boolean z) {
        return true;
    }

    public static boolean e() {
        if (j()) {
            return false;
        }
        return true;
    }

    public static boolean f() {
        if (j()) {
            return false;
        }
        String a2 = ad.a().a("feed_ad_v1_channel");
        String a3 = ad.a().a("feed_ad_v1_enable");
        boolean z = a2.contains(g.k()) || a2.equals("all");
        if (!a3.equals("true") || !z) {
            return false;
        }
        return true;
    }

    private static boolean j() {
        int a2 = af.a(RingDDApp.c(), "user_vip_type", 0);
        if (af.a(RingDDApp.c(), "user_loginStatus", 0) != 1 || a2 == 0) {
            return false;
        }
        return true;
    }

    public static synchronized boolean g() {
        boolean z;
        synchronized (a.class) {
            String a2 = ad.a().a("tencent_ad_duration");
            if (TextUtils.isEmpty(a2) || "0".equalsIgnoreCase(a2)) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }

    public static synchronized boolean h() {
        boolean z;
        synchronized (a.class) {
            String a2 = ad.a().a("baidu_ad_duration");
            if (TextUtils.isEmpty(a2) || "0".equalsIgnoreCase(a2)) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }

    public static synchronized boolean i() {
        boolean z;
        synchronized (a.class) {
            String a2 = ad.a().a("duoduo_ad_duration");
            if (TextUtils.isEmpty(a2) || "0".equalsIgnoreCase(a2)) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }
}
