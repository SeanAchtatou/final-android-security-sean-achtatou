package com.shoujiduoduo.util;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.WelcomeActivity;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.c.c;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.http.message.BasicNameValuePair;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/* compiled from: CommonUtils */
public final class g {

    /* renamed from: a  reason: collision with root package name */
    public static final NativeDES f2306a = new NativeDES();
    public static String b = "com.shoujiduoduo.ringtone.shortcut_action";
    public static int c = 1;
    private static Context d;
    private static String e;
    private static String f;
    private static String g;
    private static String h;
    private static String i;
    private static String j;
    private static String k;
    private static String l;
    private static String m;
    private static int n;
    private static String o;
    private static a p;

    /* compiled from: CommonUtils */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public b f2308a = b.none;
        public String b = "";
    }

    /* compiled from: CommonUtils */
    public enum b {
        f2309a,
        ct,
        cu,
        none
    }

    public static synchronized String a() {
        String str;
        synchronized (g.class) {
            if (e == null) {
                try {
                    x();
                } catch (Exception e2) {
                }
                if (TextUtils.isEmpty(e)) {
                    e = "DEFAULT_USER";
                }
            }
            str = e;
        }
        return str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007e A[SYNTHETIC, Splitter:B:30:0x007e] */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r8) {
        /*
            r1 = 0
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r8.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            java.lang.String r0 = r0.getDeviceId()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0014
        L_0x0013:
            return r0
        L_0x0014:
            java.io.File r3 = new java.io.File
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r2 = 1
            java.lang.String r2 = com.shoujiduoduo.util.m.a(r2)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = ".id"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r3.<init>(r0)
            boolean r0 = r3.exists()
            if (r0 == 0) goto L_0x0082
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x008e, all -> 0x007a }
            r2.<init>(r3)     // Catch:{ Exception -> 0x008e, all -> 0x007a }
        L_0x0045:
            int r5 = r2.read(r0)     // Catch:{ Exception -> 0x0055, all -> 0x008c }
            if (r5 <= 0) goto L_0x006b
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x0055, all -> 0x008c }
            r7 = 0
            r6.<init>(r0, r7, r5)     // Catch:{ Exception -> 0x0055, all -> 0x008c }
            r4.append(r6)     // Catch:{ Exception -> 0x0055, all -> 0x008c }
            goto L_0x0045
        L_0x0055:
            r0 = move-exception
            r0 = r2
        L_0x0057:
            if (r0 == 0) goto L_0x0091
            r0.close()     // Catch:{ IOException -> 0x0077 }
            r0 = r1
        L_0x005d:
            if (r0 != 0) goto L_0x0013
            java.util.UUID r0 = java.util.UUID.randomUUID()
            java.lang.String r0 = r0.toString()
            a(r3, r0)
            goto L_0x0013
        L_0x006b:
            java.lang.String r0 = r4.toString()     // Catch:{ Exception -> 0x0055, all -> 0x008c }
            if (r2 == 0) goto L_0x005d
            r2.close()     // Catch:{ IOException -> 0x0075 }
            goto L_0x005d
        L_0x0075:
            r1 = move-exception
            goto L_0x005d
        L_0x0077:
            r0 = move-exception
            r0 = r1
            goto L_0x005d
        L_0x007a:
            r0 = move-exception
            r2 = r1
        L_0x007c:
            if (r2 == 0) goto L_0x0081
            r2.close()     // Catch:{ IOException -> 0x008a }
        L_0x0081:
            throw r0
        L_0x0082:
            r3.createNewFile()     // Catch:{ IOException -> 0x0087 }
            r0 = r1
            goto L_0x005d
        L_0x0087:
            r0 = move-exception
            r0 = r1
            goto L_0x005d
        L_0x008a:
            r1 = move-exception
            goto L_0x0081
        L_0x008c:
            r0 = move-exception
            goto L_0x007c
        L_0x008e:
            r0 = move-exception
            r0 = r1
            goto L_0x0057
        L_0x0091:
            r0 = r1
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.g.a(android.content.Context):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001a A[SYNTHETIC, Splitter:B:13:0x001a] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0023 A[SYNTHETIC, Splitter:B:18:0x0023] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(java.io.File r4, java.lang.String r5) {
        /*
            r0 = 0
            r2 = 0
            java.io.OutputStreamWriter r1 = new java.io.OutputStreamWriter     // Catch:{ IOException -> 0x0016, all -> 0x0020 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0016, all -> 0x0020 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0016, all -> 0x0020 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x0016, all -> 0x0020 }
            r1.write(r5)     // Catch:{ IOException -> 0x002e, all -> 0x002b }
            r0 = 1
            if (r1 == 0) goto L_0x0015
            r1.close()     // Catch:{ IOException -> 0x0027 }
        L_0x0015:
            return r0
        L_0x0016:
            r1 = move-exception
            r1 = r2
        L_0x0018:
            if (r1 == 0) goto L_0x0015
            r1.close()     // Catch:{ IOException -> 0x001e }
            goto L_0x0015
        L_0x001e:
            r1 = move-exception
            goto L_0x0015
        L_0x0020:
            r0 = move-exception
        L_0x0021:
            if (r2 == 0) goto L_0x0026
            r2.close()     // Catch:{ IOException -> 0x0029 }
        L_0x0026:
            throw r0
        L_0x0027:
            r1 = move-exception
            goto L_0x0015
        L_0x0029:
            r1 = move-exception
            goto L_0x0026
        L_0x002b:
            r0 = move-exception
            r2 = r1
            goto L_0x0021
        L_0x002e:
            r2 = move-exception
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.g.a(java.io.File, java.lang.String):boolean");
    }

    public static String b() {
        TelephonyManager telephonyManager = (TelephonyManager) RingDDApp.c().getSystemService("phone");
        if (telephonyManager == null) {
            return "";
        }
        String line1Number = telephonyManager.getLine1Number();
        com.shoujiduoduo.base.a.a.a("CommonUtils", "phone num:" + line1Number);
        if (!TextUtils.isEmpty(line1Number)) {
            return line1Number.trim().replace("+86", "").replace(" ", "").replace("\t", "");
        }
        return line1Number;
    }

    public static synchronized String c() {
        String str;
        WifiManager wifiManager;
        synchronized (g.class) {
            if (k == null) {
                if (!(d == null || (wifiManager = (WifiManager) d.getSystemService((String) IXAdSystemUtils.NT_WIFI)) == null)) {
                    k = wifiManager.getConnectionInfo().getMacAddress();
                }
                String str2 = null;
                if (k != null) {
                    str2 = k.trim();
                }
                if (TextUtils.isEmpty(str2)) {
                    k = "MAC_ADDR_UNAVAILABLE";
                }
            }
            str = k;
        }
        return str;
    }

    public static void b(Context context) {
        d = context;
    }

    public static NetworkInfo d() {
        if (d == null) {
            return null;
        }
        return ((ConnectivityManager) d.getSystemService("connectivity")).getActiveNetworkInfo();
    }

    public static boolean e() {
        NetworkInfo d2 = d();
        if (d2 == null || !d2.isConnected()) {
            return false;
        }
        return true;
    }

    public static boolean f() {
        NetworkInfo d2 = d();
        if (d2 == null || !d2.isConnected() || d2.getType() != 1) {
            return false;
        }
        return true;
    }

    public static synchronized String g() {
        String str;
        TelephonyManager telephonyManager;
        synchronized (g.class) {
            if (l == null) {
                if (!(d == null || (telephonyManager = (TelephonyManager) d.getSystemService("phone")) == null)) {
                    l = telephonyManager.getDeviceId();
                }
                if (TextUtils.isEmpty(l)) {
                    l = "default_imei";
                }
            }
            str = l;
        }
        return str;
    }

    public static synchronized String h() {
        String str;
        TelephonyManager telephonyManager;
        synchronized (g.class) {
            if (m == null) {
                if (!(d == null || (telephonyManager = (TelephonyManager) d.getSystemService("phone")) == null)) {
                    m = telephonyManager.getSubscriberId();
                }
                if (TextUtils.isEmpty(m)) {
                    m = "default_imsi";
                }
            }
            str = m;
        }
        return str;
    }

    private static void x() {
        if (d != null) {
            String g2 = g();
            if (g2 == null) {
                String y = y();
                if (!y.equals("")) {
                    e = y;
                    return;
                }
                return;
            }
            e = g2;
        }
    }

    private static String y() {
        String readLine;
        try {
            LineNumberReader lineNumberReader = new LineNumberReader(new InputStreamReader(Runtime.getRuntime().exec("cat /proc/cpuinfo").getInputStream()));
            for (int i2 = 1; i2 < 100 && (readLine = lineNumberReader.readLine()) != null; i2++) {
                if (readLine.indexOf("Serial") > -1) {
                    return readLine.substring(readLine.indexOf(":") + 1, readLine.length()).trim();
                }
            }
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return "0000000000000000";
        }
    }

    public static synchronized String i() {
        String str;
        synchronized (g.class) {
            if (f == null) {
                f = Build.BRAND + ">" + Build.PRODUCT + ">" + Build.MODEL;
            }
            str = f;
        }
        return str;
    }

    public static synchronized String j() {
        String str;
        synchronized (g.class) {
            if (g == null) {
                g = Build.VERSION.SDK_INT + ">" + Build.VERSION.RELEASE;
            }
            str = g;
        }
        return str;
    }

    public static synchronized String k() {
        String str;
        Bundle bundle;
        synchronized (g.class) {
            if (o == null) {
                String str2 = "unknown";
                try {
                    ApplicationInfo applicationInfo = d.getPackageManager().getApplicationInfo(d.getPackageName(), 128);
                    if (!(applicationInfo == null || (bundle = applicationInfo.metaData) == null)) {
                        str2 = bundle.getString("UMENG_CHANNEL");
                        com.shoujiduoduo.base.a.a.a("CommonUtils", "channel = " + str2);
                    }
                    o = str2;
                    if (TextUtils.isEmpty(o)) {
                        o = "unknown";
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    PackageManager.NameNotFoundException nameNotFoundException = e2;
                    str = str2;
                    nameNotFoundException.printStackTrace();
                }
            }
            str = o;
        }
        return str;
    }

    @SuppressLint({"NewApi"})
    public static boolean l() {
        int i2 = Resources.getSystem().getConfiguration().uiMode & 15;
        if (i2 == 12 || i2 == 13 || (i2 != 14 && i2 != 15 && i2 != 11)) {
            return false;
        }
        return true;
    }

    public static boolean m() {
        try {
            if (Build.class.getMethod("hasSmartBar", new Class[0]) != null) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            return false;
        }
    }

    public static synchronized String n() {
        String str;
        Bundle bundle;
        synchronized (g.class) {
            if (j == null) {
                String str2 = "unknown";
                try {
                    PackageInfo packageInfo = d.getPackageManager().getPackageInfo(d.getPackageName(), 0);
                    ApplicationInfo applicationInfo = d.getPackageManager().getApplicationInfo(d.getPackageName(), 128);
                    if (!(applicationInfo == null || (bundle = applicationInfo.metaData) == null)) {
                        str2 = bundle.getString("UMENG_CHANNEL");
                        com.shoujiduoduo.base.a.a.a("CommonUtils", "channel = " + str2);
                    }
                    j = d.getResources().getString(R.string.app_version_prefix) + packageInfo.versionName + "_" + str2 + d.getResources().getString(R.string.install_source_suffix);
                    if (TextUtils.isEmpty(j)) {
                        j = "unknown";
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    e2.printStackTrace();
                    str = d.getResources().getString(R.string.app_version_prefix) + "0.0.0.0" + d.getResources().getString(R.string.install_source_suffix);
                }
            }
            str = j;
        }
        return str;
    }

    public static synchronized String o() {
        String str;
        synchronized (g.class) {
            if (h == null) {
                try {
                    h = d.getResources().getString(R.string.app_version_prefix) + d.getPackageManager().getPackageInfo(d.getPackageName(), 0).versionName;
                    if (TextUtils.isEmpty(h)) {
                        h = "unknown";
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    e2.printStackTrace();
                    str = d.getResources().getString(R.string.app_version_prefix) + "0.0.0.0";
                }
            }
            str = h;
        }
        return str;
    }

    public static synchronized int p() {
        int i2;
        synchronized (g.class) {
            if (n == 0) {
                try {
                    PackageInfo packageInfo = d.getPackageManager().getPackageInfo(d.getPackageName(), 0);
                    if (packageInfo != null) {
                        n = packageInfo.versionCode;
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    e2.printStackTrace();
                }
            }
            i2 = n;
        }
        return i2;
    }

    public static synchronized String q() {
        String str;
        synchronized (g.class) {
            if (i == null) {
                try {
                    PackageInfo packageInfo = d.getPackageManager().getPackageInfo(d.getPackageName(), 0);
                    if (packageInfo != null) {
                        i = packageInfo.versionName;
                    }
                } catch (PackageManager.NameNotFoundException e2) {
                    e2.printStackTrace();
                }
                if (TextUtils.isEmpty(i)) {
                    i = "unknown";
                }
            }
            str = i;
        }
        return str;
    }

    public static String r() {
        return d.getResources().getString(R.string.english_name);
    }

    public static boolean a(String str) {
        PackageInfo packageInfo;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            packageInfo = d.getPackageManager().getPackageInfo(str, 0);
        } catch (PackageManager.NameNotFoundException e2) {
            packageInfo = null;
        }
        if (packageInfo != null) {
            return true;
        }
        return false;
    }

    public static boolean a(Context context, String str) {
        if (ai.c(str)) {
            return false;
        }
        try {
            context.startActivity(context.getPackageManager().getLaunchIntentForPackage(str));
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public static void b(String str) {
        Uri fromFile = Uri.fromFile(new File(str));
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
        intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
        d.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, String str, int i2) {
        Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        Intent intent2 = new Intent("android.intent.action.MAIN");
        intent2.addCategory("android.intent.category.LAUNCHER");
        intent2.setClass(context, WelcomeActivity.class);
        intent.putExtra("android.intent.extra.shortcut.INTENT", intent2);
        intent.putExtra("android.intent.extra.shortcut.NAME", str);
        intent.putExtra("duplicate", false);
        intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(context, i2));
        context.sendBroadcast(intent);
    }

    public static String a(NamedNodeMap namedNodeMap, String str) {
        String nodeValue;
        Node namedItem = namedNodeMap.getNamedItem(str);
        if (namedItem == null || (nodeValue = namedItem.getNodeValue()) == null) {
            return "";
        }
        return nodeValue;
    }

    public static String a(NamedNodeMap namedNodeMap, String str, String str2) {
        String nodeValue;
        Node namedItem = namedNodeMap.getNamedItem(str);
        if (namedItem == null || (nodeValue = namedItem.getNodeValue()) == null) {
            return str2;
        }
        return nodeValue;
    }

    public static int a(NamedNodeMap namedNodeMap, String str, int i2) {
        Node namedItem = namedNodeMap.getNamedItem(str);
        if (namedItem == null) {
            return i2;
        }
        String nodeValue = namedItem.getNodeValue();
        if (TextUtils.isEmpty(nodeValue)) {
            return i2;
        }
        try {
            return Integer.parseInt(nodeValue);
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
            return i2;
        }
    }

    public static boolean a(NamedNodeMap namedNodeMap, String str, boolean z) {
        String nodeValue;
        Node namedItem = namedNodeMap.getNamedItem(str);
        if (namedItem == null || (nodeValue = namedItem.getNodeValue()) == null) {
            return z;
        }
        try {
            return Boolean.parseBoolean(nodeValue);
        } catch (Throwable th) {
            th.printStackTrace();
            return z;
        }
    }

    public static boolean c(String str) {
        if (d == null || str == null || str.length() == 0) {
            return false;
        }
        com.umeng.a.b.a(d, str);
        return true;
    }

    public static void a(Context context, String str, HashMap<String, String> hashMap, long j2) {
        hashMap.put("__ct__", String.valueOf(j2));
        com.umeng.a.b.a(context, str, hashMap);
    }

    public static int a(float f2) {
        if (d == null) {
            return -1;
        }
        return (int) ((d.getResources().getDisplayMetrics().density * f2) + 0.5f);
    }

    public static String d(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (a(str.charAt(i2))) {
                sb.append(str.charAt(i2));
            }
        }
        return sb.toString();
    }

    public static boolean a(char c2) {
        return (c2 >= 19968 && c2 <= 40869) || (c2 >= 'A' && c2 <= 'Z') || ((c2 >= 'a' && c2 <= 'z') || ((c2 >= 0 && c2 <= 9) || c2 == '-' || c2 == '_' || c2 == '(' || c2 == ')' || c2 == ' '));
    }

    public static String e(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (!b(str.charAt(i2))) {
                sb.append(str.charAt(i2));
            }
        }
        return sb.toString();
    }

    public static boolean b(char c2) {
        return (c2 >= 0 && c2 <= 8) || (c2 >= 11 && c2 <= 12) || ((c2 >= 14 && c2 <= 31) || ((c2 >= 55296 && c2 <= 57343) || c2 == 65534 || c2 == 65535));
    }

    public static synchronized b s() {
        boolean z;
        b bVar;
        synchronized (g.class) {
            if (com.shoujiduoduo.base.a.a.b != b.none) {
                bVar = com.shoujiduoduo.base.a.a.b;
            } else {
                String a2 = af.a(RingDDApp.c(), "user_phone_num", "");
                int a3 = af.a(RingDDApp.c(), "user_loginStatus", 0);
                if (TextUtils.isEmpty(a2) || a3 == 0 || (bVar = g(a2)) == b.none) {
                    if (p != null) {
                        bVar = p.f2308a;
                    } else {
                        p = new a();
                        if (d == null) {
                            p.f2308a = b.none;
                            bVar = b.none;
                        } else {
                            if (!c.a().c() || !c.a().d()) {
                                z = false;
                            } else {
                                com.shoujiduoduo.base.a.a.a("CommonUtils", "多多判断：双卡平台，并且安装了中国移动sim卡");
                                z = true;
                            }
                            if (z) {
                                com.umeng.a.b.b(d, "DUAL_SIM_CARD_CMCC");
                                p.f2308a = b.f2309a;
                                bVar = b.f2309a;
                            } else if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
                                p.f2308a = b.f2309a;
                                bVar = b.f2309a;
                            } else {
                                TelephonyManager telephonyManager = (TelephonyManager) d.getSystemService("phone");
                                if (telephonyManager == null) {
                                    p.f2308a = b.none;
                                    bVar = b.none;
                                } else {
                                    String subscriberId = telephonyManager.getSubscriberId();
                                    if (subscriberId != null) {
                                        com.shoujiduoduo.base.a.a.a("CommonUtils", "imsi:" + subscriberId);
                                        if (subscriberId.startsWith("46000") || subscriberId.startsWith("46002") || subscriberId.startsWith("46007")) {
                                            com.shoujiduoduo.base.a.a.a("CommonUtils", IXAdRequestInfo.MAX_CONTENT_LENGTH);
                                            p.f2308a = b.f2309a;
                                            bVar = b.f2309a;
                                        } else if (subscriberId.startsWith("46001") || subscriberId.startsWith("46006")) {
                                            com.shoujiduoduo.base.a.a.a("CommonUtils", "cu");
                                            p.f2308a = b.cu;
                                            bVar = b.cu;
                                        } else if (subscriberId.startsWith("46003") || subscriberId.startsWith("46005")) {
                                            com.shoujiduoduo.base.a.a.a("CommonUtils", "ct");
                                            p.f2308a = b.ct;
                                            bVar = b.ct;
                                        } else if (subscriberId.startsWith("46020")) {
                                        }
                                    } else {
                                        com.shoujiduoduo.base.a.a.a("CommonUtils", "imsi is null");
                                    }
                                    p.f2308a = b.f2309a;
                                    bVar = p.f2308a;
                                }
                            }
                        }
                    }
                }
            }
        }
        return bVar;
    }

    public static boolean f(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return Pattern.compile("^((13[0-9])|14[5,7]|(15[^4,\\D])|(18[0-9])|(17[0-9]))\\d{8}$").matcher(str.trim().replace("+86", "")).matches();
    }

    public static b g(String str) {
        String i2 = i(str);
        if (i2.equals("1")) {
            return b.cu;
        }
        if (i2.equals("2")) {
            return b.f2309a;
        }
        if (i2.equals("3")) {
            return b.ct;
        }
        return b.none;
    }

    private static String i(String str) {
        if (TextUtils.isEmpty(str)) {
            return "0";
        }
        String trim = str.trim();
        if (trim.startsWith("0") || trim.startsWith("+860")) {
            trim = trim.substring(trim.indexOf("0") + 1);
        } else if (trim.startsWith("+86")) {
            trim = trim.substring(3);
        }
        if (trim.length() < 11) {
            return "0";
        }
        String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "cmcc_num_list");
        if (TextUtils.isEmpty(a2)) {
            a2 = "134/135/136/137/138/139/147/150/151/152/157/158/159/182/183/184/187/188/178";
        }
        String a3 = com.umeng.b.a.a().a(RingDDApp.c(), "cucc_num_list");
        if (TextUtils.isEmpty(a3)) {
            a3 = "130/131/132/145/155/156/185/186/176";
        }
        String a4 = com.umeng.b.a.a().a(RingDDApp.c(), "ctcc_num_list");
        if (TextUtils.isEmpty(a4)) {
            a4 = "133/153/180/181/189/177/170/173";
        }
        if (a2.contains(trim.substring(0, 3))) {
            return "2";
        }
        if (a3.contains(trim.substring(0, 3))) {
            return "1";
        }
        if (a4.contains(trim.substring(0, 3))) {
            return "3";
        }
        HashMap hashMap = new HashMap();
        hashMap.put("num", trim.substring(0, 3));
        hashMap.put("detail", trim);
        com.umeng.a.b.a(RingDDApp.c(), "unknown_phone_type", hashMap);
        return "0";
    }

    public static synchronized boolean t() {
        boolean z = false;
        synchronized (g.class) {
            if (!"false".equals(com.umeng.b.a.a().a(d, "cmcc_cailing_switch"))) {
                if (!RingDDApp.b().a() && s().equals(b.f2309a)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public static synchronized boolean u() {
        boolean z = false;
        synchronized (g.class) {
            if (!"false".equals(com.umeng.b.a.a().a(d, "cucc_cailing_switch"))) {
                if (s().equals(b.cu)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public static synchronized boolean v() {
        boolean z = false;
        synchronized (g.class) {
            if (!"false".equals(com.umeng.b.a.a().a(d, "ctcc_cailing_switch"))) {
                if (s().equals(b.ct)) {
                    z = true;
                }
            }
        }
        return z;
    }

    public static boolean w() {
        int a2 = af.a(d, "preference_continuous_play", -1);
        if (a2 < 0) {
            af.b(d, "preference_continuous_play", 1);
            return true;
        } else if (a2 != 1) {
            return false;
        } else {
            return true;
        }
    }

    public static void a(boolean z) {
        if (d != null) {
            af.b(d, "preference_continuous_play", z ? 1 : 0);
        }
    }

    public static String a(int i2) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        do {
            int nextInt = random.nextInt();
            if (nextInt != Integer.MIN_VALUE) {
                sb.append(Math.abs(nextInt) % 10);
            }
        } while (sb.length() < i2);
        return sb.toString();
    }

    public static boolean c(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        ActivityManager activityManager = (ActivityManager) context.getSystemService(PushConstants.INTENT_ACTIVITY_NAME);
        if (!(activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null)) {
            String packageName = context.getPackageName();
            int myPid = Process.myPid();
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next.pid == myPid && packageName.equals(next.processName)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void a(String str, String str2) {
        final String str3 = "{\"AppId\":203,\"SVer\":\"" + q() + "\",\"Ver\":" + p() + ",\"OSVer\":\"" + Build.VERSION.RELEASE + "\",\"Brand\":\"" + Build.BRAND + "\",\"Uchannel\":\"" + k() + "\",\"Network\":\"" + NetworkStateUtil.d() + "\",\"YYS\":\"" + s().toString() + "\",\"Msg\":\"" + str + "\",\"Contact\":\"" + str2 + "\",\"Device\":\"" + Build.MODEL + "\",\"Uid\":\"" + a() + "\"}";
        com.shoujiduoduo.base.a.a.a("CommonUtils", "contact:" + str2 + ", msg:" + str);
        i.a(new Runnable() {
            public void run() {
                ArrayList arrayList = new ArrayList();
                arrayList.add(new BasicNameValuePair("data", str3));
                arrayList.add(new BasicNameValuePair("appid", "ringdd"));
                arrayList.add(new BasicNameValuePair("act", "fb"));
                arrayList.add(new BasicNameValuePair("pl", "ar"));
                aq.a("http://log.djduoduo.com/logs/log.php", arrayList);
            }
        });
    }

    public static String h(String str) {
        if (!TextUtils.isEmpty(str) && NativeDES.a()) {
            return s.a(f2306a.Encrypt(str));
        }
        return str;
    }

    public static boolean a(Bitmap bitmap, Bitmap.CompressFormat compressFormat, int i2, String str) {
        File file = new File(str);
        try {
            file.createNewFile();
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                bitmap.compress(compressFormat, i2, fileOutputStream);
                try {
                    fileOutputStream.flush();
                    try {
                        fileOutputStream.close();
                        return true;
                    } catch (IOException e2) {
                        return false;
                    }
                } catch (IOException e3) {
                    return false;
                }
            } catch (FileNotFoundException e4) {
                return false;
            }
        } catch (IOException e5) {
            return false;
        }
    }

    public static String a(Date date) {
        Date date2 = new Date();
        long time = date2.getTime() - date.getTime();
        if (time < 0) {
            return new SimpleDateFormat("yyyy年MM月dd日").format(date);
        }
        if (time < 60000) {
            return "刚刚";
        }
        if (time < 3600000) {
            return (time / 60000) + "分钟前";
        }
        if (date2.getDay() == date.getDay()) {
            return new SimpleDateFormat("HH:mm").format(date);
        }
        if (date2.getDay() - date.getDay() == 1) {
            return "昨天" + new SimpleDateFormat("HH:mm").format(date);
        } else if (date2.getYear() == date2.getYear()) {
            return (date.getMonth() + 1) + "月" + date.getDate() + "日";
        } else {
            return new SimpleDateFormat("yyyy年MM月dd日").format(date);
        }
    }

    public static String b(Date date) {
        long time = new Date().getTime() - date.getTime();
        if (time < 0) {
            return new SimpleDateFormat("yyyy年MM月dd日").format(date);
        }
        if (time < 60000) {
            return "刚刚";
        }
        if (time < 3600000) {
            return (time / 60000) + "分钟前";
        }
        if (time < LogBuilder.MAX_INTERVAL) {
            return (time / 3600000) + "小时前";
        }
        if (time < 2592000000L) {
            return (time / LogBuilder.MAX_INTERVAL) + "天前";
        }
        if (time < 31104000000L) {
            return (time / 2592000000L) + "月前";
        }
        return (time / 31104000000L) + "年前";
    }
}
