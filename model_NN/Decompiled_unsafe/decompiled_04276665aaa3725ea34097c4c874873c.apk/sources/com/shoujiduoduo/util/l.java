package com.shoujiduoduo.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.wifi.WifiManager;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;

/* compiled from: DeviceInfo */
public final class l {

    /* renamed from: a  reason: collision with root package name */
    public static String f2312a;
    public static int b;
    public static int c;
    public static float d;
    public static int e;
    public static float f;
    public static boolean g;
    public static boolean h;
    public static long i;
    public static int j;
    public static int k;
    private static boolean l;

    public static void a() {
        if (!l) {
            Context c2 = RingDDApp.c();
            try {
                f2312a = ((WifiManager) c2.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo().getMacAddress();
                if (TextUtils.isEmpty(f2312a)) {
                    f2312a = "";
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                f2312a = "";
            }
            a(c2);
            try {
                g = ((AudioManager) c2.getSystemService("audio")).isWiredHeadsetOn();
            } catch (Exception e3) {
                e3.printStackTrace();
                g = false;
            }
            try {
                c2.getPackageManager().getPackageInfo("miui", 8192);
                h = true;
            } catch (PackageManager.NameNotFoundException e4) {
                h = false;
            }
            i = c();
            j = (int) ((i / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID);
            k = b();
            l = true;
        }
    }

    public static void a(Context context) {
        if (b == 0) {
            try {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
                b = Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels);
                c = Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels);
                d = displayMetrics.density;
                a.a("DENSITY", "initScreenInfo--" + d);
                e = displayMetrics.densityDpi;
                f = displayMetrics.scaledDensity;
                a.a("DeviceInfo", "DENSITY:" + d + " WIDTH:" + b + " HEIGHT:" + c);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private static long c() {
        BufferedReader bufferedReader;
        long j2 = 0;
        try {
            bufferedReader = new BufferedReader(new FileReader("/proc/meminfo"), 1024);
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                bufferedReader.close();
                return j2;
            }
            j2 = ((long) Integer.valueOf(readLine.split("\\s+")[1]).intValue()) * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
            bufferedReader.close();
            return j2;
        } catch (Throwable th) {
        }
    }

    public static int b() {
        String str;
        String str2 = "";
        try {
            InputStream inputStream = new ProcessBuilder("/system/bin/cat", "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq").start().getInputStream();
            byte[] bArr = new byte[24];
            while (inputStream.read(bArr) != -1) {
                str2 = str2 + new String(bArr);
            }
            str = ((int) (Float.valueOf(str2).floatValue() / 1000.0f)) + "";
            inputStream.close();
        } catch (Error e2) {
            str = "1024";
        } catch (Exception e3) {
            str = "1024";
        }
        try {
            return Integer.parseInt(str);
        } catch (Exception e4) {
            return 512;
        }
    }
}
