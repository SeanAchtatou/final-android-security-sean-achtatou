package com.shoujiduoduo.util.f;

import cn.banshenggua.aichang.utils.FileUtils;
import com.igexin.download.Downloads;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

/* compiled from: Frames */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static int f2301a;
    private static int b;
    private e c;
    private Vector<C0055a> d = new Vector<>();

    public a(e eVar) throws d {
        this.c = eVar;
        try {
            FileInputStream fileInputStream = new FileInputStream(eVar.a());
            fileInputStream.skip((long) eVar.b());
            a(fileInputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public long a(long j) {
        long j2 = j / 260;
        if (j2 >= ((long) this.d.size())) {
            j2 = (long) (this.d.size() - 1);
        }
        return (long) this.d.get((int) j2).f2302a;
    }

    private void a(InputStream inputStream) throws d {
        try {
            int b2 = this.c.b();
            long c2 = this.c.c() - 128;
            int i = 0;
            int i2 = 0;
            while (inputStream.available() > 0 && ((long) b2) < c2) {
                int read = inputStream.read();
                while (read != 255 && read != -1) {
                    read = inputStream.read();
                }
                int read2 = inputStream.read();
                if (read2 > 224) {
                    int read3 = inputStream.read();
                    inputStream.read();
                    int b3 = b(read2, 4);
                    int b4 = b(read2, 3);
                    if ((b4 == 0) && (b3 == 0)) {
                        throw new d("MPEG 2.5 is not supported");
                    }
                    f2301a = b4 == 0 ? 2 : 1;
                    b = 4 - ((b(read2, 2) << 1) + b(read2, 1));
                    b(read2, 0);
                    int a2 = (((f2301a == 1 ? 144 : 72) * (a(b(read3, 7), b(read3, 6), b(read3, 5), b(read3, 4)) * 1000)) / a(b(read3, 3), b(read3, 2))) + b(read3, 1);
                    inputStream.skip((long) (a2 - 4));
                    i += a2;
                    i2++;
                    if (i2 == 10) {
                        this.d.add(new C0055a(b2, i));
                        i = 0;
                        i2 = 0;
                    }
                    b2 += a2;
                }
            }
            if (i != 0) {
                this.d.add(new C0055a(b2, i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public int a(int i, int i2) {
        int i3 = 0;
        switch ((i << 1) | i2) {
            case 0:
                i3 = 44100;
                break;
            case 1:
                i3 = 48000;
                break;
            case 2:
                i3 = 32000;
                break;
        }
        return f2301a == 1 ? i3 : i3 / 2;
    }

    /* access modifiers changed from: protected */
    public int a(int i, int i2, int i3, int i4) {
        return new int[][]{new int[]{0, 0, 0, 0, 0, 0}, new int[]{32, 32, 32, 32, 32, 8}, new int[]{64, 48, 40, 64, 48, 16}, new int[]{96, 56, 48, 96, 56, 24}, new int[]{128, 64, 56, 128, 64, 32}, new int[]{160, 80, 64, 160, 80, 64}, new int[]{Downloads.STATUS_RUNNING, 96, 80, Downloads.STATUS_RUNNING, 96, 80}, new int[]{224, 112, 96, 224, 112, 56}, new int[]{256, 128, 112, 256, 128, 64}, new int[]{288, 160, 128, 288, 160, 128}, new int[]{320, Downloads.STATUS_RUNNING, 160, 320, Downloads.STATUS_RUNNING, 160}, new int[]{352, 224, Downloads.STATUS_RUNNING, 352, 224, 112}, new int[]{384, 256, 224, 384, 256, 128}, new int[]{416, 320, 256, 416, 320, 256}, new int[]{FileUtils.S_IRWXU, 384, 320, FileUtils.S_IRWXU, 384, 320}, new int[]{0, 0, 0, 0, 0, 0}}[(i << 3) | (i2 << 2) | (i3 << 1) | i4][(((f2301a - 1) * 3) + b) - 1];
    }

    private int b(int i, int i2) {
        return ((1 << i2) & i) > 0 ? 1 : 0;
    }

    /* renamed from: com.shoujiduoduo.util.f.a$a  reason: collision with other inner class name */
    /* compiled from: Frames */
    class C0055a {

        /* renamed from: a  reason: collision with root package name */
        int f2302a;
        int b;

        public C0055a(int i, int i2) {
            this.f2302a = i;
            this.b = i2;
        }
    }
}
