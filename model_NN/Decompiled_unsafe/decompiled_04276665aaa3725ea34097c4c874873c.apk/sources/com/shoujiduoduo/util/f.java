package com.shoujiduoduo.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.ViewCompat;
import com.d.a.b.a.i;
import com.d.a.b.c.a;
import com.d.a.b.e.c;

/* compiled from: CircleBitmapDisplayer2 */
public class f implements a {

    /* renamed from: a  reason: collision with root package name */
    private float f2300a;
    private float b;
    private float c;
    private float d;
    private int e;
    private boolean f;
    private boolean g;

    public f() {
        this.d = 0.0f;
        this.e = ViewCompat.MEASURED_STATE_MASK;
        this.f = false;
        this.g = true;
        this.f = true;
    }

    public f(float f2, int i) {
        this();
        this.d = f2;
        this.e = i;
    }

    public void display(Bitmap bitmap, com.d.a.b.e.a aVar, i iVar) {
        Rect rect;
        if (!(aVar instanceof c)) {
            throw new IllegalArgumentException("ImageAware should wrap ImageView. ImageViewAware is expected.");
        }
        int a2 = aVar.a();
        int b2 = aVar.b();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (this.g) {
            this.b = ((float) a2) / 2.0f;
            this.c = ((float) b2) / 2.0f;
        }
        if (this.f) {
            if (this.g) {
                this.f2300a = a2 < b2 ? ((float) a2) / 2.0f : ((float) b2) / 2.0f;
            } else {
                this.f2300a = Math.min(this.b < ((float) a2) - this.b ? this.b : ((float) a2) - this.b, this.c < ((float) b2) - this.c ? this.b : ((float) b2) - this.c);
            }
        }
        if (width < height) {
            rect = new Rect(0, (height - width) / 2, width, ((height - width) / 2) + width);
        } else {
            rect = new Rect((width - height) / 2, 0, ((width - height) / 2) + height, height);
        }
        aVar.a(a(bitmap, this.b, this.c, this.f2300a, rect, new RectF(0.0f, 0.0f, (float) a2, (float) b2), a2, b2, this.d, this.e));
    }

    public static Bitmap a(Bitmap bitmap, float f2, float f3, float f4, Rect rect, RectF rectF, int i, int i2, float f5, int i3) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        canvas.drawCircle(f2, f3, f4 - f5, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rectF, paint);
        if (0.0f < f5) {
            paint.setXfermode(null);
            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(i3);
            paint.setStrokeWidth(f5);
            canvas.drawCircle(f2, f3, f4 - (f5 / 2.0f), paint);
        }
        return createBitmap;
    }
}
