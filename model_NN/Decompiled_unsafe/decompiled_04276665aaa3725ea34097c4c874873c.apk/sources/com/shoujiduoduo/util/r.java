package com.shoujiduoduo.util;

import android.content.Context;
import android.text.TextUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/* compiled from: FileUtils */
public class r {

    /* renamed from: a  reason: collision with root package name */
    private static Context f2326a = null;

    public static void a(Context context) {
        f2326a = context;
    }

    public static boolean a(String str, String str2) {
        if (str == null || str.length() == 0 || str2 == null || str2.length() == 0) {
            return false;
        }
        try {
            return new File(str).renameTo(new File(str2));
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean b(String str, String str2) {
        try {
            FileWriter fileWriter = new FileWriter(str);
            fileWriter.write(str2);
            fileWriter.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static boolean a(String str, byte[] bArr) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(str);
            fileOutputStream.write(bArr);
            fileOutputStream.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.FileOutputStream] */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.io.FileInputStream] */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0029 A[SYNTHETIC, Splitter:B:14:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002e A[SYNTHETIC, Splitter:B:17:0x002e] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0044 A[SYNTHETIC, Splitter:B:31:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0049 A[SYNTHETIC, Splitter:B:34:0x0049] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.io.File r6, java.io.File r7) {
        /*
            r2 = 0
            r0 = 0
            java.lang.System.currentTimeMillis()
            boolean r1 = r6.exists()
            if (r1 == 0) goto L_0x0031
            r1 = 2048(0x800, float:2.87E-42)
            byte[] r4 = new byte[r1]
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x005c, all -> 0x0040 }
            r3.<init>(r6)     // Catch:{ IOException -> 0x005c, all -> 0x0040 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x005f, all -> 0x0057 }
            r1.<init>(r7)     // Catch:{ IOException -> 0x005f, all -> 0x0057 }
        L_0x0019:
            int r2 = r3.read(r4)     // Catch:{ IOException -> 0x0025, all -> 0x0059 }
            r5 = -1
            if (r2 == r5) goto L_0x0032
            r5 = 0
            r1.write(r4, r5, r2)     // Catch:{ IOException -> 0x0025, all -> 0x0059 }
            goto L_0x0019
        L_0x0025:
            r2 = move-exception
            r2 = r3
        L_0x0027:
            if (r2 == 0) goto L_0x002c
            r2.close()     // Catch:{ IOException -> 0x004f }
        L_0x002c:
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x0051 }
        L_0x0031:
            return r0
        L_0x0032:
            r0 = 1
            if (r3 == 0) goto L_0x0038
            r3.close()     // Catch:{ IOException -> 0x004d }
        L_0x0038:
            if (r1 == 0) goto L_0x0031
            r1.close()     // Catch:{ IOException -> 0x003e }
            goto L_0x0031
        L_0x003e:
            r1 = move-exception
            goto L_0x0031
        L_0x0040:
            r0 = move-exception
            r3 = r2
        L_0x0042:
            if (r3 == 0) goto L_0x0047
            r3.close()     // Catch:{ IOException -> 0x0053 }
        L_0x0047:
            if (r2 == 0) goto L_0x004c
            r2.close()     // Catch:{ IOException -> 0x0055 }
        L_0x004c:
            throw r0
        L_0x004d:
            r2 = move-exception
            goto L_0x0038
        L_0x004f:
            r2 = move-exception
            goto L_0x002c
        L_0x0051:
            r1 = move-exception
            goto L_0x0031
        L_0x0053:
            r1 = move-exception
            goto L_0x0047
        L_0x0055:
            r1 = move-exception
            goto L_0x004c
        L_0x0057:
            r0 = move-exception
            goto L_0x0042
        L_0x0059:
            r0 = move-exception
            r2 = r1
            goto L_0x0042
        L_0x005c:
            r1 = move-exception
            r1 = r2
            goto L_0x0027
        L_0x005f:
            r1 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.r.a(java.io.File, java.io.File):boolean");
    }

    public static String a(String str) {
        int lastIndexOf = str.lastIndexOf(".");
        if (lastIndexOf != -1) {
            return str.substring(0, lastIndexOf);
        }
        return "";
    }

    public static String b(String str) {
        int lastIndexOf;
        if (TextUtils.isEmpty(str) || (lastIndexOf = str.lastIndexOf(".")) == -1) {
            return "";
        }
        String str2 = "";
        try {
            str2 = str.substring(lastIndexOf + 1, str.length());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(str2)) {
            return "";
        }
        return str2;
    }

    public static String c(String str) {
        int lastIndexOf;
        if (!TextUtils.isEmpty(str) && (lastIndexOf = str.lastIndexOf(File.separator)) > 0 && lastIndexOf != str.length() - 1) {
            return a(str.substring(lastIndexOf + 1, str.length()));
        }
        return str;
    }

    public static String d(String str) {
        String[] split;
        if (str == null || (split = str.split("/")) == null || split.length < 1) {
            return null;
        }
        return split[split.length - 1];
    }

    public static String e(String str) {
        int lastIndexOf = str.lastIndexOf(File.separator);
        if (lastIndexOf != -1) {
            return str.substring(0, lastIndexOf);
        }
        return "";
    }

    public static boolean f(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return new File(str).exists();
    }

    public static boolean g(String str) {
        if (ai.c(str)) {
            return false;
        }
        try {
            File file = new File(str);
            if (!file.exists() || file.delete()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
