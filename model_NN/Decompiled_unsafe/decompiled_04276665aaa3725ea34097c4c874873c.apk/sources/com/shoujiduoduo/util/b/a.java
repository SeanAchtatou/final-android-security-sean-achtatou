package com.shoujiduoduo.util.b;

import android.os.Handler;
import android.os.Looper;
import com.shoujiduoduo.base.bean.UserInfo;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.util.Iterator;

/* compiled from: CailingUtils */
public class a {

    /* renamed from: com.shoujiduoduo.util.b.a$a  reason: collision with other inner class name */
    /* compiled from: CailingUtils */
    public static class C0050a {

        /* renamed from: a  reason: collision with root package name */
        public String f2217a;
        public String b;
        public String c;
    }

    /* compiled from: CailingUtils */
    public interface b {
        void a(C0050a aVar);

        void a(String str, String str2);
    }

    public static void a(final g.b bVar, final b bVar2) {
        final Handler handler = new Handler(Looper.getMainLooper());
        i.a(new Runnable() {
            public void run() {
                if (bVar == g.b.f2309a) {
                    a.d(handler, bVar2);
                } else if (bVar == g.b.cu) {
                    a.e(handler, bVar2);
                } else if (bVar == g.b.ct) {
                    a.f(handler, bVar2);
                } else {
                    com.shoujiduoduo.base.a.a.a("CailingUtils", "wrong type");
                }
            }
        });
    }

    private static void a(Handler handler, final b bVar, final C0050a aVar) {
        handler.post(new Runnable() {
            public void run() {
                bVar.a(aVar);
            }
        });
    }

    private static void a(Handler handler, final b bVar, final String str, final String str2) {
        handler.post(new Runnable() {
            public void run() {
                bVar.a(str, str2);
            }
        });
    }

    /* access modifiers changed from: private */
    public static void d(Handler handler, b bVar) {
        c.b b2 = com.shoujiduoduo.util.c.b.a().b("");
        if (!b2.c() || !(b2 instanceof c.z)) {
            a(handler, bVar, b2.a(), b2.b());
            return;
        }
        c.z zVar = (c.z) b2;
        if (zVar.a() == null || zVar.d() == null || zVar.d().size() <= 0) {
            a(handler, bVar, WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "RingBoxResult 格式错误");
            return;
        }
        C0050a aVar = new C0050a();
        aVar.f2217a = zVar.d().get(0).b();
        aVar.c = zVar.d().get(0).c();
        aVar.b = zVar.d().get(0).d();
        a(handler, bVar, aVar);
    }

    /* access modifiers changed from: private */
    public static void e(Handler handler, b bVar) {
        boolean z = false;
        c.b c = com.shoujiduoduo.util.e.a.a().c();
        if (!c.c() || !(c instanceof c.s)) {
            a(handler, bVar, c.a(), c.b());
            return;
        }
        c.s sVar = (c.s) c;
        C0050a aVar = new C0050a();
        if (sVar.d != null) {
            int i = 0;
            while (true) {
                if (i >= sVar.d.length) {
                    break;
                } else if (sVar.d[i].c.equals("0")) {
                    aVar.f2217a = sVar.d[i].d;
                    com.shoujiduoduo.base.a.a.a("CailingUtils", "default cucc cailing id:" + aVar.f2217a);
                    break;
                } else {
                    i++;
                }
            }
        }
        if (!ai.c(aVar.f2217a)) {
            c.b d = com.shoujiduoduo.util.e.a.a().d();
            if (d instanceof c.u) {
                c.ab[] abVarArr = ((c.u) d).d;
                if (abVarArr == null || abVarArr.length <= 0) {
                    a(handler, bVar, WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "");
                    return;
                }
                int i2 = 0;
                while (true) {
                    if (i2 >= abVarArr.length) {
                        break;
                    } else if (aVar.f2217a.equals(abVarArr[i2].f2222a)) {
                        aVar.c = abVarArr[i2].b;
                        aVar.b = abVarArr[i2].d;
                        z = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z) {
                    a(handler, bVar, aVar);
                } else {
                    a(handler, bVar, WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "");
                }
            } else {
                a(handler, bVar, WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "");
            }
        } else {
            a(handler, bVar, WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "");
        }
    }

    /* access modifiers changed from: private */
    public static void f(Handler handler, b bVar) {
        boolean z;
        UserInfo c = com.shoujiduoduo.a.b.b.g().c();
        c.b d = com.shoujiduoduo.util.d.b.a().d(c.getPhoneNum());
        if (!d.c() || !(d instanceof c.z)) {
            a(handler, bVar, d.a(), d.b());
            return;
        }
        c.z zVar = (c.z) d;
        if (zVar.a() == null || zVar.d() == null || zVar.d().size() <= 0) {
            a(handler, bVar, WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "");
            return;
        }
        C0050a aVar = new C0050a();
        aVar.f2217a = zVar.d().get(0).b();
        c.b c2 = com.shoujiduoduo.util.d.b.a().c(c.getPhoneNum());
        if (!c2.c() || !(c2 instanceof c.z)) {
            a(handler, bVar, c2.a(), c2.b());
            return;
        }
        Iterator<c.ae> it = ((c.z) c2).d().iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            c.ae next = it.next();
            if (aVar.f2217a.equals(next.b())) {
                aVar.b = next.d();
                aVar.c = next.c();
                z = true;
                break;
            }
        }
        if (z) {
            a(handler, bVar, aVar);
        } else {
            a(handler, bVar, WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "");
        }
    }
}
