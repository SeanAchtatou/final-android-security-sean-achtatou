package com.shoujiduoduo.util.b;

import android.text.TextUtils;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.MusicInfo;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.util.List;

/* compiled from: RequstResult */
public class c {

    /* compiled from: RequstResult */
    public static class a extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2220a;
    }

    /* compiled from: RequstResult */
    public static class aa extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2221a;
        public String d;
    }

    /* compiled from: RequstResult */
    public static class ab {

        /* renamed from: a  reason: collision with root package name */
        public String f2222a = "";
        public String b = "";
        public String c = "";
        public String d = "";
        public String e = "";
        public String f = "";
        public String g = "";
        public String h = "";
    }

    /* compiled from: RequstResult */
    public static class ac extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2223a;
    }

    /* compiled from: RequstResult */
    public static class ad extends b {

        /* renamed from: a  reason: collision with root package name */
        public p f2224a;
    }

    /* compiled from: RequstResult */
    public static class ag extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2227a = "";
    }

    /* compiled from: RequstResult */
    public static class ah extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2228a;
        public String d;
    }

    /* compiled from: RequstResult */
    public static class ai extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2229a;
    }

    /* compiled from: RequstResult */
    public static class aj {

        /* renamed from: a  reason: collision with root package name */
        public String f2230a;
        public String b;
        public String c;
        public String d;
        public String e;
    }

    /* compiled from: RequstResult */
    public static class ak {

        /* renamed from: a  reason: collision with root package name */
        public String f2231a = "";
        public String b = "";
        public String c = "";
        public String d = "";
        public String e = "";
    }

    /* renamed from: com.shoujiduoduo.util.b.c$c  reason: collision with other inner class name */
    /* compiled from: RequstResult */
    public static class C0051c {

        /* renamed from: a  reason: collision with root package name */
        public String f2232a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
    }

    /* compiled from: RequstResult */
    public static class g extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2236a = "";
    }

    /* compiled from: RequstResult */
    public static class h extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2237a;
        public String d;
        public String e;
    }

    /* compiled from: RequstResult */
    public static class j extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2239a;
    }

    /* compiled from: RequstResult */
    public static class k extends b {

        /* renamed from: a  reason: collision with root package name */
        public C0051c f2240a;
        public String d;
    }

    /* compiled from: RequstResult */
    public static class l extends b {

        /* renamed from: a  reason: collision with root package name */
        public BizInfo f2241a;
        public BizInfo d;
        public MusicInfo e;
        public String f;
        public String g;
    }

    /* compiled from: RequstResult */
    public static class m extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2242a = "";
        public int d;
        public String e = "";
    }

    /* compiled from: RequstResult */
    public static class n extends b {

        /* renamed from: a  reason: collision with root package name */
        public a f2243a;
        public String d;

        /* compiled from: RequstResult */
        public enum a {
            none,
            wap,
            sms_code,
            all
        }
    }

    /* compiled from: RequstResult */
    public static class p {

        /* renamed from: a  reason: collision with root package name */
        public String f2246a;
        public String b;
    }

    /* compiled from: RequstResult */
    public static class q extends b {

        /* renamed from: a  reason: collision with root package name */
        public y f2247a;
    }

    /* compiled from: RequstResult */
    public static class r extends b {

        /* renamed from: a  reason: collision with root package name */
        public boolean f2248a;
    }

    /* compiled from: RequstResult */
    public static class s extends b {

        /* renamed from: a  reason: collision with root package name */
        public int f2249a;
        public ak[] d;
    }

    /* compiled from: RequstResult */
    public static class t extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2250a;
        public String d;
        public String e;
        public String f;
    }

    /* compiled from: RequstResult */
    public static class u extends b {

        /* renamed from: a  reason: collision with root package name */
        public int f2251a;
        public ab[] d;
    }

    /* compiled from: RequstResult */
    public static class v extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2252a;
        public boolean d;
    }

    /* compiled from: RequstResult */
    public static class w extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2253a = "";
    }

    /* compiled from: RequstResult */
    public static class x extends b {

        /* renamed from: a  reason: collision with root package name */
        public ae f2254a;
        public String d = "";
    }

    /* compiled from: RequstResult */
    public static class y {

        /* renamed from: a  reason: collision with root package name */
        public String f2255a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
        public String h;
    }

    /* compiled from: RequstResult */
    public static class b {
        public String b;
        public String c;

        public b() {
            this.b = "";
            this.c = "";
        }

        public b(String str, String str2) {
            this.b = str;
            this.c = str2;
        }

        public String a() {
            return this.b;
        }

        public String b() {
            return this.c;
        }

        public String toString() {
            return "code:" + this.b + ", msg:" + this.c;
        }

        public boolean c() {
            return this.b.equals("0000") || this.b.equals("000000") || this.b.equals("0");
        }
    }

    /* compiled from: RequstResult */
    public static class ae {

        /* renamed from: a  reason: collision with root package name */
        public String f2225a = "";
        public String b = "";
        public String c = "";
        public String d = "";
        public String e = "";
        public String f = "";
        public String g = "";
        public String h = "";
        public String i = "";
        public String j = "";

        public String a() {
            return this.f;
        }

        public String b() {
            return this.f2225a;
        }

        public String c() {
            return this.b;
        }

        public String d() {
            return this.d;
        }

        public String e() {
            return this.g;
        }
    }

    /* compiled from: RequstResult */
    public static class z extends b {

        /* renamed from: a  reason: collision with root package name */
        public List<ae> f2256a;

        public List<ae> d() {
            return this.f2256a;
        }
    }

    /* compiled from: RequstResult */
    public static class d extends b {

        /* renamed from: a  reason: collision with root package name */
        public b f2233a;
        public b d;

        public boolean d() {
            return this.f2233a != null && this.f2233a.c();
        }

        public boolean e() {
            return this.d != null && this.d.c();
        }
    }

    /* compiled from: RequstResult */
    public static class o extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2245a;

        public String d() {
            return this.f2245a;
        }
    }

    /* compiled from: RequstResult */
    public static class i extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2238a;

        public String d() {
            return this.f2238a;
        }
    }

    /* compiled from: RequstResult */
    public static class e extends b {

        /* renamed from: a  reason: collision with root package name */
        public b f2234a;
        public b d;
        public b e;

        public boolean d() {
            if (this.f2234a != null) {
                return this.f2234a.a().equals("0000");
            }
            return false;
        }

        public boolean e() {
            if (this.d instanceof v) {
                return ((v) this.d).d;
            }
            return false;
        }

        public boolean f() {
            if (this.e instanceof v) {
                return ((v) this.e).d;
            }
            return false;
        }
    }

    /* compiled from: RequstResult */
    public static class af extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f2226a = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;

        public boolean d() {
            return this.f2226a.equals("0") || this.f2226a.equals("2") || this.f2226a.equals("4");
        }
    }

    /* compiled from: RequstResult */
    public static class f extends b {

        /* renamed from: a  reason: collision with root package name */
        public b f2235a;
        public b d;
        public b e;
        public b f;

        public boolean d() {
            if (this.f2235a == null || !this.f2235a.c() || !(this.f2235a instanceof af)) {
                return false;
            }
            return ((af) this.f2235a).d();
        }

        public boolean e() {
            if (this.d == null || !this.d.c() || !(this.d instanceof r)) {
                return false;
            }
            return ((r) this.d).f2248a;
        }

        public String f() {
            if (this.f == null || !this.f.c() || !(this.f instanceof ah)) {
                return "";
            }
            return ((ah) this.f).f2228a;
        }

        public String g() {
            if (this.e == null || !this.e.c() || !(this.e instanceof ai)) {
                return "";
            }
            return ((ai) this.e).f2229a;
        }

        public boolean h() {
            if (this.e == null || !this.e.c() || !(this.e instanceof ai)) {
                com.shoujiduoduo.base.a.a.a("RequestResult", "is4G, return:false");
                return false;
            }
            ai aiVar = (ai) this.e;
            com.shoujiduoduo.base.a.a.a("RequestResult", "is4G, return:" + aiVar.f2229a.equals("4"));
            return aiVar.f2229a.equals("4");
        }

        public boolean i() {
            return h() && !com.umeng.b.a.a().a(RingDDApp.c(), "cucc_all_4g_open").equals("false");
        }

        public boolean j() {
            String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "cu_4g_cailing_free_switch");
            if (h() && !a2.equals("false")) {
                com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return true");
                return true;
            } else if (this.f == null || !this.f.c() || !(this.f instanceof ah)) {
                com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return false 2");
                return false;
            } else {
                ah ahVar = (ah) this.f;
                String a3 = com.umeng.b.a.a().a(RingDDApp.c(), "cu_3g_cailing_free");
                if (TextUtils.isEmpty(a3) || !a3.contains(ahVar.f2228a)) {
                    com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return false");
                    return false;
                }
                com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return true 2, provinceId:" + ahVar.f2228a + ", ids:" + a3);
                return true;
            }
        }
    }
}
