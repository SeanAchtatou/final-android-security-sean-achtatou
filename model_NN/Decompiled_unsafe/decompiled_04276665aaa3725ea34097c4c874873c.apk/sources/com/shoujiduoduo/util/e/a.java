package com.shoujiduoduo.util.e;

import android.text.TextUtils;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.af;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.u;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

/* compiled from: ChinaUnicomUtils */
public class a {
    /* access modifiers changed from: private */
    public static final c.b h = new c.b(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "对不起，中国联通的彩铃服务正在进行系统维护，请谅解");
    /* access modifiers changed from: private */
    public static final c.b i = new c.b("-2", "对不起，网络好像出了点问题，请稍后再试试");

    /* renamed from: a  reason: collision with root package name */
    private String f2291a = "3000004860";
    private String b = "860FF03C47581ED6";
    private String c = "www.shoujiduoduo.com";
    private String d = "openplatform";
    private String e;
    private String f;
    private boolean g;
    /* access modifiers changed from: private */
    public HashMap<String, c> j = new HashMap<>();

    /* renamed from: com.shoujiduoduo.util.e.a$a  reason: collision with other inner class name */
    /* compiled from: ChinaUnicomUtils */
    private enum C0054a {
        GET,
        POST
    }

    /* compiled from: ChinaUnicomUtils */
    private static class b {

        /* renamed from: a  reason: collision with root package name */
        public static a f2296a = new a();
    }

    /* compiled from: ChinaUnicomUtils */
    private enum d {
        unknown,
        open,
        close
    }

    public void a(String str, String str2) {
        this.e = str2;
        this.f = str;
        af.c(RingDDApp.c(), str + "_token", str2);
    }

    public boolean a(String str) {
        String a2 = af.a(RingDDApp.c(), str + "_token", "");
        if (TextUtils.isEmpty(a2)) {
            return false;
        }
        this.e = a2;
        this.f = str;
        return true;
    }

    public static String b(String str) {
        if (str.length() == 11) {
            return "917890004860" + str.substring(1);
        }
        return "";
    }

    public static a a() {
        return b.f2296a;
    }

    public void a(com.shoujiduoduo.util.b.b bVar) {
        a(new ArrayList(), "/v1/uerNetInfo/genUnikey", bVar, C0054a.GET);
    }

    public void b(com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("showNum", "50"));
        arrayList.add(new BasicNameValuePair("pageNo", "0"));
        a(arrayList, "/v1/ringGroup/qryUserBox", bVar, C0054a.GET);
    }

    public void a(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("boxID", str));
        a(arrayList, "/v1/ringGroup/qryBoxMem", bVar, C0054a.GET);
    }

    public void b(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("unikey", str));
        a(arrayList, "/v1/uerNetInfo/qryUserMobile", bVar, C0054a.GET);
    }

    public void a(boolean z, String str, String str2) {
        this.g = z;
        if (this.g) {
            HashMap hashMap = new HashMap();
            hashMap.put("provinceId", str);
            hashMap.put("provinceName", str2);
            com.umeng.a.b.a(RingDDApp.c(), "area_support_cucc", hashMap);
        }
    }

    public boolean b() {
        return true;
    }

    public boolean c(String str) {
        String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "cu_cailing_province_new");
        if (ai.c(a2)) {
            a2 = "11/13/17/18/19/30/31/34/36/38/50/51/70/75/76/79/81//85/87/88/89/97";
        }
        if (a2.contains("all") || a2.contains(str)) {
            return true;
        }
        return false;
    }

    public void c(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("callNumber", str));
        a(arrayList, "/v1/verifyCode/sendLoginCode", bVar, "&phone=" + str, C0054a.GET);
    }

    public void a(String str, String str2, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("callNumber", str));
        arrayList.add(new BasicNameValuePair("verifyCode", str2));
        this.f = str;
        a(arrayList, "/v1/token/codeAuthToken", bVar, "&phone=" + this.f, C0054a.GET);
    }

    public void d(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("productId", "0000001599"));
        a(arrayList, "/v1/product/subProduct", bVar, str, C0054a.GET);
    }

    public void c(com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        a(arrayList, "/v1/product/qrySubedProducts", bVar, "&phone=" + this.f, C0054a.GET);
    }

    public void d(com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("productId", "0000001599"));
        a(arrayList, "/v1/product/unSubProduct", bVar, C0054a.GET);
    }

    public void e(com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        a(arrayList, "/v1/ring/qryUserBasInfo", bVar, "&phone=" + this.f, C0054a.GET);
    }

    public void f(com.shoujiduoduo.util.b.b bVar) {
        com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "checkCailingAndVip");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new BasicNameValuePair("access_token", this.e));
        a(arrayList, arrayList2, "&phone=" + this.f, bVar);
    }

    public void e(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        a(arrayList, "/v1/ring/openAccount", bVar, str, C0054a.GET);
    }

    public void g(com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("serviceType", "0"));
        a(arrayList, "/v1/ring/closeAccount", bVar, C0054a.GET);
    }

    public void b(String str, String str2, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("toneType", "1"));
        arrayList.add(new BasicNameValuePair("toneID", str));
        arrayList.add(new BasicNameValuePair("sP_ProductID", "7000100301"));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        a(arrayList, "/v1/ring/buyTone", bVar, str2, C0054a.GET);
    }

    public void a(String str, String str2, String str3, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("toneType", "1"));
        arrayList.add(new BasicNameValuePair("toneID", str));
        arrayList.add(new BasicNameValuePair("sP_ProductID", "7000100301"));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        arrayList.add(new BasicNameValuePair("productId", "0000001599"));
        arrayList.add(new BasicNameValuePair("contentId", str2));
        a(arrayList, "/v1/ring/buyVipTone", bVar, str3, C0054a.GET);
    }

    public void f(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("toneType", "1"));
        arrayList.add(new BasicNameValuePair("toneID", str));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        a(arrayList, "/v1/ring/delTone", bVar, "&phone=" + this.f, C0054a.GET);
    }

    public void a(String str, boolean z, String str2, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("operType", z ? "2" : "1"));
        StringBuilder sb = new StringBuilder();
        StringBuilder append = sb.append("setting=").append("{").append("\"settingObjType\":\"1\",  ").append("\"timeType\":\"0\",  ").append("\"startTime\":\"0\",  ").append("\"settingID\":\"");
        if (!z) {
            str2 = "0000000000";
        }
        append.append(str2).append("\",  ").append("\"endTime\":\"0\",  ").append("\"toneType\":\"0\",  ").append("\"toneID\":\"").append(str).append("\"").append("}");
        com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "setTone, json:" + sb.toString());
        a(arrayList, "/v1/ringSetting/setTone", sb.toString(), bVar, "&phone=" + this.f, C0054a.POST);
    }

    public void h(com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("showNum", "100"));
        arrayList.add(new BasicNameValuePair("pageNo", "0"));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        a(arrayList, "/v1/ringSetting/qryToneSet", bVar, "&phone=" + this.f, C0054a.GET);
    }

    public c.b c() {
        c.b bVar;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("showNum", "100"));
        arrayList.add(new BasicNameValuePair("pageNo", "0"));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        try {
            String a2 = c.a(arrayList, "/v1/ringSetting/qryToneSet");
            if (a2 != null) {
                bVar = b(a2, "/v1/ringSetting/qryToneSet");
                if (bVar == null) {
                    bVar = h;
                }
            } else {
                bVar = h;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            bVar = i;
        }
        a("/v1/ringSetting/qryToneSet", bVar, "&phone=" + this.f);
        return bVar;
    }

    public void i(com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("showNum", "30"));
        arrayList.add(new BasicNameValuePair("pageNo", "0"));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        a(arrayList, "/v1/ring/qryUserTone", bVar, "&phone=" + this.f, C0054a.GET);
    }

    public c.b d() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("access_token", this.e));
        arrayList.add(new BasicNameValuePair("showNum", "30"));
        arrayList.add(new BasicNameValuePair("pageNo", "0"));
        arrayList.add(new BasicNameValuePair("oprType", "00"));
        try {
            String a2 = c.a(arrayList, "/v1/ring/qryUserTone");
            if (a2 == null) {
                return null;
            }
            c.b b2 = b(a2, "/v1/ring/qryUserTone");
            if (b2 != null) {
                return b2;
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void g(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("ringId", str));
        a(arrayList, "/v1/ring/qryRingById", bVar, C0054a.GET);
    }

    public c.q d(String str) {
        c.b b2;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("ringId", str));
        try {
            String a2 = c.a(arrayList, "/v1/ring/qryRingById");
            if (!(a2 == null || (b2 = b(a2, "/v1/ring/qryRingById")) == null || !(b2 instanceof c.q))) {
                return (c.q) b2;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public void a(boolean z, String str, String str2, String str3, String str4, String str5, String str6, String str7, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("toneType", "2"));
        arrayList.add(new BasicNameValuePair("operType", z ? "4" : "1"));
        arrayList.add(new BasicNameValuePair("ringID", str2));
        arrayList.add(new BasicNameValuePair("accountID", str3));
        arrayList.add(new BasicNameValuePair("fileName", str5));
        arrayList.add(new BasicNameValuePair("ftpPath", str4));
        String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "cu_ftp_ip");
        if (TextUtils.isEmpty(a2)) {
            a2 = "10.123.254.218";
        }
        arrayList.add(new BasicNameValuePair("ftpIP", a2));
        arrayList.add(new BasicNameValuePair("ftpUser", "ringdiy"));
        arrayList.add(new BasicNameValuePair("ftpPwd", "cudiy1qaz"));
        arrayList.add(new BasicNameValuePair("valiDate", "2018.10.23"));
        arrayList.add(new BasicNameValuePair("tonePrice", "0"));
        String a3 = com.umeng.b.a.a().a(RingDDApp.c(), "cu_SP_ProductID");
        if (TextUtils.isEmpty(a3)) {
            a3 = "7000102100";
        }
        arrayList.add(new BasicNameValuePair("SP_ProductID", a3));
        String a4 = com.umeng.b.a.a().a(RingDDApp.c(), "cu_sp_id");
        if (TextUtils.isEmpty(a4)) {
            a4 = "90115000";
        }
        arrayList.add(new BasicNameValuePair("spID", a4));
        String a5 = com.umeng.b.a.a().a(RingDDApp.c(), "cu_cp_id");
        if (TextUtils.isEmpty(a5)) {
            a5 = "96580000";
        }
        arrayList.add(new BasicNameValuePair("cpID", a5));
        arrayList.add(new BasicNameValuePair("toneName", str));
        arrayList.add(new BasicNameValuePair("singerName", str6));
        a(arrayList, "/v1/ring/uploadRing", bVar, str7, C0054a.GET);
    }

    public void a(String str, String str2, String str3, String str4, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("toneType", "2"));
        arrayList.add(new BasicNameValuePair("operType", "2"));
        arrayList.add(new BasicNameValuePair("ringID", str2));
        arrayList.add(new BasicNameValuePair("accountID", str3));
        arrayList.add(new BasicNameValuePair("fileName", ""));
        arrayList.add(new BasicNameValuePair("ftpPath", ""));
        arrayList.add(new BasicNameValuePair("ftpIP", ""));
        arrayList.add(new BasicNameValuePair("ftpUser", ""));
        arrayList.add(new BasicNameValuePair("ftpPwd", ""));
        arrayList.add(new BasicNameValuePair("valiDate", "2018.10.23"));
        arrayList.add(new BasicNameValuePair("tonePrice", "0"));
        String a2 = com.umeng.b.a.a().a(RingDDApp.c(), "cu_SP_ProductID");
        if (TextUtils.isEmpty(a2)) {
            a2 = "7000102100";
        }
        arrayList.add(new BasicNameValuePair("SP_ProductID", a2));
        String a3 = com.umeng.b.a.a().a(RingDDApp.c(), "cu_sp_id");
        if (TextUtils.isEmpty(a3)) {
            a3 = "90115000";
        }
        arrayList.add(new BasicNameValuePair("spID", a3));
        String a4 = com.umeng.b.a.a().a(RingDDApp.c(), "cu_cp_id");
        if (TextUtils.isEmpty(a4)) {
            a4 = "96580000";
        }
        arrayList.add(new BasicNameValuePair("cpID", a4));
        arrayList.add(new BasicNameValuePair("toneName", str));
        arrayList.add(new BasicNameValuePair("singerName", ""));
        a(arrayList, "/v1/ring/uploadRing", bVar, str4, C0054a.GET);
    }

    public void h(String str, com.shoujiduoduo.util.b.b bVar) {
        com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "qryUserLocation");
        String a2 = af.a(RingDDApp.c(), str + "_provinceName", "");
        String a3 = af.a(RingDDApp.c(), str + "_provinceId", "");
        if (!TextUtils.isEmpty(a3)) {
            c.ah ahVar = new c.ah();
            ahVar.b = "000000";
            ahVar.c = "成功";
            ahVar.f2228a = a3;
            ahVar.d = a2;
            com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "从缓存获取归属地， provinceId:" + ahVar.f2228a);
            bVar.g(ahVar);
            return;
        }
        com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "从联通查询归属地");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("callNumber", str));
        a(arrayList, "/v1/unicomAccount/qryUserLocation", bVar, "&phone=" + this.f, C0054a.GET);
    }

    public void i(String str, com.shoujiduoduo.util.b.b bVar) {
        String a2 = af.a(RingDDApp.c(), str + "_netType", "");
        if (!TextUtils.isEmpty(a2)) {
            c.ai aiVar = new c.ai();
            aiVar.b = "000000";
            aiVar.c = "成功";
            aiVar.f2229a = a2;
            com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "从缓存获取网络类型， netType:" + a2);
            bVar.g(aiVar);
            return;
        }
        com.shoujiduoduo.base.a.a.a("ChinaUnicomUtils", "去联通查询网络类型");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("callNumber", str));
        a(arrayList, "/v1/unicomAccount/queryUserInfo", bVar, "&phone=" + this.f, C0054a.GET);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:115:0x01d2 */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v15, types: [int] */
    /* JADX WARN: Type inference failed for: r1v17, types: [int] */
    /* JADX WARN: Type inference failed for: r1v20, types: [boolean] */
    /* JADX WARN: Type inference failed for: r1v21 */
    /* JADX WARN: Type inference failed for: r1v22 */
    /* JADX WARN: Type inference failed for: r1v23 */
    /* JADX WARN: Type inference failed for: r1v29 */
    /* JADX WARN: Type inference failed for: r1v30 */
    /* JADX WARN: Type inference failed for: r1v31 */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.shoujiduoduo.util.b.c.b b(java.lang.String r7, java.lang.String r8) {
        /*
            r6 = this;
            r2 = 0
            r1 = 0
            java.lang.String r0 = "ChinaUnicomUtils"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "content:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.String r3 = r3.toString()
            com.shoujiduoduo.base.a.a.a(r0, r3)
            if (r7 == 0) goto L_0x0024
            java.lang.String r0 = ""
            boolean r0 = r7.equals(r0)
            if (r0 == 0) goto L_0x0026
        L_0x0024:
            r0 = r2
        L_0x0025:
            return r0
        L_0x0026:
            org.json.JSONTokener r0 = new org.json.JSONTokener
            r0.<init>(r7)
            java.lang.Object r0 = r0.nextValue()     // Catch:{ JSONException -> 0x0058 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ JSONException -> 0x0058 }
            java.lang.String r3 = "/v1/uerNetInfo/genUnikey"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x005e
            com.shoujiduoduo.util.b.c$j r1 = new com.shoujiduoduo.util.b.c$j
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "unikey"
            java.lang.String r0 = r0.optString(r2)
            r1.f2239a = r0
            r0 = r1
            goto L_0x0025
        L_0x0058:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x0025
        L_0x005e:
            java.lang.String r3 = "/v1/uerNetInfo/qryUserMobile"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x009d
            com.shoujiduoduo.util.b.c$t r1 = new com.shoujiduoduo.util.b.c$t
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "imsi"
            java.lang.String r2 = r0.optString(r2)
            r1.d = r2
            java.lang.String r2 = "mobile"
            java.lang.String r2 = r0.optString(r2)
            r1.f2250a = r2
            java.lang.String r2 = "rateType"
            java.lang.String r2 = r0.optString(r2)
            r1.f = r2
            java.lang.String r2 = "APN"
            java.lang.String r0 = r0.optString(r2)
            r1.e = r0
            r0 = r1
            goto L_0x0025
        L_0x009d:
            java.lang.String r3 = "/v1/verifyCode/sendLoginCode"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x00bd
            com.shoujiduoduo.util.b.c$b r1 = new com.shoujiduoduo.util.b.c$b
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r0 = r0.optString(r2)
            r1.c = r0
            r0 = r1
            goto L_0x0025
        L_0x00bd:
            java.lang.String r3 = "/v1/token/codeAuthToken"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x00fc
            com.shoujiduoduo.util.b.c$a r1 = new com.shoujiduoduo.util.b.c$a
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "token"
            org.json.JSONObject r0 = r0.optJSONObject(r2)
            if (r0 == 0) goto L_0x00f9
            java.lang.String r2 = "access_token"
            java.lang.String r0 = r0.optString(r2)
            r1.f2220a = r0
            java.lang.String r0 = r1.f2220a
            r6.e = r0
            android.content.Context r0 = com.shoujiduoduo.ringtone.RingDDApp.c()
            java.lang.String r2 = "accesstoken"
            java.lang.String r3 = r6.e
            com.shoujiduoduo.util.af.c(r0, r2, r3)
        L_0x00f9:
            r0 = r1
            goto L_0x0025
        L_0x00fc:
            java.lang.String r3 = "/v1/ring/qryUserBasInfo"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x012c
            com.shoujiduoduo.util.b.c$af r1 = new com.shoujiduoduo.util.b.c$af
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "userInfo"
            org.json.JSONObject r0 = r0.optJSONObject(r2)
            if (r0 == 0) goto L_0x0129
            java.lang.String r2 = "userStatus"
            java.lang.String r0 = r0.optString(r2)
            r1.f2226a = r0
        L_0x0129:
            r0 = r1
            goto L_0x0025
        L_0x012c:
            java.lang.String r3 = "/v1/ring/openAccount"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0174
            java.lang.String r3 = "/v1/ring/closeAccount"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0174
            java.lang.String r3 = "/v1/ring/buyTone"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0174
            java.lang.String r3 = "/v1/ring/delTone"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0174
            java.lang.String r3 = "/v1/ring/buyVipTone"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0174
            java.lang.String r3 = "/v1/product/unSubProduct"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0174
            java.lang.String r3 = "/v1/ring/uploadRing"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0174
            java.lang.String r3 = "/v1/ringSetting/setTone"
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0174
            java.lang.String r3 = "/v1/product/subscribeCurrentFee"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x018c
        L_0x0174:
            com.shoujiduoduo.util.b.c$b r1 = new com.shoujiduoduo.util.b.c$b
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r0 = r0.optString(r2)
            r1.c = r0
            r0 = r1
            goto L_0x0025
        L_0x018c:
            java.lang.String r3 = "/v1/product/qrySubedProducts"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x01d7
            com.shoujiduoduo.util.b.c$r r2 = new com.shoujiduoduo.util.b.c$r
            r2.<init>()
            java.lang.String r3 = "returnCode"
            java.lang.String r3 = r0.optString(r3)
            r2.b = r3
            java.lang.String r3 = "description"
            java.lang.String r3 = r0.optString(r3)
            r2.c = r3
            java.lang.String r3 = "subedProducts"
            org.json.JSONArray r3 = r0.optJSONArray(r3)
            if (r3 == 0) goto L_0x01d2
            r0 = r1
        L_0x01b2:
            int r4 = r3.length()
            if (r0 >= r4) goto L_0x01d2
            org.json.JSONObject r4 = r3.optJSONObject(r0)
            if (r4 == 0) goto L_0x01cf
            java.lang.String r5 = "productId"
            java.lang.String r4 = r4.optString(r5)
            if (r4 == 0) goto L_0x01cf
            java.lang.String r5 = "0000001599"
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x01cf
            r1 = 1
        L_0x01cf:
            int r0 = r0 + 1
            goto L_0x01b2
        L_0x01d2:
            r2.f2248a = r1
            r0 = r2
            goto L_0x0025
        L_0x01d7:
            java.lang.String r3 = "/v1/product/subProduct"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x021a
            com.shoujiduoduo.util.b.c$ad r1 = new com.shoujiduoduo.util.b.c$ad
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "product"
            org.json.JSONObject r0 = r0.optJSONObject(r2)
            if (r0 == 0) goto L_0x0217
            com.shoujiduoduo.util.b.c$p r2 = new com.shoujiduoduo.util.b.c$p
            r2.<init>()
            r1.f2224a = r2
            com.shoujiduoduo.util.b.c$p r2 = r1.f2224a
            java.lang.String r3 = "productId"
            java.lang.String r3 = r0.optString(r3)
            r2.b = r3
            com.shoujiduoduo.util.b.c$p r2 = r1.f2224a
            java.lang.String r3 = "productName"
            java.lang.String r0 = r0.optString(r3)
            r2.f2246a = r0
        L_0x0217:
            r0 = r1
            goto L_0x0025
        L_0x021a:
            java.lang.String r3 = "/v1/ringSetting/qryToneSet"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x02a2
            com.shoujiduoduo.util.b.c$s r2 = new com.shoujiduoduo.util.b.c$s
            r2.<init>()
            java.lang.String r3 = "returnCode"
            java.lang.String r3 = r0.optString(r3)
            r2.b = r3
            java.lang.String r3 = "description"
            java.lang.String r3 = r0.optString(r3)
            r2.c = r3
            java.lang.String r3 = "totalCount"
            int r3 = r0.optInt(r3)
            r2.f2249a = r3
            java.lang.String r3 = "userToneSettingInfos"
            org.json.JSONArray r0 = r0.optJSONArray(r3)
            if (r0 == 0) goto L_0x029f
            int r3 = r0.length()
            if (r3 <= 0) goto L_0x029f
            int r3 = r0.length()
            com.shoujiduoduo.util.b.c$ak[] r3 = new com.shoujiduoduo.util.b.c.ak[r3]
            r2.d = r3
        L_0x0255:
            int r3 = r0.length()
            if (r1 >= r3) goto L_0x029f
            org.json.JSONObject r3 = r0.optJSONObject(r1)
            if (r3 == 0) goto L_0x0295
            com.shoujiduoduo.util.b.c$ak r4 = new com.shoujiduoduo.util.b.c$ak
            r4.<init>()
            java.lang.String r5 = "settingID"
            java.lang.String r5 = r3.optString(r5)
            r4.f2231a = r5
            java.lang.String r5 = "settingObjType"
            java.lang.String r5 = r3.optString(r5)
            r4.b = r5
            java.lang.String r5 = "toneID"
            java.lang.String r5 = r3.optString(r5)
            r4.d = r5
            java.lang.String r5 = "toneType"
            java.lang.String r5 = r3.optString(r5)
            r4.c = r5
            java.lang.String r5 = "timeType"
            java.lang.String r3 = r3.optString(r5)
            r4.e = r3
            com.shoujiduoduo.util.b.c$ak[] r3 = r2.d
            r3[r1] = r4
        L_0x0292:
            int r1 = r1 + 1
            goto L_0x0255
        L_0x0295:
            com.shoujiduoduo.util.b.c$ak[] r3 = r2.d
            com.shoujiduoduo.util.b.c$ak r4 = new com.shoujiduoduo.util.b.c$ak
            r4.<init>()
            r3[r1] = r4
            goto L_0x0292
        L_0x029f:
            r0 = r2
            goto L_0x0025
        L_0x02a2:
            java.lang.String r3 = "/v1/ring/qryUserTone"
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x033a
            com.shoujiduoduo.util.b.c$u r2 = new com.shoujiduoduo.util.b.c$u
            r2.<init>()
            java.lang.String r3 = "returnCode"
            java.lang.String r3 = r0.optString(r3)
            r2.b = r3
            java.lang.String r3 = "description"
            java.lang.String r3 = r0.optString(r3)
            r2.c = r3
            java.lang.String r3 = "totalCount"
            int r3 = r0.optInt(r3)
            r2.f2251a = r3
            java.lang.String r3 = "ringConciseInfos"
            org.json.JSONArray r0 = r0.optJSONArray(r3)
            if (r0 == 0) goto L_0x0337
            int r3 = r0.length()
            if (r3 <= 0) goto L_0x0337
            int r3 = r0.length()
            com.shoujiduoduo.util.b.c$ab[] r3 = new com.shoujiduoduo.util.b.c.ab[r3]
            r2.d = r3
        L_0x02dd:
            int r3 = r0.length()
            if (r1 >= r3) goto L_0x0337
            org.json.JSONObject r3 = r0.optJSONObject(r1)
            if (r3 == 0) goto L_0x032d
            com.shoujiduoduo.util.b.c$ab r4 = new com.shoujiduoduo.util.b.c$ab
            r4.<init>()
            java.lang.String r5 = "ringID"
            java.lang.String r5 = r3.optString(r5)
            r4.f2222a = r5
            java.lang.String r5 = "ringName"
            java.lang.String r5 = r3.optString(r5)
            r4.b = r5
            java.lang.String r5 = "ringPrice"
            java.lang.String r5 = r3.optString(r5)
            r4.c = r5
            java.lang.String r5 = "ringProvider"
            java.lang.String r5 = r3.optString(r5)
            r4.e = r5
            java.lang.String r5 = "ringSinger"
            java.lang.String r5 = r3.optString(r5)
            r4.d = r5
            java.lang.String r5 = "subcribeDate"
            java.lang.String r5 = r3.optString(r5)
            r4.f = r5
            java.lang.String r5 = "validDate"
            java.lang.String r3 = r3.optString(r5)
            r4.g = r3
            com.shoujiduoduo.util.b.c$ab[] r3 = r2.d
            r3[r1] = r4
        L_0x032a:
            int r1 = r1 + 1
            goto L_0x02dd
        L_0x032d:
            com.shoujiduoduo.util.b.c$ab[] r3 = r2.d
            com.shoujiduoduo.util.b.c$ab r4 = new com.shoujiduoduo.util.b.c$ab
            r4.<init>()
            r3[r1] = r4
            goto L_0x032a
        L_0x0337:
            r0 = r2
            goto L_0x0025
        L_0x033a:
            java.lang.String r1 = "/v1/ring/qryRingById"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x03b9
            com.shoujiduoduo.util.b.c$q r1 = new com.shoujiduoduo.util.b.c$q
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            com.shoujiduoduo.util.b.c$y r2 = new com.shoujiduoduo.util.b.c$y
            r2.<init>()
            r1.f2247a = r2
            java.lang.String r2 = "ring"
            org.json.JSONObject r0 = r0.optJSONObject(r2)
            if (r0 == 0) goto L_0x03b6
            com.shoujiduoduo.util.b.c$y r2 = r1.f2247a
            java.lang.String r3 = "ringId"
            java.lang.String r3 = r0.optString(r3)
            r2.f2255a = r3
            com.shoujiduoduo.util.b.c$y r2 = r1.f2247a
            java.lang.String r3 = "ringName"
            java.lang.String r3 = r0.optString(r3)
            r2.b = r3
            com.shoujiduoduo.util.b.c$y r2 = r1.f2247a
            java.lang.String r3 = "ringPrice"
            java.lang.String r3 = r0.optString(r3)
            r2.c = r3
            com.shoujiduoduo.util.b.c$y r2 = r1.f2247a
            java.lang.String r3 = "ringValidDate"
            java.lang.String r3 = r0.optString(r3)
            r2.d = r3
            com.shoujiduoduo.util.b.c$y r2 = r1.f2247a
            java.lang.String r3 = "ringValidDays"
            java.lang.String r3 = r0.optString(r3)
            r2.e = r3
            com.shoujiduoduo.util.b.c$y r2 = r1.f2247a
            java.lang.String r3 = "singerName"
            java.lang.String r3 = r0.optString(r3)
            r2.f = r3
            com.shoujiduoduo.util.b.c$y r2 = r1.f2247a
            java.lang.String r3 = "songId"
            java.lang.String r3 = r0.optString(r3)
            r2.g = r3
            com.shoujiduoduo.util.b.c$y r2 = r1.f2247a
            java.lang.String r3 = "url"
            java.lang.String r0 = r0.optString(r3)
            r2.h = r0
        L_0x03b6:
            r0 = r1
            goto L_0x0025
        L_0x03b9:
            java.lang.String r1 = "/v1/unicomAccount/queryUserInfo"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x03e1
            com.shoujiduoduo.util.b.c$ai r1 = new com.shoujiduoduo.util.b.c$ai
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "netType"
            java.lang.String r0 = r0.optString(r2)
            r1.f2229a = r0
            r0 = r1
            goto L_0x0025
        L_0x03e1:
            java.lang.String r1 = "/v1/unicomAccount/qryUserLocation"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x0411
            com.shoujiduoduo.util.b.c$ah r1 = new com.shoujiduoduo.util.b.c$ah
            r1.<init>()
            java.lang.String r2 = "returnCode"
            java.lang.String r2 = r0.optString(r2)
            r1.b = r2
            java.lang.String r2 = "description"
            java.lang.String r2 = r0.optString(r2)
            r1.c = r2
            java.lang.String r2 = "provinceId"
            java.lang.String r2 = r0.optString(r2)
            r1.f2228a = r2
            java.lang.String r2 = "provinceName"
            java.lang.String r0 = r0.optString(r2)
            r1.d = r0
            r0 = r1
            goto L_0x0025
        L_0x0411:
            java.lang.String r0 = "ChinaUnicomUtils"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "not support method : "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            com.shoujiduoduo.base.a.a.a(r0, r1)
            r0 = r2
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.e.a.b(java.lang.String, java.lang.String):com.shoujiduoduo.util.b.c$b");
    }

    private void a(List<NameValuePair> list, String str, String str2, com.shoujiduoduo.util.b.b bVar, String str3, C0054a aVar) {
        final C0054a aVar2 = aVar;
        final List<NameValuePair> list2 = list;
        final String str4 = str2;
        final String str5 = str;
        final String str6 = str3;
        final com.shoujiduoduo.util.b.b bVar2 = bVar;
        i.a(new Runnable() {
            public void run() {
                c.b f2;
                String str = null;
                try {
                    if (aVar2.equals(C0054a.POST)) {
                        str = c.a(list2, str4, str5);
                    } else if (aVar2.equals(C0054a.GET)) {
                        str = c.a(list2, str5);
                    }
                    if (str != null) {
                        f2 = a.this.b(str, str5);
                        if (f2 == null) {
                            f2 = a.h;
                        }
                    } else {
                        f2 = a.h;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    f2 = a.i;
                }
                a.this.a(str5, f2, str6);
                bVar2.g(f2);
            }
        });
    }

    /* compiled from: ChinaUnicomUtils */
    private class c {

        /* renamed from: a  reason: collision with root package name */
        public d f2297a;
        public String b;

        private c() {
            this.f2297a = d.unknown;
            this.b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
        }

        public boolean a() {
            return this.b.equals("0") || this.b.equals("2") || this.b.equals("4");
        }
    }

    private void a(List<NameValuePair> list, List<NameValuePair> list2, String str, com.shoujiduoduo.util.b.b bVar) {
        final String phoneNum = com.shoujiduoduo.a.b.b.g().c().getPhoneNum();
        final List<NameValuePair> list3 = list;
        final String str2 = str;
        final List<NameValuePair> list4 = list2;
        final com.shoujiduoduo.util.b.b bVar2 = bVar;
        i.a(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:15:0x00b7 A[Catch:{ Exception -> 0x0198 }] */
            /* JADX WARNING: Removed duplicated region for block: B:18:0x0124 A[Catch:{ Exception -> 0x0198 }] */
            /* JADX WARNING: Removed duplicated region for block: B:34:0x01c1 A[Catch:{ Exception -> 0x0198 }] */
            /* JADX WARNING: Removed duplicated region for block: B:39:0x01ff A[Catch:{ Exception -> 0x0198 }] */
            /* JADX WARNING: Removed duplicated region for block: B:40:0x0206 A[Catch:{ Exception -> 0x0198 }] */
            /* JADX WARNING: Removed duplicated region for block: B:48:0x0264 A[Catch:{ Exception -> 0x0198 }] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r6 = this;
                    com.shoujiduoduo.util.b.c$f r1 = new com.shoujiduoduo.util.b.c$f
                    r1.<init>()
                    java.lang.String r0 = "000000"
                    r1.b = r0
                    java.lang.String r0 = "成功"
                    r1.c = r0
                    com.shoujiduoduo.util.e.a r0 = com.shoujiduoduo.util.e.a.this
                    java.util.HashMap r0 = r0.j
                    java.lang.String r2 = r2
                    java.lang.Object r0 = r0.get(r2)
                    com.shoujiduoduo.util.e.a$c r0 = (com.shoujiduoduo.util.e.a.c) r0
                    if (r0 == 0) goto L_0x0157
                    boolean r2 = r0.a()     // Catch:{ Exception -> 0x0198 }
                    if (r2 == 0) goto L_0x0157
                    com.shoujiduoduo.util.b.c$af r2 = new com.shoujiduoduo.util.b.c$af     // Catch:{ Exception -> 0x0198 }
                    r2.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "000000"
                    r2.b = r3     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "成功"
                    r2.c = r3     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = r0.b     // Catch:{ Exception -> 0x0198 }
                    r2.f2226a = r3     // Catch:{ Exception -> 0x0198 }
                    r1.f2235a = r2     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = "ChinaUnicomUtils"
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
                    r3.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = "从缓存获取彩铃基础业务状态， userStatus:"
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = r0.b     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.base.a.a.a(r2, r3)     // Catch:{ Exception -> 0x0198 }
                L_0x0050:
                    if (r0 == 0) goto L_0x01b7
                    com.shoujiduoduo.util.e.a$d r2 = r0.f2297a     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.util.e.a$d r3 = com.shoujiduoduo.util.e.a.d.unknown     // Catch:{ Exception -> 0x0198 }
                    boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x0198 }
                    if (r2 != 0) goto L_0x01b7
                    com.shoujiduoduo.util.b.c$r r2 = new com.shoujiduoduo.util.b.c$r     // Catch:{ Exception -> 0x0198 }
                    r2.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "000000"
                    r2.b = r3     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "成功"
                    r2.c = r3     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.util.e.a$d r0 = r0.f2297a     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.util.e.a$d r3 = com.shoujiduoduo.util.e.a.d.open     // Catch:{ Exception -> 0x0198 }
                    boolean r0 = r0.equals(r3)     // Catch:{ Exception -> 0x0198 }
                    if (r0 == 0) goto L_0x01b4
                    r0 = 1
                L_0x0074:
                    r2.f2248a = r0     // Catch:{ Exception -> 0x0198 }
                    r1.d = r2     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r0 = "ChinaUnicomUtils"
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
                    r3.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = "从缓存获取已订购产品状态， duoduovipOpen:"
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
                    boolean r2 = r2.f2248a     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.base.a.a.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
                L_0x0092:
                    android.content.Context r0 = com.shoujiduoduo.ringtone.RingDDApp.c()     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
                    r2.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = r2     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "_netType"
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = ""
                    java.lang.String r0 = com.shoujiduoduo.util.af.a(r0, r2, r3)     // Catch:{ Exception -> 0x0198 }
                    boolean r2 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x0198 }
                    if (r2 != 0) goto L_0x0206
                    com.shoujiduoduo.util.b.c$ai r2 = new com.shoujiduoduo.util.b.c$ai     // Catch:{ Exception -> 0x0198 }
                    r2.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "000000"
                    r2.b = r3     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "成功"
                    r2.c = r3     // Catch:{ Exception -> 0x0198 }
                    r2.f2229a = r0     // Catch:{ Exception -> 0x0198 }
                    r1.e = r2     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = "ChinaUnicomUtils"
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
                    r3.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = "从缓存获取网络类型， netType:"
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.base.a.a.a(r2, r0)     // Catch:{ Exception -> 0x0198 }
                L_0x00e0:
                    android.content.Context r0 = com.shoujiduoduo.ringtone.RingDDApp.c()     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
                    r2.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = r2     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "_provinceName"
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = ""
                    java.lang.String r0 = com.shoujiduoduo.util.af.a(r0, r2, r3)     // Catch:{ Exception -> 0x0198 }
                    android.content.Context r2 = com.shoujiduoduo.ringtone.RingDDApp.c()     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
                    r3.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = r2     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = "_provinceId"
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = ""
                    java.lang.String r2 = com.shoujiduoduo.util.af.a(r2, r3, r4)     // Catch:{ Exception -> 0x0198 }
                    boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x0198 }
                    if (r3 != 0) goto L_0x0264
                    com.shoujiduoduo.util.b.c$ah r3 = new com.shoujiduoduo.util.b.c$ah     // Catch:{ Exception -> 0x0198 }
                    r3.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = "000000"
                    r3.b = r4     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = "成功"
                    r3.c = r4     // Catch:{ Exception -> 0x0198 }
                    r3.f2228a = r2     // Catch:{ Exception -> 0x0198 }
                    r3.d = r0     // Catch:{ Exception -> 0x0198 }
                    r1.f = r3     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r0 = "ChinaUnicomUtils"
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
                    r2.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = "从缓存获取归属地， provinceId:"
                    java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = r3.f2228a     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.base.a.a.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
                L_0x0151:
                    com.shoujiduoduo.util.b.b r0 = r6     // Catch:{ Exception -> 0x0198 }
                    r0.g(r1)     // Catch:{ Exception -> 0x0198 }
                L_0x0156:
                    return
                L_0x0157:
                    java.util.List r2 = r3     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "/v1/ring/qryUserBasInfo"
                    java.lang.String r2 = com.shoujiduoduo.util.e.c.a(r2, r3)     // Catch:{ Exception -> 0x0198 }
                    if (r2 == 0) goto L_0x01ad
                    com.shoujiduoduo.util.e.a r3 = com.shoujiduoduo.util.e.a.this     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = "/v1/ring/qryUserBasInfo"
                    com.shoujiduoduo.util.b.c$b r2 = r3.b(r2, r4)     // Catch:{ Exception -> 0x0198 }
                    if (r2 == 0) goto L_0x01a6
                    r1.f2235a = r2     // Catch:{ Exception -> 0x0198 }
                L_0x016d:
                    java.lang.String r2 = "ChinaUnicomUtils"
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
                    r3.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = "从联通获取彩铃基础业务状态， res:"
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.util.b.c$b r4 = r1.f2235a     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.base.a.a.a(r2, r3)     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.util.e.a r2 = com.shoujiduoduo.util.e.a.this     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "/v1/ring/qryUserBasInfo"
                    com.shoujiduoduo.util.b.c$b r4 = r1.f2235a     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r5 = r4     // Catch:{ Exception -> 0x0198 }
                    r2.a(r3, r4, r5)     // Catch:{ Exception -> 0x0198 }
                    goto L_0x0050
                L_0x0198:
                    r0 = move-exception
                    r0.printStackTrace()
                    com.shoujiduoduo.util.b.b r0 = r6
                    com.shoujiduoduo.util.b.c$b r1 = com.shoujiduoduo.util.e.a.h
                    r0.g(r1)
                    goto L_0x0156
                L_0x01a6:
                    com.shoujiduoduo.util.b.c$b r2 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
                    r1.f2235a = r2     // Catch:{ Exception -> 0x0198 }
                    goto L_0x016d
                L_0x01ad:
                    com.shoujiduoduo.util.b.c$b r2 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
                    r1.f2235a = r2     // Catch:{ Exception -> 0x0198 }
                    goto L_0x016d
                L_0x01b4:
                    r0 = 0
                    goto L_0x0074
                L_0x01b7:
                    java.util.List r0 = r5     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = "/v1/product/qrySubedProducts"
                    java.lang.String r0 = com.shoujiduoduo.util.e.c.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
                    if (r0 == 0) goto L_0x01ff
                    com.shoujiduoduo.util.e.a r2 = com.shoujiduoduo.util.e.a.this     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "/v1/product/qrySubedProducts"
                    com.shoujiduoduo.util.b.c$b r0 = r2.b(r0, r3)     // Catch:{ Exception -> 0x0198 }
                    if (r0 == 0) goto L_0x01f8
                    r1.d = r0     // Catch:{ Exception -> 0x0198 }
                L_0x01cd:
                    java.lang.String r0 = "ChinaUnicomUtils"
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
                    r2.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "从联通获取已订购产品状态， res:"
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.util.b.c$b r3 = r1.d     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.base.a.a.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.util.e.a r0 = com.shoujiduoduo.util.e.a.this     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = "/v1/product/qrySubedProducts"
                    com.shoujiduoduo.util.b.c$b r3 = r1.d     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = r4     // Catch:{ Exception -> 0x0198 }
                    r0.a(r2, r3, r4)     // Catch:{ Exception -> 0x0198 }
                    goto L_0x0092
                L_0x01f8:
                    com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
                    r1.d = r0     // Catch:{ Exception -> 0x0198 }
                    goto L_0x01cd
                L_0x01ff:
                    com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
                    r1.d = r0     // Catch:{ Exception -> 0x0198 }
                    goto L_0x01cd
                L_0x0206:
                    java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x0198 }
                    r0.<init>()     // Catch:{ Exception -> 0x0198 }
                    org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "callNumber"
                    java.lang.String r4 = r2     // Catch:{ Exception -> 0x0198 }
                    r2.<init>(r3, r4)     // Catch:{ Exception -> 0x0198 }
                    r0.add(r2)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = "/v1/unicomAccount/queryUserInfo"
                    java.lang.String r0 = com.shoujiduoduo.util.e.c.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
                    if (r0 == 0) goto L_0x025d
                    com.shoujiduoduo.util.e.a r2 = com.shoujiduoduo.util.e.a.this     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "/v1/unicomAccount/queryUserInfo"
                    com.shoujiduoduo.util.b.c$b r0 = r2.b(r0, r3)     // Catch:{ Exception -> 0x0198 }
                    if (r0 == 0) goto L_0x0256
                    r1.e = r0     // Catch:{ Exception -> 0x0198 }
                L_0x022b:
                    java.lang.String r0 = "ChinaUnicomUtils"
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
                    r2.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "从联通获取网络类型， res:"
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.util.b.c$b r3 = r1.e     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.base.a.a.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.util.e.a r0 = com.shoujiduoduo.util.e.a.this     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = "/v1/unicomAccount/queryUserInfo"
                    com.shoujiduoduo.util.b.c$b r3 = r1.e     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = r4     // Catch:{ Exception -> 0x0198 }
                    r0.a(r2, r3, r4)     // Catch:{ Exception -> 0x0198 }
                    goto L_0x00e0
                L_0x0256:
                    com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
                    r1.e = r0     // Catch:{ Exception -> 0x0198 }
                    goto L_0x022b
                L_0x025d:
                    com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
                    r1.e = r0     // Catch:{ Exception -> 0x0198 }
                    goto L_0x022b
                L_0x0264:
                    java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x0198 }
                    r0.<init>()     // Catch:{ Exception -> 0x0198 }
                    org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "callNumber"
                    java.lang.String r4 = r2     // Catch:{ Exception -> 0x0198 }
                    r2.<init>(r3, r4)     // Catch:{ Exception -> 0x0198 }
                    r0.add(r2)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = "/v1/unicomAccount/qryUserLocation"
                    java.lang.String r0 = com.shoujiduoduo.util.e.c.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
                    if (r0 == 0) goto L_0x02bb
                    com.shoujiduoduo.util.e.a r2 = com.shoujiduoduo.util.e.a.this     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "/v1/unicomAccount/qryUserLocation"
                    com.shoujiduoduo.util.b.c$b r0 = r2.b(r0, r3)     // Catch:{ Exception -> 0x0198 }
                    if (r0 == 0) goto L_0x02b4
                    r1.f = r0     // Catch:{ Exception -> 0x0198 }
                L_0x0289:
                    java.lang.String r0 = "ChinaUnicomUtils"
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0198 }
                    r2.<init>()     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = "从联通获取归属地， res:"
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.util.b.c$b r3 = r1.f     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0198 }
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.base.a.a.a(r0, r2)     // Catch:{ Exception -> 0x0198 }
                    com.shoujiduoduo.util.e.a r0 = com.shoujiduoduo.util.e.a.this     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r2 = "/v1/unicomAccount/qryUserLocation"
                    com.shoujiduoduo.util.b.c$b r3 = r1.f     // Catch:{ Exception -> 0x0198 }
                    java.lang.String r4 = r4     // Catch:{ Exception -> 0x0198 }
                    r0.a(r2, r3, r4)     // Catch:{ Exception -> 0x0198 }
                    goto L_0x0151
                L_0x02b4:
                    com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
                    r1.f = r0     // Catch:{ Exception -> 0x0198 }
                    goto L_0x0289
                L_0x02bb:
                    com.shoujiduoduo.util.b.c$b r0 = com.shoujiduoduo.util.e.a.h     // Catch:{ Exception -> 0x0198 }
                    r1.f = r0     // Catch:{ Exception -> 0x0198 }
                    goto L_0x0289
                */
                throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.e.a.AnonymousClass2.run():void");
            }
        });
    }

    private void a(List<NameValuePair> list, String str, com.shoujiduoduo.util.b.b bVar, C0054a aVar) {
        a(list, str, bVar, "", aVar);
    }

    private void a(List<NameValuePair> list, String str, com.shoujiduoduo.util.b.b bVar, String str2, C0054a aVar) {
        final C0054a aVar2 = aVar;
        final List<NameValuePair> list2 = list;
        final String str3 = str;
        final String str4 = str2;
        final com.shoujiduoduo.util.b.b bVar2 = bVar;
        i.a(new Runnable() {
            public void run() {
                c.b f2;
                String str = null;
                try {
                    if (aVar2.equals(C0054a.POST)) {
                        str = c.b(list2, str3);
                    } else if (aVar2.equals(C0054a.GET)) {
                        str = c.a(list2, str3);
                    }
                    if (str != null) {
                        f2 = a.this.b(str, str3);
                        if (f2 == null) {
                            f2 = a.h;
                        }
                    } else {
                        f2 = a.h;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    f2 = a.i;
                }
                a.this.a(str3, f2, str4);
                bVar2.g(f2);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(String str, c.b bVar, String str2) {
        if (bVar != null && bVar.a().equals("40303")) {
            HashMap hashMap = new HashMap();
            hashMap.put(PushConstants.MZ_PUSH_MESSAGE_METHOD, str);
            com.umeng.a.b.a(RingDDApp.c(), "cucc_app_reach_limit", hashMap);
        }
        if (bVar != null && bVar.a().equals("40304")) {
            HashMap hashMap2 = new HashMap();
            hashMap2.put(PushConstants.MZ_PUSH_MESSAGE_METHOD, str);
            com.umeng.a.b.a(RingDDApp.c(), "cucc_api_reach_limit", hashMap2);
        }
        String phoneNum = com.shoujiduoduo.a.b.b.g().c().getPhoneNum();
        if (str.equals("/v1/ring/qryUserBasInfo")) {
            if (bVar.c()) {
                if (bVar instanceof c.af) {
                    if (this.j.get(phoneNum) != null) {
                        this.j.get(phoneNum).b = ((c.af) bVar).f2226a;
                    } else {
                        c cVar = new c();
                        cVar.b = ((c.af) bVar).f2226a;
                        this.j.put(phoneNum, cVar);
                    }
                }
                a("cu_qry_user_basinfo", "success", str2);
                return;
            }
            a("cu_qry_user_basinfo", "fail, " + bVar.toString(), str2);
        } else if (str.equals("/v1/ring/buyTone") || str.equals("/v1/ring/buyVipTone")) {
            if (bVar.c()) {
                a("cu_buy_tone", "success", str2);
            } else {
                a("cu_buy_tone", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/v1/product/subProduct")) {
            HashMap hashMap3 = new HashMap();
            if (bVar.c()) {
                hashMap3.put(Parameters.RESOLUTION, "success");
                a("cu_open_vip", "success", str2);
                if (this.j.get(phoneNum) != null) {
                    this.j.get(phoneNum).f2297a = d.open;
                } else {
                    c cVar2 = new c();
                    cVar2.f2297a = d.open;
                    this.j.put(phoneNum, cVar2);
                }
            } else {
                hashMap3.put(Parameters.RESOLUTION, bVar.toString());
                a("cu_open_vip", "fail, " + bVar.toString(), str2);
            }
            com.umeng.a.b.a(RingDDApp.c(), "cucc_open_vip", hashMap3);
        } else if (str.equals("/v1/ring/openAccount")) {
            HashMap hashMap4 = new HashMap();
            if (bVar.c()) {
                hashMap4.put(Parameters.RESOLUTION, "success");
                a("cu_open_cailing", "success", str2);
                if (this.j.get(phoneNum) != null) {
                    this.j.get(phoneNum).b = "0";
                } else {
                    c cVar3 = new c();
                    cVar3.b = "0";
                    this.j.put(phoneNum, cVar3);
                }
            } else {
                hashMap4.put(Parameters.RESOLUTION, bVar.toString());
                a("cu_open_cailing", "fail, " + bVar.toString(), str2);
            }
            com.umeng.a.b.a(RingDDApp.c(), "cucc_open_cailing", hashMap4);
        } else if (str.equals("/v1/ringSetting/setTone")) {
            HashMap hashMap5 = new HashMap();
            if (bVar.c()) {
                hashMap5.put(Parameters.RESOLUTION, "success");
                a("cu_settone", "success", str2);
            } else {
                hashMap5.put(Parameters.RESOLUTION, bVar.toString());
                a("cu_settone", "fail, " + bVar.toString(), str2);
            }
            com.umeng.a.b.a(RingDDApp.c(), "cucc_set_ring", hashMap5);
        } else if (str.equals("/v1/ring/uploadRing")) {
            HashMap hashMap6 = new HashMap();
            if (bVar.c()) {
                hashMap6.put(Parameters.RESOLUTION, "success");
                a("cu_upload_ring", "success", str2);
            } else {
                hashMap6.put(Parameters.RESOLUTION, bVar.toString());
                a("cu_upload_ring", "fail, " + bVar.toString(), str2);
            }
            com.umeng.a.b.a(RingDDApp.c(), "cucc_upload_ring", hashMap6);
        } else if (str.equals("/v1/ringSetting/qryToneSet")) {
            if (bVar.c()) {
                a("cu_qry_toneset", "success", str2);
            } else {
                a("cu_qry_toneset", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/v1/verifyCode/sendLoginCode")) {
            if (bVar.c()) {
                a("cu_send_login_code", "success", str2);
            } else {
                a("cu_send_login_code", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/v1/token/codeAuthToken")) {
            if (bVar.c()) {
                a("cu_get_token", "success", str2);
            } else {
                a("cu_get_token", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/v1/product/qrySubedProducts")) {
            if (bVar.c()) {
                if (bVar instanceof c.r) {
                    if (this.j.get(phoneNum) != null) {
                        this.j.get(phoneNum).f2297a = ((c.r) bVar).f2248a ? d.open : d.close;
                    } else {
                        c cVar4 = new c();
                        cVar4.f2297a = ((c.r) bVar).f2248a ? d.open : d.close;
                        this.j.put(phoneNum, cVar4);
                    }
                }
                a("cu_qry_subed_products", "success", str2);
                return;
            }
            a("cu_qry_subed_products", "fail, " + bVar.toString(), str2);
        } else if (str.equals("/v1/ring/qryUserTone")) {
            if (bVar.c()) {
                a("cu_qry_user_tone", "success", str2);
            } else {
                a("cu_qry_user_tone", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/v1/unicomAccount/queryUserInfo")) {
            if (bVar.c()) {
                a("cu_qry_user_info", "success", str2);
                if (bVar instanceof c.ai) {
                    af.c(RingDDApp.c(), phoneNum + "_netType", ((c.ai) bVar).f2229a);
                    return;
                }
                return;
            }
            a("cu_qry_user_info", "fail, " + bVar.toString(), str2);
        } else if (str.equals("/v1/unicomAccount/qryUserLocation")) {
            com.shoujiduoduo.base.a.a.e("ChinaUnicomUtils", "log qryUserLocation");
            if (bVar.c()) {
                a("cu_qry_user_location", "success", str2);
                if (bVar instanceof c.ah) {
                    af.c(RingDDApp.c(), phoneNum + "_provinceId", ((c.ah) bVar).f2228a);
                    af.c(RingDDApp.c(), phoneNum + "_provinceName", ((c.ah) bVar).d);
                    return;
                }
                return;
            }
            a("cu_qry_user_location", "fail, " + bVar.toString(), str2);
        } else {
            com.shoujiduoduo.base.a.a.b("ChinaUnicomUtils", "do not care method:" + str);
        }
    }

    private void a(String str, String str2, String str3) {
        u.a("cu:" + str, str2, str3);
    }
}
