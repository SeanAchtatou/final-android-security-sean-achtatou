package com.shoujiduoduo.util;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import com.shoujiduoduo.base.a.a;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/* compiled from: StartAdConfig */
public class ah {

    /* renamed from: a  reason: collision with root package name */
    private String f2192a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private int g;
    private String h;
    private boolean i;

    public ah(String str) {
        this.h = str;
        if (!TextUtils.isEmpty(str)) {
            this.i = h();
        }
    }

    public boolean a() {
        return this.i;
    }

    public String b() {
        return this.e;
    }

    public String c() {
        if (TextUtils.isEmpty(this.d)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (this.d.contains("?")) {
            sb.append(this.d).append("&mc=").append(g.c()).append("&device_id=").append(g.g());
        } else {
            sb.append(this.d).append("?mc=").append(g.c()).append("&device_id=").append(g.g());
        }
        return sb.toString();
    }

    public String d() {
        return this.f2192a;
    }

    public String e() {
        return this.b;
    }

    public String f() {
        return this.c;
    }

    public int g() {
        int i2 = 5;
        if (this.g <= 5) {
            i2 = this.g;
        }
        if (i2 < 1) {
            return 1;
        }
        return i2;
    }

    @SuppressLint({"SimpleDateFormat"})
    private synchronized boolean h() {
        String str;
        boolean z = false;
        synchronized (this) {
            a.a("StartAdConfig", "***************begin read adconfig");
            try {
                Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(this.h))).getDocumentElement();
                a.a("StartAdConfig", "parse StartAd config");
                NodeList elementsByTagName = documentElement.getElementsByTagName("startad");
                if (elementsByTagName != null && elementsByTagName.getLength() > 0) {
                    NamedNodeMap attributes = elementsByTagName.item(0).getAttributes();
                    String a2 = g.a(attributes, "start_ver", "0.0.0.0");
                    if (TextUtils.isEmpty(a2)) {
                        str = "0.0.0.0";
                    } else {
                        str = a2;
                    }
                    String a3 = g.a(attributes, "end_ver", "9.9.9.9");
                    if (TextUtils.isEmpty(a3)) {
                        a3 = "9.9.9.9";
                    }
                    String q = g.q();
                    a.a("StartAdConfig", "start_ver:" + str + " end_ver:" + a3 + " cur_ver:" + q);
                    if (q.compareTo(str) < 0 || q.compareTo(a3) > 0) {
                        a.a("StartAdConfig", "return, 当前版本不在投放版本内");
                    } else {
                        String a4 = g.a(attributes, "start_time");
                        String a5 = g.a(attributes, "end_time");
                        String format = new SimpleDateFormat("yyyyMMddHH").format(new Date());
                        a.a("StartAdConfig", "start_time:" + a4 + " end_time:" + a5 + " cur_time:" + format);
                        if (format.compareTo(a4) < 0 || format.compareTo(a5) > 0) {
                            a.a("StartAdConfig", "return, 当前时间不在广告投放期");
                        } else {
                            String a6 = g.a(attributes, "disable_src");
                            String k = g.k();
                            a.a("StartAdConfig", "disable_src:" + a6 + ", cursrc:" + k);
                            if (a6.contains(k)) {
                                a.a("StartAdConfig", "return, cur src:" + k + " 当前渠道禁止显示广告");
                            } else {
                                String a7 = g.a(attributes, "min_time");
                                a.a("StartAdConfig", "ad duration:" + a7);
                                this.g = s.a(a7, 2);
                                this.d = g.a(attributes, "adurl");
                                this.f2192a = g.a(attributes, "apkurl");
                                this.b = g.a(attributes, "packagename");
                                this.c = g.a(attributes, "appname");
                                this.e = g.a(attributes, "url_big");
                                this.f = g.a(attributes, "adtype");
                                this.i = true;
                                a.a("StartAdConfig", "*********load startad success, 应该显示广告**********");
                                z = true;
                            }
                        }
                    }
                }
            } catch (SAXException e2) {
                e2.printStackTrace();
            } catch (IOException e3) {
                e3.printStackTrace();
            } catch (ParserConfigurationException e4) {
                e4.printStackTrace();
            } catch (Exception e5) {
                e5.printStackTrace();
            }
            a.a("StartAdConfig", "***************end read adconfig 不显示广告");
            this.i = false;
        }
        return z;
    }
}
