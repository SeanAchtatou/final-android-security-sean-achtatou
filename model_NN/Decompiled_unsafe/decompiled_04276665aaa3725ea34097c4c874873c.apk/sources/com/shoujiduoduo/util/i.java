package com.shoujiduoduo.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* compiled from: DDThreadPool */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private static ExecutorService f2311a = Executors.newCachedThreadPool();

    public static void a(Runnable runnable) {
        f2311a.execute(runnable);
    }

    public static void a() {
        if (f2311a != null) {
            f2311a.shutdown();
        }
    }
}
