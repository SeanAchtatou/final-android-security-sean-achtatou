package com.shoujiduoduo.util.widget;

import android.widget.Toast;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.ringtone.RingDDApp;

/* compiled from: KwToast */
public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static Toast f2365a = null;

    public static void a(final String str, final int i) {
        c.a().a(new c.b() {
            public void a() {
                d.c(str, i);
            }
        });
    }

    public static void a(final String str) {
        c.a().a(new c.b() {
            public void a() {
                d.c(str, 0);
            }
        });
    }

    /* access modifiers changed from: private */
    public static void c(String str, int i) {
        if (f2365a != null) {
            f2365a.setText(str);
            f2365a.setDuration(i);
            f2365a.show();
            return;
        }
        f2365a = Toast.makeText(RingDDApp.b().getApplicationContext(), str, i);
        f2365a.show();
    }

    public static void a(final int i, final int i2) {
        c.a().a(new c.b() {
            public void a() {
                d.c(i, i2);
            }
        });
    }

    public static void a(final int i) {
        c.a().a(new c.b() {
            public void a() {
                d.c(i, 1);
            }
        });
    }

    /* access modifiers changed from: private */
    public static void c(int i, int i2) {
        if (f2365a != null) {
            f2365a.setText(i);
            f2365a.setDuration(i2);
            f2365a.show();
            return;
        }
        f2365a = Toast.makeText(RingDDApp.b().getApplicationContext(), i, i2);
        f2365a.show();
    }
}
