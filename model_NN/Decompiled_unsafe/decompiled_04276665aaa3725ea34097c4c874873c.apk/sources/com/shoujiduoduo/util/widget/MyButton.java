package com.shoujiduoduo.util.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.Button;
import com.shoujiduoduo.ringtone.R;

public class MyButton extends Button {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f2349a;
    private int b;
    private int c;
    private Drawable d;
    private int e;
    private int f;
    private Drawable g;
    private int h;
    private int i;
    private Drawable j;
    private int k;
    private int l;

    public MyButton(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        a(context, attributeSet);
    }

    public MyButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public MyButton(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet);
    }

    private void a(Context context, AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.MyButton);
            int indexCount = obtainStyledAttributes.getIndexCount();
            for (int i2 = 0; i2 < indexCount; i2++) {
                int index = obtainStyledAttributes.getIndex(i2);
                switch (index) {
                    case 0:
                        this.b = obtainStyledAttributes.getDimensionPixelSize(index, 50);
                        break;
                    case 1:
                        this.c = obtainStyledAttributes.getDimensionPixelSize(index, 50);
                        break;
                    case 2:
                        this.f2349a = obtainStyledAttributes.getDrawable(index);
                        break;
                    case 3:
                        this.e = obtainStyledAttributes.getDimensionPixelSize(index, 50);
                        break;
                    case 4:
                        this.f = obtainStyledAttributes.getDimensionPixelSize(index, 50);
                        break;
                    case 5:
                        this.d = obtainStyledAttributes.getDrawable(index);
                        break;
                    case 6:
                        this.h = obtainStyledAttributes.getDimensionPixelSize(index, 50);
                        break;
                    case 7:
                        this.i = obtainStyledAttributes.getDimensionPixelSize(index, 50);
                        break;
                    case 8:
                        this.g = obtainStyledAttributes.getDrawable(index);
                        break;
                    case 9:
                        this.k = obtainStyledAttributes.getDimensionPixelSize(index, 50);
                        break;
                    case 10:
                        this.l = obtainStyledAttributes.getDimensionPixelSize(index, 50);
                        break;
                    case 11:
                        this.j = obtainStyledAttributes.getDrawable(index);
                        break;
                }
            }
            obtainStyledAttributes.recycle();
            setCompoundDrawablesWithIntrinsicBounds(this.g, this.f2349a, this.j, this.d);
        }
    }

    public void setCompoundDrawablesWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable2 != null) {
            drawable2.setBounds(0, 0, this.b <= 0 ? drawable2.getIntrinsicWidth() : this.b, this.c <= 0 ? drawable2.getMinimumHeight() : this.c);
        }
        if (drawable != null) {
            drawable.setBounds(0, 0, this.h <= 0 ? drawable.getIntrinsicWidth() : this.h, this.i <= 0 ? drawable.getMinimumHeight() : this.i);
        }
        if (drawable3 != null) {
            drawable3.setBounds(0, 0, this.k <= 0 ? drawable3.getIntrinsicWidth() : this.k, this.l <= 0 ? drawable3.getMinimumHeight() : this.l);
        }
        if (drawable4 != null) {
            drawable4.setBounds(0, 0, this.f <= 0 ? drawable4.getIntrinsicWidth() : this.e, this.f <= 0 ? drawable4.getMinimumHeight() : this.f);
        }
        setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
    }
}
