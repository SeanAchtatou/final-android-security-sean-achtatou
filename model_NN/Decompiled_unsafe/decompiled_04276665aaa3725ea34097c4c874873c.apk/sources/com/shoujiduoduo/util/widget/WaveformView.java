package com.shoujiduoduo.util.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.ao;

public class WaveformView extends View {

    /* renamed from: a  reason: collision with root package name */
    float f2353a;
    float b;
    private Paint c;
    private Paint d;
    private Paint e;
    private Paint f;
    private Paint g;
    private Paint h;
    private Paint i;
    private Paint j;
    private b k;
    private ao l;
    private short[] m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private a s;
    private float t;
    private int u;

    public enum a {
        recording,
        rec_pause,
        listen_play,
        listen_pause,
        edit_pause,
        edit_play,
        animation
    }

    public interface b {
        void a(float f);

        void b(float f);

        void d();

        void e();
    }

    public a getDrawState() {
        return this.s;
    }

    public void setWavDataProcess(ao aoVar) {
        this.l = aoVar;
    }

    public void setListener(b bVar) {
        this.k = bVar;
    }

    public WaveformView(Context context) {
        super(context);
        this.f2353a = 0.0f;
        this.b = 0.0f;
        this.m = null;
        this.n = 0;
        this.o = 0;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.q == 0) {
            this.q = getWidth();
        }
        if (this.l == null || this.l.b() != 0) {
            if (this.s == a.edit_pause || this.s == a.edit_play) {
                if (this.m != null && this.o == ((int) this.l.j()) && this.n == this.u) {
                    com.shoujiduoduo.base.a.a.b("WavformView", "onDraw, use old data");
                    c(canvas, (float) getWidth(), this.m);
                } else {
                    this.m = ao.a().a(0, 0, getWidth());
                    com.shoujiduoduo.base.a.a.b("WavformView", "onDraw, use new data");
                    c(canvas, (float) getWidth(), this.m);
                }
                this.o = (int) this.l.j();
                this.n = this.u;
            }
        } else if (this.s == a.recording) {
            a(canvas);
        } else {
            if (this.m != null && this.o == ((int) this.l.j()) && this.n == this.u) {
                b(canvas, (float) getWidth(), this.m);
            } else {
                this.m = ao.a().a(this.u, 0, getWidth());
                b(canvas, (float) getWidth(), this.m);
            }
            this.o = (int) this.l.j();
            this.n = this.u;
        }
        if (this.k != null) {
            this.k.e();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r3) {
        /*
            r2 = this;
            com.shoujiduoduo.util.widget.WaveformView$a r0 = r2.s
            com.shoujiduoduo.util.widget.WaveformView$a r1 = com.shoujiduoduo.util.widget.WaveformView.a.recording
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x000c
            r0 = 0
        L_0x000b:
            return r0
        L_0x000c:
            int r0 = r3.getAction()
            r0 = r0 & 255(0xff, float:3.57E-43)
            switch(r0) {
                case 0: goto L_0x0021;
                case 1: goto L_0x0017;
                case 2: goto L_0x002e;
                default: goto L_0x0015;
            }
        L_0x0015:
            r0 = 1
            goto L_0x000b
        L_0x0017:
            com.shoujiduoduo.util.widget.WaveformView$b r0 = r2.k
            if (r0 == 0) goto L_0x0015
            com.shoujiduoduo.util.widget.WaveformView$b r0 = r2.k
            r0.d()
            goto L_0x0015
        L_0x0021:
            com.shoujiduoduo.util.widget.WaveformView$b r0 = r2.k
            if (r0 == 0) goto L_0x002e
            com.shoujiduoduo.util.widget.WaveformView$b r0 = r2.k
            float r1 = r3.getX()
            r0.a(r1)
        L_0x002e:
            com.shoujiduoduo.util.widget.WaveformView$b r0 = r2.k
            if (r0 == 0) goto L_0x0015
            com.shoujiduoduo.util.widget.WaveformView$b r0 = r2.k
            float r1 = r3.getX()
            r0.b(r1)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.widget.WaveformView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public WaveformView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2353a = 0.0f;
        this.b = 0.0f;
        this.s = a.recording;
        this.c = new Paint();
        this.c.setColor(getResources().getColor(R.color.waveform_bkg));
        this.e = new Paint();
        this.e.setColor(getResources().getColor(R.color.waveform_bkg_edit_sel));
        this.d = new Paint();
        this.d.setColor(getResources().getColor(R.color.waveform_bkg_edit));
        this.h = new Paint();
        this.h.setColor(getResources().getColor(R.color.waveform_line));
        this.f = new Paint();
        this.f.setColor(getResources().getColor(R.color.waveform_line_unselect));
        this.f.setAntiAlias(true);
        this.j = new Paint();
        this.j.setColor(getResources().getColor(R.color.waveform_cur_pos));
        this.j.setStrokeWidth(2.0f);
        this.i = new Paint();
        this.i.setColor(getResources().getColor(R.color.waveform_played));
        this.g = new Paint();
        this.g.setColor(-1);
    }

    public void setDrawState(a aVar) {
        com.shoujiduoduo.base.a.a.b("WavformView", "set draw state:" + aVar.toString());
        this.s = aVar;
    }

    public void setStartPos(int i2) {
        this.p = i2;
    }

    public void setEndPos(int i2) {
        this.q = i2;
    }

    public void setPlayback(int i2) {
        this.r = i2;
    }

    private void a(Canvas canvas, float f2, short[] sArr) {
        try {
            b(canvas);
            if (sArr == null || sArr.length == 0) {
                com.shoujiduoduo.base.a.a.c("WavformView", "drawData error, datalist is null");
                return;
            }
            int length = sArr.length;
            this.f2353a = 0.0f;
            this.b = (float) (getHeight() / 2);
            float f3 = f2 / ((float) length);
            for (int i2 = 0; i2 < sArr.length; i2++) {
                float f4 = 0.0f + (((float) i2) * f3);
                float a2 = a(sArr[i2]) + ((float) (getHeight() / 2));
                if ((this.s == a.edit_pause || this.s == a.edit_play) && (f4 < ((float) this.p) || f4 > ((float) this.q))) {
                    canvas.drawLine((float) ((int) this.f2353a), (float) ((int) this.b), (float) ((int) f4), (float) ((int) a2), this.f);
                } else {
                    canvas.drawLine((float) ((int) this.f2353a), (float) ((int) this.b), (float) ((int) f4), (float) ((int) a2), this.h);
                }
                this.f2353a = f4;
                this.b = a2;
            }
            c(canvas);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b(Canvas canvas, float f2, short[] sArr) {
        try {
            b(canvas);
            if (sArr == null || sArr.length == 0) {
                com.shoujiduoduo.base.a.a.c("WavformView", "drawData error, datalist is null");
                return;
            }
            this.f2353a = 0.0f;
            this.b = (float) (getHeight() / 2);
            for (int i2 = 0; i2 < sArr.length; i2++) {
                float a2 = a(sArr[i2]);
                if ((this.s == a.edit_pause || this.s == a.edit_play) && (i2 < this.p || i2 > this.q)) {
                    canvas.drawLine((float) i2, ((float) (getHeight() / 2)) - Math.abs(a2), (float) i2, ((float) (getHeight() / 2)) + Math.abs(a2), this.f);
                } else {
                    canvas.drawLine((float) i2, ((float) (getHeight() / 2)) - Math.abs(a2), (float) i2, ((float) (getHeight() / 2)) + Math.abs(a2), this.h);
                }
            }
            c(canvas);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void c(Canvas canvas, float f2, short[] sArr) {
        try {
            b(canvas);
            if (sArr == null || sArr.length == 0) {
                com.shoujiduoduo.base.a.a.c("WavformView", "drawData error, datalist is null");
                return;
            }
            int length = sArr.length;
            this.f2353a = 0.0f;
            this.b = (float) (getHeight() / 2);
            float height = (float) (getHeight() / 2);
            for (int i2 = 0; i2 < length; i2++) {
                if ((this.s == a.edit_pause || this.s == a.edit_play) && (i2 < this.p || i2 > this.q)) {
                    canvas.drawLine((float) i2, height - Math.abs(a(sArr[i2])), (float) i2, height + Math.abs(a(sArr[i2])), this.f);
                } else {
                    canvas.drawLine((float) i2, height - Math.abs(a(sArr[i2])), (float) i2, height + Math.abs(a(sArr[i2])), this.h);
                }
            }
            c(canvas);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a(float f2, int i2) {
        com.shoujiduoduo.base.a.a.a("WavformView", "drawwidth:" + f2 + "   databegin:" + i2);
        this.t = (float) ((int) f2);
        this.u = i2;
    }

    private void a(Canvas canvas) {
        try {
            long j2 = this.l.j();
            if (j2 <= 11025) {
                a(canvas, (((float) j2) / 11025.0f) * ((float) getWidth()), this.l.a(0, 0, 11025));
                return;
            }
            a(canvas, (float) getWidth(), this.l.a((int) (((float) (j2 - 11025)) / ((float) this.l.k())), 0, 11025));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private float a(short s2) {
        return ((float) s2) * (((float) getHeight()) / 65536.0f);
    }

    private void b(Canvas canvas) {
        if (this.s == a.edit_pause || this.s == a.edit_play) {
            canvas.save();
            canvas.clipRect(0, 0, this.p, getHeight());
            canvas.drawPaint(this.d);
            canvas.restore();
            canvas.save();
            canvas.clipRect(this.q, 0, getWidth(), getHeight());
            canvas.drawPaint(this.d);
            canvas.restore();
            canvas.save();
            canvas.clipRect(this.p, 0, this.q, getHeight());
            canvas.drawPaint(this.e);
            canvas.restore();
            return;
        }
        canvas.drawPaint(this.c);
    }

    private void c(Canvas canvas) {
        if (this.r != -1) {
            canvas.drawLine((float) this.r, 0.0f, (float) this.r, (float) getHeight(), this.j);
        }
    }
}
