package com.shoujiduoduo.util;

import com.shoujiduoduo.base.a.a;
import java.io.File;

/* compiled from: DuoduoCache */
public abstract class q<T> {
    protected static String c = m.a(2);
    protected String b = null;

    public q(String str) {
        this.b = str;
    }

    public q() {
    }

    public static String c() {
        return c;
    }

    public boolean a(long j) {
        long lastModified = new File(c + this.b).lastModified();
        if (lastModified == 0) {
            return true;
        }
        long currentTimeMillis = System.currentTimeMillis() - lastModified;
        long j2 = currentTimeMillis / 1000;
        a.a("DuoduoCache Base Class", "time since cached: " + (j2 / 3600) + "小时" + ((j2 % 3600) / 60) + "分钟" + (j2 % 60) + "秒");
        if (currentTimeMillis > j) {
            a.a("DuoduoCache Base Class", "cache out of date.");
            return true;
        }
        a.a("DuoduoCache Base Class", "cache is valid. use cache.");
        return false;
    }
}
