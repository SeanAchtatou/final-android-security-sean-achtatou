package com.shoujiduoduo.util;

import java.lang.Character;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/* compiled from: PinyinUtils */
public class aa {
    public static String a(char c) {
        if (c > 0 && c < 128) {
            return c + "";
        }
        HanyuPinyinOutputFormat hanyuPinyinOutputFormat = new HanyuPinyinOutputFormat();
        hanyuPinyinOutputFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        hanyuPinyinOutputFormat.setVCharType(HanyuPinyinVCharType.WITH_V);
        hanyuPinyinOutputFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        if (!b(c)) {
            return c + "";
        }
        try {
            String[] hanyuPinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(c, hanyuPinyinOutputFormat);
            if (hanyuPinyinStringArray == null) {
                return c + "";
            }
            try {
                return String.valueOf(hanyuPinyinStringArray[0].charAt(0));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } catch (BadHanyuPinyinOutputFormatCombination e2) {
            e2.printStackTrace();
            return c + "";
        }
    }

    private static boolean b(char c) {
        Character.UnicodeBlock of = Character.UnicodeBlock.of(c);
        if (of == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || of == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || of == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || of == Character.UnicodeBlock.GENERAL_PUNCTUATION || of == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || of == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }
}
