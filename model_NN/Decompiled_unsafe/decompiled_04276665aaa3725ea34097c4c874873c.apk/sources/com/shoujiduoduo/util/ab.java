package com.shoujiduoduo.util;

import com.shoujiduoduo.player.PlayerService;

/* compiled from: PlayerServiceUtil */
public class ab {
    private static ab b = null;

    /* renamed from: a  reason: collision with root package name */
    private PlayerService f2178a;

    private ab() {
    }

    public static ab a() {
        if (b == null) {
            b = new ab();
        }
        return b;
    }

    public void a(PlayerService playerService) {
        this.f2178a = playerService;
    }

    public PlayerService b() {
        return this.f2178a;
    }

    public void c() {
        this.f2178a = null;
        b = null;
    }
}
