package com.shoujiduoduo.util.d;

import android.text.TextUtils;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.u;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: ChinaTelecomUtils */
public class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final c.b f2283a = new c.b(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "对不起，中国电信的彩铃服务正在进行系统维护，请谅解");
    /* access modifiers changed from: private */
    public static final c.b b = new c.b("-2", "对不起，网络好像出了点问题，请稍后再试试");
    /* access modifiers changed from: private */
    public HashMap<String, d> c = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, c> d = new HashMap<>();
    private String e;
    private String f;

    /* compiled from: ChinaTelecomUtils */
    private enum a {
        GET,
        POST
    }

    /* renamed from: com.shoujiduoduo.util.d.b$b  reason: collision with other inner class name */
    /* compiled from: ChinaTelecomUtils */
    private static class C0053b {

        /* renamed from: a  reason: collision with root package name */
        public static b f2287a = new b();
    }

    /* compiled from: ChinaTelecomUtils */
    public enum d {
        wait_open,
        open,
        close,
        unknown
    }

    /* compiled from: ChinaTelecomUtils */
    public static class c {

        /* renamed from: a  reason: collision with root package name */
        public boolean f2288a;
        public boolean b;

        public c() {
            this.f2288a = false;
            this.b = false;
        }

        public c(boolean z, boolean z2) {
            this.f2288a = z;
            this.b = z2;
        }
    }

    public static b a() {
        return C0053b.f2287a;
    }

    public d a(String str) {
        if (this.c.get(str) != null) {
            return this.c.get(str);
        }
        return d.unknown;
    }

    public c b(String str) {
        if (this.d.get(str) != null) {
            return this.d.get(str);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public c.b e(String str) {
        JSONObject optJSONObject;
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "content:" + str);
        if (str == null || str.equals("")) {
            return null;
        }
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
            JSONObject optJSONObject2 = jSONObject.optJSONObject("BasicJTResponse");
            if (optJSONObject2 != null) {
                c.b bVar = new c.b();
                bVar.b = optJSONObject2.optString("res_code");
                bVar.c = optJSONObject2.optString("res_message");
                return bVar;
            }
            JSONObject optJSONObject3 = jSONObject.optJSONObject("playModeResponse");
            if (optJSONObject3 != null) {
                c.w wVar = new c.w();
                wVar.b = optJSONObject3.optString("res_code");
                wVar.c = optJSONObject3.optString("res_message");
                wVar.f2253a = optJSONObject3.optString("play_mode");
                return wVar;
            }
            JSONObject optJSONObject4 = jSONObject.optJSONObject("queryRingResponse");
            if (optJSONObject4 != null) {
                c.z zVar = new c.z();
                zVar.b = optJSONObject4.optString("res_code");
                zVar.c = optJSONObject4.optString("res_message");
                zVar.f2256a = new ArrayList();
                JSONArray optJSONArray = optJSONObject4.optJSONArray("ring_item");
                if (optJSONArray != null) {
                    for (int i = 0; i < optJSONArray.length(); i++) {
                        c.ae aeVar = new c.ae();
                        JSONObject optJSONObject5 = optJSONArray.optJSONObject(i);
                        if (optJSONObject5 != null) {
                            aeVar.f2225a = optJSONObject5.optString("ringId");
                            aeVar.f = optJSONObject5.optString("price");
                            aeVar.b = optJSONObject5.optString("ringName");
                            aeVar.d = optJSONObject5.optString("author");
                            String optString = optJSONObject5.optString("validDate");
                            if (!TextUtils.isEmpty(optString) && optString.length() >= 10) {
                                optString = optString.substring(0, 10);
                            }
                            aeVar.g = optString;
                            zVar.f2256a.add(aeVar);
                        }
                    }
                } else {
                    JSONObject optJSONObject6 = optJSONObject4.optJSONObject("ring_item");
                    if (optJSONObject6 != null) {
                        c.ae aeVar2 = new c.ae();
                        aeVar2.f2225a = optJSONObject6.optString("ringId");
                        aeVar2.f = optJSONObject6.optString("price");
                        aeVar2.b = optJSONObject6.optString("ringName");
                        aeVar2.d = optJSONObject6.optString("author");
                        String optString2 = optJSONObject6.optString("validDate");
                        if (!TextUtils.isEmpty(optString2) && optString2.length() >= 10) {
                            optString2 = optString2.substring(0, 10);
                        }
                        aeVar2.g = optString2;
                        zVar.f2256a.add(aeVar2);
                    }
                }
                return zVar;
            }
            JSONObject optJSONObject7 = jSONObject.optJSONObject("defaultRingResponse");
            if (optJSONObject7 != null) {
                c.z zVar2 = new c.z();
                zVar2.b = optJSONObject7.optString("res_code");
                zVar2.c = optJSONObject7.optString("res_message");
                JSONObject optJSONObject8 = optJSONObject7.optJSONObject("crbt_id_list");
                if (optJSONObject8 == null) {
                    return zVar2;
                }
                zVar2.f2256a = new ArrayList();
                c.ae aeVar3 = new c.ae();
                aeVar3.f2225a = optJSONObject8.optString("crbt_id");
                zVar2.f2256a.add(aeVar3);
                return zVar2;
            }
            JSONObject optJSONObject9 = jSONObject.optJSONObject("audioFileResponse");
            if (optJSONObject9 != null) {
                c.m mVar = new c.m();
                mVar.b = optJSONObject9.optString("res_code");
                mVar.c = optJSONObject9.optString("res_message");
                JSONObject optJSONObject10 = optJSONObject9.optJSONObject("audioFileItemList");
                if (optJSONObject10 != null) {
                    JSONArray optJSONArray2 = optJSONObject10.optJSONArray("audioFileItem");
                    if (optJSONArray2 != null) {
                        int i2 = 0;
                        for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                            JSONObject optJSONObject11 = optJSONArray2.optJSONObject(i3);
                            if (optJSONObject11 != null && optJSONObject11.optInt("bit_rate", 0) >= 0) {
                                i2 = i3;
                            }
                        }
                        JSONObject optJSONObject12 = optJSONArray2.optJSONObject(i2);
                        if (optJSONObject12 != null) {
                            mVar.f2242a = optJSONObject12.optString("file_address");
                            mVar.d = optJSONObject12.optInt("bit_rate", 128);
                            mVar.e = optJSONObject12.optString("format", "mp3");
                        }
                    } else {
                        JSONObject optJSONObject13 = optJSONObject10.optJSONObject("audioFileItem");
                        if (optJSONObject13 != null) {
                            mVar.f2242a = optJSONObject13.optString("file_address");
                            mVar.d = optJSONObject13.optInt("bit_rate", 128);
                            mVar.e = optJSONObject13.optString("format", "mp3");
                        }
                    }
                }
                return mVar;
            }
            JSONObject optJSONObject14 = jSONObject.optJSONObject("music_product");
            if (optJSONObject14 != null) {
                c.x xVar = new c.x();
                xVar.b = optJSONObject14.optString("res_code");
                xVar.c = optJSONObject14.optString("res_message");
                xVar.d = optJSONObject14.optString("resource_id");
                xVar.f2254a = new c.ae();
                xVar.f2254a.f2225a = optJSONObject14.optString("product_id");
                xVar.f2254a.f = optJSONObject14.optString("price");
                xVar.f2254a.g = optJSONObject14.optString("invalid_time");
                xVar.f2254a.b = optJSONObject14.optString("song_name");
                xVar.f2254a.d = optJSONObject14.optString("singer_name");
                return xVar;
            }
            JSONObject optJSONObject15 = jSONObject.optJSONObject("UserPackageListResp");
            if (optJSONObject15 != null) {
                c.v vVar = new c.v();
                vVar.b = optJSONObject15.optString("res_code");
                vVar.c = optJSONObject15.optString("res_message");
                JSONObject optJSONObject16 = optJSONObject15.optJSONObject("user_package_list");
                if (optJSONObject16 == null || (optJSONObject = optJSONObject16.optJSONObject("user_package")) == null) {
                    return vVar;
                }
                c.aj ajVar = new c.aj();
                ajVar.f2230a = optJSONObject.optString("package_id");
                ajVar.e = optJSONObject.optString("count_down_num");
                ajVar.b = optJSONObject.optString("order_time");
                ajVar.d = optJSONObject.optString("unsubscribe_time");
                ajVar.c = optJSONObject.optString("status");
                vVar.f2252a = optJSONObject.optString("status");
                if (!ajVar.c.equals("0") && !ajVar.c.equals("2")) {
                    return vVar;
                }
                vVar.d = true;
                return vVar;
            }
            JSONObject optJSONObject17 = jSONObject.optJSONObject("EmpPackageResp");
            if (optJSONObject17 != null) {
                c.o oVar = new c.o();
                oVar.b = optJSONObject17.optString("res_code");
                oVar.c = optJSONObject17.optString("res_message");
                oVar.f2245a = optJSONObject17.optString("fee_type");
                return oVar;
            }
            JSONObject optJSONObject18 = jSONObject.optJSONObject("DEPUserInfoResponse");
            if (optJSONObject18 != null) {
                c.i iVar = new c.i();
                iVar.b = optJSONObject18.optString("res_code");
                iVar.c = optJSONObject18.optString("res_message");
                iVar.f2238a = optJSONObject18.optString("mdn");
                return iVar;
            }
            JSONObject optJSONObject19 = jSONObject.optJSONObject("ringClipJTResponse");
            if (optJSONObject19 != null) {
                c.aa aaVar = new c.aa();
                aaVar.b = optJSONObject19.optString("res_code");
                aaVar.c = optJSONObject19.optString("res_message");
                aaVar.f2221a = optJSONObject19.optString("audio_id");
                aaVar.d = optJSONObject19.optString("audio_url");
                return aaVar;
            }
            JSONObject optJSONObject20 = jSONObject.optJSONObject("queryRingStatueJTResponse");
            if (optJSONObject20 == null) {
                return null;
            }
            c.h hVar = new c.h();
            hVar.b = optJSONObject20.optString("res_code");
            hVar.c = optJSONObject20.optString("res_message");
            hVar.f2237a = optJSONObject20.optString("ring_id");
            hVar.d = optJSONObject20.optString("ringStatus");
            hVar.e = optJSONObject20.optString("audio_url");
            return hVar;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void a(String str, boolean z, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("package_id", z ? "135000000000000214309" : "135000000000000214311"));
        a(arrayList, "/package/packageservice/emplanunched", bVar, "&phone=" + str, a.POST);
    }

    public void a(String str, boolean z, String str2, boolean z2, String str3, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        if (z) {
            arrayList.add(new BasicNameValuePair("random_key", str2));
        }
        this.e = str;
        if (z) {
            this.f = z2 ? "ct_open_diy_emp" : "ct_open_vip_emp";
        } else {
            this.f = z2 ? "ct_open_diy" : "ct_open_vip";
        }
        arrayList.add(new BasicNameValuePair("package_id", z2 ? "135000000000000214309" : "135000000000000214311"));
        a(arrayList, z ? "/package/packageservice/subscribebyemp" : "/package/packageservice/subscribe", bVar, str3, a.POST);
    }

    public void a(String str, String str2, boolean z, String str3, com.shoujiduoduo.util.b.b bVar) {
        this.e = str;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("random_key", str2));
        arrayList.add(new BasicNameValuePair("order_num", z ? "2" : "1"));
        this.f = z ? "ct_open_cailing_diy_emp" : "ct_open_cailing_vip_emp";
        a(arrayList, "/package/packageservice/ringduoduosubscribebyemp", bVar, str3, a.POST);
    }

    public void a(String str, com.shoujiduoduo.util.b.b bVar) {
        this.e = str;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new BasicNameValuePair("mdn", str));
        arrayList2.add(new BasicNameValuePair("package_id", "135000000000000214311"));
        arrayList2.add(new BasicNameValuePair("is_count_down_num", "1"));
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(new BasicNameValuePair("mdn", str));
        arrayList3.add(new BasicNameValuePair("package_id", "135000000000000214309"));
        arrayList3.add(new BasicNameValuePair("is_count_down_num", "1"));
        a(str, arrayList, arrayList2, arrayList3, bVar);
    }

    public void a(String str, boolean z, boolean z2, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("package_id", z2 ? "135000000000000214309" : "135000000000000214311"));
        a(arrayList, z ? "/package/packageservice/unsubscribebyemp" : "/package/packageservice/unsubscribe", bVar, a.POST);
    }

    public void a(String str, boolean z, String str2, com.shoujiduoduo.util.b.b bVar) {
        if (!this.d.containsKey(str) || (!z ? !this.d.get(str).f2288a : !this.d.get(str).b)) {
            com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryVipState, 去电信查询");
            this.e = str;
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mdn", str));
            arrayList.add(new BasicNameValuePair("package_id", z ? "135000000000000214309" : "135000000000000214311"));
            arrayList.add(new BasicNameValuePair("is_count_down_num", "1"));
            a(arrayList, "/package/packageservice/querypackagelist", bVar, a.GET);
            return;
        }
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryVipState, 返回缓存VIP开通状态");
        c.v vVar = new c.v();
        vVar.b = "0000";
        vVar.c = "开通状态";
        vVar.f2252a = "0";
        vVar.d = true;
        bVar.g(vVar);
    }

    public void a(String str, String str2, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("template_id", "100421"));
        arrayList.add(new BasicNameValuePair("template_params", str2 + "#铃声多多"));
        a(arrayList, "/ivr/ivrservice/sendtemplatemsg", bVar, "&phone=" + str, a.POST);
    }

    public void a(String str, String str2, String str3, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("crbt_id", str2));
        arrayList.add(new BasicNameValuePair("type", "1"));
        a(arrayList, "/music/ringduoduocrbtservice/order", bVar, str3, a.POST);
    }

    public void b(String str, com.shoujiduoduo.util.b.b bVar) {
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "findMdnByImsi");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("imsi", str));
        a(arrayList, "/depservice/dep/findmdnbyimsi", bVar, a.GET);
    }

    public void a(String str, String str2, String str3, String str4, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "diyClipUpload, ringname:" + str);
        String d2 = g.d(str);
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "diyClipUpload, filtered ringname:" + d2);
        if (d2.length() > 30) {
            d2 = d2.substring(0, 30);
        }
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "diyClipUpload, ringname:" + d2 + ", length:" + d2.length());
        arrayList.add(new BasicNameValuePair("ringName", d2));
        arrayList.add(new BasicNameValuePair("userPhone", str2));
        arrayList.add(new BasicNameValuePair("url", str3));
        a(arrayList, "/ringdiy/ringdiyservice/ringClip", bVar, str4, a.POST);
    }

    public void b(String str, String str2, String str3, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("audio_id", str));
        arrayList.add(new BasicNameValuePair("userPhone", str2));
        a(arrayList, "/ringdiy/ringdiyservice/setDiyRing", bVar, str3, a.POST);
    }

    public void c(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair(WBPageConstants.ParamKey.PAGE, "0"));
        arrayList.add(new BasicNameValuePair("per_page", "100"));
        arrayList.add(new BasicNameValuePair("order", "1"));
        arrayList.add(new BasicNameValuePair("userPhone", str));
        a(arrayList, "/ringdiy/ringdiyservice/getDiyRing", bVar, a.POST);
    }

    public void b(String str, String str2, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("audio_id", str));
        arrayList.add(new BasicNameValuePair("userPhone", str2));
        a(arrayList, "/ringdiy/ringdiyservice/queryRingStatus", bVar, a.GET);
    }

    public c.h a(String str, String str2) {
        c.b e2;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("audio_id", str));
        arrayList.add(new BasicNameValuePair("userPhone", str2));
        try {
            String a2 = c.a(arrayList, "/ringdiy/ringdiyservice/queryRingStatus", ".json");
            if (!(a2 == null || (e2 = e(a2)) == null || !(e2 instanceof c.h))) {
                return (c.h) e2;
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    public void d(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        a(arrayList, "/music/crbtservice/sendrandom", bVar, "&phone=" + str, a.POST);
    }

    public c.b c(String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mdn", str));
            arrayList.add(new BasicNameValuePair(WBPageConstants.ParamKey.COUNT, "100"));
            arrayList.add(new BasicNameValuePair(WBPageConstants.ParamKey.PAGE, "0"));
            String a2 = c.a(arrayList, "/music/crbtservice/queryring", ".json");
            if (a2 == null) {
                return null;
            }
            c.b e2 = e(a2);
            a("/music/crbtservice/queryring", e2, "&phone=" + str);
            return e2;
        } catch (Exception e3) {
            e3.printStackTrace();
            return null;
        }
    }

    public void e(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair(WBPageConstants.ParamKey.COUNT, "100"));
        arrayList.add(new BasicNameValuePair(WBPageConstants.ParamKey.PAGE, "0"));
        a(arrayList, "/music/crbtservice/queryring", bVar, a.GET);
    }

    public void f(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        a(arrayList, "/music/crbtservice/querydefaultring", bVar, a.GET);
    }

    public c.b d(String str) {
        c.b bVar;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        try {
            String a2 = c.a(arrayList, "/music/crbtservice/querydefaultring", ".json");
            if (a2 != null) {
                bVar = e(a2);
                if (bVar == null) {
                    bVar = f2283a;
                }
            } else {
                bVar = b;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            bVar = b;
        }
        a("/music/crbtservice/querydefaultring", bVar, "");
        return bVar;
    }

    public void c(String str, String str2, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("crbt_id", str2));
        a(arrayList, "/music/crbtservice/setring", bVar, a.POST);
    }

    public void d(String str, String str2, com.shoujiduoduo.util.b.b bVar) {
        this.e = str;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("random_key", str2));
        a(arrayList, "/music/crbtservice/open", bVar, "&phone=" + str, a.POST);
    }

    public void a(String str, String str2, String str3, String str4, String str5, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("to_mdn", str2));
        arrayList.add(new BasicNameValuePair("crbt_id", str3));
        arrayList.add(new BasicNameValuePair("random_key", str4));
        a(arrayList, "/music/crbtservice/present", bVar, str5, a.POST);
    }

    public void e(String str, String str2, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("mdn", str));
        arrayList.add(new BasicNameValuePair("crbt_id", str2));
        a(arrayList, "/ivr/ivrservice/deletecrbtring", bVar, a.POST);
    }

    public void g(String str, com.shoujiduoduo.util.b.b bVar) {
        if (!this.c.containsKey(str) || !this.c.get(str).equals(d.open)) {
            com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "openCheck, 去电信查询状态");
            this.e = str;
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("mdn", str));
            a(arrayList, "/music/crbtservice/iscrbtuser", bVar, "&phone=" + str, a.GET);
            return;
        }
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "openCheck, 返回缓存状态：开通");
        bVar.g(new c.b("0000", "成功"));
    }

    public void h(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("resource_id", str));
        arrayList.add(new BasicNameValuePair("format", "mp3"));
        a(arrayList, "/audio/iaudiomanager/queryringtone", bVar, a.GET);
    }

    public void i(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("resource_id", str));
        arrayList.add(new BasicNameValuePair("format", "mp3"));
        a(arrayList, "/audio/iaudiomanager/querycrbt", bVar, a.GET);
    }

    public void j(String str, com.shoujiduoduo.util.b.b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("resource_id", str));
        a(arrayList, "/product/productquery/querycrbtinfo", bVar, a.GET);
    }

    private void a(List<NameValuePair> list, String str, com.shoujiduoduo.util.b.b bVar, a aVar) {
        a(list, str, bVar, "", aVar);
    }

    private void a(List<NameValuePair> list, String str, com.shoujiduoduo.util.b.b bVar, String str2, a aVar) {
        final a aVar2 = aVar;
        final List<NameValuePair> list2 = list;
        final String str3 = str;
        final String str4 = str;
        final String str5 = str2;
        final com.shoujiduoduo.util.b.b bVar2 = bVar;
        i.a(new Runnable() {
            public void run() {
                c.b c2;
                String str = null;
                try {
                    if (aVar2.equals(a.POST)) {
                        str = c.b(list2, str3, ".json");
                    } else if (aVar2.equals(a.GET)) {
                        str = c.a(list2, str3, ".json");
                    }
                    if (str != null) {
                        c2 = b.this.e(str);
                        if (c2 == null) {
                            c2 = b.f2283a;
                        }
                    } else {
                        c2 = b.b;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    c2 = b.b;
                }
                b.this.a(str4, c2, str5);
                bVar2.g(c2);
            }
        });
    }

    private void a(String str, List<NameValuePair> list, List<NameValuePair> list2, List<NameValuePair> list3, com.shoujiduoduo.util.b.b bVar) {
        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "doRequestCailingAndVipCheck");
        final String str2 = str;
        final List<NameValuePair> list4 = list;
        final List<NameValuePair> list5 = list2;
        final List<NameValuePair> list6 = list3;
        final com.shoujiduoduo.util.b.b bVar2 = bVar;
        i.a(new Runnable() {
            public void run() {
                try {
                    c.e eVar = new c.e();
                    eVar.b = "0000";
                    eVar.c = "查询成功";
                    if (!b.this.c.containsKey(str2) || !((d) b.this.c.get(str2)).equals(d.open)) {
                        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "doRequestCailingAndVipCheck, 去电信查询状态");
                        String a2 = c.a(list4, "/music/crbtservice/iscrbtuser", ".json");
                        if (a2 != null) {
                            c.b a3 = b.this.e(a2);
                            if (a3 != null) {
                                eVar.f2234a = a3;
                            } else {
                                eVar.f2234a = b.f2283a;
                            }
                        } else {
                            eVar.f2234a = b.f2283a;
                        }
                        b.this.a("/music/crbtservice/iscrbtuser", eVar.f2234a, "&phone=" + str2);
                    } else {
                        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "openCheck, 返回缓存状态：开通");
                        eVar.f2234a = new c.b("0000", "成功");
                    }
                    if (!b.this.d.containsKey(str2) || !((c) b.this.d.get(str2)).f2288a) {
                        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryVipState, 去电信查询");
                        String a4 = c.a(list5, "/package/packageservice/querypackagelist", ".json");
                        if (a4 != null) {
                            c.b a5 = b.this.e(a4);
                            if (a5 != null) {
                                eVar.d = a5;
                            } else {
                                eVar.d = b.f2283a;
                            }
                        } else {
                            eVar.d = b.f2283a;
                        }
                        b.this.a("/package/packageservice/querypackagelist", eVar.d, "&phone=" + str2);
                    } else {
                        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryVipState, 返回缓存VIP开通状态");
                        c.v vVar = new c.v();
                        vVar.b = "0000";
                        vVar.c = "开通状态";
                        vVar.d = true;
                        vVar.f2252a = "0";
                        eVar.d = vVar;
                    }
                    if (!b.this.d.containsKey(str2) || !((c) b.this.d.get(str2)).b) {
                        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryDiyVipState, 去电信查询");
                        String a6 = c.a(list6, "/package/packageservice/querypackagelist", ".json");
                        if (a6 != null) {
                            c.b a7 = b.this.e(a6);
                            if (a7 != null) {
                                eVar.e = a7;
                            } else {
                                eVar.e = b.f2283a;
                            }
                        } else {
                            eVar.e = b.f2283a;
                        }
                        b.this.a("query_diy_state", eVar.e, "&phone=" + str2);
                    } else {
                        com.shoujiduoduo.base.a.a.a("ChinaTelecomUtils", "queryDiyVipState, 返回缓存VIP开通状态");
                        c.v vVar2 = new c.v();
                        vVar2.b = "0000";
                        vVar2.c = "开通状态";
                        vVar2.d = true;
                        vVar2.f2252a = "0";
                        eVar.e = vVar2;
                    }
                    bVar2.g(eVar);
                } catch (Exception e2) {
                    e2.printStackTrace();
                    bVar2.g(b.b);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(String str, c.b bVar, String str2) {
        String str3;
        String str4;
        if (str.equals("/depservice/dep/findmdnbyimsi")) {
            HashMap hashMap = new HashMap();
            if (bVar.c()) {
                hashMap.put(Parameters.RESOLUTION, "success");
                a("ct_findmdnbyimsi", "success", str2);
            } else {
                hashMap.put(Parameters.RESOLUTION, bVar.toString());
                a("ct_findmdnbyimsi", "fail, " + bVar.toString(), str2);
            }
            com.umeng.a.b.a(RingDDApp.c(), "ctcc_find_mdn_by_imsi", hashMap);
        } else if (str.equals("/music/crbtservice/queryring")) {
            HashMap hashMap2 = new HashMap();
            if (bVar.c()) {
                hashMap2.put(Parameters.RESOLUTION, "success");
                a("ct_box_query", "success", str2);
            } else {
                hashMap2.put(Parameters.RESOLUTION, bVar.toString());
                a("ct_box_query", "fail, " + bVar.toString(), str2);
            }
            com.umeng.a.b.a(RingDDApp.c(), "ctcc_query_ringbox", hashMap2);
        } else if (str.equals("/music/ringduoduocrbtservice/order")) {
            if (bVar.c()) {
                a("ct_vip_order", "success", str2);
            } else {
                a("ct_vip_order", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/ringdiy/ringdiyservice/ringClip")) {
            if (bVar.c()) {
                a("ct_diy_clip_upload", "success", str2);
            } else {
                a("ct_diy_clip_upload", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/ringdiy/ringdiyservice/setDiyRing")) {
            if (bVar.c()) {
                a("ct_set_diy_ring", "success", str2);
            } else {
                a("ct_set_diy_ring", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/package/packageservice/emplanunched")) {
            if (bVar.c()) {
                a("ct_launch_emp", "success", str2);
            } else {
                a("ct_launch_emp", "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/package/packageservice/subscribe") || str.equals("/package/packageservice/subscribebyemp")) {
            if (bVar.c()) {
                a(this.f, "success", str2);
            } else {
                a(this.f, "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/music/crbtservice/open")) {
            if (bVar.c() || bVar.a().equals("0002") || bVar.a().equals("9028") || bVar.a().equals("0764") || bVar.a().equals("0501") || bVar.a().equals("02000000")) {
                this.c.put(this.e, d.wait_open);
            }
            if (bVar.c()) {
                a("ct_open_cailing", "success", str2);
            } else {
                a("ct_open_cailing", "fail," + bVar.toString(), str2);
            }
        } else if (str.equals("/music/crbtservice/iscrbtuser")) {
            HashMap hashMap3 = new HashMap();
            if (bVar.c()) {
                hashMap3.put(Parameters.RESOLUTION, "success");
                this.c.put(this.e, d.open);
                a("ct_check_cailing", "success", str2);
            } else {
                hashMap3.put(Parameters.RESOLUTION, bVar.toString());
                a("ct_check_cailing", "fail, " + bVar.toString(), str2);
            }
            com.umeng.a.b.a(RingDDApp.c(), "ctcc_open_check", hashMap3);
        } else if (str.equals("/package/packageservice/querypackagelist")) {
            if (!bVar.c()) {
                a("ct_vip_query", "fail, " + bVar.toString(), str2);
            } else if (bVar instanceof c.v) {
                if (((c.v) bVar).d) {
                    String str5 = "&status=" + "open";
                    if (this.d.get(this.e) != null) {
                        this.d.get(this.e).f2288a = true;
                        str4 = str5;
                    } else {
                        this.d.put(this.e, new c(true, false));
                        str4 = str5;
                    }
                } else {
                    String str6 = "&status=" + "close";
                    if (this.d.get(this.e) != null) {
                        this.d.get(this.e).f2288a = false;
                        str4 = str6;
                    } else {
                        this.d.put(this.e, new c(false, false));
                        str4 = str6;
                    }
                }
                a("ct_vip_query", "success", str4 + str2);
            }
        } else if (str.equals("query_diy_state")) {
            if (!bVar.c()) {
                a("ct_diy_query", "fail, " + bVar.toString(), str2);
            } else if (bVar instanceof c.v) {
                if (((c.v) bVar).d) {
                    String str7 = "&status=" + "open";
                    if (this.d.get(this.e) != null) {
                        this.d.get(this.e).b = true;
                        str3 = str7;
                    } else {
                        this.d.put(this.e, new c(false, true));
                        str3 = str7;
                    }
                } else {
                    String str8 = "&status=" + "close";
                    if (this.d.get(this.e) != null) {
                        this.d.get(this.e).b = false;
                        str3 = str8;
                    } else {
                        this.d.put(this.e, new c(false, false));
                        str3 = str8;
                    }
                }
                a("ct_diy_query", "success", str3 + str2);
            }
        } else if (str.equals("/package/packageservice/ringduoduosubscribebyemp")) {
            if (bVar.c() || bVar.a().equals("c0002") || bVar.a().equals("c9028") || bVar.a().equals("c0764") || bVar.a().equals("c02000000")) {
                this.c.put(this.e, d.wait_open);
            }
            if (bVar.c()) {
                a(this.f, "success", str2);
            } else {
                a(this.f, "fail, " + bVar.toString(), str2);
            }
        } else if (str.equals("/music/crbtservice/sendrandom")) {
            if (bVar.c()) {
                a("ct_sendrandom", "success", str2);
            } else {
                a("ct_sendrandom", "fail," + bVar.toString(), str2);
            }
        } else if (str.equals("/music/crbtservice/order")) {
            if (bVar.c()) {
                a("ct_buy", "success", str2);
            } else {
                a("ct_buy", "fail," + bVar.toString(), str2);
            }
        } else if (str.equals("/ivr/ivrservice/sendtemplatemsg")) {
            if (bVar.c()) {
                a("ct_send_sms", "success", str2);
            } else {
                a("ct_send_sms", "fail," + bVar.toString(), str2);
            }
        } else if (!str.equals("/music/crbtservice/present")) {
        } else {
            if (bVar.c()) {
                a("ct_give", "success", str2);
            } else {
                a("ct_give", "fail," + bVar.toString(), str2);
            }
        }
    }

    private void a(String str, String str2, String str3) {
        u.a("ct:" + str, str2, str3);
    }
}
