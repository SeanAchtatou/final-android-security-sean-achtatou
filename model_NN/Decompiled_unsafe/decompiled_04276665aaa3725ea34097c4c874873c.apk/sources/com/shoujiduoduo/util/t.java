package com.shoujiduoduo.util;

import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.text.TextUtils;
import com.meizu.cloud.pushsdk.constants.MeizuConstants;
import com.shoujiduoduo.base.a.a;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: GioneeUtil */
public class t {

    /* renamed from: a  reason: collision with root package name */
    private static int f2327a;
    private static int b;
    private static int c;
    private static boolean d = false;
    private static Object e;

    public static void a() {
        if (c()) {
            a.a("GioneeUtil", "initTypeParam in : is dual sim");
        } else {
            a.a("GioneeUtil", "initTypeParam in: not dual sim");
        }
        if (d()) {
            a.a("GioneeUtil", "hasAudioProfileMode:true");
        } else {
            a.a("GioneeUtil", "hasAudioProfileMode:false");
        }
        try {
            Class<?> cls = Class.forName("android.media.RingtoneManager");
            f2327a = ((Integer) cls.getField("TYPE_RINGTONE2").get(null)).intValue();
            b = ((Integer) cls.getField("TYPE_MMS").get(null)).intValue();
            c = ((Integer) cls.getField("TYPE_MMS2").get(null)).intValue();
            a.a("GioneeUtil", "ringtonemanager_type_ringtone2:" + f2327a);
            a.a("GioneeUtil", "ringtonemanager_type_mms:" + b);
            a.a("GioneeUtil", "ringtonemanager_type_mms2:" + c);
            d = true;
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
            a.a(e2);
        } catch (IllegalArgumentException e3) {
            e3.printStackTrace();
            a.a(e3);
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
            a.a(e4);
        } catch (NoSuchFieldException e5) {
            e5.printStackTrace();
            a.a(e5);
        }
        a.a("GioneeUtil", "initTypeParam out");
    }

    public static boolean b() {
        a.a("GioneeUtil", "isGionee in");
        try {
            Class<?> cls = Class.forName(MeizuConstants.CLS_NAME_SYSTEM_PROPERTIES);
            String str = (String) cls.getMethod("get", String.class, String.class).invoke(cls.newInstance(), new String("ro.product.name"), new String("no"));
            a.a("GioneeUtil", "ro.product.name:" + str);
            if (TextUtils.isEmpty(str)) {
                a.a("GioneeUtil", "ro.product.name is empty, is not gionee phone");
                return false;
            }
            boolean equalsIgnoreCase = str.equalsIgnoreCase("GiONEE");
            a.a("GioneeUtil", "is gionee phone:" + equalsIgnoreCase);
            return equalsIgnoreCase;
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
            a.a(e2);
            a.a("GioneeUtil", "exption cache");
            a.a("GioneeUtil", "isGionee out");
            return false;
        } catch (ClassNotFoundException e3) {
            e3.printStackTrace();
            a.a(e3);
            a.a("GioneeUtil", "exption cache");
            a.a("GioneeUtil", "isGionee out");
            return false;
        } catch (NoSuchMethodException e4) {
            e4.printStackTrace();
            a.a(e4);
            a.a("GioneeUtil", "exption cache");
            a.a("GioneeUtil", "isGionee out");
            return false;
        } catch (InstantiationException e5) {
            e5.printStackTrace();
            a.a(e5);
            a.a("GioneeUtil", "exption cache");
            a.a("GioneeUtil", "isGionee out");
            return false;
        } catch (IllegalAccessException e6) {
            e6.printStackTrace();
            a.a(e6);
            a.a("GioneeUtil", "exption cache");
            a.a("GioneeUtil", "isGionee out");
            return false;
        } catch (InvocationTargetException e7) {
            e7.printStackTrace();
            a.a(e7);
            a.a("GioneeUtil", "exption cache");
            a.a("GioneeUtil", "isGionee out");
            return false;
        } catch (Exception e8) {
            a.a("GioneeUtil", "exption cache");
            a.a("GioneeUtil", "isGionee out");
            return false;
        }
    }

    public static boolean c() {
        a.a("GioneeUtil", "isDualSim in");
        try {
            Class<?> cls = Class.forName(MeizuConstants.CLS_NAME_SYSTEM_PROPERTIES);
            String str = (String) cls.getMethod("get", String.class, String.class).invoke(cls.newInstance(), new String("ro.gn.gemini.support"), new String("no"));
            a.a("GioneeUtil", "ro.gn.gemini.support:" + str);
            if (TextUtils.isEmpty(str)) {
                a.a("GioneeUtil", "ro.gn.gemini.support is empty, is not dual sim");
                return false;
            }
            boolean equalsIgnoreCase = str.equalsIgnoreCase("yes");
            a.a("GioneeUtil", "is dual sim:" + equalsIgnoreCase);
            return equalsIgnoreCase;
        } catch (IllegalArgumentException e2) {
            e2.printStackTrace();
            a.a(e2);
            a.a("GioneeUtil", "isDualSim out, exception cache");
            return false;
        } catch (ClassNotFoundException e3) {
            e3.printStackTrace();
            a.a(e3);
            a.a("GioneeUtil", "isDualSim out, exception cache");
            return false;
        } catch (NoSuchMethodException e4) {
            e4.printStackTrace();
            a.a(e4);
            a.a("GioneeUtil", "isDualSim out, exception cache");
            return false;
        } catch (InstantiationException e5) {
            e5.printStackTrace();
            a.a(e5);
            a.a("GioneeUtil", "isDualSim out, exception cache");
            return false;
        } catch (IllegalAccessException e6) {
            e6.printStackTrace();
            a.a(e6);
            a.a("GioneeUtil", "isDualSim out, exception cache");
            return false;
        } catch (InvocationTargetException e7) {
            e7.printStackTrace();
            a.a(e7);
            a.a("GioneeUtil", "isDualSim out, exception cache");
            return false;
        }
    }

    public static void a(Context context, Uri uri, int i) {
        if (!d) {
            a();
        }
        if (d()) {
            a.a("GioneeUtil", "setRingTone, hasAudioProfileMode");
            try {
                Class<?> cls = Class.forName("com.gionee.common.audioprofile.IAudioProfileService");
                String str = (String) cls.getMethod("getActiveProfileKey", new Class[0]).invoke(f(), new Object[0]);
                a.b("GioneeUtil", "setRingTone,strActiveProfileKey : " + str);
                Method method = cls.getMethod("setRingtoneUri", String.class, Integer.TYPE, Uri.class);
                if ((i & 1) != 0) {
                    a.a("GioneeUtil", "setRingTone, type:ringtone");
                    method.invoke(f(), str, 1, uri);
                }
                if ((i & 32) != 0) {
                    a.a("GioneeUtil", "setRingTone, type:ringtone2");
                    method.invoke(f(), str, Integer.valueOf(f2327a), uri);
                }
                if ((i & 64) != 0) {
                    a.a("GioneeUtil", "setRingTone, type:sms");
                    method.invoke(f(), str, Integer.valueOf(b), uri);
                }
                if ((i & 128) != 0) {
                    a.a("GioneeUtil", "setRingTone, type:sms2");
                    method.invoke(f(), str, Integer.valueOf(c), uri);
                }
                if ((i & 2) != 0) {
                    a.a("GioneeUtil", "setRingTone, type:notification");
                    method.invoke(f(), str, 2, uri);
                }
                if ((i & 4) != 0) {
                    a.a("GioneeUtil", "setRingTone, type:alarm");
                    RingtoneManager.setActualDefaultRingtoneUri(context, 4, uri);
                }
            } catch (ClassNotFoundException e2) {
                e2.printStackTrace();
                a.a(e2);
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
                a.a(e3);
            } catch (IllegalAccessException e4) {
                e4.printStackTrace();
                a.a(e4);
            } catch (InvocationTargetException e5) {
                e5.printStackTrace();
                a.a(e5);
            } catch (NoSuchMethodException e6) {
                e6.printStackTrace();
                a.a(e6);
            } catch (Exception e7) {
            }
            a.a("GioneeUtil", "setRingTone out");
            return;
        }
        a.a("GioneeUtil", "setRingTone, no audioProfileMode");
        if ((i & 1) != 0) {
            try {
                a.a("GioneeUtil", "setRingTone, type:ringtone");
                RingtoneManager.setActualDefaultRingtoneUri(context, 1, uri);
            } catch (IllegalArgumentException e8) {
                e8.printStackTrace();
                a.a(e8);
                return;
            }
        }
        if ((i & 32) != 0) {
            a.a("GioneeUtil", "setRingTone, type:ringtone2");
            RingtoneManager.setActualDefaultRingtoneUri(context, f2327a, uri);
        }
        if ((i & 64) != 0) {
            a.a("GioneeUtil", "setRingTone, type:sms");
            RingtoneManager.setActualDefaultRingtoneUri(context, b, uri);
        }
        if ((i & 128) != 0) {
            a.a("GioneeUtil", "setRingTone, type:sms2");
            RingtoneManager.setActualDefaultRingtoneUri(context, c, uri);
        }
        if ((i & 2) != 0) {
            a.a("GioneeUtil", "setRingTone, type:notification");
            RingtoneManager.setActualDefaultRingtoneUri(context, 2, uri);
        }
        if ((i & 4) != 0) {
            a.a("GioneeUtil", "setRingTone, type:alarm");
            RingtoneManager.setActualDefaultRingtoneUri(context, 4, uri);
        }
        a.a("GioneeUtil", "setRingTone out");
    }

    private static boolean d() {
        IBinder iBinder;
        try {
            Class<?> cls = Class.forName("android.os.ServiceManager");
            iBinder = (IBinder) cls.getMethod("getService", String.class).invoke(cls.newInstance(), "audioprofile_service");
            try {
                a.b("GioneeUtil", "isBinderAlive : " + iBinder.isBinderAlive());
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            iBinder = null;
        }
        a.b("GioneeUtil", "binder : " + iBinder);
        return iBinder != null;
    }

    private static IBinder e() {
        Exception e2;
        IBinder iBinder;
        try {
            Class<?> cls = Class.forName("android.os.ServiceManager");
            iBinder = (IBinder) cls.getMethod("getService", String.class).invoke(cls.newInstance(), "audioprofile_service");
            try {
                a.b("GioneeUtil", "isBinderAlive : " + iBinder.isBinderAlive());
            } catch (Exception e3) {
                e2 = e3;
                a.b("GioneeUtil", "e : " + e2);
                a.b("GioneeUtil", "binder : " + iBinder);
                return iBinder;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            iBinder = null;
            e2 = exc;
            a.b("GioneeUtil", "e : " + e2);
            a.b("GioneeUtil", "binder : " + iBinder);
            return iBinder;
        }
        a.b("GioneeUtil", "binder : " + iBinder);
        return iBinder;
    }

    private static Object f() {
        if (e != null) {
            return e;
        }
        try {
            Class<?> cls = Class.forName("com.gionee.common.audioprofile.IAudioProfileService");
            a.b("GioneeUtil", "c : " + cls);
            IBinder e2 = e();
            Class<?>[] declaredClasses = cls.getDeclaredClasses();
            for (int i = 0; i < declaredClasses.length; i++) {
                a.b("GioneeUtil", "element : " + declaredClasses[i]);
                a.b("GioneeUtil", "element getName : " + declaredClasses[i].getName());
                if (declaredClasses[i].getName().equals("com.gionee.common.audioprofile.IAudioProfileService$Stub")) {
                    Method method = declaredClasses[i].getMethod("asInterface", IBinder.class);
                    a.b("GioneeUtil", "o[i].newInstance() ");
                    e = method.invoke(declaredClasses[i], e2);
                    a.b("GioneeUtil", "sService : " + e);
                }
            }
        } catch (Exception e3) {
            a.b("GioneeUtil", "e : " + e3);
        }
        return e;
    }
}
