package com.a.a.b.a;

import com.a.a.d.a;
import com.a.a.d.b;
import com.a.a.g;
import com.a.a.k;
import com.a.a.l;
import com.a.a.n;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: JsonTreeReader */
public final class e extends a {
    private static final Reader b = new Reader() {
        public int read(char[] cArr, int i, int i2) throws IOException {
            throw new AssertionError();
        }

        public void close() throws IOException {
            throw new AssertionError();
        }
    };
    private static final Object c = new Object();
    private final List<Object> d;

    public void a() throws IOException {
        a(b.BEGIN_ARRAY);
        this.d.add(((g) u()).iterator());
    }

    public void b() throws IOException {
        a(b.END_ARRAY);
        v();
        v();
    }

    public void c() throws IOException {
        a(b.BEGIN_OBJECT);
        this.d.add(((l) u()).o().iterator());
    }

    public void d() throws IOException {
        a(b.END_OBJECT);
        v();
        v();
    }

    public boolean e() throws IOException {
        b f = f();
        return (f == b.END_OBJECT || f == b.END_ARRAY) ? false : true;
    }

    public b f() throws IOException {
        if (this.d.isEmpty()) {
            return b.END_DOCUMENT;
        }
        Object u = u();
        if (u instanceof Iterator) {
            boolean z = this.d.get(this.d.size() - 2) instanceof l;
            Iterator it = (Iterator) u;
            if (!it.hasNext()) {
                return z ? b.END_OBJECT : b.END_ARRAY;
            }
            if (z) {
                return b.NAME;
            }
            this.d.add(it.next());
            return f();
        } else if (u instanceof l) {
            return b.BEGIN_OBJECT;
        } else {
            if (u instanceof g) {
                return b.BEGIN_ARRAY;
            }
            if (u instanceof n) {
                n nVar = (n) u;
                if (nVar.q()) {
                    return b.STRING;
                }
                if (nVar.o()) {
                    return b.BOOLEAN;
                }
                if (nVar.p()) {
                    return b.NUMBER;
                }
                throw new AssertionError();
            } else if (u instanceof k) {
                return b.NULL;
            } else {
                if (u == c) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    private Object u() {
        return this.d.get(this.d.size() - 1);
    }

    private Object v() {
        return this.d.remove(this.d.size() - 1);
    }

    private void a(b bVar) throws IOException {
        if (f() != bVar) {
            throw new IllegalStateException("Expected " + bVar + " but was " + f());
        }
    }

    public String g() throws IOException {
        a(b.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) u()).next();
        this.d.add(entry.getValue());
        return (String) entry.getKey();
    }

    public String h() throws IOException {
        b f = f();
        if (f == b.STRING || f == b.NUMBER) {
            return ((n) v()).b();
        }
        throw new IllegalStateException("Expected " + b.STRING + " but was " + f);
    }

    public boolean i() throws IOException {
        a(b.BOOLEAN);
        return ((n) v()).f();
    }

    public void j() throws IOException {
        a(b.NULL);
        v();
    }

    public double k() throws IOException {
        b f = f();
        if (f == b.NUMBER || f == b.STRING) {
            double c2 = ((n) u()).c();
            if (p() || (!Double.isNaN(c2) && !Double.isInfinite(c2))) {
                v();
                return c2;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + c2);
        }
        throw new IllegalStateException("Expected " + b.NUMBER + " but was " + f);
    }

    public long l() throws IOException {
        b f = f();
        if (f == b.NUMBER || f == b.STRING) {
            long d2 = ((n) u()).d();
            v();
            return d2;
        }
        throw new IllegalStateException("Expected " + b.NUMBER + " but was " + f);
    }

    public int m() throws IOException {
        b f = f();
        if (f == b.NUMBER || f == b.STRING) {
            int e = ((n) u()).e();
            v();
            return e;
        }
        throw new IllegalStateException("Expected " + b.NUMBER + " but was " + f);
    }

    public void close() throws IOException {
        this.d.clear();
        this.d.add(c);
    }

    public void n() throws IOException {
        if (f() == b.NAME) {
            g();
        } else {
            v();
        }
    }

    public String toString() {
        return getClass().getSimpleName();
    }

    public void o() throws IOException {
        a(b.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) u()).next();
        this.d.add(entry.getValue());
        this.d.add(new n((String) entry.getKey()));
    }
}
