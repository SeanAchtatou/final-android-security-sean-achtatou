package com.a.a.b.a;

import com.a.a.b.c;
import com.a.a.b.h;
import com.a.a.d;
import com.a.a.e;
import com.a.a.p;
import com.a.a.r;
import com.a.a.s;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/* compiled from: ReflectiveTypeAdapterFactory */
public final class i implements s {

    /* renamed from: a  reason: collision with root package name */
    private final c f258a;
    private final d b;
    private final com.a.a.b.d c;

    public i(c cVar, d dVar, com.a.a.b.d dVar2) {
        this.f258a = cVar;
        this.b = dVar;
        this.c = dVar2;
    }

    public boolean a(Field field, boolean z) {
        return a(field, z, this.c);
    }

    static boolean a(Field field, boolean z, com.a.a.b.d dVar) {
        return !dVar.a(field.getType(), z) && !dVar.a(field, z);
    }

    private List<String> a(Field field) {
        return a(this.b, field);
    }

    static List<String> a(d dVar, Field field) {
        com.a.a.a.c cVar = (com.a.a.a.c) field.getAnnotation(com.a.a.a.c.class);
        LinkedList linkedList = new LinkedList();
        if (cVar == null) {
            linkedList.add(dVar.a(field));
        } else {
            linkedList.add(cVar.a());
            for (String add : cVar.b()) {
                linkedList.add(add);
            }
        }
        return linkedList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.a.i.a(com.a.a.e, com.a.a.c.a<?>, java.lang.Class<?>):java.util.Map<java.lang.String, com.a.a.b.a.i$b>
     arg types: [com.a.a.e, com.a.a.c.a<T>, java.lang.Class<? super T>]
     candidates:
      com.a.a.b.a.i.a(java.lang.reflect.Field, boolean, com.a.a.b.d):boolean
      com.a.a.b.a.i.a(com.a.a.e, java.lang.reflect.Field, com.a.a.c.a<?>):com.a.a.r<?>
      com.a.a.b.a.i.a(com.a.a.e, com.a.a.c.a<?>, java.lang.Class<?>):java.util.Map<java.lang.String, com.a.a.b.a.i$b> */
    public <T> r<T> a(e eVar, com.a.a.c.a<T> aVar) {
        Class<? super T> a2 = aVar.a();
        if (!Object.class.isAssignableFrom(a2)) {
            return null;
        }
        return new a(this.f258a.a(aVar), a(eVar, (com.a.a.c.a<?>) aVar, (Class<?>) a2));
    }

    private b a(e eVar, Field field, String str, com.a.a.c.a<?> aVar, boolean z, boolean z2) {
        final boolean a2 = com.a.a.b.i.a((Type) aVar.a());
        final e eVar2 = eVar;
        final Field field2 = field;
        final com.a.a.c.a<?> aVar2 = aVar;
        return new b(str, z, z2) {

            /* renamed from: a  reason: collision with root package name */
            final r<?> f259a = i.this.a(eVar2, field2, aVar2);

            /* access modifiers changed from: package-private */
            public void a(com.a.a.d.c cVar, Object obj) throws IOException, IllegalAccessException {
                new l(eVar2, this.f259a, aVar2.b()).a(cVar, field2.get(obj));
            }

            /* access modifiers changed from: package-private */
            public void a(com.a.a.d.a aVar, Object obj) throws IOException, IllegalAccessException {
                Object b2 = this.f259a.b(aVar);
                if (b2 != null || !a2) {
                    field2.set(obj, b2);
                }
            }

            public boolean a(Object obj) throws IOException, IllegalAccessException {
                if (this.h && field2.get(obj) != obj) {
                    return true;
                }
                return false;
            }
        };
    }

    /* access modifiers changed from: package-private */
    public r<?> a(e eVar, Field field, com.a.a.c.a<?> aVar) {
        r<?> a2;
        com.a.a.a.b bVar = (com.a.a.a.b) field.getAnnotation(com.a.a.a.b.class);
        return (bVar == null || (a2 = d.a(this.f258a, eVar, aVar, bVar)) == null) ? eVar.a((com.a.a.c.a) aVar) : a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.a.i.a(java.lang.reflect.Field, boolean):boolean
     arg types: [java.lang.reflect.Field, int]
     candidates:
      com.a.a.b.a.i.a(com.a.a.d, java.lang.reflect.Field):java.util.List<java.lang.String>
      com.a.a.b.a.i.a(com.a.a.e, com.a.a.c.a):com.a.a.r<T>
      com.a.a.s.a(com.a.a.e, com.a.a.c.a):com.a.a.r<T>
      com.a.a.b.a.i.a(java.lang.reflect.Field, boolean):boolean */
    private Map<String, b> a(e eVar, com.a.a.c.a<?> aVar, Class<?> cls) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (cls.isInterface()) {
            return linkedHashMap;
        }
        Type b2 = aVar.b();
        while (cls != Object.class) {
            for (Field field : cls.getDeclaredFields()) {
                boolean a2 = a(field, true);
                boolean a3 = a(field, false);
                if (a2 || a3) {
                    field.setAccessible(true);
                    Type a4 = com.a.a.b.b.a(aVar.b(), cls, field.getGenericType());
                    List<String> a5 = a(field);
                    b bVar = null;
                    int i = 0;
                    while (i < a5.size()) {
                        String str = a5.get(i);
                        if (i != 0) {
                            a2 = false;
                        }
                        b bVar2 = (b) linkedHashMap.put(str, a(eVar, field, str, com.a.a.c.a.a(a4), a2, a3));
                        if (bVar != null) {
                            bVar2 = bVar;
                        }
                        i++;
                        bVar = bVar2;
                    }
                    if (bVar != null) {
                        throw new IllegalArgumentException(b2 + " declares multiple JSON fields named " + bVar.g);
                    }
                }
            }
            aVar = com.a.a.c.a.a(com.a.a.b.b.a(aVar.b(), cls, cls.getGenericSuperclass()));
            cls = aVar.a();
        }
        return linkedHashMap;
    }

    /* compiled from: ReflectiveTypeAdapterFactory */
    static abstract class b {
        final String g;
        final boolean h;
        final boolean i;

        /* access modifiers changed from: package-private */
        public abstract void a(com.a.a.d.a aVar, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: package-private */
        public abstract void a(com.a.a.d.c cVar, Object obj) throws IOException, IllegalAccessException;

        /* access modifiers changed from: package-private */
        public abstract boolean a(Object obj) throws IOException, IllegalAccessException;

        protected b(String str, boolean z, boolean z2) {
            this.g = str;
            this.h = z;
            this.i = z2;
        }
    }

    /* compiled from: ReflectiveTypeAdapterFactory */
    public static final class a<T> extends r<T> {

        /* renamed from: a  reason: collision with root package name */
        private final h<T> f260a;
        private final Map<String, b> b;

        a(h<T> hVar, Map<String, b> map) {
            this.f260a = hVar;
            this.b = map;
        }

        public T b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == com.a.a.d.b.NULL) {
                aVar.j();
                return null;
            }
            T a2 = this.f260a.a();
            try {
                aVar.c();
                while (aVar.e()) {
                    b bVar = this.b.get(aVar.g());
                    if (bVar == null || !bVar.i) {
                        aVar.n();
                    } else {
                        bVar.a(aVar, a2);
                    }
                }
                aVar.d();
                return a2;
            } catch (IllegalStateException e) {
                throw new p(e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        public void a(com.a.a.d.c cVar, T t) throws IOException {
            if (t == null) {
                cVar.f();
                return;
            }
            cVar.d();
            try {
                for (b next : this.b.values()) {
                    if (next.a(t)) {
                        cVar.a(next.g);
                        next.a(cVar, t);
                    }
                }
                cVar.e();
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }
    }
}
