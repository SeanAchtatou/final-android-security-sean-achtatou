package com.a.a.b.a;

import com.a.a.c.a;
import com.a.a.d.b;
import com.a.a.e;
import com.a.a.p;
import com.a.a.r;
import com.a.a.s;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;
import java.util.Locale;

/* compiled from: DateTypeAdapter */
public final class c extends r<Date> {

    /* renamed from: a  reason: collision with root package name */
    public static final s f251a = new s() {
        public <T> r<T> a(e eVar, a<T> aVar) {
            if (aVar.a() == Date.class) {
                return new c();
            }
            return null;
        }
    };
    private final DateFormat b = DateFormat.getDateTimeInstance(2, 2, Locale.US);
    private final DateFormat c = DateFormat.getDateTimeInstance(2, 2);

    /* renamed from: a */
    public Date b(com.a.a.d.a aVar) throws IOException {
        if (aVar.f() != b.NULL) {
            return a(aVar.h());
        }
        aVar.j();
        return null;
    }

    private synchronized Date a(String str) {
        Date a2;
        try {
            a2 = this.c.parse(str);
        } catch (ParseException e) {
            try {
                a2 = this.b.parse(str);
            } catch (ParseException e2) {
                try {
                    a2 = com.a.a.b.a.a.a.a(str, new ParsePosition(0));
                } catch (ParseException e3) {
                    throw new p(str, e3);
                }
            }
        }
        return a2;
    }

    public synchronized void a(com.a.a.d.c cVar, Date date) throws IOException {
        if (date == null) {
            cVar.f();
        } else {
            cVar.b(this.b.format(date));
        }
    }
}
