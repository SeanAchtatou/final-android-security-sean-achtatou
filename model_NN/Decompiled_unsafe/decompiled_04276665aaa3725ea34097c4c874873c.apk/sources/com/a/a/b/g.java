package com.a.a.b;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/* compiled from: LinkedTreeMap */
public final class g<K, V> extends AbstractMap<K, V> implements Serializable {
    static final /* synthetic */ boolean f = (!g.class.desiredAssertionStatus());
    private static final Comparator<Comparable> g = new Comparator<Comparable>() {
        /* renamed from: a */
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }
    };

    /* renamed from: a  reason: collision with root package name */
    Comparator<? super K> f296a;
    d<K, V> b;
    int c;
    int d;
    final d<K, V> e;
    private g<K, V>.a h;
    private g<K, V>.b i;

    public g() {
        this(g);
    }

    public g(Comparator<? super K> comparator) {
        this.c = 0;
        this.d = 0;
        this.e = new d<>();
        this.f296a = comparator == null ? g : comparator;
    }

    public int size() {
        return this.c;
    }

    public V get(Object obj) {
        d a2 = a(obj);
        if (a2 != null) {
            return a2.g;
        }
        return null;
    }

    public boolean containsKey(Object obj) {
        return a(obj) != null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V>
     arg types: [K, int]
     candidates:
      com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void
      com.a.a.b.g.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.g.a(com.a.a.b.g$d, boolean):void
      com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V> */
    public V put(K k, V v) {
        if (k == null) {
            throw new NullPointerException("key == null");
        }
        d a2 = a((Object) k, true);
        V v2 = a2.g;
        a2.g = v;
        return v2;
    }

    public void clear() {
        this.b = null;
        this.c = 0;
        this.d++;
        d<K, V> dVar = this.e;
        dVar.e = dVar;
        dVar.d = dVar;
    }

    public V remove(Object obj) {
        d b2 = b(obj);
        if (b2 != null) {
            return b2.g;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public d<K, V> a(K k, boolean z) {
        d<K, V> dVar;
        int i2;
        d<K, V> dVar2;
        int compare;
        Comparator<? super K> comparator = this.f296a;
        d<K, V> dVar3 = this.b;
        if (dVar3 != null) {
            Comparable comparable = comparator == g ? (Comparable) k : null;
            while (true) {
                if (comparable != null) {
                    compare = comparable.compareTo(dVar3.f);
                } else {
                    compare = comparator.compare(k, dVar3.f);
                }
                if (compare == 0) {
                    return dVar3;
                }
                d<K, V> dVar4 = compare < 0 ? dVar3.b : dVar3.c;
                if (dVar4 == null) {
                    int i3 = compare;
                    dVar = dVar3;
                    i2 = i3;
                    break;
                }
                dVar3 = dVar4;
            }
        } else {
            dVar = dVar3;
            i2 = 0;
        }
        if (!z) {
            return null;
        }
        d<K, V> dVar5 = this.e;
        if (dVar != null) {
            dVar2 = new d<>(dVar, k, dVar5, dVar5.e);
            if (i2 < 0) {
                dVar.b = dVar2;
            } else {
                dVar.c = dVar2;
            }
            b(dVar, true);
        } else if (comparator != g || (k instanceof Comparable)) {
            dVar2 = new d<>(dVar, k, dVar5, dVar5.e);
            this.b = dVar2;
        } else {
            throw new ClassCastException(k.getClass().getName() + " is not Comparable");
        }
        this.c++;
        this.d++;
        return dVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V>
     arg types: [java.lang.Object, int]
     candidates:
      com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void
      com.a.a.b.g.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.g.a(com.a.a.b.g$d, boolean):void
      com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V> */
    /* access modifiers changed from: package-private */
    public d<K, V> a(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return a(obj, false);
        } catch (ClassCastException e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public d<K, V> a(Map.Entry<?, ?> entry) {
        d<K, V> a2 = a(entry.getKey());
        if (a2 != null && a(a2.g, entry.getValue())) {
            return a2;
        }
        return null;
    }

    private boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.g.a(com.a.a.b.g$d, boolean):void
     arg types: [com.a.a.b.g$d<K, V>, int]
     candidates:
      com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void
      com.a.a.b.g.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V>
      com.a.a.b.g.a(com.a.a.b.g$d, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void
     arg types: [com.a.a.b.g$d<K, V>, com.a.a.b.g$d<K, V>]
     candidates:
      com.a.a.b.g.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V>
      com.a.a.b.g.a(com.a.a.b.g$d, boolean):void
      com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void
     arg types: [com.a.a.b.g$d<K, V>, ?[OBJECT, ARRAY]]
     candidates:
      com.a.a.b.g.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V>
      com.a.a.b.g.a(com.a.a.b.g$d, boolean):void
      com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void */
    /* access modifiers changed from: package-private */
    public void a(d<K, V> dVar, boolean z) {
        int i2;
        int i3 = 0;
        if (z) {
            dVar.e.d = dVar.d;
            dVar.d.e = dVar.e;
        }
        d<K, V> dVar2 = dVar.b;
        d<K, V> dVar3 = dVar.c;
        d<K, V> dVar4 = dVar.f301a;
        if (dVar2 == null || dVar3 == null) {
            if (dVar2 != null) {
                a((d) dVar, (d) dVar2);
                dVar.b = null;
            } else if (dVar3 != null) {
                a((d) dVar, (d) dVar3);
                dVar.c = null;
            } else {
                a((d) dVar, (d) null);
            }
            b(dVar4, false);
            this.c--;
            this.d++;
            return;
        }
        d<K, V> b2 = dVar2.h > dVar3.h ? dVar2.b() : dVar3.a();
        a((d) b2, false);
        d<K, V> dVar5 = dVar.b;
        if (dVar5 != null) {
            i2 = dVar5.h;
            b2.b = dVar5;
            dVar5.f301a = b2;
            dVar.b = null;
        } else {
            i2 = 0;
        }
        d<K, V> dVar6 = dVar.c;
        if (dVar6 != null) {
            i3 = dVar6.h;
            b2.c = dVar6;
            dVar6.f301a = b2;
            dVar.c = null;
        }
        b2.h = Math.max(i2, i3) + 1;
        a((d) dVar, (d) b2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.g.a(com.a.a.b.g$d, boolean):void
     arg types: [com.a.a.b.g$d<K, V>, int]
     candidates:
      com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void
      com.a.a.b.g.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V>
      com.a.a.b.g.a(com.a.a.b.g$d, boolean):void */
    /* access modifiers changed from: package-private */
    public d<K, V> b(Object obj) {
        d<K, V> a2 = a(obj);
        if (a2 != null) {
            a((d) a2, true);
        }
        return a2;
    }

    private void a(d<K, V> dVar, d<K, V> dVar2) {
        d<K, V> dVar3 = dVar.f301a;
        dVar.f301a = null;
        if (dVar2 != null) {
            dVar2.f301a = dVar3;
        }
        if (dVar3 == null) {
            this.b = dVar2;
        } else if (dVar3.b == dVar) {
            dVar3.b = dVar2;
        } else if (f || dVar3.c == dVar) {
            dVar3.c = dVar2;
        } else {
            throw new AssertionError();
        }
    }

    private void b(d<K, V> dVar, boolean z) {
        int i2;
        int i3;
        int i4;
        int i5;
        while (dVar != null) {
            d<K, V> dVar2 = dVar.b;
            d<K, V> dVar3 = dVar.c;
            int i6 = dVar2 != null ? dVar2.h : 0;
            if (dVar3 != null) {
                i2 = dVar3.h;
            } else {
                i2 = 0;
            }
            int i7 = i6 - i2;
            if (i7 == -2) {
                d<K, V> dVar4 = dVar3.b;
                d<K, V> dVar5 = dVar3.c;
                if (dVar5 != null) {
                    i4 = dVar5.h;
                } else {
                    i4 = 0;
                }
                if (dVar4 != null) {
                    i5 = dVar4.h;
                } else {
                    i5 = 0;
                }
                int i8 = i5 - i4;
                if (i8 == -1 || (i8 == 0 && !z)) {
                    a((d) dVar);
                } else if (f || i8 == 1) {
                    b((d) dVar3);
                    a((d) dVar);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i7 == 2) {
                d<K, V> dVar6 = dVar2.b;
                d<K, V> dVar7 = dVar2.c;
                int i9 = dVar7 != null ? dVar7.h : 0;
                if (dVar6 != null) {
                    i3 = dVar6.h;
                } else {
                    i3 = 0;
                }
                int i10 = i3 - i9;
                if (i10 == 1 || (i10 == 0 && !z)) {
                    b((d) dVar);
                } else if (f || i10 == -1) {
                    a((d) dVar2);
                    b((d) dVar);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i7 == 0) {
                dVar.h = i6 + 1;
                if (z) {
                    return;
                }
            } else if (f || i7 == -1 || i7 == 1) {
                dVar.h = Math.max(i6, i2) + 1;
                if (!z) {
                    return;
                }
            } else {
                throw new AssertionError();
            }
            dVar = dVar.f301a;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void
     arg types: [com.a.a.b.g$d<K, V>, com.a.a.b.g$d<K, V>]
     candidates:
      com.a.a.b.g.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V>
      com.a.a.b.g.a(com.a.a.b.g$d, boolean):void
      com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void */
    private void a(d<K, V> dVar) {
        int i2;
        int i3 = 0;
        d<K, V> dVar2 = dVar.b;
        d<K, V> dVar3 = dVar.c;
        d<K, V> dVar4 = dVar3.b;
        d<K, V> dVar5 = dVar3.c;
        dVar.c = dVar4;
        if (dVar4 != null) {
            dVar4.f301a = dVar;
        }
        a((d) dVar, (d) dVar3);
        dVar3.b = dVar;
        dVar.f301a = dVar3;
        if (dVar2 != null) {
            i2 = dVar2.h;
        } else {
            i2 = 0;
        }
        dVar.h = Math.max(i2, dVar4 != null ? dVar4.h : 0) + 1;
        int i4 = dVar.h;
        if (dVar5 != null) {
            i3 = dVar5.h;
        }
        dVar3.h = Math.max(i4, i3) + 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void
     arg types: [com.a.a.b.g$d<K, V>, com.a.a.b.g$d<K, V>]
     candidates:
      com.a.a.b.g.a(java.lang.Object, java.lang.Object):boolean
      com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V>
      com.a.a.b.g.a(com.a.a.b.g$d, boolean):void
      com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void */
    private void b(d<K, V> dVar) {
        int i2;
        int i3 = 0;
        d<K, V> dVar2 = dVar.b;
        d<K, V> dVar3 = dVar.c;
        d<K, V> dVar4 = dVar2.b;
        d<K, V> dVar5 = dVar2.c;
        dVar.b = dVar5;
        if (dVar5 != null) {
            dVar5.f301a = dVar;
        }
        a((d) dVar, (d) dVar2);
        dVar2.c = dVar;
        dVar.f301a = dVar2;
        if (dVar3 != null) {
            i2 = dVar3.h;
        } else {
            i2 = 0;
        }
        dVar.h = Math.max(i2, dVar5 != null ? dVar5.h : 0) + 1;
        int i4 = dVar.h;
        if (dVar4 != null) {
            i3 = dVar4.h;
        }
        dVar2.h = Math.max(i4, i3) + 1;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        g<K, V>.a aVar = this.h;
        if (aVar != null) {
            return aVar;
        }
        g<K, V>.a aVar2 = new a();
        this.h = aVar2;
        return aVar2;
    }

    public Set<K> keySet() {
        g<K, V>.b bVar = this.i;
        if (bVar != null) {
            return bVar;
        }
        g<K, V>.b bVar2 = new b();
        this.i = bVar2;
        return bVar2;
    }

    /* compiled from: LinkedTreeMap */
    static final class d<K, V> implements Map.Entry<K, V> {

        /* renamed from: a  reason: collision with root package name */
        d<K, V> f301a;
        d<K, V> b;
        d<K, V> c;
        d<K, V> d;
        d<K, V> e;
        final K f;
        V g;
        int h;

        d() {
            this.f = null;
            this.e = this;
            this.d = this;
        }

        d(d<K, V> dVar, K k, d<K, V> dVar2, d<K, V> dVar3) {
            this.f301a = dVar;
            this.f = k;
            this.h = 1;
            this.d = dVar2;
            this.e = dVar3;
            dVar3.d = this;
            dVar2.e = this;
        }

        public K getKey() {
            return this.f;
        }

        public V getValue() {
            return this.g;
        }

        public V setValue(V v) {
            V v2 = this.g;
            this.g = v;
            return v2;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x001b A[ORIG_RETURN, RETURN, SYNTHETIC] */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                r0 = 0
                boolean r1 = r4 instanceof java.util.Map.Entry
                if (r1 == 0) goto L_0x001c
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                K r1 = r3.f
                if (r1 != 0) goto L_0x001d
                java.lang.Object r1 = r4.getKey()
                if (r1 != 0) goto L_0x001c
            L_0x0011:
                V r1 = r3.g
                if (r1 != 0) goto L_0x002a
                java.lang.Object r1 = r4.getValue()
                if (r1 != 0) goto L_0x001c
            L_0x001b:
                r0 = 1
            L_0x001c:
                return r0
            L_0x001d:
                K r1 = r3.f
                java.lang.Object r2 = r4.getKey()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x0011
            L_0x002a:
                V r1 = r3.g
                java.lang.Object r2 = r4.getValue()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x001b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.a.a.b.g.d.equals(java.lang.Object):boolean");
        }

        public int hashCode() {
            int i = 0;
            int hashCode = this.f == null ? 0 : this.f.hashCode();
            if (this.g != null) {
                i = this.g.hashCode();
            }
            return hashCode ^ i;
        }

        public String toString() {
            return ((Object) this.f) + "=" + ((Object) this.g);
        }

        public d<K, V> a() {
            for (d<K, V> dVar = this.b; dVar != null; dVar = dVar.b) {
                this = dVar;
            }
            return this;
        }

        public d<K, V> b() {
            for (d<K, V> dVar = this.c; dVar != null; dVar = dVar.c) {
                this = dVar;
            }
            return this;
        }
    }

    /* compiled from: LinkedTreeMap */
    private abstract class c<T> implements Iterator<T> {
        d<K, V> b = g.this.e.d;
        d<K, V> c = null;
        int d = g.this.d;

        c() {
        }

        public final boolean hasNext() {
            return this.b != g.this.e;
        }

        /* access modifiers changed from: package-private */
        public final d<K, V> b() {
            d<K, V> dVar = this.b;
            if (dVar == g.this.e) {
                throw new NoSuchElementException();
            } else if (g.this.d != this.d) {
                throw new ConcurrentModificationException();
            } else {
                this.b = dVar.d;
                this.c = dVar;
                return dVar;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.a.a.b.g.a(com.a.a.b.g$d, boolean):void
         arg types: [com.a.a.b.g$d<K, V>, int]
         candidates:
          com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void
          com.a.a.b.g.a(java.lang.Object, java.lang.Object):boolean
          com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V>
          com.a.a.b.g.a(com.a.a.b.g$d, boolean):void */
        public final void remove() {
            if (this.c == null) {
                throw new IllegalStateException();
            }
            g.this.a((d) this.c, true);
            this.c = null;
            this.d = g.this.d;
        }
    }

    /* compiled from: LinkedTreeMap */
    class a extends AbstractSet<Map.Entry<K, V>> {
        a() {
        }

        public int size() {
            return g.this.c;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new g<K, V>.c<Map.Entry<K, V>>() {
                {
                    g gVar = g.this;
                }

                /* renamed from: a */
                public Map.Entry<K, V> next() {
                    return b();
                }
            };
        }

        public boolean contains(Object obj) {
            return (obj instanceof Map.Entry) && g.this.a((Map.Entry) obj) != null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.a.a.b.g.a(com.a.a.b.g$d, boolean):void
         arg types: [com.a.a.b.g$d, int]
         candidates:
          com.a.a.b.g.a(com.a.a.b.g$d, com.a.a.b.g$d):void
          com.a.a.b.g.a(java.lang.Object, java.lang.Object):boolean
          com.a.a.b.g.a(java.lang.Object, boolean):com.a.a.b.g$d<K, V>
          com.a.a.b.g.a(com.a.a.b.g$d, boolean):void */
        public boolean remove(Object obj) {
            d a2;
            if (!(obj instanceof Map.Entry) || (a2 = g.this.a((Map.Entry<?, ?>) ((Map.Entry) obj))) == null) {
                return false;
            }
            g.this.a(a2, true);
            return true;
        }

        public void clear() {
            g.this.clear();
        }
    }

    /* compiled from: LinkedTreeMap */
    final class b extends AbstractSet<K> {
        b() {
        }

        public int size() {
            return g.this.c;
        }

        public Iterator<K> iterator() {
            return new g<K, V>.c<K>() {
                {
                    g gVar = g.this;
                }

                public K next() {
                    return b().f;
                }
            };
        }

        public boolean contains(Object obj) {
            return g.this.containsKey(obj);
        }

        public boolean remove(Object obj) {
            return g.this.b(obj) != null;
        }

        public void clear() {
            g.this.clear();
        }
    }
}
