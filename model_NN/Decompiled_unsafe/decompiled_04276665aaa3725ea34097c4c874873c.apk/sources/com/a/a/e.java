package com.a.a;

import com.a.a.b.a.b;
import com.a.a.b.a.g;
import com.a.a.b.a.h;
import com.a.a.b.a.i;
import com.a.a.b.a.j;
import com.a.a.b.a.k;
import com.a.a.b.a.m;
import com.a.a.b.c;
import com.a.a.b.d;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicLongArray;

/* compiled from: Gson */
public final class e {

    /* renamed from: a  reason: collision with root package name */
    final h f313a;
    final o b;
    private final ThreadLocal<Map<com.a.a.c.a<?>, a<?>>> c;
    private final Map<com.a.a.c.a<?>, r<?>> d;
    private final List<s> e;
    private final c f;
    private final boolean g;
    private final boolean h;
    private final boolean i;
    private final boolean j;
    private final boolean k;

    public e() {
        this(d.f292a, c.IDENTITY, Collections.emptyMap(), false, false, false, true, false, false, false, q.DEFAULT, Collections.emptyList());
    }

    e(d dVar, d dVar2, Map<Type, f<?>> map, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, q qVar, List<s> list) {
        this.c = new ThreadLocal<>();
        this.d = Collections.synchronizedMap(new HashMap());
        this.f313a = new h() {
        };
        this.b = new o() {
        };
        this.f = new c(map);
        this.g = z;
        this.i = z3;
        this.h = z4;
        this.j = z5;
        this.k = z6;
        ArrayList arrayList = new ArrayList();
        arrayList.add(m.Y);
        arrayList.add(h.f256a);
        arrayList.add(dVar);
        arrayList.addAll(list);
        arrayList.add(m.D);
        arrayList.add(m.m);
        arrayList.add(m.g);
        arrayList.add(m.i);
        arrayList.add(m.k);
        r<Number> a2 = a(qVar);
        arrayList.add(m.a(Long.TYPE, Long.class, a2));
        arrayList.add(m.a(Double.TYPE, Double.class, a(z7)));
        arrayList.add(m.a(Float.TYPE, Float.class, b(z7)));
        arrayList.add(m.x);
        arrayList.add(m.o);
        arrayList.add(m.q);
        arrayList.add(m.a(AtomicLong.class, a(a2)));
        arrayList.add(m.a(AtomicLongArray.class, b(a2)));
        arrayList.add(m.s);
        arrayList.add(m.z);
        arrayList.add(m.F);
        arrayList.add(m.H);
        arrayList.add(m.a(BigDecimal.class, m.B));
        arrayList.add(m.a(BigInteger.class, m.C));
        arrayList.add(m.J);
        arrayList.add(m.L);
        arrayList.add(m.P);
        arrayList.add(m.R);
        arrayList.add(m.W);
        arrayList.add(m.N);
        arrayList.add(m.d);
        arrayList.add(com.a.a.b.a.c.f251a);
        arrayList.add(m.U);
        arrayList.add(k.f262a);
        arrayList.add(j.f261a);
        arrayList.add(m.S);
        arrayList.add(com.a.a.b.a.a.f247a);
        arrayList.add(m.b);
        arrayList.add(new b(this.f));
        arrayList.add(new g(this.f, z2));
        arrayList.add(new com.a.a.b.a.d(this.f));
        arrayList.add(m.Z);
        arrayList.add(new i(this.f, dVar2, dVar));
        this.e = Collections.unmodifiableList(arrayList);
    }

    private r<Number> a(boolean z) {
        if (z) {
            return m.v;
        }
        return new r<Number>() {
            /* renamed from: a */
            public Double b(com.a.a.d.a aVar) throws IOException {
                if (aVar.f() != com.a.a.d.b.NULL) {
                    return Double.valueOf(aVar.k());
                }
                aVar.j();
                return null;
            }

            public void a(com.a.a.d.c cVar, Number number) throws IOException {
                if (number == null) {
                    cVar.f();
                    return;
                }
                e.a(number.doubleValue());
                cVar.a(number);
            }
        };
    }

    private r<Number> b(boolean z) {
        if (z) {
            return m.u;
        }
        return new r<Number>() {
            /* renamed from: a */
            public Float b(com.a.a.d.a aVar) throws IOException {
                if (aVar.f() != com.a.a.d.b.NULL) {
                    return Float.valueOf((float) aVar.k());
                }
                aVar.j();
                return null;
            }

            public void a(com.a.a.d.c cVar, Number number) throws IOException {
                if (number == null) {
                    cVar.f();
                    return;
                }
                e.a((double) number.floatValue());
                cVar.a(number);
            }
        };
    }

    static void a(double d2) {
        if (Double.isNaN(d2) || Double.isInfinite(d2)) {
            throw new IllegalArgumentException(d2 + " is not a valid double value as per JSON specification. To override this" + " behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
        }
    }

    private static r<Number> a(q qVar) {
        if (qVar == q.DEFAULT) {
            return m.t;
        }
        return new r<Number>() {
            /* renamed from: a */
            public Number b(com.a.a.d.a aVar) throws IOException {
                if (aVar.f() != com.a.a.d.b.NULL) {
                    return Long.valueOf(aVar.l());
                }
                aVar.j();
                return null;
            }

            public void a(com.a.a.d.c cVar, Number number) throws IOException {
                if (number == null) {
                    cVar.f();
                } else {
                    cVar.b(number.toString());
                }
            }
        };
    }

    private static r<AtomicLong> a(final r<Number> rVar) {
        return new r<AtomicLong>() {
            public void a(com.a.a.d.c cVar, AtomicLong atomicLong) throws IOException {
                rVar.a(cVar, Long.valueOf(atomicLong.get()));
            }

            /* renamed from: a */
            public AtomicLong b(com.a.a.d.a aVar) throws IOException {
                return new AtomicLong(((Number) rVar.b(aVar)).longValue());
            }
        }.a();
    }

    private static r<AtomicLongArray> b(final r<Number> rVar) {
        return new r<AtomicLongArray>() {
            public void a(com.a.a.d.c cVar, AtomicLongArray atomicLongArray) throws IOException {
                cVar.b();
                int length = atomicLongArray.length();
                for (int i = 0; i < length; i++) {
                    rVar.a(cVar, Long.valueOf(atomicLongArray.get(i)));
                }
                cVar.c();
            }

            /* renamed from: a */
            public AtomicLongArray b(com.a.a.d.a aVar) throws IOException {
                ArrayList arrayList = new ArrayList();
                aVar.a();
                while (aVar.e()) {
                    arrayList.add(Long.valueOf(((Number) rVar.b(aVar)).longValue()));
                }
                aVar.b();
                int size = arrayList.size();
                AtomicLongArray atomicLongArray = new AtomicLongArray(size);
                for (int i = 0; i < size; i++) {
                    atomicLongArray.set(i, ((Long) arrayList.get(i)).longValue());
                }
                return atomicLongArray;
            }
        }.a();
    }

    public <T> r<T> a(com.a.a.c.a aVar) {
        HashMap hashMap;
        r<T> rVar = this.d.get(aVar);
        if (rVar == null) {
            Map map = this.c.get();
            boolean z = false;
            if (map == null) {
                HashMap hashMap2 = new HashMap();
                this.c.set(hashMap2);
                hashMap = hashMap2;
                z = true;
            } else {
                hashMap = map;
            }
            rVar = (a) hashMap.get(aVar);
            if (rVar == null) {
                try {
                    a aVar2 = new a();
                    hashMap.put(aVar, aVar2);
                    for (s a2 : this.e) {
                        rVar = a2.a(this, aVar);
                        if (rVar != null) {
                            aVar2.a((r) rVar);
                            this.d.put(aVar, rVar);
                            hashMap.remove(aVar);
                            if (z) {
                                this.c.remove();
                            }
                        }
                    }
                    throw new IllegalArgumentException("GSON cannot handle " + aVar);
                } catch (Throwable th) {
                    hashMap.remove(aVar);
                    if (z) {
                        this.c.remove();
                    }
                    throw th;
                }
            }
        }
        return rVar;
    }

    public <T> r<T> a(s sVar, com.a.a.c.a<T> aVar) {
        boolean z = false;
        if (!this.e.contains(sVar)) {
            z = true;
        }
        boolean z2 = z;
        for (s next : this.e) {
            if (z2) {
                r<T> a2 = next.a(this, aVar);
                if (a2 != null) {
                    return a2;
                }
            } else if (next == sVar) {
                z2 = true;
            }
        }
        throw new IllegalArgumentException("GSON cannot serialize " + aVar);
    }

    public <T> r<T> a(Class cls) {
        return a(com.a.a.c.a.b(cls));
    }

    public String a(Object obj) {
        if (obj == null) {
            return a((i) k.f322a);
        }
        return a(obj, obj.getClass());
    }

    public String a(Object obj, Type type) {
        StringWriter stringWriter = new StringWriter();
        a(obj, type, stringWriter);
        return stringWriter.toString();
    }

    public void a(Object obj, Type type, Appendable appendable) throws j {
        try {
            a(obj, type, a(com.a.a.b.j.a(appendable)));
        } catch (IOException e2) {
            throw new j(e2);
        }
    }

    public void a(Object obj, Type type, com.a.a.d.c cVar) throws j {
        r a2 = a((com.a.a.c.a) com.a.a.c.a.a(type));
        boolean g2 = cVar.g();
        cVar.b(true);
        boolean h2 = cVar.h();
        cVar.c(this.h);
        boolean i2 = cVar.i();
        cVar.d(this.g);
        try {
            a2.a(cVar, obj);
            cVar.b(g2);
            cVar.c(h2);
            cVar.d(i2);
        } catch (IOException e2) {
            throw new j(e2);
        } catch (Throwable th) {
            cVar.b(g2);
            cVar.c(h2);
            cVar.d(i2);
            throw th;
        }
    }

    public String a(i iVar) {
        StringWriter stringWriter = new StringWriter();
        a(iVar, stringWriter);
        return stringWriter.toString();
    }

    public void a(i iVar, Appendable appendable) throws j {
        try {
            a(iVar, a(com.a.a.b.j.a(appendable)));
        } catch (IOException e2) {
            throw new RuntimeException(e2);
        }
    }

    public com.a.a.d.c a(Writer writer) throws IOException {
        if (this.i) {
            writer.write(")]}'\n");
        }
        com.a.a.d.c cVar = new com.a.a.d.c(writer);
        if (this.j) {
            cVar.c("  ");
        }
        cVar.d(this.g);
        return cVar;
    }

    public com.a.a.d.a a(Reader reader) {
        com.a.a.d.a aVar = new com.a.a.d.a(reader);
        aVar.a(this.k);
        return aVar;
    }

    public void a(i iVar, com.a.a.d.c cVar) throws j {
        boolean g2 = cVar.g();
        cVar.b(true);
        boolean h2 = cVar.h();
        cVar.c(this.h);
        boolean i2 = cVar.i();
        cVar.d(this.g);
        try {
            com.a.a.b.j.a(iVar, cVar);
            cVar.b(g2);
            cVar.c(h2);
            cVar.d(i2);
        } catch (IOException e2) {
            throw new j(e2);
        } catch (Throwable th) {
            cVar.b(g2);
            cVar.c(h2);
            cVar.d(i2);
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.e.a(java.lang.String, java.lang.reflect.Type):T
     arg types: [java.lang.String, java.lang.Class<T>]
     candidates:
      com.a.a.e.a(java.lang.Object, com.a.a.d.a):void
      com.a.a.e.a(com.a.a.s, com.a.a.c.a):com.a.a.r<T>
      com.a.a.e.a(com.a.a.d.a, java.lang.reflect.Type):T
      com.a.a.e.a(java.io.Reader, java.lang.reflect.Type):T
      com.a.a.e.a(java.lang.String, java.lang.Class):T
      com.a.a.e.a(java.lang.Object, java.lang.reflect.Type):java.lang.String
      com.a.a.e.a(com.a.a.i, com.a.a.d.c):void
      com.a.a.e.a(com.a.a.i, java.lang.Appendable):void
      com.a.a.e.a(java.lang.String, java.lang.reflect.Type):T */
    public <T> T a(String str, Class<T> cls) throws p {
        return com.a.a.b.i.a((Class) cls).cast(a(str, (Type) cls));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.e.a(java.io.Reader, java.lang.reflect.Type):T
     arg types: [java.io.StringReader, java.lang.reflect.Type]
     candidates:
      com.a.a.e.a(java.lang.Object, com.a.a.d.a):void
      com.a.a.e.a(com.a.a.s, com.a.a.c.a):com.a.a.r<T>
      com.a.a.e.a(com.a.a.d.a, java.lang.reflect.Type):T
      com.a.a.e.a(java.lang.String, java.lang.Class):T
      com.a.a.e.a(java.lang.String, java.lang.reflect.Type):T
      com.a.a.e.a(java.lang.Object, java.lang.reflect.Type):java.lang.String
      com.a.a.e.a(com.a.a.i, com.a.a.d.c):void
      com.a.a.e.a(com.a.a.i, java.lang.Appendable):void
      com.a.a.e.a(java.io.Reader, java.lang.reflect.Type):T */
    public <T> T a(String str, Type type) throws p {
        if (str == null) {
            return null;
        }
        return a((Reader) new StringReader(str), type);
    }

    public <T> T a(Reader reader, Type type) throws j, p {
        com.a.a.d.a a2 = a(reader);
        T a3 = a(a2, type);
        a(a3, a2);
        return a3;
    }

    private static void a(Object obj, com.a.a.d.a aVar) {
        if (obj != null) {
            try {
                if (aVar.f() != com.a.a.d.b.END_DOCUMENT) {
                    throw new j("JSON document was not fully consumed.");
                }
            } catch (com.a.a.d.d e2) {
                throw new p(e2);
            } catch (IOException e3) {
                throw new j(e3);
            }
        }
    }

    public <T> T a(com.a.a.d.a aVar, Type type) throws j, p {
        boolean z = true;
        boolean p = aVar.p();
        aVar.a(true);
        try {
            aVar.f();
            z = false;
            T b2 = a((com.a.a.c.a) com.a.a.c.a.a(type)).b(aVar);
            aVar.a(p);
            return b2;
        } catch (EOFException e2) {
            if (z) {
                aVar.a(p);
                return null;
            }
            throw new p(e2);
        } catch (IllegalStateException e3) {
            throw new p(e3);
        } catch (IOException e4) {
            throw new p(e4);
        } catch (Throwable th) {
            aVar.a(p);
            throw th;
        }
    }

    /* compiled from: Gson */
    static class a<T> extends r<T> {

        /* renamed from: a  reason: collision with root package name */
        private r<T> f320a;

        a() {
        }

        public void a(r<T> rVar) {
            if (this.f320a != null) {
                throw new AssertionError();
            }
            this.f320a = rVar;
        }

        public T b(com.a.a.d.a aVar) throws IOException {
            if (this.f320a != null) {
                return this.f320a.b(aVar);
            }
            throw new IllegalStateException();
        }

        public void a(com.a.a.d.c cVar, T t) throws IOException {
            if (this.f320a == null) {
                throw new IllegalStateException();
            }
            this.f320a.a(cVar, t);
        }
    }

    public String toString() {
        return "{serializeNulls:" + this.g + "factories:" + this.e + ",instanceCreators:" + this.f + "}";
    }
}
