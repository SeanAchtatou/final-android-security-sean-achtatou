package com.a.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: JsonArray */
public final class g extends i implements Iterable<i> {

    /* renamed from: a  reason: collision with root package name */
    private final List<i> f321a = new ArrayList();

    public void a(i iVar) {
        if (iVar == null) {
            iVar = k.f322a;
        }
        this.f321a.add(iVar);
    }

    public Iterator<i> iterator() {
        return this.f321a.iterator();
    }

    public Number a() {
        if (this.f321a.size() == 1) {
            return this.f321a.get(0).a();
        }
        throw new IllegalStateException();
    }

    public String b() {
        if (this.f321a.size() == 1) {
            return this.f321a.get(0).b();
        }
        throw new IllegalStateException();
    }

    public double c() {
        if (this.f321a.size() == 1) {
            return this.f321a.get(0).c();
        }
        throw new IllegalStateException();
    }

    public long d() {
        if (this.f321a.size() == 1) {
            return this.f321a.get(0).d();
        }
        throw new IllegalStateException();
    }

    public int e() {
        if (this.f321a.size() == 1) {
            return this.f321a.get(0).e();
        }
        throw new IllegalStateException();
    }

    public boolean f() {
        if (this.f321a.size() == 1) {
            return this.f321a.get(0).f();
        }
        throw new IllegalStateException();
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof g) && ((g) obj).f321a.equals(this.f321a));
    }

    public int hashCode() {
        return this.f321a.hashCode();
    }
}
