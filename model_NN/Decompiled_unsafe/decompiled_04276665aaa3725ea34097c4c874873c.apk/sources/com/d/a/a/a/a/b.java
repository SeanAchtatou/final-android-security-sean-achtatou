package com.d.a.a.a.a;

import com.d.a.a.a.a;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* compiled from: LimitedAgeDiscCache */
public class b extends a {
    private final long b;
    private final Map<File, Long> c = Collections.synchronizedMap(new HashMap());

    public b(File file, com.d.a.a.a.b.a aVar, long j) {
        super(file, aVar);
        this.b = 1000 * j;
    }

    public void a(String str, File file) {
        long currentTimeMillis = System.currentTimeMillis();
        file.setLastModified(currentTimeMillis);
        this.c.put(file, Long.valueOf(currentTimeMillis));
    }

    public File a(String str) {
        boolean z;
        File a2 = super.a(str);
        if (a2.exists()) {
            Long l = this.c.get(a2);
            if (l == null) {
                z = false;
                l = Long.valueOf(a2.lastModified());
            } else {
                z = true;
            }
            if (System.currentTimeMillis() - l.longValue() > this.b) {
                a2.delete();
                this.c.remove(a2);
            } else if (!z) {
                this.c.put(a2, l);
            }
        }
        return a2;
    }
}
