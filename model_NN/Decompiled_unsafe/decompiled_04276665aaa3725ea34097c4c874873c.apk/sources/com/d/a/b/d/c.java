package com.d.a.b.d;

import com.d.a.b.d.b;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: NetworkDeniedImageDownloader */
public class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private final b f706a;

    public c(b bVar) {
        this.f706a = bVar;
    }

    public InputStream a(String str, Object obj) throws IOException {
        switch (b.a.a(str)) {
            case HTTP:
            case HTTPS:
                throw new IllegalStateException();
        }
        return this.f706a.a(str, obj);
    }
}
