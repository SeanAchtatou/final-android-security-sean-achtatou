package com.d.a.b.a;

import android.graphics.Bitmap;
import android.view.View;

/* compiled from: ImageLoadingListener */
public interface e {
    void onLoadingCancelled(String str, View view);

    void onLoadingComplete(String str, View view, Bitmap bitmap);

    void onLoadingFailed(String str, View view, c cVar);

    void onLoadingStarted(String str, View view);
}
