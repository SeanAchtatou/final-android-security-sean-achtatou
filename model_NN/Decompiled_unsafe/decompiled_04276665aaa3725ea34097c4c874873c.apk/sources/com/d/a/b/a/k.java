package com.d.a.b.a;

import android.widget.AbsListView;
import com.d.a.b.d;

/* compiled from: PauseOnScrollListener */
public class k implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private d f688a;
    private final boolean b;
    private final boolean c;
    private final AbsListView.OnScrollListener d;

    public k(d dVar, boolean z, boolean z2) {
        this(dVar, z, z2, null);
    }

    public k(d dVar, boolean z, boolean z2, AbsListView.OnScrollListener onScrollListener) {
        this.f688a = dVar;
        this.b = z;
        this.c = z2;
        this.d = onScrollListener;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        switch (i) {
            case 0:
                this.f688a.f();
                break;
            case 1:
                if (this.b) {
                    this.f688a.e();
                    break;
                }
                break;
            case 2:
                if (this.c) {
                    this.f688a.e();
                    break;
                }
                break;
        }
        if (this.d != null) {
            this.d.onScrollStateChanged(absListView, i);
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.d != null) {
            this.d.onScroll(absListView, i, i2, i3);
        }
    }
}
