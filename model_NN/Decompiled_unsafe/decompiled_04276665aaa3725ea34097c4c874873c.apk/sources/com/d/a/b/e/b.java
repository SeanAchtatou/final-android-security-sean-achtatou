package com.d.a.b.e;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import com.d.a.b.a.h;
import com.d.a.b.a.n;

/* compiled from: ImageNonViewAware */
public class b implements a {

    /* renamed from: a  reason: collision with root package name */
    protected final String f712a;
    protected final h b;
    protected final n c;

    public b(String str, h hVar, n nVar) {
        this.f712a = str;
        this.b = hVar;
        this.c = nVar;
    }

    public int a() {
        return this.b.a();
    }

    public int b() {
        return this.b.b();
    }

    public n c() {
        return this.c;
    }

    public View d() {
        return null;
    }

    public boolean e() {
        return false;
    }

    public int f() {
        return TextUtils.isEmpty(this.f712a) ? super.hashCode() : this.f712a.hashCode();
    }

    public boolean a(Drawable drawable) {
        return true;
    }

    public boolean a(Bitmap bitmap) {
        return true;
    }
}
