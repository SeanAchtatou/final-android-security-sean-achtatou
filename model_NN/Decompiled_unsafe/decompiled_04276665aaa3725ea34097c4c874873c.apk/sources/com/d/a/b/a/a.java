package com.d.a.b.a;

import java.io.IOException;
import java.io.InputStream;

/* compiled from: ContentLengthInputStream */
public class a extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final InputStream f679a;
    private final long b;
    private long c;

    public a(InputStream inputStream, long j) {
        this.f679a = inputStream;
        this.b = j;
    }

    public synchronized int available() {
        return (int) (this.b - this.c);
    }

    public void close() throws IOException {
        this.f679a.close();
    }

    public void mark(int i) {
        this.c = (long) i;
        this.f679a.mark(i);
    }

    public int read() throws IOException {
        this.c++;
        return this.f679a.read();
    }

    public int read(byte[] bArr) throws IOException {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        this.c += (long) i2;
        return this.f679a.read(bArr, i, i2);
    }

    public synchronized void reset() throws IOException {
        this.c = 0;
        this.f679a.reset();
    }

    public long skip(long j) throws IOException {
        this.c += j;
        return this.f679a.skip(j);
    }
}
