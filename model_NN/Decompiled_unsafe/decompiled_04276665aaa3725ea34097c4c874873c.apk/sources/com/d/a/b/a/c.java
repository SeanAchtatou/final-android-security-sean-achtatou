package com.d.a.b.a;

/* compiled from: FailReason */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private final a f683a;
    private final Throwable b;

    /* compiled from: FailReason */
    public enum a {
        IO_ERROR,
        DECODING_ERROR,
        NETWORK_DENIED,
        OUT_OF_MEMORY,
        UNKNOWN
    }

    public c(a aVar, Throwable th) {
        this.f683a = aVar;
        this.b = th;
    }
}
