package com.d.a.b.e;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.d.a.b.a.n;

/* compiled from: ImageAware */
public interface a {
    int a();

    boolean a(Bitmap bitmap);

    boolean a(Drawable drawable);

    int b();

    n c();

    View d();

    boolean e();

    int f();
}
