package com.ffcs.inapppaylib.bean.response;

import java.io.Serializable;

public class EmpResponse implements Serializable {
    String res_code;
    String res_message;

    public String getRes_code() {
        return this.res_code;
    }

    public void setRes_code(String str) {
        this.res_code = str;
    }

    public String getRes_message() {
        return this.res_message;
    }

    public void setRes_message(String str) {
        this.res_message = str;
    }
}
