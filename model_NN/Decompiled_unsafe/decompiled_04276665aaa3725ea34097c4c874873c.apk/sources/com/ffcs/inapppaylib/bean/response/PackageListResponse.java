package com.ffcs.inapppaylib.bean.response;

import java.util.List;

public class PackageListResponse extends EmpResponse {
    private List<UserPackage> user_package_list;

    public List<UserPackage> getUser_package_list() {
        return this.user_package_list;
    }

    public void setUser_package_list(List<UserPackage> list) {
        this.user_package_list = list;
    }
}
