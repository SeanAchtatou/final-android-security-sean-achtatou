package com.qhad.ads.sdk.adcore;

import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhVideoAdListener;
import com.qhad.ads.sdk.log.QHADLog;
import java.util.ArrayList;
import java.util.List;

class QhVideoAdListenerProxy implements DynamicObject {
    private final IQhVideoAdListener listener;

    public QhVideoAdListenerProxy(IQhVideoAdListener iQhVideoAdListener) {
        this.listener = iQhVideoAdListener;
    }

    public Object invoke(int i, Object obj) {
        switch (i) {
            case 54:
                QHADLog.d("ADSUPDATE", "QHVIDEOADLISTENER_onVideoAdLoadSucceeded");
                ArrayList arrayList = new ArrayList();
                for (DynamicObject qhVideoAdProxy : (List) obj) {
                    arrayList.add(new QhVideoAdProxy(qhVideoAdProxy));
                }
                this.listener.onVideoAdLoadSucceeded(arrayList);
                return null;
            case 55:
                QHADLog.d("ADSUPDATE", "QHVIDEOADLISTENER_onVideoAdLoadFailed");
                this.listener.onVideoAdLoadFailed();
                return null;
            default:
                return null;
        }
    }
}
