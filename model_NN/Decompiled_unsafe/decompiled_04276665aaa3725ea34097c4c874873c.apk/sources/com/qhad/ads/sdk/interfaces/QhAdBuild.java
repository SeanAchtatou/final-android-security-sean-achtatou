package com.qhad.ads.sdk.interfaces;

public interface QhAdBuild {
    public static final int API_LEVEL = 2;
    public static final String SDK_VER = "1.12";
}
