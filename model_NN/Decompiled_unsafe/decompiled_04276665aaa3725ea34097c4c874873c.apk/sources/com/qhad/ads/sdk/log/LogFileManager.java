package com.qhad.ads.sdk.log;

import android.content.Context;
import com.qhad.ads.sdk.adcore.Config;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.http.util.EncodingUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class LogFileManager {
    private static String getLogPath() {
        File file = new File(Utils.getCacheDir(), Config.ERROR_LOG_FILE_NAME);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                QHADLog.e("create log file error:" + e.getMessage());
                return null;
            }
        }
        return Utils.getCacheDir() + "/" + Config.ERROR_LOG_FILE_NAME;
    }

    public static ArrayList<JSONObject> getAllLogs() {
        ArrayList<JSONObject> arrayList = new ArrayList<>();
        try {
            String logPath = getLogPath();
            if (logPath != null) {
                FileInputStream fileInputStream = new FileInputStream(logPath);
                int available = fileInputStream.available();
                if (available > 0) {
                    byte[] bArr = new byte[available];
                    fileInputStream.read(bArr);
                    for (String jSONObject : EncodingUtils.getString(bArr, "UTF-8").split("\n")) {
                        arrayList.add(new JSONObject(jSONObject));
                    }
                }
                fileInputStream.close();
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return arrayList;
    }

    public static synchronized void saveLog(HashMap<String, String> hashMap) {
        ArrayList<JSONObject> arrayList;
        synchronized (LogFileManager.class) {
            if (getLogPath() != null) {
                ArrayList<JSONObject> allLogs = getAllLogs();
                int size = allLogs.size();
                if (size >= 50) {
                    arrayList = new ArrayList<>(allLogs.subList(size - 20, size));
                } else {
                    arrayList = allLogs;
                }
                arrayList.add(new JSONObject(hashMap));
                int size2 = arrayList.size();
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < size2; i++) {
                    sb.append(arrayList.get(i).toString());
                    if (i != size2 - 1) {
                        sb.append("\n");
                    }
                }
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(getLogPath());
                    fileOutputStream.write(sb.toString().getBytes());
                    fileOutputStream.close();
                } catch (Exception e) {
                    QHADLog.e(e.getMessage());
                }
            }
        }
        return;
    }

    private static void clearLogs() {
        String logPath = getLogPath();
        if (logPath != null) {
            new File(logPath).delete();
        }
    }

    public static void uploadAllLogs(Context context) {
        ArrayList<JSONObject> allLogs = getAllLogs();
        clearLogs();
        int size = allLogs.size();
        for (int i = 0; i < size; i++) {
            HashMap hashMap = new HashMap();
            JSONObject jSONObject = allLogs.get(i);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                try {
                    hashMap.put(next, jSONObject.getString(next));
                } catch (JSONException e) {
                    QHADLog.e("Read Logs Error:" + e.getMessage());
                }
            }
            LogUploader.postLog(hashMap, context, false);
        }
    }
}
