package com.qhad.ads.sdk.interfaces;

public interface IQhProductAdAttributes extends IQhAdAttributes {
    void setCategory(String str, int i);

    void setPrice(double d);
}
