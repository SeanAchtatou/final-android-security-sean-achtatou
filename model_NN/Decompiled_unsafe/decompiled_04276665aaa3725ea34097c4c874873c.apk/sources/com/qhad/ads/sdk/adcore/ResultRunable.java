package com.qhad.ads.sdk.adcore;

import android.os.Message;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.qhad.ads.sdk.adcore.HttpRequester;
import java.util.HashMap;

/* compiled from: HttpRequester */
class ResultRunable implements Runnable {
    private static final String UNKOWN_ERROR = "unkown error";
    private Message msg = null;

    public ResultRunable(Message message) {
        this.msg = message;
    }

    public void run() {
        try {
            HashMap hashMap = (HashMap) this.msg.obj;
            HttpRequester.Listener listener = (HttpRequester.Listener) hashMap.get("callback");
            if (this.msg.what == 0) {
                listener.onGetDataSucceed((byte[]) hashMap.get("data"));
            } else if (this.msg.what == 1) {
                listener.onGetDataFailed((String) hashMap.get(SocketMessage.MSG_ERROR_KEY));
            } else if (this.msg.what == 2) {
                listener.onGetDataFailed((String) hashMap.get(SocketMessage.MSG_ERROR_KEY));
            } else {
                listener.onGetDataFailed(UNKOWN_ERROR);
            }
        } catch (Exception e) {
            ((HttpRequester.Listener) ((HashMap) this.msg.obj).get("callback")).onGetDataFailed("HttpRequester get data error:" + e.getMessage());
        }
    }
}
