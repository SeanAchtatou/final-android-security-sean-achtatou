package com.qhad.ads.sdk.interfaces;

public interface IQhLandingPageListener {
    boolean onAppDownload(String str);

    void onPageClose();

    void onPageLoadFailed();

    void onPageLoadFinished();
}
