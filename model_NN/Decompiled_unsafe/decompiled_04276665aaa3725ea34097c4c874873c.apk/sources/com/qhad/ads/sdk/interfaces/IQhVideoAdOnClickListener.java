package com.qhad.ads.sdk.interfaces;

public interface IQhVideoAdOnClickListener {
    void onDownloadCancelled();

    void onDownloadConfirmed();

    void onLandingpageClosed();

    void onLandingpageOpened();
}
