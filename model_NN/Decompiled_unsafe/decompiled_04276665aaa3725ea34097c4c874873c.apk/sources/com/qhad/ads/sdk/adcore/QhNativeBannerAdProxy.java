package com.qhad.ads.sdk.adcore;

import android.app.Activity;
import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhAdEventListener;
import com.qhad.ads.sdk.interfaces.IQhNativeBannerAd;
import com.qhad.ads.sdk.log.QHADLog;

class QhNativeBannerAdProxy implements IQhNativeBannerAd {
    private final DynamicObject dynamicObject;

    public QhNativeBannerAdProxy(DynamicObject dynamicObject2) {
        this.dynamicObject = dynamicObject2;
    }

    public void closeAds() {
        QHADLog.d("ADSUPDATE", "QHNATIVEBANNERAD_closeAds");
        this.dynamicObject.invoke(61, null);
    }

    public void showAds(Activity activity) {
        QHADLog.d("ADSUPDATE", "QHNATIVEBANNERAD_showAds");
        this.dynamicObject.invoke(62, activity);
    }

    public void setAdEventListener(Object obj) {
        QHADLog.d("ADSUPDATE", "QHNATIVEBANNERAD_setAdEventListener");
        this.dynamicObject.invoke(63, new QhAdEventListenerProxy((IQhAdEventListener) obj));
    }
}
