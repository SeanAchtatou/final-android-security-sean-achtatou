package com.qhad.ads.sdk.adcore;

import android.content.Context;
import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhLandingPageView;
import com.qhad.ads.sdk.log.QHADLog;

class QhLandingPageViewProxy implements DynamicObject {
    private final IQhLandingPageView landingPageView;

    public QhLandingPageViewProxy(IQhLandingPageView iQhLandingPageView) {
        this.landingPageView = iQhLandingPageView;
    }

    public Object invoke(int i, Object obj) {
        switch (i) {
            case 47:
                QHADLog.d("ADSUPDATE", "QHLANDINGPAGEVIEW_open");
                Object[] objArr = (Object[]) obj;
                this.landingPageView.open((Context) objArr[0], (String) objArr[1], new QhLandingPageListenerProxy((DynamicObject) objArr[2]));
                return null;
            default:
                return null;
        }
    }
}
