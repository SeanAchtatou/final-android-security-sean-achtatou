package com.qhad.ads.sdk.interfaces;

import android.app.Activity;

public interface IQhInterstitialAd {
    void closeAds();

    void setAdEventListener(Object obj);

    void showAds(Activity activity);
}
