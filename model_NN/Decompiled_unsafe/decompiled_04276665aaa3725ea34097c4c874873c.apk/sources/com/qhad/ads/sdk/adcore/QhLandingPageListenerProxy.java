package com.qhad.ads.sdk.adcore;

import com.qhad.ads.sdk.interfaces.DynamicObject;
import com.qhad.ads.sdk.interfaces.IQhLandingPageListener;
import com.qhad.ads.sdk.log.QHADLog;

class QhLandingPageListenerProxy implements IQhLandingPageListener {
    private final DynamicObject dynamicObject;

    public QhLandingPageListenerProxy(DynamicObject dynamicObject2) {
        this.dynamicObject = dynamicObject2;
    }

    public void onPageClose() {
        QHADLog.d("ADSUPDATE", "QHLANDINGPAGELISTENER_onPageClose");
        this.dynamicObject.invoke(48, null);
    }

    public void onPageLoadFinished() {
        QHADLog.d("ADSUPDATE", "QHLANDINGPAGELISTENER_onPageLoadFinished");
        this.dynamicObject.invoke(49, null);
    }

    public void onPageLoadFailed() {
        QHADLog.d("ADSUPDATE", "QHLANDINGPAGELISTENER_onPageLoadFailed");
        this.dynamicObject.invoke(50, null);
    }

    public boolean onAppDownload(String str) {
        QHADLog.d("ADSUPDATE", "QHLANDINGPAGELISTENER_onAppDownload");
        return ((Boolean) this.dynamicObject.invoke(59, str)).booleanValue();
    }
}
