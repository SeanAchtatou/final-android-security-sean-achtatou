package com.qhad.ads.sdk.attibutes;

import com.qhad.ads.sdk.interfaces.IQhProductAdAttributes;
import java.util.HashMap;

public class QhProductAdAttributes implements IQhProductAdAttributes {
    private HashMap<String, String> productInfo = new HashMap<>();

    public HashMap<String, String> getAttributes() {
        return this.productInfo;
    }

    public void setCategory(String str, int i) {
        this.productInfo.put("qhcn", str);
        this.productInfo.put("qhtid", i + "");
    }

    public void setPrice(double d) {
        this.productInfo.put("price", d + "");
    }
}
