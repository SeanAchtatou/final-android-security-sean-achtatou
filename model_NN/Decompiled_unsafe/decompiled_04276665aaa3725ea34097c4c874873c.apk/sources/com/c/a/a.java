package com.c.a;

import com.c.a.b.a.b;
import com.c.a.b.b.b;
import com.c.a.b.b.c;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.HttpVersion;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

/* compiled from: HttpUtils */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final com.c.a.b.a f630a = new com.c.a.b.a();
    private static final ThreadFactory g = new ThreadFactory() {

        /* renamed from: a  reason: collision with root package name */
        private final AtomicInteger f631a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "HttpUtils #" + this.f631a.getAndIncrement());
            thread.setPriority(4);
            return thread;
        }
    };
    private static int h = 3;
    private static Executor i = Executors.newFixedThreadPool(h, g);
    private final DefaultHttpClient b;
    private final HttpContext c;
    private b d;
    private String e;
    private long f;

    public a() {
        this(15000);
    }

    public a(int i2) {
        this.c = new BasicHttpContext();
        this.e = "UTF-8";
        this.f = com.c.a.b.a.a();
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        ConnManagerParams.setTimeout(basicHttpParams, (long) i2);
        HttpConnectionParams.setSoTimeout(basicHttpParams, i2);
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, i2);
        ConnManagerParams.setMaxConnectionsPerRoute(basicHttpParams, new ConnPerRouteBean(10));
        ConnManagerParams.setMaxTotalConnections(basicHttpParams, 10);
        HttpConnectionParams.setTcpNoDelay(basicHttpParams, true);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, 8192);
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", com.c.a.b.b.a.a(), 443));
        this.b = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
        this.b.setHttpRequestRetryHandler(new c(5));
        this.b.addRequestInterceptor(new HttpRequestInterceptor() {
            public void process(HttpRequest httpRequest, HttpContext httpContext) throws HttpException, IOException {
                if (!httpRequest.containsHeader("Accept-Encoding")) {
                    httpRequest.addHeader("Accept-Encoding", "gzip");
                }
            }
        });
        this.b.addResponseInterceptor(new HttpResponseInterceptor() {
            public void process(HttpResponse httpResponse, HttpContext httpContext) throws HttpException, IOException {
                Header contentEncoding;
                HttpEntity entity = httpResponse.getEntity();
                if (entity != null && (contentEncoding = entity.getContentEncoding()) != null) {
                    HeaderElement[] elements = contentEncoding.getElements();
                    int length = elements.length;
                    int i = 0;
                    while (i < length && !elements[i].getName().equalsIgnoreCase("gzip")) {
                        i++;
                    }
                }
            }
        });
    }

    public <T> com.c.a.b.b<T> a(b.a aVar, String str, com.c.a.b.c cVar, com.c.a.b.a.c<T> cVar2) {
        if (str != null) {
            return a(new com.c.a.b.b.b(aVar, str), cVar, cVar2);
        }
        throw new IllegalArgumentException("url may not be null");
    }

    private <T> com.c.a.b.b<T> a(com.c.a.b.b.b bVar, com.c.a.b.c cVar, com.c.a.b.a.c<T> cVar2) {
        com.c.a.b.b<T> bVar2 = new com.c.a.b.b<>(this.b, this.c, this.e, cVar2);
        bVar2.a(this.f);
        bVar2.a(this.d);
        bVar.a(cVar, bVar2);
        bVar2.a(i, bVar);
        return bVar2;
    }
}
