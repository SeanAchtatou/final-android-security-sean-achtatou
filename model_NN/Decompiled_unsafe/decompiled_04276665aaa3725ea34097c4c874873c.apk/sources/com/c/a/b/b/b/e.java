package com.c.a.b.b.b;

/* compiled from: MinimalField */
class e {

    /* renamed from: a  reason: collision with root package name */
    private final String f648a;
    private final String b;

    e(String str, String str2) {
        this.f648a = str;
        this.b = str2;
    }

    public String a() {
        return this.f648a;
    }

    public String b() {
        return this.b;
    }

    public String toString() {
        return this.f648a + ": " + this.b;
    }
}
