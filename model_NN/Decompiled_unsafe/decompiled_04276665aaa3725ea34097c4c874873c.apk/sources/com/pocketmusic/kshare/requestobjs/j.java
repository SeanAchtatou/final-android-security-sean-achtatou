package com.pocketmusic.kshare.requestobjs;

import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.room.message.SocketMessage;
import java.util.HashMap;
import org.json.JSONObject;

/* compiled from: LiveConfig */
public class j extends n {

    /* renamed from: a  reason: collision with root package name */
    public String f1172a = "";
    public int b = 0;
    public String c = "";
    public String d = "";
    public String e = "0";
    public int f = 0;
    public int g = 2;
    public long h = 2000;

    public j() {
    }

    public j(String str) {
        this.c = str;
    }

    public void a() {
        KURL kurl = new KURL();
        HashMap hashMap = new HashMap();
        kurl.baseURL = s.a(APIKey.APIKey_LiveServer);
        hashMap.put("rid", this.c);
        kurl.getParameter.putAll(hashMap);
        a(kurl, this.ar, APIKey.APIKey_LiveServer);
    }

    public void a(JSONObject jSONObject) {
        JSONObject optJSONObject;
        if (jSONObject != null && !b(jSONObject) && (optJSONObject = jSONObject.optJSONObject(SocketMessage.MSG_RESULE_KEY)) != null) {
            this.f1172a = optJSONObject.optString("addr", "");
            this.b = optJSONObject.optInt("port", 0);
            this.d = optJSONObject.optString("livesig", "");
        }
    }
}
