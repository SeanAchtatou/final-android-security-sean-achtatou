package com.pocketmusic.kshare.requestobjs;

import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;
import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.utils.ULog;
import com.android.volley.toolbox.j;
import org.json.JSONObject;

/* compiled from: RequestObj */
public class n {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f1176a = getClass().getSimpleName();
    public int ao = NotificationManagerCompat.IMPORTANCE_UNSPECIFIED;
    public d ap = null;
    public APIKey aq = null;
    public r ar = null;
    private r b = new r() {
        public void onResponse(JSONObject jSONObject) {
            ULog.d(n.this.f1176a, "ret: " + jSONObject.toString());
        }
    };

    public int i() {
        return this.ao;
    }

    public void b(int i) {
        this.ao = i;
    }

    public d j() {
        return this.ap;
    }

    public void a(d dVar) {
        this.ap = dVar;
    }

    public boolean b(JSONObject jSONObject) {
        b((int) NotificationManagerCompat.IMPORTANCE_UNSPECIFIED);
        a((d) null);
        if (jSONObject == null || !jSONObject.has(SocketMessage.MSG_ERROR_KEY) || !jSONObject.has("code")) {
            return false;
        }
        b(jSONObject.optInt("code", NotificationManagerCompat.IMPORTANCE_UNSPECIFIED));
        String optString = jSONObject.optString("errmsg", "");
        if (!TextUtils.isEmpty(optString)) {
            a(new d(i(), optString));
        }
        return true;
    }

    public void a(r rVar) {
        this.ar = rVar;
    }

    public void a(KURL kurl, r rVar, APIKey aPIKey) {
        a(kurl, rVar, aPIKey, false);
    }

    public void a(KURL kurl, r rVar, APIKey aPIKey, boolean z) {
        if (kurl != null) {
            if (rVar == null) {
                rVar = this.b;
            }
            j jVar = new j(kurl.urlEncode(), null, rVar, rVar);
            if (aPIKey == null) {
                aPIKey = APIKey.APIKey_Default;
            }
            this.aq = aPIKey;
            ULog.d(this.f1176a, "apikey: " + aPIKey);
            ULog.d(this.f1176a, "url: " + kurl);
            jVar.a(z);
            try {
                KShareApplication.getInstance().addToRequestQueue(jVar, aPIKey.getKey());
            } catch (ACException e) {
                e.printStackTrace();
            }
        }
    }
}
