package com.pocketmusic.kshare.requestobjs;

import android.text.TextUtils;
import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.room.message.User;
import com.pocketmusic.kshare.requestobjs.g;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: Room */
public class o extends n implements Serializable {
    private static /* synthetic */ int[] T;
    public boolean A = false;
    public String B = "";
    public String C = "";
    public String D = "";
    public String E = "";
    public List<String> F = null;
    public List<String> G = null;
    public int H = 0;
    public int I = 0;
    public int J = 0;
    public String K = "";
    public String L = "";
    public String M = "";
    public String N = "0";
    public long O = 0;
    public List<e> P = null;
    public String Q = "";
    public a R = a.Normal;
    public b S = b.Audio;

    /* renamed from: a  reason: collision with root package name */
    public String f1178a = "";
    public String b = "";
    public String c = "";
    public String d = "";
    public String e = "";
    public String f = "";
    public int g = 1;
    public boolean h = false;
    public int i = 0;
    public boolean j = false;
    public int k = 0;
    public int l = 100;
    public User m = null;
    public User n = null;
    public boolean o = false;
    public int p = 1;
    public b q = null;
    public g.a r = null;
    public int s = 5;
    public long t = 0;
    public long u = 0;
    public c v = c.Default;
    public String w = "";
    public boolean x = false;
    public boolean y = false;
    public boolean z = false;

    /* compiled from: Room */
    public enum c {
        Default,
        InfoItem
    }

    static /* synthetic */ int[] c() {
        int[] iArr = T;
        if (iArr == null) {
            iArr = new int[c.values().length];
            try {
                iArr[c.Default.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[c.InfoItem.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            T = iArr;
        }
        return iArr;
    }

    /* compiled from: Room */
    public enum a {
        Normal("user"),
        Match("match"),
        Show("show");
        
        private String d;

        private a(String str) {
            this.d = str;
        }

        public String a() {
            return this.d;
        }

        public static a a(String str) {
            if (!TextUtils.isEmpty(str)) {
                for (a aVar : values()) {
                    if (str.equalsIgnoreCase(aVar.a())) {
                        return aVar;
                    }
                }
            }
            return Normal;
        }
    }

    /* compiled from: Room */
    public enum b {
        Audio("audio"),
        Video("video"),
        Multi_Mic("multi_mic");
        
        private String d;

        private b(String str) {
            this.d = str;
        }

        public String a() {
            return this.d;
        }

        public static b a(String str) {
            if (!TextUtils.isEmpty(str)) {
                for (b bVar : values()) {
                    if (str.equalsIgnoreCase(bVar.a())) {
                        return bVar;
                    }
                }
            }
            return Audio;
        }
    }

    public void a(o oVar) {
        if (oVar != null) {
            this.k = oVar.k;
            this.j = oVar.j;
        }
    }

    public void a() {
        KURL kurl = new KURL();
        HashMap hashMap = new HashMap();
        switch (c()[this.v.ordinal()]) {
            case 2:
                break;
            default:
                kurl.baseURL = s.a(APIKey.APIKey_Room_Info);
                hashMap.put("cmd", "getroominfo");
                hashMap.put("rid", this.f1178a);
                kurl.getParameter.putAll(hashMap);
                a(kurl, this.ar, APIKey.APIKey_Room_Info);
                break;
        }
        switch (c()[this.v.ordinal()]) {
        }
    }

    public String toString() {
        return "rid: " + this.f1178a + "; create: " + this.c + "; name: " + this.b + "; mClass: " + a.a(this.R);
    }

    public boolean b() {
        if (this.q == null || !this.q.a()) {
            return false;
        }
        return true;
    }

    public boolean a(String str) {
        if (!TextUtils.isEmpty(str) && this.F != null && this.F.indexOf(str) >= 0) {
            return true;
        }
        return false;
    }

    public boolean b(String str) {
        if (!TextUtils.isEmpty(str) && this.G != null && this.G.indexOf(str) >= 0) {
            return true;
        }
        return false;
    }

    public void a(JSONObject jSONObject) {
        if (jSONObject != null && !b(jSONObject)) {
            c(jSONObject.optJSONObject(SocketMessage.MSG_RESULE_KEY));
        }
    }

    public void c(JSONObject jSONObject) {
        boolean z2;
        JSONObject optJSONObject;
        JSONArray optJSONArray;
        JSONObject optJSONObject2;
        if (jSONObject != null) {
            this.f1178a = jSONObject.optString("rid", "");
            this.b = jSONObject.optString("roomname", "");
            if (jSONObject.has(SelectCountryActivity.EXTRA_COUNTRY_NAME)) {
                this.b = jSONObject.optString(SelectCountryActivity.EXTRA_COUNTRY_NAME, "");
            }
            this.c = jSONObject.optString("creator", "");
            this.g = jSONObject.optInt("allowmic", 1);
            this.i = jSONObject.optInt("allowvideo", 0);
            this.h = jSONObject.optString("needpwd", "0").equalsIgnoreCase("1");
            if (jSONObject.optString("isonline", "0").equalsIgnoreCase("1")) {
                z2 = true;
            } else {
                z2 = false;
            }
            this.j = z2;
            this.l = jSONObject.optInt("maxuser", 0);
            this.k = jSONObject.optInt("usercount", 0);
            this.e = jSONObject.optString("img_path_s", "");
            this.f = jSONObject.optString("img_path_m", "");
            this.d = jSONObject.optString("password", "");
            this.o = jSONObject.optBoolean("new", false);
            JSONObject optJSONObject3 = jSONObject.optJSONObject("anchor");
            if (optJSONObject3 != null && optJSONObject3.length() > 0) {
                this.n = new User();
                this.n.parseUser(optJSONObject3);
            }
            JSONObject optJSONObject4 = jSONObject.optJSONObject("owner");
            if (optJSONObject4 != null && optJSONObject4.length() > 0) {
                this.m = new User();
                this.m.parseUser(optJSONObject4);
                if (this.m != null) {
                    this.c = this.m.mUid;
                }
            }
            if (this.m != null && TextUtils.isEmpty(this.m.mNickname)) {
                this.m.mNickname = jSONObject.optString("nickname", "");
            }
            this.s = jSONObject.optInt("heartbeat", 5);
            this.d = jSONObject.optString("password");
            this.w = jSONObject.optString("welcome", this.w);
            this.H = jSONObject.optInt("talk_freq", 0);
            this.J = jSONObject.optInt("level_restrict", 0);
            this.I = jSONObject.optInt("freegift_freq", 0);
            if (jSONObject.has("type")) {
                this.R = a.a(jSONObject.optString("type", ""));
            } else {
                this.R = a.a(jSONObject.optString("roomtype", ""));
            }
            this.S = b.a(jSONObject.optString("onmic_media_type", ""));
            if (jSONObject.has("privilege") && (optJSONObject2 = jSONObject.optJSONObject("privilege")) != null) {
                JSONObject optJSONObject5 = optJSONObject2.optJSONObject("user");
                if (optJSONObject5 != null) {
                    this.x = optJSONObject5.optBoolean("allow", true);
                    this.D = optJSONObject5.optString("msg", "");
                }
                JSONObject optJSONObject6 = optJSONObject2.optJSONObject("show");
                if (optJSONObject6 != null) {
                    this.z = optJSONObject6.optBoolean("allow", true);
                    this.B = optJSONObject6.optString("msg", "");
                }
                JSONObject optJSONObject7 = optJSONObject2.optJSONObject("match");
                if (optJSONObject7 != null) {
                    this.y = optJSONObject7.optBoolean("allow", true);
                    this.C = optJSONObject7.optString("msg", "");
                }
                JSONObject optJSONObject8 = optJSONObject2.optJSONObject("videoenable");
                if (optJSONObject8 != null) {
                    this.A = optJSONObject8.optBoolean("allow", true);
                    this.E = optJSONObject8.optString("msg", "");
                }
            }
            if (jSONObject.optInt("fid", -1) > 0) {
                if (this.q != null) {
                    this.q = null;
                }
                this.q = new b();
                this.q.a(jSONObject, false);
            }
            if (jSONObject.has("admins")) {
                if (this.F != null) {
                    this.F.clear();
                } else {
                    this.F = new ArrayList();
                }
                JSONArray optJSONArray2 = jSONObject.optJSONArray("admins");
                if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                    for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                        String optString = optJSONArray2.optString(i2, "");
                        if (!TextUtils.isEmpty(optString)) {
                            this.F.add(optString);
                        }
                    }
                }
            }
            if (jSONObject.has("vips")) {
                if (this.G == null) {
                    this.G = new ArrayList();
                }
                this.G.clear();
                JSONArray optJSONArray3 = jSONObject.optJSONArray("vips");
                if (optJSONArray3 != null && optJSONArray3.length() > 0) {
                    for (int i3 = 0; i3 < optJSONArray3.length(); i3++) {
                        String optString2 = optJSONArray3.optString(i3, "");
                        if (!TextUtils.isEmpty(optString2)) {
                            this.G.add(optString2);
                        }
                    }
                }
            }
            if (jSONObject.has("extents") && (optJSONObject = jSONObject.optJSONObject("extents")) != null && optJSONObject.has("hanhua") && (optJSONArray = optJSONObject.optJSONArray("hanhua")) != null && optJSONArray.length() > 0) {
                if (this.P == null) {
                    this.P = new ArrayList();
                }
                this.P.clear();
                for (int i4 = 0; i4 < optJSONArray.length(); i4++) {
                    JSONObject optJSONObject9 = optJSONArray.optJSONObject(i4);
                    if (optJSONObject9 != null && optJSONObject9.has("gid")) {
                        e eVar = new e();
                        eVar.a(optJSONObject9);
                        this.P.add(eVar);
                    }
                }
            }
            this.N = jSONObject.optString("micnt", "0");
        }
    }
}
