package com.pocketmusic.kshare.requestobjs;

import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.io.Serializable;
import org.json.JSONObject;

/* compiled from: Club */
public class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public String f1155a = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
    public String b = "";
    public String c = "";
    public String d = "";
    public String e = "0";
    public o f = null;
    public m g = null;
    public c h = null;
    public String i = "";
    public boolean j = false;
    public String k = "";
    public String l = "";
    public String m = "";
    public C0032b n = C0032b.Normal;
    public a o = a.NO_GET;
    public String p = "";
    public String q;

    /* renamed from: com.pocketmusic.kshare.requestobjs.b$b  reason: collision with other inner class name */
    /* compiled from: Club */
    public enum C0032b {
        Normal,
        MyClub,
        Info
    }

    /* compiled from: Club */
    public enum a {
        Normal("no"),
        Member("member"),
        Apply("apply"),
        NO_GET("__no__get__"),
        HAS_FAMILY("hasfamily");
        
        private String f = "";

        private a(String str) {
            this.f = str;
        }
    }

    public String toString() {
        if (this.f == null) {
            return "fid: " + this.f1155a + "; fname: " + this.b;
        }
        return "fid: " + this.f1155a + "; fname: " + this.b + this.f.toString();
    }

    public boolean a() {
        try {
            if (Integer.parseInt(this.f1155a) > 0) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a(JSONObject jSONObject, boolean z) {
        if (jSONObject != null) {
            if (this.f != null) {
                this.f = null;
            }
            if (z) {
                this.f = new o();
                this.f.c(jSONObject);
            }
            this.f1155a = jSONObject.optString("fid", this.f1155a);
            this.q = jSONObject.optString("rid", this.q);
            this.b = jSONObject.optString("family_name", this.b);
            this.d = jSONObject.optString("qqgroup", this.d);
            this.e = jSONObject.optString("population", "0");
            this.c = jSONObject.optString("family_owner", WeiboAuthException.DEFAULT_AUTH_ERROR_CODE);
        }
    }

    public void a(JSONObject jSONObject) {
        a(jSONObject, true);
    }
}
