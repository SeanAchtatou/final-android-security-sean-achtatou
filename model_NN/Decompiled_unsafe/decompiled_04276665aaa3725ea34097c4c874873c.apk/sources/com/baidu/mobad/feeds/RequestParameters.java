package com.baidu.mobad.feeds;

import com.baidu.mobads.interfaces.feeds.IXAdFeedsRequestParameters;
import com.igexin.download.Downloads;
import java.util.HashMap;
import java.util.Map;

public final class RequestParameters implements IXAdFeedsRequestParameters {
    public static final int ADS_TYPE_DOWNLOAD = 2;
    public static final int ADS_TYPE_OPENPAGE = 1;
    public static final int DOWNLOAD_APP_CONFIRM_ALWAYS = 2;
    public static final int DOWNLOAD_APP_CONFIRM_CUSTOM_BY_APP = 4;
    public static final int DOWNLOAD_APP_CONFIRM_NEVER = 3;
    public static final int DOWNLOAD_APP_CONFIRM_ONLY_MOBILE = 1;
    public static final int MAX_ASSETS_RESERVED = 15;
    public static final String TAG = "RequestParameters";

    /* renamed from: a  reason: collision with root package name */
    private final String f406a;
    private int b;
    private boolean c;
    private Map<String, String> d;
    private int e;
    private int f;
    private int g;
    protected String mPlacementId;

    public static class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public String f407a;
        /* access modifiers changed from: private */
        public Map<String, String> b = new HashMap();
        /* access modifiers changed from: private */
        public int c = 3;
        /* access modifiers changed from: private */
        public boolean d = false;
        /* access modifiers changed from: private */
        public int e = 640;
        /* access modifiers changed from: private */
        public int f = 480;
        /* access modifiers changed from: private */
        public int g = 1;

        public final Builder setWidth(int i) {
            this.e = i;
            return this;
        }

        public final Builder setHeight(int i) {
            this.f = i;
            return this;
        }

        @Deprecated
        public final Builder confirmDownloading(boolean z) {
            if (z) {
                downloadAppConfirmPolicy(2);
            } else {
                downloadAppConfirmPolicy(3);
            }
            return this;
        }

        public final Builder downloadAppConfirmPolicy(int i) {
            this.g = i;
            return this;
        }

        public final Builder addExtra(String str, String str2) {
            this.b.put(str, str2);
            return this;
        }

        public final RequestParameters build() {
            return new RequestParameters(this);
        }
    }

    private RequestParameters(Builder builder) {
        this.e = 0;
        this.f = 0;
        this.f406a = builder.f407a;
        this.b = builder.c;
        this.e = builder.e;
        this.f = builder.f;
        this.c = builder.d;
        this.g = builder.g;
        setExtras(builder.b);
    }

    public final String getKeywords() {
        return this.f406a;
    }

    public int getWidth() {
        return this.e;
    }

    public int getHeight() {
        return this.f;
    }

    public int getAdsType() {
        return this.b;
    }

    public void setAdsType(int i) {
        this.b = i;
    }

    public boolean isConfirmDownloading() {
        return this.c;
    }

    public Map<String, String> getExtras() {
        return this.d;
    }

    public void setExtras(Map<String, String> map) {
        this.d = map;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("mKeywords", this.f406a);
        hashMap.put("adsType", Integer.valueOf(this.b));
        hashMap.put("confirmDownloading", Boolean.valueOf(this.c));
        HashMap hashMap2 = new HashMap();
        if (this.d != null) {
            for (Map.Entry next : this.d.entrySet()) {
                hashMap2.put(next.getKey(), next.getValue());
            }
        }
        hashMap.put(Downloads.COLUMN_EXTRAS, hashMap2);
        return hashMap;
    }

    public String getAdPlacementId() {
        return this.mPlacementId;
    }

    public int getAPPConfirmPolicy() {
        return this.g;
    }
}
