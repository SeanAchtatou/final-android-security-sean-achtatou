package com.baidu.mobad.chuilei;

import com.baidu.mobad.feeds.RequestParameters;
import com.baidu.mobads.interfaces.feeds.IXAdFeedsRequestParameters;
import java.util.HashMap;
import java.util.Map;

public class BaiduChuileiRequestParameters implements IXAdFeedsRequestParameters {

    /* renamed from: a  reason: collision with root package name */
    private RequestParameters f398a = new RequestParameters.Builder().build();

    public RequestParameters getRequestParameters() {
        return this.f398a;
    }

    public String getKeywords() {
        return this.f398a.getKeywords();
    }

    public int getAdsType() {
        return this.f398a.getAdsType();
    }

    @Deprecated
    public boolean isConfirmDownloading() {
        return this.f398a.isConfirmDownloading();
    }

    public int getAPPConfirmPolicy() {
        return this.f398a.getAPPConfirmPolicy();
    }

    public Map<String, String> getExtras() {
        return this.f398a.getExtras();
    }

    public HashMap<String, Object> toHashMap() {
        return this.f398a.toHashMap();
    }

    public String getAdPlacementId() {
        return this.f398a.getAdPlacementId();
    }
}
