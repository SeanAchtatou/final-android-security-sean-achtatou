package com.baidu.mobads;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.widget.ImageView;

class ae extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    int f451a = 0;
    RectF b = new RectF();
    private final Paint c;
    private final Context d;

    public ae(Context context) {
        super(context);
        this.d = context;
        this.c = new Paint();
        this.c.setAntiAlias(true);
        this.c.setStyle(Paint.Style.STROKE);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int width = getWidth() / 2;
        int a2 = a(this.d, 15.0f);
        int a3 = a(this.d, 4.0f);
        this.b.left = (float) (width - ((a2 + 1) + (a3 / 2)));
        this.b.top = (float) (width - ((a2 + 1) + (a3 / 2)));
        this.b.right = (float) (a2 + 1 + (a3 / 2) + width);
        this.b.bottom = (float) (width + a2 + 1 + (a3 / 2));
        this.c.setColor(-1907998);
        this.c.setStrokeWidth((float) a3);
        canvas.drawArc(this.b, (float) (this.f451a + 0), 72.0f, false, this.c);
        this.c.setColor(-1594427658);
        canvas.drawArc(this.b, (float) (this.f451a + 72), 270.0f, false, this.c);
        this.f451a += 10;
        if (this.f451a >= 360) {
            this.f451a = 0;
        }
        super.onDraw(canvas);
        invalidate();
    }

    public static int a(Context context, float f) {
        return (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
    }
}
