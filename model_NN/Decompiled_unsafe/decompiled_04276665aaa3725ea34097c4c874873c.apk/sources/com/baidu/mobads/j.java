package com.baidu.mobads;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.baidu.mobads.AppActivity;

class j extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppActivity.b f517a;
    final /* synthetic */ AppActivity b;

    j(AppActivity appActivity, AppActivity.b bVar) {
        this.b = appActivity;
        this.f517a = bVar;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str != null) {
            try {
                if (!str.equals("about:blank")) {
                    this.b.a(webView, str, (Runnable) null, new k(this, str));
                    WebView.HitTestResult hitTestResult = webView.getHitTestResult();
                    if (hitTestResult == null || hitTestResult.getType() != 0) {
                        return true;
                    }
                    this.b.D.d(AppActivity.o, "AppActivity shouldOverrideUrlLoading and hitType==0");
                    this.f517a.f434a = true;
                    return true;
                }
            } catch (Exception e) {
                this.b.D.d(AppActivity.o, e.getMessage());
                return true;
            }
        }
        return false;
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (!(this.b.curWebview == null || str == null)) {
            this.b.curWebview.f449a = str;
        }
        this.f517a.b = str;
        this.f517a.c = false;
        if (!this.f517a.f434a) {
            a(str);
        }
    }

    public void onPageFinished(WebView webView, String str) {
        if (!this.f517a.f434a && this.f517a.b.equals(str)) {
            if (this.b.g == -1) {
                this.b.g = (int) (System.currentTimeMillis() - this.b.s);
            }
            if (!this.f517a.c) {
                this.f517a.c = true;
                a(this.f517a.b, 0);
            }
        }
        this.f517a.f434a = false;
        this.f517a.b = "";
        if (this.b.H != null) {
            this.b.H.onPageFinished(webView);
        }
        super.onPageFinished(webView, str);
    }

    private void a(String str) {
        if (this.b.q != null) {
            try {
                if (this.f517a.d) {
                    this.b.A.o = str;
                    if (this.b.A.A == 0) {
                        this.b.A.A = System.currentTimeMillis();
                    }
                    this.b.A.p = AppActivity.i(this.b);
                    if (this.b.curWebview != null) {
                        this.b.A.q = this.b.curWebview.getContentHeight();
                        this.b.A.r = this.b.curWebview.getProgress();
                    }
                    this.b.A.v = System.currentTimeMillis() - this.b.s;
                    this.b.A.w = this.b.v;
                    this.b.A.y = this.b.e;
                    this.f517a.d = false;
                    return;
                }
                this.b.D.i(AppActivity.o, "App2Activity - not send 37");
            } catch (Exception e) {
                this.b.D.d(AppActivity.o, e.getMessage());
            }
        }
    }

    private void a(String str, int i) {
        if (this.b.q != null) {
            try {
                this.b.A.o = str;
                if (this.b.A.B == 0) {
                    this.b.A.B = System.currentTimeMillis();
                }
                this.b.A.p = AppActivity.i(this.b);
                this.b.A.w = this.b.v;
                this.b.A.y = this.b.e;
                if (this.b.curWebview != null) {
                    this.b.A.q = this.b.curWebview.getContentHeight();
                    this.b.A.r = this.b.curWebview.getProgress();
                }
                this.b.A.v = System.currentTimeMillis() - this.b.s;
                this.f517a.d = true;
            } catch (Exception e) {
                this.b.D.d(AppActivity.o, e.getMessage());
            }
        }
    }

    public void onLoadResource(WebView webView, String str) {
        try {
            if (!this.f517a.c && !str.equals(this.f517a.b) && !this.f517a.f434a) {
                this.f517a.c = true;
                if (this.b.g == -1) {
                    this.b.g = (int) (System.currentTimeMillis() - this.b.s);
                }
                a(this.f517a.b, 0);
            }
            WebView.HitTestResult hitTestResult = webView.getHitTestResult();
            if (hitTestResult != null && hitTestResult.getType() > 0) {
                this.b.a(webView, str, new l(this), (Runnable) null);
            }
        } catch (Exception e) {
            this.b.D.d(AppActivity.o, e.getMessage());
        }
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.b);
        builder.setMessage("ssl证书验证失败，是否继续访问该网页？");
        builder.setPositiveButton("继续", new m(this, sslErrorHandler));
        builder.setNegativeButton("取消", new n(this, sslErrorHandler));
        builder.setOnKeyListener(new o(this, sslErrorHandler));
        builder.create().show();
    }
}
