package com.baidu.mobads.openad.c;

import android.content.Context;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Observable;

public class a extends Observable implements IOAdDownloader, Runnable {

    /* renamed from: a  reason: collision with root package name */
    protected Context f541a;
    protected URL b;
    protected URL c;
    protected String d;
    protected int e;
    protected Boolean f = true;
    protected String g;
    protected int h;
    protected IOAdDownloader.DownloadStatus i;
    protected volatile int j;
    protected int k;
    protected ArrayList<C0014a> l;
    g m = null;
    private String n;
    private String o;

    public a(Context context, URL url, String str, String str2, int i2, String str3, String str4) {
        this.f541a = context;
        this.b = url;
        this.d = str;
        this.e = i2;
        if (str2 == null || str2.trim().length() <= 0) {
            String file = url.getFile();
            this.g = file.substring(file.lastIndexOf(47) + 1);
        } else {
            this.g = str2;
        }
        this.h = -1;
        this.i = IOAdDownloader.DownloadStatus.NONE;
        this.j = 0;
        this.k = 0;
        this.n = str3;
        this.o = str4;
        this.l = new ArrayList<>();
    }

    public void pause() {
        try {
            m.a().f().d("Downloader", "execute Pause; state = " + this.i);
            if (this.i == IOAdDownloader.DownloadStatus.DOWNLOADING || this.i == IOAdDownloader.DownloadStatus.ERROR || this.i == IOAdDownloader.DownloadStatus.NONE) {
                if (this.l != null) {
                    for (int i2 = 0; i2 < this.l.size(); i2++) {
                        if (!this.l.get(i2).a()) {
                            this.l.get(i2).c();
                        }
                    }
                }
                a(IOAdDownloader.DownloadStatus.PAUSED);
            }
        } catch (Exception e2) {
            m.a().f().d("Downloader", "pause exception");
            com.baidu.mobads.c.a.a().a("apk download pause failed: " + e2.toString());
        }
    }

    public void resume() {
        try {
            m.a().f().d("Downloader", "execute Resume; state = " + this.i);
            if (this.i == IOAdDownloader.DownloadStatus.PAUSED || this.i == IOAdDownloader.DownloadStatus.ERROR || this.i == IOAdDownloader.DownloadStatus.CANCELLED) {
                a(IOAdDownloader.DownloadStatus.INITING);
                new Thread(this).start();
            }
        } catch (Exception e2) {
            m.a().f().d("Downloader", "resume exception");
            com.baidu.mobads.c.a.a().a("apk download resume failed: " + e2.toString());
        }
    }

    public void cancel() {
        try {
            m.a().f().d("Downloader", "execute Cancel; state = " + this.i);
            if (this.i == IOAdDownloader.DownloadStatus.PAUSED || this.i == IOAdDownloader.DownloadStatus.DOWNLOADING) {
                if (this.l != null) {
                    for (int i2 = 0; i2 < this.l.size(); i2++) {
                        if (!this.l.get(i2).a()) {
                            this.l.get(i2).c();
                        }
                    }
                }
                a(IOAdDownloader.DownloadStatus.CANCELLED);
            }
        } catch (Exception e2) {
            m.a().f().d("Downloader", "cancel exception");
            com.baidu.mobads.c.a.a().a("apk download cancel failed: " + e2.toString());
        }
    }

    public String getURL() {
        return this.b.toString();
    }

    public int getFileSize() {
        return this.h;
    }

    public float getProgress() {
        return Math.abs((((float) this.j) / ((float) this.h)) * 100.0f);
    }

    public String getTitle() {
        return this.n;
    }

    public String getPackageName() {
        return this.o;
    }

    public IOAdDownloader.DownloadStatus getState() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public void a(IOAdDownloader.DownloadStatus downloadStatus) {
        this.i = downloadStatus;
        a();
    }

    public void start() {
        m.a().f().d("Downloader", "execute Start; state = " + this.i);
        if (this.i == IOAdDownloader.DownloadStatus.NONE) {
            a(IOAdDownloader.DownloadStatus.INITING);
            new Thread(this).start();
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void a(int i2) {
        this.j += i2;
        int progress = (int) getProgress();
        if (this.k < progress) {
            this.k = progress;
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        setChanged();
        notifyObservers();
    }

    public String getOutputPath() {
        return this.d + this.g;
    }

    /* access modifiers changed from: protected */
    public synchronized void b() {
        this.i = IOAdDownloader.DownloadStatus.ERROR;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.l.size()) {
                if (!this.l.get(i3).a()) {
                    this.l.get(i3).c();
                }
                i2 = i3 + 1;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0352  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0255 A[EDGE_INSN: B:134:0x0255->B:72:0x0255 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x027c A[EDGE_INSN: B:137:0x027c->B:79:0x027c ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:143:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0111  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0155  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01de  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01f6  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0210  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0238  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x025f  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0282  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x02a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.net.HttpURLConnection r16) {
        /*
            r15 = this;
            java.net.URL r2 = r15.c
            java.lang.String r4 = r2.toString()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r15.d
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r15.g
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = ".tmp"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r5 = r2.toString()
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            int r2 = r2.size()
            if (r2 != 0) goto L_0x01ed
            java.io.File r2 = new java.io.File
            java.lang.String r3 = r15.d
            r2.<init>(r3)
            boolean r3 = r2.exists()
            if (r3 != 0) goto L_0x0046
            r2.mkdirs()
        L_0x0046:
            r2 = 0
            java.io.File r6 = new java.io.File
            r6.<init>(r5)
            java.lang.Boolean r3 = r15.f
            boolean r3 = r3.booleanValue()
            if (r3 == 0) goto L_0x0102
            boolean r3 = r6.exists()
            if (r3 == 0) goto L_0x0102
            long r8 = r6.length()
            int r3 = r15.h
            long r10 = (long) r3
            int r3 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r3 != 0) goto L_0x0102
            com.baidu.mobads.openad.c.g r3 = new com.baidu.mobads.openad.c.g     // Catch:{ Exception -> 0x03a3 }
            android.content.Context r7 = r15.f541a     // Catch:{ Exception -> 0x03a3 }
            r3.<init>(r7)     // Catch:{ Exception -> 0x03a3 }
            r15.m = r3     // Catch:{ Exception -> 0x03a3 }
            com.baidu.mobads.openad.c.g r3 = r15.m     // Catch:{ Exception -> 0x03a3 }
            java.util.List r7 = r3.b(r4, r5)     // Catch:{ Exception -> 0x03a3 }
            if (r7 == 0) goto L_0x0102
            int r3 = r7.size()     // Catch:{ Exception -> 0x03a3 }
            if (r3 <= 0) goto L_0x0102
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x03a3 }
            r3.<init>()     // Catch:{ Exception -> 0x03a3 }
            java.util.HashSet r8 = new java.util.HashSet     // Catch:{ Exception -> 0x00f3 }
            r8.<init>()     // Catch:{ Exception -> 0x00f3 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ Exception -> 0x00f3 }
        L_0x008a:
            boolean r2 = r7.hasNext()     // Catch:{ Exception -> 0x00f3 }
            if (r2 == 0) goto L_0x0101
            java.lang.Object r2 = r7.next()     // Catch:{ Exception -> 0x00f3 }
            com.baidu.mobads.openad.c.h r2 = (com.baidu.mobads.openad.c.h) r2     // Catch:{ Exception -> 0x00f3 }
            int r9 = r2.c()     // Catch:{ Exception -> 0x00f3 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x00f3 }
            boolean r9 = r8.contains(r9)     // Catch:{ Exception -> 0x00f3 }
            if (r9 != 0) goto L_0x008a
            int r9 = r2.c()     // Catch:{ Exception -> 0x00f3 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x00f3 }
            r8.add(r9)     // Catch:{ Exception -> 0x00f3 }
            r3.add(r2)     // Catch:{ Exception -> 0x00f3 }
            com.baidu.mobads.j.m r9 = com.baidu.mobads.j.m.a()     // Catch:{ Exception -> 0x00f3 }
            com.baidu.mobads.interfaces.utils.IXAdLogger r9 = r9.f()     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r10 = "Downloader"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f3 }
            r11.<init>()     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r12 = "resume from db: start="
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x00f3 }
            int r12 = r2.d()     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r12 = ";end ="
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x00f3 }
            int r12 = r2.e()     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r12 = ";complete="
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x00f3 }
            int r2 = r2.a()     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r2 = r11.append(r2)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00f3 }
            r9.d(r10, r2)     // Catch:{ Exception -> 0x00f3 }
            goto L_0x008a
        L_0x00f3:
            r2 = move-exception
        L_0x00f4:
            com.baidu.mobads.j.m r7 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r7 = r7.f()
            java.lang.String r8 = "Downloader"
            r7.d(r8, r2)
        L_0x0101:
            r2 = r3
        L_0x0102:
            if (r2 == 0) goto L_0x010b
            int r3 = r2.size()
            r7 = 1
            if (r3 >= r7) goto L_0x019b
        L_0x010b:
            boolean r2 = r6.exists()
            if (r2 == 0) goto L_0x0114
            r6.delete()
        L_0x0114:
            r6.createNewFile()     // Catch:{ Exception -> 0x0181 }
            java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x0181 }
            java.lang.String r3 = "rwd"
            r2.<init>(r6, r3)     // Catch:{ Exception -> 0x0181 }
            int r3 = r15.h     // Catch:{ Exception -> 0x0181 }
            long r6 = (long) r3     // Catch:{ Exception -> 0x0181 }
            r2.setLength(r6)     // Catch:{ Exception -> 0x0181 }
            r2.close()     // Catch:{ Exception -> 0x0181 }
            com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()     // Catch:{ Exception -> 0x0181 }
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()     // Catch:{ Exception -> 0x0181 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0181 }
            r3.<init>()     // Catch:{ Exception -> 0x0181 }
            java.lang.String r6 = "Downloader.init():  建立完random文件 ts:"
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x0181 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0181 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x0181 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0181 }
            r2.d(r3)     // Catch:{ Exception -> 0x0181 }
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            r7 = -1
            r3 = 0
            int r2 = r15.e
            r6 = 1
            if (r2 <= r6) goto L_0x01de
            int r2 = r15.h
            float r2 = (float) r2
            int r6 = r15.e
            float r6 = (float) r6
            float r2 = r2 / r6
            r6 = 1204289536(0x47c80000, float:102400.0)
            float r2 = r2 / r6
            int r2 = java.lang.Math.round(r2)
            r6 = 102400(0x19000, float:1.43493E-40)
            int r10 = r2 * r6
        L_0x0168:
            int r2 = r15.h
            if (r7 >= r2) goto L_0x019a
            int r6 = r7 + 1
            int r2 = r7 + r10
            int r8 = r15.h
            if (r2 >= r8) goto L_0x0197
            int r7 = r7 + r10
        L_0x0175:
            int r3 = r3 + 1
            com.baidu.mobads.openad.c.h r2 = new com.baidu.mobads.openad.c.h
            r8 = 0
            r2.<init>(r3, r4, r5, r6, r7, r8)
            r9.add(r2)
            goto L_0x0168
        L_0x0181:
            r2 = move-exception
            com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
            java.lang.String r3 = "Downloader"
            java.lang.String r4 = " 建立文件失败:"
            r2.d(r3, r4)
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR
            r15.a(r2)
        L_0x0196:
            return
        L_0x0197:
            int r7 = r15.h
            goto L_0x0175
        L_0x019a:
            r2 = r9
        L_0x019b:
            java.util.Iterator r3 = r2.iterator()
        L_0x019f:
            boolean r2 = r3.hasNext()
            if (r2 == 0) goto L_0x01ed
            java.lang.Object r2 = r3.next()
            com.baidu.mobads.openad.c.h r2 = (com.baidu.mobads.openad.c.h) r2
            com.baidu.mobads.openad.c.a$a r6 = new com.baidu.mobads.openad.c.a$a
            int r8 = r2.c()
            java.net.URL r9 = r15.c
            java.lang.String r10 = r2.f()
            int r11 = r2.d()
            int r12 = r2.e()
            int r13 = r2.a()
            r7 = r15
            r6.<init>(r8, r9, r10, r11, r12, r13)
            int r7 = r2.d()
            if (r7 != 0) goto L_0x01d8
            int r2 = r2.a()
            if (r2 != 0) goto L_0x01d8
            r0 = r16
            r6.a(r0)
        L_0x01d8:
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            r2.add(r6)
            goto L_0x019f
        L_0x01de:
            r6 = 0
            int r7 = r15.h
            r3 = 1
            com.baidu.mobads.openad.c.h r2 = new com.baidu.mobads.openad.c.h
            r8 = 0
            r2.<init>(r3, r4, r5, r6, r7, r8)
            r9.add(r2)
            r2 = r9
            goto L_0x019b
        L_0x01ed:
            r3 = 0
            java.lang.Boolean r2 = r15.f
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L_0x0210
            r2 = 0
            r6 = r3
            r3 = r2
        L_0x01f9:
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            int r2 = r2.size()
            if (r3 >= r2) goto L_0x0211
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            java.lang.Object r2 = r2.get(r3)
            com.baidu.mobads.openad.c.a$a r2 = (com.baidu.mobads.openad.c.a.C0014a) r2
            int r2 = r2.f
            int r6 = r6 + r2
            int r2 = r3 + 1
            r3 = r2
            goto L_0x01f9
        L_0x0210:
            r6 = r3
        L_0x0211:
            r15.j = r6
            float r2 = r15.getProgress()
            int r2 = (int) r2
            r15.k = r2
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.DOWNLOADING
            r15.a(r2)
            com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
            java.lang.String r3 = "Downloader"
            java.lang.String r6 = "Downloader starts unfinished threads and waits threads end"
            r2.d(r3, r6)
            r2 = 0
            r3 = r2
        L_0x0230:
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            int r2 = r2.size()
            if (r3 >= r2) goto L_0x0255
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            java.lang.Object r2 = r2.get(r3)
            com.baidu.mobads.openad.c.a$a r2 = (com.baidu.mobads.openad.c.a.C0014a) r2
            boolean r2 = r2.a()
            if (r2 != 0) goto L_0x0251
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            java.lang.Object r2 = r2.get(r3)
            com.baidu.mobads.openad.c.a$a r2 = (com.baidu.mobads.openad.c.a.C0014a) r2
            r2.b()
        L_0x0251:
            int r2 = r3 + 1
            r3 = r2
            goto L_0x0230
        L_0x0255:
            r2 = 0
            r3 = r2
        L_0x0257:
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            int r2 = r2.size()
            if (r3 >= r2) goto L_0x027c
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            java.lang.Object r2 = r2.get(r3)
            com.baidu.mobads.openad.c.a$a r2 = (com.baidu.mobads.openad.c.a.C0014a) r2
            boolean r2 = r2.a()
            if (r2 != 0) goto L_0x0278
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            java.lang.Object r2 = r2.get(r3)
            com.baidu.mobads.openad.c.a$a r2 = (com.baidu.mobads.openad.c.a.C0014a) r2
            r2.d()
        L_0x0278:
            int r2 = r3 + 1
            r3 = r2
            goto L_0x0257
        L_0x027c:
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = r15.i
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r3 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.DOWNLOADING
            if (r2 != r3) goto L_0x0352
            r6 = 0
            r2 = 0
            r3 = r2
        L_0x0285:
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            int r2 = r2.size()
            if (r3 >= r2) goto L_0x03a9
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            java.lang.Object r2 = r2.get(r3)
            com.baidu.mobads.openad.c.a$a r2 = (com.baidu.mobads.openad.c.a.C0014a) r2
            boolean r2 = r2.a()
            if (r2 != 0) goto L_0x0341
            r2 = 1
        L_0x029c:
            if (r2 == 0) goto L_0x0346
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR
            r15.a(r2)
        L_0x02a3:
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = r15.i
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r3 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.COMPLETED
            if (r2 == r3) goto L_0x0196
            com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
            java.lang.String r3 = "Downloader"
            java.lang.String r6 = "save database now"
            r2.d(r3, r6)
            java.lang.Boolean r2 = r15.f
            boolean r2 = r2.booleanValue()
            if (r2 == 0) goto L_0x0196
            com.baidu.mobads.openad.c.g r2 = r15.m     // Catch:{ Exception -> 0x0331 }
            if (r2 != 0) goto L_0x02cd
            com.baidu.mobads.openad.c.g r2 = new com.baidu.mobads.openad.c.g     // Catch:{ Exception -> 0x0331 }
            android.content.Context r3 = r15.f541a     // Catch:{ Exception -> 0x0331 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0331 }
            r15.m = r2     // Catch:{ Exception -> 0x0331 }
        L_0x02cd:
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ Exception -> 0x0331 }
            r10.<init>()     // Catch:{ Exception -> 0x0331 }
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l     // Catch:{ Exception -> 0x0331 }
            java.util.Iterator r11 = r2.iterator()     // Catch:{ Exception -> 0x0331 }
        L_0x02d8:
            boolean r2 = r11.hasNext()     // Catch:{ Exception -> 0x0331 }
            if (r2 == 0) goto L_0x038d
            java.lang.Object r2 = r11.next()     // Catch:{ Exception -> 0x0331 }
            r0 = r2
            com.baidu.mobads.openad.c.a$a r0 = (com.baidu.mobads.openad.c.a.C0014a) r0     // Catch:{ Exception -> 0x0331 }
            r9 = r0
            com.baidu.mobads.openad.c.h r2 = new com.baidu.mobads.openad.c.h     // Catch:{ Exception -> 0x0331 }
            int r3 = r9.f542a     // Catch:{ Exception -> 0x0331 }
            int r6 = r9.d     // Catch:{ Exception -> 0x0331 }
            int r7 = r9.e     // Catch:{ Exception -> 0x0331 }
            int r8 = r9.f     // Catch:{ Exception -> 0x0331 }
            r2.<init>(r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0331 }
            r10.add(r2)     // Catch:{ Exception -> 0x0331 }
            com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()     // Catch:{ Exception -> 0x0331 }
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()     // Catch:{ Exception -> 0x0331 }
            java.lang.String r3 = "Downloader"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0331 }
            r6.<init>()     // Catch:{ Exception -> 0x0331 }
            java.lang.String r7 = "save to db: start="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0331 }
            int r7 = r9.d     // Catch:{ Exception -> 0x0331 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0331 }
            java.lang.String r7 = ";end ="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0331 }
            int r7 = r9.e     // Catch:{ Exception -> 0x0331 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0331 }
            java.lang.String r7 = ";complete="
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0331 }
            int r7 = r9.f     // Catch:{ Exception -> 0x0331 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0331 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0331 }
            r2.d(r3, r6)     // Catch:{ Exception -> 0x0331 }
            goto L_0x02d8
        L_0x0331:
            r2 = move-exception
            com.baidu.mobads.j.m r3 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r3 = r3.f()
            java.lang.String r4 = "Downloader"
            r3.d(r4, r2)
            goto L_0x0196
        L_0x0341:
            int r2 = r3 + 1
            r3 = r2
            goto L_0x0285
        L_0x0346:
            java.util.ArrayList<com.baidu.mobads.openad.c.a$a> r2 = r15.l
            r15.a(r2)
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.COMPLETED
            r15.a(r2)
            goto L_0x02a3
        L_0x0352:
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = r15.i
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r3 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR
            if (r2 != r3) goto L_0x035f
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR
            r15.a(r2)
            goto L_0x02a3
        L_0x035f:
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = r15.i
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r3 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.CANCELLED
            if (r2 != r3) goto L_0x0376
            com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
            java.lang.String r3 = "Downloader"
            java.lang.String r6 = "Downloader is cancelled"
            r2.d(r3, r6)
            goto L_0x02a3
        L_0x0376:
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = r15.i
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r3 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.PAUSED
            if (r2 != r3) goto L_0x02a3
            com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
            java.lang.String r3 = "Downloader"
            java.lang.String r6 = "Downloader is paused"
            r2.d(r3, r6)
            goto L_0x02a3
        L_0x038d:
            com.baidu.mobads.openad.c.g r2 = r15.m     // Catch:{ Exception -> 0x0331 }
            boolean r2 = r2.a(r4, r5)     // Catch:{ Exception -> 0x0331 }
            if (r2 == 0) goto L_0x039c
            com.baidu.mobads.openad.c.g r2 = r15.m     // Catch:{ Exception -> 0x0331 }
            r2.b(r10)     // Catch:{ Exception -> 0x0331 }
            goto L_0x0196
        L_0x039c:
            com.baidu.mobads.openad.c.g r2 = r15.m     // Catch:{ Exception -> 0x0331 }
            r2.a(r10)     // Catch:{ Exception -> 0x0331 }
            goto L_0x0196
        L_0x03a3:
            r3 = move-exception
            r14 = r3
            r3 = r2
            r2 = r14
            goto L_0x00f4
        L_0x03a9:
            r2 = r6
            goto L_0x029c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobads.openad.c.a.a(java.net.HttpURLConnection):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        a(com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005e, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00e1, code lost:
        r1.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0100, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0101, code lost:
        r4 = r1;
        r1 = r0;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0058 A[ExcHandler: Exception (e java.lang.Exception), PHI: r0 
      PHI: (r0v4 java.net.HttpURLConnection) = (r0v0 java.net.HttpURLConnection), (r0v8 java.net.HttpURLConnection) binds: [B:4:0x000a, B:23:0x0061] A[DONT_GENERATE, DONT_INLINE], Splitter:B:4:0x000a] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00e1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            r0 = 0
            r3 = 1
            java.net.URL r1 = r5.c
            if (r1 == 0) goto L_0x000a
            int r1 = r5.h
            if (r1 >= r3) goto L_0x00e5
        L_0x000a:
            com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()     // Catch:{ Exception -> 0x0058, all -> 0x00db }
            com.baidu.mobads.interfaces.utils.IXAdURIUitls r1 = r1.i()     // Catch:{ Exception -> 0x0058, all -> 0x00db }
            java.net.URL r2 = r5.b     // Catch:{ Exception -> 0x0058, all -> 0x00db }
            java.net.HttpURLConnection r0 = r1.getHttpURLConnection(r2)     // Catch:{ Exception -> 0x0058, all -> 0x00db }
            java.lang.String r1 = "Range"
            java.lang.String r2 = "bytes=0-"
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0058 }
            r1 = 10000(0x2710, float:1.4013E-41)
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0058 }
            com.baidu.mobads.AdSettings$b r1 = com.baidu.mobads.AdSettings.b.HTTPS_PROTOCOL_TYPE     // Catch:{ Exception -> 0x0058 }
            java.lang.String r1 = r1.a()     // Catch:{ Exception -> 0x0058 }
            java.lang.String r2 = com.baidu.mobads.AdSettings.getSupportHttps()     // Catch:{ Exception -> 0x0058 }
            boolean r1 = r2.equals(r1)     // Catch:{ Exception -> 0x0058 }
            if (r1 == 0) goto L_0x0050
            r1 = 0
            r0.setInstanceFollowRedirects(r1)     // Catch:{ Exception -> 0x0058 }
            java.net.HttpURLConnection r0 = r5.b(r0)     // Catch:{ Exception -> 0x0058 }
        L_0x003c:
            int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x0058 }
            int r1 = r1 / 100
            r2 = 2
            if (r1 == r2) goto L_0x0061
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r1 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR     // Catch:{ Exception -> 0x0058 }
            r5.a(r1)     // Catch:{ Exception -> 0x0058 }
            if (r0 == 0) goto L_0x004f
        L_0x004c:
            r0.disconnect()
        L_0x004f:
            return
        L_0x0050:
            r1 = 1
            r0.setInstanceFollowRedirects(r1)     // Catch:{ Exception -> 0x0058 }
            r0.connect()     // Catch:{ Exception -> 0x0058 }
            goto L_0x003c
        L_0x0058:
            r1 = move-exception
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r1 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR     // Catch:{ all -> 0x0100 }
            r5.a(r1)     // Catch:{ all -> 0x0100 }
            if (r0 == 0) goto L_0x004f
            goto L_0x004c
        L_0x0061:
            java.lang.String r1 = r0.getContentType()     // Catch:{ Exception -> 0x0058 }
            java.lang.String r2 = "text/html"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x0058 }
            if (r1 == 0) goto L_0x0075
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r1 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR     // Catch:{ Exception -> 0x0058 }
            r5.a(r1)     // Catch:{ Exception -> 0x0058 }
            if (r0 == 0) goto L_0x004f
            goto L_0x004c
        L_0x0075:
            int r1 = r0.getContentLength()     // Catch:{ Exception -> 0x0058 }
            if (r1 >= r3) goto L_0x0083
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r1 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR     // Catch:{ Exception -> 0x0058 }
            r5.a(r1)     // Catch:{ Exception -> 0x0058 }
            if (r0 == 0) goto L_0x004f
            goto L_0x004c
        L_0x0083:
            r2 = 5120000(0x4e2000, float:7.174648E-39)
            if (r1 >= r2) goto L_0x008b
            r2 = 1
            r5.e = r2     // Catch:{ Exception -> 0x0058 }
        L_0x008b:
            java.net.URL r2 = r0.getURL()     // Catch:{ Exception -> 0x0058 }
            r5.c = r2     // Catch:{ Exception -> 0x0058 }
            java.lang.String r2 = "mounted"
            java.lang.String r3 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x0058 }
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x0058 }
            if (r2 != 0) goto L_0x00a5
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r1 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR     // Catch:{ Exception -> 0x0058 }
            r5.a(r1)     // Catch:{ Exception -> 0x0058 }
            if (r0 == 0) goto L_0x004f
            goto L_0x004c
        L_0x00a5:
            java.lang.String r2 = "Content-Range"
            java.lang.String r2 = r0.getHeaderField(r2)     // Catch:{ Exception -> 0x0058 }
            if (r2 != 0) goto L_0x00cd
            java.lang.String r2 = "Accept-Ranges"
            java.lang.String r2 = r0.getHeaderField(r2)     // Catch:{ Exception -> 0x0058 }
            if (r2 == 0) goto L_0x00c3
            java.lang.String r2 = "Accept-Ranges"
            java.lang.String r2 = r0.getHeaderField(r2)     // Catch:{ Exception -> 0x0058 }
            java.lang.String r3 = "none"
            boolean r2 = r2.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0058 }
            if (r2 == 0) goto L_0x00cd
        L_0x00c3:
            r2 = 0
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x0058 }
            r5.f = r2     // Catch:{ Exception -> 0x0058 }
            r2 = 1
            r5.e = r2     // Catch:{ Exception -> 0x0058 }
        L_0x00cd:
            int r2 = r5.h     // Catch:{ Exception -> 0x0058 }
            r3 = -1
            if (r2 != r3) goto L_0x00d4
            r5.h = r1     // Catch:{ Exception -> 0x0058 }
        L_0x00d4:
            r5.a(r0)     // Catch:{ Exception -> 0x0058 }
            if (r0 == 0) goto L_0x004f
            goto L_0x004c
        L_0x00db:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x00df:
            if (r1 == 0) goto L_0x00e4
            r1.disconnect()
        L_0x00e4:
            throw r0
        L_0x00e5:
            r0 = 0
            r5.a(r0)     // Catch:{ Exception -> 0x00eb }
            goto L_0x004f
        L_0x00eb:
            r0 = move-exception
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r1 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR
            r5.a(r1)
            com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
            java.lang.String r2 = "Downloader"
            r1.d(r2, r0)
            goto L_0x004f
        L_0x0100:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x00df
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobads.openad.c.a.run():void");
    }

    /* access modifiers changed from: protected */
    public void a(ArrayList<C0014a> arrayList) {
        m.a().k().renameFile(this.d + this.g + ".tmp", this.d + this.g);
    }

    /* renamed from: com.baidu.mobads.openad.c.a$a  reason: collision with other inner class name */
    protected class C0014a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        protected int f542a;
        protected URL b;
        protected String c;
        protected int d;
        protected int e;
        protected int f;
        protected boolean g;
        protected Thread h;
        private volatile boolean j = false;
        private volatile int k = 0;
        private HttpURLConnection l;

        public C0014a(int i2, URL url, String str, int i3, int i4, int i5) {
            this.f542a = i2;
            this.b = url;
            this.c = str;
            this.d = i3;
            this.e = i4;
            this.f = i5;
            this.g = false;
        }

        public boolean a() {
            return this.g;
        }

        public synchronized void b() {
            this.j = false;
            this.h = new Thread(this);
            this.h.start();
        }

        public synchronized void c() {
            this.j = true;
            this.k++;
        }

        public void a(HttpURLConnection httpURLConnection) {
            this.l = httpURLConnection;
        }

        public void d() {
            if (this.h != null) {
                this.h.join();
                return;
            }
            m.a().f().w("DownloadThread", "Warning: mThread in DownloadThread.waitFinish is null");
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.io.RandomAccessFile} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: java.io.BufferedInputStream} */
        /* JADX WARN: Type inference failed for: r2v0 */
        /* JADX WARN: Type inference failed for: r2v9 */
        /* JADX WARN: Type inference failed for: r2v13 */
        /* JADX WARN: Type inference failed for: r2v14 */
        /* JADX WARN: Type inference failed for: r2v15 */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:136:0x033b A[SYNTHETIC, Splitter:B:136:0x033b] */
        /* JADX WARNING: Removed duplicated region for block: B:139:0x0340 A[SYNTHETIC, Splitter:B:139:0x0340] */
        /* JADX WARNING: Removed duplicated region for block: B:142:0x0345 A[SYNTHETIC, Splitter:B:142:0x0345] */
        /* JADX WARNING: Removed duplicated region for block: B:203:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0136 A[Catch:{ all -> 0x04c9 }] */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x0175 A[SYNTHETIC, Splitter:B:50:0x0175] */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x017a A[SYNTHETIC, Splitter:B:53:0x017a] */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x017f A[SYNTHETIC, Splitter:B:56:0x017f] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r12 = this;
                r2 = 0
                r11 = 2
                r10 = 1
                r9 = 0
                int r5 = r12.k
                r1 = 0
                r3 = 0
                int r0 = r12.d     // Catch:{ Exception -> 0x04cf, all -> 0x04b2 }
                int r4 = r12.f     // Catch:{ Exception -> 0x04cf, all -> 0x04b2 }
                int r0 = r0 + r4
                int r4 = r12.e     // Catch:{ Exception -> 0x04cf, all -> 0x04b2 }
                if (r0 < r4) goto L_0x005f
                r0 = 1
                r12.g = r0     // Catch:{ Exception -> 0x04cf, all -> 0x04b2 }
                r1 = r2
                r3 = r2
                r4 = r2
            L_0x0017:
                com.baidu.mobads.j.m r0 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r0 = r0.f()
                java.lang.String r2 = "DownloadThread"
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "Thread["
                java.lang.StringBuilder r6 = r6.append(r7)
                int r7 = r12.f542a
                java.lang.StringBuilder r6 = r6.append(r7)
                java.lang.String r7 = "] ver("
                java.lang.StringBuilder r6 = r6.append(r7)
                java.lang.StringBuilder r5 = r6.append(r5)
                java.lang.String r6 = ") executed end; isFinished="
                java.lang.StringBuilder r5 = r5.append(r6)
                boolean r6 = r12.g
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r5 = r5.toString()
                r0.d(r2, r5)
                if (r1 == 0) goto L_0x0054
                r1.close()     // Catch:{ IOException -> 0x03cb }
            L_0x0054:
                if (r3 == 0) goto L_0x0059
                r3.close()     // Catch:{ IOException -> 0x03e5 }
            L_0x0059:
                if (r4 == 0) goto L_0x005e
                r4.disconnect()     // Catch:{ Exception -> 0x03ff }
            L_0x005e:
                return
            L_0x005f:
                java.net.HttpURLConnection r0 = r12.l     // Catch:{ Exception -> 0x04cf, all -> 0x04b2 }
                if (r0 != 0) goto L_0x02df
                java.net.URL r0 = r12.b     // Catch:{ Exception -> 0x04cf, all -> 0x04b2 }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x04cf, all -> 0x04b2 }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x04cf, all -> 0x04b2 }
                com.baidu.mobads.openad.c.a r4 = com.baidu.mobads.openad.c.a.this     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                java.lang.Boolean r4 = r4.f     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                boolean r4 = r4.booleanValue()     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                if (r4 == 0) goto L_0x0119
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                r4.<init>()     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                int r6 = r12.d     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                int r7 = r12.f     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                int r6 = r6 + r7
                java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                java.lang.String r6 = "-"
                java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                int r6 = r12.e     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                java.lang.String r6 = "Range"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                r7.<init>()     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                java.lang.String r8 = "bytes="
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                java.lang.StringBuilder r4 = r7.append(r4)     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                r0.setRequestProperty(r6, r4)     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
            L_0x00ab:
                r0.connect()     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                int r4 = r0.getResponseCode()     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                int r6 = r12.k     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                if (r5 == r6) goto L_0x019b
                com.baidu.mobads.j.m r4 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r4 = r4.f()
                java.lang.String r6 = "DownloadThread"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "Thread["
                java.lang.StringBuilder r7 = r7.append(r8)
                int r8 = r12.f542a
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.String r8 = "] ver("
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.StringBuilder r5 = r7.append(r5)
                java.lang.String r7 = ") executed end; isFinished="
                java.lang.StringBuilder r5 = r5.append(r7)
                boolean r7 = r12.g
                java.lang.StringBuilder r5 = r5.append(r7)
                java.lang.String r5 = r5.toString()
                r4.d(r6, r5)
                if (r2 == 0) goto L_0x00f3
                r3.close()     // Catch:{ IOException -> 0x0416 }
            L_0x00f3:
                if (r2 == 0) goto L_0x00f8
                r1.close()     // Catch:{ IOException -> 0x0430 }
            L_0x00f8:
                if (r0 == 0) goto L_0x005e
                r0.disconnect()     // Catch:{ Exception -> 0x00ff }
                goto L_0x005e
            L_0x00ff:
                r0 = move-exception
                com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
                java.lang.Object[] r2 = new java.lang.Object[r11]
                java.lang.String r3 = "DownloadThread"
                r2[r9] = r3
                java.lang.String r0 = r0.getMessage()
                r2[r10] = r0
            L_0x0114:
                r1.w(r2)
                goto L_0x005e
            L_0x0119:
                r4 = 0
                r12.f = r4     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                goto L_0x00ab
            L_0x011d:
                r1 = move-exception
                r3 = r0
                r0 = r1
                r1 = r2
            L_0x0121:
                com.baidu.mobads.j.m r4 = com.baidu.mobads.j.m.a()     // Catch:{ all -> 0x04c9 }
                com.baidu.mobads.interfaces.utils.IXAdLogger r4 = r4.f()     // Catch:{ all -> 0x04c9 }
                java.lang.String r6 = "DownloadThread"
                java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x04c9 }
                r4.d(r6, r0)     // Catch:{ all -> 0x04c9 }
                int r0 = r12.k     // Catch:{ all -> 0x04c9 }
                if (r5 != r0) goto L_0x013b
                com.baidu.mobads.openad.c.a r0 = com.baidu.mobads.openad.c.a.this     // Catch:{ all -> 0x04c9 }
                r0.b()     // Catch:{ all -> 0x04c9 }
            L_0x013b:
                com.baidu.mobads.j.m r0 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r0 = r0.f()
                java.lang.String r4 = "DownloadThread"
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "Thread["
                java.lang.StringBuilder r6 = r6.append(r7)
                int r7 = r12.f542a
                java.lang.StringBuilder r6 = r6.append(r7)
                java.lang.String r7 = "] ver("
                java.lang.StringBuilder r6 = r6.append(r7)
                java.lang.StringBuilder r5 = r6.append(r5)
                java.lang.String r6 = ") executed end; isFinished="
                java.lang.StringBuilder r5 = r5.append(r6)
                boolean r6 = r12.g
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r5 = r5.toString()
                r0.d(r4, r5)
                if (r1 == 0) goto L_0x0178
                r1.close()     // Catch:{ IOException -> 0x0397 }
            L_0x0178:
                if (r2 == 0) goto L_0x017d
                r2.close()     // Catch:{ IOException -> 0x03b1 }
            L_0x017d:
                if (r3 == 0) goto L_0x005e
                r3.disconnect()     // Catch:{ Exception -> 0x0184 }
                goto L_0x005e
            L_0x0184:
                r0 = move-exception
                com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
                java.lang.Object[] r2 = new java.lang.Object[r11]
                java.lang.String r3 = "DownloadThread"
                r2[r9] = r3
                java.lang.String r0 = r0.getMessage()
                r2[r10] = r0
                goto L_0x0114
            L_0x019b:
                int r4 = r4 / 100
                if (r4 == r11) goto L_0x0204
                com.baidu.mobads.openad.c.a r4 = com.baidu.mobads.openad.c.a.this     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                r4.b()     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                com.baidu.mobads.j.m r4 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r4 = r4.f()
                java.lang.String r6 = "DownloadThread"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "Thread["
                java.lang.StringBuilder r7 = r7.append(r8)
                int r8 = r12.f542a
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.String r8 = "] ver("
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.StringBuilder r5 = r7.append(r5)
                java.lang.String r7 = ") executed end; isFinished="
                java.lang.StringBuilder r5 = r5.append(r7)
                boolean r7 = r12.g
                java.lang.StringBuilder r5 = r5.append(r7)
                java.lang.String r5 = r5.toString()
                r4.d(r6, r5)
                if (r2 == 0) goto L_0x01e1
                r3.close()     // Catch:{ IOException -> 0x044a }
            L_0x01e1:
                if (r2 == 0) goto L_0x01e6
                r1.close()     // Catch:{ IOException -> 0x0464 }
            L_0x01e6:
                if (r0 == 0) goto L_0x005e
                r0.disconnect()     // Catch:{ Exception -> 0x01ed }
                goto L_0x005e
            L_0x01ed:
                r0 = move-exception
                com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
                java.lang.Object[] r2 = new java.lang.Object[r11]
                java.lang.String r3 = "DownloadThread"
                r2[r9] = r3
                java.lang.String r0 = r0.getMessage()
                r2[r10] = r0
                goto L_0x0114
            L_0x0204:
                java.lang.String r4 = r0.getContentType()     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                java.lang.String r6 = "text/html"
                boolean r4 = r4.equals(r6)     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                if (r4 == 0) goto L_0x0275
                com.baidu.mobads.openad.c.a r4 = com.baidu.mobads.openad.c.a.this     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                r4.b()     // Catch:{ Exception -> 0x011d, all -> 0x04b7 }
                com.baidu.mobads.j.m r4 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r4 = r4.f()
                java.lang.String r6 = "DownloadThread"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "Thread["
                java.lang.StringBuilder r7 = r7.append(r8)
                int r8 = r12.f542a
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.String r8 = "] ver("
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.StringBuilder r5 = r7.append(r5)
                java.lang.String r7 = ") executed end; isFinished="
                java.lang.StringBuilder r5 = r5.append(r7)
                boolean r7 = r12.g
                java.lang.StringBuilder r5 = r5.append(r7)
                java.lang.String r5 = r5.toString()
                r4.d(r6, r5)
                if (r2 == 0) goto L_0x0252
                r3.close()     // Catch:{ IOException -> 0x047e }
            L_0x0252:
                if (r2 == 0) goto L_0x0257
                r1.close()     // Catch:{ IOException -> 0x0498 }
            L_0x0257:
                if (r0 == 0) goto L_0x005e
                r0.disconnect()     // Catch:{ Exception -> 0x025e }
                goto L_0x005e
            L_0x025e:
                r0 = move-exception
                com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
                java.lang.Object[] r2 = new java.lang.Object[r11]
                java.lang.String r3 = "DownloadThread"
                r2[r9] = r3
                java.lang.String r0 = r0.getMessage()
                r2[r10] = r0
                goto L_0x0114
            L_0x0275:
                r4 = r0
            L_0x0276:
                java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x04d9, all -> 0x04c2 }
                java.io.InputStream r0 = r4.getInputStream()     // Catch:{ Exception -> 0x04d9, all -> 0x04c2 }
                r3.<init>(r0)     // Catch:{ Exception -> 0x04d9, all -> 0x04c2 }
                int r0 = r12.d     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                int r1 = r12.f     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                int r0 = r0 + r1
                com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                java.lang.String r6 = "DownloadThread"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                r7.<init>()     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                java.lang.String r8 = "tmpStartByte = "
                java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                r1.d(r6, r7)     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                java.lang.String r6 = r12.c     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                java.lang.String r7 = "rw"
                r1.<init>(r6, r7)     // Catch:{ Exception -> 0x04de, all -> 0x04c6 }
                long r6 = (long) r0
                r1.seek(r6)     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                r2 = 102400(0x19000, float:1.43493E-40)
                byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
            L_0x02b6:
                com.baidu.mobads.openad.c.a r6 = com.baidu.mobads.openad.c.a.this     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r6 = r6.i     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r7 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.DOWNLOADING     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                if (r6 != r7) goto L_0x02d1
                r6 = 0
                r7 = 102400(0x19000, float:1.43493E-40)
                int r6 = r3.read(r2, r6, r7)     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                r7 = -1
                if (r6 == r7) goto L_0x02d1
                int r7 = r12.e     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                if (r0 >= r7) goto L_0x02d1
                int r7 = r12.k     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                if (r5 == r7) goto L_0x02e6
            L_0x02d1:
                int r2 = r12.e     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                if (r0 < r2) goto L_0x0017
                r0 = 1
                r12.g = r0     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                goto L_0x0017
            L_0x02da:
                r0 = move-exception
                r2 = r3
                r3 = r4
                goto L_0x0121
            L_0x02df:
                java.net.HttpURLConnection r1 = r12.l     // Catch:{ Exception -> 0x04cf, all -> 0x04b2 }
                r0 = 0
                r12.l = r0     // Catch:{ Exception -> 0x04d4, all -> 0x04bd }
                r4 = r1
                goto L_0x0276
            L_0x02e6:
                r7 = 0
                r1.write(r2, r7, r6)     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                int r7 = r12.f     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                int r7 = r7 + r6
                r12.f = r7     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                int r0 = r0 + r6
                com.baidu.mobads.openad.c.a r7 = com.baidu.mobads.openad.c.a.this     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                r7.a(r6)     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                monitor-enter(r12)     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
                boolean r6 = r12.j     // Catch:{ all -> 0x02fc }
                if (r6 == 0) goto L_0x0349
                monitor-exit(r12)     // Catch:{ all -> 0x02fc }
                goto L_0x02d1
            L_0x02fc:
                r0 = move-exception
                monitor-exit(r12)     // Catch:{ all -> 0x02fc }
                throw r0     // Catch:{ Exception -> 0x02da, all -> 0x02ff }
            L_0x02ff:
                r0 = move-exception
                r2 = r1
            L_0x0301:
                com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
                java.lang.String r6 = "DownloadThread"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "Thread["
                java.lang.StringBuilder r7 = r7.append(r8)
                int r8 = r12.f542a
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.String r8 = "] ver("
                java.lang.StringBuilder r7 = r7.append(r8)
                java.lang.StringBuilder r5 = r7.append(r5)
                java.lang.String r7 = ") executed end; isFinished="
                java.lang.StringBuilder r5 = r5.append(r7)
                boolean r7 = r12.g
                java.lang.StringBuilder r5 = r5.append(r7)
                java.lang.String r5 = r5.toString()
                r1.d(r6, r5)
                if (r2 == 0) goto L_0x033e
                r2.close()     // Catch:{ IOException -> 0x034c }
            L_0x033e:
                if (r3 == 0) goto L_0x0343
                r3.close()     // Catch:{ IOException -> 0x0365 }
            L_0x0343:
                if (r4 == 0) goto L_0x0348
                r4.disconnect()     // Catch:{ Exception -> 0x037e }
            L_0x0348:
                throw r0
            L_0x0349:
                monitor-exit(r12)     // Catch:{ all -> 0x02fc }
                goto L_0x02b6
            L_0x034c:
                r1 = move-exception
                com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
                java.lang.Object[] r5 = new java.lang.Object[r11]
                java.lang.String r6 = "DownloadThread"
                r5[r9] = r6
                java.lang.String r1 = r1.getMessage()
                r5[r10] = r1
                r2.w(r5)
                goto L_0x033e
            L_0x0365:
                r1 = move-exception
                com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
                java.lang.Object[] r3 = new java.lang.Object[r11]
                java.lang.String r5 = "DownloadThread"
                r3[r9] = r5
                java.lang.String r1 = r1.getMessage()
                r3[r10] = r1
                r2.w(r3)
                goto L_0x0343
            L_0x037e:
                r1 = move-exception
                com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
                java.lang.Object[] r3 = new java.lang.Object[r11]
                java.lang.String r4 = "DownloadThread"
                r3[r9] = r4
                java.lang.String r1 = r1.getMessage()
                r3[r10] = r1
                r2.w(r3)
                goto L_0x0348
            L_0x0397:
                r0 = move-exception
                com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
                java.lang.Object[] r4 = new java.lang.Object[r11]
                java.lang.String r5 = "DownloadThread"
                r4[r9] = r5
                java.lang.String r0 = r0.getMessage()
                r4[r10] = r0
                r1.w(r4)
                goto L_0x0178
            L_0x03b1:
                r0 = move-exception
                com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
                java.lang.Object[] r2 = new java.lang.Object[r11]
                java.lang.String r4 = "DownloadThread"
                r2[r9] = r4
                java.lang.String r0 = r0.getMessage()
                r2[r10] = r0
                r1.w(r2)
                goto L_0x017d
            L_0x03cb:
                r0 = move-exception
                com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
                java.lang.Object[] r2 = new java.lang.Object[r11]
                java.lang.String r5 = "DownloadThread"
                r2[r9] = r5
                java.lang.String r0 = r0.getMessage()
                r2[r10] = r0
                r1.w(r2)
                goto L_0x0054
            L_0x03e5:
                r0 = move-exception
                com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
                java.lang.Object[] r2 = new java.lang.Object[r11]
                java.lang.String r3 = "DownloadThread"
                r2[r9] = r3
                java.lang.String r0 = r0.getMessage()
                r2[r10] = r0
                r1.w(r2)
                goto L_0x0059
            L_0x03ff:
                r0 = move-exception
                com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
                java.lang.Object[] r2 = new java.lang.Object[r11]
                java.lang.String r3 = "DownloadThread"
                r2[r9] = r3
                java.lang.String r0 = r0.getMessage()
                r2[r10] = r0
                goto L_0x0114
            L_0x0416:
                r3 = move-exception
                com.baidu.mobads.j.m r4 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r4 = r4.f()
                java.lang.Object[] r5 = new java.lang.Object[r11]
                java.lang.String r6 = "DownloadThread"
                r5[r9] = r6
                java.lang.String r3 = r3.getMessage()
                r5[r10] = r3
                r4.w(r5)
                goto L_0x00f3
            L_0x0430:
                r1 = move-exception
                com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
                java.lang.Object[] r3 = new java.lang.Object[r11]
                java.lang.String r4 = "DownloadThread"
                r3[r9] = r4
                java.lang.String r1 = r1.getMessage()
                r3[r10] = r1
                r2.w(r3)
                goto L_0x00f8
            L_0x044a:
                r3 = move-exception
                com.baidu.mobads.j.m r4 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r4 = r4.f()
                java.lang.Object[] r5 = new java.lang.Object[r11]
                java.lang.String r6 = "DownloadThread"
                r5[r9] = r6
                java.lang.String r3 = r3.getMessage()
                r5[r10] = r3
                r4.w(r5)
                goto L_0x01e1
            L_0x0464:
                r1 = move-exception
                com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
                java.lang.Object[] r3 = new java.lang.Object[r11]
                java.lang.String r4 = "DownloadThread"
                r3[r9] = r4
                java.lang.String r1 = r1.getMessage()
                r3[r10] = r1
                r2.w(r3)
                goto L_0x01e6
            L_0x047e:
                r3 = move-exception
                com.baidu.mobads.j.m r4 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r4 = r4.f()
                java.lang.Object[] r5 = new java.lang.Object[r11]
                java.lang.String r6 = "DownloadThread"
                r5[r9] = r6
                java.lang.String r3 = r3.getMessage()
                r5[r10] = r3
                r4.w(r5)
                goto L_0x0252
            L_0x0498:
                r1 = move-exception
                com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
                com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
                java.lang.Object[] r3 = new java.lang.Object[r11]
                java.lang.String r4 = "DownloadThread"
                r3[r9] = r4
                java.lang.String r1 = r1.getMessage()
                r3[r10] = r1
                r2.w(r3)
                goto L_0x0257
            L_0x04b2:
                r0 = move-exception
                r3 = r2
                r4 = r2
                goto L_0x0301
            L_0x04b7:
                r1 = move-exception
                r3 = r2
                r4 = r0
                r0 = r1
                goto L_0x0301
            L_0x04bd:
                r0 = move-exception
                r3 = r2
                r4 = r1
                goto L_0x0301
            L_0x04c2:
                r0 = move-exception
                r3 = r2
                goto L_0x0301
            L_0x04c6:
                r0 = move-exception
                goto L_0x0301
            L_0x04c9:
                r0 = move-exception
                r4 = r3
                r3 = r2
                r2 = r1
                goto L_0x0301
            L_0x04cf:
                r0 = move-exception
                r1 = r2
                r3 = r2
                goto L_0x0121
            L_0x04d4:
                r0 = move-exception
                r3 = r1
                r1 = r2
                goto L_0x0121
            L_0x04d9:
                r0 = move-exception
                r1 = r2
                r3 = r4
                goto L_0x0121
            L_0x04de:
                r0 = move-exception
                r1 = r2
                r2 = r3
                r3 = r4
                goto L_0x0121
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobads.openad.c.a.C0014a.run():void");
        }
    }

    private HttpURLConnection b(HttpURLConnection httpURLConnection) {
        Exception e2;
        HttpURLConnection httpURLConnection2;
        HttpURLConnection httpURLConnection3 = httpURLConnection;
        while (true) {
            try {
                int responseCode = httpURLConnection3.getResponseCode();
                if (responseCode != 302 && responseCode != 301) {
                    return httpURLConnection3;
                }
                this.b = new URL(httpURLConnection3.getHeaderField("Location"));
                httpURLConnection2 = (HttpURLConnection) this.b.openConnection();
                try {
                    httpURLConnection2.setConnectTimeout(10000);
                    httpURLConnection2.setInstanceFollowRedirects(false);
                    httpURLConnection2.setRequestProperty("Range", "bytes=0-");
                    httpURLConnection3 = httpURLConnection2;
                } catch (Exception e3) {
                    e2 = e3;
                    e2.printStackTrace();
                    return httpURLConnection2;
                }
            } catch (Exception e4) {
                Exception exc = e4;
                httpURLConnection2 = httpURLConnection3;
                e2 = exc;
                e2.printStackTrace();
                return httpURLConnection2;
            }
        }
    }

    public void removeObservers() {
        deleteObservers();
    }

    public String getTargetURL() {
        if (this.c == null) {
            return null;
        }
        return this.c.toString();
    }
}
