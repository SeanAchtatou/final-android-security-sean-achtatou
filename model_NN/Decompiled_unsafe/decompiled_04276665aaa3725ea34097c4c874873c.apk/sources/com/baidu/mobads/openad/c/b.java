package com.baidu.mobads.openad.c;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;
import com.baidu.mobads.AppActivity;
import com.baidu.mobads.command.a;
import com.baidu.mobads.interfaces.download.activate.IXAppInfo;
import com.baidu.mobads.j.j;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class b implements Observer {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static NotificationManager f543a;
    private static int b = 10091;
    private static HashMap<String, b> g = new HashMap<>();
    /* access modifiers changed from: private */
    public a c = null;
    private Context d;
    private com.baidu.mobads.b.b e = null;
    private String f = "";
    private Handler h = new Handler(Looper.getMainLooper());

    public a a() {
        return this.c;
    }

    public static synchronized b a(String str) {
        b bVar;
        synchronized (b.class) {
            bVar = g.get(str);
        }
        return bVar;
    }

    public static synchronized b b(String str) {
        b remove;
        synchronized (b.class) {
            remove = g.remove(str);
        }
        return remove;
    }

    public static synchronized void a(String str, b bVar) {
        synchronized (b.class) {
            g.put(str, bVar);
        }
    }

    public static synchronized int c(String str) {
        int i;
        synchronized (b.class) {
            b bVar = g.get(str);
            if (bVar == null || bVar.a() == null) {
                i = b;
                b = i + 1;
            } else {
                i = bVar.a().f;
            }
        }
        return i;
    }

    public b(Context context, a aVar) {
        m.a().f().d("OAdApkDownloaderObserver", "observer created");
        if (f543a == null) {
            f543a = (NotificationManager) context.getSystemService("notification");
        }
        this.d = context.getApplicationContext();
        this.c = aVar;
        a(this.c.i, this);
    }

    public void update(Observable observable, Object obj) {
        int progress;
        IOAdDownloader iOAdDownloader = (IOAdDownloader) observable;
        this.c.g = iOAdDownloader.getState();
        if (this.c.g == IOAdDownloader.DownloadStatus.DOWNLOADING) {
            if (this.c.d < 0) {
                m.a().f().d("OAdApkDownloaderObserver", "download update---mExtraInfo.contentLength < 0");
                this.c.d = (long) iOAdDownloader.getFileSize();
                this.c.k = iOAdDownloader.getTargetURL();
                this.c.a(this.d);
                this.f = String.format(Locale.CHINA, "%.1fM", Float.valueOf(((float) this.c.d) / 1048576.0f));
            }
            if (iOAdDownloader.getProgress() > 0.0f && (progress = (int) iOAdDownloader.getProgress()) > this.c.e) {
                this.c.e = progress;
                b();
                return;
            }
            return;
        }
        if (this.c.g == IOAdDownloader.DownloadStatus.COMPLETED) {
            m.a().m().sendDownloadAdLog(this.d, this.c.i, 528, this.c.p);
            m.a().f().d("OAdApkDownloaderObserver", "download success-->>" + iOAdDownloader.getOutputPath());
            boolean z = this.c.l;
            m.a().f().d("OAdApkDownloaderObserver", "launch installing .............");
            String str = this.c.c + this.c.b;
            if (!this.c.i.contains(".")) {
                this.c.i = m.a().l().getLocalApkFileInfo(this.d, str).packageName;
            }
            if (this.e == null) {
                com.baidu.mobads.b.a aVar = new com.baidu.mobads.b.a(this.c);
                this.e = new com.baidu.mobads.b.b(this.d, this.c.i, new File(str), z);
                this.e.a(aVar);
                this.e.a();
            }
            com.baidu.mobads.c.a.a().a(this.d, this.c);
            IXAppInfo a2 = com.baidu.mobads.command.a.a.a(this.c);
            if (a2 != null) {
                com.baidu.mobads.production.a.b().getXMonitorActivation(this.d, j.a()).addAppInfoForMonitor(a2);
            }
        } else if (this.c.g == IOAdDownloader.DownloadStatus.ERROR) {
            this.c.k = iOAdDownloader.getTargetURL();
            m.a().f().e("OAdApkDownloaderObserver", "download failed-->>" + iOAdDownloader.getOutputPath());
            com.baidu.mobads.c.a.a().a(this.c);
        } else if (iOAdDownloader.getState() == IOAdDownloader.DownloadStatus.INITING) {
            this.c.q++;
        }
        b();
        this.c.a(this.d);
    }

    public void b() {
        this.h.post(new c(this));
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        Toast.makeText(this.d, str, 0).show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    @TargetApi(16)
    public Notification d() {
        String str = this.c.f469a;
        String str2 = "正在下载 " + this.c.f469a;
        String str3 = "";
        int i = 17301633;
        if (this.c.g == IOAdDownloader.DownloadStatus.COMPLETED) {
            str = this.c.g.getMessage() + ": " + str;
            str3 = " 点击这里安装应用";
            i = 17301634;
        } else if (this.c.g == IOAdDownloader.DownloadStatus.PAUSED) {
            str = this.c.g.getMessage() + ": " + str;
            str2 = "已为您暂停下载， 点击通知栏继续下载";
            str3 = "目前不在wifi网络下， 点击这里继续下载";
            i = 17301634;
        } else if (this.c.g == IOAdDownloader.DownloadStatus.ERROR) {
            str = this.c.g.getMessage() + ": " + str;
            str3 = " 稍后点击这里重新下载";
            i = 17301634;
        } else if (this.c.g == IOAdDownloader.DownloadStatus.DOWNLOADING) {
            str = this.c.g.getMessage() + ": " + str;
            str3 = "下载进度: " + this.c.e + "%  应用大小: " + this.f;
        } else if (this.c.g == IOAdDownloader.DownloadStatus.INITING) {
            str = this.c.g.getMessage() + ": " + str;
            str3 = this.c.g.getMessage();
        }
        Intent intent = new Intent(this.d, AppActivity.class);
        intent.putExtra("dealWithDownload", true);
        intent.putExtra("status", this.c.g.getCode());
        intent.putExtra("pk", this.c.i);
        intent.putExtra("localApkPath", this.c.c + this.c.b);
        intent.putExtra("title", str);
        intent.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
        intent.setAction(Long.toString(System.currentTimeMillis()));
        PendingIntent activity = PendingIntent.getActivity(this.d, this.c.f, intent, NTLMConstants.FLAG_UNIDENTIFIED_10);
        if (Build.VERSION.SDK_INT >= 16) {
            if (this.c.h == null) {
                this.c.h = new Notification.Builder(this.d);
            }
            return ((Notification.Builder) this.c.h).setContentTitle(str).setContentText(str3).setTicker(str2).setSmallIcon(i).setContentIntent(activity).setAutoCancel(true).setProgress(100, this.c.e, false).build();
        }
        if (this.c.h == null) {
            this.c.h = new Notification();
        }
        Notification notification = (Notification) this.c.h;
        notification.icon = i;
        notification.flags |= 16;
        notification.tickerText = str2;
        notification.contentIntent = activity;
        try {
            notification.getClass().getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class).invoke(notification, this.d, str, str3, activity);
            return notification;
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException e2) {
            return notification;
        }
    }
}
