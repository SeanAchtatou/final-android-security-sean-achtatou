package com.baidu.mobads.openad.c;

import android.content.Context;
import com.baidu.mobads.d.a;
import com.baidu.mobads.interfaces.download.IXAdStaticImgDownloader;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.a.b;
import com.baidu.mobads.openad.a.c;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloaderManager;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class d implements IOAdDownloaderManager {
    private static d b = null;

    /* renamed from: a  reason: collision with root package name */
    protected Context f545a;
    private HashMap<String, IOAdDownloader> c = new HashMap<>();
    private c d;
    private AtomicBoolean e = new AtomicBoolean(false);

    protected d(Context context) {
        this.f545a = context;
    }

    public static d a(Context context) {
        if (b == null) {
            b = new d(context);
        }
        return b;
    }

    public Boolean removeAdsApkDownloader(String str) {
        synchronized (this.c) {
            this.c.remove(str);
        }
        return true;
    }

    public IOAdDownloader getAdsApkDownloader(String str) {
        IOAdDownloader iOAdDownloader;
        synchronized (this.c) {
            iOAdDownloader = this.c.get(str);
        }
        return iOAdDownloader;
    }

    public void a(String str, IOAdDownloader iOAdDownloader) {
        synchronized (this.c) {
            this.c.put(str, iOAdDownloader);
        }
    }

    public void removeAllAdsApkDownloaderes() {
        synchronized (this.c) {
            this.c.clear();
        }
    }

    public ArrayList<IOAdDownloader> getAllAdsApkDownloaderes() {
        ArrayList<IOAdDownloader> arrayList = null;
        synchronized (this.c) {
            Collection<IOAdDownloader> values = this.c.values();
            if (values.size() > 0) {
                ArrayList<IOAdDownloader> arrayList2 = new ArrayList<>();
                for (IOAdDownloader add : values) {
                    arrayList2.add(add);
                }
                arrayList = arrayList2;
            }
        }
        return arrayList;
    }

    public synchronized IOAdDownloader createAdsApkDownloader(URL url, String str, String str2, int i, String str3, String str4) {
        a aVar;
        aVar = new a(this.f545a, url, str, str2, i, str3, str4);
        a(str4, aVar);
        try {
            if (this.d == null) {
                this.d = new c(this.f545a);
                this.d.a(new b(this.d));
                this.d.addEventListener("network_changed", new e(this));
                this.d.a();
            }
        } catch (Exception e2) {
            m.a().f().d("OAdDownloadManager", e2);
        }
        return aVar;
    }

    public IOAdDownloader createSimpleFileDownloader(URL url, String str, String str2, boolean z) {
        return new f(this.f545a, url, str, str2, z);
    }

    public IXAdStaticImgDownloader createImgHttpDownloader(URL url, String str, String str2) {
        return new a(this.f545a, url, str, str2);
    }

    public void resumeUndownloadedAfterRestartApp(long j) {
        List<String> a2;
        if (!this.e.getAndSet(true) && (a2 = com.baidu.mobads.command.a.a(this.f545a, j)) != null && a2.size() > 0) {
            int i = 0;
            while (i < a2.size()) {
                try {
                    String str = a2.get(i);
                    if (b.a(str) == null && getAdsApkDownloader(str) == null) {
                        com.baidu.mobads.command.a a3 = com.baidu.mobads.command.a.a(this.f545a, str);
                        if (a3 == null) {
                            m.a().f().d("OAdDownloadManager", "pack[" + str + "] has no local data, continue");
                        } else {
                            IOAdDownloader createAdsApkDownloader = createAdsApkDownloader(new URL(a3.j), a3.c, a3.b, 1, a3.f469a, a3.i);
                            createAdsApkDownloader.addObserver(new b(this.f545a, a3));
                            createAdsApkDownloader.start();
                        }
                    } else {
                        m.a().f().d("OAdDownloadManager", "pack[" + str + "] has been stated before, continue");
                    }
                    i++;
                } catch (Exception e2) {
                    m.a().f().d("OAdDownloadManager", e2);
                    return;
                }
            }
        }
    }
}
