package com.baidu.mobads.openad.e;

import android.os.Build;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.d.d;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

class b extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f554a;
    final /* synthetic */ double b;
    final /* synthetic */ a c;

    b(a aVar, d dVar, double d) {
        this.c = aVar;
        this.f554a = dVar;
        this.b = d;
    }

    public void run() {
        List<String> list;
        InputStream inputStream = null;
        try {
            if (this.f554a.c > 0) {
                Thread.sleep(this.f554a.c);
            }
            this.c.e.set(true);
            HttpURLConnection unused = this.c.g = (HttpURLConnection) new URL(this.f554a.f556a).openConnection();
            this.c.g.setConnectTimeout((int) this.b);
            this.c.g.setUseCaches(false);
            if (this.f554a.b != null && this.f554a.b.length() > 0) {
                this.c.g.setRequestProperty("User-Agent", this.f554a.b);
            }
            this.c.g.setRequestProperty("Content-type", this.f554a.d);
            this.c.g.setRequestProperty("Connection", "keep-alive");
            this.c.g.setRequestProperty("Cache-Control", "no-cache");
            if (Integer.parseInt(Build.VERSION.SDK) < 8) {
                System.setProperty("http.keepAlive", "false");
            }
            if (a.b != null) {
                String str = "";
                if (this.c.d != null) {
                    str = this.c.d;
                }
                String a2 = a.b.a(this.f554a.f556a);
                if (a2 != null) {
                    str = str + MiPushClient.ACCEPT_TIME_SEPARATOR + a2;
                }
                if (str.length() > 0) {
                    this.c.g.setRequestProperty("Cookie", str);
                }
            }
            if (this.f554a.e == 1) {
                this.c.g.setRequestMethod("GET");
                this.c.g.connect();
                inputStream = this.c.g.getInputStream();
                Map<String, List<String>> headerFields = this.c.g.getHeaderFields();
                if (headerFields.containsKey("Set-Cookie") && (list = headerFields.get("Set-Cookie")) != null) {
                    for (String a3 : list) {
                        a.b.a(this.f554a.f556a, a3);
                    }
                }
                if (!this.c.f.booleanValue()) {
                    this.c.dispatchEvent(new d("URLLoader.Load.Complete", a.b(inputStream), this.f554a.a()));
                }
            } else if (this.f554a.e == 0) {
                this.c.g.setRequestMethod("POST");
                this.c.g.setDoInput(true);
                this.c.g.setDoOutput(true);
                if (this.f554a.b() != null) {
                    String encodedQuery = this.f554a.b().build().getEncodedQuery();
                    OutputStream outputStream = this.c.g.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    bufferedWriter.write(encodedQuery);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                }
                this.c.g.connect();
                this.c.g.getResponseCode();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    m.a().f().e("OAdURLLoader", e.getMessage());
                }
                try {
                    this.c.g.disconnect();
                } catch (Exception e2) {
                }
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    m.a().f().e("OAdURLLoader", e3.getMessage());
                }
                try {
                    this.c.g.disconnect();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
    }
}
