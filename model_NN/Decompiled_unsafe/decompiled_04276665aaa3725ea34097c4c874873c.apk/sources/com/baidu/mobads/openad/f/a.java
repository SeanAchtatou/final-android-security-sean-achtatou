package com.baidu.mobads.openad.f;

import cn.banshenggua.aichang.player.PlayerService;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.utils.IOAdTimer;
import com.qq.e.comm.constants.ErrorCode;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicInteger;

public class a implements IOAdTimer {
    private static String c = "OAdTimer";

    /* renamed from: a  reason: collision with root package name */
    protected int f557a;
    /* access modifiers changed from: private */
    public IOAdTimer.EventHandler b;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    private Timer g;
    /* access modifiers changed from: private */
    public AtomicInteger h;

    static /* synthetic */ int f(a aVar) {
        int i = aVar.e;
        aVar.e = i - 1;
        return i;
    }

    public a(int i) {
        this(i, ErrorCode.InitError.INIT_AD_ERROR);
    }

    public a(int i, int i2) {
        this.f557a = ErrorCode.InitError.INIT_AD_ERROR;
        this.f557a = i2;
        int i3 = i / this.f557a;
        m.a().f().i(c, "RendererTimer(duration=" + i3 + ")");
        this.d = i3;
        this.e = i3;
        this.g = new Timer();
        this.h = new AtomicInteger(-1);
    }

    public void setEventHandler(IOAdTimer.EventHandler eventHandler) {
        this.b = eventHandler;
    }

    public void start() {
        m.a().f().i(c, "start");
        this.h.set(0);
        this.g.scheduleAtFixedRate(new b(this), 0, (long) this.f557a);
    }

    public void stop() {
        m.a().f().i(c, PlayerService.ACTION_STOP);
        this.h.set(2);
        if (this.g != null) {
            this.g.purge();
            this.g.cancel();
            this.g = null;
        }
    }

    public void pause() {
        m.a().f().i(c, "pause");
        this.h.set(1);
    }

    public void resume() {
        m.a().f().i(c, "resume");
        this.h.set(0);
    }

    public int getCurrentCount() {
        return this.f;
    }

    public int getRepeatCount() {
        return this.d;
    }

    public void reset() {
        m.a().f().i(c, "reset");
        this.h.set(-1);
        this.e = this.d;
    }
}
