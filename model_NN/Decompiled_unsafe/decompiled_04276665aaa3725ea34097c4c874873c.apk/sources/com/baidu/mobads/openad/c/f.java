package com.baidu.mobads.openad.c;

import android.content.Context;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;
import java.net.URL;
import java.util.Observable;

public class f extends Observable implements IOAdDownloader, Runnable {

    /* renamed from: a  reason: collision with root package name */
    protected Context f547a;
    protected URL b;
    protected String c;
    protected String d;
    protected int e;
    protected IOAdDownloader.DownloadStatus f;
    protected int g;
    protected int h;
    private boolean i = false;

    protected f(Context context, URL url, String str, String str2, boolean z) {
        this.f547a = context;
        this.b = url;
        this.c = str;
        this.i = z;
        if (str2 == null || str2.trim().length() <= 0) {
            String file = url.getFile();
            this.d = file.substring(file.lastIndexOf(47) + 1);
        } else {
            this.d = str2;
        }
        this.e = -1;
        this.f = IOAdDownloader.DownloadStatus.DOWNLOADING;
        this.g = 0;
        this.h = 0;
    }

    @Deprecated
    public void pause() {
    }

    @Deprecated
    public void cancel() {
    }

    @Deprecated
    public void resume() {
    }

    public void start() {
        a(IOAdDownloader.DownloadStatus.DOWNLOADING);
        b();
    }

    public String getURL() {
        return this.b.toString();
    }

    public int getFileSize() {
        return this.e;
    }

    public float getProgress() {
        return Math.abs((((float) this.g) / ((float) this.e)) * 100.0f);
    }

    public IOAdDownloader.DownloadStatus getState() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public void a(IOAdDownloader.DownloadStatus downloadStatus) {
        this.f = downloadStatus;
        c();
    }

    /* access modifiers changed from: protected */
    public void b() {
        new Thread(this).start();
    }

    /* access modifiers changed from: protected */
    public void a(int i2, float f2) {
        this.g += i2;
        c();
    }

    /* access modifiers changed from: protected */
    public void c() {
        setChanged();
        notifyObservers();
    }

    public String getOutputPath() {
        return this.c + this.d;
    }

    private void d() {
        a(IOAdDownloader.DownloadStatus.ERROR);
    }

    /* JADX WARNING: Removed duplicated region for block: B:122:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00bd A[SYNTHETIC, Splitter:B:37:0x00bd] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00c2 A[SYNTHETIC, Splitter:B:40:0x00c2] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00c7 A[SYNTHETIC, Splitter:B:43:0x00c7] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0102 A[SYNTHETIC, Splitter:B:69:0x0102] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0107 A[SYNTHETIC, Splitter:B:72:0x0107] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x010c A[SYNTHETIC, Splitter:B:75:0x010c] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0111  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r13 = this;
            r2 = 0
            r11 = 2
            r10 = 1
            r5 = 0
            java.net.URL r0 = r13.b     // Catch:{ Exception -> 0x021f, all -> 0x00fc }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x021f, all -> 0x00fc }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x021f, all -> 0x00fc }
            r1 = 10000(0x2710, float:1.4013E-41)
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            r1 = 1
            r0.setInstanceFollowRedirects(r1)     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            r0.connect()     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            int r1 = r1 / 100
            if (r1 == r11) goto L_0x0023
            r13.d()     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
        L_0x0023:
            int r1 = r0.getContentLength()     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            if (r1 <= 0) goto L_0x002b
            r13.e = r1     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
        L_0x002b:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            java.lang.String r3 = r13.c     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            boolean r3 = r1.exists()     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            if (r3 != 0) goto L_0x003b
            r1.mkdirs()     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
        L_0x003b:
            java.io.BufferedInputStream r4 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0225, all -> 0x01fc }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x022c, all -> 0x0204 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x022c, all -> 0x0204 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x022c, all -> 0x0204 }
            r6.<init>()     // Catch:{ Exception -> 0x022c, all -> 0x0204 }
            java.lang.String r7 = r13.getOutputPath()     // Catch:{ Exception -> 0x022c, all -> 0x0204 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x022c, all -> 0x0204 }
            java.lang.String r7 = ".tmp"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x022c, all -> 0x0204 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x022c, all -> 0x0204 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x022c, all -> 0x0204 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x022c, all -> 0x0204 }
            r1 = 10240(0x2800, float:1.4349E-41)
            byte[] r6 = new byte[r1]     // Catch:{ Exception -> 0x0233, all -> 0x020b }
            boolean r1 = r13.i     // Catch:{ Exception -> 0x0233, all -> 0x020b }
            if (r1 == 0) goto L_0x023b
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0233, all -> 0x020b }
            r1.<init>()     // Catch:{ Exception -> 0x0233, all -> 0x020b }
        L_0x0072:
            r2 = r5
        L_0x0073:
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r7 = r13.f     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r8 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.DOWNLOADING     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            if (r7 != r8) goto L_0x00d0
            r7 = 0
            r8 = 10240(0x2800, float:1.4349E-41)
            int r7 = r4.read(r6, r7, r8)     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            r8 = -1
            if (r7 == r8) goto L_0x00d0
            r8 = 0
            r3.write(r6, r8, r7)     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            if (r1 == 0) goto L_0x008d
            r8 = 0
            r1.write(r6, r8, r7)     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
        L_0x008d:
            int r2 = r2 + r7
            float r8 = (float) r2     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            int r9 = r13.e     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            float r9 = (float) r9     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            float r8 = r8 / r9
            r13.a(r7, r8)     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            goto L_0x0073
        L_0x0097:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r3
            r3 = r4
            r4 = r0
            r0 = r12
        L_0x009e:
            com.baidu.mobads.j.m r6 = com.baidu.mobads.j.m.a()     // Catch:{ all -> 0x0218 }
            com.baidu.mobads.interfaces.utils.IXAdLogger r6 = r6.f()     // Catch:{ all -> 0x0218 }
            r7 = 2
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0218 }
            r8 = 0
            java.lang.String r9 = "OAdSimpleFileDownloader"
            r7[r8] = r9     // Catch:{ all -> 0x0218 }
            r8 = 1
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0218 }
            r7[r8] = r0     // Catch:{ all -> 0x0218 }
            r6.e(r7)     // Catch:{ all -> 0x0218 }
            r13.d()     // Catch:{ all -> 0x0218 }
            if (r1 == 0) goto L_0x00c0
            r1.close()     // Catch:{ IOException -> 0x0160 }
        L_0x00c0:
            if (r2 == 0) goto L_0x00c5
            r2.close()     // Catch:{ IOException -> 0x017a }
        L_0x00c5:
            if (r3 == 0) goto L_0x00ca
            r3.close()     // Catch:{ IOException -> 0x0194 }
        L_0x00ca:
            if (r4 == 0) goto L_0x00cf
            r4.disconnect()
        L_0x00cf:
            return
        L_0x00d0:
            if (r1 == 0) goto L_0x00d2
        L_0x00d2:
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = r13.f     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r6 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.DOWNLOADING     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            if (r2 != r6) goto L_0x00f5
            r13.a()     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.COMPLETED     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            r13.a(r2)     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
        L_0x00e0:
            if (r3 == 0) goto L_0x00e5
            r3.close()     // Catch:{ IOException -> 0x01ae }
        L_0x00e5:
            if (r1 == 0) goto L_0x00ea
            r1.close()     // Catch:{ IOException -> 0x01c8 }
        L_0x00ea:
            if (r4 == 0) goto L_0x00ef
            r4.close()     // Catch:{ IOException -> 0x01e2 }
        L_0x00ef:
            if (r0 == 0) goto L_0x00cf
            r0.disconnect()
            goto L_0x00cf
        L_0x00f5:
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r2 = r13.f     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            com.baidu.mobads.openad.interfaces.download.IOAdDownloader$DownloadStatus r6 = com.baidu.mobads.openad.interfaces.download.IOAdDownloader.DownloadStatus.ERROR     // Catch:{ Exception -> 0x0097, all -> 0x0211 }
            if (r2 != r6) goto L_0x00e0
            goto L_0x00e0
        L_0x00fc:
            r0 = move-exception
            r3 = r2
            r4 = r2
            r1 = r2
        L_0x0100:
            if (r3 == 0) goto L_0x0105
            r3.close()     // Catch:{ IOException -> 0x0115 }
        L_0x0105:
            if (r2 == 0) goto L_0x010a
            r2.close()     // Catch:{ IOException -> 0x012e }
        L_0x010a:
            if (r4 == 0) goto L_0x010f
            r4.close()     // Catch:{ IOException -> 0x0147 }
        L_0x010f:
            if (r1 == 0) goto L_0x0114
            r1.disconnect()
        L_0x0114:
            throw r0
        L_0x0115:
            r3 = move-exception
            com.baidu.mobads.j.m r6 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r6 = r6.f()
            java.lang.Object[] r7 = new java.lang.Object[r11]
            java.lang.String r8 = "OAdSimpleFileDownloader"
            r7[r5] = r8
            java.lang.String r3 = r3.getMessage()
            r7[r10] = r3
            r6.e(r7)
            goto L_0x0105
        L_0x012e:
            r2 = move-exception
            com.baidu.mobads.j.m r3 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r3 = r3.f()
            java.lang.Object[] r6 = new java.lang.Object[r11]
            java.lang.String r7 = "OAdSimpleFileDownloader"
            r6[r5] = r7
            java.lang.String r2 = r2.getMessage()
            r6[r10] = r2
            r3.e(r6)
            goto L_0x010a
        L_0x0147:
            r2 = move-exception
            com.baidu.mobads.j.m r3 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r3 = r3.f()
            java.lang.Object[] r4 = new java.lang.Object[r11]
            java.lang.String r6 = "OAdSimpleFileDownloader"
            r4[r5] = r6
            java.lang.String r2 = r2.getMessage()
            r4[r10] = r2
            r3.e(r4)
            goto L_0x010f
        L_0x0160:
            r0 = move-exception
            com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
            java.lang.Object[] r6 = new java.lang.Object[r11]
            java.lang.String r7 = "OAdSimpleFileDownloader"
            r6[r5] = r7
            java.lang.String r0 = r0.getMessage()
            r6[r10] = r0
            r1.e(r6)
            goto L_0x00c0
        L_0x017a:
            r0 = move-exception
            com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
            java.lang.Object[] r2 = new java.lang.Object[r11]
            java.lang.String r6 = "OAdSimpleFileDownloader"
            r2[r5] = r6
            java.lang.String r0 = r0.getMessage()
            r2[r10] = r0
            r1.e(r2)
            goto L_0x00c5
        L_0x0194:
            r0 = move-exception
            com.baidu.mobads.j.m r1 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r1 = r1.f()
            java.lang.Object[] r2 = new java.lang.Object[r11]
            java.lang.String r3 = "OAdSimpleFileDownloader"
            r2[r5] = r3
            java.lang.String r0 = r0.getMessage()
            r2[r10] = r0
            r1.e(r2)
            goto L_0x00ca
        L_0x01ae:
            r2 = move-exception
            com.baidu.mobads.j.m r3 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r3 = r3.f()
            java.lang.Object[] r6 = new java.lang.Object[r11]
            java.lang.String r7 = "OAdSimpleFileDownloader"
            r6[r5] = r7
            java.lang.String r2 = r2.getMessage()
            r6[r10] = r2
            r3.e(r6)
            goto L_0x00e5
        L_0x01c8:
            r1 = move-exception
            com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
            java.lang.Object[] r3 = new java.lang.Object[r11]
            java.lang.String r6 = "OAdSimpleFileDownloader"
            r3[r5] = r6
            java.lang.String r1 = r1.getMessage()
            r3[r10] = r1
            r2.e(r3)
            goto L_0x00ea
        L_0x01e2:
            r1 = move-exception
            com.baidu.mobads.j.m r2 = com.baidu.mobads.j.m.a()
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r2.f()
            java.lang.Object[] r3 = new java.lang.Object[r11]
            java.lang.String r4 = "OAdSimpleFileDownloader"
            r3[r5] = r4
            java.lang.String r1 = r1.getMessage()
            r3[r10] = r1
            r2.e(r3)
            goto L_0x00ef
        L_0x01fc:
            r1 = move-exception
            r3 = r2
            r4 = r2
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0100
        L_0x0204:
            r1 = move-exception
            r3 = r2
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0100
        L_0x020b:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x0100
        L_0x0211:
            r2 = move-exception
            r12 = r2
            r2 = r1
            r1 = r0
            r0 = r12
            goto L_0x0100
        L_0x0218:
            r0 = move-exception
            r12 = r1
            r1 = r4
            r4 = r3
            r3 = r12
            goto L_0x0100
        L_0x021f:
            r0 = move-exception
            r1 = r2
            r3 = r2
            r4 = r2
            goto L_0x009e
        L_0x0225:
            r1 = move-exception
            r3 = r2
            r4 = r0
            r0 = r1
            r1 = r2
            goto L_0x009e
        L_0x022c:
            r1 = move-exception
            r3 = r4
            r4 = r0
            r0 = r1
            r1 = r2
            goto L_0x009e
        L_0x0233:
            r1 = move-exception
            r12 = r1
            r1 = r3
            r3 = r4
            r4 = r0
            r0 = r12
            goto L_0x009e
        L_0x023b:
            r1 = r2
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobads.openad.c.f.run():void");
    }

    /* access modifiers changed from: protected */
    public void a() {
        m.a().k().renameFile(this.c + this.d + ".tmp", this.c + this.d);
    }

    @Deprecated
    public String getTitle() {
        return null;
    }

    @Deprecated
    public String getPackageName() {
        return null;
    }

    public void removeObservers() {
    }

    @Deprecated
    public String getTargetURL() {
        return null;
    }
}
