package com.baidu.mobads.openad.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.igexin.sdk.PushConsts;

public class b extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private c f537a;

    public b(c cVar) {
        this.f537a = cVar;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(PushConsts.ACTION_BROADCAST_NETWORK_CHANGE)) {
            this.f537a.b();
        }
    }
}
