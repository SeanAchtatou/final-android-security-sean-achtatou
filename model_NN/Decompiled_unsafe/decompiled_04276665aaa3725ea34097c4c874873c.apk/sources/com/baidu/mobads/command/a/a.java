package com.baidu.mobads.command.a;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;
import com.baidu.mobads.command.b;
import com.baidu.mobads.interfaces.IXAdContainerContext;
import com.baidu.mobads.interfaces.IXAdContainerFactory;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.IXAdResource;
import com.baidu.mobads.interfaces.IXNonLinearAdSlot;
import com.baidu.mobads.interfaces.download.activate.IXAppInfo;
import com.baidu.mobads.interfaces.utils.IXAdIOUtils;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.baidu.mobads.interfaces.utils.IXAdURIUitls;
import com.baidu.mobads.j.d;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.download.IOAdDownloader;
import java.io.File;
import java.net.URL;

public class a extends b {
    public a(IXNonLinearAdSlot iXNonLinearAdSlot, IXAdInstanceInfo iXAdInstanceInfo, IXAdResource iXAdResource) {
        super(iXNonLinearAdSlot, iXAdInstanceInfo, iXAdResource);
    }

    public void a() {
        String str;
        String str2;
        com.baidu.mobads.command.a aVar;
        boolean z = false;
        d m = m.a().m();
        IXAdIOUtils k = m.a().k();
        IXAdURIUitls i = m.a().i();
        IXAdSystemUtils n = m.a().n();
        IXAdContainerContext adContainerContext = this.b.getCurrentXAdContainer().getAdContainerContext();
        try {
            String appPackageName = this.c.getAppPackageName();
            this.e.i("XAdDownloadAPKCommand", "download pkg = " + appPackageName);
            if ((appPackageName == null || appPackageName.equals("")) && !TextUtils.isEmpty(this.c.getOriginClickUrl())) {
                this.e.i("XAdDownloadAPKCommand", "start to download but package is empty");
                str = m.getMD5(this.c.getOriginClickUrl());
            } else {
                str = appPackageName;
            }
            IOAdDownloader adsApkDownloader = com.baidu.mobads.openad.c.d.a(this.f470a).getAdsApkDownloader(str);
            com.baidu.mobads.openad.c.b a2 = com.baidu.mobads.openad.c.b.a(str);
            if (a2 == null || adsApkDownloader == null) {
                if (adsApkDownloader != null) {
                    adsApkDownloader.cancel();
                    adsApkDownloader.removeObservers();
                }
                com.baidu.mobads.openad.c.b.b(str);
                com.baidu.mobads.openad.c.d.a(this.f470a).removeAdsApkDownloader(str);
            } else {
                com.baidu.mobads.command.a a3 = a2.a();
                IOAdDownloader.DownloadStatus state = adsApkDownloader.getState();
                this.e.d("XAdDownloadAPKCommand", "startDownload>> downloader exist: state=" + state);
                if (state == IOAdDownloader.DownloadStatus.CANCELLED || state == IOAdDownloader.DownloadStatus.ERROR || state == IOAdDownloader.DownloadStatus.PAUSED) {
                    adsApkDownloader.resume();
                    i.pintHttpInNewThread(this.c.getClickThroughUrl());
                    return;
                } else if (state == IOAdDownloader.DownloadStatus.COMPLETED) {
                    if (a(this.f470a, a3)) {
                        i.pintHttpInNewThread(this.c.getClickThroughUrl());
                        b(a3);
                        return;
                    }
                    adsApkDownloader.cancel();
                    adsApkDownloader.removeObservers();
                    com.baidu.mobads.openad.c.b.b(str);
                    com.baidu.mobads.openad.c.d.a(this.f470a).removeAdsApkDownloader(str);
                } else if (state == IOAdDownloader.DownloadStatus.DOWNLOADING || state == IOAdDownloader.DownloadStatus.INITING) {
                    m.sendDownloadAdLog(this.f470a, "downloading", 529, this.b.getProdInfo().getProdType());
                    Toast.makeText(this.f470a, adsApkDownloader.getTitle() + adsApkDownloader.getState().getMessage(), 0).show();
                    return;
                }
            }
            com.baidu.mobads.command.a a4 = com.baidu.mobads.command.a.a(this.f470a, str);
            if (a4 != null) {
                if (a4.g != IOAdDownloader.DownloadStatus.COMPLETED || !a(this.f470a, a4)) {
                    i.pintHttpInNewThread(this.c.getClickThroughUrl());
                    aVar = a4;
                } else {
                    b(a4);
                    return;
                }
            } else if (b()) {
                m.sendDownloadAdLog(this.f470a, "alreadyinstalled1", 529, this.b.getProdInfo().getProdType());
                m.a().l().openApp(this.f470a, this.c.getAppPackageName());
                i.pintHttpInNewThread(this.c.getClickThroughUrl());
                com.baidu.mobads.production.a.b().getXMonitorActivation(this.f470a, this.e).startMonitor();
                return;
            } else {
                String appName = this.c.getAppName();
                if ((appName == null || appName.equals("")) && ((appName = this.c.getTitle()) == null || appName.equals(""))) {
                    str2 = "您点击的应用";
                } else {
                    str2 = appName;
                }
                com.baidu.mobads.command.a aVar2 = new com.baidu.mobads.command.a(str, str2);
                aVar2.a(this.c.getQueryKey(), this.c.getAdId(), this.c.getClickThroughUrl(), this.c.isAutoOpen());
                aVar2.a(m.getMD5(aVar2.j) + ".apk", k.getStoreagePath(this.f470a));
                aVar2.b(this.b.getAdRequestInfo().getApid(), this.b.getProdInfo().getProdType());
                aVar2.f = com.baidu.mobads.openad.c.b.c(str);
                if (!this.c.isActionOnlyWifi()) {
                    z = true;
                }
                aVar2.r = z;
                aVar2.a(System.currentTimeMillis());
                aVar2.b(this.c.getAppSize());
                aVar2.a(this.c.isTooLarge());
                aVar = aVar2;
            }
            aVar.s = System.currentTimeMillis();
            IOAdDownloader createAdsApkDownloader = adContainerContext.getDownloaderManager(this.f470a).createAdsApkDownloader(new URL(aVar.j), aVar.c, aVar.b, 3, aVar.f469a, aVar.i);
            if (this.c.getAPOOpen() && this.c.getPage() != null && !this.c.getPage().equals("")) {
                aVar.v = true;
                aVar.w = this.c.getPage();
            }
            createAdsApkDownloader.addObserver(new com.baidu.mobads.openad.c.b(this.f470a, aVar));
            if (aVar.r || !n.is3GConnected(this.f470a).booleanValue()) {
                m.sendDownloadAdLog(this.f470a, aVar.i, 527, this.b.getProdInfo().getProdType());
                createAdsApkDownloader.start();
                return;
            }
            m.sendDownloadAdLog(this.f470a, "waitwifi", 529, this.b.getProdInfo().getProdType());
            createAdsApkDownloader.pause();
            Toast.makeText(this.f470a, createAdsApkDownloader.getTitle() + " 将在连入Wifi后开始下载", 0).show();
        } catch (Exception e) {
            this.e.e("XAdDownloadAPKCommand", e);
            com.baidu.mobads.c.a.a().a("ad app download failed: " + e.toString());
        }
    }

    private boolean b() {
        return m.a().l().isInstalled(this.f470a, this.c.getAppPackageName());
    }

    /* access modifiers changed from: protected */
    public boolean a(Context context, com.baidu.mobads.command.a aVar) {
        boolean isInstalled = m.a().l().isInstalled(context, aVar.i);
        d m = m.a().m();
        if (isInstalled) {
            m.sendDownloadAdLog(this.f470a, "alreadyinstalled", 529, this.b.getProdInfo().getProdType());
            m.a().l().openApp(context, aVar.i);
            return true;
        }
        m.sendDownloadAdLog(this.f470a, "alreadydownloaded", 529, this.b.getProdInfo().getProdType());
        String str = aVar.c + aVar.b;
        File file = new File(str);
        if (!file.exists() || file.length() <= 0) {
            return false;
        }
        context.startActivity(m.a().l().getInstallIntent(str));
        return true;
    }

    private void b(com.baidu.mobads.command.a aVar) {
        if (com.baidu.mobads.production.a.b() != null) {
            IXAppInfo a2 = a(aVar);
            if (a2 != null) {
                com.baidu.mobads.production.a.b().getXMonitorActivation(this.f470a, this.e).addAppInfoForMonitor(a2);
            } else {
                this.e.e("addAppInfoForMonitor error, appInfo is null");
            }
        }
    }

    public static IXAppInfo a(com.baidu.mobads.command.a aVar) {
        IXAdContainerFactory b;
        if (aVar == null || (b = com.baidu.mobads.production.a.b()) == null) {
            return null;
        }
        IXAppInfo createAppInfo = b.createAppInfo();
        createAppInfo.setAdId(aVar.g());
        createAppInfo.setAppSize(aVar.e());
        createAppInfo.setClickTime(aVar.c());
        createAppInfo.setPackageName(aVar.d());
        createAppInfo.setQk(aVar.h());
        createAppInfo.setProd(aVar.i());
        createAppInfo.setTooLarge(aVar.f());
        return createAppInfo;
    }
}
