package com.baidu.mobads.command;

import android.os.Parcel;
import android.os.Parcelable;

final class c implements Parcelable.Creator<XAdLandingPageExtraInfo> {
    c() {
    }

    /* renamed from: a */
    public XAdLandingPageExtraInfo createFromParcel(Parcel parcel) {
        return new XAdLandingPageExtraInfo(parcel, (c) null);
    }

    /* renamed from: a */
    public XAdLandingPageExtraInfo[] newArray(int i) {
        return new XAdLandingPageExtraInfo[i];
    }
}
