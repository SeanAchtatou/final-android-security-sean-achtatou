package com.baidu.mobads.vo.a;

import android.content.Context;
import com.baidu.mobads.command.XAdCommandExtraInfo;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.baidu.mobads.j.d;
import com.baidu.mobads.j.m;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.net.URLEncoder;
import java.util.HashMap;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    public long f601a;
    public String b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    protected Context l;
    protected d m;
    protected IXAdSystemUtils n;

    /* access modifiers changed from: protected */
    public abstract HashMap<String, String> b();

    public a(XAdCommandExtraInfo xAdCommandExtraInfo) {
        this(xAdCommandExtraInfo.getAdInstanceInfo().getAdId(), xAdCommandExtraInfo.getAdInstanceInfo().getQueryKey(), xAdCommandExtraInfo.mProdType);
    }

    public a(String str, String str2, String str3) {
        this.b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
        this.c = "";
        this.d = "";
        this.e = "";
        this.f = "";
        this.g = "";
        this.h = "";
        this.j = "";
        this.k = "";
        this.m = m.a().m();
        this.n = m.a().n();
        this.l = m.a().d();
        this.f601a = System.currentTimeMillis();
        this.b = str;
        this.c = str2;
        this.e = this.m.getAppSec(this.l);
        if (this.l != null) {
            this.d = this.l.getPackageName();
        }
        this.f = this.m.getAppId(this.l);
        this.h = this.n.getEncodedSN(this.l);
        this.i = "android";
        this.g = "android_" + com.baidu.mobads.a.a.c + "_" + "4.1.30";
        this.j = str3;
        this.k = m.a().n().getCUID(this.l);
    }

    /* access modifiers changed from: protected */
    public HashMap<String, String> a() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("adid", this.b);
        hashMap.put("appsec", this.e);
        hashMap.put("appsid", this.f);
        hashMap.put("pack", this.d);
        hashMap.put("qk", this.c);
        hashMap.put(IXAdRequestInfo.SN, this.h);
        hashMap.put("ts", "" + this.f601a);
        hashMap.put("v", this.g);
        hashMap.put(IXAdRequestInfo.OS, this.i);
        hashMap.put("prod", this.j);
        hashMap.put("cuid", this.k);
        hashMap.put(IXAdRequestInfo.P_VER, "8.30");
        return hashMap;
    }

    public HashMap<String, String> c() {
        HashMap<String, String> a2 = a();
        HashMap<String, String> b2 = b();
        if (b2 != null) {
            a2.putAll(b2);
        }
        return a2;
    }

    public String toString() {
        return a(c());
    }

    /* access modifiers changed from: protected */
    public String a(HashMap<String, String> hashMap) {
        StringBuilder sb = new StringBuilder();
        try {
            d m2 = m.a().m();
            StringBuilder sb2 = new StringBuilder();
            for (String next : hashMap.keySet()) {
                String str = hashMap.get(next);
                if (!(next == null || str == null)) {
                    String a2 = a(next);
                    String a3 = a(str);
                    sb.append(a2 + "=" + a3 + "&");
                    sb2.append(a3 + MiPushClient.ACCEPT_TIME_SEPARATOR);
                }
            }
            sb2.append("mobads,");
            sb.append("vd=" + m2.getMD5(sb2.toString()) + "&");
            return sb.toString();
        } catch (Exception e2) {
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public String a(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8").replaceAll("\\+", "%20").replaceAll("\\%21", "!").replaceAll("\\%27", "'").replaceAll("\\%28", "(").replaceAll("\\%29", ")").replaceAll("\\%7E", "~");
        } catch (Exception e2) {
            return str;
        }
    }
}
