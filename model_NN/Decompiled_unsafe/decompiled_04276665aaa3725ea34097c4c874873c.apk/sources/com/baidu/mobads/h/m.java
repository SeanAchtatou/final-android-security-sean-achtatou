package com.baidu.mobads.h;

import android.util.Log;
import com.baidu.mobads.a.b;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;
import com.baidu.mobads.openad.interfaces.event.IOAdEventListener;

class m implements IOAdEventListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ double f497a;
    final /* synthetic */ l b;

    m(l lVar, double d) {
        this.b = lVar;
        this.f497a = d;
    }

    public void run(IOAdEvent iOAdEvent) {
        boolean z;
        boolean z2 = true;
        this.b.f496a.b.l();
        if ("URLLoader.Load.Complete".equals(iOAdEvent.getType())) {
            e unused = this.b.f496a.b.j = new e((String) iOAdEvent.getData().get("message"));
            double a2 = b.a();
            float f = this.b.f496a.b.j().getFloat("__badApkVersion__8.30", 0.0f);
            if (((float) this.b.f496a.b.j.b()) == f) {
                z = true;
            } else {
                z = false;
            }
            Boolean valueOf = Boolean.valueOf(z);
            if (a2 >= this.b.f496a.b.j.b() || Math.floor(a2) != Math.floor(this.b.f496a.b.j.b())) {
                z2 = false;
            }
            Boolean valueOf2 = Boolean.valueOf(z2);
            Log.i("XAdApkLoader", "try to download apk badVer=" + f + ", isBad=" + valueOf + ", compatible=" + valueOf2);
            if (this.f497a < this.b.f496a.b.j.b() && this.b.f496a.b.j != null && this.b.f496a.b.j.a().booleanValue() && valueOf2.booleanValue() && !valueOf.booleanValue()) {
                this.b.f496a.b.a(this.b.f496a.b.j);
            }
        }
    }
}
