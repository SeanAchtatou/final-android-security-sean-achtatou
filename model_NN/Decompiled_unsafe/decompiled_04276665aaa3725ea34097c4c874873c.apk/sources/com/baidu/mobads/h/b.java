package com.baidu.mobads.h;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import com.baidu.mobads.h.g;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.j.m;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FilenameFilter;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;

public class b extends File {

    /* renamed from: a  reason: collision with root package name */
    private e f487a;
    private Class<?> b;
    private Context c;
    private PublicKey d;
    private IXAdLogger e;

    public b(String str, Context context) {
        this(str, context, null);
    }

    public b(String str, Context context, e eVar) {
        super(str);
        this.b = null;
        this.c = null;
        this.e = m.a().f();
        this.c = context;
        this.f487a = eVar;
        if (eVar != null) {
            try {
                this.d = c("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBcp8gg3O7bjdnz+pSxg+JH/mbcKfm7dEjcRqVNAFwG7bTpLwDQh40bZJzrcBKQWbD6kArR6TPuQUCMQ09/y55Vk1P2Kq7vJGGisFpjlqv2qlg8drLdhXkLQUt/SeZVJgT+CNxVbuzxAF61EEf8M0MHi1I2dm6n6lOA6fomiCD9wIDAQAB");
            } catch (Exception e2) {
                this.d = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.f487a != null) {
            String a2 = a(new File(getAbsolutePath()));
            String b2 = b(this.f487a.d());
            if (!b2.equalsIgnoreCase(a2)) {
                throw new g.a("doCheckApkIntegrity failed, md5sum: " + a2 + ", checksum in json info: " + b2);
            }
            return;
        }
        Log.i("XAdLocalApkFile", "built-in apk, no need to check");
    }

    /* access modifiers changed from: protected */
    public Class<?> b() {
        if (this.b == null) {
            File file = new File(getAbsolutePath());
            try {
                this.b = b(file);
            } finally {
                file.delete();
            }
        }
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        renameTo(new File(str));
    }

    /* access modifiers changed from: protected */
    public double c() {
        if (this.f487a == null) {
            return 0.0d;
        }
        return this.f487a.b();
    }

    private String b(String str) {
        if (this.d != null) {
            byte[] decode = Base64.decode(str, 0);
            try {
                Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                instance.init(2, this.d);
                return new String(instance.doFinal(decode), "UTF-8").trim();
            } catch (Exception e2) {
                this.e.e("ErrorWhileVerifySigNature", e2);
            }
        }
        return null;
    }

    private static PublicKey c(String str) {
        try {
            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(str, 0)));
        } catch (NoSuchAlgorithmException e2) {
            throw new Exception("NoSuchAlgorithmException");
        } catch (InvalidKeySpecException e3) {
            throw new Exception("InvalidKeySpecException");
        } catch (NullPointerException e4) {
            throw new Exception("NullPointerException");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0083 A[SYNTHETIC, Splitter:B:24:0x0083] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009e A[SYNTHETIC, Splitter:B:31:0x009e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.io.File r11) {
        /*
            r10 = this;
            r9 = 2
            r8 = 1
            r3 = 0
            r2 = 0
            java.lang.String r0 = ""
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0069, all -> 0x009a }
            r1.<init>(r11)     // Catch:{ Exception -> 0x0069, all -> 0x009a }
            java.lang.String r2 = "MD5"
            java.security.MessageDigest r2 = java.security.MessageDigest.getInstance(r2)     // Catch:{ Exception -> 0x00b7 }
            java.security.DigestInputStream r4 = new java.security.DigestInputStream     // Catch:{ Exception -> 0x00b7 }
            r4.<init>(r1, r2)     // Catch:{ Exception -> 0x00b7 }
            r5 = 4096(0x1000, float:5.74E-42)
            byte[] r5 = new byte[r5]     // Catch:{ Exception -> 0x00b7 }
        L_0x001a:
            int r6 = r4.read(r5)     // Catch:{ Exception -> 0x00b7 }
            r7 = -1
            if (r6 != r7) goto L_0x001a
            byte[] r5 = r2.digest()     // Catch:{ Exception -> 0x00b7 }
            r2 = r3
        L_0x0026:
            int r4 = r5.length     // Catch:{ Exception -> 0x00b7 }
            if (r2 >= r4) goto L_0x0050
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b7 }
            r4.<init>()     // Catch:{ Exception -> 0x00b7 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x00b7 }
            byte r4 = r5[r2]     // Catch:{ Exception -> 0x00b7 }
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 + 256
            r6 = 16
            java.lang.String r4 = java.lang.Integer.toString(r4, r6)     // Catch:{ Exception -> 0x00b7 }
            r6 = 1
            java.lang.String r4 = r4.substring(r6)     // Catch:{ Exception -> 0x00b7 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x00b7 }
            java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x00b7 }
            int r0 = r2 + 1
            r2 = r0
            r0 = r4
            goto L_0x0026
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()     // Catch:{ Exception -> 0x0056 }
        L_0x0055:
            return r0
        L_0x0056:
            r1 = move-exception
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r10.e
            java.lang.Object[] r4 = new java.lang.Object[r9]
            java.lang.String r5 = "XAdLocalApkFile"
            r4[r3] = r5
            java.lang.String r1 = r1.getMessage()
            r4[r8] = r1
            r2.e(r4)
            goto L_0x0055
        L_0x0069:
            r0 = move-exception
            r1 = r2
        L_0x006b:
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r10.e     // Catch:{ all -> 0x00b5 }
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00b5 }
            r5 = 0
            java.lang.String r6 = "XAdLocalApkFile"
            r4[r5] = r6     // Catch:{ all -> 0x00b5 }
            r5 = 1
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00b5 }
            r4[r5] = r0     // Catch:{ all -> 0x00b5 }
            r2.e(r4)     // Catch:{ all -> 0x00b5 }
            java.lang.String r0 = ""
            if (r1 == 0) goto L_0x0055
            r1.close()     // Catch:{ Exception -> 0x0087 }
            goto L_0x0055
        L_0x0087:
            r1 = move-exception
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r10.e
            java.lang.Object[] r4 = new java.lang.Object[r9]
            java.lang.String r5 = "XAdLocalApkFile"
            r4[r3] = r5
            java.lang.String r1 = r1.getMessage()
            r4[r8] = r1
            r2.e(r4)
            goto L_0x0055
        L_0x009a:
            r0 = move-exception
            r1 = r2
        L_0x009c:
            if (r1 == 0) goto L_0x00a1
            r1.close()     // Catch:{ Exception -> 0x00a2 }
        L_0x00a1:
            throw r0
        L_0x00a2:
            r1 = move-exception
            com.baidu.mobads.interfaces.utils.IXAdLogger r2 = r10.e
            java.lang.Object[] r4 = new java.lang.Object[r9]
            java.lang.String r5 = "XAdLocalApkFile"
            r4[r3] = r5
            java.lang.String r1 = r1.getMessage()
            r4[r8] = r1
            r2.e(r4)
            goto L_0x00a1
        L_0x00b5:
            r0 = move-exception
            goto L_0x009c
        L_0x00b7:
            r0 = move-exception
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobads.h.b.a(java.io.File):java.lang.String");
    }

    private void d() {
        File[] listFiles = this.c.getFilesDir().listFiles(new a());
        int i = 0;
        while (listFiles != null && i < listFiles.length) {
            if (listFiles[i].getAbsolutePath().contains("__xadsdk__remote__final__")) {
                Log.i("XAdLocalApkFile", "clearDexCacheFiles-->" + i + "--" + listFiles[i].getAbsolutePath());
                listFiles[i].delete();
            }
            i++;
        }
    }

    /* JADX INFO: finally extract failed */
    @TargetApi(14)
    private Class<?> b(File file) {
        Class<?> cls;
        this.e.d("XAdLocalApkFile", "Android version:" + Build.VERSION.RELEASE);
        try {
            synchronized (g.class) {
                String absolutePath = file.getAbsolutePath();
                ClassLoader classLoader = getClass().getClassLoader();
                String absolutePath2 = this.c.getFilesDir().getAbsolutePath();
                DexClassLoader dexClassLoader = new DexClassLoader(absolutePath, absolutePath2, null, classLoader);
                this.e.i("XAdLocalApkFile", "dexPath=" + absolutePath + ", cl=" + classLoader + ", dir=" + absolutePath2 + ", loader=" + dexClassLoader + ", len=" + file.length() + ", list=" + file.list());
                cls = Class.forName("com.baidu.mobads.container.AllInOneXAdContainerFactory", true, dexClassLoader);
            }
            d();
        } catch (Exception e2) {
            try {
                this.e.e("XAdLocalApkFile", e2.getMessage());
                d();
                cls = null;
            } catch (Throwable th) {
                d();
                throw th;
            }
        }
        this.e.i("XAdLocalApkFile", "jar.path=" + file.getAbsolutePath() + ", clz=" + cls);
        return cls;
    }

    class a implements FilenameFilter {
        a() {
        }

        public boolean accept(File file, String str) {
            return true;
        }
    }
}
