package com.baidu.mobads.h;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import com.baidu.mobads.h.g;

class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g.c f498a;
    final /* synthetic */ Handler b;
    final /* synthetic */ g c;

    n(g gVar, g.c cVar, Handler handler) {
        this.c = gVar;
        this.f498a = cVar;
        this.b = handler;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.h.g.b(com.baidu.mobads.h.g, boolean):void
     arg types: [com.baidu.mobads.h.g, int]
     candidates:
      com.baidu.mobads.h.g.b(com.baidu.mobads.h.g$c, android.os.Handler):void
      com.baidu.mobads.h.g.b(com.baidu.mobads.h.g, com.baidu.mobads.h.e):void
      com.baidu.mobads.h.g.b(com.baidu.mobads.h.g, boolean):void */
    public void run() {
        SharedPreferences.Editor edit;
        String str;
        int i;
        try {
            synchronized (g.class) {
                this.c.b(this.f498a, this.b);
            }
            SharedPreferences.Editor edit2 = this.c.j().edit();
            edit2.putString("previousProxyVersion", this.c.a());
            if (Build.VERSION.SDK_INT >= 9) {
                edit2.apply();
            } else {
                edit2.commit();
            }
        } catch (Throwable th) {
            try {
                this.c.l.e("XAdApkLoader", "Load APK Failed: " + th.toString());
                this.c.b(false);
                if (i < 9) {
                    edit.commit();
                }
            } finally {
                edit = this.c.j().edit();
                str = "previousProxyVersion";
                edit.putString(str, this.c.a());
                if (Build.VERSION.SDK_INT >= 9) {
                    edit.apply();
                } else {
                    edit.commit();
                }
            }
        }
    }
}
