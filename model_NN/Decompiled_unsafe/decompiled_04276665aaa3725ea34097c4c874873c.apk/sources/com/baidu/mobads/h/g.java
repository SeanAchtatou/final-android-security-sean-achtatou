package com.baidu.mobads.h;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.baidu.mobads.interfaces.IXAdContainerFactory;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.j.m;
import java.io.File;
import java.io.IOException;
import java.lang.Thread;

public class g {

    /* renamed from: a  reason: collision with root package name */
    protected static Thread.UncaughtExceptionHandler f492a;
    protected static volatile a b = null;
    protected static volatile a c = null;
    protected static final Handler e = new h(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static String h;
    protected String d = null;
    protected Handler f = e;
    @SuppressLint({"HandlerLeak"})
    protected final Handler g = new i(this, Looper.getMainLooper());
    /* access modifiers changed from: private */
    public com.baidu.mobads.openad.e.a i;
    /* access modifiers changed from: private */
    public e j;
    /* access modifiers changed from: private */
    public final Context k;
    /* access modifiers changed from: private */
    public IXAdLogger l = m.a().f();
    private c m;

    public interface c {
        void a(boolean z);
    }

    public final String a() {
        return "8.30";
    }

    public g(Context context) {
        if (h == null) {
            h = m.a().i().replaceURLWithSupportProtocol("http://mobads.baidu.com/ads/pa/") + com.baidu.mobads.a.b.b() + "/__pasys_remote_banner.php";
        }
        this.k = context;
        this.d = context.getDir("baidu_ad_sdk", 0).getAbsolutePath() + "/";
        if (f492a == null) {
            f492a = q.a(context);
            q.a(context).a(new j(this));
        }
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof q)) {
            Thread.setDefaultUncaughtExceptionHandler(f492a);
        }
    }

    /* access modifiers changed from: private */
    public SharedPreferences j() {
        return this.k.getSharedPreferences("com.baidu.mobads.loader", 0);
    }

    /* access modifiers changed from: protected */
    public void b() {
        new File(e()).delete();
    }

    /* access modifiers changed from: protected */
    @TargetApi(9)
    public void a(String str) {
        if (b != null) {
            SharedPreferences.Editor edit = j().edit();
            edit.putFloat("__badApkVersion__8.30", (float) b.f486a);
            if (Build.VERSION.SDK_INT >= 9) {
                edit.apply();
            } else {
                edit.commit();
            }
        }
    }

    private boolean k() {
        String string = j().getString("previousProxyVersion", null);
        String a2 = a();
        if (string != null && string.equals(a2)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        Message obtainMessage = this.f.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putBoolean("success", z);
        obtainMessage.setData(bundle);
        obtainMessage.what = 0;
        this.f.sendMessage(obtainMessage);
    }

    /* access modifiers changed from: protected */
    public String c() {
        return this.d + "__xadsdk__remote__final__builtin__.jar";
    }

    /* access modifiers changed from: protected */
    public void d() {
        String c2 = c();
        b bVar = new b(c2, this.k);
        try {
            m.a().k().copyFileFromAssetsTo(this.k, "bdxadsdk.jar", c2);
            if (!bVar.exists() || !bVar.canRead()) {
                throw new b("loadBuiltInApk failed: " + c2);
            }
            c(bVar);
        } catch (IOException e2) {
            throw new b("loadBuiltInApk failed: " + e2.toString());
        }
    }

    /* access modifiers changed from: private */
    public void a(b bVar) {
        Class<?> b2 = bVar.b();
        synchronized (this) {
            c = new a(b2, this.k, com.baidu.mobads.a.b.a(), com.baidu.mobads.a.b.f446a);
        }
    }

    private void b(b bVar) {
        Log.i("XAdApkLoader", "len=" + bVar.length() + ", path=" + bVar.getAbsolutePath());
        if (b == null) {
            b = new a(bVar.b(), this.k, com.baidu.mobads.a.b.a(), com.baidu.mobads.a.b.f446a);
            try {
                this.l.d("XAdApkLoader", "preloaded apk.version=" + b.a().getRemoteVersion());
            } catch (a e2) {
                this.l.w("XAdApkLoader", "preload local apk " + bVar.getAbsolutePath() + " failed, msg:" + e2.getMessage() + ", v=" + b.f486a);
                a(e2.getMessage());
                throw e2;
            }
        } else {
            this.l.w("XAdApkLoader", "mApkBuilder already initialized, version: " + b.f486a);
        }
    }

    private void c(b bVar) {
        synchronized (this) {
            b(bVar);
            this.l.d("XAdApkLoader", "loaded: " + bVar.getPath());
            b(true);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        a(z, z ? "apk Successfully Loaded" : "apk Load Failed");
        new Handler(Looper.getMainLooper()).postDelayed(new k(this, z), 5000);
    }

    /* access modifiers changed from: private */
    public synchronized void l() {
        try {
            if (this.i != null) {
                this.i.removeAllListeners();
                this.i.a();
            }
            this.i = null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return;
    }

    private void a(boolean z, String str) {
        if (this.m != null) {
            this.m.a(z);
        }
    }

    /* access modifiers changed from: protected */
    public String e() {
        return this.d + "__xadsdk__remote__final__downloaded__.jar";
    }

    /* access modifiers changed from: protected */
    public void f() {
        b bVar = new b(e(), this.k);
        Boolean valueOf = Boolean.valueOf(bVar.exists());
        Boolean valueOf2 = Boolean.valueOf(bVar.canRead());
        long length = bVar.length();
        if (!valueOf.booleanValue() || !valueOf2.booleanValue() || length <= 0) {
            this.l.d("XAdApkLoader", "no downloaded file yet, use built-in apk file");
            try {
                d();
            } catch (b e2) {
                this.l.e("XAdApkLoader", "loadBuiltInApk failed: " + e2.toString());
                throw new a("load built-in apk failed" + e2.toString());
            }
        } else {
            try {
                if (Boolean.valueOf(k()).booleanValue()) {
                    throw new a("XAdApkLoader upgraded, drop stale downloaded file, use built-in instead");
                }
                synchronized (this) {
                    Log.i("XAdApkLoader", "loadDownloadedOrBuiltInApk len=" + bVar.length() + ", path=" + bVar.getAbsolutePath());
                    b(bVar);
                    double d2 = (double) j().getFloat("__badApkVersion__8.30", -1.0f);
                    this.l.d("XAdApkLoader", "downloadedApkFile.getApkVersion(): " + bVar.c() + ", badApkVersion: " + d2);
                    if (bVar.c() == d2) {
                        throw new a("downloaded file marked bad, drop it and use built-in");
                    }
                    this.l.d("XAdApkLoader", "loaded: " + bVar.getPath());
                    b(true);
                }
            } catch (a e3) {
                this.l.e("XAdApkLoader", "load downloaded apk failed: " + e3.toString() + ", fallback to built-in");
                if (bVar != null && bVar.exists()) {
                    bVar.delete();
                }
                h();
                try {
                    d();
                } catch (b e4) {
                    throw new a("load built-in apk also failed" + e4.toString());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(e eVar) {
        if (eVar.a().booleanValue()) {
            c a2 = c.a(this.k, eVar, this.d, this.g);
            if (!a2.isAlive()) {
                this.l.d("XAdApkLoader", "XApkDownloadThread starting ...");
                a2.start();
                return;
            }
            this.l.d("XAdApkLoader", "XApkDownloadThread already started");
            a2.a(eVar.c());
        }
    }

    /* access modifiers changed from: private */
    public void b(c cVar, Handler handler) {
        this.m = cVar;
        this.f = handler;
        if (b == null) {
            f();
        } else {
            b(true);
        }
    }

    @TargetApi(9)
    public void a(c cVar, Handler handler) {
        new Thread(new n(this, cVar, handler)).start();
    }

    public void a(c cVar) {
        a(cVar, e);
    }

    public IXAdContainerFactory g() {
        return a(b);
    }

    private IXAdContainerFactory a(a aVar) {
        if (aVar == null) {
            return null;
        }
        try {
            return aVar.a();
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void h() {
        if (b != null) {
            b.b();
            b = null;
        }
    }

    public static final class a extends Exception {
        public a(String str) {
            m.a().f().e(str);
        }
    }

    protected static final class b extends Exception {
        public b(String str) {
            m.a().f().e(str);
        }
    }
}
