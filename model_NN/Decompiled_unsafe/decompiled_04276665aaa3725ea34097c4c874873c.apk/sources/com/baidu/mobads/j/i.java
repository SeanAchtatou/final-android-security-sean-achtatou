package com.baidu.mobads.j;

import android.content.Context;
import android.os.Environment;
import com.baidu.mobads.interfaces.utils.IXAdIOUtils;
import java.io.File;

public class i implements IXAdIOUtils {
    public String getStoreagePath(Context context, String str, String str2) {
        try {
            return getExternalFilesDir(context).getPath() + str2;
        } catch (Exception e) {
            return str + str2;
        }
    }

    public String getStoreagePath(Context context) {
        return getStoreagePath(context, IXAdIOUtils.DEFAULT_SD_CARD_PATH, IXAdIOUtils.DEFAULT_CACHE_PATH);
    }

    public File getExternalFilesDir(Context context) {
        try {
            if (m.a().n().isUseOldStoragePath()) {
                return Environment.getExternalStorageDirectory();
            }
            return context.getExternalFilesDir(null);
        } catch (Exception e) {
            m.a().f().e("XAdIOUtils", e.getMessage());
            return null;
        }
    }

    public File deleteFileRecursive(File file) {
        File file2 = null;
        try {
            if (file.isDirectory()) {
                File[] listFiles = file.listFiles();
                for (File deleteFileRecursive : listFiles) {
                    File deleteFileRecursive2 = deleteFileRecursive(deleteFileRecursive);
                    if (deleteFileRecursive2 != null) {
                        return deleteFileRecursive2;
                    }
                }
            }
            if (file.delete()) {
                return null;
            }
            return file;
        } catch (Exception e) {
            if (!file.delete()) {
                file2 = file;
            }
            return file2;
        }
    }

    public File deleteFileRecursive(String str) {
        return deleteFileRecursive(new File(str));
    }

    public boolean renameFile(String str, String str2) {
        try {
            File file = new File(str);
            File file2 = new File(str2);
            if (file == null || !file.exists()) {
                return false;
            }
            return file.renameTo(file2);
        } catch (Exception e) {
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0018  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void copyFileInputStream(java.io.InputStream r5, java.lang.String r6) {
        /*
            r4 = this;
            r2 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ all -> 0x0032 }
            r1.<init>(r6)     // Catch:{ all -> 0x0032 }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]     // Catch:{ all -> 0x0015 }
        L_0x000a:
            int r2 = r5.read(r0)     // Catch:{ all -> 0x0015 }
            if (r2 <= 0) goto L_0x0021
            r3 = 0
            r1.write(r0, r3, r2)     // Catch:{ all -> 0x0015 }
            goto L_0x000a
        L_0x0015:
            r0 = move-exception
        L_0x0016:
            if (r5 == 0) goto L_0x001b
            r5.close()
        L_0x001b:
            if (r1 == 0) goto L_0x0020
            r1.close()
        L_0x0020:
            throw r0
        L_0x0021:
            r5.close()     // Catch:{ all -> 0x0015 }
            r1.close()     // Catch:{ all -> 0x0015 }
            if (r5 == 0) goto L_0x002c
            r5.close()
        L_0x002c:
            if (r1 == 0) goto L_0x0031
            r1.close()
        L_0x0031:
            return
        L_0x0032:
            r0 = move-exception
            r1 = r2
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobads.j.i.copyFileInputStream(java.io.InputStream, java.lang.String):void");
    }

    public void copyFileFromAssetsTo(Context context, String str, String str2) {
        try {
            m.a().k().copyFileInputStream(context.getAssets().open(str), str2);
        } catch (Exception e) {
            m.a().f().d(e);
        }
    }
}
