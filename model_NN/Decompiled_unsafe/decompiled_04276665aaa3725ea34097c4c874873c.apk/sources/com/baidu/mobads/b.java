package com.baidu.mobads;

import android.view.View;
import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;
import org.json.JSONObject;

class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ IOAdEvent f462a;
    final /* synthetic */ a b;

    b(a aVar, IOAdEvent iOAdEvent) {
        this.b = aVar;
        this.f462a = iOAdEvent;
    }

    public void run() {
        if (IXAdEvent.AD_LOADED.equals(this.f462a.getType())) {
            this.b.f444a.d.onAdReady(this.b.f444a);
        } else if (IXAdEvent.AD_STARTED.equals(this.f462a.getType())) {
            this.b.f444a.d.onAdSwitch();
            this.b.f444a.d.onAdShow(new JSONObject());
        } else if (IXAdEvent.AD_ERROR.equals(this.f462a.getType())) {
            this.b.f444a.d.onAdFailed(m.a().q().getMessage(this.f462a.getData()));
        } else if ("AdUserClick".equals(this.f462a.getType())) {
            this.b.f444a.d.onAdClick(new JSONObject());
        } else if (IXAdEvent.AD_USER_CLOSE.equals(this.f462a.getType())) {
            m.a().m().a((View) this.b.f444a);
            this.b.f444a.d.onAdClose(new JSONObject());
        }
    }
}
