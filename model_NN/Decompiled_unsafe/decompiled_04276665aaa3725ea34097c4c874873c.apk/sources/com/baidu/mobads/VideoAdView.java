package com.baidu.mobads;

import android.content.Context;
import android.widget.RelativeLayout;
import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.openad.interfaces.event.IOAdEventListener;
import com.baidu.mobads.production.i.b;
import com.qq.e.comm.constants.ErrorCode;

public class VideoAdView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private b f441a;
    private IOAdEventListener b = new ak(this);
    /* access modifiers changed from: private */
    public VideoAdViewListener c;

    public VideoAdView(Context context) {
        super(context);
    }

    public VideoAdView(Context context, String str) {
        super(context, null);
    }

    public enum VideoDuration {
        DURATION_15_SECONDS(15),
        DURATION_30_SECONDS(30),
        DURATION_45_SECONDS(45);
        

        /* renamed from: a  reason: collision with root package name */
        private int f442a;

        private VideoDuration(int i) {
            this.f442a = i;
        }

        /* access modifiers changed from: protected */
        public int getValue() {
            return this.f442a;
        }
    }

    public enum VideoSize {
        SIZE_16x9(320, 180),
        SIZE_4x3(400, ErrorCode.InitError.INIT_AD_ERROR);
        

        /* renamed from: a  reason: collision with root package name */
        private int f443a;
        private int b;

        private VideoSize(int i, int i2) {
            this.f443a = i;
            this.b = i2;
        }

        /* access modifiers changed from: protected */
        public int getWidth() {
            return this.f443a;
        }

        /* access modifiers changed from: protected */
        public int getHeight() {
            return this.b;
        }
    }

    public void requestAd(VideoAdRequest videoAdRequest) {
        this.f441a = new b(getContext(), "TODO");
        this.f441a.setActivity(getContext());
        this.f441a.setAdSlotBase(this);
        this.f441a.addEventListener(IXAdEvent.AD_CLICK_THRU, this.b);
        this.f441a.addEventListener(IXAdEvent.AD_LOADED, this.b);
        this.f441a.addEventListener(IXAdEvent.AD_STARTED, this.b);
        this.f441a.addEventListener(IXAdEvent.AD_STOPPED, this.b);
        this.f441a.addEventListener(IXAdEvent.AD_ERROR, this.b);
        this.f441a.request();
    }

    public void startVideo() {
        this.f441a.start();
    }

    public static void setAppSid(Context context, String str) {
        AdView.setAppSid(context, str);
    }

    public void setListener(VideoAdViewListener videoAdViewListener) {
        this.c = videoAdViewListener;
    }
}
