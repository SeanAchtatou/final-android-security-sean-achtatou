package com.baidu.mobads;

import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.interfaces.event.IOAdEvent;

class ai implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ IOAdEvent f454a;
    final /* synthetic */ ah b;

    ai(ah ahVar, IOAdEvent iOAdEvent) {
        this.b = ahVar;
        this.f454a = iOAdEvent;
    }

    public void run() {
        if (IXAdEvent.AD_LOADED.equals(this.f454a.getType())) {
            m.a().f().i(this.f454a);
        } else if (IXAdEvent.AD_STARTED.equals(this.f454a.getType())) {
            this.b.f453a.c.onAdPresent();
        } else if ("AdUserClick".equals(this.f454a.getType())) {
            this.b.f453a.c.onAdClick();
        } else if (IXAdEvent.AD_STOPPED.equals(this.f454a.getType())) {
            this.b.f453a.f438a.removeAllListeners();
            this.b.f453a.c.onAdDismissed();
        } else if (IXAdEvent.AD_ERROR.equals(this.f454a.getType())) {
            this.b.f453a.f438a.removeAllListeners();
            this.b.f453a.c.onAdFailed(m.a().q().getMessage(this.f454a.getData()));
        }
    }
}
