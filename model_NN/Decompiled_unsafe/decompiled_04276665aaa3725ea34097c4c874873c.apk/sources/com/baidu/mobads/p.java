package com.baidu.mobads;

import android.view.ViewTreeObserver;

class p implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppActivity f560a;

    p(AppActivity appActivity) {
        this.f560a = appActivity;
    }

    public boolean onPreDraw() {
        this.f560a.d.getViewTreeObserver().removeOnPreDrawListener(this);
        this.f560a.a(this.f560a.d);
        return true;
    }
}
