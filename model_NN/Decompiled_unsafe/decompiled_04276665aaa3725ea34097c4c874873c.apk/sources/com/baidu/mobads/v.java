package com.baidu.mobads;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.baidu.mobads.CpuInfoManager;
import com.baidu.mobads.production.c.b;

final class v implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f599a;
    final /* synthetic */ int b;
    final /* synthetic */ String c;
    final /* synthetic */ CpuInfoManager.UrlListener d;

    v(Context context, int i, String str, CpuInfoManager.UrlListener urlListener) {
        this.f599a = context;
        this.b = i;
        this.c = str;
        this.d = urlListener;
    }

    public void run() {
        String a2 = new b(this.f599a.getApplicationContext(), this.b, this.c).a();
        if (this.d != null) {
            new Handler(Looper.getMainLooper()).post(new w(this, a2));
        }
    }
}
