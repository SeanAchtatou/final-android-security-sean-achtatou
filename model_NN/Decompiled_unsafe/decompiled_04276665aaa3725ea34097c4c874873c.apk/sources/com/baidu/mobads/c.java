package com.baidu.mobads;

import android.annotation.SuppressLint;
import android.view.KeyEvent;
import com.baidu.mobads.ao;

class c implements ao.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AdView f465a;

    c(AdView adView) {
        this.f465a = adView;
    }

    public void a(int i) {
        this.f465a.c.a(i);
    }

    public void a(boolean z) {
        this.f465a.c.a(z);
    }

    public void a(int i, int i2) {
        this.f465a.a();
    }

    @SuppressLint({"MissingSuperCall"})
    public void a() {
        this.f465a.c.k();
    }

    public void b() {
        this.f465a.a();
        this.f465a.c.j();
    }

    public boolean a(int i, KeyEvent keyEvent) {
        return this.f465a.c.a(i, keyEvent);
    }
}
