package com.baidu.mobads;

import android.view.MotionEvent;
import android.view.View;

class i implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    String f503a = "click";
    final /* synthetic */ AppActivity b;

    i(AppActivity appActivity) {
        this.b = appActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.AppActivity.a(com.baidu.mobads.AppActivity, boolean):boolean
     arg types: [com.baidu.mobads.AppActivity, int]
     candidates:
      com.baidu.mobads.AppActivity.a(com.baidu.mobads.AppActivity, android.view.View):void
      com.baidu.mobads.AppActivity.a(com.baidu.mobads.AppActivity, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.AppActivity.b(com.baidu.mobads.AppActivity, boolean):boolean
     arg types: [com.baidu.mobads.AppActivity, int]
     candidates:
      com.baidu.mobads.AppActivity.b(com.baidu.mobads.AppActivity, android.view.View):void
      com.baidu.mobads.AppActivity.b(com.baidu.mobads.AppActivity, boolean):boolean */
    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean unused = this.b.y = true;
        try {
            switch (motionEvent.getAction()) {
                case 0:
                    boolean unused2 = this.b.z = true;
                    break;
                case 1:
                    this.b.A.s.incrementAndGet();
                    view.performClick();
                    break;
                case 2:
                    break;
                default:
                    this.b.D.e(AppActivity.o, "unprocessed action=" + motionEvent.getAction());
                    break;
            }
        } catch (Exception e) {
            this.b.D.d(AppActivity.o, e.getMessage());
        }
        return false;
    }
}
