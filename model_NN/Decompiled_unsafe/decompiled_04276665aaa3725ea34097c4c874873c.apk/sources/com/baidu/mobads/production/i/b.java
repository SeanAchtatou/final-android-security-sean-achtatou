package com.baidu.mobads.production.i;

import android.content.Context;
import android.widget.RelativeLayout;
import com.baidu.mobads.interfaces.IXAdConstants4PDK;
import com.baidu.mobads.interfaces.IXAdContainer;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.IXAdInternalConstants;
import com.baidu.mobads.interfaces.IXLinearAdSlot;
import com.baidu.mobads.openad.e.d;
import com.baidu.mobads.openad.interfaces.event.IOAdEventDispatcher;
import com.baidu.mobads.production.a;
import com.baidu.mobads.production.t;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class b extends a implements IXLinearAdSlot, IOAdEventDispatcher {
    private a w;

    public b(Context context, String str) {
        super(context);
        setId(str);
        this.p = IXAdConstants4PDK.SlotType.SLOT_TYPE_PREROLL;
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.n = 8000;
    }

    public void c() {
        this.s.i("XPrerollAdSlot", "afterAdContainerInit()");
        dispatchEvent(new com.baidu.mobads.openad.d.b(com.baidu.mobads.openad.d.b.COMPLETE));
    }

    public void request() {
        int i;
        int i2;
        this.w = new a(getApplicationContext(), getActivity(), this.p, this);
        this.w.d(getId());
        HashMap<String, String> parameter = getParameter();
        String str = parameter.get(IXAdInternalConstants.PARAMETER_KEY_OF_BASE_WIDTH);
        String str2 = parameter.get(IXAdInternalConstants.PARAMETER_KEY_OF_BASE_HEIGHT);
        if (str != null) {
            try {
                i = Integer.parseInt(str);
            } catch (Exception e) {
                i = 0;
            }
        } else {
            i = 0;
        }
        if (str2 != null) {
            try {
                i2 = Integer.parseInt(str2);
            } catch (Exception e2) {
                i2 = 0;
            }
        } else {
            i2 = 0;
        }
        this.w.a(i);
        this.w.b(i2);
        super.a(this.w);
    }

    /* access modifiers changed from: protected */
    public void a(d dVar, t tVar, int i) {
        String str = getParameter().get(IXAdInternalConstants.PARAMETER_KEY_OF_AD_REQUESTING_TIMEOUT);
        if (str != null) {
            try {
                i = Integer.parseInt(str);
            } catch (Exception e) {
            }
        }
        tVar.a(dVar, (double) i);
    }

    /* access modifiers changed from: protected */
    public void c(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
        if (iXAdContainer.getAdContainerContext().getAdInstanceInfo().getCreativeType() == IXAdInstanceInfo.CreativeType.STATIC_IMAGE || iXAdContainer.getAdContainerContext().getAdInstanceInfo().getCreativeType() == IXAdInstanceInfo.CreativeType.GIF) {
            start();
        }
    }

    /* access modifiers changed from: protected */
    public void d(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
        HashSet hashSet = new HashSet();
        hashSet.addAll(iXAdContainer.getAdContainerContext().getAdInstanceInfo().getStartTrackers());
        a(hashSet);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void
     arg types: [com.baidu.mobads.openad.e.d, int]
     candidates:
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.a, java.net.HttpURLConnection):java.net.HttpURLConnection
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, double):void
      com.baidu.mobads.openad.e.a.a(com.baidu.mobads.openad.e.d, java.lang.Boolean):void */
    private void a(Set<String> set) {
        com.baidu.mobads.openad.e.a aVar = new com.baidu.mobads.openad.e.a();
        for (String dVar : set) {
            d dVar2 = new d(dVar, "");
            dVar2.e = 1;
            aVar.a(dVar2, (Boolean) true);
        }
    }

    /* access modifiers changed from: protected */
    public void e(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
        super.e(iXAdContainer, hashMap);
        this.m = IXAdConstants4PDK.SlotState.COMPLETED;
    }

    /* renamed from: m */
    public com.baidu.mobads.vo.d getAdRequestInfo() {
        return this.w;
    }

    public void setVideoDisplayBase(RelativeLayout relativeLayout) {
        this.e = relativeLayout;
    }

    public void setActivityState(IXAdConstants4PDK.ActivityState activityState) {
    }

    public void setVideoState(IXAdConstants4PDK.VideoState videoState) {
    }

    public void setParameter(String str, Object obj) {
    }

    public Object getParameter(String str) {
        return null;
    }

    public void setContentVideoAssetCurrentTimePosition(double d) {
    }

    public void notifyVisitorAction(IXAdConstants4PDK.VisitorAction visitorAction) {
    }

    public void setMaxDuration(int i) {
    }

    public void setMaxAdNum(int i) {
    }

    public int getDuration() {
        if (this.h == null) {
            return super.getDuration();
        }
        return (int) this.h.getDuration();
    }

    public int getPlayheadTime() {
        if (this.h == null) {
            return super.getPlayheadTime();
        }
        return (int) this.h.getPlayheadTime();
    }

    public void load() {
        this.r.set(true);
        super.load();
    }

    public void start() {
        if (this.r.get()) {
            super.start();
        } else {
            load();
        }
    }

    public void stop() {
        this.s.i("XPrerollAdSlot", "stop()" + getSlotState().getValue());
        super.stop();
    }

    public void pause() {
        this.s.i("XPrerollAdSlot", "pause()" + getSlotState().getValue());
        if (getSlotState() == IXAdConstants4PDK.SlotState.PLAYING) {
            super.pause();
        }
    }

    public void resume() {
        this.s.i("XPrerollAdSlot", "resume()" + getSlotState().getValue());
        if (getSlotState() == IXAdConstants4PDK.SlotState.PAUSED) {
            super.resume();
        }
    }
}
