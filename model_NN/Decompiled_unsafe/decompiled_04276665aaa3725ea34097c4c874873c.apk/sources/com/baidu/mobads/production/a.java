package com.baidu.mobads.production;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import cn.banshenggua.aichang.player.PlayerService;
import com.baidu.mobads.an;
import com.baidu.mobads.h.g;
import com.baidu.mobads.h.q;
import com.baidu.mobads.interfaces.IXAdConstants4PDK;
import com.baidu.mobads.interfaces.IXAdContainer;
import com.baidu.mobads.interfaces.IXAdContainerContext;
import com.baidu.mobads.interfaces.IXAdContainerFactory;
import com.baidu.mobads.interfaces.IXAdInstanceInfo;
import com.baidu.mobads.interfaces.IXAdProdInfo;
import com.baidu.mobads.interfaces.IXAdResponseInfo;
import com.baidu.mobads.interfaces.IXNonLinearAdSlot;
import com.baidu.mobads.interfaces.error.XAdErrorCode;
import com.baidu.mobads.interfaces.event.IXAdEvent;
import com.baidu.mobads.interfaces.utils.IXAdLogger;
import com.baidu.mobads.interfaces.utils.IXAdURIUitls;
import com.baidu.mobads.j.m;
import com.baidu.mobads.openad.b.b;
import com.baidu.mobads.openad.d.c;
import com.baidu.mobads.openad.interfaces.event.IOAdEventListener;
import com.baidu.mobads.vo.d;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class a extends c implements IXNonLinearAdSlot {

    /* renamed from: a  reason: collision with root package name */
    public static IXAdContainerFactory f561a;
    private static final String[] w = {"android.permission.READ_PHONE_STATE", "android.permission.ACCESS_COARSE_LOCATION"};
    protected Boolean b;
    public IXAdInstanceInfo d;
    /* access modifiers changed from: protected */
    public RelativeLayout e;
    /* access modifiers changed from: protected */
    public Context f;
    protected int g;
    public IXAdContainer h;
    protected String i;
    protected t j;
    protected com.baidu.mobads.openad.f.a k;
    protected d l;
    protected IXAdConstants4PDK.SlotState m;
    protected int n;
    protected int o;
    protected IXAdConstants4PDK.SlotType p;
    protected HashMap<String, String> q;
    protected AtomicBoolean r;
    protected final IXAdLogger s;
    protected long t;
    protected long u;
    protected long v;
    private IXAdResponseInfo x;
    private String y;
    private IOAdEventListener z;

    /* access modifiers changed from: protected */
    public abstract void a(com.baidu.mobads.openad.e.d dVar, t tVar, int i2);

    public abstract void c();

    /* access modifiers changed from: protected */
    public abstract void c(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap);

    /* access modifiers changed from: protected */
    public abstract void d();

    /* access modifiers changed from: protected */
    public abstract void d(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap);

    public a(Context context) {
        this.b = false;
        this.d = null;
        this.g = 0;
        this.m = IXAdConstants4PDK.SlotState.IDEL;
        this.n = 5000;
        this.o = 0;
        this.q = new HashMap<>();
        this.r = new AtomicBoolean();
        this.z = new b(this);
        this.s = m.a().f();
        new Thread(new e(this, context)).start();
    }

    private void c(Context context) {
        new Handler(Looper.getMainLooper()).postDelayed(new f(this, context), 2000);
    }

    public a(Context context, String str, IXAdConstants4PDK.SlotType slotType) {
        this(context);
        this.f = context;
        c(str);
        setId(str);
        this.p = slotType;
    }

    public IXAdConstants4PDK.SlotState getSlotState() {
        return this.m;
    }

    public void setActivity(Context context) {
        this.f = context;
        this.t = System.currentTimeMillis();
        a();
        this.r.set(false);
        d();
        b.a(getApplicationContext());
        com.baidu.mobads.c.a.a().a(getApplicationContext());
        m.a().a(getApplicationContext());
        this.k = new com.baidu.mobads.openad.f.a(this.n);
        this.k.setEventHandler(new i(this));
        b(this.f);
        q.a(this.f).a();
    }

    public void setAdSlotBase(RelativeLayout relativeLayout) {
        this.e = relativeLayout;
    }

    public void setId(String str) {
        this.y = str;
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    public String getId() {
        return this.y;
    }

    public IXAdConstants4PDK.SlotType getType() {
        return this.p;
    }

    /* access modifiers changed from: private */
    public void a(XAdErrorCode xAdErrorCode, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("msg", xAdErrorCode);
        dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_ERROR, hashMap));
        m.a().q().printErrorMessage(xAdErrorCode, str);
    }

    /* access modifiers changed from: protected */
    public void a(Context context) {
        if (BaiduXAdSDKContext.mApkLoader == null) {
            synchronized (g.class) {
                if (BaiduXAdSDKContext.mApkLoader == null) {
                    BaiduXAdSDKContext.mApkLoader = new g(context.getApplicationContext());
                }
            }
        }
        BaiduXAdSDKContext.mApkLoader.a(new j(this));
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        this.s.i("XAbstractAdProdTemplate", "doubleCheck:" + str + ", bfp=" + this.b + ", apk=" + BaiduXAdSDKContext.isRemoteLoadSuccess);
        if (BaiduXAdSDKContext.isRemoteLoadSuccess.booleanValue()) {
            m.a().a(getAdContainerFactory());
        }
        if (BaiduXAdSDKContext.isRemoteLoadSuccess.booleanValue() && this.b.booleanValue()) {
            IXAdResponseInfo adResponseInfo = getAdResponseInfo();
            if (adResponseInfo != null) {
                b(adResponseInfo);
            }
            a(getAdResponseInfo());
        }
    }

    private void b(IXAdResponseInfo iXAdResponseInfo) {
        this.s.i("XAbstractAdProdTemplate", "try2CachingVideoAdCreativeAsset");
        IXAdURIUitls i2 = m.a().i();
        IXAdInstanceInfo primaryAdInstanceInfo = iXAdResponseInfo.getPrimaryAdInstanceInfo();
        String videoUrl = primaryAdInstanceInfo.getVideoUrl();
        Boolean isHttpProtocol = i2.isHttpProtocol(videoUrl);
        this.s.i("XAbstractAdProdTemplate", "try2CachingVideoAdCreativeAsset, should cache=" + isHttpProtocol);
        if (isHttpProtocol.booleanValue()) {
            primaryAdInstanceInfo.setLocalCreativeURL(null);
            String str = getApplicationContext().getFilesDir().getPath() + File.separator + "__bidu_cache_dir" + File.separator;
            String adId = primaryAdInstanceInfo.getAdId();
            com.baidu.mobads.j.g b2 = m.a().b();
            b2.a(str);
            b2.a(videoUrl, str, adId, new k(this, Looper.getMainLooper(), primaryAdInstanceInfo));
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(d dVar) {
        this.s.i("XAbstractAdProdTemplate", "doRequest()");
        new Thread(new l(this, dVar)).start();
        return true;
    }

    /* access modifiers changed from: protected */
    public void b(d dVar) {
        this.l = dVar;
        g();
        this.b = false;
        String b2 = this.i == null ? dVar.b() : this.i;
        this.j = new t();
        try {
            this.j.a(new b());
        } catch (ClassNotFoundException e2) {
            this.j.a(new com.baidu.mobads.openad.b.d());
        }
        com.baidu.mobads.c.a.b = b2;
        com.baidu.mobads.openad.e.d dVar2 = new com.baidu.mobads.openad.e.d(b2, "");
        dVar2.e = 1;
        this.j.addEventListener("URLLoader.Load.Complete", this.z);
        this.j.addEventListener("URLLoader.Load.Error", this.z);
        a(dVar2, this.j, this.n);
    }

    /* access modifiers changed from: protected */
    public void a(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
        h();
        c(iXAdContainer, hashMap);
        dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_LOADED));
    }

    /* access modifiers changed from: protected */
    public void b(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
        this.m = IXAdConstants4PDK.SlotState.PLAYING;
        d(iXAdContainer, hashMap);
        dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_STARTED));
    }

    /* access modifiers changed from: protected */
    public void e(IXAdContainer iXAdContainer, HashMap<String, Object> hashMap) {
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        com.baidu.mobads.c.a.a().a(str);
        dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_ERROR));
    }

    /* access modifiers changed from: protected */
    public void a(IXAdResponseInfo iXAdResponseInfo) {
        this.s.i("XAbstractAdProdTemplate", "handleAllReady");
        this.g++;
        this.d = iXAdResponseInfo.getPrimaryAdInstanceInfo();
        Context applicationContext = getApplicationContext();
        an anVar = new an(applicationContext, getActivity(), this.l.d(), this.e, new o(applicationContext, this), iXAdResponseInfo, null);
        if (Looper.myLooper() == Looper.getMainLooper()) {
            a(anVar);
        } else {
            new Handler(Looper.getMainLooper()).post(new m(this, anVar));
        }
    }

    /* access modifiers changed from: protected */
    public void a(IXAdResponseInfo iXAdResponseInfo, IXAdInstanceInfo iXAdInstanceInfo) {
        this.d = iXAdInstanceInfo;
    }

    public Context getApplicationContext() {
        Activity activity = getActivity();
        return activity == null ? this.f : activity.getApplicationContext();
    }

    public Activity getActivity() {
        if (this.f instanceof Activity) {
            return (Activity) this.f;
        }
        if (this.e == null || !(this.e.getContext() instanceof Activity)) {
            return null;
        }
        return (Activity) this.e.getContext();
    }

    public IXAdContainer getCurrentXAdContainer() {
        return this.h;
    }

    public IXAdContainerFactory getAdContainerFactory() {
        return f561a;
    }

    public static IXAdContainerFactory b() {
        return f561a;
    }

    /* access modifiers changed from: protected */
    public void a(IXAdContainerContext iXAdContainerContext) {
        try {
            this.s.i("XAbstractAdProdTemplate", "processAllReadyOnUIThread()");
            this.u = System.currentTimeMillis();
            this.h = b(iXAdContainerContext);
            this.v = System.currentTimeMillis();
            if (this.h == null) {
                this.s.e("XAbstractAdProdTemplate", "processAllReadyOnUIThread(), mAdContainer is null");
                dispatchEvent(new com.baidu.mobads.f.a(IXAdEvent.AD_STOPPED));
                return;
            }
            this.s.i("XAbstractAdProdTemplate", "processAllReadyOnUIThread(), mAdContainer be created");
            this.q.put("start", "" + this.t);
            this.q.put("container_before_created", "" + this.u);
            this.q.put("container_after_created", "" + this.v);
            this.h.setParameters(this.q);
            com.baidu.mobads.a.a.c = this.h.getRemoteVersion();
            this.s.i("XAbstractAdProdTemplate", "processAllReadyOnUIThread(), mAdContainer be created, hasCalledLoadAtAppSide=" + this.r.get());
            if (this.r.get()) {
                this.h.load();
            }
            c();
            c(this.f);
        } catch (Exception e2) {
            this.s.e(m.a().q().genCompleteErrorMessage(XAdErrorCode.PERMISSION_PROBLEM, e2.getMessage()));
            com.baidu.mobads.c.a.a().a("process all ready on UI Thread exception: " + e2.toString());
        }
    }

    private IXAdContainer b(IXAdContainerContext iXAdContainerContext) {
        IXAdContainer iXAdContainer = null;
        this.s.i("XAbstractAdProdTemplate", "createAdContainer");
        if (!(f561a == null || (iXAdContainer = f561a.createXAdContainer(iXAdContainerContext, null)) == null)) {
            this.s.i("XAbstractAdProdTemplate", "createAdContainer() apk.version=" + f561a.getRemoteVersion());
        }
        return iXAdContainer;
    }

    public IXAdProdInfo getProdInfo() {
        return this.l.d();
    }

    public void setParameter(HashMap<String, String> hashMap) {
        this.q = hashMap;
    }

    public HashMap<String, String> getParameter() {
        return this.q;
    }

    public int getDuration() {
        return -1;
    }

    public int getPlayheadTime() {
        return -1;
    }

    public IXAdInstanceInfo getCurrentAdInstance() {
        return this.d;
    }

    public ViewGroup getProdBase() {
        return this.e;
    }

    public void load() {
        if (this.h != null) {
            this.h.load();
        } else {
            this.r.set(true);
        }
    }

    public void resize() {
        if (this.h != null && getApplicationContext() != null) {
            new Handler(getApplicationContext().getMainLooper()).post(new n(this));
        }
    }

    public void pause() {
        e();
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (this.h != null && getApplicationContext() != null) {
            this.m = IXAdConstants4PDK.SlotState.PAUSED;
            new Handler(getApplicationContext().getMainLooper()).post(new c(this));
        }
    }

    public void start() {
        if (this.h != null) {
            this.h.start();
        }
    }

    public void resume() {
        f();
    }

    /* access modifiers changed from: protected */
    public void f() {
        if (this.h != null && getApplicationContext() != null) {
            this.m = IXAdConstants4PDK.SlotState.PLAYING;
            new Handler(getApplicationContext().getMainLooper()).post(new d(this));
        }
    }

    public void stop() {
        m.a().f().i("XAbstractAdProdTemplate", PlayerService.ACTION_STOP);
        if (this.h != null) {
            this.h.stop();
            this.h = null;
        }
    }

    /* access modifiers changed from: protected */
    public void g() {
        if (this.j != null) {
            this.j.removeAllListeners();
            this.j.a();
        }
    }

    /* access modifiers changed from: protected */
    public void h() {
        if (this.k != null) {
            this.k.stop();
        }
        this.k = null;
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (this.k != null) {
            this.k.start();
        }
    }

    public void j() {
        if (this.h != null) {
            this.h.onAttachedToWindow();
        }
    }

    @SuppressLint({"MissingSuperCall"})
    public void k() {
        if (this.h != null) {
            this.h.onDetachedFromWindow();
        }
    }

    public void a(int i2) {
        if (this.h != null) {
            this.h.onWindowVisibilityChanged(i2);
        }
    }

    public void a(boolean z2) {
        if (this.h != null) {
            this.h.onWindowFocusChanged(z2);
        }
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        if (this.h != null) {
            return this.h.processKeyEvent(i2, keyEvent).booleanValue();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        if (TextUtils.isEmpty(str)) {
            this.s.e("代码位id(adPlaceId)不可以为空");
        }
    }

    public void b(Context context) {
        try {
            com.baidu.mobads.j.d m2 = m.a().m();
            a(context, "android.permission.INTERNET");
            a(context, "android.permission.ACCESS_NETWORK_STATE");
            if (m2.isOldPermissionModel()) {
                a(context, "android.permission.READ_PHONE_STATE");
                a(context, "android.permission.WRITE_EXTERNAL_STORAGE");
                return;
            }
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < w.length; i2++) {
                if (!m2.checkSelfPermission(context, w[i2])) {
                    arrayList.add(w[i2]);
                }
            }
            int size = arrayList.size();
            if (size > 0) {
                m2.a(context, (String[]) arrayList.toArray(new String[size]), 1234323329);
            }
        } catch (Exception e2) {
            this.s.e(m.a().q().genCompleteErrorMessage(XAdErrorCode.PERMISSION_PROBLEM, e2.getMessage()));
            com.baidu.mobads.c.a.a().a("check permission exception: " + e2.toString());
        }
    }

    public void a(Context context, String str) {
        if (!m.a().m().hasPermission(context, str)) {
            String str2 = "Cannot request an ad without necessary permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"" + str + "\" />";
            this.s.e("BaiduMobAds SDK", str2);
            throw new SecurityException(str2);
        }
    }

    public void l() {
        if (this.h != null) {
            this.h.destroy();
        }
        BaiduXAdSDKContext.exit();
    }

    public IXAdResponseInfo getAdResponseInfo() {
        return this.x;
    }

    public void setAdResponseInfo(IXAdResponseInfo iXAdResponseInfo) {
        this.x = iXAdResponseInfo;
    }

    public Boolean isAdServerRequestingSuccess() {
        return this.b;
    }
}
