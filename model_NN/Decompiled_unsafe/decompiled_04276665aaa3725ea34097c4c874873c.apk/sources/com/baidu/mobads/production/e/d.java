package com.baidu.mobads.production.e;

import android.view.View;
import android.widget.RelativeLayout;

class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RelativeLayout.LayoutParams f570a;
    final /* synthetic */ View b;
    final /* synthetic */ c c;

    d(c cVar, RelativeLayout.LayoutParams layoutParams, View view) {
        this.c = cVar;
        this.f570a = layoutParams;
        this.b = view;
    }

    public void run() {
        this.f570a.addRule(11);
        this.f570a.rightMargin = 0;
        this.f570a.topMargin = this.b.getTop();
        this.c.g.setLayoutParams(this.f570a);
    }
}
