package com.baidu.mobads.interfaces;

import cn.banshenggua.aichang.room.message.SocketMessage;

public interface IXAdConstants4PDK {
    public static final String EVENT_ERROR = "EVENT_ERROR";
    public static final String EVENT_REQUEST_COMPLETE = "EVENT_REQUEST_COMPLETE";
    public static final String EVENT_REQUEST_CONTENT_VIDEO_PAUSE = "EVENT_REQUEST_CONTENT_VIDEO_PAUSE";
    public static final String EVENT_REQUEST_CONTENT_VIDEO_RESUME = "EVENT_REQUEST_CONTENT_VIDEO_RESUME";
    public static final String EVENT_SLOT_CLICKED = "EVENT_SLOT_CLICKED";
    public static final String EVENT_SLOT_ENDED = "EVENT_SLOT_ENDED";
    public static final String EVENT_SLOT_PRELOADED = "EVENT_SLOT_PRELOADED";
    public static final String EVENT_SLOT_STARTED = "EVENT_SLOT_STARTED";

    public enum VideoAssetPlayMode {
        VIDEO_ASSET_AUTO_PLAY_TYPE_ATTENDED("VIDEO_ASSET_AUTO_PLAY_TYPE_ATTENDED"),
        VIDEO_ASSET_AUTO_PLAY_TYPE_UNATTENDED("VIDEO_ASSET_AUTO_PLAY_TYPE_UNATTENDED");
        

        /* renamed from: a  reason: collision with root package name */
        private final String f511a;

        private VideoAssetPlayMode(String str) {
            this.f511a = str;
        }

        public String getValue() {
            return this.f511a;
        }

        public static VideoAssetPlayMode parse(String str) {
            for (VideoAssetPlayMode videoAssetPlayMode : values()) {
                if (videoAssetPlayMode.f511a.equalsIgnoreCase(str)) {
                    return videoAssetPlayMode;
                }
            }
            return null;
        }
    }

    public enum ScreenSizeMode {
        NORMAL("normal"),
        FULL_SCREEN("full_screen");
        

        /* renamed from: a  reason: collision with root package name */
        private final String f508a;

        private ScreenSizeMode(String str) {
            this.f508a = str;
        }

        public String getValue() {
            return this.f508a;
        }

        public static ScreenSizeMode parse(String str) {
            for (ScreenSizeMode screenSizeMode : values()) {
                if (screenSizeMode.f508a.equalsIgnoreCase(str)) {
                    return screenSizeMode;
                }
            }
            return null;
        }
    }

    public enum VideoState {
        IDLE("IDLE"),
        PLAYING("PLAYING"),
        PAUSED("PAUSED"),
        COMPLETED("COMPLETED");
        

        /* renamed from: a  reason: collision with root package name */
        private final String f512a;

        private VideoState(String str) {
            this.f512a = str;
        }

        public String getValue() {
            return this.f512a;
        }

        public static VideoState parse(String str) {
            for (VideoState videoState : values()) {
                if (videoState.f512a.equalsIgnoreCase(str)) {
                    return videoState;
                }
            }
            return null;
        }
    }

    public enum ActivityState {
        CREATE("CREATE"),
        START("START"),
        RESTART("RESTART"),
        PAUSE("PAUSE"),
        RESUME("RESUME"),
        STOP("STOP"),
        DESTROY("DESTROY");
        

        /* renamed from: a  reason: collision with root package name */
        private final String f507a;

        private ActivityState(String str) {
            this.f507a = str;
        }

        public String getValue() {
            return this.f507a;
        }

        public static ActivityState parse(String str) {
            for (ActivityState activityState : values()) {
                if (activityState.f507a.equalsIgnoreCase(str)) {
                    return activityState;
                }
            }
            return null;
        }
    }

    public enum SlotState {
        IDEL("idel"),
        LOADING("loading"),
        LOADED("loaded"),
        PLAYING("playing"),
        PAUSED("paused"),
        COMPLETED("completed"),
        ERROR(SocketMessage.MSG_ERROR_KEY);
        

        /* renamed from: a  reason: collision with root package name */
        private final String f509a;

        private SlotState(String str) {
            this.f509a = str;
        }

        public String getValue() {
            return this.f509a;
        }

        public static SlotState parse(String str) {
            for (SlotState slotState : values()) {
                if (slotState.f509a.equalsIgnoreCase(str)) {
                    return slotState;
                }
            }
            return null;
        }
    }

    public enum SlotType {
        SLOT_TYPE_BANNER("banner"),
        SLOT_TYPE_SPLASH("rsplash"),
        SLOT_TYPE_VERLINK("verlink"),
        SLOT_TYPE_FRONTLINK("frontlink"),
        SLOT_TYPE_INTERSTITIAL("int"),
        SLOT_TYPE_FEEDS("feed"),
        SLOT_TYPE_PREROLL("preroll"),
        SLOT_TYPE_MIDROLL("midroll"),
        SLOT_TYPE_POSTROLL("postroll"),
        SLOT_TYPE_OVERLAY("overlay"),
        SLOT_TYPE_PAUSE_ROLL("pauseroll");
        

        /* renamed from: a  reason: collision with root package name */
        private final String f510a;

        private SlotType(String str) {
            this.f510a = str;
        }

        public String getValue() {
            return this.f510a;
        }

        public static SlotType parse(String str) {
            for (SlotType slotType : values()) {
                if (slotType.f510a.equalsIgnoreCase(str)) {
                    return slotType;
                }
            }
            return null;
        }
    }

    public enum VisitorAction {
        PAUSE_BUTTON_CLICKED("PAUSE_BUTTON_CLICKED"),
        RESUME_BUTTON_CLICKED("RESUME_BUTTON_CLICKED");
        

        /* renamed from: a  reason: collision with root package name */
        private final String f513a;

        private VisitorAction(String str) {
            this.f513a = str;
        }

        public String getValue() {
            return this.f513a;
        }

        public static VisitorAction parse(String str) {
            for (VisitorAction visitorAction : values()) {
                if (visitorAction.f513a.equalsIgnoreCase(str)) {
                    return visitorAction;
                }
            }
            return null;
        }
    }
}
