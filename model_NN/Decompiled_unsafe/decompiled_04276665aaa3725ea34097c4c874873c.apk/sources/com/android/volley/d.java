package com.android.volley;

/* compiled from: DefaultRetryPolicy */
public class d implements p {

    /* renamed from: a  reason: collision with root package name */
    private int f330a;
    private int b;
    private final int c;
    private final float d;

    public d() {
        this(2500, 1, 1.0f);
    }

    public d(int i, int i2, float f) {
        this.f330a = i;
        this.c = i2;
        this.d = f;
    }

    public int a() {
        return this.f330a;
    }

    public int b() {
        return this.b;
    }

    public void a(s sVar) throws s {
        this.b++;
        this.f330a = (int) (((float) this.f330a) + (((float) this.f330a) * this.d));
        if (!c()) {
            throw sVar;
        }
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.b <= this.c;
    }
}
