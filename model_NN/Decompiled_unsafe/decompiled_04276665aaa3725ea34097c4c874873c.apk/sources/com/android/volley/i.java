package com.android.volley;

import com.igexin.download.Downloads;
import java.util.Map;

/* compiled from: NetworkResponse */
public class i {

    /* renamed from: a  reason: collision with root package name */
    public final int f335a;
    public final byte[] b;
    public final Map<String, String> c;
    public final boolean d;

    public i(int i, byte[] bArr, Map<String, String> map, boolean z) {
        this.f335a = i;
        this.b = bArr;
        this.c = map;
        this.d = z;
    }

    public i(byte[] bArr, Map<String, String> map) {
        this(Downloads.STATUS_SUCCESS, bArr, map, false);
    }
}
