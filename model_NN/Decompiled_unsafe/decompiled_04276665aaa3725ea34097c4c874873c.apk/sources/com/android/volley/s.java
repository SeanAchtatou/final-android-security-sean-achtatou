package com.android.volley;

/* compiled from: VolleyError */
public class s extends Exception {

    /* renamed from: a  reason: collision with root package name */
    public final i f342a;

    public s() {
        this.f342a = null;
    }

    public s(i iVar) {
        this.f342a = iVar;
    }

    public s(Throwable th) {
        super(th);
        this.f342a = null;
    }
}
