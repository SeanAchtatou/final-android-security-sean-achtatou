package com.android.volley.toolbox;

import android.os.SystemClock;
import com.android.volley.b;
import com.android.volley.t;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: DiskBasedCache */
public class c implements com.android.volley.b {

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, a> f351a;
    private long b;
    private final File c;
    private final int d;

    public c(File file, int i) {
        this.f351a = new LinkedHashMap(16, 0.75f, true);
        this.b = 0;
        this.c = file;
        this.d = i;
    }

    public c(File file) {
        this(file, 5242880);
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0066 A[SYNTHETIC, Splitter:B:33:0x0066] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.android.volley.b.a a(java.lang.String r9) {
        /*
            r8 = this;
            r1 = 0
            monitor-enter(r8)
            java.util.Map<java.lang.String, com.android.volley.toolbox.c$a> r0 = r8.f351a     // Catch:{ all -> 0x006a }
            java.lang.Object r0 = r0.get(r9)     // Catch:{ all -> 0x006a }
            com.android.volley.toolbox.c$a r0 = (com.android.volley.toolbox.c.a) r0     // Catch:{ all -> 0x006a }
            if (r0 != 0) goto L_0x000f
            r0 = r1
        L_0x000d:
            monitor-exit(r8)
            return r0
        L_0x000f:
            java.io.File r3 = r8.c(r9)     // Catch:{ all -> 0x006a }
            com.android.volley.toolbox.c$b r2 = new com.android.volley.toolbox.c$b     // Catch:{ IOException -> 0x003d, all -> 0x0062 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x003d, all -> 0x0062 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x003d, all -> 0x0062 }
            r5 = 0
            r2.<init>(r4, r5)     // Catch:{ IOException -> 0x003d, all -> 0x0062 }
            com.android.volley.toolbox.c.a.a(r2)     // Catch:{ IOException -> 0x0072 }
            long r4 = r3.length()     // Catch:{ IOException -> 0x0072 }
            int r6 = r2.f353a     // Catch:{ IOException -> 0x0072 }
            long r6 = (long) r6     // Catch:{ IOException -> 0x0072 }
            long r4 = r4 - r6
            int r4 = (int) r4     // Catch:{ IOException -> 0x0072 }
            byte[] r4 = a(r2, r4)     // Catch:{ IOException -> 0x0072 }
            com.android.volley.b$a r0 = r0.a(r4)     // Catch:{ IOException -> 0x0072 }
            if (r2 == 0) goto L_0x000d
            r2.close()     // Catch:{ IOException -> 0x003a }
            goto L_0x000d
        L_0x003a:
            r0 = move-exception
            r0 = r1
            goto L_0x000d
        L_0x003d:
            r0 = move-exception
            r2 = r1
        L_0x003f:
            java.lang.String r4 = "%s: %s"
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0070 }
            r6 = 0
            java.lang.String r3 = r3.getAbsolutePath()     // Catch:{ all -> 0x0070 }
            r5[r6] = r3     // Catch:{ all -> 0x0070 }
            r3 = 1
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0070 }
            r5[r3] = r0     // Catch:{ all -> 0x0070 }
            com.android.volley.t.b(r4, r5)     // Catch:{ all -> 0x0070 }
            r8.b(r9)     // Catch:{ all -> 0x0070 }
            if (r2 == 0) goto L_0x005d
            r2.close()     // Catch:{ IOException -> 0x005f }
        L_0x005d:
            r0 = r1
            goto L_0x000d
        L_0x005f:
            r0 = move-exception
            r0 = r1
            goto L_0x000d
        L_0x0062:
            r0 = move-exception
            r2 = r1
        L_0x0064:
            if (r2 == 0) goto L_0x0069
            r2.close()     // Catch:{ IOException -> 0x006d }
        L_0x0069:
            throw r0     // Catch:{ all -> 0x006a }
        L_0x006a:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x006d:
            r0 = move-exception
            r0 = r1
            goto L_0x000d
        L_0x0070:
            r0 = move-exception
            goto L_0x0064
        L_0x0072:
            r0 = move-exception
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.volley.toolbox.c.a(java.lang.String):com.android.volley.b$a");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0055 A[SYNTHETIC, Splitter:B:28:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x005a A[SYNTHETIC, Splitter:B:31:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0063 A[SYNTHETIC, Splitter:B:36:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x004d A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a() {
        /*
            r9 = this;
            r0 = 0
            monitor-enter(r9)
            java.io.File r1 = r9.c     // Catch:{ all -> 0x0067 }
            boolean r1 = r1.exists()     // Catch:{ all -> 0x0067 }
            if (r1 != 0) goto L_0x0025
            java.io.File r0 = r9.c     // Catch:{ all -> 0x0067 }
            boolean r0 = r0.mkdirs()     // Catch:{ all -> 0x0067 }
            if (r0 != 0) goto L_0x0023
            java.lang.String r0 = "Unable to create cache dir %s"
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0067 }
            r2 = 0
            java.io.File r3 = r9.c     // Catch:{ all -> 0x0067 }
            java.lang.String r3 = r3.getAbsolutePath()     // Catch:{ all -> 0x0067 }
            r1[r2] = r3     // Catch:{ all -> 0x0067 }
            com.android.volley.t.c(r0, r1)     // Catch:{ all -> 0x0067 }
        L_0x0023:
            monitor-exit(r9)
            return
        L_0x0025:
            java.io.File r1 = r9.c     // Catch:{ all -> 0x0067 }
            java.io.File[] r3 = r1.listFiles()     // Catch:{ all -> 0x0067 }
            if (r3 == 0) goto L_0x0023
            int r4 = r3.length     // Catch:{ all -> 0x0067 }
            r2 = r0
        L_0x002f:
            if (r2 >= r4) goto L_0x0023
            r5 = r3[r2]     // Catch:{ all -> 0x0067 }
            r1 = 0
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0051, all -> 0x0060 }
            r0.<init>(r5)     // Catch:{ IOException -> 0x0051, all -> 0x0060 }
            com.android.volley.toolbox.c$a r1 = com.android.volley.toolbox.c.a.a(r0)     // Catch:{ IOException -> 0x0073 }
            long r6 = r5.length()     // Catch:{ IOException -> 0x0073 }
            r1.f352a = r6     // Catch:{ IOException -> 0x0073 }
            java.lang.String r6 = r1.b     // Catch:{ IOException -> 0x0073 }
            r9.a(r6, r1)     // Catch:{ IOException -> 0x0073 }
            if (r0 == 0) goto L_0x004d
            r0.close()     // Catch:{ IOException -> 0x006c }
        L_0x004d:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x002f
        L_0x0051:
            r0 = move-exception
            r0 = r1
        L_0x0053:
            if (r5 == 0) goto L_0x0058
            r5.delete()     // Catch:{ all -> 0x006e }
        L_0x0058:
            if (r0 == 0) goto L_0x004d
            r0.close()     // Catch:{ IOException -> 0x005e }
            goto L_0x004d
        L_0x005e:
            r0 = move-exception
            goto L_0x004d
        L_0x0060:
            r0 = move-exception
        L_0x0061:
            if (r1 == 0) goto L_0x0066
            r1.close()     // Catch:{ IOException -> 0x006a }
        L_0x0066:
            throw r0     // Catch:{ all -> 0x0067 }
        L_0x0067:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x006a:
            r1 = move-exception
            goto L_0x0066
        L_0x006c:
            r0 = move-exception
            goto L_0x004d
        L_0x006e:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0061
        L_0x0073:
            r1 = move-exception
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.volley.toolbox.c.a():void");
    }

    public synchronized void a(String str, b.a aVar) {
        a(aVar.f327a.length);
        File c2 = c(str);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(c2);
            a aVar2 = new a(str, aVar);
            aVar2.a(fileOutputStream);
            fileOutputStream.write(aVar.f327a);
            fileOutputStream.close();
            a(str, aVar2);
        } catch (IOException e) {
            if (!c2.delete()) {
                t.b("Could not clean up file %s", c2.getAbsolutePath());
            }
        }
    }

    public synchronized void b(String str) {
        boolean delete = c(str).delete();
        e(str);
        if (!delete) {
            t.b("Could not delete cache entry for key=%s, filename=%s", str, d(str));
        }
    }

    private String d(String str) {
        int length = str.length() / 2;
        return String.valueOf(String.valueOf(str.substring(0, length).hashCode())) + String.valueOf(str.substring(length).hashCode());
    }

    public File c(String str) {
        return new File(this.c, d(str));
    }

    private void a(int i) {
        int i2;
        if (this.b + ((long) i) >= ((long) this.d)) {
            if (t.b) {
                t.a("Pruning old cache entries.", new Object[0]);
            }
            long j = this.b;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            Iterator<Map.Entry<String, a>> it = this.f351a.entrySet().iterator();
            int i3 = 0;
            while (true) {
                if (it.hasNext()) {
                    a aVar = (a) it.next().getValue();
                    if (c(aVar.b).delete()) {
                        this.b -= aVar.f352a;
                    } else {
                        t.b("Could not delete cache entry for key=%s, filename=%s", aVar.b, d(aVar.b));
                    }
                    it.remove();
                    i2 = i3 + 1;
                    if (((float) (this.b + ((long) i))) < ((float) this.d) * 0.9f) {
                        break;
                    }
                    i3 = i2;
                } else {
                    i2 = i3;
                    break;
                }
            }
            if (t.b) {
                t.a("pruned %d files, %d bytes, %d ms", Integer.valueOf(i2), Long.valueOf(this.b - j), Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
            }
        }
    }

    private void a(String str, a aVar) {
        if (!this.f351a.containsKey(str)) {
            this.b += aVar.f352a;
        } else {
            this.b = (aVar.f352a - this.f351a.get(str).f352a) + this.b;
        }
        this.f351a.put(str, aVar);
    }

    private void e(String str) {
        a aVar = this.f351a.get(str);
        if (aVar != null) {
            this.b -= aVar.f352a;
            this.f351a.remove(str);
        }
    }

    private static byte[] a(InputStream inputStream, int i) throws IOException {
        byte[] bArr = new byte[i];
        int i2 = 0;
        while (i2 < i) {
            int read = inputStream.read(bArr, i2, i - i2);
            if (read == -1) {
                break;
            }
            i2 += read;
        }
        if (i2 == i) {
            return bArr;
        }
        throw new IOException("Expected " + i + " bytes, read " + i2 + " bytes");
    }

    /* compiled from: DiskBasedCache */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        public long f352a;
        public String b;
        public String c;
        public long d;
        public long e;
        public long f;
        public Map<String, String> g;

        private a() {
        }

        public a(String str, b.a aVar) {
            this.b = str;
            this.f352a = (long) aVar.f327a.length;
            this.c = aVar.b;
            this.d = aVar.c;
            this.e = aVar.d;
            this.f = aVar.e;
            this.g = aVar.f;
        }

        public static a a(InputStream inputStream) throws IOException {
            a aVar = new a();
            if (c.a(inputStream) != 538051844) {
                throw new IOException();
            }
            aVar.b = c.c(inputStream);
            aVar.c = c.c(inputStream);
            if (aVar.c.equals("")) {
                aVar.c = null;
            }
            aVar.d = c.b(inputStream);
            aVar.e = c.b(inputStream);
            aVar.f = c.b(inputStream);
            aVar.g = c.d(inputStream);
            return aVar;
        }

        public b.a a(byte[] bArr) {
            b.a aVar = new b.a();
            aVar.f327a = bArr;
            aVar.b = this.c;
            aVar.c = this.d;
            aVar.d = this.e;
            aVar.e = this.f;
            aVar.f = this.g;
            return aVar;
        }

        public boolean a(OutputStream outputStream) {
            try {
                c.a(outputStream, 538051844);
                c.a(outputStream, this.b);
                c.a(outputStream, this.c == null ? "" : this.c);
                c.a(outputStream, this.d);
                c.a(outputStream, this.e);
                c.a(outputStream, this.f);
                c.a(this.g, outputStream);
                outputStream.flush();
                return true;
            } catch (IOException e2) {
                t.b("%s", e2.toString());
                return false;
            }
        }
    }

    /* compiled from: DiskBasedCache */
    private static class b extends FilterInputStream {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public int f353a;

        /* synthetic */ b(InputStream inputStream, b bVar) {
            this(inputStream);
        }

        private b(InputStream inputStream) {
            super(inputStream);
            this.f353a = 0;
        }

        public int read() throws IOException {
            int read = super.read();
            if (read != -1) {
                this.f353a++;
            }
            return read;
        }

        public int read(byte[] bArr, int i, int i2) throws IOException {
            int read = super.read(bArr, i, i2);
            if (read != -1) {
                this.f353a += read;
            }
            return read;
        }
    }

    private static int e(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        if (read != -1) {
            return read;
        }
        throw new EOFException();
    }

    static void a(OutputStream outputStream, int i) throws IOException {
        outputStream.write((i >> 0) & 255);
        outputStream.write((i >> 8) & 255);
        outputStream.write((i >> 16) & 255);
        outputStream.write((i >> 24) & 255);
    }

    static int a(InputStream inputStream) throws IOException {
        return 0 | (e(inputStream) << 0) | (e(inputStream) << 8) | (e(inputStream) << 16) | (e(inputStream) << 24);
    }

    static void a(OutputStream outputStream, long j) throws IOException {
        outputStream.write((byte) ((int) (j >>> 0)));
        outputStream.write((byte) ((int) (j >>> 8)));
        outputStream.write((byte) ((int) (j >>> 16)));
        outputStream.write((byte) ((int) (j >>> 24)));
        outputStream.write((byte) ((int) (j >>> 32)));
        outputStream.write((byte) ((int) (j >>> 40)));
        outputStream.write((byte) ((int) (j >>> 48)));
        outputStream.write((byte) ((int) (j >>> 56)));
    }

    static long b(InputStream inputStream) throws IOException {
        return 0 | ((((long) e(inputStream)) & 255) << 0) | ((((long) e(inputStream)) & 255) << 8) | ((((long) e(inputStream)) & 255) << 16) | ((((long) e(inputStream)) & 255) << 24) | ((((long) e(inputStream)) & 255) << 32) | ((((long) e(inputStream)) & 255) << 40) | ((((long) e(inputStream)) & 255) << 48) | ((((long) e(inputStream)) & 255) << 56);
    }

    static void a(OutputStream outputStream, String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        a(outputStream, (long) bytes.length);
        outputStream.write(bytes, 0, bytes.length);
    }

    static String c(InputStream inputStream) throws IOException {
        return new String(a(inputStream, (int) b(inputStream)), "UTF-8");
    }

    static void a(Map<String, String> map, OutputStream outputStream) throws IOException {
        if (map != null) {
            a(outputStream, map.size());
            for (Map.Entry next : map.entrySet()) {
                a(outputStream, (String) next.getKey());
                a(outputStream, (String) next.getValue());
            }
            return;
        }
        a(outputStream, 0);
    }

    static Map<String, String> d(InputStream inputStream) throws IOException {
        Map<String, String> hashMap;
        int a2 = a(inputStream);
        if (a2 == 0) {
            hashMap = Collections.emptyMap();
        } else {
            hashMap = new HashMap<>(a2);
        }
        for (int i = 0; i < a2; i++) {
            hashMap.put(c(inputStream).intern(), c(inputStream).intern());
        }
        return hashMap;
    }
}
