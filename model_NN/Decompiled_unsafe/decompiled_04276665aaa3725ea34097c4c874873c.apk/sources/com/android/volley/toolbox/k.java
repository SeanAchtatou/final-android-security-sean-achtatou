package com.android.volley.toolbox;

import cn.banshenggua.aichang.utils.StringUtil;
import com.android.volley.i;
import com.android.volley.l;
import com.android.volley.n;
import com.android.volley.t;
import java.io.UnsupportedEncodingException;

/* compiled from: JsonRequest */
public abstract class k<T> extends l<T> {

    /* renamed from: a  reason: collision with root package name */
    private static final String f363a = String.format("application/json; charset=%s", StringUtil.Encoding);
    private final n.b<T> b;
    private final String c;

    /* access modifiers changed from: protected */
    public abstract n<T> a(i iVar);

    public k(int i, String str, String str2, n.b<T> bVar, n.a aVar) {
        super(i, str, aVar);
        this.b = bVar;
        this.c = str2;
    }

    /* access modifiers changed from: protected */
    public void b(T t) {
        this.b.onResponse(t);
    }

    public String l() {
        return p();
    }

    public byte[] m() {
        return q();
    }

    public String p() {
        return f363a;
    }

    public byte[] q() {
        try {
            if (this.c == null) {
                return null;
            }
            return this.c.getBytes(StringUtil.Encoding);
        } catch (UnsupportedEncodingException e) {
            t.d("Unsupported Encoding while trying to get the bytes of %s using %s", this.c, StringUtil.Encoding);
            return null;
        }
    }
}
