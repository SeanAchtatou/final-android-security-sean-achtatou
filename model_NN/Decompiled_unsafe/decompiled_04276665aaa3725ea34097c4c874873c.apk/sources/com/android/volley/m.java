package com.android.volley;

import android.os.Handler;
import android.os.Looper;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: RequestQueue */
public class m {

    /* renamed from: a  reason: collision with root package name */
    private AtomicInteger f339a;
    private final Map<String, Queue<l>> b;
    private final Set<l> c;
    private final PriorityBlockingQueue<l> d;
    private final PriorityBlockingQueue<l> e;
    private final b f;
    private final f g;
    private final o h;
    private g[] i;
    private c j;

    /* compiled from: RequestQueue */
    public interface a {
        boolean a(l<?> lVar);
    }

    public m(b bVar, f fVar, int i2, o oVar) {
        this.f339a = new AtomicInteger();
        this.b = new HashMap();
        this.c = new HashSet();
        this.d = new PriorityBlockingQueue<>();
        this.e = new PriorityBlockingQueue<>();
        this.f = bVar;
        this.g = fVar;
        this.i = new g[i2];
        this.h = oVar;
    }

    public m(b bVar, f fVar, int i2) {
        this(bVar, fVar, i2, new e(new Handler(Looper.getMainLooper())));
    }

    public m(b bVar, f fVar) {
        this(bVar, fVar, 4);
    }

    public void a() {
        b();
        this.j = new c(this.d, this.e, this.f, this.h);
        this.j.start();
        for (int i2 = 0; i2 < this.i.length; i2++) {
            g gVar = new g(this.e, this.g, this.f, this.h);
            this.i[i2] = gVar;
            gVar.start();
        }
    }

    public void b() {
        if (this.j != null) {
            this.j.a();
        }
        for (int i2 = 0; i2 < this.i.length; i2++) {
            if (this.i[i2] != null) {
                this.i[i2].a();
            }
        }
    }

    public int c() {
        return this.f339a.incrementAndGet();
    }

    public void a(a aVar) {
        synchronized (this.c) {
            for (l next : this.c) {
                if (aVar.a(next)) {
                    next.g();
                }
            }
        }
    }

    public void a(final Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Cannot cancelAll with a null tag");
        }
        a((a) new a() {
            public boolean a(l<?> lVar) {
                return lVar.b() == obj;
            }
        });
    }

    public l a(l lVar) {
        lVar.a(this);
        synchronized (this.c) {
            this.c.add(lVar);
        }
        lVar.a(c());
        lVar.a("add-to-queue");
        if (!lVar.r()) {
            this.e.add(lVar);
        } else {
            synchronized (this.b) {
                String e2 = lVar.e();
                if (this.b.containsKey(e2)) {
                    Object obj = this.b.get(e2);
                    if (obj == null) {
                        obj = new LinkedList();
                    }
                    obj.add(lVar);
                    this.b.put(e2, obj);
                    if (t.b) {
                        t.a("Request for cacheKey=%s is in flight, putting on hold.", e2);
                    }
                } else {
                    this.b.put(e2, null);
                    this.d.add(lVar);
                }
            }
        }
        return lVar;
    }

    /* access modifiers changed from: package-private */
    public void b(l lVar) {
        synchronized (this.c) {
            this.c.remove(lVar);
        }
        if (lVar.r()) {
            synchronized (this.b) {
                String e2 = lVar.e();
                Queue remove = this.b.remove(e2);
                if (remove != null) {
                    if (t.b) {
                        t.a("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(remove.size()), e2);
                    }
                    this.d.addAll(remove);
                }
            }
        }
    }
}
