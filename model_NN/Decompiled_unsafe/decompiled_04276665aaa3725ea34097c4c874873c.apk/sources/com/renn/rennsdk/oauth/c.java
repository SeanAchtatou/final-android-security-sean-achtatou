package com.renn.rennsdk.oauth;

/* compiled from: RRException */
public class c extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private int f1266a;
    private String b;
    private String c;

    public c(String str) {
        super(str);
        this.b = str;
    }

    public String toString() {
        return "RRException [mExceptionCode=" + this.f1266a + ", mExceptionMsg=" + this.b + ", mExceptionDescription=" + this.c + "]";
    }
}
