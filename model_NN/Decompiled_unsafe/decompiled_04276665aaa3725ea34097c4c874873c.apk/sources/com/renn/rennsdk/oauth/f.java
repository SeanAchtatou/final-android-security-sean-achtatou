package com.renn.rennsdk.oauth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.sina.weibo.sdk.register.mobile.MobileRegisterActivity;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: RenrenAccountManager */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private Activity f1271a;
    private AccountManager b;
    private String c;
    private String d;
    private String e;
    /* access modifiers changed from: private */
    public d f;
    private Handler g;
    private String h = null;
    private String i = null;
    private String j = null;
    private String k = null;
    private String l = null;
    private long m = -1;
    /* access modifiers changed from: private */
    public String n = null;
    /* access modifiers changed from: private */
    public boolean o = false;
    private a p = new a(this, null);
    private b q = new b(this, null);
    private c r = new c(this, null);

    /* compiled from: RenrenAccountManager */
    public interface d {
        void a(e eVar);

        void a(boolean z);
    }

    /* compiled from: RenrenAccountManager */
    public enum e {
        OPERATION_CANCELED,
        AUTHENTICATOR_EXCEPTION,
        IO_EXCEPTION
    }

    public f(Activity activity, String str, String str2, String str3, String str4) {
        this.f1271a = activity;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.j = str4;
        this.b = AccountManager.get(this.f1271a);
        this.g = new Handler();
    }

    public boolean a(d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("the login callback is null for RenrenAccountManager.login(LoginCallback cb)");
        } else if (!g()) {
            return false;
        } else {
            this.f = dVar;
            h();
            return true;
        }
    }

    public String a() {
        if (this.h != null) {
            return this.h;
        }
        throw new IllegalStateException("please call login and success before call RenrenAccountManager.getAccessToken()");
    }

    public String b() {
        if (this.i != null) {
            return this.i;
        }
        throw new IllegalStateException("please call login and success before call RenrenAccountManager.getScope()");
    }

    public long c() {
        if (this.m >= 0) {
            return this.m;
        }
        throw new IllegalStateException("please call login and success before call RenrenAccountManager.getExpires()");
    }

    public String d() {
        if (this.j != null) {
            return this.j;
        }
        throw new IllegalStateException("please call login and success before call RenrenAccountManager.getScope()");
    }

    public String e() {
        if (this.k != null) {
            return this.k;
        }
        throw new IllegalStateException("please call login and success before call RenrenAccountManager.getScope()");
    }

    public String f() {
        if (this.l != null) {
            return this.l;
        }
        throw new IllegalStateException("please call login and success before call RenrenAccountManager.getScope()");
    }

    public boolean g() {
        boolean z;
        boolean z2;
        boolean z3;
        try {
            if (this.f1271a.getPackageManager().getPackageInfo("com.renren.mobile.android", 0).versionCode <= 5090200) {
                z3 = false;
            } else {
                z3 = true;
            }
            try {
                if (Integer.parseInt(this.f1271a.getPackageManager().getPackageInfo("com.renren.mobile.android", 0).versionName.split("[.]")[0].replace("v", "")) < 5) {
                    z = false;
                } else {
                    z = z3;
                }
            } catch (Exception e2) {
                z = z3;
            }
        } catch (PackageManager.NameNotFoundException e3) {
            z = false;
        }
        try {
            if (this.f1271a.getPackageManager().getPackageInfo("com.renren.mobile.apad", 0).versionCode < 3000000) {
                z2 = false;
            } else {
                z2 = true;
            }
        } catch (PackageManager.NameNotFoundException e4) {
            z2 = false;
        }
        if (!z && !z2) {
            return false;
        }
        if (z && !z2) {
            this.n = "com.renren.renren_account_manager";
            this.o = false;
            return true;
        } else if (z || !z2) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.f1271a.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            double d2 = (double) (displayMetrics.density * 160.0f);
            double d3 = (double) displayMetrics.widthPixels;
            double d4 = (double) displayMetrics.heightPixels;
            if ((d4 * d4) + (d3 * d3) > d2 * d2 * 5.0d * 5.0d) {
                this.n = "com.renren.renren_account_manager_for_hd";
                this.o = true;
                return false;
            }
            this.n = "com.renren.renren_account_manager";
            this.o = false;
            return true;
        } else {
            this.n = "com.renren.renren_account_manager_for_hd";
            this.o = true;
            return false;
        }
    }

    private void h() {
        Account[] accountsByType = this.b.getAccountsByType(this.n);
        if (accountsByType.length == 0) {
            this.b.addAccount(this.n, null, null, null, this.f1271a, this.p, this.g);
        } else {
            a(accountsByType[0]);
        }
    }

    /* access modifiers changed from: private */
    public void a(Account account) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(Parameters.PACKAGE_NAME, this.f1271a.getPackageName());
            jSONObject.put("client_id", this.c);
            if (!TextUtils.isEmpty(this.j)) {
                jSONObject.put("token_type", this.j);
            }
            if (!TextUtils.isEmpty(this.d)) {
                jSONObject.put("scope", this.d);
            }
            if (!TextUtils.isEmpty(this.e)) {
                jSONObject.put("client_info", this.e);
            }
            Bundle bundle = new Bundle();
            bundle.putString("API_VERSION", "2.0");
            bundle.putBoolean("key_for_fix_4_0_bug", true);
            bundle.putParcelable("key_for_fix_4_0_bug_account", account);
            this.b.addAccount(this.n, jSONObject.toString(), null, bundle, this.f1271a, this.q, this.g);
        } catch (JSONException e2) {
            throw new RuntimeException();
        }
    }

    /* access modifiers changed from: private */
    public void a(Bundle bundle) {
        String string = bundle.getString("authtoken");
        if (string == null) {
            string = bundle.getString("key_for_fix_4_0_bug_token");
        }
        this.b.invalidateAuthToken(this.n, string);
        try {
            JSONObject jSONObject = new JSONObject(string);
            this.h = jSONObject.getString("access_token");
            this.m = Long.parseLong(jSONObject.getString(MobileRegisterActivity.RESPONSE_EXPIRES));
            this.k = jSONObject.optString("mac_key");
            this.l = jSONObject.optString("mac_algorithm");
            this.i = jSONObject.optString("scope", "");
        } catch (JSONException e2) {
            throw new RuntimeException();
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        this.b.updateCredentials(new Account(str, this.n), "", null, this.f1271a, this.r, this.g);
    }

    /* compiled from: RenrenAccountManager */
    private class a implements AccountManagerCallback<Bundle> {
        private a() {
        }

        /* synthetic */ a(f fVar, a aVar) {
            this();
        }

        public void run(AccountManagerFuture<Bundle> accountManagerFuture) {
            Bundle bundle;
            try {
                bundle = accountManagerFuture.getResult();
            } catch (OperationCanceledException e) {
                if (f.this.f != null) {
                    f.this.f.a(e.OPERATION_CANCELED);
                    bundle = null;
                }
                bundle = null;
            } catch (AuthenticatorException e2) {
                if (f.this.f != null) {
                    f.this.f.a(e.AUTHENTICATOR_EXCEPTION);
                    bundle = null;
                }
                bundle = null;
            } catch (IOException e3) {
                if (f.this.f != null) {
                    f.this.f.a(e.IO_EXCEPTION);
                }
                bundle = null;
            }
            if (bundle != null) {
                f.this.a(new Account(bundle.getString("authAccount"), f.this.n));
            }
        }
    }

    /* compiled from: RenrenAccountManager */
    private class b implements AccountManagerCallback<Bundle> {
        private b() {
        }

        /* synthetic */ b(f fVar, b bVar) {
            this();
        }

        public void run(AccountManagerFuture<Bundle> accountManagerFuture) {
            Bundle bundle;
            try {
                bundle = accountManagerFuture.getResult();
            } catch (OperationCanceledException e) {
                if (f.this.f != null) {
                    f.this.f.a(e.OPERATION_CANCELED);
                    bundle = null;
                }
                bundle = null;
            } catch (AuthenticatorException e2) {
                if (e2.getMessage().startsWith("[ACCOUNT_VERIFY_HEAD]")) {
                    f.this.a(e2.getMessage().substring("[ACCOUNT_VERIFY_HEAD]".length()));
                    bundle = null;
                } else {
                    if (f.this.f != null) {
                        f.this.f.a(e.AUTHENTICATOR_EXCEPTION);
                        bundle = null;
                    }
                    bundle = null;
                }
            } catch (IOException e3) {
                if (f.this.f != null) {
                    f.this.f.a(e.IO_EXCEPTION);
                }
                bundle = null;
            }
            if (bundle != null) {
                f.this.a(bundle);
                if (f.this.f != null) {
                    f.this.f.a(f.this.o);
                }
            }
        }
    }

    /* compiled from: RenrenAccountManager */
    private class c implements AccountManagerCallback<Bundle> {
        private c() {
        }

        /* synthetic */ c(f fVar, c cVar) {
            this();
        }

        public void run(AccountManagerFuture<Bundle> accountManagerFuture) {
            Bundle bundle;
            try {
                bundle = accountManagerFuture.getResult();
            } catch (OperationCanceledException e) {
                if (f.this.f != null) {
                    f.this.f.a(e.OPERATION_CANCELED);
                    bundle = null;
                }
                bundle = null;
            } catch (AuthenticatorException e2) {
                if (f.this.f != null) {
                    f.this.f.a(e.AUTHENTICATOR_EXCEPTION);
                    bundle = null;
                }
                bundle = null;
            } catch (IOException e3) {
                if (f.this.f != null) {
                    f.this.f.a(e.IO_EXCEPTION);
                }
                bundle = null;
            }
            if (bundle != null) {
                f.this.a(new Account(bundle.getString("authAccount"), f.this.n));
            }
        }
    }
}
