package com.renn.rennsdk.oauth;

import android.content.Context;
import android.content.SharedPreferences;
import com.renn.rennsdk.a;

/* compiled from: ValueStorage */
public class i {
    private static i c;

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f1281a;
    private SharedPreferences.Editor b = this.f1281a.edit();

    public i(Context context) {
        this.f1281a = context.getSharedPreferences(context.getPackageName(), 0);
    }

    public static synchronized i a(Context context) {
        i iVar;
        synchronized (i.class) {
            if (c == null) {
                c = new i(context);
            }
            iVar = c;
        }
        return iVar;
    }

    public void a(String str, String str2) {
        this.b.putString(str, str2);
        this.b.commit();
    }

    public String a(String str) {
        return this.f1281a.getString(str, "");
    }

    public void a(String str, Long l) {
        this.b.putLong(str, l.longValue());
        this.b.commit();
    }

    public Long b(String str) {
        return Long.valueOf(this.f1281a.getLong(str, 0));
    }

    public void a(String str, a.C0037a aVar) {
        if (aVar == a.C0037a.Bearer) {
            this.b.putInt(str, 0);
        } else if (aVar == a.C0037a.MAC) {
            this.b.putInt(str, 1);
        }
        this.b.commit();
    }

    public a.C0037a c(String str) {
        if (this.f1281a.getInt(str, 0) == 1) {
            return a.C0037a.MAC;
        }
        return a.C0037a.Bearer;
    }
}
