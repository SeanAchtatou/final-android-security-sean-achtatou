package com.b.a.a.a;

import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.IOException;

public class t {

    /* renamed from: a  reason: collision with root package name */
    public static t f377a = new t(z.f381a, aa.f366a);
    private final o b;
    private final k c;
    private final boolean d;

    t(ac acVar, boolean z, s sVar) throws ad, IOException {
        this.d = z;
        switch (sVar.f376a) {
            case -3:
                if (sVar.c.equals("text")) {
                    if (sVar.a() == 40 && sVar.a() == 41) {
                        this.b = y.f380a;
                        break;
                    } else {
                        throw new ad(acVar, "after text", sVar, "()");
                    }
                } else {
                    this.b = new m(sVar.c);
                    break;
                }
                break;
            case 42:
                this.b = a.f365a;
                break;
            case 46:
                if (sVar.a() != 46) {
                    sVar.b();
                    this.b = z.f381a;
                    break;
                } else {
                    this.b = q.f374a;
                    break;
                }
            case 64:
                if (sVar.a() == -3) {
                    this.b = new j(sVar.c);
                    break;
                } else {
                    throw new ad(acVar, "after @ in node test", sVar, SelectCountryActivity.EXTRA_COUNTRY_NAME);
                }
            default:
                throw new ad(acVar, "at begininning of step", sVar, "'.' or '*' or name");
        }
        if (sVar.a() == 91) {
            sVar.a();
            this.c = n.a(acVar, sVar);
            if (sVar.f376a != 93) {
                throw new ad(acVar, "after predicate expression", sVar, "]");
            }
            sVar.a();
            return;
        }
        this.c = aa.f366a;
    }

    t(o oVar, k kVar) {
        this.b = oVar;
        this.c = kVar;
        this.d = false;
    }

    public boolean a() {
        return this.d;
    }

    public boolean b() {
        return this.b.a();
    }

    public o c() {
        return this.b;
    }

    public k d() {
        return this.c;
    }

    public String toString() {
        return new StringBuffer().append(this.b.toString()).append(this.c.toString()).toString();
    }
}
