package com.b.a.a;

import java.util.Hashtable;

public class r {

    /* renamed from: a  reason: collision with root package name */
    private static d f390a = new s();
    private static b b = new t();

    public interface a {
    }

    public interface b {
        a a();
    }

    private static class c extends Hashtable implements a {
        private c() {
        }

        c(s sVar) {
            this();
        }
    }

    public interface d {
        String a(String str);
    }

    static a a() {
        return b.a();
    }

    public static String a(String str) {
        return f390a.a(str);
    }
}
