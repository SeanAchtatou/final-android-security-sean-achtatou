package com.b.a.a.a;

public abstract class i extends e {

    /* renamed from: a  reason: collision with root package name */
    private final int f371a;

    i(String str, int i) {
        super(str);
        this.f371a = i;
    }

    public double a() {
        return (double) this.f371a;
    }

    /* access modifiers changed from: protected */
    public String a(String str) {
        return new StringBuffer().append("[").append(super.toString()).append(str).append("'").append(this.f371a).append("']").toString();
    }
}
