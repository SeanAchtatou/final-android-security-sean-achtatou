package com.b.a.a;

class b {

    /* renamed from: a  reason: collision with root package name */
    private final int[] f382a;
    private int b;
    private int c;

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer((this.f382a.length * 11) / 10);
        for (int length = this.c < this.f382a.length ? this.f382a.length - this.c : 0; length < this.f382a.length; length++) {
            int i = this.f382a[(this.b + length) % this.f382a.length];
            if (i < 65536) {
                stringBuffer.append((char) i);
            } else {
                stringBuffer.append(Integer.toString(i - 65536));
            }
        }
        return stringBuffer.toString();
    }
}
