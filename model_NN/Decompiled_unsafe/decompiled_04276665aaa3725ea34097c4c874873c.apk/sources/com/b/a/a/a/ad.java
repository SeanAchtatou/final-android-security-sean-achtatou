package com.b.a.a.a;

import java.io.IOException;

public class ad extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private Throwable f368a;

    ad(ac acVar, Exception exc) {
        super(new StringBuffer().append(acVar).append(" ").append(exc).toString());
        this.f368a = null;
        this.f368a = exc;
    }

    public ad(ac acVar, String str) {
        super(new StringBuffer().append(acVar).append(" ").append(str).toString());
        this.f368a = null;
    }

    ad(ac acVar, String str, s sVar, String str2) {
        this(acVar, new StringBuffer().append(str).append(" got \"").append(a(sVar)).append("\" instead of expected ").append(str2).toString());
    }

    private static String a(s sVar) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(b(sVar));
            if (sVar.f376a != -1) {
                sVar.a();
                stringBuffer.append(b(sVar));
                sVar.b();
            }
            return stringBuffer.toString();
        } catch (IOException e) {
            return new StringBuffer().append("(cannot get  info: ").append(e).append(")").toString();
        }
    }

    private static String b(s sVar) {
        switch (sVar.f376a) {
            case -3:
                return sVar.c;
            case -2:
                return new StringBuffer().append(sVar.b).append("").toString();
            case -1:
                return "<end of expression>";
            default:
                return new StringBuffer().append((char) sVar.f376a).append("").toString();
        }
    }

    public Throwable getCause() {
        return this.f368a;
    }
}
