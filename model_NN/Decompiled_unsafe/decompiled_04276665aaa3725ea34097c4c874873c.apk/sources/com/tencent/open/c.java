package com.tencent.open;

import android.content.Context;
import android.location.Location;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private d f2515a;

    /* compiled from: ProGuard */
    public interface a {
        void onLocationUpdate(Location location);
    }

    public boolean a() {
        return com.tencent.map.a.a.a.a().a("OpenSdk", "WQMPF-XMH66-ISQXP-OIGMM-BNL7M");
    }

    public void a(Context context, a aVar) {
        this.f2515a = new d(aVar);
        com.tencent.map.a.a.a.a().a(context, this.f2515a);
    }

    public void b() {
        com.tencent.map.a.a.a.a().b();
        this.f2515a = null;
    }
}
