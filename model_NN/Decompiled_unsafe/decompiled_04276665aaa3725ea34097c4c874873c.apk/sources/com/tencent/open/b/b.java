package com.tencent.open.b;

import android.os.Bundle;
import java.io.Serializable;
import java.util.HashMap;

/* compiled from: ProGuard */
public class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public final HashMap<String, String> f2504a = new HashMap<>();

    public b(Bundle bundle) {
        if (bundle != null) {
            for (String next : bundle.keySet()) {
                this.f2504a.put(next, bundle.getString(next));
            }
        }
    }
}
