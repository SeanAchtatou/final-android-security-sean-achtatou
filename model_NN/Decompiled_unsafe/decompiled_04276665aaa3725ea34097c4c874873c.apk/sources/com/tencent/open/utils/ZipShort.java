package com.tencent.open.utils;

import android.support.v4.view.MotionEventCompat;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: ProGuard */
public final class ZipShort implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int f2540a;

    public ZipShort(byte[] bArr) {
        this(bArr, 0);
    }

    public ZipShort(byte[] bArr, int i) {
        this.f2540a = (bArr[i + 1] << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK;
        this.f2540a += bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
    }

    public ZipShort(int i) {
        this.f2540a = i;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ZipShort) || this.f2540a != ((ZipShort) obj).getValue()) {
            return false;
        }
        return true;
    }

    public byte[] getBytes() {
        return new byte[]{(byte) (this.f2540a & 255), (byte) ((this.f2540a & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8)};
    }

    public int getValue() {
        return this.f2540a;
    }

    public int hashCode() {
        return this.f2540a;
    }
}
