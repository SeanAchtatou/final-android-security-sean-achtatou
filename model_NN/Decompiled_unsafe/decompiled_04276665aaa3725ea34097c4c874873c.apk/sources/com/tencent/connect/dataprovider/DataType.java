package com.tencent.connect.dataprovider;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public final class DataType {
    public static final int CONTENT_AND_IMAGE_PATH = 1;
    public static final int CONTENT_AND_VIDEO_PATH = 2;
    public static final int CONTENT_ONLY = 4;

    /* compiled from: ProGuard */
    public static class TextAndMediaPath implements Parcelable {
        public static final Parcelable.Creator<TextAndMediaPath> CREATOR = new Parcelable.Creator<TextAndMediaPath>() {
            public TextAndMediaPath createFromParcel(Parcel parcel) {
                return new TextAndMediaPath(parcel);
            }

            public TextAndMediaPath[] newArray(int i) {
                return new TextAndMediaPath[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private String f2411a;
        private String b;

        public TextAndMediaPath(String str, String str2) {
            this.f2411a = str;
            this.b = str2;
        }

        public String getText() {
            return this.f2411a;
        }

        public String getMediaPath() {
            return this.b;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f2411a);
            parcel.writeString(this.b);
        }

        private TextAndMediaPath(Parcel parcel) {
            this.f2411a = parcel.readString();
            this.b = parcel.readString();
        }
    }

    /* compiled from: ProGuard */
    public static class TextOnly implements Parcelable {
        public static final Parcelable.Creator<TextOnly> CREATOR = new Parcelable.Creator<TextOnly>() {
            public TextOnly createFromParcel(Parcel parcel) {
                return new TextOnly(parcel);
            }

            public TextOnly[] newArray(int i) {
                return new TextOnly[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private String f2412a;

        public TextOnly(String str) {
            this.f2412a = str;
        }

        public String getText() {
            return this.f2412a;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f2412a);
        }

        private TextOnly(Parcel parcel) {
            this.f2412a = parcel.readString();
        }
    }
}
