package com.tencent.wxop.stat.a;

import android.content.Context;
import com.meizu.cloud.pushsdk.notification.model.NotificationStyle;
import com.tencent.wxop.stat.e;
import com.tencent.wxop.stat.f;
import java.util.Map;
import java.util.Properties;
import org.json.JSONException;
import org.json.JSONObject;

public final class a extends d {
    protected b bj = new b();
    private long bk = -1;

    public a(Context context, int i, String str, f fVar) {
        super(context, i, fVar);
        this.bj.f2610a = str;
    }

    public final b ab() {
        return this.bj;
    }

    public final e ac() {
        return e.CUSTOM;
    }

    public final boolean b(JSONObject jSONObject) {
        Properties p;
        jSONObject.put(NotificationStyle.EXPANDABLE_IMAGE_URL, this.bj.f2610a);
        if (this.bk > 0) {
            jSONObject.put("du", this.bk);
        }
        if (this.bj.bl == null) {
            if (!(this.bj.f2610a == null || (p = e.p(this.bj.f2610a)) == null || p.size() <= 0)) {
                if (this.bj.bm == null || this.bj.bm.length() == 0) {
                    this.bj.bm = new JSONObject(p);
                } else {
                    for (Map.Entry entry : p.entrySet()) {
                        try {
                            this.bj.bm.put(entry.getKey().toString(), entry.getValue());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            jSONObject.put("kv", this.bj.bm);
            return true;
        }
        jSONObject.put("ar", this.bj.bl);
        return true;
    }
}
