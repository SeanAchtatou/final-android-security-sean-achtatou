package com.tencent.wxop.stat.b;

import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

final class k extends i {
    static final /* synthetic */ boolean ad = (!h.class.desiredAssertionStatus());
    private static final byte[] cM = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, SocksProxyConstants.V4_REPLY_REQUEST_GRANTED, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] cN = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, SocksProxyConstants.V4_REPLY_REQUEST_GRANTED, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    public final boolean ba = true;
    public final boolean bb = true;
    private final byte[] cO = new byte[2];
    public final boolean cP = false;
    private final byte[] cQ = cM;
    private int cc;
    int cp = 0;

    public k() {
        this.cI = null;
        this.cc = this.bb ? 19 : -1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean a(byte[] bArr, int i) {
        byte b;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        byte b2;
        byte b3;
        int i7;
        byte b4;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12 = 0;
        byte[] bArr2 = this.cQ;
        byte[] bArr3 = this.cI;
        int i13 = this.cc;
        int i14 = i + 0;
        switch (this.cp) {
            case 0:
                b = -1;
                i2 = 0;
                break;
            case 1:
                if (2 <= i14) {
                    this.cp = 0;
                    b = ((this.cO[0] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr[0] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
                    i2 = 2;
                    break;
                }
                b = -1;
                i2 = 0;
                break;
            case 2:
                if (i14 > 0) {
                    this.cp = 0;
                    b = ((this.cO[0] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((this.cO[1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[0] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
                    i2 = 1;
                    break;
                }
                b = -1;
                i2 = 0;
                break;
            default:
                b = -1;
                i2 = 0;
                break;
        }
        if (b != -1) {
            bArr3[0] = bArr2[(b >> 18) & 63];
            bArr3[1] = bArr2[(b >> 12) & 63];
            bArr3[2] = bArr2[(b >> 6) & 63];
            int i15 = 4;
            bArr3[3] = bArr2[b & 63];
            int i16 = i13 - 1;
            if (i16 == 0) {
                if (this.cP) {
                    i15 = 5;
                    bArr3[4] = 13;
                }
                i5 = i15 + 1;
                bArr3[i15] = 10;
                i4 = 19;
            } else {
                i4 = i16;
                i5 = 4;
            }
        } else {
            i4 = i13;
            i5 = 0;
        }
        while (i3 + 3 <= i14) {
            byte b5 = ((bArr[i3] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr[i3 + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[i3 + 2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
            bArr3[i5] = bArr2[(b5 >> 18) & 63];
            bArr3[i5 + 1] = bArr2[(b5 >> 12) & 63];
            bArr3[i5 + 2] = bArr2[(b5 >> 6) & 63];
            bArr3[i5 + 3] = bArr2[b5 & 63];
            int i17 = i3 + 3;
            int i18 = i5 + 4;
            int i19 = i4 - 1;
            if (i19 == 0) {
                if (this.cP) {
                    i11 = i18 + 1;
                    bArr3[i18] = 13;
                } else {
                    i11 = i18;
                }
                i10 = i11 + 1;
                bArr3[i11] = 10;
                i3 = i17;
                i9 = 19;
            } else {
                i9 = i19;
                i10 = i18;
                i3 = i17;
            }
        }
        if (i3 - this.cp == i14 - 1) {
            if (this.cp > 0) {
                b4 = this.cO[0];
                i8 = 1;
            } else {
                b4 = bArr[i3];
                i3++;
                i8 = 0;
            }
            int i20 = (b4 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 4;
            this.cp -= i8;
            int i21 = i5 + 1;
            bArr3[i5] = bArr2[(i20 >> 6) & 63];
            int i22 = i21 + 1;
            bArr3[i21] = bArr2[i20 & 63];
            if (this.ba) {
                int i23 = i22 + 1;
                bArr3[i22] = 61;
                i22 = i23 + 1;
                bArr3[i23] = 61;
            }
            if (this.bb) {
                if (this.cP) {
                    bArr3[i22] = 13;
                    i22++;
                }
                bArr3[i22] = 10;
                i22++;
            }
            i5 = i22;
        } else if (i3 - this.cp == i14 - 2) {
            if (this.cp > 1) {
                b2 = this.cO[0];
                i12 = 1;
            } else {
                b2 = bArr[i3];
                i3++;
            }
            int i24 = (b2 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 10;
            if (this.cp > 0) {
                b3 = this.cO[i12];
                i12++;
            } else {
                b3 = bArr[i3];
                i3++;
            }
            int i25 = ((b3 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 2) | i24;
            this.cp -= i12;
            int i26 = i5 + 1;
            bArr3[i5] = bArr2[(i25 >> 12) & 63];
            int i27 = i26 + 1;
            bArr3[i26] = bArr2[(i25 >> 6) & 63];
            int i28 = i27 + 1;
            bArr3[i27] = bArr2[i25 & 63];
            if (this.ba) {
                i7 = i28 + 1;
                bArr3[i28] = 61;
            } else {
                i7 = i28;
            }
            if (this.bb) {
                if (this.cP) {
                    bArr3[i7] = 13;
                    i7++;
                }
                bArr3[i7] = 10;
                i7++;
            }
            i5 = i7;
        } else if (this.bb && i5 > 0 && i4 != 19) {
            if (this.cP) {
                i6 = i5 + 1;
                bArr3[i5] = 13;
            } else {
                i6 = i5;
            }
            i5 = i6 + 1;
            bArr3[i6] = 10;
        }
        if (!ad && this.cp != 0) {
            throw new AssertionError();
        } else if (ad || i3 == i14) {
            this.g = i5;
            this.cc = i4;
            return true;
        } else {
            throw new AssertionError();
        }
    }
}
