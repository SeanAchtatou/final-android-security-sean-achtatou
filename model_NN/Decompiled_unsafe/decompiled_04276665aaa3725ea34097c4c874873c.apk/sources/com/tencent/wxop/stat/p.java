package com.tencent.wxop.stat;

import android.content.Context;
import com.tencent.wxop.stat.a.d;

final class p {
    private static volatile long bU = 0;
    /* access modifiers changed from: private */
    public d bP;
    private d bQ = null;
    /* access modifiers changed from: private */
    public boolean bR = false;
    /* access modifiers changed from: private */
    public Context bS = null;
    private long bT = System.currentTimeMillis();

    public p(d dVar) {
        this.bP = dVar;
        this.bQ = c.j();
        this.bR = dVar.X();
        this.bS = dVar.J();
    }

    private void H() {
        if (t.ai().aI <= 0 || !c.ax) {
            a(new s(this));
            return;
        }
        t.ai().b(this.bP, null, this.bR, true);
        t.ai().b(-1);
    }

    private void a(aj ajVar) {
        ak.Z(e.aY).a(this.bP, ajVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:80:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void ah() {
        /*
            r9 = this;
            r1 = 1
            r8 = 0
            r2 = 0
            int r0 = com.tencent.wxop.stat.c.ae
            if (r0 <= 0) goto L_0x00cc
            long r4 = r9.bT
            long r6 = com.tencent.wxop.stat.e.aO
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x0040
            java.util.Map r0 = com.tencent.wxop.stat.e.aN
            r0.clear()
            long r4 = r9.bT
            long r6 = com.tencent.wxop.stat.c.af
            long r4 = r4 + r6
            long unused = com.tencent.wxop.stat.e.aO = r4
            boolean r0 = com.tencent.wxop.stat.c.k()
            if (r0 == 0) goto L_0x0040
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.e.aV
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "clear methodsCalledLimitMap, nextLimitCallClearTime="
            r3.<init>(r4)
            long r4 = com.tencent.wxop.stat.e.aO
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.b(r3)
        L_0x0040:
            com.tencent.wxop.stat.a.d r0 = r9.bP
            com.tencent.wxop.stat.a.e r0 = r0.ac()
            int r0 = r0.r()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)
            java.util.Map r0 = com.tencent.wxop.stat.e.aN
            java.lang.Object r0 = r0.get(r3)
            java.lang.Integer r0 = (java.lang.Integer) r0
            if (r0 == 0) goto L_0x00c1
            java.util.Map r4 = com.tencent.wxop.stat.e.aN
            int r5 = r0.intValue()
            int r5 = r5 + 1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r4.put(r3, r5)
            int r3 = r0.intValue()
            int r4 = com.tencent.wxop.stat.c.ae
            if (r3 <= r4) goto L_0x00cc
            boolean r3 = com.tencent.wxop.stat.c.k()
            if (r3 == 0) goto L_0x00bd
            com.tencent.wxop.stat.b.b r3 = com.tencent.wxop.stat.e.aV
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "event "
            r4.<init>(r5)
            com.tencent.wxop.stat.a.d r5 = r9.bP
            java.lang.String r5 = r5.af()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = " was discard, cause of called limit, current:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r4 = ", limit:"
            java.lang.StringBuilder r0 = r0.append(r4)
            int r4 = com.tencent.wxop.stat.c.ae
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = ", period:"
            java.lang.StringBuilder r0 = r0.append(r4)
            long r4 = com.tencent.wxop.stat.c.af
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = " ms"
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            r3.d(r0)
        L_0x00bd:
            r0 = r1
        L_0x00be:
            if (r0 == 0) goto L_0x00ce
        L_0x00c0:
            return
        L_0x00c1:
            java.util.Map r0 = com.tencent.wxop.stat.e.aN
            java.lang.Integer r4 = java.lang.Integer.valueOf(r1)
            r0.put(r3, r4)
        L_0x00cc:
            r0 = r2
            goto L_0x00be
        L_0x00ce:
            int r0 = com.tencent.wxop.stat.c.ay
            if (r0 <= 0) goto L_0x0104
            long r4 = r9.bT
            long r6 = com.tencent.wxop.stat.p.bU
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 < 0) goto L_0x0104
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.e.p(r0)
            long r4 = r9.bT
            long r6 = com.tencent.wxop.stat.c.az
            long r4 = r4 + r6
            com.tencent.wxop.stat.p.bU = r4
            boolean r0 = com.tencent.wxop.stat.c.k()
            if (r0 == 0) goto L_0x0104
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.e.aV
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "nextFlushTime="
            r3.<init>(r4)
            long r4 = com.tencent.wxop.stat.p.bU
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.b(r3)
        L_0x0104:
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.g r0 = com.tencent.wxop.stat.g.r(r0)
            boolean r0 = r0.X()
            if (r0 == 0) goto L_0x02f3
            boolean r0 = com.tencent.wxop.stat.c.k()
            if (r0 == 0) goto L_0x012e
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.e.aV
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "sendFailedCount="
            r3.<init>(r4)
            int r4 = com.tencent.wxop.stat.e.aI
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.b(r3)
        L_0x012e:
            boolean r0 = com.tencent.wxop.stat.e.a()
            if (r0 != 0) goto L_0x02d3
            com.tencent.wxop.stat.a.d r0 = r9.bP
            com.tencent.wxop.stat.f r0 = r0.ae()
            if (r0 == 0) goto L_0x014c
            com.tencent.wxop.stat.a.d r0 = r9.bP
            com.tencent.wxop.stat.f r0 = r0.ae()
            boolean r0 = r0.R()
            if (r0 == 0) goto L_0x014c
            com.tencent.wxop.stat.d r0 = com.tencent.wxop.stat.d.INSTANT
            r9.bQ = r0
        L_0x014c:
            boolean r0 = com.tencent.wxop.stat.c.ah
            if (r0 == 0) goto L_0x0162
            android.content.Context r0 = com.tencent.wxop.stat.e.aY
            com.tencent.wxop.stat.g r0 = com.tencent.wxop.stat.g.r(r0)
            boolean r0 = r0.W()
            if (r0 == 0) goto L_0x0162
            com.tencent.wxop.stat.d r0 = com.tencent.wxop.stat.d.INSTANT
            r9.bQ = r0
        L_0x0162:
            boolean r0 = com.tencent.wxop.stat.c.k()
            if (r0 == 0) goto L_0x0184
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.e.aV
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "strategy="
            r3.<init>(r4)
            com.tencent.wxop.stat.d r4 = r9.bQ
            java.lang.String r4 = r4.name()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.b(r3)
        L_0x0184:
            int[] r0 = com.tencent.wxop.stat.j.bL
            com.tencent.wxop.stat.d r3 = r9.bQ
            int r3 = r3.ordinal()
            r0 = r0[r3]
            switch(r0) {
                case 1: goto L_0x01ad;
                case 2: goto L_0x01b2;
                case 3: goto L_0x027c;
                case 4: goto L_0x027c;
                case 5: goto L_0x028b;
                case 6: goto L_0x029f;
                case 7: goto L_0x02c1;
                default: goto L_0x0191;
            }
        L_0x0191:
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.e.aV
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Invalid stat strategy:"
            r1.<init>(r2)
            com.tencent.wxop.stat.d r2 = com.tencent.wxop.stat.c.j()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.error(r1)
            goto L_0x00c0
        L_0x01ad:
            r9.H()
            goto L_0x00c0
        L_0x01b2:
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.t r0 = com.tencent.wxop.stat.t.s(r0)
            com.tencent.wxop.stat.a.d r1 = r9.bP
            boolean r3 = r9.bR
            r0.b(r1, r8, r3, r2)
            boolean r0 = com.tencent.wxop.stat.c.k()
            if (r0 == 0) goto L_0x01f8
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.e.aV
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "PERIOD currTime="
            r1.<init>(r2)
            long r2 = r9.bT
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ",nextPeriodSendTs="
            java.lang.StringBuilder r1 = r1.append(r2)
            long r2 = com.tencent.wxop.stat.e.aZ
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ",difftime="
            java.lang.StringBuilder r1 = r1.append(r2)
            long r2 = com.tencent.wxop.stat.e.aZ
            long r4 = r9.bT
            long r2 = r2 - r4
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.b(r1)
        L_0x01f8:
            long r0 = com.tencent.wxop.stat.e.aZ
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x0234
            android.content.Context r0 = r9.bS
            java.lang.String r1 = "last_period_ts"
            long r0 = com.tencent.wxop.stat.b.q.f(r0, r1)
            com.tencent.wxop.stat.e.aZ = r0
            long r0 = r9.bT
            long r2 = com.tencent.wxop.stat.e.aZ
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0217
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.e.q(r0)
        L_0x0217:
            long r0 = r9.bT
            int r2 = com.tencent.wxop.stat.c.u()
            int r2 = r2 * 60
            int r2 = r2 * 1000
            long r2 = (long) r2
            long r0 = r0 + r2
            long r2 = com.tencent.wxop.stat.e.aZ
            int r2 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x022b
            com.tencent.wxop.stat.e.aZ = r0
        L_0x022b:
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.af r0 = com.tencent.wxop.stat.af.Y(r0)
            r0.ah()
        L_0x0234:
            boolean r0 = com.tencent.wxop.stat.c.k()
            if (r0 == 0) goto L_0x026d
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.e.aV
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "PERIOD currTime="
            r1.<init>(r2)
            long r2 = r9.bT
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ",nextPeriodSendTs="
            java.lang.StringBuilder r1 = r1.append(r2)
            long r2 = com.tencent.wxop.stat.e.aZ
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ",difftime="
            java.lang.StringBuilder r1 = r1.append(r2)
            long r2 = com.tencent.wxop.stat.e.aZ
            long r4 = r9.bT
            long r2 = r2 - r4
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.b(r1)
        L_0x026d:
            long r0 = r9.bT
            long r2 = com.tencent.wxop.stat.e.aZ
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c0
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.e.q(r0)
            goto L_0x00c0
        L_0x027c:
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.t r0 = com.tencent.wxop.stat.t.s(r0)
            com.tencent.wxop.stat.a.d r1 = r9.bP
            boolean r3 = r9.bR
            r0.b(r1, r8, r3, r2)
            goto L_0x00c0
        L_0x028b:
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.t r0 = com.tencent.wxop.stat.t.s(r0)
            com.tencent.wxop.stat.a.d r2 = r9.bP
            com.tencent.wxop.stat.q r3 = new com.tencent.wxop.stat.q
            r3.<init>(r9)
            boolean r4 = r9.bR
            r0.b(r2, r3, r4, r1)
            goto L_0x00c0
        L_0x029f:
            android.content.Context r0 = com.tencent.wxop.stat.e.aY
            com.tencent.wxop.stat.g r0 = com.tencent.wxop.stat.g.r(r0)
            int r0 = r0.D()
            if (r0 != r1) goto L_0x02b2
            r9.H()
            goto L_0x00c0
        L_0x02b2:
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.t r0 = com.tencent.wxop.stat.t.s(r0)
            com.tencent.wxop.stat.a.d r1 = r9.bP
            boolean r3 = r9.bR
            r0.b(r1, r8, r3, r2)
            goto L_0x00c0
        L_0x02c1:
            android.content.Context r0 = r9.bS
            boolean r0 = com.tencent.wxop.stat.b.l.y(r0)
            if (r0 == 0) goto L_0x00c0
            com.tencent.wxop.stat.r r0 = new com.tencent.wxop.stat.r
            r0.<init>(r9)
            r9.a(r0)
            goto L_0x00c0
        L_0x02d3:
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.t r0 = com.tencent.wxop.stat.t.s(r0)
            com.tencent.wxop.stat.a.d r1 = r9.bP
            boolean r3 = r9.bR
            r0.b(r1, r8, r3, r2)
            long r0 = r9.bT
            long r2 = com.tencent.wxop.stat.e.aX
            long r0 = r0 - r2
            r2 = 1800000(0x1b7740, double:8.89318E-318)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c0
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.e.n(r0)
            goto L_0x00c0
        L_0x02f3:
            android.content.Context r0 = r9.bS
            com.tencent.wxop.stat.t r0 = com.tencent.wxop.stat.t.s(r0)
            com.tencent.wxop.stat.a.d r1 = r9.bP
            boolean r3 = r9.bR
            r0.b(r1, r8, r3, r2)
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.p.ah():void");
    }
}
