package com.tencent.wxop.stat;

import android.content.Context;
import com.tencent.wxop.stat.b.l;
import java.lang.Thread;

final class an implements Runnable {
    final /* synthetic */ Context e;

    an(Context context) {
        this.e = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.wxop.stat.b.l.a(android.content.Context, boolean):int
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.wxop.stat.b.l.a(android.content.Context, int):void
      com.tencent.wxop.stat.b.l.a(android.content.Context, boolean):int */
    public final void run() {
        g.r(e.aY).aa();
        l.a(this.e, true);
        t.s(this.e);
        ak.Z(this.e);
        Thread.UncaughtExceptionHandler unused = e.aW = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new n());
        if (c.j() == d.APP_LAUNCH) {
            e.o(this.e);
        }
        if (c.k()) {
            e.aV.e("Init MTA StatService success.");
        }
    }
}
