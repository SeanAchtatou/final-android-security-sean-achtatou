package com.tencent.wxop.stat;

import android.content.Context;

final class as implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f2615a;
    final /* synthetic */ f bM;
    final /* synthetic */ Context co;

    as(String str, Context context, f fVar) {
        this.f2615a = str;
        this.co = context;
        this.bM = fVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.wxop.stat.e.a(android.content.Context, boolean, com.tencent.wxop.stat.f):int
     arg types: [android.content.Context, int, com.tencent.wxop.stat.f]
     candidates:
      com.tencent.wxop.stat.e.a(android.content.Context, java.lang.String, com.tencent.wxop.stat.f):void
      com.tencent.wxop.stat.e.a(android.content.Context, java.lang.String, java.lang.String):boolean
      com.tencent.wxop.stat.e.a(android.content.Context, boolean, com.tencent.wxop.stat.f):int */
    public final void run() {
        try {
            synchronized (e.aT) {
                if (e.aT.size() >= c.v()) {
                    e.aV.error("The number of page events exceeds the maximum value " + Integer.toString(c.v()));
                    return;
                }
                String unused = e.aR = this.f2615a;
                if (e.aT.containsKey(e.aR)) {
                    e.aV.d("Duplicate PageID : " + e.aR + ", onResume() repeated?");
                    return;
                }
                e.aT.put(e.aR, Long.valueOf(System.currentTimeMillis()));
                e.a(this.co, true, this.bM);
            }
        } catch (Throwable th) {
            e.aV.b(th);
            e.a(this.co, th);
        }
    }
}
