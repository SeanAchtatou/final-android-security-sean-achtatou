package com.tencent.map.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import java.util.ArrayList;
import java.util.List;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private Context f2447a = null;
    /* access modifiers changed from: private */
    public WifiManager b = null;
    private a c = null;
    private Handler d = null;
    private Runnable e = new Runnable() {
        public final void run() {
            g.a(g.this);
        }
    };
    /* access modifiers changed from: private */
    public int f = 1;
    /* access modifiers changed from: private */
    public c g = null;
    /* access modifiers changed from: private */
    public b h = null;
    private boolean i = false;
    private byte[] j = new byte[0];

    public class a extends BroadcastReceiver {

        /* renamed from: a  reason: collision with root package name */
        private int f2449a = 4;
        private List<ScanResult> b = null;
        private boolean c = false;

        public a() {
        }

        private void a(List<ScanResult> list) {
            if (list != null) {
                if (this.c) {
                    if (this.b == null) {
                        this.b = new ArrayList();
                    }
                    int size = this.b.size();
                    for (ScanResult next : list) {
                        int i = 0;
                        while (true) {
                            if (i >= size) {
                                break;
                            } else if (this.b.get(i).BSSID.equals(next.BSSID)) {
                                this.b.remove(i);
                                break;
                            } else {
                                i++;
                            }
                        }
                        this.b.add(next);
                    }
                    return;
                }
                if (this.b == null) {
                    this.b = new ArrayList();
                } else {
                    this.b.clear();
                }
                for (ScanResult add : list) {
                    this.b.add(add);
                }
            }
        }

        public final void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.net.wifi.WIFI_STATE_CHANGED")) {
                this.f2449a = intent.getIntExtra("wifi_state", 4);
                if (g.this.g != null) {
                    g.this.g.b(this.f2449a);
                }
            }
            if (intent.getAction().equals("android.net.wifi.SCAN_RESULTS") || intent.getAction().equals("android.net.wifi.WIFI_STATE_CHANGED")) {
                List<ScanResult> list = null;
                if (g.this.b != null) {
                    list = g.this.b.getScanResults();
                }
                if (intent.getAction().equals("android.net.wifi.WIFI_STATE_CHANGED")) {
                    if (list == null) {
                        return;
                    }
                    if (list != null && list.size() == 0) {
                        return;
                    }
                }
                if (this.c || this.b == null || this.b.size() < 4 || list == null || list.size() > 2) {
                    a(list);
                    this.c = false;
                    b unused = g.this.h = new b(g.this, this.b, System.currentTimeMillis(), this.f2449a);
                    if (g.this.g != null) {
                        g.this.g.a(g.this.h);
                    }
                    g.this.a(((long) g.this.f) * 20000);
                    return;
                }
                a(list);
                this.c = true;
                g.this.a(0);
            }
        }
    }

    public class b implements Cloneable {

        /* renamed from: a  reason: collision with root package name */
        private List<ScanResult> f2450a = null;

        public b(g gVar, List<ScanResult> list, long j, int i) {
            if (list != null) {
                this.f2450a = new ArrayList();
                for (ScanResult add : list) {
                    this.f2450a.add(add);
                }
            }
        }

        public final List<ScanResult> a() {
            return this.f2450a;
        }

        public final Object clone() {
            b bVar;
            try {
                bVar = (b) super.clone();
            } catch (Exception e) {
                bVar = null;
            }
            if (this.f2450a != null) {
                bVar.f2450a = new ArrayList();
                bVar.f2450a.addAll(this.f2450a);
            }
            return bVar;
        }
    }

    public interface c {
        void a(b bVar);

        void b(int i);
    }

    static /* synthetic */ void a(g gVar) {
        if (gVar.b != null && gVar.b.isWifiEnabled()) {
            gVar.b.startScan();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r3 = this;
            byte[] r1 = r3.j
            monitor-enter(r1)
            boolean r0 = r3.i     // Catch:{ all -> 0x0013 }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r1)     // Catch:{ all -> 0x0013 }
        L_0x0008:
            return
        L_0x0009:
            android.content.Context r0 = r3.f2447a     // Catch:{ all -> 0x0013 }
            if (r0 == 0) goto L_0x0011
            com.tencent.map.b.g$a r0 = r3.c     // Catch:{ all -> 0x0013 }
            if (r0 != 0) goto L_0x0016
        L_0x0011:
            monitor-exit(r1)     // Catch:{ all -> 0x0013 }
            goto L_0x0008
        L_0x0013:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0016:
            android.content.Context r0 = r3.f2447a     // Catch:{ Exception -> 0x0029 }
            com.tencent.map.b.g$a r2 = r3.c     // Catch:{ Exception -> 0x0029 }
            r0.unregisterReceiver(r2)     // Catch:{ Exception -> 0x0029 }
        L_0x001d:
            android.os.Handler r0 = r3.d     // Catch:{ all -> 0x0013 }
            java.lang.Runnable r2 = r3.e     // Catch:{ all -> 0x0013 }
            r0.removeCallbacks(r2)     // Catch:{ all -> 0x0013 }
            r0 = 0
            r3.i = r0     // Catch:{ all -> 0x0013 }
            monitor-exit(r1)     // Catch:{ all -> 0x0013 }
            goto L_0x0008
        L_0x0029:
            r0 = move-exception
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.map.b.g.a():void");
    }

    public final void a(long j2) {
        if (this.d != null && this.i) {
            this.d.removeCallbacks(this.e);
            this.d.postDelayed(this.e, j2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.content.Context r6, com.tencent.map.b.g.c r7, int r8) {
        /*
            r5 = this;
            r0 = 1
            r1 = 0
            byte[] r2 = r5.j
            monitor-enter(r2)
            boolean r3 = r5.i     // Catch:{ all -> 0x0068 }
            if (r3 == 0) goto L_0x000b
            monitor-exit(r2)     // Catch:{ all -> 0x0068 }
        L_0x000a:
            return r0
        L_0x000b:
            if (r6 == 0) goto L_0x000f
            if (r7 != 0) goto L_0x0012
        L_0x000f:
            monitor-exit(r2)
            r0 = r1
            goto L_0x000a
        L_0x0012:
            android.os.Handler r0 = new android.os.Handler     // Catch:{ all -> 0x0068 }
            android.os.Looper r3 = android.os.Looper.getMainLooper()     // Catch:{ all -> 0x0068 }
            r0.<init>(r3)     // Catch:{ all -> 0x0068 }
            r5.d = r0     // Catch:{ all -> 0x0068 }
            r5.f2447a = r6     // Catch:{ all -> 0x0068 }
            r5.g = r7     // Catch:{ all -> 0x0068 }
            r0 = 1
            r5.f = r0     // Catch:{ all -> 0x0068 }
            android.content.Context r0 = r5.f2447a     // Catch:{ Exception -> 0x0064 }
            java.lang.String r3 = "wifi"
            java.lang.Object r0 = r0.getSystemService(r3)     // Catch:{ Exception -> 0x0064 }
            android.net.wifi.WifiManager r0 = (android.net.wifi.WifiManager) r0     // Catch:{ Exception -> 0x0064 }
            r5.b = r0     // Catch:{ Exception -> 0x0064 }
            android.content.IntentFilter r0 = new android.content.IntentFilter     // Catch:{ Exception -> 0x0064 }
            r0.<init>()     // Catch:{ Exception -> 0x0064 }
            com.tencent.map.b.g$a r3 = new com.tencent.map.b.g$a     // Catch:{ Exception -> 0x0064 }
            r3.<init>()     // Catch:{ Exception -> 0x0064 }
            r5.c = r3     // Catch:{ Exception -> 0x0064 }
            android.net.wifi.WifiManager r3 = r5.b     // Catch:{ Exception -> 0x0064 }
            if (r3 == 0) goto L_0x0044
            com.tencent.map.b.g$a r3 = r5.c     // Catch:{ Exception -> 0x0064 }
            if (r3 != 0) goto L_0x0047
        L_0x0044:
            monitor-exit(r2)     // Catch:{ all -> 0x0068 }
            r0 = r1
            goto L_0x000a
        L_0x0047:
            java.lang.String r3 = "android.net.wifi.WIFI_STATE_CHANGED"
            r0.addAction(r3)     // Catch:{ Exception -> 0x0064 }
            java.lang.String r3 = "android.net.wifi.SCAN_RESULTS"
            r0.addAction(r3)     // Catch:{ Exception -> 0x0064 }
            android.content.Context r3 = r5.f2447a     // Catch:{ Exception -> 0x0064 }
            com.tencent.map.b.g$a r4 = r5.c     // Catch:{ Exception -> 0x0064 }
            r3.registerReceiver(r4, r0)     // Catch:{ Exception -> 0x0064 }
            r0 = 0
            r5.a(r0)     // Catch:{ all -> 0x0068 }
            r0 = 1
            r5.i = r0     // Catch:{ all -> 0x0068 }
            monitor-exit(r2)     // Catch:{ all -> 0x0068 }
            boolean r0 = r5.i
            goto L_0x000a
        L_0x0064:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0068 }
            r0 = r1
            goto L_0x000a
        L_0x0068:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.map.b.g.a(android.content.Context, com.tencent.map.b.g$c, int):boolean");
    }

    public final boolean b() {
        return this.i;
    }

    public final boolean c() {
        if (this.f2447a == null || this.b == null) {
            return false;
        }
        return this.b.isWifiEnabled();
    }
}
