package com.tencent.weibo.sdk.android.component.sso.tools;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class Cryptor {
    public static final int QUOTIENT = 79764919;
    public static final int SALT_LEN = 2;
    public static final int ZERO_LEN = 7;
    private int contextStart;
    private int crypt;
    private boolean header = true;
    private byte[] key;
    private byte[] out;
    private int padding;
    private byte[] plain;
    private int pos;
    private int preCrypt;
    private byte[] prePlain;
    private Random random = new Random();

    public static byte[] MD5Hash(byte[] bArr, int i) {
        return new byte[2];
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int CRC32Hash(byte[] r9) {
        /*
            r3 = 0
            int r5 = r9.length
            r1 = -1
            r4 = r3
        L_0x0004:
            if (r4 < r5) goto L_0x0009
            r0 = r1 ^ -1
            return r0
        L_0x0009:
            byte r0 = r9[r4]
            r2 = r3
            r8 = r0
            r0 = r1
            r1 = r8
        L_0x000f:
            r6 = 8
            if (r2 < r6) goto L_0x0018
            int r1 = r4 + 1
            r4 = r1
            r1 = r0
            goto L_0x0004
        L_0x0018:
            r6 = r1 ^ r0
            int r6 = r6 >>> 31
            r7 = 1
            if (r6 != r7) goto L_0x002b
            int r0 = r0 << 1
            r6 = 79764919(0x4c11db7, float:4.540137E-36)
            r0 = r0 ^ r6
        L_0x0025:
            int r1 = r1 << 1
            byte r1 = (byte) r1
            int r2 = r2 + 1
            goto L_0x000f
        L_0x002b:
            int r0 = r0 << 1
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.weibo.sdk.android.component.sso.tools.Cryptor.CRC32Hash(byte[]):int");
    }

    public static byte[] _4bytesEncryptAFrame(int i, byte[] bArr) {
        short[] sArr = {(short) (65535 & i), (short) (i >>> 16)};
        short[] sArr2 = {(short) ((bArr[1] << 8) | bArr[0]), (short) ((bArr[3] << 8) | bArr[2]), (short) ((bArr[5] << 8) | bArr[4]), (short) ((bArr[7] << 8) | bArr[6])};
        short s = sArr[0];
        short s2 = sArr[1];
        short s3 = 32;
        short s4 = s2;
        short s5 = s;
        short s6 = 0;
        while (true) {
            short s7 = (short) (s3 - 1);
            if (s3 <= 0) {
                return new byte[]{(byte) (s4 >> 8), (byte) (s4 & 255), (byte) (s5 >> 8), (byte) (s5 & 255)};
            }
            short s8 = (short) (s6 + 12895);
            s5 = (short) (((((s4 << 4) + sArr2[0]) ^ (s4 + s8)) ^ ((s4 >> 5) + sArr2[1])) + s5);
            s4 = (short) (((((s5 << 4) + sArr2[2]) ^ (s5 + s8)) ^ ((s5 >> 5) + sArr2[3])) + s4);
            s6 = s8;
            s3 = s7;
        }
    }

    public static int _4bytesDecryptAFrame(long j, byte[] bArr) {
        short[] sArr = {(short) ((int) (65535 & j)), (short) ((int) (j >> 16))};
        short[] sArr2 = {(short) ((bArr[1] << 8) | bArr[0]), (short) ((bArr[3] << 8) | bArr[2]), (short) ((bArr[5] << 8) | bArr[4]), (short) ((bArr[7] << 8) | bArr[6])};
        short s = 32;
        short s2 = sArr[0];
        short s3 = sArr[1];
        short s4 = (short) 412640;
        while (true) {
            short s5 = (short) (s - 1);
            if (s <= 0) {
                sArr[0] = s2;
                sArr[1] = s3;
                return (sArr[1] << 16) | (sArr[0] & 65535);
            }
            s3 = (short) (s3 - ((((s2 << 4) + sArr2[2]) ^ (s2 + s4)) ^ ((s2 >> 5) + sArr2[3])));
            s2 = (short) (s2 - ((((s3 << 4) + sArr2[0]) ^ (s3 + s4)) ^ ((s3 >> 5) + sArr2[1])));
            s4 = (short) (s4 - 12895);
            s = s5;
        }
    }

    public static long getUnsignedInt(byte[] bArr, int i, int i2) {
        int i3;
        long j = 0;
        if (i2 > 8) {
            i3 = i + 8;
        } else {
            i3 = i + i2;
        }
        while (i < i3) {
            j = (j << 8) | ((long) (bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD));
            i++;
        }
        return (4294967295L & j) | (j >>> 32);
    }

    public byte[] decrypt(byte[] bArr, int i, int i2, byte[] bArr2) {
        this.preCrypt = 0;
        this.crypt = 0;
        this.key = bArr2;
        byte[] bArr3 = new byte[(i + 8)];
        if (i2 % 8 != 0 || i2 < 16) {
            return null;
        }
        this.prePlain = decipher(bArr, i);
        this.pos = this.prePlain[0] & 7;
        int i3 = (i2 - this.pos) - 10;
        if (i3 < 0) {
            return null;
        }
        for (int i4 = i; i4 < bArr3.length; i4++) {
            bArr3[i4] = 0;
        }
        this.out = new byte[i3];
        this.preCrypt = 0;
        this.crypt = 8;
        this.contextStart = 8;
        this.pos++;
        this.padding = 1;
        byte[] bArr4 = bArr3;
        while (this.padding <= 2) {
            if (this.pos < 8) {
                this.pos++;
                this.padding++;
            }
            if (this.pos == 8) {
                if (!decrypt8Bytes(bArr, i, i2)) {
                    return null;
                }
                bArr4 = bArr;
            }
        }
        int i5 = i3;
        byte[] bArr5 = bArr4;
        int i6 = 0;
        byte[] bArr6 = bArr5;
        while (i5 != 0) {
            if (this.pos < 8) {
                this.out[i6] = (byte) (bArr6[(this.preCrypt + i) + this.pos] ^ this.prePlain[this.pos]);
                i6++;
                i5--;
                this.pos++;
            }
            if (this.pos == 8) {
                this.preCrypt = this.crypt - 8;
                if (!decrypt8Bytes(bArr, i, i2)) {
                    return null;
                }
                bArr6 = bArr;
            }
        }
        this.padding = 1;
        byte[] bArr7 = bArr6;
        while (this.padding < 8) {
            if (this.pos < 8) {
                if ((bArr7[(this.preCrypt + i) + this.pos] ^ this.prePlain[this.pos]) != 0) {
                    return null;
                }
                this.pos++;
            }
            if (this.pos == 8) {
                this.preCrypt = this.crypt;
                if (!decrypt8Bytes(bArr, i, i2)) {
                    return null;
                }
                bArr7 = bArr;
            }
            this.padding++;
        }
        return this.out;
    }

    public byte[] decrypt(byte[] bArr, byte[] bArr2) {
        return decrypt(bArr, 0, bArr.length, bArr2);
    }

    public byte[] encrypt(byte[] bArr, int i, int i2, byte[] bArr2) {
        int i3;
        int i4;
        this.plain = new byte[8];
        this.prePlain = new byte[8];
        this.pos = 1;
        this.padding = 0;
        this.preCrypt = 0;
        this.crypt = 0;
        this.key = bArr2;
        this.header = true;
        this.pos = (i2 + 10) % 8;
        if (this.pos != 0) {
            this.pos = 8 - this.pos;
        }
        this.out = new byte[(this.pos + i2 + 10)];
        this.plain[0] = (byte) ((rand() & 248) | this.pos);
        for (int i5 = 1; i5 <= this.pos; i5++) {
            this.plain[i5] = (byte) (rand() & 255);
        }
        this.pos++;
        for (int i6 = 0; i6 < 8; i6++) {
            this.prePlain[i6] = 0;
        }
        this.padding = 1;
        while (this.padding <= 2) {
            if (this.pos < 8) {
                byte[] bArr3 = this.plain;
                int i7 = this.pos;
                this.pos = i7 + 1;
                bArr3[i7] = (byte) (rand() & 255);
                this.padding++;
            }
            if (this.pos == 8) {
                encrypt8Bytes();
            }
        }
        int i8 = i;
        int i9 = i2;
        while (i9 > 0) {
            if (this.pos < 8) {
                byte[] bArr4 = this.plain;
                int i10 = this.pos;
                this.pos = i10 + 1;
                i3 = i8 + 1;
                bArr4[i10] = bArr[i8];
                i4 = i9 - 1;
            } else {
                i3 = i8;
                i4 = i9;
            }
            if (this.pos == 8) {
                encrypt8Bytes();
            }
            i9 = i4;
            i8 = i3;
        }
        this.padding = 1;
        while (this.padding <= 7) {
            if (this.pos < 8) {
                byte[] bArr5 = this.plain;
                int i11 = this.pos;
                this.pos = i11 + 1;
                bArr5[i11] = 0;
                this.padding++;
            }
            if (this.pos == 8) {
                encrypt8Bytes();
            }
        }
        return this.out;
    }

    public byte[] encrypt(byte[] bArr, byte[] bArr2) {
        return encrypt(bArr, 0, bArr.length, bArr2);
    }

    private byte[] encipher(byte[] bArr) {
        int i = 16;
        try {
            long unsignedInt = getUnsignedInt(bArr, 0, 4);
            long unsignedInt2 = getUnsignedInt(bArr, 4, 4);
            long unsignedInt3 = getUnsignedInt(this.key, 0, 4);
            long unsignedInt4 = getUnsignedInt(this.key, 4, 4);
            long unsignedInt5 = getUnsignedInt(this.key, 8, 4);
            long unsignedInt6 = getUnsignedInt(this.key, 12, 4);
            long j = 0;
            long j2 = -1640531527 & 4294967295L;
            while (true) {
                int i2 = i - 1;
                if (i <= 0) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8);
                    DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
                    dataOutputStream.writeInt((int) unsignedInt);
                    dataOutputStream.writeInt((int) unsignedInt2);
                    dataOutputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
                j = (j + j2) & 4294967295L;
                unsignedInt = (unsignedInt + ((((unsignedInt2 << 4) + unsignedInt3) ^ (unsignedInt2 + j)) ^ ((unsignedInt2 >>> 5) + unsignedInt4))) & 4294967295L;
                unsignedInt2 = (unsignedInt2 + ((((unsignedInt << 4) + unsignedInt5) ^ (unsignedInt + j)) ^ ((unsignedInt >>> 5) + unsignedInt6))) & 4294967295L;
                i = i2;
            }
        } catch (IOException e) {
            return null;
        }
    }

    private byte[] decipher(byte[] bArr, int i) {
        int i2 = 16;
        try {
            long unsignedInt = getUnsignedInt(bArr, i, 4);
            long unsignedInt2 = getUnsignedInt(bArr, i + 4, 4);
            long unsignedInt3 = getUnsignedInt(this.key, 0, 4);
            long unsignedInt4 = getUnsignedInt(this.key, 4, 4);
            long unsignedInt5 = getUnsignedInt(this.key, 8, 4);
            long unsignedInt6 = getUnsignedInt(this.key, 12, 4);
            long j = -478700656 & 4294967295L;
            long j2 = -1640531527 & 4294967295L;
            while (true) {
                int i3 = i2 - 1;
                if (i2 <= 0) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8);
                    DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
                    dataOutputStream.writeInt((int) unsignedInt);
                    dataOutputStream.writeInt((int) unsignedInt2);
                    dataOutputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
                unsignedInt2 = (unsignedInt2 - ((((unsignedInt << 4) + unsignedInt5) ^ (unsignedInt + j)) ^ ((unsignedInt >>> 5) + unsignedInt6))) & 4294967295L;
                unsignedInt = (unsignedInt - ((((unsignedInt2 << 4) + unsignedInt3) ^ (unsignedInt2 + j)) ^ ((unsignedInt2 >>> 5) + unsignedInt4))) & 4294967295L;
                j = (j - j2) & 4294967295L;
                i2 = i3;
            }
        } catch (IOException e) {
            return null;
        }
    }

    private byte[] decipher(byte[] bArr) {
        return decipher(bArr, 0);
    }

    private void encrypt8Bytes() {
        this.pos = 0;
        while (this.pos < 8) {
            if (this.header) {
                byte[] bArr = this.plain;
                int i = this.pos;
                bArr[i] = (byte) (bArr[i] ^ this.prePlain[this.pos]);
            } else {
                byte[] bArr2 = this.plain;
                int i2 = this.pos;
                bArr2[i2] = (byte) (bArr2[i2] ^ this.out[this.preCrypt + this.pos]);
            }
            this.pos++;
        }
        System.arraycopy(encipher(this.plain), 0, this.out, this.crypt, 8);
        this.pos = 0;
        while (this.pos < 8) {
            byte[] bArr3 = this.out;
            int i3 = this.crypt + this.pos;
            bArr3[i3] = (byte) (bArr3[i3] ^ this.prePlain[this.pos]);
            this.pos++;
        }
        System.arraycopy(this.plain, 0, this.prePlain, 0, 8);
        this.preCrypt = this.crypt;
        this.crypt += 8;
        this.pos = 0;
        this.header = false;
    }

    private boolean decrypt8Bytes(byte[] bArr, int i, int i2) {
        this.pos = 0;
        while (this.pos < 8) {
            if (this.contextStart + this.pos >= i2) {
                return true;
            }
            byte[] bArr2 = this.prePlain;
            int i3 = this.pos;
            bArr2[i3] = (byte) (bArr2[i3] ^ bArr[(this.crypt + i) + this.pos]);
            this.pos++;
        }
        this.prePlain = decipher(this.prePlain);
        if (this.prePlain == null) {
            return false;
        }
        this.contextStart += 8;
        this.crypt += 8;
        this.pos = 0;
        return true;
    }

    private int rand() {
        return this.random.nextInt();
    }

    public byte[] decrypt(byte[] bArr, byte[] bArr2, int i) {
        byte[] decrypt = decrypt(bArr, 0, bArr.length, bArr2);
        return decrypt == null ? getRandomByte(i) : decrypt;
    }

    private byte[] getRandomByte(int i) {
        byte[] bArr = new byte[i];
        this.random.nextBytes(bArr);
        return bArr;
    }
}
