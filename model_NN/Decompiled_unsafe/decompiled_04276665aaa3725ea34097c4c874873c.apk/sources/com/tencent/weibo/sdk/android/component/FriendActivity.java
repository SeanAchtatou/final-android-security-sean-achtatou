package com.tencent.weibo.sdk.android.component;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.open.wpa.WPA;
import com.tencent.weibo.sdk.android.api.PublishWeiBoAPI;
import com.tencent.weibo.sdk.android.api.adapter.FriendAdapter;
import com.tencent.weibo.sdk.android.api.util.BackGroudSeletor;
import com.tencent.weibo.sdk.android.api.util.FirendCompare;
import com.tencent.weibo.sdk.android.api.util.HypyUtil;
import com.tencent.weibo.sdk.android.api.util.Util;
import com.tencent.weibo.sdk.android.component.LetterListView;
import com.tencent.weibo.sdk.android.model.AccountModel;
import com.tencent.weibo.sdk.android.model.Firend;
import com.tencent.weibo.sdk.android.model.ModelResult;
import com.tencent.weibo.sdk.android.network.HttpCallback;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FriendActivity extends Activity implements LetterListView.OnTouchingLetterChangedListener, HttpCallback {
    private FriendAdapter adapter;
    private Map<String, List<Firend>> child = new HashMap();
    private ProgressDialog dialog;
    private EditText editText;
    private List<String> group = new ArrayList();
    private List<String> groups = new ArrayList();
    private LetterListView letterListView;
    private ExpandableListView listView;
    private Map<String, List<Firend>> newchild = new HashMap();
    private List<String> newgourp = new ArrayList();
    /* access modifiers changed from: private */
    public int[] nums;
    /* access modifiers changed from: private */
    public TextView textView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getdate();
        setContentView((LinearLayout) initview());
    }

    private View initview() {
        LinearLayout linearLayout = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        relativeLayout.setGravity(0);
        relativeLayout.setBackgroundDrawable(BackGroudSeletor.getdrawble("up_bg2x", this));
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(15);
        layoutParams2.addRule(9, -1);
        layoutParams2.addRule(15, -1);
        layoutParams2.topMargin = 10;
        layoutParams2.leftMargin = 10;
        layoutParams2.bottomMargin = 10;
        Button button = new Button(this);
        button.setBackgroundDrawable(BackGroudSeletor.createBgByImageIds(new String[]{"return_btn2x", "return_btn_hover"}, this));
        button.setText("  返回");
        button.setLayoutParams(layoutParams2);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                FriendActivity.this.finish();
            }
        });
        TextView textView2 = new TextView(this);
        textView2.setText("好友列表");
        textView2.setTextColor(-1);
        textView2.setTextSize(24.0f);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(13, -1);
        textView2.setLayoutParams(layoutParams3);
        relativeLayout.addView(textView2);
        relativeLayout.addView(button);
        linearLayout.addView(relativeLayout);
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout2.setOrientation(0);
        linearLayout2.setGravity(17);
        this.editText = new EditText(this);
        this.editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        this.editText.setPadding(20, 0, 10, 0);
        this.editText.setBackgroundDrawable(BackGroudSeletor.getdrawble("searchbg_", this));
        this.editText.setCompoundDrawablesWithIntrinsicBounds(BackGroudSeletor.getdrawble("search_", this), (Drawable) null, (Drawable) null, (Drawable) null);
        this.editText.setHint("搜索");
        this.editText.setTextSize(18.0f);
        this.editText.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void afterTextChanged(Editable editable) {
                FriendActivity.this.search(editable.toString());
            }
        });
        linearLayout2.addView(this.editText);
        linearLayout.addView(linearLayout2);
        this.listView = new ExpandableListView(this);
        new FrameLayout.LayoutParams(-1, -1);
        this.listView.setLayoutParams(layoutParams);
        this.listView.setGroupIndicator(null);
        LinearLayout linearLayout3 = new LinearLayout(this);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.textView = new TextView(this);
        linearLayout3.setPadding(30, 0, 0, 0);
        linearLayout3.setGravity(16);
        this.textView.setTextSize(18.0f);
        this.textView.setTextColor(-1);
        this.textView.setText("常用联系人");
        linearLayout3.addView(this.textView);
        linearLayout3.setBackgroundColor(Color.parseColor("#b0bac3"));
        this.letterListView = new LetterListView(this, this.group);
        this.letterListView.setOnTouchingLetterChangedListener(this);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(50, -1);
        RelativeLayout relativeLayout2 = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams4.addRule(11);
        this.letterListView.setLayoutParams(layoutParams4);
        relativeLayout2.setLayoutParams(layoutParams5);
        relativeLayout2.addView(this.listView);
        relativeLayout2.addView(linearLayout3);
        relativeLayout2.addView(this.letterListView);
        linearLayout.addView(relativeLayout2);
        return linearLayout;
    }

    public void search(String str) {
        this.newgourp.clear();
        this.newchild.clear();
        for (int i = 0; i < this.group.size(); i++) {
            for (int i2 = 0; i2 < this.child.get(this.group.get(i)).size(); i2++) {
                if (((Firend) this.child.get(this.group.get(i)).get(i2)).getNick().contains(str)) {
                    if (this.newchild.get(this.group.get(i)) == null) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add((Firend) this.child.get(this.group.get(i)).get(i2));
                        this.newchild.put(this.group.get(i), arrayList);
                        this.newgourp.add(this.group.get(i));
                    } else {
                        this.newchild.get(this.group.get(i)).add((Firend) this.child.get(this.group.get(i)).get(i2));
                    }
                }
            }
        }
        Log.d("size", String.valueOf(this.newgourp.size()) + "---" + this.newchild.size());
        this.nums = new int[this.newgourp.size()];
        for (int i3 = 0; i3 < this.nums.length; i3++) {
            this.nums[i3] = totle(i3);
        }
        this.letterListView.setB(this.newgourp);
        this.adapter.setChild(this.newchild);
        this.adapter.setGroup(this.newgourp);
        this.adapter.notifyDataSetChanged();
        ex(this.newgourp, this.newchild);
    }

    private void getdate() {
        if (this.dialog == null) {
            this.dialog = new ProgressDialog(this);
            this.dialog.setMessage("请稍后...");
            this.dialog.show();
        }
        new PublishWeiBoAPI(new AccountModel(Util.getSharePersistent(getApplicationContext(), "ACCESS_TOKEN"))).mutual_list(this, this, null, 0, 0, 0, 10);
    }

    private void ex(final List<String> list, final Map<String, List<Firend>> map) {
        for (int i = 0; i < list.size(); i++) {
            this.listView.expandGroup(i);
        }
        this.listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            public void onGroupExpand(int i) {
            }
        });
        this.listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long j) {
                return true;
            }
        });
        this.listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
                Intent intent = new Intent();
                intent.setClass(FriendActivity.this, PublishActivity.class);
                intent.putExtra("firend", ((Firend) ((List) map.get(list.get(i))).get(i2)).getNick());
                FriendActivity.this.setResult(-1, intent);
                FriendActivity.this.finish();
                return true;
            }
        });
        this.listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            public void onScroll(AbsListView absListView, int i, int i2, int i3) {
                Log.d("first", new StringBuilder(String.valueOf(i)).toString());
                int i4 = 0;
                while (i4 < list.size()) {
                    if (i4 == 0 && i >= 0 && i < FriendActivity.this.nums[i4]) {
                        FriendActivity.this.textView.setText(((String) list.get(i4)).toUpperCase());
                        return;
                    } else if (i >= FriendActivity.this.nums[i4] || i < FriendActivity.this.nums[i4 - 1]) {
                        i4++;
                    } else {
                        FriendActivity.this.textView.setText(((String) list.get(i4)).toUpperCase());
                        return;
                    }
                }
            }
        });
    }

    public void onTouchingLetterChanged(int i) {
        this.listView.setSelectedGroup(i);
    }

    public void onResult(Object obj) {
        if (this.dialog != null && this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
        if (obj != null && ((ModelResult) obj).isSuccess()) {
            getJsonData((JSONObject) ((ModelResult) obj).getObj());
            this.nums = new int[this.group.size()];
            for (int i = 0; i < this.nums.length; i++) {
                this.nums[i] = totle(i);
            }
            this.letterListView.setB(this.group);
            this.adapter = new FriendAdapter(this, this.group, this.child);
            this.listView.setAdapter(this.adapter);
            ex(this.group, this.child);
            Log.d("发送成功", obj.toString());
        }
    }

    private void getJsonData(JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = jSONObject.getJSONObject("data").getJSONArray("info");
            for (int i = 0; i < jSONArray.length(); i++) {
                Firend firend = new Firend();
                firend.setNick(((JSONObject) jSONArray.get(i)).getString(WBPageConstants.ParamKey.NICK));
                firend.setName(((JSONObject) jSONArray.get(i)).getString(SelectCountryActivity.EXTRA_COUNTRY_NAME));
                firend.setHeadurl(String.valueOf(((JSONObject) jSONArray.get(i)).getString("headurl").replaceAll("\\/", "/")) + "/180");
                arrayList.add(firend);
            }
            Collections.sort(arrayList, new FirendCompare());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            if (this.child.get(HypyUtil.cn2py(((Firend) arrayList.get(i2)).getNick()).substring(0, 1).toUpperCase()) != null) {
                this.child.get(HypyUtil.cn2py(((Firend) arrayList.get(i2)).getNick()).substring(0, 1).toUpperCase()).add((Firend) arrayList.get(i2));
            } else {
                Log.d(WPA.CHAT_TYPE_GROUP, HypyUtil.cn2py(((Firend) arrayList.get(i2)).getNick()).substring(0, 1));
                this.group.add(HypyUtil.cn2py(((Firend) arrayList.get(i2)).getNick()).substring(0, 1).toUpperCase());
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add((Firend) arrayList.get(i2));
                this.child.put(HypyUtil.cn2py(((Firend) arrayList.get(i2)).getNick()).substring(0, 1).toUpperCase(), arrayList2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.dialog != null && this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
    }

    private int totle(int i) {
        if (i == 0) {
            return this.child.get(this.group.get(i)).size() + 1;
        }
        return this.child.get(this.group.get(i)).size() + 1 + totle(i - 1);
    }
}
