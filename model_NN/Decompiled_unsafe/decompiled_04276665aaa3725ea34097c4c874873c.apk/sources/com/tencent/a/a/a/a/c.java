package com.tencent.a.a.a.a;

import android.util.Log;
import com.tencent.stat.DeviceInfo;
import org.json.JSONException;
import org.json.JSONObject;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    String f2371a = null;
    String b = null;
    String c = "0";
    long d = 0;

    static c c(String str) {
        c cVar = new c();
        if (h.d(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.isNull(DeviceInfo.TAG_IMEI)) {
                    cVar.f2371a = jSONObject.getString(DeviceInfo.TAG_IMEI);
                }
                if (!jSONObject.isNull(DeviceInfo.TAG_MAC)) {
                    cVar.b = jSONObject.getString(DeviceInfo.TAG_MAC);
                }
                if (!jSONObject.isNull(DeviceInfo.TAG_MID)) {
                    cVar.c = jSONObject.getString(DeviceInfo.TAG_MID);
                }
                if (!jSONObject.isNull("ts")) {
                    cVar.d = jSONObject.getLong("ts");
                }
            } catch (JSONException e) {
                Log.w("MID", e);
            }
        }
        return cVar;
    }

    private JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            h.a(jSONObject, DeviceInfo.TAG_IMEI, this.f2371a);
            h.a(jSONObject, DeviceInfo.TAG_MAC, this.b);
            h.a(jSONObject, DeviceInfo.TAG_MID, this.c);
            jSONObject.put("ts", this.d);
        } catch (JSONException e) {
            Log.w("MID", e);
        }
        return jSONObject;
    }

    public final String c() {
        return this.c;
    }

    public final String toString() {
        return d().toString();
    }
}
