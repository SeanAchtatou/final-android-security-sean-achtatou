package com.tencent.stat.a;

import android.content.Context;
import com.meizu.cloud.pushsdk.notification.model.AdvanceSetting;
import com.tencent.stat.common.k;
import org.json.JSONObject;

public class i extends e {

    /* renamed from: a  reason: collision with root package name */
    private static String f2575a = null;
    private String l = null;
    private String m = null;

    public i(Context context, int i) {
        super(context, i);
        this.l = k.p(context);
        if (f2575a == null) {
            f2575a = k.m(context);
        }
    }

    public f a() {
        return f.NETWORK_MONITOR;
    }

    public void a(String str) {
        this.m = str;
    }

    public boolean a(JSONObject jSONObject) {
        k.a(jSONObject, "op", f2575a);
        k.a(jSONObject, AdvanceSetting.CLEAR_NOTIFICATION, this.l);
        jSONObject.put("sp", this.m);
        return true;
    }
}
