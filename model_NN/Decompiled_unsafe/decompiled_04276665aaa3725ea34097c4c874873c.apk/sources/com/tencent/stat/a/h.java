package com.tencent.stat.a;

import android.content.Context;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.meizu.cloud.pushsdk.notification.model.AdvanceSetting;
import com.tencent.stat.StatAppMonitor;
import com.tencent.stat.common.k;
import org.json.JSONObject;

public class h extends e {
    private static String l = null;
    private static String m = null;

    /* renamed from: a  reason: collision with root package name */
    private StatAppMonitor f2574a = null;

    public h(Context context, int i, StatAppMonitor statAppMonitor) {
        super(context, i);
        this.f2574a = statAppMonitor.clone();
    }

    public f a() {
        return f.MONITOR_STAT;
    }

    public boolean a(JSONObject jSONObject) {
        if (this.f2574a == null) {
            return false;
        }
        jSONObject.put("na", this.f2574a.getInterfaceName());
        jSONObject.put("rq", this.f2574a.getReqSize());
        jSONObject.put("rp", this.f2574a.getRespSize());
        jSONObject.put("rt", this.f2574a.getResultType());
        jSONObject.put(IXAdRequestInfo.MAX_TITLE_LENGTH, this.f2574a.getMillisecondsConsume());
        jSONObject.put("rc", this.f2574a.getReturnCode());
        jSONObject.put("sp", this.f2574a.getSampling());
        if (m == null) {
            m = k.r(this.k);
        }
        k.a(jSONObject, "av", m);
        if (l == null) {
            l = k.m(this.k);
        }
        k.a(jSONObject, "op", l);
        jSONObject.put(AdvanceSetting.CLEAR_NOTIFICATION, k.p(this.k));
        return true;
    }
}
