package com.tencent.stat.common;

import android.content.Context;

public class f {

    /* renamed from: a  reason: collision with root package name */
    static long f2584a = -1;

    static long a(Context context, String str) {
        return p.a(context, str, f2584a);
    }

    static void a(Context context, String str, long j) {
        p.b(context, str, j);
    }

    public static synchronized boolean a(Context context) {
        boolean z;
        synchronized (f.class) {
            long a2 = a(context, "1.6.2_begin_protection");
            long a3 = a(context, "1.6.2_end__protection");
            if (a2 <= 0 || a3 != f2584a) {
                if (a2 == f2584a) {
                    a(context, "1.6.2_begin_protection", System.currentTimeMillis());
                }
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static synchronized void b(Context context) {
        synchronized (f.class) {
            if (a(context, "1.6.2_end__protection") == f2584a) {
                a(context, "1.6.2_end__protection", System.currentTimeMillis());
            }
        }
    }
}
