package com.huawei.hms.api.internal;

import android.content.Context;
import com.huawei.hms.a.a;
import com.huawei.hms.a.d;
import com.huawei.hms.api.HuaweiApiAvailability;

public abstract class g {
    public static int a(Context context) {
        a.a(context, "context must not be null.");
        d dVar = new d(context);
        d.a a2 = dVar.a(HuaweiApiAvailability.SERVICES_PACKAGE);
        if (d.a.NOT_INSTALLED.equals(a2)) {
            return 1;
        }
        if (d.a.DISABLED.equals(a2)) {
            return 3;
        }
        if (!HuaweiApiAvailability.SERVICES_SIGNATURE.equalsIgnoreCase(dVar.d(HuaweiApiAvailability.SERVICES_PACKAGE))) {
            return 9;
        }
        return dVar.b(HuaweiApiAvailability.SERVICES_PACKAGE) < 20400300 ? 2 : 0;
    }
}
