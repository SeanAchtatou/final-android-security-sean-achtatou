package com.huawei.hms.api.internal;

import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import com.huawei.hms.core.aidl.IMessageEntity;
import com.huawei.hms.core.aidl.ResponseHeader;
import com.huawei.hms.core.aidl.a;
import com.huawei.hms.core.aidl.c;
import com.huawei.hms.core.aidl.e;
import com.huawei.hms.support.api.transport.DatagramTransport;

public class h extends c.a {

    /* renamed from: a  reason: collision with root package name */
    private final Class<? extends IMessageEntity> f741a;
    private final DatagramTransport.a b;

    public h(Class<? extends IMessageEntity> cls, DatagramTransport.a aVar) {
        this.f741a = cls;
        this.b = aVar;
    }

    /* access modifiers changed from: protected */
    public IMessageEntity a() {
        if (this.f741a != null) {
            try {
                return (IMessageEntity) this.f741a.newInstance();
            } catch (IllegalAccessException e) {
                Log.e("IPCCallback", "instancing exception.", e);
            } catch (InstantiationException e2) {
                Log.e("IPCCallback", "instancing exception.", e2);
            }
        }
        return null;
    }

    public void a(a aVar) {
        if (TextUtils.isEmpty(aVar.f743a)) {
            Log.e("IPCCallback", "URI cannot be null.");
            throw new RemoteException();
        }
        e eVar = new e();
        ResponseHeader responseHeader = new ResponseHeader();
        eVar.a(aVar.b, responseHeader);
        IMessageEntity iMessageEntity = null;
        if (aVar.b() > 0 && (iMessageEntity = a()) != null) {
            eVar.a(aVar.a(), iMessageEntity);
        }
        this.b.a(responseHeader.getStatusCode(), iMessageEntity);
    }
}
