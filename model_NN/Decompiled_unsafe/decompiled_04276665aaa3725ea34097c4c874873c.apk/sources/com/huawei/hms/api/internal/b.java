package com.huawei.hms.api.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.IBinder;
import com.huawei.hms.a.a.a;
import com.huawei.hms.api.Api;
import com.huawei.hms.api.ConnectionResult;
import com.huawei.hms.api.HuaweiApiAvailability;
import com.huawei.hms.api.HuaweiApiClient;
import com.huawei.hms.core.aidl.d;
import com.huawei.hms.support.api.ResolveResult;
import com.huawei.hms.support.api.entity.auth.PermissionInfo;
import com.huawei.hms.support.api.entity.auth.Scope;
import com.huawei.hms.support.api.entity.core.ConnectInfo;
import com.huawei.hms.support.api.entity.core.ConnectResp;
import com.huawei.hms.support.api.entity.core.DisconnectInfo;
import com.huawei.hms.support.api.entity.core.DisconnectResp;
import com.huawei.hms.support.log.c;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class b extends HuaweiApiClient implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private final Context f736a;
    private final String b;
    private volatile d c;
    private AtomicInteger d = new AtomicInteger(1);
    private List<Scope> e;
    private List<PermissionInfo> f;
    private Map<Api<?>, Api.ApiOptions> g;
    private HuaweiApiClient.ConnectionCallbacks h;
    private HuaweiApiClient.OnConnectionFailedListener i;

    public b(Context context) {
        this.f736a = context;
        this.b = a(this.f736a);
    }

    private static String a(Context context) {
        Object obj;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null && applicationInfo.metaData != null && (obj = applicationInfo.metaData.get("com.huawei.hms.client.appid")) != null) {
                return String.valueOf(obj);
            }
            a.d("HuaweiApiClientImpl", "Failed to read meta data for the AppID");
            return "";
        } catch (PackageManager.NameNotFoundException e2) {
            return "";
        }
    }

    /* access modifiers changed from: private */
    public void a(ResolveResult<DisconnectResp> resolveResult) {
        if (c.a()) {
            c.a("HuaweiApiClientImpl", "get disconnect result and result is " + resolveResult.getStatus().getStatusCode());
        }
        this.f736a.unbindService(this);
        this.d.set(1);
        if (this.h != null) {
            this.h.onConnectionSuspended(1);
        }
    }

    /* access modifiers changed from: private */
    public void b(ResolveResult<ConnectResp> resolveResult) {
        int statusCode = resolveResult.getStatus().getStatusCode();
        if (statusCode == 0) {
            this.d.set(3);
            if (this.h != null) {
                this.h.onConnected();
                return;
            }
            return;
        }
        this.d.set(1);
        this.f736a.unbindService(this);
        if (this.i != null) {
            this.i.onConnectionFailed(new ConnectionResult(statusCode));
        }
    }

    private void d() {
        Intent intent = new Intent("com.huawei.hms.core.aidlservice");
        intent.setPackage(HuaweiApiAvailability.SERVICES_PACKAGE);
        this.f736a.bindService(intent, this, 1);
    }

    private void e() {
        DisconnectInfo disconnectInfo = new DisconnectInfo();
        disconnectInfo.scopeList = this.e;
        disconnectInfo.apiNameList = new ArrayList();
        if (this.g != null) {
            for (Api<?> apiName : this.g.keySet()) {
                disconnectInfo.apiNameList.add(apiName.getApiName());
            }
        }
        com.huawei.hms.support.api.a.a.a(this, disconnectInfo).setResultCallback(new c(this));
    }

    private void f() {
        ConnectInfo connectInfo = new ConnectInfo();
        connectInfo.fingerprint = new com.huawei.hms.a.d(this.f736a).d(this.f736a.getPackageName());
        if (connectInfo.fingerprint == null) {
            connectInfo.fingerprint = "";
        }
        connectInfo.scopeList = this.e;
        connectInfo.apiNameList = new ArrayList();
        if (this.g != null) {
            for (Api<?> apiName : this.g.keySet()) {
                connectInfo.apiNameList.add(apiName.getApiName());
            }
        }
        com.huawei.hms.support.api.a.a.a(this, connectInfo).setResultCallback(new e(this));
    }

    public List<Scope> a() {
        return this.e;
    }

    public void a(List<Scope> list) {
        this.e = list;
    }

    public void a(Map<Api<?>, Api.ApiOptions> map) {
        this.g = map;
    }

    public List<PermissionInfo> b() {
        return this.f;
    }

    public void b(List<PermissionInfo> list) {
        this.f = list;
    }

    public d c() {
        return this.c;
    }

    public void connect() {
        int i2 = this.d.get();
        a.a("HuaweiApiClientImpl", "connect, Connection Status: " + i2);
        if (i2 != 3 && i2 != 2 && i2 != 4) {
            int a2 = g.a(this.f736a);
            if (a2 == 0) {
                d();
                this.d.set(2);
            } else if (this.i != null) {
                this.i.onConnectionFailed(new ConnectionResult(a2));
            }
        }
    }

    public void disconnect() {
        int i2 = this.d.get();
        a.a("HuaweiApiClientImpl", "disconnect, Connection Status: " + i2);
        switch (i2) {
            case 2:
                this.d.set(4);
                return;
            case 3:
                e();
                this.d.set(4);
                return;
            default:
                return;
        }
    }

    public String getAppID() {
        return this.b;
    }

    public Context getContext() {
        return this.f736a;
    }

    public String getPackageName() {
        return this.f736a.getPackageName();
    }

    public String getTransportName() {
        return IPCTransport.class.getName();
    }

    public boolean isConnected() {
        return this.d.get() == 3;
    }

    public boolean isConnecting() {
        return this.d.get() == 2;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.c = d.a.a(iBinder);
        if (this.c == null) {
            a.d("HuaweiApiClientImpl", "mCoreService must not be null.");
            this.d.set(1);
            this.f736a.unbindService(this);
            if (this.i != null) {
                this.i.onConnectionFailed(new ConnectionResult(10));
                return;
            }
            return;
        }
        f();
        this.d.set(2);
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.d.set(1);
        this.c = null;
        if (this.h != null) {
            this.h.onConnectionSuspended(1);
        }
        if (c.a()) {
            c.a("HuaweiApiClientImpl", "enter into  onServiceDisconnected");
        }
    }

    public void setConnectionCallbacks(HuaweiApiClient.ConnectionCallbacks connectionCallbacks) {
        this.h = connectionCallbacks;
    }

    public void setConnectionFailedListener(HuaweiApiClient.OnConnectionFailedListener onConnectionFailedListener) {
        this.i = onConnectionFailedListener;
    }
}
