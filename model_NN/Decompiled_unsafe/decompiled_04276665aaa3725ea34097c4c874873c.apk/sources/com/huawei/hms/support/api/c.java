package com.huawei.hms.support.api;

import com.huawei.hms.core.aidl.IMessageEntity;
import com.huawei.hms.support.api.transport.DatagramTransport;
import java.util.concurrent.atomic.AtomicBoolean;

class c implements DatagramTransport.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AtomicBoolean f749a;
    final /* synthetic */ a b;

    c(a aVar, AtomicBoolean atomicBoolean) {
        this.b = aVar;
        this.f749a = atomicBoolean;
    }

    public void a(int i, IMessageEntity iMessageEntity) {
        if (!this.f749a.get()) {
            this.b.a(i, iMessageEntity);
        }
        this.b.f747a.countDown();
    }
}
