package com.huawei.hms.support.api.entity.auth;

import com.huawei.hms.core.aidl.IMessageEntity;
import com.huawei.hms.core.aidl.a.a;

public class CertFingerprintInfo implements IMessageEntity {
    @a
    private String appID;
    @a
    private String fingerprintInfo;
    @a
    private String packageName;

    public CertFingerprintInfo() {
    }

    public CertFingerprintInfo(String str, String str2, String str3) {
        this.appID = str;
        this.packageName = str2;
        this.fingerprintInfo = str3;
    }

    public String getAppID() {
        return this.appID;
    }

    public String getFingerprintInfo() {
        return this.fingerprintInfo;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setAppID(String str) {
        this.appID = str;
    }

    public void setFingerprintInfo(String str) {
        this.fingerprintInfo = str;
    }

    public void setPackageName(String str) {
        this.packageName = str;
    }
}
