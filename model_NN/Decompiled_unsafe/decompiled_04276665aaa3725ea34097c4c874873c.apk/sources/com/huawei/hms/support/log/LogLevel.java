package com.huawei.hms.support.log;

import android.util.SparseArray;
import java.util.HashMap;
import java.util.Map;

public enum LogLevel {
    ALL(0),
    VERBOSE(2),
    DEBUG(3),
    INFO(4),
    WARN(5),
    ERROR(6),
    ASSERT(7),
    OUT(100),
    NONE(255);
    

    /* renamed from: a  reason: collision with root package name */
    private static final SparseArray<LogLevel> f765a = new SparseArray<>();
    private static final Map<String, LogLevel> b = new HashMap();
    private final int c;

    static {
        for (LogLevel logLevel : values()) {
            f765a.put(logLevel.value(), logLevel);
            b.put(logLevel.name(), logLevel);
        }
    }

    private LogLevel(int i) {
        this.c = i;
    }

    public static LogLevel get(int i) {
        return f765a.get(i);
    }

    public static LogLevel get(String str) {
        return b.get(str);
    }

    public boolean isEnable(LogLevel logLevel) {
        return logLevel.c <= this.c;
    }

    public int value() {
        return this.c;
    }
}
