package com.huawei.hms.support.log;

import android.util.Log;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public abstract class f {
    private static String c = System.getProperty("line.separator");

    /* renamed from: a  reason: collision with root package name */
    protected LogLevel f775a = LogLevel.INFO;
    protected LogLevel b = LogLevel.ERROR;
    private int d = 0;
    private String e;
    private long f;
    private final Map<String, LogLevel> g = new HashMap();

    public f(String str, long j) {
        this.e = str;
        this.f = j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008f A[SYNTHETIC, Splitter:B:24:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b8 A[SYNTHETIC, Splitter:B:34:0x00b8] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00cb A[SYNTHETIC, Splitter:B:41:0x00cb] */
    /* JADX WARNING: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.io.File r7, java.lang.String r8, boolean r9, boolean r10, java.lang.String r11) {
        /*
            r6 = this;
            r0 = 0
            java.io.OutputStreamWriter r1 = new java.io.OutputStreamWriter     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x00e1, all -> 0x00df }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x00e1, all -> 0x00df }
            r3 = 1
            r2.<init>(r7, r3)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x00e1, all -> 0x00df }
            java.lang.String r3 = "UTF-8"
            r1.<init>(r2, r3)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x00e1, all -> 0x00df }
            if (r9 == 0) goto L_0x0056
            if (r10 == 0) goto L_0x0067
            java.lang.String r0 = "success"
        L_0x0014:
            com.huawei.hms.support.log.a.a r2 = com.huawei.hms.support.log.a.a.a()     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            java.lang.String r3 = "rename to "
            com.huawei.hms.support.log.a.a r3 = r2.a(r3)     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            com.huawei.hms.support.log.a.a r3 = r3.a(r11)     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            java.lang.String r4 = " "
            com.huawei.hms.support.log.a.a r3 = r3.a(r4)     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            r3.a(r0)     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            int r0 = r6.d     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            if (r0 <= 0) goto L_0x003a
            com.huawei.hms.support.log.a.a r0 = r2.b()     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            java.lang.String r3 = com.huawei.hms.support.log.b.a()     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            r0.a(r3)     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
        L_0x003a:
            com.huawei.hms.support.log.e$a r0 = new com.huawei.hms.support.log.e$a     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            r3 = 0
            com.huawei.hms.support.log.LogLevel r4 = com.huawei.hms.support.log.LogLevel.OUT     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            r0.<init>(r3, r4)     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            com.huawei.hms.support.log.e r0 = r0.a()     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            com.huawei.hms.support.log.e r0 = r0.a(r2)     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            java.lang.String r0 = r0.toString()     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            r1.write(r0)     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            java.lang.String r0 = com.huawei.hms.support.log.f.c     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            r1.write(r0)     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
        L_0x0056:
            r1.write(r8)     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            java.lang.String r0 = com.huawei.hms.support.log.f.c     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            r1.write(r0)     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            r1.flush()     // Catch:{ FileNotFoundException -> 0x00d8, IOException -> 0x009c }
            if (r1 == 0) goto L_0x0066
            r1.close()     // Catch:{ IOException -> 0x006a }
        L_0x0066:
            return
        L_0x0067:
            java.lang.String r0 = "failure"
            goto L_0x0014
        L_0x006a:
            r0 = move-exception
            java.lang.String r0 = "LoggerBase"
            java.lang.String r1 = "println, close oswriter error"
            android.util.Log.e(r0, r1)
            goto L_0x0066
        L_0x0073:
            r1 = move-exception
            r1 = r0
        L_0x0075:
            java.lang.String r0 = "LoggerBase"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c5 }
            r2.<init>()     // Catch:{ all -> 0x00c5 }
            java.lang.String r3 = "println error, error:FileNotFoundException, file:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00c5 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ all -> 0x00c5 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00c5 }
            android.util.Log.e(r0, r2)     // Catch:{ all -> 0x00c5 }
            if (r1 == 0) goto L_0x0066
            r1.close()     // Catch:{ IOException -> 0x0093 }
            goto L_0x0066
        L_0x0093:
            r0 = move-exception
            java.lang.String r0 = "LoggerBase"
            java.lang.String r1 = "println, close oswriter error"
            android.util.Log.e(r0, r1)
            goto L_0x0066
        L_0x009c:
            r0 = move-exception
            r0 = r1
        L_0x009e:
            java.lang.String r1 = "LoggerBase"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00da }
            r2.<init>()     // Catch:{ all -> 0x00da }
            java.lang.String r3 = "println error, error:IOException, file:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00da }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ all -> 0x00da }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00da }
            android.util.Log.e(r1, r2)     // Catch:{ all -> 0x00da }
            if (r0 == 0) goto L_0x0066
            r0.close()     // Catch:{ IOException -> 0x00bc }
            goto L_0x0066
        L_0x00bc:
            r0 = move-exception
            java.lang.String r0 = "LoggerBase"
            java.lang.String r1 = "println, close oswriter error"
            android.util.Log.e(r0, r1)
            goto L_0x0066
        L_0x00c5:
            r0 = move-exception
        L_0x00c6:
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x00c9:
            if (r0 == 0) goto L_0x00ce
            r0.close()     // Catch:{ IOException -> 0x00cf }
        L_0x00ce:
            throw r1
        L_0x00cf:
            r0 = move-exception
            java.lang.String r0 = "LoggerBase"
            java.lang.String r2 = "println, close oswriter error"
            android.util.Log.e(r0, r2)
            goto L_0x00ce
        L_0x00d8:
            r0 = move-exception
            goto L_0x0075
        L_0x00da:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00c6
        L_0x00df:
            r1 = move-exception
            goto L_0x00c9
        L_0x00e1:
            r1 = move-exception
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huawei.hms.support.log.f.a(java.io.File, java.lang.String, boolean, boolean, java.lang.String):void");
    }

    private boolean a(File file, File file2) {
        if (file2.exists() && !file2.delete() && Log.isLoggable("LoggerBase", 6)) {
            Log.e("LoggerBase", "delete file failed:" + file2);
        }
        return file.renameTo(file2);
    }

    public LogLevel a(String str) {
        LogLevel logLevel;
        synchronized (this) {
            logLevel = this.g.get(str);
            if (logLevel == null) {
                logLevel = this.f775a;
            }
        }
        return logLevel;
    }

    public abstract void a(e eVar);

    /* access modifiers changed from: protected */
    public void a(String str, LogLevel logLevel, String str2) {
        if (a(str, logLevel)) {
            b(str2);
        }
    }

    public boolean a(String str, LogLevel logLevel) {
        return logLevel.value() >= a(str).value();
    }

    public long b() {
        return this.f;
    }

    public void b(LogLevel logLevel) {
        this.f775a = logLevel;
        synchronized (this) {
            for (Map.Entry<String, LogLevel> value : this.g.entrySet()) {
                value.setValue(logLevel);
            }
        }
    }

    public abstract void b(e eVar);

    /* access modifiers changed from: protected */
    public void b(String str) {
        boolean z = true;
        boolean z2 = false;
        synchronized (this) {
            if (this.e != null) {
                String str2 = null;
                long b2 = b();
                File file = new File(this.e);
                if (!file.exists()) {
                    file.setReadable(true);
                    file.setWritable(true);
                    file.setExecutable(false, false);
                }
                if (b2 > 0) {
                    File parentFile = file.getParentFile();
                    if (parentFile == null || parentFile.exists() || parentFile.mkdirs()) {
                        if (file.length() + ((long) str.length()) > b2) {
                            str2 = this.e + ".bak";
                            z2 = a(file, new File(str2));
                            a(file, str, z, z2, str2);
                            this.d++;
                        }
                    }
                }
                z = false;
                a(file, str, z, z2, str2);
                this.d++;
            }
        }
    }
}
