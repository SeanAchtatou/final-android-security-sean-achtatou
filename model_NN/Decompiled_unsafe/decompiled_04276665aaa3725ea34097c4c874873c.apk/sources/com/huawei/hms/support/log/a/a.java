package com.huawei.hms.support.log.a;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f767a = new StringBuilder(64);

    public static a a() {
        return new a();
    }

    public <T> a a(T t) {
        if (this.f767a != null) {
            this.f767a.append((Object) t);
        }
        return this;
    }

    public a b() {
        return a(10);
    }

    public String c() {
        if (this.f767a == null) {
            return "";
        }
        String sb = this.f767a.toString();
        this.f767a = null;
        return sb;
    }

    public String toString() {
        return this.f767a == null ? "" : this.f767a.toString();
    }
}
