package com.huawei.hms.support.api;

import com.huawei.hms.support.api.client.Result;

public class ResolveResult<T> extends Result {

    /* renamed from: a  reason: collision with root package name */
    private T f746a;

    public ResolveResult() {
        this.f746a = null;
    }

    public ResolveResult(T t) {
        this.f746a = t;
    }

    public T getValue() {
        return this.f746a;
    }
}
