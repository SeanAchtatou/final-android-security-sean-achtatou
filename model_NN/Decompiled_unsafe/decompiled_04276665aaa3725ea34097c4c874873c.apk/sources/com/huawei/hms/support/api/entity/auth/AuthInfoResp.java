package com.huawei.hms.support.api.entity.auth;

import com.huawei.hms.core.aidl.IMessageEntity;
import com.huawei.hms.core.aidl.a.a;

public class AuthInfoResp implements IMessageEntity {
    @a
    private AuthorizationInfo authInfo;
    @a
    private String errorReason;
    @a
    private int rtnCode = 0;

    public AuthorizationInfo getAuthInfo() {
        return this.authInfo;
    }

    public String getErrorReason() {
        return this.errorReason;
    }

    public int getRtnCode() {
        return this.rtnCode;
    }

    public void setAuthInfo(AuthorizationInfo authorizationInfo) {
        this.authInfo = authorizationInfo;
    }

    public void setErrorReason(String str) {
        this.errorReason = str;
    }

    public void setRtnCode(int i) {
        this.rtnCode = i;
    }
}
