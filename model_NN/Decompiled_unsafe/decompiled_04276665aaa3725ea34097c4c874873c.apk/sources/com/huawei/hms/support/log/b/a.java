package com.huawei.hms.support.log.b;

public abstract class a extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private volatile boolean f769a;
    private volatile boolean b;
    private b c;

    public a(String str) {
        this(str, null);
    }

    public a(String str, b bVar) {
        super(str);
        this.f769a = true;
        this.b = false;
        this.c = bVar;
    }

    private boolean f() {
        return a();
    }

    private void g() {
        e();
        c();
    }

    private boolean h() {
        return b();
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract boolean b();

    /* access modifiers changed from: protected */
    public void c() {
    }

    public String d() {
        StringBuilder sb = new StringBuilder(64);
        sb.append(getName());
        sb.append('{');
        sb.append(getId());
        sb.append('}');
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (!this.b && this.c != null) {
            this.c.a(this);
        }
    }

    public void run() {
        if (f()) {
            while (this.f769a) {
                try {
                    if (!h()) {
                        break;
                    }
                } catch (Exception e) {
                    try {
                        sleep(1000);
                    } catch (InterruptedException e2) {
                    }
                }
            }
        }
        g();
    }

    public String toString() {
        return d();
    }
}
