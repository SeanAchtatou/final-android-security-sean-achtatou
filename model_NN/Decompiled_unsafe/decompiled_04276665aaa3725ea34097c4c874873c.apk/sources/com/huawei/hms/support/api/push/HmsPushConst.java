package com.huawei.hms.support.api.push;

public interface HmsPushConst {
    public static final String NEW_LINE = "\n\t";

    public interface ErrorCode {
        public static final int NETWORK_INVALID = 907122005;
        public static final int PLUGIN_TOKEN_INVALID = 907122004;
        public static final int REPORT_PARAM_INVALID = 907122003;
        public static final int REPORT_PLUGIN_TOO_FREQUENTLY = 907122006;
        public static final int REPORT_SYSTEM_ERROR = 907122002;
        public static final int REPORT_TAG_SUCCESS = 907122001;
    }
}
