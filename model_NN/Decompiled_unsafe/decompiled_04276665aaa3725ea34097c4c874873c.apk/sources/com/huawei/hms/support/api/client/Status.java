package com.huawei.hms.support.api.client;

import android.app.Activity;
import android.app.PendingIntent;
import com.qq.e.comm.constants.ErrorCode;
import java.util.Arrays;

public final class Status {
    public static final Status CoreException = new Status(500);
    public static final Status MessageNotFound = new Status(ErrorCode.NetWorkError.TIME_OUT_ERROR);
    public static final Status SUCCESS = new Status(0);

    /* renamed from: a  reason: collision with root package name */
    private int f750a;
    private String b;
    private PendingIntent c;

    public Status(int i) {
        this(i, null);
    }

    public Status(int i, String str) {
        this(i, str, null);
    }

    public Status(int i, String str, PendingIntent pendingIntent) {
        this.f750a = i;
        this.b = str;
        this.c = pendingIntent;
    }

    public static boolean equal(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public boolean equals(Object obj) {
        if (obj instanceof Status) {
            Status status = (Status) obj;
            return this.f750a == status.f750a && equal(this.b, status.b) && equal(this.c, status.c);
        }
    }

    public PendingIntent getResolution() {
        return this.c;
    }

    public int getStatusCode() {
        return this.f750a;
    }

    public String getStatusMessage() {
        return this.b;
    }

    public boolean hasResolution() {
        return this.c != null;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f750a), this.b, this.c});
    }

    public boolean isSuccess() {
        return this.f750a <= 0;
    }

    public void startResolutionForResult(Activity activity, int i) {
        if (hasResolution()) {
            activity.startIntentSenderForResult(this.c.getIntentSender(), i, null, 0, 0, 0);
        }
    }

    public String toString() {
        return getClass().getName() + " {\n\tstatusCode: " + this.f750a + "\n\tstatusMessage: " + this.b + "\n\tmPendingIntent: " + this.c + "\n}";
    }
}
