package com.huawei.hms.support.api;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Pair;
import com.huawei.hms.core.aidl.IMessageEntity;
import com.huawei.hms.support.api.client.ApiClient;
import com.huawei.hms.support.api.client.PendingResult;
import com.huawei.hms.support.api.client.Result;
import com.huawei.hms.support.api.client.ResultCallback;
import com.huawei.hms.support.api.entity.core.CommonCode;
import com.huawei.hms.support.api.transport.DatagramTransport;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class a<R extends Result, T extends IMessageEntity> extends PendingResult<R> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public CountDownLatch f747a;
    /* access modifiers changed from: private */
    public R b = null;
    private WeakReference<ApiClient> c;
    protected DatagramTransport transport = null;

    /* renamed from: com.huawei.hms.support.api.a$a  reason: collision with other inner class name */
    public class C0024a<R extends Result> extends Handler {
        public C0024a() {
            this(Looper.getMainLooper());
        }

        public C0024a(Looper looper) {
            super(looper);
        }

        public void a(ResultCallback<? super R> resultCallback, R r) {
            sendMessage(obtainMessage(1, new Pair(resultCallback, r)));
        }

        /* access modifiers changed from: protected */
        public void b(ResultCallback<? super R> resultCallback, R r) {
            resultCallback.onResult(r);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Pair pair = (Pair) message.obj;
                    b((ResultCallback) pair.first, (Result) pair.second);
                    return;
                default:
                    return;
            }
        }
    }

    public a(ApiClient apiClient, String str, IMessageEntity iMessageEntity) {
        a(apiClient, str, iMessageEntity, getResponseType());
    }

    public a(ApiClient apiClient, String str, IMessageEntity iMessageEntity, Class<T> cls) {
        a(apiClient, str, iMessageEntity, cls);
    }

    /* access modifiers changed from: private */
    public void a(int i, IMessageEntity iMessageEntity) {
        if (i <= 0) {
            this.b = onComplete(iMessageEntity);
        } else {
            this.b = onError(i);
        }
    }

    private void a(ApiClient apiClient, String str, IMessageEntity iMessageEntity, Class<T> cls) {
        if (apiClient == null) {
            throw new IllegalArgumentException("apiClient cannot be null.");
        }
        this.c = new WeakReference<>(apiClient);
        this.f747a = new CountDownLatch(1);
        try {
            this.transport = (DatagramTransport) Class.forName(apiClient.getTransportName()).getConstructor(String.class, IMessageEntity.class, Class.class).newInstance(str, iMessageEntity, cls);
        } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            throw new IllegalStateException("Instancing transport exception, " + e.getMessage(), e);
        }
    }

    public final R await() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("await must not be called on the UI thread");
        }
        ApiClient apiClient = this.c.get();
        if (!checkApiClient(apiClient)) {
            a(CommonCode.ErrorCode.CLIENT_API_INVALID, null);
            return this.b;
        }
        this.transport.a(apiClient, new b(this));
        try {
            this.f747a.await();
        } catch (InterruptedException e) {
            a(CommonCode.ErrorCode.INTERNAL_ERROR, null);
        }
        return this.b;
    }

    public R await(long j, TimeUnit timeUnit) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("await must not be called on the UI thread");
        }
        ApiClient apiClient = this.c.get();
        if (!checkApiClient(apiClient)) {
            a(CommonCode.ErrorCode.CLIENT_API_INVALID, null);
            return this.b;
        }
        AtomicBoolean atomicBoolean = new AtomicBoolean();
        this.transport.b(apiClient, new c(this, atomicBoolean));
        try {
            if (!this.f747a.await(j, timeUnit)) {
                atomicBoolean.set(true);
                a(CommonCode.ErrorCode.EXECUTE_TIMEOUT, null);
            }
        } catch (InterruptedException e) {
            a(CommonCode.ErrorCode.INTERNAL_ERROR, null);
        }
        return this.b;
    }

    /* access modifiers changed from: protected */
    public boolean checkApiClient(ApiClient apiClient) {
        return apiClient != null && apiClient.isConnected();
    }

    /* access modifiers changed from: protected */
    public Class<T> getResponseType() {
        return (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    public abstract R onComplete(IMessageEntity iMessageEntity);

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: R
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected R onError(int r3) {
        /*
            r2 = this;
            java.lang.Class r0 = r2.getClass()
            java.lang.reflect.Type r0 = r0.getGenericSuperclass()
            java.lang.reflect.ParameterizedType r0 = (java.lang.reflect.ParameterizedType) r0
            java.lang.reflect.Type[] r0 = r0.getActualTypeArguments()
            r1 = 0
            r0 = r0[r1]
            java.lang.Class r0 = com.huawei.hms.support.a.a.a(r0)
            java.lang.Object r0 = r0.newInstance()     // Catch:{ Exception -> 0x002a }
            com.huawei.hms.support.api.client.Result r0 = (com.huawei.hms.support.api.client.Result) r0     // Catch:{ Exception -> 0x002a }
            r2.b = r0     // Catch:{ Exception -> 0x002a }
            R r0 = r2.b     // Catch:{ Exception -> 0x002a }
            com.huawei.hms.support.api.client.Status r1 = new com.huawei.hms.support.api.client.Status     // Catch:{ Exception -> 0x002a }
            r1.<init>(r3)     // Catch:{ Exception -> 0x002a }
            r0.setStatus(r1)     // Catch:{ Exception -> 0x002a }
            R r0 = r2.b
        L_0x0029:
            return r0
        L_0x002a:
            r0 = move-exception
            r0 = 0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.huawei.hms.support.api.a.onError(int):com.huawei.hms.support.api.client.Result");
    }

    public final void setResultCallback(Looper looper, ResultCallback<R> resultCallback) {
        if (looper == null) {
            looper = Looper.myLooper();
        }
        C0024a aVar = new C0024a(looper);
        ApiClient apiClient = this.c.get();
        if (!checkApiClient(apiClient)) {
            a(CommonCode.ErrorCode.CLIENT_API_INVALID, null);
            aVar.a(resultCallback, this.b);
            return;
        }
        this.transport.b(apiClient, new d(this, aVar, resultCallback));
    }

    public final void setResultCallback(ResultCallback<R> resultCallback) {
        setResultCallback(Looper.getMainLooper(), resultCallback);
    }
}
