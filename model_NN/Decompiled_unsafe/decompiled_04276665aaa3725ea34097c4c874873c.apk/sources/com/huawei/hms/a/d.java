package com.huawei.hms.a;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private final PackageManager f729a;

    public enum a {
        ENABLED,
        DISABLED,
        NOT_INSTALLED
    }

    public d(Context context) {
        this.f729a = context.getPackageManager();
    }

    private byte[] f(String str) {
        InputStream inputStream = null;
        try {
            PackageInfo packageInfo = this.f729a.getPackageInfo(str, 64);
            if (packageInfo == null || packageInfo.signatures.length <= 0) {
                c.a(inputStream);
                com.huawei.hms.a.a.a.d("PackageManagerHelper", "Failed to get application signature certificate fingerprint.");
                return new byte[0];
            }
            inputStream = c.a(packageInfo.signatures[0].toByteArray());
            return CertificateFactory.getInstance("X.509").generateCertificate(inputStream).getEncoded();
        } catch (PackageManager.NameNotFoundException e) {
            com.huawei.hms.a.a.a.b("PackageManagerHelper", "Failed to get application signature certificate fingerprint.", e);
        } catch (IOException e2) {
            com.huawei.hms.a.a.a.b("PackageManagerHelper", "Failed to get application signature certificate fingerprint.", e2);
        } catch (CertificateException e3) {
            com.huawei.hms.a.a.a.b("PackageManagerHelper", "Failed to get application signature certificate fingerprint.", e3);
        } finally {
            c.a(inputStream);
        }
    }

    public a a(String str) {
        try {
            return this.f729a.getApplicationInfo(str, 0).enabled ? a.ENABLED : a.DISABLED;
        } catch (PackageManager.NameNotFoundException e) {
            return a.NOT_INSTALLED;
        }
    }

    public boolean a(String str, String str2) {
        try {
            PackageInfo packageInfo = this.f729a.getPackageInfo(str, 8);
            if (packageInfo == null || packageInfo.providers == null) {
                return false;
            }
            for (ProviderInfo providerInfo : packageInfo.providers) {
                if (str2.equals(providerInfo.authority)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public boolean a(String str, String str2, String str3) {
        InputStream inputStream = null;
        boolean z = false;
        PackageInfo packageArchiveInfo = this.f729a.getPackageArchiveInfo(str, 64);
        if (packageArchiveInfo != null && packageArchiveInfo.signatures.length > 0 && str2.equals(packageArchiveInfo.packageName)) {
            try {
                inputStream = c.a(packageArchiveInfo.signatures[0].toByteArray());
                z = str3.equalsIgnoreCase(b.b(e.a(CertificateFactory.getInstance("X.509").generateCertificate(inputStream).getEncoded()), true));
            } catch (IOException e) {
                com.huawei.hms.a.a.a.b("PackageManagerHelper", "Failed to get the archive signature certificate fingerprint.", e);
            } catch (CertificateException e2) {
                com.huawei.hms.a.a.a.b("PackageManagerHelper", "Failed to get application signature certificate fingerprint.", e2);
            } finally {
                c.a(inputStream);
            }
        }
        return z;
    }

    public int b(String str) {
        try {
            PackageInfo packageInfo = this.f729a.getPackageInfo(str, 16);
            if (packageInfo != null) {
                return packageInfo.versionCode;
            }
            return 0;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }

    public String c(String str) {
        try {
            PackageInfo packageInfo = this.f729a.getPackageInfo(str, 16);
            return (packageInfo == null || packageInfo.versionName == null) ? "" : packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    public String d(String str) {
        byte[] f = f(str);
        if (f == null || f.length == 0) {
            return null;
        }
        return b.b(e.a(f), true);
    }

    public int e(String str) {
        try {
            ApplicationInfo applicationInfo = this.f729a.getApplicationInfo(str, 0);
            if (applicationInfo != null) {
                return applicationInfo.targetSdkVersion;
            }
            return 0;
        } catch (PackageManager.NameNotFoundException e) {
            return 0;
        }
    }
}
