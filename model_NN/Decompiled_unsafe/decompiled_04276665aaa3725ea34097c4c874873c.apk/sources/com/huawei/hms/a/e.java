package com.huawei.hms.a;

import com.huawei.hms.a.a.a;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class e {
    public static byte[] a(File file) {
        BufferedInputStream bufferedInputStream;
        NoSuchAlgorithmException e;
        BufferedInputStream bufferedInputStream2 = null;
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            try {
                byte[] bArr = new byte[4096];
                int i = 0;
                while (true) {
                    int read = bufferedInputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    i += read;
                    instance.update(bArr, 0, read);
                }
                if (i > 0) {
                    byte[] digest = instance.digest();
                    c.a((InputStream) bufferedInputStream);
                    return digest;
                }
                c.a((InputStream) bufferedInputStream);
                return new byte[0];
            } catch (NoSuchAlgorithmException e2) {
                e = e2;
                try {
                    a.b("SHA256", "NoSuchAlgorithmException", e);
                    c.a((InputStream) bufferedInputStream);
                    return new byte[0];
                } catch (Throwable th) {
                    th = th;
                    bufferedInputStream2 = bufferedInputStream;
                    c.a((InputStream) bufferedInputStream2);
                    throw th;
                }
            } catch (IOException e3) {
                bufferedInputStream2 = bufferedInputStream;
                try {
                    a.d("SHA256", "An exception occurred while computing file 'SHA-256'.");
                    c.a((InputStream) bufferedInputStream2);
                    return new byte[0];
                } catch (Throwable th2) {
                    th = th2;
                    c.a((InputStream) bufferedInputStream2);
                    throw th;
                }
            }
        } catch (NoSuchAlgorithmException e4) {
            NoSuchAlgorithmException noSuchAlgorithmException = e4;
            bufferedInputStream = null;
            e = noSuchAlgorithmException;
            a.b("SHA256", "NoSuchAlgorithmException", e);
            c.a((InputStream) bufferedInputStream);
            return new byte[0];
        } catch (IOException e5) {
            a.d("SHA256", "An exception occurred while computing file 'SHA-256'.");
            c.a((InputStream) bufferedInputStream2);
            return new byte[0];
        }
    }

    public static byte[] a(byte[] bArr) {
        try {
            return MessageDigest.getInstance("SHA-256").digest(bArr);
        } catch (NoSuchAlgorithmException e) {
            a.b("SHA256", "NoSuchAlgorithmException", e);
            return new byte[0];
        }
    }
}
