package com.baoyz.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.TypedValue;

/* compiled from: SmartisanDrawable */
public class f extends d {

    /* renamed from: a  reason: collision with root package name */
    RectF f627a;
    float b;
    float c;
    float d;
    float e;
    float f;
    final float g = 153.0f;
    final float h = ((float) b(12));
    final float i = ((float) (2.670353755551324d * ((double) this.h)));
    final float j = ((float) b(3));
    final float k = ((float) ((int) (((double) this.i) * 0.15d)));
    final float l = 0.43633232f;
    final float m = ((float) ((int) (((double) this.k) * Math.sin(0.4363323152065277d))));
    final float n = ((float) ((int) (((double) this.k) * Math.cos(0.4363323152065277d))));
    final Paint o = new Paint();
    int p;
    boolean q;
    float r;

    public f(Context context, PullRefreshLayout pullRefreshLayout) {
        super(context, pullRefreshLayout);
        this.o.setAntiAlias(true);
        this.o.setStrokeJoin(Paint.Join.ROUND);
        this.o.setStrokeCap(Paint.Cap.ROUND);
        this.o.setStrokeWidth(this.j);
        this.o.setStyle(Paint.Style.STROKE);
        this.o.setColor(-7829368);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.c = (float) d().getFinalOffset();
        this.b = this.c;
        this.f627a = new RectF(((float) (rect.width() / 2)) - (this.b / 2.0f), ((float) rect.top) - (this.c / 2.0f), ((float) (rect.width() / 2)) + (this.b / 2.0f), ((float) rect.top) + (this.c / 2.0f));
        this.d = this.f627a.centerX();
        this.e = this.f627a.centerY();
    }

    public void a(float f2) {
        this.f = f2;
        invalidateSelf();
    }

    public void a(int[] iArr) {
        if (iArr != null && iArr.length > 0) {
            this.o.setColor(iArr[0]);
        }
    }

    public void a(int i2) {
        this.p += i2;
        invalidateSelf();
    }

    public void start() {
        this.q = true;
        this.r = 0.0f;
        invalidateSelf();
    }

    public void stop() {
        this.q = false;
    }

    public boolean isRunning() {
        return this.q;
    }

    public void draw(Canvas canvas) {
        canvas.save();
        canvas.translate(0.0f, (float) (this.p / 2));
        canvas.clipRect(this.f627a);
        if (((float) this.p) > this.c && !isRunning()) {
            canvas.rotate(((((float) this.p) - this.c) / this.c) * 360.0f, this.d, this.e);
        }
        if (isRunning()) {
            canvas.rotate(this.r, this.d, this.e);
            this.r = this.r < 360.0f ? this.r + 10.0f : 0.0f;
            invalidateSelf();
        }
        if (this.f <= 0.5f) {
            float f2 = this.f / 0.5f;
            float f3 = this.d - this.h;
            float f4 = (this.e + this.i) - (this.i * f2);
            canvas.drawLine(f3, f4, f3, f4 + this.i, this.o);
            canvas.drawLine(f3, f4, f3 - this.m, f4 + this.n, this.o);
            float f5 = this.h + this.d;
            float f6 = (this.i * f2) + (this.e - this.i);
            canvas.drawLine(f5, f6, f5, f6 - this.i, this.o);
            canvas.drawLine(f5, f6, f5 + this.m, f6 - this.n, this.o);
        } else {
            float f7 = (this.f - 0.5f) / 0.5f;
            float f8 = this.d - this.h;
            float f9 = this.e;
            canvas.drawLine(f8, f9, f8, (this.i + f9) - (this.i * f7), this.o);
            RectF rectF = new RectF(this.d - this.h, this.e - this.h, this.d + this.h, this.e + this.h);
            canvas.drawArc(rectF, 180.0f, 153.0f * f7, false, this.o);
            float f10 = this.d + this.h;
            float f11 = this.e;
            canvas.drawLine(f10, f11, f10, (f11 - this.i) + (this.i * f7), this.o);
            canvas.drawArc(rectF, 0.0f, 153.0f * f7, false, this.o);
            canvas.save();
            canvas.rotate(153.0f * f7, this.d, this.e);
            canvas.drawLine(f8, f9, f8 - this.m, f9 + this.n, this.o);
            canvas.drawLine(f10, f11, f10 + this.m, f11 - this.n, this.o);
            canvas.restore();
        }
        canvas.restore();
    }

    private int b(int i2) {
        return (int) TypedValue.applyDimension(1, (float) i2, c().getResources().getDisplayMetrics());
    }
}
