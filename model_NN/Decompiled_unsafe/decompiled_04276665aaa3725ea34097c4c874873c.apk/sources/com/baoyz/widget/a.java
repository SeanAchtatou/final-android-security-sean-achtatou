package com.baoyz.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.util.TypedValue;
import com.igexin.download.Downloads;
import java.security.InvalidParameterException;

/* compiled from: CirclesDrawable */
class a extends d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private static final float f614a = ((float) C0015a.values().length);
    private static final float b = (10000.0f / f614a);
    private static int o;
    private static int p;
    private static int q;
    private static int r;
    private int A;
    private Rect B;
    private Paint c;
    private Paint d;
    private Paint e;
    private RectF f = new RectF();
    private int g;
    private Path h;
    private int i;
    private C0015a j;
    private int k;
    private int l;
    private int m;
    private ColorFilter n;
    private int s;
    private int t;
    private boolean u;
    private Handler v = new Handler();
    private int w;
    private boolean x;
    private int y;
    private int z;

    /* renamed from: com.baoyz.widget.a$a  reason: collision with other inner class name */
    /* compiled from: CirclesDrawable */
    private enum C0015a {
        FOLDING_DOWN,
        FOLDING_LEFT,
        FOLDING_UP,
        FOLDING_RIGHT
    }

    public a(Context context, PullRefreshLayout pullRefreshLayout) {
        super(context, pullRefreshLayout);
    }

    public void start() {
        this.w = 2500;
        this.x = true;
        this.v.postDelayed(this, 10);
    }

    public void stop() {
        this.x = false;
        this.v.removeCallbacks(this);
    }

    public boolean isRunning() {
        return this.x;
    }

    public void a(int[] iArr) {
        b(iArr);
    }

    public void a(float f2) {
        b((int) (2500.0f * f2));
    }

    private void b(int i2) {
        int i3;
        boolean z2 = true;
        boolean z3 = false;
        if (((float) i2) == 10000.0f) {
            i2 = 0;
        }
        this.j = C0015a.values()[(int) (((float) i2) / b)];
        a(this.j);
        int i4 = (int) (((float) i2) % b);
        if (!this.u) {
            if (i4 != ((int) (((float) i2) % (b / 2.0f)))) {
                z3 = true;
            }
            i3 = i4;
        } else {
            if (i4 != ((int) (((float) i2) % (b / 2.0f)))) {
                z2 = false;
            }
            boolean z4 = z2;
            i3 = (int) (b - ((float) i4));
            z3 = z4;
        }
        this.c.setColor(this.s);
        this.d.setColor(this.t);
        if (!z3) {
            this.e.setColor(this.d.getColor());
        } else {
            this.e.setColor(this.c.getColor());
        }
        this.e.setAlpha(((int) (55.0f * (((float) i3) / b))) + Downloads.STATUS_SUCCESS);
        this.m = (int) (((float) this.k) + ((((float) i3) / b) * ((float) (this.l - this.k))));
    }

    public void a(int i2) {
        this.y += i2;
        invalidateSelf();
    }

    public void run() {
        this.w += 80;
        if (((float) this.w) > 10000.0f) {
            this.w = 0;
        }
        if (this.x) {
            this.v.postDelayed(this, 20);
            b(this.w);
            invalidateSelf();
        }
    }

    private void b(int[] iArr) {
        c(iArr);
        this.h = new Path();
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        this.c = new Paint(paint);
        this.d = new Paint(paint);
        this.e = new Paint(paint);
        setColorFilter(this.n);
    }

    private void c(int[] iArr) {
        if (iArr == null || iArr.length < 4) {
            throw new InvalidParameterException("The color scheme length must be 4");
        }
        o = iArr[0];
        p = iArr[1];
        q = iArr[2];
        r = iArr[3];
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.z = c(40);
        this.A = this.z;
        this.y = (-this.A) - ((d().getFinalOffset() - this.A) / 2);
        this.B = rect;
        a(this.z, this.A);
    }

    private void a(C0015a aVar) {
        switch (aVar) {
            case FOLDING_DOWN:
                this.s = o;
                this.t = p;
                this.u = false;
                return;
            case FOLDING_LEFT:
                this.s = o;
                this.t = q;
                this.u = true;
                return;
            case FOLDING_UP:
                this.s = q;
                this.t = r;
                this.u = true;
                return;
            case FOLDING_RIGHT:
                this.s = p;
                this.t = r;
                this.u = false;
                return;
            default:
                return;
        }
    }

    public void draw(Canvas canvas) {
        if (this.j != null) {
            canvas.save();
            canvas.translate((float) ((this.B.width() / 2) - (this.z / 2)), (float) this.y);
            a(canvas);
            canvas.restore();
        }
    }

    private void a(int i2, int i3) {
        this.g = Math.min(i2, i3);
        this.i = this.g / 2;
        this.f.set(0.0f, 0.0f, (float) this.g, (float) this.g);
        this.k = (-this.g) / 6;
        this.l = this.g + (this.g / 6);
    }

    private void a(Canvas canvas) {
        switch (this.j) {
            case FOLDING_DOWN:
            case FOLDING_UP:
                c(canvas);
                break;
            case FOLDING_LEFT:
            case FOLDING_RIGHT:
                b(canvas);
                break;
        }
        canvas.drawPath(this.h, this.e);
    }

    private void b(Canvas canvas) {
        canvas.drawArc(this.f, 90.0f, 180.0f, true, this.c);
        canvas.drawArc(this.f, -270.0f, -180.0f, true, this.d);
        this.h.reset();
        this.h.moveTo((float) this.i, 0.0f);
        this.h.cubicTo((float) this.m, 0.0f, (float) this.m, (float) this.g, (float) this.i, (float) this.g);
    }

    private void c(Canvas canvas) {
        canvas.drawArc(this.f, 0.0f, -180.0f, true, this.c);
        canvas.drawArc(this.f, -180.0f, -180.0f, true, this.d);
        this.h.reset();
        this.h.moveTo(0.0f, (float) this.i);
        this.h.cubicTo(0.0f, (float) this.m, (float) this.g, (float) this.m, (float) this.g, (float) this.i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.n = colorFilter;
        this.c.setColorFilter(colorFilter);
        this.d.setColorFilter(colorFilter);
        this.e.setColorFilter(colorFilter);
    }

    private int c(int i2) {
        return (int) TypedValue.applyDimension(1, (float) i2, c().getResources().getDisplayMetrics());
    }
}
