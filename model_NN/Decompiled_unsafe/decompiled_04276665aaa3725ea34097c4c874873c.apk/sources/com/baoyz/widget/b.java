package com.baoyz.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import java.util.ArrayList;

/* compiled from: MaterialDrawable */
class b extends d implements Animatable {

    /* renamed from: a  reason: collision with root package name */
    private static final Interpolator f617a = new LinearInterpolator();
    /* access modifiers changed from: private */
    public static final Interpolator b = new a();
    /* access modifiers changed from: private */
    public static final Interpolator c = new d();
    private static final Interpolator d = new AccelerateDecelerateInterpolator();
    private final int[] e = {-16777216};
    private final ArrayList<Animation> f = new ArrayList<>();
    private final c g;
    private float h;
    private Resources i;
    /* access modifiers changed from: private */
    public View j;
    /* access modifiers changed from: private */
    public Animation k;
    /* access modifiers changed from: private */
    public float l;
    private double m;
    private double n;
    private Animation o;
    private int p;
    private int q;
    private ShapeDrawable r;
    private int s;
    private int t;
    private final Drawable.Callback u = new Drawable.Callback() {
        public void invalidateDrawable(Drawable drawable) {
            b.this.invalidateSelf();
        }

        public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
            b.this.scheduleSelf(runnable, j);
        }

        public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
            b.this.unscheduleSelf(runnable);
        }
    };

    public b(Context context, PullRefreshLayout pullRefreshLayout) {
        super(context, pullRefreshLayout);
        this.j = pullRefreshLayout;
        this.i = context.getResources();
        this.g = new c(this.u);
        this.g.a(this.e);
        b(1);
        f();
        e();
        c(-328966);
        this.t = d(40);
        this.s = (-this.t) - ((d().getFinalOffset() - this.t) / 2);
    }

    private void e() {
        float f2 = c().getResources().getDisplayMetrics().density;
        this.p = (int) (f2 * 3.5f);
        this.r = new ShapeDrawable(new C0016b(this.p, (int) (20.0f * f2 * 2.0f)));
        this.r.getPaint().setShadowLayer((float) this.p, (float) ((int) (0.0f * f2)), (float) ((int) (1.75f * f2)), 503316480);
        this.q = this.p;
        this.r.getPaint().setColor(-1);
    }

    /* renamed from: com.baoyz.widget.b$b  reason: collision with other inner class name */
    /* compiled from: MaterialDrawable */
    private class C0016b extends OvalShape {
        private RadialGradient b;
        private int c;
        private Paint d = new Paint();
        private int e;

        public C0016b(int i, int i2) {
            this.c = i;
            this.e = i2;
            this.b = new RadialGradient((float) (this.e / 2), (float) (this.e / 2), (float) this.c, new int[]{1023410176, 0}, (float[]) null, Shader.TileMode.CLAMP);
            this.d.setShader(this.b);
        }

        public void draw(Canvas canvas, Paint paint) {
            int centerX = b.this.getBounds().centerX();
            int centerY = b.this.getBounds().centerY();
            canvas.drawCircle((float) centerX, (float) centerY, (float) ((this.e / 2) + this.c), this.d);
            canvas.drawCircle((float) centerX, (float) centerY, (float) (this.e / 2), paint);
        }
    }

    private void a(double d2, double d3, double d4, double d5, float f2, float f3) {
        c cVar = this.g;
        float f4 = this.i.getDisplayMetrics().density;
        this.m = ((double) f4) * d2;
        this.n = ((double) f4) * d3;
        cVar.a(((float) d5) * f4);
        cVar.a(((double) f4) * d4);
        cVar.b(0);
        cVar.a(f2 * f4, f4 * f3);
        cVar.a((int) this.m, (int) this.n);
    }

    public void b(int i2) {
        if (i2 == 0) {
            a(56.0d, 56.0d, 12.5d, 3.0d, 12.0f, 6.0f);
        } else {
            a(40.0d, 40.0d, 8.75d, 2.5d, 10.0f, 5.0f);
        }
    }

    public void a(boolean z) {
        this.g.a(z);
    }

    public void b(float f2) {
        this.g.e(f2);
    }

    public void a(float f2, float f3) {
        this.g.b(f2);
        this.g.c(f3);
    }

    public void c(float f2) {
        this.g.d(f2);
    }

    public void c(int i2) {
        this.g.a(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void a(float f2) {
        float f3 = 0.0f;
        if (f2 >= 0.4f) {
            float f4 = (f2 - 0.4f) / 0.6f;
            setAlpha((int) (255.0f * f4));
            a(true);
            a(0.0f, Math.min(0.8f, f4 * 0.8f));
            b(Math.min(1.0f, f4));
            if (f4 >= 0.8f) {
                f3 = ((f4 - 0.8f) / 0.2f) * 0.25f;
            }
            c(f3);
        }
    }

    public void a(int... iArr) {
        this.g.a(iArr);
        this.g.b(0);
    }

    public void a(int i2) {
        this.s += i2;
        invalidateSelf();
    }

    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        int save = canvas.save();
        canvas.translate(0.0f, (float) this.s);
        this.r.draw(canvas);
        canvas.rotate(this.h, bounds.exactCenterX(), bounds.exactCenterY());
        this.g.a(canvas, bounds);
        canvas.restoreToCount(save);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
    }

    public void setBounds(int i2, int i3, int i4, int i5) {
        int i6 = i4 - i2;
        super.setBounds((i6 / 2) - (this.t / 2), i3, (i6 / 2) + (this.t / 2), this.t + i3);
    }

    private int d(int i2) {
        return (int) TypedValue.applyDimension(1, (float) i2, c().getResources().getDisplayMetrics());
    }

    public void setAlpha(int i2) {
        this.g.c(i2);
    }

    public int getAlpha() {
        return this.g.b();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.g.a(colorFilter);
    }

    /* access modifiers changed from: package-private */
    public void d(float f2) {
        this.h = f2;
        invalidateSelf();
    }

    public int getOpacity() {
        return -3;
    }

    public boolean isRunning() {
        ArrayList<Animation> arrayList = this.f;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            Animation animation = arrayList.get(i2);
            if (animation.hasStarted() && !animation.hasEnded()) {
                return true;
            }
        }
        return false;
    }

    public void start() {
        this.k.reset();
        this.g.j();
        if (this.g.g() != this.g.d()) {
            this.j.startAnimation(this.o);
            return;
        }
        this.g.b(0);
        this.g.k();
        this.j.startAnimation(this.k);
    }

    public void stop() {
        this.j.clearAnimation();
        d(0.0f);
        this.g.a(false);
        this.g.b(0);
        this.g.k();
    }

    private void f() {
        final c cVar = this.g;
        AnonymousClass1 r1 = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                cVar.b(cVar.e() + ((cVar.f() - cVar.e()) * f));
                cVar.d(((((float) (Math.floor((double) (cVar.i() / 0.8f)) + 1.0d)) - cVar.i()) * f) + cVar.i());
                cVar.e(1.0f - f);
            }
        };
        r1.setInterpolator(d);
        r1.setDuration(666);
        r1.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                cVar.a();
                cVar.j();
                cVar.a(false);
                b.this.j.startAnimation(b.this.k);
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        AnonymousClass3 r2 = new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                float f2 = cVar.f();
                float e = cVar.e();
                float i = cVar.i();
                cVar.c(((0.8f - ((float) Math.toRadians(((double) cVar.c()) / (6.283185307179586d * cVar.h())))) * b.c.getInterpolation(f)) + f2);
                cVar.b((b.b.getInterpolation(f) * 0.8f) + e);
                cVar.d((0.25f * f) + i);
                b.this.d((144.0f * f) + (720.0f * (b.this.l / 5.0f)));
            }
        };
        r2.setRepeatCount(-1);
        r2.setRepeatMode(1);
        r2.setInterpolator(f617a);
        r2.setDuration(1333);
        r2.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                float unused = b.this.l = 0.0f;
            }

            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
                cVar.j();
                cVar.a();
                cVar.b(cVar.g());
                float unused = b.this.l = (b.this.l + 1.0f) % 5.0f;
            }
        });
        this.o = r1;
        this.k = r2;
    }

    /* compiled from: MaterialDrawable */
    private static class c {

        /* renamed from: a  reason: collision with root package name */
        private final RectF f624a = new RectF();
        private final Paint b = new Paint();
        private final Paint c = new Paint();
        private final Drawable.Callback d;
        private float e = 0.0f;
        private float f = 0.0f;
        private float g = 0.0f;
        private float h = 5.0f;
        private float i = 2.5f;
        private int[] j;
        private int k;
        private float l;
        private float m;
        private float n;
        private boolean o;
        private Path p;
        private float q;
        private double r;
        private int s;
        private int t;
        private int u;
        private final Paint v = new Paint();
        private int w;

        public c(Drawable.Callback callback) {
            this.d = callback;
            this.b.setStrokeCap(Paint.Cap.SQUARE);
            this.b.setAntiAlias(true);
            this.b.setStyle(Paint.Style.STROKE);
            this.c.setStyle(Paint.Style.FILL);
            this.c.setAntiAlias(true);
        }

        public void a(int i2) {
            this.w = i2;
        }

        public void a(float f2, float f3) {
            this.s = (int) f2;
            this.t = (int) f3;
        }

        public void a(Canvas canvas, Rect rect) {
            RectF rectF = this.f624a;
            rectF.set(rect);
            rectF.inset(this.i, this.i);
            float f2 = (this.e + this.g) * 360.0f;
            float f3 = ((this.f + this.g) * 360.0f) - f2;
            this.b.setColor(this.j[this.k]);
            canvas.drawArc(rectF, f2, f3, false, this.b);
            a(canvas, f2, f3, rect);
            if (this.u < 255) {
                this.v.setColor(this.w);
                this.v.setAlpha(255 - this.u);
                canvas.drawCircle(rect.exactCenterX(), rect.exactCenterY(), (float) (rect.width() / 2), this.v);
            }
        }

        private void a(Canvas canvas, float f2, float f3, Rect rect) {
            if (this.o) {
                if (this.p == null) {
                    this.p = new Path();
                    this.p.setFillType(Path.FillType.EVEN_ODD);
                } else {
                    this.p.reset();
                }
                float f4 = ((float) (((int) this.i) / 2)) * this.q;
                float cos = (float) ((this.r * Math.cos(0.0d)) + ((double) rect.exactCenterX()));
                this.p.moveTo(0.0f, 0.0f);
                this.p.lineTo(((float) this.s) * this.q, 0.0f);
                this.p.lineTo((((float) this.s) * this.q) / 2.0f, ((float) this.t) * this.q);
                this.p.offset(cos - f4, (float) ((this.r * Math.sin(0.0d)) + ((double) rect.exactCenterY())));
                this.p.close();
                this.c.setColor(this.j[this.k]);
                canvas.rotate((f2 + f3) - 5.0f, rect.exactCenterX(), rect.exactCenterY());
                canvas.drawPath(this.p, this.c);
            }
        }

        public void a(int[] iArr) {
            this.j = iArr;
            b(0);
        }

        public void b(int i2) {
            this.k = i2;
        }

        public void a() {
            this.k = (this.k + 1) % this.j.length;
        }

        public void a(ColorFilter colorFilter) {
            this.b.setColorFilter(colorFilter);
            l();
        }

        public void c(int i2) {
            this.u = i2;
        }

        public int b() {
            return this.u;
        }

        public void a(float f2) {
            this.h = f2;
            this.b.setStrokeWidth(f2);
            l();
        }

        public float c() {
            return this.h;
        }

        public void b(float f2) {
            this.e = f2;
            l();
        }

        public float d() {
            return this.e;
        }

        public float e() {
            return this.l;
        }

        public float f() {
            return this.m;
        }

        public void c(float f2) {
            this.f = f2;
            l();
        }

        public float g() {
            return this.f;
        }

        public void d(float f2) {
            this.g = f2;
            l();
        }

        public void a(int i2, int i3) {
            float f2;
            float min = (float) Math.min(i2, i3);
            if (this.r <= 0.0d || min < 0.0f) {
                f2 = (float) Math.ceil((double) (this.h / 2.0f));
            } else {
                f2 = (float) (((double) (min / 2.0f)) - this.r);
            }
            this.i = f2;
        }

        public void a(double d2) {
            this.r = d2;
        }

        public double h() {
            return this.r;
        }

        public void a(boolean z) {
            if (this.o != z) {
                this.o = z;
                l();
            }
        }

        public void e(float f2) {
            if (f2 != this.q) {
                this.q = f2;
                l();
            }
        }

        public float i() {
            return this.n;
        }

        public void j() {
            this.l = this.e;
            this.m = this.f;
            this.n = this.g;
        }

        public void k() {
            this.l = 0.0f;
            this.m = 0.0f;
            this.n = 0.0f;
            b(0.0f);
            c(0.0f);
            d(0.0f);
        }

        private void l() {
            this.d.invalidateDrawable(null);
        }
    }

    /* compiled from: MaterialDrawable */
    private static class a extends AccelerateDecelerateInterpolator {
        private a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(long, long):long}
          ClspMth{java.lang.Math.max(float, float):float} */
        public float getInterpolation(float f) {
            return super.getInterpolation(Math.max(0.0f, (f - 0.5f) * 2.0f));
        }
    }

    /* compiled from: MaterialDrawable */
    private static class d extends AccelerateDecelerateInterpolator {
        private d() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        public float getInterpolation(float f) {
            return super.getInterpolation(Math.min(1.0f, 2.0f * f));
        }
    }
}
