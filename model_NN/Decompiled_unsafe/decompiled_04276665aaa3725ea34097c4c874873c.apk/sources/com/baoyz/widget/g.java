package com.baoyz.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import java.security.InvalidParameterException;

/* compiled from: WaterDropDrawable */
class g extends d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private static final float f628a = ((float) a.values().length);
    private static final float b = (10000.0f / f628a);
    private int c;
    private Point d;
    private Point e;
    private Point f;
    private Point g;
    private Paint h = new Paint();
    private Path i;
    private int j;
    private int k;
    private int l;
    private int[] m;
    private a n;
    private Handler o = new Handler();
    private boolean p;

    /* compiled from: WaterDropDrawable */
    private enum a {
        ONE,
        TWO,
        TREE,
        FOUR
    }

    public g(Context context, PullRefreshLayout pullRefreshLayout) {
        super(context, pullRefreshLayout);
        this.h.setColor(-16776961);
        this.h.setStyle(Paint.Style.FILL);
        this.h.setAntiAlias(true);
        this.i = new Path();
        this.d = new Point();
        this.e = new Point();
        this.f = new Point();
        this.g = new Point();
    }

    public void draw(Canvas canvas) {
        canvas.save();
        canvas.translate(0.0f, this.l > 0 ? (float) this.l : 0.0f);
        this.i.reset();
        this.i.moveTo((float) this.d.x, (float) this.d.y);
        this.i.cubicTo((float) this.f.x, (float) this.f.y, (float) this.g.x, (float) this.g.y, (float) this.e.x, (float) this.e.y);
        canvas.drawPath(this.i, this.h);
        canvas.restore();
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.k = rect.width();
        a();
        super.onBoundsChange(rect);
    }

    private void a() {
        int i2 = this.j;
        int i3 = this.k;
        if (i2 > d().getFinalOffset()) {
            i2 = d().getFinalOffset();
        }
        int finalOffset = (int) ((((float) i2) / ((float) d().getFinalOffset())) * ((float) (i3 / 2)));
        this.d.set(finalOffset, 0);
        this.e.set(i3 - finalOffset, 0);
        this.f.set((i3 / 2) - i2, i2);
        this.g.set((i3 / 2) + i2, i2);
    }

    public void a(int[] iArr) {
        if (iArr == null || iArr.length < 4) {
            throw new InvalidParameterException("The color scheme length must be 4");
        }
        this.h.setColor(iArr[0]);
        this.m = iArr;
    }

    public void a(float f2) {
        this.h.setColor(a(f2, this.m[0], this.m[1]));
    }

    private void b(int i2) {
        int i3 = (int) (((float) (((float) i2) == 10000.0f ? 0 : i2)) / b);
        this.n = a.values()[i3];
        this.h.setColor(a(((float) (i2 % 2500)) / 2500.0f, this.m[i3], this.m[(i3 + 1) % a.values().length]));
    }

    public void a(int i2) {
        this.j += i2;
        this.l = this.j - d().getFinalOffset();
        a();
        invalidateSelf();
    }

    public void start() {
        this.c = 2500;
        this.p = true;
        this.o.postDelayed(this, 20);
    }

    public void stop() {
        this.o.removeCallbacks(this);
    }

    public boolean isRunning() {
        return this.p;
    }

    public void run() {
        this.c += 60;
        if (((float) this.c) > 10000.0f) {
            this.c = 0;
        }
        if (this.p) {
            this.o.postDelayed(this, 20);
            b(this.c);
            invalidateSelf();
        }
    }

    private int a(float f2, int i2, int i3) {
        int i4 = (i2 >> 24) & 255;
        int i5 = (i2 >> 16) & 255;
        int i6 = (i2 >> 8) & 255;
        int i7 = i2 & 255;
        return ((i4 + ((int) (((float) (((i3 >> 24) & 255) - i4)) * f2))) << 24) | ((i5 + ((int) (((float) (((i3 >> 16) & 255) - i5)) * f2))) << 16) | ((((int) (((float) (((i3 >> 8) & 255) - i6)) * f2)) + i6) << 8) | (((int) (((float) ((i3 & 255) - i7)) * f2)) + i7);
    }
}
