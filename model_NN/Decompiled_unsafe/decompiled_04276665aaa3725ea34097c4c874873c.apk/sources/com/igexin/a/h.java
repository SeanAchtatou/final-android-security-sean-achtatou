package com.igexin.a;

import android.content.Context;
import android.util.Log;
import com.igexin.a.a.j;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class h {

    /* renamed from: a  reason: collision with root package name */
    protected final Set<String> f782a;
    protected final e b;
    protected final d c;
    protected boolean d;
    protected boolean e;
    protected g f;

    protected h() {
        this(new j(), new a());
    }

    protected h(e eVar, d dVar) {
        this.f782a = new HashSet();
        if (eVar == null) {
            throw new IllegalArgumentException("Cannot pass null library loader");
        } else if (dVar == null) {
            throw new IllegalArgumentException("Cannot pass null library installer");
        } else {
            this.b = eVar;
            this.c = dVar;
        }
    }

    private void c(Context context, String str, String str2) {
        if (!this.f782a.contains(str) || this.d) {
            try {
                this.b.a(str);
                this.f782a.add(str);
                a("%s (%s) was loaded normally!", str, str2);
            } catch (UnsatisfiedLinkError e2) {
                a("Loading the library normally failed: %s", Log.getStackTraceString(e2));
                a("%s (%s) was not loaded normally, re-linking...", str, str2);
                File a2 = a(context, str, str2);
                if (!a2.exists() || this.d) {
                    if (this.d) {
                        a("Forcing a re-link of %s (%s)...", str, str2);
                    }
                    b(context, str, str2);
                    this.c.a(context, this.b.a(), this.b.c(str), a2, this);
                }
                try {
                    if (this.e) {
                        for (String d2 : new j(a2).b()) {
                            a(context, this.b.d(d2));
                        }
                    }
                } catch (IOException e3) {
                }
                this.b.b(a2.getAbsolutePath());
                this.f782a.add(str);
                a("%s (%s) was re-linked!", str, str2);
            }
        } else {
            a("%s already loaded previously!", str);
        }
    }

    public h a() {
        this.d = true;
        return this;
    }

    public h a(g gVar) {
        this.f = gVar;
        return this;
    }

    /* access modifiers changed from: protected */
    public File a(Context context) {
        return context.getDir("lib", 0);
    }

    /* access modifiers changed from: protected */
    public File a(Context context, String str, String str2) {
        String c2 = this.b.c(str);
        return k.a(str2) ? new File(a(context), c2) : new File(a(context), c2 + "." + str2);
    }

    public void a(Context context, String str) {
        a(context, str, null, null);
    }

    public void a(Context context, String str, String str2, f fVar) {
        if (context == null) {
            throw new IllegalArgumentException("Given context is null");
        } else if (k.a(str)) {
            throw new IllegalArgumentException("Given library is either null or empty");
        } else {
            a("Beginning load of %s...", str);
            if (fVar == null) {
                c(context, str, str2);
                return;
            }
            try {
                c(context, str, str2);
                fVar.a();
            } catch (UnsatisfiedLinkError e2) {
                fVar.a(e2);
            }
        }
    }

    public void a(String str) {
        if (this.f != null) {
            this.f.a(str);
        }
    }

    public void a(String str, Object... objArr) {
        a(String.format(Locale.US, str, objArr));
    }

    public h b() {
        this.e = true;
        return this;
    }

    /* access modifiers changed from: protected */
    public void b(Context context, String str, String str2) {
        File a2 = a(context);
        File a3 = a(context, str, str2);
        File[] listFiles = a2.listFiles(new i(this, this.b.c(str)));
        if (listFiles != null) {
            for (File file : listFiles) {
                if (this.d || !file.getAbsolutePath().equals(a3.getAbsolutePath())) {
                    file.delete();
                }
            }
        }
    }
}
