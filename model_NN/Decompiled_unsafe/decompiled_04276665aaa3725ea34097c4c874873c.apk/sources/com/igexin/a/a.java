package com.igexin.a;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class a implements d {
    private long a(InputStream inputStream, OutputStream outputStream) {
        long j = 0;
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                outputStream.flush();
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }

    private void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007a, code lost:
        if (r2 == null) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0081, code lost:
        throw new com.igexin.a.b(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0082, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0083, code lost:
        r3 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0094, code lost:
        throw new com.igexin.a.b(r16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0103, code lost:
        r3 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0110, code lost:
        r4 = null;
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x011b, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x011c, code lost:
        r12 = r4;
        r4 = null;
        r2 = r12;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:10:0x001e, B:53:0x00b7] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0086 A[SYNTHETIC, Splitter:B:36:0x0086] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.content.Context r14, java.lang.String[] r15, java.lang.String r16, java.io.File r17, com.igexin.a.h r18) {
        /*
            r13 = this;
            r3 = 0
            android.content.pm.ApplicationInfo r6 = r14.getApplicationInfo()     // Catch:{ all -> 0x013d }
            r2 = 0
        L_0x0006:
            int r4 = r2 + 1
            r5 = 5
            if (r2 >= r5) goto L_0x0157
            java.util.zip.ZipFile r5 = new java.util.zip.ZipFile     // Catch:{ IOException -> 0x0027 }
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x0027 }
            java.lang.String r7 = r6.sourceDir     // Catch:{ IOException -> 0x0027 }
            r2.<init>(r7)     // Catch:{ IOException -> 0x0027 }
            r7 = 1
            r5.<init>(r2, r7)     // Catch:{ IOException -> 0x0027 }
        L_0x0018:
            if (r5 != 0) goto L_0x002a
            java.lang.String r2 = "FATAL! Couldn't find application APK!"
            r0 = r18
            r0.a(r2)     // Catch:{ all -> 0x0082 }
            if (r5 == 0) goto L_0x0026
            r5.close()     // Catch:{ IOException -> 0x0137 }
        L_0x0026:
            return
        L_0x0027:
            r2 = move-exception
            r2 = r4
            goto L_0x0006
        L_0x002a:
            r2 = 0
        L_0x002b:
            int r6 = r2 + 1
            r3 = 5
            if (r2 >= r3) goto L_0x0126
            r4 = 0
            r3 = 0
            int r7 = r15.length     // Catch:{ all -> 0x0082 }
            r2 = 0
            r12 = r2
            r2 = r3
            r3 = r4
            r4 = r12
        L_0x0038:
            if (r4 >= r7) goto L_0x0153
            r2 = r15[r4]     // Catch:{ all -> 0x0082 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0082 }
            r3.<init>()     // Catch:{ all -> 0x0082 }
            java.lang.String r8 = "lib"
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ all -> 0x0082 }
            char r8 = java.io.File.separatorChar     // Catch:{ all -> 0x0082 }
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ all -> 0x0082 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ all -> 0x0082 }
            char r3 = java.io.File.separatorChar     // Catch:{ all -> 0x0082 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0082 }
            r0 = r16
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0082 }
            java.lang.String r3 = r2.toString()     // Catch:{ all -> 0x0082 }
            java.util.zip.ZipEntry r2 = r5.getEntry(r3)     // Catch:{ all -> 0x0082 }
            if (r2 == 0) goto L_0x008a
            r4 = r2
            r2 = r3
        L_0x0069:
            if (r2 == 0) goto L_0x0078
            java.lang.String r3 = "Looking for %s in APK..."
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0082 }
            r8 = 0
            r7[r8] = r2     // Catch:{ all -> 0x0082 }
            r0 = r18
            r0.a(r3, r7)     // Catch:{ all -> 0x0082 }
        L_0x0078:
            if (r4 != 0) goto L_0x0095
            if (r2 == 0) goto L_0x008d
            com.igexin.a.b r3 = new com.igexin.a.b     // Catch:{ all -> 0x0082 }
            r3.<init>(r2)     // Catch:{ all -> 0x0082 }
            throw r3     // Catch:{ all -> 0x0082 }
        L_0x0082:
            r2 = move-exception
            r3 = r5
        L_0x0084:
            if (r3 == 0) goto L_0x0089
            r3.close()     // Catch:{ IOException -> 0x013a }
        L_0x0089:
            throw r2
        L_0x008a:
            int r4 = r4 + 1
            goto L_0x0038
        L_0x008d:
            com.igexin.a.b r2 = new com.igexin.a.b     // Catch:{ all -> 0x0082 }
            r0 = r16
            r2.<init>(r0)     // Catch:{ all -> 0x0082 }
            throw r2     // Catch:{ all -> 0x0082 }
        L_0x0095:
            java.lang.String r3 = "Found %s! Extracting..."
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ all -> 0x0082 }
            r8 = 0
            r7[r8] = r2     // Catch:{ all -> 0x0082 }
            r0 = r18
            r0.a(r3, r7)     // Catch:{ all -> 0x0082 }
            boolean r2 = r17.exists()     // Catch:{ IOException -> 0x00b1 }
            if (r2 != 0) goto L_0x00b5
            boolean r2 = r17.createNewFile()     // Catch:{ IOException -> 0x00b1 }
            if (r2 != 0) goto L_0x00b5
            r2 = r6
            goto L_0x002b
        L_0x00b1:
            r2 = move-exception
            r2 = r6
            goto L_0x002b
        L_0x00b5:
            r2 = 0
            r3 = 0
            java.io.InputStream r4 = r5.getInputStream(r4)     // Catch:{ FileNotFoundException -> 0x0102, IOException -> 0x010f, all -> 0x011b }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x014c, IOException -> 0x0147, all -> 0x0140 }
            r0 = r17
            r2.<init>(r0)     // Catch:{ FileNotFoundException -> 0x014c, IOException -> 0x0147, all -> 0x0140 }
            long r8 = r13.a(r4, r2)     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x014a, all -> 0x0142 }
            java.io.FileDescriptor r3 = r2.getFD()     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x014a, all -> 0x0142 }
            r3.sync()     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x014a, all -> 0x0142 }
            long r10 = r17.length()     // Catch:{ FileNotFoundException -> 0x0150, IOException -> 0x014a, all -> 0x0142 }
            int r3 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r3 == 0) goto L_0x00de
            r13.a(r4)     // Catch:{ all -> 0x0082 }
            r13.a(r2)     // Catch:{ all -> 0x0082 }
            r2 = r6
            goto L_0x002b
        L_0x00de:
            r13.a(r4)     // Catch:{ all -> 0x0082 }
            r13.a(r2)     // Catch:{ all -> 0x0082 }
            r2 = 1
            r3 = 0
            r0 = r17
            r0.setReadable(r2, r3)     // Catch:{ all -> 0x0082 }
            r2 = 1
            r3 = 0
            r0 = r17
            r0.setExecutable(r2, r3)     // Catch:{ all -> 0x0082 }
            r2 = 1
            r0 = r17
            r0.setWritable(r2)     // Catch:{ all -> 0x0082 }
            if (r5 == 0) goto L_0x0026
            r5.close()     // Catch:{ IOException -> 0x00ff }
            goto L_0x0026
        L_0x00ff:
            r2 = move-exception
            goto L_0x0026
        L_0x0102:
            r4 = move-exception
            r12 = r3
            r3 = r2
            r2 = r12
        L_0x0106:
            r13.a(r3)     // Catch:{ all -> 0x0082 }
            r13.a(r2)     // Catch:{ all -> 0x0082 }
            r2 = r6
            goto L_0x002b
        L_0x010f:
            r4 = move-exception
            r4 = r2
            r2 = r3
        L_0x0112:
            r13.a(r4)     // Catch:{ all -> 0x0082 }
            r13.a(r2)     // Catch:{ all -> 0x0082 }
            r2 = r6
            goto L_0x002b
        L_0x011b:
            r4 = move-exception
            r12 = r4
            r4 = r2
            r2 = r12
        L_0x011f:
            r13.a(r4)     // Catch:{ all -> 0x0082 }
            r13.a(r3)     // Catch:{ all -> 0x0082 }
            throw r2     // Catch:{ all -> 0x0082 }
        L_0x0126:
            java.lang.String r2 = "FATAL! Couldn't extract the library from the APK!"
            r0 = r18
            r0.a(r2)     // Catch:{ all -> 0x0082 }
            if (r5 == 0) goto L_0x0026
            r5.close()     // Catch:{ IOException -> 0x0134 }
            goto L_0x0026
        L_0x0134:
            r2 = move-exception
            goto L_0x0026
        L_0x0137:
            r2 = move-exception
            goto L_0x0026
        L_0x013a:
            r3 = move-exception
            goto L_0x0089
        L_0x013d:
            r2 = move-exception
            goto L_0x0084
        L_0x0140:
            r2 = move-exception
            goto L_0x011f
        L_0x0142:
            r3 = move-exception
            r12 = r3
            r3 = r2
            r2 = r12
            goto L_0x011f
        L_0x0147:
            r2 = move-exception
            r2 = r3
            goto L_0x0112
        L_0x014a:
            r3 = move-exception
            goto L_0x0112
        L_0x014c:
            r2 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x0106
        L_0x0150:
            r3 = move-exception
            r3 = r4
            goto L_0x0106
        L_0x0153:
            r4 = r2
            r2 = r3
            goto L_0x0069
        L_0x0157:
            r5 = r3
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.a.a.a(android.content.Context, java.lang.String[], java.lang.String, java.io.File, com.igexin.a.h):void");
    }
}
