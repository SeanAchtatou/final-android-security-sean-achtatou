package com.igexin.push.core.a;

import android.text.TextUtils;
import com.igexin.b.a.b.c;
import com.igexin.b.a.c.a;
import com.igexin.b.a.d.d;
import com.igexin.push.c.i;
import com.igexin.push.config.SDKUrlConfig;
import com.igexin.push.core.b.ae;
import com.igexin.push.core.f;
import com.igexin.push.core.g;
import com.igexin.push.d.b;
import com.igexin.push.util.e;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.tencent.connect.common.Constants;

public class m extends a {
    private void b() {
        b.a().f();
        a.b("loginRsp|" + g.r + "|success");
        a.b("isCidBroadcasted|" + g.m);
        if (!g.m) {
            e.a().k();
            g.m = true;
        }
        g.l = true;
        e.a().l();
        e.a().h();
        if (TextUtils.isEmpty(g.y)) {
            a.b("LoginResultAction device id is empty, get device id from server +++++");
            e.a().i();
        }
        e.f();
        e();
        a();
        d();
        com.igexin.push.core.b.g.a().b();
        f.a().h().a();
        f();
    }

    private void c() {
        a.b("loginRsp|" + g.r + "|failed");
        a.b("LoginResultAction login failed, clear session or cid");
        com.igexin.push.core.b.g.a().c();
        e.a().c(true);
    }

    private void d() {
        try {
            if ((System.currentTimeMillis() - g.K) - LogBuilder.MAX_INTERVAL > 0) {
                c.b().a(new com.igexin.push.f.a.c(new com.igexin.push.core.c.g(SDKUrlConfig.getConfigServiceUrl())), false, true);
            }
        } catch (Exception e) {
        }
    }

    private void e() {
        if ((System.currentTimeMillis() - g.H) - 259200000 >= 0 && com.igexin.push.config.m.h) {
            String d = e.a().d(Parameters.USERAGENT);
            if (d == null || "1".equals(d)) {
                c.b().a(new o(this), false, true);
            }
        }
    }

    private void f() {
        try {
            if (System.currentTimeMillis() - g.M > LogBuilder.MAX_INTERVAL) {
                com.igexin.push.core.b.g.a().g(System.currentTimeMillis());
                ae.a().b(Constants.VIA_REPORT_TYPE_QQFAVORITES);
            }
        } catch (Throwable th) {
            a.b("LoginResultAction|report third party guard exception :" + th.toString());
        }
    }

    public void a() {
        boolean z = true;
        long currentTimeMillis = System.currentTimeMillis() - g.G;
        boolean z2 = !com.igexin.b.b.a.a(g.A, g.z);
        if (currentTimeMillis - LogBuilder.MAX_INTERVAL <= 0) {
            z = false;
        }
        if (z || z2) {
            g.G = System.currentTimeMillis();
            if (TextUtils.isEmpty(g.y)) {
                if (g.av != null) {
                    g.av.u();
                    g.av = null;
                }
                g.av = new n(this, 5000);
                f.a().a(g.av);
            } else {
                e.a().j();
            }
            if (z2) {
                com.igexin.push.core.b.g.a().d(g.z);
            }
        }
    }

    public boolean a(d dVar) {
        return false;
    }

    public boolean a(Object obj) {
        if (!(obj instanceof com.igexin.push.d.c.m)) {
            return true;
        }
        g.E = 0;
        if (g.l) {
            return true;
        }
        i.a().e().h();
        if (((com.igexin.push.d.c.m) obj).f998a) {
            b();
            return true;
        }
        c();
        return true;
    }
}
