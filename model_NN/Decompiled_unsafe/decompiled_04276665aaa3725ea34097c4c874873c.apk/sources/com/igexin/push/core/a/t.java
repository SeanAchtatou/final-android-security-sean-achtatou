package com.igexin.push.core.a;

import android.text.TextUtils;
import com.igexin.b.a.c.a;
import com.igexin.b.a.d.d;
import com.igexin.push.config.l;
import com.igexin.push.core.f;
import com.igexin.push.core.g;
import com.igexin.push.d.c.e;
import com.igexin.push.d.c.k;
import com.igexin.push.d.c.q;

public class t extends a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f907a = (l.f884a + "_RegisterResultAction");

    public boolean a(d dVar) {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int
     arg types: [java.lang.String, com.igexin.push.d.c.k, int]
     candidates:
      com.igexin.push.e.j.a(android.content.Context, com.igexin.b.a.b.c, com.igexin.push.e.k):void
      com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int */
    public boolean a(Object obj) {
        boolean z = false;
        if (obj instanceof q) {
            q qVar = (q) obj;
            g.E = 0;
            a.b("register resp |" + qVar.f1002a + "|" + g.q);
            a.b("register resp cid = " + qVar.c + " device id = " + qVar.d);
            if (qVar.f1002a != g.q) {
                g.m = false;
                a.b(f907a + " change session : from [" + g.q + "] to [" + qVar.f1002a + "]");
                a.b(f907a + " change cid : from [" + g.r + "] to [" + qVar.c + "]");
                if (TextUtils.isEmpty(qVar.c) || TextUtils.isEmpty(qVar.d)) {
                    com.igexin.push.core.b.g.a().b(qVar.f1002a);
                } else {
                    com.igexin.push.core.b.g.a().a(qVar.c, qVar.d, qVar.f1002a);
                }
                g.G = 0;
                z = true;
            }
            a.b("loginReqAfterRegister|new session:" + g.q + ", cid :" + g.r + ", devId :" + g.y);
            k c = e.a().c();
            f.a().g().a("S-" + c.f996a, (e) c, true);
            if (z) {
            }
        }
        return true;
    }
}
