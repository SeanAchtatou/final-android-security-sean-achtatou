package com.igexin.push.core.b;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.igexin.b.a.b.c;
import com.igexin.b.a.c.a;
import com.igexin.download.Downloads;
import com.igexin.push.core.bean.k;
import com.igexin.push.core.c.m;
import com.igexin.push.core.f;
import com.meizu.cloud.pushsdk.notification.model.TimeDisplaySetting;
import java.util.ArrayList;
import java.util.Iterator;

public class ae implements a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f915a = ae.class.getName();
    private static int b = Downloads.STATUS_SUCCESS;
    private static int c = 50;
    private static ae f;
    private int d;
    private ArrayList<k> e = null;

    private ae() {
    }

    static /* synthetic */ int a(ae aeVar) {
        int i = aeVar.d;
        aeVar.d = i + 1;
        return i;
    }

    public static ae a() {
        if (f == null) {
            f = new ae();
        }
        return f;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0095  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.igexin.push.core.bean.k> a(java.lang.String r9) {
        /*
            r8 = this;
            r6 = 0
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            com.igexin.push.core.f r0 = com.igexin.push.core.f.a()     // Catch:{ Throwable -> 0x009b, all -> 0x0091 }
            com.igexin.push.b.b r0 = r0.k()     // Catch:{ Throwable -> 0x009b, all -> 0x0091 }
            java.lang.String r1 = "st"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x009b, all -> 0x0091 }
            r3 = 0
            java.lang.String r4 = "type"
            r2[r3] = r4     // Catch:{ Throwable -> 0x009b, all -> 0x0091 }
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Throwable -> 0x009b, all -> 0x0091 }
            r4 = 0
            r3[r4] = r9     // Catch:{ Throwable -> 0x009b, all -> 0x0091 }
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x009b, all -> 0x0091 }
            if (r1 == 0) goto L_0x008b
            int r0 = r1.getCount()     // Catch:{ Throwable -> 0x0064 }
            if (r0 <= 0) goto L_0x008b
        L_0x002c:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x0064 }
            if (r0 == 0) goto L_0x008b
            r0 = 2
            byte[] r0 = r1.getBlob(r0)     // Catch:{ Throwable -> 0x0064 }
            byte[] r0 = com.igexin.b.b.a.c(r0)     // Catch:{ Throwable -> 0x0064 }
            com.igexin.push.core.bean.k r2 = new com.igexin.push.core.bean.k     // Catch:{ Throwable -> 0x0064 }
            r2.<init>()     // Catch:{ Throwable -> 0x0064 }
            r3 = 0
            int r3 = r1.getInt(r3)     // Catch:{ Throwable -> 0x0064 }
            r2.a(r3)     // Catch:{ Throwable -> 0x0064 }
            r3 = 1
            int r3 = r1.getInt(r3)     // Catch:{ Throwable -> 0x0064 }
            r2.b(r3)     // Catch:{ Throwable -> 0x0064 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Throwable -> 0x0064 }
            r3.<init>(r0)     // Catch:{ Throwable -> 0x0064 }
            r2.a(r3)     // Catch:{ Throwable -> 0x0064 }
            r0 = 3
            long r4 = r1.getLong(r0)     // Catch:{ Throwable -> 0x0064 }
            r2.a(r4)     // Catch:{ Throwable -> 0x0064 }
            r7.add(r2)     // Catch:{ Throwable -> 0x0064 }
            goto L_0x002c
        L_0x0064:
            r0 = move-exception
        L_0x0065:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0099 }
            r2.<init>()     // Catch:{ all -> 0x0099 }
            java.lang.String r3 = com.igexin.push.core.b.ae.f915a     // Catch:{ all -> 0x0099 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0099 }
            java.lang.String r3 = "|getThirdGuardData exception:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0099 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0099 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0099 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0099 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x0099 }
            if (r1 == 0) goto L_0x008a
            r1.close()
        L_0x008a:
            return r7
        L_0x008b:
            if (r1 == 0) goto L_0x008a
            r1.close()
            goto L_0x008a
        L_0x0091:
            r0 = move-exception
            r1 = r6
        L_0x0093:
            if (r1 == 0) goto L_0x0098
            r1.close()
        L_0x0098:
            throw r0
        L_0x0099:
            r0 = move-exception
            goto L_0x0093
        L_0x009b:
            r0 = move-exception
            r1 = r6
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.b.ae.a(java.lang.String):java.util.ArrayList");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.database.sqlite.SQLiteDatabase r8) {
        /*
            r7 = this;
            r6 = 0
            com.igexin.push.core.f r0 = com.igexin.push.core.f.a()     // Catch:{ Throwable -> 0x0021, all -> 0x0029 }
            com.igexin.push.b.b r0 = r0.k()     // Catch:{ Throwable -> 0x0021, all -> 0x0029 }
            java.lang.String r1 = "st"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x0021, all -> 0x0029 }
            if (r0 == 0) goto L_0x001b
            int r1 = r0.getCount()     // Catch:{ Throwable -> 0x0034, all -> 0x0030 }
            r7.d = r1     // Catch:{ Throwable -> 0x0034, all -> 0x0030 }
        L_0x001b:
            if (r0 == 0) goto L_0x0020
            r0.close()
        L_0x0020:
            return
        L_0x0021:
            r0 = move-exception
            r0 = r6
        L_0x0023:
            if (r0 == 0) goto L_0x0020
            r0.close()
            goto L_0x0020
        L_0x0029:
            r0 = move-exception
        L_0x002a:
            if (r6 == 0) goto L_0x002f
            r6.close()
        L_0x002f:
            throw r0
        L_0x0030:
            r1 = move-exception
            r6 = r0
            r0 = r1
            goto L_0x002a
        L_0x0034:
            r1 = move-exception
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.b.ae.a(android.database.sqlite.SQLiteDatabase):void");
    }

    public void a(String str, String str2) {
        if (this.d >= b) {
            a.b(f915a + "|rowCount >= 200 can not insert");
            return;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("type", str);
        contentValues.put("value", com.igexin.b.b.a.b(str2.getBytes()));
        contentValues.put("time", Long.valueOf(System.currentTimeMillis()));
        c.b().a(new af(this, contentValues), false, true);
    }

    public void a(String str, ArrayList<k> arrayList) {
        try {
            String[] strArr = new String[arrayList.size()];
            Iterator<k> it = arrayList.iterator();
            int i = 0;
            while (it.hasNext()) {
                k next = it.next();
                strArr[i] = String.valueOf(next.a());
                this.e.remove(next);
                i++;
            }
            f.a().k().a(TimeDisplaySetting.START_SHOW_TIME, new String[]{"id"}, strArr);
            if (this.e.size() > 0) {
                c(str);
            }
        } catch (Throwable th) {
            a.b(f915a + "|onReportResult exception:" + th.toString());
        }
    }

    public void b(SQLiteDatabase sQLiteDatabase) {
    }

    public void b(String str) {
        this.e = a(str);
        c(str);
    }

    public void c(SQLiteDatabase sQLiteDatabase) {
    }

    public void c(String str) {
        try {
            ArrayList arrayList = new ArrayList();
            StringBuilder sb = new StringBuilder();
            Iterator<k> it = this.e.iterator();
            while (it.hasNext()) {
                k next = it.next();
                if (arrayList.size() < c) {
                    arrayList.add(next);
                    sb.append(next.b());
                    sb.append("\n");
                }
            }
            String sb2 = sb.toString();
            if (!TextUtils.isEmpty(sb2)) {
                c.b().a(new com.igexin.push.f.a.c(new m(sb2.getBytes(), str, arrayList)), false, true);
            }
        } catch (Throwable th) {
            a.b(f915a + "|doSTReport exception:" + th.toString());
        }
    }
}
