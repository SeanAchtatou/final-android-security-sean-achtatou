package com.igexin.push.core.c;

import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.g;
import com.igexin.push.d.c.a;
import java.util.TimerTask;

class e extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PushTaskBean f962a;
    final /* synthetic */ a b;
    final /* synthetic */ c c;

    e(c cVar, PushTaskBean pushTaskBean, a aVar) {
        this.c = cVar;
        this.f962a = pushTaskBean;
        this.b = aVar;
    }

    public void run() {
        if (g.ak.containsKey(this.f962a.getTaskId())) {
            g.ak.get(this.f962a.getTaskId()).cancel();
            g.ak.remove(this.f962a.getTaskId());
        }
        this.c.a(this.f962a, this.b);
        this.b.b(this.b.c() + 1);
    }
}
