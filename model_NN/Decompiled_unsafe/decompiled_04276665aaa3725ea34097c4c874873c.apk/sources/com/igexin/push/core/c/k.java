package com.igexin.push.core.c;

import android.content.ContentValues;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.igexin.b.a.b.f;
import com.igexin.push.core.g;
import com.igexin.push.f.a.b;
import com.igexin.push.util.e;
import com.meizu.cloud.pushsdk.notification.model.NotificationStyle;
import org.json.JSONObject;

public class k extends b {

    /* renamed from: a  reason: collision with root package name */
    public boolean f967a;
    private boolean g = false;
    private int h;

    public k(String str, byte[] bArr, int i, boolean z) {
        super(str);
        this.g = z;
        this.h = i;
        a(bArr, i);
    }

    private void a(byte[] bArr, int i) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("action", "upload_BI");
            jSONObject.put("BIType", String.valueOf(i));
            jSONObject.put(IXAdRequestInfo.CELL_ID, g.r);
            jSONObject.put("BIData", new String(f.f(bArr, 0), "UTF-8"));
            b(jSONObject.toString().getBytes());
        } catch (Exception e) {
        }
    }

    public void a(byte[] bArr) {
        JSONObject jSONObject = new JSONObject(new String(bArr));
        if (jSONObject.has(SocketMessage.MSG_RESULE_KEY) && "ok".equals(jSONObject.getString(SocketMessage.MSG_RESULE_KEY))) {
            this.f967a = true;
            if (this.h == 10) {
                e.g();
            }
            if (this.g) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("type", "0");
                com.igexin.push.core.f.a().k().a(NotificationStyle.BANNER_IMAGE_URL, contentValues, new String[]{"type"}, new String[]{"2"});
                com.igexin.push.core.b.g.a().c(System.currentTimeMillis());
            }
        }
    }

    public int b() {
        return 0;
    }
}
