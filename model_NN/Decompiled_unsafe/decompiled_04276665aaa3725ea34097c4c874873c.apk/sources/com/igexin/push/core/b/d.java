package com.igexin.push.core.b;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.igexin.b.a.b.c;
import com.igexin.b.b.a;
import com.igexin.push.core.bean.j;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class d implements a {

    /* renamed from: a  reason: collision with root package name */
    private static d f919a;
    private List<j> b = new CopyOnWriteArrayList();

    private d() {
    }

    private int a(int i) {
        int i2 = 0;
        Iterator<j> it = this.b.iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                return i3;
            }
            i2 = it.next().c() == i ? i3 + 1 : i3;
        }
    }

    public static d a() {
        if (f919a == null) {
            f919a = new d();
        }
        return f919a;
    }

    private static ContentValues b(j jVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", Long.valueOf(jVar.a()));
        contentValues.put("data", a.b(jVar.b().getBytes()));
        contentValues.put("type", Byte.valueOf(jVar.c()));
        contentValues.put("time", Long.valueOf(jVar.d()));
        return contentValues;
    }

    private j b(long j) {
        for (j next : this.b) {
            if (next.a() == j) {
                return next;
            }
        }
        return null;
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
    }

    public void a(j jVar) {
        if (this.b.size() < 107 && jVar != null) {
            switch (jVar.c()) {
                case 1:
                    if (a(1) >= 1) {
                        return;
                    }
                    break;
                case 2:
                    if (a(2) >= 3) {
                        return;
                    }
                    break;
                case 3:
                    if (a(3) >= 90) {
                        return;
                    }
                    break;
                case 5:
                    if (a(5) >= 3) {
                        return;
                    }
                    break;
                case 6:
                    if (a(6) >= 10) {
                        return;
                    }
                    break;
            }
            this.b.add(jVar);
            c.b().a(new e(this, b(jVar)), true, false);
        }
    }

    public boolean a(long j) {
        j b2 = b(j);
        if (b2 == null) {
            return false;
        }
        this.b.remove(b2);
        c.b().a(new f(this, b(b2), j), true, false);
        return true;
    }

    public List<j> b() {
        return this.b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x004a, code lost:
        if (r0 != null) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x004c, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005c, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0060, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0061, code lost:
        r12 = r1;
        r1 = r0;
        r0 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0049 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0004] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(android.database.sqlite.SQLiteDatabase r14) {
        /*
            r13 = this;
            r0 = 0
            java.lang.String r1 = "select id,data,type,time from ral"
            r2 = 0
            android.database.Cursor r0 = r14.rawQuery(r1, r2)     // Catch:{ Exception -> 0x0049, all -> 0x0056 }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            if (r0 == 0) goto L_0x0050
        L_0x000e:
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            if (r1 == 0) goto L_0x0050
            r1 = 0
            long r2 = r0.getLong(r1)     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            r1 = 2
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            byte r5 = (byte) r1     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            r1 = 3
            long r6 = r0.getLong(r1)     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            java.util.List<com.igexin.push.core.bean.j> r10 = r13.b     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            com.igexin.push.core.bean.j r1 = new com.igexin.push.core.bean.j     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            r11 = 1
            byte[] r11 = r0.getBlob(r11)     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            byte[] r11 = com.igexin.b.b.a.c(r11)     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            r4.<init>(r11)     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            r1.<init>(r2, r4, r5, r6)     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            r10.add(r1)     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            long r4 = r8 - r6
            r6 = 259200000(0xf731400, double:1.280618154E-315)
            int r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r1 <= 0) goto L_0x000e
            r13.a(r2)     // Catch:{ Exception -> 0x0049, all -> 0x0060 }
            goto L_0x000e
        L_0x0049:
            r1 = move-exception
            if (r0 == 0) goto L_0x004f
            r0.close()
        L_0x004f:
            return
        L_0x0050:
            if (r0 == 0) goto L_0x004f
            r0.close()
            goto L_0x004f
        L_0x0056:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x005a:
            if (r1 == 0) goto L_0x005f
            r1.close()
        L_0x005f:
            throw r0
        L_0x0060:
            r1 = move-exception
            r12 = r1
            r1 = r0
            r0 = r12
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.b.d.b(android.database.sqlite.SQLiteDatabase):void");
    }

    public void c(SQLiteDatabase sQLiteDatabase) {
    }
}
