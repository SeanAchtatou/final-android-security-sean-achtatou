package com.igexin.push.core;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.igexin.assist.sdk.b;
import com.igexin.push.config.m;
import com.igexin.push.core.bean.g;
import com.igexin.push.util.a;
import com.igexin.sdk.PushConsts;

public class e extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private static String f972a = e.class.getName();

    private void a() {
        g.J = System.currentTimeMillis();
        boolean z = true;
        if (a.a(System.currentTimeMillis()) && "1".equals(com.igexin.push.core.a.e.a().d("ccs"))) {
            z = false;
        }
        if (z) {
            com.igexin.push.core.a.e.a().x();
        }
        if (g.E > m.y) {
            long j = m.y / 10;
            long random = ((long) (((Math.random() * ((double) j)) * 2.0d) - ((double) j))) + m.y;
            com.igexin.b.a.c.a.b(f972a + "|userPresent, reConnectDelayTime > " + m.y + ", resetDelay = " + random);
            g.E = random;
        }
    }

    private void a(Intent intent) {
        String stringExtra = intent.getStringExtra("action");
        if (stringExtra.equals(PushConsts.ACTION_SERVICE_INITIALIZE)) {
            com.igexin.push.core.a.e.a().a(intent);
        } else if (stringExtra.equals(PushConsts.ACTION_SERVICE_INITIALIZE_SLAVE)) {
            com.igexin.push.core.a.e.a().b(intent);
            b.a().c(g.f);
        } else if (stringExtra.equals(PushConsts.ACTION_BROADCAST_PUSHMANAGER)) {
            com.igexin.push.core.a.e.a().a(intent.getBundleExtra("bundle"));
        } else if (stringExtra.equals(PushConsts.ACTION_BROADCAST_USER_PRESENT)) {
            a();
        } else if (stringExtra.equals("com.igexin.sdk.action.extdownloadsuccess")) {
            com.igexin.push.core.a.e.a().d(intent);
        }
    }

    public void handleMessage(Message message) {
        try {
            if (message.obj != null) {
                if (message.what == a.c) {
                    Intent intent = (Intent) message.obj;
                    if (intent.hasExtra("action")) {
                        a(intent);
                    }
                } else if (message.what == a.d) {
                    com.igexin.push.core.a.e.a().c((Intent) message.obj);
                } else if (message.what == a.e) {
                    com.igexin.push.core.a.e.a().c((Intent) message.obj);
                } else if (message.what == a.g) {
                    Bundle bundle = (Bundle) message.obj;
                    String string = bundle.getString("taskid");
                    String string2 = bundle.getString("messageid");
                    String string3 = bundle.getString("actionid");
                    com.igexin.b.a.c.a.b(f972a + "|hand execute_action taskid = " + string + ", actionid = " + string3);
                    com.igexin.push.core.a.e.a().b(string, string2, string3);
                } else if (message.what == a.h) {
                    a.a((g) message.obj);
                } else if (message.what == a.j) {
                    com.igexin.push.core.a.e.a().e((Intent) message.obj);
                } else if (message.what != a.f) {
                }
            } else if (message.what == a.i) {
                com.igexin.push.core.a.e.a().s();
            }
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b(f972a + "|" + th.toString());
        }
    }
}
