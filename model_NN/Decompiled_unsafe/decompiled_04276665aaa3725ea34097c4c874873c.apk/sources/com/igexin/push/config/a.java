package com.igexin.push.config;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import com.igexin.b.a.b.c;

public class a implements com.igexin.push.core.b.a {

    /* renamed from: a  reason: collision with root package name */
    public static final String f873a = a.class.getName();
    private static a b;

    public static a a() {
        if (b == null) {
            b = new a();
        }
        return b;
    }

    /* access modifiers changed from: private */
    public void a(SQLiteDatabase sQLiteDatabase, int i) {
        sQLiteDatabase.delete("config", "id = ?", new String[]{String.valueOf(i)});
    }

    /* access modifiers changed from: private */
    public void a(SQLiteDatabase sQLiteDatabase, int i, String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", Integer.valueOf(i));
        contentValues.put("value", str);
        sQLiteDatabase.replace("config", null, contentValues);
    }

    /* access modifiers changed from: private */
    public void a(SQLiteDatabase sQLiteDatabase, int i, byte[] bArr) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", Integer.valueOf(i));
        contentValues.put("value", bArr);
        sQLiteDatabase.replace("config", null, contentValues);
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
    }

    public void a(String str) {
        c.b().a(new i(this, str), true, false);
    }

    public void b() {
        c.b().a(new c(this), false, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.config.o.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.igexin.push.config.o.a(org.json.JSONObject, java.lang.String):java.lang.String[]
      com.igexin.push.config.o.a(java.lang.String, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x025d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:?, code lost:
        com.igexin.b.a.c.a.b(com.igexin.push.config.a.f873a + "|" + r0.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004c, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004d, code lost:
        if (r0 != null) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x004f, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f8, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00fb, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:262:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00f8 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:4:0x000b] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00fb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(android.database.sqlite.SQLiteDatabase r7) {
        /*
            r6 = this;
            r3 = 0
            r2 = 0
            java.lang.String r0 = "select id, value from config order by id"
            r1 = 0
            android.database.Cursor r1 = r7.rawQuery(r0, r1)     // Catch:{ Exception -> 0x0436, all -> 0x0432 }
            if (r1 == 0) goto L_0x03ee
        L_0x000b:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 == 0) goto L_0x03ee
            r0 = 0
            r4 = 1
            int r5 = r1.getInt(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            r0 = 20
            if (r5 == r0) goto L_0x002f
            r0 = 21
            if (r5 == r0) goto L_0x002f
            r0 = 22
            if (r5 == r0) goto L_0x002f
            r0 = 24
            if (r5 == r0) goto L_0x002f
            r0 = 26
            if (r5 == r0) goto L_0x002f
            r0 = 45
            if (r5 != r0) goto L_0x00dc
        L_0x002f:
            byte[] r0 = r1.getBlob(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            r4 = r2
        L_0x0034:
            switch(r5) {
                case 1: goto L_0x0038;
                case 2: goto L_0x00e4;
                case 3: goto L_0x00ff;
                case 4: goto L_0x0113;
                case 5: goto L_0x0127;
                case 6: goto L_0x013b;
                case 7: goto L_0x014f;
                case 8: goto L_0x0163;
                case 9: goto L_0x0177;
                case 10: goto L_0x0037;
                case 11: goto L_0x018b;
                case 12: goto L_0x019f;
                case 13: goto L_0x01b3;
                case 14: goto L_0x01c7;
                case 15: goto L_0x01db;
                case 16: goto L_0x01ef;
                case 17: goto L_0x0203;
                case 18: goto L_0x0217;
                case 19: goto L_0x022b;
                case 20: goto L_0x023f;
                case 21: goto L_0x0280;
                case 22: goto L_0x0291;
                case 23: goto L_0x02a2;
                case 24: goto L_0x02b6;
                case 25: goto L_0x02cc;
                case 26: goto L_0x02e0;
                case 27: goto L_0x02fe;
                case 28: goto L_0x0312;
                case 29: goto L_0x031e;
                case 30: goto L_0x0037;
                case 31: goto L_0x0037;
                case 32: goto L_0x0037;
                case 33: goto L_0x0037;
                case 34: goto L_0x0037;
                case 35: goto L_0x0037;
                case 36: goto L_0x0037;
                case 37: goto L_0x0037;
                case 38: goto L_0x0037;
                case 39: goto L_0x0037;
                case 40: goto L_0x0332;
                case 41: goto L_0x0346;
                case 42: goto L_0x035b;
                case 43: goto L_0x036f;
                case 44: goto L_0x0037;
                case 45: goto L_0x0383;
                case 46: goto L_0x03b2;
                case 47: goto L_0x03c6;
                case 48: goto L_0x03da;
                default: goto L_0x0037;
            }     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
        L_0x0037:
            goto L_0x000b
        L_0x0038:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.f885a = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x004b:
            r0 = move-exception
            r0 = r1
        L_0x004d:
            if (r0 == 0) goto L_0x0052
            r0.close()
        L_0x0052:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = com.igexin.push.config.a.f873a
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "|current ver = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "2.10.1.0"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ", last ver = "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.igexin.push.core.g.N
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            java.lang.String r0 = "2.10.1.0"
            java.lang.String r1 = com.igexin.push.core.g.N
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x040c
            com.igexin.push.core.bean.g r0 = com.igexin.push.config.m.s
            if (r0 == 0) goto L_0x040d
            com.igexin.push.core.bean.g r0 = com.igexin.push.config.m.s
            java.util.Map r0 = r0.b()
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x03f5
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = com.igexin.push.config.a.f873a
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "|extMap is empty  = false"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            com.igexin.push.core.bean.g r0 = com.igexin.push.config.m.s
            java.util.Map r0 = r0.b()
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x00bc:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x03f5
            com.igexin.push.core.bean.g r0 = com.igexin.push.config.m.s
            java.util.Map r0 = r0.b()
            java.lang.Object r3 = r1.next()
            java.lang.Object r0 = r0.get(r3)
            com.igexin.push.core.bean.f r0 = (com.igexin.push.core.bean.f) r0
            if (r0 == 0) goto L_0x00bc
            java.lang.String r0 = r0.c()
            com.igexin.push.util.e.b(r0)
            goto L_0x00bc
        L_0x00dc:
            java.lang.String r0 = r1.getString(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            r4 = r0
            r0 = r2
            goto L_0x0034
        L_0x00e4:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.b = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x00f8:
            r0 = move-exception
        L_0x00f9:
            if (r1 == 0) goto L_0x00fe
            r1.close()
        L_0x00fe:
            throw r0
        L_0x00ff:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Long r0 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            long r4 = r0.longValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.c = r4     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0113:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.f = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0127:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.g = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x013b:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.h = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x014f:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.i = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0163:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.j = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0177:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.k = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x018b:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.n = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x019f:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Long r0 = java.lang.Long.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            long r4 = r0.longValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.o = r4     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x01b3:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.l = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x01c7:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.m = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x01db:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.d = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x01ef:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.e = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0203:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.p = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0217:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.q = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x022b:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.r = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x023f:
            if (r0 == 0) goto L_0x000b
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x025d, all -> 0x00f8 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x025d, all -> 0x00f8 }
            byte[] r0 = com.igexin.b.a.a.a.c(r0, r5)     // Catch:{ Exception -> 0x025d, all -> 0x00f8 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x025d, all -> 0x00f8 }
            com.igexin.push.core.a.e r0 = com.igexin.push.core.a.e.a()     // Catch:{ Exception -> 0x025d, all -> 0x00f8 }
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x025d, all -> 0x00f8 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x025d, all -> 0x00f8 }
            com.igexin.push.core.bean.g r0 = r0.a(r5)     // Catch:{ Exception -> 0x025d, all -> 0x00f8 }
            com.igexin.push.config.m.s = r0     // Catch:{ Exception -> 0x025d, all -> 0x00f8 }
            goto L_0x000b
        L_0x025d:
            r0 = move-exception
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            r4.<init>()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.String r5 = com.igexin.push.config.a.f873a     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.String r5 = "|"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0280:
            if (r0 == 0) goto L_0x000b
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            byte[] r0 = com.igexin.b.a.a.a.c(r0, r5)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.t = r4     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0291:
            if (r0 == 0) goto L_0x000b
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            byte[] r0 = com.igexin.b.a.a.a.c(r0, r5)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.u = r4     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x02a2:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.v = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x02b6:
            if (r0 == 0) goto L_0x000b
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x02c9, all -> 0x00f8 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x02c9, all -> 0x00f8 }
            byte[] r0 = com.igexin.b.a.a.a.c(r0, r5)     // Catch:{ Exception -> 0x02c9, all -> 0x00f8 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x02c9, all -> 0x00f8 }
            r0 = 0
            com.igexin.push.config.o.a(r4, r0)     // Catch:{ Exception -> 0x02c9, all -> 0x00f8 }
            goto L_0x000b
        L_0x02c9:
            r0 = move-exception
            goto L_0x000b
        L_0x02cc:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.x = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x02e0:
            if (r0 == 0) goto L_0x000b
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x02fb, all -> 0x00f8 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x02fb, all -> 0x00f8 }
            byte[] r0 = com.igexin.b.a.a.a.c(r0, r5)     // Catch:{ Exception -> 0x02fb, all -> 0x00f8 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x02fb, all -> 0x00f8 }
            org.json.JSONArray r0 = new org.json.JSONArray     // Catch:{ Exception -> 0x02fb, all -> 0x00f8 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x02fb, all -> 0x00f8 }
            java.lang.String[] r0 = com.igexin.push.core.a.s.a(r0)     // Catch:{ Exception -> 0x02fb, all -> 0x00f8 }
            com.igexin.push.config.SDKUrlConfig.setIdcConfigUrl(r0)     // Catch:{ Exception -> 0x02fb, all -> 0x00f8 }
            goto L_0x000b
        L_0x02fb:
            r0 = move-exception
            goto L_0x000b
        L_0x02fe:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.E = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0312:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            com.igexin.push.config.m.F = r4     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x031e:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.G = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0332:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.H = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0346:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            long r4 = (long) r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.I = r4     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x035b:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.J = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x036f:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.K = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x0383:
            if (r0 == 0) goto L_0x000b
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            byte[] r0 = com.igexin.b.a.a.a.c(r0, r5)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.L = r4     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            r0.<init>()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.String r4 = com.igexin.push.config.a.f873a     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.String r4 = "|read from db hideRightIconBlackList = "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.String r4 = com.igexin.push.config.m.L     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x03b2:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.M = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x03c6:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.N = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x03da:
            java.lang.String r0 = "null"
            boolean r0 = r4.equals(r0)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            if (r0 != 0) goto L_0x000b
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            com.igexin.push.config.m.O = r0     // Catch:{ Exception -> 0x004b, all -> 0x00f8 }
            goto L_0x000b
        L_0x03ee:
            if (r1 == 0) goto L_0x0052
            r1.close()
            goto L_0x0052
        L_0x03f5:
            com.igexin.push.config.m.s = r2
            r6.h()
        L_0x03fa:
            com.igexin.push.core.b.g r0 = com.igexin.push.core.b.g.a()
            java.lang.String r1 = "2.10.1.0"
            r0.e(r1)
            com.igexin.push.core.b.g r0 = com.igexin.push.core.b.g.a()
            r2 = 0
            r0.e(r2)
        L_0x040c:
            return
        L_0x040d:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0430 }
            java.lang.String r1 = com.igexin.push.core.g.ac     // Catch:{ Exception -> 0x0430 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0430 }
            java.io.File[] r1 = r0.listFiles()     // Catch:{ Exception -> 0x0430 }
            int r2 = r1.length     // Catch:{ Exception -> 0x0430 }
            r0 = r3
        L_0x041a:
            if (r0 >= r2) goto L_0x03fa
            r3 = r1[r0]     // Catch:{ Exception -> 0x0430 }
            java.lang.String r4 = r3.getName()     // Catch:{ Exception -> 0x0430 }
            java.lang.String r5 = "tdata_"
            boolean r4 = r4.startsWith(r5)     // Catch:{ Exception -> 0x0430 }
            if (r4 == 0) goto L_0x042d
            r3.delete()     // Catch:{ Exception -> 0x0430 }
        L_0x042d:
            int r0 = r0 + 1
            goto L_0x041a
        L_0x0430:
            r0 = move-exception
            goto L_0x03fa
        L_0x0432:
            r0 = move-exception
            r1 = r2
            goto L_0x00f9
        L_0x0436:
            r0 = move-exception
            r0 = r2
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.config.a.b(android.database.sqlite.SQLiteDatabase):void");
    }

    public void b(String str) {
        c.b().a(new j(this, str), true, false);
    }

    public void c() {
        c.b().a(new d(this), false, true);
    }

    public void c(SQLiteDatabase sQLiteDatabase) {
        a(sQLiteDatabase, 1, String.valueOf(m.f885a));
        a(sQLiteDatabase, 2, String.valueOf(m.b));
        a(sQLiteDatabase, 3, String.valueOf(m.c));
        a(sQLiteDatabase, 4, String.valueOf(m.f));
        a(sQLiteDatabase, 5, String.valueOf(m.g));
        a(sQLiteDatabase, 6, String.valueOf(m.h));
        a(sQLiteDatabase, 7, String.valueOf(m.i));
        a(sQLiteDatabase, 8, String.valueOf(m.j));
        a(sQLiteDatabase, 9, String.valueOf(m.k));
        a(sQLiteDatabase, 11, String.valueOf(m.n));
        a(sQLiteDatabase, 12, String.valueOf(m.o));
        a(sQLiteDatabase, 13, String.valueOf(m.l));
        a(sQLiteDatabase, 14, String.valueOf(m.m));
        a(sQLiteDatabase, 15, String.valueOf(m.d));
        a(sQLiteDatabase, 3, String.valueOf(m.c));
        a(sQLiteDatabase, 17, String.valueOf(m.p));
        a(sQLiteDatabase, 18, String.valueOf(m.q));
        a(sQLiteDatabase, 19, String.valueOf(m.r));
        a(sQLiteDatabase, 25, String.valueOf(m.x));
    }

    public void d() {
        c.b().a(new e(this), false, true);
    }

    public void e() {
        c.b().a(new f(this), false, true);
    }

    public void f() {
        c.b().a(new g(this), false, true);
    }

    public void g() {
        c.b().a(new h(this), true, false);
    }

    public void h() {
        c.b().a(new b(this), true, false);
    }
}
