package com.igexin.push.b;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.igexin.b.a.b.c;
import com.igexin.b.a.d.d;
import com.igexin.push.core.f;
import java.util.LinkedList;
import java.util.List;

public class a extends d {
    private static final String e = a.class.getName();

    /* renamed from: a  reason: collision with root package name */
    protected SQLiteDatabase f853a;
    protected Cursor b;
    List<com.igexin.push.core.b.a> c = new LinkedList();
    boolean d;

    public a() {
        super(1);
    }

    public void a(com.igexin.push.core.b.a aVar) {
        this.c.add(aVar);
    }

    public void a_() {
        super.a_();
        this.f853a = f.a().k().getWritableDatabase();
        this.f853a.setVersion(3);
        for (com.igexin.push.core.b.a a2 : this.c) {
            a2.a(this.f853a);
        }
        for (com.igexin.push.core.b.a next : this.c) {
            if (this.d) {
                next.c(this.f853a);
            } else {
                next.b(this.f853a);
            }
        }
        c.b().a(new c(-980948));
        c.b().c();
    }

    public final int b() {
        return -2147483639;
    }

    public void c() {
        super.c();
        if (this.b != null) {
            try {
                this.b.close();
            } catch (Exception e2) {
            }
        }
    }

    public void d() {
        super.d();
        this.n = true;
        this.I = true;
    }

    /* access modifiers changed from: protected */
    public void e() {
    }
}
