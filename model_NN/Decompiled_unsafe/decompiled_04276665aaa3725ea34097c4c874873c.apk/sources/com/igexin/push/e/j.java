package com.igexin.push.e;

import android.content.Context;
import com.igexin.b.a.b.c;
import com.igexin.push.config.SDKUrlConfig;
import com.igexin.push.core.g;
import com.igexin.push.d.b;
import com.igexin.push.d.c.a;
import com.igexin.push.d.c.d;
import com.igexin.push.d.c.e;
import com.igexin.push.d.c.f;
import com.igexin.push.d.c.i;
import com.igexin.push.d.c.k;
import com.igexin.push.d.c.m;
import com.igexin.push.d.c.n;
import com.igexin.push.d.c.o;
import com.igexin.push.d.c.p;
import com.igexin.push.d.c.q;
import com.igexin.push.d.h;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private static String f1014a = j.class.getName();
    private c b;
    private k c;
    private boolean d;
    private long e = 0;
    private long f = 0;
    private boolean g;

    private String b(e eVar) {
        if (eVar instanceof f) {
            return "R-" + ((f) eVar).a();
        }
        if (eVar instanceof q) {
            return "R-" + ((q) eVar).b;
        }
        if (eVar instanceof k) {
            return "S-" + String.valueOf(((k) eVar).f996a);
        }
        if (eVar instanceof m) {
            if (((m) eVar).e != 0) {
                return "S-" + String.valueOf(((m) eVar).e);
            }
        } else if (eVar instanceof n) {
            return "S-" + String.valueOf(((n) eVar).f999a);
        } else {
            if (eVar instanceof o) {
                return "S-" + String.valueOf(((o) eVar).e);
            }
            if (eVar instanceof d) {
                return "C-" + ((d) eVar).g;
            }
            if (eVar instanceof p) {
                return "C-" + ((p) eVar).g;
            }
            if (eVar instanceof a) {
                return "C-" + ((a) eVar).d;
            }
            if (eVar instanceof com.igexin.push.d.c.c) {
                return "C-" + ((com.igexin.push.d.c.c) eVar).d;
            }
        }
        return "";
    }

    private boolean c() {
        if (!com.igexin.push.config.m.n || this.e + this.f < com.igexin.push.config.m.o) {
            return false;
        }
        a aVar = new a();
        aVar.a(com.igexin.push.core.c.check);
        com.igexin.push.core.f.a().h().a(aVar);
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int
     arg types: [java.lang.String, com.igexin.push.d.c.e, int]
     candidates:
      com.igexin.push.e.j.a(android.content.Context, com.igexin.b.a.b.c, com.igexin.push.e.k):void
      com.igexin.push.e.j.a(java.lang.String, com.igexin.push.d.c.e, boolean):int */
    public int a(String str, e eVar) {
        return a(str, eVar, false);
    }

    public int a(String str, e eVar, boolean z) {
        if (str == null || eVar == null) {
            return -1;
        }
        if (!g.l && !(eVar instanceof i) && !(eVar instanceof k) && !(eVar instanceof f)) {
            com.igexin.b.a.c.a.a("snl|sendData|not online|" + eVar.getClass().getName());
            return -3;
        } else if (!this.d) {
            return com.igexin.push.core.f.a().h().a(str, eVar);
        } else {
            if (z) {
                int i = 10;
                if (com.igexin.push.config.m.e > 0) {
                    i = com.igexin.push.config.m.e;
                }
                if (this.b.a(SDKUrlConfig.getCmAddress(), 3, com.igexin.push.core.f.a().f(), eVar, true, i, new h()) == null) {
                    return -2;
                }
            } else if (this.b.a(SDKUrlConfig.getCmAddress(), 3, com.igexin.push.core.f.a().f(), eVar, true) == null) {
                return -2;
            }
            byte[] d2 = eVar.d();
            if (d2 != null) {
                this.f = ((long) d2.length) + 8 + this.f;
            } else {
                this.f += 8;
            }
            c();
            return 0;
        }
    }

    public void a(Context context, c cVar, k kVar) {
        this.b = cVar;
        this.c = kVar;
    }

    public void a(e eVar) {
        if (eVar != null) {
            if (this.d) {
                String b2 = b(eVar);
                if (!b2.equals("S-") && !b2.equals("R-")) {
                    if (b2.length() > 0 && !b2.equals("C-") && !b2.equals("C-" + g.r) && !b2.equals("R-" + g.B) && !b2.equals("S-" + g.q)) {
                        com.igexin.push.core.f.a().h().b(b2, eVar);
                    } else if (this.c != null) {
                        this.c.a(eVar);
                    }
                    byte[] d2 = eVar.d();
                    if (d2 != null) {
                        this.e = ((long) d2.length) + 8 + this.e;
                    } else {
                        this.e += 8;
                    }
                    c();
                }
            } else if (this.c != null) {
                this.c.a(eVar);
            }
        }
    }

    public void a(boolean z) {
        com.igexin.b.a.c.a.b(f1014a + "|call setASNL, param isASNL = " + z + "; this.isASNL = " + this.d);
        if (this.d != z) {
            this.d = z;
            this.g = false;
            if (z) {
                com.igexin.b.a.c.a.b(f1014a + "|commit reconnectCheckNotify");
                this.f = 0;
                this.e = 0;
                com.igexin.push.core.a.e.a().d(true);
                return;
            }
            com.igexin.b.a.c.a.b(f1014a + "|isASNL = false, this.isASNL = true, disconnect tcp !!!");
            com.igexin.push.core.a.e.a().f();
        }
    }

    public boolean a() {
        return this.d;
    }

    public void b() {
        g.E = 0;
        if (!this.d && this.c != null) {
            this.c.b();
        }
    }

    public void b(boolean z) {
        if (g.l) {
            g.l = false;
            com.igexin.push.core.a.e.a().l();
        }
        com.igexin.push.c.a e2 = com.igexin.push.c.i.a().e();
        if (!z) {
            com.igexin.b.a.c.a.b(f1014a + "|disconnected by network ####");
            com.igexin.push.core.i.a().a(com.igexin.push.core.k.NETWORK_ERROR);
            e2.i();
            com.igexin.b.a.c.a.b(f1014a + "|autoReconnect notifyNetworkDisconnected");
            com.igexin.push.core.a.e.a().d(true);
        } else {
            e2.i();
            com.igexin.b.a.c.a.b(f1014a + "|disconnected by user ####");
        }
        if (this.d) {
            com.igexin.push.core.f.a().h().b();
        } else if (this.c != null) {
            this.c.a(z);
        }
    }

    public void c(boolean z) {
        g.E = b.a().c().a(z);
        com.igexin.push.c.i.a().e().c();
        com.igexin.push.f.b.g.g().h();
    }
}
