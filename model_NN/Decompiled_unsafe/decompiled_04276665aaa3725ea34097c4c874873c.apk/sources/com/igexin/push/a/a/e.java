package com.igexin.push.a.a;

import android.text.TextUtils;
import com.igexin.b.a.b.c;
import com.igexin.push.config.SDKUrlConfig;
import com.igexin.push.core.c.k;
import com.igexin.push.f.a;

class e extends a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f852a;

    e(d dVar) {
        this.f852a = dVar;
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            String h = com.igexin.push.util.e.h();
            if (!TextUtils.isEmpty(h)) {
                c.b().a(new com.igexin.push.f.a.c(new k(SDKUrlConfig.getBiUploadServiceUrl(), h.getBytes(), 10, false)), false, true);
            }
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b("UploadBITask|" + th.toString());
        }
    }
}
