package com.igexin.push.c;

import android.text.TextUtils;
import com.igexin.b.a.c.a;
import com.igexin.push.core.g;

public class r extends m implements o {
    private static final String e = r.class.getName();
    private static r f;

    private r() {
        super(g.ay, g.aA);
    }

    public static synchronized r a() {
        r rVar;
        synchronized (r.class) {
            if (f == null) {
                f = new r();
            }
            rVar = f;
        }
        return rVar;
    }

    public void a(g gVar, j jVar) {
        p a2;
        if (jVar != null && !TextUtils.isEmpty(jVar.a()) && (a2 = a(jVar.a())) != null) {
            b(jVar);
            if (gVar == g.SUCCESS) {
                a2.g();
                a2.j();
                k();
                n();
            } else if (gVar == g.EXCEPTION || gVar == g.FAILED) {
                o();
                a.b(e + "|detect" + c(jVar) + "failed --------------");
                if (q()) {
                    a2.g();
                } else {
                    a2.a(false);
                }
            }
        }
    }

    public void a(j jVar) {
    }

    public h b() {
        return h.WIFI;
    }

    public o c() {
        return this;
    }
}
