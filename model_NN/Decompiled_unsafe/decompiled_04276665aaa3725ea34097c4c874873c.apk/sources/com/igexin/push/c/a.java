package com.igexin.push.c;

import com.igexin.push.config.SDKUrlConfig;
import com.igexin.push.config.m;
import com.igexin.push.core.a.e;
import com.igexin.push.core.g;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class a {
    private static final String f = a.class.getName();

    /* renamed from: a  reason: collision with root package name */
    public volatile d f857a = d.NORMAL;
    public AtomicBoolean b = new AtomicBoolean(false);
    protected int c;
    protected volatile long d;
    protected volatile long e;
    private int g;
    private int h;
    private int i;
    private j j;
    private final List<e> k = new ArrayList();
    private final List<j> l = new ArrayList();
    private final Object m = new Object();
    private final Object n = new Object();
    private int o = 0;
    private final Comparator<j> p = new b(this);

    private String a(boolean z) {
        String b2;
        synchronized (this.m) {
            this.g = this.g >= this.l.size() ? 0 : this.g;
            this.j = this.l.get(this.g);
            b2 = this.j.b(z);
        }
        return b2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0059  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void a(com.igexin.push.c.d r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0080 }
            r0.<init>()     // Catch:{ all -> 0x0080 }
            java.lang.String r1 = com.igexin.push.c.a.f     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0080 }
            java.lang.String r1 = "|set domain type = "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x0080 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0080 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x0080 }
            boolean r0 = com.igexin.push.config.m.f     // Catch:{ all -> 0x0080 }
            if (r0 == 0) goto L_0x0041
            com.igexin.push.c.d r0 = r2.f857a     // Catch:{ all -> 0x0080 }
            if (r0 == r3) goto L_0x0029
            r0 = 0
            r2.a(r0)     // Catch:{ all -> 0x0080 }
        L_0x0029:
            int[] r0 = com.igexin.push.c.c.f859a     // Catch:{ all -> 0x0080 }
            int r1 = r3.ordinal()     // Catch:{ all -> 0x0080 }
            r0 = r0[r1]     // Catch:{ all -> 0x0080 }
            switch(r0) {
                case 1: goto L_0x004a;
                case 2: goto L_0x0083;
                case 3: goto L_0x0043;
                default: goto L_0x0034;
            }     // Catch:{ all -> 0x0080 }
        L_0x0034:
            r2.f857a = r3     // Catch:{ all -> 0x0080 }
            com.igexin.push.c.i r0 = com.igexin.push.c.i.a()     // Catch:{ all -> 0x0080 }
            com.igexin.push.c.m r0 = r0.i()     // Catch:{ all -> 0x0080 }
            r0.p()     // Catch:{ all -> 0x0080 }
        L_0x0041:
            monitor-exit(r2)
            return
        L_0x0043:
            com.igexin.push.c.d r0 = r2.f857a     // Catch:{ all -> 0x0080 }
            if (r0 == r3) goto L_0x004a
            r0 = 0
            r2.o = r0     // Catch:{ all -> 0x0080 }
        L_0x004a:
            r0 = 0
            r2.g = r0     // Catch:{ all -> 0x0080 }
            r0 = 1
            java.lang.String r0 = r2.a(r0)     // Catch:{ all -> 0x0080 }
            com.igexin.push.config.SDKUrlConfig.setCmAddress(r0)     // Catch:{ all -> 0x0080 }
            com.igexin.push.c.d r0 = com.igexin.push.c.d.NORMAL     // Catch:{ all -> 0x0080 }
            if (r3 != r0) goto L_0x005f
            java.util.concurrent.atomic.AtomicBoolean r0 = r2.b     // Catch:{ all -> 0x0080 }
            r1 = 0
            r0.set(r1)     // Catch:{ all -> 0x0080 }
        L_0x005f:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0080 }
            r0.<init>()     // Catch:{ all -> 0x0080 }
            java.lang.String r1 = com.igexin.push.c.a.f     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0080 }
            java.lang.String r1 = "|set domain type normal cm = "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0080 }
            java.lang.String r1 = com.igexin.push.config.SDKUrlConfig.getCmAddress()     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0080 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0080 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x0080 }
            goto L_0x0034
        L_0x0080:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0083:
            java.util.concurrent.atomic.AtomicBoolean r0 = r2.b     // Catch:{ all -> 0x0080 }
            r1 = 1
            r0.set(r1)     // Catch:{ all -> 0x0080 }
            com.igexin.push.c.d r0 = r2.f857a     // Catch:{ all -> 0x0080 }
            if (r0 == r3) goto L_0x0093
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0080 }
            r2.d = r0     // Catch:{ all -> 0x0080 }
        L_0x0093:
            java.lang.String[] r0 = com.igexin.push.config.SDKUrlConfig.XFR_ADDRESS_BAK     // Catch:{ all -> 0x0080 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x0080 }
            com.igexin.push.config.SDKUrlConfig.setCmAddress(r0)     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0080 }
            r0.<init>()     // Catch:{ all -> 0x0080 }
            java.lang.String r1 = com.igexin.push.c.a.f     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0080 }
            java.lang.String r1 = "|set domain type backup cm = "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0080 }
            java.lang.String r1 = com.igexin.push.config.SDKUrlConfig.getCmAddress()     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0080 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0080 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x0080 }
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.c.a.a(com.igexin.push.c.d):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String b(boolean r11) {
        /*
            r10 = this;
            r1 = 0
            r2 = 0
            java.lang.Object r3 = r10.n     // Catch:{ Exception -> 0x00cb }
            monitor-enter(r3)     // Catch:{ Exception -> 0x00cb }
            java.util.List<com.igexin.push.c.e> r0 = r10.k     // Catch:{ all -> 0x00c8 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00c8 }
            if (r0 == 0) goto L_0x002e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r0.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r2 = com.igexin.push.c.a.f     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x00c8 }
            java.lang.String r2 = "cm list size = 0"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x00c8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00c8 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x00c8 }
            r0 = 0
            r10.i = r0     // Catch:{ all -> 0x00c8 }
            r0 = 0
            r10.h = r0     // Catch:{ all -> 0x00c8 }
            monitor-exit(r3)     // Catch:{ all -> 0x00c8 }
            r0 = r1
        L_0x002d:
            return r0
        L_0x002e:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r0.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r4 = com.igexin.push.c.a.f     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x00c8 }
            java.lang.String r4 = "cm try = "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x00c8 }
            int r4 = r10.i     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x00c8 }
            java.lang.String r4 = " times"
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x00c8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00c8 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x00c8 }
            int r0 = r10.i     // Catch:{ all -> 0x00c8 }
            java.util.List<com.igexin.push.c.e> r4 = r10.k     // Catch:{ all -> 0x00c8 }
            int r4 = r4.size()     // Catch:{ all -> 0x00c8 }
            int r4 = r4 * 3
            if (r0 < r4) goto L_0x0084
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r0.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r2 = com.igexin.push.c.a.f     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x00c8 }
            java.lang.String r2 = "cm invalid"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x00c8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00c8 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x00c8 }
            r0 = 0
            r10.i = r0     // Catch:{ all -> 0x00c8 }
            r0 = 0
            r10.h = r0     // Catch:{ all -> 0x00c8 }
            java.util.List<com.igexin.push.c.e> r0 = r10.k     // Catch:{ all -> 0x00c8 }
            r0.clear()     // Catch:{ all -> 0x00c8 }
            monitor-exit(r3)     // Catch:{ all -> 0x00c8 }
            r0 = r1
            goto L_0x002d
        L_0x0084:
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00c8 }
            java.util.List<com.igexin.push.c.e> r0 = r10.k     // Catch:{ all -> 0x00c8 }
            java.util.Iterator r6 = r0.iterator()     // Catch:{ all -> 0x00c8 }
        L_0x008e:
            boolean r0 = r6.hasNext()     // Catch:{ all -> 0x00c8 }
            if (r0 == 0) goto L_0x00ef
            java.lang.Object r0 = r6.next()     // Catch:{ all -> 0x00c8 }
            com.igexin.push.c.e r0 = (com.igexin.push.c.e) r0     // Catch:{ all -> 0x00c8 }
            long r8 = r0.b     // Catch:{ all -> 0x00c8 }
            int r7 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r7 >= 0) goto L_0x008e
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c8 }
            r7.<init>()     // Catch:{ all -> 0x00c8 }
            java.lang.String r8 = com.igexin.push.c.a.f     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x00c8 }
            java.lang.String r8 = "|add["
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x00c8 }
            java.lang.String r0 = r0.f861a     // Catch:{ all -> 0x00c8 }
            java.lang.StringBuilder r0 = r7.append(r0)     // Catch:{ all -> 0x00c8 }
            java.lang.String r7 = "] outDate"
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ all -> 0x00c8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00c8 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x00c8 }
            r6.remove()     // Catch:{ all -> 0x00c8 }
            goto L_0x008e
        L_0x00c8:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00c8 }
            throw r0     // Catch:{ Exception -> 0x00cb }
        L_0x00cb:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = com.igexin.push.c.a.f
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            r0 = r1
            goto L_0x002d
        L_0x00ef:
            java.util.List<com.igexin.push.c.e> r0 = r10.k     // Catch:{ all -> 0x00c8 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00c8 }
            if (r0 == 0) goto L_0x00fb
            monitor-exit(r3)     // Catch:{ all -> 0x00c8 }
            r0 = r1
            goto L_0x002d
        L_0x00fb:
            int r0 = r10.h     // Catch:{ all -> 0x00c8 }
            java.util.List<com.igexin.push.c.e> r4 = r10.k     // Catch:{ all -> 0x00c8 }
            int r4 = r4.size()     // Catch:{ all -> 0x00c8 }
            if (r0 < r4) goto L_0x0125
            r0 = r2
        L_0x0106:
            r10.h = r0     // Catch:{ all -> 0x00c8 }
            java.util.List<com.igexin.push.c.e> r0 = r10.k     // Catch:{ all -> 0x00c8 }
            int r2 = r10.h     // Catch:{ all -> 0x00c8 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x00c8 }
            com.igexin.push.c.e r0 = (com.igexin.push.c.e) r0     // Catch:{ all -> 0x00c8 }
            java.lang.String r0 = r0.f861a     // Catch:{ all -> 0x00c8 }
            int r2 = r10.h     // Catch:{ all -> 0x00c8 }
            int r2 = r2 + 1
            r10.h = r2     // Catch:{ all -> 0x00c8 }
            if (r11 == 0) goto L_0x0122
            int r2 = r10.i     // Catch:{ all -> 0x00c8 }
            int r2 = r2 + 1
            r10.i = r2     // Catch:{ all -> 0x00c8 }
        L_0x0122:
            monitor-exit(r3)     // Catch:{ all -> 0x00c8 }
            goto L_0x002d
        L_0x0125:
            int r0 = r10.h     // Catch:{ all -> 0x00c8 }
            goto L_0x0106
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.c.a.b(boolean):java.lang.String");
    }

    private void k() {
        com.igexin.b.a.c.a.b(f + "|before disconnect, type = " + this.f857a);
        switch (c.f859a[this.f857a.ordinal()]) {
            case 1:
                if (System.currentTimeMillis() - this.e > 172800000 && this.c > m.B) {
                    a(d.BACKUP);
                    return;
                }
                return;
            case 2:
                if (System.currentTimeMillis() - this.d > m.z) {
                    a(d.TRY_NORMAL);
                    return;
                }
                return;
            case 3:
            default:
                return;
        }
    }

    public void a() {
        try {
            boolean z = !e.a().B();
            String b2 = b(z);
            com.igexin.b.a.c.a.b(f + "|get from cm = " + b2);
            if (b2 == null) {
                if (!m.f || this.f857a != d.BACKUP) {
                    if (this.j != null && !this.j.f()) {
                        this.g++;
                    }
                    b2 = a(z);
                } else {
                    this.g = this.g >= SDKUrlConfig.XFR_ADDRESS_BAK.length ? 0 : this.g;
                    b2 = SDKUrlConfig.XFR_ADDRESS_BAK[this.g];
                    this.g++;
                }
            }
            if (!SDKUrlConfig.getCmAddress().equals(b2)) {
                com.igexin.b.a.c.a.b(f + "|address changed : form [" + SDKUrlConfig.getCmAddress() + "] to [" + b2 + "]");
            }
            SDKUrlConfig.setCmAddress(b2);
        } catch (Exception e2) {
            e2.printStackTrace();
            com.igexin.b.a.c.a.b(f + "|switch address|" + e2.toString());
        }
    }

    public void a(List<e> list) {
        synchronized (this.n) {
            this.h = 0;
            this.i = 0;
            this.k.clear();
            if (list != null) {
                this.k.addAll(list);
                com.igexin.b.a.c.a.b(f + "|set cm list: " + list.toString());
            }
        }
    }

    public synchronized void b() {
        com.igexin.b.a.c.a.b(f + "|login or register success, cmConncetTryCnt = 0");
        this.i = 0;
        if (this.j != null) {
            this.j.h();
        }
    }

    public void b(List<j> list) {
        synchronized (this.m) {
            this.l.clear();
            this.l.addAll(list);
            Collections.sort(this.l, this.p);
        }
    }

    public synchronized void c() {
        this.c++;
        com.igexin.b.a.c.a.b(f + "|before login success, loginFailedlCnt = " + this.c);
    }

    public void d() {
        int i2 = 0;
        synchronized (this.m) {
            this.g = 0;
            Collections.sort(this.l, this.p);
            try {
                int length = SDKUrlConfig.getXfrAddress().length;
                if (length >= 3) {
                    while (true) {
                        int i3 = i2;
                        if (i3 >= this.l.size()) {
                            break;
                        }
                        this.l.get(i3).b(length - i3);
                        i2 = i3 + 1;
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                com.igexin.b.a.c.a.b(f + "|" + e2.toString());
            }
        }
    }

    public void e() {
        com.igexin.b.a.c.a.b(f + "|detect success, current type = " + this.f857a);
        if (this.f857a == d.BACKUP) {
            a(d.TRY_NORMAL);
            e.a().c(true);
        }
    }

    public void f() {
        com.igexin.b.a.c.a.b(f + "|detect max cnt ,set type = backup");
        a(d.BACKUP);
        e.a().c(true);
    }

    public void g() {
        switch (c.f859a[this.f857a.ordinal()]) {
            case 2:
                if (System.currentTimeMillis() - this.d > m.z) {
                    a(d.TRY_NORMAL);
                    return;
                }
                return;
            case 3:
            default:
                return;
        }
    }

    public void h() {
        if (this.f857a != d.BACKUP) {
            this.c = 0;
        }
        switch (c.f859a[this.f857a.ordinal()]) {
            case 1:
                this.e = System.currentTimeMillis();
                i.a().i().p();
                this.b.set(false);
                return;
            case 2:
            default:
                return;
            case 3:
                a(d.NORMAL);
                this.b.set(false);
                return;
        }
    }

    public void i() {
        k();
        if (g.l && this.f857a != d.BACKUP) {
            this.e = System.currentTimeMillis();
            i.a().i().p();
        }
        switch (c.f859a[this.f857a.ordinal()]) {
            case 1:
            case 2:
            default:
                return;
            case 3:
                int i2 = this.o + 1;
                this.o = i2;
                if (i2 >= 10) {
                    this.c = 0;
                    this.d = System.currentTimeMillis();
                    a(d.BACKUP);
                    return;
                }
                return;
        }
    }

    public boolean j() {
        return this.f857a == d.NORMAL;
    }
}
