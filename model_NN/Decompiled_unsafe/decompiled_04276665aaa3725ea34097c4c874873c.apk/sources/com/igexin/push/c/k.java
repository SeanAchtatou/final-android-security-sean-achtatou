package com.igexin.push.c;

import com.igexin.b.a.c.a;
import com.igexin.push.core.g;
import com.igexin.push.f.b.h;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class k extends h {

    /* renamed from: a  reason: collision with root package name */
    public static final AtomicBoolean f867a = new AtomicBoolean(false);
    private static final String b = k.class.getName();
    private static k c;
    private boolean e;

    private k() {
        super(150);
        this.o = true;
    }

    public static synchronized k b_() {
        k kVar;
        synchronized (k.class) {
            if (c == null) {
                c = new k();
            }
            kVar = c;
        }
        return kVar;
    }

    /* access modifiers changed from: protected */
    public void a() {
        a(f.f862a, TimeUnit.MILLISECONDS);
        if (this.e) {
            a.b(b + "|detect task already stop");
            return;
        }
        a.b(b + "|" + (f.f862a / 1000) + "s passed, do task method, start redect ~~~~");
        if (g.h) {
            i.a().d();
        } else {
            a.b(b + "|" + (f.f862a / 1000) + "s passed, network is unavailable, stop ###");
        }
    }

    public void a(long j) {
        a(j, TimeUnit.MILLISECONDS);
    }

    public int b() {
        return 20150607;
    }

    public void g() {
        this.o = false;
        this.e = true;
        p();
    }
}
