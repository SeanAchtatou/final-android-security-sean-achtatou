package com.igexin.push.c;

import android.text.TextUtils;
import com.igexin.b.a.b.c;
import com.igexin.b.a.b.f;
import com.igexin.b.a.c.a;
import com.igexin.push.config.SDKUrlConfig;
import com.igexin.push.core.a.e;
import com.igexin.push.core.g;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class m {
    private static final String e = m.class.getName();

    /* renamed from: a  reason: collision with root package name */
    protected long f868a;
    protected final Map<String, p> b = new LinkedHashMap();
    protected final Map<String, j> c = new HashMap();
    protected a d = new a();
    private final Object f = new Object();
    private final Object g = new Object();
    private int h;
    private AtomicBoolean i = new AtomicBoolean(false);
    private final Comparator<Map.Entry<String, j>> j = new n(this);
    private int k = s();

    public m(String str, String str2) {
        if (!i.f865a) {
            b(str);
            c(str2);
            return;
        }
        a(false);
    }

    private j a(JSONObject jSONObject) {
        if (!jSONObject.has("domain")) {
            return null;
        }
        j jVar = new j();
        jVar.a(jSONObject.getString("domain"));
        if (jSONObject.has("port")) {
            jVar.a(jSONObject.getInt("port"));
        }
        if (jSONObject.has(Parameters.IP_ADDRESS)) {
            jVar.b(jSONObject.getString(Parameters.IP_ADDRESS));
        }
        if (jSONObject.has("consumeTime")) {
            jVar.a(jSONObject.getLong("consumeTime"));
        }
        if (jSONObject.has("detectSuccessTime")) {
            jVar.b(jSONObject.getLong("detectSuccessTime"));
        }
        if (jSONObject.has("isDomain")) {
            jVar.a(jSONObject.getBoolean("isDomain"));
        }
        if (!jSONObject.has("connectTryCnt")) {
            return jVar;
        }
        jVar.b(jSONObject.getInt("connectTryCnt"));
        return jVar;
    }

    private List<String> a() {
        String[] xfrAddress = SDKUrlConfig.getXfrAddress();
        ArrayList arrayList = new ArrayList();
        for (String str : xfrAddress) {
            if (!arrayList.contains(str)) {
                arrayList.add(str);
            }
        }
        return arrayList;
    }

    private List<String> a(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (i2 < jSONArray.length()) {
            try {
                arrayList.add(jSONArray.getJSONObject(i2).getString("domain"));
                i2++;
            } catch (Exception e2) {
            }
        }
        return arrayList;
    }

    private void a(j jVar) {
        p pVar = new p();
        pVar.a(c());
        pVar.a(jVar);
        synchronized (this.g) {
            this.b.put(jVar.a(), pVar);
        }
        c.b().a(pVar, true, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.core.b.g.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.igexin.push.core.b.g.b(android.database.sqlite.SQLiteDatabase, int):java.lang.String
      com.igexin.push.core.b.g.b(java.lang.String, boolean):boolean */
    private void a(boolean z) {
        this.f868a = 0;
        if (t()) {
            if (g.ax != null) {
                com.igexin.push.core.b.g.a().b("null", true);
            }
        } else if (g.ay != null) {
            com.igexin.push.core.b.g.a().b("null", false);
        }
        List<String> a2 = a();
        ArrayList arrayList = new ArrayList();
        for (String next : a2) {
            j jVar = new j(next, Integer.parseInt(f.a(next)[2]));
            if (z) {
                a(jVar);
            }
            arrayList.add(jVar);
        }
        this.d.b(arrayList);
        a2.clear();
    }

    private void b(String str) {
        JSONObject jSONObject;
        JSONArray jSONArray = null;
        if (TextUtils.isEmpty(str)) {
            a(true);
            return;
        }
        try {
            jSONObject = new JSONObject(str);
        } catch (JSONException e2) {
            jSONObject = null;
        }
        if (jSONObject == null || jSONObject.length() == 0) {
            a(true);
            return;
        }
        if (jSONObject.has("lastDetectTime")) {
            try {
                this.f868a = jSONObject.getLong("lastDetectTime");
            } catch (JSONException e3) {
            }
        }
        if (Math.abs(System.currentTimeMillis() - this.f868a) >= f.f862a) {
            a(true);
            return;
        }
        if (jSONObject.has("list")) {
            try {
                jSONArray = jSONObject.getJSONArray("list");
            } catch (JSONException e4) {
            }
        }
        if (jSONArray == null || jSONArray.length() == 0) {
            a(true);
            return;
        }
        List<String> a2 = a(jSONArray);
        if (a2 == null || a2.isEmpty()) {
            a(true);
            return;
        }
        List<String> a3 = a();
        ArrayList arrayList = new ArrayList(a3);
        arrayList.retainAll(a2);
        if (arrayList.size() != a2.size()) {
            a.b(e + " | db cache xfr != default, use default");
            arrayList.clear();
            a3.clear();
            a2.clear();
            a(true);
            return;
        }
        a.b(e + " | db cache xfr == default, use cache");
        b(jSONArray);
    }

    private void b(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (true) {
            try {
                int i3 = i2;
                if (i3 < jSONArray.length()) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i3);
                    j a2 = a(jSONObject);
                    if (a2 != null) {
                        this.c.put(a2.a(), a2);
                    } else {
                        try {
                            a2 = d(jSONObject.getString("domain"));
                        } catch (Exception e2) {
                            a.b(e + "|initWithCacheData exception " + e2.toString());
                            this.c.clear();
                            a(true);
                            return;
                        }
                    }
                    if (a2 != null) {
                        a(a2);
                        arrayList.add(a2);
                    }
                    i2 = i3 + 1;
                } else {
                    this.d.b(arrayList);
                    return;
                }
            } catch (Exception e3) {
                a.b(e + "|initWithCacheData exception " + e3.toString());
                return;
            }
        }
    }

    private void c(String str) {
        JSONObject jSONObject;
        if (!TextUtils.isEmpty(str)) {
            try {
                jSONObject = new JSONObject(str);
            } catch (JSONException e2) {
                jSONObject = null;
            }
            if (jSONObject != null && jSONObject.length() != 0) {
                if (jSONObject.has("detectFailedCnt")) {
                    try {
                        this.h = jSONObject.getInt("detectFailedCnt");
                    } catch (JSONException e3) {
                    }
                }
                if (jSONObject.has("loginFailedlCnt")) {
                    try {
                        this.d.c = jSONObject.getInt("loginFailedlCnt");
                    } catch (JSONException e4) {
                    }
                }
                if (jSONObject.has("lastChange2BackupTime")) {
                    try {
                        this.d.d = jSONObject.getLong("lastChange2BackupTime");
                    } catch (JSONException e5) {
                    }
                }
                if (jSONObject.has("lastOfflineTime")) {
                    try {
                        this.d.e = jSONObject.getLong("lastOfflineTime");
                    } catch (JSONException e6) {
                    }
                }
                if (jSONObject.has("domainType")) {
                    try {
                        this.d.f857a = d.a(jSONObject.getInt("domainType"));
                        if (this.d.f857a == d.BACKUP) {
                            this.d.b.set(true);
                        }
                    } catch (JSONException e7) {
                    }
                }
            }
        }
    }

    private j d(String str) {
        j jVar = new j();
        String[] a2 = f.a(str);
        jVar.a(str);
        jVar.a(Integer.parseInt(a2[2]));
        return jVar;
    }

    private void r() {
        synchronized (this.f) {
            this.c.clear();
        }
    }

    private int s() {
        return a().size();
    }

    private boolean t() {
        return b() == h.MOBILE;
    }

    /* access modifiers changed from: protected */
    public p a(String str) {
        p pVar;
        synchronized (this.g) {
            Iterator<Map.Entry<String, p>> it = this.b.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    pVar = null;
                    break;
                }
                Map.Entry next = it.next();
                if (((String) next.getKey()).equals(str)) {
                    pVar = (p) next.getValue();
                    break;
                }
            }
        }
        return pVar;
    }

    public abstract h b();

    /* access modifiers changed from: protected */
    public void b(j jVar) {
        synchronized (this.f) {
            this.c.put(jVar.a(), jVar);
        }
        this.d.d();
    }

    public abstract o c();

    /* access modifiers changed from: protected */
    public String c(j jVar) {
        return jVar.a() + "[" + jVar.c() + "] ";
    }

    public void d() {
        if (!m()) {
            a.b(e + "|startDetect detect = false, return !!!");
            return;
        }
        a.b(e + "|startDetect detect = true, start detect !!!");
        this.i.set(true);
        j();
    }

    public void e() {
        synchronized (this.g) {
            for (Map.Entry next : this.b.entrySet()) {
                ((p) next.getValue()).a((o) null);
                ((p) next.getValue()).g();
            }
        }
    }

    public void f() {
        this.i.set(true);
        synchronized (this.g) {
            for (Map.Entry next : this.b.entrySet()) {
                ((p) next.getValue()).a(c());
                ((p) next.getValue()).a(true);
            }
        }
    }

    public j g() {
        j jVar;
        synchronized (this.f) {
            if (this.c.isEmpty()) {
                jVar = null;
            } else {
                ArrayList arrayList = new ArrayList(this.c.entrySet());
                Collections.sort(arrayList, this.j);
                jVar = (j) ((Map.Entry) arrayList.get(0)).getValue();
            }
        }
        return jVar;
    }

    public void h() {
        e();
        r();
        List<String> a2 = a();
        synchronized (this.g) {
            int size = this.b.size();
            if (a2.size() < size) {
                int size2 = size - a2.size();
                Iterator<Map.Entry<String, p>> it = this.b.entrySet().iterator();
                int i2 = 0;
                while (it.hasNext() && i2 < size2) {
                    ((p) it.next().getValue()).h();
                    it.remove();
                    i2++;
                }
            }
            ArrayList arrayList = new ArrayList(this.b.values());
            this.b.clear();
            ArrayList arrayList2 = new ArrayList();
            for (int i3 = 0; i3 < a2.size(); i3++) {
                j jVar = new j();
                String[] a3 = f.a(a2.get(i3));
                jVar.a(a2.get(i3));
                jVar.a(Integer.parseInt(a3[2]));
                if (i3 < size) {
                    p pVar = (p) arrayList.get(i3);
                    pVar.a(jVar);
                    this.b.put(jVar.a(), pVar);
                } else {
                    a(jVar);
                }
                arrayList2.add(jVar);
            }
            this.d.b(arrayList2);
        }
    }

    public void i() {
        e();
        r();
        List<String> a2 = a();
        synchronized (this.g) {
            for (Map.Entry<String, p> value : this.b.entrySet()) {
                ((p) value.getValue()).h();
            }
            this.b.clear();
            ArrayList arrayList = new ArrayList();
            j jVar = new j();
            String[] a3 = f.a(a2.get(0));
            jVar.a(a2.get(0));
            jVar.a(Integer.parseInt(a3[2]));
            arrayList.add(jVar);
            this.d.b(arrayList);
            arrayList.clear();
        }
    }

    public void j() {
        this.f868a = System.currentTimeMillis();
        synchronized (this.g) {
            for (Map.Entry next : this.b.entrySet()) {
                ((p) next.getValue()).a(c());
                if (((p) next.getValue()).c_() != null) {
                    ((p) next.getValue()).c_().b();
                }
                ((p) next.getValue()).i();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.core.b.g.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.igexin.push.core.b.g.b(android.database.sqlite.SQLiteDatabase, int):java.lang.String
      com.igexin.push.core.b.g.b(java.lang.String, boolean):boolean */
    public synchronized void k() {
        this.f868a = System.currentTimeMillis();
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        synchronized (this.g) {
            try {
                jSONObject.put("lastDetectTime", this.f868a);
                jSONObject.put("list", jSONArray);
                for (Map.Entry<String, p> value : this.b.entrySet()) {
                    JSONObject i2 = ((p) value.getValue()).c_().i();
                    if (i2 != null) {
                        jSONArray.put(i2);
                    }
                }
            } catch (Exception e2) {
            }
        }
        if (jSONObject.length() > 0) {
            if (t()) {
                com.igexin.push.core.b.g.a().b(jSONObject.toString(), true);
            } else {
                com.igexin.push.core.b.g.a().b(jSONObject.toString(), false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.core.b.g.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.igexin.push.core.b.g.b(android.database.sqlite.SQLiteDatabase, int):java.lang.String
      com.igexin.push.core.b.g.b(java.lang.String, boolean):boolean */
    /* access modifiers changed from: protected */
    public void l() {
        com.igexin.push.core.b.g.a().b("null", true);
        com.igexin.push.core.b.g.a().b("null", false);
    }

    /* access modifiers changed from: protected */
    public boolean m() {
        long abs = Math.abs(System.currentTimeMillis() - this.f868a);
        if (abs >= f.f862a - 3600) {
            a.b(e + "|current time - last detect time > " + (f.f862a / 1000) + " s, detect = true");
            k.f867a.set(true);
            return true;
        }
        if (!k.f867a.getAndSet(true)) {
            long abs2 = Math.abs(f.f862a - abs);
            k.b_().a(abs2);
            a.b(e + "|set next detect time = " + abs2);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public synchronized void n() {
        if (!g.l && this.i.getAndSet(false)) {
            a.b(e + "|online = false, reconnect");
            e.a().d(true);
        }
        if (this.h != 0) {
            this.h = 0;
            p();
        }
        this.d.e();
    }

    /* access modifiers changed from: protected */
    public synchronized void o() {
        this.h++;
        a.b(e + "|detect failed cnt = " + (this.h / this.k));
        int i2 = this.h / this.k;
        if (i2 <= com.igexin.push.config.m.A) {
            p();
        }
        if (i2 >= com.igexin.push.config.m.A && !this.d.b.get()) {
            a.b(e + "|detect failed cnt = " + (this.h / this.k) + ", enter backup ++++++++");
            this.d.f();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.core.b.g.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.igexin.push.core.b.g.a(android.database.sqlite.SQLiteDatabase, int):byte[]
      com.igexin.push.core.b.g.a(com.igexin.push.core.b.g, java.lang.String):byte[]
      com.igexin.push.core.b.g.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: protected */
    public synchronized void p() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("detectFailedCnt", this.h);
            jSONObject.put("loginFailedlCnt", this.d.c);
            jSONObject.put("lastChange2BackupTime", this.d.d);
            jSONObject.put("lastOfflineTime", this.d.e);
            jSONObject.put("domainType", this.d.f857a.b());
        } catch (Exception e2) {
        }
        if (jSONObject.length() > 0) {
            if (t()) {
                com.igexin.push.core.b.g.a().a(jSONObject.toString(), true);
            } else {
                com.igexin.push.core.b.g.a().a(jSONObject.toString(), false);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean q() {
        boolean z;
        synchronized (this.f) {
            Iterator<Map.Entry<String, j>> it = this.c.entrySet().iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((j) it.next().getValue()).e() != 2147483647L) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
        }
        return z;
    }
}
