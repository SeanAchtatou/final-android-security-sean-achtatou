package com.igexin.push.extension;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Process;
import com.igexin.push.config.m;
import com.igexin.push.core.bean.f;
import com.igexin.push.core.c.i;
import com.igexin.push.core.g;
import com.igexin.push.extension.stub.IPushExtension;
import com.igexin.push.util.e;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static String f1015a = a.class.getName();
    private static a c;
    private List<IPushExtension> b = new ArrayList();

    private a() {
    }

    public static a a() {
        if (c == null) {
            c = new a();
        }
        return c;
    }

    private void a(f fVar) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) g.f.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 1 || !activeNetworkInfo.isConnected()) {
            com.igexin.b.a.c.a.b(f1015a + "|init ext not exist, is wifi state = false");
        } else {
            new Thread(new i(g.f, fVar, true)).start();
        }
    }

    private void a(f fVar, File file) {
        File file2 = new File(g.ab + "/" + fVar.c());
        if (file2.exists() && fVar.f().equals(com.igexin.b.b.a.a(g.f, file2.getAbsolutePath()))) {
            com.igexin.b.a.c.a.b(f1015a + "|public local file match the condition");
            if (!file.createNewFile()) {
                return;
            }
            if (e.a(file2, file, fVar.f())) {
                com.igexin.b.a.c.a.b(f1015a + "|restart the service to load ext = " + file.getAbsolutePath());
                Process.killProcess(Process.myPid());
                return;
            }
            com.igexin.b.a.c.a.b(f1015a + "|copy " + file2.getAbsolutePath() + " to " + file.getAbsolutePath() + " " + "failed...");
            e.b(fVar.c());
        }
    }

    private boolean a(Context context, f fVar, String str) {
        long currentTimeMillis = System.currentTimeMillis();
        if (fVar.h() == 0 || fVar.i() + fVar.h() >= currentTimeMillis) {
            if (a(context, str, fVar.d(), fVar.j(), fVar.c()) && fVar.i() != 0) {
                fVar.b(currentTimeMillis);
            }
            if (!fVar.g()) {
                return true;
            }
            e.b(fVar.c());
            return false;
        }
        e.b(fVar.c());
        return false;
    }

    private void b(Context context) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("com.igexin.push.extension.distribution.basic.stub.PushExtension");
        arrayList.add("com.igexin.push.extension.distribution.gbd.stub.PushExtension");
        arrayList.add("com.igexin.push.extension.distribution.lbs.stub.PushExtension");
        arrayList.add("com.igexin.push.extension.distribution.diagnosis.stub.PushExtension");
        arrayList.add("com.igexin.push.extension.distribution.gks.stub.PushExtension");
        for (String str : arrayList) {
            try {
                IPushExtension iPushExtension = (IPushExtension) context.getClassLoader().loadClass(str).newInstance();
                iPushExtension.init(g.f);
                this.b.add(iPushExtension);
                com.igexin.b.a.c.a.b("[main] extension loaded(mock): " + str);
            } catch (Exception e) {
                com.igexin.b.a.c.a.b(f1015a + e.toString());
            }
        }
    }

    public boolean a(Context context) {
        f fVar;
        try {
            if (m.s == null) {
                b(context);
            } else {
                Map<Integer, f> b2 = m.s.b();
                ArrayList<Integer> arrayList = new ArrayList<>();
                for (Map.Entry next : b2.entrySet()) {
                    int intValue = ((Integer) next.getKey()).intValue();
                    fVar = (f) next.getValue();
                    String str = g.ac + "/" + fVar.c();
                    File file = new File(str);
                    if (!file.exists()) {
                        a(fVar, file);
                        a(fVar);
                    } else if (!a(context, fVar, str)) {
                        arrayList.add(Integer.valueOf(intValue));
                    }
                }
                if (!arrayList.isEmpty()) {
                    for (Integer intValue2 : arrayList) {
                        b2.remove(Integer.valueOf(intValue2.intValue()));
                    }
                    com.igexin.b.a.c.a.b(f1015a + "|ext info info changed, save new exts");
                    com.igexin.push.config.a.a().g();
                }
            }
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b(f1015a + "|" + th.toString());
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.util.e.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.igexin.push.util.e.a(byte[], java.lang.String, boolean):void
      com.igexin.push.util.e.a(java.io.File, java.io.File, java.lang.String):boolean
      com.igexin.push.util.e.a(java.lang.String, java.lang.String, boolean):boolean */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.content.Context r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, java.lang.String r14) {
        /*
            r9 = this;
            r0 = 0
            r2 = 1
            r1 = 0
            java.io.File r3 = new java.io.File
            r3.<init>(r11)
            java.io.File r4 = new java.io.File
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r5 = r5.append(r11)
            java.lang.String r6 = ".jar"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.<init>(r5)
            java.io.File r5 = r10.getFilesDir()
            java.lang.String r5 = r5.getAbsolutePath()
            com.igexin.push.util.e.a(r5, r14, r1)
            com.igexin.push.util.e.b(r3, r4, r13)
            boolean r3 = r4.exists()
            if (r3 == 0) goto L_0x00ad
            dalvik.system.DexClassLoader r3 = new dalvik.system.DexClassLoader     // Catch:{ Throwable -> 0x0083 }
            java.lang.String r5 = r4.getAbsolutePath()     // Catch:{ Throwable -> 0x0083 }
            java.io.File r6 = r10.getFilesDir()     // Catch:{ Throwable -> 0x0083 }
            java.lang.String r6 = r6.getAbsolutePath()     // Catch:{ Throwable -> 0x0083 }
            r7 = 0
            java.lang.ClassLoader r8 = r10.getClassLoader()     // Catch:{ Throwable -> 0x0083 }
            r3.<init>(r5, r6, r7, r8)     // Catch:{ Throwable -> 0x0083 }
            java.lang.Class r0 = r3.loadClass(r12)     // Catch:{ Exception -> 0x0061 }
        L_0x004e:
            r4.delete()     // Catch:{ Throwable -> 0x0083 }
            java.io.File r3 = r10.getFilesDir()     // Catch:{ Throwable -> 0x0083 }
            java.lang.String r3 = r3.getAbsolutePath()     // Catch:{ Throwable -> 0x0083 }
            r5 = 1
            com.igexin.push.util.e.a(r3, r14, r5)     // Catch:{ Throwable -> 0x0083 }
            if (r0 != 0) goto L_0x00af
            r0 = r1
        L_0x0060:
            return r0
        L_0x0061:
            r3 = move-exception
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0083 }
            r5.<init>()     // Catch:{ Throwable -> 0x0083 }
            java.lang.String r6 = com.igexin.push.extension.a.f1015a     // Catch:{ Throwable -> 0x0083 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x0083 }
            java.lang.String r6 = "|"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x0083 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0083 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Throwable -> 0x0083 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x0083 }
            com.igexin.b.a.c.a.b(r3)     // Catch:{ Throwable -> 0x0083 }
            goto L_0x004e
        L_0x0083:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = com.igexin.push.extension.a.f1015a
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            boolean r0 = r4.exists()
            if (r0 == 0) goto L_0x00ad
            r4.delete()
        L_0x00ad:
            r0 = r1
            goto L_0x0060
        L_0x00af:
            java.lang.Object r0 = r0.newInstance()     // Catch:{ Throwable -> 0x0083 }
            com.igexin.push.extension.stub.IPushExtension r0 = (com.igexin.push.extension.stub.IPushExtension) r0     // Catch:{ Throwable -> 0x0083 }
            if (r0 == 0) goto L_0x00ad
            android.content.Context r3 = com.igexin.push.core.g.f     // Catch:{ Exception -> 0x00df }
            r0.init(r3)     // Catch:{ Exception -> 0x00df }
            java.util.List<com.igexin.push.extension.stub.IPushExtension> r3 = r9.b     // Catch:{ Exception -> 0x00df }
            r3.add(r0)     // Catch:{ Exception -> 0x00df }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00df }
            r0.<init>()     // Catch:{ Exception -> 0x00df }
            java.lang.String r3 = com.igexin.push.extension.a.f1015a     // Catch:{ Exception -> 0x00df }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x00df }
            java.lang.String r3 = "| [main] extension loaded: "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x00df }
            java.lang.StringBuilder r0 = r0.append(r11)     // Catch:{ Exception -> 0x00df }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00df }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Exception -> 0x00df }
            r0 = r2
            goto L_0x0060
        L_0x00df:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0083 }
            r2.<init>()     // Catch:{ Throwable -> 0x0083 }
            java.lang.String r3 = com.igexin.push.extension.a.f1015a     // Catch:{ Throwable -> 0x0083 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0083 }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0083 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0083 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0083 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0083 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Throwable -> 0x0083 }
            goto L_0x00ad
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.extension.a.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    public void b() {
        for (IPushExtension onDestroy : this.b) {
            onDestroy.onDestroy();
        }
    }

    public List<IPushExtension> c() {
        return this.b;
    }
}
