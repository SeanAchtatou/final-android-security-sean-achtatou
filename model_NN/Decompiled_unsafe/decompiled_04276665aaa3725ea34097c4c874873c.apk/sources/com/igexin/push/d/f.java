package com.igexin.push.d;

import com.igexin.b.a.c.a;
import com.igexin.push.core.g;

public class f implements i {
    private long a(long j) {
        long j2 = j / 10;
        return ((long) (((Math.random() * ((double) j2)) * 2.0d) - ((double) j2))) + j;
    }

    public long a(boolean z) {
        a.b("NormalModel|resetDelay = " + z);
        if (z) {
            g.E = 0;
            a.b("NormalModel|isResetDelay = true, reconnect delayTime = 0");
        }
        boolean a2 = com.igexin.push.util.a.a(System.currentTimeMillis());
        boolean b = com.igexin.push.util.a.b();
        a.b("NormalModel|isSdkOn = " + g.i + " isPushOn = " + g.j + " checkIsSilentTime = " + a2 + " isBlockEndTime = " + b + " isNetworkAvailable = " + g.h);
        if (!g.h || !g.i || !g.j || a2 || !b) {
            a.b("NormalModel|reconnect stop, interval= 1h ++++");
            return 3600000;
        }
        if (g.E <= 0) {
            g.E = 200;
        } else if (g.E <= 10000) {
            g.E += 500;
        } else if (g.E <= StatisticConfig.MIN_UPLOAD_INTERVAL) {
            g.E += 1500;
        } else {
            g.E += 120000;
        }
        if (g.E > 3600000) {
            g.E = 3600000;
        }
        long a3 = a(g.E);
        a.b("NormalModel|after add auto reconnect delay time = " + a3);
        return a3;
    }
}
