package com.igexin.push.d.a;

import com.igexin.b.a.b.b;
import com.igexin.b.a.b.d;
import com.igexin.push.d.c.e;
import com.igexin.push.d.c.h;
import com.igexin.push.d.c.j;
import com.igexin.push.d.c.m;
import com.igexin.push.d.c.o;
import com.igexin.push.d.c.p;
import com.igexin.push.d.c.q;
import org.json.JSONObject;

public class a extends b {
    public a(String str, b bVar) {
        super(str, true);
        a(bVar);
    }

    private boolean a(com.igexin.push.d.c.b bVar, e eVar) {
        String string;
        if (bVar.b != 26) {
            return false;
        }
        p pVar = (p) eVar;
        if (pVar.a() && pVar.e != null) {
            try {
                JSONObject jSONObject = new JSONObject((String) pVar.e);
                if (jSONObject.has("action") && (string = jSONObject.getString("action")) != null && string.equals("redirect_server")) {
                    return true;
                }
            } catch (Exception e) {
                com.igexin.b.a.c.a.b("CommandFilter|" + e.toString());
            }
        }
        return false;
    }

    public Object a(com.igexin.b.a.b.e eVar, d dVar, Object obj) {
        if (obj instanceof e) {
            e eVar2 = (e) obj;
            com.igexin.push.d.c.b bVar = new com.igexin.push.d.c.b();
            bVar.b = (byte) eVar2.i;
            bVar.a(eVar2.d());
            bVar.c = eVar2.j;
            bVar.d = eVar2.k;
            return bVar;
        } else if (!(obj instanceof e[])) {
            return null;
        } else {
            e[] eVarArr = (e[]) obj;
            com.igexin.push.d.c.b[] bVarArr = new com.igexin.push.d.c.b[eVarArr.length];
            for (int i = 0; i < eVarArr.length; i++) {
                bVarArr[i] = new com.igexin.push.d.c.b();
                bVarArr[i].b = (byte) eVarArr[i].i;
                bVarArr[i].a(eVarArr[i].d());
            }
            return bVarArr;
        }
    }

    /* renamed from: b */
    public com.igexin.b.a.d.a.e c(com.igexin.b.a.b.e eVar, d dVar, Object obj) {
        e aVar;
        if (obj == null) {
            return null;
        }
        if (obj instanceof h) {
            return (com.igexin.b.a.d.a.e) obj;
        }
        com.igexin.push.d.c.b bVar = (com.igexin.push.d.c.b) obj;
        switch (bVar.b) {
            case 5:
                aVar = new m();
                break;
            case 9:
                aVar = new q();
                break;
            case 26:
                aVar = new p();
                break;
            case 28:
                aVar = new com.igexin.push.d.c.a();
                break;
            case 37:
                aVar = new o();
                break;
            case 97:
                aVar = new j();
                break;
            default:
                aVar = null;
                break;
        }
        if ((bVar.f != 1 && bVar.f != 7) || aVar == null) {
            return null;
        }
        aVar.a(bVar.e);
        if (bVar.f != 7) {
            if (!a(bVar, aVar)) {
                aVar = null;
            }
            return aVar;
        } else if (bVar.g != 32 || a(bVar, aVar)) {
            return aVar;
        } else {
            return null;
        }
    }
}
