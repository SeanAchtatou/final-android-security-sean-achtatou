package com.igexin.push.d.c;

import com.igexin.b.a.b.f;
import com.igexin.push.util.EncryptUtils;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public int f989a;
    public byte b;
    public byte c;
    public byte d;
    public byte[] e;
    public int f;
    public byte g;

    public void a(byte[] bArr) {
        if (bArr == null) {
            this.f989a = 0;
            return;
        }
        this.e = bArr;
        this.f989a = bArr.length;
    }

    public byte[] a() {
        if (this.e == null) {
            return null;
        }
        byte[] bArr = new byte[(this.f989a + 11)];
        int a2 = f.a(EncryptUtils.getPacketId(), bArr, 0);
        int a3 = a2 + f.a((int) (System.currentTimeMillis() / 1000), bArr, a2);
        int b2 = a3 + f.b(this.f989a, bArr, a3);
        f.a(this.e, 0, bArr, b2 + f.c(this.b, bArr, b2), this.e.length);
        return bArr;
    }
}
