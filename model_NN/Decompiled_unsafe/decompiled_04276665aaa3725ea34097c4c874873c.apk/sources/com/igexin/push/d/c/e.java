package com.igexin.push.d.c;

import com.igexin.b.a.d.a;

public abstract class e extends a {
    public int i;
    public byte j;
    public byte k = 11;

    /* access modifiers changed from: protected */
    public int a(String str) {
        if (str.equals("UTF-8")) {
            return 1;
        }
        if (str.equals("UTF-16")) {
            return 2;
        }
        if (str.equals("UTF-16BE")) {
            return 16;
        }
        if (str.equals("UTF-16LE")) {
            return 17;
        }
        if (str.equals("GBK")) {
            return 25;
        }
        if (str.equals("GB2312")) {
            return 26;
        }
        if (str.equals("GB18030")) {
            return 27;
        }
        return str.equals("ISO-8859-1") ? 33 : 1;
    }

    /* access modifiers changed from: protected */
    public String a(byte b) {
        switch (b & 63) {
            case 1:
                return "UTF-8";
            case 2:
                return "UTF-16";
            case 16:
                return "UTF-16BE";
            case 17:
                return "UTF-16LE";
            case 25:
                return "GBK";
            case 26:
                return "GB2312";
            case 27:
                return "GB18030";
            case 33:
                return "ISO-8859-1";
            default:
                return "UTF-8";
        }
    }

    public abstract void a(byte[] bArr);

    public int b() {
        return this.i;
    }

    public abstract byte[] d();
}
