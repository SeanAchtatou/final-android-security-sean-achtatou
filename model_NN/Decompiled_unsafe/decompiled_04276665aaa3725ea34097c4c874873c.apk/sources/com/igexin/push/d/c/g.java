package com.igexin.push.d.c;

import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class g {

    /* renamed from: a  reason: collision with root package name */
    public int f993a;
    public int b;
    public int c;
    public int d;
    public int e;
    public int f;
    public byte g;
    public byte h;
    public byte i;
    public byte j;
    public byte k;
    public byte l;
    public byte m;
    public byte[] n;
    public int o;
    public int p;
    public int q;

    public int a() {
        this.d |= this.g;
        this.d |= this.h;
        this.d |= this.i;
        return this.d;
    }

    public void a(byte b2) {
        this.d = b2 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
        this.g = (byte) (b2 & 192);
        this.h = (byte) (b2 & 48);
        this.i = (byte) (b2 & 15);
    }

    public int b() {
        this.f |= this.j;
        this.f |= this.k;
        this.f |= this.l;
        this.f |= this.m;
        return this.f;
    }
}
