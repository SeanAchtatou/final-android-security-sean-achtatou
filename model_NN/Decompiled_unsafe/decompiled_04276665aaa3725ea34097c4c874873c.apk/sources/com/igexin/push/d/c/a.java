package com.igexin.push.d.c;

import com.igexin.b.a.b.f;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class a extends e {

    /* renamed from: a  reason: collision with root package name */
    public int f988a;
    public int b;
    public Object c;
    public String d;
    public String e = "UTF-8";
    private int f = 0;
    private int g = 0;

    public a() {
        this.i = 28;
    }

    public int a() {
        return this.f;
    }

    public void a(int i) {
        this.f = i;
    }

    public void a(byte[] bArr) {
        int i;
        this.f988a = f.c(bArr, 0);
        this.b = bArr[2] & 192;
        this.e = a(bArr[2]);
        int i2 = 3;
        int i3 = 0;
        while (true) {
            i = i3 | (bArr[i2] & Byte.MAX_VALUE);
            if ((bArr[i2] & 128) == 0) {
                break;
            }
            i3 = i << 7;
            i2++;
        }
        int i4 = i2 + 1;
        if (i > 0) {
            if (this.b == 192) {
                this.c = new byte[i];
                System.arraycopy(bArr, i4, this.c, 0, i);
            } else {
                try {
                    this.c = new String(bArr, i4, i, this.e);
                } catch (Exception e2) {
                }
            }
        }
        int i5 = i + i4;
        byte b2 = bArr[i5] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
        int i6 = i5 + 1;
        if (bArr.length > i6) {
            try {
                this.d = new String(bArr, i6, b2, this.e);
            } catch (Exception e3) {
            }
            int i7 = i6 + b2;
        }
    }

    public void b(int i) {
        this.g = i;
    }

    public int c() {
        return this.g;
    }

    public byte[] d() {
        int i = 0;
        try {
            byte[] bytes = this.d.getBytes(this.e);
            byte[] bytes2 = !"".equals(this.c) ? this.b == 192 ? (byte[]) this.c : ((String) this.c).getBytes(this.e) : null;
            if (bytes2 != null) {
                i = bytes2.length;
            }
            byte[] a2 = f.a(i);
            byte[] bArr = new byte[(a2.length + 4 + i + bytes.length)];
            try {
                int b2 = f.b(this.f988a, bArr, 0);
                int c2 = b2 + f.c(this.b | a(this.e), bArr, b2);
                int a3 = c2 + f.a(a2, 0, bArr, c2, a2.length);
                if (i > 0) {
                    a3 += f.a(bytes2, 0, bArr, a3, i);
                }
                int c3 = a3 + f.c(bytes.length, bArr, a3);
                int a4 = c3 + f.a(bytes, 0, bArr, c3, bytes.length);
                return bArr;
            } catch (Exception e2) {
                return bArr;
            }
        } catch (Exception e3) {
            return null;
        }
    }
}
