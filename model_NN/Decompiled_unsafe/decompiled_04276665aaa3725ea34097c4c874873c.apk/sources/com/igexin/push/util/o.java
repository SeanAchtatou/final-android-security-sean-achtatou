package com.igexin.push.util;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.LinkedBlockingQueue;

final class o implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    boolean f1034a;
    private final LinkedBlockingQueue<IBinder> b;

    private o() {
        this.f1034a = false;
        this.b = new LinkedBlockingQueue<>(1);
    }

    public IBinder a() {
        if (this.f1034a) {
            throw new IllegalStateException();
        }
        this.f1034a = true;
        return this.b.take();
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            this.b.put(iBinder);
        } catch (Exception e) {
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
    }
}
