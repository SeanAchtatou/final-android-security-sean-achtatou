package com.igexin.push.util;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

final class p implements IInterface {

    /* renamed from: a  reason: collision with root package name */
    private IBinder f1035a;

    public p(IBinder iBinder) {
        this.f1035a = iBinder;
    }

    public String a() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        String str = null;
        try {
            obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
            this.f1035a.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            str = obtain2.readString();
        } catch (Exception e) {
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
        return str;
    }

    public IBinder asBinder() {
        return this.f1035a;
    }
}
