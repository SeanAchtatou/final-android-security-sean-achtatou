package com.igexin.download;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.lang.reflect.Method;

public class e implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadService f842a;

    public e(DownloadService downloadService) {
        this.f842a = downloadService;
    }

    public void a() {
        synchronized (this.f842a) {
            if (this.f842a.h != null) {
                Object unused = this.f842a.h = (Object) null;
                try {
                    this.f842a.unbindService(this);
                } catch (IllegalArgumentException e) {
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.download.DownloadService.a(com.igexin.download.DownloadService, boolean):boolean
     arg types: [com.igexin.download.DownloadService, int]
     candidates:
      com.igexin.download.DownloadService.a(int, long):long
      com.igexin.download.DownloadService.a(com.igexin.download.DownloadService, android.database.CharArrayBuffer):android.database.CharArrayBuffer
      com.igexin.download.DownloadService.a(com.igexin.download.DownloadService, com.igexin.download.f):com.igexin.download.f
      com.igexin.download.DownloadService.a(com.igexin.download.DownloadService, java.lang.Object):java.lang.Object
      com.igexin.download.DownloadService.a(com.igexin.download.DownloadService, int):void
      com.igexin.download.DownloadService.a(android.database.Cursor, int):boolean
      com.igexin.download.DownloadService.a(com.igexin.download.DownloadService, boolean):boolean */
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        boolean unused = this.f842a.g = false;
        synchronized (this.f842a) {
            try {
                Method method = Class.forName("android.media.IMediaScannerService").getField("Stub").getType().getMethod("asInterface", IBinder.class);
                Object unused2 = this.f842a.h = method.invoke(null, iBinder);
                if (this.f842a.h != null) {
                    this.f842a.a();
                }
            } catch (Exception e) {
            }
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.f842a) {
            Object unused = this.f842a.h = (Object) null;
        }
    }
}
