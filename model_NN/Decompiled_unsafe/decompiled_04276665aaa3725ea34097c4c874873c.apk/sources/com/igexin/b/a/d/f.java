package com.igexin.b.a.d;

import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

final class f {

    /* renamed from: a  reason: collision with root package name */
    final BlockingQueue<d> f813a = new SynchronousQueue();
    final HashMap<Integer, g> b = new HashMap<>();
    final ReentrantLock c = new ReentrantLock();
    ThreadFactory d = new h(this);
    volatile long e = TimeUnit.SECONDS.toNanos(60);
    volatile int f = 0;
    volatile int g;
    volatile int h = Integer.MAX_VALUE;
    final /* synthetic */ e i;

    public f(e eVar) {
        this.i = eVar;
    }

    /* access modifiers changed from: package-private */
    public final d a() {
        while (true) {
            try {
                d poll = this.g > this.f ? this.f813a.poll(this.e, TimeUnit.NANOSECONDS) : this.f813a.take();
                if (poll != null) {
                    return poll;
                }
                if (this.f813a.isEmpty()) {
                    return null;
                }
            } catch (InterruptedException e2) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(d dVar) {
        if (dVar == null) {
            throw new NullPointerException();
        }
        if (dVar.z != 0) {
            ReentrantLock reentrantLock = this.c;
            reentrantLock.lock();
            try {
                g gVar = this.b.get(Integer.valueOf(dVar.z));
                if (gVar != null) {
                    gVar.f814a.offer(dVar);
                    return;
                }
                reentrantLock.unlock();
            } finally {
                reentrantLock.unlock();
            }
        }
        b(dVar);
    }

    /* access modifiers changed from: package-private */
    public final boolean a(g gVar) {
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            int i2 = this.g - 1;
            this.g = i2;
            if (i2 == 0 && !this.f813a.isEmpty()) {
                Thread f2 = f(null);
                if (f2 != null) {
                    f2.start();
                }
            } else if (!gVar.f814a.isEmpty()) {
                reentrantLock.unlock();
                return true;
            }
            this.b.remove(Integer.valueOf(gVar.d));
            return false;
        } finally {
            reentrantLock.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(d dVar) {
        if (this.g < this.f && c(dVar)) {
            return;
        }
        if (!this.f813a.offer(dVar)) {
            if (!d(dVar)) {
            }
        } else if (this.g == 0) {
            e(dVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean c(d dVar) {
        Thread thread = null;
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            if (this.g < this.f) {
                thread = f(dVar);
            }
            if (thread == null) {
                return false;
            }
            thread.start();
            return true;
        } finally {
            reentrantLock.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean d(d dVar) {
        Thread thread = null;
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            if (this.g < this.h) {
                thread = f(dVar);
            }
            if (thread == null) {
                return false;
            }
            thread.start();
            return true;
        } finally {
            reentrantLock.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final void e(d dVar) {
        Thread thread = null;
        ReentrantLock reentrantLock = this.c;
        reentrantLock.lock();
        try {
            if (this.g < Math.max(this.f, 1) && !this.f813a.isEmpty()) {
                thread = f(null);
            }
            if (thread != null) {
                thread.start();
            }
        } finally {
            reentrantLock.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final Thread f(d dVar) {
        g gVar = new g(this, dVar);
        if (!(dVar == null || dVar.z == 0)) {
            this.b.put(Integer.valueOf(dVar.z), gVar);
        }
        Thread newThread = this.d.newThread(gVar);
        if (newThread != null) {
            this.g++;
        }
        return newThread;
    }
}
