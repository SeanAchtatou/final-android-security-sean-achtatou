package com.igexin.b.a.b.a.a;

import android.text.TextUtils;
import com.igexin.b.a.b.a.a.a.d;
import com.igexin.b.a.b.f;
import com.igexin.b.a.c.a;
import com.igexin.push.c.i;
import com.igexin.push.config.SDKUrlConfig;
import java.net.InetSocketAddress;
import java.net.Socket;

public final class c extends a {
    private static final String i = c.class.getName();
    private Socket L;
    private d j;

    public c(d dVar) {
        super(-2037, null, null);
        this.j = dVar;
    }

    public void a_() {
        super.a_();
        i.a().e().a();
        String cmAddress = SDKUrlConfig.getCmAddress();
        try {
            String[] a2 = f.a(cmAddress);
            String str = a2[1];
            int parseInt = Integer.parseInt(a2[2]);
            a.b(i + "|start connect :  " + cmAddress + " *********");
            if (this.j != null) {
                this.j.a(cmAddress);
            }
            this.L = new Socket();
            try {
                this.L.connect(new InetSocketAddress(str, parseInt), 10000);
                a.b(i + "|connected :  " + cmAddress + " #########");
                if (this.f != b.INTERRUPT) {
                    this.f = b.NORMAL;
                }
            } catch (Exception e) {
                if (this.f != b.INTERRUPT) {
                    this.f = b.EXCEPTION;
                    this.g = e.toString();
                }
            }
            this.e = true;
        } catch (Exception e2) {
            a.b(i + "|ips invalid, " + e2.toString());
            throw e2;
        }
    }

    public final int b() {
        return -2037;
    }

    public void f() {
        super.f();
        a.b(i + "|dispose");
        if (this.j != null) {
            if (this.f == b.INTERRUPT) {
                this.j.a(this);
            } else if (this.f == b.EXCEPTION) {
                if (!TextUtils.isEmpty(this.g)) {
                    this.j.a(new Exception(this.g));
                }
            } else if (this.f == b.NORMAL && this.L != null) {
                this.j.a(this.L);
            }
        }
        this.j = null;
    }

    public void h() {
        this.f = b.INTERRUPT;
        try {
            if (this.L != null && !this.e) {
                this.L.notifyAll();
            }
        } catch (Exception e) {
        }
    }
}
