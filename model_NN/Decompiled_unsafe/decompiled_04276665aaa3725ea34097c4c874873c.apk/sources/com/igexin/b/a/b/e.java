package com.igexin.b.a.b;

import com.igexin.b.a.d.d;

public abstract class e extends d {

    /* renamed from: a  reason: collision with root package name */
    public String f805a;
    public b b;
    public Object c;
    public d d;

    public e(int i, String str, b bVar) {
        super(i);
        if (str != null) {
            this.f805a = a(str);
        }
        this.b = bVar;
    }

    public e(String str, b bVar) {
        this(0, str, bVar);
    }

    private String a(String str) {
        return f.a(f.a(str));
    }

    public void f() {
        if (this.b != null) {
            this.b.a(false);
        }
        this.b = null;
        this.d = null;
        this.c = null;
        this.f805a = null;
        super.f();
    }
}
