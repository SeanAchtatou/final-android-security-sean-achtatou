package com.igexin.b.a.b;

import android.support.v4.media.TransportMediator;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public final class f {
    public static int a(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) ((i >> 24) & 255);
        bArr[i2 + 1] = (byte) ((i >> 16) & 255);
        bArr[i2 + 2] = (byte) ((i >> 8) & 255);
        bArr[i2 + 3] = (byte) (i & 255);
        return 4;
    }

    public static int a(long j, byte[] bArr, int i) {
        bArr[i] = (byte) ((int) ((j >> 56) & 255));
        bArr[i + 1] = (byte) ((int) ((j >> 48) & 255));
        bArr[i + 2] = (byte) ((int) ((j >> 40) & 255));
        bArr[i + 3] = (byte) ((int) ((j >> 32) & 255));
        bArr[i + 4] = (byte) ((int) ((j >> 24) & 255));
        bArr[i + 5] = (byte) ((int) ((j >> 16) & 255));
        bArr[i + 6] = (byte) ((int) ((j >> 8) & 255));
        bArr[i + 7] = (byte) ((int) (255 & j));
        return 8;
    }

    public static int a(byte[] bArr, int i) {
        return bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
    }

    public static int a(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        System.arraycopy(bArr, i, bArr2, i2, i3);
        return i3;
    }

    public static String a(String[] strArr) {
        StringBuilder sb = new StringBuilder();
        if (!strArr[0].equals("")) {
            sb.append(strArr[0]).append("://");
        }
        if (!strArr[1].equals("")) {
            sb.append(strArr[1]);
        }
        if (!strArr[2].equals("")) {
            sb.append(':').append(strArr[2]);
        }
        if (!strArr[3].equals("")) {
            sb.append(strArr[3]);
            if (!strArr[3].equals("/")) {
                sb.append('/');
            }
        }
        if (!strArr[4].equals("")) {
            sb.append(strArr[4]);
        }
        if (!strArr[5].equals("")) {
            sb.append('?').append(strArr[5]);
        }
        return sb.toString();
    }

    private static void a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public static void a(InputStream inputStream, OutputStream outputStream, int i) {
        a aVar = new a(outputStream, i);
        a(inputStream, aVar);
        aVar.a();
    }

    public static byte[] a(int i) {
        int i2 = 0;
        int i3 = 0;
        do {
            int i4 = ((i & TransportMediator.KEYCODE_MEDIA_PAUSE) << 24) | i2;
            i >>>= 7;
            i3++;
            if (i > 0) {
                i2 = (i4 >>> 8) | Integer.MIN_VALUE;
                continue;
            } else {
                i2 = i4;
                continue;
            }
        } while (i > 0);
        byte[] bArr = new byte[i3];
        int i5 = 24;
        for (int i6 = 0; i6 < i3; i6++) {
            bArr[i6] = (byte) (i2 >>> i5);
            i5 -= 8;
        }
        return bArr;
    }

    public static byte[] a(byte[] bArr) {
        return c(bArr);
    }

    public static String[] a(String str) {
        StringBuilder sb = new StringBuilder(str.toLowerCase());
        String[] strArr = new String[6];
        for (int i = 0; i < 6; i++) {
            strArr[i] = "";
        }
        int indexOf = str.indexOf(":");
        if (indexOf > 0) {
            strArr[0] = str.substring(0, indexOf);
            sb.delete(0, indexOf + 1);
        } else if (indexOf == 0) {
            throw new IllegalArgumentException("url format error - protocol");
        }
        if (sb.length() >= 2 && sb.charAt(0) == '/' && sb.charAt(1) == '/') {
            sb.delete(0, 2);
            int indexOf2 = sb.toString().indexOf(47);
            if (indexOf2 < 0) {
                indexOf2 = sb.length();
            }
            if (indexOf2 != 0) {
                int indexOf3 = sb.toString().indexOf(58);
                if (indexOf3 < 0) {
                    indexOf3 = indexOf2;
                } else if (indexOf3 > indexOf2) {
                    throw new IllegalArgumentException("url format error - port");
                } else {
                    strArr[2] = sb.toString().substring(indexOf3 + 1, indexOf2);
                }
                strArr[1] = sb.toString().substring(0, indexOf3);
                sb.delete(0, indexOf2);
            }
        }
        if (sb.length() > 0) {
            String sb2 = sb.toString();
            int lastIndexOf = sb2.lastIndexOf(47);
            if (lastIndexOf > 0) {
                strArr[3] = sb2.substring(0, lastIndexOf);
            } else if (lastIndexOf == 0) {
                if (sb2.indexOf(63) > 0) {
                    throw new IllegalArgumentException("url format error - path");
                }
                strArr[3] = sb2;
                return strArr;
            }
            if (lastIndexOf < sb2.length() - 1) {
                String substring = sb2.substring(lastIndexOf + 1, sb2.length());
                int indexOf4 = substring.indexOf(63);
                if (indexOf4 >= 0) {
                    strArr[4] = substring.substring(0, indexOf4);
                    strArr[5] = substring.substring(indexOf4 + 1);
                } else {
                    strArr[4] = substring;
                }
            }
        } else {
            strArr[3] = "/";
        }
        return strArr;
    }

    public static int b(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) ((i >> 8) & 255);
        bArr[i2 + 1] = (byte) (i & 255);
        return 2;
    }

    public static short b(byte[] bArr, int i) {
        return (short) (((bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[i + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD));
    }

    public static byte[] b(int i) {
        return new byte[]{(byte) ((i >> 24) & 255), (byte) ((i >> 16) & 255), (byte) ((i >> 8) & 255), (byte) (i & 255)};
    }

    public static byte[] b(byte[] bArr) {
        return d(bArr);
    }

    public static int c(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) i;
        return 1;
    }

    public static int c(byte[] bArr, int i) {
        return ((bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[i + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0031 A[SYNTHETIC, Splitter:B:22:0x0031] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] c(byte[] r5) {
        /*
            r0 = 0
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream
            r2.<init>()
            java.util.zip.GZIPOutputStream r1 = new java.util.zip.GZIPOutputStream     // Catch:{ Throwable -> 0x001e, all -> 0x002b }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x001e, all -> 0x002b }
            r1.write(r5)     // Catch:{ Throwable -> 0x0044, all -> 0x0042 }
            r1.finish()     // Catch:{ Throwable -> 0x0044, all -> 0x0042 }
            byte[] r0 = r2.toByteArray()     // Catch:{ Throwable -> 0x0044, all -> 0x0042 }
            if (r1 == 0) goto L_0x001a
            r1.close()     // Catch:{ Exception -> 0x0038 }
        L_0x001a:
            r2.close()     // Catch:{ Exception -> 0x003a }
        L_0x001d:
            return r0
        L_0x001e:
            r1 = move-exception
            r1 = r0
        L_0x0020:
            if (r1 == 0) goto L_0x0025
            r1.close()     // Catch:{ Exception -> 0x003c }
        L_0x0025:
            r2.close()     // Catch:{ Exception -> 0x0029 }
            goto L_0x001d
        L_0x0029:
            r1 = move-exception
            goto L_0x001d
        L_0x002b:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x002f:
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ Exception -> 0x003e }
        L_0x0034:
            r2.close()     // Catch:{ Exception -> 0x0040 }
        L_0x0037:
            throw r0
        L_0x0038:
            r1 = move-exception
            goto L_0x001a
        L_0x003a:
            r1 = move-exception
            goto L_0x001d
        L_0x003c:
            r1 = move-exception
            goto L_0x0025
        L_0x003e:
            r1 = move-exception
            goto L_0x0034
        L_0x0040:
            r1 = move-exception
            goto L_0x0037
        L_0x0042:
            r0 = move-exception
            goto L_0x002f
        L_0x0044:
            r3 = move-exception
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.b.a.b.f.c(byte[]):byte[]");
    }

    public static int d(byte[] bArr, int i) {
        return ((bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24) | ((bArr[i + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr[i + 2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[i + 3] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x001e A[SYNTHETIC, Splitter:B:11:0x001e] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0023 A[SYNTHETIC, Splitter:B:14:0x0023] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0045 A[SYNTHETIC, Splitter:B:33:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x004a A[SYNTHETIC, Splitter:B:36:0x004a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] d(byte[] r7) {
        /*
            r0 = 0
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream
            r3.<init>(r7)
            java.util.zip.GZIPInputStream r2 = new java.util.zip.GZIPInputStream     // Catch:{ Throwable -> 0x0068, all -> 0x003e }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0068, all -> 0x003e }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x006c, all -> 0x0061 }
            r1.<init>()     // Catch:{ Throwable -> 0x006c, all -> 0x0061 }
        L_0x0010:
            int r4 = r2.read()     // Catch:{ Throwable -> 0x001b, all -> 0x0066 }
            r5 = -1
            if (r4 == r5) goto L_0x002a
            r1.write(r4)     // Catch:{ Throwable -> 0x001b, all -> 0x0066 }
            goto L_0x0010
        L_0x001b:
            r4 = move-exception
        L_0x001c:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ Exception -> 0x0055 }
        L_0x0021:
            if (r2 == 0) goto L_0x0026
            r2.close()     // Catch:{ Exception -> 0x0057 }
        L_0x0026:
            r3.close()     // Catch:{ Exception -> 0x0059 }
        L_0x0029:
            return r0
        L_0x002a:
            byte[] r0 = r1.toByteArray()     // Catch:{ Throwable -> 0x001b, all -> 0x0066 }
            if (r1 == 0) goto L_0x0033
            r1.close()     // Catch:{ Exception -> 0x0051 }
        L_0x0033:
            if (r2 == 0) goto L_0x0038
            r2.close()     // Catch:{ Exception -> 0x0053 }
        L_0x0038:
            r3.close()     // Catch:{ Exception -> 0x003c }
            goto L_0x0029
        L_0x003c:
            r1 = move-exception
            goto L_0x0029
        L_0x003e:
            r1 = move-exception
            r2 = r0
            r6 = r0
            r0 = r1
            r1 = r6
        L_0x0043:
            if (r1 == 0) goto L_0x0048
            r1.close()     // Catch:{ Exception -> 0x005b }
        L_0x0048:
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ Exception -> 0x005d }
        L_0x004d:
            r3.close()     // Catch:{ Exception -> 0x005f }
        L_0x0050:
            throw r0
        L_0x0051:
            r1 = move-exception
            goto L_0x0033
        L_0x0053:
            r1 = move-exception
            goto L_0x0038
        L_0x0055:
            r1 = move-exception
            goto L_0x0021
        L_0x0057:
            r1 = move-exception
            goto L_0x0026
        L_0x0059:
            r1 = move-exception
            goto L_0x0029
        L_0x005b:
            r1 = move-exception
            goto L_0x0048
        L_0x005d:
            r1 = move-exception
            goto L_0x004d
        L_0x005f:
            r1 = move-exception
            goto L_0x0050
        L_0x0061:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0043
        L_0x0066:
            r0 = move-exception
            goto L_0x0043
        L_0x0068:
            r1 = move-exception
            r1 = r0
            r2 = r0
            goto L_0x001c
        L_0x006c:
            r1 = move-exception
            r1 = r0
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.b.a.b.f.d(byte[]):byte[]");
    }

    public static long e(byte[] bArr, int i) {
        return ((((long) bArr[i]) & 255) << 56) | ((((long) bArr[i + 1]) & 255) << 48) | ((((long) bArr[i + 2]) & 255) << 40) | ((((long) bArr[i + 3]) & 255) << 32) | ((((long) bArr[i + 4]) & 255) << 24) | ((((long) bArr[i + 5]) & 255) << 16) | ((((long) bArr[i + 6]) & 255) << 8) | (((long) bArr[i + 7]) & 255);
    }

    public static byte[] f(byte[] bArr, int i) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(byteArrayInputStream, byteArrayOutputStream, i);
            try {
                byteArrayInputStream.close();
            } catch (Throwable th) {
            }
            try {
                byteArrayOutputStream.close();
            } catch (Throwable th2) {
            }
            return byteArrayOutputStream.toByteArray();
        } catch (Throwable th3) {
            try {
                byteArrayInputStream.close();
            } catch (Throwable th4) {
            }
            byteArrayOutputStream.close();
            throw th3;
        }
    }
}
