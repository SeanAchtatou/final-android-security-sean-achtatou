package com.igexin.b.a.b.a.a;

import java.io.BufferedOutputStream;
import java.io.OutputStream;

public class n {

    /* renamed from: a  reason: collision with root package name */
    private BufferedOutputStream f803a;

    public n(OutputStream outputStream) {
        this.f803a = new BufferedOutputStream(outputStream);
    }

    public void a() {
        this.f803a.close();
    }

    public void a(byte[] bArr) {
        this.f803a.write(bArr, 0, bArr.length);
        this.f803a.flush();
    }
}
