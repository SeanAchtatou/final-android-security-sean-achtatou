package com.igexin.b.a.b.a.a;

import android.text.TextUtils;
import com.igexin.b.a.b.a.a.a.b;
import com.igexin.b.a.b.d;
import com.igexin.b.a.c.a;

public final class j extends a {
    private static final String L = j.class.getName();
    private b M;
    private byte[] N;
    private d O;
    m i;
    com.igexin.b.a.b.b j;

    public j(m mVar, com.igexin.b.a.b.b bVar, d dVar) {
        super(-2035, null, bVar);
        this.j = bVar;
        this.i = mVar;
        this.O = dVar;
    }

    public void a(b bVar) {
        this.M = bVar;
    }

    public void a_() {
        super.a_();
        Thread currentThread = Thread.currentThread();
        a.b(L + "|" + currentThread + " running");
        while (this.h && !currentThread.isInterrupted() && !this.e) {
            try {
                this.j.c(null, this.O, this.i);
                this.f = b.NORMAL;
            } catch (Throwable th) {
                this.h = false;
                if (this.f != b.INTERRUPT) {
                    this.f = b.EXCEPTION;
                    if (th.getMessage() == null || !th.getMessage().equals("read = -1, end of stream !")) {
                        this.g = th.toString();
                    } else {
                        this.g = "end of stream";
                    }
                }
            }
        }
        this.e = true;
        a.b(L + "|finish ~~~~~~");
    }

    public final int b() {
        return -2035;
    }

    public void f() {
        super.f();
        a.b(L + "|dispose");
        if (this.M != null) {
            if (this.f == b.INTERRUPT) {
                this.M.a(this);
            } else if (this.f == b.EXCEPTION && !TextUtils.isEmpty(this.g)) {
                this.M.a(new Exception(this.g));
            }
        }
        if (this.N != null) {
            this.N = null;
        }
        this.M = null;
    }

    public void h() {
        this.h = false;
        this.f = b.INTERRUPT;
        try {
            if (!this.e && this.i.f802a != null) {
                this.i.f802a.notifyAll();
            }
        } catch (Exception e) {
        }
    }
}
