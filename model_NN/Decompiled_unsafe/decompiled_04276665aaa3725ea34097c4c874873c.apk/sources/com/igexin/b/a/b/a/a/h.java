package com.igexin.b.a.b.a.a;

import java.util.Comparator;

class h implements Comparator<k> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f800a;

    h(d dVar) {
        this.f800a = dVar;
    }

    /* renamed from: a */
    public int compare(k kVar, k kVar2) {
        if (kVar == null) {
            return 1;
        }
        if (kVar2 == null) {
            return -1;
        }
        if (((long) kVar.y) + kVar.w <= ((long) kVar2.y) + kVar2.w) {
            return ((long) kVar.y) + kVar.w < ((long) kVar2.y) + kVar2.w ? -1 : 0;
        }
        return 1;
    }
}
