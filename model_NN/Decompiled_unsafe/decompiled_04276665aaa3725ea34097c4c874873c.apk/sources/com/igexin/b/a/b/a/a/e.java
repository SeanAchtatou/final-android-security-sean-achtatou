package com.igexin.b.a.b.a.a;

import android.os.Message;
import com.igexin.b.a.b.a.a.a.d;
import com.igexin.b.a.c.a;
import java.net.Socket;

class e implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f797a;

    e(d dVar) {
        this.f797a = dVar;
    }

    public void a(com.igexin.b.a.b.e eVar) {
        this.f797a.o.sendEmptyMessage(3);
    }

    public void a(Exception exc) {
        a.b(d.e + "|c ex = " + exc.toString());
        this.f797a.b();
    }

    public void a(String str) {
        this.f797a.o.sendEmptyMessage(1);
    }

    public void a(Socket socket) {
        Message obtain = Message.obtain();
        obtain.obj = socket;
        obtain.what = 2;
        this.f797a.o.sendMessage(obtain);
    }
}
