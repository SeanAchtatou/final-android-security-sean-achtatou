package com.igexin.b.b;

import android.content.Context;
import com.igexin.b.a.b.f;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f817a = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    public static String a(int i) {
        int i2 = i < 0 ? 100 : i;
        if (i2 > 100) {
            i2 = 10;
        }
        Random random = new Random();
        char[] cArr = new char[i2];
        for (int i3 = 0; i3 < i2; i3++) {
            cArr[i3] = f817a[random.nextInt(f817a.length)];
        }
        return new String(cArr);
    }

    public static String a(Context context, String str) {
        File file = new File(str);
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            if (instance == null) {
                return "0";
            }
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] bArr = new byte[10240];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read <= 0) {
                        byte[] digest = instance.digest();
                        String a2 = a(digest, 0, digest.length);
                        fileInputStream.close();
                        return a2;
                    } else if (read < 10240) {
                        byte[] bArr2 = new byte[read];
                        System.arraycopy(bArr, 0, bArr2, 0, read);
                        instance.update(bArr2);
                    } else {
                        instance.update(bArr);
                    }
                }
            } catch (IOException e) {
                return "0";
            }
        } catch (NoSuchAlgorithmException e2) {
            return "0";
        }
    }

    public static String a(String str) {
        MessageDigest messageDigest;
        int i = 0;
        byte[] bytes = str.getBytes();
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            messageDigest = null;
        }
        if (messageDigest == null) {
            return null;
        }
        messageDigest.update(bytes);
        byte[] digest = messageDigest.digest();
        char[] cArr2 = new char[32];
        for (int i2 = 0; i2 < 16; i2++) {
            byte b = digest[i2];
            int i3 = i + 1;
            cArr2[i] = cArr[(b >>> 4) & 15];
            i = i3 + 1;
            cArr2[i3] = cArr[b & 15];
        }
        return new String(cArr2);
    }

    public static String a(byte[] bArr, int i, int i2) {
        StringBuilder sb = new StringBuilder();
        for (int i3 = i; i3 < i + i2; i3++) {
            sb.append(String.format("%02X", Byte.valueOf(bArr[i3])));
        }
        return sb.toString();
    }

    public static boolean a(String str, String str2) {
        return str == null ? str2 == null : str.equals(str2);
    }

    public static byte[] a(byte[] bArr) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            messageDigest = null;
        }
        if (messageDigest == null) {
            return null;
        }
        messageDigest.update(bArr);
        return messageDigest.digest();
    }

    public static byte[] b(byte[] bArr) {
        byte[] c;
        if (bArr == null || (c = f.c(bArr)) == null) {
            return null;
        }
        String a2 = a(String.valueOf(System.currentTimeMillis()));
        int length = c.length;
        byte[] bArr2 = new byte[(length + 16)];
        byte[] bytes = a2.substring(0, 8).getBytes();
        byte[] bytes2 = a2.substring(24, 32).getBytes();
        System.arraycopy(bytes, 0, bArr2, 0, 8);
        System.arraycopy(c, 0, bArr2, 8, length);
        System.arraycopy(bytes2, 0, bArr2, length + 8, 8);
        return bArr2;
    }

    public static byte[] c(byte[] bArr) {
        if (bArr == null || bArr.length < 16) {
            return null;
        }
        byte[] bArr2 = new byte[(bArr.length - 16)];
        System.arraycopy(bArr, 8, bArr2, 0, bArr.length - 16);
        return f.d(bArr2);
    }
}
