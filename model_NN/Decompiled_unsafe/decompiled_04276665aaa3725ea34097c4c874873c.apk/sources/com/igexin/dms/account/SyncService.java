package com.igexin.dms.account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class SyncService extends Service {
    private static final String TAG = "GTSync";
    private d syncAdapter;

    public IBinder onBind(Intent intent) {
        return this.syncAdapter.getSyncAdapterBinder();
    }

    public void onCreate() {
        super.onCreate();
        this.syncAdapter = new d(this, getApplicationContext(), true);
    }
}
