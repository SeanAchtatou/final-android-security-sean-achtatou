package com.igexin.dms.a;

public class b {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f820a = (!b.class.desiredAssertionStatus());

    private b() {
    }

    public static byte[] a(String str, int i) {
        return a(str.getBytes(), i);
    }

    public static byte[] a(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        d dVar = new d(i3, new byte[((i2 * 3) / 4)]);
        if (!dVar.a(bArr, i, i2, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (dVar.b == dVar.f821a.length) {
            return dVar.f821a;
        } else {
            byte[] bArr2 = new byte[dVar.b];
            System.arraycopy(dVar.f821a, 0, bArr2, 0, dVar.b);
            return bArr2;
        }
    }

    public static byte[] b(byte[] bArr, int i) {
        return b(bArr, 0, bArr.length, i);
    }

    public static byte[] b(byte[] bArr, int i, int i2, int i3) {
        e eVar = new e(i3, null);
        int i4 = (i2 / 3) * 4;
        if (!eVar.d) {
            switch (i2 % 3) {
                case 1:
                    i4 += 2;
                    break;
                case 2:
                    i4 += 3;
                    break;
            }
        } else if (i2 % 3 > 0) {
            i4 += 4;
        }
        if (eVar.e && i2 > 0) {
            i4 += (eVar.f ? 2 : 1) * (((i2 - 1) / 57) + 1);
        }
        eVar.f821a = new byte[i4];
        eVar.a(bArr, i, i2, true);
        if (f820a || eVar.b == i4) {
            return eVar.f821a;
        }
        throw new AssertionError();
    }
}
