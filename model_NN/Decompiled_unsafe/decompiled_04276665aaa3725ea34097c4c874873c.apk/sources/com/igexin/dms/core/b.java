package com.igexin.dms.core;

import android.content.Context;
import android.content.Intent;
import com.igexin.dms.a.a;
import com.igexin.dms.a.f;
import com.igexin.dms.components.GTAReceiver;
import java.util.TimerTask;

class b extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f829a;
    final /* synthetic */ a b;

    b(a aVar, Context context) {
        this.b = aVar;
        this.f829a = context;
    }

    public void run() {
        try {
            this.b.c(this.f829a);
            f.a(this.b.b, "isAvailable:" + e.f832a + " dmThresholdTimes:" + e.b + " delayTime:" + e.c + " dmMinInterval:" + e.d);
            if (!a.b(this.f829a)) {
                f.a(this.b.b, "isTurnOn:false");
            } else if (a.a(this.f829a) == null) {
                f.a(this.b.b, "serviceClass == null");
            } else {
                Intent intent = new Intent();
                intent.setClass(this.f829a, GTAReceiver.class);
                intent.putExtra("last_dm_time", a.f828a);
                this.f829a.sendBroadcast(intent);
                this.b.c();
                this.b.a(this.f829a, GTAReceiver.class.getCanonicalName());
                c cVar = new c(this);
                cVar.setPriority(10);
                cVar.start();
                a.c(this.f829a);
            }
        } catch (Throwable th) {
            f.a(this.b.b, th.toString());
        }
    }
}
