package com.igexin.dms.core;

import android.content.Context;
import com.igexin.dms.a.f;
import java.io.File;

class d extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f831a;
    final /* synthetic */ a b;

    d(a aVar, Context context) {
        this.b = aVar;
        this.f831a = context;
    }

    public void run() {
        try {
            File dir = this.f831a.getDir("dms", 0);
            new NativeCaller().doDaemon(new File(dir, "lock_dm").getAbsolutePath(), new File(dir, "lock_gt").getAbsolutePath(), new File(dir, "observer_dm").getAbsolutePath(), new File(dir, "observer_gt").getAbsolutePath());
        } catch (Throwable th) {
            f.a(this.b.b, th.toString());
        }
    }
}
