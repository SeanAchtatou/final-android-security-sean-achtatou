package com.igexin.sdk;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.igexin.assist.sdk.AssistPushConsts;
import com.igexin.b.a.c.a;
import com.igexin.push.util.e;
import com.igexin.push.util.q;
import com.igexin.sdk.a.c;
import com.xiaomi.mipush.sdk.MiPushClient;

public class PushManager {

    /* renamed from: a  reason: collision with root package name */
    private long f1039a;
    private long b;
    private long c;
    private byte[] d;

    private PushManager() {
    }

    private Class a(Context context) {
        return GTServiceManager.getInstance().getUserPushService(context);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.lang.String r7) {
        /*
            r6 = this;
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ Exception -> 0x003c }
            byte[] r1 = r7.getBytes()     // Catch:{ Exception -> 0x003c }
            r0.update(r1)     // Catch:{ Exception -> 0x003c }
            byte[] r2 = r0.digest()     // Catch:{ Exception -> 0x003c }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003c }
            java.lang.String r0 = ""
            r3.<init>(r0)     // Catch:{ Exception -> 0x003c }
            int r4 = r2.length     // Catch:{ Exception -> 0x003c }
            r0 = 0
            r1 = r0
        L_0x001b:
            if (r1 >= r4) goto L_0x0037
            byte r0 = r2[r1]     // Catch:{ Exception -> 0x003c }
            if (r0 >= 0) goto L_0x0023
            int r0 = r0 + 256
        L_0x0023:
            r5 = 16
            if (r0 >= r5) goto L_0x002c
            java.lang.String r5 = "0"
            r3.append(r5)     // Catch:{ Exception -> 0x003c }
        L_0x002c:
            java.lang.String r0 = java.lang.Integer.toHexString(r0)     // Catch:{ Exception -> 0x003c }
            r3.append(r0)     // Catch:{ Exception -> 0x003c }
            int r0 = r1 + 1
            r1 = r0
            goto L_0x001b
        L_0x0037:
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x003c }
        L_0x003b:
            return r0
        L_0x003c:
            r0 = move-exception
            r0 = 0
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.sdk.PushManager.a(java.lang.String):java.lang.String");
    }

    private void a(Context context, Bundle bundle) {
        try {
            Intent intent = new Intent(context.getApplicationContext(), a(context));
            intent.putExtra("action", PushConsts.ACTION_BROADCAST_PUSHMANAGER);
            intent.putExtra("bundle", bundle);
            context.getApplicationContext().startService(intent);
        } catch (Throwable th) {
        }
    }

    public static PushManager getInstance() {
        return d.f1050a;
    }

    public boolean bindAlias(Context context, String str) {
        a.b("PushManager|call bindAlias");
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.c < 5000) {
            Log.e("PushManager", "call - > bindAlias failed, it be called too frequently");
            return false;
        }
        this.c = currentTimeMillis;
        Bundle bundle = new Bundle();
        bundle.putString("action", "bindAlias");
        bundle.putString("alias", str);
        a(context, bundle);
        return true;
    }

    public String getClientid(Context context) {
        String a2;
        if (this.d == null) {
            try {
                ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                if (!(applicationInfo == null || applicationInfo.metaData == null)) {
                    String string = applicationInfo.metaData.getString(AssistPushConsts.GETUI_APPID);
                    String string2 = applicationInfo.metaData.getString(AssistPushConsts.GETUI_APPSECRET);
                    String string3 = applicationInfo.metaData.getString(AssistPushConsts.GETUI_APPKEY);
                    if (string != null) {
                        string = string.trim();
                    }
                    if (string2 != null) {
                        string2 = string2.trim();
                    }
                    if (string3 != null) {
                        string3 = string3.trim();
                    }
                    if (!TextUtils.isEmpty(string) && !TextUtils.isEmpty(string2) && !TextUtils.isEmpty(string3) && (a2 = a(string + string2 + string3 + context.getPackageName())) != null) {
                        this.d = a2.getBytes();
                    }
                }
            } catch (Exception e) {
                a.b("PushManager|" + e.toString());
            }
        }
        if (this.d != null) {
            byte[] a3 = e.a(context.getFilesDir().getPath() + "/" + "init.pid");
            if (!(this.d == null || a3 == null || this.d.length != a3.length)) {
                byte[] bArr = new byte[a3.length];
                for (int i = 0; i < bArr.length; i++) {
                    bArr[i] = (byte) (this.d[i] ^ a3[i]);
                }
                return new String(bArr);
            }
        }
        return null;
    }

    public String getVersion(Context context) {
        return PushBuildConfig.sdk_conf_version;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.<init>(android.content.Context, java.lang.Class<?>):void}
     arg types: [android.content.Context, java.lang.Class<T>]
     candidates:
      ClspMth{android.content.Intent.<init>(java.lang.String, android.net.Uri):void}
      ClspMth{android.content.Intent.<init>(android.content.Context, java.lang.Class<?>):void} */
    public <T extends Service> void initialize(Context context, Class<T> cls) {
        try {
            String packageName = context.getApplicationContext().getPackageName();
            String a2 = com.igexin.push.util.a.a(context);
            if (a2 == null || (!a2.contains("gtsync") && !a2.contains("gtdms"))) {
                if (!com.igexin.push.util.a.a("PushManager", context, cls)) {
                    a.b("PushManager|init checkServiceSetCorrectly false");
                }
                if (cls == null || com.igexin.push.core.a.n.equals(cls.getName())) {
                    q.a(context, "us", "");
                    cls = PushService.class;
                } else if (!GTServiceManager.getInstance().getUserPushService(context).getName().equals(cls.getName())) {
                    q.a(context, "us", cls.getName());
                }
                Intent intent = new Intent(context.getApplicationContext(), (Class<?>) cls);
                intent.putExtra("action", PushConsts.ACTION_SERVICE_INITIALIZE);
                intent.putExtra("op_app", packageName);
                context.getApplicationContext().startService(intent);
                return;
            }
            a.b("PushManager|init by default = " + a2);
        } catch (Throwable th) {
            a.b("PushManager|initialize|" + th.toString());
        }
    }

    public boolean isPushTurnedOn(Context context) {
        return new c(context).c();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.<init>(android.content.Context, java.lang.Class<?>):void}
     arg types: [android.content.Context, java.lang.Class<T>]
     candidates:
      ClspMth{android.content.Intent.<init>(java.lang.String, android.net.Uri):void}
      ClspMth{android.content.Intent.<init>(android.content.Context, java.lang.Class<?>):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x006d, code lost:
        com.igexin.b.a.c.a.b("PushManager|registerPushIntentService|" + r0.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0088, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.igexin.b.a.c.a.b("PushManager|registerPushIntentService|" + r0.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:3:0x000b, B:5:0x0016] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T extends com.igexin.sdk.GTIntentService> void registerPushIntentService(android.content.Context r4, java.lang.Class<T> r5) {
        /*
            r3 = this;
            java.lang.String r0 = "PushManager|call registerPushIntentService"
            com.igexin.b.a.c.a.b(r0)
            if (r5 != 0) goto L_0x0016
            java.lang.String r0 = "uis"
            java.lang.String r1 = ""
            com.igexin.push.util.q.a(r4, r0, r1)     // Catch:{ Throwable -> 0x006c }
            java.lang.String r0 = "PushManager"
            java.lang.String r1 = "call -> registerPushIntentService, parameter [userIntentService] is null, use default Receiver"
            android.util.Log.d(r0, r1)     // Catch:{ Throwable -> 0x006c }
        L_0x0015:
            return
        L_0x0016:
            java.lang.String r0 = r5.getName()     // Catch:{ Exception -> 0x0088 }
            java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x0088 }
        L_0x001d:
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Throwable -> 0x006c }
            r0.<init>(r4, r5)     // Catch:{ Throwable -> 0x006c }
            boolean r0 = com.igexin.push.util.a.b(r0, r4)     // Catch:{ Throwable -> 0x006c }
            if (r0 != 0) goto L_0x004a
            java.lang.String r0 = "PushManager"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x006c }
            r1.<init>()     // Catch:{ Throwable -> 0x006c }
            java.lang.String r2 = "call - > registerPushIntentService, parameter [userIntentService] is set, but didn't find class \""
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x006c }
            java.lang.String r2 = r5.getName()     // Catch:{ Throwable -> 0x006c }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x006c }
            java.lang.String r2 = "\", please check your AndroidManifest"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x006c }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x006c }
            android.util.Log.e(r0, r1)     // Catch:{ Throwable -> 0x006c }
        L_0x004a:
            com.igexin.sdk.GTServiceManager r0 = com.igexin.sdk.GTServiceManager.getInstance()     // Catch:{ Throwable -> 0x006c }
            java.lang.Class r0 = r0.getUserIntentService(r4)     // Catch:{ Throwable -> 0x006c }
            if (r0 == 0) goto L_0x0062
            java.lang.String r1 = r5.getName()     // Catch:{ Throwable -> 0x006c }
            java.lang.String r0 = r0.getName()     // Catch:{ Throwable -> 0x006c }
            boolean r0 = r1.equals(r0)     // Catch:{ Throwable -> 0x006c }
            if (r0 != 0) goto L_0x0015
        L_0x0062:
            java.lang.String r0 = "uis"
            java.lang.String r1 = r5.getName()     // Catch:{ Throwable -> 0x006c }
            com.igexin.push.util.q.a(r4, r0, r1)     // Catch:{ Throwable -> 0x006c }
            goto L_0x0015
        L_0x006c:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "PushManager|registerPushIntentService|"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            goto L_0x0015
        L_0x0088:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x006c }
            r1.<init>()     // Catch:{ Throwable -> 0x006c }
            java.lang.String r2 = "PushManager|registerPushIntentService|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x006c }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x006c }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Throwable -> 0x006c }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x006c }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Throwable -> 0x006c }
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.sdk.PushManager.registerPushIntentService(android.content.Context, java.lang.Class):void");
    }

    public boolean sendFeedbackMessage(Context context, String str, String str2, int i) {
        if (str == null || str2 == null || i < 90001 || i > 90999) {
            Log.e("PushManager", "call - > sendFeedbackMessage failed, parameter is illegal");
            return false;
        }
        Bundle bundle = new Bundle();
        bundle.putString("action", "sendFeedbackMessage");
        bundle.putString("taskid", str);
        bundle.putString("messageid", str2);
        bundle.putString("actionid", String.valueOf(i));
        a(context, bundle);
        return true;
    }

    public boolean sendMessage(Context context, String str, byte[] bArr) {
        long currentTimeMillis = System.currentTimeMillis();
        if (str == null || bArr == null || ((long) bArr.length) > PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM || currentTimeMillis - this.b < 1000) {
            Log.e("PushManager", "call - > sendMessage failed, parameter is illegal or it be called too frequently");
            return false;
        }
        this.b = currentTimeMillis;
        Bundle bundle = new Bundle();
        bundle.putString("action", "sendMessage");
        bundle.putString("taskid", str);
        bundle.putByteArray("extraData", bArr);
        a(context, bundle);
        return true;
    }

    public boolean setHeartbeatInterval(Context context, int i) {
        if (i < 0) {
            Log.e("PushManager", "call -> setHeartbeatInterval failed, parameter [interval] < 0, illegal");
            return false;
        }
        Bundle bundle = new Bundle();
        bundle.putString("action", "setHeartbeatInterval");
        bundle.putInt("interval", i);
        a(context, bundle);
        return true;
    }

    public boolean setSilentTime(Context context, int i, int i2) {
        if (i < 0 || i >= 24 || i2 < 0 || i2 > 23) {
            Log.e("PushManager", "call - > setSilentTime failed, parameter [beginHour] or [duration] value exceeding");
            return false;
        }
        Bundle bundle = new Bundle();
        bundle.putString("action", "setSilentTime");
        bundle.putInt("beginHour", i);
        bundle.putInt("duration", i2);
        a(context, bundle);
        return true;
    }

    public boolean setSocketTimeout(Context context, int i) {
        if (i < 0) {
            Log.e("PushManager", "call - > setSocketTimeout failed, parameter [timeout] < 0, illegal");
            return false;
        }
        Bundle bundle = new Bundle();
        bundle.putString("action", "setSocketTimeout");
        bundle.putInt("timeout", i);
        a(context, bundle);
        return true;
    }

    public int setTag(Context context, Tag[] tagArr, String str) {
        if (tagArr == null) {
            Log.e("PushManager", "call -> setTag failed, parameter [tags] is null");
            a.b("PushManager|tags is null");
            return PushConsts.SETTAG_ERROR_NULL;
        } else if (str == null) {
            Log.e("PushManager", "call -> setTag failed, parameter [sn] is null");
            a.b("PushManager|sn is null");
            return PushConsts.SETTAG_SN_NULL;
        } else if (((long) tagArr.length) > 200) {
            Log.e("PushManager", "call -> setTag failed, parameter [tags] len > 200 is exceeds");
            a.b("PushManager|tags len > 200 is exceeds");
            return PushConsts.SETTAG_ERROR_COUNT;
        } else {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.f1039a < 1000) {
                Log.e("PushManager", "call - > setTag failed, it be called too frequently");
                return PushConsts.SETTAG_ERROR_FREQUENCY;
            }
            StringBuilder sb = new StringBuilder();
            for (Tag tag : tagArr) {
                if (!(tag == null || tag.getName() == null)) {
                    sb.append(tag.getName());
                    sb.append(MiPushClient.ACCEPT_TIME_SEPARATOR);
                }
            }
            if (sb.length() <= 0) {
                return PushConsts.SETTAG_ERROR_NULL;
            }
            sb.deleteCharAt(sb.length() - 1);
            Bundle bundle = new Bundle();
            bundle.putString("action", "setTag");
            bundle.putString("tags", sb.toString());
            bundle.putString(IXAdRequestInfo.SN, str);
            this.f1039a = currentTimeMillis;
            a(context, bundle);
            return 0;
        }
    }

    public void stopService(Context context) {
        Bundle bundle = new Bundle();
        bundle.putString("action", "stopService");
        a(context, bundle);
    }

    public void turnOffPush(Context context) {
        a.b("PushManager|call turnOffPush");
        Bundle bundle = new Bundle();
        bundle.putString("action", "turnOffPush");
        a(context, bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void turnOnPush(Context context) {
        a.b("PushManager|call turnOnPush");
        Intent intent = new Intent(context.getApplicationContext(), a(context));
        intent.putExtra("action", PushConsts.ACTION_SERVICE_INITIALIZE_SLAVE);
        intent.putExtra("op_app", context.getApplicationContext().getPackageName());
        intent.putExtra("isSlave", true);
        context.getApplicationContext().startService(intent);
    }

    public boolean unBindAlias(Context context, String str, boolean z) {
        a.b("PushManager|call unBindAlias");
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.c < 5000) {
            Log.e("PushManager", "call - > unBindAlias failed, it be called too frequently");
            return false;
        }
        this.c = currentTimeMillis;
        Bundle bundle = new Bundle();
        bundle.putString("action", "unbindAlias");
        bundle.putString("alias", str);
        bundle.putBoolean("isSeft", z);
        a(context, bundle);
        return true;
    }
}
