package com.igexin.sdk;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.huawei.hms.support.api.push.PushReceiver;
import com.igexin.assist.MessageBean;
import com.igexin.assist.action.MessageManger;
import com.igexin.assist.sdk.AssistPushConsts;
import org.json.JSONArray;
import org.json.JSONObject;

public class HmsPushReceiver extends PushReceiver {
    public static final String MSG_KEY_PAYLOAD = "gt_payload";
    public static final String TAG = "Assist_HmsPushReceiver";

    public void onEvent(Context context, PushReceiver.Event event, Bundle bundle) {
        JSONObject jSONObject;
        try {
            if (PushReceiver.Event.NOTIFICATION_OPENED.equals(event)) {
                String string = bundle.getString(PushReceiver.BOUND_KEY.pushMsgKey);
                Log.d(TAG, "onEvent payload ...");
                if (!TextUtils.isEmpty(string)) {
                    JSONArray jSONArray = new JSONArray(string);
                    if (jSONArray.length() >= 1 && (jSONObject = jSONArray.getJSONObject(0)) != null && jSONObject.has("gt_payload")) {
                        String string2 = jSONObject.getString("gt_payload");
                        if (context != null && !TextUtils.isEmpty(string2)) {
                            MessageBean messageBean = new MessageBean(context, AssistPushConsts.MSG_TYPE_PAYLOAD, string2);
                            messageBean.setMessageSource(AssistPushConsts.HW_PREFIX);
                            MessageManger.getInstance().addMessage(messageBean);
                        }
                    }
                }
            }
            super.onEvent(context, event, bundle);
        } catch (Throwable th) {
        }
    }

    public boolean onPushMsg(Context context, byte[] bArr, Bundle bundle) {
        try {
            String str = new String(bArr, "UTF-8");
            Log.d(TAG, "onPushMsg payload ...");
            if (context == null || TextUtils.isEmpty(str)) {
                return false;
            }
            MessageBean messageBean = new MessageBean(context, AssistPushConsts.MSG_TYPE_PAYLOAD, str);
            messageBean.setMessageSource(AssistPushConsts.HW_PREFIX);
            MessageManger.getInstance().addMessage(messageBean);
            return false;
        } catch (Throwable th) {
            return false;
        }
    }

    public void onPushState(Context context, boolean z) {
    }

    public void onToken(Context context, String str, Bundle bundle) {
        try {
            Log.d(TAG, "onToken :" + str);
            if (context != null && !TextUtils.isEmpty(str)) {
                MessageManger.getInstance().addMessage(new MessageBean(context, AssistPushConsts.MSG_TYPE_TOKEN, AssistPushConsts.HW_PREFIX + str));
            }
        } catch (Throwable th) {
        }
    }
}
