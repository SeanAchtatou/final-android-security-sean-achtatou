package com.igexin.sdk;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.igexin.b.a.c.a;

public class PushService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private static String f1041a = "PushService";

    private int a(Intent intent) {
        Class userPushService = GTServiceManager.getInstance().getUserPushService(this);
        if (intent != null) {
            intent.setClass(getApplicationContext(), userPushService);
        } else {
            intent = new Intent(getApplicationContext(), userPushService);
        }
        getApplicationContext().startService(intent);
        stopSelf();
        return 1;
    }

    public IBinder onBind(Intent intent) {
        a.b(f1041a + "|onBind");
        return GTServiceManager.getInstance().onBind(intent);
    }

    public void onCreate() {
        super.onCreate();
        GTServiceManager.getInstance().onCreate(this);
    }

    public void onDestroy() {
        a.b(f1041a + "|onDestroy");
        if (!GTServiceManager.getInstance().isUserPushServiceSet(this)) {
            GTServiceManager.getInstance().onDestroy();
        }
        super.onDestroy();
    }

    public void onLowMemory() {
        super.onLowMemory();
        a.b(f1041a + "|onLowMemory");
        GTServiceManager.getInstance().onLowMemory();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        super.onStartCommand(intent, i, i2);
        try {
            return GTServiceManager.getInstance().isUserPushServiceSet(this) ? a(intent) : GTServiceManager.getInstance().onStartCommand(this, intent, i, i2);
        } catch (Throwable th) {
            a.b(f1041a + "|" + th.toString());
            return 1;
        }
    }
}
