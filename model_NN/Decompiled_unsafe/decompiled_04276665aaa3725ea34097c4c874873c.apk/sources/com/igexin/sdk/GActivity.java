package com.igexin.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.igexin.b.a.c.a;
import com.igexin.push.core.a.e;

public class GActivity extends Activity {
    public static final String TAG = GActivity.class.getName();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Intent intent;
        super.onCreate(bundle);
        try {
            Intent intent2 = getIntent();
            intent = new Intent(this, e.a().a((Context) this));
            if (intent2 != null) {
                if (intent2.hasExtra("action") && intent2.hasExtra("isSlave")) {
                    intent.putExtra("action", intent2.getStringExtra("action"));
                    intent.putExtra("isSlave", intent2.getBooleanExtra("isSlave", false));
                    if (intent2.hasExtra("op_app")) {
                        intent.putExtra("op_app", intent2.getStringExtra("op_app"));
                    }
                    a.b("GActivity action = " + intent2.getStringExtra("action") + ", isSlave = " + intent2.getBooleanExtra("isSlave", false));
                }
            }
        } catch (Exception e) {
            a.b(TAG + "|put extra exception" + e.toString());
        } catch (Throwable th) {
            a.b(TAG + th.toString());
        }
        startService(intent);
        a.b(TAG + "|start PushService from GActivity");
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
