package com.igexin.sdk.message;

import com.igexin.push.core.g;
import java.io.Serializable;

public class BaseMessage implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f1051a = g.f974a;
    private String b = g.e;
    private String c = g.r;

    public String getAppid() {
        return this.f1051a;
    }

    public String getClientId() {
        return this.c;
    }

    public String getPkgName() {
        return this.b;
    }

    public void setAppid(String str) {
        this.f1051a = str;
    }

    public void setClientId(String str) {
        this.c = str;
    }

    public void setPkgName(String str) {
        this.b = str;
    }
}
