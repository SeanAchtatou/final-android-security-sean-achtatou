package com.igexin.sdk.message;

public class SetTagCmdMessage extends GTCmdMessage {

    /* renamed from: a  reason: collision with root package name */
    private String f1055a;
    private String b;

    public SetTagCmdMessage() {
    }

    public SetTagCmdMessage(String str, String str2, int i) {
        super(i);
        this.f1055a = str;
        this.b = str2;
    }

    public String getCode() {
        return this.b;
    }

    public String getSn() {
        return this.f1055a;
    }

    public void setCode(String str) {
        this.b = str;
    }

    public void setSn(String str) {
        this.f1055a = str;
    }
}
