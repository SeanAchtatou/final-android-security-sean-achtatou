package com.igexin.assist.control;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.huawei.hms.api.ConnectionResult;
import com.huawei.hms.api.HuaweiApiAvailability;
import com.huawei.hms.api.HuaweiApiClient;
import com.huawei.hms.support.api.push.HuaweiPush;
import com.meizu.cloud.pushsdk.constants.MeizuConstants;

public class HmsPushManager implements HuaweiApiClient.ConnectionCallbacks, HuaweiApiClient.OnConnectionFailedListener, AbstractPushManager {
    public static final String HONOR = "honor".toLowerCase();
    public static final String HUAWEI = "Huawei".toLowerCase();
    public static final String TAG = "Assist_HmsPushManager";
    private static final String b = Build.BRAND;

    /* renamed from: a  reason: collision with root package name */
    private HuaweiApiClient f789a;

    public HmsPushManager(Context context) {
        try {
            this.f789a = new HuaweiApiClient.Builder(context).addApi(HuaweiPush.PUSH_API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
        } catch (Throwable th) {
        }
    }

    public static boolean checkHWDevice(Context context) {
        int i;
        try {
            if (TextUtils.equals(HUAWEI, b.toLowerCase()) || TextUtils.equals(HONOR, b.toLowerCase())) {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(HuaweiApiAvailability.SERVICES_PACKAGE, 0);
                try {
                    Class<?> cls = Class.forName(MeizuConstants.CLS_NAME_SYSTEM_PROPERTIES);
                    i = Integer.parseInt((String) cls.getDeclaredMethod("get", String.class).invoke(cls, "ro.build.hw_emui_api_level"));
                } catch (Throwable th) {
                    i = 0;
                }
                if (packageInfo != null) {
                    if (packageInfo.versionCode >= 20401300 && i > 9) {
                        return true;
                    }
                }
                return false;
            }
        } catch (Throwable th2) {
        }
        return false;
    }

    public String getToken(Context context) {
        try {
            if (this.f789a == null || !this.f789a.isConnected()) {
                return "";
            }
            HuaweiPush.HuaweiPushApi.getToken(this.f789a);
            return "";
        } catch (Throwable th) {
            return "";
        }
    }

    public void onConnected() {
        try {
            Log.d(TAG, "hms push connetioned");
            if (this.f789a != null && this.f789a.isConnected()) {
                HuaweiPush.HuaweiPushApi.getToken(this.f789a).setResultCallback(new a(this));
            }
        } catch (Throwable th) {
        }
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        try {
            Log.d(TAG, "hms push connetion failed");
            if (connectionResult != null) {
                Log.d(TAG, "hms push connetion failed code = " + connectionResult.getErrorCode());
            }
        } catch (Throwable th) {
        }
    }

    public void onConnectionSuspended(int i) {
    }

    public void register(Context context) {
        try {
            Log.d(TAG, "Register hmspush, pkg = " + context.getPackageName());
            if (this.f789a != null) {
                this.f789a.connect();
            }
        } catch (Throwable th) {
        }
    }

    public void setSilentTime(Context context, int i, int i2) {
    }

    public void turnOffPush(Context context) {
    }

    public void turnOnPush(Context context) {
        getToken(context);
    }

    public void unregister(Context context) {
        try {
            Log.d(TAG, "Unregister hmspush");
            if (this.f789a != null) {
                this.f789a.disconnect();
            }
        } catch (Throwable th) {
        }
    }
}
