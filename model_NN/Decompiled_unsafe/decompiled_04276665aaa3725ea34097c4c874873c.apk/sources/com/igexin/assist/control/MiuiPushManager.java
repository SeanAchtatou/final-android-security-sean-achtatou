package com.igexin.assist.control;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.igexin.assist.sdk.AssistPushConsts;
import com.xiaomi.mipush.sdk.MiPushClient;

public class MiuiPushManager implements AbstractPushManager {
    public static final String TAG = "Assist_MiuiPushManager";

    /* renamed from: a  reason: collision with root package name */
    private static final String f790a = "Xiaomi".toLowerCase();
    private static final String b = Build.BRAND;
    private String c = "";
    private String d = "";

    public MiuiPushManager(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            this.c = (String) applicationInfo.metaData.get(AssistPushConsts.MIPUSH_APPID);
            this.c = this.c.replace(AssistPushConsts.XM_PREFIX, "");
            this.d = (String) applicationInfo.metaData.get(AssistPushConsts.MIPUSH_APPKEY);
            this.d = this.d.replace(AssistPushConsts.XM_PREFIX, "");
        } catch (Throwable th) {
        }
    }

    public static boolean checkXMDevice(Context context) {
        PackageInfo packageInfo;
        try {
            return TextUtils.equals(f790a, b.toLowerCase()) && (packageInfo = context.getPackageManager().getPackageInfo("com.xiaomi.xmsf", 0)) != null && packageInfo.versionCode >= 105;
        } catch (Throwable th) {
            return false;
        }
    }

    public String getToken(Context context) {
        if (context == null) {
            return null;
        }
        return MiPushClient.getRegId(context);
    }

    public void register(Context context) {
        try {
            if (TextUtils.isEmpty(this.c) || TextUtils.isEmpty(this.d)) {
                Log.d(TAG, "Register mipush appId not find");
                return;
            }
            Log.d(TAG, "Register mipush, pkg = " + context.getPackageName());
            MiPushClient.registerPush(context, this.c, this.d);
        } catch (Throwable th) {
        }
    }

    public void setSilentTime(Context context, int i, int i2) {
        if (i2 == 0) {
            turnOnPush(context);
            return;
        }
        MiPushClient.setAcceptTime(context, (i + i2) % 24, 0, i, 0, null);
    }

    public void turnOffPush(Context context) {
        if (context != null) {
            MiPushClient.pausePush(context, this.c);
        }
    }

    public void turnOnPush(Context context) {
        if (context != null) {
            MiPushClient.resumePush(context, this.c);
        }
    }

    public void unregister(Context context) {
        try {
            Log.d(TAG, "Unregister mipush");
            if (context != null) {
                MiPushClient.unregisterPush(context);
            }
        } catch (Throwable th) {
        }
    }
}
