package com.igexin.assist.sdk;

import android.content.Context;
import com.igexin.assist.control.AbstractPushManager;
import com.igexin.push.config.m;
import java.lang.reflect.Constructor;

public class a {
    static AbstractPushManager a(Context context) {
        try {
            if (b(context) && m.M) {
                com.igexin.b.a.c.a.b("Assist_PushMangerFactory|MiuiPushManager checkDevice flag = true");
                try {
                    Constructor<?> constructor = Class.forName("com.igexin.assist.control.MiuiPushManager").getConstructor(Context.class);
                    if (constructor != null) {
                        return (AbstractPushManager) constructor.newInstance(context);
                    }
                } catch (ClassNotFoundException e) {
                }
                com.igexin.b.a.c.a.b("Assist_PushMangerFactory|OtherPushManager = null");
                return null;
            } else if (!c(context) || !m.N) {
                if (d(context) && m.O) {
                    com.igexin.b.a.c.a.b("Assist_PushMangerFactory|HmsPushManager checkDevice flag = true");
                    try {
                        Constructor<?> constructor2 = Class.forName("com.igexin.assist.control.HmsPushManager").getConstructor(Context.class);
                        if (constructor2 != null) {
                            return (AbstractPushManager) constructor2.newInstance(context);
                        }
                    } catch (ClassNotFoundException e2) {
                    }
                }
                com.igexin.b.a.c.a.b("Assist_PushMangerFactory|OtherPushManager = null");
                return null;
            } else {
                com.igexin.b.a.c.a.b("Assist_PushMangerFactory|FlymePushManager checkDevice flag = true");
                Constructor<?> constructor3 = Class.forName("com.igexin.assist.control.FlymePushManager").getConstructor(Context.class);
                if (constructor3 != null) {
                    return (AbstractPushManager) constructor3.newInstance(context);
                }
                com.igexin.b.a.c.a.b("Assist_PushMangerFactory|OtherPushManager = null");
                return null;
            }
        } catch (ClassNotFoundException e3) {
            e3.printStackTrace();
        } catch (Throwable th) {
        }
    }

    public static boolean b(Context context) {
        try {
            return ((Boolean) Class.forName("com.igexin.assist.control.MiuiPushManager").getMethod("checkXMDevice", Context.class).invoke(null, context)).booleanValue();
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean c(Context context) {
        try {
            return ((Boolean) Class.forName("com.igexin.assist.control.FlymePushManager").getMethod("checkMZDevice", Context.class).invoke(null, context)).booleanValue();
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean d(Context context) {
        try {
            return ((Boolean) Class.forName("com.igexin.assist.control.HmsPushManager").getMethod("checkHWDevice", Context.class).invoke(null, context)).booleanValue();
        } catch (Throwable th) {
            return false;
        }
    }
}
