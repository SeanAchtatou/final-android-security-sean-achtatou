package com.duoduo.dynamicdex.data;

import android.app.Activity;
import android.view.ViewGroup;
import com.duoduo.dynamicdex.DuoMobApp;
import com.duoduo.mobads.gdt.IGdtNativeAd;
import com.duoduo.mobads.gdt.IGdtNativeAdListener;
import com.duoduo.mobads.gdt.IGdtSplashAdListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Constructor;

public class DGdtUtils implements IAdUtils {
    private boolean mCopySuc = false;
    private Class<?> mGdtNativeClazz = null;
    private Class<?> mSplashAdClazz = null;

    public boolean isEmpty() {
        return this.mGdtNativeClazz == null && this.mSplashAdClazz == null;
    }

    public void load(String str, ClassLoader classLoader) {
        if (classLoader != null) {
            try {
                this.mGdtNativeClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.gdt.GdtNativeAdWrapper");
            } catch (Exception e) {
            }
            try {
                this.mSplashAdClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.gdt.GdtAdSplashWrapper");
            } catch (Exception e2) {
            }
            try {
                this.mCopySuc = copyJar(str, classLoader);
            } catch (Exception e3) {
            }
        }
    }

    private boolean copyJar(String str, ClassLoader classLoader) {
        boolean z;
        boolean z2 = true;
        if (this.mGdtNativeClazz == null) {
            return false;
        }
        try {
            String str2 = String.valueOf(DuoMobApp.Ins.getApp().getDir("e_qq_com_plugin", 0).getAbsolutePath()) + File.separator + "gdt_plugin.jar.sig";
            if (!(innerCp("/assets/gdt_plugin/gdtadv2.jar", new StringBuilder(String.valueOf(DuoMobApp.Ins.getApp().getDir("e_qq_com_plugin", 0).getAbsolutePath())).append(File.separator).append("gdt_plugin.jar").toString())) || !innerCp("/assets/gdt_plugin.jar.sig", str2)) {
                z = false;
            } else {
                z = true;
            }
            String str3 = String.valueOf(DuoMobApp.Ins.getApp().getDir("e_qq_com_plugin", 0).getAbsolutePath()) + File.separator + "update_lc";
            if (!z || !innerCp("/assets/update_lc", str3)) {
                z2 = false;
            }
            return z2;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean innerCp(String str, String str2) {
        try {
            if (this.mGdtNativeClazz == null) {
                return false;
            }
            File file = new File(str2);
            InputStream resourceAsStream = this.mGdtNativeClazz.getResourceAsStream(str);
            if (resourceAsStream == null) {
                return false;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = resourceAsStream.read(bArr);
                if (read <= 0) {
                    resourceAsStream.close();
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    return true;
                }
                fileOutputStream.write(bArr, 0, read);
            }
        } catch (Exception e) {
            return false;
        }
    }

    public IGdtNativeAd getNativeAd(Activity activity, String str, String str2, IGdtNativeAdListener iGdtNativeAdListener) {
        if (!this.mCopySuc || this.mGdtNativeClazz == null) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mGdtNativeClazz.getConstructor(Activity.class, String.class, String.class, IGdtNativeAdListener.class);
            if (constructor != null) {
                return (IGdtNativeAd) constructor.newInstance(activity, str, str2, iGdtNativeAdListener);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Object getSplashAd(Activity activity, ViewGroup viewGroup, String str, String str2, IGdtSplashAdListener iGdtSplashAdListener) {
        if (!this.mCopySuc || this.mSplashAdClazz == null) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mSplashAdClazz.getConstructor(Activity.class, ViewGroup.class, String.class, String.class, IGdtSplashAdListener.class);
            if (constructor != null) {
                return constructor.newInstance(activity, viewGroup, str, str2, iGdtSplashAdListener);
            }
        } catch (Exception e) {
        }
        return null;
    }
}
