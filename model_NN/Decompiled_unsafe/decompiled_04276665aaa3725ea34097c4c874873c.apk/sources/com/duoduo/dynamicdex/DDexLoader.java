package com.duoduo.dynamicdex;

import android.os.AsyncTask;
import android.util.Log;
import dalvik.system.DexClassLoader;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.net.URL;

public enum DDexLoader {
    Ins;
    
    public static final int ERROR_CANT_REJECT = -1001;
    public static final int ERROR_CLASS_NOT_FOUND = -1005;
    public static final int ERROR_DEX_UNKONWN = -1006;
    public static final int ERROR_ILLEGAL_ACCESS = -1004;
    public static final int ERROR_ILLEGAL_ARGUMENT = -1002;
    public static final int ERROR_NO_SUCH_FILED = -1003;
    private static final String TAG = "DDexLoader";

    public interface DexLoadListener {
        void loadFailed(int i);

        void loaded(ClassLoader classLoader);
    }

    public void load(String str, String str2, DexLoadListener dexLoadListener) {
        String str3 = String.valueOf(DuoMobApp.Ins.getApp().getDir("duo_jar", 0).getAbsolutePath()) + File.separator + str2;
        if (str3 == null || !new File(str3).exists()) {
            download(str, str3, dexLoadListener);
        } else {
            loadFmStorage(str3, dexLoadListener);
        }
    }

    public void loadFmStorage(String str, DexLoadListener dexLoadListener) {
        File file = new File(str);
        File dir = DuoMobApp.Ins.getApp().getDir("dex", 0);
        try {
            ClassLoader classLoader = DuoMobApp.Ins.getApp().getClassLoader();
            DexClassLoader dexClassLoader = new DexClassLoader(file.getAbsolutePath(), dir.getAbsolutePath(), null, classLoader);
            Object combineArray = combineArray(getDexElements(getPathList(dexClassLoader)), getDexElements(getPathList(classLoader)));
            Object pathList = getPathList(classLoader);
            setField(pathList, pathList.getClass(), "dexElements", combineArray);
            if (dexLoadListener != null) {
                dexLoadListener.loaded(dexClassLoader);
            }
        } catch (IllegalArgumentException e) {
            if (dexLoadListener != null) {
                dexLoadListener.loadFailed(ERROR_ILLEGAL_ARGUMENT);
            }
        } catch (NoSuchFieldException e2) {
            if (dexLoadListener != null) {
                dexLoadListener.loadFailed(ERROR_NO_SUCH_FILED);
            }
        } catch (IllegalAccessException e3) {
            if (dexLoadListener != null) {
                dexLoadListener.loadFailed(ERROR_ILLEGAL_ACCESS);
            }
        } catch (ClassNotFoundException e4) {
            if (dexLoadListener != null) {
                dexLoadListener.loadFailed(ERROR_CLASS_NOT_FOUND);
            }
        } catch (Exception e5) {
            if (dexLoadListener != null) {
                dexLoadListener.loadFailed(ERROR_DEX_UNKONWN);
            }
        }
    }

    private static Object getPathList(Object obj) throws IllegalArgumentException, NoSuchFieldException, IllegalAccessException, ClassNotFoundException {
        return getField(obj, Class.forName("dalvik.system.BaseDexClassLoader"), "pathList");
    }

    private static Object getDexElements(Object obj) throws IllegalArgumentException, NoSuchFieldException, IllegalAccessException {
        return getField(obj, obj.getClass(), "dexElements");
    }

    private static Object getField(Object obj, Class<?> cls, String str) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Field declaredField = cls.getDeclaredField(str);
        declaredField.setAccessible(true);
        return declaredField.get(obj);
    }

    private static void setField(Object obj, Class<?> cls, String str, Object obj2) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Field declaredField = cls.getDeclaredField(str);
        declaredField.setAccessible(true);
        declaredField.set(obj, obj2);
    }

    private static Object combineArray(Object obj, Object obj2) {
        Class<?> componentType = obj.getClass().getComponentType();
        int length = Array.getLength(obj);
        int length2 = Array.getLength(obj2) + length;
        Object newInstance = Array.newInstance(componentType, length2);
        for (int i = 0; i < length2; i++) {
            if (i < length) {
                Array.set(newInstance, i, Array.get(obj, i));
            } else {
                Array.set(newInstance, i, Array.get(obj2, i - length));
            }
        }
        return newInstance;
    }

    private void download(String str, String str2, final DexLoadListener dexLoadListener) {
        if (str == null || str2 == null) {
            Log.e(TAG, "download(error):url filePath null");
            if (dexLoadListener != null) {
                dexLoadListener.loadFailed(ERROR_ILLEGAL_ARGUMENT);
                return;
            }
            return;
        }
        new AsyncTask<String, String, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... strArr) {
                try {
                    URL url = new URL(strArr[0]);
                    url.openConnection().connect();
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(url.openStream(), 8192);
                    String str = String.valueOf(strArr[1]) + ".tmp";
                    File file = new File(str);
                    if (file != null && file.exists()) {
                        file.delete();
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(str);
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = bufferedInputStream.read(bArr);
                        if (read == -1) {
                            fileOutputStream.flush();
                            fileOutputStream.close();
                            bufferedInputStream.close();
                            moveFile(str, strArr[1]);
                            return strArr[1];
                        }
                        fileOutputStream.write(bArr, 0, read);
                    }
                } catch (Exception e) {
                    Log.e("Error: ", e == null ? "unknwonerror" : e.getMessage());
                    return null;
                }
            }

            public boolean moveFile(String str, String str2) {
                if (str == null || str2 == null) {
                    return false;
                }
                File file = new File(str);
                if (!file.exists() || !file.isFile()) {
                    return false;
                }
                File file2 = new File(getFilePath(str2));
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                return file.renameTo(new File(str2));
            }

            public String getFilePath(String str) {
                int lastIndexOf;
                if (str == null || (lastIndexOf = str.lastIndexOf(File.separator)) == -1) {
                    return "";
                }
                return str.substring(0, lastIndexOf);
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String str) {
                if (str != null && str != "") {
                    DDexLoader.this.loadFmStorage(str, dexLoadListener);
                }
            }
        }.execute(str, str2);
    }
}
