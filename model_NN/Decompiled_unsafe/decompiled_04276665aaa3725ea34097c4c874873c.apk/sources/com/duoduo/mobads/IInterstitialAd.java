package com.duoduo.mobads;

import android.app.Activity;

public interface IInterstitialAd {
    boolean isAdReady();

    void loadAd();

    void setListener(IInterstitialAdListener iInterstitialAdListener);

    void showAd(Activity activity);
}
