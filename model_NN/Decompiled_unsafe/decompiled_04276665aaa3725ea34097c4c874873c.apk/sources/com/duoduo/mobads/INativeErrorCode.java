package com.duoduo.mobads;

public interface INativeErrorCode {
    int getErrorCode();
}
