package com.duoduo.mobads;

import android.view.View;

public interface IAdView {
    View getAdView();
}
