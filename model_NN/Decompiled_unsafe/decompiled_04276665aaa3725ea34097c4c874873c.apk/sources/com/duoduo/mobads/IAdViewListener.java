package com.duoduo.mobads;

import org.json.JSONObject;

public interface IAdViewListener {
    void onAdClick(JSONObject jSONObject);

    void onAdClose(JSONObject jSONObject);

    void onAdFailed(String str);

    void onAdReady(IAdView iAdView);

    void onAdShow(JSONObject jSONObject);

    void onAdSwitch();
}
