package com.qq.e.comm.managers.plugin;

import android.content.Context;
import com.qq.e.comm.net.NetworkCallBack;
import com.qq.e.comm.net.NetworkClient;
import com.qq.e.comm.net.NetworkClientImpl;
import com.qq.e.comm.net.rr.PlainRequest;
import com.qq.e.comm.net.rr.Request;
import com.qq.e.comm.net.rr.Response;
import com.qq.e.comm.util.FileUtil;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.Md5Util;
import com.qq.e.comm.util.StringUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f1226a = Pattern.compile(".*plugin\\.dex-(\\d+)\\.jar.*");
    /* access modifiers changed from: private */
    public final Context b;

    /* renamed from: com.qq.e.comm.managers.plugin.a$a  reason: collision with other inner class name */
    class C0034a implements NetworkCallBack {

        /* renamed from: a  reason: collision with root package name */
        private final String f1227a;
        private final int b;

        public C0034a(String str, int i) {
            this.f1227a = str;
            this.b = i;
        }

        private static String a(Response response, File file) {
            FileOutputStream fileOutputStream;
            InputStream inputStream;
            String str = null;
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                inputStream = response.getStreamContent();
                try {
                    fileOutputStream = new FileOutputStream(file);
                } catch (IOException e) {
                    e = e;
                    fileOutputStream = null;
                    try {
                        GDTLogger.e("IOException While Update Plugin", e);
                        FileUtil.tryClose(inputStream);
                        FileUtil.tryClose(fileOutputStream);
                        return str;
                    } catch (Throwable th) {
                        th = th;
                        FileUtil.tryClose(inputStream);
                        FileUtil.tryClose(fileOutputStream);
                        throw th;
                    }
                } catch (NoSuchAlgorithmException e2) {
                    e = e2;
                    fileOutputStream = null;
                    GDTLogger.e("MD5SUMException While Update Plugin", e);
                    FileUtil.tryClose(inputStream);
                    FileUtil.tryClose(fileOutputStream);
                    return str;
                } catch (Throwable th2) {
                    fileOutputStream = null;
                    th = th2;
                    FileUtil.tryClose(inputStream);
                    FileUtil.tryClose(fileOutputStream);
                    throw th;
                }
                try {
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = inputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        instance.update(bArr, 0, read);
                        fileOutputStream.write(bArr, 0, read);
                    }
                    FileUtil.tryClose(inputStream);
                    FileUtil.tryClose(fileOutputStream);
                    str = Md5Util.byteArrayToHexString(instance.digest());
                    FileUtil.tryClose(inputStream);
                    FileUtil.tryClose(fileOutputStream);
                } catch (IOException e3) {
                    e = e3;
                    GDTLogger.e("IOException While Update Plugin", e);
                    FileUtil.tryClose(inputStream);
                    FileUtil.tryClose(fileOutputStream);
                    return str;
                } catch (NoSuchAlgorithmException e4) {
                    e = e4;
                    GDTLogger.e("MD5SUMException While Update Plugin", e);
                    FileUtil.tryClose(inputStream);
                    FileUtil.tryClose(fileOutputStream);
                    return str;
                }
            } catch (IOException e5) {
                e = e5;
                fileOutputStream = null;
                inputStream = null;
            } catch (NoSuchAlgorithmException e6) {
                e = e6;
                fileOutputStream = null;
                inputStream = null;
                GDTLogger.e("MD5SUMException While Update Plugin", e);
                FileUtil.tryClose(inputStream);
                FileUtil.tryClose(fileOutputStream);
                return str;
            } catch (Throwable th3) {
                fileOutputStream = null;
                inputStream = null;
                th = th3;
                FileUtil.tryClose(inputStream);
                FileUtil.tryClose(fileOutputStream);
                throw th;
            }
            return str;
        }

        private boolean a(File file) {
            try {
                StringUtil.writeTo(this.b + "#####" + this.f1227a, file);
                return true;
            } catch (IOException e) {
                GDTLogger.e("IOException While Update Plugin", e);
                return false;
            }
        }

        public final void onException(Exception exc) {
            GDTLogger.w("Exception While Update Plugin", exc);
        }

        public final void onResponse(Request request, Response response) {
            String str;
            String str2;
            if (response.getStatusCode() == 200) {
                try {
                    File file = new File(a.this.b.getDir("e_qq_com_plugin", 0), "gdt_plugin.tmp");
                    File file2 = new File(a.this.b.getDir("e_qq_com_plugin", 0), "gdt_plugin.tmp.sig");
                    String a2 = a(response, file);
                    if (!com.qq.e.comm.util.a.a().b(this.f1227a, a2)) {
                        file.delete();
                        GDTLogger.report(String.format("Fail to update plugin while verifying,sig=%s,md5=%s", this.f1227a, a2));
                    } else if (!a(file2) || !FileUtil.renameTo(file, c.b(a.this.b)) || FileUtil.renameTo(file2, c.d(a.this.b))) {
                    }
                } catch (Throwable th) {
                    GDTLogger.e("UnknownException While Update Plugin", th);
                } finally {
                    str = "TIMESTAMP_AFTER_DOWNPLUGIN:";
                    StringBuilder append = new StringBuilder(str).append(System.nanoTime());
                    str2 = ";sig=";
                    GDTLogger.d(append.append(str2).append(this.f1227a).toString());
                }
            } else {
                GDTLogger.report("DownLoad Plugin Jar Status error,response status code=" + response.getStatusCode());
            }
        }
    }

    public a(Context context) {
        this.b = context.getApplicationContext();
    }

    public final void a(String str, String str2) {
        boolean z;
        if (!StringUtil.isEmpty(str) && !StringUtil.isEmpty(str2)) {
            String str3 = "0";
            Matcher matcher = f1226a.matcher(str2);
            if (matcher.matches()) {
                str3 = matcher.group(1);
            }
            int parseInteger = StringUtil.parseInteger(str3, 0);
            if (parseInteger < 529) {
                GDTLogger.i("online plugin version is smaller than asset plugin version" + parseInteger + ",529" + ".download give up");
                z = false;
            } else {
                z = true;
            }
            if (z) {
                NetworkClientImpl.getInstance().submit(new PlainRequest(str2, Request.Method.GET, null), NetworkClient.Priority.High, new C0034a(str, parseInteger));
            }
        }
    }
}
