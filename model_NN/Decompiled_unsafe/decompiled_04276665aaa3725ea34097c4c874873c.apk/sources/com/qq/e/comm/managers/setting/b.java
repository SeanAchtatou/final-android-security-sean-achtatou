package com.qq.e.comm.managers.setting;

import com.qq.e.comm.constants.Constants;

public final class b extends c {
    public b() {
        a(Constants.KEYS.SDKServerGetADReportSamplingRate, 1);
        a(Constants.KEYS.SDKServerExpReportSamplingRate, 1);
        a(Constants.KEYS.SDKServerClickReportSamplingRate, 100);
        a(Constants.KEYS.RequireWindowFocus, 1);
        a(Constants.KEYS.SHOW_LOGO, 0);
        a(Constants.KEYS.INNER_BROWSER_SCHEME, "weixin,tel,openapp.jdmobile");
        a(Constants.KEYS.THIRD_PARTY_BROWSER, "com.android.browser,com.android.chrome,com.baidu.browser.apps,com.UCMobile,com.tencent.mtt");
        a(Constants.KEYS.Banner_RF, 30000);
        a(Constants.KEYS.SPLASH_LOADTIMEOUT, 3000);
        a(Constants.KEYS.SPLASH_EXPOSURE_TIME, 5000);
        a(Constants.KEYS.SPLASH_NETWORK_PERMISION, 26);
        a(Constants.KEYS.SPLASH_MAX_REQUEST_NUM, 100);
        a(Constants.KEYS.FORCE_EXPOSURE, 1);
    }
}
