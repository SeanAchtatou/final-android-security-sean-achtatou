package com.qq.e.ads.banner;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.widget.FrameLayout;
import com.qq.e.ads.cfg.BannerRollAnimation;
import com.qq.e.ads.cfg.DownAPPConfirmPolicy;
import com.qq.e.comm.a;
import com.qq.e.comm.adevent.ADEvent;
import com.qq.e.comm.adevent.ADListener;
import com.qq.e.comm.managers.GDTADManager;
import com.qq.e.comm.pi.BVI;
import com.qq.e.comm.pi.POFactory;
import com.qq.e.comm.util.GDTLogger;
import com.qq.e.comm.util.StringUtil;

@SuppressLint({"ViewConstructor"})
public class BannerView extends FrameLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public BVI f1201a;
    /* access modifiers changed from: private */
    public BannerADListener b;
    private boolean c = false;
    private boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public Integer f;
    /* access modifiers changed from: private */
    public BannerRollAnimation g;
    /* access modifiers changed from: private */
    public DownAPPConfirmPolicy h;
    /* access modifiers changed from: private */
    public Boolean i;
    private volatile int j = 0;

    class ADListenerAdapter implements ADListener {
        private ADListenerAdapter() {
        }

        /* synthetic */ ADListenerAdapter(BannerView bannerView, byte b) {
            this();
        }

        public void onADEvent(ADEvent aDEvent) {
            if (BannerView.this.b == null) {
                GDTLogger.i("No DevADListener Binded");
                return;
            }
            switch (aDEvent.getType()) {
                case 1:
                    if (aDEvent.getParas().length != 1 || !(aDEvent.getParas()[0] instanceof Integer)) {
                        GDTLogger.e("AdEvent.Paras error for Banner(" + aDEvent + ")");
                        return;
                    } else {
                        BannerView.this.b.onNoAD(((Integer) aDEvent.getParas()[0]).intValue());
                        return;
                    }
                case 2:
                    BannerView.this.b.onADReceiv();
                    return;
                case 3:
                    BannerView.this.b.onADExposure();
                    return;
                case 4:
                    BannerView.this.b.onADClosed();
                    return;
                case 5:
                    BannerView.this.b.onADClicked();
                    return;
                case 6:
                    BannerView.this.b.onADLeftApplication();
                    return;
                case 7:
                    BannerView.this.b.onADOpenOverlay();
                    return;
                case 8:
                    BannerView.this.b.onADCloseOverlay();
                    return;
                default:
                    return;
            }
        }
    }

    public BannerView(Activity activity, ADSize aDSize, String str, String str2) {
        super(activity);
        if (StringUtil.isEmpty(str) || StringUtil.isEmpty(str2) || activity == null) {
            GDTLogger.e(String.format("Banner ADView Contructor paras error,appid=%s,posId=%s,context=%s", str, str2, activity));
            return;
        }
        this.c = true;
        if (!a.a(activity)) {
            GDTLogger.e("Required Activity/Service/Permission Not Declared in AndroidManifest.xml");
            return;
        }
        this.d = true;
        setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        final Activity activity2 = activity;
        final String str3 = str;
        final ADSize aDSize2 = aDSize;
        final String str4 = str2;
        GDTADManager.INIT_EXECUTOR.execute(new Runnable() {
            public void run() {
                if (GDTADManager.getInstance().initWith(activity2, str3)) {
                    try {
                        final POFactory pOFactory = GDTADManager.getInstance().getPM().getPOFactory();
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.qq.e.ads.banner.BannerView.a(com.qq.e.ads.banner.BannerView, boolean):boolean
                             arg types: [com.qq.e.ads.banner.BannerView, int]
                             candidates:
                              com.qq.e.ads.banner.BannerView.a(com.qq.e.ads.banner.BannerView, com.qq.e.comm.pi.BVI):com.qq.e.comm.pi.BVI
                              com.qq.e.ads.banner.BannerView.a(com.qq.e.ads.banner.BannerView, boolean):boolean */
                            public void run() {
                                try {
                                    if (pOFactory != null) {
                                        BVI unused = BannerView.this.f1201a = pOFactory.getBannerView(activity2, aDSize2, str3, str4);
                                        BannerView.this.f1201a.setAdListener(new ADListenerAdapter(BannerView.this, (byte) 0));
                                        BannerView.this.addView(BannerView.this.f1201a.getView());
                                        boolean unused2 = BannerView.this.e = true;
                                        if (BannerView.this.h != null) {
                                            BannerView.this.setDownConfirmPilicy(BannerView.this.h);
                                        }
                                        if (BannerView.this.f != null) {
                                            BannerView.this.setRefresh(BannerView.this.f.intValue());
                                        }
                                        if (BannerView.this.g != null) {
                                            BannerView.this.setRollAnimation(BannerView.this.g);
                                        }
                                        if (BannerView.this.i != null) {
                                            BannerView.this.setShowClose(BannerView.this.i.booleanValue());
                                        }
                                        while (BannerView.f(BannerView.this) > 0) {
                                            BannerView.this.loadAD();
                                        }
                                    }
                                } catch (Throwable th) {
                                    GDTLogger.e("Exception while init Banner Core", th);
                                } finally {
                                    boolean unused3 = BannerView.this.e = true;
                                }
                            }
                        });
                    } catch (Throwable th) {
                        GDTLogger.e("Exception while init Banner plugin", th);
                    }
                } else {
                    GDTLogger.e("Fail to init ADManager");
                }
            }
        });
    }

    static /* synthetic */ int f(BannerView bannerView) {
        int i2 = bannerView.j;
        bannerView.j = i2 - 1;
        return i2;
    }

    public void destroy() {
        if (this.f1201a != null) {
            this.f1201a.destroy();
        }
    }

    public void loadAD() {
        if (!this.c || !this.d) {
            GDTLogger.e("Banner init Paras OR Context error,See More logs while new BannerView");
        } else if (!this.e) {
            this.j++;
        } else if (this.f1201a != null) {
            this.f1201a.fetchAd();
        } else {
            GDTLogger.e("Banner Init error,See More Logs");
        }
    }

    public void setADListener(BannerADListener bannerADListener) {
        this.b = bannerADListener;
    }

    public void setDownConfirmPilicy(DownAPPConfirmPolicy downAPPConfirmPolicy) {
        this.h = downAPPConfirmPolicy;
        if (downAPPConfirmPolicy != null && this.f1201a != null) {
            this.f1201a.setDownAPPConfirmPolicy(downAPPConfirmPolicy.value());
        }
    }

    public void setRefresh(int i2) {
        this.f = Integer.valueOf(i2);
        if (i2 < 30 && i2 != 0) {
            i2 = 30;
        } else if (i2 > 120) {
            i2 = 120;
        }
        if (this.f1201a != null) {
            this.f1201a.setRefresh(i2);
        }
    }

    public void setRollAnimation(BannerRollAnimation bannerRollAnimation) {
        this.g = bannerRollAnimation;
        if (bannerRollAnimation != null && this.f1201a != null) {
            this.f1201a.setRollAnimation(bannerRollAnimation.value());
        }
    }

    public void setShowClose(boolean z) {
        this.i = Boolean.valueOf(z);
        if (this.f1201a != null) {
            this.f1201a.setShowCloseButton(z);
        }
    }
}
