package com.cmsc.cmmusic.init;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.UUID;

public class DeviceUuidFactory {
    protected static final String PREFS_DEVICE_ID = "device_id";
    protected static final String PREFS_FILE = "device_id.xml";
    protected static DeviceUuidFactory uuidFactory;

    public static DeviceUuidFactory getInstance() {
        if (uuidFactory != null) {
            return uuidFactory;
        }
        DeviceUuidFactory deviceUuidFactory = new DeviceUuidFactory();
        uuidFactory = deviceUuidFactory;
        return deviceUuidFactory;
    }

    public String getUuid(Context context, int i) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREFS_FILE, 0);
        String string = sharedPreferences.getString("device_id" + i, null);
        if (string != null) {
            return string;
        }
        String replace = UUID.randomUUID().toString().replace("-", "");
        sharedPreferences.edit().putString("device_id", replace).commit();
        return replace;
    }
}
