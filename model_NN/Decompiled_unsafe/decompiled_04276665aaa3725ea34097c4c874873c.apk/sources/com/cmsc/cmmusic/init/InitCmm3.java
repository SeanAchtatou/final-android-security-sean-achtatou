package com.cmsc.cmmusic.init;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.lang.reflect.InvocationTargetException;
import java.util.Hashtable;

class InitCmm3 {
    static native Hashtable<String, String> initCmm2(Context context);

    InitCmm3() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.lang.String initCheck(android.content.Context r9) throws java.lang.Throwable {
        /*
            r1 = 1
            r2 = 0
            boolean r4 = com.cmsc.cmmusic.init.NetMode.simInserted(r9, r2)
            boolean r5 = com.cmsc.cmmusic.init.NetMode.simInserted(r9, r1)
            java.lang.String r0 = "SDK_LW_CMM"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r6 = "isInserted sim0:"
            r3.<init>(r6)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r6 = "_sim1:"
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            android.util.Log.i(r0, r3)
            if (r4 != 0) goto L_0x0032
            if (r5 != 0) goto L_0x0032
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException
            r0.<init>()
            throw r0
        L_0x0032:
            java.lang.String r3 = ""
            java.lang.String r0 = ""
            if (r4 == 0) goto L_0x0115
            java.lang.String r3 = com.cmsc.cmmusic.init.GetAppInfo.getIMSI(r9, r2)
            boolean r4 = com.cmsc.cmmusic.init.GetAppInfo.isValidityImsi(r3)
            if (r4 == 0) goto L_0x0115
            java.lang.String r4 = "SDK_LW_CMM"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "initCheck sim0_imsi:"
            r6.<init>(r7)
            java.lang.StringBuilder r6 = r6.append(r3)
            java.lang.String r6 = r6.toString()
            android.util.Log.i(r4, r6)
            r6 = r1
            r7 = r3
        L_0x0058:
            if (r5 == 0) goto L_0x0111
            java.lang.String r0 = com.cmsc.cmmusic.init.GetAppInfo.getIMSI(r9, r1)
            boolean r3 = com.cmsc.cmmusic.init.GetAppInfo.isValidityImsi(r0)
            if (r3 == 0) goto L_0x0111
            java.lang.String r3 = "SDK_LW_CMM"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "initCheck sim1_imsi:"
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            android.util.Log.i(r3, r4)
            r3 = r0
            r0 = r1
        L_0x007a:
            boolean r4 = r7.equals(r3)
            if (r4 == 0) goto L_0x008d
            java.lang.String r0 = "SDK_LW_CMM"
            java.lang.String r1 = "sima equals simb conver to singlesim!"
            android.util.Log.i(r0, r1)
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException
            r0.<init>()
            throw r0
        L_0x008d:
            if (r6 != 0) goto L_0x009e
            if (r0 != 0) goto L_0x009e
            java.lang.String r0 = "SDK_LW_CMM"
            java.lang.String r1 = "no CM_SIM"
            android.util.Log.i(r0, r1)
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException
            r0.<init>()
            throw r0
        L_0x009e:
            java.lang.String r5 = ""
            java.lang.String r4 = ""
            if (r6 == 0) goto L_0x010f
            java.lang.String r5 = "SDK_LW_CMM"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r8 = "which_0;imsi:"
            r6.<init>(r8)
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            android.util.Log.i(r5, r6)
            java.lang.String r2 = com.cmsc.cmmusic.init.InitCmm1.initCheck(r9, r2, r7)
        L_0x00bc:
            if (r0 == 0) goto L_0x010d
            java.lang.String r0 = "SDK_LW_CMM"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "which_1;imsi:"
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r3)
            java.lang.String r4 = r4.toString()
            android.util.Log.i(r0, r4)
            java.lang.String r0 = com.cmsc.cmmusic.init.InitCmm1.initCheck(r9, r1, r3)
        L_0x00d6:
            java.lang.String r1 = "SDK_LW_CMM"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "initCheck result: sim0_"
            r3.<init>(r4)
            java.lang.StringBuilder r3 = r3.append(r2)
            java.lang.String r4 = "-----sim1_"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            android.util.Log.i(r1, r3)
            java.lang.String r1 = com.cmsc.cmmusic.init.InitCmm1.INIT_SUCCESS
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x010a
            java.lang.String r1 = com.cmsc.cmmusic.init.InitCmm1.INIT_SUCCESS
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x010a
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException
            r0.<init>()
            throw r0
        L_0x010a:
            java.lang.String r0 = com.cmsc.cmmusic.init.InitCmm1.INIT_SUCCESS
            return r0
        L_0x010d:
            r0 = r4
            goto L_0x00d6
        L_0x010f:
            r2 = r5
            goto L_0x00bc
        L_0x0111:
            r3 = r0
            r0 = r2
            goto L_0x007a
        L_0x0115:
            r6 = r2
            r7 = r3
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cmsc.cmmusic.init.InitCmm3.initCheck(android.content.Context):java.lang.String");
    }

    private static Hashtable<String, String> initCmm(Context context) throws Throwable {
        String str = Build.MODEL;
        String str2 = Build.VERSION.RELEASE;
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        boolean simInserted = NetMode.simInserted(context, 0);
        boolean simInserted2 = NetMode.simInserted(context, 1);
        String str3 = "";
        String str4 = "";
        if (simInserted) {
            str3 = GetAppInfo.getIMSI(context, 0);
            if (GetAppInfo.isValidityImsi(str3)) {
                Log.i("SDK_LW_CMM", "sim1 exist, but null");
                str3 = "";
            }
        }
        if (simInserted2) {
            str4 = GetAppInfo.getIMSI(context, 1);
            if (GetAppInfo.isValidityImsi(str4)) {
                Log.i("SDK_LW_CMM", "sim2 exist, but null");
                str4 = "";
            }
        }
        Log.i("SDK_LW_CMM", "init2 calling");
        Log.i("SDK_LW_CMM", "chCode=" + GetAppInfo.getChannelCode(context) + ", devicemodel=" + str + ", deviceID=" + deviceId + ", release=" + str2 + ", subscriberID=" + str3 + "_" + str4);
        if ("sdk".equals(str)) {
            Log.i("SDK_LW_CMM", "google_sdk...模拟器运行...not apn setting");
        }
        if ("".equals(str3) && "".equals(str4)) {
            throw new NoSuchMethodException();
        } else if (!str3.equals(str4)) {
            return init2(context, str3, str4);
        } else {
            Log.i("SDK_LW_CMM", "sima equals simb conver to singlesim!");
            throw new NoSuchMethodException();
        }
    }

    private static Hashtable<String, String> init2(Context context, String str, String str2) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        if (!"".equals(str)) {
            if (!"".equals(str2)) {
                Log.i("SDK_LW_CMM", "sim sim");
                Hashtable<String, String> init1 = InitCmm1.init1(context, str, 0);
                Hashtable<String, String> init12 = InitCmm1.init1(context, str2, 1);
                Log.i("SDK_LW_CMM", "code-0:" + init1.get("code"));
                Log.i("SDK_LW_CMM", "code-1:" + init12.get("code"));
                if (InitCmm1.INIT_SUCCESS.equals(init1.get("code"))) {
                    init1.put("detail", String.valueOf(init1.get("code")) + " " + init12.get("code"));
                    return init1;
                } else if (InitCmm1.INIT_SUCCESS.equals(init12.get("code"))) {
                    init12.put("detail", String.valueOf(init1.get("code")) + " " + init12.get("code"));
                    return init12;
                } else {
                    Hashtable<String, String> hashtable = new Hashtable<>();
                    hashtable.put("code", Constants.VIA_REPORT_TYPE_WPA_STATE);
                    hashtable.put("detail", String.valueOf(init1.get("code")) + " " + init12.get("code"));
                    hashtable.put(SocialConstants.PARAM_APP_DESC, "双卡槽初始化都失败（可从detail字段查询每个卡槽失败的code）");
                    return hashtable;
                }
            } else {
                Log.i("SDK_LW_CMM", "sim null");
                return InitCmm1.init1(context, str, 0);
            }
        } else if (!"".equals(str2)) {
            Log.i("SDK_LW_CMM", "null sim");
            return InitCmm1.init1(context, str2, 1);
        } else {
            Log.i("SDK_LW_CMM", "null null");
            Hashtable<String, String> hashtable2 = new Hashtable<>();
            hashtable2.put("code", "4");
            hashtable2.put(SocialConstants.PARAM_APP_DESC, "无sim卡");
            return hashtable2;
        }
    }
}
