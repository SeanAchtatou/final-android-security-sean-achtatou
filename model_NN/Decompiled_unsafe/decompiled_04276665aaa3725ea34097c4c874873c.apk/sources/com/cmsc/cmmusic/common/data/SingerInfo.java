package com.cmsc.cmmusic.common.data;

public class SingerInfo {
    private String astro;
    private String birthCity;
    private String birthday;
    private String bloodType;
    private String country;
    private String englishName;
    private String height;
    private String imgUrl;
    private String name;
    private String nickName;
    private String sex;
    private String singerId;
    private String startPlace;

    public String getSingerId() {
        return this.singerId;
    }

    public void setSingerId(String str) {
        this.singerId = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getEnglishName() {
        return this.englishName;
    }

    public void setEnglishName(String str) {
        this.englishName = str;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String str) {
        this.country = str;
    }

    public String getSex() {
        return this.sex;
    }

    public void setSex(String str) {
        this.sex = str;
    }

    public String getBloodType() {
        return this.bloodType;
    }

    public void setBloodType(String str) {
        this.bloodType = str;
    }

    public String getAstro() {
        return this.astro;
    }

    public void setAstro(String str) {
        this.astro = str;
    }

    public String getBirthday() {
        return this.birthday;
    }

    public void setBirthday(String str) {
        this.birthday = str;
    }

    public String getBirthCity() {
        return this.birthCity;
    }

    public void setBirthCity(String str) {
        this.birthCity = str;
    }

    public String getImgUrl() {
        return this.imgUrl;
    }

    public void setImgUrl(String str) {
        this.imgUrl = str;
    }

    public String getNickName() {
        return this.nickName;
    }

    public void setNickName(String str) {
        this.nickName = str;
    }

    public String getHeight() {
        return this.height;
    }

    public void setHeight(String str) {
        this.height = str;
    }

    public String getStartPlace() {
        return this.startPlace;
    }

    public void setStartPlace(String str) {
        this.startPlace = str;
    }

    public String toString() {
        return "SingerInfo [singerId=" + this.singerId + ", name=" + this.name + ", englishName=" + this.englishName + ", country=" + this.country + ", sex=" + this.sex + ", bloodType=" + this.bloodType + ", astro=" + this.astro + ", birthday=" + this.birthday + ", birthCity=" + this.birthCity + ", imgUrl=" + this.imgUrl + ", nickName=" + this.nickName + ", height=" + this.height + ", startPlace=" + this.startPlace + "]";
    }
}
