package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.ClubUserInfo;
import com.cmsc.cmmusic.common.data.MusicInfo;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.tencent.connect.common.Constants;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

final class AutoOpenRingbackOrderRingbackView extends OrderView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = null;
    private static final String LOG_TAG = "AutoOpenRingbackOrderRingbackView";
    private TextView notes;
    private TextView txtInvalidDate;
    private TextView txtPrice;
    private TextView userLevel;

    static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
        int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
        if (iArr == null) {
            iArr = new int[OrderPolicy.OrderType.values().length];
            try {
                iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
        }
        return iArr;
    }

    public AutoOpenRingbackOrderRingbackView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        ClubUserInfo clubUserInfo;
        String str;
        setUserTip("点击“确认”将该歌曲设置为您的彩铃");
        MusicInfo musicInfo = this.policyObj.getMusicInfo();
        if (musicInfo != null) {
            this.txtInvalidDate.setText("彩铃有效期：" + getFormatTime(musicInfo));
            BizInfo bizInfo = getBizInfo();
            List<ClubUserInfo> clubUserInfos = this.policyObj.getClubUserInfos();
            if (clubUserInfos == null || clubUserInfos.size() <= 0) {
                clubUserInfo = null;
            } else {
                clubUserInfo = clubUserInfos.get(0);
            }
            if (clubUserInfo != null) {
                str = clubUserInfo.getMemLevel();
            } else {
                str = null;
            }
            this.txtPrice.setText("彩铃价格: " + EnablerInterface.getPriceString(bizInfo.getSalePrice()));
            if ("3".equals(str)) {
                this.userLevel.setText("\n会员级别: 特级会员");
                this.notes.setText("");
                this.notes.setVisibility(8);
            } else {
                this.userLevel.setText("\n会员级别: 非特级会员");
                this.notes.setText("\n提示:\n1、订购彩铃将会开通彩铃功能；\n2、开通彩铃功能价格: 5元/月");
                this.notes.setVisibility(0);
            }
        }
        Log.d(LOG_TAG, "get url by net. user : " + this.policyObj.getMobile() + " , price : " + ((Object) this.txtPrice.getText()));
        Log.d(LOG_TAG, "orderType : " + this.orderType + " , songName : " + this.curSongName + " , singerName : " + this.curSingerName);
    }

    private BizInfo getBizInfo() {
        ArrayList<BizInfo> bizInfos = this.policyObj.getBizInfos();
        if (bizInfos == null) {
            return null;
        }
        Iterator<BizInfo> it = bizInfos.iterator();
        while (it.hasNext()) {
            BizInfo next = it.next();
            if (next != null) {
                String bizType = next.getBizType();
                if ("00".equals(bizType) || "70".equals(bizType) || Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE.equals(bizType)) {
                    return next;
                }
            }
        }
        return bizInfos.get(0);
    }

    private String getFormatTime(MusicInfo musicInfo) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        try {
            Date parse = simpleDateFormat.parse(musicInfo.getCrbtValidity());
            simpleDateFormat.applyPattern("yyyy-MM-dd");
            return simpleDateFormat.format(parse);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        Log.d(LOG_TAG, "sure button clicked");
        this.mCurActivity.showProgressBar("请稍候...");
        try {
            switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[this.orderType.ordinal()]) {
                case 1:
                    Log.d(LOG_TAG, "buy Ringback");
                    BizInfo bizInfo = getBizInfo();
                    if (bizInfo == null) {
                        this.mCurActivity.hideProgressBar();
                        return;
                    } else {
                        EnablerInterface.buyRingBackByOpenRingBack(this.mCurActivity, this.curMusicID, bizInfo.getBizCode(), bizInfo.getBizType(), new CMMusicCallback<OrderResult>() {
                            public void operationResult(OrderResult orderResult) {
                                AutoOpenRingbackOrderRingbackView.this.mCurActivity.closeActivity(orderResult);
                            }
                        });
                        return;
                    }
                case 2:
                    Log.d(LOG_TAG, "Get download url by sms");
                    this.mCurActivity.closeActivity(null);
                    return;
                case 3:
                    Log.d(LOG_TAG, "Get download url by sign code");
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.mCurActivity.hideProgressBar();
        }
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        linearLayout.addView(getMemInfoView());
        this.userLevel = new TextView(this.mCurActivity);
        this.userLevel.setTextAppearance(this.mCurActivity, 16973892);
        linearLayout.addView(this.userLevel);
        this.txtInvalidDate = new TextView(this.mCurActivity);
        this.txtInvalidDate.setTextAppearance(this.mCurActivity, 16973892);
        linearLayout.addView(this.txtInvalidDate);
        this.txtPrice = new TextView(this.mCurActivity);
        this.txtPrice.setTextAppearance(this.mCurActivity, 16973892);
        linearLayout.addView(this.txtPrice);
        this.notes = new TextView(this.mCurActivity);
        this.notes.setTextAppearance(this.mCurActivity, 16973892);
        linearLayout.addView(this.notes);
    }
}
