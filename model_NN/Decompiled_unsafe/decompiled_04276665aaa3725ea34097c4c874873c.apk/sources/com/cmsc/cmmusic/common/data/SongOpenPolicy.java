package com.cmsc.cmmusic.common.data;

import java.util.List;

public class SongOpenPolicy extends Result {
    private String mobile;
    private List<SongMonthInfo> songMonthInfos;

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String str) {
        this.mobile = str;
    }

    public List<SongMonthInfo> getSongMonthInfos() {
        return this.songMonthInfos;
    }

    public void setSongMonthInfos(List<SongMonthInfo> list) {
        this.songMonthInfos = list;
    }

    public String toString() {
        return "SongOpenPolicy [mobile=" + this.mobile + ", songMonthInfos=" + this.songMonthInfos + "]";
    }
}
