package com.cmsc.cmmusic.common.data;

public class AccountInfo {
    private String accountName;
    private String accountType;
    private String createTime;
    private String updateTime;
    private String verified;

    public String getAccountName() {
        return this.accountName;
    }

    public void setAccountName(String str) {
        this.accountName = str;
    }

    public String getAccountType() {
        return this.accountType;
    }

    public void setAccountType(String str) {
        this.accountType = str;
    }

    public String getVerified() {
        return this.verified;
    }

    public void setVerified(String str) {
        this.verified = str;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(String str) {
        this.createTime = str;
    }

    public String getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(String str) {
        this.updateTime = str;
    }

    public String toString() {
        return "AccountInfo [accountName=" + this.accountName + ", accountType=" + this.accountType + ", verified=" + this.verified + ", createTime=" + this.createTime + ", updateTime=" + this.updateTime + "]";
    }
}
