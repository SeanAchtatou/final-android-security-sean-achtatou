package com.cmsc.cmmusic.common;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class DesUtils {
    private Cipher decryptCipher = null;

    public static byte[] hexStr2ByteArr(String str) throws Exception {
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) Integer.parseInt(new String(bytes, i, 2), 16);
        }
        return bArr;
    }

    public DesUtils(String str) throws Exception {
        Key key = getKey(str.getBytes());
        this.decryptCipher = Cipher.getInstance("DES");
        this.decryptCipher.init(2, key);
    }

    public byte[] decrypt(byte[] bArr) throws Exception {
        return this.decryptCipher.doFinal(bArr);
    }

    public String decrypt(String str) throws Exception {
        return new String(decrypt(hexStr2ByteArr(str)));
    }

    private Key getKey(byte[] bArr) throws Exception {
        byte[] bArr2 = new byte[8];
        int i = 0;
        while (i < bArr.length && i < bArr2.length) {
            bArr2[i] = bArr[i];
            i++;
        }
        return new SecretKeySpec(bArr2, "DES");
    }
}
