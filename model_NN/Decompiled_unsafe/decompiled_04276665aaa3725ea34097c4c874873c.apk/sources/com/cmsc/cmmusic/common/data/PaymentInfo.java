package com.cmsc.cmmusic.common.data;

public class PaymentInfo {
    private String accessPlatformID;
    private String amount;
    private String currencyType;
    private String dealResult;
    private String description;
    private String identityID;
    private String oldBalance;
    private String optTime;
    private String optType;
    private String orderContent;
    private String orderDesc;
    private String orderNum;
    private String payChannel;
    private String relationID;
    private String tradeNum;

    public String getIdentityID() {
        return this.identityID;
    }

    public void setIdentityID(String str) {
        this.identityID = str;
    }

    public String getCurrencyType() {
        return this.currencyType;
    }

    public void setCurrencyType(String str) {
        this.currencyType = str;
    }

    public String getTradeNum() {
        return this.tradeNum;
    }

    public void setTradeNum(String str) {
        this.tradeNum = str;
    }

    public String getOptType() {
        return this.optType;
    }

    public void setOptType(String str) {
        this.optType = str;
    }

    public String getAmount() {
        return this.amount;
    }

    public void setAmount(String str) {
        this.amount = str;
    }

    public String getRelationID() {
        return this.relationID;
    }

    public void setRelationID(String str) {
        this.relationID = str;
    }

    public String getOrderNum() {
        return this.orderNum;
    }

    public void setOrderNum(String str) {
        this.orderNum = str;
    }

    public String getOrderContent() {
        return this.orderContent;
    }

    public void setOrderContent(String str) {
        this.orderContent = str;
    }

    public String getOrderDesc() {
        return this.orderDesc;
    }

    public void setOrderDesc(String str) {
        this.orderDesc = str;
    }

    public String getOldBalance() {
        return this.oldBalance;
    }

    public void setOldBalance(String str) {
        this.oldBalance = str;
    }

    public String getDealResult() {
        return this.dealResult;
    }

    public void setDealResult(String str) {
        this.dealResult = str;
    }

    public String getOptTime() {
        return this.optTime;
    }

    public void setOptTime(String str) {
        this.optTime = str;
    }

    public String getAccessPlatformID() {
        return this.accessPlatformID;
    }

    public void setAccessPlatformID(String str) {
        this.accessPlatformID = str;
    }

    public String getPayChannel() {
        return this.payChannel;
    }

    public void setPayChannel(String str) {
        this.payChannel = str;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String str) {
        this.description = str;
    }

    public String toString() {
        return "PaymentInfo [identityID=" + this.identityID + ", currencyType=" + this.currencyType + ", tradeNum=" + this.tradeNum + ", optType=" + this.optType + ", amount=" + this.amount + ", relationID=" + this.relationID + ", orderNum=" + this.orderNum + ", orderContent=" + this.orderContent + ", orderDesc=" + this.orderDesc + ", oldBalance=" + this.oldBalance + ", dealResult=" + this.dealResult + ", optTime=" + this.optTime + ", accessPlatformID=" + this.accessPlatformID + ", payChannel=" + this.payChannel + ", description=" + this.description + "]";
    }
}
