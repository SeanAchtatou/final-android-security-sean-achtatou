package com.cmsc.cmmusic.common.data;

public class OrderResult extends Result {
    private String downUrl;
    private String orderId;
    private int resultCode;

    public OrderResult() {
    }

    public OrderResult(Result result) {
        if (result != null) {
            setResCode(result.getResCode());
            setResMsg(result.getResMsg());
        }
    }

    public int getResultCode() {
        return this.resultCode;
    }

    public void setResultCode(int i) {
        this.resultCode = i;
    }

    public String getOrderId() {
        return this.orderId;
    }

    public void setOrderId(String str) {
        this.orderId = str;
    }

    public String getDownUrl() {
        return this.downUrl;
    }

    public void setDownUrl(String str) {
        this.downUrl = str;
    }

    public String toString() {
        return "OrderResult [resCode=" + getResCode() + ", resMsg=" + getResMsg() + ", resultCode=" + this.resultCode + ", orderId=" + this.orderId + ", downUrl=" + this.downUrl + "]";
    }
}
