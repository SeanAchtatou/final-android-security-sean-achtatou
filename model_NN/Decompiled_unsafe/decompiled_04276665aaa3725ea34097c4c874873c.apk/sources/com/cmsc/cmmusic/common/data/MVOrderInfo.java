package com.cmsc.cmmusic.common.data;

public class MVOrderInfo {
    private String name;
    private String serviceId;
    private String type;

    public String getServiceId() {
        return this.serviceId;
    }

    public void setServiceId(String str) {
        this.serviceId = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String str) {
        this.type = str;
    }

    public String toString() {
        return "MVOrderInfo [serviceId=" + this.serviceId + ", name=" + this.name + ", type=" + this.type + "]";
    }
}
