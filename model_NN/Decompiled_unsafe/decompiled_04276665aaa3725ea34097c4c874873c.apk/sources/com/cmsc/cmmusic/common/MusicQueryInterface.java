package com.cmsc.cmmusic.common;

import android.content.Context;
import com.cmsc.cmmusic.common.data.AlbumInfo;
import com.cmsc.cmmusic.common.data.AlbumListRsp;
import com.cmsc.cmmusic.common.data.ChartInfo;
import com.cmsc.cmmusic.common.data.ChartListRsp;
import com.cmsc.cmmusic.common.data.MusicInfo;
import com.cmsc.cmmusic.common.data.MusicInfoResult;
import com.cmsc.cmmusic.common.data.MusicListRsp;
import com.cmsc.cmmusic.common.data.SingerInfo;
import com.cmsc.cmmusic.common.data.SingerInfoRsp;
import com.cmsc.cmmusic.common.data.TagInfo;
import com.cmsc.cmmusic.common.data.TagListRsp;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class MusicQueryInterface {
    public static ChartListRsp getChartInfo(Context context, int i, int i2) {
        try {
            return getChartInfo(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/search/chart/list", EnablerInterface.buildRequsetXml("<pageNumber>" + i + "</pageNumber><numberPerPage>" + i2 + "</numberPerPage>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static MusicListRsp getMusicsByChartId(Context context, String str, int i, int i2) {
        try {
            return getMusics(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/search/music/listbychart", EnablerInterface.buildRequsetXml("<chartCode>" + str + "</chartCode><pageNumber>" + i + "</pageNumber><numberPerPage>" + i2 + "</numberPerPage>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static MusicListRsp getMusicsByAlbumId(Context context, String str, int i, int i2) {
        try {
            return getMusics(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/search/music/listbyalbum", EnablerInterface.buildRequsetXml("<albumId>" + str + "</albumId><pageNumber>" + i + "</pageNumber><numberPerPage>" + i2 + "</numberPerPage>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static MusicListRsp getMusicsBySingerId(Context context, String str, int i, int i2) {
        try {
            return getMusics(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/search/music/listbysinger", EnablerInterface.buildRequsetXml("<singerId>" + str + "</singerId><pageNumber>" + i + "</pageNumber><numberPerPage>" + i2 + "</numberPerPage>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static SingerInfoRsp getSingerInfo(Context context, String str) {
        try {
            return getSingerInfo(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/search/singer/infobyid", EnablerInterface.buildRequsetXml("<singerID>" + str + "</singerID>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static MusicListRsp getMusicsByTagId(Context context, String str, int i, int i2) {
        try {
            return getMusics(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/search/music/listbytag", EnablerInterface.buildRequsetXml("<tagId>" + str + "</tagId><pageNumber>" + i + "</pageNumber><numberPerPage>" + i2 + "</numberPerPage>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static MusicListRsp getMusicsByKey(Context context, String str, String str2, int i, int i2) {
        try {
            return getMusics(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/search/music/listbykey", EnablerInterface.buildRequsetXml("<key>" + str + "</key><keyType>" + URLEncoder.encode(str2, "UTF-8") + "</keyType><pageNumber>" + i + "</pageNumber><numberPerPage>" + i2 + "</numberPerPage>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static MusicInfoResult getMusicInfoByMusicId(Context context, String str) {
        try {
            return EnablerInterface.getMusicInfo(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/search/music/querybymusic", EnablerInterface.buildRequsetXml("<musicId>" + str + "</musicId>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static TagListRsp getTags(Context context, String str, int i, int i2) {
        if (str == null) {
            str = "";
        }
        try {
            return getTags(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/search/tag/list", EnablerInterface.buildRequsetXml("<tagId>" + str + "</tagId><pageNumber>" + i + "</pageNumber><numberPerPage>" + i2 + "</numberPerPage>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static TagListRsp getTags(InputStream inputStream) throws IOException, XmlPullParserException {
        TagInfo tagInfo;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        TagListRsp tagListRsp = new TagListRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            TagInfo tagInfo2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        tagInfo = tagInfo2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        tagInfo2 = tagInfo;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            tagListRsp.setResCode(newPullParser.nextText());
                            tagInfo = tagInfo2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            tagListRsp.setResMsg(newPullParser.nextText());
                            tagInfo = tagInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("resCounter")) {
                            tagListRsp.setResCounter(newPullParser.nextText());
                            tagInfo = tagInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("TagInfo")) {
                            tagInfo = new TagInfo();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("tagId")) {
                            tagInfo2.setTagId(newPullParser.nextText());
                            tagInfo = tagInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase(SelectCountryActivity.EXTRA_COUNTRY_NAME)) {
                            tagInfo2.setName(newPullParser.nextText());
                            tagInfo = tagInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("description")) {
                            tagInfo2.setDescription(newPullParser.nextText());
                            tagInfo = tagInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("hasChild")) {
                            tagInfo2.setHasChild(newPullParser.nextText());
                            tagInfo = tagInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("imgUrl")) {
                            tagInfo2.setImgUrl(newPullParser.nextText());
                            tagInfo = tagInfo2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        tagInfo2 = tagInfo;
                        break;
                    case 3:
                        if (name.equalsIgnoreCase("TagInfo")) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                tagListRsp.setTagInfos(arrayList2);
                            }
                            arrayList2.add(tagInfo2);
                            break;
                        }
                        break;
                }
                tagInfo = tagInfo2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                tagInfo2 = tagInfo;
            }
            return tagListRsp;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                }
            }
        }
    }

    public static AlbumListRsp getAlbumsBySingerId(Context context, String str, int i, int i2) {
        try {
            return getAlbums(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/search/album/listbysinger", EnablerInterface.buildRequsetXml("<singerId>" + str + "</singerId><pageNumber>" + i + "</pageNumber><numberPerPage>" + i2 + "</numberPerPage>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static AlbumListRsp getAlbumsByMusicId(Context context, String str, int i, int i2) {
        try {
            return getAlbums(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/search/album/listbymusic", EnablerInterface.buildRequsetXml("<musicId>" + str + "</musicId><pageNumber>" + i + "</pageNumber><numberPerPage>" + i2 + "</numberPerPage>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static AlbumListRsp getAlbums(InputStream inputStream) throws IOException, XmlPullParserException {
        AlbumInfo albumInfo;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        AlbumListRsp albumListRsp = new AlbumListRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            AlbumInfo albumInfo2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        albumInfo = albumInfo2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        albumInfo2 = albumInfo;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            albumListRsp.setResCode(newPullParser.nextText());
                            albumInfo = albumInfo2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            albumListRsp.setResMsg(newPullParser.nextText());
                            albumInfo = albumInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("resCounter")) {
                            albumListRsp.setResCounter(newPullParser.nextText());
                            albumInfo = albumInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("AlbumInfo")) {
                            albumInfo = new AlbumInfo();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("albumId")) {
                            albumInfo2.setAlbumId(newPullParser.nextText());
                            albumInfo = albumInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase(SelectCountryActivity.EXTRA_COUNTRY_NAME)) {
                            albumInfo2.setName(newPullParser.nextText());
                            albumInfo = albumInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("description")) {
                            albumInfo2.setDescription(newPullParser.nextText());
                            albumInfo = albumInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("language")) {
                            albumInfo2.setLanguage(newPullParser.nextText());
                            albumInfo = albumInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("singerName")) {
                            albumInfo2.setSingerName(newPullParser.nextText());
                            albumInfo = albumInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("singerId")) {
                            albumInfo2.setSingerId(newPullParser.nextText());
                            albumInfo = albumInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("imgUrl")) {
                            albumInfo2.setImgUrl(newPullParser.nextText());
                            albumInfo = albumInfo2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        albumInfo2 = albumInfo;
                        break;
                    case 3:
                        if (name.equalsIgnoreCase("AlbumInfo")) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                albumListRsp.setAlbumInfos(arrayList2);
                            }
                            arrayList2.add(albumInfo2);
                            break;
                        }
                        break;
                }
                albumInfo = albumInfo2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                albumInfo2 = albumInfo;
            }
            return albumListRsp;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                }
            }
        }
    }

    private static MusicListRsp getMusics(InputStream inputStream) throws IOException, XmlPullParserException {
        MusicInfo musicInfo;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        MusicListRsp musicListRsp = new MusicListRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            MusicInfo musicInfo2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        musicInfo = musicInfo2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        musicInfo2 = musicInfo;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            musicListRsp.setResCode(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            musicListRsp.setResMsg(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("resCounter")) {
                            musicListRsp.setResCounter(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("MusicInfo")) {
                            musicInfo = new MusicInfo();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("musicId")) {
                            musicInfo2.setMusicId(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase(WBPageConstants.ParamKey.COUNT)) {
                            musicInfo2.setCount(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("crbtValidity")) {
                            musicInfo2.setCrbtValidity(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("price")) {
                            musicInfo2.setPrice(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("songName")) {
                            musicInfo2.setSongName(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("singerName")) {
                            musicInfo2.setSingerName(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("singerId")) {
                            musicInfo2.setSingerId(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("ringValidity")) {
                            musicInfo2.setRingValidity(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("songValidity")) {
                            musicInfo2.setSongValidity(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("albumPicDir")) {
                            musicInfo2.setAlbumPicDir(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("singerPicDir")) {
                            musicInfo2.setSingerPicDir(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("crbtListenDir")) {
                            musicInfo2.setCrbtListenDir(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("ringListenDir")) {
                            musicInfo2.setRingListenDir(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("songListenDir")) {
                            musicInfo2.setSongListenDir(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("lrcDir")) {
                            musicInfo2.setLrcDir(newPullParser.nextText());
                            musicInfo = musicInfo2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        musicInfo2 = musicInfo;
                        break;
                    case 3:
                        if (name.equalsIgnoreCase("MusicInfo")) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                musicListRsp.setMusics(arrayList2);
                            }
                            arrayList2.add(musicInfo2);
                            break;
                        }
                        break;
                }
                musicInfo = musicInfo2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                musicInfo2 = musicInfo;
            }
            return musicListRsp;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                }
            }
        }
    }

    private static ChartListRsp getChartInfo(InputStream inputStream) throws IOException, XmlPullParserException {
        ChartInfo chartInfo;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        ChartListRsp chartListRsp = new ChartListRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            ChartInfo chartInfo2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        chartInfo = chartInfo2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        chartInfo2 = chartInfo;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            chartListRsp.setResCode(newPullParser.nextText());
                            chartInfo = chartInfo2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            chartListRsp.setResMsg(newPullParser.nextText());
                            chartInfo = chartInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("resCounter")) {
                            chartListRsp.setResCounter(newPullParser.nextText());
                            chartInfo = chartInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("ChartInfo")) {
                            chartInfo = new ChartInfo();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("chartCode")) {
                            chartInfo2.setChartCode(newPullParser.nextText());
                            chartInfo = chartInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("chartName")) {
                            chartInfo2.setChartName(newPullParser.nextText());
                            chartInfo = chartInfo2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        chartInfo2 = chartInfo;
                        break;
                    case 3:
                        if (name.equalsIgnoreCase("ChartInfo")) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                chartListRsp.setChartInfos(arrayList2);
                            }
                            arrayList2.add(chartInfo2);
                            break;
                        }
                        break;
                }
                chartInfo = chartInfo2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                chartInfo2 = chartInfo;
            }
            return chartListRsp;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                }
            }
        }
    }

    private static SingerInfoRsp getSingerInfo(InputStream inputStream) throws XmlPullParserException, IOException {
        if (inputStream == null) {
            return null;
        }
        SingerInfoRsp singerInfoRsp = new SingerInfoRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            SingerInfo singerInfo = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("SingerInfo")) {
                                    if (!name.equalsIgnoreCase("singerId")) {
                                        if (!name.equalsIgnoreCase(SelectCountryActivity.EXTRA_COUNTRY_NAME)) {
                                            if (!name.equalsIgnoreCase("englishName")) {
                                                if (!name.equalsIgnoreCase("country")) {
                                                    if (!name.equalsIgnoreCase("sex")) {
                                                        if (!name.equalsIgnoreCase("bloodType")) {
                                                            if (!name.equalsIgnoreCase("astro")) {
                                                                if (!name.equalsIgnoreCase("birthday")) {
                                                                    if (!name.equalsIgnoreCase("birthCity")) {
                                                                        if (!name.equalsIgnoreCase("imgUrl")) {
                                                                            if (!name.equalsIgnoreCase("nickName")) {
                                                                                if (!name.equalsIgnoreCase("height")) {
                                                                                    if (!name.equalsIgnoreCase("startPlace")) {
                                                                                        break;
                                                                                    } else {
                                                                                        singerInfo.setStartPlace(newPullParser.nextText());
                                                                                        break;
                                                                                    }
                                                                                } else {
                                                                                    singerInfo.setHeight(newPullParser.nextText());
                                                                                    break;
                                                                                }
                                                                            } else {
                                                                                singerInfo.setNickName(newPullParser.nextText());
                                                                                break;
                                                                            }
                                                                        } else {
                                                                            singerInfo.setImgUrl(newPullParser.nextText());
                                                                            break;
                                                                        }
                                                                    } else {
                                                                        singerInfo.setBirthCity(newPullParser.nextText());
                                                                        break;
                                                                    }
                                                                } else {
                                                                    singerInfo.setBirthday(newPullParser.nextText());
                                                                    break;
                                                                }
                                                            } else {
                                                                singerInfo.setAstro(newPullParser.nextText());
                                                                break;
                                                            }
                                                        } else {
                                                            singerInfo.setBloodType(newPullParser.nextText());
                                                            break;
                                                        }
                                                    } else {
                                                        singerInfo.setSex(newPullParser.nextText());
                                                        break;
                                                    }
                                                } else {
                                                    singerInfo.setCountry(newPullParser.nextText());
                                                    break;
                                                }
                                            } else {
                                                singerInfo.setEnglishName(newPullParser.nextText());
                                                break;
                                            }
                                        } else {
                                            singerInfo.setName(newPullParser.nextText());
                                            break;
                                        }
                                    } else {
                                        singerInfo.setSingerId(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    singerInfo = new SingerInfo();
                                    break;
                                }
                            } else {
                                singerInfoRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            singerInfoRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                    case 3:
                        if (!name.equalsIgnoreCase("SingerInfo")) {
                            break;
                        } else {
                            singerInfoRsp.setSingerInfo(singerInfo);
                            break;
                        }
                }
            }
            return singerInfoRsp;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }
}
