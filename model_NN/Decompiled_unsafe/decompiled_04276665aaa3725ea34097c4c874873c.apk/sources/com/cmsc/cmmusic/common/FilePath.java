package com.cmsc.cmmusic.common;

import android.os.Environment;

public class FilePath {
    public static final String CMMUSIC = "/CMMUSIC/";
    public static final String DEFAULT_PATH = "";
    public static final String SDCARD = getSdcardPath();
    public static final String SPEC_MEMBER = "Member/";

    public static boolean isCanUseSdCard() {
        try {
            return Environment.getExternalStorageState().equals("mounted");
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getSdcardPath() {
        if (isCanUseSdCard()) {
            return Environment.getExternalStorageDirectory().getPath();
        }
        return null;
    }
}
