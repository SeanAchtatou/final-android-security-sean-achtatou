package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.method.DigitsKeyListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.CrbtOpenCheckRsp;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.init.Utils;

final class CrbtOpenCheckView extends BaseView {
    private EditText edPhoneNum = new EditText(this.mCurActivity);

    public CrbtOpenCheckView(Context context, Bundle bundle) {
        super(context, bundle);
        setVisibility(0);
        TextView textView = new TextView(this.mCurActivity);
        textView.setTextAppearance(this.mCurActivity, 16973892);
        textView.setText("\n尊敬的用户，您正在使用查询对应手机号是否开通了彩铃功能。");
        this.rootView.addView(textView);
        this.edPhoneNum.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.edPhoneNum.setHint("请输入要查询的手机号码");
        this.edPhoneNum.setInputType(3);
        this.edPhoneNum.setKeyListener(new DigitsKeyListener(false, false));
        this.rootView.addView(this.edPhoneNum);
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        final String editable = this.edPhoneNum.getText().toString();
        if (editable == null || !Utils.validatePhoneNumber(editable)) {
            this.edPhoneNum.requestFocus();
            this.edPhoneNum.setError(Html.fromHtml("<font color='red'>请正确输入手机号码</font>"));
            return;
        }
        this.mCurActivity.showProgressBar("请稍候...");
        new Thread(new Runnable() {
            public void run() {
                try {
                    CrbtOpenCheckRsp crbtOpenCheck = EnablerInterface.crbtOpenCheck(CrbtOpenCheckView.this.mCurActivity, editable);
                    if (crbtOpenCheck != null && "000000".equals(crbtOpenCheck.getResCode())) {
                        CrbtOpenCheckView.this.mCurActivity.closeActivity(crbtOpenCheck);
                    } else if (crbtOpenCheck != null) {
                        CrbtOpenCheckView.this.mCurActivity.closeActivity(crbtOpenCheck);
                    } else {
                        CrbtOpenCheckView.this.mCurActivity.showToast("查询失败，请重试");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CrbtOpenCheckView.this.mCurActivity.showToast("查询失败，请重试");
                } finally {
                    CrbtOpenCheckView.this.mCurActivity.hideProgressBar();
                }
            }
        }).start();
    }

    /* access modifiers changed from: package-private */
    public void updateView(OrderPolicy orderPolicy) {
    }
}
