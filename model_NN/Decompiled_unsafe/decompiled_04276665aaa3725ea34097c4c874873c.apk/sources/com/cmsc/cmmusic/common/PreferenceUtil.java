package com.cmsc.cmmusic.common;

import android.content.Context;
import android.content.SharedPreferences;

final class PreferenceUtil {
    private static final String CHECKSTATE = "CHECKSTATE";
    private static final String FIRST_USE = "FIRST_USE";
    private static final String NAME = "NAME";

    PreferenceUtil() {
    }

    static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(context.getApplicationInfo().name, 0);
    }

    static String getName(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences == null) {
            return "";
        }
        return preferences.getString(NAME, "");
    }

    static void saveName(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString(NAME, str);
            edit.commit();
        }
    }

    public static boolean getCheckState(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences == null) {
            return false;
        }
        return preferences.getBoolean(CHECKSTATE, false);
    }

    public static void saveCheckState(Context context, boolean z) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putBoolean(CHECKSTATE, z);
            edit.commit();
        }
    }

    public static boolean getFirstUse(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences == null) {
            return false;
        }
        return preferences.getBoolean(FIRST_USE, true);
    }

    public static void saveFirstUse(Context context, boolean z) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putBoolean(FIRST_USE, z);
            edit.commit();
        }
    }

    static String getPlayUri(Context context) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences == null) {
            return "";
        }
        return preferences.getString("playuri", "");
    }

    static void savePlayUri(Context context, String str) {
        SharedPreferences preferences = getPreferences(context);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString("playuri", str);
            edit.commit();
        }
    }
}
