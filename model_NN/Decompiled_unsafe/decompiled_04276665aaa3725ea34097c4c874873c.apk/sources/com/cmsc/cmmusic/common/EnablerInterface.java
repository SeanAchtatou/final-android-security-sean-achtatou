package com.cmsc.cmmusic.common;

import android.content.Context;
import android.util.Log;
import cn.banshenggua.aichang.utils.StringUtil;
import com.cmsc.cmmusic.common.data.AccountInfo;
import com.cmsc.cmmusic.common.data.AlbumInfo;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.ClubUserInfo;
import com.cmsc.cmmusic.common.data.CrbtOpenCheckRsp;
import com.cmsc.cmmusic.common.data.DownloadRsp;
import com.cmsc.cmmusic.common.data.GetUserInfoRsp;
import com.cmsc.cmmusic.common.data.LoginResult;
import com.cmsc.cmmusic.common.data.MVInfo;
import com.cmsc.cmmusic.common.data.MVMonthPolicy;
import com.cmsc.cmmusic.common.data.MVOrderInfo;
import com.cmsc.cmmusic.common.data.MemberOpenPolicy;
import com.cmsc.cmmusic.common.data.MusicInfo;
import com.cmsc.cmmusic.common.data.MusicInfoResult;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.OwnRingBackgroundMusicRsp;
import com.cmsc.cmmusic.common.data.OwnRingRsp;
import com.cmsc.cmmusic.common.data.PaymentUserInfo;
import com.cmsc.cmmusic.common.data.RegistResult;
import com.cmsc.cmmusic.common.data.RegistRsp;
import com.cmsc.cmmusic.common.data.Result;
import com.cmsc.cmmusic.common.data.ServiceExInfo;
import com.cmsc.cmmusic.common.data.SmsLoginAuthResult;
import com.cmsc.cmmusic.common.data.SongMonthInfo;
import com.cmsc.cmmusic.common.data.SongOpenPolicy;
import com.cmsc.cmmusic.common.data.SongRecommendContentIds;
import com.cmsc.cmmusic.common.data.SongRecommendResult;
import com.cmsc.cmmusic.common.data.TransferResult;
import com.cmsc.cmmusic.common.data.UserInfo;
import com.cmsc.cmmusic.init.InitCmmInterface;
import com.cmsc.cmmusic.init.NetMode;
import com.igexin.assist.sdk.AssistPushConsts;
import com.meizu.cloud.pushsdk.platform.message.BasicPushStatus;
import com.migu.sdk.api.CommonPayInfo;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.open.SocialConstants;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

final class EnablerInterface {
    EnablerInterface() {
    }

    static OrderPolicy getOrderPolicy(Context context, String str, OrderPolicy.OrderPolicyType orderPolicyType, boolean z) throws IOException, XmlPullParserException {
        OrderPolicy orderPolicy = new OrderPolicy();
        if (!NetMode.isConnected(context)) {
            Log.d("getOrderPolicy", "not Connected");
            orderPolicy.setResCode("000000");
            orderPolicy.setOrderType(OrderPolicy.OrderType.sms);
            return orderPolicy;
        } else if (InitCmmInterface.isGetPhoneNumber(context)) {
            Log.d("initCheck", "Connected by net");
            return requsetOrderPolicy(context, str, orderPolicyType);
        } else if (z) {
            orderPolicy.setResCode("000000");
            orderPolicy.setOrderType(OrderPolicy.OrderType.sms);
            return orderPolicy;
        } else {
            orderPolicy.setResCode("000000");
            orderPolicy.setOrderType(OrderPolicy.OrderType.verifyCode);
            return orderPolicy;
        }
    }

    private static OrderPolicy requsetOrderPolicy(Context context, String str, OrderPolicy.OrderPolicyType orderPolicyType) throws IOException, XmlPullParserException {
        InputStream inputStream;
        String buildRequsetXml = buildRequsetXml("<musicId>" + str + "</musicId>");
        if (OrderPolicy.OrderPolicyType.vibrateRing == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/ring/policy", buildGetVibrateRingPolicyRequsetXml(str));
        } else if (OrderPolicy.OrderPolicyType.fullSong == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/song/policy", buildGetSongPolicyRequsetXml(str));
        } else if (OrderPolicy.OrderPolicyType.ringback == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/policy", buildRequsetXml);
        } else if (OrderPolicy.OrderPolicyType.openRingbackMonth == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/mon/bizInfo", buildRequsetXml("<serviceId>" + str + "</serviceId><type>8</type>"));
        } else if (OrderPolicy.OrderPolicyType.openRingback == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/mon/bizInfo", buildRequsetXml("<type>4</type>"));
        } else if (OrderPolicy.OrderPolicyType.checkRingbackOpen == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/open/check", buildRequsetXml);
        } else if (OrderPolicy.OrderPolicyType.openOwnRingback == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/mon/bizInfo", buildRequsetXml("<type>7</type>"));
        } else if (OrderPolicy.OrderPolicyType.openMember == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/mon/bizInfo", buildRequsetXml("<type>3</type>"));
        } else if (OrderPolicy.OrderPolicyType.openBjhy == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/mon/bizInfo", buildRequsetXml("<type>6</type>"));
        } else if (OrderPolicy.OrderPolicyType.openSongMonth == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/song/month/query", buildRequsetXml);
        } else if (OrderPolicy.OrderPolicyType.openCPMonth == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/mon/bizInfo", buildRequsetXml("<serviceId>" + str + "</serviceId><type>5</type>"));
        } else if (OrderPolicy.OrderPolicyType.cpFullSong == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/song/policy", buildGetSongPolicyRequsetXml(str));
        } else if (OrderPolicy.OrderPolicyType.cpVibrateRing == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/ring/policy", buildGetVibrateRingPolicyRequsetXml(str));
        } else if (OrderPolicy.OrderPolicyType.mvDownLoad == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/mv/policy", buildRequsetXml("<mvId>" + str + "</mvId><resource>050014</resource>" + "<resource>050013</resource>"));
        } else if (OrderPolicy.OrderPolicyType.digitalAlbum == orderPolicyType) {
            inputStream = HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/album/orderCount", buildGetDigitalAlbumInfoRequsetXml(str));
        } else {
            inputStream = null;
        }
        if (inputStream == null) {
            return null;
        }
        OrderPolicy orderPolicy = getOrderPolicy(inputStream);
        orderPolicy.setOrderType(OrderPolicy.OrderType.net);
        return orderPolicy;
    }

    static String buildGetDigitalAlbumInfoRequsetXml(String str) {
        return buildRequsetXml("<serviceId>" + str + "</serviceId><queryType>2</queryType>");
    }

    static String buildGetVibrateRingPolicyRequsetXml(String str) {
        return buildRequsetXml("<musicId>" + str + "</musicId><resource>000018</resource>" + "<resource>999992</resource>");
    }

    static String buildGetSongPolicyRequsetXml(String str) {
        return buildRequsetXml("<musicId>" + str + "</musicId><resource>000009</resource>" + "<resource>020007</resource><resource>020022</resource>");
    }

    static MemberOpenPolicy queryMemberOpenPolicyByNet(Context context) throws IOException, XmlPullParserException {
        return queryMemberOpenPolicy(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/user/member/query", ""));
    }

    private static MemberOpenPolicy queryMemberOpenPolicy(InputStream inputStream) throws IOException, XmlPullParserException {
        ClubUserInfo clubUserInfo;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        MemberOpenPolicy memberOpenPolicy = new MemberOpenPolicy();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            ClubUserInfo clubUserInfo2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        clubUserInfo = clubUserInfo2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        clubUserInfo2 = clubUserInfo;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            memberOpenPolicy.setResCode(newPullParser.nextText());
                            clubUserInfo = clubUserInfo2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            memberOpenPolicy.setResMsg(newPullParser.nextText());
                            clubUserInfo = clubUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("mobile")) {
                            memberOpenPolicy.setMobile(newPullParser.nextText());
                            clubUserInfo = clubUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("ClubUserInfo")) {
                            clubUserInfo = new ClubUserInfo();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("memLevel")) {
                            clubUserInfo2.setMemLevel(newPullParser.nextText());
                            clubUserInfo = clubUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("memDesc")) {
                            clubUserInfo2.setMemDesc(newPullParser.nextText());
                            clubUserInfo = clubUserInfo2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        clubUserInfo2 = clubUserInfo;
                        break;
                    case 3:
                        if (name.equalsIgnoreCase("ClubUserInfo")) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                memberOpenPolicy.setClubUserInfos(arrayList2);
                            }
                            arrayList2.add(clubUserInfo2);
                            break;
                        }
                        break;
                }
                clubUserInfo = clubUserInfo2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                clubUserInfo2 = clubUserInfo;
            }
            return memberOpenPolicy;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    static SongOpenPolicy querySongOpenPolicyByNet(Context context) throws IOException, XmlPullParserException {
        return querySongOpenPolicy(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/song/month/query", ""));
    }

    private static SongOpenPolicy querySongOpenPolicy(InputStream inputStream) throws IOException, XmlPullParserException {
        SongMonthInfo songMonthInfo;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        SongOpenPolicy songOpenPolicy = new SongOpenPolicy();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            SongMonthInfo songMonthInfo2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        songMonthInfo = songMonthInfo2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        songMonthInfo2 = songMonthInfo;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            songOpenPolicy.setResCode(newPullParser.nextText());
                            songMonthInfo = songMonthInfo2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            songOpenPolicy.setResMsg(newPullParser.nextText());
                            songMonthInfo = songMonthInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("mobile")) {
                            songOpenPolicy.setMobile(newPullParser.nextText());
                            songMonthInfo = songMonthInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("SongMonthInfo")) {
                            songMonthInfo = new SongMonthInfo();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("type")) {
                            songMonthInfo2.setType(newPullParser.nextText());
                            songMonthInfo = songMonthInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("typeDesc")) {
                            songMonthInfo2.setTypeDesc(newPullParser.nextText());
                            songMonthInfo = songMonthInfo2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        songMonthInfo2 = songMonthInfo;
                        break;
                    case 3:
                        if (name.equalsIgnoreCase("SongMonthInfo")) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                songOpenPolicy.setSongMonthInfos(arrayList2);
                            }
                            arrayList2.add(songMonthInfo2);
                            break;
                        }
                        break;
                }
                songMonthInfo = songMonthInfo2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                songMonthInfo2 = songMonthInfo;
            }
            return songOpenPolicy;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    static void getFullSongDownloadUrl(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, CMMusicCallback<OrderResult> cMMusicCallback) {
        getDownloadSongUrl(context, "http://218.200.227.123:95/sdkServer/1.0/song/downlink", getFullSong12Id(str), str2, str3, str4, str5, str6, str7, "2".equals(str4) ? "PL949" : "PL207", cMMusicCallback);
    }

    static void getVibrateRingDownloadUrl(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, CMMusicCallback<OrderResult> cMMusicCallback) {
        getDownloadSongUrl(context, "http://218.200.227.123:95/sdkServer/1.0/ring/downlink", getVibrateRing12Id(str), str2, str3, str4, str5, str6, str7, "PL102", cMMusicCallback);
    }

    static void giveSong(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, CMMusicCallback<OrderResult> cMMusicCallback) {
        String buildOrderParam = MiguSdkUtil.buildOrderParam(context, str, buildRequsetXml("<receivemdn>" + str2 + "</receivemdn><musicId>" + str3 + "</musicId><bizCode>" + str4 + "</bizCode><biztype>" + str5 + "</biztype>"));
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setContentId(str3);
        commonPayInfo.setPrice(str6);
        commonPayInfo.setProductId(str4);
        MiguSdkUtil.pay(context, commonPayInfo, str7, MiguSdkUtil.buildCpparam(context, str8, str9), buildOrderParam, cMMusicCallback);
    }

    static void getDownloadSongUrl(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, CMMusicCallback<OrderResult> cMMusicCallback) {
        String buildOrderParam = MiguSdkUtil.buildOrderParam(context, str, buildRequsetXml("<musicId>" + str2 + "</musicId><bizCode>" + str3 + "</bizCode><biztype>" + str4 + "</biztype><codeRate>" + str5 + "</codeRate>"));
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str6);
        commonPayInfo.setContentId(str2);
        commonPayInfo.setProductId(str3);
        MiguSdkUtil.pay(context, commonPayInfo, str7, MiguSdkUtil.buildCpparam(context, str8, str9), buildOrderParam, cMMusicCallback);
    }

    static String getDigitalAlbum12Id(String str) {
        String decrypt32MusicId = decrypt32MusicId(str);
        if (11 == decrypt32MusicId.length()) {
            return String.valueOf(decrypt32MusicId) + "5";
        }
        return decrypt32MusicId;
    }

    static void orderDigitalAlbum(Context context, String str, String str2, String str3, String str4, String str5, CMMusicCallback<OrderResult> cMMusicCallback) {
        String buildOrderParam = MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/album/order", buildRequsetXml("<serviceId>" + str + "</serviceId>"));
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str3);
        commonPayInfo.setContentId(getDigitalAlbum12Id(str));
        commonPayInfo.setProductId(str2);
        MiguSdkUtil.pay(context, commonPayInfo, str4, MiguSdkUtil.buildCpparam(context, str5, "PL922"), buildOrderParam, cMMusicCallback);
    }

    static void giveDigitalAlbum(Context context, String str, String str2, String str3, String str4, String str5, String str6, CMMusicCallback<OrderResult> cMMusicCallback) {
        String buildOrderParam = MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/album/present", buildRequsetXml("<serviceId>" + str + "</serviceId><receiveMdn>" + str2 + "</receiveMdn>"));
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str4);
        commonPayInfo.setContentId(getDigitalAlbum12Id(str));
        commonPayInfo.setProductId(str3);
        MiguSdkUtil.pay(context, commonPayInfo, str5, MiguSdkUtil.buildCpparam(context, str6, "PL923"), buildOrderParam, cMMusicCallback);
    }

    static Result getValidateCode(Context context, String str) throws IOException, XmlPullParserException {
        Result result = getResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/getValidateCode", buildRequsetXml("<MSISDN>" + str + "</MSISDN><validType>1</validType>")));
        Log.d("getValidateCode", result.toString());
        return result;
    }

    static CrbtOpenCheckRsp crbtOpenCheck(Context context, String str) throws IOException, XmlPullParserException {
        CrbtOpenCheckRsp crbtOpenCheckResult = getCrbtOpenCheckResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/open/check", buildRequsetXml("<MSISDN>" + str + "</MSISDN>")));
        Log.d("crbtOpenCheck", crbtOpenCheckResult.toString());
        return crbtOpenCheckResult;
    }

    static Result getIsOwnRingOrderMonthUserRsp(Context context) throws IOException, XmlPullParserException {
        Result result = getResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/diycrbt/queryMonth", ""));
        Log.i("EnablerInterface_getIsOwnRingOrderMonthUserData", result.toString());
        return result;
    }

    static void orderOwnRingback(Context context, String str, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.order(context, MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/diycrbt/orderDiycrbt", buildRequsetXml("<crbtId>" + str + "</crbtId>")), cMMusicCallback);
    }

    private static CrbtOpenCheckRsp getCrbtOpenCheckResult(InputStream inputStream) throws XmlPullParserException, IOException {
        if (inputStream == null) {
            return null;
        }
        CrbtOpenCheckRsp crbtOpenCheckRsp = new CrbtOpenCheckRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("price")) {
                                    if (!name.equalsIgnoreCase("description")) {
                                        if (!name.equalsIgnoreCase("mobile")) {
                                            break;
                                        } else {
                                            crbtOpenCheckRsp.setMobile(newPullParser.nextText());
                                            break;
                                        }
                                    } else {
                                        crbtOpenCheckRsp.setDescription(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    crbtOpenCheckRsp.setPrice(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                crbtOpenCheckRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            crbtOpenCheckRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return crbtOpenCheckRsp;
            }
            try {
                return crbtOpenCheckRsp;
            } catch (IOException e) {
                return crbtOpenCheckRsp;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    static SmsLoginAuthResult smsLoginAuth(Context context, String str, String str2) throws IOException, XmlPullParserException {
        SmsLoginAuthResult smsLoginAuthResult = getSmsLoginAuthResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/smsLoginAuth", buildRequsetXml("<MSISDN>" + str + "</MSISDN><smsCode>" + str2 + "</smsCode>")));
        Log.d("smsLoginAuth", smsLoginAuthResult.toString());
        return smsLoginAuthResult;
    }

    private static SmsLoginAuthResult getSmsLoginAuthResult(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        SmsLoginAuthResult smsLoginAuthResult = new SmsLoginAuthResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase(AssistPushConsts.MSG_TYPE_TOKEN)) {
                                    break;
                                } else {
                                    smsLoginAuthResult.setToken(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                smsLoginAuthResult.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            smsLoginAuthResult.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return smsLoginAuthResult;
            }
            try {
                return smsLoginAuthResult;
            } catch (IOException e) {
                return smsLoginAuthResult;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    static void openRingback(Context context, String str, String str2, CMMusicCallback<OrderResult> cMMusicCallback) {
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str2);
        commonPayInfo.setProductId(str);
        MiguSdkUtil.payMon(context, commonPayInfo, cMMusicCallback);
    }

    static String getRingback12Id(String str) {
        String decrypt32MusicId = decrypt32MusicId(str);
        if (11 == decrypt32MusicId.length()) {
            return String.valueOf(decrypt32MusicId) + "0";
        }
        return decrypt32MusicId;
    }

    static void giveRingback(Context context, String str, String str2, String str3, String str4, String str5, String str6, String str7, CMMusicCallback<OrderResult> cMMusicCallback) {
        String buildOrderParam = MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/present", buildRequsetXml("<receivemdn>" + str + "</receivemdn><musicId>" + str2 + "</musicId><bizCode>" + str3 + "</bizCode><bizType>" + str4 + "</bizType>"));
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str5);
        commonPayInfo.setContentId(getRingback12Id(str2));
        commonPayInfo.setProductId(str3);
        MiguSdkUtil.pay(context, commonPayInfo, str6, MiguSdkUtil.buildCpparam(context, str7, "PL008"), buildOrderParam, cMMusicCallback);
    }

    static void buyRingback(Context context, String str, String str2, String str3, String str4, String str5, String str6, CMMusicCallback<OrderResult> cMMusicCallback) {
        String buildOrderParam = MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/order", buildRequsetXml("<musicId>" + str + "</musicId><bizCode>" + str2 + "</bizCode><bizType>" + str3 + "</bizType>"));
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str4);
        commonPayInfo.setContentId(getRingback12Id(str));
        commonPayInfo.setProductId(str2);
        MiguSdkUtil.pay(context, commonPayInfo, str5, MiguSdkUtil.buildCpparam(context, str6, "PL004"), buildOrderParam, cMMusicCallback);
    }

    static void buyRingbackBox(Context context, String str, String str2, CMMusicCallback<OrderResult> cMMusicCallback) {
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str2);
        commonPayInfo.setProductId(str);
        MiguSdkUtil.payMon(context, commonPayInfo, cMMusicCallback);
    }

    static void openCPMonth(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str2);
        commonPayInfo.setProductId(str);
        MiguSdkUtil.payMon(context, commonPayInfo, str3, cMMusicCallback);
    }

    static void openRingbackMonth(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str2);
        commonPayInfo.setProductId(str);
        MiguSdkUtil.payMon(context, commonPayInfo, str3, cMMusicCallback);
    }

    static void openBjhy(Context context, String str, String str2, CMMusicCallback<OrderResult> cMMusicCallback) {
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str2);
        commonPayInfo.setProductId(str);
        MiguSdkUtil.payMon(context, commonPayInfo, cMMusicCallback);
    }

    static void orderOwnRingMonth(Context context, String str, String str2, CMMusicCallback<OrderResult> cMMusicCallback) {
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str2);
        commonPayInfo.setProductId(str);
        MiguSdkUtil.payMon(context, commonPayInfo, cMMusicCallback);
    }

    private static String getServiceId(String str) {
        if ("3".equals(str)) {
            return "600956020000006033";
        }
        if ("5".equals(str)) {
            return "600956020000006035";
        }
        return null;
    }

    static void getCPVibrateRingTimeDownloadUrl(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        getDownloadCPSongUrl(context, "http://218.200.227.123:95/sdkServer/1.0/cp/timerdownlink", getVibrateRing12Id(str), str2, str3, "PL955", cMMusicCallback);
    }

    static String getVibrateRing12Id(String str) {
        String decrypt32MusicId = decrypt32MusicId(str);
        if (11 == decrypt32MusicId.length()) {
            return String.valueOf(decrypt32MusicId) + "1";
        }
        return decrypt32MusicId;
    }

    private static String decrypt32MusicId(String str) {
        if (32 != str.length()) {
            return str;
        }
        try {
            return new DesUtils("AwKRdpqs").decrypt(str);
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    static void getCPFullSongTimeDownloadUrl(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        getDownloadCPSongUrl(context, "http://218.200.227.123:95/sdkServer/1.0/cp/timesdownlink", getFullSong12Id(str), str2, str3, "PL956", cMMusicCallback);
    }

    static String getFullSong12Id(String str) {
        String decrypt32MusicId = decrypt32MusicId(str);
        if (11 == decrypt32MusicId.length()) {
            return String.valueOf(decrypt32MusicId) + "2";
        }
        return decrypt32MusicId;
    }

    static void getDownloadCPSongUrl(Context context, String str, String str2, String str3, String str4, String str5, CMMusicCallback<OrderResult> cMMusicCallback) {
        String buildOrderParam = MiguSdkUtil.buildOrderParam(context, str, buildRequsetXml("<musicId>" + str2 + "</musicId><codeRate>" + str3 + "</codeRate>"));
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(BasicPushStatus.SUCCESS_CODE);
        commonPayInfo.setContentId(str2);
        commonPayInfo.setProductId("600902020000004000");
        MiguSdkUtil.pay(context, commonPayInfo, str4, MiguSdkUtil.buildCpparam(context, "E", str5), buildOrderParam, cMMusicCallback);
    }

    static void exclusiveTimesOrder(Context context, String str, String str2, String str3, String str4, String str5, CMMusicCallback<OrderResult> cMMusicCallback) {
        String buildOrderParam = MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/exclusive/open", buildRequsetXml("<serviceId>" + str + "</serviceId>"), str5);
        CommonPayInfo commonPayInfo = new CommonPayInfo();
        commonPayInfo.setPrice(str3);
        commonPayInfo.setContentId(str);
        commonPayInfo.setProductId(str2);
        MiguSdkUtil.pay(context, commonPayInfo, str4, MiguSdkUtil.buildCpparam(context, "G", "PL998", str5), buildOrderParam, cMMusicCallback);
    }

    static void buyRingBackByOpenRingBack(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/simpOrder", buildRequsetXml("<musicId>" + str + "</musicId><bizCode>" + str2 + "</bizCode><bizType>" + str3 + "</bizType>"));
    }

    static void buyRingBackByOpenMember(Context context, String str, String str2, String str3, CMMusicCallback<OrderResult> cMMusicCallback) {
        MiguSdkUtil.buildOrderParam(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/simpMemberOrder", buildRequsetXml("<musicId>" + str + "</musicId><bizCode>" + str2 + "</bizCode><bizType>" + str3 + "</bizType>"));
    }

    static Result deletePersonRingByNet(Context context, String str) throws IOException, XmlPullParserException {
        Result result = getResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/box/delete", buildRequsetXml("<crbtId>" + str + "</crbtId>")));
        Log.d("deletePersonRing", result.toString());
        return result;
    }

    static Result queryCrbtMonth(Context context, String str) {
        try {
            return getResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/crbt/querymonth", buildRequsetXml("<serviceId>" + str + "</serviceId>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static GetUserInfoRsp getUserInfo(Context context) throws IOException, XmlPullParserException {
        GetUserInfoRsp userInfo = getUserInfo(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/user/query", ""));
        Log.d("getUserInfo", userInfo.toString());
        return userInfo;
    }

    static OwnRingBackgroundMusicRsp getOwnRingBackgroundMusicRsp(Context context, String str) throws IOException, XmlPullParserException {
        OwnRingBackgroundMusicRsp ownRingBackgroundMusicRsp = getOwnRingBackgroundMusicRsp(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/diycrbt/bgMusic", buildRequsetXml("<bgId>" + str + "</bgId>")));
        Log.i("EnablerInterface_getOwnRingBackgroundMusicRsp", ownRingBackgroundMusicRsp.toString());
        return ownRingBackgroundMusicRsp;
    }

    static Result getReturnOwnRingMonthRsp(Context context) throws IOException, XmlPullParserException {
        Result result = getResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/diycrbt/cancelMonth", ""));
        Log.i("EnablerInterface_cancleOwnRingMonthresult", result.toString());
        return result;
    }

    static OwnRingRsp getTextOwnRingRsp(Context context, String str, String str2, String str3, String str4) throws IOException, XmlPullParserException {
        OwnRingRsp ownRingRsp = getOwnRingRsp(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/diycrbt/createRing", buildRequsetXml("<content>" + URLEncoder.encode(str, "UTF-8") + "</content><bgMusic>" + str2 + "</bgMusic><sex>" + str3 + "</sex><ringName>" + URLEncoder.encode(str4, StringUtil.Encoding) + "</ringName>")));
        Log.i("getOwnRingRsp", ownRingRsp.toString());
        return ownRingRsp;
    }

    static ServiceExInfo getServiceEx(Context context, String str) throws IOException, XmlPullParserException {
        return getServiceExRsp(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/cp/serviceEx", buildRequsetXml("<contentId>" + str + "</contentId>")));
    }

    private static ServiceExInfo getServiceExRsp(InputStream inputStream) throws XmlPullParserException, IOException {
        if (inputStream == null) {
            return null;
        }
        ServiceExInfo serviceExInfo = new ServiceExInfo();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            Log.d("resultStream", inputStream.toString());
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("contentId")) {
                            if (!name.equalsIgnoreCase("serviceId")) {
                                if (!name.equalsIgnoreCase("cpId")) {
                                    if (!name.equalsIgnoreCase("cpName")) {
                                        if (!name.equalsIgnoreCase("serviceName")) {
                                            if (!name.equalsIgnoreCase("copyright_id")) {
                                                if (!name.equalsIgnoreCase("prodType")) {
                                                    if (!name.equalsIgnoreCase("channelSubName")) {
                                                        if (!name.equalsIgnoreCase("appName")) {
                                                            if (!name.equalsIgnoreCase("phoneNum")) {
                                                                if (!name.equalsIgnoreCase("copyrightName")) {
                                                                    if (!name.equalsIgnoreCase("price")) {
                                                                        if (!name.equalsIgnoreCase("noticeBefore")) {
                                                                            if (!name.equalsIgnoreCase("noticeAfter")) {
                                                                                if (!name.equalsIgnoreCase("resCode")) {
                                                                                    if (!name.equalsIgnoreCase("resMsg")) {
                                                                                        if (!name.equalsIgnoreCase("monLevel")) {
                                                                                            break;
                                                                                        } else {
                                                                                            serviceExInfo.setMonLevel(newPullParser.nextText());
                                                                                            break;
                                                                                        }
                                                                                    } else {
                                                                                        serviceExInfo.setResMsg(newPullParser.nextText());
                                                                                        break;
                                                                                    }
                                                                                } else {
                                                                                    serviceExInfo.setResCode(newPullParser.nextText());
                                                                                    break;
                                                                                }
                                                                            } else {
                                                                                serviceExInfo.setNoticeAfter(newPullParser.nextText());
                                                                                break;
                                                                            }
                                                                        } else {
                                                                            serviceExInfo.setNoticeBefore(newPullParser.nextText());
                                                                            break;
                                                                        }
                                                                    } else {
                                                                        serviceExInfo.setPrice(newPullParser.nextText());
                                                                        break;
                                                                    }
                                                                } else {
                                                                    serviceExInfo.setCopyrightName(newPullParser.nextText());
                                                                    break;
                                                                }
                                                            } else {
                                                                serviceExInfo.setPhoneNum(newPullParser.nextText());
                                                                break;
                                                            }
                                                        } else {
                                                            serviceExInfo.setAppName(newPullParser.nextText());
                                                            break;
                                                        }
                                                    } else {
                                                        serviceExInfo.setChannelSubName(newPullParser.nextText());
                                                        break;
                                                    }
                                                } else {
                                                    serviceExInfo.setProdType(newPullParser.nextText());
                                                    break;
                                                }
                                            } else {
                                                serviceExInfo.setCopyright_id(newPullParser.nextText());
                                                break;
                                            }
                                        } else {
                                            serviceExInfo.setServiceName(newPullParser.nextText());
                                            break;
                                        }
                                    } else {
                                        serviceExInfo.setCpName(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    serviceExInfo.setCpId(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                serviceExInfo.setServiceId(newPullParser.nextText());
                                break;
                            }
                        } else {
                            serviceExInfo.setContentId(newPullParser.nextText());
                            break;
                        }
                }
            }
            Log.d("serviceExInfo", serviceExInfo.toString());
            return serviceExInfo;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    static OwnRingRsp getMp3OwnRingRsp(Context context, File file, String str, int i) throws IOException, XmlPullParserException {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("audioType", i).put("ringName", str);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        OwnRingRsp ownRingRsp = getOwnRingRsp(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/diycrbt/upload?" + MiguSdkUtil.encodeBase64(jSONObject.toString().getBytes()), file));
        Log.d("getOwnRingRsp", ownRingRsp.toString());
        return ownRingRsp;
    }

    static MVMonthPolicy getMvMonthPolicy(Context context, String[] strArr, boolean z) throws IOException, XmlPullParserException {
        MVMonthPolicy mVMonthPolicy = new MVMonthPolicy();
        if (!NetMode.isConnected(context)) {
            Log.d("getOrderPolicy", "not Connected");
            mVMonthPolicy.setResCode("000000");
            mVMonthPolicy.setOrderType(MVMonthPolicy.OrderType.sms);
            return mVMonthPolicy;
        } else if (InitCmmInterface.isGetPhoneNumber(context)) {
            Log.d("initCheck", "Connected by net");
            if (strArr == null) {
                return null;
            }
            StringBuilder sb = new StringBuilder();
            int length = strArr.length;
            for (int i = 0; i < length; i++) {
                sb.append("<serviceId>" + strArr[i] + "</serviceId>");
            }
            MVMonthPolicy mvMonthPolicy = getMvMonthPolicy(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/mv/query", buildRequsetXml(sb.toString())));
            Log.d("getMvMonthPolicy", mvMonthPolicy.toString());
            return mvMonthPolicy;
        } else if (z) {
            mVMonthPolicy.setResCode("000000");
            mVMonthPolicy.setOrderType(MVMonthPolicy.OrderType.sms);
            return mVMonthPolicy;
        } else {
            mVMonthPolicy.setResCode("000000");
            mVMonthPolicy.setOrderType(MVMonthPolicy.OrderType.verifyCode);
            return mVMonthPolicy;
        }
    }

    private static GetUserInfoRsp getUserInfo(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        GetUserInfoRsp getUserInfoRsp = new GetUserInfoRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            UserInfo userInfo = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("UserInfo")) {
                                    if (!name.equalsIgnoreCase("uid")) {
                                        if (!name.equalsIgnoreCase("username")) {
                                            if (!name.equalsIgnoreCase("memLevel")) {
                                                if (!name.equalsIgnoreCase("email")) {
                                                    if (!name.equalsIgnoreCase("imageUrl")) {
                                                        if (!name.equalsIgnoreCase("imageUrl2")) {
                                                            if (!name.equalsIgnoreCase("imageUrl3")) {
                                                                if (!name.equalsIgnoreCase("spaceUrl")) {
                                                                    if (!name.equalsIgnoreCase("mbUrl")) {
                                                                        break;
                                                                    } else {
                                                                        userInfo.setMbUrl(newPullParser.nextText());
                                                                        break;
                                                                    }
                                                                } else {
                                                                    userInfo.setSpaceUrl(newPullParser.nextText());
                                                                    break;
                                                                }
                                                            } else {
                                                                userInfo.setImageUrl3(newPullParser.nextText());
                                                                break;
                                                            }
                                                        } else {
                                                            userInfo.setImageUrl2(newPullParser.nextText());
                                                            break;
                                                        }
                                                    } else {
                                                        userInfo.setImageUrl(newPullParser.nextText());
                                                        break;
                                                    }
                                                } else {
                                                    userInfo.setEmail(newPullParser.nextText());
                                                    break;
                                                }
                                            } else {
                                                userInfo.setMemLevel(newPullParser.nextText());
                                                break;
                                            }
                                        } else {
                                            userInfo.setUsername(newPullParser.nextText());
                                            break;
                                        }
                                    } else {
                                        userInfo.setUid(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    userInfo = new UserInfo();
                                    break;
                                }
                            } else {
                                getUserInfoRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            getUserInfoRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                    case 3:
                        if (!name.equalsIgnoreCase("UserInfo")) {
                            break;
                        } else {
                            getUserInfoRsp.setUserInfo(userInfo);
                            break;
                        }
                }
            }
            return getUserInfoRsp;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    private static OwnRingBackgroundMusicRsp getOwnRingBackgroundMusicRsp(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        OwnRingBackgroundMusicRsp ownRingBackgroundMusicRsp = new OwnRingBackgroundMusicRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("musicUrl")) {
                                    break;
                                } else {
                                    ownRingBackgroundMusicRsp.setmusicUrl(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                ownRingBackgroundMusicRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            ownRingBackgroundMusicRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return ownRingBackgroundMusicRsp;
            }
            try {
                return ownRingBackgroundMusicRsp;
            } catch (IOException e) {
                return ownRingBackgroundMusicRsp;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    static OwnRingRsp getOwnRingRsp(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        OwnRingRsp ownRingRsp = new OwnRingRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("crbtId")) {
                                    if (!name.equalsIgnoreCase("listenUrl")) {
                                        break;
                                    } else {
                                        ownRingRsp.setListenUrl(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    ownRingRsp.setCrbtId(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                ownRingRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            ownRingRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return ownRingRsp;
            }
            try {
                return ownRingRsp;
            } catch (IOException e) {
                return ownRingRsp;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    static String buildRequsetXml(String str) {
        return "<?xml version='1.0' encoding='UTF-8'?><request>" + str + "</request>";
    }

    static MusicInfoResult getMusicInfo(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        MusicInfoResult musicInfoResult = new MusicInfoResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            MusicInfo musicInfo = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("MusicInfo")) {
                                    if (!name.equalsIgnoreCase("musicId")) {
                                        if (!name.equalsIgnoreCase(WBPageConstants.ParamKey.COUNT)) {
                                            if (!name.equalsIgnoreCase("crbtValidity")) {
                                                if (!name.equalsIgnoreCase("price")) {
                                                    if (!name.equalsIgnoreCase("songName")) {
                                                        if (!name.equalsIgnoreCase("singerName")) {
                                                            if (!name.equalsIgnoreCase("singerId")) {
                                                                if (!name.equalsIgnoreCase("ringValidity")) {
                                                                    if (!name.equalsIgnoreCase("songValidity")) {
                                                                        if (!name.equalsIgnoreCase("albumPicDir")) {
                                                                            if (!name.equalsIgnoreCase("singerPicDir")) {
                                                                                if (!name.equalsIgnoreCase("crbtListenDir")) {
                                                                                    if (!name.equalsIgnoreCase("ringListenDir")) {
                                                                                        if (!name.equalsIgnoreCase("songListenDir")) {
                                                                                            if (!name.equalsIgnoreCase("lrcDir")) {
                                                                                                if (!name.equalsIgnoreCase("hasDolby")) {
                                                                                                    break;
                                                                                                } else {
                                                                                                    musicInfo.setHasDolby(newPullParser.nextText());
                                                                                                    break;
                                                                                                }
                                                                                            } else {
                                                                                                musicInfo.setLrcDir(newPullParser.nextText());
                                                                                                break;
                                                                                            }
                                                                                        } else {
                                                                                            musicInfo.setSongListenDir(newPullParser.nextText());
                                                                                            break;
                                                                                        }
                                                                                    } else {
                                                                                        musicInfo.setRingListenDir(newPullParser.nextText());
                                                                                        break;
                                                                                    }
                                                                                } else {
                                                                                    musicInfo.setCrbtListenDir(newPullParser.nextText());
                                                                                    break;
                                                                                }
                                                                            } else {
                                                                                musicInfo.setSingerPicDir(newPullParser.nextText());
                                                                                break;
                                                                            }
                                                                        } else {
                                                                            musicInfo.setAlbumPicDir(newPullParser.nextText());
                                                                            break;
                                                                        }
                                                                    } else {
                                                                        musicInfo.setSongValidity(newPullParser.nextText());
                                                                        break;
                                                                    }
                                                                } else {
                                                                    musicInfo.setRingValidity(newPullParser.nextText());
                                                                    break;
                                                                }
                                                            } else {
                                                                musicInfo.setSingerId(newPullParser.nextText());
                                                                break;
                                                            }
                                                        } else {
                                                            musicInfo.setSingerName(newPullParser.nextText());
                                                            break;
                                                        }
                                                    } else {
                                                        musicInfo.setSongName(newPullParser.nextText());
                                                        break;
                                                    }
                                                } else {
                                                    musicInfo.setPrice(newPullParser.nextText());
                                                    break;
                                                }
                                            } else {
                                                musicInfo.setCrbtValidity(newPullParser.nextText());
                                                break;
                                            }
                                        } else {
                                            musicInfo.setCount(newPullParser.nextText());
                                            break;
                                        }
                                    } else {
                                        musicInfo.setMusicId(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    musicInfo = new MusicInfo();
                                    break;
                                }
                            } else {
                                musicInfoResult.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            musicInfoResult.setResCode(newPullParser.nextText());
                            break;
                        }
                    case 3:
                        if (!name.equalsIgnoreCase("MusicInfo")) {
                            break;
                        } else {
                            musicInfoResult.setMusicInfo(musicInfo);
                            break;
                        }
                }
            }
            return musicInfoResult;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    static void getOrderResult(InputStream inputStream, OrderResult orderResult) throws IOException, XmlPullParserException {
        if (inputStream != null) {
            try {
                XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
                newPullParser.setInput(inputStream, "UTF-8");
                for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                    String name = newPullParser.getName();
                    switch (eventType) {
                        case 2:
                            if (!name.equalsIgnoreCase("resCode")) {
                                if (!name.equalsIgnoreCase("resMsg")) {
                                    if (!name.equalsIgnoreCase("downUrl")) {
                                        break;
                                    } else {
                                        orderResult.setDownUrl(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    orderResult.setResMsg(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                orderResult.setResCode(newPullParser.nextText());
                                break;
                            }
                    }
                }
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                    }
                }
            }
        }
    }

    static DownloadRsp getDownloadRsp(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        DownloadRsp downloadRsp = new DownloadRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("downUrl")) {
                                    if (!name.equalsIgnoreCase("downloadKey")) {
                                        break;
                                    } else {
                                        downloadRsp.setDownloadKey(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    downloadRsp.setDownUrl(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                downloadRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            downloadRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return downloadRsp;
            }
            try {
                return downloadRsp;
            } catch (IOException e) {
                return downloadRsp;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    static Result getResult(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        Result result = new Result();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                break;
                            } else {
                                result.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            result.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return result;
            }
            try {
                return result;
            } catch (IOException e) {
                return result;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    static RegistRsp getRegistResult(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        RegistRsp registRsp = new RegistRsp();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("isExistent")) {
                                    if (!name.equalsIgnoreCase("isVerified")) {
                                        if (!name.equalsIgnoreCase("registerMode")) {
                                            if (!name.equalsIgnoreCase("mobile")) {
                                                break;
                                            } else {
                                                registRsp.setMobile(newPullParser.nextText());
                                                break;
                                            }
                                        } else {
                                            registRsp.setRegisterMode(newPullParser.nextText());
                                            break;
                                        }
                                    } else {
                                        registRsp.setIsVerified(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    registRsp.setIsExistent(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                registRsp.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            registRsp.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return registRsp;
            }
            try {
                return registRsp;
            } catch (IOException e) {
                return registRsp;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    static RegistResult getRegistResult1(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        RegistResult registResult = new RegistResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("identityID")) {
                                    break;
                                } else {
                                    registResult.setIdentityID(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                registResult.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            registResult.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return registResult;
            }
            try {
                return registResult;
            } catch (IOException e) {
                return registResult;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    static TransferResult getTransferResult(InputStream inputStream) throws IOException, XmlPullParserException {
        if (inputStream == null) {
            return null;
        }
        TransferResult transferResult = new TransferResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 2:
                        if (!name.equalsIgnoreCase("resCode")) {
                            if (!name.equalsIgnoreCase("resMsg")) {
                                if (!name.equalsIgnoreCase("balance")) {
                                    if (!name.equalsIgnoreCase("tradeNum")) {
                                        break;
                                    } else {
                                        transferResult.setTradeNum(newPullParser.nextText());
                                        break;
                                    }
                                } else {
                                    transferResult.setBalance(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                transferResult.setResMsg(newPullParser.nextText());
                                break;
                            }
                        } else {
                            transferResult.setResCode(newPullParser.nextText());
                            break;
                        }
                }
            }
            if (inputStream == null) {
                return transferResult;
            }
            try {
                return transferResult;
            } catch (IOException e) {
                return transferResult;
            }
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                }
            }
        }
    }

    private static OrderPolicy getOrderPolicy(InputStream inputStream) throws IOException, XmlPullParserException {
        MVInfo mVInfo;
        BizInfo bizInfo;
        ArrayList arrayList;
        ClubUserInfo clubUserInfo;
        MusicInfo musicInfo;
        ArrayList arrayList2;
        SongMonthInfo songMonthInfo;
        AlbumInfo albumInfo;
        ArrayList arrayList3;
        ArrayList arrayList4 = null;
        if (inputStream == null) {
            return null;
        }
        OrderPolicy orderPolicy = new OrderPolicy();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            SongMonthInfo songMonthInfo2 = null;
            ClubUserInfo clubUserInfo2 = null;
            MVInfo mVInfo2 = null;
            AlbumInfo albumInfo2 = null;
            MusicInfo musicInfo2 = null;
            BizInfo bizInfo2 = null;
            ArrayList arrayList5 = null;
            ArrayList arrayList6 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        mVInfo = mVInfo2;
                        bizInfo = bizInfo2;
                        arrayList = arrayList5;
                        clubUserInfo = clubUserInfo2;
                        musicInfo = musicInfo2;
                        arrayList2 = arrayList6;
                        songMonthInfo = songMonthInfo2;
                        albumInfo = albumInfo2;
                        arrayList3 = arrayList4;
                        continue;
                        arrayList4 = arrayList3;
                        albumInfo2 = albumInfo;
                        songMonthInfo2 = songMonthInfo;
                        arrayList6 = arrayList2;
                        musicInfo2 = musicInfo;
                        clubUserInfo2 = clubUserInfo;
                        arrayList5 = arrayList;
                        bizInfo2 = bizInfo;
                        mVInfo2 = mVInfo;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            orderPolicy.setResCode(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            orderPolicy.setResMsg(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("mobile")) {
                            orderPolicy.setMobile(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("monLevel")) {
                            orderPolicy.setMonLevel(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("BizInfo") || name.equalsIgnoreCase("BizInfoMon")) {
                            if (arrayList5 == null) {
                                arrayList5 = new ArrayList();
                                orderPolicy.setBizInfos(arrayList5);
                            }
                            BizInfo bizInfo3 = new BizInfo();
                            arrayList5.add(bizInfo3);
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo3;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("bizCode")) {
                            bizInfo2.setBizCode(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("hold2")) {
                            bizInfo2.setHold2(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("bizType")) {
                            bizInfo2.setBizType(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("originalPrice")) {
                            bizInfo2.setOriginalPrice(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("salePrice")) {
                            bizInfo2.setSalePrice(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("payType")) {
                            bizInfo2.setPayType(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("description") || name.equalsIgnoreCase(SocialConstants.PARAM_APP_DESC)) {
                            if (bizInfo2 != null) {
                                bizInfo2.setDescription(newPullParser.nextText());
                                mVInfo = mVInfo2;
                                bizInfo = bizInfo2;
                                arrayList = arrayList5;
                                clubUserInfo = clubUserInfo2;
                                musicInfo = musicInfo2;
                                arrayList2 = arrayList6;
                                songMonthInfo = songMonthInfo2;
                                albumInfo = albumInfo2;
                                arrayList3 = arrayList4;
                            } else {
                                orderPolicy.setOpenrbDescription(newPullParser.nextText());
                                mVInfo = mVInfo2;
                                bizInfo = bizInfo2;
                                arrayList = arrayList5;
                                clubUserInfo = clubUserInfo2;
                                musicInfo = musicInfo2;
                                arrayList2 = arrayList6;
                                songMonthInfo = songMonthInfo2;
                                albumInfo = albumInfo2;
                                arrayList3 = arrayList4;
                            }
                        } else if (name.equalsIgnoreCase("resource")) {
                            bizInfo2.setResource(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("MusicInfo")) {
                            MusicInfo musicInfo3 = new MusicInfo();
                            orderPolicy.setMusicInfo(musicInfo3);
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo3;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("AlbumInfo")) {
                            AlbumInfo albumInfo3 = new AlbumInfo();
                            orderPolicy.setAlbumInfo(albumInfo3);
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo3;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("songName")) {
                            musicInfo2.setSongName(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("singerName")) {
                            if (musicInfo2 != null) {
                                musicInfo2.setSingerName(newPullParser.nextText());
                                mVInfo = mVInfo2;
                                bizInfo = bizInfo2;
                                arrayList = arrayList5;
                                clubUserInfo = clubUserInfo2;
                                musicInfo = musicInfo2;
                                arrayList2 = arrayList6;
                                songMonthInfo = songMonthInfo2;
                                albumInfo = albumInfo2;
                                arrayList3 = arrayList4;
                            } else if (mVInfo2 != null) {
                                mVInfo2.setSingerName(newPullParser.nextText());
                                mVInfo = mVInfo2;
                                bizInfo = bizInfo2;
                                arrayList = arrayList5;
                                clubUserInfo = clubUserInfo2;
                                musicInfo = musicInfo2;
                                arrayList2 = arrayList6;
                                songMonthInfo = songMonthInfo2;
                                albumInfo = albumInfo2;
                                arrayList3 = arrayList4;
                            } else if (albumInfo2 != null) {
                                albumInfo2.setSingerName(newPullParser.nextText());
                                mVInfo = mVInfo2;
                                bizInfo = bizInfo2;
                                arrayList = arrayList5;
                                clubUserInfo = clubUserInfo2;
                                musicInfo = musicInfo2;
                                arrayList2 = arrayList6;
                                songMonthInfo = songMonthInfo2;
                                albumInfo = albumInfo2;
                                arrayList3 = arrayList4;
                            }
                        } else if (name.equalsIgnoreCase("price")) {
                            if (musicInfo2 != null) {
                                musicInfo2.setPrice(newPullParser.nextText());
                                mVInfo = mVInfo2;
                                bizInfo = bizInfo2;
                                arrayList = arrayList5;
                                clubUserInfo = clubUserInfo2;
                                musicInfo = musicInfo2;
                                arrayList2 = arrayList6;
                                songMonthInfo = songMonthInfo2;
                                albumInfo = albumInfo2;
                                arrayList3 = arrayList4;
                            } else if (mVInfo2 != null) {
                                mVInfo2.setPrice(newPullParser.nextText());
                                mVInfo = mVInfo2;
                                bizInfo = bizInfo2;
                                arrayList = arrayList5;
                                clubUserInfo = clubUserInfo2;
                                musicInfo = musicInfo2;
                                arrayList2 = arrayList6;
                                songMonthInfo = songMonthInfo2;
                                albumInfo = albumInfo2;
                                arrayList3 = arrayList4;
                            } else if (albumInfo2 != null) {
                                albumInfo2.setPrice(newPullParser.nextText());
                                mVInfo = mVInfo2;
                                bizInfo = bizInfo2;
                                arrayList = arrayList5;
                                clubUserInfo = clubUserInfo2;
                                musicInfo = musicInfo2;
                                arrayList2 = arrayList6;
                                songMonthInfo = songMonthInfo2;
                                albumInfo = albumInfo2;
                                arrayList3 = arrayList4;
                            } else {
                                orderPolicy.setOpenrbPrice(newPullParser.nextText());
                                mVInfo = mVInfo2;
                                bizInfo = bizInfo2;
                                arrayList = arrayList5;
                                clubUserInfo = clubUserInfo2;
                                musicInfo = musicInfo2;
                                arrayList2 = arrayList6;
                                songMonthInfo = songMonthInfo2;
                                albumInfo = albumInfo2;
                                arrayList3 = arrayList4;
                            }
                        } else if (name.equalsIgnoreCase("downUrl")) {
                            orderPolicy.setDownUrl(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("downUrlL")) {
                            orderPolicy.setDownUrlL(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("invalidDate")) {
                            orderPolicy.setInvalidDate(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("restTimes")) {
                            orderPolicy.setRestTimes(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("crbtValidity")) {
                            musicInfo2.setCrbtValidity(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("ClubUserInfo")) {
                            if (arrayList6 == null) {
                                arrayList6 = new ArrayList();
                                orderPolicy.setClubUserInfos(arrayList6);
                            }
                            ClubUserInfo clubUserInfo3 = new ClubUserInfo();
                            arrayList6.add(clubUserInfo3);
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo3;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("memLevel")) {
                            clubUserInfo2.setMemLevel(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("memDesc")) {
                            clubUserInfo2.setMemDesc(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("SongMonthInfo")) {
                            if (arrayList4 == null) {
                                arrayList4 = new ArrayList();
                                orderPolicy.setSongMonthInfos(arrayList4);
                            }
                            SongMonthInfo songMonthInfo3 = new SongMonthInfo();
                            arrayList4.add(songMonthInfo3);
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo3;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("type")) {
                            songMonthInfo2.setType(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("typeDesc")) {
                            songMonthInfo2.setTypeDesc(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("userMonType")) {
                            orderPolicy.setMonthType(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase(SelectCountryActivity.EXTRA_COUNTRY_NAME)) {
                            if (albumInfo2 != null) {
                                albumInfo2.setName(newPullParser.nextText());
                                mVInfo = mVInfo2;
                                bizInfo = bizInfo2;
                                arrayList = arrayList5;
                                clubUserInfo = clubUserInfo2;
                                musicInfo = musicInfo2;
                                arrayList2 = arrayList6;
                                songMonthInfo = songMonthInfo2;
                                albumInfo = albumInfo2;
                                arrayList3 = arrayList4;
                            } else {
                                orderPolicy.setCpName(newPullParser.nextText());
                                mVInfo = mVInfo2;
                                bizInfo = bizInfo2;
                                arrayList = arrayList5;
                                clubUserInfo = clubUserInfo2;
                                musicInfo = musicInfo2;
                                arrayList2 = arrayList6;
                                songMonthInfo = songMonthInfo2;
                                albumInfo = albumInfo2;
                                arrayList3 = arrayList4;
                            }
                        } else if (name.equalsIgnoreCase("mvInfo")) {
                            MVInfo mVInfo3 = new MVInfo();
                            orderPolicy.setMvInfo(mVInfo3);
                            mVInfo = mVInfo3;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("mvName")) {
                            mVInfo2.setMvName(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        } else if (name.equalsIgnoreCase("sum")) {
                            orderPolicy.setSum(newPullParser.nextText());
                            mVInfo = mVInfo2;
                            bizInfo = bizInfo2;
                            arrayList = arrayList5;
                            clubUserInfo = clubUserInfo2;
                            musicInfo = musicInfo2;
                            arrayList2 = arrayList6;
                            songMonthInfo = songMonthInfo2;
                            albumInfo = albumInfo2;
                            arrayList3 = arrayList4;
                        }
                        arrayList4 = arrayList3;
                        albumInfo2 = albumInfo;
                        songMonthInfo2 = songMonthInfo;
                        arrayList6 = arrayList2;
                        musicInfo2 = musicInfo;
                        clubUserInfo2 = clubUserInfo;
                        arrayList5 = arrayList;
                        bizInfo2 = bizInfo;
                        mVInfo2 = mVInfo;
                        break;
                }
                mVInfo = mVInfo2;
                bizInfo = bizInfo2;
                arrayList = arrayList5;
                clubUserInfo = clubUserInfo2;
                musicInfo = musicInfo2;
                arrayList2 = arrayList6;
                songMonthInfo = songMonthInfo2;
                albumInfo = albumInfo2;
                arrayList3 = arrayList4;
                arrayList4 = arrayList3;
                albumInfo2 = albumInfo;
                songMonthInfo2 = songMonthInfo;
                arrayList6 = arrayList2;
                musicInfo2 = musicInfo;
                clubUserInfo2 = clubUserInfo;
                arrayList5 = arrayList;
                bizInfo2 = bizInfo;
                mVInfo2 = mVInfo;
            }
            Logger.i("TAG", "resCode:" + orderPolicy.getResCode() + "\n " + "resMsg:" + orderPolicy.getResMsg() + "\n" + " mobile:" + orderPolicy.getMobile() + "\n" + " BizInfo:" + bizInfo2 + "\n" + "MusicInfo:" + musicInfo2 + "\n" + "downUrl:" + orderPolicy.getDownUrl() + "\n" + "downUrlL:" + orderPolicy.getDownUrlL() + "\n " + "invalidDate:" + orderPolicy.getInvalidDate() + "\n" + "monthType:" + orderPolicy.getMonthType() + "\n" + "MVInfo:" + orderPolicy.getMvInfo() + "userMonType:" + orderPolicy.getUserMonType());
            if (arrayList5 != null && arrayList5.size() > 0) {
                Iterator it = arrayList5.iterator();
                while (it.hasNext()) {
                    Logger.i("TAG", ((BizInfo) it.next()).toString());
                }
            }
            if (musicInfo2 != null) {
                Logger.i("TAG", musicInfo2.toString());
            }
            return orderPolicy;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public static MVMonthPolicy getMvMonthPolicy(InputStream inputStream) throws IOException, XmlPullParserException {
        MVOrderInfo mVOrderInfo;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        MVMonthPolicy mVMonthPolicy = new MVMonthPolicy();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            MVOrderInfo mVOrderInfo2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        mVOrderInfo = mVOrderInfo2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        mVOrderInfo2 = mVOrderInfo;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            mVMonthPolicy.setResCode(newPullParser.nextText());
                            mVOrderInfo = mVOrderInfo2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            mVMonthPolicy.setResMsg(newPullParser.nextText());
                            mVOrderInfo = mVOrderInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("mobile")) {
                            mVMonthPolicy.setMobile(newPullParser.nextText());
                            mVOrderInfo = mVOrderInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("orderInfo")) {
                            mVOrderInfo = new MVOrderInfo();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("serviceId")) {
                            mVOrderInfo2.setServiceId(newPullParser.nextText());
                            mVOrderInfo = mVOrderInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase(SelectCountryActivity.EXTRA_COUNTRY_NAME)) {
                            mVOrderInfo2.setName(newPullParser.nextText());
                            mVOrderInfo = mVOrderInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("type")) {
                            mVOrderInfo2.setType(newPullParser.nextText());
                            mVOrderInfo = mVOrderInfo2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        mVOrderInfo2 = mVOrderInfo;
                        break;
                    case 3:
                        if (name.equalsIgnoreCase("orderInfo")) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                mVMonthPolicy.setmVOrderInfos(arrayList2);
                            }
                            arrayList2.add(mVOrderInfo2);
                            break;
                        }
                        break;
                }
                mVOrderInfo = mVOrderInfo2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                mVOrderInfo2 = mVOrderInfo;
            }
            return mVMonthPolicy;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    static LoginResult getLoginResult(InputStream inputStream) throws IOException, XmlPullParserException {
        AccountInfo accountInfo;
        PaymentUserInfo paymentUserInfo;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        LoginResult loginResult = new LoginResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            AccountInfo accountInfo2 = null;
            PaymentUserInfo paymentUserInfo2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        accountInfo = accountInfo2;
                        paymentUserInfo = paymentUserInfo2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        paymentUserInfo2 = paymentUserInfo;
                        accountInfo2 = accountInfo;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            loginResult.setResCode(newPullParser.nextText());
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            loginResult.setResMsg(newPullParser.nextText());
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("AccountInfo")) {
                            accountInfo = new AccountInfo();
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("accountName")) {
                            accountInfo2.setAccountName(newPullParser.nextText());
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("accountType")) {
                            accountInfo2.setAccountType(newPullParser.nextText());
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("verified")) {
                            accountInfo2.setVerified(newPullParser.nextText());
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("createTime")) {
                            if (accountInfo2 != null) {
                                accountInfo2.setCreateTime(newPullParser.nextText());
                                accountInfo = accountInfo2;
                                paymentUserInfo = paymentUserInfo2;
                                arrayList = arrayList2;
                            } else if (paymentUserInfo2 != null) {
                                paymentUserInfo2.setCreateTime(newPullParser.nextText());
                                accountInfo = accountInfo2;
                                paymentUserInfo = paymentUserInfo2;
                                arrayList = arrayList2;
                            }
                        } else if (name.equalsIgnoreCase("updateTime")) {
                            if (accountInfo2 != null) {
                                accountInfo2.setUpdateTime(newPullParser.nextText());
                                accountInfo = accountInfo2;
                                paymentUserInfo = paymentUserInfo2;
                                arrayList = arrayList2;
                            } else if (paymentUserInfo2 != null) {
                                paymentUserInfo2.setUpdateTime(newPullParser.nextText());
                                accountInfo = accountInfo2;
                                paymentUserInfo = paymentUserInfo2;
                                arrayList = arrayList2;
                            }
                        } else if (name.equalsIgnoreCase("UserInfo")) {
                            accountInfo = accountInfo2;
                            paymentUserInfo = new PaymentUserInfo();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("identityID")) {
                            paymentUserInfo2.setIdentityID(newPullParser.nextText());
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        paymentUserInfo2 = paymentUserInfo;
                        accountInfo2 = accountInfo;
                        break;
                    case 3:
                        if (!name.equalsIgnoreCase("AccountInfo")) {
                            if (name.equalsIgnoreCase("PaymentUserInfo")) {
                                loginResult.setUserInfo(paymentUserInfo2);
                                break;
                            }
                        } else {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                loginResult.setAccountInfoList(arrayList2);
                            }
                            arrayList2.add(accountInfo2);
                            accountInfo = accountInfo2;
                            paymentUserInfo = paymentUserInfo2;
                            arrayList = arrayList2;
                            continue;
                            arrayList2 = arrayList;
                            paymentUserInfo2 = paymentUserInfo;
                            accountInfo2 = accountInfo;
                        }
                        break;
                }
                accountInfo = accountInfo2;
                paymentUserInfo = paymentUserInfo2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                paymentUserInfo2 = paymentUserInfo;
                accountInfo2 = accountInfo;
            }
            return loginResult;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    static SongRecommendResult getSongRecommendResult(InputStream inputStream) throws IOException, XmlPullParserException {
        SongRecommendContentIds songRecommendContentIds;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (inputStream == null) {
            return null;
        }
        SongRecommendResult songRecommendResult = new SongRecommendResult();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(inputStream, "UTF-8");
            SongRecommendContentIds songRecommendContentIds2 = null;
            for (int eventType = newPullParser.getEventType(); 1 != eventType; eventType = newPullParser.next()) {
                String name = newPullParser.getName();
                switch (eventType) {
                    case 0:
                        songRecommendContentIds = songRecommendContentIds2;
                        arrayList = arrayList2;
                        continue;
                        arrayList2 = arrayList;
                        songRecommendContentIds2 = songRecommendContentIds;
                    case 2:
                        if (name.equalsIgnoreCase("resCode")) {
                            songRecommendResult.setResCode(newPullParser.nextText());
                            songRecommendContentIds = songRecommendContentIds2;
                            arrayList = arrayList2;
                            continue;
                        } else if (name.equalsIgnoreCase("resMsg")) {
                            songRecommendResult.setResMsg(newPullParser.nextText());
                            songRecommendContentIds = songRecommendContentIds2;
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("SongRecommendContentIds")) {
                            songRecommendContentIds = new SongRecommendContentIds();
                            arrayList = arrayList2;
                        } else if (name.equalsIgnoreCase("contentIds")) {
                            songRecommendContentIds2.setContentId(newPullParser.nextText());
                            songRecommendContentIds = songRecommendContentIds2;
                            arrayList = arrayList2;
                        }
                        arrayList2 = arrayList;
                        songRecommendContentIds2 = songRecommendContentIds;
                        break;
                    case 3:
                        if (name.equalsIgnoreCase("SongRecommendContentIds")) {
                            if (arrayList2 == null) {
                                arrayList2 = new ArrayList();
                                songRecommendResult.setSongRecommendContentIds(arrayList2);
                            }
                            arrayList2.add(songRecommendContentIds2);
                            break;
                        }
                        break;
                }
                songRecommendContentIds = songRecommendContentIds2;
                arrayList = arrayList2;
                arrayList2 = arrayList;
                songRecommendContentIds2 = songRecommendContentIds;
            }
            return songRecommendResult;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
    }

    static String getPriceString(String str) {
        if ("0".equals(str)) {
            return "免费";
        }
        return String.valueOf(Float.valueOf(Float.valueOf(str).floatValue() / 100.0f).toString()) + " 元";
    }

    static String getPrice(OrderPolicy orderPolicy, String str) {
        ArrayList<BizInfo> bizInfos;
        BizInfo bizInfo;
        String salePrice;
        if (orderPolicy == null || (bizInfos = orderPolicy.getBizInfos()) == null || (bizInfo = bizInfos.get(0)) == null || (salePrice = bizInfo.getSalePrice()) == null || salePrice.trim().length() == 0) {
            return str;
        }
        return salePrice;
    }

    static String getBizCode(OrderPolicy orderPolicy) {
        if (orderPolicy == null) {
            return null;
        }
        ArrayList<BizInfo> bizInfos = orderPolicy.getBizInfos();
        if (bizInfos == null) {
            return null;
        }
        BizInfo bizInfo = bizInfos.get(0);
        if (bizInfo == null) {
            return null;
        }
        return bizInfo.getBizCode();
    }

    static BizInfo findBizInfoByPrice(int i, OrderPolicy orderPolicy) {
        if (orderPolicy == null) {
            return null;
        }
        ArrayList<BizInfo> bizInfos = orderPolicy.getBizInfos();
        if (bizInfos == null) {
            return null;
        }
        Iterator<BizInfo> it = bizInfos.iterator();
        while (it.hasNext()) {
            BizInfo next = it.next();
            if (i == Integer.valueOf(next.getSalePrice()).intValue()) {
                return next;
            }
        }
        return null;
    }
}
