package com.cmsc.cmmusic.common.data;

public class TransferResult extends Result {
    private String balance;
    private String tradeNum;

    public String getBalance() {
        return this.balance;
    }

    public void setBalance(String str) {
        this.balance = str;
    }

    public String getTradeNum() {
        return this.tradeNum;
    }

    public void setTradeNum(String str) {
        this.tradeNum = str;
    }

    public String toString() {
        return "TransferResult [balance=" + this.balance + ", tradeNum=" + this.tradeNum + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
