package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.method.DigitsKeyListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.init.Utils;

final class DigitalAlbumGiveView extends DigitalAlbumOrderView {
    private EditText edtPhoneNum;
    private LinearLayout phoneNumView;
    private TextView txtPhoneTip;

    public DigitalAlbumGiveView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        super.updateNetView();
        setUserTip("点击“确认”将把该专辑赠送给您的好友");
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        String editable = this.edtPhoneNum.getText().toString();
        if (editable == null || !Utils.validatePhoneNumber(editable)) {
            this.edtPhoneNum.requestFocus();
            this.edtPhoneNum.setError(Html.fromHtml("<font color='red'>请正确输入手机号码</font>"));
            return;
        }
        BizInfo phonePayTypeBizInfo = getPhonePayTypeBizInfo();
        if (phonePayTypeBizInfo != null) {
            EnablerInterface.giveDigitalAlbum(this.mCurActivity, this.curExtraInfo.getString("MusicId"), editable, phonePayTypeBizInfo.getBizCode(), phonePayTypeBizInfo.getSalePrice(), this.policyObj.getMonLevel(), phonePayTypeBizInfo.getHold2(), new CMMusicCallback<OrderResult>() {
                public void operationResult(OrderResult orderResult) {
                    DigitalAlbumGiveView.this.mCurActivity.closeActivity(orderResult);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        linearLayout.addView(getPhoneNumView());
        super.initContentView(linearLayout);
    }

    private LinearLayout getPhoneNumView() {
        this.phoneNumView = new LinearLayout(this.mCurActivity);
        this.phoneNumView.setOrientation(1);
        this.phoneNumView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.phoneNumView.setVisibility(0);
        this.phoneNumView.setPadding(0, 10, 0, 0);
        this.phoneNumView.setGravity(16);
        this.txtPhoneTip = new TextView(this.mCurActivity);
        this.txtPhoneTip.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.txtPhoneTip.setTextAppearance(this.mCurActivity, 16973892);
        this.txtPhoneTip.setText("\n赠送手机号：");
        this.phoneNumView.addView(this.txtPhoneTip);
        this.edtPhoneNum = new EditText(this.mCurActivity);
        this.edtPhoneNum.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.edtPhoneNum.setInputType(3);
        this.edtPhoneNum.setKeyListener(new DigitsKeyListener(false, false));
        this.phoneNumView.addView(this.edtPhoneNum);
        return this.phoneNumView;
    }
}
