package com.cmsc.cmmusic.common.data;

public class SongMonthInfo {
    private String type;
    private String typeDesc;

    public String getType() {
        return this.type;
    }

    public void setType(String str) {
        this.type = str;
    }

    public String getTypeDesc() {
        return this.typeDesc;
    }

    public void setTypeDesc(String str) {
        this.typeDesc = str;
    }

    public String toString() {
        return "SongMonthInfo [type=" + this.type + ", typeDesc=" + this.typeDesc + "]";
    }
}
