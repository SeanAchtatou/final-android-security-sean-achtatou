package com.cmsc.cmmusic.common.data;

public class AlbumInfo {
    private String albumId;
    private String description;
    private String imgUrl;
    private String language;
    private String name;
    private String price;
    private String singerId;
    private String singerName;

    public String getAlbumId() {
        return this.albumId;
    }

    public void setAlbumId(String str) {
        this.albumId = str;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String str) {
        this.description = str;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String str) {
        this.language = str;
    }

    public String getSingerName() {
        return this.singerName;
    }

    public void setSingerName(String str) {
        this.singerName = str;
    }

    public String getSingerId() {
        return this.singerId;
    }

    public void setSingerId(String str) {
        this.singerId = str;
    }

    public String getImgUrl() {
        return this.imgUrl;
    }

    public void setImgUrl(String str) {
        this.imgUrl = str;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String str) {
        this.price = str;
    }

    public String toString() {
        return "AlbumInfo [albumId=" + this.albumId + ", name=" + this.name + ", description=" + this.description + ", language=" + this.language + ", singerName=" + this.singerName + ", singerId=" + this.singerId + ", imgUrl=" + this.imgUrl + ", price=" + this.price + "]";
    }
}
