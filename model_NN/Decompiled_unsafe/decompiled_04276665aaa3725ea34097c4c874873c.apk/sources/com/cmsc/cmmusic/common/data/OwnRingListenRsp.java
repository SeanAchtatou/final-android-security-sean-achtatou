package com.cmsc.cmmusic.common.data;

public class OwnRingListenRsp extends Result {
    private String musicUrl;

    public String getmusicUrl() {
        return this.musicUrl;
    }

    public void setmusicUrl(String str) {
        this.musicUrl = str;
    }

    public String toString() {
        return "OwnRingRsp [musicUrl=" + this.musicUrl + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
