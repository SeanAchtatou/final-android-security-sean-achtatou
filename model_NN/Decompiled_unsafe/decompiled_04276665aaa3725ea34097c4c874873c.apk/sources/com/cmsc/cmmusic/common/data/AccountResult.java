package com.cmsc.cmmusic.common.data;

public class AccountResult extends Result {
    private String accountStatus;
    private String balance;
    private String passwordFlag;

    public String getBalance() {
        return this.balance;
    }

    public void setBalance(String str) {
        this.balance = str;
    }

    public String getAccountStatus() {
        return this.accountStatus;
    }

    public void setAccountStatus(String str) {
        this.accountStatus = str;
    }

    public String getPasswordFlag() {
        return this.passwordFlag;
    }

    public void setPasswordFlag(String str) {
        this.passwordFlag = str;
    }

    public String toString() {
        return "AccountResult [balance=" + this.balance + ", accountStatus=" + this.accountStatus + ", passwordFlag=" + this.passwordFlag + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
