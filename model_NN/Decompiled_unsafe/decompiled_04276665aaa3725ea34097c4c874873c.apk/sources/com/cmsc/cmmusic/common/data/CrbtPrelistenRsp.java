package com.cmsc.cmmusic.common.data;

public class CrbtPrelistenRsp extends Result {
    private String invalidDate;
    private String mobile;
    private String price;
    private String streamUrl;

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String str) {
        this.price = str;
    }

    public String getInvalidDate() {
        return this.invalidDate;
    }

    public void setInvalidDate(String str) {
        this.invalidDate = str;
    }

    public String getStreamUrl() {
        return this.streamUrl;
    }

    public void setStreamUrl(String str) {
        this.streamUrl = str;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String str) {
        this.mobile = str;
    }

    public String toString() {
        return "CrbtPrelistenRsp [price=" + this.price + ", invalidDate=" + this.invalidDate + ", streamUrl=" + this.streamUrl + ", mobile=" + this.mobile + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
