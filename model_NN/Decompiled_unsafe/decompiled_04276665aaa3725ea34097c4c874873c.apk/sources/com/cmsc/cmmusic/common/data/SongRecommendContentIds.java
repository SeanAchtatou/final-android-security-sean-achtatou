package com.cmsc.cmmusic.common.data;

public class SongRecommendContentIds extends Result {
    private String contentId;

    public SongRecommendContentIds() {
    }

    public SongRecommendContentIds(Result result) {
        if (result != null) {
            setResCode(result.getResCode());
            setResMsg(result.getResMsg());
        }
    }

    public String getContentId() {
        return this.contentId;
    }

    public void setContentId(String str) {
        this.contentId = str;
    }

    public String toString() {
        return "SongRecommendContentIds [contentId=" + this.contentId + "]";
    }
}
