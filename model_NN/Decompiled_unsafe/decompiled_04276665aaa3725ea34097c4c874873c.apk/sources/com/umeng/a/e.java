package com.umeng.a;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.a.a.bb;

/* compiled from: InternalConfig */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private static String[] f2757a = new String[2];

    public static String[] a(Context context) {
        String[] a2;
        if (!TextUtils.isEmpty(f2757a[0]) && !TextUtils.isEmpty(f2757a[1])) {
            return f2757a;
        }
        if (context == null || (a2 = bb.a(context).a()) == null) {
            return null;
        }
        f2757a[0] = a2[0];
        f2757a[1] = a2[1];
        return f2757a;
    }
}
