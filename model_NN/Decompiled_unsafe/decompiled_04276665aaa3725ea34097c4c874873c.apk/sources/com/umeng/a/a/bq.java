package com.umeng.a.a;

import android.content.Context;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;

/* compiled from: UMCCDBUtils */
public class bq {
    public static String a(Context context) {
        return "/data/data/" + context.getPackageName() + "/databases/cc/";
    }

    public static String a(List<String> list) {
        return TextUtils.join("!", list);
    }

    public static JSONArray a(String str) {
        String[] split = str.split("!");
        JSONArray jSONArray = new JSONArray();
        for (String put : split) {
            jSONArray.put(put);
        }
        return jSONArray;
    }

    public static List<String> b(String str) {
        return new ArrayList(Arrays.asList(str.split("!")));
    }
}
