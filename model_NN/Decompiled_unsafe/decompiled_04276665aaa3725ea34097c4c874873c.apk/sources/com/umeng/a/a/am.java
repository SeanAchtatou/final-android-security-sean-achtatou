package com.umeng.a.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Imprint */
public class am implements be<am, e>, Serializable, Cloneable {
    public static final Map<e, bj> d;
    /* access modifiers changed from: private */
    public static final bz e = new bz("Imprint");
    /* access modifiers changed from: private */
    public static final br f = new br("property", (byte) 13, 1);
    /* access modifiers changed from: private */
    public static final br g = new br("version", (byte) 8, 2);
    /* access modifiers changed from: private */
    public static final br h = new br("checksum", (byte) 11, 3);
    private static final Map<Class<? extends cb>, cc> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public Map<String, an> f2638a;
    public int b;
    public String c;
    private byte j = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.umeng.a.a.am$e, com.umeng.a.a.bj]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(cd.class, new b());
        i.put(ce.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.PROPERTY, (Object) new bj("property", (byte) 1, new bm((byte) 13, new bk((byte) 11), new bn((byte) 12, an.class))));
        enumMap.put((Object) e.VERSION, (Object) new bj("version", (byte) 1, new bk((byte) 8)));
        enumMap.put((Object) e.CHECKSUM, (Object) new bj("checksum", (byte) 1, new bk((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        bj.a(am.class, d);
    }

    /* compiled from: Imprint */
    public enum e {
        PROPERTY(1, "property"),
        VERSION(2, "version"),
        CHECKSUM(3, "checksum");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.a(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public String a() {
            return this.f;
        }
    }

    public Map<String, an> a() {
        return this.f2638a;
    }

    public boolean b() {
        return this.f2638a != null;
    }

    public void a(boolean z) {
        if (!z) {
            this.f2638a = null;
        }
    }

    public int c() {
        return this.b;
    }

    public am a(int i2) {
        this.b = i2;
        b(true);
        return this;
    }

    public boolean d() {
        return bc.a(this.j, 0);
    }

    public void b(boolean z) {
        this.j = bc.a(this.j, 0, z);
    }

    public String e() {
        return this.c;
    }

    public am a(String str) {
        this.c = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(bu buVar) throws bh {
        i.get(buVar.y()).b().b(buVar, this);
    }

    public void b(bu buVar) throws bh {
        i.get(buVar.y()).b().a(buVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Imprint(");
        sb.append("property:");
        if (this.f2638a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2638a);
        }
        sb.append(", ");
        sb.append("version:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("checksum:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }

    public void f() throws bh {
        if (this.f2638a == null) {
            throw new bv("Required field 'property' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new bv("Required field 'checksum' was not present! Struct: " + toString());
        }
    }

    /* compiled from: Imprint */
    private static class b implements cc {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Imprint */
    private static class a extends cd<am> {
        private a() {
        }

        /* renamed from: a */
        public void b(bu buVar, am amVar) throws bh {
            buVar.f();
            while (true) {
                br h = buVar.h();
                if (h.b == 0) {
                    buVar.g();
                    if (!amVar.d()) {
                        throw new bv("Required field 'version' was not found in serialized data! Struct: " + toString());
                    }
                    amVar.f();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 13) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            bt j = buVar.j();
                            amVar.f2638a = new HashMap(j.c * 2);
                            for (int i = 0; i < j.c; i++) {
                                String v = buVar.v();
                                an anVar = new an();
                                anVar.a(buVar);
                                amVar.f2638a.put(v, anVar);
                            }
                            buVar.k();
                            amVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 8) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            amVar.b = buVar.s();
                            amVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 11) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            amVar.c = buVar.v();
                            amVar.c(true);
                            break;
                        }
                    default:
                        bx.a(buVar, h.b);
                        break;
                }
                buVar.i();
            }
        }

        /* renamed from: b */
        public void a(bu buVar, am amVar) throws bh {
            amVar.f();
            buVar.a(am.e);
            if (amVar.f2638a != null) {
                buVar.a(am.f);
                buVar.a(new bt((byte) 11, (byte) 12, amVar.f2638a.size()));
                for (Map.Entry next : amVar.f2638a.entrySet()) {
                    buVar.a((String) next.getKey());
                    ((an) next.getValue()).b(buVar);
                }
                buVar.d();
                buVar.b();
            }
            buVar.a(am.g);
            buVar.a(amVar.b);
            buVar.b();
            if (amVar.c != null) {
                buVar.a(am.h);
                buVar.a(amVar.c);
                buVar.b();
            }
            buVar.c();
            buVar.a();
        }
    }

    /* compiled from: Imprint */
    private static class d implements cc {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Imprint */
    private static class c extends ce<am> {
        private c() {
        }

        public void a(bu buVar, am amVar) throws bh {
            ca caVar = (ca) buVar;
            caVar.a(amVar.f2638a.size());
            for (Map.Entry next : amVar.f2638a.entrySet()) {
                caVar.a((String) next.getKey());
                ((an) next.getValue()).b(caVar);
            }
            caVar.a(amVar.b);
            caVar.a(amVar.c);
        }

        public void b(bu buVar, am amVar) throws bh {
            ca caVar = (ca) buVar;
            bt btVar = new bt((byte) 11, (byte) 12, caVar.s());
            amVar.f2638a = new HashMap(btVar.c * 2);
            for (int i = 0; i < btVar.c; i++) {
                String v = caVar.v();
                an anVar = new an();
                anVar.a(caVar);
                amVar.f2638a.put(v, anVar);
            }
            amVar.a(true);
            amVar.b = caVar.s();
            amVar.b(true);
            amVar.c = caVar.v();
            amVar.c(true);
        }
    }
}
