package com.umeng.a.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Response */
public class ao implements be<ao, e>, Serializable, Cloneable {
    public static final Map<e, bj> d;
    /* access modifiers changed from: private */
    public static final bz e = new bz("Response");
    /* access modifiers changed from: private */
    public static final br f = new br("resp_code", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final br g = new br("msg", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final br h = new br("imprint", (byte) 12, 3);
    private static final Map<Class<? extends cb>, cc> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f2642a;
    public String b;
    public am c;
    private byte j = 0;
    private e[] k = {e.MSG, e.IMPRINT};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.umeng.a.a.ao$e, com.umeng.a.a.bj]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(cd.class, new b());
        i.put(ce.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.RESP_CODE, (Object) new bj("resp_code", (byte) 1, new bk((byte) 8)));
        enumMap.put((Object) e.MSG, (Object) new bj("msg", (byte) 2, new bk((byte) 11)));
        enumMap.put((Object) e.IMPRINT, (Object) new bj("imprint", (byte) 2, new bn((byte) 12, am.class)));
        d = Collections.unmodifiableMap(enumMap);
        bj.a(ao.class, d);
    }

    /* compiled from: Response */
    public enum e {
        RESP_CODE(1, "resp_code"),
        MSG(2, "msg"),
        IMPRINT(3, "imprint");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.a(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public String a() {
            return this.f;
        }
    }

    public boolean a() {
        return bc.a(this.j, 0);
    }

    public void a(boolean z) {
        this.j = bc.a(this.j, 0, z);
    }

    public String b() {
        return this.b;
    }

    public boolean c() {
        return this.b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public am d() {
        return this.c;
    }

    public boolean e() {
        return this.c != null;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(bu buVar) throws bh {
        i.get(buVar.y()).b().b(buVar, this);
    }

    public void b(bu buVar) throws bh {
        i.get(buVar.y()).b().a(buVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Response(");
        sb.append("resp_code:");
        sb.append(this.f2642a);
        if (c()) {
            sb.append(", ");
            sb.append("msg:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("imprint:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void f() throws bh {
        if (this.c != null) {
            this.c.f();
        }
    }

    /* compiled from: Response */
    private static class b implements cc {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Response */
    private static class a extends cd<ao> {
        private a() {
        }

        /* renamed from: a */
        public void b(bu buVar, ao aoVar) throws bh {
            buVar.f();
            while (true) {
                br h = buVar.h();
                if (h.b == 0) {
                    buVar.g();
                    if (!aoVar.a()) {
                        throw new bv("Required field 'resp_code' was not found in serialized data! Struct: " + toString());
                    }
                    aoVar.f();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 8) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            aoVar.f2642a = buVar.s();
                            aoVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 11) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            aoVar.b = buVar.v();
                            aoVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 12) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            aoVar.c = new am();
                            aoVar.c.a(buVar);
                            aoVar.c(true);
                            break;
                        }
                    default:
                        bx.a(buVar, h.b);
                        break;
                }
                buVar.i();
            }
        }

        /* renamed from: b */
        public void a(bu buVar, ao aoVar) throws bh {
            aoVar.f();
            buVar.a(ao.e);
            buVar.a(ao.f);
            buVar.a(aoVar.f2642a);
            buVar.b();
            if (aoVar.b != null && aoVar.c()) {
                buVar.a(ao.g);
                buVar.a(aoVar.b);
                buVar.b();
            }
            if (aoVar.c != null && aoVar.e()) {
                buVar.a(ao.h);
                aoVar.c.b(buVar);
                buVar.b();
            }
            buVar.c();
            buVar.a();
        }
    }

    /* compiled from: Response */
    private static class d implements cc {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Response */
    private static class c extends ce<ao> {
        private c() {
        }

        public void a(bu buVar, ao aoVar) throws bh {
            ca caVar = (ca) buVar;
            caVar.a(aoVar.f2642a);
            BitSet bitSet = new BitSet();
            if (aoVar.c()) {
                bitSet.set(0);
            }
            if (aoVar.e()) {
                bitSet.set(1);
            }
            caVar.a(bitSet, 2);
            if (aoVar.c()) {
                caVar.a(aoVar.b);
            }
            if (aoVar.e()) {
                aoVar.c.b(caVar);
            }
        }

        public void b(bu buVar, ao aoVar) throws bh {
            ca caVar = (ca) buVar;
            aoVar.f2642a = caVar.s();
            aoVar.a(true);
            BitSet b = caVar.b(2);
            if (b.get(0)) {
                aoVar.b = caVar.v();
                aoVar.b(true);
            }
            if (b.get(1)) {
                aoVar.c = new am();
                aoVar.c.a(caVar);
                aoVar.c(true);
            }
        }
    }
}
