package com.umeng.a.a;

import android.content.Context;
import com.umeng.a.a;
import com.umeng.a.a.g;

/* compiled from: ImLatent */
public class ai implements y {
    private static ai l = null;

    /* renamed from: a  reason: collision with root package name */
    private final long f2631a = 1296000000;
    private final long b = 129600000;
    private final int c = 1800000;
    private final int d = 10000;
    private bb e;
    private ae f;
    private long g = 1296000000;
    private int h = 10000;
    private long i = 0;
    private long j = 0;
    private Context k;

    public static synchronized ai a(Context context, ae aeVar) {
        ai aiVar;
        synchronized (ai.class) {
            if (l == null) {
                l = new ai(context, aeVar);
                l.a(g.a(context).b());
            }
            aiVar = l;
        }
        return aiVar;
    }

    private ai(Context context, ae aeVar) {
        this.k = context;
        this.e = bb.a(context);
        this.f = aeVar;
    }

    public boolean a() {
        if (this.e.f() || this.f.e()) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis() - this.f.l();
        if (currentTimeMillis > this.g) {
            this.i = (long) ar.a(this.h, b.a(this.k));
            this.j = currentTimeMillis;
            return true;
        } else if (currentTimeMillis <= 129600000) {
            return false;
        } else {
            this.i = 0;
            this.j = currentTimeMillis;
            return true;
        }
    }

    public long b() {
        return this.i;
    }

    public void a(g.a aVar) {
        this.g = aVar.a(1296000000L);
        int b2 = aVar.b(0);
        if (b2 != 0) {
            this.h = b2;
        } else if (a.i <= 0 || a.i > 1800000) {
            this.h = 10000;
        } else {
            this.h = a.i;
        }
    }
}
