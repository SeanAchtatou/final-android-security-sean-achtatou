package com.umeng.a.a;

/* compiled from: TProtocolUtil */
public class bx {

    /* renamed from: a  reason: collision with root package name */
    private static int f2684a = Integer.MAX_VALUE;

    public static void a(bu buVar, byte b) throws bh {
        a(buVar, b, f2684a);
    }

    public static void a(bu buVar, byte b, int i) throws bh {
        int i2 = 0;
        if (i <= 0) {
            throw new bh("Maximum skip depth exceeded");
        }
        switch (b) {
            case 2:
                buVar.p();
                return;
            case 3:
                buVar.q();
                return;
            case 4:
                buVar.u();
                return;
            case 5:
            case 7:
            case 9:
            default:
                return;
            case 6:
                buVar.r();
                return;
            case 8:
                buVar.s();
                return;
            case 10:
                buVar.t();
                return;
            case 11:
                buVar.w();
                return;
            case 12:
                buVar.f();
                while (true) {
                    br h = buVar.h();
                    if (h.b == 0) {
                        buVar.g();
                        return;
                    } else {
                        a(buVar, h.b, i - 1);
                        buVar.i();
                    }
                }
            case 13:
                bt j = buVar.j();
                while (i2 < j.c) {
                    a(buVar, j.f2682a, i - 1);
                    a(buVar, j.b, i - 1);
                    i2++;
                }
                buVar.k();
                return;
            case 14:
                by n = buVar.n();
                while (i2 < n.b) {
                    a(buVar, n.f2685a, i - 1);
                    i2++;
                }
                buVar.o();
                return;
            case 15:
                bs l = buVar.l();
                while (i2 < l.b) {
                    a(buVar, l.f2681a, i - 1);
                    i2++;
                }
                buVar.m();
                return;
        }
    }
}
