package com.umeng.a.a;

import android.content.Context;
import android.telephony.TelephonyManager;

/* compiled from: ImeiTracker */
public class f extends cy {

    /* renamed from: a  reason: collision with root package name */
    private Context f2725a;

    public f(Context context) {
        super("imei");
        this.f2725a = context;
    }

    public String a() {
        TelephonyManager telephonyManager = (TelephonyManager) this.f2725a.getSystemService("phone");
        if (telephonyManager == null) {
        }
        try {
            if (at.a(this.f2725a, "android.permission.READ_PHONE_STATE")) {
                return telephonyManager.getDeviceId();
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
