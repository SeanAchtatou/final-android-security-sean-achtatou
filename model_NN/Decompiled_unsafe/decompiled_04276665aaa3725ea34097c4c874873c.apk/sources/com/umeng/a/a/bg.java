package com.umeng.a.a;

import com.umeng.a.a.bp;

/* compiled from: TDeserializer */
public class bg {

    /* renamed from: a  reason: collision with root package name */
    private final bu f2672a;
    private final cg b;

    public bg() {
        this(new bp.a());
    }

    public bg(bw bwVar) {
        this.b = new cg();
        this.f2672a = bwVar.a(this.b);
    }

    public void a(be beVar, byte[] bArr) throws bh {
        try {
            this.b.a(bArr);
            beVar.a(this.f2672a);
        } finally {
            this.b.a();
            this.f2672a.x();
        }
    }
}
