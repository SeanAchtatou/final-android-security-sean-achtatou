package com.umeng.a.a;

import android.content.Context;

/* compiled from: CacheService */
public final class r implements u {
    private static r c;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public u f2741a = new q(this.b);
    private Context b;

    private r(Context context) {
        this.b = context;
    }

    public synchronized q a(Context context) {
        return (q) this.f2741a;
    }

    public static synchronized r b(Context context) {
        r rVar;
        synchronized (r.class) {
            if (c == null && context != null) {
                c = new r(context);
            }
            rVar = c;
        }
        return rVar;
    }

    public void a(final Object obj) {
        ax.b(new ba() {
            public void a() {
                r.this.f2741a.a(obj);
            }
        });
    }

    public void a() {
        ax.b(new ba() {
            public void a() {
                r.this.f2741a.a();
            }
        });
    }

    public void b() {
        ax.c(new ba() {
            public void a() {
                r.this.f2741a.b();
            }
        });
    }
}
