package com.umeng.a.a;

import android.content.Context;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.umeng.a.a.g;
import org.json.JSONObject;

/* compiled from: Defcon */
public class ah implements y {
    private static ah c = null;

    /* renamed from: a  reason: collision with root package name */
    private int f2630a = 0;
    private final long b = 60000;

    public static synchronized ah a(Context context) {
        ah ahVar;
        synchronized (ah.class) {
            if (c == null) {
                c = new ah();
                c.a(g.a(context).b().a(0));
            }
            ahVar = c;
        }
        return ahVar;
    }

    private ah() {
    }

    public void a(JSONObject jSONObject, Context context) {
        if (this.f2630a == 1) {
            jSONObject.remove(SocketMessage.MSG_ERROR_KEY);
            jSONObject.remove("ekv");
            jSONObject.remove("gkv");
            jSONObject.remove("cc");
            cx.a(context).a(false, true);
            co.a(context).b(new ck());
        } else if (this.f2630a == 2) {
            jSONObject.remove("sessions");
            try {
                jSONObject.put("sessions", a());
            } catch (Exception e) {
            }
            jSONObject.remove(SocketMessage.MSG_ERROR_KEY);
            jSONObject.remove("ekv");
            jSONObject.remove("gkv");
            jSONObject.remove("cc");
            cx.a(context).a(false, true);
            co.a(context).b(new ck());
        } else if (this.f2630a == 3) {
            jSONObject.remove("sessions");
            jSONObject.remove(SocketMessage.MSG_ERROR_KEY);
            jSONObject.remove("ekv");
            jSONObject.remove("gkv");
            jSONObject.remove("cc");
            cx.a(context).a(false, true);
            co.a(context).b(new ck());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException} */
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            long currentTimeMillis = System.currentTimeMillis();
            jSONObject.put("id", ad.a());
            jSONObject.put("start_time", currentTimeMillis);
            jSONObject.put("end_time", currentTimeMillis + 60000);
            jSONObject.put("duration", 60000L);
        } catch (Throwable th) {
        }
        return jSONObject;
    }

    public long b() {
        switch (this.f2630a) {
            case 1:
                return 14400000;
            case 2:
                return 28800000;
            case 3:
                return LogBuilder.MAX_INTERVAL;
            default:
                return 0;
        }
    }

    public void a(int i) {
        if (i >= 0 && i <= 3) {
            this.f2630a = i;
        }
    }

    public boolean c() {
        return this.f2630a != 0;
    }

    public void a(g.a aVar) {
        a(aVar.a(0));
    }
}
