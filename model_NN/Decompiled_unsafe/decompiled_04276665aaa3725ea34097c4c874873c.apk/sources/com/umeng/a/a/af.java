package com.umeng.a.a;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.a.a.cx;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: ViewPageTracker */
public class af {
    private static JSONObject b = new JSONObject();

    /* renamed from: a  reason: collision with root package name */
    private final Map<String, Long> f2628a = new HashMap();

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            synchronized (this.f2628a) {
                this.f2628a.put(str, Long.valueOf(System.currentTimeMillis()));
            }
        }
    }

    public void b(String str) {
        Long remove;
        if (!TextUtils.isEmpty(str)) {
            synchronized (this.f2628a) {
                remove = this.f2628a.remove(str);
            }
            if (remove != null) {
                long currentTimeMillis = System.currentTimeMillis() - remove.longValue();
                synchronized (b) {
                    try {
                        b = new JSONObject();
                        b.put("page_name", str);
                        b.put("duration", currentTimeMillis);
                    } catch (Throwable th) {
                    }
                }
            }
        }
    }

    public void a() {
        long j;
        String str;
        String str2 = null;
        long j2 = 0;
        synchronized (this.f2628a) {
            for (Map.Entry next : this.f2628a.entrySet()) {
                if (((Long) next.getValue()).longValue() > j2) {
                    long longValue = ((Long) next.getValue()).longValue();
                    str = (String) next.getKey();
                    j = longValue;
                } else {
                    j = j2;
                    str = str2;
                }
                str2 = str;
                j2 = j;
            }
        }
        if (str2 != null) {
            b(str2);
        }
    }

    public static void a(Context context) {
        if (context != null) {
            try {
                synchronized (b) {
                    if (b.length() > 0) {
                        cx.a(context).a(ad.a(), b, cx.a.PAGE);
                        b = new JSONObject();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
