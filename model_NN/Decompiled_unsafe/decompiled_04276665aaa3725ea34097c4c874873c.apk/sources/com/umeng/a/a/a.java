package com.umeng.a.a;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.sina.weibo.sdk.constant.WBPageConstants;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: CCSQLManager */
public class a {
    public static boolean a(SQLiteDatabase sQLiteDatabase, String str) {
        try {
            if (b(sQLiteDatabase, str) >= 0) {
                sQLiteDatabase.execSQL("delete from " + str);
            }
            return true;
        } catch (SQLException e) {
            aw.c("cleanTableData faild!" + e.toString());
            return false;
        }
    }

    public static int b(SQLiteDatabase sQLiteDatabase, String str) {
        Cursor cursor = null;
        int i = 0;
        try {
            cursor = sQLiteDatabase.rawQuery("select * from " + str, null);
            i = cursor.getCount();
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            aw.c("count error " + e.toString());
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return i;
    }

    public static boolean a(SQLiteDatabase sQLiteDatabase, Collection<cm> collection) {
        try {
            sQLiteDatabase.beginTransaction();
            if (b(sQLiteDatabase, "aggregated_cache") > 0) {
                a(sQLiteDatabase, "aggregated_cache");
            }
            for (cm a2 : collection) {
                sQLiteDatabase.insert("aggregated_cache", null, a(a2));
            }
            sQLiteDatabase.setTransactionSuccessful();
            sQLiteDatabase.endTransaction();
            return true;
        } catch (SQLException e) {
            aw.c("insert to Aggregated cache table faild!");
            sQLiteDatabase.endTransaction();
            return false;
        } catch (Throwable th) {
            sQLiteDatabase.endTransaction();
            throw th;
        }
    }

    public static boolean a(ck ckVar, SQLiteDatabase sQLiteDatabase, Collection<cm> collection) {
        try {
            sQLiteDatabase.beginTransaction();
            for (cm a2 : collection) {
                sQLiteDatabase.insert("aggregated", null, a(a2));
            }
            sQLiteDatabase.setTransactionSuccessful();
            a(sQLiteDatabase, "aggregated_cache");
            ckVar.a("success", false);
            sQLiteDatabase.endTransaction();
            return true;
        } catch (SQLException e) {
            aw.c("insert to Aggregated cache table faild!");
            sQLiteDatabase.endTransaction();
            return false;
        } catch (Throwable th) {
            sQLiteDatabase.endTransaction();
            throw th;
        }
    }

    public static boolean a(SQLiteDatabase sQLiteDatabase, ck ckVar) {
        try {
            sQLiteDatabase.beginTransaction();
            if (b(sQLiteDatabase, "aggregated_cache") <= 0) {
                ckVar.a("faild", false);
                return false;
            }
            sQLiteDatabase.execSQL("insert into aggregated(key, count, value, totalTimestamp, timeWindowNum, label) select key, count, value, totalTimestamp, timeWindowNum, label from aggregated_cache");
            sQLiteDatabase.setTransactionSuccessful();
            a(sQLiteDatabase, "aggregated_cache");
            ckVar.a("success", false);
            sQLiteDatabase.endTransaction();
            return true;
        } catch (SQLException e) {
            ckVar.a(false, false);
            aw.c("cacheToAggregatedTable happen " + e.toString());
            return false;
        } finally {
            sQLiteDatabase.endTransaction();
        }
    }

    private static ContentValues a(cm cmVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("key", cmVar.a());
        contentValues.put("label", cmVar.b());
        contentValues.put(WBPageConstants.ParamKey.COUNT, Long.valueOf(cmVar.f()));
        contentValues.put("value", Long.valueOf(cmVar.e()));
        contentValues.put("totalTimestamp", Long.valueOf(cmVar.d()));
        contentValues.put("timeWindowNum", cmVar.g());
        return contentValues;
    }

    public static boolean b(SQLiteDatabase sQLiteDatabase, ck ckVar) {
        Cursor cursor = null;
        try {
            HashMap hashMap = new HashMap();
            cursor = sQLiteDatabase.rawQuery("select * from aggregated_cache", null);
            while (cursor.moveToNext()) {
                cm cmVar = new cm();
                cmVar.a(bq.b(cursor.getString(cursor.getColumnIndex("key"))));
                cmVar.b(bq.b(cursor.getString(cursor.getColumnIndex("label"))));
                cmVar.c((long) cursor.getInt(cursor.getColumnIndex(WBPageConstants.ParamKey.COUNT)));
                cmVar.b((long) cursor.getInt(cursor.getColumnIndex("value")));
                cmVar.b(cursor.getString(cursor.getColumnIndex("timeWindowNum")));
                cmVar.a(Long.parseLong(cursor.getString(cursor.getColumnIndex("totalTimestamp"))));
                hashMap.put(bq.b(cursor.getString(cursor.getColumnIndex("key"))), cmVar);
            }
            if (hashMap.size() > 0) {
                ckVar.a(hashMap, false);
            } else {
                ckVar.a("faild", false);
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (SQLException e) {
            ckVar.a(false, false);
            aw.c("cacheToMemory happen " + e.toString());
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return false;
    }

    public static void a(SQLiteDatabase sQLiteDatabase, boolean z, ck ckVar) {
        a(sQLiteDatabase, "system");
        a(sQLiteDatabase, "aggregated");
        if (!z) {
            a(sQLiteDatabase, "limitedck");
            ckVar.a("success", false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void a(SQLiteDatabase sQLiteDatabase, String str, long j, long j2) {
        try {
            int b = b(sQLiteDatabase, "system");
            int c = cp.a().c();
            if (b < c) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("key", str);
                contentValues.put("timeStamp", Long.valueOf(j2));
                contentValues.put(WBPageConstants.ParamKey.COUNT, Long.valueOf(j));
                sQLiteDatabase.insert("system", null, contentValues);
            } else if (b == c) {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("key", "__meta_ve_of");
                contentValues2.put("timeStamp", Long.valueOf(System.currentTimeMillis()));
                contentValues2.put(WBPageConstants.ParamKey.COUNT, (Integer) 1);
                sQLiteDatabase.insert("system", null, contentValues2);
            } else {
                c(sQLiteDatabase, "__meta_ve_of");
            }
        } catch (SQLException e) {
        }
    }

    private static void c(SQLiteDatabase sQLiteDatabase, String str) {
        try {
            sQLiteDatabase.beginTransaction();
            sQLiteDatabase.execSQL("update system set count=count+1 where key like '" + str + "'");
            sQLiteDatabase.setTransactionSuccessful();
            if (sQLiteDatabase != null) {
                sQLiteDatabase.endTransaction();
            }
        } catch (SQLException e) {
            if (sQLiteDatabase != null) {
                sQLiteDatabase.endTransaction();
            }
        } catch (Throwable th) {
            if (sQLiteDatabase != null) {
                sQLiteDatabase.endTransaction();
            }
            throw th;
        }
    }

    public static void a(ck ckVar, SQLiteDatabase sQLiteDatabase, List<String> list) {
        try {
            sQLiteDatabase.beginTransaction();
            if (b(sQLiteDatabase, "limitedck") > 0) {
                a(sQLiteDatabase, "limitedck");
            }
            for (String put : list) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("ck", put);
                sQLiteDatabase.insert("limitedck", null, contentValues);
            }
            sQLiteDatabase.setTransactionSuccessful();
            ckVar.a("success", false);
        } catch (SQLException e) {
            aw.c("insertToLimitCKTable error " + e.toString());
        } finally {
            sQLiteDatabase.endTransaction();
        }
    }

    public static void a(SQLiteDatabase sQLiteDatabase, Map<String, cn> map, ck ckVar) {
        int i = 0;
        Cursor cursor = null;
        try {
            cn cnVar = map.get("__ag_of");
            if (cnVar != null) {
                cursor = sQLiteDatabase.rawQuery("select * from " + "system where key=\"__ag_of\"", null);
                cursor.moveToFirst();
                long j = 0;
                while (!cursor.isAfterLast()) {
                    if (cursor.getCount() > 0) {
                        i = cursor.getInt(cursor.getColumnIndex(WBPageConstants.ParamKey.COUNT));
                        j = cursor.getLong(cursor.getColumnIndex("timeStamp"));
                        sQLiteDatabase.execSQL("delete from " + "system where key=\"__ag_of\"");
                    }
                    cursor.moveToNext();
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("key", cnVar.a());
                contentValues.put(WBPageConstants.ParamKey.COUNT, Long.valueOf(i == 0 ? cnVar.c() : ((long) i) + cnVar.c()));
                if (j == 0) {
                    j = cnVar.b();
                }
                contentValues.put("timeStamp", Long.valueOf(j));
                sQLiteDatabase.insert("system", null, contentValues);
                ckVar.a("success", false);
                if (cursor != null) {
                    cursor.close();
                }
            } else if (cursor != null) {
                cursor.close();
            }
        } catch (SQLException e) {
            aw.c("save to system table error " + e.toString());
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v14, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v20, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v21, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v22, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v23, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v24, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0069  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.database.sqlite.SQLiteDatabase r6) {
        /*
            r1 = 0
            r6.beginTransaction()     // Catch:{ SQLException -> 0x003d, all -> 0x0065 }
            java.lang.String r0 = "aggregated_cache"
            int r0 = b(r6, r0)     // Catch:{ SQLException -> 0x003d, all -> 0x0065 }
            if (r0 > 0) goto L_0x001a
            r0 = 0
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ SQLException -> 0x003d, all -> 0x0065 }
            if (r1 == 0) goto L_0x0016
            r1.close()
        L_0x0016:
            r6.endTransaction()
        L_0x0019:
            return r0
        L_0x001a:
            java.lang.String r0 = "select * from aggregated_cache"
            r2 = 0
            android.database.Cursor r2 = r6.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x003d, all -> 0x0065 }
            boolean r0 = r2.moveToLast()     // Catch:{ SQLException -> 0x0072 }
            if (r0 == 0) goto L_0x0079
            java.lang.String r0 = "timeWindowNum"
            int r0 = r2.getColumnIndex(r0)     // Catch:{ SQLException -> 0x0072 }
            java.lang.String r0 = r2.getString(r0)     // Catch:{ SQLException -> 0x0072 }
        L_0x0031:
            r6.setTransactionSuccessful()     // Catch:{ SQLException -> 0x0077 }
            if (r2 == 0) goto L_0x0039
            r2.close()
        L_0x0039:
            r6.endTransaction()
            goto L_0x0019
        L_0x003d:
            r0 = move-exception
            r2 = r1
            r5 = r0
            r0 = r1
            r1 = r5
        L_0x0042:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0070 }
            r3.<init>()     // Catch:{ all -> 0x0070 }
            java.lang.String r4 = "queryLastTimeWindowNumFromCache error "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0070 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0070 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x0070 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0070 }
            com.umeng.a.a.aw.c(r1)     // Catch:{ all -> 0x0070 }
            if (r2 == 0) goto L_0x0061
            r2.close()
        L_0x0061:
            r6.endTransaction()
            goto L_0x0019
        L_0x0065:
            r0 = move-exception
            r2 = r1
        L_0x0067:
            if (r2 == 0) goto L_0x006c
            r2.close()
        L_0x006c:
            r6.endTransaction()
            throw r0
        L_0x0070:
            r0 = move-exception
            goto L_0x0067
        L_0x0072:
            r0 = move-exception
            r5 = r0
            r0 = r1
            r1 = r5
            goto L_0x0042
        L_0x0077:
            r1 = move-exception
            goto L_0x0042
        L_0x0079:
            r0 = r1
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.a.a(android.database.sqlite.SQLiteDatabase):java.lang.String");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: org.json.JSONObject} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONObject b(android.database.sqlite.SQLiteDatabase r10) {
        /*
            r1 = 0
            java.lang.String r0 = "aggregated"
            int r0 = b(r10, r0)     // Catch:{ SQLException -> 0x00d1, all -> 0x00c7 }
            if (r0 <= 0) goto L_0x00c1
            java.lang.String r0 = "select * from aggregated"
            r2 = 0
            android.database.Cursor r2 = r10.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x00d1, all -> 0x00c7 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ SQLException -> 0x0099 }
            r0.<init>()     // Catch:{ SQLException -> 0x0099 }
        L_0x0015:
            boolean r3 = r2.moveToNext()     // Catch:{ SQLException -> 0x0099 }
            if (r3 == 0) goto L_0x00bb
            java.lang.String r3 = "key"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r4 = r2.getString(r3)     // Catch:{ Exception -> 0x0091 }
            boolean r3 = r0.has(r4)     // Catch:{ Exception -> 0x0091 }
            if (r3 == 0) goto L_0x0093
            org.json.JSONArray r3 = r0.getJSONArray(r4)     // Catch:{ Exception -> 0x0091 }
            r0.remove(r4)     // Catch:{ Exception -> 0x0091 }
        L_0x0032:
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x0091 }
            r5.<init>()     // Catch:{ Exception -> 0x0091 }
            java.lang.String r6 = "v_sum"
            java.lang.String r7 = "value"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x0091 }
            long r8 = r2.getLong(r7)     // Catch:{ Exception -> 0x0091 }
            r5.put(r6, r8)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r6 = "ts_sum"
            java.lang.String r7 = "totalTimestamp"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x0091 }
            long r8 = r2.getLong(r7)     // Catch:{ Exception -> 0x0091 }
            r5.put(r6, r8)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r6 = "tw_num"
            java.lang.String r7 = "timeWindowNum"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x0091 }
            int r7 = java.lang.Integer.parseInt(r7)     // Catch:{ Exception -> 0x0091 }
            r5.put(r6, r7)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r6 = "count"
            java.lang.String r7 = "count"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x0091 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x0091 }
            r5.put(r6, r7)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r6 = "labels"
            java.lang.String r7 = "label"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x0091 }
            org.json.JSONArray r7 = com.umeng.a.a.bq.a(r7)     // Catch:{ Exception -> 0x0091 }
            r5.put(r6, r7)     // Catch:{ Exception -> 0x0091 }
            r3.put(r5)     // Catch:{ Exception -> 0x0091 }
            r0.put(r4, r3)     // Catch:{ Exception -> 0x0091 }
            goto L_0x0015
        L_0x0091:
            r3 = move-exception
            goto L_0x0015
        L_0x0093:
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ Exception -> 0x0091 }
            r3.<init>()     // Catch:{ Exception -> 0x0091 }
            goto L_0x0032
        L_0x0099:
            r0 = move-exception
        L_0x009a:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00cf }
            r3.<init>()     // Catch:{ all -> 0x00cf }
            java.lang.String r4 = "readAllAggregatedDataForUpload error "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00cf }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00cf }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00cf }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00cf }
            com.umeng.a.a.aw.c(r0)     // Catch:{ all -> 0x00cf }
            if (r2 == 0) goto L_0x00b9
            r2.close()
        L_0x00b9:
            r0 = r1
        L_0x00ba:
            return r0
        L_0x00bb:
            if (r2 == 0) goto L_0x00ba
            r2.close()
            goto L_0x00ba
        L_0x00c1:
            if (r1 == 0) goto L_0x00b9
            r1.close()
            goto L_0x00b9
        L_0x00c7:
            r0 = move-exception
            r2 = r1
        L_0x00c9:
            if (r2 == 0) goto L_0x00ce
            r2.close()
        L_0x00ce:
            throw r0
        L_0x00cf:
            r0 = move-exception
            goto L_0x00c9
        L_0x00d1:
            r0 = move-exception
            r2 = r1
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.a.b(android.database.sqlite.SQLiteDatabase):org.json.JSONObject");
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x009a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONObject a(com.umeng.a.a.ck r11, android.database.sqlite.SQLiteDatabase r12) {
        /*
            r1 = 0
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ SQLException -> 0x00a4, all -> 0x0096 }
            r2.<init>()     // Catch:{ SQLException -> 0x00a4, all -> 0x0096 }
            java.lang.String r0 = "system"
            int r0 = b(r12, r0)     // Catch:{ SQLException -> 0x00a4, all -> 0x0096 }
            if (r0 <= 0) goto L_0x008e
            java.lang.String r0 = "select * from system"
            r3 = 0
            android.database.Cursor r0 = r12.rawQuery(r0, r3)     // Catch:{ SQLException -> 0x00a4, all -> 0x0096 }
        L_0x0015:
            boolean r3 = r0.moveToNext()     // Catch:{ SQLException -> 0x0064, all -> 0x009e }
            if (r3 == 0) goto L_0x008f
            java.lang.String r3 = "key"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Exception -> 0x005c }
            java.lang.String r4 = r0.getString(r3)     // Catch:{ Exception -> 0x005c }
            boolean r3 = r2.has(r4)     // Catch:{ Exception -> 0x005c }
            if (r3 == 0) goto L_0x005e
            org.json.JSONArray r3 = r2.getJSONArray(r4)     // Catch:{ Exception -> 0x005c }
            r2.remove(r4)     // Catch:{ Exception -> 0x005c }
        L_0x0032:
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x005c }
            r5.<init>()     // Catch:{ Exception -> 0x005c }
            java.lang.String r6 = "value"
            java.lang.String r7 = "count"
            int r7 = r0.getColumnIndex(r7)     // Catch:{ Exception -> 0x005c }
            int r7 = r0.getInt(r7)     // Catch:{ Exception -> 0x005c }
            r5.put(r6, r7)     // Catch:{ Exception -> 0x005c }
            java.lang.String r6 = "ts"
            java.lang.String r7 = "timeStamp"
            int r7 = r0.getColumnIndex(r7)     // Catch:{ Exception -> 0x005c }
            long r8 = r0.getLong(r7)     // Catch:{ Exception -> 0x005c }
            r5.put(r6, r8)     // Catch:{ Exception -> 0x005c }
            r3.put(r5)     // Catch:{ Exception -> 0x005c }
            r2.put(r4, r3)     // Catch:{ Exception -> 0x005c }
            goto L_0x0015
        L_0x005c:
            r3 = move-exception
            goto L_0x0015
        L_0x005e:
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ Exception -> 0x005c }
            r3.<init>()     // Catch:{ Exception -> 0x005c }
            goto L_0x0032
        L_0x0064:
            r2 = move-exception
            r10 = r2
            r2 = r0
            r0 = r10
        L_0x0068:
            java.lang.String r3 = "faild"
            r4 = 0
            r11.a(r3, r4)     // Catch:{ all -> 0x00a2 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a2 }
            r3.<init>()     // Catch:{ all -> 0x00a2 }
            java.lang.String r4 = "readAllSystemDataForUpload error "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00a2 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a2 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x00a2 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00a2 }
            com.umeng.a.a.aw.c(r0)     // Catch:{ all -> 0x00a2 }
            if (r2 == 0) goto L_0x008d
            r2.close()
        L_0x008d:
            return r1
        L_0x008e:
            r0 = r1
        L_0x008f:
            if (r0 == 0) goto L_0x0094
            r0.close()
        L_0x0094:
            r1 = r2
            goto L_0x008d
        L_0x0096:
            r0 = move-exception
            r2 = r1
        L_0x0098:
            if (r2 == 0) goto L_0x009d
            r2.close()
        L_0x009d:
            throw r0
        L_0x009e:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0098
        L_0x00a2:
            r0 = move-exception
            goto L_0x0098
        L_0x00a4:
            r0 = move-exception
            r2 = r1
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.a.a(com.umeng.a.a.ck, android.database.sqlite.SQLiteDatabase):org.json.JSONObject");
    }

    /* JADX WARN: Type inference failed for: r0v4, types: [java.util.List<java.lang.String>] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<java.lang.String> c(android.database.sqlite.SQLiteDatabase r5) {
        /*
            r1 = 0
            java.lang.String r0 = "limitedck"
            int r0 = b(r5, r0)     // Catch:{ SQLException -> 0x0061, all -> 0x0057 }
            if (r0 <= 0) goto L_0x0051
            java.lang.String r0 = "select * from limitedck"
            r2 = 0
            android.database.Cursor r2 = r5.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x0061, all -> 0x0057 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ SQLException -> 0x0029 }
            r0.<init>()     // Catch:{ SQLException -> 0x0029 }
        L_0x0015:
            boolean r3 = r2.moveToNext()     // Catch:{ SQLException -> 0x0029 }
            if (r3 == 0) goto L_0x004b
            java.lang.String r3 = "ck"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ SQLException -> 0x0029 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ SQLException -> 0x0029 }
            r0.add(r3)     // Catch:{ SQLException -> 0x0029 }
            goto L_0x0015
        L_0x0029:
            r0 = move-exception
        L_0x002a:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x005f }
            r3.<init>()     // Catch:{ all -> 0x005f }
            java.lang.String r4 = "loadLimitCKFromDB error "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x005f }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x005f }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x005f }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x005f }
            com.umeng.a.a.aw.c(r0)     // Catch:{ all -> 0x005f }
            if (r2 == 0) goto L_0x0049
            r2.close()
        L_0x0049:
            r0 = r1
        L_0x004a:
            return r0
        L_0x004b:
            if (r2 == 0) goto L_0x004a
            r2.close()
            goto L_0x004a
        L_0x0051:
            if (r1 == 0) goto L_0x0049
            r1.close()
            goto L_0x0049
        L_0x0057:
            r0 = move-exception
            r2 = r1
        L_0x0059:
            if (r2 == 0) goto L_0x005e
            r2.close()
        L_0x005e:
            throw r0
        L_0x005f:
            r0 = move-exception
            goto L_0x0059
        L_0x0061:
            r0 = move-exception
            r2 = r1
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.a.c(android.database.sqlite.SQLiteDatabase):java.util.List");
    }
}
