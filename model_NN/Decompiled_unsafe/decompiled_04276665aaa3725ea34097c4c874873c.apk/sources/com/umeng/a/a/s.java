package com.umeng.a.a;

import com.umeng.a.a;
import java.lang.Thread;

/* compiled from: CrashHandler */
public class s implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private Thread.UncaughtExceptionHandler f2745a;
    private x b;

    public s() {
        if (Thread.getDefaultUncaughtExceptionHandler() != this) {
            this.f2745a = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
    }

    public void a(x xVar) {
        this.b = xVar;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        a(th);
        if (this.f2745a != null && this.f2745a != Thread.getDefaultUncaughtExceptionHandler()) {
            this.f2745a.uncaughtException(thread, th);
        }
    }

    private void a(Throwable th) {
        if (a.f) {
            this.b.a(th);
        } else {
            this.b.a(null);
        }
    }
}
