package com.umeng.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.umeng.a.a;
import com.umeng.a.a.cx;
import java.lang.reflect.Method;
import org.json.JSONObject;

/* compiled from: SessionTracker */
public class ad {
    private static String c = null;
    private static Context d = null;

    /* renamed from: a  reason: collision with root package name */
    private final String f2626a = "a_start_time";
    private final String b = "a_end_time";

    public boolean a(Context context) {
        SharedPreferences a2 = aa.a(context);
        String string = a2.getString("session_id", null);
        if (string == null) {
            return false;
        }
        long j = a2.getLong("session_start_time", 0);
        long j2 = a2.getLong("session_end_time", 0);
        if (j2 == 0 || Math.abs(j2 - j) > LogBuilder.MAX_INTERVAL) {
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("__ii", string);
            jSONObject.put("__e", j);
            jSONObject.put("__f", j2);
            double[] a3 = a.a();
            if (a3 != null) {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("lat", a3[0]);
                jSONObject2.put("lng", a3[1]);
                jSONObject2.put("ts", System.currentTimeMillis());
                jSONObject.put("__d", jSONObject2);
            }
            Class<?> cls = Class.forName("android.net.TrafficStats");
            Method method = cls.getMethod("getUidRxBytes", Integer.TYPE);
            Method method2 = cls.getMethod("getUidTxBytes", Integer.TYPE);
            int i = context.getApplicationInfo().uid;
            if (i == -1) {
                return false;
            }
            long longValue = ((Long) method.invoke(null, Integer.valueOf(i))).longValue();
            long longValue2 = ((Long) method2.invoke(null, Integer.valueOf(i))).longValue();
            if (longValue > 0 && longValue2 > 0) {
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put("download_traffic", longValue);
                jSONObject3.put("upload_traffic", longValue2);
                jSONObject.put("__c", jSONObject3);
            }
            cx.a(context).a(string, jSONObject, cx.a.NEWSESSION);
            af.a(d);
            p.b(d);
            a(a2);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    private void a(SharedPreferences sharedPreferences) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.remove("session_start_time");
        edit.remove("session_end_time");
        edit.remove("a_start_time");
        edit.remove("a_end_time");
        edit.commit();
    }

    public String b(Context context) {
        String c2 = at.c(context);
        String a2 = a.a(context);
        long currentTimeMillis = System.currentTimeMillis();
        if (a2 == null) {
            throw new RuntimeException("Appkey is null or empty, Please check AndroidManifest.xml");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(currentTimeMillis).append(a2).append(c2);
        c = au.a(sb.toString());
        return c;
    }

    public void c(Context context) {
        d = context;
        SharedPreferences a2 = aa.a(context);
        if (a2 != null) {
            SharedPreferences.Editor edit = a2.edit();
            int i = a2.getInt("versioncode", 0);
            int parseInt = Integer.parseInt(at.a(d));
            if (i != 0 && parseInt != i) {
                try {
                    edit.putInt("vers_code", i);
                    edit.putString("vers_name", a2.getString("versionname", ""));
                    edit.commit();
                } catch (Throwable th) {
                }
                if (g(context) == null) {
                    a(context, a2);
                }
                e(d);
                r.b(d).b();
                f(d);
                r.b(d).a();
            } else if (b(a2)) {
                aw.b("Start new session: " + a(context, a2));
            } else {
                String string = a2.getString("session_id", null);
                edit.putLong("a_start_time", System.currentTimeMillis());
                edit.putLong("a_end_time", 0);
                edit.commit();
                aw.b("Extend current session: " + string);
            }
        }
    }

    public void d(Context context) {
        SharedPreferences a2 = aa.a(context);
        if (a2 != null) {
            if (a2.getLong("a_start_time", 0) != 0 || !a.e) {
                long currentTimeMillis = System.currentTimeMillis();
                SharedPreferences.Editor edit = a2.edit();
                edit.putLong("a_start_time", 0);
                edit.putLong("a_end_time", currentTimeMillis);
                edit.putLong("session_end_time", currentTimeMillis);
                edit.commit();
                return;
            }
            aw.c("onPause called before onResume");
        }
    }

    private boolean b(SharedPreferences sharedPreferences) {
        long j = sharedPreferences.getLong("a_start_time", 0);
        long j2 = sharedPreferences.getLong("a_end_time", 0);
        long currentTimeMillis = System.currentTimeMillis();
        if (j != 0 && currentTimeMillis - j < a.g) {
            aw.c("onResume called before onPause");
            return false;
        } else if (currentTimeMillis - j2 <= a.g) {
            return false;
        } else {
            String a2 = a();
            if (!TextUtils.isEmpty(a2)) {
                long j3 = sharedPreferences.getLong("session_end_time", 0);
                if (j3 == 0) {
                    j3 = System.currentTimeMillis();
                }
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("__f", j3);
                    cx.a(d).a(a2, jSONObject, cx.a.END);
                } catch (Throwable th) {
                }
            }
            return true;
        }
    }

    private String a(Context context, SharedPreferences sharedPreferences) {
        r b2 = r.b(context);
        String b3 = b(context);
        long currentTimeMillis = System.currentTimeMillis();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("__e", currentTimeMillis);
            cx.a(d).a(b3, jSONObject, cx.a.BEGIN);
        } catch (Throwable th) {
        }
        a(context);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("session_id", b3);
        edit.putLong("session_start_time", System.currentTimeMillis());
        edit.putLong("session_end_time", 0);
        edit.putLong("a_start_time", currentTimeMillis);
        edit.putLong("a_end_time", 0);
        edit.putInt("versioncode", Integer.parseInt(at.a(context)));
        edit.putString("versionname", at.b(context));
        edit.commit();
        b2.a((Object) true);
        return b3;
    }

    public boolean e(Context context) {
        boolean z = false;
        SharedPreferences a2 = aa.a(context);
        if (!(a2 == null || a2.getString("session_id", null) == null)) {
            long j = a2.getLong("a_start_time", 0);
            long j2 = a2.getLong("a_end_time", 0);
            if (j > 0 && j2 == 0) {
                z = true;
                d(context);
            }
            long j3 = a2.getLong("session_end_time", 0);
            try {
                JSONObject jSONObject = new JSONObject();
                if (j3 == 0) {
                    j3 = System.currentTimeMillis();
                }
                jSONObject.put("__f", j3);
                cx.a(d).a(a(), jSONObject, cx.a.END);
            } catch (Throwable th) {
            }
            a(context);
        }
        return z;
    }

    public void f(Context context) {
        SharedPreferences a2 = aa.a(context);
        if (a2 != null) {
            String b2 = b(context);
            SharedPreferences.Editor edit = a2.edit();
            long currentTimeMillis = System.currentTimeMillis();
            edit.putString("session_id", b2);
            edit.putLong("session_start_time", System.currentTimeMillis());
            edit.putLong("session_end_time", 0);
            edit.putLong("a_start_time", currentTimeMillis);
            edit.putLong("a_end_time", 0);
            edit.putInt("versioncode", Integer.parseInt(at.a(context)));
            edit.putString("versionname", at.b(context));
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("__e", currentTimeMillis);
                cx.a(d).a(b2, jSONObject, cx.a.BEGIN);
            } catch (Throwable th) {
            }
            edit.commit();
        }
    }

    public static String g(Context context) {
        if (c == null) {
            c = aa.a(context).getString("session_id", null);
        }
        return c;
    }

    public static String a() {
        return g(d);
    }
}
