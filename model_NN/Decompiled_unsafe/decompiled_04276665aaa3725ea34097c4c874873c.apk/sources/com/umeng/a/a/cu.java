package com.umeng.a.a;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: UMDBCreater */
class cu extends SQLiteOpenHelper {
    /* access modifiers changed from: private */
    public static Context b = null;

    /* renamed from: a  reason: collision with root package name */
    private String f2714a;

    /* compiled from: UMDBCreater */
    private static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final cu f2715a = new cu(cu.b, cw.a(cu.b), "ua.db", null, 1);
    }

    public static synchronized cu a(Context context) {
        cu a2;
        synchronized (cu.class) {
            b = context;
            a2 = a.f2715a;
        }
        return a2;
    }

    private cu(Context context, String str, String str2, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        this(new ct(context, str), str2, cursorFactory, i);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private cu(android.content.Context r2, java.lang.String r3, android.database.sqlite.SQLiteDatabase.CursorFactory r4, int r5) {
        /*
            r1 = this;
            if (r3 == 0) goto L_0x000a
            java.lang.String r0 = ""
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x000c
        L_0x000a:
            java.lang.String r3 = "ua.db"
        L_0x000c:
            r1.<init>(r2, r3, r4, r5)
            r0 = 0
            r1.f2714a = r0
            r1.b()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.cu.<init>(android.content.Context, java.lang.String, android.database.sqlite.SQLiteDatabase$CursorFactory, int):void");
    }

    private void b() {
        try {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (!cw.a("__sd", writableDatabase)) {
                c(writableDatabase);
            }
            if (!cw.a("__et", writableDatabase)) {
                b(writableDatabase);
            }
            if (!cw.a("__er", writableDatabase)) {
                a(writableDatabase);
            }
        } catch (Exception e) {
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.beginTransaction();
            c(sQLiteDatabase);
            b(sQLiteDatabase);
            a(sQLiteDatabase);
            sQLiteDatabase.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sQLiteDatabase.endTransaction();
        }
    }

    private void a(SQLiteDatabase sQLiteDatabase) {
        try {
            this.f2714a = "create table if not exists __er(id INTEGER primary key autoincrement, __i TEXT, __a TEXT, __t INTEGER)";
            sQLiteDatabase.execSQL(this.f2714a);
        } catch (SQLException e) {
        }
    }

    private void b(SQLiteDatabase sQLiteDatabase) {
        try {
            this.f2714a = "create table if not exists __et(id INTEGER primary key autoincrement, __i TEXT, __e TEXT, __s TEXT, __t INTEGER)";
            sQLiteDatabase.execSQL(this.f2714a);
        } catch (SQLException e) {
        }
    }

    private void c(SQLiteDatabase sQLiteDatabase) {
        try {
            this.f2714a = "create table if not exists __sd(id INTEGER primary key autoincrement, __ii TEXT unique, __a TEXT, __b TEXT, __c TEXT, __d TEXT, __e TEXT, __f TEXT, __g TEXT)";
            sQLiteDatabase.execSQL(this.f2714a);
        } catch (SQLException e) {
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }
}
