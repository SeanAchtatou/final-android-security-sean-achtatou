package com.umeng.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.tencent.stat.DeviceInfo;
import com.umeng.a.a.az;
import com.umeng.a.a.g;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: CacheImpl */
public final class q implements u, y {
    /* access modifiers changed from: private */
    public static Context j;

    /* renamed from: a  reason: collision with root package name */
    String f2738a = null;
    private bb b = null;
    /* access modifiers changed from: private */
    public ae c = null;
    /* access modifiers changed from: private */
    public ah d = null;
    /* access modifiers changed from: private */
    public ag e = null;
    /* access modifiers changed from: private */
    public ai f = null;
    private a g = null;
    /* access modifiers changed from: private */
    public g.a h = null;
    private long i = 0;
    private int k = 10;
    private JSONArray l = new JSONArray();
    private final int m = 5000;
    private int n = 0;
    private int o = 0;
    private long p = 0;
    private final long q = 28800000;

    public q(Context context) {
        j = context;
        this.c = new ae(context);
        this.b = bb.a(context);
        this.h = g.a(context).b();
        this.g = new a();
        this.e = ag.a(j);
        this.d = ah.a(j);
        this.f = ai.a(j, this.c);
        SharedPreferences a2 = aa.a(j);
        this.p = a2.getLong("thtstart", 0);
        this.n = a2.getInt("gkvc", 0);
        this.o = a2.getInt("ekvc", 0);
        this.f2738a = g.a(j).b().a((String) null);
    }

    public void a() {
        if (at.j(j)) {
            d();
        } else {
            aw.a("network is unavailable");
        }
    }

    public void a(Object obj) {
        if (this.c.e()) {
            this.i = this.c.k();
        }
        boolean z = true;
        if (obj instanceof JSONObject) {
            z = false;
            try {
                b((JSONObject) obj);
            } catch (Throwable th) {
            }
        }
        if (a(z)) {
            d();
        }
    }

    private void b(JSONObject jSONObject) {
        try {
            if (2050 == jSONObject.getInt("__t")) {
                if (c(this.n)) {
                    this.n++;
                } else {
                    return;
                }
            } else if (2049 == jSONObject.getInt("__t")) {
                if (c(this.o)) {
                    this.o++;
                } else {
                    return;
                }
            }
            if (this.l.length() > this.k) {
                cx.a(j).a(this.l);
                this.l = new JSONArray();
            }
            if (this.p == 0) {
                this.p = System.currentTimeMillis();
            }
            this.l.put(jSONObject);
        } catch (Throwable th) {
        }
    }

    public void b() {
        c(a(new int[0]));
    }

    private void a(int i2) {
        c(a(i2, (int) (System.currentTimeMillis() - this.c.l())));
        ax.a(new ba() {
            public void a() {
                q.this.a();
            }
        }, (long) i2);
    }

    private void c(JSONObject jSONObject) {
        b a2;
        if (jSONObject != null) {
            try {
                e a3 = e.a(j);
                a3.a();
                try {
                    String encodeToString = Base64.encodeToString(new bi().a(a3.b()), 0);
                    if (!TextUtils.isEmpty(encodeToString)) {
                        JSONObject jSONObject2 = jSONObject.getJSONObject("header");
                        jSONObject2.put("id_tracking", encodeToString);
                        jSONObject.put("header", jSONObject2);
                    }
                } catch (Exception e2) {
                }
                byte[] bytes = String.valueOf(jSONObject).getBytes();
                if (bytes != null && !ar.a(j, bytes)) {
                    if (e()) {
                        a2 = b.b(j, com.umeng.a.a.a(j), bytes);
                    } else {
                        a2 = b.a(j, com.umeng.a.a.a(j), bytes);
                    }
                    byte[] c2 = a2.c();
                    bb a4 = bb.a(j);
                    a4.e();
                    a4.a(c2);
                    a3.c();
                }
            } catch (Exception e3) {
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONObject a(int... r11) {
        /*
            r10 = this;
            r1 = 0
            android.content.Context r0 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r0 = com.umeng.a.a.a(r0)     // Catch:{ Throwable -> 0x0429 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x0429 }
            if (r0 == 0) goto L_0x0014
            java.lang.String r0 = "Appkey is missing ,Please check AndroidManifest.xml"
            com.umeng.a.a.aw.c(r0)     // Catch:{ Throwable -> 0x0429 }
            r0 = r1
        L_0x0013:
            return r0
        L_0x0014:
            android.content.Context r0 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            r10.a(r0)     // Catch:{ Throwable -> 0x0429 }
            android.content.Context r0 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            com.umeng.a.a.cx r0 = com.umeng.a.a.cx.a(r0)     // Catch:{ Throwable -> 0x0429 }
            org.json.JSONObject r0 = r0.a()     // Catch:{ Throwable -> 0x0429 }
            if (r0 != 0) goto L_0x002a
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0429 }
            r0.<init>()     // Catch:{ Throwable -> 0x0429 }
        L_0x002a:
            java.lang.String r2 = "body"
            org.json.JSONObject r2 = r0.getJSONObject(r2)     // Catch:{ Throwable -> 0x03dd }
            r3 = r2
        L_0x0031:
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r2 = r3.toString()     // Catch:{ Throwable -> 0x0429 }
            r4.<init>(r2)     // Catch:{ Throwable -> 0x0429 }
            android.content.Context r2 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            android.content.SharedPreferences r2 = com.umeng.a.a.aa.a(r2)     // Catch:{ Throwable -> 0x0429 }
            if (r2 == 0) goto L_0x0055
            java.lang.String r5 = "userlevel"
            java.lang.String r6 = ""
            java.lang.String r5 = r2.getString(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Throwable -> 0x0429 }
            if (r6 != 0) goto L_0x0055
            java.lang.String r6 = "userlevel"
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
        L_0x0055:
            com.umeng.a.a.ae r5 = r10.c     // Catch:{ Throwable -> 0x0429 }
            boolean r5 = r5.e()     // Catch:{ Throwable -> 0x0429 }
            if (r5 == 0) goto L_0x007b
            long r6 = r10.i     // Catch:{ Throwable -> 0x0429 }
            r8 = 0
            int r5 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r5 == 0) goto L_0x007b
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0429 }
            r5.<init>()     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "ts"
            long r8 = r10.i     // Catch:{ Throwable -> 0x0429 }
            r5.put(r6, r8)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "activate_msg"
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "activate_msg"
            r4.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
        L_0x007b:
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0429 }
            r5.<init>()     // Catch:{ Throwable -> 0x0429 }
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            com.umeng.a.a.co r6 = com.umeng.a.a.co.a(r6)     // Catch:{ Throwable -> 0x0429 }
            org.json.JSONObject r6 = r6.a()     // Catch:{ Throwable -> 0x0429 }
            if (r6 == 0) goto L_0x0097
            int r7 = r6.length()     // Catch:{ Throwable -> 0x0429 }
            if (r7 <= 0) goto L_0x0097
            java.lang.String r7 = "ag"
            r5.put(r7, r6)     // Catch:{ Throwable -> 0x0429 }
        L_0x0097:
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            com.umeng.a.a.co r6 = com.umeng.a.a.co.a(r6)     // Catch:{ Throwable -> 0x0429 }
            org.json.JSONObject r6 = r6.b()     // Catch:{ Throwable -> 0x0429 }
            if (r6 == 0) goto L_0x00ae
            int r7 = r6.length()     // Catch:{ Throwable -> 0x0429 }
            if (r7 <= 0) goto L_0x00ae
            java.lang.String r7 = "ve_meta"
            r5.put(r7, r6)     // Catch:{ Throwable -> 0x0429 }
        L_0x00ae:
            int r6 = r5.length()     // Catch:{ Throwable -> 0x0429 }
            if (r6 <= 0) goto L_0x00be
            java.lang.String r6 = "cc"
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "cc"
            r4.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
        L_0x00be:
            android.content.Context r5 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String[] r5 = com.umeng.a.e.a(r5)     // Catch:{ Throwable -> 0x0429 }
            if (r5 == 0) goto L_0x00fd
            r6 = 0
            r6 = r5[r6]     // Catch:{ Throwable -> 0x0429 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Throwable -> 0x0429 }
            if (r6 != 0) goto L_0x00fd
            r6 = 1
            r6 = r5[r6]     // Catch:{ Throwable -> 0x0429 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Throwable -> 0x0429 }
            if (r6 != 0) goto L_0x00fd
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0429 }
            r6.<init>()     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r7 = "provider"
            r8 = 0
            r8 = r5[r8]     // Catch:{ Throwable -> 0x0429 }
            r6.put(r7, r8)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r7 = "puid"
            r8 = 1
            r5 = r5[r8]     // Catch:{ Throwable -> 0x0429 }
            r6.put(r7, r5)     // Catch:{ Throwable -> 0x0429 }
            int r5 = r6.length()     // Catch:{ Throwable -> 0x0429 }
            if (r5 <= 0) goto L_0x00fd
            java.lang.String r5 = "active_user"
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "active_user"
            r4.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
        L_0x00fd:
            android.content.Context r5 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            com.umeng.a.a.ag r5 = com.umeng.a.a.ag.a(r5)     // Catch:{ Throwable -> 0x0429 }
            boolean r5 = r5.a()     // Catch:{ Throwable -> 0x0429 }
            if (r5 == 0) goto L_0x010c
            r10.d(r3)     // Catch:{ Throwable -> 0x0429 }
        L_0x010c:
            com.umeng.a.a.ah r5 = r10.d     // Catch:{ Throwable -> 0x0429 }
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            r5.a(r3, r6)     // Catch:{ Throwable -> 0x0429 }
            if (r11 == 0) goto L_0x013f
            int r5 = r11.length     // Catch:{ Throwable -> 0x0429 }
            r6 = 2
            if (r5 != r6) goto L_0x013f
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0429 }
            r5.<init>()     // Catch:{ Throwable -> 0x0429 }
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0429 }
            r6.<init>()     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r7 = "interval"
            r8 = 0
            r8 = r11[r8]     // Catch:{ Throwable -> 0x0429 }
            int r8 = r8 / 1000
            r6.put(r7, r8)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r7 = "latency"
            r8 = 1
            r8 = r11[r8]     // Catch:{ Throwable -> 0x0429 }
            r6.put(r7, r8)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r7 = "latent"
            r5.put(r7, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "control_policy"
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
        L_0x013f:
            int r5 = r3.length()     // Catch:{ Throwable -> 0x0429 }
            if (r5 <= 0) goto L_0x03e6
            java.lang.String r5 = "body"
            r0.put(r5, r3)     // Catch:{ Throwable -> 0x0429 }
        L_0x014a:
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0429 }
            r3.<init>()     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "appkey"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.a(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "channel"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.b(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            android.content.Context r5 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = com.umeng.a.a.c(r5)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = com.umeng.a.a.au.a(r5)     // Catch:{ Throwable -> 0x0429 }
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Throwable -> 0x0429 }
            if (r6 != 0) goto L_0x017a
            java.lang.String r6 = "secret"
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
        L_0x017a:
            java.lang.String r5 = "display_name"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.at.t(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "package_name"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.at.q(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "app_signature"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.at.r(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            if (r2 != 0) goto L_0x01a3
            android.content.Context r5 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x040c }
            android.content.SharedPreferences r2 = com.umeng.a.a.aa.a(r5)     // Catch:{ Throwable -> 0x040c }
        L_0x01a3:
            if (r2 == 0) goto L_0x01c4
            java.lang.String r5 = "vers_name"
            java.lang.String r6 = ""
            java.lang.String r5 = r2.getString(r5, r6)     // Catch:{ Throwable -> 0x040c }
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Throwable -> 0x040c }
            if (r6 != 0) goto L_0x03f0
            java.lang.String r6 = "app_version"
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x040c }
            java.lang.String r5 = "version_code"
            java.lang.String r6 = "vers_code"
            r7 = 0
            int r6 = r2.getInt(r6, r7)     // Catch:{ Throwable -> 0x040c }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x040c }
        L_0x01c4:
            java.lang.String r5 = com.umeng.a.a.f2622a     // Catch:{ Throwable -> 0x0429 }
            if (r5 == 0) goto L_0x01da
            java.lang.String r5 = com.umeng.a.a.b     // Catch:{ Throwable -> 0x0429 }
            if (r5 == 0) goto L_0x01da
            java.lang.String r5 = "wrapper_type"
            java.lang.String r6 = com.umeng.a.a.f2622a     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "wrapper_version"
            java.lang.String r6 = com.umeng.a.a.b     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
        L_0x01da:
            java.lang.String r5 = "sdk_type"
            java.lang.String r6 = "Android"
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "sdk_version"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.e(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "vertical_type"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            int r6 = com.umeng.a.a.d(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "idmd5"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.at.d(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "cpu"
            java.lang.String r6 = com.umeng.a.a.at.a()     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "os"
            java.lang.String r6 = "Android"
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "os_version"
            java.lang.String r6 = android.os.Build.VERSION.RELEASE     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            android.content.Context r5 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            int[] r5 = com.umeng.a.a.at.o(r5)     // Catch:{ Throwable -> 0x0429 }
            if (r5 == 0) goto L_0x0243
            java.lang.String r6 = "resolution"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0429 }
            r7.<init>()     // Catch:{ Throwable -> 0x0429 }
            r8 = 1
            r8 = r5[r8]     // Catch:{ Throwable -> 0x0429 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r8 = "*"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x0429 }
            r8 = 0
            r5 = r5[r8]     // Catch:{ Throwable -> 0x0429 }
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x0429 }
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
        L_0x0243:
            java.lang.String r5 = "mc"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.at.n(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "device_id"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.at.c(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "device_model"
            java.lang.String r6 = android.os.Build.MODEL     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "device_board"
            java.lang.String r6 = android.os.Build.BOARD     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "device_brand"
            java.lang.String r6 = android.os.Build.BRAND     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "device_manutime"
            long r6 = android.os.Build.TIME     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "device_manufacturer"
            java.lang.String r6 = android.os.Build.MANUFACTURER     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "device_manuid"
            java.lang.String r6 = android.os.Build.ID     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "device_name"
            java.lang.String r6 = android.os.Build.DEVICE     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            android.content.Context r5 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = com.umeng.a.a.at.u(r5)     // Catch:{ Throwable -> 0x0429 }
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Throwable -> 0x0429 }
            if (r6 != 0) goto L_0x029b
            java.lang.String r6 = "sub_os_name"
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
        L_0x029b:
            android.content.Context r5 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = com.umeng.a.a.at.v(r5)     // Catch:{ Throwable -> 0x0429 }
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Throwable -> 0x0429 }
            if (r6 != 0) goto L_0x02ac
            java.lang.String r6 = "sub_os_version"
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
        L_0x02ac:
            android.content.Context r5 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String[] r5 = com.umeng.a.a.at.h(r5)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "Wi-Fi"
            r7 = 0
            r7 = r5[r7]     // Catch:{ Throwable -> 0x0429 }
            boolean r6 = r6.equals(r7)     // Catch:{ Throwable -> 0x0429 }
            if (r6 == 0) goto L_0x0436
            java.lang.String r6 = "access"
            java.lang.String r7 = "wifi"
            r3.put(r6, r7)     // Catch:{ Throwable -> 0x0429 }
        L_0x02c4:
            java.lang.String r6 = ""
            r7 = 1
            r7 = r5[r7]     // Catch:{ Throwable -> 0x0429 }
            boolean r6 = r6.equals(r7)     // Catch:{ Throwable -> 0x0429 }
            if (r6 != 0) goto L_0x02d7
            java.lang.String r6 = "access_subtype"
            r7 = 1
            r5 = r5[r7]     // Catch:{ Throwable -> 0x0429 }
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
        L_0x02d7:
            android.content.Context r5 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = com.umeng.a.a.at.e(r5)     // Catch:{ Throwable -> 0x0429 }
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Throwable -> 0x0429 }
            if (r6 != 0) goto L_0x0453
            java.lang.String r6 = "mccmnc"
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
        L_0x02e8:
            android.content.Context r5 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String[] r5 = com.umeng.a.a.at.l(r5)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "country"
            r7 = 0
            r7 = r5[r7]     // Catch:{ Throwable -> 0x0429 }
            r3.put(r6, r7)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "language"
            r7 = 1
            r5 = r5[r7]     // Catch:{ Throwable -> 0x0429 }
            r3.put(r6, r5)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "timezone"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            int r6 = com.umeng.a.a.at.k(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "carrier"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.at.g(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "successful_requests"
            java.lang.String r6 = "successful_request"
            r7 = 0
            int r6 = r2.getInt(r6, r7)     // Catch:{ Exception -> 0x045f }
            r3.put(r5, r6)     // Catch:{ Exception -> 0x045f }
            java.lang.String r5 = "failed_requests"
            java.lang.String r6 = "failed_requests"
            r7 = 0
            int r6 = r2.getInt(r6, r7)     // Catch:{ Exception -> 0x045f }
            r3.put(r5, r6)     // Catch:{ Exception -> 0x045f }
            java.lang.String r5 = "req_time"
            java.lang.String r6 = "last_request_spent_ms"
            r7 = 0
            int r6 = r2.getInt(r6, r7)     // Catch:{ Exception -> 0x045f }
            r3.put(r5, r6)     // Catch:{ Exception -> 0x045f }
        L_0x0338:
            android.content.Context r5 = com.umeng.a.a.q.j     // Catch:{ Exception -> 0x045c }
            com.umeng.a.a.g r5 = com.umeng.a.a.g.a(r5)     // Catch:{ Exception -> 0x045c }
            com.umeng.a.a.am r5 = r5.a()     // Catch:{ Exception -> 0x045c }
            if (r5 == 0) goto L_0x0357
            com.umeng.a.a.bi r6 = new com.umeng.a.a.bi     // Catch:{ Exception -> 0x045c }
            r6.<init>()     // Catch:{ Exception -> 0x045c }
            byte[] r5 = r6.a(r5)     // Catch:{ Exception -> 0x045c }
            java.lang.String r6 = "imprint"
            r7 = 0
            java.lang.String r5 = android.util.Base64.encodeToString(r5, r7)     // Catch:{ Exception -> 0x045c }
            r3.put(r6, r5)     // Catch:{ Exception -> 0x045c }
        L_0x0357:
            java.lang.String r5 = "header"
            r0.put(r5, r3)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "sdk_version"
            java.lang.String r6 = "sdk_version"
            java.lang.String r6 = r3.getString(r6)     // Catch:{ Throwable -> 0x0429 }
            org.json.JSONObject r5 = r4.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "device_id"
            java.lang.String r7 = "device_id"
            java.lang.String r7 = r3.getString(r7)     // Catch:{ Throwable -> 0x0429 }
            org.json.JSONObject r5 = r5.put(r6, r7)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "device_model"
            java.lang.String r7 = "device_model"
            java.lang.String r7 = r3.getString(r7)     // Catch:{ Throwable -> 0x0429 }
            org.json.JSONObject r5 = r5.put(r6, r7)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "version"
            java.lang.String r7 = "version_code"
            java.lang.String r7 = r3.getString(r7)     // Catch:{ Throwable -> 0x0429 }
            org.json.JSONObject r5 = r5.put(r6, r7)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "appkey"
            java.lang.String r7 = "appkey"
            java.lang.String r7 = r3.getString(r7)     // Catch:{ Throwable -> 0x0429 }
            org.json.JSONObject r5 = r5.put(r6, r7)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = "channel"
            java.lang.String r7 = "channel"
            java.lang.String r7 = r3.getString(r7)     // Catch:{ Throwable -> 0x0429 }
            r5.put(r6, r7)     // Catch:{ Throwable -> 0x0429 }
            boolean r3 = r10.a(r3)     // Catch:{ Throwable -> 0x0429 }
            if (r3 != 0) goto L_0x03aa
            r0 = r1
        L_0x03aa:
            boolean r3 = com.umeng.a.a.aw.f2653a     // Catch:{ Throwable -> 0x0429 }
            if (r3 == 0) goto L_0x03bb
            int r3 = r4.length()     // Catch:{ Throwable -> 0x0429 }
            if (r3 <= 0) goto L_0x03bb
            java.lang.String r3 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x0429 }
            com.umeng.a.a.aw.a(r3)     // Catch:{ Throwable -> 0x0429 }
        L_0x03bb:
            if (r2 == 0) goto L_0x0013
            android.content.SharedPreferences$Editor r1 = r2.edit()     // Catch:{ Throwable -> 0x03da }
            java.lang.String r2 = "vers_name"
            r1.remove(r2)     // Catch:{ Throwable -> 0x03da }
            java.lang.String r2 = "vers_code"
            r1.remove(r2)     // Catch:{ Throwable -> 0x03da }
            java.lang.String r2 = "vers_date"
            r1.remove(r2)     // Catch:{ Throwable -> 0x03da }
            java.lang.String r2 = "vers_pre_version"
            r1.remove(r2)     // Catch:{ Throwable -> 0x03da }
            r1.commit()     // Catch:{ Throwable -> 0x03da }
            goto L_0x0013
        L_0x03da:
            r1 = move-exception
            goto L_0x0013
        L_0x03dd:
            r2 = move-exception
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0429 }
            r2.<init>()     // Catch:{ Throwable -> 0x0429 }
            r3 = r2
            goto L_0x0031
        L_0x03e6:
            java.lang.String r3 = "body"
            r0.remove(r3)     // Catch:{ Throwable -> 0x03ed }
            goto L_0x014a
        L_0x03ed:
            r3 = move-exception
            goto L_0x014a
        L_0x03f0:
            java.lang.String r5 = "app_version"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x040c }
            java.lang.String r6 = com.umeng.a.a.at.b(r6)     // Catch:{ Throwable -> 0x040c }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x040c }
            java.lang.String r5 = "version_code"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x040c }
            java.lang.String r6 = com.umeng.a.a.at.a(r6)     // Catch:{ Throwable -> 0x040c }
            int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ Throwable -> 0x040c }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x040c }
            goto L_0x01c4
        L_0x040c:
            r5 = move-exception
            java.lang.String r5 = "app_version"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.at.b(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r5 = "version_code"
            android.content.Context r6 = com.umeng.a.a.q.j     // Catch:{ Throwable -> 0x0429 }
            java.lang.String r6 = com.umeng.a.a.at.a(r6)     // Catch:{ Throwable -> 0x0429 }
            int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ Throwable -> 0x0429 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            goto L_0x01c4
        L_0x0429:
            r0 = move-exception
            android.content.Context r0 = com.umeng.a.a.q.j
            com.umeng.a.a.bb r0 = com.umeng.a.a.bb.a(r0)
            r0.e()
            r0 = r1
            goto L_0x0013
        L_0x0436:
            java.lang.String r6 = "2G/3G"
            r7 = 0
            r7 = r5[r7]     // Catch:{ Throwable -> 0x0429 }
            boolean r6 = r6.equals(r7)     // Catch:{ Throwable -> 0x0429 }
            if (r6 == 0) goto L_0x044a
            java.lang.String r6 = "access"
            java.lang.String r7 = "2G/3G"
            r3.put(r6, r7)     // Catch:{ Throwable -> 0x0429 }
            goto L_0x02c4
        L_0x044a:
            java.lang.String r6 = "access"
            java.lang.String r7 = "unknow"
            r3.put(r6, r7)     // Catch:{ Throwable -> 0x0429 }
            goto L_0x02c4
        L_0x0453:
            java.lang.String r5 = "mccmnc"
            java.lang.String r6 = ""
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0429 }
            goto L_0x02e8
        L_0x045c:
            r5 = move-exception
            goto L_0x0357
        L_0x045f:
            r5 = move-exception
            goto L_0x0338
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.q.a(int[]):org.json.JSONObject");
    }

    private void d(JSONObject jSONObject) throws JSONException {
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put(ag.a(j).d(), ag.a(j).c());
        jSONObject.put("group_info", jSONObject2);
    }

    public boolean a(JSONObject jSONObject) {
        if (TextUtils.isEmpty(Parameters.DEVICE_ID) || TextUtils.isEmpty(DeviceInfo.TAG_MAC) || TextUtils.isEmpty("resolution") || TextUtils.isEmpty(LogBuilder.KEY_APPKEY) || TextUtils.isEmpty(LogBuilder.KEY_CHANNEL) || TextUtils.isEmpty("app_signature") || TextUtils.isEmpty(Parameters.PACKAGE_NAME) || TextUtils.isEmpty("app_version")) {
            return false;
        }
        return true;
    }

    private boolean a(boolean z) {
        if (!at.j(j)) {
            aw.c("network is unavailable");
            return false;
        } else if (this.c.e()) {
            return true;
        } else {
            return this.g.b(z).a(z);
        }
    }

    private void d() {
        try {
            if (this.b.f()) {
                ac acVar = new ac(j, this.c);
                acVar.a(this);
                if (this.d.c()) {
                    acVar.b(true);
                }
                acVar.a();
                return;
            }
            JSONObject a2 = a(new int[0]);
            if (a2.length() > 0) {
                ac acVar2 = new ac(j, this.c);
                acVar2.a(this);
                if (this.d.c()) {
                    acVar2.b(true);
                }
                acVar2.a(a2);
                acVar2.a(e());
                acVar2.a();
            }
        } catch (Throwable th) {
            if (th != null) {
                th.printStackTrace();
            }
        }
    }

    private boolean e() {
        switch (this.h.c(-1)) {
            case -1:
                return com.umeng.a.a.h;
            case 0:
            default:
                return false;
            case 1:
                return true;
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        a(i2);
    }

    public void a(g.a aVar) {
        this.e.a(aVar);
        this.d.a(aVar);
        this.f.a(aVar);
        this.g.a(aVar);
        this.f2738a = g.a(j).b().a((String) null);
    }

    /* compiled from: CacheImpl */
    public class a {
        private az.h b;
        private int c = -1;
        private int d = -1;
        private int e = -1;
        private int f = -1;

        public a() {
            int[] a2 = q.this.h.a(-1, -1);
            this.c = a2[0];
            this.d = a2[1];
        }

        /* access modifiers changed from: protected */
        public void a(boolean z) {
            az.h bVar;
            boolean z2 = true;
            int i = 0;
            if (q.this.d.c()) {
                if (!(this.b instanceof az.b) || !this.b.a()) {
                    z2 = false;
                }
                if (z2) {
                    bVar = this.b;
                } else {
                    bVar = new az.b(q.this.c, q.this.d);
                }
                this.b = bVar;
                return;
            }
            if (!(this.b instanceof az.c) || !this.b.a()) {
                z2 = false;
            }
            if (z2) {
                return;
            }
            if (z && q.this.f.a()) {
                this.b = new az.c((int) q.this.f.b());
                q.this.b((int) q.this.f.b());
            } else if (aw.f2653a && q.this.h.b()) {
                this.b = new az.a(q.this.c);
            } else if (!q.this.e.a() || !"RPT".equals(q.this.e.d())) {
                int i2 = this.e;
                int i3 = this.f;
                if (this.c != -1) {
                    i2 = this.c;
                    i3 = this.d;
                }
                this.b = a(i2, i3);
            } else {
                if (q.this.e.b() == 6) {
                    if (q.this.h.a()) {
                        i = q.this.h.d(90000);
                    } else if (this.d > 0) {
                        i = this.d;
                    } else {
                        i = this.f;
                    }
                }
                this.b = a(q.this.e.b(), i);
            }
        }

        public az.h b(boolean z) {
            a(z);
            return this.b;
        }

        private az.h a(int i, int i2) {
            switch (i) {
                case 0:
                    return this.b instanceof az.g ? this.b : new az.g();
                case 1:
                    return this.b instanceof az.d ? this.b : new az.d();
                case 2:
                case 3:
                case 7:
                default:
                    if (this.b instanceof az.d) {
                        return this.b;
                    }
                    return new az.d();
                case 4:
                    if (this.b instanceof az.f) {
                        return this.b;
                    }
                    return new az.f(q.this.c);
                case 5:
                    if (this.b instanceof az.i) {
                        return this.b;
                    }
                    return new az.i(q.j);
                case 6:
                    if (!(this.b instanceof az.e)) {
                        return new az.e(q.this.c, (long) i2);
                    }
                    az.h hVar = this.b;
                    ((az.e) hVar).a((long) i2);
                    return hVar;
                case 8:
                    if (this.b instanceof az.j) {
                        return this.b;
                    }
                    return new az.j(q.this.c);
            }
        }

        public void a(g.a aVar) {
            int[] a2 = aVar.a(-1, -1);
            this.c = a2[0];
            this.d = a2[1];
        }
    }

    private boolean c(int i2) {
        if (this.p == 0) {
            return true;
        }
        if (System.currentTimeMillis() - this.p > 28800000) {
            f();
            return true;
        } else if (i2 > 5000) {
            return false;
        } else {
            return true;
        }
    }

    public void a(Context context) {
        try {
            if (j == null) {
                j = context;
            }
            if (this.l.length() > 0) {
                cx.a(j).a(this.l);
                this.l = new JSONArray();
            }
            aa.a(j).edit().putLong("thtstart", this.p).putInt("gkvc", this.n).putInt("ekvc", this.o).commit();
        } catch (Throwable th) {
        }
    }

    private void f() {
        this.n = 0;
        this.o = 0;
        this.p = System.currentTimeMillis();
    }
}
