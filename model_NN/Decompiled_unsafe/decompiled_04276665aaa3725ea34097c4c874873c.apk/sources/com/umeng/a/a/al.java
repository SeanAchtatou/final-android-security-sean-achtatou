package com.umeng.a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: IdTracking */
public class al implements be<al, e>, Serializable, Cloneable {
    public static final Map<e, bj> d;
    /* access modifiers changed from: private */
    public static final bz e = new bz("IdTracking");
    /* access modifiers changed from: private */
    public static final br f = new br("snapshots", (byte) 13, 1);
    /* access modifiers changed from: private */
    public static final br g = new br("journals", (byte) 15, 2);
    /* access modifiers changed from: private */
    public static final br h = new br("checksum", (byte) 11, 3);
    private static final Map<Class<? extends cb>, cc> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public Map<String, ak> f2636a;
    public List<aj> b;
    public String c;
    private e[] j = {e.JOURNALS, e.CHECKSUM};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.umeng.a.a.al$e, com.umeng.a.a.bj]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(cd.class, new b());
        i.put(ce.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.SNAPSHOTS, (Object) new bj("snapshots", (byte) 1, new bm((byte) 13, new bk((byte) 11), new bn((byte) 12, ak.class))));
        enumMap.put((Object) e.JOURNALS, (Object) new bj("journals", (byte) 2, new bl((byte) 15, new bn((byte) 12, aj.class))));
        enumMap.put((Object) e.CHECKSUM, (Object) new bj("checksum", (byte) 2, new bk((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        bj.a(al.class, d);
    }

    /* compiled from: IdTracking */
    public enum e {
        SNAPSHOTS(1, "snapshots"),
        JOURNALS(2, "journals"),
        CHECKSUM(3, "checksum");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.a(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public String a() {
            return this.f;
        }
    }

    public Map<String, ak> a() {
        return this.f2636a;
    }

    public al a(Map<String, ak> map) {
        this.f2636a = map;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f2636a = null;
        }
    }

    public List<aj> b() {
        return this.b;
    }

    public al a(List<aj> list) {
        this.b = list;
        return this;
    }

    public boolean c() {
        return this.b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.b = null;
        }
    }

    public boolean d() {
        return this.c != null;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(bu buVar) throws bh {
        i.get(buVar.y()).b().b(buVar, this);
    }

    public void b(bu buVar) throws bh {
        i.get(buVar.y()).b().a(buVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdTracking(");
        sb.append("snapshots:");
        if (this.f2636a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2636a);
        }
        if (c()) {
            sb.append(", ");
            sb.append("journals:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("checksum:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void e() throws bh {
        if (this.f2636a == null) {
            throw new bv("Required field 'snapshots' was not present! Struct: " + toString());
        }
    }

    /* compiled from: IdTracking */
    private static class b implements cc {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: IdTracking */
    private static class a extends cd<al> {
        private a() {
        }

        /* renamed from: a */
        public void b(bu buVar, al alVar) throws bh {
            buVar.f();
            while (true) {
                br h = buVar.h();
                if (h.b == 0) {
                    buVar.g();
                    alVar.e();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.b != 13) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            bt j = buVar.j();
                            alVar.f2636a = new HashMap(j.c * 2);
                            for (int i = 0; i < j.c; i++) {
                                String v = buVar.v();
                                ak akVar = new ak();
                                akVar.a(buVar);
                                alVar.f2636a.put(v, akVar);
                            }
                            buVar.k();
                            alVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.b != 15) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            bs l = buVar.l();
                            alVar.b = new ArrayList(l.b);
                            for (int i2 = 0; i2 < l.b; i2++) {
                                aj ajVar = new aj();
                                ajVar.a(buVar);
                                alVar.b.add(ajVar);
                            }
                            buVar.m();
                            alVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.b != 11) {
                            bx.a(buVar, h.b);
                            break;
                        } else {
                            alVar.c = buVar.v();
                            alVar.c(true);
                            break;
                        }
                    default:
                        bx.a(buVar, h.b);
                        break;
                }
                buVar.i();
            }
        }

        /* renamed from: b */
        public void a(bu buVar, al alVar) throws bh {
            alVar.e();
            buVar.a(al.e);
            if (alVar.f2636a != null) {
                buVar.a(al.f);
                buVar.a(new bt((byte) 11, (byte) 12, alVar.f2636a.size()));
                for (Map.Entry next : alVar.f2636a.entrySet()) {
                    buVar.a((String) next.getKey());
                    ((ak) next.getValue()).b(buVar);
                }
                buVar.d();
                buVar.b();
            }
            if (alVar.b != null && alVar.c()) {
                buVar.a(al.g);
                buVar.a(new bs((byte) 12, alVar.b.size()));
                for (aj b : alVar.b) {
                    b.b(buVar);
                }
                buVar.e();
                buVar.b();
            }
            if (alVar.c != null && alVar.d()) {
                buVar.a(al.h);
                buVar.a(alVar.c);
                buVar.b();
            }
            buVar.c();
            buVar.a();
        }
    }

    /* compiled from: IdTracking */
    private static class d implements cc {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: IdTracking */
    private static class c extends ce<al> {
        private c() {
        }

        public void a(bu buVar, al alVar) throws bh {
            ca caVar = (ca) buVar;
            caVar.a(alVar.f2636a.size());
            for (Map.Entry next : alVar.f2636a.entrySet()) {
                caVar.a((String) next.getKey());
                ((ak) next.getValue()).b(caVar);
            }
            BitSet bitSet = new BitSet();
            if (alVar.c()) {
                bitSet.set(0);
            }
            if (alVar.d()) {
                bitSet.set(1);
            }
            caVar.a(bitSet, 2);
            if (alVar.c()) {
                caVar.a(alVar.b.size());
                for (aj b : alVar.b) {
                    b.b(caVar);
                }
            }
            if (alVar.d()) {
                caVar.a(alVar.c);
            }
        }

        public void b(bu buVar, al alVar) throws bh {
            ca caVar = (ca) buVar;
            bt btVar = new bt((byte) 11, (byte) 12, caVar.s());
            alVar.f2636a = new HashMap(btVar.c * 2);
            for (int i = 0; i < btVar.c; i++) {
                String v = caVar.v();
                ak akVar = new ak();
                akVar.a(caVar);
                alVar.f2636a.put(v, akVar);
            }
            alVar.a(true);
            BitSet b = caVar.b(2);
            if (b.get(0)) {
                bs bsVar = new bs((byte) 12, caVar.s());
                alVar.b = new ArrayList(bsVar.b);
                for (int i2 = 0; i2 < bsVar.b; i2++) {
                    aj ajVar = new aj();
                    ajVar.a(caVar);
                    alVar.b.add(ajVar);
                }
                alVar.b(true);
            }
            if (b.get(1)) {
                alVar.c = caVar.v();
                alVar.c(true);
            }
        }
    }
}
