package com.umeng.a.a;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* compiled from: AbstractIdTracker */
public abstract class cy {

    /* renamed from: a  reason: collision with root package name */
    private final int f2720a = 10;
    private final int b = 20;
    private final String c;
    private List<aj> d;
    private ak e;

    public abstract String a();

    public cy(String str) {
        this.c = str;
    }

    public boolean e() {
        return b();
    }

    public String f() {
        return this.c;
    }

    public boolean g() {
        if (this.e == null || this.e.d() <= 20) {
            return true;
        }
        return false;
    }

    private boolean b() {
        ak akVar = this.e;
        String a2 = akVar == null ? null : akVar.a();
        int d2 = akVar == null ? 0 : akVar.d();
        String a3 = a(a());
        if (a3 == null || a3.equals(a2)) {
            return false;
        }
        if (akVar == null) {
            akVar = new ak();
        }
        akVar.a(a3);
        akVar.a(System.currentTimeMillis());
        akVar.a(d2 + 1);
        aj ajVar = new aj();
        ajVar.a(this.c);
        ajVar.c(a3);
        ajVar.b(a2);
        ajVar.a(akVar.b());
        if (this.d == null) {
            this.d = new ArrayList(2);
        }
        this.d.add(ajVar);
        if (this.d.size() > 10) {
            this.d.remove(0);
        }
        this.e = akVar;
        return true;
    }

    public ak h() {
        return this.e;
    }

    public List<aj> i() {
        return this.d;
    }

    public void a(List<aj> list) {
        this.d = list;
    }

    public String a(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() == 0 || "0".equals(trim) || "unknown".equals(trim.toLowerCase(Locale.US))) {
            return null;
        }
        return trim;
    }

    public void a(al alVar) {
        this.e = alVar.a().get(this.c);
        List<aj> b2 = alVar.b();
        if (b2 != null && b2.size() > 0) {
            if (this.d == null) {
                this.d = new ArrayList();
            }
            for (aj next : b2) {
                if (this.c.equals(next.f2632a)) {
                    this.d.add(next);
                }
            }
        }
    }
}
