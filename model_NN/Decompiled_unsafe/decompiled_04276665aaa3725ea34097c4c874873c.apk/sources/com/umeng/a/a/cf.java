package com.umeng.a.a;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: TIOStreamTransport */
public class cf extends ch {

    /* renamed from: a  reason: collision with root package name */
    protected InputStream f2688a = null;
    protected OutputStream b = null;

    protected cf() {
    }

    public cf(OutputStream outputStream) {
        this.b = outputStream;
    }

    public int a(byte[] bArr, int i, int i2) throws ci {
        if (this.f2688a == null) {
            throw new ci(1, "Cannot read from null inputStream");
        }
        try {
            int read = this.f2688a.read(bArr, i, i2);
            if (read >= 0) {
                return read;
            }
            throw new ci(4);
        } catch (IOException e) {
            throw new ci(0, e);
        }
    }

    public void b(byte[] bArr, int i, int i2) throws ci {
        if (this.b == null) {
            throw new ci(1, "Cannot write to null outputStream");
        }
        try {
            this.b.write(bArr, i, i2);
        } catch (IOException e) {
            throw new ci(0, e);
        }
    }
}
