package com.umeng.a.a;

import android.text.TextUtils;
import android.util.Log;
import java.util.Formatter;

/* compiled from: MLog */
public class aw {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f2653a = false;
    private static String b = "MobclickAgent";
    private static int c = 2000;

    public static void a(String str, Object... objArr) {
        String str2 = "";
        try {
            if (str.contains("%")) {
                c(b, new Formatter().format(str, objArr).toString(), null);
                return;
            }
            if (objArr != null) {
                str2 = (String) objArr[0];
            }
            c(str, str2, null);
        } catch (Throwable th) {
            a(th);
        }
    }

    public static void a(Throwable th) {
        c(b, null, th);
    }

    public static void a(String str, Throwable th) {
        c(b, str, th);
    }

    public static void a(String str) {
        a(b, str, null);
    }

    public static void b(String str) {
        b(b, str, null);
    }

    public static void c(String str) {
        c(b, str, null);
    }

    public static void a(String str, String str2, Throwable th) {
        if (f2653a) {
            a(2, str, str2, th);
        }
    }

    public static void b(String str, String str2, Throwable th) {
        if (f2653a) {
            a(3, str, str2, th);
        }
    }

    public static void c(String str, String str2, Throwable th) {
        if (f2653a) {
            a(5, str, str2, th);
        }
    }

    private static void a(int i, String str, String str2, Throwable th) {
        int i2 = 0;
        if (!TextUtils.isEmpty(str2)) {
            int length = str2.length();
            int i3 = c;
            int i4 = 0;
            while (true) {
                if (i2 < 100) {
                    if (length <= i3) {
                        switch (i) {
                            case 1:
                                Log.v(str, str2.substring(i4, length));
                                break;
                            case 2:
                                Log.d(str, str2.substring(i4, length));
                                break;
                            case 3:
                                Log.i(str, str2.substring(i4, length));
                                break;
                            case 4:
                                Log.w(str, str2.substring(i4, length));
                                break;
                            case 5:
                                Log.e(str, str2.substring(i4, length));
                                break;
                        }
                    } else {
                        switch (i) {
                            case 1:
                                Log.v(str, str2.substring(i4, i3));
                                break;
                            case 2:
                                Log.d(str, str2.substring(i4, i3));
                                break;
                            case 3:
                                Log.i(str, str2.substring(i4, i3));
                                break;
                            case 4:
                                Log.w(str, str2.substring(i4, i3));
                                break;
                            case 5:
                                Log.e(str, str2.substring(i4, i3));
                                break;
                        }
                        i2++;
                        i4 = i3;
                        i3 = c + i3;
                    }
                }
            }
        }
        if (th != null) {
            String b2 = b(th);
            if (!TextUtils.isEmpty(b2)) {
                switch (i) {
                    case 1:
                        Log.v(str, b2);
                        return;
                    case 2:
                        Log.d(str, b2);
                        return;
                    case 3:
                        Log.i(str, b2);
                        return;
                    case 4:
                        Log.w(str, b2);
                        return;
                    case 5:
                        Log.e(str, b2);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0029 A[SYNTHETIC, Splitter:B:16:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0037 A[SYNTHETIC, Splitter:B:23:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(java.lang.Throwable r4) {
        /*
            r2 = 0
            java.lang.String r0 = ""
            java.io.StringWriter r3 = new java.io.StringWriter     // Catch:{ Throwable -> 0x0025, all -> 0x0032 }
            r3.<init>()     // Catch:{ Throwable -> 0x0025, all -> 0x0032 }
            java.io.PrintWriter r1 = new java.io.PrintWriter     // Catch:{ Throwable -> 0x004b, all -> 0x0046 }
            r1.<init>(r3)     // Catch:{ Throwable -> 0x004b, all -> 0x0046 }
            r4.printStackTrace(r1)     // Catch:{ Throwable -> 0x004f, all -> 0x0049 }
            r1.flush()     // Catch:{ Throwable -> 0x004f, all -> 0x0049 }
            r3.flush()     // Catch:{ Throwable -> 0x004f, all -> 0x0049 }
            java.lang.String r0 = r3.toString()     // Catch:{ Throwable -> 0x004f, all -> 0x0049 }
            if (r3 == 0) goto L_0x001f
            r3.close()     // Catch:{ Throwable -> 0x0040 }
        L_0x001f:
            if (r1 == 0) goto L_0x0024
            r1.close()
        L_0x0024:
            return r0
        L_0x0025:
            r1 = move-exception
            r1 = r2
        L_0x0027:
            if (r2 == 0) goto L_0x002c
            r2.close()     // Catch:{ Throwable -> 0x0042 }
        L_0x002c:
            if (r1 == 0) goto L_0x0024
            r1.close()
            goto L_0x0024
        L_0x0032:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x0035:
            if (r3 == 0) goto L_0x003a
            r3.close()     // Catch:{ Throwable -> 0x0044 }
        L_0x003a:
            if (r1 == 0) goto L_0x003f
            r1.close()
        L_0x003f:
            throw r0
        L_0x0040:
            r2 = move-exception
            goto L_0x001f
        L_0x0042:
            r2 = move-exception
            goto L_0x002c
        L_0x0044:
            r2 = move-exception
            goto L_0x003a
        L_0x0046:
            r0 = move-exception
            r1 = r2
            goto L_0x0035
        L_0x0049:
            r0 = move-exception
            goto L_0x0035
        L_0x004b:
            r1 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0027
        L_0x004f:
            r2 = move-exception
            r2 = r3
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.aw.b(java.lang.Throwable):java.lang.String");
    }
}
