package com.umeng.a.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: UMCCStorageManager */
public class cq {

    /* renamed from: a  reason: collision with root package name */
    private static Context f2710a;

    /* compiled from: UMCCStorageManager */
    private static final class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final cq f2711a = new cq();
    }

    private cq() {
        if (f2710a != null) {
        }
    }

    public static cq a(Context context) {
        f2710a = context;
        return a.f2711a;
    }

    public void a(ck ckVar) {
        try {
            SQLiteDatabase a2 = z.a(f2710a).a();
            String a3 = a.a(a2);
            String a4 = cs.a(System.currentTimeMillis());
            if (a3.equals("0")) {
                ckVar.a("faild", false);
                return;
            }
            if (!a3.equals(a4)) {
                a.a(a2, ckVar);
            } else {
                a.b(a2, ckVar);
            }
            z.a(f2710a).c();
        } catch (Exception e) {
            ckVar.a(false, false);
            aw.c("load agg data error");
        } finally {
            z.a(f2710a).c();
        }
    }

    public void a(ck ckVar, Map<List<String>, cm> map) {
        try {
            a.a(z.a(f2710a).b(), map.values());
            ckVar.a("success", false);
        } catch (Exception e) {
            aw.c("save agg data error");
        } finally {
            z.a(f2710a).c();
        }
    }

    /* JADX INFO: finally extract failed */
    public JSONObject a() {
        try {
            JSONObject b = a.b(z.a(f2710a).a());
            z.a(f2710a).c();
            return b;
        } catch (Exception e) {
            aw.c("upload agg date error");
            z.a(f2710a).c();
            return null;
        } catch (Throwable th) {
            z.a(f2710a).c();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public JSONObject b(ck ckVar) {
        try {
            JSONObject a2 = a.a(ckVar, z.a(f2710a).a());
            z.a(f2710a).c();
            return a2;
        } catch (Exception e) {
            e.printStackTrace();
            z.a(f2710a).c();
            return null;
        } catch (Throwable th) {
            z.a(f2710a).c();
            throw th;
        }
    }

    public void a(ck ckVar, boolean z) {
        try {
            a.a(z.a(f2710a).b(), z, ckVar);
        } catch (Exception e) {
            aw.c("notifyUploadSuccess error");
        } finally {
            z.a(f2710a).c();
        }
    }

    public void a(ck ckVar, String str, long j, long j2) {
        try {
            a.a(z.a(f2710a).b(), str, j, j2);
            ckVar.a("success", false);
        } catch (Exception e) {
            aw.c("package size to big or envelopeOverflowPackageCount exception");
        } finally {
            z.a(f2710a).c();
        }
    }

    public void a(ck ckVar, List<String> list) {
        try {
            a.a(ckVar, z.a(f2710a).b(), list);
        } catch (Exception e) {
            aw.c("saveToLimitCKTable exception");
        } finally {
            z.a(f2710a).c();
        }
    }

    public void b(ck ckVar, Map<String, cn> map) {
        try {
            a.a(z.a(f2710a).b(), map, ckVar);
        } catch (Exception e) {
            aw.c("arrgetated system buffer exception");
        } finally {
            z.a(f2710a).c();
        }
    }

    /* JADX INFO: finally extract failed */
    public List<String> b() {
        try {
            List<String> c = a.c(z.a(f2710a).a());
            z.a(f2710a).c();
            return c;
        } catch (Exception e) {
            aw.c("loadCKToMemory exception");
            z.a(f2710a).c();
            return null;
        } catch (Throwable th) {
            z.a(f2710a).c();
            throw th;
        }
    }

    public void c(ck ckVar, Map<List<String>, cm> map) {
        try {
            a.a(ckVar, z.a(f2710a).b(), map.values());
        } catch (Exception e) {
            aw.c("cacheToData error");
        } finally {
            z.a(f2710a).c();
        }
    }
}
