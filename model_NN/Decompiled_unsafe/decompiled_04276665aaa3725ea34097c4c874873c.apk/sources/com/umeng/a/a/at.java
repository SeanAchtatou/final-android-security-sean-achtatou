package com.umeng.a.a;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.umeng.a.a;
import com.umeng.a.b;
import com.umeng.a.c;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Locale;
import java.util.Properties;

/* compiled from: DeviceConfig */
public class at {

    /* renamed from: a  reason: collision with root package name */
    protected static final String f2651a = at.class.getName();

    public static String a(Context context) {
        try {
            return String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    public static String b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    public static boolean a(Context context, String str) {
        boolean z;
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                if (((Integer) Class.forName("android.content.Context").getMethod("checkSelfPermission", String.class).invoke(context, str)).intValue() == 0) {
                    z = true;
                } else {
                    z = false;
                }
                return z;
            } catch (Throwable th) {
                return false;
            }
        } else if (context.getPackageManager().checkPermission(str, context.getPackageName()) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x000c A[Catch:{ Throwable -> 0x0072 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String b() {
        /*
            r1 = 0
            r2 = 0
            java.util.Enumeration r3 = java.net.NetworkInterface.getNetworkInterfaces()     // Catch:{ Throwable -> 0x0072 }
        L_0x0006:
            boolean r0 = r3.hasMoreElements()     // Catch:{ Throwable -> 0x0072 }
            if (r0 == 0) goto L_0x0073
            java.lang.Object r0 = r3.nextElement()     // Catch:{ Throwable -> 0x0072 }
            java.net.NetworkInterface r0 = (java.net.NetworkInterface) r0     // Catch:{ Throwable -> 0x0072 }
            java.lang.String r4 = "wlan0"
            java.lang.String r5 = r0.getName()     // Catch:{ Throwable -> 0x0072 }
            boolean r4 = r4.equals(r5)     // Catch:{ Throwable -> 0x0072 }
            if (r4 != 0) goto L_0x002a
            java.lang.String r4 = "eth0"
            java.lang.String r5 = r0.getName()     // Catch:{ Throwable -> 0x0072 }
            boolean r4 = r4.equals(r5)     // Catch:{ Throwable -> 0x0072 }
            if (r4 == 0) goto L_0x0006
        L_0x002a:
            byte[] r3 = r0.getHardwareAddress()     // Catch:{ Throwable -> 0x0072 }
            if (r3 == 0) goto L_0x0033
            int r0 = r3.length     // Catch:{ Throwable -> 0x0072 }
            if (r0 != 0) goto L_0x0035
        L_0x0033:
            r0 = r1
        L_0x0034:
            return r0
        L_0x0035:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0072 }
            r4.<init>()     // Catch:{ Throwable -> 0x0072 }
            int r5 = r3.length     // Catch:{ Throwable -> 0x0072 }
            r0 = r2
        L_0x003c:
            if (r0 >= r5) goto L_0x0056
            byte r2 = r3[r0]     // Catch:{ Throwable -> 0x0072 }
            java.lang.String r6 = "%02X:"
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Throwable -> 0x0072 }
            r8 = 0
            java.lang.Byte r2 = java.lang.Byte.valueOf(r2)     // Catch:{ Throwable -> 0x0072 }
            r7[r8] = r2     // Catch:{ Throwable -> 0x0072 }
            java.lang.String r2 = java.lang.String.format(r6, r7)     // Catch:{ Throwable -> 0x0072 }
            r4.append(r2)     // Catch:{ Throwable -> 0x0072 }
            int r0 = r0 + 1
            goto L_0x003c
        L_0x0056:
            int r0 = r4.length()     // Catch:{ Throwable -> 0x0072 }
            if (r0 <= 0) goto L_0x0065
            int r0 = r4.length()     // Catch:{ Throwable -> 0x0072 }
            int r0 = r0 + -1
            r4.deleteCharAt(r0)     // Catch:{ Throwable -> 0x0072 }
        L_0x0065:
            java.lang.String r0 = r4.toString()     // Catch:{ Throwable -> 0x0072 }
            java.util.Locale r2 = java.util.Locale.getDefault()     // Catch:{ Throwable -> 0x0072 }
            java.lang.String r0 = r0.toLowerCase(r2)     // Catch:{ Throwable -> 0x0072 }
            goto L_0x0034
        L_0x0072:
            r0 = move-exception
        L_0x0073:
            r0 = r1
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.at.b():java.lang.String");
    }

    private static String c() {
        int i = 0;
        try {
            String[] strArr = {"/sys/class/net/wlan0/address", "/sys/class/net/eth0/address", "/sys/devices/virtual/net/wlan0/address"};
            while (true) {
                int i2 = i;
                if (i2 >= strArr.length) {
                    break;
                }
                try {
                    String a2 = a(strArr[i2]);
                    if (a2 != null) {
                        return a2;
                    }
                    i = i2 + 1;
                } catch (Throwable th) {
                }
            }
        } catch (Throwable th2) {
        }
        return null;
    }

    private static String a(String str) {
        BufferedReader bufferedReader;
        String str2 = null;
        try {
            FileReader fileReader = new FileReader(str);
            if (fileReader != null) {
                try {
                    bufferedReader = new BufferedReader(fileReader, 1024);
                    try {
                        str2 = bufferedReader.readLine();
                        if (fileReader != null) {
                            try {
                                fileReader.close();
                            } catch (Throwable th) {
                            }
                        }
                        if (bufferedReader != null) {
                            try {
                                bufferedReader.close();
                            } catch (Throwable th2) {
                            }
                        }
                    } catch (Throwable th3) {
                        th = th3;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    bufferedReader = null;
                    if (fileReader != null) {
                        try {
                            fileReader.close();
                        } catch (Throwable th5) {
                        }
                    }
                    if (bufferedReader != null) {
                        try {
                            bufferedReader.close();
                        } catch (Throwable th6) {
                        }
                    }
                    throw th;
                }
            }
        } catch (Throwable th7) {
        }
        return str2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0044 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a() {
        /*
            r0 = 0
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ FileNotFoundException -> 0x0047 }
            java.lang.String r2 = "/proc/cpuinfo"
            r1.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0047 }
            if (r1 == 0) goto L_0x001b
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x002e }
            r3 = 1024(0x400, float:1.435E-42)
            r2.<init>(r1, r3)     // Catch:{ Throwable -> 0x002e }
            java.lang.String r0 = r2.readLine()     // Catch:{ Throwable -> 0x002e }
            r2.close()     // Catch:{ Throwable -> 0x002e }
            r1.close()     // Catch:{ Throwable -> 0x002e }
        L_0x001b:
            if (r0 == 0) goto L_0x0044
            r1 = 58
            int r1 = r0.indexOf(r1)
            int r1 = r1 + 1
            java.lang.String r0 = r0.substring(r1)
            java.lang.String r0 = r0.trim()
        L_0x002d:
            return r0
        L_0x002e:
            r1 = move-exception
            java.lang.String r2 = com.umeng.a.a.at.f2651a     // Catch:{ FileNotFoundException -> 0x0037 }
            java.lang.String r3 = "Could not read from file /proc/cpuinfo"
            com.umeng.a.a.aw.c(r2, r3, r1)     // Catch:{ FileNotFoundException -> 0x0037 }
            goto L_0x001b
        L_0x0037:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x003b:
            java.lang.String r2 = com.umeng.a.a.at.f2651a
            java.lang.String r3 = "Could not open file /proc/cpuinfo"
            com.umeng.a.a.aw.c(r2, r3, r0)
            r0 = r1
            goto L_0x001b
        L_0x0044:
            java.lang.String r0 = ""
            goto L_0x002d
        L_0x0047:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.at.a():java.lang.String");
    }

    public static String c(Context context) {
        if (b.a.E_UM_ANALYTICS_OEM.a() == a.d(context) || b.a.E_UM_GAME_OEM.a() == a.d(context)) {
            return A(context);
        }
        return z(context);
    }

    public static String d(Context context) {
        return au.b(c(context));
    }

    public static String e(Context context) {
        if (f(context) == null) {
            return null;
        }
        int i = context.getResources().getConfiguration().mcc;
        int i2 = context.getResources().getConfiguration().mnc;
        if (i == 0) {
            return null;
        }
        String valueOf = String.valueOf(i2);
        if (i2 < 10) {
            valueOf = String.format("%02d", Integer.valueOf(i2));
        }
        return new StringBuffer().append(String.valueOf(i)).append(valueOf).toString();
    }

    public static String f(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (a(context, "android.permission.READ_PHONE_STATE")) {
            return telephonyManager.getSubscriberId();
        }
        return null;
    }

    public static String g(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (a(context, "android.permission.READ_PHONE_STATE") && telephonyManager != null) {
                return telephonyManager.getNetworkOperatorName();
            }
        } catch (Throwable th) {
        }
        return "";
    }

    public static String[] h(Context context) {
        String[] strArr = {"", ""};
        try {
            if (!a(context, "android.permission.ACCESS_NETWORK_STATE")) {
                strArr[0] = "";
                return strArr;
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                strArr[0] = "";
                return strArr;
            }
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(1);
            if (networkInfo == null || networkInfo.getState() != NetworkInfo.State.CONNECTED) {
                NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(0);
                if (networkInfo2 != null && networkInfo2.getState() == NetworkInfo.State.CONNECTED) {
                    strArr[0] = "2G/3G";
                    strArr[1] = networkInfo2.getSubtypeName();
                    return strArr;
                }
                return strArr;
            }
            strArr[0] = "Wi-Fi";
            return strArr;
        } catch (Throwable th) {
        }
    }

    public static boolean i(Context context) {
        return "Wi-Fi".equals(h(context)[0]);
    }

    public static boolean j(Context context) {
        ConnectivityManager connectivityManager;
        NetworkInfo activeNetworkInfo;
        try {
            if (!(!a(context, "android.permission.ACCESS_NETWORK_STATE") || (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null)) {
                return activeNetworkInfo.isConnectedOrConnecting();
            }
        } catch (Throwable th) {
        }
        return false;
    }

    public static int k(Context context) {
        try {
            Calendar instance = Calendar.getInstance(x(context));
            if (instance != null) {
                return instance.getTimeZone().getRawOffset() / 3600000;
            }
        } catch (Throwable th) {
            aw.b(f2651a, "error in getTimeZone", th);
        }
        return 8;
    }

    public static String[] l(Context context) {
        String[] strArr = new String[2];
        try {
            Locale x = x(context);
            if (x != null) {
                strArr[0] = x.getCountry();
                strArr[1] = x.getLanguage();
            }
            if (TextUtils.isEmpty(strArr[0])) {
                strArr[0] = "Unknown";
            }
            if (TextUtils.isEmpty(strArr[1])) {
                strArr[1] = "Unknown";
            }
        } catch (Throwable th) {
            aw.c(f2651a, "error in getLocaleInfo", th);
        }
        return strArr;
    }

    private static Locale x(Context context) {
        Locale locale = null;
        try {
            Configuration configuration = new Configuration();
            configuration.setToDefaults();
            Settings.System.getConfiguration(context.getContentResolver(), configuration);
            if (configuration != null) {
                locale = configuration.locale;
            }
        } catch (Throwable th) {
            aw.a(f2651a, "fail to read user config locale");
        }
        if (locale == null) {
            return Locale.getDefault();
        }
        return locale;
    }

    public static String m(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                String string = applicationInfo.metaData.getString("UMENG_APPKEY");
                if (string != null) {
                    return string.trim();
                }
                aw.a(f2651a, "getAppkey failed. the applicationinfo is null!");
            }
        } catch (Throwable th) {
            aw.c(f2651a, "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.", th);
        }
        return null;
    }

    public static String n(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
            return y(context);
        }
        if (Build.VERSION.SDK_INT == 23) {
            String b = b();
            if (!TextUtils.isEmpty(b)) {
                return b;
            }
            if (c.f2751a) {
                return c();
            }
            return y(context);
        }
        String b2 = b();
        if (TextUtils.isEmpty(b2)) {
            return y(context);
        }
        return b2;
    }

    private static String y(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(IXAdSystemUtils.NT_WIFI);
            if (a(context, "android.permission.ACCESS_WIFI_STATE")) {
                return wifiManager.getConnectionInfo().getMacAddress();
            }
            return "";
        } catch (Throwable th) {
            return "";
        }
    }

    public static int[] o(Context context) {
        int i;
        int i2;
        int i3;
        int i4;
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            if ((context.getApplicationInfo().flags & 8192) == 0) {
                i2 = a(displayMetrics, "noncompatWidthPixels");
                i = a(displayMetrics, "noncompatHeightPixels");
            } else {
                i = -1;
                i2 = -1;
            }
            if (i2 == -1 || i == -1) {
                i3 = displayMetrics.widthPixels;
                i4 = displayMetrics.heightPixels;
            } else {
                i3 = i2;
                i4 = i;
            }
            int[] iArr = new int[2];
            if (i3 > i4) {
                iArr[0] = i4;
                iArr[1] = i3;
                return iArr;
            }
            iArr[0] = i3;
            iArr[1] = i4;
            return iArr;
        } catch (Throwable th) {
            return null;
        }
    }

    private static int a(Object obj, String str) {
        try {
            Field declaredField = DisplayMetrics.class.getDeclaredField(str);
            declaredField.setAccessible(true);
            return declaredField.getInt(obj);
        } catch (Throwable th) {
            return -1;
        }
    }

    public static String p(Context context) {
        Object obj;
        String obj2;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null || applicationInfo.metaData == null || (obj = applicationInfo.metaData.get("UMENG_CHANNEL")) == null || (obj2 = obj.toString()) == null) {
                return "Unknown";
            }
            return obj2;
        } catch (Throwable th) {
            return "Unknown";
        }
    }

    public static String q(Context context) {
        return context.getPackageName();
    }

    public static String r(Context context) {
        try {
            return a(MessageDigest.getInstance("MD5").digest(((X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(new ByteArrayInputStream(context.getPackageManager().getPackageInfo(q(context), 64).signatures[0].toByteArray()))).getEncoded()));
        } catch (Throwable th) {
            return null;
        }
    }

    private static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (int i = 0; i < bArr.length; i++) {
            String hexString = Integer.toHexString(bArr[i]);
            int length = hexString.length();
            if (length == 1) {
                hexString = "0" + hexString;
            }
            if (length > 2) {
                hexString = hexString.substring(length - 2, length);
            }
            sb.append(hexString.toUpperCase(Locale.getDefault()));
            if (i < bArr.length - 1) {
                sb.append(':');
            }
        }
        return sb.toString();
    }

    public static String s(Context context) {
        return context.getPackageManager().getApplicationLabel(context.getApplicationInfo()).toString();
    }

    public static String t(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.loadLabel(context.getPackageManager()).toString();
        } catch (Throwable th) {
            return null;
        }
    }

    private static String z(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
            String B = B(context);
            if (!TextUtils.isEmpty(B)) {
                return B;
            }
            String y = y(context);
            if (!TextUtils.isEmpty(y)) {
                return y;
            }
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (TextUtils.isEmpty(string)) {
                return d();
            }
            return string;
        } else if (Build.VERSION.SDK_INT == 23) {
            String B2 = B(context);
            if (!TextUtils.isEmpty(B2)) {
                return B2;
            }
            String b = b();
            if (TextUtils.isEmpty(b)) {
                if (c.f2751a) {
                    b = c();
                } else {
                    b = y(context);
                }
            }
            if (!TextUtils.isEmpty(b)) {
                return b;
            }
            String string2 = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (TextUtils.isEmpty(string2)) {
                return d();
            }
            return string2;
        } else {
            String B3 = B(context);
            if (!TextUtils.isEmpty(B3)) {
                return B3;
            }
            String d = d();
            if (!TextUtils.isEmpty(d)) {
                return d;
            }
            String string3 = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (!TextUtils.isEmpty(string3)) {
                return string3;
            }
            String b2 = b();
            if (TextUtils.isEmpty(b2)) {
                return y(context);
            }
            return b2;
        }
    }

    private static String A(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (!TextUtils.isEmpty(string)) {
                return string;
            }
            String y = y(context);
            if (!TextUtils.isEmpty(y)) {
                return y;
            }
            String d = d();
            if (TextUtils.isEmpty(d)) {
                return B(context);
            }
            return d;
        } else if (Build.VERSION.SDK_INT == 23) {
            String string2 = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (!TextUtils.isEmpty(string2)) {
                return string2;
            }
            String b = b();
            if (TextUtils.isEmpty(b)) {
                if (c.f2751a) {
                    b = c();
                } else {
                    b = y(context);
                }
            }
            if (!TextUtils.isEmpty(b)) {
                return b;
            }
            String d2 = d();
            if (TextUtils.isEmpty(d2)) {
                return B(context);
            }
            return d2;
        } else {
            String string3 = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (!TextUtils.isEmpty(string3)) {
                return string3;
            }
            String d3 = d();
            if (!TextUtils.isEmpty(d3)) {
                return d3;
            }
            String B = B(context);
            if (!TextUtils.isEmpty(B)) {
                return B;
            }
            String b2 = b();
            if (TextUtils.isEmpty(b2)) {
                return y(context);
            }
            return b2;
        }
    }

    private static String B(Context context) {
        String str;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return "";
        }
        try {
            if (a(context, "android.permission.READ_PHONE_STATE")) {
                str = telephonyManager.getDeviceId();
            } else {
                str = "";
            }
            return str;
        } catch (Throwable th) {
            return "";
        }
    }

    private static String d() {
        if (Build.VERSION.SDK_INT >= 9) {
            return Build.SERIAL;
        }
        return "";
    }

    public static String u(Context context) {
        Properties e = e();
        try {
            String property = e.getProperty("ro.miui.ui.version.name");
            if (!TextUtils.isEmpty(property)) {
                return "MIUI";
            }
            if (f()) {
                return "Flyme";
            }
            if (!TextUtils.isEmpty(a(e))) {
                return "YunOS";
            }
            return property;
        } catch (Throwable th) {
            return null;
        }
    }

    public static String v(Context context) {
        Properties e = e();
        try {
            String property = e.getProperty("ro.miui.ui.version.name");
            if (!TextUtils.isEmpty(property)) {
                return property;
            }
            if (f()) {
                try {
                    return b(e);
                } catch (Throwable th) {
                    return property;
                }
            } else {
                try {
                    return a(e);
                } catch (Throwable th2) {
                    return property;
                }
            }
        } catch (Throwable th3) {
            return null;
        }
    }

    private static String a(Properties properties) {
        String property = properties.getProperty("ro.yunos.version");
        if (!TextUtils.isEmpty(property)) {
            return property;
        }
        return null;
    }

    private static String b(Properties properties) {
        try {
            String lowerCase = properties.getProperty("ro.build.display.id").toLowerCase(Locale.getDefault());
            if (lowerCase.contains("flyme os")) {
                return lowerCase.split(" ")[2];
            }
        } catch (Throwable th) {
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0023 A[SYNTHETIC, Splitter:B:12:0x0023] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002c A[SYNTHETIC, Splitter:B:17:0x002c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.Properties e() {
        /*
            java.util.Properties r2 = new java.util.Properties
            r2.<init>()
            r1 = 0
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x001f, all -> 0x0029 }
            java.io.File r3 = new java.io.File     // Catch:{ Throwable -> 0x001f, all -> 0x0029 }
            java.io.File r4 = android.os.Environment.getRootDirectory()     // Catch:{ Throwable -> 0x001f, all -> 0x0029 }
            java.lang.String r5 = "build.prop"
            r3.<init>(r4, r5)     // Catch:{ Throwable -> 0x001f, all -> 0x0029 }
            r0.<init>(r3)     // Catch:{ Throwable -> 0x001f, all -> 0x0029 }
            r2.load(r0)     // Catch:{ Throwable -> 0x0039, all -> 0x0034 }
            if (r0 == 0) goto L_0x001e
            r0.close()     // Catch:{ Throwable -> 0x0030 }
        L_0x001e:
            return r2
        L_0x001f:
            r0 = move-exception
            r0 = r1
        L_0x0021:
            if (r0 == 0) goto L_0x001e
            r0.close()     // Catch:{ Throwable -> 0x0027 }
            goto L_0x001e
        L_0x0027:
            r0 = move-exception
            goto L_0x001e
        L_0x0029:
            r0 = move-exception
        L_0x002a:
            if (r1 == 0) goto L_0x002f
            r1.close()     // Catch:{ Throwable -> 0x0032 }
        L_0x002f:
            throw r0
        L_0x0030:
            r0 = move-exception
            goto L_0x001e
        L_0x0032:
            r1 = move-exception
            goto L_0x002f
        L_0x0034:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x002a
        L_0x0039:
            r1 = move-exception
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.at.e():java.util.Properties");
    }

    private static boolean f() {
        try {
            Build.class.getMethod("hasSmartBar", new Class[0]);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    public static String w(Context context) {
        String str;
        if (context == null) {
            return null;
        }
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager == null || !a(context, "android.permission.READ_PHONE_STATE")) {
                str = null;
            } else {
                str = telephonyManager.getDeviceId();
            }
            try {
                if (!TextUtils.isEmpty(str)) {
                    return str;
                }
                String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
                if (!TextUtils.isEmpty(string) || Build.VERSION.SDK_INT < 9) {
                    return string;
                }
                return Build.SERIAL;
            } catch (Throwable th) {
                return str;
            }
        } catch (Throwable th2) {
            return null;
        }
    }
}
