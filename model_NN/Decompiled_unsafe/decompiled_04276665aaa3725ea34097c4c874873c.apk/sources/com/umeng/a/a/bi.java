package com.umeng.a.a;

import com.umeng.a.a.bp;
import java.io.ByteArrayOutputStream;

/* compiled from: TSerializer */
public class bi {

    /* renamed from: a  reason: collision with root package name */
    private final ByteArrayOutputStream f2673a;
    private final cf b;
    private bu c;

    public bi() {
        this(new bp.a());
    }

    public bi(bw bwVar) {
        this.f2673a = new ByteArrayOutputStream();
        this.b = new cf(this.f2673a);
        this.c = bwVar.a(this.b);
    }

    public byte[] a(be beVar) throws bh {
        this.f2673a.reset();
        beVar.b(this.c);
        return this.f2673a.toByteArray();
    }
}
