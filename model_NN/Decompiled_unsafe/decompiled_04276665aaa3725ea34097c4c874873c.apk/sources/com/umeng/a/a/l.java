package com.umeng.a.a;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: UOPTracker */
public class l extends cy {

    /* renamed from: a  reason: collision with root package name */
    private Context f2731a;

    public l(Context context) {
        super("uop");
        this.f2731a = context;
    }

    public String a() {
        SharedPreferences a2 = aa.a(this.f2731a);
        if (a2 != null) {
            return a2.getString("uopdta", "");
        }
        return "";
    }
}
