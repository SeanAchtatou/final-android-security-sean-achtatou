package com.umeng.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.meizu.cloud.pushsdk.constants.MeizuConstants;

/* compiled from: UUIDTracker */
public class n extends cy {

    /* renamed from: a  reason: collision with root package name */
    private Context f2733a = null;
    private String b = null;
    private String c = null;

    public n(Context context) {
        super("uuid");
        this.f2733a = context;
        this.b = null;
        this.c = null;
    }

    public String a() {
        SharedPreferences a2;
        SharedPreferences.Editor edit;
        try {
            if (!(TextUtils.isEmpty(a("ro.yunos.version", "")) || this.f2733a == null || (a2 = aa.a(this.f2733a)) == null)) {
                String string = a2.getString("yosuid", "");
                if (!TextUtils.isEmpty(string)) {
                    return string;
                }
                this.c = b("23346339");
                if (!(TextUtils.isEmpty(this.c) || this.f2733a == null || a2 == null || (edit = a2.edit()) == null)) {
                    edit.putString("yosuid", this.c).commit();
                }
                return this.c;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v1, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r3v2, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r3v3, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r3v4 */
    /* JADX WARN: Type inference failed for: r3v5 */
    /* JADX WARN: Type inference failed for: r3v6 */
    /* JADX WARN: Type inference failed for: r3v7 */
    /* JADX WARN: Type inference failed for: r3v8 */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r3v11 */
    /* JADX WARN: Type inference failed for: r3v12 */
    /* JADX WARN: Type inference failed for: r3v13 */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0194, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0195, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0162, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0163, code lost:
        r4 = null;
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d9 A[SYNTHETIC, Splitter:B:31:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00de A[SYNTHETIC, Splitter:B:34:0x00de] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e3 A[SYNTHETIC, Splitter:B:37:0x00e3] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0111 A[SYNTHETIC, Splitter:B:57:0x0111] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0116 A[SYNTHETIC, Splitter:B:60:0x0116] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x011b A[SYNTHETIC, Splitter:B:63:0x011b] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0139 A[SYNTHETIC, Splitter:B:76:0x0139] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x013e A[SYNTHETIC, Splitter:B:79:0x013e] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0143 A[SYNTHETIC, Splitter:B:82:0x0143] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0162 A[ExcHandler: all (r1v28 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:13:0x00a7] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String b(java.lang.String r8) {
        /*
            r7 = this;
            r3 = 0
            java.lang.String r0 = "ro.yunos.openuuid"
            java.lang.String r1 = ""
            java.lang.String r0 = a(r0, r1)
            r7.c = r0
            java.lang.String r0 = r7.c
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0016
            java.lang.String r0 = r7.c
        L_0x0015:
            return r0
        L_0x0016:
            java.lang.String r0 = "ro.aliyun.clouduuid"
            java.lang.String r1 = ""
            java.lang.String r0 = a(r0, r1)
            r7.b = r0
            java.lang.String r0 = r7.b
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0032
            java.lang.String r0 = "ro.sys.aliyun.clouduuid"
            java.lang.String r1 = ""
            java.lang.String r0 = a(r0, r1)
            r7.b = r0
        L_0x0032:
            java.lang.String r0 = r7.b
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00eb
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0108, all -> 0x0133 }
            java.lang.String r1 = "https://cmnsguider.yunos.com:443/genDeviceToken"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0108, all -> 0x0133 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0108, all -> 0x0133 }
            javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0     // Catch:{ Exception -> 0x0108, all -> 0x0133 }
            r1 = 30000(0x7530, float:4.2039E-41)
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            r1 = 30000(0x7530, float:4.2039E-41)
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            r1 = 1
            r0.setDoInput(r1)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            r1 = 0
            r0.setUseCaches(r1)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.lang.String r1 = "Content-Type"
            java.lang.String r2 = "application/x-www-form-urlencoded"
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            com.umeng.a.a.n$1 r1 = new com.umeng.a.a.n$1     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            r1.<init>()     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            r0.setHostnameVerifier(r1)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            r1.<init>()     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.lang.String r2 = "appKey="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.lang.String r2 = "23338940"
            java.lang.String r4 = "UTF-8"
            java.lang.String r2 = java.net.URLEncoder.encode(r2, r4)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.lang.String r2 = "&uuid="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.lang.String r2 = "FC1FE84794417B1BEF276234F6FB4E63"
            java.lang.String r4 = "UTF-8"
            java.lang.String r2 = java.net.URLEncoder.encode(r2, r4)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.io.DataOutputStream r5 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            java.io.OutputStream r2 = r0.getOutputStream()     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            r5.<init>(r2)     // Catch:{ Exception -> 0x017f, all -> 0x015b }
            r5.writeBytes(r1)     // Catch:{ Exception -> 0x0185, all -> 0x0162 }
            r5.flush()     // Catch:{ Exception -> 0x0185, all -> 0x0162 }
            int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x0185, all -> 0x0162 }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x019d
            java.io.InputStream r4 = r0.getInputStream()     // Catch:{ Exception -> 0x0194, all -> 0x0162 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0198, all -> 0x0168 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0198, all -> 0x0168 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0198, all -> 0x0168 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0198, all -> 0x0168 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00d2, all -> 0x016d }
            r1.<init>()     // Catch:{ Exception -> 0x00d2, all -> 0x016d }
        L_0x00c8:
            java.lang.String r3 = r2.readLine()     // Catch:{ Exception -> 0x00d2, all -> 0x016d }
            if (r3 == 0) goto L_0x00ef
            r1.append(r3)     // Catch:{ Exception -> 0x00d2, all -> 0x016d }
            goto L_0x00c8
        L_0x00d2:
            r1 = move-exception
            r3 = r4
        L_0x00d4:
            r1.printStackTrace()     // Catch:{ Exception -> 0x018b, all -> 0x0173 }
        L_0x00d7:
            if (r5 == 0) goto L_0x00dc
            r5.close()     // Catch:{ Exception -> 0x00f9 }
        L_0x00dc:
            if (r2 == 0) goto L_0x00e1
            r2.close()     // Catch:{ Exception -> 0x00fe }
        L_0x00e1:
            if (r3 == 0) goto L_0x00e6
            r3.close()     // Catch:{ Exception -> 0x0103 }
        L_0x00e6:
            if (r0 == 0) goto L_0x00eb
            r0.disconnect()
        L_0x00eb:
            java.lang.String r0 = r7.c
            goto L_0x0015
        L_0x00ef:
            if (r1 == 0) goto L_0x00f7
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00d2, all -> 0x016d }
            r7.c = r1     // Catch:{ Exception -> 0x00d2, all -> 0x016d }
        L_0x00f7:
            r3 = r4
            goto L_0x00d7
        L_0x00f9:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00dc
        L_0x00fe:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00e1
        L_0x0103:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00e6
        L_0x0108:
            r0 = move-exception
            r1 = r3
            r2 = r3
            r4 = r3
        L_0x010c:
            r0.printStackTrace()     // Catch:{ all -> 0x017a }
            if (r1 == 0) goto L_0x0114
            r1.close()     // Catch:{ Exception -> 0x0124 }
        L_0x0114:
            if (r3 == 0) goto L_0x0119
            r3.close()     // Catch:{ Exception -> 0x0129 }
        L_0x0119:
            if (r2 == 0) goto L_0x011e
            r2.close()     // Catch:{ Exception -> 0x012e }
        L_0x011e:
            if (r4 == 0) goto L_0x00eb
            r4.disconnect()
            goto L_0x00eb
        L_0x0124:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0114
        L_0x0129:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0119
        L_0x012e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x011e
        L_0x0133:
            r0 = move-exception
            r5 = r3
            r4 = r3
            r1 = r3
        L_0x0137:
            if (r5 == 0) goto L_0x013c
            r5.close()     // Catch:{ Exception -> 0x014c }
        L_0x013c:
            if (r3 == 0) goto L_0x0141
            r3.close()     // Catch:{ Exception -> 0x0151 }
        L_0x0141:
            if (r4 == 0) goto L_0x0146
            r4.close()     // Catch:{ Exception -> 0x0156 }
        L_0x0146:
            if (r1 == 0) goto L_0x014b
            r1.disconnect()
        L_0x014b:
            throw r0
        L_0x014c:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x013c
        L_0x0151:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0141
        L_0x0156:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0146
        L_0x015b:
            r1 = move-exception
            r5 = r3
            r4 = r3
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0137
        L_0x0162:
            r1 = move-exception
            r4 = r3
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0137
        L_0x0168:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0137
        L_0x016d:
            r1 = move-exception
            r3 = r2
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0137
        L_0x0173:
            r1 = move-exception
            r4 = r3
            r3 = r2
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0137
        L_0x017a:
            r0 = move-exception
            r5 = r1
            r1 = r4
            r4 = r2
            goto L_0x0137
        L_0x017f:
            r1 = move-exception
            r2 = r3
            r4 = r0
            r0 = r1
            r1 = r3
            goto L_0x010c
        L_0x0185:
            r1 = move-exception
            r2 = r3
            r4 = r0
            r0 = r1
            r1 = r5
            goto L_0x010c
        L_0x018b:
            r1 = move-exception
            r4 = r0
            r0 = r1
            r1 = r5
            r6 = r3
            r3 = r2
            r2 = r6
            goto L_0x010c
        L_0x0194:
            r1 = move-exception
            r2 = r3
            goto L_0x00d4
        L_0x0198:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x00d4
        L_0x019d:
            r2 = r3
            goto L_0x00d7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.n.b(java.lang.String):java.lang.String");
    }

    public static String a(String str, String str2) {
        try {
            return (String) Class.forName(MeizuConstants.CLS_NAME_SYSTEM_PROPERTIES).getMethod("get", String.class, String.class).invoke(null, str, str2);
        } catch (Exception e) {
            e.printStackTrace();
            return str2;
        }
    }
}
