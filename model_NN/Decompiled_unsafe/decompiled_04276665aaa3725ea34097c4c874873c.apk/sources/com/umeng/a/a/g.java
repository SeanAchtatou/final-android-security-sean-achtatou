package com.umeng.a.a;

import android.content.Context;
import android.text.TextUtils;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: ImprintHandler */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f2726a = "pbl0".getBytes();
    private static g e;
    private y b;
    private a c = new a();
    private am d = null;
    private Context f;

    g(Context context) {
        this.f = context;
    }

    public static synchronized g a(Context context) {
        g gVar;
        synchronized (g.class) {
            if (e == null) {
                e = new g(context);
                e.c();
            }
            gVar = e;
        }
        return gVar;
    }

    public void a(y yVar) {
        this.b = yVar;
    }

    public String a(am amVar) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : new TreeMap(amVar.a()).entrySet()) {
            sb.append((String) entry.getKey());
            if (((an) entry.getValue()).b()) {
                sb.append(((an) entry.getValue()).a());
            }
            sb.append(((an) entry.getValue()).c());
            sb.append(((an) entry.getValue()).e());
        }
        sb.append(amVar.b);
        return au.a(sb.toString()).toLowerCase(Locale.US);
    }

    private boolean c(am amVar) {
        if (!amVar.e().equals(a(amVar))) {
            return false;
        }
        for (an next : amVar.a().values()) {
            byte[] a2 = ar.a(next.e());
            byte[] a3 = a(next);
            int i = 0;
            while (true) {
                if (i < 4) {
                    if (a2[i] != a3[i]) {
                        return false;
                    }
                    i++;
                }
            }
        }
        return true;
    }

    public byte[] a(an anVar) {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(null);
        allocate.putLong(anVar.c());
        byte[] array = allocate.array();
        byte[] bArr = f2726a;
        byte[] bArr2 = new byte[4];
        for (int i = 0; i < 4; i++) {
            bArr2[i] = (byte) (array[i] ^ bArr[i]);
        }
        return bArr2;
    }

    public void b(am amVar) {
        am a2;
        String str = null;
        if (amVar != null && c(amVar)) {
            boolean z = false;
            synchronized (this) {
                am amVar2 = this.d;
                String e2 = amVar2 == null ? null : amVar2.e();
                if (amVar2 == null) {
                    a2 = d(amVar);
                } else {
                    a2 = a(amVar2, amVar);
                }
                this.d = a2;
                if (a2 != null) {
                    str = a2.e();
                }
                if (!a(e2, str)) {
                    z = true;
                }
            }
            if (this.d != null && z) {
                this.c.a(this.d);
                if (this.b != null) {
                    this.b.a(this.c);
                }
            }
        }
    }

    private boolean a(String str, String str2) {
        if (str != null) {
            return str.equals(str2);
        }
        if (str2 != null) {
            return false;
        }
        return true;
    }

    private am a(am amVar, am amVar2) {
        if (amVar2 != null) {
            Map<String, an> a2 = amVar.a();
            for (Map.Entry next : amVar2.a().entrySet()) {
                if (((an) next.getValue()).b()) {
                    a2.put(next.getKey(), next.getValue());
                } else {
                    a2.remove(next.getKey());
                }
            }
            amVar.a(amVar2.c());
            amVar.a(a(amVar));
        }
        return amVar;
    }

    private am d(am amVar) {
        Map<String, an> a2 = amVar.a();
        ArrayList<String> arrayList = new ArrayList<>(a2.size() / 2);
        for (Map.Entry next : a2.entrySet()) {
            if (!((an) next.getValue()).b()) {
                arrayList.add(next.getKey());
            }
        }
        for (String remove : arrayList) {
            a2.remove(remove);
        }
        return amVar;
    }

    public synchronized am a() {
        return this.d;
    }

    public a b() {
        return this.c;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026 A[SYNTHETIC, Splitter:B:8:0x0026] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c() {
        /*
            r4 = this;
            r2 = 0
            java.io.File r0 = new java.io.File
            android.content.Context r1 = r4.f
            java.io.File r1 = r1.getFilesDir()
            java.lang.String r3 = ".imprint"
            r0.<init>(r1, r3)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0015
        L_0x0014:
            return
        L_0x0015:
            android.content.Context r0 = r4.f     // Catch:{ Exception -> 0x0040, all -> 0x0049 }
            java.lang.String r1 = ".imprint"
            java.io.FileInputStream r1 = r0.openFileInput(r1)     // Catch:{ Exception -> 0x0040, all -> 0x0049 }
            byte[] r2 = com.umeng.a.a.au.b(r1)     // Catch:{ Exception -> 0x0051 }
            com.umeng.a.a.au.c(r1)
        L_0x0024:
            if (r2 == 0) goto L_0x0014
            com.umeng.a.a.am r0 = new com.umeng.a.a.am     // Catch:{ Exception -> 0x003b }
            r0.<init>()     // Catch:{ Exception -> 0x003b }
            com.umeng.a.a.bg r1 = new com.umeng.a.a.bg     // Catch:{ Exception -> 0x003b }
            r1.<init>()     // Catch:{ Exception -> 0x003b }
            r1.a(r0, r2)     // Catch:{ Exception -> 0x003b }
            r4.d = r0     // Catch:{ Exception -> 0x003b }
            com.umeng.a.a.g$a r1 = r4.c     // Catch:{ Exception -> 0x003b }
            r1.a(r0)     // Catch:{ Exception -> 0x003b }
            goto L_0x0014
        L_0x003b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0014
        L_0x0040:
            r0 = move-exception
            r1 = r2
        L_0x0042:
            r0.printStackTrace()     // Catch:{ all -> 0x004e }
            com.umeng.a.a.au.c(r1)
            goto L_0x0024
        L_0x0049:
            r0 = move-exception
        L_0x004a:
            com.umeng.a.a.au.c(r2)
            throw r0
        L_0x004e:
            r0 = move-exception
            r2 = r1
            goto L_0x004a
        L_0x0051:
            r0 = move-exception
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.g.c():void");
    }

    public void d() {
        if (this.d != null) {
            try {
                au.a(new File(this.f.getFilesDir(), ".imprint"), new bi().a(this.d));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* compiled from: ImprintHandler */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private int f2727a = -1;
        private int b = -1;
        private int c = -1;
        private int d = -1;
        private int e = -1;
        private String f = null;
        private int g = -1;
        private String h = null;
        private int i = -1;
        private int j = -1;
        private String k = null;
        private String l = null;
        private String m = null;
        private String n = null;
        private String o = null;

        a() {
        }

        public void a(am amVar) {
            if (amVar != null) {
                this.f2727a = a(amVar, "defcon");
                this.b = a(amVar, "latent");
                this.c = a(amVar, "codex");
                this.d = a(amVar, "report_policy");
                this.e = a(amVar, "report_interval");
                this.f = b(amVar, "client_test");
                this.g = a(amVar, "test_report_interval");
                this.h = b(amVar, "umid");
                this.i = a(amVar, "integrated_test");
                this.j = a(amVar, "latent_hours");
                this.k = b(amVar, "country");
                this.l = b(amVar, "domain_p");
                this.m = b(amVar, "domain_s");
                this.n = b(amVar, "initial_view_time");
                this.o = b(amVar, "track_list");
            }
        }

        public String a(String str) {
            if (this.o != null) {
                return this.o;
            }
            return str;
        }

        public String b(String str) {
            if (this.m != null) {
                return this.m;
            }
            return str;
        }

        public String c(String str) {
            if (this.l != null) {
                return this.l;
            }
            return str;
        }

        public int a(int i2) {
            return (this.f2727a != -1 && this.f2727a <= 3 && this.f2727a >= 0) ? this.f2727a : i2;
        }

        public int b(int i2) {
            return (this.b != -1 && this.b >= 0 && this.b <= 1800) ? this.b * 1000 : i2;
        }

        public int c(int i2) {
            if (this.c == 0 || this.c == 1 || this.c == -1) {
                return this.c;
            }
            return i2;
        }

        public int[] a(int i2, int i3) {
            if (this.d == -1 || !az.a(this.d)) {
                return new int[]{i2, i3};
            }
            if (this.e == -1 || this.e < 90 || this.e > 86400) {
                this.e = 90;
            }
            return new int[]{this.d, this.e * 1000};
        }

        public String d(String str) {
            return (this.f == null || !ag.a(this.f)) ? str : this.f;
        }

        public int d(int i2) {
            return (this.g == -1 || this.g < 90 || this.g > 86400) ? i2 : this.g * 1000;
        }

        public boolean a() {
            return this.g != -1;
        }

        public String e(String str) {
            return this.h;
        }

        public boolean b() {
            return this.i == 1;
        }

        public long a(long j2) {
            return (this.j != -1 && this.j >= 48) ? 3600000 * ((long) this.j) : j2;
        }

        private int a(am amVar, String str) {
            if (amVar != null) {
                try {
                    if (amVar.b()) {
                        an anVar = amVar.a().get(str);
                        if (anVar == null || TextUtils.isEmpty(anVar.a())) {
                            return -1;
                        }
                        try {
                            return Integer.parseInt(anVar.a().trim());
                        } catch (Exception e2) {
                        }
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                    return -1;
                }
            }
            return -1;
        }

        private String b(am amVar, String str) {
            String str2;
            an anVar;
            if (amVar == null) {
                return null;
            }
            try {
                if (!amVar.b() || (anVar = amVar.a().get(str)) == null || TextUtils.isEmpty(anVar.a())) {
                    return null;
                }
                str2 = anVar.a();
                return str2;
            } catch (Exception e2) {
                e2.printStackTrace();
                str2 = null;
            }
        }
    }
}
