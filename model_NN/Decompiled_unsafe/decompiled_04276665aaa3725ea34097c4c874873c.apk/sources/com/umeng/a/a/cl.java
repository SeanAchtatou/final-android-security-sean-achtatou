package com.umeng.a.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: UMCCAggregatedListObject */
public class cl implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private Map<List<String>, cm> f2693a = new HashMap();
    private long b = 0;

    public Map<List<String>, cm> a() {
        return this.f2693a;
    }

    public void a(Map<List<String>, cm> map) {
        if (this.f2693a.size() <= 0) {
            this.f2693a = map;
        } else {
            b(map);
        }
    }

    private void b(Map<List<String>, cm> map) {
        new ArrayList();
        new ArrayList();
        Iterator<Map.Entry<List<String>, cm>> it = this.f2693a.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            List list = (List) next.getKey();
            Iterator<Map.Entry<List<String>, cm>> it2 = this.f2693a.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry next2 = it2.next();
                List list2 = (List) next.getKey();
                if (!list.equals(list2)) {
                    this.f2693a.put(list2, next2.getValue());
                } else {
                    cm cmVar = (cm) next2.getValue();
                    a((cm) next.getValue(), cmVar);
                    this.f2693a.remove(list);
                    this.f2693a.put(list, cmVar);
                }
            }
        }
    }

    private void a(cm cmVar, cm cmVar2) {
        cmVar2.c(cmVar2.f() + cmVar.f());
        cmVar2.b(cmVar2.e() + cmVar.e());
        cmVar2.a(cmVar2.d() + cmVar.d());
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < cmVar.c().size()) {
                cmVar2.a(cmVar.c().get(i2));
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void b() {
        this.f2693a.clear();
    }
}
