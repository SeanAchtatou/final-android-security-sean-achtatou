package com.umeng.a.a;

import android.content.Context;

/* compiled from: IDFATracker */
public class c extends cy {

    /* renamed from: a  reason: collision with root package name */
    private Context f2687a;

    public c(Context context) {
        super("idfa");
        this.f2687a = context;
    }

    public String a() {
        String a2 = aq.a(this.f2687a);
        return a2 == null ? "" : a2;
    }
}
