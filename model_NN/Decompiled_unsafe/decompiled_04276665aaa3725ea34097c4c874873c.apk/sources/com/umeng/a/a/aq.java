package com.umeng.a.a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

/* compiled from: AdvertisingId */
public class aq {

    /* compiled from: AdvertisingId */
    private static final class a {

        /* renamed from: a  reason: collision with root package name */
        private final String f2646a;
        private final boolean b;

        a(String str, boolean z) {
            this.f2646a = str;
            this.b = z;
        }

        /* access modifiers changed from: private */
        public String a() {
            return this.f2646a;
        }
    }

    public static String a(Context context) {
        try {
            a b2 = b(context);
            if (b2 == null) {
                return null;
            }
            return b2.a();
        } catch (Exception e) {
            return null;
        }
    }

    private static a b(Context context) throws Exception {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            b bVar = new b();
            Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
            intent.setPackage("com.google.android.gms");
            if (context.bindService(intent, bVar, 1)) {
                try {
                    c cVar = new c(bVar.a());
                    a aVar = new a(cVar.a(), cVar.a(true));
                    context.unbindService(bVar);
                    return aVar;
                } catch (Exception e) {
                    throw e;
                } catch (Throwable th) {
                    context.unbindService(bVar);
                    throw th;
                }
            } else {
                throw new IOException("Google Play connection failed");
            }
        } catch (Exception e2) {
            throw e2;
        }
    }

    /* compiled from: AdvertisingId */
    private static final class b implements ServiceConnection {

        /* renamed from: a  reason: collision with root package name */
        boolean f2647a;
        private final LinkedBlockingQueue<IBinder> b;

        private b() {
            this.f2647a = false;
            this.b = new LinkedBlockingQueue<>(1);
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.b.put(iBinder);
            } catch (InterruptedException e) {
            }
        }

        public void onServiceDisconnected(ComponentName componentName) {
        }

        public IBinder a() throws InterruptedException {
            if (this.f2647a) {
                throw new IllegalStateException();
            }
            this.f2647a = true;
            return this.b.take();
        }
    }

    /* compiled from: AdvertisingId */
    private static final class c implements IInterface {

        /* renamed from: a  reason: collision with root package name */
        private IBinder f2648a;

        public c(IBinder iBinder) {
            this.f2648a = iBinder;
        }

        public IBinder asBinder() {
            return this.f2648a;
        }

        public String a() throws RemoteException {
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                this.f2648a.transact(1, obtain, obtain2, 0);
                obtain2.readException();
                return obtain2.readString();
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }

        public boolean a(boolean z) throws RemoteException {
            boolean z2 = true;
            Parcel obtain = Parcel.obtain();
            Parcel obtain2 = Parcel.obtain();
            try {
                obtain.writeInterfaceToken("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
                obtain.writeInt(z ? 1 : 0);
                this.f2648a.transact(2, obtain, obtain2, 0);
                obtain2.readException();
                if (obtain2.readInt() == 0) {
                    z2 = false;
                }
                return z2;
            } finally {
                obtain2.recycle();
                obtain.recycle();
            }
        }
    }
}
