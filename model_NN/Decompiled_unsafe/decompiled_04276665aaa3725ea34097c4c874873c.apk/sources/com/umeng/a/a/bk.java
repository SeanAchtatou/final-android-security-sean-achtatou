package com.umeng.a.a;

import java.io.Serializable;

/* compiled from: FieldValueMetaData */
public class bk implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public final byte f2675a;
    private final boolean b;
    private final String c;
    private final boolean d;

    public bk(byte b2, boolean z) {
        this.f2675a = b2;
        this.b = false;
        this.c = null;
        this.d = z;
    }

    public bk(byte b2) {
        this(b2, false);
    }
}
