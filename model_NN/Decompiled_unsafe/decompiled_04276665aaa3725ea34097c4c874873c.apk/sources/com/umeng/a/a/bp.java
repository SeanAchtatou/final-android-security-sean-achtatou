package com.umeng.a.a;

import android.support.v4.media.TransportMediator;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

/* compiled from: TCompactProtocol */
public class bp extends bu {
    private static final bz d = new bz("");
    private static final br f = new br("", (byte) 0, 0);
    private static final byte[] g = new byte[16];

    /* renamed from: a  reason: collision with root package name */
    byte[] f2678a = new byte[5];
    byte[] b = new byte[10];
    byte[] c = new byte[1];
    private bd h = new bd(15);
    private short i = 0;
    private br j = null;
    private Boolean k = null;
    private final long l;
    private byte[] m = new byte[1];

    static {
        g[0] = 0;
        g[2] = 1;
        g[3] = 3;
        g[6] = 4;
        g[8] = 5;
        g[10] = 6;
        g[4] = 7;
        g[11] = 8;
        g[15] = 9;
        g[14] = 10;
        g[13] = 11;
        g[12] = 12;
    }

    /* compiled from: TCompactProtocol */
    public static class a implements bw {

        /* renamed from: a  reason: collision with root package name */
        private final long f2679a = -1;

        public bu a(ch chVar) {
            return new bp(chVar, this.f2679a);
        }
    }

    public bp(ch chVar, long j2) {
        super(chVar);
        this.l = j2;
    }

    public void x() {
        this.h.b();
        this.i = 0;
    }

    public void a(bz bzVar) throws bh {
        this.h.a(this.i);
        this.i = 0;
    }

    public void a() throws bh {
        this.i = this.h.a();
    }

    public void a(br brVar) throws bh {
        if (brVar.b == 2) {
            this.j = brVar;
        } else {
            a(brVar, (byte) -1);
        }
    }

    private void a(br brVar, byte b2) throws bh {
        if (b2 == -1) {
            b2 = e(brVar.b);
        }
        if (brVar.c <= this.i || brVar.c - this.i > 15) {
            b(b2);
            a(brVar.c);
        } else {
            d((int) (((brVar.c - this.i) << 4) | b2));
        }
        this.i = brVar.c;
    }

    public void c() throws bh {
        b((byte) 0);
    }

    public void a(bt btVar) throws bh {
        if (btVar.c == 0) {
            d(0);
            return;
        }
        b(btVar.c);
        d((int) ((e(btVar.f2682a) << 4) | e(btVar.b)));
    }

    public void a(bs bsVar) throws bh {
        a(bsVar.f2681a, bsVar.b);
    }

    public void a(byte b2) throws bh {
        b(b2);
    }

    public void a(short s) throws bh {
        b(c((int) s));
    }

    public void a(int i2) throws bh {
        b(c(i2));
    }

    public void a(long j2) throws bh {
        b(c(j2));
    }

    public void a(String str) throws bh {
        try {
            byte[] bytes = str.getBytes("UTF-8");
            a(bytes, 0, bytes.length);
        } catch (UnsupportedEncodingException e) {
            throw new bh("UTF-8 not supported!");
        }
    }

    public void a(ByteBuffer byteBuffer) throws bh {
        a(byteBuffer.array(), byteBuffer.position() + byteBuffer.arrayOffset(), byteBuffer.limit() - byteBuffer.position());
    }

    private void a(byte[] bArr, int i2, int i3) throws bh {
        b(i3);
        this.e.b(bArr, i2, i3);
    }

    public void d() throws bh {
    }

    public void e() throws bh {
    }

    public void b() throws bh {
    }

    /* access modifiers changed from: protected */
    public void a(byte b2, int i2) throws bh {
        if (i2 <= 14) {
            d((int) ((i2 << 4) | e(b2)));
            return;
        }
        d((int) (e(b2) | 240));
        b(i2);
    }

    private void b(int i2) throws bh {
        int i3 = 0;
        while ((i2 & -128) != 0) {
            this.f2678a[i3] = (byte) ((i2 & TransportMediator.KEYCODE_MEDIA_PAUSE) | 128);
            i2 >>>= 7;
            i3++;
        }
        this.f2678a[i3] = (byte) i2;
        this.e.b(this.f2678a, 0, i3 + 1);
    }

    private void b(long j2) throws bh {
        int i2 = 0;
        while ((-128 & j2) != 0) {
            this.b[i2] = (byte) ((int) ((127 & j2) | 128));
            j2 >>>= 7;
            i2++;
        }
        this.b[i2] = (byte) ((int) j2);
        this.e.b(this.b, 0, i2 + 1);
    }

    private long c(long j2) {
        return (j2 << 1) ^ (j2 >> 63);
    }

    private int c(int i2) {
        return (i2 << 1) ^ (i2 >> 31);
    }

    private void b(byte b2) throws bh {
        this.m[0] = b2;
        this.e.b(this.m);
    }

    private void d(int i2) throws bh {
        b((byte) i2);
    }

    public bz f() throws bh {
        this.h.a(this.i);
        this.i = 0;
        return d;
    }

    public void g() throws bh {
        this.i = this.h.a();
    }

    public br h() throws bh {
        short s;
        byte q = q();
        if (q == 0) {
            return f;
        }
        short s2 = (short) ((q & 240) >> 4);
        if (s2 == 0) {
            s = r();
        } else {
            s = (short) (s2 + this.i);
        }
        br brVar = new br("", d((byte) (q & 15)), s);
        if (c(q)) {
            this.k = ((byte) (q & 15)) == 1 ? Boolean.TRUE : Boolean.FALSE;
        }
        this.i = brVar.c;
        return brVar;
    }

    public bt j() throws bh {
        int z = z();
        byte q = z == 0 ? 0 : q();
        return new bt(d((byte) (q >> 4)), d((byte) (q & 15)), z);
    }

    public bs l() throws bh {
        byte q = q();
        int i2 = (q >> 4) & 15;
        if (i2 == 15) {
            i2 = z();
        }
        return new bs(d(q), i2);
    }

    public by n() throws bh {
        return new by(l());
    }

    public boolean p() throws bh {
        if (this.k != null) {
            boolean booleanValue = this.k.booleanValue();
            this.k = null;
            return booleanValue;
        } else if (q() != 1) {
            return false;
        } else {
            return true;
        }
    }

    public byte q() throws bh {
        if (this.e.d() > 0) {
            byte b2 = this.e.b()[this.e.c()];
            this.e.a(1);
            return b2;
        }
        this.e.d(this.c, 0, 1);
        return this.c[0];
    }

    public short r() throws bh {
        return (short) g(z());
    }

    public int s() throws bh {
        return g(z());
    }

    public long t() throws bh {
        return d(A());
    }

    public double u() throws bh {
        byte[] bArr = new byte[8];
        this.e.d(bArr, 0, 8);
        return Double.longBitsToDouble(a(bArr));
    }

    public String v() throws bh {
        int z = z();
        f(z);
        if (z == 0) {
            return "";
        }
        try {
            if (this.e.d() < z) {
                return new String(e(z), "UTF-8");
            }
            String str = new String(this.e.b(), this.e.c(), z, "UTF-8");
            this.e.a(z);
            return str;
        } catch (UnsupportedEncodingException e) {
            throw new bh("UTF-8 not supported!");
        }
    }

    public ByteBuffer w() throws bh {
        int z = z();
        f(z);
        if (z == 0) {
            return ByteBuffer.wrap(new byte[0]);
        }
        byte[] bArr = new byte[z];
        this.e.d(bArr, 0, z);
        return ByteBuffer.wrap(bArr);
    }

    private byte[] e(int i2) throws bh {
        if (i2 == 0) {
            return new byte[0];
        }
        byte[] bArr = new byte[i2];
        this.e.d(bArr, 0, i2);
        return bArr;
    }

    private void f(int i2) throws bv {
        if (i2 < 0) {
            throw new bv("Negative length: " + i2);
        } else if (this.l != -1 && ((long) i2) > this.l) {
            throw new bv("Length exceeded max allowed: " + i2);
        }
    }

    public void i() throws bh {
    }

    public void k() throws bh {
    }

    public void m() throws bh {
    }

    public void o() throws bh {
    }

    private int z() throws bh {
        int i2 = 0;
        if (this.e.d() >= 5) {
            byte[] b2 = this.e.b();
            int c2 = this.e.c();
            int i3 = 0;
            int i4 = 0;
            while (true) {
                byte b3 = b2[c2 + i2];
                i4 |= (b3 & Byte.MAX_VALUE) << i3;
                if ((b3 & 128) != 128) {
                    this.e.a(i2 + 1);
                    return i4;
                }
                i3 += 7;
                i2++;
            }
        } else {
            int i5 = 0;
            while (true) {
                byte q = q();
                i5 |= (q & Byte.MAX_VALUE) << i2;
                if ((q & 128) != 128) {
                    return i5;
                }
                i2 += 7;
            }
        }
    }

    private long A() throws bh {
        int i2 = 0;
        long j2 = 0;
        if (this.e.d() >= 10) {
            byte[] b2 = this.e.b();
            int c2 = this.e.c();
            int i3 = 0;
            while (true) {
                byte b3 = b2[c2 + i2];
                j2 |= ((long) (b3 & Byte.MAX_VALUE)) << i3;
                if ((b3 & 128) != 128) {
                    break;
                }
                i3 += 7;
                i2++;
            }
            this.e.a(i2 + 1);
        } else {
            while (true) {
                byte q = q();
                j2 |= ((long) (q & Byte.MAX_VALUE)) << i2;
                if ((q & 128) != 128) {
                    break;
                }
                i2 += 7;
            }
        }
        return j2;
    }

    private int g(int i2) {
        return (i2 >>> 1) ^ (-(i2 & 1));
    }

    private long d(long j2) {
        return (j2 >>> 1) ^ (-(1 & j2));
    }

    private long a(byte[] bArr) {
        return ((((long) bArr[7]) & 255) << 56) | ((((long) bArr[6]) & 255) << 48) | ((((long) bArr[5]) & 255) << 40) | ((((long) bArr[4]) & 255) << 32) | ((((long) bArr[3]) & 255) << 24) | ((((long) bArr[2]) & 255) << 16) | ((((long) bArr[1]) & 255) << 8) | (((long) bArr[0]) & 255);
    }

    private boolean c(byte b2) {
        byte b3 = b2 & 15;
        if (b3 == 1 || b3 == 2) {
            return true;
        }
        return false;
    }

    private byte d(byte b2) throws bv {
        switch ((byte) (b2 & 15)) {
            case 0:
                return 0;
            case 1:
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 6;
            case 5:
                return 8;
            case 6:
                return 10;
            case 7:
                return 4;
            case 8:
                return 11;
            case 9:
                return 15;
            case 10:
                return 14;
            case 11:
                return 13;
            case 12:
                return 12;
            default:
                throw new bv("don't know what type: " + ((int) ((byte) (b2 & 15))));
        }
    }

    private byte e(byte b2) {
        return g[b2];
    }
}
