package com.umeng.a.a;

/* compiled from: TField */
public class br {

    /* renamed from: a  reason: collision with root package name */
    public final String f2680a;
    public final byte b;
    public final short c;

    public br() {
        this("", (byte) 0, 0);
    }

    public br(String str, byte b2, short s) {
        this.f2680a = str;
        this.b = b2;
        this.c = s;
    }

    public String toString() {
        return "<TField name:'" + this.f2680a + "' type:" + ((int) this.b) + " field-id:" + ((int) this.c) + ">";
    }
}
