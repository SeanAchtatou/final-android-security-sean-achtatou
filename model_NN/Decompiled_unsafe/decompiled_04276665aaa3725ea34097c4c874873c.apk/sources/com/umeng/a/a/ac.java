package com.umeng.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import com.umeng.a.a;
import com.umeng.a.a.bb;
import com.umeng.a.a.bo;
import java.io.File;
import java.io.FileInputStream;
import org.json.JSONObject;

/* compiled from: Sender */
public class ac {
    private static Context d;

    /* renamed from: a  reason: collision with root package name */
    private e f2624a;
    private g b;
    private final int c = 1;
    /* access modifiers changed from: private */
    public ae e;
    /* access modifiers changed from: private */
    public w f;
    private JSONObject g;
    private boolean h = false;
    /* access modifiers changed from: private */
    public boolean i;

    public ac(Context context, ae aeVar) {
        this.f2624a = e.a(context);
        this.b = g.a(context);
        d = context;
        this.e = aeVar;
        this.f = new w(context);
        this.f.a(this.e);
    }

    public void a(JSONObject jSONObject) {
        this.g = jSONObject;
    }

    public void a(boolean z) {
        this.h = z;
    }

    public void b(boolean z) {
        this.i = z;
    }

    public void a(y yVar) {
        this.b.a(yVar);
    }

    public void a() {
        try {
            if (this.g != null) {
                c();
            } else {
                b();
            }
        } catch (Throwable th) {
        }
        try {
            if (at.i(d)) {
                SharedPreferences a2 = aa.a(d);
                if (a2 != null) {
                    String string = a2.getString("uopdta", "");
                    long b2 = cs.b(System.currentTimeMillis());
                    if (TextUtils.isEmpty(string)) {
                        long j = a2.getLong("uopdte", -1);
                        int i2 = a2.getInt("uopcnt", 0);
                        if (j == -1) {
                            a2.edit().putInt("uopcnt", i2 + 1).commit();
                            this.f.a();
                        } else if (b2 != j) {
                            a2.edit().putInt("uopcnt", 1).commit();
                            this.f.a();
                        } else if (i2 < 2) {
                            a2.edit().putInt("uopcnt", i2 + 1).commit();
                            this.f.a();
                        }
                        a2.edit().putLong("uopdte", b2).commit();
                        return;
                    }
                    return;
                }
                this.f.a();
            }
        } catch (Throwable th2) {
        }
    }

    private void b() {
        bb.a(d).g().a(new bb.b() {
            public void a(File file) {
            }

            public boolean b(File file) {
                FileInputStream fileInputStream;
                byte[] b;
                int a2;
                try {
                    fileInputStream = new FileInputStream(file);
                    try {
                        b = au.b(fileInputStream);
                    } catch (Throwable th) {
                        th = th;
                    }
                    try {
                        au.c(fileInputStream);
                        byte[] a3 = ac.this.f.a(b);
                        if (a3 == null) {
                            a2 = 1;
                        } else {
                            a2 = ac.this.a(a3);
                        }
                        if (!ac.this.i && a2 == 1) {
                            return false;
                        }
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileInputStream = null;
                    au.c(fileInputStream);
                    throw th;
                }
            }

            public void c(File file) {
                ac.this.e.j();
            }
        });
    }

    private void c() {
        b b2;
        int a2;
        try {
            this.f2624a.a();
            try {
                String encodeToString = Base64.encodeToString(new bi().a(this.f2624a.b()), 0);
                if (!TextUtils.isEmpty(encodeToString)) {
                    JSONObject jSONObject = this.g.getJSONObject("header");
                    jSONObject.put("id_tracking", encodeToString);
                    this.g.put("header", jSONObject);
                }
            } catch (Exception e2) {
            }
            byte[] bytes = String.valueOf(this.g).getBytes();
            if (bytes != null && !ar.a(d, bytes)) {
                if (!this.h) {
                    b2 = b.a(d, a.a(d), bytes);
                } else {
                    b2 = b.b(d, a.a(d), bytes);
                }
                byte[] c2 = b2.c();
                bb.a(d).e();
                byte[] a3 = this.f.a(c2);
                if (a3 == null) {
                    a2 = 1;
                } else {
                    a2 = a(a3);
                }
                switch (a2) {
                    case 1:
                        if (!this.i) {
                            bb.a(d).a(c2);
                            return;
                        }
                        return;
                    case 2:
                        this.f2624a.c();
                        this.e.j();
                        return;
                    case 3:
                        this.e.j();
                        return;
                    default:
                        return;
                }
            }
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public int a(byte[] bArr) {
        ao aoVar = new ao();
        try {
            new bg(new bo.a()).a(aoVar, bArr);
            if (aoVar.f2642a == 1) {
                this.b.b(aoVar.d());
                this.b.d();
            }
            aw.b("send log:" + aoVar.b());
        } catch (Throwable th) {
        }
        if (aoVar.f2642a == 1) {
            return 2;
        }
        return 3;
    }
}
