package com.umeng.a.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import com.umeng.a.a;
import com.umeng.a.c;
import java.net.URLEncoder;

/* compiled from: NetworkHelper */
public class w {

    /* renamed from: a  reason: collision with root package name */
    private String f2747a;
    private String b = "10.0.0.172";
    private int c = 80;
    private Context d;
    private v e;

    public w(Context context) {
        this.d = context;
        this.f2747a = a(context);
    }

    public void a(v vVar) {
        this.e = vVar;
    }

    private void b() {
        String c2 = g.a(this.d).b().c("");
        String b2 = g.a(this.d).b().b("");
        if (!TextUtils.isEmpty(c2)) {
            c.c = ar.b(c2);
        }
        if (!TextUtils.isEmpty(b2)) {
            c.d = ar.b(b2);
        }
        c.e = new String[]{c.c, c.d};
        int b3 = ag.a(this.d).b();
        if (b3 == -1) {
            return;
        }
        if (b3 == 0) {
            c.e = new String[]{c.c, c.d};
        } else if (b3 == 1) {
            c.e = new String[]{c.d, c.c};
        }
    }

    public byte[] a(byte[] bArr) {
        byte[] bArr2 = null;
        b();
        int i = 0;
        while (true) {
            if (i >= c.e.length) {
                break;
            }
            bArr2 = a(bArr, c.e[i]);
            if (bArr2 == null) {
                if (this.e != null) {
                    this.e.d();
                }
                i++;
            } else if (this.e != null) {
                this.e.c();
            }
        }
        return bArr2;
    }

    private boolean c() {
        String extraInfo;
        if (this.d.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", this.d.getPackageName()) != 0) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) this.d.getSystemService("connectivity");
            if (!at.a(this.d, "android.permission.ACCESS_NETWORK_STATE")) {
                return false;
            }
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getType() == 1 || (extraInfo = activeNetworkInfo.getExtraInfo()) == null || (!extraInfo.equals("cmwap") && !extraInfo.equals("3gwap") && !extraInfo.equals("uniwap")))) {
                return true;
            }
            return false;
        } catch (Throwable th) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00ff  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] a(byte[] r9, java.lang.String r10) {
        /*
            r8 = this;
            r4 = 0
            r1 = 0
            r3 = 1
            com.umeng.a.a.v r0 = r8.e     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            if (r0 == 0) goto L_0x000c
            com.umeng.a.a.v r0 = r8.e     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            r0.a()     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
        L_0x000c:
            boolean r0 = r8.c()     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            if (r0 == 0) goto L_0x00d4
            java.net.Proxy r0 = new java.net.Proxy     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            java.net.Proxy$Type r2 = java.net.Proxy.Type.HTTP     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            java.net.InetSocketAddress r5 = new java.net.InetSocketAddress     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            java.lang.String r6 = r8.b     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            int r7 = r8.c     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            r5.<init>(r6, r7)     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            r0.<init>(r2, r5)     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            java.net.URL r2 = new java.net.URL     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            r2.<init>(r10)     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            java.net.URLConnection r0 = r2.openConnection(r0)     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            r2 = r0
        L_0x002e:
            java.lang.String r0 = "X-Umeng-UTC"
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x00e7 }
            java.lang.String r5 = java.lang.String.valueOf(r6)     // Catch:{ Throwable -> 0x00e7 }
            r2.setRequestProperty(r0, r5)     // Catch:{ Throwable -> 0x00e7 }
            java.lang.String r0 = "X-Umeng-Sdk"
            java.lang.String r5 = r8.f2747a     // Catch:{ Throwable -> 0x00e7 }
            r2.setRequestProperty(r0, r5)     // Catch:{ Throwable -> 0x00e7 }
            java.lang.String r0 = "Msg-Type"
            java.lang.String r5 = "envelope/json"
            r2.setRequestProperty(r0, r5)     // Catch:{ Throwable -> 0x00e7 }
            java.lang.String r0 = "Content-Type"
            java.lang.String r5 = "envelope/json"
            r2.setRequestProperty(r0, r5)     // Catch:{ Throwable -> 0x00e7 }
            r0 = 10000(0x2710, float:1.4013E-41)
            r2.setConnectTimeout(r0)     // Catch:{ Throwable -> 0x00e7 }
            r0 = 30000(0x7530, float:4.2039E-41)
            r2.setReadTimeout(r0)     // Catch:{ Throwable -> 0x00e7 }
            java.lang.String r0 = "POST"
            r2.setRequestMethod(r0)     // Catch:{ Throwable -> 0x00e7 }
            r0 = 1
            r2.setDoOutput(r0)     // Catch:{ Throwable -> 0x00e7 }
            r0 = 1
            r2.setDoInput(r0)     // Catch:{ Throwable -> 0x00e7 }
            r0 = 0
            r2.setUseCaches(r0)     // Catch:{ Throwable -> 0x00e7 }
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Throwable -> 0x00e7 }
            r5 = 8
            if (r0 >= r5) goto L_0x0078
            java.lang.String r0 = "http.keepAlive"
            java.lang.String r5 = "false"
            java.lang.System.setProperty(r0, r5)     // Catch:{ Throwable -> 0x00e7 }
        L_0x0078:
            java.io.OutputStream r0 = r2.getOutputStream()     // Catch:{ Throwable -> 0x00e7 }
            r0.write(r9)     // Catch:{ Throwable -> 0x00e7 }
            r0.flush()     // Catch:{ Throwable -> 0x00e7 }
            r0.close()     // Catch:{ Throwable -> 0x00e7 }
            com.umeng.a.a.v r0 = r8.e     // Catch:{ Throwable -> 0x00e7 }
            if (r0 == 0) goto L_0x008e
            com.umeng.a.a.v r0 = r8.e     // Catch:{ Throwable -> 0x00e7 }
            r0.b()     // Catch:{ Throwable -> 0x00e7 }
        L_0x008e:
            int r5 = r2.getResponseCode()     // Catch:{ Throwable -> 0x00e7 }
            java.lang.String r0 = "Content-Type"
            java.lang.String r0 = r2.getHeaderField(r0)     // Catch:{ Throwable -> 0x00e7 }
            boolean r6 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Throwable -> 0x00e7 }
            if (r6 != 0) goto L_0x0108
            java.lang.String r6 = "application/thrift"
            boolean r0 = r0.equalsIgnoreCase(r6)     // Catch:{ Throwable -> 0x00e7 }
            if (r0 == 0) goto L_0x0108
            r0 = r3
        L_0x00a7:
            r3 = 200(0xc8, float:2.8E-43)
            if (r5 != r3) goto L_0x00f4
            if (r0 == 0) goto L_0x00f4
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00e7 }
            r0.<init>()     // Catch:{ Throwable -> 0x00e7 }
            java.lang.String r3 = "Send message to "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x00e7 }
            java.lang.StringBuilder r0 = r0.append(r10)     // Catch:{ Throwable -> 0x00e7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00e7 }
            com.umeng.a.a.aw.b(r0)     // Catch:{ Throwable -> 0x00e7 }
            java.io.InputStream r3 = r2.getInputStream()     // Catch:{ Throwable -> 0x00e7 }
            byte[] r0 = com.umeng.a.a.au.b(r3)     // Catch:{ all -> 0x00e2 }
            com.umeng.a.a.au.c(r3)     // Catch:{ Throwable -> 0x00e7 }
            if (r2 == 0) goto L_0x00d3
            r2.disconnect()
        L_0x00d3:
            return r0
        L_0x00d4:
            java.net.URL r0 = new java.net.URL     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            r0.<init>(r10)     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Throwable -> 0x0105, all -> 0x00fb }
            r2 = r0
            goto L_0x002e
        L_0x00e2:
            r0 = move-exception
            com.umeng.a.a.au.c(r3)     // Catch:{ Throwable -> 0x00e7 }
            throw r0     // Catch:{ Throwable -> 0x00e7 }
        L_0x00e7:
            r0 = move-exception
        L_0x00e8:
            java.lang.String r3 = "IOException,Failed to send message."
            com.umeng.a.a.aw.a(r3, r0)     // Catch:{ all -> 0x0103 }
            if (r2 == 0) goto L_0x00f2
            r2.disconnect()
        L_0x00f2:
            r0 = r1
            goto L_0x00d3
        L_0x00f4:
            if (r2 == 0) goto L_0x00f9
            r2.disconnect()
        L_0x00f9:
            r0 = r1
            goto L_0x00d3
        L_0x00fb:
            r0 = move-exception
            r2 = r1
        L_0x00fd:
            if (r2 == 0) goto L_0x0102
            r2.disconnect()
        L_0x0102:
            throw r0
        L_0x0103:
            r0 = move-exception
            goto L_0x00fd
        L_0x0105:
            r0 = move-exception
            r2 = r1
            goto L_0x00e8
        L_0x0108:
            r0 = r4
            goto L_0x00a7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.w.a(byte[], java.lang.String):byte[]");
    }

    private String a(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Android");
        stringBuffer.append("/");
        stringBuffer.append("6.1.1");
        stringBuffer.append(" ");
        try {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(at.s(context));
            stringBuffer2.append("/");
            stringBuffer2.append(at.b(context));
            stringBuffer2.append(" ");
            stringBuffer2.append(Build.MODEL);
            stringBuffer2.append("/");
            stringBuffer2.append(Build.VERSION.RELEASE);
            stringBuffer2.append(" ");
            stringBuffer2.append(au.a(a.a(context)));
            stringBuffer.append(URLEncoder.encode(stringBuffer2.toString(), "UTF-8"));
        } catch (Throwable th) {
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0090, code lost:
        if (r0 != null) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00d5, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00d6, code lost:
        r9 = r1;
        r1 = r0;
        r0 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00db A[SYNTHETIC, Splitter:B:30:0x00db] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x008f A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r10 = this;
            r0 = 0
            java.lang.String r1 = java.security.KeyStore.getDefaultType()     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            java.security.KeyStore r1 = java.security.KeyStore.getInstance(r1)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r2 = 0
            r3 = 0
            r1.load(r2, r3)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            com.umeng.a.a.ab r2 = new com.umeng.a.a.ab     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.conn.ssl.X509HostnameVerifier r1 = org.apache.http.conn.ssl.SSLSocketFactory.STRICT_HOSTNAME_VERIFIER     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r2.setHostnameVerifier(r1)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            java.lang.String r3 = "https://uop.umeng.com"
            r1.<init>(r3)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.params.BasicHttpParams r3 = new org.apache.http.params.BasicHttpParams     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r3.<init>()     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.HttpVersion r4 = org.apache.http.HttpVersion.HTTP_1_1     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.params.HttpProtocolParams.setVersion(r3, r4)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            java.lang.String r4 = "ISO-8859-1"
            org.apache.http.params.HttpProtocolParams.setContentCharset(r3, r4)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r4 = 1
            org.apache.http.params.HttpProtocolParams.setUseExpectContinue(r3, r4)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r4 = 10000(0x2710, double:4.9407E-320)
            org.apache.http.conn.params.ConnManagerParams.setTimeout(r3, r4)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r4 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r3, r4)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r4 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r3, r4)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.conn.scheme.SchemeRegistry r4 = new org.apache.http.conn.scheme.SchemeRegistry     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r4.<init>()     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.conn.scheme.Scheme r5 = new org.apache.http.conn.scheme.Scheme     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            java.lang.String r6 = "http"
            org.apache.http.conn.scheme.PlainSocketFactory r7 = org.apache.http.conn.scheme.PlainSocketFactory.getSocketFactory()     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r8 = 80
            r5.<init>(r6, r7, r8)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r4.register(r5)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.conn.scheme.Scheme r5 = new org.apache.http.conn.scheme.Scheme     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            java.lang.String r6 = "https"
            r7 = 443(0x1bb, float:6.21E-43)
            r5.<init>(r6, r2, r7)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r4.register(r5)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager r2 = new org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r2.<init>(r3, r4)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.impl.client.DefaultHttpClient r4 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            r4.<init>(r2, r3)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.HttpResponse r1 = r4.execute(r1)     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            org.apache.http.HttpEntity r1 = r1.getEntity()     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            java.io.InputStream r0 = r1.getContent()     // Catch:{ Throwable -> 0x008f, all -> 0x00d5 }
            if (r0 == 0) goto L_0x00cd
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            r1.<init>()     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
        L_0x0083:
            int r3 = r0.read(r2)     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            r4 = -1
            if (r3 == r4) goto L_0x0096
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            goto L_0x0083
        L_0x008f:
            r1 = move-exception
            if (r0 == 0) goto L_0x0095
            r0.close()     // Catch:{ Throwable -> 0x00df }
        L_0x0095:
            return
        L_0x0096:
            r1.close()     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            java.lang.String r2 = new java.lang.String     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            byte[] r1 = r1.toByteArray()     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            java.lang.String r3 = "UTF-8"
            r2.<init>(r1, r3)     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            boolean r1 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            if (r1 != 0) goto L_0x00cd
            int r1 = r2.length()     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            if (r1 <= 0) goto L_0x00cd
            int r1 = r2.length()     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            r3 = 50
            if (r1 >= r3) goto L_0x00cd
            android.content.Context r1 = r10.d     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            android.content.SharedPreferences r1 = com.umeng.a.a.aa.a(r1)     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            if (r1 == 0) goto L_0x00cd
            android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            java.lang.String r3 = "uopdta"
            android.content.SharedPreferences$Editor r1 = r1.putString(r3, r2)     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
            r1.commit()     // Catch:{ Throwable -> 0x008f, all -> 0x00e3 }
        L_0x00cd:
            if (r0 == 0) goto L_0x0095
            r0.close()     // Catch:{ Throwable -> 0x00d3 }
            goto L_0x0095
        L_0x00d3:
            r0 = move-exception
            goto L_0x0095
        L_0x00d5:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x00d9:
            if (r1 == 0) goto L_0x00de
            r1.close()     // Catch:{ Throwable -> 0x00e1 }
        L_0x00de:
            throw r0
        L_0x00df:
            r0 = move-exception
            goto L_0x0095
        L_0x00e1:
            r1 = move-exception
            goto L_0x00de
        L_0x00e3:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x00d9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.w.a():void");
    }
}
