package com.umeng.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Base64;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

/* compiled from: UMStoreManager */
public class cx {

    /* renamed from: a  reason: collision with root package name */
    private static Context f2717a = null;
    private static String b = null;
    private List<String> c;

    /* compiled from: UMStoreManager */
    public enum a {
        AUTOPAGE,
        PAGE,
        BEGIN,
        END,
        NEWSESSION
    }

    /* compiled from: UMStoreManager */
    private static class b {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final cx f2719a = new cx();
    }

    private cx() {
        this.c = new ArrayList();
        if (f2717a != null) {
            b();
            this.c.clear();
        }
    }

    public static final cx a(Context context) {
        f2717a = context;
        return b.f2719a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0088, code lost:
        r0.endTransaction();
        com.umeng.a.a.cv.a(com.umeng.a.a.cx.f2717a).b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0095, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0096, code lost:
        r6 = r1;
        r1 = r0;
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0087 A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(org.json.JSONArray r8) {
        /*
            r7 = this;
            r0 = 0
            android.content.Context r1 = com.umeng.a.a.cx.f2717a     // Catch:{ Throwable -> 0x0087, all -> 0x0095 }
            com.umeng.a.a.cv r1 = com.umeng.a.a.cv.a(r1)     // Catch:{ Throwable -> 0x0087, all -> 0x0095 }
            android.database.sqlite.SQLiteDatabase r0 = r1.a()     // Catch:{ Throwable -> 0x0087, all -> 0x0095 }
            r0.beginTransaction()     // Catch:{ Throwable -> 0x0087, all -> 0x00a6 }
            r1 = 0
        L_0x000f:
            int r2 = r8.length()     // Catch:{ Throwable -> 0x0087, all -> 0x00a6 }
            if (r1 >= r2) goto L_0x0077
            org.json.JSONObject r3 = r8.getJSONObject(r1)     // Catch:{ Exception -> 0x00ab }
            android.content.ContentValues r4 = new android.content.ContentValues     // Catch:{ Exception -> 0x00ab }
            r4.<init>()     // Catch:{ Exception -> 0x00ab }
            java.lang.String r2 = "__i"
            java.lang.String r2 = r3.optString(r2)     // Catch:{ Exception -> 0x00ab }
            boolean r5 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x00ab }
            if (r5 == 0) goto L_0x0038
            android.content.Context r2 = com.umeng.a.a.cx.f2717a     // Catch:{ Exception -> 0x00ab }
            java.lang.String r2 = com.umeng.a.a.ad.g(r2)     // Catch:{ Exception -> 0x00ab }
            boolean r5 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x00ab }
            if (r5 == 0) goto L_0x0038
            java.lang.String r2 = ""
        L_0x0038:
            java.lang.String r5 = "__i"
            r4.put(r5, r2)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r2 = "__e"
            java.lang.String r5 = "id"
            java.lang.String r5 = r3.optString(r5)     // Catch:{ Exception -> 0x00ab }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r2 = "__t"
            java.lang.String r5 = "__t"
            int r5 = r3.optInt(r5)     // Catch:{ Exception -> 0x00ab }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x00ab }
            r4.put(r2, r5)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r2 = "__i"
            r3.remove(r2)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r2 = "__t"
            r3.remove(r2)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r2 = "__s"
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ab }
            java.lang.String r3 = r7.a(r3)     // Catch:{ Exception -> 0x00ab }
            r4.put(r2, r3)     // Catch:{ Exception -> 0x00ab }
            java.lang.String r2 = "__et"
            r3 = 0
            r0.insert(r2, r3, r4)     // Catch:{ Exception -> 0x00ab }
        L_0x0074:
            int r1 = r1 + 1
            goto L_0x000f
        L_0x0077:
            r0.setTransactionSuccessful()     // Catch:{ Throwable -> 0x0087, all -> 0x00a6 }
            r0.endTransaction()
            android.content.Context r0 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r0 = com.umeng.a.a.cv.a(r0)
            r0.b()
        L_0x0086:
            return
        L_0x0087:
            r1 = move-exception
            r0.endTransaction()
            android.content.Context r0 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r0 = com.umeng.a.a.cv.a(r0)
            r0.b()
            goto L_0x0086
        L_0x0095:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0099:
            r1.endTransaction()
            android.content.Context r1 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r1 = com.umeng.a.a.cv.a(r1)
            r1.b()
            throw r0
        L_0x00a6:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0099
        L_0x00ab:
            r2 = move-exception
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.cx.a(org.json.JSONArray):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0048, code lost:
        r0.endTransaction();
        com.umeng.a.a.cv.a(com.umeng.a.a.cx.f2717a).b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0055, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0056, code lost:
        r4 = r1;
        r1 = r0;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0047 A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r6, java.lang.String r7, int r8) {
        /*
            r5 = this;
            r0 = 0
            android.content.Context r1 = com.umeng.a.a.cx.f2717a     // Catch:{ Throwable -> 0x0047, all -> 0x0055 }
            com.umeng.a.a.cv r1 = com.umeng.a.a.cv.a(r1)     // Catch:{ Throwable -> 0x0047, all -> 0x0055 }
            android.database.sqlite.SQLiteDatabase r0 = r1.a()     // Catch:{ Throwable -> 0x0047, all -> 0x0055 }
            r0.beginTransaction()     // Catch:{ Throwable -> 0x0047, all -> 0x0066 }
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ Throwable -> 0x0047, all -> 0x0066 }
            r1.<init>()     // Catch:{ Throwable -> 0x0047, all -> 0x0066 }
            java.lang.String r2 = "__i"
            r1.put(r2, r6)     // Catch:{ Throwable -> 0x0047, all -> 0x0066 }
            java.lang.String r2 = r5.a(r7)     // Catch:{ Throwable -> 0x0047, all -> 0x0066 }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Throwable -> 0x0047, all -> 0x0066 }
            if (r3 != 0) goto L_0x0036
            java.lang.String r3 = "__a"
            r1.put(r3, r2)     // Catch:{ Throwable -> 0x0047, all -> 0x0066 }
            java.lang.String r2 = "__t"
            java.lang.Integer r3 = java.lang.Integer.valueOf(r8)     // Catch:{ Throwable -> 0x0047, all -> 0x0066 }
            r1.put(r2, r3)     // Catch:{ Throwable -> 0x0047, all -> 0x0066 }
            java.lang.String r2 = "__er"
            r3 = 0
            r0.insert(r2, r3, r1)     // Catch:{ Throwable -> 0x0047, all -> 0x0066 }
        L_0x0036:
            r0.setTransactionSuccessful()     // Catch:{ Throwable -> 0x0047, all -> 0x0066 }
            r0.endTransaction()
            android.content.Context r0 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r0 = com.umeng.a.a.cv.a(r0)
            r0.b()
        L_0x0045:
            r0 = 0
            return r0
        L_0x0047:
            r1 = move-exception
            r0.endTransaction()
            android.content.Context r0 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r0 = com.umeng.a.a.cv.a(r0)
            r0.b()
            goto L_0x0045
        L_0x0055:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0059:
            r1.endTransaction()
            android.content.Context r1 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r1 = com.umeng.a.a.cv.a(r1)
            r1.b()
            throw r0
        L_0x0066:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.cx.a(java.lang.String, java.lang.String, int):boolean");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r1v4, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARN: Type inference failed for: r1v9 */
    /* JADX WARN: Type inference failed for: r1v46 */
    /* JADX WARN: Type inference failed for: r1v51 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01e8  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r9, org.json.JSONObject r10, com.umeng.a.a.cx.a r11) {
        /*
            r8 = this;
            r6 = 0
            r1 = 0
            if (r10 != 0) goto L_0x0005
        L_0x0004:
            return r6
        L_0x0005:
            android.content.Context r0 = com.umeng.a.a.cx.f2717a     // Catch:{ Throwable -> 0x01cf, all -> 0x01e4 }
            com.umeng.a.a.cv r0 = com.umeng.a.a.cv.a(r0)     // Catch:{ Throwable -> 0x01cf, all -> 0x01e4 }
            android.database.sqlite.SQLiteDatabase r2 = r0.a()     // Catch:{ Throwable -> 0x01cf, all -> 0x01e4 }
            r2.beginTransaction()     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            com.umeng.a.a.cx$a r0 = com.umeng.a.a.cx.a.BEGIN     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            if (r11 != r0) goto L_0x0051
            java.lang.String r0 = "__e"
            java.lang.Object r0 = r10.get(r0)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            long r4 = r0.longValue()     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            r0.<init>()     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r3 = "__ii"
            r0.put(r3, r9)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r3 = "__e"
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            r0.put(r3, r4)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r3 = "__sd"
            r4 = 0
            r2.insert(r3, r4, r0)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            r0 = r1
        L_0x003c:
            r2.setTransactionSuccessful()     // Catch:{ Throwable -> 0x0203, all -> 0x01fa }
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            r2.endTransaction()
            android.content.Context r0 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r0 = com.umeng.a.a.cv.a(r0)
            r0.b()
            goto L_0x0004
        L_0x0051:
            com.umeng.a.a.cx$a r0 = com.umeng.a.a.cx.a.END     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            if (r11 != r0) goto L_0x0095
            java.lang.String r0 = "__f"
            java.lang.Object r0 = r10.get(r0)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            long r4 = r0.longValue()     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            r0.<init>()     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r3 = "update __sd set __f=\""
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r3 = "\" where "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r3 = "__ii"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r3 = "=\""
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r3 = "\""
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            r2.execSQL(r0)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            r0 = r1
            goto L_0x003c
        L_0x0095:
            com.umeng.a.a.cx$a r0 = com.umeng.a.a.cx.a.PAGE     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            if (r11 != r0) goto L_0x00a0
            java.lang.String r0 = "__a"
            r8.a(r9, r10, r2, r0)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            r0 = r1
            goto L_0x003c
        L_0x00a0:
            com.umeng.a.a.cx$a r0 = com.umeng.a.a.cx.a.AUTOPAGE     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            if (r11 != r0) goto L_0x00ab
            java.lang.String r0 = "__b"
            r8.a(r9, r10, r2, r0)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            r0 = r1
            goto L_0x003c
        L_0x00ab:
            com.umeng.a.a.cx$a r0 = com.umeng.a.a.cx.a.NEWSESSION     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            if (r11 != r0) goto L_0x0212
            java.lang.String r0 = "__d"
            org.json.JSONObject r0 = r10.getJSONObject(r0)     // Catch:{ Exception -> 0x00ed }
            r4 = r0
        L_0x00b6:
            if (r4 == 0) goto L_0x020e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            r0.<init>()     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r3 = "select __d from __sd where __ii=\""
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r3 = "\""
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            r3 = 0
            android.database.Cursor r0 = r2.rawQuery(r0, r3)     // Catch:{ Throwable -> 0x01ff, all -> 0x01f8 }
            if (r0 == 0) goto L_0x020b
        L_0x00d8:
            boolean r3 = r0.moveToNext()     // Catch:{ Throwable -> 0x0203, all -> 0x01fa }
            if (r3 == 0) goto L_0x00f0
            java.lang.String r1 = "__d"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ Throwable -> 0x0203, all -> 0x01fa }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Throwable -> 0x0203, all -> 0x01fa }
            java.lang.String r1 = r8.b(r1)     // Catch:{ Throwable -> 0x0203, all -> 0x01fa }
            goto L_0x00d8
        L_0x00ed:
            r0 = move-exception
            r4 = r1
            goto L_0x00b6
        L_0x00f0:
            r3 = r1
        L_0x00f1:
            if (r4 == 0) goto L_0x0146
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Exception -> 0x0208 }
            r1.<init>()     // Catch:{ Exception -> 0x0208 }
            boolean r5 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x0208 }
            if (r5 != 0) goto L_0x0103
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Exception -> 0x0208 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0208 }
        L_0x0103:
            r1.put(r4)     // Catch:{ Exception -> 0x0208 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0208 }
            java.lang.String r1 = r8.a(r1)     // Catch:{ Exception -> 0x0208 }
            boolean r3 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0208 }
            if (r3 != 0) goto L_0x0146
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0208 }
            r3.<init>()     // Catch:{ Exception -> 0x0208 }
            java.lang.String r4 = "update  __sd set __d=\""
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0208 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0208 }
            java.lang.String r3 = "\" where "
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0208 }
            java.lang.String r3 = "__ii"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0208 }
            java.lang.String r3 = "=\""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0208 }
            java.lang.StringBuilder r1 = r1.append(r9)     // Catch:{ Exception -> 0x0208 }
            java.lang.String r3 = "\""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0208 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0208 }
            r2.execSQL(r1)     // Catch:{ Exception -> 0x0208 }
        L_0x0146:
            java.lang.String r1 = "__c"
            org.json.JSONObject r1 = r10.getJSONObject(r1)     // Catch:{ Exception -> 0x0206 }
            if (r1 == 0) goto L_0x018e
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0206 }
            java.lang.String r1 = r8.a(r1)     // Catch:{ Exception -> 0x0206 }
            boolean r3 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0206 }
            if (r3 != 0) goto L_0x018e
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0206 }
            r3.<init>()     // Catch:{ Exception -> 0x0206 }
            java.lang.String r4 = "update  __sd set __c=\""
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0206 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r3 = "\" where "
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r3 = "__ii"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r3 = "=\""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0206 }
            java.lang.StringBuilder r1 = r1.append(r9)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r3 = "\""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0206 }
            r2.execSQL(r1)     // Catch:{ Exception -> 0x0206 }
        L_0x018e:
            java.lang.String r1 = "__f"
            long r4 = r10.getLong(r1)     // Catch:{ Exception -> 0x01cc }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cc }
            r1.<init>()     // Catch:{ Exception -> 0x01cc }
            java.lang.String r3 = "update  __sd set __f=\""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x01cc }
            java.lang.String r3 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x01cc }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x01cc }
            java.lang.String r3 = "\" where "
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x01cc }
            java.lang.String r3 = "__ii"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x01cc }
            java.lang.String r3 = "=\""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x01cc }
            java.lang.StringBuilder r1 = r1.append(r9)     // Catch:{ Exception -> 0x01cc }
            java.lang.String r3 = "\""
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x01cc }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01cc }
            r2.execSQL(r1)     // Catch:{ Exception -> 0x01cc }
            goto L_0x003c
        L_0x01cc:
            r1 = move-exception
            goto L_0x003c
        L_0x01cf:
            r0 = move-exception
            r0 = r1
        L_0x01d1:
            if (r0 == 0) goto L_0x01d6
            r0.close()
        L_0x01d6:
            r1.endTransaction()
            android.content.Context r0 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r0 = com.umeng.a.a.cv.a(r0)
            r0.b()
            goto L_0x0004
        L_0x01e4:
            r0 = move-exception
            r2 = r1
        L_0x01e6:
            if (r1 == 0) goto L_0x01eb
            r1.close()
        L_0x01eb:
            r2.endTransaction()
            android.content.Context r1 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r1 = com.umeng.a.a.cv.a(r1)
            r1.b()
            throw r0
        L_0x01f8:
            r0 = move-exception
            goto L_0x01e6
        L_0x01fa:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x01e6
        L_0x01ff:
            r0 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x01d1
        L_0x0203:
            r1 = move-exception
            r1 = r2
            goto L_0x01d1
        L_0x0206:
            r1 = move-exception
            goto L_0x018e
        L_0x0208:
            r1 = move-exception
            goto L_0x0146
        L_0x020b:
            r3 = r1
            goto L_0x00f1
        L_0x020e:
            r3 = r1
            r0 = r1
            goto L_0x00f1
        L_0x0212:
            r0 = r1
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.cx.a(java.lang.String, org.json.JSONObject, com.umeng.a.a.cx$a):boolean");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v8 */
    /* JADX WARN: Type inference failed for: r2v9, types: [java.lang.CharSequence, java.lang.String] */
    /* JADX WARN: Type inference failed for: r2v20 */
    /* JADX WARN: Type inference failed for: r1v20, types: [java.lang.String] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c5  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r6, org.json.JSONObject r7, android.database.sqlite.SQLiteDatabase r8, java.lang.String r9) throws org.json.JSONException {
        /*
            r5 = this;
            r1 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            r0.<init>()     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            java.lang.String r2 = "select "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            java.lang.String r2 = " from "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            java.lang.String r2 = "__sd"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            java.lang.String r2 = " where "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            java.lang.String r2 = "__ii"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            java.lang.String r2 = "=\""
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            java.lang.String r2 = "\""
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            r2 = 0
            android.database.Cursor r0 = r8.rawQuery(r0, r2)     // Catch:{ Throwable -> 0x00ba, all -> 0x00c2 }
            if (r0 == 0) goto L_0x00d0
        L_0x0043:
            boolean r2 = r0.moveToNext()     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            if (r2 == 0) goto L_0x0056
            int r1 = r0.getColumnIndex(r9)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.String r1 = r5.b(r1)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            goto L_0x0043
        L_0x0056:
            r2 = r1
        L_0x0057:
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            r1.<init>()     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            if (r3 != 0) goto L_0x0067
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
        L_0x0067:
            r1.put(r7)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.String r1 = r5.a(r1)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            if (r2 != 0) goto L_0x00b4
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            r2.<init>()     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.String r3 = "update __sd set "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.String r3 = "=\""
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.String r2 = "\" where "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.String r2 = "__ii"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.String r2 = "=\""
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.String r2 = "\""
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
            r8.execSQL(r1)     // Catch:{ Throwable -> 0x00ce, all -> 0x00c9 }
        L_0x00b4:
            if (r0 == 0) goto L_0x00b9
            r0.close()
        L_0x00b9:
            return
        L_0x00ba:
            r0 = move-exception
            r0 = r1
        L_0x00bc:
            if (r0 == 0) goto L_0x00b9
            r0.close()
            goto L_0x00b9
        L_0x00c2:
            r0 = move-exception
        L_0x00c3:
            if (r1 == 0) goto L_0x00c8
            r1.close()
        L_0x00c8:
            throw r0
        L_0x00c9:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x00c3
        L_0x00ce:
            r1 = move-exception
            goto L_0x00bc
        L_0x00d0:
            r2 = r1
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.cx.a(java.lang.String, org.json.JSONObject, android.database.sqlite.SQLiteDatabase, java.lang.String):void");
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        c(jSONObject2);
        b(jSONObject2);
        a(jSONObject2);
        try {
            if (jSONObject2.length() > 0) {
                jSONObject.put("body", jSONObject2);
            }
        } catch (Throwable th) {
        }
        return jSONObject;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0093  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(org.json.JSONObject r12) {
        /*
            r11 = this;
            r0 = 0
            android.content.Context r1 = com.umeng.a.a.cx.f2717a     // Catch:{ Throwable -> 0x016f, all -> 0x0162 }
            com.umeng.a.a.cv r1 = com.umeng.a.a.cv.a(r1)     // Catch:{ Throwable -> 0x016f, all -> 0x0162 }
            android.database.sqlite.SQLiteDatabase r2 = r1.a()     // Catch:{ Throwable -> 0x016f, all -> 0x0162 }
            r2.beginTransaction()     // Catch:{ Throwable -> 0x0173, all -> 0x0169 }
            java.lang.String r1 = "select *  from __et"
            r3 = 0
            android.database.Cursor r1 = r2.rawQuery(r1, r3)     // Catch:{ Throwable -> 0x0173, all -> 0x0169 }
            if (r1 == 0) goto L_0x014c
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r4.<init>()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r5.<init>()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
        L_0x0021:
            boolean r0 = r1.moveToNext()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r0 == 0) goto L_0x00ca
            java.lang.String r0 = "__t"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            int r3 = r1.getInt(r0)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.String r0 = "__i"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.String r6 = "__s"
            int r6 = r1.getColumnIndex(r6)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.String r6 = r1.getString(r6)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.String r7 = ""
            boolean r7 = r7.equals(r0)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r7 == 0) goto L_0x0051
            java.lang.String r0 = com.umeng.a.a.ad.a()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
        L_0x0051:
            switch(r3) {
                case 2049: goto L_0x0055;
                case 2050: goto L_0x00a3;
                default: goto L_0x0054;
            }     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
        L_0x0054:
            goto L_0x0021
        L_0x0055:
            boolean r3 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r3 != 0) goto L_0x0021
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.String r3 = r11.b(r6)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r7.<init>(r3)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            boolean r3 = r4.has(r0)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r3 == 0) goto L_0x008a
            org.json.JSONArray r3 = r4.optJSONArray(r0)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
        L_0x006e:
            r3.put(r7)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r4.put(r0, r3)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            goto L_0x0021
        L_0x0075:
            r0 = move-exception
            r0 = r1
            r1 = r2
        L_0x0078:
            if (r0 == 0) goto L_0x007d
            r0.close()
        L_0x007d:
            r1.endTransaction()
            android.content.Context r0 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r0 = com.umeng.a.a.cv.a(r0)
            r0.b()
        L_0x0089:
            return
        L_0x008a:
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r3.<init>()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            goto L_0x006e
        L_0x0090:
            r0 = move-exception
        L_0x0091:
            if (r1 == 0) goto L_0x0096
            r1.close()
        L_0x0096:
            r2.endTransaction()
            android.content.Context r1 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r1 = com.umeng.a.a.cv.a(r1)
            r1.b()
            throw r0
        L_0x00a3:
            boolean r3 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r3 != 0) goto L_0x0021
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.String r3 = r11.b(r6)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r7.<init>(r3)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            boolean r3 = r5.has(r0)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r3 == 0) goto L_0x00c4
            org.json.JSONArray r3 = r5.optJSONArray(r0)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
        L_0x00bc:
            r3.put(r7)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r5.put(r0, r3)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            goto L_0x0021
        L_0x00c4:
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r3.<init>()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            goto L_0x00bc
        L_0x00ca:
            int r0 = r4.length()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r0 <= 0) goto L_0x010b
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r3.<init>()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.util.Iterator r6 = r4.keys()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
        L_0x00d9:
            boolean r0 = r6.hasNext()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r0 == 0) goto L_0x0100
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r7.<init>()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.Object r0 = r6.next()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.String r8 = r4.optString(r0)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            org.json.JSONArray r9 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r9.<init>(r8)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r7.put(r0, r9)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            int r0 = r7.length()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r0 <= 0) goto L_0x00d9
            r3.put(r7)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            goto L_0x00d9
        L_0x0100:
            int r0 = r3.length()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r0 <= 0) goto L_0x010b
            java.lang.String r0 = "ekv"
            r12.put(r0, r3)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
        L_0x010b:
            int r0 = r5.length()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r0 <= 0) goto L_0x014c
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r3.<init>()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.util.Iterator r4 = r5.keys()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
        L_0x011a:
            boolean r0 = r4.hasNext()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r0 == 0) goto L_0x0141
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r6.<init>()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.Object r0 = r4.next()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            java.lang.String r7 = r5.optString(r0)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            org.json.JSONArray r8 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r8.<init>(r7)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            r6.put(r0, r8)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            int r0 = r6.length()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r0 <= 0) goto L_0x011a
            r3.put(r6)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            goto L_0x011a
        L_0x0141:
            int r0 = r3.length()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r0 <= 0) goto L_0x014c
            java.lang.String r0 = "gkv"
            r12.put(r0, r3)     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
        L_0x014c:
            r2.setTransactionSuccessful()     // Catch:{ Throwable -> 0x0075, all -> 0x0090 }
            if (r1 == 0) goto L_0x0154
            r1.close()
        L_0x0154:
            r2.endTransaction()
            android.content.Context r0 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r0 = com.umeng.a.a.cv.a(r0)
            r0.b()
            goto L_0x0089
        L_0x0162:
            r1 = move-exception
            r2 = r0
            r10 = r0
            r0 = r1
            r1 = r10
            goto L_0x0091
        L_0x0169:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0091
        L_0x016f:
            r1 = move-exception
            r1 = r0
            goto L_0x0078
        L_0x0173:
            r1 = move-exception
            r1 = r2
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.cx.a(org.json.JSONObject):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: android.database.Cursor} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v7, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: android.database.sqlite.SQLiteDatabase} */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003f, code lost:
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0090, code lost:
        r5 = r2;
        r2 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003f A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:3:0x000b] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0079  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(org.json.JSONObject r7) {
        /*
            r6 = this;
            r0 = 0
            android.content.Context r1 = com.umeng.a.a.cx.f2717a     // Catch:{ Throwable -> 0x0095, all -> 0x0072 }
            com.umeng.a.a.cv r1 = com.umeng.a.a.cv.a(r1)     // Catch:{ Throwable -> 0x0095, all -> 0x0072 }
            android.database.sqlite.SQLiteDatabase r1 = r1.a()     // Catch:{ Throwable -> 0x0095, all -> 0x0072 }
            r1.beginTransaction()     // Catch:{ Throwable -> 0x003f, all -> 0x0089 }
            java.lang.String r2 = "select *  from __er"
            r3 = 0
            android.database.Cursor r0 = r1.rawQuery(r2, r3)     // Catch:{ Throwable -> 0x003f, all -> 0x0089 }
            if (r0 == 0) goto L_0x005d
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
            r2.<init>()     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
        L_0x001c:
            boolean r3 = r0.moveToNext()     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
            if (r3 == 0) goto L_0x0052
            java.lang.String r3 = "__a"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
            boolean r4 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
            if (r4 != 0) goto L_0x001c
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
            java.lang.String r3 = r6.b(r3)     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
            r4.<init>(r3)     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
            r2.put(r4)     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
            goto L_0x001c
        L_0x003f:
            r2 = move-exception
        L_0x0040:
            if (r0 == 0) goto L_0x0045
            r0.close()
        L_0x0045:
            r1.endTransaction()
            android.content.Context r0 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r0 = com.umeng.a.a.cv.a(r0)
            r0.b()
        L_0x0051:
            return
        L_0x0052:
            int r3 = r2.length()     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
            if (r3 <= 0) goto L_0x005d
            java.lang.String r3 = "error"
            r7.put(r3, r2)     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
        L_0x005d:
            r1.setTransactionSuccessful()     // Catch:{ Throwable -> 0x003f, all -> 0x008f }
            if (r0 == 0) goto L_0x0065
            r0.close()
        L_0x0065:
            r1.endTransaction()
            android.content.Context r0 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r0 = com.umeng.a.a.cv.a(r0)
            r0.b()
            goto L_0x0051
        L_0x0072:
            r1 = move-exception
            r2 = r0
            r5 = r0
            r0 = r1
            r1 = r5
        L_0x0077:
            if (r1 == 0) goto L_0x007c
            r1.close()
        L_0x007c:
            r2.endTransaction()
            android.content.Context r1 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r1 = com.umeng.a.a.cv.a(r1)
            r1.b()
            throw r0
        L_0x0089:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x0077
        L_0x008f:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r0
            r0 = r5
            goto L_0x0077
        L_0x0095:
            r1 = move-exception
            r1 = r0
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.cx.b(org.json.JSONObject):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0119, code lost:
        if (r2 != null) goto L_0x011b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x011b, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x011e, code lost:
        r3.endTransaction();
        com.umeng.a.a.cv.a(com.umeng.a.a.cx.f2717a).b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x014d, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x014e, code lost:
        r16 = r4;
        r4 = r3;
        r3 = r2;
        r2 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0156, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0166, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0167, code lost:
        r16 = r4;
        r4 = r3;
        r3 = r2;
        r2 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0118 A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:1:0x0002] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0156  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c(org.json.JSONObject r18) {
        /*
            r17 = this;
            r3 = 0
            r2 = 0
            android.content.Context r4 = com.umeng.a.a.cx.f2717a     // Catch:{ Throwable -> 0x0118, all -> 0x014d }
            com.umeng.a.a.cv r4 = com.umeng.a.a.cv.a(r4)     // Catch:{ Throwable -> 0x0118, all -> 0x014d }
            android.database.sqlite.SQLiteDatabase r3 = r4.a()     // Catch:{ Throwable -> 0x0118, all -> 0x014d }
            r3.beginTransaction()     // Catch:{ Throwable -> 0x0118, all -> 0x0166 }
            java.lang.String r4 = "select *  from __sd"
            r5 = 0
            android.database.Cursor r2 = r3.rawQuery(r4, r5)     // Catch:{ Throwable -> 0x0118, all -> 0x0166 }
            if (r2 == 0) goto L_0x0138
            org.json.JSONArray r4 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r4.<init>()     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r0 = r17
            java.util.List<java.lang.String> r5 = r0.c     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r5.clear()     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
        L_0x0024:
            boolean r5 = r2.moveToNext()     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            if (r5 == 0) goto L_0x012b
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r5.<init>()     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r6 = "__f"
            int r6 = r2.getColumnIndex(r6)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r6 = r2.getString(r6)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r7 = "__e"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            boolean r8 = android.text.TextUtils.isEmpty(r6)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            if (r8 != 0) goto L_0x0024
            boolean r8 = android.text.TextUtils.isEmpty(r7)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            if (r8 != 0) goto L_0x0024
            long r8 = java.lang.Long.parseLong(r6)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            long r10 = java.lang.Long.parseLong(r7)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            long r8 = r8 - r10
            r10 = 0
            int r8 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r8 <= 0) goto L_0x0024
            java.lang.String r8 = "__a"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r8 = r2.getString(r8)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r9 = "__b"
            int r9 = r2.getColumnIndex(r9)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r9 = r2.getString(r9)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r10 = "__c"
            int r10 = r2.getColumnIndex(r10)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r10 = r2.getString(r10)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r11 = "__d"
            int r11 = r2.getColumnIndex(r11)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r11 = r2.getString(r11)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r12 = "__ii"
            int r12 = r2.getColumnIndex(r12)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r12 = r2.getString(r12)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r0 = r17
            java.util.List<java.lang.String> r13 = r0.c     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r13.add(r12)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r13 = "id"
            r5.put(r13, r12)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r12 = "start_time"
            r5.put(r12, r7)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r12 = "end_time"
            r5.put(r12, r6)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            java.lang.String r12 = "duration"
            long r14 = java.lang.Long.parseLong(r6)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            long r6 = java.lang.Long.parseLong(r7)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            long r6 = r14 - r6
            r5.put(r12, r6)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            boolean r6 = android.text.TextUtils.isEmpty(r8)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            if (r6 != 0) goto L_0x00cb
            java.lang.String r6 = "pages"
            org.json.JSONArray r7 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r0 = r17
            java.lang.String r8 = r0.b(r8)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r7.<init>(r8)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r5.put(r6, r7)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
        L_0x00cb:
            boolean r6 = android.text.TextUtils.isEmpty(r9)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            if (r6 != 0) goto L_0x00e1
            java.lang.String r6 = "autopages"
            org.json.JSONArray r7 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r0 = r17
            java.lang.String r8 = r0.b(r9)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r7.<init>(r8)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r5.put(r6, r7)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
        L_0x00e1:
            boolean r6 = android.text.TextUtils.isEmpty(r10)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            if (r6 != 0) goto L_0x00f7
            java.lang.String r6 = "traffic"
            org.json.JSONObject r7 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r0 = r17
            java.lang.String r8 = r0.b(r10)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r7.<init>(r8)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r5.put(r6, r7)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
        L_0x00f7:
            boolean r6 = android.text.TextUtils.isEmpty(r11)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            if (r6 != 0) goto L_0x010d
            java.lang.String r6 = "locations"
            org.json.JSONArray r7 = new org.json.JSONArray     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r0 = r17
            java.lang.String r8 = r0.b(r11)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r7.<init>(r8)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            r5.put(r6, r7)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
        L_0x010d:
            int r6 = r5.length()     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            if (r6 <= 0) goto L_0x0024
            r4.put(r5)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            goto L_0x0024
        L_0x0118:
            r4 = move-exception
            if (r2 == 0) goto L_0x011e
            r2.close()
        L_0x011e:
            r3.endTransaction()
            android.content.Context r2 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r2 = com.umeng.a.a.cv.a(r2)
            r2.b()
        L_0x012a:
            return
        L_0x012b:
            int r5 = r4.length()     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            if (r5 <= 0) goto L_0x0138
            java.lang.String r5 = "sessions"
            r0 = r18
            r0.put(r5, r4)     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
        L_0x0138:
            r3.setTransactionSuccessful()     // Catch:{ Throwable -> 0x0118, all -> 0x016e }
            if (r2 == 0) goto L_0x0140
            r2.close()
        L_0x0140:
            r3.endTransaction()
            android.content.Context r2 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r2 = com.umeng.a.a.cv.a(r2)
            r2.b()
            goto L_0x012a
        L_0x014d:
            r4 = move-exception
            r16 = r4
            r4 = r3
            r3 = r2
            r2 = r16
        L_0x0154:
            if (r3 == 0) goto L_0x0159
            r3.close()
        L_0x0159:
            r4.endTransaction()
            android.content.Context r3 = com.umeng.a.a.cx.f2717a
            com.umeng.a.a.cv r3 = com.umeng.a.a.cv.a(r3)
            r3.b()
            throw r2
        L_0x0166:
            r4 = move-exception
            r16 = r4
            r4 = r3
            r3 = r2
            r2 = r16
            goto L_0x0154
        L_0x016e:
            r4 = move-exception
            r16 = r4
            r4 = r3
            r3 = r2
            r2 = r16
            goto L_0x0154
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.cx.c(org.json.JSONObject):void");
    }

    public void a(boolean z, boolean z2) {
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        SQLiteDatabase sQLiteDatabase2 = null;
        try {
            sQLiteDatabase = cv.a(f2717a).a();
            try {
                sQLiteDatabase.beginTransaction();
                sQLiteDatabase.execSQL("delete from __er");
                sQLiteDatabase.execSQL("delete from __et");
                if (!z2) {
                    if (this.c.size() > 0) {
                        for (int i = 0; i < this.c.size(); i++) {
                            sQLiteDatabase.execSQL("delete from __sd where __ii=\"" + this.c.get(i) + "\"");
                        }
                    }
                    this.c.clear();
                } else if (z) {
                    sQLiteDatabase.execSQL("delete from __sd");
                }
                sQLiteDatabase.setTransactionSuccessful();
                sQLiteDatabase.endTransaction();
                cv.a(f2717a).b();
            } catch (Throwable th2) {
                th = th2;
                sQLiteDatabase.endTransaction();
                cv.a(f2717a).b();
                throw th;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            sQLiteDatabase = null;
            th = th4;
            sQLiteDatabase.endTransaction();
            cv.a(f2717a).b();
            throw th;
        }
    }

    private void b() {
        try {
            if (TextUtils.isEmpty(b)) {
                SharedPreferences a2 = aa.a(f2717a);
                String string = a2.getString("ek__id", null);
                if (TextUtils.isEmpty(string)) {
                    string = at.w(f2717a);
                    if (!TextUtils.isEmpty(string)) {
                        a2.edit().putString("ek__id", string).commit();
                    }
                }
                if (!TextUtils.isEmpty(string)) {
                    String substring = string.substring(1, 9);
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < substring.length(); i++) {
                        char charAt = substring.charAt(i);
                        if (!Character.isDigit(charAt)) {
                            sb.append(charAt);
                        } else if (Integer.parseInt(Character.toString(charAt)) == 0) {
                            sb.append(0);
                        } else {
                            sb.append(10 - Integer.parseInt(Character.toString(charAt)));
                        }
                    }
                    b = sb.toString();
                }
                if (!TextUtils.isEmpty(b)) {
                    b += new StringBuilder(b).reverse().toString();
                    String string2 = a2.getString("ek_key", null);
                    if (TextUtils.isEmpty(string2)) {
                        a2.edit().putString("ek_key", a("umeng+")).commit();
                    } else if (!"umeng+".equals(b(string2))) {
                        a(true, false);
                    }
                }
            }
        } catch (Throwable th) {
        }
    }

    public String a(String str) {
        try {
            if (TextUtils.isEmpty(b)) {
                return str;
            }
            return Base64.encodeToString(ar.a(str.getBytes(), b.getBytes()), 0);
        } catch (Exception e) {
            return null;
        }
    }

    public String b(String str) {
        try {
            if (TextUtils.isEmpty(b)) {
                return str;
            }
            return new String(ar.b(Base64.decode(str.getBytes(), 0), b.getBytes()));
        } catch (Exception e) {
            return null;
        }
    }
}
