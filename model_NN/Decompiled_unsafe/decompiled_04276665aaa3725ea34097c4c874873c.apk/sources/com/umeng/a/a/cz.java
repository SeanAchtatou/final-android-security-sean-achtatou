package com.umeng.a.a;

import android.content.Context;
import android.provider.Settings;

/* compiled from: AndroidIdTracker */
public class cz extends cy {

    /* renamed from: a  reason: collision with root package name */
    private Context f2721a;

    public cz(Context context) {
        super("android_id");
        this.f2721a = context;
    }

    public String a() {
        try {
            return Settings.Secure.getString(this.f2721a.getContentResolver(), "android_id");
        } catch (Exception e) {
            return null;
        }
    }
}
