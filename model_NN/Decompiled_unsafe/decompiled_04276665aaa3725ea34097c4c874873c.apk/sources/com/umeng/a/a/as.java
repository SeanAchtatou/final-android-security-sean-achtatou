package com.umeng.a.a;

/* compiled from: DeflaterHelper */
public class as {

    /* renamed from: a  reason: collision with root package name */
    public static int f2650a;

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(byte[] r6) throws java.io.IOException {
        /*
            r0 = 0
            r4 = 0
            if (r6 == 0) goto L_0x0007
            int r1 = r6.length
            if (r1 > 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            java.util.zip.Deflater r2 = new java.util.zip.Deflater
            r2.<init>()
            r2.setInput(r6)
            r2.finish()
            r1 = 8192(0x2000, float:1.14794E-41)
            byte[] r3 = new byte[r1]
            com.umeng.a.a.as.f2650a = r4
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0046 }
            r1.<init>()     // Catch:{ all -> 0x0046 }
        L_0x001e:
            boolean r0 = r2.finished()     // Catch:{ all -> 0x0032 }
            if (r0 != 0) goto L_0x0039
            int r0 = r2.deflate(r3)     // Catch:{ all -> 0x0032 }
            int r4 = com.umeng.a.a.as.f2650a     // Catch:{ all -> 0x0032 }
            int r4 = r4 + r0
            com.umeng.a.a.as.f2650a = r4     // Catch:{ all -> 0x0032 }
            r4 = 0
            r1.write(r3, r4, r0)     // Catch:{ all -> 0x0032 }
            goto L_0x001e
        L_0x0032:
            r0 = move-exception
        L_0x0033:
            if (r1 == 0) goto L_0x0038
            r1.close()
        L_0x0038:
            throw r0
        L_0x0039:
            r2.end()     // Catch:{ all -> 0x0032 }
            if (r1 == 0) goto L_0x0041
            r1.close()
        L_0x0041:
            byte[] r0 = r1.toByteArray()
            goto L_0x0007
        L_0x0046:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.a.a.as.a(byte[]):byte[]");
    }
}
