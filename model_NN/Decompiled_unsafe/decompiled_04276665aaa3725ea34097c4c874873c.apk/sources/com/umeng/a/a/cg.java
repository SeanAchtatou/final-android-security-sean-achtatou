package com.umeng.a.a;

/* compiled from: TMemoryInputTransport */
public final class cg extends ch {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f2689a;
    private int b;
    private int c;

    public void a(byte[] bArr) {
        c(bArr, 0, bArr.length);
    }

    public void c(byte[] bArr, int i, int i2) {
        this.f2689a = bArr;
        this.b = i;
        this.c = i + i2;
    }

    public void a() {
        this.f2689a = null;
    }

    public int a(byte[] bArr, int i, int i2) throws ci {
        int d = d();
        if (i2 > d) {
            i2 = d;
        }
        if (i2 > 0) {
            System.arraycopy(this.f2689a, this.b, bArr, i, i2);
            a(i2);
        }
        return i2;
    }

    public void b(byte[] bArr, int i, int i2) throws ci {
        throw new UnsupportedOperationException("No writing allowed!");
    }

    public byte[] b() {
        return this.f2689a;
    }

    public int c() {
        return this.b;
    }

    public int d() {
        return this.c - this.b;
    }

    public void a(int i) {
        this.b += i;
    }
}
