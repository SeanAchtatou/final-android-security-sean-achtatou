package com.umeng.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.tencent.open.GameAppOperation;
import com.umeng.a.a;
import java.io.File;
import org.json.JSONObject;

/* compiled from: Envelope */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f2664a = {0, 0, 0, 0, 0, 0, 0, 0};
    private final int b = 1;
    private final int c = 0;
    private String d = "1.0";
    private String e = null;
    private byte[] f = null;
    private byte[] g = null;
    private byte[] h = null;
    private int i = 0;
    private int j = 0;
    private int k = 0;
    private byte[] l = null;
    private byte[] m = null;
    private boolean n = false;

    private b(byte[] bArr, String str, byte[] bArr2) throws Exception {
        if (bArr == null || bArr.length == 0) {
            throw new Exception("entity is null or empty");
        }
        this.e = str;
        this.k = bArr.length;
        this.l = as.a(bArr);
        this.j = (int) (System.currentTimeMillis() / 1000);
        this.m = bArr2;
    }

    public static String a(Context context) {
        SharedPreferences a2 = aa.a(context);
        if (a2 == null) {
            return null;
        }
        return a2.getString(GameAppOperation.GAME_SIGNATURE, null);
    }

    public void a(String str) {
        this.f = ar.a(str);
    }

    public String a() {
        return ar.a(this.f);
    }

    public void a(int i2) {
        this.i = i2;
    }

    public void a(boolean z) {
        this.n = z;
    }

    public static b a(Context context, String str, byte[] bArr) {
        try {
            String n2 = at.n(context);
            String c2 = at.c(context);
            SharedPreferences a2 = aa.a(context);
            String string = a2.getString(GameAppOperation.GAME_SIGNATURE, null);
            int i2 = a2.getInt("serial", 1);
            b bVar = new b(bArr, str, (c2 + n2).getBytes());
            bVar.a(string);
            bVar.a(i2);
            bVar.b();
            a2.edit().putInt("serial", i2 + 1).putString(GameAppOperation.GAME_SIGNATURE, bVar.a()).commit();
            bVar.b(context);
            return bVar;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static b b(Context context, String str, byte[] bArr) {
        try {
            String n2 = at.n(context);
            String c2 = at.c(context);
            SharedPreferences a2 = aa.a(context);
            String string = a2.getString(GameAppOperation.GAME_SIGNATURE, null);
            int i2 = a2.getInt("serial", 1);
            b bVar = new b(bArr, str, (c2 + n2).getBytes());
            bVar.a(true);
            bVar.a(string);
            bVar.a(i2);
            bVar.b();
            a2.edit().putInt("serial", i2 + 1).putString(GameAppOperation.GAME_SIGNATURE, bVar.a()).commit();
            bVar.b(context);
            return bVar;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void b() {
        if (this.f == null) {
            this.f = d();
        }
        if (this.n) {
            byte[] bArr = new byte[16];
            try {
                System.arraycopy(this.f, 1, bArr, 0, 16);
                this.l = ar.a(this.l, bArr);
            } catch (Exception e2) {
            }
        }
        this.g = a(this.f, this.j);
        this.h = e();
    }

    private byte[] a(byte[] bArr, int i2) {
        byte[] b2 = ar.b(this.m);
        byte[] b3 = ar.b(this.l);
        int length = b2.length;
        byte[] bArr2 = new byte[(length * 2)];
        for (int i3 = 0; i3 < length; i3++) {
            bArr2[i3 * 2] = b3[i3];
            bArr2[(i3 * 2) + 1] = b2[i3];
        }
        for (int i4 = 0; i4 < 2; i4++) {
            bArr2[i4] = bArr[i4];
            bArr2[(bArr2.length - i4) - 1] = bArr[(bArr.length - i4) - 1];
        }
        byte[] bArr3 = {(byte) (i2 & 255), (byte) ((i2 >> 8) & 255), (byte) ((i2 >> 16) & 255), (byte) (i2 >>> 24)};
        for (int i5 = 0; i5 < bArr2.length; i5++) {
            bArr2[i5] = (byte) (bArr2[i5] ^ bArr3[i5 % 4]);
        }
        return bArr2;
    }

    private byte[] d() {
        return a(this.f2664a, (int) (System.currentTimeMillis() / 1000));
    }

    private byte[] e() {
        return ar.b((ar.a(this.f) + this.i + this.j + this.k + ar.a(this.g)).getBytes());
    }

    public byte[] c() {
        ap apVar = new ap();
        apVar.a(this.d);
        apVar.b(this.e);
        apVar.c(ar.a(this.f));
        apVar.a(this.i);
        apVar.b(this.j);
        apVar.c(this.k);
        apVar.a(this.l);
        apVar.d(this.n ? 1 : 0);
        apVar.d(ar.a(this.g));
        apVar.e(ar.a(this.h));
        try {
            return new bi().a(apVar);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void b(Context context) {
        String str = this.e;
        String e2 = g.a(context).b().e(null);
        String a2 = ar.a(this.f);
        byte[] bArr = new byte[16];
        System.arraycopy(this.f, 2, bArr, 0, 16);
        String a3 = ar.a(ar.b(bArr));
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(LogBuilder.KEY_APPKEY, str);
            if (e2 != null) {
                jSONObject.put("umid", e2);
            }
            jSONObject.put(GameAppOperation.GAME_SIGNATURE, a2);
            jSONObject.put("checksum", a3);
            File file = new File(context.getFilesDir(), ".umeng");
            if (!file.exists()) {
                file.mkdir();
            }
            au.a(new File(file, "exchangeIdentity.json"), jSONObject.toString());
        } catch (Throwable th) {
            th.printStackTrace();
        }
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(LogBuilder.KEY_APPKEY, str);
            jSONObject2.put(LogBuilder.KEY_CHANNEL, a.b(context));
            if (e2 != null) {
                jSONObject2.put("umid", au.b(e2));
            }
            au.a(new File(context.getFilesDir(), "exid.dat"), jSONObject2.toString());
        } catch (Throwable th2) {
            th2.printStackTrace();
        }
    }

    public String toString() {
        int i2 = 1;
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("version : %s\n", this.d));
        sb.append(String.format("address : %s\n", this.e));
        sb.append(String.format("signature : %s\n", ar.a(this.f)));
        sb.append(String.format("serial : %s\n", Integer.valueOf(this.i)));
        sb.append(String.format("timestamp : %d\n", Integer.valueOf(this.j)));
        sb.append(String.format("length : %d\n", Integer.valueOf(this.k)));
        sb.append(String.format("guid : %s\n", ar.a(this.g)));
        sb.append(String.format("checksum : %s ", ar.a(this.h)));
        Object[] objArr = new Object[1];
        if (!this.n) {
            i2 = 0;
        }
        objArr[0] = Integer.valueOf(i2);
        sb.append(String.format("codex : %d", objArr));
        return sb.toString();
    }
}
