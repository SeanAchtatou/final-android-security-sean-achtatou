package com.umeng.b.a;

import com.umeng.b.b;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: OnlineConfigUClient */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2760a = a.class.getName();
    private Map<String, String> b;

    public <T extends c> T a(b bVar, Class cls) {
        JSONObject jSONObject;
        String trim = bVar.c().trim();
        b(trim);
        if (b.c.equals(trim)) {
            jSONObject = a(bVar.b());
        } else if (b.b.equals(trim)) {
            jSONObject = a(bVar.d, bVar.a());
        } else {
            jSONObject = null;
        }
        if (jSONObject == null) {
            return null;
        }
        try {
            return (c) cls.getConstructor(JSONObject.class).newInstance(jSONObject);
        } catch (SecurityException e) {
            b.b(f2760a, "SecurityException", e);
        } catch (NoSuchMethodException e2) {
            b.b(f2760a, "NoSuchMethodException", e2);
        } catch (IllegalArgumentException e3) {
            b.b(f2760a, "IllegalArgumentException", e3);
        } catch (InstantiationException e4) {
            b.b(f2760a, "InstantiationException", e4);
        } catch (IllegalAccessException e5) {
            b.b(f2760a, "IllegalAccessException", e5);
        } catch (InvocationTargetException e6) {
            b.b(f2760a, "InvocationTargetException", e6);
        }
        return null;
    }

    private JSONObject a(String str, JSONObject jSONObject) {
        InputStream inputStream;
        String jSONObject2 = jSONObject.toString();
        int nextInt = new Random().nextInt(1000);
        b.c(f2760a, nextInt + ":\trequest: " + str + f.f2766a + jSONObject2);
        HttpPost httpPost = new HttpPost(str);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(b());
        try {
            if (a()) {
                byte[] a2 = d.a(jSONObject2, Charset.defaultCharset().name());
                httpPost.addHeader("Content-Encoding", "deflate");
                httpPost.setEntity(new InputStreamEntity(new ByteArrayInputStream(a2), (long) a2.length));
            } else {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new BasicNameValuePair("content", jSONObject2));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            }
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = execute.getEntity();
                if (entity == null) {
                    return null;
                }
                InputStream content = entity.getContent();
                Header firstHeader = execute.getFirstHeader("Content-Encoding");
                if (firstHeader == null || !firstHeader.getValue().equalsIgnoreCase("deflate")) {
                    inputStream = content;
                } else {
                    inputStream = new InflaterInputStream(content);
                }
                String a3 = a(inputStream);
                b.a(f2760a, nextInt + ":\tresponse: " + f.f2766a + a3);
                if (a3 == null) {
                    return null;
                }
                return new JSONObject(a3);
            }
            b.c(f2760a, nextInt + ":\tFailed to send message. StatusCode = " + execute.getStatusLine().getStatusCode() + f.f2766a + str);
            return null;
        } catch (ClientProtocolException e) {
            b.c(f2760a, nextInt + ":\tClientProtocolException,Failed to send message." + str, e);
            return null;
        } catch (IOException e2) {
            b.c(f2760a, nextInt + ":\tIOException,Failed to send message." + str, e2);
            return null;
        } catch (JSONException e3) {
            b.c(f2760a, nextInt + ":\tIOException,Failed to send message." + str, e3);
            return null;
        }
    }

    public boolean a() {
        return false;
    }

    private static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine + "\n");
                } else {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e) {
                        b.b(f2760a, "Caught IOException in convertStreamToString()", e);
                        return null;
                    }
                }
            } catch (IOException e2) {
                b.b(f2760a, "Caught IOException in convertStreamToString()", e2);
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e3) {
                    b.b(f2760a, "Caught IOException in convertStreamToString()", e3);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e4) {
                    b.b(f2760a, "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            }
        }
    }

    private JSONObject a(String str) {
        InputStream inputStream;
        int nextInt = new Random().nextInt(1000);
        try {
            String property = System.getProperty("line.separator");
            if (str.length() <= 1) {
                b.b(f2760a, nextInt + ":\tInvalid baseUrl.");
                return null;
            }
            b.a(f2760a, nextInt + ":\tget: " + str);
            HttpGet httpGet = new HttpGet(str);
            if (this.b != null && this.b.size() > 0) {
                for (String next : this.b.keySet()) {
                    httpGet.addHeader(next, this.b.get(next));
                }
            }
            HttpResponse execute = new DefaultHttpClient(b()).execute(httpGet);
            if (execute.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    InputStream content = entity.getContent();
                    Header firstHeader = execute.getFirstHeader("Content-Encoding");
                    if (firstHeader != null && firstHeader.getValue().equalsIgnoreCase("gzip")) {
                        b.a(f2760a, nextInt + "  Use GZIPInputStream get data....");
                        inputStream = new GZIPInputStream(content);
                    } else if (firstHeader == null || !firstHeader.getValue().equalsIgnoreCase("deflate")) {
                        inputStream = content;
                    } else {
                        b.a(f2760a, nextInt + "  Use InflaterInputStream get data....");
                        inputStream = new InflaterInputStream(content);
                    }
                    String a2 = a(inputStream);
                    b.a(f2760a, nextInt + ":\tresponse: " + property + a2);
                    if (a2 == null) {
                        return null;
                    }
                    return new JSONObject(a2);
                }
            } else {
                b.c(f2760a, nextInt + ":\tFailed to send message. StatusCode = " + execute.getStatusLine().getStatusCode() + f.f2766a + str);
            }
            return null;
        } catch (ClientProtocolException e) {
            b.c(f2760a, nextInt + ":\tClientProtocolException,Failed to send message." + str, e);
            return null;
        } catch (Exception e2) {
            b.c(f2760a, nextInt + ":\tIOException,Failed to send message." + str, e2);
            return null;
        }
    }

    private HttpParams b() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 20000);
        HttpProtocolParams.setUserAgent(basicHttpParams, System.getProperty("http.agent"));
        return basicHttpParams;
    }

    private void b(String str) {
        if (f.a(str) || !(b.c.equals(str.trim()) ^ b.b.equals(str.trim()))) {
            throw new RuntimeException("验证请求方式失败[" + str + "]");
        }
    }
}
