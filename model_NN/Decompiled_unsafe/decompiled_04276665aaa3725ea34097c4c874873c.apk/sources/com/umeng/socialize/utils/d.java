package com.umeng.socialize.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;
import com.meizu.cloud.pushsdk.constants.MeizuConstants;

/* compiled from: DeviceConfig */
public class d {
    public static boolean a(String str, Context context) {
        boolean z = true;
        if (context == null) {
            return false;
        }
        try {
            context.getPackageManager().getPackageInfo(str, 1);
        } catch (PackageManager.NameNotFoundException e) {
            z = false;
        }
        return z;
    }

    public static boolean a(Context context, String str) {
        if (context.getPackageManager().checkPermission(str, context.getPackageName()) != 0) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r4) {
        /*
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r4.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            if (r0 != 0) goto L_0x0011
            java.lang.String r1 = "DeviceConfig"
            java.lang.String r2 = "No IMEI."
            com.umeng.socialize.utils.g.e(r1, r2)
        L_0x0011:
            java.lang.String r1 = ""
            java.lang.String r2 = "android.permission.READ_PHONE_STATE"
            boolean r2 = a(r4, r2)     // Catch:{ Exception -> 0x006a }
            if (r2 == 0) goto L_0x0072
            java.lang.String r0 = r0.getDeviceId()     // Catch:{ Exception -> 0x006a }
        L_0x001f:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x0069
            java.lang.String r0 = "DeviceConfig"
            java.lang.String r1 = "No IMEI."
            com.umeng.socialize.utils.g.e(r0, r1)
            java.lang.String r0 = f(r4)
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x0069
            java.lang.String r0 = "DeviceConfig"
            java.lang.String r1 = "Failed to take mac as IMEI. Try to use Secure.ANDROID_ID instead."
            com.umeng.socialize.utils.g.e(r0, r1)
            android.content.ContentResolver r0 = r4.getContentResolver()
            java.lang.String r1 = "android_id"
            java.lang.String r0 = android.provider.Settings.Secure.getString(r0, r1)
            java.lang.String r1 = "DeviceConfig"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getDeviceId: Secure.ANDROID_ID: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            com.umeng.socialize.utils.g.e(r1, r2)
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x0069
            java.lang.String r0 = a()
        L_0x0069:
            return r0
        L_0x006a:
            r0 = move-exception
            java.lang.String r2 = "DeviceConfig"
            java.lang.String r3 = "No IMEI."
            com.umeng.socialize.utils.g.c(r2, r3, r0)
        L_0x0072:
            r0 = r1
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.utils.d.a(android.content.Context):java.lang.String");
    }

    public static String a() {
        try {
            Class<?> cls = Class.forName(MeizuConstants.CLS_NAME_SYSTEM_PROPERTIES);
            return (String) cls.getMethod("get", String.class, String.class).invoke(cls, "ro.serialno", "unknown");
        } catch (Exception e) {
            return null;
        }
    }

    public static String[] b(Context context) {
        String[] strArr = {"Unknown", "Unknown"};
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
            strArr[0] = "Unknown";
            return strArr;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            strArr[0] = "Unknown";
            return strArr;
        } else if (connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            strArr[0] = "Wi-Fi";
            return strArr;
        } else {
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
            if (networkInfo.getState() != NetworkInfo.State.CONNECTED) {
                return strArr;
            }
            strArr[0] = "2G/3G";
            strArr[1] = networkInfo.getSubtypeName();
            return strArr;
        }
    }

    public static boolean c(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                return activeNetworkInfo.isConnectedOrConnecting();
            }
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public static boolean d(Context context) {
        if (!a(context, "android.permission.ACCESS_NETWORK_STATE") || !c(context)) {
            return false;
        }
        return true;
    }

    public static boolean b() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        return false;
    }

    public static String e(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    public static String f(Context context) {
        Exception e;
        String str;
        try {
            str = c();
            try {
                if (TextUtils.isEmpty(str)) {
                    g.e("DeviceConfig", "Could not get mac address.[no permission android.permission.ACCESS_WIFI_STATE");
                }
            } catch (Exception e2) {
                e = e2;
                g.e("DeviceConfig", "Could not get mac address." + e.toString());
                return str;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str = "";
            e = exc;
        }
        return str;
    }

    private static String c() {
        int i = 0;
        String[] strArr = {"/sys/class/net/wlan0/address", "/sys/class/net/eth0/address", "/sys/devices/virtual/net/wlan0/address"};
        while (i < strArr.length) {
            try {
                String a2 = a(strArr[i]);
                if (a2 != null) {
                    return a2;
                }
                i++;
            } catch (Exception e) {
                g.b("DeviceConfig", "open file  Failed", e);
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x005c A[SYNTHETIC, Splitter:B:35:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0061 A[SYNTHETIC, Splitter:B:38:0x0061] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.lang.String r7) throws java.io.FileNotFoundException {
        /*
            r0 = 0
            java.io.FileReader r3 = new java.io.FileReader
            r3.<init>(r7)
            if (r3 == 0) goto L_0x001d
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0028, all -> 0x0057 }
            r1 = 1024(0x400, float:1.435E-42)
            r2.<init>(r3, r1)     // Catch:{ IOException -> 0x0028, all -> 0x0057 }
            java.lang.String r0 = r2.readLine()     // Catch:{ IOException -> 0x0071 }
            if (r3 == 0) goto L_0x0018
            r3.close()     // Catch:{ IOException -> 0x001e }
        L_0x0018:
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x0023 }
        L_0x001d:
            return r0
        L_0x001e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0018
        L_0x0023:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001d
        L_0x0028:
            r1 = move-exception
            r2 = r0
        L_0x002a:
            java.lang.String r4 = "DeviceConfig"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x006f }
            r5.<init>()     // Catch:{ all -> 0x006f }
            java.lang.String r6 = "Could not read from file "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x006f }
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ all -> 0x006f }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x006f }
            com.umeng.socialize.utils.g.b(r4, r5, r1)     // Catch:{ all -> 0x006f }
            if (r3 == 0) goto L_0x0047
            r3.close()     // Catch:{ IOException -> 0x0052 }
        L_0x0047:
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x004d }
            goto L_0x001d
        L_0x004d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001d
        L_0x0052:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0047
        L_0x0057:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x005a:
            if (r3 == 0) goto L_0x005f
            r3.close()     // Catch:{ IOException -> 0x0065 }
        L_0x005f:
            if (r2 == 0) goto L_0x0064
            r2.close()     // Catch:{ IOException -> 0x006a }
        L_0x0064:
            throw r0
        L_0x0065:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005f
        L_0x006a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0064
        L_0x006f:
            r0 = move-exception
            goto L_0x005a
        L_0x0071:
            r1 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.utils.d.a(java.lang.String):java.lang.String");
    }
}
