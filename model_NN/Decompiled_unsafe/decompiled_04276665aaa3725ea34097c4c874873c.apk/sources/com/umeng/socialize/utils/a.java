package com.umeng.socialize.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;

/* compiled from: BitmapUtils */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static String f2891a = "/mnt/sdcard/";
    public static int b = 768;
    public static int c = 1024;

    static {
        a();
    }

    public static void a() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            f2891a = Environment.getExternalStorageDirectory().getPath() + File.separator + "umeng_cache" + File.separator;
        } else {
            f2891a = Environment.getDataDirectory().getPath() + File.separator + "umeng_cache" + File.separator;
        }
        File file = new File(f2891a);
        if (!file.exists()) {
            file.mkdir();
        }
        try {
            e(f2891a);
        } catch (Exception e) {
            g.c("BitmapUtils", "清除缓存抛出异常 ： " + e.toString());
        }
        System.gc();
    }

    private static BitmapFactory.Options c(String str, int i, int i2) {
        BitmapFactory.Options options = null;
        g.c("bitmapOptions", str);
        InputStream a2 = a(str);
        if (a2 != null) {
            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            try {
                BitmapFactory.decodeStream(a2, null, options);
                int ceil = (int) Math.ceil((double) (options.outHeight / i2));
                int ceil2 = (int) Math.ceil((double) (options.outWidth / i));
                if (ceil > 1 && ceil2 > 1) {
                    if (ceil > ceil2) {
                        options.inSampleSize = ceil;
                    } else {
                        options.inSampleSize = ceil2;
                    }
                }
                options.inJustDecodeBounds = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
            a(a2);
        }
        return options;
    }

    public static InputStream a(String str) {
        Exception e;
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(new File(d(str)));
        } catch (Exception e2) {
            try {
                e2.printStackTrace();
                inputStream = null;
            } catch (Exception e3) {
                Exception exc = e3;
                inputStream = null;
                e = exc;
                g.b("BitmapUtil", "读取图片流出错" + e.toString());
                return inputStream;
            }
        }
        if (inputStream != null) {
            try {
                if (inputStream.available() > 0) {
                    return inputStream;
                }
            } catch (Exception e4) {
                e = e4;
                g.b("BitmapUtil", "读取图片流出错" + e.toString());
                return inputStream;
            }
        }
        InputStream openStream = new URL(str).openStream();
        try {
            a(d(str), openStream);
            return new FileInputStream(new File(d(str)));
        } catch (Exception e5) {
            Exception exc2 = e5;
            inputStream = openStream;
            e = exc2;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0021 A[SYNTHETIC, Splitter:B:13:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0037 A[SYNTHETIC, Splitter:B:26:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0041 A[SYNTHETIC, Splitter:B:32:0x0041] */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x0032=Splitter:B:23:0x0032, B:10:0x001c=Splitter:B:10:0x001c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.lang.String r4, java.io.InputStream r5) {
        /*
            r2 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x004d, IOException -> 0x0030, all -> 0x003d }
            java.io.File r0 = new java.io.File     // Catch:{ FileNotFoundException -> 0x004d, IOException -> 0x0030, all -> 0x003d }
            r0.<init>(r4)     // Catch:{ FileNotFoundException -> 0x004d, IOException -> 0x0030, all -> 0x003d }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x004d, IOException -> 0x0030, all -> 0x003d }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x001b, IOException -> 0x004b }
        L_0x000f:
            int r2 = r5.read(r0)     // Catch:{ FileNotFoundException -> 0x001b, IOException -> 0x004b }
            r3 = -1
            if (r2 == r3) goto L_0x0025
            r3 = 0
            r1.write(r0, r3, r2)     // Catch:{ FileNotFoundException -> 0x001b, IOException -> 0x004b }
            goto L_0x000f
        L_0x001b:
            r0 = move-exception
        L_0x001c:
            r0.printStackTrace()     // Catch:{ all -> 0x0049 }
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ IOException -> 0x0045 }
        L_0x0024:
            return
        L_0x0025:
            r1.flush()     // Catch:{ FileNotFoundException -> 0x001b, IOException -> 0x004b }
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ IOException -> 0x002e }
            goto L_0x0024
        L_0x002e:
            r0 = move-exception
            goto L_0x0024
        L_0x0030:
            r0 = move-exception
            r1 = r2
        L_0x0032:
            r0.printStackTrace()     // Catch:{ all -> 0x0049 }
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ IOException -> 0x003b }
            goto L_0x0024
        L_0x003b:
            r0 = move-exception
            goto L_0x0024
        L_0x003d:
            r0 = move-exception
            r1 = r2
        L_0x003f:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ IOException -> 0x0047 }
        L_0x0044:
            throw r0
        L_0x0045:
            r0 = move-exception
            goto L_0x0024
        L_0x0047:
            r1 = move-exception
            goto L_0x0044
        L_0x0049:
            r0 = move-exception
            goto L_0x003f
        L_0x004b:
            r0 = move-exception
            goto L_0x0032
        L_0x004d:
            r0 = move-exception
            r1 = r2
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.utils.a.a(java.lang.String, java.io.InputStream):void");
    }

    public static Bitmap a(String str, int i, int i2) {
        InputStream inputStream;
        Throwable th;
        Bitmap bitmap = null;
        g.c("loadImageUrl", str);
        if (!TextUtils.isEmpty(str)) {
            try {
                inputStream = a(str);
                try {
                    bitmap = BitmapFactory.decodeStream(inputStream, null, c(str, i, i2));
                    a(inputStream);
                } catch (Exception e) {
                    e = e;
                    try {
                        e.printStackTrace();
                        a(inputStream);
                        return bitmap;
                    } catch (Throwable th2) {
                        th = th2;
                        a(inputStream);
                        throw th;
                    }
                }
            } catch (Exception e2) {
                e = e2;
                inputStream = null;
                e.printStackTrace();
                a(inputStream);
                return bitmap;
            } catch (Throwable th3) {
                inputStream = null;
                th = th3;
                a(inputStream);
                throw th;
            }
        }
        return bitmap;
    }

    public static boolean b(String str) {
        if (!TextUtils.isEmpty(str) && new File(d(str)).getAbsoluteFile().exists()) {
            return true;
        }
        return false;
    }

    public static boolean a(String str, int i) {
        File file = new File(d(str));
        if (!file.exists() || file.length() < ((long) i)) {
            return false;
        }
        return true;
    }

    public static Bitmap c(String str) {
        InputStream a2 = a(str);
        if (a2 == null) {
            return null;
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(a2, null, null);
        a(a2);
        return decodeStream;
    }

    public static Bitmap b(String str, int i, int i2) {
        InputStream a2 = a(str);
        if (a2 == null) {
            return null;
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(a2, null, c(str, i, i2));
        a(a2);
        return decodeStream;
    }

    private static void a(InputStream inputStream) {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception e) {
                g.c("BitmapUtils", e.toString());
            }
        }
    }

    public static String d(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str.startsWith("http://") || str.startsWith("https://")) {
            return f2891a + com.umeng.socialize.d.b.a.c(str);
        }
        return str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0068 A[SYNTHETIC, Splitter:B:23:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0073 A[SYNTHETIC, Splitter:B:29:0x0073] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(android.graphics.Bitmap r6) {
        /*
            r0 = 0
            if (r6 == 0) goto L_0x0009
            boolean r1 = r6.isRecycled()
            if (r1 == 0) goto L_0x0011
        L_0x0009:
            java.lang.String r1 = "BitmapUtils"
            java.lang.String r2 = "bitmap2Bytes  ==> bitmap == null or bitmap.isRecycled()"
            com.umeng.socialize.utils.g.c(r1, r2)
        L_0x0010:
            return r0
        L_0x0011:
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x005b, all -> 0x006e }
            r2.<init>()     // Catch:{ Exception -> 0x005b, all -> 0x006e }
            int r1 = r6.getRowBytes()     // Catch:{ Exception -> 0x007b }
            int r3 = r6.getHeight()     // Catch:{ Exception -> 0x007b }
            int r1 = r1 * r3
            int r3 = r1 / 1024
            r1 = 100
            float r4 = (float) r3     // Catch:{ Exception -> 0x007b }
            float r5 = com.umeng.socialize.Config.imageSize     // Catch:{ Exception -> 0x007b }
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 <= 0) goto L_0x0032
            float r4 = com.umeng.socialize.Config.imageSize     // Catch:{ Exception -> 0x007b }
            float r3 = (float) r3     // Catch:{ Exception -> 0x007b }
            float r3 = r4 / r3
            float r1 = (float) r1     // Catch:{ Exception -> 0x007b }
            float r1 = r1 * r3
            int r1 = (int) r1     // Catch:{ Exception -> 0x007b }
        L_0x0032:
            java.lang.String r3 = "BitmapUtil"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007b }
            r4.<init>()     // Catch:{ Exception -> 0x007b }
            java.lang.String r5 = "compress quality:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x007b }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x007b }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x007b }
            com.umeng.socialize.utils.g.c(r3, r4)     // Catch:{ Exception -> 0x007b }
            android.graphics.Bitmap$CompressFormat r3 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x007b }
            r6.compress(r3, r1, r2)     // Catch:{ Exception -> 0x007b }
            byte[] r0 = r2.toByteArray()     // Catch:{ Exception -> 0x007b }
            if (r2 == 0) goto L_0x0010
            r2.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x0010
        L_0x0059:
            r1 = move-exception
            goto L_0x0010
        L_0x005b:
            r1 = move-exception
            r2 = r0
        L_0x005d:
            java.lang.String r3 = "BitmapUtils"
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0079 }
            com.umeng.socialize.utils.g.b(r3, r1)     // Catch:{ all -> 0x0079 }
            if (r2 == 0) goto L_0x0010
            r2.close()     // Catch:{ IOException -> 0x006c }
            goto L_0x0010
        L_0x006c:
            r1 = move-exception
            goto L_0x0010
        L_0x006e:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0071:
            if (r2 == 0) goto L_0x0076
            r2.close()     // Catch:{ IOException -> 0x0077 }
        L_0x0076:
            throw r0
        L_0x0077:
            r1 = move-exception
            goto L_0x0076
        L_0x0079:
            r0 = move-exception
            goto L_0x0071
        L_0x007b:
            r1 = move-exception
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.utils.a.a(android.graphics.Bitmap):byte[]");
    }

    public static int a(BitmapFactory.Options options, int i, int i2) {
        int i3 = options.outHeight;
        int i4 = options.outWidth;
        int i5 = 1;
        if (i3 > i2 || i4 > i) {
            int i6 = i3 / 2;
            int i7 = i4 / 2;
            while (i6 / i5 > i2 && i7 / i5 > i) {
                i5 *= 2;
            }
        }
        return i5;
    }

    public static BitmapFactory.Options a(byte[] bArr) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        int ceil = (int) Math.ceil((double) (options.outWidth / b));
        int ceil2 = (int) Math.ceil((double) (options.outHeight / c));
        if (ceil2 <= 1 || ceil <= 1) {
            if (ceil2 > 2) {
                options.inSampleSize = ceil2;
            } else if (ceil > 2) {
                options.inSampleSize = ceil;
            }
        } else if (ceil2 > ceil) {
            options.inSampleSize = ceil2;
        } else {
            options.inSampleSize = ceil;
        }
        options.inJustDecodeBounds = false;
        return options;
    }

    private static int c() {
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (int) ((((double) statFs.getBlockSize()) * ((double) statFs.getAvailableBlocks())) / 1048576.0d);
    }

    private static void e(String str) {
        File[] listFiles = new File(str).listFiles();
        if (listFiles.length != 0) {
            int i = 0;
            for (File length : listFiles) {
                i = (int) (((long) i) + length.length());
            }
            if (i > 10485760 || 40 > c()) {
                int length2 = (int) ((0.4d * ((double) listFiles.length)) + 1.0d);
                Arrays.sort(listFiles, new C0071a());
                for (int i2 = 0; i2 < length2; i2++) {
                    listFiles[i2].delete();
                }
            }
        }
    }

    /* renamed from: com.umeng.socialize.utils.a$a  reason: collision with other inner class name */
    /* compiled from: BitmapUtils */
    private static class C0071a implements Comparator<File> {
        private C0071a() {
        }

        /* renamed from: a */
        public int compare(File file, File file2) {
            if (file.lastModified() > file2.lastModified()) {
                return 1;
            }
            if (file.lastModified() == file2.lastModified()) {
                return 0;
            }
            return -1;
        }
    }

    public static void b() {
        a();
    }
}
