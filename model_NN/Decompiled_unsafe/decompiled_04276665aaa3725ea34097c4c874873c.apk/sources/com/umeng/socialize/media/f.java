package com.umeng.socialize.media;

import com.umeng.socialize.d.b.e;
import com.umeng.socialize.media.UMediaObject;
import java.util.HashMap;
import java.util.Map;

/* compiled from: UMEmoji */
public class f extends a {
    public g j;
    public g k;

    public UMediaObject.a g() {
        return UMediaObject.a.IMAGE;
    }

    public final Map<String, Object> h() {
        HashMap hashMap = new HashMap();
        if (c()) {
            hashMap.put(e.e, this.b);
            hashMap.put(e.f, g());
        }
        return hashMap;
    }

    public byte[] i() {
        if (this.j != null) {
            return this.k.i();
        }
        return null;
    }

    public String toString() {
        return "UMEmoji [" + this.j.toString() + "]";
    }

    public g j() {
        return this.j;
    }
}
