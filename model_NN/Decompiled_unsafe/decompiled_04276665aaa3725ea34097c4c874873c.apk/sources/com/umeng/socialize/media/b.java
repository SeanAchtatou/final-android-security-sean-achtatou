package com.umeng.socialize.media;

import android.os.Bundle;
import android.text.TextUtils;
import com.umeng.socialize.Config;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.utils.a;
import com.umeng.socialize.utils.g;
import java.util.HashMap;
import java.util.Map;

/* compiled from: QQShareContent */
public class b extends d {

    /* renamed from: a  reason: collision with root package name */
    public int f2876a = 1;
    public Map<String, String> b = new HashMap();

    public b(ShareContent shareContent) {
        super(shareContent);
        if (shareContent.mMedia != null && (shareContent.mMedia instanceof p)) {
            a((p) shareContent.mMedia);
        }
        if (shareContent.mMedia != null && (shareContent.mMedia instanceof h)) {
            a((h) shareContent.mMedia);
        }
    }

    public Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("summary", g());
        if (h() != null && TextUtils.isEmpty(g())) {
            this.f2876a = 5;
            a(bundle);
        } else if (k() == null && j() == null) {
            b(bundle);
        } else {
            this.f2876a = 2;
            c(bundle);
        }
        bundle.putInt("req_type", this.f2876a);
        if (TextUtils.isEmpty(i())) {
            b("http://dev.umeng.com/");
        }
        bundle.putString("targetUrl", i());
        if (!TextUtils.isEmpty(f())) {
            bundle.putString("title", f());
        } else {
            bundle.putString("title", " ");
        }
        if (Config.QQWITHQZONE == 1) {
            bundle.putInt("cflag", 1);
        } else if (Config.QQWITHQZONE == 2) {
            bundle.putInt("cflag", 2);
        }
        if (!TextUtils.isEmpty(Config.appName)) {
            bundle.putString("appName", Config.appName);
        }
        return bundle;
    }

    private void a(Bundle bundle) {
        a(h());
        String str = this.b.get("image_path_local");
        String str2 = this.b.get("image_path_url");
        if (!TextUtils.isEmpty(str) && a.b(str)) {
            bundle.putString("imageLocalUrl", str);
        } else if (!TextUtils.isEmpty(str2)) {
            bundle.putString("imageUrl", str2);
        }
    }

    private void b(Bundle bundle) {
        a(bundle);
    }

    private void c(Bundle bundle) {
        p pVar;
        if (j() != null) {
            p j = j();
            b();
            pVar = j;
        } else if (k() != null) {
            h k = k();
            c();
            pVar = k;
        } else {
            pVar = null;
        }
        String str = this.b.get("image_path_local");
        String str2 = this.b.get("image_path_url");
        if (!TextUtils.isEmpty(str) && a.b(str)) {
            bundle.putString("imageLocalUrl", str);
        } else if (!TextUtils.isEmpty(str2)) {
            bundle.putString("imageUrl", str2);
        }
        bundle.putString("audio_url", pVar.b());
    }

    /* access modifiers changed from: protected */
    public void a(g gVar) {
        if (gVar != null) {
            String str = "";
            g.d("10.12", "image=" + gVar);
            if (TextUtils.isEmpty(i())) {
                if (!TextUtils.isEmpty(gVar.f())) {
                    b(gVar.f());
                } else {
                    b(gVar.b());
                }
            }
            String b2 = gVar.b();
            if (gVar.j() != null) {
                str = gVar.j().toString();
            }
            if (!a.b(str)) {
                str = "";
            }
            g.d("10.12", "image path =" + str);
            this.b.put("image_path_local", str);
            this.b.put("image_path_url", b2);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        p j = j();
        this.b.put("audio_url", j.b());
        boolean isEmpty = TextUtils.isEmpty(i());
        if (!TextUtils.isEmpty(j.e())) {
            this.b.put("image_path_url", j.e());
        } else {
            a(j.n());
        }
        if (!TextUtils.isEmpty(j.d())) {
            a(j.d());
        }
        if (!isEmpty) {
            return;
        }
        if (!TextUtils.isEmpty(j.f())) {
            b(j.f());
        } else {
            b(j.b());
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        h k = k();
        this.b.put("audio_url", k.b());
        boolean isEmpty = TextUtils.isEmpty(i());
        if (!TextUtils.isEmpty(k.e())) {
            this.b.put("image_path_url", k.e());
        } else {
            a(k.o());
        }
        if (!TextUtils.isEmpty(k.d())) {
            a(k.d());
        }
        if (!isEmpty) {
            return;
        }
        if (!TextUtils.isEmpty(k.f())) {
            b(k.f());
        } else {
            b(k.b());
        }
    }
}
