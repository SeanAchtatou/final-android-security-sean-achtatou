package com.umeng.socialize.c;

import android.location.Location;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.Serializable;

/* compiled from: UMLocation */
public class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private double f2789a;
    private double b;

    public c(double d, double d2) {
        this.f2789a = d;
        this.b = d2;
    }

    public String toString() {
        return "(" + this.b + MiPushClient.ACCEPT_TIME_SEPARATOR + this.f2789a + ")";
    }

    public static c a(Location location) {
        try {
            if (!(location.getLatitude() == 0.0d || location.getLongitude() == 0.0d)) {
                return new c(location.getLatitude(), location.getLongitude());
            }
        } catch (Exception e) {
        }
        return null;
    }
}
