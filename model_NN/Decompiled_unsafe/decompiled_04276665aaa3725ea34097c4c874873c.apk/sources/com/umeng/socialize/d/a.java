package com.umeng.socialize.d;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.socialize.Config;
import com.umeng.socialize.d.a.b;
import com.umeng.socialize.d.b.e;
import com.umeng.socialize.utils.h;

/* compiled from: ActionBarRequest */
public class a extends b {
    private int g = 0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(Context context, boolean z) {
        super(context, "", b.class, 1, b.C0069b.GET);
        int i = 1;
        this.b = context;
        this.g = !z ? 0 : i;
    }

    public void a() {
        a("dc", Config.Descriptor);
        a(e.k, String.valueOf(this.g));
        a("use_coco2dx", String.valueOf(Config.UseCocos));
        if (!TextUtils.isEmpty(Config.EntityName)) {
            a(e.m, Config.EntityName);
        }
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "/bar/get/" + h.a(this.b) + "/";
    }
}
