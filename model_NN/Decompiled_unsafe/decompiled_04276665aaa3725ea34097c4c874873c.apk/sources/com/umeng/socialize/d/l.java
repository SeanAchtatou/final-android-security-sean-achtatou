package com.umeng.socialize.d;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.socialize.Config;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.common.a;
import com.umeng.socialize.d.a.b;
import com.umeng.socialize.d.a.f;
import com.umeng.socialize.d.b.e;
import com.umeng.socialize.d.b.g;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.utils.h;
import java.util.Map;

/* compiled from: SharePostRequest */
public class l extends b {
    private String g;
    private String h;
    private ShareContent i;

    public l(Context context, String str, String str2, ShareContent shareContent) {
        super(context, "", f.class, 9, b.C0069b.POST);
        this.b = context;
        this.g = str;
        this.h = str2;
        this.i = shareContent;
        g.b("xxxx content=" + shareContent.mMedia);
    }

    public void a() {
        a("to", this.g);
        a("ct", this.i.mText);
        a("usid", this.h);
        a("ak", h.a(this.b));
        a("ek", Config.EntityKey);
        if (this.i.mLocation != null) {
            a("lc", this.i.mLocation.toString());
        }
        b(this.i.mMedia);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "/share/add/" + h.a(this.b) + "/" + Config.EntityKey + "/";
    }

    public Map<String, g.a> c() {
        if (this.i == null || this.i.mMedia == null || this.i.mMedia.c()) {
            return super.c();
        }
        Map<String, g.a> c = super.c();
        if (this.i.mMedia instanceof com.umeng.socialize.media.g) {
            com.umeng.socialize.media.g gVar = (com.umeng.socialize.media.g) this.i.mMedia;
            gVar.j().getPath();
            byte[] l = gVar.l();
            String a2 = a.a(l);
            if (TextUtils.isEmpty(a2)) {
                a2 = "png";
            }
            com.umeng.socialize.utils.g.b("xxxx filedata=" + l);
            c.put(e.b, new g.a((System.currentTimeMillis() + "") + "." + a2, l));
        }
        return c;
    }
}
