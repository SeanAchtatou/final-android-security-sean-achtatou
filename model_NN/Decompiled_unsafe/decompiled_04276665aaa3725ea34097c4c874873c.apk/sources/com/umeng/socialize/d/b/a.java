package com.umeng.socialize.d.b;

import android.text.TextUtils;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: AesHelper */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static byte[] f2805a = null;
    private static byte[] b = "nmeug.f9/Om+L823".getBytes();
    private static boolean c = true;

    public static String a(String str, String str2) throws Exception {
        Cipher instance = Cipher.getInstance("AES/CBC/NoPadding");
        int blockSize = instance.getBlockSize();
        byte[] bytes = str.getBytes(str2);
        int length = bytes.length;
        if (length % blockSize != 0) {
            length += blockSize - (length % blockSize);
        }
        byte[] bArr = new byte[length];
        System.arraycopy(bytes, 0, bArr, 0, bytes.length);
        instance.init(1, new SecretKeySpec(f2805a, "AES"), new IvParameterSpec(b));
        return b.a(instance.doFinal(bArr));
    }

    public static String b(String str, String str2) throws Exception {
        Cipher instance = Cipher.getInstance("AES/CBC/NoPadding");
        instance.init(2, new SecretKeySpec(f2805a, "AES"), new IvParameterSpec(b));
        return new String(instance.doFinal(b.a(str)), str2);
    }

    public static void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            String c2 = c(str);
            if (c2.length() >= 16) {
                c2 = c2.substring(0, 16);
            }
            f2805a = c2.getBytes();
        }
    }

    public static byte[] b(String str) {
        return c(str, "UTF-8");
    }

    public static byte[] c(String str, String str2) {
        if (str == null) {
            return null;
        }
        try {
            return str.getBytes(str2);
        } catch (UnsupportedEncodingException e) {
            throw a(str2, e);
        }
    }

    private static IllegalStateException a(String str, UnsupportedEncodingException unsupportedEncodingException) {
        return new IllegalStateException(str + ": " + unsupportedEncodingException);
    }

    public static String a(byte[] bArr, String str) {
        if (bArr == null) {
            return null;
        }
        try {
            return new String(bArr, str);
        } catch (UnsupportedEncodingException e) {
            throw a(str, e);
        }
    }

    public static String a(byte[] bArr) {
        return a(bArr, "UTF-8");
    }

    public static String c(String str) {
        if (str == null) {
            return null;
        }
        try {
            byte[] bytes = str.getBytes();
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(bytes);
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < digest.length; i++) {
                stringBuffer.append(String.format("%02X", Byte.valueOf(digest[i])));
            }
            return stringBuffer.toString();
        } catch (Exception e) {
            return str.replaceAll("[^[a-z][A-Z][0-9][.][_]]", "");
        }
    }
}
