package com.umeng.socialize.d;

import com.meizu.cloud.pushsdk.notification.model.TimeDisplaySetting;
import com.umeng.socialize.d.a.f;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ShareMultiFollowResponse */
public class k extends f {

    /* renamed from: a  reason: collision with root package name */
    public Map<String, Integer> f2814a;

    public k(JSONObject jSONObject) {
        super(jSONObject);
    }

    public void a() {
        int i;
        super.a();
        this.f2814a = new HashMap();
        Iterator<String> keys = this.j.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            try {
                JSONObject jSONObject = this.j.getJSONObject(next);
                if (jSONObject != null) {
                    i = jSONObject.optInt(TimeDisplaySetting.START_SHOW_TIME, -102);
                } else {
                    i = -102;
                }
                this.f2814a.put(next, Integer.valueOf(i));
            } catch (JSONException e) {
            }
        }
    }
}
