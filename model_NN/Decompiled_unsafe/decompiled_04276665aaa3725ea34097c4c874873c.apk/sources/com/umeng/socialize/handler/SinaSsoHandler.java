package com.umeng.socialize.handler;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.sina.sso.RemoteSSO;
import com.sina.weibo.sdk.api.share.BaseResponse;
import com.sina.weibo.sdk.api.share.IWeiboShareAPI;
import com.sina.weibo.sdk.api.share.SendMultiMessageToWeiboRequest;
import com.sina.weibo.sdk.api.share.WeiboShareSDK;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.net.AsyncWeiboRunner;
import com.sina.weibo.sdk.net.RequestListener;
import com.sina.weibo.sdk.net.WeiboParameters;
import com.sina.weibo.sdk.utils.LogUtil;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.SocializeException;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.c.a;
import com.umeng.socialize.c.c;
import com.umeng.socialize.common.b;
import com.umeng.socialize.d.h;
import com.umeng.socialize.d.i;
import com.umeng.socialize.media.e;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.view.UMFriendListener;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SinaSsoHandler extends UMAPIShareHandler {
    /* access modifiers changed from: private */
    public PlatformConfig.SinaWeibo b = null;
    /* access modifiers changed from: private */
    public SinaPreferences c;
    private UMShareListener d;
    private AuthListener e;
    private Context f;
    private SsoHandler g;
    private AuthInfo h;
    private IWeiboShareAPI i;

    public void a(Context context, PlatformConfig.Platform platform) {
        super.a(context, platform);
        this.f = context.getApplicationContext();
        this.b = (PlatformConfig.SinaWeibo) platform;
        this.c = new SinaPreferences(this.f, "sina");
        this.h = new AuthInfo(context, ((PlatformConfig.SinaWeibo) platform).appKey, Config.REDIRECT_URL, "email,direct_messages_read,direct_messages_write,friendships_groups_read,friendships_groups_write,statuses_to_me_read,follow_app_official_microblog,invitation_write");
        this.g = new SsoHandler((Activity) context, this.h);
        this.i = WeiboShareSDK.createWeiboAPI(context, this.b.appKey);
        this.i.registerApp();
        g.c("sina", "onCreate");
    }

    public IWeiboShareAPI c() {
        return this.i;
    }

    public boolean d() {
        return this.c.c();
    }

    public boolean a(Context context) {
        return g();
    }

    public boolean b(Context context) {
        return d();
    }

    public a e() {
        return a.SINA;
    }

    public String f() {
        return this.c.b();
    }

    public boolean g() {
        return this.i.isWeiboAppInstalled();
    }

    public void a(Activity activity, UMAuthListener uMAuthListener) {
        this.e = new AuthListener(uMAuthListener);
        this.g.authorize(this.e);
    }

    /* access modifiers changed from: protected */
    public void a(String str, WeiboParameters weiboParameters, String str2, RequestListener requestListener, String str3) {
        if (str3 == null || TextUtils.isEmpty(str) || weiboParameters == null || TextUtils.isEmpty(str2) || requestListener == null) {
            LogUtil.e("SinaSsoHandler", "Argument error!");
            return;
        }
        weiboParameters.put("access_token", str3);
        new AsyncWeiboRunner(this.f).requestAsync(str, weiboParameters, str2, requestListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.socialize.handler.SinaSsoHandler.a(android.app.Activity, com.umeng.socialize.UMAuthListener):void
     arg types: [android.app.Activity, com.umeng.socialize.handler.SinaSsoHandler$2]
     candidates:
      com.umeng.socialize.handler.SinaSsoHandler.a(com.umeng.socialize.handler.SinaSsoHandler, android.os.Bundle):void
      com.umeng.socialize.handler.SinaSsoHandler.a(com.umeng.socialize.ShareContent, android.os.Bundle):com.umeng.socialize.ShareContent
      com.umeng.socialize.handler.SinaSsoHandler.a(android.app.Activity, com.umeng.socialize.view.UMFriendListener):void
      com.umeng.socialize.handler.SinaSsoHandler.a(android.content.Context, com.umeng.socialize.PlatformConfig$Platform):void
      com.umeng.socialize.handler.SinaSsoHandler.a(android.content.Context, com.umeng.socialize.UMAuthListener):void
      com.umeng.socialize.handler.UMAPIShareHandler.a(com.umeng.socialize.ShareContent, com.umeng.socialize.UMShareListener):void
      com.umeng.socialize.handler.UMAPIShareHandler.a(android.content.Context, com.umeng.socialize.PlatformConfig$Platform):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.app.Activity, com.umeng.socialize.view.UMFriendListener):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.PlatformConfig$Platform):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.UMAuthListener):void
      com.umeng.socialize.editorpage.IEditor.a(com.umeng.socialize.ShareContent, android.os.Bundle):com.umeng.socialize.ShareContent
      com.umeng.socialize.handler.SinaSsoHandler.a(android.app.Activity, com.umeng.socialize.UMAuthListener):void */
    public void b(final Activity activity, final UMAuthListener uMAuthListener) {
        if (this.c.b() != null) {
            WeiboParameters weiboParameters = new WeiboParameters(this.b.appKey);
            weiboParameters.put("uid", this.c.b());
            a("https://api.weibo.com/2/users/show.json", weiboParameters, "GET", new RequestListener() {
                public void onComplete(String str) {
                    HashMap hashMap = new HashMap();
                    hashMap.put(SocketMessage.MSG_RESULE_KEY, str);
                    uMAuthListener.onComplete(a.SINA, 2, hashMap);
                }

                public void onWeiboException(WeiboException weiboException) {
                    uMAuthListener.onError(a.SINA, 2, new Throwable(weiboException));
                }
            }, this.c.a());
            return;
        }
        a(activity, (UMAuthListener) new UMAuthListener() {
            public void onComplete(a aVar, int i, Map<String, String> map) {
                b.b(new Runnable() {
                    public void run() {
                        SinaSsoHandler.this.b(activity, uMAuthListener);
                    }
                });
            }

            public void onError(a aVar, int i, Throwable th) {
                g.b("xxxx 授权失败");
            }

            public void onCancel(a aVar, int i) {
                g.b("xxxx 授权取消");
            }
        });
    }

    public void a(Context context, UMAuthListener uMAuthListener) {
        this.c.f();
        uMAuthListener.onComplete(a.SINA, 1, null);
    }

    public boolean a(Activity activity, ShareContent shareContent, final UMShareListener uMShareListener) {
        if (activity == null) {
            g.c("UMError", "Sina share activity is null");
            return false;
        }
        e eVar = new e(shareContent);
        eVar.a(activity);
        SendMultiMessageToWeiboRequest sendMultiMessageToWeiboRequest = new SendMultiMessageToWeiboRequest();
        sendMultiMessageToWeiboRequest.transaction = String.valueOf(System.currentTimeMillis());
        sendMultiMessageToWeiboRequest.multiMessage = eVar.a();
        AuthInfo authInfo = new AuthInfo(activity, this.b.appKey, Config.REDIRECT_URL, "email,direct_messages_read,direct_messages_write,friendships_groups_read,friendships_groups_write,statuses_to_me_read,follow_app_official_microblog,invitation_write");
        String str = "";
        if (this.c != null) {
            str = this.c.a();
        }
        this.d = uMShareListener;
        this.i.sendRequest(activity, sendMultiMessageToWeiboRequest, authInfo, str, new WeiboAuthListener() {
            public void onWeiboException(WeiboException weiboException) {
                g.c("sina_share", "weibo share exception");
                if (uMShareListener != null) {
                    uMShareListener.onError(a.SINA, new Throwable(weiboException));
                }
            }

            public void onComplete(Bundle bundle) {
                g.c("sina_share", "weibo share complete");
                if (uMShareListener != null) {
                    uMShareListener.onResult(a.SINA);
                }
                SinaSsoHandler.this.c.a(bundle).e();
            }

            public void onCancel() {
                g.c("sina_share", "weibo share cancel");
                if (uMShareListener != null) {
                    uMShareListener.onCancel(a.SINA);
                }
            }
        });
        return true;
    }

    public void a(int i2, int i3, Intent intent) {
        if (this.g != null) {
            this.g.authorizeCallBack(i2, i3, intent);
        }
        this.g = null;
    }

    public boolean a() {
        return true;
    }

    public int b() {
        return 5659;
    }

    public void b(int i2, int i3, Intent intent) {
        if (i2 == 5659) {
            if (i3 == -1) {
                String stringExtra = intent.getStringExtra(SocketMessage.MSG_ERROR_KEY);
                if (stringExtra == null) {
                    stringExtra = intent.getStringExtra("error_type");
                }
                if (stringExtra == null) {
                    this.e.onComplete(intent.getExtras());
                } else if (stringExtra.equals("access_denied") || stringExtra.equals("OAuthAccessDeniedException")) {
                    g.c("Weibo-authorize", "Login canceled by user.");
                    this.e.onCancel();
                } else {
                    String stringExtra2 = intent.getStringExtra("error_description");
                    if (stringExtra2 != null) {
                        stringExtra = stringExtra + ":" + stringExtra2;
                    }
                    g.c("Weibo-authorize", "Login failed: " + stringExtra);
                    new SocializeException(i3, stringExtra);
                }
            } else if (i3 != 0) {
            } else {
                if (intent != null) {
                    g.c("Weibo-authorize", "Login failed: " + intent.getStringExtra(SocketMessage.MSG_ERROR_KEY));
                    new SocializeException(intent.getIntExtra("error_code", -1), intent.getStringExtra(SocketMessage.MSG_ERROR_KEY) + " : " + intent.getStringExtra("failing_url"));
                    return;
                }
                g.c("Weibo-authorize", "Login canceled by user.");
                this.e.onCancel();
            }
        }
    }

    public Bundle a(ShareContent shareContent) {
        File j;
        Bundle bundle = new Bundle();
        bundle.putString("media", a.SINA.toString());
        bundle.putString("title", "分享到新浪微博");
        bundle.putString("txt", shareContent.mText);
        if (!(shareContent.mMedia == null || !(shareContent.mMedia instanceof com.umeng.socialize.media.g) || (j = ((com.umeng.socialize.media.g) shareContent.mMedia).j()) == null)) {
            bundle.putString("pic", j.getAbsolutePath());
        }
        bundle.putBoolean(IXAdRequestInfo.AD_TYPE, true);
        bundle.putBoolean("location", true);
        if (shareContent.mFollow == null) {
            bundle.putBoolean("follow_", false);
        } else if (!this.c.d()) {
            bundle.putBoolean("follow_", true);
        } else {
            bundle.putBoolean("follow_", false);
        }
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void h() {
        super.h();
        this.c.a(true);
    }

    public ShareContent a(ShareContent shareContent, Bundle bundle) {
        shareContent.mText = bundle.getString("txt");
        if (!bundle.getBoolean("follow_")) {
            shareContent.mFollow = null;
        }
        if (bundle.getString("pic") == null && (shareContent.mMedia instanceof com.umeng.socialize.media.g)) {
            shareContent.mMedia = null;
        }
        if (bundle.getSerializable("location") != null) {
            shareContent.mLocation = (c) bundle.getSerializable("location");
        }
        return shareContent;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.socialize.handler.SinaSsoHandler.a(android.app.Activity, com.umeng.socialize.UMAuthListener):void
     arg types: [android.app.Activity, com.umeng.socialize.handler.SinaSsoHandler$4]
     candidates:
      com.umeng.socialize.handler.SinaSsoHandler.a(com.umeng.socialize.handler.SinaSsoHandler, android.os.Bundle):void
      com.umeng.socialize.handler.SinaSsoHandler.a(com.umeng.socialize.ShareContent, android.os.Bundle):com.umeng.socialize.ShareContent
      com.umeng.socialize.handler.SinaSsoHandler.a(android.app.Activity, com.umeng.socialize.view.UMFriendListener):void
      com.umeng.socialize.handler.SinaSsoHandler.a(android.content.Context, com.umeng.socialize.PlatformConfig$Platform):void
      com.umeng.socialize.handler.SinaSsoHandler.a(android.content.Context, com.umeng.socialize.UMAuthListener):void
      com.umeng.socialize.handler.UMAPIShareHandler.a(com.umeng.socialize.ShareContent, com.umeng.socialize.UMShareListener):void
      com.umeng.socialize.handler.UMAPIShareHandler.a(android.content.Context, com.umeng.socialize.PlatformConfig$Platform):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.app.Activity, com.umeng.socialize.view.UMFriendListener):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.PlatformConfig$Platform):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.UMAuthListener):void
      com.umeng.socialize.editorpage.IEditor.a(com.umeng.socialize.ShareContent, android.os.Bundle):com.umeng.socialize.ShareContent
      com.umeng.socialize.handler.SinaSsoHandler.a(android.app.Activity, com.umeng.socialize.UMAuthListener):void */
    public void a(final Activity activity, final UMFriendListener uMFriendListener) {
        if (activity == null) {
            g.c("UMError", "Sina getFriend activity is null");
            return;
        }
        String b2 = this.c.b();
        if (b2 == null) {
            a(activity, (UMAuthListener) new UMAuthListener() {
                public void onComplete(a aVar, int i, final Map<String, String> map) {
                    b.b(new Runnable() {
                        public void run() {
                            i a2 = com.umeng.socialize.d.g.a(new h(activity, a.SINA, (String) map.get("uid")));
                            if (a2 == null) {
                                g.b("follow", "resp = null");
                            } else if (!a2.b()) {
                                g.b("follow", "follow fail e =" + a2.k);
                            } else {
                                HashMap hashMap = new HashMap();
                                hashMap.put("friend", a2.f2813a);
                                hashMap.put("json", a2.c());
                                uMFriendListener.a(a.SINA, 2, hashMap);
                            }
                        }
                    });
                }

                public void onError(a aVar, int i, Throwable th) {
                    g.b("auth fail");
                }

                public void onCancel(a aVar, int i) {
                    g.b("auth cancle");
                }
            });
            return;
        }
        i a2 = com.umeng.socialize.d.g.a(new h(activity, a.SINA, b2));
        if (a2 == null) {
            uMFriendListener.a(a.SINA, 2, new Throwable("resp = null"));
        } else if (!a2.b()) {
            uMFriendListener.a(a.SINA, 2, new Throwable(a2.k));
        } else {
            HashMap hashMap = new HashMap();
            hashMap.put("friend", a2.f2813a);
            hashMap.put("json", a2.c());
            uMFriendListener.a(a.SINA, 2, hashMap);
        }
    }

    public void a(BaseResponse baseResponse) {
        switch (baseResponse.errCode) {
            case 0:
                g.c("sina_share", "weibo share error ok");
                if (g()) {
                    this.d.onResult(a.SINA);
                    return;
                }
                return;
            case 1:
                g.c("sina_share", "weibo share cancel");
                this.d.onCancel(a.SINA);
                return;
            case 2:
                g.c("sina_share", "weibo share fail");
                this.d.onError(a.SINA, new Throwable(baseResponse.errMsg));
                return;
            default:
                return;
        }
    }

    class AuthListener implements WeiboAuthListener {
        private UMAuthListener b = null;

        AuthListener(UMAuthListener uMAuthListener) {
            this.b = uMAuthListener;
        }

        public void onComplete(Bundle bundle) {
            SinaSsoHandler.this.c.a(bundle).e();
            SinaSsoHandler.this.b(bundle);
            if (this.b != null) {
                this.b.onComplete(a.SINA, 0, SinaSsoHandler.this.a(bundle));
            }
        }

        public void onCancel() {
            if (this.b != null) {
                this.b.onCancel(a.SINA, 0);
            }
        }

        public void onWeiboException(WeiboException weiboException) {
            if (this.b != null) {
                this.b.onError(a.SINA, 0, new Throwable(weiboException));
            }
        }
    }

    class AuthListenerWrapper implements UMAuthListener {

        /* renamed from: a  reason: collision with root package name */
        private UMAuthListener f2852a;

        public void onComplete(a aVar, int i, Map<String, String> map) {
        }

        public void onError(a aVar, int i, Throwable th) {
            if (this.f2852a != null) {
                this.f2852a.onError(aVar, i, th);
            }
        }

        public void onCancel(a aVar, int i) {
            if (this.f2852a != null) {
                this.f2852a.onCancel(aVar, i);
            }
        }
    }

    /* access modifiers changed from: private */
    public Map<String, String> a(Bundle bundle) {
        if (bundle == null || bundle.isEmpty()) {
            return null;
        }
        Set<String> keySet = bundle.keySet();
        HashMap hashMap = new HashMap();
        for (String next : keySet) {
            hashMap.put(next, bundle.getString(next));
        }
        return hashMap;
    }

    /* access modifiers changed from: private */
    public void b(final Bundle bundle) throws SocializeException {
        new Thread(new Runnable() {
            public void run() {
                com.umeng.socialize.d.e eVar = new com.umeng.socialize.d.e(SinaSsoHandler.this.i());
                eVar.a("to", "sina");
                eVar.a("usid", bundle.getString("uid"));
                eVar.a("access_token", bundle.getString("access_token"));
                eVar.a(Oauth2AccessToken.KEY_REFRESH_TOKEN, bundle.getString(Oauth2AccessToken.KEY_REFRESH_TOKEN));
                eVar.a("expires_in", bundle.getString("expires_in"));
                eVar.a("app_id", SinaSsoHandler.this.b.appKey);
                eVar.a("app_secret", SinaSsoHandler.this.b.appSecret);
                com.umeng.socialize.d.g.a(eVar);
            }
        }).start();
    }

    static class SinaConnection implements ServiceConnection {

        /* renamed from: a  reason: collision with root package name */
        public boolean f2853a;
        public boolean b;
        public String c;
        public String d;
        private WeakReference<Activity> e;
        private String f;
        private String[] g;

        public void onServiceDisconnected(ComponentName componentName) {
            this.b = false;
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            this.b = true;
            RemoteSSO asInterface = RemoteSSO.Stub.asInterface(iBinder);
            try {
                this.c = asInterface.getPackageName();
                this.d = asInterface.getActivityName();
                this.f2853a = a(this.e.get(), 5659);
            } catch (RemoteException e2) {
                e2.printStackTrace();
            }
        }

        private boolean a(Activity activity, int i) {
            boolean z = true;
            Intent intent = new Intent();
            intent.setClassName(this.c, this.d);
            intent.putExtra(WBConstants.SSO_APP_KEY, this.f);
            intent.putExtra(WBConstants.SSO_REDIRECT_URL, "http://sns.whalecloud.com");
            if (this.g != null && this.g.length > 0) {
                intent.putExtra("scope", TextUtils.join(MiPushClient.ACCEPT_TIME_SEPARATOR, this.g));
            }
            if (!a(activity, intent)) {
                return false;
            }
            try {
                activity.startActivityForResult(intent, i);
            } catch (ActivityNotFoundException e2) {
                z = false;
            }
            if (this.b) {
                this.b = a(activity);
                if (this.b) {
                    activity.getApplication().unbindService(this);
                }
            }
            return z;
        }

        private boolean a(Context context, Intent intent) {
            PackageManager packageManager = context.getPackageManager();
            ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 0);
            if (resolveActivity == null) {
                return false;
            }
            try {
                for (Signature charsString : packageManager.getPackageInfo(resolveActivity.activityInfo.packageName, 64).signatures) {
                    if ("30820295308201fea00302010202044b4ef1bf300d06092a864886f70d010105050030818d310b300906035504061302434e3110300e060355040813074265694a696e673110300e060355040713074265694a696e67312c302a060355040a132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c7464312c302a060355040b132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c74643020170d3130303131343130323831355a180f32303630303130323130323831355a30818d310b300906035504061302434e3110300e060355040813074265694a696e673110300e060355040713074265694a696e67312c302a060355040a132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c7464312c302a060355040b132353696e612e436f6d20546563686e6f6c6f677920284368696e612920436f2e204c746430819f300d06092a864886f70d010101050003818d00308189028181009d367115bc206c86c237bb56c8e9033111889b5691f051b28d1aa8e42b66b7413657635b44786ea7e85d451a12a82a331fced99c48717922170b7fc9bc1040753c0d38b4cf2b22094b1df7c55705b0989441e75913a1a8bd2bc591aa729a1013c277c01c98cbec7da5ad7778b2fad62b85ac29ca28ced588638c98d6b7df5a130203010001300d06092a864886f70d0101050500038181000ad4b4c4dec800bd8fd2991adfd70676fce8ba9692ae50475f60ec468d1b758a665e961a3aedbece9fd4d7ce9295cd83f5f19dc441a065689d9820faedbb7c4a4c4635f5ba1293f6da4b72ed32fb8795f736a20c95cda776402099054fccefb4a1a558664ab8d637288feceba9508aa907fc1fe2b1ae5a0dec954ed831c0bea4".equals(charsString.toCharsString())) {
                        return true;
                    }
                }
                return false;
            } catch (PackageManager.NameNotFoundException e2) {
                return false;
            }
        }

        private boolean a(Context context) {
            List<ActivityManager.RunningServiceInfo> runningServices = ((ActivityManager) context.getSystemService(PushConstants.INTENT_ACTIVITY_NAME)).getRunningServices(100);
            if (runningServices.size() <= 0) {
                return false;
            }
            for (int i = 0; i < runningServices.size(); i++) {
                if (runningServices.get(i).service.getClassName().equals("com.sina.weibo.business.RemoteSSOService")) {
                    return true;
                }
            }
            return false;
        }
    }
}
