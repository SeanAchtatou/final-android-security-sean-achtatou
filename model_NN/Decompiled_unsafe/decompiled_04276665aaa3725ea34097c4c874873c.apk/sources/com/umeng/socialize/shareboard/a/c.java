package com.umeng.socialize.shareboard.a;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: SNSPlatformAdapter */
class c implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f2885a;
    final /* synthetic */ a b;

    c(a aVar, View view) {
        this.b = aVar;
        this.f2885a = view;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.f2885a.setBackgroundColor(-3355444);
            return false;
        } else if (motionEvent.getAction() != 1) {
            return false;
        } else {
            this.f2885a.setBackgroundColor(-1);
            return false;
        }
    }
}
