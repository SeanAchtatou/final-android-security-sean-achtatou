package com.umeng.socialize.shareboard;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import com.umeng.socialize.common.g;
import com.umeng.socialize.shareboard.b.a;
import com.umeng.socialize.utils.ShareBoardlistener;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ShareBoard */
public class b extends PopupWindow {

    /* renamed from: a  reason: collision with root package name */
    private final g f2886a;
    private Context b = null;
    private c c = null;
    private ShareBoardlistener d;
    private a e;
    private List<a> f = new ArrayList();

    public b(Context context, List<a> list) {
        super(context);
        setWindowLayoutMode(-1, -1);
        this.f2886a = g.a(context);
        this.b = context;
        this.c = a(context);
        setContentView(this.c);
        this.f = list;
        this.e = new com.umeng.socialize.shareboard.a.a(this.b, list, this);
        this.c.a(this.e);
        setAnimationStyle(this.f2886a.d("umeng_socialize_shareboard_animation"));
        setFocusable(true);
    }

    public ShareBoardlistener a() {
        return this.d;
    }

    public void a(ShareBoardlistener shareBoardlistener) {
        this.d = shareBoardlistener;
    }

    private c a(Context context) {
        c cVar = new c(context);
        cVar.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        cVar.a(new d(this));
        return cVar;
    }
}
