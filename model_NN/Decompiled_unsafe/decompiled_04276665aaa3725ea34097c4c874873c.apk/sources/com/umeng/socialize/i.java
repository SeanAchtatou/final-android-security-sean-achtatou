package com.umeng.socialize;

import android.app.Activity;
import android.content.Context;
import com.umeng.socialize.b.a;
import com.umeng.socialize.common.b;

/* compiled from: UMShareAPI */
class i extends b.a<Void> {
    final /* synthetic */ Activity c;
    final /* synthetic */ ShareAction d;
    final /* synthetic */ UMShareListener e;
    final /* synthetic */ UMShareAPI f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    i(UMShareAPI uMShareAPI, Context context, Activity activity, ShareAction shareAction, UMShareListener uMShareListener) {
        super(context);
        this.f = uMShareAPI;
        this.c = activity;
        this.d = shareAction;
        this.e = uMShareListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Void c() {
        if (this.f.f2773a != null) {
            this.f.f2773a.a(this.c, this.d, this.e);
            return null;
        }
        this.f.f2773a = new a(this.c);
        this.f.f2773a.a(this.c, this.d, this.e);
        return null;
    }
}
