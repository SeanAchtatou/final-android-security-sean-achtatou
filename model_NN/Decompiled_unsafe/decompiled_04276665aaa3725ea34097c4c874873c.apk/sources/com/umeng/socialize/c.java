package com.umeng.socialize;

import com.umeng.socialize.shareboard.a;
import com.umeng.socialize.utils.ShareBoardlistener;

/* compiled from: ShareAction */
class c implements ShareBoardlistener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareAction f2785a;

    c(ShareAction shareAction) {
        this.f2785a = shareAction;
    }

    public void a(a aVar, com.umeng.socialize.c.a aVar2) {
        ShareContent shareContent;
        int indexOf = this.f2785a.g.indexOf(aVar2);
        int size = this.f2785a.i.size();
        if (size != 0) {
            if (indexOf < size) {
                shareContent = (ShareContent) this.f2785a.i.get(indexOf);
            } else {
                shareContent = (ShareContent) this.f2785a.i.get(size - 1);
            }
            ShareContent unused = this.f2785a.f2771a = shareContent;
        }
        int size2 = this.f2785a.j.size();
        if (size2 != 0) {
            if (indexOf < size2) {
                UMShareListener unused2 = this.f2785a.d = (UMShareListener) this.f2785a.j.get(indexOf);
            } else {
                UMShareListener unused3 = this.f2785a.d = (UMShareListener) this.f2785a.j.get(size2 - 1);
            }
        }
        this.f2785a.setPlatform(aVar2);
        this.f2785a.share();
    }
}
