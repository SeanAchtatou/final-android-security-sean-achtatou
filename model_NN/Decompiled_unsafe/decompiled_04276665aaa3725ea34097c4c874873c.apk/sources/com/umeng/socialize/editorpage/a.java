package com.umeng.socialize.editorpage;

import com.umeng.socialize.editorpage.KeyboardListenRelativeLayout;
import com.umeng.socialize.utils.g;

/* compiled from: ShareActivity */
class a implements KeyboardListenRelativeLayout.IOnKeyboardStateChangedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareActivity f2819a;

    a(ShareActivity shareActivity) {
        this.f2819a = shareActivity;
    }

    public void a(int i) {
        int unused = this.f2819a.z = i;
        g.c("ShareActivity", "onKeyboardStateChanged  now state is " + i);
    }
}
