package com.umeng.socialize.editorpage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.mobads.interfaces.IXAdRequestInfo;
import com.umeng.socialize.Config;
import com.umeng.socialize.c.a;
import com.umeng.socialize.c.c;
import com.umeng.socialize.common.g;
import com.umeng.socialize.editorpage.location.b;
import com.umeng.socialize.editorpage.location.d;
import com.umeng.socialize.utils.h;
import java.io.File;
import java.util.Set;

public class ShareActivity extends Activity implements View.OnClickListener {
    private static int c = 140;
    private boolean A = false;
    private Dialog B;
    private Set<String> C = null;
    private b D = null;

    /* renamed from: a  reason: collision with root package name */
    protected ImageView f2818a;
    TextWatcher b = new b(this);
    private String d;
    private String e;
    private String f;
    private boolean g;
    private boolean h;
    private boolean i;
    private g j;
    private Button k;
    private Button l;
    private EditText m;
    private ImageButton n;
    private ImageButton o;
    private View p;
    private View q;
    private TextView r;
    private CheckBox s;
    private KeyboardListenRelativeLayout t;
    /* access modifiers changed from: private */
    public Context u;
    /* access modifiers changed from: private */
    public boolean v;
    private a w;
    private com.umeng.socialize.editorpage.location.a x;
    /* access modifiers changed from: private */
    public c y;
    /* access modifiers changed from: private */
    public int z;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z2;
        this.j = g.a(this);
        this.A = h.c(this);
        if (!this.A) {
            setTheme(this.j.d("Theme.UMDefault"));
        }
        super.onCreate(bundle);
        this.u = this;
        setContentView(this.j.a("umeng_socialize_post_share"));
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.softInputMode = 16;
        if (this.A) {
            int[] b2 = h.b(this.u);
            attributes.width = b2[0];
            attributes.height = b2[1];
        }
        getWindow().setAttributes(attributes);
        this.t = (KeyboardListenRelativeLayout) findViewById(this.j.b("umeng_socialize_share_root"));
        this.t.a(new a(this));
        Bundle extras = getIntent().getExtras();
        this.w = a(extras.getString("media"));
        if (this.w == a.RENREN) {
            c = 120;
        } else {
            c = 140;
        }
        this.d = extras.getString("title");
        this.e = extras.getString("txt");
        this.f = extras.getString("pic");
        this.g = extras.getBoolean("follow_", false);
        this.h = extras.getBoolean(IXAdRequestInfo.AD_TYPE);
        this.h = false;
        if (!extras.getBoolean("location") || !Config.ShareLocation) {
            z2 = false;
        } else {
            z2 = true;
        }
        this.i = z2;
        c();
        if (this.i) {
            b();
        }
    }

    private a a(String str) {
        if (str.equals("TENCENT")) {
            return a.TENCENT;
        }
        if (str.equals("RENREN")) {
            return a.RENREN;
        }
        if (str.equals("DOUBAN")) {
            return a.DOUBAN;
        }
        return a.SINA;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.i) {
            e();
        }
        this.m.requestFocus();
        super.onResume();
    }

    private void b() {
        this.x = new com.umeng.socialize.editorpage.location.a();
        d dVar = new d();
        dVar.a(this);
        this.x.a(dVar);
        this.x.a(this);
    }

    private void c() {
        ((TextView) findViewById(this.j.b("umeng_socialize_title_bar_middleTv"))).setText(this.d);
        this.k = (Button) findViewById(this.j.b("umeng_socialize_title_bar_leftBt"));
        this.l = (Button) findViewById(this.j.b("umeng_socialize_title_bar_rightBt"));
        this.k.setOnClickListener(this);
        this.l.setOnClickListener(this);
        this.m = (EditText) findViewById(this.j.b("umeng_socialize_share_edittext"));
        if (!TextUtils.isEmpty(this.e)) {
            this.m.setText(this.e);
            this.m.setSelection(this.e.length());
        }
        this.m.addTextChangedListener(this.b);
        this.r = (TextView) findViewById(this.j.b("umeng_socialize_share_word_num"));
        this.v = d();
        if (this.i) {
            findViewById(this.j.b("umeng_socialize_share_location")).setVisibility(0);
            this.o = (ImageButton) findViewById(this.j.b("umeng_socialize_location_ic"));
            this.o.setOnClickListener(this);
            this.o.setVisibility(0);
            this.o.setImageResource(this.j.c("umeng_socialize_location_off"));
            this.p = findViewById(this.j.b("umeng_socialize_location_progressbar"));
        }
        if (this.h) {
            this.n = (ImageButton) findViewById(this.j.b("umeng_socialize_share_at"));
            this.n.setVisibility(0);
            this.n.setOnClickListener(this);
        }
        if (this.f != null) {
            findViewById(this.j.b("umeng_socialize_share_image")).setVisibility(0);
            this.f2818a = (ImageView) findViewById(this.j.b("umeng_socialize_share_previewImg"));
            this.q = findViewById(this.j.b("umeng_socialize_share_previewImg_remove"));
            this.q.setOnClickListener(this);
            this.f2818a.setVisibility(0);
            if (this.f.equals("video")) {
                this.f2818a.setImageResource(g.a(this.u, "drawable", "umeng_socialize_share_video"));
            } else if (this.f.equals("music")) {
                this.f2818a.setImageResource(g.a(this.u, "drawable", "umeng_socialize_share_music"));
            } else {
                this.f2818a.setImageURI(Uri.fromFile(new File(this.f)));
            }
        }
        if (this.g) {
            this.s = (CheckBox) findViewById(this.j.b("umeng_socialize_follow_check"));
            this.s.setOnClickListener(this);
            this.s.setVisibility(0);
        }
    }

    private void a(View view) {
        String obj = this.m.getText().toString();
        if (TextUtils.isEmpty(obj.trim())) {
            Toast.makeText(this, "输入内容为空...", 0).show();
        } else if (h.a(obj) > c) {
            Toast.makeText(this, "输入内容超过140个字.", 0).show();
        } else if (this.v) {
            Toast.makeText(this.u, "超出最大字数限制....", 0).show();
        } else {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putString("txt", obj);
            bundle.putString("pic", this.f);
            bundle.putBoolean("follow_", this.g);
            bundle.putSerializable("location", this.y);
            intent.putExtras(bundle);
            setResult(-1, intent);
            a();
        }
    }

    public void onCancel(View view) {
        setResult(1000);
        a();
    }

    private void b(View view) {
        this.f = null;
        findViewById(this.j.b("umeng_socialize_share_image")).setVisibility(8);
    }

    public void onAtFriends(View view) {
        if (this.B == null) {
            this.B = f();
        }
        if (!this.B.isShowing()) {
            this.B.show();
        }
    }

    public void onFollowStatChanged(View view) {
        this.g = this.s.isChecked();
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == this.j.b("umeng_socialize_share_previewImg_remove")) {
            b(view);
        } else if (id == this.j.b("umeng_socialize_title_bar_rightBt")) {
            a(view);
        } else if (id == this.j.b("umeng_socialize_title_bar_leftBt")) {
            onCancel(view);
        } else if (id == this.j.b("umeng_socialize_share_at")) {
            onAtFriends(view);
        } else if (id == this.j.b("umeng_socialize_location_ic")) {
            c(view);
        } else if (id == this.j.b("umeng_socialize_follow_check")) {
            onFollowStatChanged(view);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4 && keyEvent.getRepeatCount() == 0) {
            setResult(1000);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.z == -3) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(getWindow().peekDecorView().getWindowToken(), 0);
            new Handler().postDelayed(new c(this), 500);
            return;
        }
        finish();
    }

    /* access modifiers changed from: private */
    public boolean d() {
        int a2 = c - h.a(this.m.getText().toString());
        com.umeng.socialize.utils.g.c("ShareActivity", "onTextChanged " + a2 + "   " + h.a(this.m.getText().toString()));
        this.r.setText("" + a2);
        if (a2 >= 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.x != null) {
            this.x.a();
        }
        if (this.D != null) {
            this.D.cancel(true);
        }
        super.onDestroy();
    }

    private void c(View view) {
        if (this.y != null) {
            new AlertDialog.Builder(this).setMessage("是否删除位置信息？").setCancelable(false).setPositiveButton("是", new e(this)).setNegativeButton("否", new d(this)).create().show();
        } else {
            e();
        }
    }

    private void e() {
        if (this.x == null) {
            b();
        }
        if (!(this.D == null || this.D.getStatus() == AsyncTask.Status.FINISHED)) {
            this.D.cancel(true);
        }
        this.D = new f(this, this.x);
        this.D.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        if (z2) {
            this.o.setVisibility(8);
            this.p.setVisibility(0);
        } else if (this.y == null) {
            this.o.setImageResource(this.j.c("umeng_socialize_location_off"));
            this.o.setVisibility(0);
            this.p.setVisibility(8);
        } else {
            this.o.setImageResource(this.j.c("umeng_socialize_location_on"));
            this.o.setVisibility(0);
            this.p.setVisibility(8);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (!com.umeng.socialize.common.h.d || keyEvent.getKeyCode() != 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        new Handler().postDelayed(new g(this), 400);
        return true;
    }

    private Dialog f() {
        try {
            return (Dialog) Class.forName("com.umeng.socialize.view.ShareAtDialogV2").getConstructor(ShareActivity.class, a.class, String.class).newInstance(this, this.w, Config.UID);
        } catch (Exception e2) {
            com.umeng.socialize.utils.g.c("ShareActivity", "如果需要使用‘@好友’功能，请添加相应的jar文件；否则忽略此信息", e2);
            return null;
        }
    }
}
