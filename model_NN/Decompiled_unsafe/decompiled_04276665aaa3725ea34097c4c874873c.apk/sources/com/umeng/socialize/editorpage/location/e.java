package com.umeng.socialize.editorpage.location;

import android.location.LocationListener;

/* compiled from: SocializeLocationManager */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f2830a;
    final /* synthetic */ long b;
    final /* synthetic */ float c;
    final /* synthetic */ LocationListener d;
    final /* synthetic */ d e;

    e(d dVar, String str, long j, float f, LocationListener locationListener) {
        this.e = dVar;
        this.f2830a = str;
        this.b = j;
        this.c = f;
        this.d = locationListener;
    }

    public void run() {
        this.e.f2829a.requestLocationUpdates(this.f2830a, this.b, this.c, this.d);
    }
}
