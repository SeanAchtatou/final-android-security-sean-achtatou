package com.umeng.socialize.editorpage.location;

import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

/* compiled from: SocializeLocationManager */
public class d {

    /* renamed from: a  reason: collision with root package name */
    LocationManager f2829a = null;

    public void a(Context context) {
        if (com.umeng.socialize.utils.d.a(context, "android.permission.ACCESS_FINE_LOCATION") || com.umeng.socialize.utils.d.a(context, "android.permission.ACCESS_COARSE_LOCATION")) {
            this.f2829a = (LocationManager) context.getApplicationContext().getSystemService("location");
        }
    }

    public String a(Criteria criteria, boolean z) {
        if (this.f2829a == null) {
            return null;
        }
        return this.f2829a.getBestProvider(criteria, z);
    }

    public Location a(String str) {
        if (this.f2829a == null) {
            return null;
        }
        return this.f2829a.getLastKnownLocation(str);
    }

    public boolean b(String str) {
        if (this.f2829a == null) {
            return false;
        }
        return this.f2829a.isProviderEnabled(str);
    }

    public void a(Activity activity, String str, long j, float f, LocationListener locationListener) {
        if (this.f2829a != null) {
            activity.runOnUiThread(new e(this, str, j, f, locationListener));
        }
    }

    public void a(LocationListener locationListener) {
        if (this.f2829a != null) {
            this.f2829a.removeUpdates(locationListener);
        }
    }
}
