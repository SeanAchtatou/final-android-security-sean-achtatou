package com.umeng.socialize;

import android.app.Activity;
import android.content.Context;
import com.umeng.socialize.c.a;
import com.umeng.socialize.common.b;
import com.umeng.socialize.view.UMFriendListener;

/* compiled from: UMShareAPI */
class h extends b.a<Void> {
    final /* synthetic */ Activity c;
    final /* synthetic */ a d;
    final /* synthetic */ UMFriendListener e;
    final /* synthetic */ UMShareAPI f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    h(UMShareAPI uMShareAPI, Context context, Activity activity, a aVar, UMFriendListener uMFriendListener) {
        super(context);
        this.f = uMShareAPI;
        this.c = activity;
        this.d = aVar;
        this.e = uMFriendListener;
    }

    /* access modifiers changed from: protected */
    public Object c() {
        if (this.f.f2773a != null) {
            this.f.f2773a.a(this.c, this.d, this.e);
            return null;
        }
        this.f.f2773a = new com.umeng.socialize.b.a(this.c);
        this.f.f2773a.a(this.c, this.d, this.e);
        return null;
    }
}
