package com.umeng.socialize.a;

import com.umeng.socialize.c.a;
import com.umeng.socialize.d.a.f;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: AnalyticsResponse */
public class b extends f {

    /* renamed from: a  reason: collision with root package name */
    public Map<a, Integer> f2776a;
    public String b;

    public b(JSONObject jSONObject) {
        super(jSONObject);
    }

    public String toString() {
        return "ShareMultiResponse [mInfoMap=" + this.f2776a + ", mWeiboId=" + this.b + ", mMsg=" + this.k + ", mStCode=" + this.l + "]";
    }
}
