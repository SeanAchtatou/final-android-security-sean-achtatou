package com.xiaomi.channel.commonutils.misc;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final boolean f2905a = "@SHIP.TO.2A2FE0D7@".contains("2A2FE0D7");
    public static final boolean b = (f2905a || "DEBUG".equalsIgnoreCase("@SHIP.TO.2A2FE0D7@"));
    public static final boolean c = "LOGABLE".equalsIgnoreCase("@SHIP.TO.2A2FE0D7@");
    public static final boolean d = "@SHIP.TO.2A2FE0D7@".contains("YY");
    public static boolean e = "@SHIP.TO.2A2FE0D7@".equalsIgnoreCase("TEST");
    public static final boolean f = "BETA".equalsIgnoreCase("@SHIP.TO.2A2FE0D7@");
    public static final boolean g;
    private static int h;

    static {
        boolean z = false;
        if ("@SHIP.TO.2A2FE0D7@" != 0 && "@SHIP.TO.2A2FE0D7@".startsWith("RC")) {
            z = true;
        }
        g = z;
        h = 1;
        if ("@SHIP.TO.2A2FE0D7@".equalsIgnoreCase("SANDBOX")) {
            h = 2;
        } else if ("@SHIP.TO.2A2FE0D7@".equalsIgnoreCase("ONEBOX")) {
            h = 3;
        } else {
            h = 1;
        }
    }

    public static int c() {
        return h;
    }
}
