package com.xiaomi.channel.commonutils.misc;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.LinkedBlockingQueue;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private a f2908a;
    private Handler b;
    private volatile boolean c;
    private final boolean d;
    private int e;

    class a extends Thread {
        private final LinkedBlockingQueue<b> b = new LinkedBlockingQueue<>();

        public a() {
            super("PackageProcessor");
        }

        public void a(b bVar) {
            this.b.add(bVar);
        }
    }

    public abstract class b {
    }

    public f() {
        this(false);
    }

    public f(boolean z) {
        this(z, 0);
    }

    public f(boolean z, int i) {
        this.b = null;
        this.c = false;
        this.e = 0;
        this.b = new g(this, Looper.getMainLooper());
        this.d = z;
        this.e = i;
    }

    public synchronized void a(b bVar) {
        if (this.f2908a == null) {
            this.f2908a = new a();
            this.f2908a.setDaemon(this.d);
            this.c = false;
            this.f2908a.start();
        }
        this.f2908a.a(bVar);
    }
}
