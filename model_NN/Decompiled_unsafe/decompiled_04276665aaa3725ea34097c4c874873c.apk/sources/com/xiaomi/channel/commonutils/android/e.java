package com.xiaomi.channel.commonutils.android;

import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private static Boolean f2901a;

    public static synchronized boolean a() {
        boolean booleanValue;
        boolean z = false;
        synchronized (e.class) {
            if (f2901a == null) {
                try {
                    Properties properties = new Properties();
                    properties.load(new FileInputStream(new File(Environment.getRootDirectory(), "build.prop")));
                    if (!(properties.getProperty("ro.miui.ui.version.code", null) == null && properties.getProperty("ro.miui.ui.version.name", null) == null)) {
                        z = true;
                    }
                    f2901a = Boolean.valueOf(z);
                } catch (Throwable th) {
                    f2901a = false;
                }
            }
            booleanValue = f2901a.booleanValue();
        }
        return booleanValue;
    }
}
