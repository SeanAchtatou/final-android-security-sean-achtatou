package com.xiaomi.channel.commonutils.android;

import android.content.Context;
import android.text.TextUtils;
import com.xiaomi.channel.commonutils.file.a;
import com.xiaomi.channel.commonutils.string.d;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class c {
    public static boolean a(Context context, String str, String str2) {
        InputStream inputStream;
        FileInputStream fileInputStream;
        InputStream inputStream2;
        byte[] b;
        File file;
        FileInputStream fileInputStream2;
        FileOutputStream fileOutputStream = null;
        try {
            inputStream = context.getAssets().open(str);
            try {
                b = a.b(inputStream);
                file = new File(str2);
                if (file.exists()) {
                    fileInputStream = new FileInputStream(file);
                    try {
                        String a2 = d.a(a.b(fileInputStream));
                        String a3 = d.a(b);
                        if (TextUtils.isEmpty(a2) || !a2.equals(a3)) {
                            fileInputStream2 = fileInputStream;
                        } else {
                            a.a(inputStream);
                            a.a(fileInputStream);
                            a.a((OutputStream) null);
                            return false;
                        }
                    } catch (Exception e) {
                        e = e;
                        inputStream2 = inputStream;
                        try {
                            e.printStackTrace();
                            a.a(inputStream2);
                            a.a(fileInputStream);
                            a.a(fileOutputStream);
                            return false;
                        } catch (Throwable th) {
                            th = th;
                            inputStream = inputStream2;
                            a.a(inputStream);
                            a.a(fileInputStream);
                            a.a(fileOutputStream);
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        a.a(inputStream);
                        a.a(fileInputStream);
                        a.a(fileOutputStream);
                        throw th;
                    }
                } else {
                    fileInputStream2 = null;
                }
            } catch (Exception e2) {
                e = e2;
                fileInputStream = null;
                inputStream2 = inputStream;
                e.printStackTrace();
                a.a(inputStream2);
                a.a(fileInputStream);
                a.a(fileOutputStream);
                return false;
            } catch (Throwable th3) {
                th = th3;
                fileInputStream = null;
                a.a(inputStream);
                a.a(fileInputStream);
                a.a(fileOutputStream);
                throw th;
            }
            try {
                FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                try {
                    fileOutputStream2.write(b);
                    fileOutputStream2.flush();
                    a.a(inputStream);
                    a.a(fileInputStream2);
                    a.a(fileOutputStream2);
                    return true;
                } catch (Exception e3) {
                    e = e3;
                    fileOutputStream = fileOutputStream2;
                    fileInputStream = fileInputStream2;
                    inputStream2 = inputStream;
                    e.printStackTrace();
                    a.a(inputStream2);
                    a.a(fileInputStream);
                    a.a(fileOutputStream);
                    return false;
                } catch (Throwable th4) {
                    th = th4;
                    fileOutputStream = fileOutputStream2;
                    fileInputStream = fileInputStream2;
                    a.a(inputStream);
                    a.a(fileInputStream);
                    a.a(fileOutputStream);
                    throw th;
                }
            } catch (Exception e4) {
                e = e4;
                fileInputStream = fileInputStream2;
                inputStream2 = inputStream;
                e.printStackTrace();
                a.a(inputStream2);
                a.a(fileInputStream);
                a.a(fileOutputStream);
                return false;
            } catch (Throwable th5) {
                th = th5;
                fileInputStream = fileInputStream2;
                a.a(inputStream);
                a.a(fileInputStream);
                a.a(fileOutputStream);
                throw th;
            }
        } catch (Exception e5) {
            e = e5;
            fileInputStream = null;
            inputStream2 = null;
        } catch (Throwable th6) {
            th = th6;
            fileInputStream = null;
            inputStream = null;
            a.a(inputStream);
            a.a(fileInputStream);
            a.a(fileOutputStream);
            throw th;
        }
    }
}
