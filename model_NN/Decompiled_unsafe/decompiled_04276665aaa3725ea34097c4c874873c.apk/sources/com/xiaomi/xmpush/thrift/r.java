package com.xiaomi.xmpush.thrift;

import com.igexin.assist.sdk.AssistPushConsts;
import com.sina.weibo.sdk.component.WidgetRequestParam;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.meta_data.e;
import org.apache.thrift.meta_data.g;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class r implements Serializable, Cloneable, b<r, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> l;
    private static final k m = new k("XmPushActionNotification");
    private static final c n = new c("debug", (byte) 11, 1);
    private static final c o = new c("target", (byte) 12, 2);
    private static final c p = new c("id", (byte) 11, 3);
    private static final c q = new c("appId", (byte) 11, 4);
    private static final c r = new c("type", (byte) 11, 5);
    private static final c s = new c("requireAck", (byte) 2, 6);
    private static final c t = new c(AssistPushConsts.MSG_TYPE_PAYLOAD, (byte) 11, 7);
    private static final c u = new c("extra", (byte) 13, 8);
    private static final c v = new c("packageName", (byte) 11, 9);
    private static final c w = new c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 10);
    private static final c x = new c("binaryExtra", (byte) 11, 14);

    /* renamed from: a  reason: collision with root package name */
    public String f2983a;
    public j b;
    public String c;
    public String d;
    public String e;
    public boolean f;
    public String g;
    public Map<String, String> h;
    public String i;
    public String j;
    public ByteBuffer k;
    private BitSet y;

    public enum a {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, "id"),
        APP_ID(4, "appId"),
        TYPE(5, "type"),
        REQUIRE_ACK(6, "requireAck"),
        PAYLOAD(7, AssistPushConsts.MSG_TYPE_PAYLOAD),
        EXTRA(8, "extra"),
        PACKAGE_NAME(9, "packageName"),
        CATEGORY(10, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY),
        BINARY_EXTRA(14, "binaryExtra");
        
        private static final Map<String, a> l = new HashMap();
        private final short m;
        private final String n;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                l.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.m = s;
            this.n = str;
        }

        public String a() {
            return this.n;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.r$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new org.apache.thrift.meta_data.b("debug", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.thrift.meta_data.b("target", (byte) 2, new g((byte) 12, j.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.thrift.meta_data.b("id", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.thrift.meta_data.b("appId", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.TYPE, (Object) new org.apache.thrift.meta_data.b("type", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.REQUIRE_ACK, (Object) new org.apache.thrift.meta_data.b("requireAck", (byte) 1, new org.apache.thrift.meta_data.c((byte) 2)));
        enumMap.put((Object) a.PAYLOAD, (Object) new org.apache.thrift.meta_data.b(AssistPushConsts.MSG_TYPE_PAYLOAD, (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.EXTRA, (Object) new org.apache.thrift.meta_data.b("extra", (byte) 2, new e((byte) 13, new org.apache.thrift.meta_data.c((byte) 11), new org.apache.thrift.meta_data.c((byte) 11))));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.thrift.meta_data.b("packageName", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.thrift.meta_data.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.BINARY_EXTRA, (Object) new org.apache.thrift.meta_data.b("binaryExtra", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        l = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(r.class, l);
    }

    public r() {
        this.y = new BitSet(1);
        this.f = true;
    }

    public r(String str, boolean z) {
        this();
        this.c = str;
        this.f = z;
        b(true);
    }

    public r a(String str) {
        this.c = str;
        return this;
    }

    public r a(ByteBuffer byteBuffer) {
        this.k = byteBuffer;
        return this;
    }

    public r a(Map<String, String> map) {
        this.h = map;
        return this;
    }

    public r a(boolean z) {
        this.f = z;
        b(true);
        return this;
    }

    public r a(byte[] bArr) {
        a(ByteBuffer.wrap(bArr));
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                if (!f()) {
                    throw new org.apache.thrift.protocol.g("Required field 'requireAck' was not found in serialized data! Struct: " + toString());
                }
                n();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2983a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = new j();
                        this.b.a(fVar);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.b != 2) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = fVar.q();
                        b(true);
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 8:
                    if (i2.b != 13) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        org.apache.thrift.protocol.e k2 = fVar.k();
                        this.h = new HashMap(k2.c * 2);
                        for (int i3 = 0; i3 < k2.c; i3++) {
                            this.h.put(fVar.w(), fVar.w());
                        }
                        fVar.l();
                        break;
                    }
                case 9:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.i = fVar.w();
                        break;
                    }
                case 10:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.j = fVar.w();
                        break;
                    }
                case 11:
                case 12:
                case 13:
                default:
                    i.a(fVar, i2.b);
                    break;
                case 14:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.k = fVar.x();
                        break;
                    }
            }
            fVar.j();
        }
    }

    public boolean a() {
        return this.f2983a != null;
    }

    public r b(String str) {
        this.d = str;
        return this;
    }

    public void b(f fVar) {
        n();
        fVar.a(m);
        if (this.f2983a != null && a()) {
            fVar.a(n);
            fVar.a(this.f2983a);
            fVar.b();
        }
        if (this.b != null && b()) {
            fVar.a(o);
            this.b.b(fVar);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(p);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null && d()) {
            fVar.a(q);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && e()) {
            fVar.a(r);
            fVar.a(this.e);
            fVar.b();
        }
        fVar.a(s);
        fVar.a(this.f);
        fVar.b();
        if (this.g != null && g()) {
            fVar.a(t);
            fVar.a(this.g);
            fVar.b();
        }
        if (this.h != null && i()) {
            fVar.a(u);
            fVar.a(new org.apache.thrift.protocol.e((byte) 11, (byte) 11, this.h.size()));
            for (Map.Entry next : this.h.entrySet()) {
                fVar.a((String) next.getKey());
                fVar.a((String) next.getValue());
            }
            fVar.d();
            fVar.b();
        }
        if (this.i != null && j()) {
            fVar.a(v);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && k()) {
            fVar.a(w);
            fVar.a(this.j);
            fVar.b();
        }
        if (this.k != null && m()) {
            fVar.a(x);
            fVar.a(this.k);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.y.set(0, z);
    }

    public boolean b() {
        return this.b != null;
    }

    public r c(String str) {
        this.e = str;
        return this;
    }

    public boolean d() {
        return this.d != null;
    }

    public boolean e() {
        return this.e != null;
    }

    public boolean f() {
        return this.y.get(0);
    }

    public boolean g() {
        return this.g != null;
    }

    public Map<String, String> h() {
        return this.h;
    }

    public boolean i() {
        return this.h != null;
    }

    public boolean j() {
        return this.i != null;
    }

    public boolean k() {
        return this.j != null;
    }

    public byte[] l() {
        a(org.apache.thrift.c.c(this.k));
        return this.k.array();
    }

    public boolean m() {
        return this.k != null;
    }

    public void n() {
        if (this.c == null) {
            throw new org.apache.thrift.protocol.g("Required field 'id' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionNotification(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.f2983a == null) {
                sb.append("null");
            } else {
                sb.append(this.f2983a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        if (d()) {
            sb.append(", ");
            sb.append("appId:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("type:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        sb.append(", ");
        sb.append("requireAck:");
        sb.append(this.f);
        if (g()) {
            sb.append(", ");
            sb.append("payload:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("extra:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("category:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (m()) {
            sb.append(", ");
            sb.append("binaryExtra:");
            if (this.k == null) {
                sb.append("null");
            } else {
                org.apache.thrift.c.a(this.k, sb);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
