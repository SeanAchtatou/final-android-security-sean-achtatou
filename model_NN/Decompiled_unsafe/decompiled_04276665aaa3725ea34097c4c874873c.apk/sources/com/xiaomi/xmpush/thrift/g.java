package com.xiaomi.xmpush.thrift;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class g implements Serializable, Cloneable, b<g, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> h;
    private static final k i = new k("OnlineConfigItem");
    private static final c j = new c("key", (byte) 8, 1);
    private static final c k = new c("type", (byte) 8, 2);
    private static final c l = new c("clear", (byte) 2, 3);
    private static final c m = new c("intValue", (byte) 8, 4);
    private static final c n = new c("longValue", (byte) 10, 5);
    private static final c o = new c("stringValue", (byte) 11, 6);
    private static final c p = new c("boolValue", (byte) 2, 7);

    /* renamed from: a  reason: collision with root package name */
    public int f2961a;
    public int b;
    public boolean c;
    public int d;
    public long e;
    public String f;
    public boolean g;
    private BitSet q = new BitSet(6);

    public enum a {
        KEY(1, "key"),
        TYPE(2, "type"),
        CLEAR(3, "clear"),
        INT_VALUE(4, "intValue"),
        LONG_VALUE(5, "longValue"),
        STRING_VALUE(6, "stringValue"),
        BOOL_VALUE(7, "boolValue");
        
        private static final Map<String, a> h = new HashMap();
        private final short i;
        private final String j;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                h.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.i = s;
            this.j = str;
        }

        public String a() {
            return this.j;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.g$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.KEY, (Object) new org.apache.thrift.meta_data.b("key", (byte) 2, new org.apache.thrift.meta_data.c((byte) 8)));
        enumMap.put((Object) a.TYPE, (Object) new org.apache.thrift.meta_data.b("type", (byte) 2, new org.apache.thrift.meta_data.c((byte) 8)));
        enumMap.put((Object) a.CLEAR, (Object) new org.apache.thrift.meta_data.b("clear", (byte) 2, new org.apache.thrift.meta_data.c((byte) 2)));
        enumMap.put((Object) a.INT_VALUE, (Object) new org.apache.thrift.meta_data.b("intValue", (byte) 2, new org.apache.thrift.meta_data.c((byte) 8)));
        enumMap.put((Object) a.LONG_VALUE, (Object) new org.apache.thrift.meta_data.b("longValue", (byte) 2, new org.apache.thrift.meta_data.c((byte) 10)));
        enumMap.put((Object) a.STRING_VALUE, (Object) new org.apache.thrift.meta_data.b("stringValue", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.BOOL_VALUE, (Object) new org.apache.thrift.meta_data.b("boolValue", (byte) 2, new org.apache.thrift.meta_data.c((byte) 2)));
        h = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(g.class, h);
    }

    public int a() {
        return this.f2961a;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                n();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 8) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2961a = fVar.t();
                        a(true);
                        break;
                    }
                case 2:
                    if (i2.b != 8) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = fVar.t();
                        b(true);
                        break;
                    }
                case 3:
                    if (i2.b != 2) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.q();
                        c(true);
                        break;
                    }
                case 4:
                    if (i2.b != 8) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.t();
                        d(true);
                        break;
                    }
                case 5:
                    if (i2.b != 10) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.u();
                        e(true);
                        break;
                    }
                case 6:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = fVar.w();
                        break;
                    }
                case 7:
                    if (i2.b != 2) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.q();
                        f(true);
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z) {
        this.q.set(0, z);
    }

    public void b(f fVar) {
        n();
        fVar.a(i);
        if (b()) {
            fVar.a(j);
            fVar.a(this.f2961a);
            fVar.b();
        }
        if (d()) {
            fVar.a(k);
            fVar.a(this.b);
            fVar.b();
        }
        if (e()) {
            fVar.a(l);
            fVar.a(this.c);
            fVar.b();
        }
        if (g()) {
            fVar.a(m);
            fVar.a(this.d);
            fVar.b();
        }
        if (i()) {
            fVar.a(n);
            fVar.a(this.e);
            fVar.b();
        }
        if (this.f != null && k()) {
            fVar.a(o);
            fVar.a(this.f);
            fVar.b();
        }
        if (m()) {
            fVar.a(p);
            fVar.a(this.g);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.q.set(1, z);
    }

    public boolean b() {
        return this.q.get(0);
    }

    public int c() {
        return this.b;
    }

    public void c(boolean z) {
        this.q.set(2, z);
    }

    public void d(boolean z) {
        this.q.set(3, z);
    }

    public boolean d() {
        return this.q.get(1);
    }

    public void e(boolean z) {
        this.q.set(4, z);
    }

    public boolean e() {
        return this.q.get(2);
    }

    public int f() {
        return this.d;
    }

    public void f(boolean z) {
        this.q.set(5, z);
    }

    public boolean g() {
        return this.q.get(3);
    }

    public long h() {
        return this.e;
    }

    public boolean i() {
        return this.q.get(4);
    }

    public String j() {
        return this.f;
    }

    public boolean k() {
        return this.f != null;
    }

    public boolean l() {
        return this.g;
    }

    public boolean m() {
        return this.q.get(5);
    }

    public void n() {
    }
}
