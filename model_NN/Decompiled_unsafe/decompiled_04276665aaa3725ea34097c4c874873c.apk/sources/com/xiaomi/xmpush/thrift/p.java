package com.xiaomi.xmpush.thrift;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.meta_data.d;
import org.apache.thrift.meta_data.g;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class p implements Serializable, Cloneable, b<p, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> b;
    private static final k c = new k("XmPushActionCustomConfig");
    private static final c d = new c("customConfigs", (byte) 15, 1);

    /* renamed from: a  reason: collision with root package name */
    public List<g> f2979a;

    public enum a {
        CUSTOM_CONFIGS(1, "customConfigs");
        
        private static final Map<String, a> b = new HashMap();
        private final short c;
        private final String d;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                b.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.c = s;
            this.d = str;
        }

        public String a() {
            return this.d;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.p$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.CUSTOM_CONFIGS, (Object) new org.apache.thrift.meta_data.b("customConfigs", (byte) 1, new d((byte) 15, new g((byte) 12, g.class))));
        b = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(p.class, b);
    }

    public List<g> a() {
        return this.f2979a;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i = fVar.i();
            if (i.b == 0) {
                fVar.h();
                c();
                return;
            }
            switch (i.c) {
                case 1:
                    if (i.b != 15) {
                        i.a(fVar, i.b);
                        break;
                    } else {
                        org.apache.thrift.protocol.d m = fVar.m();
                        this.f2979a = new ArrayList(m.b);
                        for (int i2 = 0; i2 < m.b; i2++) {
                            g gVar = new g();
                            gVar.a(fVar);
                            this.f2979a.add(gVar);
                        }
                        fVar.n();
                        break;
                    }
                default:
                    i.a(fVar, i.b);
                    break;
            }
            fVar.j();
        }
    }

    public void b(f fVar) {
        c();
        fVar.a(c);
        if (this.f2979a != null) {
            fVar.a(d);
            fVar.a(new org.apache.thrift.protocol.d((byte) 12, this.f2979a.size()));
            for (g b2 : this.f2979a) {
                b2.b(fVar);
            }
            fVar.e();
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void c() {
        if (this.f2979a == null) {
            throw new org.apache.thrift.protocol.g("Required field 'customConfigs' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("XmPushActionCustomConfig(");
        sb.append("customConfigs:");
        if (this.f2979a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2979a);
        }
        sb.append(")");
        return sb.toString();
    }
}
