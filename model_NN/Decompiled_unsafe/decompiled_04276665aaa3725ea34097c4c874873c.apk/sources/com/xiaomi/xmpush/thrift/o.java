package com.xiaomi.xmpush.thrift;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.meta_data.g;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class o implements Serializable, Cloneable, b<o, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> i;
    private static final k j = new k("XmPushActionContainer");
    private static final c k = new c("action", (byte) 8, 1);
    private static final c l = new c("encryptAction", (byte) 2, 2);
    private static final c m = new c("isRequest", (byte) 2, 3);
    private static final c n = new c("pushAction", (byte) 11, 4);
    private static final c o = new c("appid", (byte) 11, 5);
    private static final c p = new c("packageName", (byte) 11, 6);
    private static final c q = new c("target", (byte) 12, 7);
    private static final c r = new c("metaInfo", (byte) 12, 8);

    /* renamed from: a  reason: collision with root package name */
    public a f2977a;
    public boolean b = true;
    public boolean c = true;
    public ByteBuffer d;
    public String e;
    public String f;
    public j g;
    public i h;
    private BitSet s = new BitSet(2);

    public enum a {
        ACTION(1, "action"),
        ENCRYPT_ACTION(2, "encryptAction"),
        IS_REQUEST(3, "isRequest"),
        PUSH_ACTION(4, "pushAction"),
        APPID(5, "appid"),
        PACKAGE_NAME(6, "packageName"),
        TARGET(7, "target"),
        META_INFO(8, "metaInfo");
        
        private static final Map<String, a> i = new HashMap();
        private final short j;
        private final String k;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                i.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.j = s;
            this.k = str;
        }

        public String a() {
            return this.k;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.o$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.ACTION, (Object) new org.apache.thrift.meta_data.b("action", (byte) 1, new org.apache.thrift.meta_data.a((byte) 16, a.class)));
        enumMap.put((Object) a.ENCRYPT_ACTION, (Object) new org.apache.thrift.meta_data.b("encryptAction", (byte) 1, new org.apache.thrift.meta_data.c((byte) 2)));
        enumMap.put((Object) a.IS_REQUEST, (Object) new org.apache.thrift.meta_data.b("isRequest", (byte) 1, new org.apache.thrift.meta_data.c((byte) 2)));
        enumMap.put((Object) a.PUSH_ACTION, (Object) new org.apache.thrift.meta_data.b("pushAction", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.APPID, (Object) new org.apache.thrift.meta_data.b("appid", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.thrift.meta_data.b("packageName", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.thrift.meta_data.b("target", (byte) 1, new g((byte) 12, j.class)));
        enumMap.put((Object) a.META_INFO, (Object) new org.apache.thrift.meta_data.b("metaInfo", (byte) 2, new g((byte) 12, i.class)));
        i = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(o.class, i);
    }

    public a a() {
        return this.f2977a;
    }

    public o a(a aVar) {
        this.f2977a = aVar;
        return this;
    }

    public o a(i iVar) {
        this.h = iVar;
        return this;
    }

    public o a(j jVar) {
        this.g = jVar;
        return this;
    }

    public o a(String str) {
        this.e = str;
        return this;
    }

    public o a(ByteBuffer byteBuffer) {
        this.d = byteBuffer;
        return this;
    }

    public o a(boolean z) {
        this.b = z;
        b(true);
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                if (!d()) {
                    throw new org.apache.thrift.protocol.g("Required field 'encryptAction' was not found in serialized data! Struct: " + toString());
                } else if (!e()) {
                    throw new org.apache.thrift.protocol.g("Required field 'isRequest' was not found in serialized data! Struct: " + toString());
                } else {
                    o();
                    return;
                }
            } else {
                switch (i2.c) {
                    case 1:
                        if (i2.b != 8) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.f2977a = a.a(fVar.t());
                            break;
                        }
                    case 2:
                        if (i2.b != 2) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.b = fVar.q();
                            b(true);
                            break;
                        }
                    case 3:
                        if (i2.b != 2) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.c = fVar.q();
                            d(true);
                            break;
                        }
                    case 4:
                        if (i2.b != 11) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.d = fVar.x();
                            break;
                        }
                    case 5:
                        if (i2.b != 11) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.e = fVar.w();
                            break;
                        }
                    case 6:
                        if (i2.b != 11) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.f = fVar.w();
                            break;
                        }
                    case 7:
                        if (i2.b != 12) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.g = new j();
                            this.g.a(fVar);
                            break;
                        }
                    case 8:
                        if (i2.b != 12) {
                            i.a(fVar, i2.b);
                            break;
                        } else {
                            this.h = new i();
                            this.h.a(fVar);
                            break;
                        }
                    default:
                        i.a(fVar, i2.b);
                        break;
                }
                fVar.j();
            }
        }
    }

    public o b(String str) {
        this.f = str;
        return this;
    }

    public void b(f fVar) {
        o();
        fVar.a(j);
        if (this.f2977a != null) {
            fVar.a(k);
            fVar.a(this.f2977a.a());
            fVar.b();
        }
        fVar.a(l);
        fVar.a(this.b);
        fVar.b();
        fVar.a(m);
        fVar.a(this.c);
        fVar.b();
        if (this.d != null) {
            fVar.a(n);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && i()) {
            fVar.a(o);
            fVar.a(this.e);
            fVar.b();
        }
        if (this.f != null && k()) {
            fVar.a(p);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null) {
            fVar.a(q);
            this.g.b(fVar);
            fVar.b();
        }
        if (this.h != null && n()) {
            fVar.a(r);
            this.h.b(fVar);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.s.set(0, z);
    }

    public o c(boolean z) {
        this.c = z;
        d(true);
        return this;
    }

    public boolean c() {
        return this.b;
    }

    public void d(boolean z) {
        this.s.set(1, z);
    }

    public boolean d() {
        return this.s.get(0);
    }

    public boolean e() {
        return this.s.get(1);
    }

    public byte[] f() {
        a(org.apache.thrift.c.c(this.d));
        return this.d.array();
    }

    public String h() {
        return this.e;
    }

    public boolean i() {
        return this.e != null;
    }

    public String j() {
        return this.f;
    }

    public boolean k() {
        return this.f != null;
    }

    public i m() {
        return this.h;
    }

    public boolean n() {
        return this.h != null;
    }

    public void o() {
        if (this.f2977a == null) {
            throw new org.apache.thrift.protocol.g("Required field 'action' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.thrift.protocol.g("Required field 'pushAction' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new org.apache.thrift.protocol.g("Required field 'target' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("XmPushActionContainer(");
        sb.append("action:");
        if (this.f2977a == null) {
            sb.append("null");
        } else {
            sb.append(this.f2977a);
        }
        sb.append(", ");
        sb.append("encryptAction:");
        sb.append(this.b);
        sb.append(", ");
        sb.append("isRequest:");
        sb.append(this.c);
        sb.append(", ");
        sb.append("pushAction:");
        if (this.d == null) {
            sb.append("null");
        } else {
            org.apache.thrift.c.a(this.d, sb);
        }
        if (i()) {
            sb.append(", ");
            sb.append("appid:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        sb.append(", ");
        sb.append("target:");
        if (this.g == null) {
            sb.append("null");
        } else {
            sb.append(this.g);
        }
        if (n()) {
            sb.append(", ");
            sb.append("metaInfo:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
