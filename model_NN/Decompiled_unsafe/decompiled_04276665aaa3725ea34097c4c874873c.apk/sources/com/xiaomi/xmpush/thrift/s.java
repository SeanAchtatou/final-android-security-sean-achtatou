package com.xiaomi.xmpush.thrift;

import com.igexin.assist.sdk.AssistPushConsts;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.meta_data.e;
import org.apache.thrift.meta_data.g;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class s implements Serializable, Cloneable, b<s, a> {
    private static final c A = new c("id", (byte) 11, 3);
    private static final c B = new c("appId", (byte) 11, 4);
    private static final c C = new c("appVersion", (byte) 11, 5);
    private static final c D = new c("packageName", (byte) 11, 6);
    private static final c E = new c(AssistPushConsts.MSG_TYPE_TOKEN, (byte) 11, 7);
    private static final c F = new c("deviceId", (byte) 11, 8);
    private static final c G = new c("aliasName", (byte) 11, 9);
    private static final c H = new c("sdkVersion", (byte) 11, 10);
    private static final c I = new c("regId", (byte) 11, 11);
    private static final c J = new c("pushSdkVersionName", (byte) 11, 12);
    private static final c K = new c("pushSdkVersionCode", (byte) 8, 13);
    private static final c L = new c("appVersionCode", (byte) 8, 14);
    private static final c M = new c("androidId", (byte) 11, 15);
    private static final c N = new c("imei", (byte) 11, 16);
    private static final c O = new c("serial", (byte) 11, 17);
    private static final c P = new c("imeiMd5", (byte) 11, 18);
    private static final c Q = new c("spaceId", (byte) 8, 19);
    private static final c R = new c("connectionAttrs", (byte) 13, 100);
    private static final c S = new c("cleanOldRegInfo", (byte) 2, 101);
    private static final c T = new c("oldRegId", (byte) 11, 102);
    public static final Map<a, org.apache.thrift.meta_data.b> w;
    private static final k x = new k("XmPushActionRegistration");
    private static final c y = new c("debug", (byte) 11, 1);
    private static final c z = new c("target", (byte) 12, 2);
    private BitSet U = new BitSet(4);

    /* renamed from: a  reason: collision with root package name */
    public String f2985a;
    public j b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;
    public int m;
    public int n;
    public String o;
    public String p;
    public String q;
    public String r;
    public int s;
    public Map<String, String> t;
    public boolean u = false;
    public String v;

    public enum a {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, "id"),
        APP_ID(4, "appId"),
        APP_VERSION(5, "appVersion"),
        PACKAGE_NAME(6, "packageName"),
        TOKEN(7, AssistPushConsts.MSG_TYPE_TOKEN),
        DEVICE_ID(8, "deviceId"),
        ALIAS_NAME(9, "aliasName"),
        SDK_VERSION(10, "sdkVersion"),
        REG_ID(11, "regId"),
        PUSH_SDK_VERSION_NAME(12, "pushSdkVersionName"),
        PUSH_SDK_VERSION_CODE(13, "pushSdkVersionCode"),
        APP_VERSION_CODE(14, "appVersionCode"),
        ANDROID_ID(15, "androidId"),
        IMEI(16, "imei"),
        SERIAL(17, "serial"),
        IMEI_MD5(18, "imeiMd5"),
        SPACE_ID(19, "spaceId"),
        CONNECTION_ATTRS(100, "connectionAttrs"),
        CLEAN_OLD_REG_INFO(101, "cleanOldRegInfo"),
        OLD_REG_ID(102, "oldRegId");
        
        private static final Map<String, a> w = new HashMap();
        private final short x;
        private final String y;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                w.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.x = s;
            this.y = str;
        }

        public String a() {
            return this.y;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.s$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new org.apache.thrift.meta_data.b("debug", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.thrift.meta_data.b("target", (byte) 2, new g((byte) 12, j.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.thrift.meta_data.b("id", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.thrift.meta_data.b("appId", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.APP_VERSION, (Object) new org.apache.thrift.meta_data.b("appVersion", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.thrift.meta_data.b("packageName", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.TOKEN, (Object) new org.apache.thrift.meta_data.b(AssistPushConsts.MSG_TYPE_TOKEN, (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.DEVICE_ID, (Object) new org.apache.thrift.meta_data.b("deviceId", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.ALIAS_NAME, (Object) new org.apache.thrift.meta_data.b("aliasName", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.SDK_VERSION, (Object) new org.apache.thrift.meta_data.b("sdkVersion", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.REG_ID, (Object) new org.apache.thrift.meta_data.b("regId", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.PUSH_SDK_VERSION_NAME, (Object) new org.apache.thrift.meta_data.b("pushSdkVersionName", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.PUSH_SDK_VERSION_CODE, (Object) new org.apache.thrift.meta_data.b("pushSdkVersionCode", (byte) 2, new org.apache.thrift.meta_data.c((byte) 8)));
        enumMap.put((Object) a.APP_VERSION_CODE, (Object) new org.apache.thrift.meta_data.b("appVersionCode", (byte) 2, new org.apache.thrift.meta_data.c((byte) 8)));
        enumMap.put((Object) a.ANDROID_ID, (Object) new org.apache.thrift.meta_data.b("androidId", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.IMEI, (Object) new org.apache.thrift.meta_data.b("imei", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.SERIAL, (Object) new org.apache.thrift.meta_data.b("serial", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.IMEI_MD5, (Object) new org.apache.thrift.meta_data.b("imeiMd5", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.SPACE_ID, (Object) new org.apache.thrift.meta_data.b("spaceId", (byte) 2, new org.apache.thrift.meta_data.c((byte) 8)));
        enumMap.put((Object) a.CONNECTION_ATTRS, (Object) new org.apache.thrift.meta_data.b("connectionAttrs", (byte) 2, new e((byte) 13, new org.apache.thrift.meta_data.c((byte) 11), new org.apache.thrift.meta_data.c((byte) 11))));
        enumMap.put((Object) a.CLEAN_OLD_REG_INFO, (Object) new org.apache.thrift.meta_data.b("cleanOldRegInfo", (byte) 2, new org.apache.thrift.meta_data.c((byte) 2)));
        enumMap.put((Object) a.OLD_REG_ID, (Object) new org.apache.thrift.meta_data.b("oldRegId", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        w = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(s.class, w);
    }

    public s a(int i2) {
        this.m = i2;
        a(true);
        return this;
    }

    public s a(String str) {
        this.c = str;
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.b == 0) {
                fVar.h();
                y();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f2985a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.b != 12) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.b = new j();
                        this.b.a(fVar);
                        break;
                    }
                case 3:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.f = fVar.w();
                        break;
                    }
                case 7:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 8:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.h = fVar.w();
                        break;
                    }
                case 9:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.i = fVar.w();
                        break;
                    }
                case 10:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.j = fVar.w();
                        break;
                    }
                case 11:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.k = fVar.w();
                        break;
                    }
                case 12:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.l = fVar.w();
                        break;
                    }
                case 13:
                    if (i2.b != 8) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.m = fVar.t();
                        a(true);
                        break;
                    }
                case 14:
                    if (i2.b != 8) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.n = fVar.t();
                        b(true);
                        break;
                    }
                case 15:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.o = fVar.w();
                        break;
                    }
                case 16:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.p = fVar.w();
                        break;
                    }
                case 17:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.q = fVar.w();
                        break;
                    }
                case 18:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.r = fVar.w();
                        break;
                    }
                case 19:
                    if (i2.b != 8) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.s = fVar.t();
                        c(true);
                        break;
                    }
                case 100:
                    if (i2.b != 13) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        org.apache.thrift.protocol.e k2 = fVar.k();
                        this.t = new HashMap(k2.c * 2);
                        for (int i3 = 0; i3 < k2.c; i3++) {
                            this.t.put(fVar.w(), fVar.w());
                        }
                        fVar.l();
                        break;
                    }
                case 101:
                    if (i2.b != 2) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.u = fVar.q();
                        d(true);
                        break;
                    }
                case 102:
                    if (i2.b != 11) {
                        i.a(fVar, i2.b);
                        break;
                    } else {
                        this.v = fVar.w();
                        break;
                    }
                default:
                    i.a(fVar, i2.b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z2) {
        this.U.set(0, z2);
    }

    public boolean a() {
        return this.f2985a != null;
    }

    public s b(int i2) {
        this.n = i2;
        b(true);
        return this;
    }

    public s b(String str) {
        this.d = str;
        return this;
    }

    public void b(f fVar) {
        y();
        fVar.a(x);
        if (this.f2985a != null && a()) {
            fVar.a(y);
            fVar.a(this.f2985a);
            fVar.b();
        }
        if (this.b != null && b()) {
            fVar.a(z);
            this.b.b(fVar);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(A);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null) {
            fVar.a(B);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && f()) {
            fVar.a(C);
            fVar.a(this.e);
            fVar.b();
        }
        if (this.f != null && g()) {
            fVar.a(D);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null) {
            fVar.a(E);
            fVar.a(this.g);
            fVar.b();
        }
        if (this.h != null && j()) {
            fVar.a(F);
            fVar.a(this.h);
            fVar.b();
        }
        if (this.i != null && k()) {
            fVar.a(G);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && l()) {
            fVar.a(H);
            fVar.a(this.j);
            fVar.b();
        }
        if (this.k != null && m()) {
            fVar.a(I);
            fVar.a(this.k);
            fVar.b();
        }
        if (this.l != null && n()) {
            fVar.a(J);
            fVar.a(this.l);
            fVar.b();
        }
        if (o()) {
            fVar.a(K);
            fVar.a(this.m);
            fVar.b();
        }
        if (p()) {
            fVar.a(L);
            fVar.a(this.n);
            fVar.b();
        }
        if (this.o != null && q()) {
            fVar.a(M);
            fVar.a(this.o);
            fVar.b();
        }
        if (this.p != null && r()) {
            fVar.a(N);
            fVar.a(this.p);
            fVar.b();
        }
        if (this.q != null && s()) {
            fVar.a(O);
            fVar.a(this.q);
            fVar.b();
        }
        if (this.r != null && t()) {
            fVar.a(P);
            fVar.a(this.r);
            fVar.b();
        }
        if (u()) {
            fVar.a(Q);
            fVar.a(this.s);
            fVar.b();
        }
        if (this.t != null && v()) {
            fVar.a(R);
            fVar.a(new org.apache.thrift.protocol.e((byte) 11, (byte) 11, this.t.size()));
            for (Map.Entry next : this.t.entrySet()) {
                fVar.a((String) next.getKey());
                fVar.a((String) next.getValue());
            }
            fVar.d();
            fVar.b();
        }
        if (w()) {
            fVar.a(S);
            fVar.a(this.u);
            fVar.b();
        }
        if (this.v != null && x()) {
            fVar.a(T);
            fVar.a(this.v);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z2) {
        this.U.set(1, z2);
    }

    public boolean b() {
        return this.b != null;
    }

    public s c(int i2) {
        this.s = i2;
        c(true);
        return this;
    }

    public s c(String str) {
        this.e = str;
        return this;
    }

    public void c(boolean z2) {
        this.U.set(2, z2);
    }

    public s d(String str) {
        this.f = str;
        return this;
    }

    public void d(boolean z2) {
        this.U.set(3, z2);
    }

    public s e(String str) {
        this.g = str;
        return this;
    }

    public s f(String str) {
        this.h = str;
        return this;
    }

    public boolean f() {
        return this.e != null;
    }

    public s g(String str) {
        this.l = str;
        return this;
    }

    public boolean g() {
        return this.f != null;
    }

    public s h(String str) {
        this.o = str;
        return this;
    }

    public s i(String str) {
        this.p = str;
        return this;
    }

    public s j(String str) {
        this.q = str;
        return this;
    }

    public boolean j() {
        return this.h != null;
    }

    public s k(String str) {
        this.r = str;
        return this;
    }

    public boolean k() {
        return this.i != null;
    }

    public boolean l() {
        return this.j != null;
    }

    public boolean m() {
        return this.k != null;
    }

    public boolean n() {
        return this.l != null;
    }

    public boolean o() {
        return this.U.get(0);
    }

    public boolean p() {
        return this.U.get(1);
    }

    public boolean q() {
        return this.o != null;
    }

    public boolean r() {
        return this.p != null;
    }

    public boolean s() {
        return this.q != null;
    }

    public boolean t() {
        return this.r != null;
    }

    public String toString() {
        boolean z2 = false;
        StringBuilder sb = new StringBuilder("XmPushActionRegistration(");
        boolean z3 = true;
        if (a()) {
            sb.append("debug:");
            if (this.f2985a == null) {
                sb.append("null");
            } else {
                sb.append(this.f2985a);
            }
            z3 = false;
        }
        if (b()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.b == null) {
                sb.append("null");
            } else {
                sb.append(this.b);
            }
        } else {
            z2 = z3;
        }
        if (!z2) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (f()) {
            sb.append(", ");
            sb.append("appVersion:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        sb.append(", ");
        sb.append("token:");
        if (this.g == null) {
            sb.append("null");
        } else {
            sb.append(this.g);
        }
        if (j()) {
            sb.append(", ");
            sb.append("deviceId:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("aliasName:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (l()) {
            sb.append(", ");
            sb.append("sdkVersion:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (m()) {
            sb.append(", ");
            sb.append("regId:");
            if (this.k == null) {
                sb.append("null");
            } else {
                sb.append(this.k);
            }
        }
        if (n()) {
            sb.append(", ");
            sb.append("pushSdkVersionName:");
            if (this.l == null) {
                sb.append("null");
            } else {
                sb.append(this.l);
            }
        }
        if (o()) {
            sb.append(", ");
            sb.append("pushSdkVersionCode:");
            sb.append(this.m);
        }
        if (p()) {
            sb.append(", ");
            sb.append("appVersionCode:");
            sb.append(this.n);
        }
        if (q()) {
            sb.append(", ");
            sb.append("androidId:");
            if (this.o == null) {
                sb.append("null");
            } else {
                sb.append(this.o);
            }
        }
        if (r()) {
            sb.append(", ");
            sb.append("imei:");
            if (this.p == null) {
                sb.append("null");
            } else {
                sb.append(this.p);
            }
        }
        if (s()) {
            sb.append(", ");
            sb.append("serial:");
            if (this.q == null) {
                sb.append("null");
            } else {
                sb.append(this.q);
            }
        }
        if (t()) {
            sb.append(", ");
            sb.append("imeiMd5:");
            if (this.r == null) {
                sb.append("null");
            } else {
                sb.append(this.r);
            }
        }
        if (u()) {
            sb.append(", ");
            sb.append("spaceId:");
            sb.append(this.s);
        }
        if (v()) {
            sb.append(", ");
            sb.append("connectionAttrs:");
            if (this.t == null) {
                sb.append("null");
            } else {
                sb.append(this.t);
            }
        }
        if (w()) {
            sb.append(", ");
            sb.append("cleanOldRegInfo:");
            sb.append(this.u);
        }
        if (x()) {
            sb.append(", ");
            sb.append("oldRegId:");
            if (this.v == null) {
                sb.append("null");
            } else {
                sb.append(this.v);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public boolean u() {
        return this.U.get(2);
    }

    public boolean v() {
        return this.t != null;
    }

    public boolean w() {
        return this.U.get(3);
    }

    public boolean x() {
        return this.v != null;
    }

    public void y() {
        if (this.c == null) {
            throw new org.apache.thrift.protocol.g("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.thrift.protocol.g("Required field 'appId' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new org.apache.thrift.protocol.g("Required field 'token' was not present! Struct: " + toString());
        }
    }
}
