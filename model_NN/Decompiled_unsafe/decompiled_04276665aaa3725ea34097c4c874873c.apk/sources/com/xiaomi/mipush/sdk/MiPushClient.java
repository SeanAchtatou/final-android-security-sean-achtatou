package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.text.TextUtils;
import com.qhad.ads.sdk.adcore.HttpCacher;
import com.xiaomi.channel.commonutils.android.e;
import com.xiaomi.channel.commonutils.android.f;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.channel.commonutils.string.d;
import com.xiaomi.push.service.v;
import com.xiaomi.xmpush.thrift.ab;
import com.xiaomi.xmpush.thrift.i;
import com.xiaomi.xmpush.thrift.m;
import com.xiaomi.xmpush.thrift.r;
import com.xiaomi.xmpush.thrift.s;
import com.xiaomi.xmpush.thrift.x;
import com.xiaomi.xmpush.thrift.z;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

public abstract class MiPushClient {
    public static final String ACCEPT_TIME_SEPARATOR = ",";
    public static final String COMMAND_REGISTER = "register";
    public static final String COMMAND_SET_ACCEPT_TIME = "accept-time";
    public static final String COMMAND_SET_ACCOUNT = "set-account";
    public static final String COMMAND_SET_ALIAS = "set-alias";
    public static final String COMMAND_SUBSCRIBE_TOPIC = "subscribe-topic";
    public static final String COMMAND_UNSET_ACCOUNT = "unset-account";
    public static final String COMMAND_UNSET_ALIAS = "unset-alias";
    public static final String COMMAND_UNSUBSCRIBE_TOPIC = "unsubscibe-topic";
    public static final String PREF_EXTRA = "mipush_extra";
    private static boolean awakeService = true;
    /* access modifiers changed from: private */
    public static Context sContext;
    private static long sCurMsgId = System.currentTimeMillis();

    @Deprecated
    public abstract class MiPushClientCallback {
        private String category;

        /* access modifiers changed from: protected */
        public String getCategory() {
            return this.category;
        }

        public void onCommandResult(String str, long j, String str2, List<String> list) {
        }

        public void onInitializeResult(long j, String str, String str2) {
        }

        public void onReceiveMessage(MiPushMessage miPushMessage) {
        }

        public void onReceiveMessage(String str, String str2, String str3, boolean z) {
        }

        public void onSubscribeResult(long j, String str, String str2) {
        }

        public void onUnsubscribeResult(long j, String str, String str2) {
        }

        /* access modifiers changed from: protected */
        public void setCategory(String str) {
            this.category = str;
        }
    }

    public class a extends Exception {

        /* renamed from: a  reason: collision with root package name */
        private PackageItemInfo f2917a;

        public a(String str, PackageItemInfo packageItemInfo) {
            super(str);
            this.f2917a = packageItemInfo;
        }
    }

    private static boolean acceptTimeSet(Context context, String str, String str2) {
        return TextUtils.equals(context.getSharedPreferences(PREF_EXTRA, 0).getString("accept_time", ""), str + ACCEPT_TIME_SEPARATOR + str2);
    }

    public static long accountSetTime(Context context, String str) {
        return context.getSharedPreferences(PREF_EXTRA, 0).getLong("account_" + str, -1);
    }

    static synchronized void addAcceptTime(Context context, String str, String str2) {
        synchronized (MiPushClient.class) {
            context.getSharedPreferences(PREF_EXTRA, 0).edit().putString("accept_time", str + ACCEPT_TIME_SEPARATOR + str2).commit();
        }
    }

    static synchronized void addAccount(Context context, String str) {
        synchronized (MiPushClient.class) {
            context.getSharedPreferences(PREF_EXTRA, 0).edit().putLong("account_" + str, System.currentTimeMillis()).commit();
        }
    }

    static synchronized void addAlias(Context context, String str) {
        synchronized (MiPushClient.class) {
            context.getSharedPreferences(PREF_EXTRA, 0).edit().putLong("alias_" + str, System.currentTimeMillis()).commit();
        }
    }

    private static void addPullNotificationTime(Context context) {
        context.getSharedPreferences(PREF_EXTRA, 0).edit().putLong("last_pull_notification", System.currentTimeMillis()).commit();
    }

    private static void addRegRequestTime(Context context) {
        context.getSharedPreferences(PREF_EXTRA, 0).edit().putLong("last_reg_request", System.currentTimeMillis()).commit();
    }

    static synchronized void addTopic(Context context, String str) {
        synchronized (MiPushClient.class) {
            context.getSharedPreferences(PREF_EXTRA, 0).edit().putLong("topic_" + str, System.currentTimeMillis()).commit();
        }
    }

    public static long aliasSetTime(Context context, String str) {
        return context.getSharedPreferences(PREF_EXTRA, 0).getLong("alias_" + str, -1);
    }

    public static void awakeApps(Context context, String[] strArr) {
        new Thread(new f(strArr, context)).start();
    }

    /* access modifiers changed from: private */
    public static void awakePushServiceByPackageInfo(Context context, PackageInfo packageInfo) {
        ServiceInfo[] serviceInfoArr = packageInfo.services;
        if (serviceInfoArr != null) {
            int length = serviceInfoArr.length;
            int i = 0;
            while (i < length) {
                ServiceInfo serviceInfo = serviceInfoArr[i];
                if (!serviceInfo.exported || !serviceInfo.enabled || !"com.xiaomi.mipush.sdk.PushMessageHandler".equals(serviceInfo.name) || context.getPackageName().equals(serviceInfo.packageName)) {
                    i++;
                } else {
                    try {
                        Thread.sleep(((long) ((Math.random() * 2.0d) + 1.0d)) * 1000);
                        Intent intent = new Intent();
                        intent.setClassName(serviceInfo.packageName, serviceInfo.name);
                        intent.setAction("com.xiaomi.mipush.sdk.WAKEUP");
                        context.startService(intent);
                        return;
                    } catch (Throwable th) {
                        return;
                    }
                }
            }
        }
    }

    private static void awakePushServices(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_EXTRA, 0);
        if (System.currentTimeMillis() - 600000 >= sharedPreferences.getLong("wake_up", 0)) {
            sharedPreferences.edit().putLong("wake_up", System.currentTimeMillis()).commit();
            if (!shouldUseMIUIPush(context) && 1 == a.a(context).m()) {
                new Thread(new e(context)).start();
            }
        }
    }

    public static void checkManifest(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4100);
            checkReceivers(context);
            checkServices(context, packageInfo);
            checkPermissions(context, packageInfo);
        } catch (PackageManager.NameNotFoundException e) {
            b.a(e);
        }
    }

    private static void checkNotNull(Object obj, String str) {
        if (obj == null) {
            throw new IllegalArgumentException("param " + str + " is not nullable");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x005f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void checkPermissions(android.content.Context r9, android.content.pm.PackageInfo r10) {
        /*
            r8 = 0
            r2 = 1
            r1 = 0
            java.util.HashSet r3 = new java.util.HashSet
            r3.<init>()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r4 = r9.getPackageName()
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = ".permission.MIPUSH_RECEIVE"
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = r0.toString()
            r0 = 7
            java.lang.String[] r0 = new java.lang.String[r0]
            java.lang.String r5 = "android.permission.INTERNET"
            r0[r1] = r5
            java.lang.String r5 = "android.permission.ACCESS_NETWORK_STATE"
            r0[r2] = r5
            r5 = 2
            r0[r5] = r4
            r5 = 3
            java.lang.String r6 = "android.permission.ACCESS_WIFI_STATE"
            r0[r5] = r6
            r5 = 4
            java.lang.String r6 = "android.permission.READ_PHONE_STATE"
            r0[r5] = r6
            r5 = 5
            java.lang.String r6 = "android.permission.GET_TASKS"
            r0[r5] = r6
            r5 = 6
            java.lang.String r6 = "android.permission.VIBRATE"
            r0[r5] = r6
            java.util.List r0 = java.util.Arrays.asList(r0)
            r3.addAll(r0)
            android.content.pm.PermissionInfo[] r0 = r10.permissions
            if (r0 == 0) goto L_0x00b5
            android.content.pm.PermissionInfo[] r5 = r10.permissions
            int r6 = r5.length
            r0 = r1
        L_0x0050:
            if (r0 >= r6) goto L_0x00b5
            r7 = r5[r0]
            java.lang.String r7 = r7.name
            boolean r7 = r4.equals(r7)
            if (r7 == 0) goto L_0x006f
            r0 = r2
        L_0x005d:
            if (r0 != 0) goto L_0x0072
            com.xiaomi.mipush.sdk.MiPushClient$a r0 = new com.xiaomi.mipush.sdk.MiPushClient$a
            java.lang.String r3 = "<permission android:name=\"%1$s\" /> is undefined."
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r1] = r4
            java.lang.String r1 = java.lang.String.format(r3, r2)
            r0.<init>(r1, r8)
            throw r0
        L_0x006f:
            int r0 = r0 + 1
            goto L_0x0050
        L_0x0072:
            java.lang.String[] r0 = r10.requestedPermissions
            if (r0 == 0) goto L_0x0093
            java.lang.String[] r4 = r10.requestedPermissions
            int r5 = r4.length
            r0 = r1
        L_0x007a:
            if (r0 >= r5) goto L_0x0093
            r6 = r4[r0]
            boolean r7 = android.text.TextUtils.isEmpty(r6)
            if (r7 != 0) goto L_0x00b1
            boolean r7 = r3.contains(r6)
            if (r7 == 0) goto L_0x00b1
            r3.remove(r6)
            boolean r6 = r3.isEmpty()
            if (r6 == 0) goto L_0x00b1
        L_0x0093:
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x00b4
            com.xiaomi.mipush.sdk.MiPushClient$a r0 = new com.xiaomi.mipush.sdk.MiPushClient$a
            java.lang.String r4 = "<use-permission android:name=\"%1$s\" /> is missing."
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.util.Iterator r3 = r3.iterator()
            java.lang.Object r3 = r3.next()
            r2[r1] = r3
            java.lang.String r1 = java.lang.String.format(r4, r2)
            r0.<init>(r1, r8)
            throw r0
        L_0x00b1:
            int r0 = r0 + 1
            goto L_0x007a
        L_0x00b4:
            return
        L_0x00b5:
            r0 = r1
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.mipush.sdk.MiPushClient.checkPermissions(android.content.Context, android.content.pm.PackageInfo):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0090 A[EDGE_INSN: B:29:0x0090->B:17:0x0090 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a7 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void checkReceivers(android.content.Context r9) {
        /*
            r6 = 2
            r2 = 0
            r3 = 1
            android.content.pm.PackageManager r1 = r9.getPackageManager()
            java.lang.String r4 = r9.getPackageName()
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r5 = "android.net.conn.CONNECTIVITY_CHANGE"
            r0.<init>(r5)
            r0.setPackage(r4)
            java.lang.Class<com.xiaomi.push.service.receivers.NetworkStatusReceiver> r5 = com.xiaomi.push.service.receivers.NetworkStatusReceiver.class
            java.lang.Boolean[] r6 = new java.lang.Boolean[r6]
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r3)
            r6[r2] = r7
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r3)
            r6[r3] = r7
            findAndCheckReceiverInfo(r1, r0, r5, r6)
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r5 = com.xiaomi.push.service.z.o
            r0.<init>(r5)
            r0.setPackage(r4)
            java.lang.String r5 = "com.xiaomi.push.service.receivers.PingReceiver"
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ ClassNotFoundException -> 0x009b }
            r6 = 2
            java.lang.Boolean[] r6 = new java.lang.Boolean[r6]     // Catch:{ ClassNotFoundException -> 0x009b }
            r7 = 0
            r8 = 1
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch:{ ClassNotFoundException -> 0x009b }
            r6[r7] = r8     // Catch:{ ClassNotFoundException -> 0x009b }
            r7 = 1
            r8 = 0
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch:{ ClassNotFoundException -> 0x009b }
            r6[r7] = r8     // Catch:{ ClassNotFoundException -> 0x009b }
            findAndCheckReceiverInfo(r1, r0, r5, r6)     // Catch:{ ClassNotFoundException -> 0x009b }
        L_0x004e:
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r5 = "com.xiaomi.mipush.RECEIVE_MESSAGE"
            r0.<init>(r5)
            r0.setPackage(r4)
            r4 = 16384(0x4000, float:2.2959E-41)
            java.util.List r0 = r1.queryBroadcastReceivers(r0, r4)
            java.util.Iterator r4 = r0.iterator()
            r1 = r2
        L_0x0063:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x00aa
            java.lang.Object r0 = r4.next()
            android.content.pm.ResolveInfo r0 = (android.content.pm.ResolveInfo) r0
            android.content.pm.ActivityInfo r0 = r0.activityInfo
            if (r0 == 0) goto L_0x00a0
            java.lang.String r5 = r0.name     // Catch:{ ClassNotFoundException -> 0x00a2 }
            boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ ClassNotFoundException -> 0x00a2 }
            if (r5 != 0) goto L_0x00a0
            java.lang.Class<com.xiaomi.mipush.sdk.PushMessageReceiver> r5 = com.xiaomi.mipush.sdk.PushMessageReceiver.class
            java.lang.String r6 = r0.name     // Catch:{ ClassNotFoundException -> 0x00a2 }
            java.lang.Class r6 = java.lang.Class.forName(r6)     // Catch:{ ClassNotFoundException -> 0x00a2 }
            boolean r5 = r5.isAssignableFrom(r6)     // Catch:{ ClassNotFoundException -> 0x00a2 }
            if (r5 == 0) goto L_0x00a0
            boolean r0 = r0.enabled     // Catch:{ ClassNotFoundException -> 0x00a2 }
            if (r0 == 0) goto L_0x00a0
            r0 = r3
        L_0x008e:
            if (r0 == 0) goto L_0x00a7
        L_0x0090:
            if (r0 != 0) goto L_0x00a9
            com.xiaomi.mipush.sdk.MiPushClient$a r0 = new com.xiaomi.mipush.sdk.MiPushClient$a
            java.lang.String r1 = "Receiver: none of the subclasses of PushMessageReceiver is enabled or defined."
            r2 = 0
            r0.<init>(r1, r2)
            throw r0
        L_0x009b:
            r0 = move-exception
            com.xiaomi.channel.commonutils.logger.b.a(r0)
            goto L_0x004e
        L_0x00a0:
            r0 = r2
            goto L_0x008e
        L_0x00a2:
            r0 = move-exception
            com.xiaomi.channel.commonutils.logger.b.a(r0)
            r0 = r1
        L_0x00a7:
            r1 = r0
            goto L_0x0063
        L_0x00a9:
            return
        L_0x00aa:
            r0 = r1
            goto L_0x0090
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.mipush.sdk.MiPushClient.checkReceivers(android.content.Context):void");
    }

    private static void checkServices(Context context, PackageInfo packageInfo) {
        HashMap hashMap = new HashMap();
        hashMap.put("com.xiaomi.push.service.XMPushService", new Object[]{true, false, ""});
        hashMap.put(PushMessageHandler.class.getCanonicalName(), new Object[]{true, true, ""});
        hashMap.put(MessageHandleService.class.getCanonicalName(), new Object[]{true, false, ""});
        hashMap.put("com.xiaomi.push.service.XMJobService", new Object[]{true, false, "android.permission.BIND_JOB_SERVICE"});
        if (packageInfo.services != null) {
            for (ServiceInfo serviceInfo : packageInfo.services) {
                if (!TextUtils.isEmpty(serviceInfo.name) && hashMap.containsKey(serviceInfo.name)) {
                    Object[] objArr = (Object[]) hashMap.remove(serviceInfo.name);
                    boolean booleanValue = ((Boolean) objArr[0]).booleanValue();
                    boolean booleanValue2 = ((Boolean) objArr[1]).booleanValue();
                    String str = (String) objArr[2];
                    if (booleanValue != serviceInfo.enabled) {
                        throw new a(String.format("Wrong attribute: %n    <service android:name=\"%1$s\" .../> android:enabled should be %<b.", serviceInfo.name, Boolean.valueOf(booleanValue)), serviceInfo);
                    } else if (booleanValue2 != serviceInfo.exported) {
                        throw new a(String.format("Wrong attribute: %n    <service android:name=\"%1$s\" .../> android:exported should be %<b.", serviceInfo.name, Boolean.valueOf(booleanValue2)), serviceInfo);
                    } else if (!TextUtils.isEmpty(str) && !TextUtils.equals(str, serviceInfo.permission)) {
                        throw new a(String.format("Wrong attribute: %n    <service android:name=\"%1$s\" .../> android:permission should be \"%2$s\".", serviceInfo.name, str), serviceInfo);
                    } else if (hashMap.isEmpty()) {
                        break;
                    }
                }
            }
        }
        if (!hashMap.isEmpty()) {
            throw new a(String.format("<service android:name=\"%1$s\" /> is missing or disabled.", hashMap.keySet().iterator().next()), null);
        }
    }

    protected static void clearExtras(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_EXTRA, 0);
        long j = sharedPreferences.getLong("wake_up", 0);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.clear();
        if (j > 0) {
            edit.putLong("wake_up", j);
        }
        edit.commit();
    }

    public static void clearLocalNotificationType(Context context) {
        j.a(context).e();
    }

    public static void clearNotification(Context context) {
        j.a(context).a(-1);
    }

    public static void clearNotification(Context context, int i) {
        j.a(context).a(i);
    }

    private static void findAndCheckReceiverInfo(PackageManager packageManager, Intent intent, Class<?> cls, Boolean[] boolArr) {
        boolean z;
        Iterator<ResolveInfo> it = packageManager.queryBroadcastReceivers(intent, 16384).iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            ActivityInfo activityInfo = it.next().activityInfo;
            if (activityInfo != null && cls.getCanonicalName().equals(activityInfo.name)) {
                if (boolArr[0].booleanValue() != activityInfo.enabled) {
                    throw new a(String.format("Wrong attribute: %n    <receiver android:name=\"%1$s\" .../> android:enabled should be %<b.", activityInfo.name, boolArr[0]), activityInfo);
                } else if (boolArr[1].booleanValue() != activityInfo.exported) {
                    throw new a(String.format("Wrong attribute: %n    <receiver android:name=\"%1$s\" .../> android:exported should be %<b.", activityInfo.name, boolArr[1]), activityInfo);
                } else {
                    z = true;
                }
            }
        }
        if (!z) {
            throw new a(String.format("<receiver android:name=\"%1$s\" /> is missing or disabled.", cls.getCanonicalName()), null);
        }
    }

    protected static synchronized String generatePacketID() {
        String str;
        synchronized (MiPushClient.class) {
            str = d.a(4) + sCurMsgId;
            sCurMsgId++;
        }
        return str;
    }

    public static List<String> getAllAlias(Context context) {
        ArrayList arrayList = new ArrayList();
        for (String next : context.getSharedPreferences(PREF_EXTRA, 0).getAll().keySet()) {
            if (next.startsWith("alias_")) {
                arrayList.add(next.substring("alias_".length()));
            }
        }
        return arrayList;
    }

    public static List<String> getAllTopic(Context context) {
        ArrayList arrayList = new ArrayList();
        for (String next : context.getSharedPreferences(PREF_EXTRA, 0).getAll().keySet()) {
            if (next.startsWith("topic_") && !next.contains("**ALL**")) {
                arrayList.add(next.substring("topic_".length()));
            }
        }
        return arrayList;
    }

    public static List<String> getAllUserAccount(Context context) {
        ArrayList arrayList = new ArrayList();
        for (String next : context.getSharedPreferences(PREF_EXTRA, 0).getAll().keySet()) {
            if (next.startsWith("account_")) {
                arrayList.add(next.substring("account_".length()));
            }
        }
        return arrayList;
    }

    private static boolean getDefaultSwitch() {
        return !e.a();
    }

    public static String getRegId(Context context) {
        if (a.a(context).i()) {
            return a.a(context).e();
        }
        return null;
    }

    @Deprecated
    public static void initialize(Context context, String str, String str2, MiPushClientCallback miPushClientCallback) {
        boolean z = false;
        checkNotNull(context, "context");
        checkNotNull(str, "appID");
        checkNotNull(str2, "appToken");
        try {
            sContext = context.getApplicationContext();
            if (sContext == null) {
                sContext = context;
            }
            if (miPushClientCallback != null) {
                PushMessageHandler.a(miPushClientCallback);
            }
            if (a.a(sContext).m() != Constants.a()) {
                z = true;
            }
            if (z || shouldSendRegRequest(sContext)) {
                if (z || !a.a(sContext).a(str, str2) || a.a(sContext).n()) {
                    String a2 = d.a(6);
                    a.a(sContext).h();
                    a.a(sContext).a(Constants.a());
                    a.a(sContext).a(str, str2, a2);
                    clearExtras(sContext);
                    s sVar = new s();
                    sVar.a(generatePacketID());
                    sVar.b(str);
                    sVar.e(str2);
                    sVar.d(context.getPackageName());
                    sVar.f(a2);
                    sVar.c(com.xiaomi.channel.commonutils.android.b.a(context, context.getPackageName()));
                    sVar.b(com.xiaomi.channel.commonutils.android.b.b(context, context.getPackageName()));
                    sVar.g("3_0_3");
                    sVar.a(30003);
                    sVar.h(com.xiaomi.push.service.d.b(sContext));
                    String d = com.xiaomi.push.service.d.d(sContext);
                    if (!TextUtils.isEmpty(d)) {
                        if (!e.a()) {
                            sVar.i(d);
                        }
                        sVar.k(d.a(d));
                    }
                    sVar.j(com.xiaomi.push.service.d.a());
                    int b = com.xiaomi.push.service.d.b();
                    if (b >= 0) {
                        sVar.c(b);
                    }
                    j.a(sContext).a(sVar, z);
                } else {
                    if (1 == PushMessageHelper.getPushMode(context)) {
                        checkNotNull(miPushClientCallback, "callback");
                        miPushClientCallback.onInitializeResult(0, null, a.a(context).e());
                    } else {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(a.a(context).e());
                        PushMessageHelper.sendCommandMessageBroadcast(sContext, PushMessageHelper.generateCommandMessage(COMMAND_REGISTER, arrayList, 0, null, null));
                    }
                    j.a(context).a();
                    if (a.a(sContext).a()) {
                        r rVar = new r();
                        rVar.b(a.a(context).c());
                        rVar.c("client_info_update");
                        rVar.a(generatePacketID());
                        rVar.h = new HashMap();
                        rVar.h.put("app_version", com.xiaomi.channel.commonutils.android.b.a(sContext, sContext.getPackageName()));
                        rVar.h.put("app_version_code", Integer.toString(com.xiaomi.channel.commonutils.android.b.b(sContext, sContext.getPackageName())));
                        rVar.h.put("push_sdk_vn", "3_0_3");
                        rVar.h.put("push_sdk_vc", Integer.toString(30003));
                        String g = a.a(sContext).g();
                        if (!TextUtils.isEmpty(g)) {
                            rVar.h.put("deviceid", g);
                        }
                        j.a(context).a(rVar, com.xiaomi.xmpush.thrift.a.Notification, false, null);
                    }
                    if (!f.a(sContext, "update_devId", false)) {
                        updateIMEI();
                        f.b(sContext, "update_devId", true);
                    }
                    if (shouldUseMIUIPush(sContext) && shouldPullNotification(sContext)) {
                        r rVar2 = new r();
                        rVar2.b(a.a(sContext).c());
                        rVar2.c("pull");
                        rVar2.a(generatePacketID());
                        rVar2.a(false);
                        j.a(sContext).a(rVar2, com.xiaomi.xmpush.thrift.a.Notification, false, null, false);
                        addPullNotificationTime(sContext);
                    }
                }
                if (awakeService) {
                    awakePushServices(sContext);
                }
                addRegRequestTime(sContext);
                scheduleOcVersionCheckJob();
                loadPlugin();
                return;
            }
            j.a(context).a();
            b.a("Could not send  register message within 5s repeatly .");
        } catch (Throwable th) {
            b.a(th);
        }
    }

    private static void loadPlugin() {
        if (v.a(sContext).a(com.xiaomi.xmpush.thrift.b.DataCollectionSwitch.a(), getDefaultSwitch())) {
            com.xiaomi.channel.commonutils.misc.d.a(sContext).a(new c(), 10);
        }
    }

    public static void pausePush(Context context, String str) {
        setAcceptTime(context, 0, 0, 0, 0, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.mipush.sdk.j.a(com.xiaomi.xmpush.thrift.s, boolean):void
     arg types: [com.xiaomi.xmpush.thrift.s, int]
     candidates:
      com.xiaomi.mipush.sdk.j.a(com.xiaomi.mipush.sdk.j, java.lang.Integer):java.lang.Integer
      com.xiaomi.mipush.sdk.j.a(com.xiaomi.xmpush.thrift.s, boolean):void */
    static void reInitialize(Context context) {
        if (a.a(context).i()) {
            String a2 = d.a(6);
            String c = a.a(context).c();
            String d = a.a(context).d();
            a.a(context).h();
            a.a(context).a(c, d, a2);
            s sVar = new s();
            sVar.a(generatePacketID());
            sVar.b(c);
            sVar.e(d);
            sVar.f(a2);
            sVar.d(context.getPackageName());
            sVar.c(com.xiaomi.channel.commonutils.android.b.a(context, context.getPackageName()));
            j.a(context).a(sVar, false);
        }
    }

    public static void registerPush(Context context, String str, String str2) {
        new Thread(new b(context, str, str2)).start();
    }

    static synchronized void removeAccount(Context context, String str) {
        synchronized (MiPushClient.class) {
            context.getSharedPreferences(PREF_EXTRA, 0).edit().remove("account_" + str).commit();
        }
    }

    static synchronized void removeAlias(Context context, String str) {
        synchronized (MiPushClient.class) {
            context.getSharedPreferences(PREF_EXTRA, 0).edit().remove("alias_" + str).commit();
        }
    }

    static synchronized void removeTopic(Context context, String str) {
        synchronized (MiPushClient.class) {
            context.getSharedPreferences(PREF_EXTRA, 0).edit().remove("topic_" + str).commit();
        }
    }

    static void reportIgnoreRegMessageClicked(Context context, String str, i iVar, String str2, String str3) {
        r rVar = new r();
        if (TextUtils.isEmpty(str3)) {
            b.d("do not report clicked message");
            return;
        }
        rVar.b(str3);
        rVar.c("bar:click");
        rVar.a(str);
        rVar.a(false);
        j.a(context).a(rVar, com.xiaomi.xmpush.thrift.a.Notification, false, true, iVar, true, str2, str3);
    }

    public static void reportMessageClicked(Context context, MiPushMessage miPushMessage) {
        i iVar = new i();
        iVar.a(miPushMessage.getMessageId());
        iVar.b(miPushMessage.getTopic());
        iVar.d(miPushMessage.getDescription());
        iVar.c(miPushMessage.getTitle());
        iVar.c(miPushMessage.getNotifyId());
        iVar.a(miPushMessage.getNotifyType());
        iVar.b(miPushMessage.getPassThrough());
        iVar.a(miPushMessage.getExtra());
        reportMessageClicked(context, miPushMessage.getMessageId(), iVar, null);
    }

    @Deprecated
    public static void reportMessageClicked(Context context, String str) {
        reportMessageClicked(context, str, null, null);
    }

    static void reportMessageClicked(Context context, String str, i iVar, String str2) {
        r rVar = new r();
        if (!TextUtils.isEmpty(str2)) {
            rVar.b(str2);
        } else if (a.a(context).b()) {
            rVar.b(a.a(context).c());
        } else {
            b.d("do not report clicked message");
            return;
        }
        rVar.c("bar:click");
        rVar.a(str);
        rVar.a(false);
        j.a(context).a(rVar, com.xiaomi.xmpush.thrift.a.Notification, false, iVar);
    }

    public static void resumePush(Context context, String str) {
        setAcceptTime(context, 0, 0, 23, 59, str);
    }

    private static void scheduleOcVersionCheckJob() {
        com.xiaomi.channel.commonutils.misc.d.a(sContext).a(new g(sContext), v.a(sContext).a(com.xiaomi.xmpush.thrift.b.OcVersionCheckFrequency.a(), (int) HttpCacher.TIME_DAY), 5);
    }

    public static void setAcceptTime(Context context, int i, int i2, int i3, int i4, String str) {
        if (i < 0 || i >= 24 || i3 < 0 || i3 >= 24 || i2 < 0 || i2 >= 60 || i4 < 0 || i4 >= 60) {
            throw new IllegalArgumentException("the input parameter is not valid.");
        }
        long rawOffset = (long) (((TimeZone.getTimeZone("GMT+08").getRawOffset() - TimeZone.getDefault().getRawOffset()) / 1000) / 60);
        long j = ((((long) ((i * 60) + i2)) + rawOffset) + 1440) % 1440;
        long j2 = ((rawOffset + ((long) ((i3 * 60) + i4))) + 1440) % 1440;
        ArrayList arrayList = new ArrayList();
        arrayList.add(String.format("%1$02d:%2$02d", Long.valueOf(j / 60), Long.valueOf(j % 60)));
        arrayList.add(String.format("%1$02d:%2$02d", Long.valueOf(j2 / 60), Long.valueOf(j2 % 60)));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(String.format("%1$02d:%2$02d", Integer.valueOf(i), Integer.valueOf(i2)));
        arrayList2.add(String.format("%1$02d:%2$02d", Integer.valueOf(i3), Integer.valueOf(i4)));
        if (!acceptTimeSet(context, (String) arrayList.get(0), (String) arrayList.get(1))) {
            setCommand(context, COMMAND_SET_ACCEPT_TIME, arrayList, str);
        } else if (1 == PushMessageHelper.getPushMode(context)) {
            PushMessageHandler.a(context, str, COMMAND_SET_ACCEPT_TIME, 0, null, arrayList2);
        } else {
            PushMessageHelper.sendCommandMessageBroadcast(context, PushMessageHelper.generateCommandMessage(COMMAND_SET_ACCEPT_TIME, arrayList2, 0, null, null));
        }
    }

    public static void setAlias(Context context, String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            setCommand(context, COMMAND_SET_ALIAS, str, str2);
        }
    }

    protected static void setCommand(Context context, String str, String str2, String str3) {
        ArrayList arrayList = new ArrayList();
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
        }
        if (!COMMAND_SET_ALIAS.equalsIgnoreCase(str) || System.currentTimeMillis() - aliasSetTime(context, str2) >= 3600000) {
            if (COMMAND_UNSET_ALIAS.equalsIgnoreCase(str) && aliasSetTime(context, str2) < 0) {
                b.a("Don't cancel alias for " + arrayList + " is unseted");
            } else if (!COMMAND_SET_ACCOUNT.equalsIgnoreCase(str) || System.currentTimeMillis() - accountSetTime(context, str2) >= 3600000) {
                if (!COMMAND_UNSET_ACCOUNT.equalsIgnoreCase(str) || accountSetTime(context, str2) >= 0) {
                    setCommand(context, str, arrayList, str3);
                } else {
                    b.a("Don't cancel account for " + arrayList + " is unseted");
                }
            } else if (1 == PushMessageHelper.getPushMode(context)) {
                PushMessageHandler.a(context, str3, str, 0, null, arrayList);
            } else {
                PushMessageHelper.sendCommandMessageBroadcast(context, PushMessageHelper.generateCommandMessage(COMMAND_SET_ACCOUNT, arrayList, 0, null, null));
            }
        } else if (1 == PushMessageHelper.getPushMode(context)) {
            PushMessageHandler.a(context, str3, str, 0, null, arrayList);
        } else {
            PushMessageHelper.sendCommandMessageBroadcast(context, PushMessageHelper.generateCommandMessage(COMMAND_SET_ALIAS, arrayList, 0, null, null));
        }
    }

    protected static void setCommand(Context context, String str, ArrayList<String> arrayList, String str2) {
        if (!TextUtils.isEmpty(a.a(context).c())) {
            m mVar = new m();
            mVar.a(generatePacketID());
            mVar.b(a.a(context).c());
            mVar.c(str);
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                mVar.d(it.next());
            }
            mVar.f(str2);
            mVar.e(context.getPackageName());
            j.a(context).a(mVar, com.xiaomi.xmpush.thrift.a.Command, (i) null);
        }
    }

    public static void setLocalNotificationType(Context context, int i) {
        j.a(context).b(i & -1);
    }

    public static void setUserAccount(Context context, String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            setCommand(context, COMMAND_SET_ACCOUNT, str, str2);
        }
    }

    private static boolean shouldPullNotification(Context context) {
        return System.currentTimeMillis() - context.getSharedPreferences(PREF_EXTRA, 0).getLong("last_pull_notification", -1) > 300000;
    }

    private static boolean shouldSendRegRequest(Context context) {
        return System.currentTimeMillis() - context.getSharedPreferences(PREF_EXTRA, 0).getLong("last_reg_request", -1) > 5000;
    }

    public static boolean shouldUseMIUIPush(Context context) {
        return j.a(context).b();
    }

    public static void subscribe(Context context, String str, String str2) {
        if (!TextUtils.isEmpty(a.a(context).c()) && !TextUtils.isEmpty(str)) {
            if (System.currentTimeMillis() - topicSubscribedTime(context, str) > LogBuilder.MAX_INTERVAL) {
                x xVar = new x();
                xVar.a(generatePacketID());
                xVar.b(a.a(context).c());
                xVar.c(str);
                xVar.d(context.getPackageName());
                xVar.e(str2);
                j.a(context).a(xVar, com.xiaomi.xmpush.thrift.a.Subscription, (i) null);
            } else if (1 == PushMessageHelper.getPushMode(context)) {
                PushMessageHandler.a(context, str2, 0, null, str);
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.add(str);
                PushMessageHelper.sendCommandMessageBroadcast(context, PushMessageHelper.generateCommandMessage(COMMAND_SUBSCRIBE_TOPIC, arrayList, 0, null, null));
            }
        }
    }

    public static long topicSubscribedTime(Context context, String str) {
        return context.getSharedPreferences(PREF_EXTRA, 0).getLong("topic_" + str, -1);
    }

    public static void unregisterPush(Context context) {
        if (a.a(context).b()) {
            z zVar = new z();
            zVar.a(generatePacketID());
            zVar.b(a.a(context).c());
            zVar.c(a.a(context).e());
            zVar.e(a.a(context).d());
            zVar.d(context.getPackageName());
            j.a(context).a(zVar);
            PushMessageHandler.a();
            a.a(context).k();
            clearExtras(context);
            clearLocalNotificationType(context);
            clearNotification(context);
        }
    }

    public static void unsetAlias(Context context, String str, String str2) {
        setCommand(context, COMMAND_UNSET_ALIAS, str, str2);
    }

    public static void unsetUserAccount(Context context, String str, String str2) {
        setCommand(context, COMMAND_UNSET_ACCOUNT, str, str2);
    }

    public static void unsubscribe(Context context, String str, String str2) {
        if (a.a(context).b()) {
            if (topicSubscribedTime(context, str) < 0) {
                b.a("Don't cancel subscribe for " + str + " is unsubscribed");
                return;
            }
            ab abVar = new ab();
            abVar.a(generatePacketID());
            abVar.b(a.a(context).c());
            abVar.c(str);
            abVar.d(context.getPackageName());
            abVar.e(str2);
            j.a(context).a(abVar, com.xiaomi.xmpush.thrift.a.UnSubscription, (i) null);
        }
    }

    private static void updateIMEI() {
        new Thread(new d()).start();
    }
}
