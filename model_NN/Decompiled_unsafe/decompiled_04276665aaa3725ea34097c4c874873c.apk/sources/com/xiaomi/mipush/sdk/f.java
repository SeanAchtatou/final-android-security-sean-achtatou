package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.text.TextUtils;
import com.xiaomi.channel.commonutils.logger.b;

final class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String[] f2923a;
    final /* synthetic */ Context b;

    f(String[] strArr, Context context) {
        this.f2923a = strArr;
        this.b = context;
    }

    public void run() {
        PackageInfo packageInfo;
        try {
            for (String str : this.f2923a) {
                if (!TextUtils.isEmpty(str) && (packageInfo = this.b.getPackageManager().getPackageInfo(str, 4)) != null) {
                    MiPushClient.awakePushServiceByPackageInfo(this.b, packageInfo);
                }
            }
        } catch (Throwable th) {
            b.a(th);
        }
    }
}
