package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.igexin.assist.sdk.AssistPushConsts;
import com.xiaomi.channel.commonutils.string.d;
import com.xiaomi.mipush.sdk.PushMessageHandler;
import com.xiaomi.push.service.r;
import com.xiaomi.push.service.v;
import com.xiaomi.push.service.z;
import com.xiaomi.xmpush.thrift.a;
import com.xiaomi.xmpush.thrift.aa;
import com.xiaomi.xmpush.thrift.ac;
import com.xiaomi.xmpush.thrift.ad;
import com.xiaomi.xmpush.thrift.f;
import com.xiaomi.xmpush.thrift.h;
import com.xiaomi.xmpush.thrift.k;
import com.xiaomi.xmpush.thrift.n;
import com.xiaomi.xmpush.thrift.o;
import com.xiaomi.xmpush.thrift.p;
import com.xiaomi.xmpush.thrift.q;
import com.xiaomi.xmpush.thrift.t;
import com.xiaomi.xmpush.thrift.w;
import com.xiaomi.xmpush.thrift.y;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TimeZone;
import org.apache.thrift.b;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private static i f2927a = null;
    private static Queue<String> c;
    private static Object d = new Object();
    private Context b;

    private i(Context context) {
        this.b = context.getApplicationContext();
        if (this.b == null) {
            this.b = context;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:71:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.Intent a(android.content.Context r6, java.lang.String r7, java.util.Map<java.lang.String, java.lang.String> r8) {
        /*
            r1 = 0
            if (r8 == 0) goto L_0x000b
            java.lang.String r0 = "notify_effect"
            boolean r0 = r8.containsKey(r0)
            if (r0 != 0) goto L_0x000c
        L_0x000b:
            return r1
        L_0x000c:
            java.lang.String r0 = "notify_effect"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r2 = com.xiaomi.push.service.z.f2947a
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0056
            android.content.pm.PackageManager r0 = r6.getPackageManager()     // Catch:{ Exception -> 0x0039 }
            android.content.Intent r0 = r0.getLaunchIntentForPackage(r7)     // Catch:{ Exception -> 0x0039 }
        L_0x0024:
            if (r0 == 0) goto L_0x000b
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r0.addFlags(r2)
            android.content.pm.PackageManager r2 = r6.getPackageManager()     // Catch:{ Exception -> 0x016c }
            r3 = 65536(0x10000, float:9.18355E-41)
            android.content.pm.ResolveInfo r2 = r2.resolveActivity(r0, r3)     // Catch:{ Exception -> 0x016c }
            if (r2 == 0) goto L_0x000b
            r1 = r0
            goto L_0x000b
        L_0x0039:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Cause: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.xiaomi.channel.commonutils.logger.b.d(r0)
            r0 = r1
            goto L_0x0024
        L_0x0056:
            java.lang.String r2 = com.xiaomi.push.service.z.b
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x00e8
            java.lang.String r0 = "intent_uri"
            boolean r0 = r8.containsKey(r0)
            if (r0 == 0) goto L_0x0095
            java.lang.String r0 = "intent_uri"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x0199
            r2 = 1
            android.content.Intent r0 = android.content.Intent.parseUri(r0, r2)     // Catch:{ URISyntaxException -> 0x018c }
            r0.setPackage(r7)     // Catch:{ URISyntaxException -> 0x0079 }
            goto L_0x0024
        L_0x0079:
            r2 = move-exception
        L_0x007a:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Cause: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r2 = r2.getMessage()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r2 = r2.toString()
            com.xiaomi.channel.commonutils.logger.b.d(r2)
            goto L_0x0024
        L_0x0095:
            java.lang.String r0 = "class_name"
            boolean r0 = r8.containsKey(r0)
            if (r0 == 0) goto L_0x0196
            java.lang.String r0 = "class_name"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            android.content.ComponentName r3 = new android.content.ComponentName
            r3.<init>(r7, r0)
            r2.setComponent(r3)
            java.lang.String r0 = "intent_flag"
            boolean r0 = r8.containsKey(r0)     // Catch:{ NumberFormatException -> 0x00cc }
            if (r0 == 0) goto L_0x00c9
            java.lang.String r0 = "intent_flag"
            java.lang.Object r0 = r8.get(r0)     // Catch:{ NumberFormatException -> 0x00cc }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ NumberFormatException -> 0x00cc }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x00cc }
            r2.setFlags(r0)     // Catch:{ NumberFormatException -> 0x00cc }
        L_0x00c9:
            r0 = r2
            goto L_0x0024
        L_0x00cc:
            r0 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Cause by intent_flag: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.xiaomi.channel.commonutils.logger.b.d(r0)
            goto L_0x00c9
        L_0x00e8:
            java.lang.String r2 = com.xiaomi.push.service.z.c
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0196
            java.lang.String r0 = "web_uri"
            java.lang.Object r0 = r8.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x0196
            java.lang.String r0 = r0.trim()
            java.lang.String r2 = "http://"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0194
            java.lang.String r2 = "https://"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0194
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "http://"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r2 = r0
        L_0x0122:
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0189 }
            r0.<init>(r2)     // Catch:{ MalformedURLException -> 0x0189 }
            java.lang.String r0 = r0.getProtocol()     // Catch:{ MalformedURLException -> 0x0189 }
            java.lang.String r3 = "http"
            boolean r3 = r3.equals(r0)     // Catch:{ MalformedURLException -> 0x0189 }
            if (r3 != 0) goto L_0x013b
            java.lang.String r3 = "https"
            boolean r0 = r3.equals(r0)     // Catch:{ MalformedURLException -> 0x0189 }
            if (r0 == 0) goto L_0x0191
        L_0x013b:
            android.content.Intent r0 = new android.content.Intent     // Catch:{ MalformedURLException -> 0x0189 }
            java.lang.String r3 = "android.intent.action.VIEW"
            r0.<init>(r3)     // Catch:{ MalformedURLException -> 0x0189 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ MalformedURLException -> 0x014b }
            r0.setData(r2)     // Catch:{ MalformedURLException -> 0x014b }
            goto L_0x0024
        L_0x014b:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
        L_0x014f:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Cause: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.xiaomi.channel.commonutils.logger.b.d(r0)
            r0 = r2
            goto L_0x0024
        L_0x016c:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Cause: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.xiaomi.channel.commonutils.logger.b.d(r0)
            goto L_0x000b
        L_0x0189:
            r0 = move-exception
            r2 = r1
            goto L_0x014f
        L_0x018c:
            r0 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x007a
        L_0x0191:
            r0 = r1
            goto L_0x0024
        L_0x0194:
            r2 = r0
            goto L_0x0122
        L_0x0196:
            r0 = r1
            goto L_0x0024
        L_0x0199:
            r0 = r1
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.mipush.sdk.i.a(android.content.Context, java.lang.String, java.util.Map):android.content.Intent");
    }

    private PushMessageHandler.a a(o oVar, boolean z, byte[] bArr) {
        List<String> list;
        ArrayList arrayList;
        ArrayList arrayList2;
        ArrayList arrayList3;
        MiPushMessage miPushMessage = null;
        try {
            b a2 = h.a(this.b, oVar);
            if (a2 == null) {
                com.xiaomi.channel.commonutils.logger.b.d("receiving an un-recognized message. " + oVar.f2977a);
                return null;
            }
            com.xiaomi.channel.commonutils.logger.b.c("receive a message." + a2);
            a a3 = oVar.a();
            com.xiaomi.channel.commonutils.logger.b.a("processing a message, action=" + a3);
            switch (a3) {
                case SendMessage:
                    if (!a.a(this.b).l() || z) {
                        w wVar = (w) a2;
                        h l = wVar.l();
                        if (l == null) {
                            com.xiaomi.channel.commonutils.logger.b.d("receive an empty message without push content, drop it");
                            return null;
                        }
                        if (z) {
                            if (r.b(oVar)) {
                                MiPushClient.reportIgnoreRegMessageClicked(this.b, l.b(), oVar.m(), oVar.f, l.d());
                            } else {
                                MiPushClient.reportMessageClicked(this.b, l.b(), oVar.m(), l.d());
                            }
                        }
                        if (!z) {
                            if (!TextUtils.isEmpty(wVar.j()) && MiPushClient.aliasSetTime(this.b, wVar.j()) < 0) {
                                MiPushClient.addAlias(this.b, wVar.j());
                            } else if (!TextUtils.isEmpty(wVar.h()) && MiPushClient.topicSubscribedTime(this.b, wVar.h()) < 0) {
                                MiPushClient.addTopic(this.b, wVar.h());
                            }
                        }
                        String str = (oVar.h == null || oVar.h.s() == null) ? null : oVar.h.j.get("jobkey");
                        String b2 = TextUtils.isEmpty(str) ? l.b() : str;
                        if (z || !a(this.b, b2)) {
                            MiPushMessage generateMessage = PushMessageHelper.generateMessage(wVar, oVar.m(), z);
                            if (generateMessage.getPassThrough() != 0 || z || !r.a(generateMessage.getExtra())) {
                                com.xiaomi.channel.commonutils.logger.b.a("receive a message, msgid=" + l.b() + ", jobkey=" + b2);
                                if (!z || generateMessage.getExtra() == null || !generateMessage.getExtra().containsKey("notify_effect")) {
                                    miPushMessage = generateMessage;
                                } else {
                                    Map<String, String> extra = generateMessage.getExtra();
                                    String str2 = extra.get("notify_effect");
                                    if (r.b(oVar)) {
                                        Intent a4 = a(this.b, oVar.f, extra);
                                        if (a4 == null) {
                                            com.xiaomi.channel.commonutils.logger.b.a("Getting Intent fail from ignore reg message. ");
                                            return null;
                                        }
                                        String f = l.f();
                                        if (!TextUtils.isEmpty(f)) {
                                            a4.putExtra(AssistPushConsts.MSG_TYPE_PAYLOAD, f);
                                        }
                                        this.b.startActivity(a4);
                                        return null;
                                    }
                                    Intent a5 = a(this.b, this.b.getPackageName(), extra);
                                    if (a5 == null) {
                                        return null;
                                    }
                                    if (!str2.equals(z.c)) {
                                        a5.putExtra(PushMessageHelper.KEY_MESSAGE, generateMessage);
                                    }
                                    this.b.startActivity(a5);
                                    return null;
                                }
                            } else {
                                r.a(this.b, oVar, bArr);
                                return null;
                            }
                        } else {
                            com.xiaomi.channel.commonutils.logger.b.a("drop a duplicate message, key=" + b2);
                        }
                        if (oVar.m() != null || z) {
                            return miPushMessage;
                        }
                        a(wVar, oVar);
                        return miPushMessage;
                    }
                    com.xiaomi.channel.commonutils.logger.b.a("receive a message in pause state. drop it");
                    return null;
                case Registration:
                    t tVar = (t) a2;
                    if (tVar.f == 0) {
                        a.a(this.b).b(tVar.h, tVar.i);
                    }
                    if (!TextUtils.isEmpty(tVar.h)) {
                        arrayList3 = new ArrayList();
                        arrayList3.add(tVar.h);
                    } else {
                        arrayList3 = null;
                    }
                    MiPushCommandMessage generateCommandMessage = PushMessageHelper.generateCommandMessage(MiPushClient.COMMAND_REGISTER, arrayList3, tVar.f, tVar.g, null);
                    j.a(this.b).d();
                    return generateCommandMessage;
                case UnRegistration:
                    if (((aa) a2).f == 0) {
                        a.a(this.b).h();
                        MiPushClient.clearExtras(this.b);
                    }
                    PushMessageHandler.a();
                    return null;
                case Subscription:
                    y yVar = (y) a2;
                    if (yVar.f == 0) {
                        MiPushClient.addTopic(this.b, yVar.h());
                    }
                    if (!TextUtils.isEmpty(yVar.h())) {
                        arrayList2 = new ArrayList();
                        arrayList2.add(yVar.h());
                    } else {
                        arrayList2 = null;
                    }
                    return PushMessageHelper.generateCommandMessage(MiPushClient.COMMAND_SUBSCRIBE_TOPIC, arrayList2, yVar.f, yVar.g, yVar.k());
                case UnSubscription:
                    ac acVar = (ac) a2;
                    if (acVar.f == 0) {
                        MiPushClient.removeTopic(this.b, acVar.h());
                    }
                    if (!TextUtils.isEmpty(acVar.h())) {
                        arrayList = new ArrayList();
                        arrayList.add(acVar.h());
                    } else {
                        arrayList = null;
                    }
                    return PushMessageHelper.generateCommandMessage(MiPushClient.COMMAND_UNSUBSCRIBE_TOPIC, arrayList, acVar.f, acVar.g, acVar.k());
                case Command:
                    n nVar = (n) a2;
                    String e = nVar.e();
                    List<String> k = nVar.k();
                    if (nVar.g == 0) {
                        if (TextUtils.equals(e, MiPushClient.COMMAND_SET_ACCEPT_TIME) && k != null && k.size() > 1) {
                            MiPushClient.addAcceptTime(this.b, k.get(0), k.get(1));
                            if (!"00:00".equals(k.get(0)) || !"00:00".equals(k.get(1))) {
                                a.a(this.b).a(false);
                            } else {
                                a.a(this.b).a(true);
                            }
                            list = a(TimeZone.getTimeZone("GMT+08"), TimeZone.getDefault(), k);
                            return PushMessageHelper.generateCommandMessage(e, list, nVar.g, nVar.h, nVar.m());
                        } else if (TextUtils.equals(e, MiPushClient.COMMAND_SET_ALIAS) && k != null && k.size() > 0) {
                            MiPushClient.addAlias(this.b, k.get(0));
                            list = k;
                            return PushMessageHelper.generateCommandMessage(e, list, nVar.g, nVar.h, nVar.m());
                        } else if (TextUtils.equals(e, MiPushClient.COMMAND_UNSET_ALIAS) && k != null && k.size() > 0) {
                            MiPushClient.removeAlias(this.b, k.get(0));
                            list = k;
                            return PushMessageHelper.generateCommandMessage(e, list, nVar.g, nVar.h, nVar.m());
                        } else if (TextUtils.equals(e, MiPushClient.COMMAND_SET_ACCOUNT) && k != null && k.size() > 0) {
                            MiPushClient.addAccount(this.b, k.get(0));
                            list = k;
                            return PushMessageHelper.generateCommandMessage(e, list, nVar.g, nVar.h, nVar.m());
                        } else if (TextUtils.equals(e, MiPushClient.COMMAND_UNSET_ACCOUNT) && k != null && k.size() > 0) {
                            MiPushClient.removeAccount(this.b, k.get(0));
                        }
                    }
                    list = k;
                    return PushMessageHelper.generateCommandMessage(e, list, nVar.g, nVar.h, nVar.m());
                case Notification:
                    com.xiaomi.xmpush.thrift.r rVar = (com.xiaomi.xmpush.thrift.r) a2;
                    if ("registration id expired".equalsIgnoreCase(rVar.e)) {
                        MiPushClient.reInitialize(this.b);
                        return null;
                    } else if ("client_info_update_ok".equalsIgnoreCase(rVar.e)) {
                        if (rVar.h() == null || !rVar.h().containsKey("app_version")) {
                            return null;
                        }
                        a.a(this.b).a(rVar.h().get("app_version"));
                        return null;
                    } else if ("awake_app".equalsIgnoreCase(rVar.e)) {
                        if (rVar.h() == null || !rVar.h().containsKey("packages")) {
                            return null;
                        }
                        MiPushClient.awakeApps(this.b, rVar.h().get("packages").split(MiPushClient.ACCEPT_TIME_SEPARATOR));
                        return null;
                    } else if (f.NormalClientConfigUpdate.p.equalsIgnoreCase(rVar.e)) {
                        q qVar = new q();
                        try {
                            ad.a(qVar, rVar.l());
                            com.xiaomi.push.service.w.a(v.a(this.b), qVar);
                            return null;
                        } catch (org.apache.thrift.f e2) {
                            com.xiaomi.channel.commonutils.logger.b.a(e2);
                            return null;
                        }
                    } else if (!f.CustomClientConfigUpdate.p.equalsIgnoreCase(rVar.e)) {
                        return null;
                    } else {
                        p pVar = new p();
                        try {
                            ad.a(pVar, rVar.l());
                            com.xiaomi.push.service.w.a(v.a(this.b), pVar);
                            return null;
                        } catch (org.apache.thrift.f e3) {
                            com.xiaomi.channel.commonutils.logger.b.a(e3);
                            return null;
                        }
                    }
                default:
                    return null;
            }
        } catch (org.apache.thrift.f e4) {
            com.xiaomi.channel.commonutils.logger.b.a(e4);
            com.xiaomi.channel.commonutils.logger.b.d("receive a message which action string is not valid. is the reg expired?");
            return null;
        }
    }

    private PushMessageHandler.a a(o oVar, byte[] bArr) {
        String str = null;
        try {
            b a2 = h.a(this.b, oVar);
            if (a2 == null) {
                com.xiaomi.channel.commonutils.logger.b.d("message arrived: receiving an un-recognized message. " + oVar.f2977a);
                return null;
            }
            com.xiaomi.channel.commonutils.logger.b.c("message arrived: receive a message." + a2);
            a a3 = oVar.a();
            com.xiaomi.channel.commonutils.logger.b.a("message arrived: processing an arrived message, action=" + a3);
            switch (a3) {
                case SendMessage:
                    w wVar = (w) a2;
                    h l = wVar.l();
                    if (l == null) {
                        com.xiaomi.channel.commonutils.logger.b.d("message arrived: receive an empty message without push content, drop it");
                        return null;
                    }
                    if (!(oVar.h == null || oVar.h.s() == null)) {
                        str = oVar.h.j.get("jobkey");
                    }
                    MiPushMessage generateMessage = PushMessageHelper.generateMessage(wVar, oVar.m(), false);
                    generateMessage.setArrivedMessage(true);
                    com.xiaomi.channel.commonutils.logger.b.a("message arrived: receive a message, msgid=" + l.b() + ", jobkey=" + str);
                    return generateMessage;
                default:
                    return null;
            }
        } catch (org.apache.thrift.f e) {
            com.xiaomi.channel.commonutils.logger.b.a(e);
            com.xiaomi.channel.commonutils.logger.b.d("message arrived: receive a message which action string is not valid. is the reg expired?");
            return null;
        }
    }

    public static i a(Context context) {
        if (f2927a == null) {
            f2927a = new i(context);
        }
        return f2927a;
    }

    private void a(o oVar) {
        com.xiaomi.xmpush.thrift.i m = oVar.m();
        k kVar = new k();
        kVar.b(oVar.h());
        kVar.a(m.b());
        kVar.a(m.d());
        if (!TextUtils.isEmpty(m.f())) {
            kVar.c(m.f());
        }
        kVar.a(ad.a(this.b, oVar.f));
        j.a(this.b).a(kVar, a.AckMessage, false, oVar.m());
    }

    private void a(w wVar, o oVar) {
        com.xiaomi.xmpush.thrift.i m = oVar.m();
        k kVar = new k();
        kVar.b(wVar.e());
        kVar.a(wVar.c());
        kVar.a(wVar.l().h());
        if (!TextUtils.isEmpty(wVar.h())) {
            kVar.c(wVar.h());
        }
        if (!TextUtils.isEmpty(wVar.j())) {
            kVar.d(wVar.j());
        }
        kVar.a(ad.a(this.b, oVar.f));
        j.a(this.b).a(kVar, a.AckMessage, m);
    }

    private static boolean a(Context context, String str) {
        boolean z = false;
        synchronized (d) {
            SharedPreferences j = a.a(context).j();
            if (c == null) {
                String[] split = j.getString("pref_msg_ids", "").split(MiPushClient.ACCEPT_TIME_SEPARATOR);
                c = new LinkedList();
                for (String add : split) {
                    c.add(add);
                }
            }
            if (c.contains(str)) {
                z = true;
            } else {
                c.add(str);
                if (c.size() > 25) {
                    c.poll();
                }
                String a2 = d.a(c, MiPushClient.ACCEPT_TIME_SEPARATOR);
                SharedPreferences.Editor edit = j.edit();
                edit.putString("pref_msg_ids", a2);
                edit.commit();
            }
        }
        return z;
    }

    public PushMessageHandler.a a(Intent intent) {
        String action = intent.getAction();
        com.xiaomi.channel.commonutils.logger.b.a("receive an intent from server, action=" + action);
        String stringExtra = intent.getStringExtra("mrt");
        if (stringExtra == null) {
            stringExtra = Long.toString(System.currentTimeMillis());
        }
        if ("com.xiaomi.mipush.RECEIVE_MESSAGE".equals(action)) {
            byte[] byteArrayExtra = intent.getByteArrayExtra("mipush_payload");
            boolean booleanExtra = intent.getBooleanExtra("mipush_notified", false);
            if (byteArrayExtra == null) {
                com.xiaomi.channel.commonutils.logger.b.d("receiving an empty message, drop");
                return null;
            }
            o oVar = new o();
            try {
                ad.a(oVar, byteArrayExtra);
                a a2 = a.a(this.b);
                com.xiaomi.xmpush.thrift.i m = oVar.m();
                if (oVar.a() == a.SendMessage && m != null && !a2.l() && !booleanExtra) {
                    if (m != null) {
                        oVar.m().a("mrt", stringExtra);
                        oVar.m().a("mat", Long.toString(System.currentTimeMillis()));
                    }
                    a(oVar);
                }
                if (oVar.a() == a.SendMessage && !oVar.c()) {
                    if (!r.b(oVar)) {
                        Object[] objArr = new Object[2];
                        objArr[0] = oVar.j();
                        objArr[1] = m != null ? m.b() : "";
                        com.xiaomi.channel.commonutils.logger.b.a(String.format("drop an un-encrypted messages. %1$s, %2$s", objArr));
                        return null;
                    } else if (!booleanExtra || m.s() == null || !m.s().containsKey("notify_effect")) {
                        com.xiaomi.channel.commonutils.logger.b.a(String.format("drop an un-encrypted messages. %1$s, %2$s", oVar.j(), m.b()));
                        return null;
                    }
                }
                if (a2.i() || oVar.f2977a == a.Registration) {
                    if (!a2.i() || !a2.n()) {
                        return a(oVar, booleanExtra, byteArrayExtra);
                    }
                    if (oVar.f2977a == a.UnRegistration) {
                        a2.h();
                        MiPushClient.clearExtras(this.b);
                        PushMessageHandler.a();
                    } else {
                        MiPushClient.unregisterPush(this.b);
                    }
                } else if (r.b(oVar)) {
                    return a(oVar, booleanExtra, byteArrayExtra);
                } else {
                    com.xiaomi.channel.commonutils.logger.b.d("receive message without registration. need unregister or re-register!");
                }
            } catch (org.apache.thrift.f e) {
                com.xiaomi.channel.commonutils.logger.b.a(e);
            } catch (Exception e2) {
                com.xiaomi.channel.commonutils.logger.b.a(e2);
            }
        } else if ("com.xiaomi.mipush.ERROR".equals(action)) {
            MiPushCommandMessage miPushCommandMessage = new MiPushCommandMessage();
            o oVar2 = new o();
            try {
                byte[] byteArrayExtra2 = intent.getByteArrayExtra("mipush_payload");
                if (byteArrayExtra2 != null) {
                    ad.a(oVar2, byteArrayExtra2);
                }
            } catch (org.apache.thrift.f e3) {
            }
            miPushCommandMessage.setCommand(String.valueOf(oVar2.a()));
            miPushCommandMessage.setResultCode((long) intent.getIntExtra("mipush_error_code", 0));
            miPushCommandMessage.setReason(intent.getStringExtra("mipush_error_msg"));
            com.xiaomi.channel.commonutils.logger.b.d("receive a error message. code = " + intent.getIntExtra("mipush_error_code", 0) + ", msg= " + intent.getStringExtra("mipush_error_msg"));
            return miPushCommandMessage;
        } else if ("com.xiaomi.mipush.MESSAGE_ARRIVED".equals(action)) {
            byte[] byteArrayExtra3 = intent.getByteArrayExtra("mipush_payload");
            if (byteArrayExtra3 == null) {
                com.xiaomi.channel.commonutils.logger.b.d("message arrived: receiving an empty message, drop");
                return null;
            }
            o oVar3 = new o();
            try {
                ad.a(oVar3, byteArrayExtra3);
                a a3 = a.a(this.b);
                if (r.b(oVar3)) {
                    com.xiaomi.channel.commonutils.logger.b.d("message arrived: receive ignore reg message, ignore!");
                } else if (!a3.i()) {
                    com.xiaomi.channel.commonutils.logger.b.d("message arrived: receive message without registration. need unregister or re-register!");
                } else if (!a3.i() || !a3.n()) {
                    return a(oVar3, byteArrayExtra3);
                } else {
                    com.xiaomi.channel.commonutils.logger.b.d("message arrived: app info is invalidated");
                }
            } catch (org.apache.thrift.f e4) {
                com.xiaomi.channel.commonutils.logger.b.a(e4);
            } catch (Exception e5) {
                com.xiaomi.channel.commonutils.logger.b.a(e5);
            }
        }
        return null;
    }

    public List<String> a(TimeZone timeZone, TimeZone timeZone2, List<String> list) {
        if (timeZone.equals(timeZone2)) {
            return list;
        }
        long rawOffset = (long) (((timeZone.getRawOffset() - timeZone2.getRawOffset()) / 1000) / 60);
        long parseLong = Long.parseLong(list.get(0).split(":")[0]);
        long parseLong2 = Long.parseLong(list.get(0).split(":")[1]);
        long j = ((((parseLong * 60) + parseLong2) - rawOffset) + 1440) % 1440;
        long parseLong3 = (((Long.parseLong(list.get(1).split(":")[1]) + (60 * Long.parseLong(list.get(1).split(":")[0]))) - rawOffset) + 1440) % 1440;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = arrayList;
        arrayList2.add(String.format("%1$02d:%2$02d", Long.valueOf(j / 60), Long.valueOf(j % 60)));
        ArrayList arrayList3 = arrayList;
        arrayList3.add(String.format("%1$02d:%2$02d", Long.valueOf(parseLong3 / 60), Long.valueOf(parseLong3 % 60)));
        return arrayList;
    }
}
