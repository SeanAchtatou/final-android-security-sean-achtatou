package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.pm.PackageInfo;
import java.util.List;

final class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f2922a;

    e(Context context) {
        this.f2922a = context;
    }

    public void run() {
        try {
            List<PackageInfo> installedPackages = this.f2922a.getPackageManager().getInstalledPackages(4);
            if (installedPackages != null) {
                for (PackageInfo access$100 : installedPackages) {
                    MiPushClient.awakePushServiceByPackageInfo(this.f2922a, access$100);
                }
            }
        } catch (Throwable th) {
        }
    }
}
