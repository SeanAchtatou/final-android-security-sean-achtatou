package com.xiaomi.mipush.sdk;

import android.os.Build;
import android.text.TextUtils;

public class XmSystemUtils {
    private static final String XIAOMI = "Xiaomi".toLowerCase();

    public static boolean isBrandXiaoMi() {
        try {
            return TextUtils.equals(XIAOMI, Build.BRAND.toLowerCase());
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
