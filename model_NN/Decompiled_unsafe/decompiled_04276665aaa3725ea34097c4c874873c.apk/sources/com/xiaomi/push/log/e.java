package com.xiaomi.push.log;

import com.xiaomi.channel.commonutils.logger.LoggerInterface;

public class e implements LoggerInterface {

    /* renamed from: a  reason: collision with root package name */
    private LoggerInterface f2932a = null;
    private LoggerInterface b = null;

    public e(LoggerInterface loggerInterface, LoggerInterface loggerInterface2) {
        this.f2932a = loggerInterface;
        this.b = loggerInterface2;
    }

    public void log(String str) {
        if (this.f2932a != null) {
            this.f2932a.log(str);
        }
        if (this.b != null) {
            this.b.log(str);
        }
    }

    public void log(String str, Throwable th) {
        if (this.f2932a != null) {
            this.f2932a.log(str, th);
        }
        if (this.b != null) {
            this.b.log(str, th);
        }
    }
}
