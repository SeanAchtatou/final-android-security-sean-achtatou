package com.sina.weibo.sdk.call;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.sina.weibo.sdk.constant.WBPageConstants;
import java.util.HashMap;

class CommonUtils {
    CommonUtils() {
    }

    public static String buildUriQuery(HashMap<String, String> hashMap) {
        StringBuilder sb = new StringBuilder();
        for (String next : hashMap.keySet()) {
            String str = hashMap.get(next);
            if (str != null) {
                sb.append("&").append(next).append("=").append(str);
            }
        }
        return sb.toString().replaceFirst("&", "?");
    }

    public static void openWeiboActivity(Context context, String str, String str2, String str3) throws WeiboNotInstalledException {
        if (str3 != null) {
            try {
                Intent intent = new Intent();
                intent.setAction(str);
                intent.setData(Uri.parse(str2));
                intent.setPackage(str3);
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                if (str3 != null) {
                    try {
                        Intent intent2 = new Intent();
                        intent2.setAction(str);
                        intent2.setData(Uri.parse(str2));
                        context.startActivity(intent2);
                    } catch (ActivityNotFoundException e2) {
                        throw new WeiboNotInstalledException(WBPageConstants.ExceptionMsg.WEIBO_NOT_INSTALLED);
                    }
                } else {
                    throw new WeiboNotInstalledException(WBPageConstants.ExceptionMsg.WEIBO_NOT_INSTALLED);
                }
            }
        } else {
            Intent intent3 = new Intent();
            intent3.setAction(str);
            intent3.setData(Uri.parse(str2));
            context.startActivity(intent3);
        }
    }

    public static void openWeiboActivity(Context context, String str, String str2) throws WeiboNotInstalledException {
        try {
            Intent intent = new Intent();
            intent.setAction(str);
            intent.setData(Uri.parse(str2));
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            throw new WeiboNotInstalledException(WBPageConstants.ExceptionMsg.WEIBO_NOT_INSTALLED);
        }
    }
}
