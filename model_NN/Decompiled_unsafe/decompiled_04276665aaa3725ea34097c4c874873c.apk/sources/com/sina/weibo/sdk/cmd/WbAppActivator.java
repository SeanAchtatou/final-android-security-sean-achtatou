package com.sina.weibo.sdk.cmd;

import android.content.Context;
import android.content.SharedPreferences;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.net.NetUtils;
import com.sina.weibo.sdk.net.WeiboParameters;
import com.sina.weibo.sdk.utils.LogUtil;
import com.sina.weibo.sdk.utils.Utility;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class WbAppActivator {
    /* access modifiers changed from: private */
    public static final String TAG = WbAppActivator.class.getName();
    private static WbAppActivator mInstance;
    /* access modifiers changed from: private */
    public String mAppkey;
    /* access modifiers changed from: private */
    public Context mContext;
    private AppInstallCmdExecutor mInstallExecutor;
    private AppInvokeCmdExecutor mInvokeExecutor;
    /* access modifiers changed from: private */
    public volatile ReentrantLock mLock = new ReentrantLock(true);

    private WbAppActivator(Context context, String str) {
        this.mContext = context.getApplicationContext();
        this.mInvokeExecutor = new AppInvokeCmdExecutor(this.mContext);
        this.mInstallExecutor = new AppInstallCmdExecutor(this.mContext);
        this.mAppkey = str;
    }

    public static synchronized WbAppActivator getInstance(Context context, String str) {
        WbAppActivator wbAppActivator;
        synchronized (WbAppActivator.class) {
            if (mInstance == null) {
                mInstance = new WbAppActivator(context, str);
            }
            wbAppActivator = mInstance;
        }
        return wbAppActivator;
    }

    public void activateApp() {
        final SharedPreferences weiboSdkSp = FrequencyHelper.getWeiboSdkSp(this.mContext);
        long frequency = FrequencyHelper.getFrequency(this.mContext, weiboSdkSp);
        long currentTimeMillis = System.currentTimeMillis() - FrequencyHelper.getLastTime(this.mContext, weiboSdkSp);
        if (currentTimeMillis < frequency) {
            LogUtil.v(TAG, String.format("it's only %d ms from last time get cmd", Long.valueOf(currentTimeMillis)));
            return;
        }
        new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:18:0x00bd  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x010a  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r7 = this;
                    java.lang.String r0 = com.sina.weibo.sdk.cmd.WbAppActivator.TAG
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder
                    java.lang.String r2 = "mLock.isLocked()--->"
                    r1.<init>(r2)
                    com.sina.weibo.sdk.cmd.WbAppActivator r2 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    java.util.concurrent.locks.ReentrantLock r2 = r2.mLock
                    boolean r2 = r2.isLocked()
                    java.lang.StringBuilder r1 = r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    com.sina.weibo.sdk.utils.LogUtil.v(r0, r1)
                    com.sina.weibo.sdk.cmd.WbAppActivator r0 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    java.util.concurrent.locks.ReentrantLock r0 = r0.mLock
                    boolean r0 = r0.tryLock()
                    if (r0 != 0) goto L_0x002d
                L_0x002c:
                    return
                L_0x002d:
                    r1 = 0
                    com.sina.weibo.sdk.cmd.WbAppActivator r0 = com.sina.weibo.sdk.cmd.WbAppActivator.this     // Catch:{ WeiboException -> 0x00a6 }
                    android.content.Context r0 = r0.mContext     // Catch:{ WeiboException -> 0x00a6 }
                    com.sina.weibo.sdk.cmd.WbAppActivator r2 = com.sina.weibo.sdk.cmd.WbAppActivator.this     // Catch:{ WeiboException -> 0x00a6 }
                    java.lang.String r2 = r2.mAppkey     // Catch:{ WeiboException -> 0x00a6 }
                    java.lang.String r0 = com.sina.weibo.sdk.cmd.WbAppActivator.requestCmdInfo(r0, r2)     // Catch:{ WeiboException -> 0x00a6 }
                    if (r0 == 0) goto L_0x0155
                    java.lang.String r2 = com.sina.weibo.sdk.utils.AesEncrypt.Decrypt(r0)     // Catch:{ WeiboException -> 0x00a6 }
                    com.sina.weibo.sdk.cmd.CmdInfo r0 = new com.sina.weibo.sdk.cmd.CmdInfo     // Catch:{ WeiboException -> 0x00a6 }
                    r0.<init>(r2)     // Catch:{ WeiboException -> 0x00a6 }
                    com.sina.weibo.sdk.cmd.WbAppActivator r1 = com.sina.weibo.sdk.cmd.WbAppActivator.this     // Catch:{ WeiboException -> 0x014f, all -> 0x014a }
                    java.util.List r2 = r0.getInstallCmds()     // Catch:{ WeiboException -> 0x014f, all -> 0x014a }
                    r1.handleInstallCmd(r2)     // Catch:{ WeiboException -> 0x014f, all -> 0x014a }
                    com.sina.weibo.sdk.cmd.WbAppActivator r1 = com.sina.weibo.sdk.cmd.WbAppActivator.this     // Catch:{ WeiboException -> 0x014f, all -> 0x014a }
                    java.util.List r2 = r0.getInvokeCmds()     // Catch:{ WeiboException -> 0x014f, all -> 0x014a }
                    r1.handleInvokeCmd(r2)     // Catch:{ WeiboException -> 0x014f, all -> 0x014a }
                L_0x005b:
                    com.sina.weibo.sdk.cmd.WbAppActivator r1 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    java.util.concurrent.locks.ReentrantLock r1 = r1.mLock
                    r1.unlock()
                    if (r0 == 0) goto L_0x0085
                    com.sina.weibo.sdk.cmd.WbAppActivator r1 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    android.content.Context r1 = r1.mContext
                    android.content.SharedPreferences r2 = r0
                    int r0 = r0.getFrequency()
                    long r4 = (long) r0
                    com.sina.weibo.sdk.cmd.WbAppActivator.FrequencyHelper.saveFrequency(r1, r2, r4)
                    com.sina.weibo.sdk.cmd.WbAppActivator r0 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    android.content.Context r0 = r0.mContext
                    android.content.SharedPreferences r1 = r0
                    long r2 = java.lang.System.currentTimeMillis()
                    com.sina.weibo.sdk.cmd.WbAppActivator.FrequencyHelper.saveLastTime(r0, r1, r2)
                L_0x0085:
                    java.lang.String r0 = com.sina.weibo.sdk.cmd.WbAppActivator.TAG
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder
                    java.lang.String r2 = "after unlock \n mLock.isLocked()--->"
                    r1.<init>(r2)
                    com.sina.weibo.sdk.cmd.WbAppActivator r2 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    java.util.concurrent.locks.ReentrantLock r2 = r2.mLock
                    boolean r2 = r2.isLocked()
                    java.lang.StringBuilder r1 = r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    com.sina.weibo.sdk.utils.LogUtil.v(r0, r1)
                    goto L_0x002c
                L_0x00a6:
                    r0 = move-exception
                L_0x00a7:
                    java.lang.String r2 = com.sina.weibo.sdk.cmd.WbAppActivator.TAG     // Catch:{ all -> 0x00fe }
                    java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00fe }
                    com.sina.weibo.sdk.utils.LogUtil.e(r2, r0)     // Catch:{ all -> 0x00fe }
                    com.sina.weibo.sdk.cmd.WbAppActivator r0 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    java.util.concurrent.locks.ReentrantLock r0 = r0.mLock
                    r0.unlock()
                    if (r1 == 0) goto L_0x00dc
                    com.sina.weibo.sdk.cmd.WbAppActivator r0 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    android.content.Context r0 = r0.mContext
                    android.content.SharedPreferences r2 = r0
                    int r1 = r1.getFrequency()
                    long r4 = (long) r1
                    com.sina.weibo.sdk.cmd.WbAppActivator.FrequencyHelper.saveFrequency(r0, r2, r4)
                    com.sina.weibo.sdk.cmd.WbAppActivator r0 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    android.content.Context r0 = r0.mContext
                    android.content.SharedPreferences r1 = r0
                    long r2 = java.lang.System.currentTimeMillis()
                    com.sina.weibo.sdk.cmd.WbAppActivator.FrequencyHelper.saveLastTime(r0, r1, r2)
                L_0x00dc:
                    java.lang.String r0 = com.sina.weibo.sdk.cmd.WbAppActivator.TAG
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder
                    java.lang.String r2 = "after unlock \n mLock.isLocked()--->"
                    r1.<init>(r2)
                    com.sina.weibo.sdk.cmd.WbAppActivator r2 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    java.util.concurrent.locks.ReentrantLock r2 = r2.mLock
                    boolean r2 = r2.isLocked()
                    java.lang.StringBuilder r1 = r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    com.sina.weibo.sdk.utils.LogUtil.v(r0, r1)
                    goto L_0x002c
                L_0x00fe:
                    r0 = move-exception
                L_0x00ff:
                    com.sina.weibo.sdk.cmd.WbAppActivator r2 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    java.util.concurrent.locks.ReentrantLock r2 = r2.mLock
                    r2.unlock()
                    if (r1 == 0) goto L_0x0129
                    com.sina.weibo.sdk.cmd.WbAppActivator r2 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    android.content.Context r2 = r2.mContext
                    android.content.SharedPreferences r3 = r0
                    int r1 = r1.getFrequency()
                    long r4 = (long) r1
                    com.sina.weibo.sdk.cmd.WbAppActivator.FrequencyHelper.saveFrequency(r2, r3, r4)
                    com.sina.weibo.sdk.cmd.WbAppActivator r1 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    android.content.Context r1 = r1.mContext
                    android.content.SharedPreferences r2 = r0
                    long r4 = java.lang.System.currentTimeMillis()
                    com.sina.weibo.sdk.cmd.WbAppActivator.FrequencyHelper.saveLastTime(r1, r2, r4)
                L_0x0129:
                    java.lang.String r1 = com.sina.weibo.sdk.cmd.WbAppActivator.TAG
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    java.lang.String r3 = "after unlock \n mLock.isLocked()--->"
                    r2.<init>(r3)
                    com.sina.weibo.sdk.cmd.WbAppActivator r3 = com.sina.weibo.sdk.cmd.WbAppActivator.this
                    java.util.concurrent.locks.ReentrantLock r3 = r3.mLock
                    boolean r3 = r3.isLocked()
                    java.lang.StringBuilder r2 = r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    com.sina.weibo.sdk.utils.LogUtil.v(r1, r2)
                    throw r0
                L_0x014a:
                    r1 = move-exception
                    r6 = r1
                    r1 = r0
                    r0 = r6
                    goto L_0x00ff
                L_0x014f:
                    r1 = move-exception
                    r6 = r1
                    r1 = r0
                    r0 = r6
                    goto L_0x00a7
                L_0x0155:
                    r0 = r1
                    goto L_0x005b
                */
                throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.cmd.WbAppActivator.AnonymousClass1.run():void");
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public static String requestCmdInfo(Context context, String str) {
        String packageName = context.getPackageName();
        String sign = Utility.getSign(context, packageName);
        WeiboParameters weiboParameters = new WeiboParameters(str);
        weiboParameters.put(LogBuilder.KEY_APPKEY, str);
        weiboParameters.put("packagename", packageName);
        weiboParameters.put("key_hash", sign);
        weiboParameters.put("version", WBConstants.WEIBO_SDK_VERSION_CODE);
        return NetUtils.internalHttpRequest(context, "http://api.weibo.cn/2/client/common_config", "GET", weiboParameters);
    }

    /* access modifiers changed from: private */
    public void handleInstallCmd(List<AppInstallCmd> list) {
        if (list != null) {
            this.mInstallExecutor.start();
            for (AppInstallCmd doExecutor : list) {
                this.mInstallExecutor.doExecutor(doExecutor);
            }
            this.mInstallExecutor.stop();
        }
    }

    /* access modifiers changed from: private */
    public void handleInvokeCmd(List<AppInvokeCmd> list) {
        if (list != null) {
            for (AppInvokeCmd doExecutor : list) {
                this.mInvokeExecutor.doExecutor(doExecutor);
            }
        }
    }

    private static class FrequencyHelper {
        private static final int DEFAULT_FREQUENCY = 3600000;
        private static final String KEY_FREQUENCY = "frequency_get_cmd";
        private static final String KEY_LAST_TIME_GET_CMD = "last_time_get_cmd";
        private static final String WEIBO_SDK_PREFERENCES_NAME = "com_sina_weibo_sdk";

        private FrequencyHelper() {
        }

        public static SharedPreferences getWeiboSdkSp(Context context) {
            return context.getSharedPreferences(WEIBO_SDK_PREFERENCES_NAME, 0);
        }

        public static long getFrequency(Context context, SharedPreferences sharedPreferences) {
            if (sharedPreferences != null) {
                return sharedPreferences.getLong(KEY_FREQUENCY, 3600000);
            }
            return 3600000;
        }

        public static void saveFrequency(Context context, SharedPreferences sharedPreferences, long j) {
            if (sharedPreferences != null && j > 0) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putLong(KEY_FREQUENCY, j);
                edit.commit();
            }
        }

        public static long getLastTime(Context context, SharedPreferences sharedPreferences) {
            if (sharedPreferences != null) {
                return sharedPreferences.getLong(KEY_LAST_TIME_GET_CMD, 0);
            }
            return 0;
        }

        public static void saveLastTime(Context context, SharedPreferences sharedPreferences, long j) {
            if (sharedPreferences != null) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putLong(KEY_LAST_TIME_GET_CMD, j);
                edit.commit();
            }
        }
    }
}
