package com.sina.weibo.sdk.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.meizu.cloud.pushsdk.constants.MeizuConstants;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.net.NetUtils;
import com.sina.weibo.sdk.net.WeiboParameters;
import com.tencent.connect.common.Constants;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.concurrent.locks.ReentrantLock;
import org.json.JSONException;
import org.json.JSONObject;

public class AidTask {
    private static final String AID_FILE_NAME = "weibo_sdk_aid";
    private static final int MAX_RETRY_NUM = 3;
    private static final String TAG = "AidTask";
    private static final int VERSION = 1;
    public static final int WHAT_LOAD_AID_ERR = 1002;
    public static final int WHAT_LOAD_AID_SUC = 1001;
    private static AidTask sInstance;
    /* access modifiers changed from: private */
    public AidInfo mAidInfo;
    private String mAppKey;
    private Context mContext;
    /* access modifiers changed from: private */
    public CallbackHandler mHandler;
    /* access modifiers changed from: private */
    public volatile ReentrantLock mTaskLock = new ReentrantLock(true);

    public interface AidResultCallBack {
        void onAidGenFailed(Exception exc);

        void onAidGenSuccessed(AidInfo aidInfo);
    }

    private static class CallbackHandler extends Handler {
        private WeakReference<AidResultCallBack> callBackReference;

        public CallbackHandler(Looper looper) {
            super(looper);
        }

        public void setCallback(AidResultCallBack aidResultCallBack) {
            if (this.callBackReference == null) {
                this.callBackReference = new WeakReference<>(aidResultCallBack);
            } else if (this.callBackReference.get() != aidResultCallBack) {
                this.callBackReference = new WeakReference<>(aidResultCallBack);
            }
        }

        public void handleMessage(Message message) {
            AidResultCallBack aidResultCallBack = this.callBackReference.get();
            switch (message.what) {
                case 1001:
                    if (aidResultCallBack != null) {
                        aidResultCallBack.onAidGenSuccessed(((AidInfo) message.obj).cloneAidInfo());
                        return;
                    }
                    return;
                case 1002:
                    if (aidResultCallBack != null) {
                        aidResultCallBack.onAidGenFailed((WeiboException) message.obj);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public static final class AidInfo {
        private String mAid;
        private String mSubCookie;

        public String getAid() {
            return this.mAid;
        }

        public String getSubCookie() {
            return this.mSubCookie;
        }

        public static AidInfo parseJson(String str) throws WeiboException {
            AidInfo aidInfo = new AidInfo();
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.has(SocketMessage.MSG_ERROR_KEY) || jSONObject.has("error_code")) {
                    LogUtil.d(AidTask.TAG, "loadAidFromNet has error !!!");
                    throw new WeiboException("loadAidFromNet has error !!!");
                }
                aidInfo.mAid = jSONObject.optString("aid", "");
                aidInfo.mSubCookie = jSONObject.optString("sub", "");
                return aidInfo;
            } catch (JSONException e) {
                LogUtil.d(AidTask.TAG, "loadAidFromNet JSONException Msg : " + e.getMessage());
                throw new WeiboException("loadAidFromNet has error !!!");
            }
        }

        /* access modifiers changed from: package-private */
        public AidInfo cloneAidInfo() {
            AidInfo aidInfo = new AidInfo();
            aidInfo.mAid = this.mAid;
            aidInfo.mSubCookie = this.mSubCookie;
            return aidInfo;
        }
    }

    private AidTask(Context context) {
        this.mContext = context.getApplicationContext();
        this.mHandler = new CallbackHandler(this.mContext.getMainLooper());
        new Thread(new Runnable() {
            public void run() {
                for (int i = 0; i < 1; i++) {
                    try {
                        AidTask.this.getAidInfoFile(i).delete();
                    } catch (Exception e) {
                    }
                }
            }
        }).start();
    }

    public static synchronized AidTask getInstance(Context context) {
        AidTask aidTask;
        synchronized (AidTask.class) {
            if (sInstance == null) {
                sInstance = new AidTask(context);
            }
            aidTask = sInstance;
        }
        return aidTask;
    }

    public void aidTaskInit(String str) {
        if (!TextUtils.isEmpty(str)) {
            LogUtil.e(TAG, "aidTaskInit ");
            initAidInfo(str);
        }
    }

    private void initAidInfo(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.mAppKey = str;
            new Thread(new Runnable() {
                public void run() {
                    if (!AidTask.this.mTaskLock.tryLock()) {
                        LogUtil.e(AidTask.TAG, "tryLock : false, return");
                        return;
                    }
                    AidInfo access$2 = AidTask.this.loadAidInfoFromCache();
                    if (access$2 == null) {
                        int i = 1;
                        do {
                            i++;
                            try {
                                String access$3 = AidTask.this.loadAidFromNet();
                                AidInfo parseJson = AidInfo.parseJson(access$3);
                                AidTask.this.cacheAidInfo(access$3);
                                AidTask.this.mAidInfo = parseJson;
                                break;
                            } catch (WeiboException e) {
                                LogUtil.e(AidTask.TAG, "AidTaskInit WeiboException Msg : " + e.getMessage());
                                if (i >= 3) {
                                }
                            }
                        } while (i >= 3);
                    } else {
                        AidTask.this.mAidInfo = access$2;
                    }
                    AidTask.this.mTaskLock.unlock();
                }
            }).start();
        }
    }

    public AidInfo getAidSync(String str) throws WeiboException {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        LogUtil.e(TAG, "getAidSync ");
        if (this.mAidInfo == null) {
            aidTaskInit(str);
        }
        return this.mAidInfo;
    }

    public void getAidAsync(String str, AidResultCallBack aidResultCallBack) {
        if (!TextUtils.isEmpty(str)) {
            if (this.mAidInfo == null || aidResultCallBack == null) {
                generateAid(str, aidResultCallBack);
            } else {
                aidResultCallBack.onAidGenSuccessed(this.mAidInfo.cloneAidInfo());
            }
        }
    }

    private void generateAid(String str, final AidResultCallBack aidResultCallBack) {
        if (!TextUtils.isEmpty(str)) {
            this.mAppKey = str;
            new Thread(new Runnable() {
                public void run() {
                    AidTask.this.mTaskLock.lock();
                    AidInfo access$2 = AidTask.this.loadAidInfoFromCache();
                    WeiboException e = null;
                    if (access$2 == null) {
                        try {
                            String access$3 = AidTask.this.loadAidFromNet();
                            access$2 = AidInfo.parseJson(access$3);
                            AidTask.this.cacheAidInfo(access$3);
                            AidTask.this.mAidInfo = access$2;
                        } catch (WeiboException e2) {
                            e = e2;
                            LogUtil.e(AidTask.TAG, "AidTaskInit WeiboException Msg : " + e.getMessage());
                        }
                    }
                    AidTask.this.mTaskLock.unlock();
                    Message obtain = Message.obtain();
                    if (access$2 != null) {
                        obtain.what = 1001;
                        obtain.obj = access$2;
                    } else {
                        obtain.what = 1002;
                        obtain.obj = e;
                    }
                    AidTask.this.mHandler.setCallback(aidResultCallBack);
                    AidTask.this.mHandler.sendMessage(obtain);
                }
            }).start();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0035 A[SYNTHETIC, Splitter:B:21:0x0035] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.sina.weibo.sdk.utils.AidTask.AidInfo loadAidInfoFromCache() {
        /*
            r5 = this;
            r0 = 0
            monitor-enter(r5)
            r1 = 1
            java.io.File r2 = r5.getAidInfoFile(r1)     // Catch:{ Exception -> 0x0025, all -> 0x002f }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0025, all -> 0x002f }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0025, all -> 0x002f }
            int r2 = r1.available()     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            r1.read(r2)     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            com.sina.weibo.sdk.utils.AidTask$AidInfo r0 = com.sina.weibo.sdk.utils.AidTask.AidInfo.parseJson(r3)     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x003c }
        L_0x0023:
            monitor-exit(r5)
            return r0
        L_0x0025:
            r1 = move-exception
            r1 = r0
        L_0x0027:
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x002d }
            goto L_0x0023
        L_0x002d:
            r1 = move-exception
            goto L_0x0023
        L_0x002f:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0033:
            if (r1 == 0) goto L_0x0038
            r1.close()     // Catch:{ IOException -> 0x003e }
        L_0x0038:
            throw r0     // Catch:{ all -> 0x0039 }
        L_0x0039:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x003c:
            r1 = move-exception
            goto L_0x0023
        L_0x003e:
            r1 = move-exception
            goto L_0x0038
        L_0x0040:
            r0 = move-exception
            goto L_0x0033
        L_0x0042:
            r2 = move-exception
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.utils.AidTask.loadAidInfoFromCache():com.sina.weibo.sdk.utils.AidTask$AidInfo");
    }

    /* access modifiers changed from: private */
    public File getAidInfoFile(int i) {
        return new File(this.mContext.getFilesDir(), AID_FILE_NAME + i);
    }

    /* access modifiers changed from: private */
    public String loadAidFromNet() throws WeiboException {
        String packageName = this.mContext.getPackageName();
        String sign = Utility.getSign(this.mContext, packageName);
        String mfp = getMfp(this.mContext);
        WeiboParameters weiboParameters = new WeiboParameters(this.mAppKey);
        weiboParameters.put(LogBuilder.KEY_APPKEY, this.mAppKey);
        weiboParameters.put("mfp", mfp);
        weiboParameters.put("packagename", packageName);
        weiboParameters.put("key_hash", sign);
        try {
            String internalHttpRequest = NetUtils.internalHttpRequest(this.mContext, "https://api.weibo.com/oauth2/getaid.json", "GET", weiboParameters);
            LogUtil.d(TAG, "loadAidFromNet response : " + internalHttpRequest);
            return internalHttpRequest;
        } catch (WeiboException e) {
            LogUtil.d(TAG, "loadAidFromNet WeiboException Msg : " + e.getMessage());
            throw e;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0027 A[SYNTHETIC, Splitter:B:18:0x0027] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0030 A[SYNTHETIC, Splitter:B:23:0x0030] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void cacheAidInfo(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x0034 }
            if (r0 == 0) goto L_0x0009
        L_0x0007:
            monitor-exit(r4)
            return
        L_0x0009:
            r1 = 0
            r0 = 1
            java.io.File r2 = r4.getAidInfoFile(r0)     // Catch:{ Exception -> 0x0023, all -> 0x002d }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0023, all -> 0x002d }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0023, all -> 0x002d }
            byte[] r1 = r5.getBytes()     // Catch:{ Exception -> 0x003e, all -> 0x0039 }
            r0.write(r1)     // Catch:{ Exception -> 0x003e, all -> 0x0039 }
            if (r0 == 0) goto L_0x0007
            r0.close()     // Catch:{ IOException -> 0x0021 }
            goto L_0x0007
        L_0x0021:
            r0 = move-exception
            goto L_0x0007
        L_0x0023:
            r0 = move-exception
            r0 = r1
        L_0x0025:
            if (r0 == 0) goto L_0x0007
            r0.close()     // Catch:{ IOException -> 0x002b }
            goto L_0x0007
        L_0x002b:
            r0 = move-exception
            goto L_0x0007
        L_0x002d:
            r0 = move-exception
        L_0x002e:
            if (r1 == 0) goto L_0x0033
            r1.close()     // Catch:{ IOException -> 0x0037 }
        L_0x0033:
            throw r0     // Catch:{ all -> 0x0034 }
        L_0x0034:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0037:
            r1 = move-exception
            goto L_0x0033
        L_0x0039:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x002e
        L_0x003e:
            r1 = move-exception
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.utils.AidTask.cacheAidInfo(java.lang.String):void");
    }

    private static String getMfp(Context context) {
        String str;
        try {
            str = new String(genMfpString(context).getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            str = "";
        }
        LogUtil.d(TAG, "genMfpString() utf-8 string : " + str);
        try {
            String encryptRsa = encryptRsa(str, "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDHHM0Fi2Z6+QYKXqFUX2Cy6AaWq3cPi+GSn9oeAwQbPZR75JB7Netm0HtBVVbtPhzT7UO2p1JhFUKWqrqoYuAjkgMVPmA0sFrQohns5EE44Y86XQopD4ZO+dE5KjUZFE6vrPO3rWW3np2BqlgKpjnYZri6TJApmIpGcQg9/G/3zQIDAQAB");
            LogUtil.d(TAG, "encryptRsa() string : " + encryptRsa);
            return encryptRsa;
        } catch (Exception e2) {
            LogUtil.e(TAG, e2.getMessage());
            return "";
        }
    }

    private static String genMfpString(Context context) {
        String generateUAAid;
        JSONObject jSONObject = new JSONObject();
        try {
            String os = getOS();
            if (!TextUtils.isEmpty(os)) {
                jSONObject.put("1", os);
            }
            String imei = getImei(context);
            if (!TextUtils.isEmpty(imei)) {
                jSONObject.put("2", imei);
            }
            String meid = getMeid(context);
            if (!TextUtils.isEmpty(meid)) {
                jSONObject.put("3", meid);
            }
            String imsi = getImsi(context);
            if (!TextUtils.isEmpty(imsi)) {
                jSONObject.put("4", imsi);
            }
            String mac = getMac(context);
            if (!TextUtils.isEmpty(mac)) {
                jSONObject.put("5", mac);
            }
            String iccid = getIccid(context);
            if (!TextUtils.isEmpty(iccid)) {
                jSONObject.put(Constants.VIA_SHARE_TYPE_INFO, iccid);
            }
            String serialNo = getSerialNo();
            if (!TextUtils.isEmpty(serialNo)) {
                jSONObject.put("7", serialNo);
            }
            String androidId = getAndroidId(context);
            if (!TextUtils.isEmpty(androidId)) {
                jSONObject.put(Constants.VIA_REPORT_TYPE_SHARE_TO_QQ, androidId);
            }
            String cpu = getCpu();
            if (!TextUtils.isEmpty(cpu)) {
                jSONObject.put(Constants.VIA_REPORT_TYPE_JOININ_GROUP, cpu);
            }
            String model = getModel();
            if (!TextUtils.isEmpty(model)) {
                jSONObject.put(Constants.VIA_REPORT_TYPE_MAKE_FRIEND, model);
            }
            String sdSize = getSdSize();
            if (!TextUtils.isEmpty(sdSize)) {
                jSONObject.put(Constants.VIA_REPORT_TYPE_WPA_STATE, sdSize);
            }
            String resolution = getResolution(context);
            if (!TextUtils.isEmpty(resolution)) {
                jSONObject.put(Constants.VIA_REPORT_TYPE_START_WAP, resolution);
            }
            String ssid = getSsid(context);
            if (!TextUtils.isEmpty(ssid)) {
                jSONObject.put(Constants.VIA_REPORT_TYPE_START_GROUP, ssid);
            }
            String deviceName = getDeviceName();
            if (!TextUtils.isEmpty(deviceName)) {
                jSONObject.put("18", deviceName);
            }
            String connectType = getConnectType(context);
            if (!TextUtils.isEmpty(connectType)) {
                jSONObject.put(Constants.VIA_ACT_TYPE_NINETEEN, connectType);
            }
            try {
                generateUAAid = Utility.generateUAAid(context);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!TextUtils.isEmpty(generateUAAid)) {
                jSONObject.put("20", generateUAAid);
            }
            return jSONObject.toString();
        } catch (JSONException e2) {
            return "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x00c2 A[SYNTHETIC, Splitter:B:19:0x00c2] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String encryptRsa(java.lang.String r9, java.lang.String r10) throws java.lang.Exception {
        /*
            java.lang.String r0 = "RSA/ECB/PKCS1Padding"
            javax.crypto.Cipher r3 = javax.crypto.Cipher.getInstance(r0)
            java.security.PublicKey r0 = getPublicKey(r10)
            r1 = 1
            r3.init(r1, r0)
            r2 = 0
            java.lang.String r0 = "UTF-8"
            byte[] r4 = r9.getBytes(r0)
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x00be }
            r1.<init>()     // Catch:{ all -> 0x00be }
            r0 = 0
        L_0x001b:
            r2 = 117(0x75, float:1.64E-43)
            int r2 = splite(r4, r0, r2)     // Catch:{ all -> 0x00ca }
            r5 = -1
            if (r2 != r5) goto L_0x008b
            r1.flush()     // Catch:{ all -> 0x00ca }
            byte[] r0 = r1.toByteArray()     // Catch:{ all -> 0x00ca }
            java.lang.String r2 = "AidTask"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            java.lang.String r4 = "encryptRsa total enBytes len = "
            r3.<init>(r4)     // Catch:{ all -> 0x00ca }
            int r4 = r0.length     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00ca }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00ca }
            com.sina.weibo.sdk.utils.LogUtil.d(r2, r3)     // Catch:{ all -> 0x00ca }
            byte[] r0 = com.sina.weibo.sdk.utils.Base64.encodebyte(r0)     // Catch:{ all -> 0x00ca }
            java.lang.String r2 = "AidTask"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            java.lang.String r4 = "encryptRsa total base64byte len = "
            r3.<init>(r4)     // Catch:{ all -> 0x00ca }
            int r4 = r0.length     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00ca }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00ca }
            com.sina.weibo.sdk.utils.LogUtil.d(r2, r3)     // Catch:{ all -> 0x00ca }
            java.lang.String r2 = "01"
            java.lang.String r2 = new java.lang.String     // Catch:{ all -> 0x00ca }
            java.lang.String r3 = "UTF-8"
            r2.<init>(r0, r3)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            java.lang.String r3 = "01"
            r0.<init>(r3)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x00ca }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ca }
            java.lang.String r2 = "AidTask"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            java.lang.String r4 = "encryptRsa total base64string : "
            r3.<init>(r4)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ all -> 0x00ca }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00ca }
            com.sina.weibo.sdk.utils.LogUtil.d(r2, r3)     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x008a
            r1.close()     // Catch:{ IOException -> 0x00c6 }
        L_0x008a:
            return r0
        L_0x008b:
            byte[] r5 = r3.doFinal(r4, r0, r2)     // Catch:{ all -> 0x00ca }
            r1.write(r5)     // Catch:{ all -> 0x00ca }
            java.lang.String r6 = "AidTask"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            java.lang.String r8 = "encryptRsa offset = "
            r7.<init>(r8)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ all -> 0x00ca }
            java.lang.String r8 = "     len = "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ all -> 0x00ca }
            java.lang.String r8 = "     enBytes len = "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x00ca }
            int r5 = r5.length     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ all -> 0x00ca }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00ca }
            com.sina.weibo.sdk.utils.LogUtil.d(r6, r5)     // Catch:{ all -> 0x00ca }
            int r0 = r0 + r2
            goto L_0x001b
        L_0x00be:
            r0 = move-exception
            r1 = r2
        L_0x00c0:
            if (r1 == 0) goto L_0x00c5
            r1.close()     // Catch:{ IOException -> 0x00c8 }
        L_0x00c5:
            throw r0
        L_0x00c6:
            r1 = move-exception
            goto L_0x008a
        L_0x00c8:
            r1 = move-exception
            goto L_0x00c5
        L_0x00ca:
            r0 = move-exception
            goto L_0x00c0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.utils.AidTask.encryptRsa(java.lang.String, java.lang.String):java.lang.String");
    }

    private static int splite(byte[] bArr, int i, int i2) {
        if (i >= bArr.length) {
            return -1;
        }
        return Math.min(bArr.length - i, i2);
    }

    private static PublicKey getPublicKey(String str) throws Exception {
        return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(str.getBytes())));
    }

    private static String getOS() {
        try {
            return "Android " + Build.VERSION.RELEASE;
        } catch (Exception e) {
            return "";
        }
    }

    private static String getImei(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            return "";
        }
    }

    private static String getMeid(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            return "";
        }
    }

    private static String getImsi(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        } catch (Exception e) {
            return "";
        }
    }

    private static String getMac(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(IXAdSystemUtils.NT_WIFI);
            if (wifiManager == null) {
                return "";
            }
            WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            return connectionInfo != null ? connectionInfo.getMacAddress() : "";
        } catch (Exception e) {
            return "";
        }
    }

    private static String getIccid(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getSimSerialNumber();
        } catch (Exception e) {
            return "";
        }
    }

    private static String getSerialNo() {
        try {
            Class<?> cls = Class.forName(MeizuConstants.CLS_NAME_SYSTEM_PROPERTIES);
            return (String) cls.getMethod("get", String.class, String.class).invoke(cls, "ro.serialno", "unknown");
        } catch (Exception e) {
            return "";
        }
    }

    private static String getAndroidId(Context context) {
        try {
            return Settings.Secure.getString(context.getContentResolver(), "android_id");
        } catch (Exception e) {
            return "";
        }
    }

    private static String getCpu() {
        try {
            return Build.CPU_ABI;
        } catch (Exception e) {
            return "";
        }
    }

    private static String getModel() {
        try {
            return Build.MODEL;
        } catch (Exception e) {
            return "";
        }
    }

    private static String getSdSize() {
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return Long.toString(((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize()));
        } catch (Exception e) {
            return "";
        }
    }

    private static String getResolution(Context context) {
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            return String.valueOf(String.valueOf(displayMetrics.widthPixels)) + "*" + String.valueOf(displayMetrics.heightPixels);
        } catch (Exception e) {
            return "";
        }
    }

    private static String getSsid(Context context) {
        try {
            WifiInfo connectionInfo = ((WifiManager) context.getSystemService(IXAdSystemUtils.NT_WIFI)).getConnectionInfo();
            if (connectionInfo != null) {
                return connectionInfo.getSSID();
            }
        } catch (Exception e) {
        }
        return "";
    }

    private static String getDeviceName() {
        try {
            return Build.BRAND;
        } catch (Exception e) {
            return "";
        }
    }

    private static String getConnectType(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                if (activeNetworkInfo.getType() == 0) {
                    switch (activeNetworkInfo.getSubtype()) {
                        case 1:
                        case 2:
                        case 4:
                        case 7:
                        case 11:
                            return "2G";
                        case 3:
                        case 5:
                        case 6:
                        case 8:
                        case 9:
                        case 10:
                        case 12:
                        case 14:
                        case 15:
                            return "3G";
                        case 13:
                            return "4G";
                        default:
                            return "none";
                    }
                } else if (activeNetworkInfo.getType() == 1) {
                    return IXAdSystemUtils.NT_WIFI;
                }
            }
            return "none";
        } catch (Exception e) {
            return "none";
        }
    }
}
