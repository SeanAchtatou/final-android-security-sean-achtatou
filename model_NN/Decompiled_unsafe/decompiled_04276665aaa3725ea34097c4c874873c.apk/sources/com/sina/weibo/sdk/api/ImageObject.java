package com.sina.weibo.sdk.api;

import android.os.Parcel;
import android.os.Parcelable;
import com.sina.weibo.sdk.utils.LogUtil;
import java.io.File;

public class ImageObject extends BaseMediaObject {
    public static final Parcelable.Creator<ImageObject> CREATOR = new Parcelable.Creator<ImageObject>() {
        public ImageObject createFromParcel(Parcel parcel) {
            return new ImageObject(parcel);
        }

        public ImageObject[] newArray(int i) {
            return new ImageObject[i];
        }
    };
    private static final int DATA_SIZE = 2097152;
    public byte[] imageData;
    public String imagePath;

    public ImageObject() {
    }

    public ImageObject(Parcel parcel) {
        this.imageData = parcel.createByteArray();
        this.imagePath = parcel.readString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0027 A[SYNTHETIC, Splitter:B:13:0x0027] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0034 A[SYNTHETIC, Splitter:B:20:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void setImageObject(android.graphics.Bitmap r4) {
        /*
            r3 = this;
            r2 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0019, all -> 0x0030 }
            r1.<init>()     // Catch:{ Exception -> 0x0019, all -> 0x0030 }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x0044 }
            r2 = 85
            r4.compress(r0, r2, r1)     // Catch:{ Exception -> 0x0044 }
            byte[] r0 = r1.toByteArray()     // Catch:{ Exception -> 0x0044 }
            r3.imageData = r0     // Catch:{ Exception -> 0x0044 }
            if (r1 == 0) goto L_0x0018
            r1.close()     // Catch:{ IOException -> 0x003d }
        L_0x0018:
            return
        L_0x0019:
            r0 = move-exception
            r1 = r2
        L_0x001b:
            r0.printStackTrace()     // Catch:{ all -> 0x0042 }
            java.lang.String r0 = "Weibo.ImageObject"
            java.lang.String r2 = "put thumb failed"
            com.sina.weibo.sdk.utils.LogUtil.e(r0, r2)     // Catch:{ all -> 0x0042 }
            if (r1 == 0) goto L_0x0018
            r1.close()     // Catch:{ IOException -> 0x002b }
            goto L_0x0018
        L_0x002b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0018
        L_0x0030:
            r0 = move-exception
            r1 = r2
        L_0x0032:
            if (r1 == 0) goto L_0x0037
            r1.close()     // Catch:{ IOException -> 0x0038 }
        L_0x0037:
            throw r0
        L_0x0038:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0037
        L_0x003d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0018
        L_0x0042:
            r0 = move-exception
            goto L_0x0032
        L_0x0044:
            r0 = move-exception
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.api.ImageObject.setImageObject(android.graphics.Bitmap):void");
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByteArray(this.imageData);
        parcel.writeString(this.imagePath);
    }

    public boolean checkArgs() {
        if (this.imageData == null && this.imagePath == null) {
            LogUtil.e("Weibo.ImageObject", "imageData and imagePath are null");
            return false;
        } else if (this.imageData != null && this.imageData.length > 2097152) {
            LogUtil.e("Weibo.ImageObject", "imageData is too large");
            return false;
        } else if (this.imagePath == null || this.imagePath.length() <= 512) {
            if (this.imagePath != null) {
                File file = new File(this.imagePath);
                try {
                    if (!file.exists() || file.length() == 0 || file.length() > 10485760) {
                        LogUtil.e("Weibo.ImageObject", "checkArgs fail, image content is too large or not exists");
                        return false;
                    }
                } catch (SecurityException e) {
                    LogUtil.e("Weibo.ImageObject", "checkArgs fail, image content is too large or not exists");
                    return false;
                }
            }
            return true;
        } else {
            LogUtil.e("Weibo.ImageObject", "imagePath is too length");
            return false;
        }
    }

    public int getObjType() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public BaseMediaObject toExtraMediaObject(String str) {
        return this;
    }

    /* access modifiers changed from: protected */
    public String toExtraMediaString() {
        return "";
    }
}
