package com.sina.weibo.sdk.component;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.net.HttpManager;
import com.sina.weibo.sdk.net.WeiboParameters;
import com.sina.weibo.sdk.utils.LogUtil;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GameManager {
    private static final String BOUNDARY = HttpManager.getBoundry();
    public static final String DEFAULT_CHARSET = "UTF-8";
    private static final String HTTP_METHOD_GET = "GET";
    private static final String HTTP_METHOD_POST = "POST";
    private static String INVITATION_ONE_FRINED_URL = "http://widget.weibo.com/invitation/appinfo.php?";
    private static String INVITATION_URL = "http://widget.weibo.com/invitation/app.php?";
    private static final String MULTIPART_FORM_DATA = "multipart/form-data";
    private static final String TAG = "GameManager";
    private static StringBuffer URL = new StringBuffer("https://api.weibo.com/2/proxy/darwin/graph/game/");
    private static String URL_ACHIEVEMENT_ADD_UPDATE = (((Object) URL) + "achievement/add.json");
    private static String URL_ACHIEVEMENT_READ_PLAYER_FRIENDS = (((Object) URL) + "score/read_player_friends.json");
    private static String URL_ACHIEVEMENT_READ_PLAYER_SCORE = (((Object) URL) + "score/read_player.json");
    private static String URL_ACHIEVEMENT_RELATION_ADD_UPDATE = (((Object) URL) + "achievement/gain/add.json");
    private static String URL_ACHIEVEMENT_SCORE_ADD_UPDATE = (((Object) URL) + "score/add.json");
    private static String URL_ACHIEVEMENT_USER_GAIN = (((Object) URL) + "achievement/user_gain.json");

    public static String AddOrUpdateGameAchievement(Context context, WeiboParameters weiboParameters) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        weiboParameters.put("updated_time", simpleDateFormat.format(date));
        if (TextUtils.isEmpty((String) weiboParameters.get(WBConstants.GAME_PARAMS_GAME_CREATE_TIME))) {
            weiboParameters.put(WBConstants.GAME_PARAMS_GAME_CREATE_TIME, simpleDateFormat.format(date));
        }
        String readRsponse = HttpManager.readRsponse(requestHttpExecute(context, URL_ACHIEVEMENT_ADD_UPDATE, "POST", weiboParameters));
        LogUtil.d(TAG, "Response : " + readRsponse);
        return readRsponse;
    }

    public static String addOrUpdateGameAchievementRelation(Context context, WeiboParameters weiboParameters) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        weiboParameters.put("updated_time", simpleDateFormat.format(date));
        if (TextUtils.isEmpty((String) weiboParameters.get(WBConstants.GAME_PARAMS_GAME_CREATE_TIME))) {
            weiboParameters.put(WBConstants.GAME_PARAMS_GAME_CREATE_TIME, simpleDateFormat.format(date));
        }
        String readRsponse = HttpManager.readRsponse(requestHttpExecute(context, URL_ACHIEVEMENT_RELATION_ADD_UPDATE, "POST", weiboParameters));
        LogUtil.d(TAG, "Response : " + readRsponse);
        return readRsponse;
    }

    public static String addOrUpdateAchievementScore(Context context, String str, String str2, String str3, String str4, String str5) {
        WeiboParameters weiboParameters = new WeiboParameters("");
        if (!TextUtils.isEmpty(str)) {
            weiboParameters.put("access_token", str);
        }
        if (!TextUtils.isEmpty(str2)) {
            weiboParameters.put("source", str2);
        }
        if (!TextUtils.isEmpty(str3)) {
            weiboParameters.put(WBConstants.GAME_PARAMS_GAME_ID, str3);
        }
        if (!TextUtils.isEmpty(str4)) {
            weiboParameters.put("uid", str4);
        }
        if (!TextUtils.isEmpty(str5)) {
            weiboParameters.put(WBConstants.GAME_PARAMS_SCORE, str5);
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        weiboParameters.put("updated_time", simpleDateFormat.format(date));
        if (TextUtils.isEmpty((String) weiboParameters.get(WBConstants.GAME_PARAMS_GAME_CREATE_TIME))) {
            weiboParameters.put(WBConstants.GAME_PARAMS_GAME_CREATE_TIME, simpleDateFormat.format(date));
        }
        String readRsponse = HttpManager.readRsponse(requestHttpExecute(context, URL_ACHIEVEMENT_SCORE_ADD_UPDATE, "POST", weiboParameters));
        LogUtil.d(TAG, "Response : " + readRsponse);
        return readRsponse;
    }

    public static String readPlayerScoreInfo(Context context, String str, String str2, String str3, String str4) {
        WeiboParameters weiboParameters = new WeiboParameters("");
        if (!TextUtils.isEmpty(str)) {
            weiboParameters.put("access_token", str);
        }
        if (!TextUtils.isEmpty(str2)) {
            weiboParameters.put("source", str2);
        }
        if (!TextUtils.isEmpty(str3)) {
            weiboParameters.put(WBConstants.GAME_PARAMS_GAME_ID, str3);
        }
        if (!TextUtils.isEmpty(str4)) {
            weiboParameters.put("uid", str4);
        }
        String readRsponse = HttpManager.readRsponse(requestHttpExecute(context, URL_ACHIEVEMENT_READ_PLAYER_SCORE, "GET", weiboParameters));
        LogUtil.d(TAG, "Response : " + readRsponse);
        return readRsponse;
    }

    public static String readPlayerFriendsScoreInfo(Context context, String str, String str2, String str3, String str4) {
        WeiboParameters weiboParameters = new WeiboParameters("");
        if (!TextUtils.isEmpty(str)) {
            weiboParameters.put("access_token", str);
        }
        if (!TextUtils.isEmpty(str2)) {
            weiboParameters.put("source", str2);
        }
        if (!TextUtils.isEmpty(str3)) {
            weiboParameters.put(WBConstants.GAME_PARAMS_GAME_ID, str3);
        }
        if (!TextUtils.isEmpty(str4)) {
            weiboParameters.put("uid", str4);
        }
        weiboParameters.put(WBConstants.GAME_PARAMS_GAME_CREATE_TIME, new Timestamp(new Date().getTime()));
        String readRsponse = HttpManager.readRsponse(requestHttpExecute(context, URL_ACHIEVEMENT_READ_PLAYER_FRIENDS, "GET", weiboParameters));
        LogUtil.d(TAG, "Response : " + readRsponse);
        return readRsponse;
    }

    public static String readPlayerAchievementGain(Context context, String str, String str2, String str3, String str4) {
        WeiboParameters weiboParameters = new WeiboParameters("");
        if (!TextUtils.isEmpty(str)) {
            weiboParameters.put("access_token", str);
        }
        if (!TextUtils.isEmpty(str2)) {
            weiboParameters.put("source", str2);
        }
        if (!TextUtils.isEmpty(str3)) {
            weiboParameters.put(WBConstants.GAME_PARAMS_GAME_ID, str3);
        }
        if (!TextUtils.isEmpty(str4)) {
            weiboParameters.put("uid", str4);
        }
        weiboParameters.put(WBConstants.GAME_PARAMS_GAME_CREATE_TIME, new Timestamp(new Date().getTime()));
        String readRsponse = HttpManager.readRsponse(requestHttpExecute(context, URL_ACHIEVEMENT_USER_GAIN, "GET", weiboParameters));
        LogUtil.d(TAG, "Response : " + readRsponse);
        return readRsponse;
    }

    public void invatationWeiboFriendsByList(Context context, String str, String str2, String str3, WeiboAuthListener weiboAuthListener) {
        WeiboParameters weiboParameters = new WeiboParameters(str2);
        weiboParameters.put("access_token", str);
        weiboParameters.put("source", str2);
        GameRequestParam gameRequestParam = new GameRequestParam(context);
        gameRequestParam.setAppKey(str2);
        gameRequestParam.setToken(str);
        gameRequestParam.setLauncher(BrowserLauncher.GAME);
        gameRequestParam.setUrl(String.valueOf(INVITATION_URL.toString()) + weiboParameters.encodeUrl());
        gameRequestParam.setAuthListener(weiboAuthListener);
        Intent intent = new Intent(context, WeiboSdkBrowser.class);
        Bundle createRequestParamBundle = gameRequestParam.createRequestParamBundle();
        createRequestParamBundle.putString("key_specify_title", str3);
        intent.putExtras(createRequestParamBundle);
        context.startActivity(intent);
    }

    public void invatationWeiboFriendsInOnePage(Context context, String str, String str2, String str3, WeiboAuthListener weiboAuthListener, ArrayList<String> arrayList) {
        StringBuffer stringBuffer = new StringBuffer();
        if (arrayList != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= arrayList.size()) {
                    break;
                }
                String str4 = arrayList.get(i2);
                if (i2 == 0) {
                    stringBuffer.append(str4);
                } else {
                    stringBuffer.append(MiPushClient.ACCEPT_TIME_SEPARATOR + str4);
                }
                i = i2 + 1;
            }
        }
        WeiboParameters weiboParameters = new WeiboParameters(str2);
        weiboParameters.put("access_token", str);
        weiboParameters.put("source", str2);
        GameRequestParam gameRequestParam = new GameRequestParam(context);
        gameRequestParam.setAppKey(str2);
        gameRequestParam.setToken(str);
        gameRequestParam.setLauncher(BrowserLauncher.GAME);
        gameRequestParam.setUrl(String.valueOf(INVITATION_ONE_FRINED_URL.toString()) + weiboParameters.encodeUrl() + "&uids=" + stringBuffer.toString());
        gameRequestParam.setAuthListener(weiboAuthListener);
        Intent intent = new Intent(context, WeiboSdkBrowser.class);
        Bundle createRequestParamBundle = gameRequestParam.createRequestParamBundle();
        createRequestParamBundle.putString("key_specify_title", str3);
        intent.putExtras(createRequestParamBundle);
        context.startActivity(intent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0074 A[SYNTHETIC, Splitter:B:19:0x0074] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.apache.http.HttpResponse requestHttpExecute(android.content.Context r7, java.lang.String r8, java.lang.String r9, com.sina.weibo.sdk.net.WeiboParameters r10) {
        /*
            r1 = 0
            org.apache.http.client.HttpClient r3 = com.sina.weibo.sdk.net.HttpManager.getNewHttpClient()     // Catch:{ IOException -> 0x013e, all -> 0x013a }
            org.apache.http.params.HttpParams r0 = r3.getParams()     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.String r2 = "http.route.default-proxy"
            org.apache.http.HttpHost r4 = com.sina.weibo.sdk.net.NetStateManager.getAPN()     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            r0.setParameter(r2, r4)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.String r0 = "GET"
            boolean r0 = r9.equals(r0)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            if (r0 == 0) goto L_0x007b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.String r2 = java.lang.String.valueOf(r8)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.String r2 = "?"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.String r2 = r10.encodeUrl()     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.String r2 = r0.toString()     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.String r4 = "GameManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.String r6 = "requestHttpExecute GET Url : "
            r5.<init>(r6)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            com.sina.weibo.sdk.utils.LogUtil.d(r4, r2)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
        L_0x004e:
            org.apache.http.HttpResponse r0 = r3.execute(r0)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            org.apache.http.StatusLine r2 = r0.getStatusLine()     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            int r2 = r2.getStatusCode()     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 == r4) goto L_0x012c
            java.lang.String r0 = com.sina.weibo.sdk.net.HttpManager.readRsponse(r0)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            com.sina.weibo.sdk.exception.WeiboHttpException r4 = new com.sina.weibo.sdk.exception.WeiboHttpException     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            r4.<init>(r0, r2)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            throw r4     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
        L_0x0068:
            r0 = move-exception
            r2 = r3
        L_0x006a:
            com.sina.weibo.sdk.exception.WeiboException r3 = new com.sina.weibo.sdk.exception.WeiboException     // Catch:{ all -> 0x0070 }
            r3.<init>(r0)     // Catch:{ all -> 0x0070 }
            throw r3     // Catch:{ all -> 0x0070 }
        L_0x0070:
            r0 = move-exception
            r3 = r2
        L_0x0072:
            if (r1 == 0) goto L_0x0077
            r1.close()     // Catch:{ IOException -> 0x0135 }
        L_0x0077:
            com.sina.weibo.sdk.net.HttpManager.shutdownHttpClient(r3)
            throw r0
        L_0x007b:
            java.lang.String r0 = "POST"
            boolean r0 = r9.equals(r0)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            if (r0 == 0) goto L_0x011a
            java.lang.String r0 = "GameManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.String r4 = "requestHttpExecute POST Url : "
            r2.<init>(r4)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            com.sina.weibo.sdk.utils.LogUtil.d(r0, r2)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            org.apache.http.client.methods.HttpPost r4 = new org.apache.http.client.methods.HttpPost     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            r4.<init>(r8)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            r2.<init>()     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            boolean r0 = r10.hasBinaryData()     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            if (r0 == 0) goto L_0x00cf
            java.lang.String r0 = "Content-Type"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            java.lang.String r5 = "multipart/form-data; boundary="
            r1.<init>(r5)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            java.lang.String r5 = com.sina.weibo.sdk.component.GameManager.BOUNDARY     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            r4.setHeader(r0, r1)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            com.sina.weibo.sdk.net.HttpManager.buildParams(r2, r10)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
        L_0x00c0:
            org.apache.http.entity.ByteArrayEntity r0 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            byte[] r1 = r2.toByteArray()     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            r0.<init>(r1)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            r4.setEntity(r0)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            r0 = r4
            r1 = r2
            goto L_0x004e
        L_0x00cf:
            java.lang.String r0 = "content-type"
            java.lang.Object r0 = r10.get(r0)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            if (r0 == 0) goto L_0x010e
            boolean r1 = r0 instanceof java.lang.String     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            if (r1 == 0) goto L_0x010e
            java.lang.String r1 = "content-type"
            r10.remove(r1)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            java.lang.String r1 = "Content-Type"
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            r4.setHeader(r1, r0)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
        L_0x00e7:
            java.lang.String r0 = r10.encodeUrl()     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            java.lang.String r1 = "GameManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            java.lang.String r6 = "requestHttpExecute POST postParam : "
            r5.<init>(r6)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            com.sina.weibo.sdk.utils.LogUtil.d(r1, r5)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            r2.write(r0)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            goto L_0x00c0
        L_0x0109:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x006a
        L_0x010e:
            java.lang.String r0 = "Content-Type"
            java.lang.String r1 = "application/x-www-form-urlencoded"
            r4.setHeader(r0, r1)     // Catch:{ IOException -> 0x0109, all -> 0x0116 }
            goto L_0x00e7
        L_0x0116:
            r0 = move-exception
            r1 = r2
            goto L_0x0072
        L_0x011a:
            java.lang.String r0 = "DELETE"
            boolean r0 = r9.equals(r0)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            if (r0 == 0) goto L_0x0142
            org.apache.http.client.methods.HttpDelete r0 = new org.apache.http.client.methods.HttpDelete     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            r0.<init>(r8)     // Catch:{ IOException -> 0x0068, all -> 0x0129 }
            goto L_0x004e
        L_0x0129:
            r0 = move-exception
            goto L_0x0072
        L_0x012c:
            if (r1 == 0) goto L_0x0131
            r1.close()     // Catch:{ IOException -> 0x0138 }
        L_0x0131:
            com.sina.weibo.sdk.net.HttpManager.shutdownHttpClient(r3)
            return r0
        L_0x0135:
            r1 = move-exception
            goto L_0x0077
        L_0x0138:
            r1 = move-exception
            goto L_0x0131
        L_0x013a:
            r0 = move-exception
            r3 = r1
            goto L_0x0072
        L_0x013e:
            r0 = move-exception
            r2 = r1
            goto L_0x006a
        L_0x0142:
            r0 = r1
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.component.GameManager.requestHttpExecute(android.content.Context, java.lang.String, java.lang.String, com.sina.weibo.sdk.net.WeiboParameters):org.apache.http.HttpResponse");
    }
}
