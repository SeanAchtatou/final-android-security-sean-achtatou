package com.sina.weibo.sdk.cmd;

import com.sina.weibo.sdk.cmd.BaseCmd;

interface CmdExecutor<T extends BaseCmd> {
    boolean doExecutor(BaseCmd baseCmd);
}
