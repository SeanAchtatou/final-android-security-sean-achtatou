package com.sina.weibo.sdk.utils;

import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public final class Base64 {
    private static char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".toCharArray();
    private static byte[] codes = new byte[256];

    static {
        for (int i = 0; i < 256; i++) {
            codes[i] = -1;
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            codes[i2] = (byte) (i2 - 65);
        }
        for (int i3 = 97; i3 <= 122; i3++) {
            codes[i3] = (byte) ((i3 + 26) - 97);
        }
        for (int i4 = 48; i4 <= 57; i4++) {
            codes[i4] = (byte) ((i4 + 52) - 48);
        }
        codes[43] = 62;
        codes[47] = 63;
    }

    public static byte[] decode(byte[] bArr) {
        int i = 0;
        int length = ((bArr.length + 3) / 4) * 3;
        if (bArr.length > 0 && bArr[bArr.length - 1] == 61) {
            length--;
        }
        if (bArr.length > 1 && bArr[bArr.length - 2] == 61) {
            length--;
        }
        byte[] bArr2 = new byte[length];
        byte b = 0;
        int i2 = 0;
        for (byte b2 : bArr) {
            byte b3 = codes[b2 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD];
            if (b3 >= 0) {
                int i3 = b << 6;
                int i4 = i2 + 6;
                byte b4 = i3 | b3;
                if (i4 >= 8) {
                    int i5 = i4 - 8;
                    bArr2[i] = (byte) ((b4 >> i5) & 255);
                    i++;
                    b = b4;
                    i2 = i5;
                } else {
                    byte b5 = b4;
                    i2 = i4;
                    b = b5;
                }
            }
        }
        if (i == bArr2.length) {
            return bArr2;
        }
        throw new RuntimeException("miscalculated data length!");
    }

    public static char[] encode(byte[] bArr) {
        boolean z;
        boolean z2;
        int i;
        char[] cArr = new char[(((bArr.length + 2) / 3) * 4)];
        int i2 = 0;
        int i3 = 0;
        while (i3 < bArr.length) {
            int i4 = (bArr[i3] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8;
            if (i3 + 1 < bArr.length) {
                i4 |= bArr[i3 + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                z = true;
            } else {
                z = false;
            }
            int i5 = i4 << 8;
            if (i3 + 2 < bArr.length) {
                i5 |= bArr[i3 + 2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                z2 = true;
            } else {
                z2 = false;
            }
            cArr[i2 + 3] = alphabet[z2 ? i5 & 63 : 64];
            int i6 = i5 >> 6;
            int i7 = i2 + 2;
            char[] cArr2 = alphabet;
            if (z) {
                i = i6 & 63;
            } else {
                i = 64;
            }
            cArr[i7] = cArr2[i];
            int i8 = i6 >> 6;
            cArr[i2 + 1] = alphabet[i8 & 63];
            cArr[i2 + 0] = alphabet[(i8 >> 6) & 63];
            i3 += 3;
            i2 += 4;
        }
        return cArr;
    }

    public static byte[] encodebyte(byte[] bArr) {
        boolean z;
        boolean z2;
        int i;
        byte[] bArr2 = new byte[(((bArr.length + 2) / 3) * 4)];
        int i2 = 0;
        int i3 = 0;
        while (i3 < bArr.length) {
            int i4 = (bArr[i3] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8;
            if (i3 + 1 < bArr.length) {
                i4 |= bArr[i3 + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                z = true;
            } else {
                z = false;
            }
            int i5 = i4 << 8;
            if (i3 + 2 < bArr.length) {
                i5 |= bArr[i3 + 2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                z2 = true;
            } else {
                z2 = false;
            }
            bArr2[i2 + 3] = (byte) alphabet[z2 ? i5 & 63 : 64];
            int i6 = i5 >> 6;
            int i7 = i2 + 2;
            char[] cArr = alphabet;
            if (z) {
                i = i6 & 63;
            } else {
                i = 64;
            }
            bArr2[i7] = (byte) cArr[i];
            int i8 = i6 >> 6;
            bArr2[i2 + 1] = (byte) alphabet[i8 & 63];
            bArr2[i2 + 0] = (byte) alphabet[(i8 >> 6) & 63];
            i3 += 3;
            i2 += 4;
        }
        return bArr2;
    }
}
