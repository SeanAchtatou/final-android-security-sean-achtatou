package com.sina.weibo.sdk.statistic;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.StringUtil;
import com.sina.weibo.sdk.utils.LogUtil;
import com.sina.weibo.sdk.utils.MD5;
import com.sina.weibo.sdk.utils.Utility;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPOutputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class LogReport {
    private static final int CONNECTION_TIMEOUT = 25000;
    private static final String PRIVATE_CODE = "dqwef1864il4c9m6";
    private static final int SOCKET_TIMEOUT = 20000;
    private static String UPLOADTIME = "uploadtime";
    private static String mAid;
    private static String mAppkey;
    private static String mBaseUrl = "https://api.weibo.com/2/proxy/sdk/statistic.json";
    private static String mChannel;
    private static String mKeyHash;
    public static LogReport mLogReport;
    private static String mPackageName;
    private static JSONObject mParams;
    private static String mVersionName;

    public LogReport(Context context) {
        try {
            if (mPackageName == null) {
                mPackageName = context.getPackageName();
            }
            mAppkey = StatisticConfig.getAppkey(context);
            checkAid(context);
            mKeyHash = Utility.getSign(context, mPackageName);
            mVersionName = LogBuilder.getVersion(context);
            mChannel = StatisticConfig.getChannel(context);
        } catch (Exception e) {
            LogUtil.e(WBAgent.TAG, e.toString());
        }
        initCommonParams();
    }

    private static JSONObject initCommonParams() {
        if (mParams == null) {
            mParams = new JSONObject();
        }
        try {
            mParams.put(LogBuilder.KEY_APPKEY, mAppkey);
            mParams.put("platform", "Android");
            mParams.put("packagename", mPackageName);
            mParams.put("key_hash", mKeyHash);
            mParams.put("version", mVersionName);
            mParams.put(LogBuilder.KEY_CHANNEL, mChannel);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mParams;
    }

    private static void checkAid(Context context) {
        if (TextUtils.isEmpty(mAid)) {
            mAid = Utility.getAid(context, mAppkey);
        }
        if (mParams == null) {
            mParams = new JSONObject();
        }
        try {
            mParams.put("aid", mAid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void setPackageName(String str) {
        mPackageName = str;
    }

    public static String getPackageName() {
        return mPackageName;
    }

    public static synchronized void uploadAppLogs(Context context, String str) {
        synchronized (LogReport.class) {
            if (mLogReport == null) {
                mLogReport = new LogReport(context);
            }
            if (!isNetworkConnected(context)) {
                LogUtil.i(WBAgent.TAG, "network is not connected");
                LogFileUtil.writeToFile(LogFileUtil.getAppLogPath(LogFileUtil.ANALYTICS_FILE_NAME), str, true);
            } else {
                List<JSONArray> validUploadLogs = LogBuilder.getValidUploadLogs(str);
                if (validUploadLogs == null) {
                    LogUtil.i(WBAgent.TAG, "applogs is null");
                } else {
                    ArrayList<JSONArray> arrayList = new ArrayList<>();
                    checkAid(context);
                    for (JSONArray next : validUploadLogs) {
                        HttpResponse requestHttpExecute = requestHttpExecute(mBaseUrl, "POST", mParams, next);
                        if (requestHttpExecute != null && requestHttpExecute.getStatusLine().getStatusCode() == 200) {
                            updateTime(context, Long.valueOf(System.currentTimeMillis()));
                        } else {
                            arrayList.add(next);
                            LogUtil.e(WBAgent.TAG, "upload applogs error");
                        }
                    }
                    LogFileUtil.delete(LogFileUtil.getAppLogPath(LogFileUtil.ANALYTICS_FILE_NAME));
                    if (arrayList.size() > 0) {
                        for (JSONArray jSONArray : arrayList) {
                            LogFileUtil.writeToFile(LogFileUtil.getAppLogPath(LogFileUtil.ANALYTICS_FILE_NAME), jSONArray.toString(), true);
                            LogUtil.d(WBAgent.TAG, "save failed_log");
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x0139 A[SYNTHETIC, Splitter:B:58:0x0139] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0149 A[SYNTHETIC, Splitter:B:66:0x0149] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0157 A[SYNTHETIC, Splitter:B:72:0x0157] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:27:0x00b2=Splitter:B:27:0x00b2, B:55:0x0134=Splitter:B:55:0x0134, B:63:0x0144=Splitter:B:63:0x0144} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.apache.http.HttpResponse requestHttpExecute(java.lang.String r8, java.lang.String r9, org.json.JSONObject r10, org.json.JSONArray r11) {
        /*
            r0 = 0
            r2 = 0
            org.apache.http.client.HttpClient r3 = com.sina.weibo.sdk.net.HttpManager.getNewHttpClient()     // Catch:{ UnsupportedEncodingException -> 0x017f, ClientProtocolException -> 0x0178, IOException -> 0x0141, all -> 0x0151 }
            if (r10 != 0) goto L_0x000c
            org.json.JSONObject r10 = initCommonParams()     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
        L_0x000c:
            java.lang.String r1 = "time"
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ JSONException -> 0x00ab }
            r6 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 / r6
            r10.put(r1, r4)     // Catch:{ JSONException -> 0x00ab }
            java.lang.String r1 = "length"
            int r4 = r11.length()     // Catch:{ JSONException -> 0x00ab }
            r10.put(r1, r4)     // Catch:{ JSONException -> 0x00ab }
            java.lang.String r1 = "sign"
            java.lang.String r4 = "aid"
            java.lang.String r4 = r10.getString(r4)     // Catch:{ JSONException -> 0x00ab }
            java.lang.String r5 = "appkey"
            java.lang.String r5 = r10.getString(r5)     // Catch:{ JSONException -> 0x00ab }
            java.lang.String r6 = "time"
            long r6 = r10.getLong(r6)     // Catch:{ JSONException -> 0x00ab }
            java.lang.String r4 = getSign(r4, r5, r6)     // Catch:{ JSONException -> 0x00ab }
            r10.put(r1, r4)     // Catch:{ JSONException -> 0x00ab }
            java.lang.String r1 = "content"
            r10.put(r1, r11)     // Catch:{ JSONException -> 0x00ab }
            java.lang.String r1 = "WBAgent"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x00ab }
            java.lang.String r5 = "post content--- "
            r4.<init>(r5)     // Catch:{ JSONException -> 0x00ab }
            java.lang.String r5 = r10.toString()     // Catch:{ JSONException -> 0x00ab }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ JSONException -> 0x00ab }
            java.lang.String r4 = r4.toString()     // Catch:{ JSONException -> 0x00ab }
            com.sina.weibo.sdk.utils.LogUtil.d(r1, r4)     // Catch:{ JSONException -> 0x00ab }
        L_0x0059:
            java.lang.String r1 = "GET"
            boolean r1 = r9.equals(r1)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            if (r1 == 0) goto L_0x00be
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.lang.String r2 = java.lang.String.valueOf(r8)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            r1.<init>(r2)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.lang.String r2 = "?"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.lang.String r2 = r10.toString()     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.lang.String r2 = r1.toString()     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            r1.<init>(r2)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            r2 = r0
        L_0x0082:
            org.apache.http.HttpResponse r0 = r3.execute(r1)     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            java.lang.String r1 = "WBAgent"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            java.lang.String r5 = "status code = "
            r4.<init>(r5)     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            org.apache.http.StatusLine r5 = r0.getStatusLine()     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            int r5 = r5.getStatusCode()     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            java.lang.String r4 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            com.sina.weibo.sdk.utils.LogUtil.i(r1, r4)     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            if (r2 == 0) goto L_0x00a7
            r2.close()     // Catch:{ IOException -> 0x016a }
        L_0x00a7:
            shutdownHttpClient(r3)
        L_0x00aa:
            return r0
        L_0x00ab:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            goto L_0x0059
        L_0x00b0:
            r1 = move-exception
            r2 = r0
        L_0x00b2:
            r1.printStackTrace()     // Catch:{ all -> 0x0171 }
            if (r2 == 0) goto L_0x00ba
            r2.close()     // Catch:{ IOException -> 0x0161 }
        L_0x00ba:
            shutdownHttpClient(r3)
            goto L_0x00aa
        L_0x00be:
            java.lang.String r1 = "POST"
            boolean r1 = r9.equals(r1)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            if (r1 == 0) goto L_0x0184
            java.lang.String r1 = com.sina.weibo.sdk.statistic.LogReport.mAppkey     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            if (r1 == 0) goto L_0x00de
            java.lang.String r1 = "WBAgent"
            java.lang.String r4 = "unexpected null AppKey"
            com.sina.weibo.sdk.utils.LogUtil.e(r1, r4)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            if (r0 == 0) goto L_0x00da
            r2.close()     // Catch:{ IOException -> 0x015e }
        L_0x00da:
            shutdownHttpClient(r3)
            goto L_0x00aa
        L_0x00de:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.lang.String r2 = java.lang.String.valueOf(r8)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            r1.<init>(r2)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.lang.String r2 = "?"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.lang.String r2 = "source="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.lang.String r2 = com.sina.weibo.sdk.statistic.LogReport.mAppkey     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.lang.String r1 = r1.toString()     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            org.apache.http.client.methods.HttpPost r1 = getNewHttpPost(r1, r10)     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            r2.<init>()     // Catch:{ UnsupportedEncodingException -> 0x00b0, ClientProtocolException -> 0x017c, IOException -> 0x0173, all -> 0x016d }
            boolean r4 = com.sina.weibo.sdk.statistic.StatisticConfig.isNeedGizp()     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            if (r4 == 0) goto L_0x0127
            java.lang.String r4 = r10.toString()     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            byte[] r4 = gzipLogs(r4)     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            r2.write(r4)     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
        L_0x0117:
            org.apache.http.entity.ByteArrayEntity r4 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            byte[] r5 = r2.toByteArray()     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            r4.<init>(r5)     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            r1.setEntity(r4)     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            goto L_0x0082
        L_0x0125:
            r1 = move-exception
            goto L_0x00b2
        L_0x0127:
            java.lang.String r4 = r10.toString()     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            byte[] r4 = r4.getBytes()     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            r2.write(r4)     // Catch:{ UnsupportedEncodingException -> 0x0125, ClientProtocolException -> 0x0133, IOException -> 0x0176 }
            goto L_0x0117
        L_0x0133:
            r1 = move-exception
        L_0x0134:
            r1.printStackTrace()     // Catch:{ all -> 0x0171 }
            if (r2 == 0) goto L_0x013c
            r2.close()     // Catch:{ IOException -> 0x0164 }
        L_0x013c:
            shutdownHttpClient(r3)
            goto L_0x00aa
        L_0x0141:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x0144:
            r1.printStackTrace()     // Catch:{ all -> 0x0171 }
            if (r2 == 0) goto L_0x014c
            r2.close()     // Catch:{ IOException -> 0x0166 }
        L_0x014c:
            shutdownHttpClient(r3)
            goto L_0x00aa
        L_0x0151:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r0 = r1
        L_0x0155:
            if (r2 == 0) goto L_0x015a
            r2.close()     // Catch:{ IOException -> 0x0168 }
        L_0x015a:
            shutdownHttpClient(r3)
            throw r0
        L_0x015e:
            r1 = move-exception
            goto L_0x00da
        L_0x0161:
            r1 = move-exception
            goto L_0x00ba
        L_0x0164:
            r1 = move-exception
            goto L_0x013c
        L_0x0166:
            r1 = move-exception
            goto L_0x014c
        L_0x0168:
            r1 = move-exception
            goto L_0x015a
        L_0x016a:
            r1 = move-exception
            goto L_0x00a7
        L_0x016d:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0155
        L_0x0171:
            r0 = move-exception
            goto L_0x0155
        L_0x0173:
            r1 = move-exception
            r2 = r0
            goto L_0x0144
        L_0x0176:
            r1 = move-exception
            goto L_0x0144
        L_0x0178:
            r1 = move-exception
            r2 = r0
            r3 = r0
            goto L_0x0134
        L_0x017c:
            r1 = move-exception
            r2 = r0
            goto L_0x0134
        L_0x017f:
            r1 = move-exception
            r2 = r0
            r3 = r0
            goto L_0x00b2
        L_0x0184:
            r1 = r0
            r2 = r0
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.statistic.LogReport.requestHttpExecute(java.lang.String, java.lang.String, org.json.JSONObject, org.json.JSONArray):org.apache.http.HttpResponse");
    }

    private static boolean isNetworkConnected(Context context) {
        NetworkInfo networkInfo;
        if (context == null) {
            LogUtil.e(WBAgent.TAG, "unexpected null context in isNetworkConnected");
            return false;
        } else if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
            return false;
        } else {
            try {
                networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            } catch (NullPointerException e) {
                networkInfo = null;
            }
            if (networkInfo == null || !networkInfo.isAvailable()) {
                return false;
            }
            return true;
        }
    }

    private static synchronized HttpPost getNewHttpPost(String str, JSONObject jSONObject) {
        HttpPost httpPost;
        synchronized (LogReport.class) {
            httpPost = new HttpPost(str);
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
            httpPost.setHeader("Connection", "Keep-Alive");
            httpPost.addHeader("Content-Encoding", StatisticConfig.isNeedGizp() ? "gzip" : "charset=UTF-8");
            httpPost.addHeader("Accept", "*/*");
            httpPost.addHeader("Accept-Language", "en-us");
            httpPost.addHeader("Accept-Encoding", "gzip");
        }
        return httpPost;
    }

    private static String getSign(String str, String str2, long j) {
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
        }
        sb.append(str2).append(PRIVATE_CODE).append(j);
        String hexdigest = MD5.hexdigest(sb.toString());
        String substring = hexdigest.substring(hexdigest.length() - 6);
        String hexdigest2 = MD5.hexdigest(String.valueOf(substring) + substring.substring(0, 4));
        return String.valueOf(substring) + hexdigest2.substring(hexdigest2.length() - 1);
    }

    private static byte[] gzipLogs(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bytes = str.getBytes(StringUtil.Encoding);
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(bytes);
            gZIPOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static long getTime(Context context) {
        return context.getSharedPreferences(UPLOADTIME, 0).getLong("lasttime", 0);
    }

    private static void updateTime(Context context, Long l) {
        SharedPreferences.Editor edit = context.getSharedPreferences(UPLOADTIME, 0).edit();
        edit.putLong("lasttime", l.longValue());
        edit.commit();
    }

    private static void shutdownHttpClient(HttpClient httpClient) {
        if (httpClient != null) {
            try {
                httpClient.getConnectionManager().closeExpiredConnections();
            } catch (Exception e) {
            }
        }
    }
}
