package com.meizu.cloud.pushsdk.a.f;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.meizu.cloud.pushsdk.a.e.o;
import com.meizu.cloud.pushsdk.a.g.a;
import java.lang.ref.WeakReference;

public class f extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<o> f1107a;

    public f(o oVar) {
        super(Looper.getMainLooper());
        this.f1107a = new WeakReference<>(oVar);
    }

    public void handleMessage(Message message) {
        o oVar = this.f1107a.get();
        switch (message.what) {
            case 1:
                if (oVar != null) {
                    a aVar = (a) message.obj;
                    oVar.a(aVar.f1108a, aVar.b);
                    return;
                }
                return;
            default:
                super.handleMessage(message);
                return;
        }
    }
}
