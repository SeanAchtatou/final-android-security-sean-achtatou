package com.meizu.cloud.pushsdk.a.h;

final class i {

    /* renamed from: a  reason: collision with root package name */
    final byte[] f1119a;
    int b;
    int c;
    boolean d;
    boolean e;
    i f;
    i g;

    i() {
        this.f1119a = new byte[2048];
        this.e = true;
        this.d = false;
    }

    i(i iVar) {
        this(iVar.f1119a, iVar.b, iVar.c);
        iVar.d = true;
    }

    i(byte[] bArr, int i, int i2) {
        this.f1119a = bArr;
        this.b = i;
        this.c = i2;
        this.e = false;
        this.d = true;
    }

    public i a() {
        i iVar = this.f != this ? this.f : null;
        this.g.f = this.f;
        this.f.g = this.g;
        this.f = null;
        this.g = null;
        return iVar;
    }

    public i a(int i) {
        if (i <= 0 || i > this.c - this.b) {
            throw new IllegalArgumentException();
        }
        i iVar = new i(this);
        iVar.c = iVar.b + i;
        this.b += i;
        this.g.a(iVar);
        return iVar;
    }

    public i a(i iVar) {
        iVar.g = this;
        iVar.f = this.f;
        this.f.g = iVar;
        this.f = iVar;
        return iVar;
    }

    public void a(i iVar, int i) {
        if (!iVar.e) {
            throw new IllegalArgumentException();
        }
        if (iVar.c + i > 2048) {
            if (iVar.d) {
                throw new IllegalArgumentException();
            } else if ((iVar.c + i) - iVar.b > 2048) {
                throw new IllegalArgumentException();
            } else {
                System.arraycopy(iVar.f1119a, iVar.b, iVar.f1119a, 0, iVar.c - iVar.b);
                iVar.c -= iVar.b;
                iVar.b = 0;
            }
        }
        System.arraycopy(this.f1119a, this.b, iVar.f1119a, iVar.c, i);
        iVar.c += i;
        this.b += i;
    }

    public void b() {
        if (this.g == this) {
            throw new IllegalStateException();
        } else if (this.g.e) {
            int i = this.c - this.b;
            if (i <= (this.g.d ? 0 : this.g.b) + (2048 - this.g.c)) {
                a(this.g, i);
                a();
                j.a(this);
            }
        }
    }
}
