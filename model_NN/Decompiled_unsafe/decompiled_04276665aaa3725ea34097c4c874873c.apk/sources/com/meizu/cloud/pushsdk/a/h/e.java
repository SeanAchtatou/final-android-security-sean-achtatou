package com.meizu.cloud.pushsdk.a.h;

public abstract class e implements k {

    /* renamed from: a  reason: collision with root package name */
    private final k f1112a;

    public e(k kVar) {
        if (kVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f1112a = kVar;
    }

    public void a(a aVar, long j) {
        this.f1112a.a(aVar, j);
    }

    public void close() {
        this.f1112a.close();
    }

    public void flush() {
        this.f1112a.flush();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f1112a.toString() + ")";
    }
}
