package com.meizu.cloud.pushsdk;

import android.content.Context;
import android.content.Intent;
import com.meizu.cloud.pushinternal.DebugLogger;
import com.meizu.cloud.pushsdk.handler.a.b;
import com.meizu.cloud.pushsdk.handler.a.d;
import com.meizu.cloud.pushsdk.handler.a.e;
import com.meizu.cloud.pushsdk.handler.a.f;
import com.meizu.cloud.pushsdk.handler.c;
import com.meizu.cloud.pushsdk.notification.PushNotificationBuilder;
import com.meizu.cloud.pushsdk.platform.message.PushSwitchStatus;
import com.meizu.cloud.pushsdk.platform.message.RegisterStatus;
import com.meizu.cloud.pushsdk.platform.message.SubAliasStatus;
import com.meizu.cloud.pushsdk.platform.message.SubTagsStatus;
import com.meizu.cloud.pushsdk.platform.message.UnRegisterStatus;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PushMessageProxy {
    private static final String TAG = "PushMessageProxy";
    static volatile PushMessageProxy singleton = null;
    private Context context;
    private Map<Integer, c> managerHashMap;
    /* access modifiers changed from: private */
    public Map<String, com.meizu.cloud.pushsdk.handler.a> messageListenerMap;

    public class a extends com.meizu.cloud.pushsdk.handler.a {
        public a() {
        }

        public void a(Context context, Intent intent) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.a(context, intent);
                }
            }
        }

        public void a(Context context, PushSwitchStatus pushSwitchStatus) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.a(context, pushSwitchStatus);
                }
            }
        }

        public void a(Context context, RegisterStatus registerStatus) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.a(context, registerStatus);
                }
            }
        }

        public void a(Context context, SubAliasStatus subAliasStatus) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.a(context, subAliasStatus);
                }
            }
        }

        public void a(Context context, SubTagsStatus subTagsStatus) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.a(context, subTagsStatus);
                }
            }
        }

        public void a(Context context, UnRegisterStatus unRegisterStatus) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.a(context, unRegisterStatus);
                }
            }
        }

        public void a(Context context, String str) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.a(context, str);
                }
            }
        }

        public void a(Context context, String str, String str2, String str3) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.a(context, str, str2, str3);
                }
            }
        }

        public void a(Context context, boolean z) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.a(context, z);
                }
            }
        }

        public void a(PushNotificationBuilder pushNotificationBuilder) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.a(pushNotificationBuilder);
                }
            }
        }

        public void b(Context context, String str) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.b(context, str);
                }
            }
        }

        public void b(Context context, String str, String str2, String str3) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.b(context, str, str2, str3);
                }
            }
        }

        public void c(Context context, String str) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.c(context, str);
                }
            }
        }

        public void c(Context context, String str, String str2, String str3) {
            for (Map.Entry value : PushMessageProxy.this.messageListenerMap.entrySet()) {
                com.meizu.cloud.pushsdk.handler.a aVar = (com.meizu.cloud.pushsdk.handler.a) value.getValue();
                if (aVar != null) {
                    aVar.c(context, str, str2, str3);
                }
            }
        }
    }

    public PushMessageProxy(Context context2) {
        this(context2, null);
    }

    public PushMessageProxy(Context context2, List<c> list) {
        this(context2, list, null);
    }

    public PushMessageProxy(Context context2, List<c> list, com.meizu.cloud.pushsdk.handler.a aVar) {
        this.managerHashMap = new HashMap();
        this.messageListenerMap = null;
        if (context2 == null) {
            throw new IllegalArgumentException("Context must not be null.");
        }
        this.context = context2.getApplicationContext();
        this.messageListenerMap = new HashMap();
        a aVar2 = new a();
        if (list == null) {
            addHandler(new com.meizu.cloud.pushsdk.handler.a.c(context2, aVar2));
            addHandler(new b(context2, aVar2));
            addHandler(new e(context2, aVar2));
            addHandler(new com.meizu.cloud.pushsdk.handler.a.a.a(context2, aVar2));
            addHandler(new d(context2, aVar2));
            addHandler(new f(context2, aVar2));
            addHandler(new com.meizu.cloud.pushsdk.handler.a.a.b(context2, aVar2));
            addHandler(new com.meizu.cloud.pushsdk.handler.a.b.a(context2, aVar2));
            addHandler(new com.meizu.cloud.pushsdk.handler.a.b.c(context2, aVar2));
            addHandler(new com.meizu.cloud.pushsdk.handler.a.b.f(context2, aVar2));
            addHandler(new com.meizu.cloud.pushsdk.handler.a.b.d(context2, aVar2));
            addHandler(new com.meizu.cloud.pushsdk.handler.a.b.e(context2, aVar2));
            addHandler(new com.meizu.cloud.pushsdk.handler.a.c.a(context2, aVar2));
            addHandler(new com.meizu.cloud.pushsdk.handler.a.b.b(context2, aVar2));
            return;
        }
        addHandler(list);
    }

    public static PushMessageProxy with(Context context2) {
        if (singleton == null) {
            synchronized (PushMessageProxy.class) {
                if (singleton == null) {
                    DebugLogger.i(TAG, "PushMessageProxy init");
                    singleton = new PushMessageProxy(context2);
                }
            }
        }
        return singleton;
    }

    public PushMessageProxy addHandler(c cVar) {
        this.managerHashMap.put(Integer.valueOf(cVar.a()), cVar);
        return this;
    }

    public PushMessageProxy addHandler(List<c> list) {
        if (list == null) {
            throw new IllegalArgumentException("messageManagerList must not be null.");
        }
        for (c addHandler : list) {
            addHandler(addHandler);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public boolean isOnMainThread() {
        return Thread.currentThread() == this.context.getMainLooper().getThread();
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void processMessage(android.content.Intent r4) {
        /*
            r3 = this;
            java.lang.String r0 = "PushMessageProxy"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "is onMainThread "
            java.lang.StringBuilder r1 = r1.append(r2)
            boolean r2 = r3.isOnMainThread()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.meizu.cloud.pushinternal.DebugLogger.e(r0, r1)
            java.util.Map<java.lang.Integer, com.meizu.cloud.pushsdk.handler.c> r0 = r3.managerHashMap
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x0026:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x003e
            java.lang.Object r0 = r1.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r0 = r0.getValue()
            com.meizu.cloud.pushsdk.handler.c r0 = (com.meizu.cloud.pushsdk.handler.c) r0
            boolean r0 = r0.b(r4)
            if (r0 == 0) goto L_0x0026
        L_0x003e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.meizu.cloud.pushsdk.PushMessageProxy.processMessage(android.content.Intent):void");
    }

    public PushMessageProxy receiverListener(String str, com.meizu.cloud.pushsdk.handler.a aVar) {
        this.messageListenerMap.put(str, aVar);
        return this;
    }

    public PushMessageProxy unReceiverListener(String str) {
        this.messageListenerMap.put(str, null);
        return this;
    }
}
