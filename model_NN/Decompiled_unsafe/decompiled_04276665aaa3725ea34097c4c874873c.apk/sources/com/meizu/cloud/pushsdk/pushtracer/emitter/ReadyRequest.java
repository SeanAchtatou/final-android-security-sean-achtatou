package com.meizu.cloud.pushsdk.pushtracer.emitter;

import com.meizu.cloud.pushsdk.a.d.i;
import java.util.LinkedList;

public class ReadyRequest {
    private final LinkedList<Long> ids;
    private final boolean oversize;
    private final i request;

    public ReadyRequest(boolean z, i iVar, LinkedList<Long> linkedList) {
        this.oversize = z;
        this.request = iVar;
        this.ids = linkedList;
    }

    public LinkedList<Long> getEventIds() {
        return this.ids;
    }

    public i getRequest() {
        return this.request;
    }

    public boolean isOversize() {
        return this.oversize;
    }
}
