package com.meizu.cloud.pushsdk.a.f;

import com.meizu.cloud.pushsdk.a.a.b;
import com.meizu.cloud.pushsdk.a.a.d;
import com.meizu.cloud.pushsdk.a.a.e;
import com.meizu.cloud.pushsdk.a.c.a;
import com.meizu.cloud.pushsdk.a.d.k;

public class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public final int f1103a;
    public final b b;
    private final d c;

    public c(b bVar) {
        this.b = bVar;
        this.f1103a = bVar.f();
        this.c = bVar.d();
    }

    private void a(final b bVar, final a aVar) {
        com.meizu.cloud.pushsdk.a.b.b.a().b().c().execute(new Runnable() {
            public void run() {
                bVar.b(aVar);
                bVar.p();
            }
        });
    }

    private void b() {
        k kVar = null;
        try {
            kVar = b.a(this.b);
            if (kVar == null) {
                a(this.b, com.meizu.cloud.pushsdk.a.i.b.a(new a()));
            } else if (this.b.g() == e.OK_HTTP_RESPONSE) {
                this.b.b(kVar);
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.b);
            } else if (kVar.a() >= 400) {
                a(this.b, com.meizu.cloud.pushsdk.a.i.b.a(new a(kVar), this.b, kVar.a()));
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.b);
            } else {
                com.meizu.cloud.pushsdk.a.a.c a2 = this.b.a(kVar);
                if (!a2.b()) {
                    a(this.b, a2.c());
                    com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.b);
                    return;
                }
                a2.a(kVar);
                this.b.a(a2);
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.b);
            }
        } catch (Exception e) {
            a(this.b, com.meizu.cloud.pushsdk.a.i.b.a(new a(e)));
        } finally {
            com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.b);
        }
    }

    private void c() {
        try {
            k b2 = b.b(this.b);
            if (b2 == null) {
                a(this.b, com.meizu.cloud.pushsdk.a.i.b.a(new a()));
            } else if (b2.a() >= 400) {
                a(this.b, com.meizu.cloud.pushsdk.a.i.b.a(new a(b2), this.b, b2.a()));
            } else {
                this.b.j();
            }
        } catch (Exception e) {
            a(this.b, com.meizu.cloud.pushsdk.a.i.b.a(new a(e)));
        }
    }

    private void d() {
        k kVar = null;
        try {
            kVar = b.c(this.b);
            if (kVar == null) {
                a(this.b, com.meizu.cloud.pushsdk.a.i.b.a(new a()));
            } else if (this.b.g() == e.OK_HTTP_RESPONSE) {
                this.b.b(kVar);
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.b);
            } else if (kVar.a() >= 400) {
                a(this.b, com.meizu.cloud.pushsdk.a.i.b.a(new a(kVar), this.b, kVar.a()));
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.b);
            } else {
                com.meizu.cloud.pushsdk.a.a.c a2 = this.b.a(kVar);
                if (!a2.b()) {
                    a(this.b, a2.c());
                    com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.b);
                    return;
                }
                a2.a(kVar);
                this.b.a(a2);
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.b);
            }
        } catch (Exception e) {
            a(this.b, com.meizu.cloud.pushsdk.a.i.b.a(new a(e)));
        } finally {
            com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.b);
        }
    }

    public d a() {
        return this.c;
    }

    public void run() {
        com.meizu.cloud.pushsdk.a.a.a.a("execution started : " + this.b.toString());
        switch (this.b.h()) {
            case 0:
                b();
                break;
            case 1:
                c();
                break;
            case 2:
                d();
                break;
        }
        com.meizu.cloud.pushsdk.a.a.a.a("execution done : " + this.b.toString());
    }
}
