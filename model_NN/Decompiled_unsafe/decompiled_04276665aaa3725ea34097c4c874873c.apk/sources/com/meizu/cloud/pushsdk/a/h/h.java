package com.meizu.cloud.pushsdk.a.h;

import android.support.v4.media.session.PlaybackStateCompat;
import java.io.IOException;
import java.io.InputStream;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

final class h implements c {

    /* renamed from: a  reason: collision with root package name */
    public final a f1117a;
    public final l b;
    /* access modifiers changed from: private */
    public boolean c;

    public h(l lVar) {
        this(lVar, new a());
    }

    public h(l lVar, a aVar) {
        if (lVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        this.f1117a = aVar;
        this.b = lVar;
    }

    public long b(a aVar, long j) {
        if (aVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.c) {
            throw new IllegalStateException("closed");
        } else if (this.f1117a.b == 0 && this.b.b(this.f1117a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) == -1) {
            return -1;
        } else {
            return this.f1117a.b(aVar, Math.min(j, this.f1117a.b));
        }
    }

    public void close() {
        if (!this.c) {
            this.c = true;
            this.b.close();
            this.f1117a.j();
        }
    }

    public InputStream d() {
        return new InputStream() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(long, long):long}
             arg types: [long, int]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(float, float):float}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(long, long):long} */
            public int available() {
                if (!h.this.c) {
                    return (int) Math.min(h.this.f1117a.b, 2147483647L);
                }
                throw new IOException("closed");
            }

            public void close() {
                h.this.close();
            }

            public int read() {
                if (h.this.c) {
                    throw new IOException("closed");
                } else if (h.this.f1117a.b == 0 && h.this.b.b(h.this.f1117a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) == -1) {
                    return -1;
                } else {
                    return h.this.f1117a.f() & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                }
            }

            public int read(byte[] bArr, int i, int i2) {
                if (h.this.c) {
                    throw new IOException("closed");
                }
                n.a((long) bArr.length, (long) i, (long) i2);
                if (h.this.f1117a.b == 0 && h.this.b.b(h.this.f1117a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) == -1) {
                    return -1;
                }
                return h.this.f1117a.a(bArr, i, i2);
            }

            public String toString() {
                return h.this + ".inputStream()";
            }
        };
    }

    public String h() {
        this.f1117a.a(this.b);
        return this.f1117a.h();
    }

    public byte[] i() {
        this.f1117a.a(this.b);
        return this.f1117a.i();
    }

    public String toString() {
        return "buffer(" + this.b + ")";
    }
}
