package com.meizu.cloud.pushsdk.a.b;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f1074a = null;
    private final d b = new c();

    private b() {
    }

    public static b a() {
        if (f1074a == null) {
            synchronized (b.class) {
                if (f1074a == null) {
                    f1074a = new b();
                }
            }
        }
        return f1074a;
    }

    public d b() {
        return this.b;
    }
}
