package com.meizu.cloud.pushsdk.a.b;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;

public class e implements Executor {

    /* renamed from: a  reason: collision with root package name */
    private final Handler f1076a = new Handler(Looper.getMainLooper());

    public void execute(Runnable runnable) {
        this.f1076a.post(runnable);
    }
}
