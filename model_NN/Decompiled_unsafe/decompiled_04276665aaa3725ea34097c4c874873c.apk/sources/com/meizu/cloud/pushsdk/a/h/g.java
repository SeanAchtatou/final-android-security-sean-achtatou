package com.meizu.cloud.pushsdk.a.h;

import android.support.v4.media.session.PlaybackStateCompat;

final class g implements b {

    /* renamed from: a  reason: collision with root package name */
    public final a f1116a;
    public final k b;
    private boolean c;

    public g(k kVar) {
        this(kVar, new a());
    }

    public g(k kVar, a aVar) {
        if (kVar == null) {
            throw new IllegalArgumentException("sink == null");
        }
        this.f1116a = aVar;
        this.b = kVar;
    }

    public long a(l lVar) {
        if (lVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long b2 = lVar.b(this.f1116a, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH);
            if (b2 == -1) {
                return j;
            }
            j += b2;
            a();
        }
    }

    public b a() {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        long e = this.f1116a.e();
        if (e > 0) {
            this.b.a(this.f1116a, e);
        }
        return this;
    }

    public void a(a aVar, long j) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1116a.a(aVar, j);
        a();
    }

    public a b() {
        return this.f1116a;
    }

    public b b(d dVar) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1116a.b(dVar);
        return a();
    }

    public b b(String str) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1116a.b(str);
        return a();
    }

    public b c(byte[] bArr) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1116a.c(bArr);
        return a();
    }

    public b c(byte[] bArr, int i, int i2) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1116a.c(bArr, i, i2);
        return a();
    }

    public void close() {
        if (!this.c) {
            Throwable th = null;
            try {
                if (this.f1116a.b > 0) {
                    this.b.a(this.f1116a, this.f1116a.b);
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.b.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.c = true;
            if (th != null) {
                n.a(th);
            }
        }
    }

    public b e(long j) {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        this.f1116a.e(j);
        return a();
    }

    public void flush() {
        if (this.c) {
            throw new IllegalStateException("closed");
        }
        if (this.f1116a.b > 0) {
            this.b.a(this.f1116a, this.f1116a.b);
        }
        this.b.flush();
    }

    public String toString() {
        return "buffer(" + this.b + ")";
    }
}
