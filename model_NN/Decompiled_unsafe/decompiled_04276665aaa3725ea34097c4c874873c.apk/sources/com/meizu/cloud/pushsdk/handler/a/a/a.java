package com.meizu.cloud.pushsdk.handler.a.a;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.meizu.cloud.pushinternal.DebugLogger;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.meizu.cloud.pushsdk.handler.MessageV3;
import com.meizu.cloud.pushsdk.notification.MPushMessage;
import com.meizu.cloud.pushsdk.notification.e;
import com.meizu.cloud.pushsdk.util.PushPreferencesUtils;
import com.meizu.cloud.pushsdk.util.UxIPUtils;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Map;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class a extends com.meizu.cloud.pushsdk.handler.a.a<MessageV3> {
    public a(Context context, com.meizu.cloud.pushsdk.handler.a aVar) {
        super(context, aVar);
    }

    private Intent a(Context context, MessageV3 messageV3) {
        Intent intent;
        if (messageV3.getClickType() == 0) {
            Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(messageV3.getUploadDataPackageName());
            if (launchIntentForPackage == null || messageV3.getParamsMap() == null) {
                return launchIntentForPackage;
            }
            for (Map.Entry next : messageV3.getParamsMap().entrySet()) {
                DebugLogger.i("AbstractMessageHandler", " launcher activity key " + ((String) next.getKey()) + " value " + ((String) next.getValue()));
                if (!TextUtils.isEmpty((CharSequence) next.getKey()) && !TextUtils.isEmpty((CharSequence) next.getValue())) {
                    launchIntentForPackage.putExtra((String) next.getKey(), b((String) next.getValue()));
                }
            }
            return launchIntentForPackage;
        } else if (1 == messageV3.getClickType()) {
            String str = "";
            if (messageV3.getParamsMap() != null) {
                for (Map.Entry next2 : messageV3.getParamsMap().entrySet()) {
                    DebugLogger.i("AbstractMessageHandler", " key " + ((String) next2.getKey()) + " value " + ((String) next2.getValue()));
                    String str2 = (TextUtils.isEmpty((CharSequence) next2.getKey()) || TextUtils.isEmpty((CharSequence) next2.getValue())) ? str : str + "S." + ((String) next2.getKey()) + "=" + b((String) next2.getValue()) + ";";
                    DebugLogger.i("AbstractMessageHandler", "paramValue " + str2);
                    str = str2;
                }
            }
            String str3 = "intent:#Intent;component=" + messageV3.getUploadDataPackageName() + "/" + messageV3.getActivity() + (TextUtils.isEmpty(str) ? ";" : ";" + str) + "end";
            DebugLogger.i("AbstractMessageHandler", "open activity intent uri " + str3);
            try {
                intent = Intent.parseUri(str3, 1);
            } catch (URISyntaxException e) {
                DebugLogger.e("AbstractMessageHandler", "parse Uri error " + e.getMessage());
                intent = null;
            }
            return intent;
        } else if (2 == messageV3.getClickType()) {
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(messageV3.getWebUrl()));
            String uriPackageName = messageV3.getUriPackageName();
            if (TextUtils.isEmpty(uriPackageName)) {
                return intent2;
            }
            intent2.setPackage(uriPackageName);
            DebugLogger.i("AbstractMessageHandler", "set uri package " + uriPackageName);
            return intent2;
        } else if (3 != messageV3.getClickType()) {
            return null;
        } else {
            DebugLogger.i("AbstractMessageHandler", "CLICK_TYPE_SELF_DEFINE_ACTION");
            return null;
        }
    }

    private String b(String str) {
        try {
            str = URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            DebugLogger.i("AbstractMessageHandler", "encode url fail");
        }
        Log.i("AbstractMessageHandler", "encode all value is " + str);
        return str;
    }

    public int a() {
        return 64;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void b(MessageV3 messageV3) {
        UxIPUtils.onClickPushMessageEvent(c(), messageV3.getUploadDataPackageName(), messageV3.getDeviceId(), messageV3.getTaskId(), messageV3.getSeqId(), messageV3.getPushTimestamp());
    }

    /* access modifiers changed from: protected */
    public void a(MessageV3 messageV3, e eVar) {
        PushPreferencesUtils.putDiscardNotificationIdByPackageName(c(), messageV3.getPackageName(), 0);
        Intent a2 = a(c(), messageV3);
        if (a2 != null) {
            a2.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            try {
                c().startActivity(a2);
            } catch (Exception e) {
                DebugLogger.e("AbstractMessageHandler", "Click message StartActivity error " + e.getMessage());
            }
        }
        if (!TextUtils.isEmpty(messageV3.getTitle()) && !TextUtils.isEmpty(messageV3.getContent())) {
            b().a(c(), messageV3.getTitle(), messageV3.getContent(), a(messageV3.getWebUrl(), messageV3.getParamsMap()));
        }
    }

    public boolean a(Intent intent) {
        DebugLogger.i("AbstractMessageHandler", "start NotificationClickMessageHandler match");
        return PushConstants.MZ_PUSH_ON_MESSAGE_ACTION.equals(intent.getAction()) && PushConstants.MZ_PUSH_MESSAGE_METHOD_ACTION_PRIVATE.equals(i(intent));
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public MessageV3 c(Intent intent) {
        try {
            DebugLogger.e("AbstractMessageHandler", "parse message V3");
            MessageV3 messageV3 = (MessageV3) intent.getParcelableExtra(PushConstants.MZ_PUSH_PRIVATE_MESSAGE);
            if (messageV3 != null) {
                return messageV3;
            }
            DebugLogger.e("AbstractMessageHandler", "parse MessageV2 to MessageV3");
            MPushMessage mPushMessage = (MPushMessage) intent.getSerializableExtra(PushConstants.MZ_PUSH_PRIVATE_MESSAGE);
            return MessageV3.parse(g(intent), d(intent), mPushMessage.getTaskId(), mPushMessage);
        } catch (Exception e) {
            DebugLogger.e("AbstractMessageHandler", "cannot get messageV3");
            if (0 != 0) {
                return null;
            }
            DebugLogger.e("AbstractMessageHandler", "parse MessageV2 to MessageV3");
            MPushMessage mPushMessage2 = (MPushMessage) intent.getSerializableExtra(PushConstants.MZ_PUSH_PRIVATE_MESSAGE);
            return MessageV3.parse(g(intent), d(intent), mPushMessage2.getTaskId(), mPushMessage2);
        } catch (Throwable th) {
            Throwable th2 = th;
            if (0 == 0) {
                DebugLogger.e("AbstractMessageHandler", "parse MessageV2 to MessageV3");
                MPushMessage mPushMessage3 = (MPushMessage) intent.getSerializableExtra(PushConstants.MZ_PUSH_PRIVATE_MESSAGE);
                MessageV3.parse(g(intent), d(intent), mPushMessage3.getTaskId(), mPushMessage3);
            }
            throw th2;
        }
    }
}
