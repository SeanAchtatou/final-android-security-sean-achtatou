package com.meizu.cloud.pushsdk;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.igexin.sdk.PushConsts;
import com.meizu.cloud.pushinternal.DebugLogger;
import com.meizu.cloud.pushsdk.common.base.WorkReceiver;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.meizu.cloud.pushsdk.platform.api.PushPlatformManager;
import com.meizu.cloud.pushsdk.util.MzSystemUtils;
import com.meizu.cloud.pushsdk.util.UxIPUtils;

public class SystemReceiver extends WorkReceiver {
    public SystemReceiver() {
        if (!MzSystemUtils.isBrandMeizu()) {
        }
    }

    public void a(Context context) {
        if (MzSystemUtils.isBrandMeizu()) {
            String appVersionName = MzSystemUtils.getAppVersionName(context, "com.meizu.cloud");
            DebugLogger.i("SystemReceiver", context.getPackageName() + " start register cloudVersion_name " + appVersionName);
            Intent intent = new Intent();
            if ("com.meizu.cloud".equals(MzSystemUtils.getMzPushServicePackageName(context))) {
                DebugLogger.e("SystemReceiver", "cloud pushService start");
                intent.setAction("com.meizu.pushservice.action.START");
                intent.setClassName("com.meizu.cloud", "com.meizu.cloud.pushsdk.pushservice.MzPushService");
            } else if (!TextUtils.isEmpty(appVersionName) && MzSystemUtils.compareVersion(appVersionName, "4.5.7")) {
                DebugLogger.e("SystemReceiver", "flyme 4.x start register cloud versionName " + appVersionName);
                intent.setPackage("com.meizu.cloud");
                intent.setAction(PushConstants.MZ_PUSH_ON_START_PUSH_REGISTER);
            } else if (TextUtils.isEmpty(appVersionName) || !appVersionName.startsWith("3")) {
                DebugLogger.e("SystemReceiver", context.getPackageName() + " start register ");
                intent.setClassName(context.getPackageName(), "com.meizu.cloud.pushsdk.pushservice.MzPushService");
                intent.setAction("com.meizu.pushservice.action.START");
            } else {
                DebugLogger.e("SystemReceiver", "flyme 3.x start register cloud versionName " + appVersionName);
                intent.setAction(PushConstants.REQUEST_REGISTRATION_INTENT);
                intent.setPackage("com.meizu.cloud");
                intent.putExtra(PushConstants.EXTRA_APPLICATION_PENDING_INTENT, PendingIntent.getBroadcast(context, 0, new Intent(), 0));
            }
            a(context, intent);
        }
    }

    public void a(Context context, Intent intent) {
        try {
            if (MzSystemUtils.isBrandMeizu()) {
                context.startService(intent);
            }
        } catch (SecurityException e) {
            DebugLogger.e("SystemReceiver", "start service error " + e.getMessage());
        }
    }

    public void onHandleIntent(Context context, Intent intent) {
        if (intent != null) {
            try {
                if (!MzSystemUtils.isBrandMeizu()) {
                    return;
                }
                if (intent.getAction().equals(PushConsts.ACTION_BROADCAST_TO_BOOT) || "com.meizu.cloud.pushservice.action.PUSH_SERVICE_START".equals(intent.getAction())) {
                    a(context);
                } else if ("android.intent.action.PACKAGE_REMOVED".equals(intent.getAction())) {
                    String encodedSchemeSpecificPart = intent.getData().getEncodedSchemeSpecificPart();
                    DebugLogger.e("SystemReceiver", encodedSchemeSpecificPart + " has been uninstall");
                    if (!TextUtils.isEmpty(encodedSchemeSpecificPart) && !MzSystemUtils.isApkRemoved(context, encodedSchemeSpecificPart)) {
                        DebugLogger.e("SystemReceiver", context.getPackageName() + " SystemReceiver start unregister packageName " + encodedSchemeSpecificPart);
                        MzSystemUtils.putApkRemovedFlag(context, encodedSchemeSpecificPart, true);
                        PushPlatformManager.getInstance(context).unRegisterAdvance(encodedSchemeSpecificPart);
                    }
                } else if ("android.intent.action.PACKAGE_ADDED".equals(intent.getAction())) {
                    String encodedSchemeSpecificPart2 = intent.getData().getEncodedSchemeSpecificPart();
                    DebugLogger.e("SystemReceiver", encodedSchemeSpecificPart2 + " has been install");
                    MzSystemUtils.putApkRemovedFlag(context, encodedSchemeSpecificPart2, false);
                }
            } catch (Exception e) {
                DebugLogger.e("SystemReceiver", "onHandleIntent Exception " + e.getMessage());
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        try {
            if (MzSystemUtils.isBrandMeizu()) {
                super.onReceive(context, intent);
            }
        } catch (Exception e) {
            Exception exc = e;
            DebugLogger.e("SystemReceiver", "Event core error " + exc.getMessage());
            Context context2 = context;
            UxIPUtils.onRecordMessageFlow(context2, context.getPackageName(), null, null, "3.3.161226", "SystemReceiver " + exc.getMessage(), 3000);
        }
    }
}
