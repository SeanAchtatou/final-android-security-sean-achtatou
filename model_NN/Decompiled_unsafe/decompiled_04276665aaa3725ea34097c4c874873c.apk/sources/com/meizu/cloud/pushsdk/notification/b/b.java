package com.meizu.cloud.pushsdk.notification.b;

import com.meizu.cloud.pushinternal.DebugLogger;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private final File f1145a;
    private final File b;
    private String c = this.b.getAbsolutePath();

    public b(String str, String str2) {
        this.f1145a = new File(str);
        this.b = new File(str2);
        DebugLogger.i("ZipExtractTask", "Extract mInput file = " + this.f1145a.toString());
        DebugLogger.i("ZipExtractTask", "Extract mOutput file = " + this.b.toString());
    }

    private int a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[8192];
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 8192);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream, 8192);
        int i = 0;
        while (true) {
            try {
                int read = bufferedInputStream.read(bArr, 0, 8192);
                if (read == -1) {
                    break;
                }
                bufferedOutputStream.write(bArr, 0, read);
                i = read + i;
            } catch (IOException e) {
                DebugLogger.e("ZipExtractTask", "Extracted IOException:" + e.toString());
                try {
                    bufferedOutputStream.close();
                } catch (IOException e2) {
                    DebugLogger.e("ZipExtractTask", "out.close() IOException e=" + e2.toString());
                }
                try {
                    bufferedInputStream.close();
                } catch (IOException e3) {
                    DebugLogger.e("ZipExtractTask", "in.close() IOException e=" + e3.toString());
                }
            } catch (Throwable th) {
                try {
                    bufferedOutputStream.close();
                } catch (IOException e4) {
                    DebugLogger.e("ZipExtractTask", "out.close() IOException e=" + e4.toString());
                }
                try {
                    bufferedInputStream.close();
                } catch (IOException e5) {
                    DebugLogger.e("ZipExtractTask", "in.close() IOException e=" + e5.toString());
                }
                throw th;
            }
        }
        bufferedOutputStream.flush();
        try {
            bufferedOutputStream.close();
        } catch (IOException e6) {
            DebugLogger.e("ZipExtractTask", "out.close() IOException e=" + e6.toString());
        }
        try {
            bufferedInputStream.close();
        } catch (IOException e7) {
            DebugLogger.e("ZipExtractTask", "in.close() IOException e=" + e7.toString());
        }
        return i;
    }

    private void b() {
        if (this.f1145a != null && this.f1145a.exists()) {
            if (this.f1145a.delete()) {
                DebugLogger.i("ZipExtractTask", "Delete file:" + this.f1145a.toString() + " after extracted.");
            } else {
                DebugLogger.i("ZipExtractTask", "Can't delete file:" + this.f1145a.toString() + " after extracted.");
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0163 A[SYNTHETIC, Splitter:B:43:0x0163] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0205 A[SYNTHETIC, Splitter:B:64:0x0205] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private long c() {
        /*
            r14 = this;
            r1 = 0
            r2 = 0
            long r6 = android.os.SystemClock.currentThreadTimeMillis()
            r4 = 0
            java.util.zip.ZipFile r3 = new java.util.zip.ZipFile     // Catch:{ ZipException -> 0x0236, IOException -> 0x0229, all -> 0x0201 }
            java.io.File r0 = r14.f1145a     // Catch:{ ZipException -> 0x0236, IOException -> 0x0229, all -> 0x0201 }
            r3.<init>(r0)     // Catch:{ ZipException -> 0x0236, IOException -> 0x0229, all -> 0x0201 }
            java.util.Enumeration r8 = r3.entries()     // Catch:{ ZipException -> 0x023d, IOException -> 0x0230 }
        L_0x0013:
            boolean r0 = r8.hasMoreElements()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            if (r0 == 0) goto L_0x016a
            java.lang.Object r0 = r8.nextElement()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.util.zip.ZipEntry r0 = (java.util.zip.ZipEntry) r0     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            boolean r9 = r0.isDirectory()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            if (r9 != 0) goto L_0x0013
            java.lang.String r9 = r0.getName()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            if (r1 != 0) goto L_0x005a
            if (r9 == 0) goto L_0x005a
            java.lang.String r10 = "/"
            java.lang.String[] r10 = r9.split(r10)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            r11 = 0
            r1 = r10[r11]     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r10 = "ZipExtractTask"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            r11.<init>()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r12 = "Extract temp directory="
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.io.File r12 = r14.b     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r12 = "/"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.StringBuilder r11 = r11.append(r1)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r11 = r11.toString()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            com.meizu.cloud.pushinternal.DebugLogger.i(r10, r11)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
        L_0x005a:
            java.io.File r10 = new java.io.File     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.io.File r11 = r14.b     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            r10.<init>(r11, r9)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.io.File r9 = r10.getParentFile()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            boolean r9 = r9.exists()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            if (r9 != 0) goto L_0x0095
            java.io.File r9 = r10.getParentFile()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            boolean r9 = r9.mkdirs()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            if (r9 == 0) goto L_0x011f
            java.lang.String r9 = "ZipExtractTask"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            r11.<init>()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r12 = "Make Destination directory="
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.io.File r12 = r10.getParentFile()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r12 = r12.getAbsolutePath()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r11 = r11.toString()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            com.meizu.cloud.pushinternal.DebugLogger.i(r9, r11)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
        L_0x0095:
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            r9.<init>(r10)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.io.InputStream r0 = r3.getInputStream(r0)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            int r0 = r14.a(r0, r9)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            long r10 = (long) r0     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            long r4 = r4 + r10
            r9.close()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            goto L_0x0013
        L_0x00a9:
            r0 = move-exception
            r13 = r0
            r0 = r1
            r1 = r13
        L_0x00ad:
            java.lang.String r8 = "ZipExtractTask"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0227 }
            r9.<init>()     // Catch:{ all -> 0x0227 }
            java.lang.String r10 = "ZipException :"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0227 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0227 }
            java.lang.StringBuilder r1 = r9.append(r1)     // Catch:{ all -> 0x0227 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0227 }
            com.meizu.cloud.pushinternal.DebugLogger.e(r8, r1)     // Catch:{ all -> 0x0227 }
            if (r3 == 0) goto L_0x00ce
            r3.close()     // Catch:{ IOException -> 0x01bf }
        L_0x00ce:
            r1 = r2
            r2 = r4
        L_0x00d0:
            long r4 = android.os.SystemClock.currentThreadTimeMillis()
            java.lang.String r8 = "ZipExtractTask"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Extract file "
            java.lang.StringBuilder r9 = r9.append(r10)
            java.io.File r10 = r14.f1145a
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = ", UseTime ="
            java.lang.StringBuilder r9 = r9.append(r10)
            long r4 = r4 - r6
            java.lang.String r4 = java.lang.String.valueOf(r4)
            java.lang.StringBuilder r4 = r9.append(r4)
            java.lang.String r4 = r4.toString()
            com.meizu.cloud.pushinternal.DebugLogger.i(r8, r4)
            if (r1 == 0) goto L_0x011b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.io.File r4 = r14.b
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = "/"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.meizu.cloud.pushsdk.notification.b.a.b(r0)
        L_0x011b:
            r14.b()
            return r2
        L_0x011f:
            java.lang.String r9 = "ZipExtractTask"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            r11.<init>()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r12 = "Can't make destination directory="
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.io.File r12 = r10.getParentFile()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r12 = r12.getAbsolutePath()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r11 = r11.toString()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            com.meizu.cloud.pushinternal.DebugLogger.i(r9, r11)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            goto L_0x0095
        L_0x0141:
            r0 = move-exception
            r13 = r0
            r0 = r1
            r1 = r13
        L_0x0145:
            java.lang.String r8 = "ZipExtractTask"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0227 }
            r9.<init>()     // Catch:{ all -> 0x0227 }
            java.lang.String r10 = "Extracted IOException:"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x0227 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0227 }
            java.lang.StringBuilder r1 = r9.append(r1)     // Catch:{ all -> 0x0227 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0227 }
            com.meizu.cloud.pushinternal.DebugLogger.e(r8, r1)     // Catch:{ all -> 0x0227 }
            if (r3 == 0) goto L_0x0166
            r3.close()     // Catch:{ IOException -> 0x01e0 }
        L_0x0166:
            r1 = r2
            r2 = r4
            goto L_0x00d0
        L_0x016a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            r0.<init>()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.io.File r8 = r14.b     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r8 = "/"
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r0 = r0.toString()     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            java.lang.String r8 = r14.c     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            boolean r8 = r8.equals(r0)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            if (r8 != 0) goto L_0x0243
            java.lang.String r8 = r14.c     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            com.meizu.cloud.pushsdk.notification.b.a.a(r0, r8)     // Catch:{ ZipException -> 0x00a9, IOException -> 0x0141 }
            r0 = 1
        L_0x0191:
            if (r3 == 0) goto L_0x0196
            r3.close()     // Catch:{ IOException -> 0x019c }
        L_0x0196:
            r2 = r4
            r13 = r0
            r0 = r1
            r1 = r13
            goto L_0x00d0
        L_0x019c:
            r2 = move-exception
            java.lang.String r3 = "ZipExtractTask"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Extracted IOException:"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r2 = r2.toString()
            java.lang.StringBuilder r2 = r8.append(r2)
            java.lang.String r2 = r2.toString()
            com.meizu.cloud.pushinternal.DebugLogger.e(r3, r2)
            r2 = r4
            r13 = r0
            r0 = r1
            r1 = r13
            goto L_0x00d0
        L_0x01bf:
            r1 = move-exception
            java.lang.String r3 = "ZipExtractTask"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Extracted IOException:"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r1 = r8.append(r1)
            java.lang.String r1 = r1.toString()
            com.meizu.cloud.pushinternal.DebugLogger.e(r3, r1)
            r1 = r2
            r2 = r4
            goto L_0x00d0
        L_0x01e0:
            r1 = move-exception
            java.lang.String r3 = "ZipExtractTask"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Extracted IOException:"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r1 = r8.append(r1)
            java.lang.String r1 = r1.toString()
            com.meizu.cloud.pushinternal.DebugLogger.e(r3, r1)
            r1 = r2
            r2 = r4
            goto L_0x00d0
        L_0x0201:
            r0 = move-exception
            r3 = r1
        L_0x0203:
            if (r3 == 0) goto L_0x0208
            r3.close()     // Catch:{ IOException -> 0x0209 }
        L_0x0208:
            throw r0
        L_0x0209:
            r1 = move-exception
            java.lang.String r2 = "ZipExtractTask"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Extracted IOException:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            com.meizu.cloud.pushinternal.DebugLogger.e(r2, r1)
            goto L_0x0208
        L_0x0227:
            r0 = move-exception
            goto L_0x0203
        L_0x0229:
            r0 = move-exception
            r3 = r1
            r13 = r1
            r1 = r0
            r0 = r13
            goto L_0x0145
        L_0x0230:
            r0 = move-exception
            r13 = r0
            r0 = r1
            r1 = r13
            goto L_0x0145
        L_0x0236:
            r0 = move-exception
            r3 = r1
            r13 = r1
            r1 = r0
            r0 = r13
            goto L_0x00ad
        L_0x023d:
            r0 = move-exception
            r13 = r0
            r0 = r1
            r1 = r13
            goto L_0x00ad
        L_0x0243:
            r0 = r2
            goto L_0x0191
        */
        throw new UnsupportedOperationException("Method not decompiled: com.meizu.cloud.pushsdk.notification.b.b.c():long");
    }

    public boolean a() {
        return c() > 0;
    }
}
