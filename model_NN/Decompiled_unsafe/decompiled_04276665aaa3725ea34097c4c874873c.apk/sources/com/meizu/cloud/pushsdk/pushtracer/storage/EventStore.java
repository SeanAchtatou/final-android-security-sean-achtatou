package com.meizu.cloud.pushsdk.pushtracer.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import com.meizu.cloud.pushsdk.pushtracer.dataload.DataLoad;
import com.meizu.cloud.pushsdk.pushtracer.dataload.TrackerDataload;
import com.meizu.cloud.pushsdk.pushtracer.emitter.EmittableEvents;
import com.meizu.cloud.pushsdk.pushtracer.utils.Logger;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class EventStore {
    private String TAG = EventStore.class.getSimpleName();
    private String[] allColumns = {"id", "eventData", "dateCreated"};
    private SQLiteDatabase database;
    private EventStoreHelper dbHelper;
    private long lastInsertedRowId = -1;
    private int sendLimit;

    public EventStore(Context context, int i) {
        this.dbHelper = EventStoreHelper.getInstance(context);
        open();
        this.sendLimit = i;
        Logger.d(this.TAG, "DB Path: " + this.database.getPath(), new Object[0]);
    }

    private static Map<String, String> deserializer(byte[] bArr) {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            HashMap hashMap = (HashMap) objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            return hashMap;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static byte[] serialize(Map<String, String> map) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(map);
            objectOutputStream.close();
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void add(DataLoad dataLoad) {
        insertEvent(dataLoad);
    }

    public void close() {
        this.dbHelper.close();
    }

    public List<Map<String, Object>> getAllEvents() {
        return queryDatabase(null, null);
    }

    public List<Map<String, Object>> getDescEventsInRange(int i) {
        return queryDatabase(null, "id ASC LIMIT " + i);
    }

    public EmittableEvents getEmittableEvents() {
        LinkedList linkedList = new LinkedList();
        ArrayList arrayList = new ArrayList();
        for (Map next : getDescEventsInRange(this.sendLimit)) {
            TrackerDataload trackerDataload = new TrackerDataload();
            trackerDataload.addMap((Map) next.get("eventData"));
            linkedList.add((Long) next.get("id"));
            arrayList.add(trackerDataload);
        }
        return new EmittableEvents(arrayList, linkedList);
    }

    public Map<String, Object> getEvent(long j) {
        List<Map<String, Object>> queryDatabase = queryDatabase("id=" + j, null);
        if (!queryDatabase.isEmpty()) {
            return queryDatabase.get(0);
        }
        return null;
    }

    public long getLastInsertedRowId() {
        return this.lastInsertedRowId;
    }

    public long getSize() {
        return DatabaseUtils.queryNumEntries(this.database, EventStoreHelper.TABLE_EVENTS);
    }

    public long insertEvent(DataLoad dataLoad) {
        if (isDatabaseOpen()) {
            byte[] serialize = serialize(dataLoad.getMap());
            ContentValues contentValues = new ContentValues(2);
            contentValues.put("eventData", serialize);
            this.lastInsertedRowId = this.database.insert(EventStoreHelper.TABLE_EVENTS, null, contentValues);
        }
        Logger.d(this.TAG, "Added event to database: " + this.lastInsertedRowId, new Object[0]);
        return this.lastInsertedRowId;
    }

    public boolean isDatabaseOpen() {
        return this.database != null && this.database.isOpen();
    }

    public void open() {
        if (!isDatabaseOpen()) {
            this.database = this.dbHelper.getWritableDatabase();
            this.database.enableWriteAheadLogging();
        }
    }

    public List<Map<String, Object>> queryDatabase(String str, String str2) {
        ArrayList arrayList = new ArrayList();
        if (isDatabaseOpen()) {
            Cursor query = this.database.query(EventStoreHelper.TABLE_EVENTS, this.allColumns, str, null, null, null, str2);
            query.moveToFirst();
            while (!query.isAfterLast()) {
                HashMap hashMap = new HashMap();
                hashMap.put("id", Long.valueOf(query.getLong(0)));
                hashMap.put("eventData", deserializer(query.getBlob(1)));
                hashMap.put("dateCreated", query.getString(2));
                query.moveToNext();
                arrayList.add(hashMap);
            }
            query.close();
        }
        return arrayList;
    }

    public boolean removeAllEvents() {
        int i = -1;
        if (isDatabaseOpen()) {
            i = this.database.delete(EventStoreHelper.TABLE_EVENTS, null, null);
        }
        Logger.d(this.TAG, "Removing all events from database.", new Object[0]);
        return i == 0;
    }

    public boolean removeEvent(long j) {
        int i = -1;
        if (isDatabaseOpen()) {
            i = this.database.delete(EventStoreHelper.TABLE_EVENTS, "id=" + j, null);
        }
        Logger.d(this.TAG, "Removed event from database: " + j, new Object[0]);
        return i == 1;
    }
}
