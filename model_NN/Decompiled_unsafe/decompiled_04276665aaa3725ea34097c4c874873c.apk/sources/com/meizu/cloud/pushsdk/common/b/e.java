package com.meizu.cloud.pushsdk.common.b;

import android.text.TextUtils;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;

public abstract class e {
    private static HashMap<String, Class<?>> d = new HashMap<>();
    /* access modifiers changed from: private */
    public static HashMap<Class<?>, HashMap<String, Method>> e = new HashMap<>();
    private static HashMap<String, Field> f = new HashMap<>();

    /* renamed from: a  reason: collision with root package name */
    protected Object[] f1132a;
    protected Class<?>[] b;
    protected String c;

    class a extends e {
        private Class<?> d;

        public a(Class<?> cls) {
            this.d = cls;
        }

        public <T> c<T> a() {
            HashMap hashMap;
            c<T> cVar = new c<>();
            if (this.d != null && !TextUtils.isEmpty(this.c)) {
                if (this.f1132a != null && this.f1132a.length > 0 && this.b == null) {
                    this.b = e.c(this.f1132a);
                }
                try {
                    HashMap hashMap2 = (HashMap) e.e.get(this.d);
                    if (hashMap2 == null) {
                        HashMap hashMap3 = new HashMap();
                        e.e.put(this.d, hashMap3);
                        hashMap = hashMap3;
                    } else {
                        hashMap = hashMap2;
                    }
                    Method method = (Method) hashMap.get(this.c);
                    if (method == null) {
                        method = e.b(this.d, this.c, this.b);
                        if (!method.isAccessible()) {
                            method.setAccessible(true);
                        }
                        hashMap.put(this.c, method);
                    }
                    cVar.b = method.invoke(this.d, this.f1132a);
                    cVar.f1133a = true;
                } catch (Exception e) {
                    c.a("Reflector", e);
                }
            }
            c.a("Reflector", "[Clz.invoke]:, mMethodName='" + this.c + ", \nmTypes=" + Arrays.toString(this.b) + ", \nmArgs=" + Arrays.toString(this.f1132a) + ", \nresult=" + cVar);
            return cVar;
        }
    }

    class b {
        private b() {
        }
    }

    public class c<T> {

        /* renamed from: a  reason: collision with root package name */
        public boolean f1133a = false;
        public T b;

        public String toString() {
            return "Result{ok=" + this.f1133a + ", value=" + ((Object) this.b) + '}';
        }
    }

    public static e a(Class<?> cls) {
        return new a(cls);
    }

    public static e a(String str) {
        Class<?> cls = d.get(str);
        if (cls == null) {
            try {
                cls = Class.forName(str);
                d.put(str, cls);
            } catch (ClassNotFoundException e2) {
                c.a("Reflector", e2);
            }
        }
        return a(cls);
    }

    private static boolean a(Method method, String str, Class<?>[] clsArr) {
        return method.getName().equals(str) && a(method.getParameterTypes(), clsArr);
    }

    private static boolean a(Class<?>[] clsArr, Class<?>[] clsArr2) {
        if (clsArr.length != clsArr2.length) {
            return false;
        }
        for (int i = 0; i < clsArr2.length; i++) {
            if (clsArr2[i] != b.class && !b(clsArr[i]).isAssignableFrom(b(clsArr2[i]))) {
                return false;
            }
        }
        return true;
    }

    public static Class<?> b(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        return cls.isPrimitive() ? Boolean.TYPE == cls ? Boolean.class : Integer.TYPE == cls ? Integer.class : Long.TYPE == cls ? Long.class : Short.TYPE == cls ? Short.class : Byte.TYPE == cls ? Byte.class : Double.TYPE == cls ? Double.class : Float.TYPE == cls ? Float.class : Character.TYPE == cls ? Character.class : Void.TYPE == cls ? Void.class : cls : cls;
    }

    /* access modifiers changed from: private */
    public static Method b(Class<?> cls, String str, Class... clsArr) {
        try {
            return cls.getMethod(str, clsArr);
        } catch (NoSuchMethodException e2) {
            try {
                return cls.getDeclaredMethod(str, clsArr);
            } catch (NoSuchMethodException e3) {
                return c(cls, str, clsArr);
            }
        }
    }

    private static Method c(Class<?> cls, String str, Class<?>[] clsArr) {
        Method method;
        int i = 0;
        Method[] methods = cls.getMethods();
        int length = methods.length;
        int i2 = 0;
        while (true) {
            if (i2 < length) {
                method = methods[i2];
                if (a(method, str, clsArr)) {
                    break;
                }
                i2++;
            } else {
                Method[] declaredMethods = cls.getDeclaredMethods();
                int length2 = declaredMethods.length;
                while (i < length2) {
                    method = declaredMethods[i];
                    if (!a(method, str, clsArr)) {
                        i++;
                    }
                }
                throw new NoSuchMethodException("No similar method " + str + " with params " + Arrays.toString(clsArr) + " could be found on type " + cls);
            }
        }
        return method;
    }

    /* access modifiers changed from: private */
    public static Class<?>[] c(Object[] objArr) {
        if (objArr == null) {
            return null;
        }
        Class<?>[] clsArr = new Class[objArr.length];
        for (int i = 0; i < objArr.length; i++) {
            clsArr[i] = objArr[i].getClass();
        }
        return clsArr;
    }

    public abstract <T> c<T> a();

    public e a(Object[] objArr) {
        this.f1132a = objArr;
        return this;
    }

    public e b(String str) {
        this.c = str;
        return this;
    }
}
