package org.apache.mina.filter.stream;

import java.io.IOException;
import java.io.InputStream;
import org.apache.mina.core.buffer.IoBuffer;

public class StreamWriteFilter extends AbstractStreamWriteFilter<InputStream> {
    /* access modifiers changed from: protected */
    public IoBuffer getNextBuffer(InputStream inputStream) throws IOException {
        byte[] bArr = new byte[getWriteBufferSize()];
        int i = 0;
        int i2 = 0;
        while (i2 < bArr.length && (i = inputStream.read(bArr, i2, bArr.length - i2)) != -1) {
            i2 += i;
        }
        if (i == -1 && i2 == 0) {
            return null;
        }
        return IoBuffer.wrap(bArr, 0, i2);
    }

    /* access modifiers changed from: protected */
    public Class<InputStream> getMessageClass() {
        return InputStream.class;
    }
}
