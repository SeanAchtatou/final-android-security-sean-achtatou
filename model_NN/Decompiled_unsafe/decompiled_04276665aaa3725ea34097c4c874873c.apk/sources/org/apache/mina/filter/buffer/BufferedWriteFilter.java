package org.apache.mina.filter.buffer;

import org.a.b;
import org.a.c;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.DefaultWriteRequest;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.util.LazyInitializedCacheMap;

public final class BufferedWriteFilter extends IoFilterAdapter {
    public static final int DEFAULT_BUFFER_SIZE = 8192;
    private int bufferSize;
    private final LazyInitializedCacheMap<IoSession, IoBuffer> buffersMap;
    private final b logger;

    public BufferedWriteFilter() {
        this(8192, null);
    }

    public BufferedWriteFilter(int i) {
        this(i, null);
    }

    public BufferedWriteFilter(int i, LazyInitializedCacheMap<IoSession, IoBuffer> lazyInitializedCacheMap) {
        this.logger = c.a(BufferedWriteFilter.class);
        this.bufferSize = 8192;
        this.bufferSize = i;
        if (lazyInitializedCacheMap == null) {
            this.buffersMap = new LazyInitializedCacheMap<>();
        } else {
            this.buffersMap = lazyInitializedCacheMap;
        }
    }

    public int getBufferSize() {
        return this.bufferSize;
    }

    public void setBufferSize(int i) {
        this.bufferSize = i;
    }

    public void filterWrite(IoFilter.NextFilter nextFilter, IoSession ioSession, WriteRequest writeRequest) throws Exception {
        Object message = writeRequest.getMessage();
        if (message instanceof IoBuffer) {
            write(ioSession, (IoBuffer) message);
            return;
        }
        throw new IllegalArgumentException("This filter should only buffer IoBuffer objects");
    }

    private void write(IoSession ioSession, IoBuffer ioBuffer) {
        write(ioSession, ioBuffer, this.buffersMap.putIfAbsent(ioSession, new IoBufferLazyInitializer(this.bufferSize)));
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void write(org.apache.mina.core.session.IoSession r4, org.apache.mina.core.buffer.IoBuffer r5, org.apache.mina.core.buffer.IoBuffer r6) {
        /*
            r3 = this;
            int r0 = r5.remaining()     // Catch:{ Throwable -> 0x003d }
            int r1 = r6.capacity()     // Catch:{ Throwable -> 0x003d }
            if (r0 < r1) goto L_0x001e
            org.apache.mina.core.filterchain.IoFilterChain r0 = r4.getFilterChain()     // Catch:{ Throwable -> 0x003d }
            org.apache.mina.core.filterchain.IoFilter$NextFilter r0 = r0.getNextFilter(r3)     // Catch:{ Throwable -> 0x003d }
            r3.internalFlush(r0, r4, r6)     // Catch:{ Throwable -> 0x003d }
            org.apache.mina.core.write.DefaultWriteRequest r1 = new org.apache.mina.core.write.DefaultWriteRequest     // Catch:{ Throwable -> 0x003d }
            r1.<init>(r5)     // Catch:{ Throwable -> 0x003d }
            r0.filterWrite(r4, r1)     // Catch:{ Throwable -> 0x003d }
        L_0x001d:
            return
        L_0x001e:
            int r1 = r6.limit()     // Catch:{ Throwable -> 0x003d }
            int r2 = r6.position()     // Catch:{ Throwable -> 0x003d }
            int r1 = r1 - r2
            if (r0 <= r1) goto L_0x0034
            org.apache.mina.core.filterchain.IoFilterChain r0 = r4.getFilterChain()     // Catch:{ Throwable -> 0x003d }
            org.apache.mina.core.filterchain.IoFilter$NextFilter r0 = r0.getNextFilter(r3)     // Catch:{ Throwable -> 0x003d }
            r3.internalFlush(r0, r4, r6)     // Catch:{ Throwable -> 0x003d }
        L_0x0034:
            monitor-enter(r6)     // Catch:{ Throwable -> 0x003d }
            r6.put(r5)     // Catch:{ all -> 0x003a }
            monitor-exit(r6)     // Catch:{ all -> 0x003a }
            goto L_0x001d
        L_0x003a:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x003a }
            throw r0     // Catch:{ Throwable -> 0x003d }
        L_0x003d:
            r0 = move-exception
            org.apache.mina.core.filterchain.IoFilterChain r1 = r4.getFilterChain()
            r1.fireExceptionCaught(r0)
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.filter.buffer.BufferedWriteFilter.write(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.core.buffer.IoBuffer):void");
    }

    private void internalFlush(IoFilter.NextFilter nextFilter, IoSession ioSession, IoBuffer ioBuffer) throws Exception {
        IoBuffer duplicate;
        synchronized (ioBuffer) {
            ioBuffer.flip();
            duplicate = ioBuffer.duplicate();
            ioBuffer.clear();
        }
        this.logger.b("Flushing buffer: {}", duplicate);
        nextFilter.filterWrite(ioSession, new DefaultWriteRequest(duplicate));
    }

    public void flush(IoSession ioSession) {
        try {
            internalFlush(ioSession.getFilterChain().getNextFilter(this), ioSession, this.buffersMap.get(ioSession));
        } catch (Throwable th) {
            ioSession.getFilterChain().fireExceptionCaught(th);
        }
    }

    private void free(IoSession ioSession) {
        IoBuffer remove = this.buffersMap.remove(ioSession);
        if (remove != null) {
            remove.free();
        }
    }

    public void exceptionCaught(IoFilter.NextFilter nextFilter, IoSession ioSession, Throwable th) throws Exception {
        free(ioSession);
        nextFilter.exceptionCaught(ioSession, th);
    }

    public void sessionClosed(IoFilter.NextFilter nextFilter, IoSession ioSession) throws Exception {
        free(ioSession);
        nextFilter.sessionClosed(ioSession);
    }
}
