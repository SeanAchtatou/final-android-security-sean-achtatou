package org.apache.mina.filter.logging;

import org.a.b;
import org.a.c;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;

public class LoggingFilter extends IoFilterAdapter {
    private LogLevel exceptionCaughtLevel;
    private final b logger;
    private LogLevel messageReceivedLevel;
    private LogLevel messageSentLevel;
    private final String name;
    private LogLevel sessionClosedLevel;
    private LogLevel sessionCreatedLevel;
    private LogLevel sessionIdleLevel;
    private LogLevel sessionOpenedLevel;

    public LoggingFilter() {
        this(LoggingFilter.class.getName());
    }

    public LoggingFilter(Class<?> cls) {
        this(cls.getName());
    }

    public LoggingFilter(String str) {
        this.exceptionCaughtLevel = LogLevel.WARN;
        this.messageSentLevel = LogLevel.INFO;
        this.messageReceivedLevel = LogLevel.INFO;
        this.sessionCreatedLevel = LogLevel.INFO;
        this.sessionOpenedLevel = LogLevel.INFO;
        this.sessionIdleLevel = LogLevel.INFO;
        this.sessionClosedLevel = LogLevel.INFO;
        if (str == null) {
            this.name = LoggingFilter.class.getName();
        } else {
            this.name = str;
        }
        this.logger = c.a(this.name);
    }

    public String getName() {
        return this.name;
    }

    private void log(LogLevel logLevel, String str, Throwable th) {
        switch (logLevel) {
            case TRACE:
                this.logger.a(str, th);
                return;
            case DEBUG:
                this.logger.b(str, th);
                return;
            case INFO:
                this.logger.c(str, th);
                return;
            case WARN:
                this.logger.d(str, th);
                return;
            case ERROR:
                this.logger.e(str, th);
                return;
            default:
                return;
        }
    }

    private void log(LogLevel logLevel, String str, Object obj) {
        switch (logLevel) {
            case TRACE:
                this.logger.a(str, obj);
                return;
            case DEBUG:
                this.logger.b(str, obj);
                return;
            case INFO:
                this.logger.c(str, obj);
                return;
            case WARN:
                this.logger.d(str, obj);
                return;
            case ERROR:
                this.logger.e(str, obj);
                return;
            default:
                return;
        }
    }

    private void log(LogLevel logLevel, String str) {
        switch (logLevel) {
            case TRACE:
                this.logger.a(str);
                return;
            case DEBUG:
                this.logger.b(str);
                return;
            case INFO:
                this.logger.c(str);
                return;
            case WARN:
                this.logger.d(str);
                return;
            case ERROR:
                this.logger.e(str);
                return;
            default:
                return;
        }
    }

    public void exceptionCaught(IoFilter.NextFilter nextFilter, IoSession ioSession, Throwable th) throws Exception {
        log(this.exceptionCaughtLevel, "EXCEPTION :", th);
        nextFilter.exceptionCaught(ioSession, th);
    }

    public void messageReceived(IoFilter.NextFilter nextFilter, IoSession ioSession, Object obj) throws Exception {
        log(this.messageReceivedLevel, "RECEIVED: {}", obj);
        nextFilter.messageReceived(ioSession, obj);
    }

    public void messageSent(IoFilter.NextFilter nextFilter, IoSession ioSession, WriteRequest writeRequest) throws Exception {
        log(this.messageSentLevel, "SENT: {}", writeRequest.getMessage());
        nextFilter.messageSent(ioSession, writeRequest);
    }

    public void sessionCreated(IoFilter.NextFilter nextFilter, IoSession ioSession) throws Exception {
        log(this.sessionCreatedLevel, "CREATED");
        nextFilter.sessionCreated(ioSession);
    }

    public void sessionOpened(IoFilter.NextFilter nextFilter, IoSession ioSession) throws Exception {
        log(this.sessionOpenedLevel, "OPENED");
        nextFilter.sessionOpened(ioSession);
    }

    public void sessionIdle(IoFilter.NextFilter nextFilter, IoSession ioSession, IdleStatus idleStatus) throws Exception {
        log(this.sessionIdleLevel, "IDLE");
        nextFilter.sessionIdle(ioSession, idleStatus);
    }

    public void sessionClosed(IoFilter.NextFilter nextFilter, IoSession ioSession) throws Exception {
        log(this.sessionClosedLevel, "CLOSED");
        nextFilter.sessionClosed(ioSession);
    }

    public void setExceptionCaughtLogLevel(LogLevel logLevel) {
        this.exceptionCaughtLevel = logLevel;
    }

    public LogLevel getExceptionCaughtLogLevel() {
        return this.exceptionCaughtLevel;
    }

    public void setMessageReceivedLogLevel(LogLevel logLevel) {
        this.messageReceivedLevel = logLevel;
    }

    public LogLevel getMessageReceivedLogLevel() {
        return this.messageReceivedLevel;
    }

    public void setMessageSentLogLevel(LogLevel logLevel) {
        this.messageSentLevel = logLevel;
    }

    public LogLevel getMessageSentLogLevel() {
        return this.messageSentLevel;
    }

    public void setSessionCreatedLogLevel(LogLevel logLevel) {
        this.sessionCreatedLevel = logLevel;
    }

    public LogLevel getSessionCreatedLogLevel() {
        return this.sessionCreatedLevel;
    }

    public void setSessionOpenedLogLevel(LogLevel logLevel) {
        this.sessionOpenedLevel = logLevel;
    }

    public LogLevel getSessionOpenedLogLevel() {
        return this.sessionOpenedLevel;
    }

    public void setSessionIdleLogLevel(LogLevel logLevel) {
        this.sessionIdleLevel = logLevel;
    }

    public LogLevel getSessionIdleLogLevel() {
        return this.sessionIdleLevel;
    }

    public void setSessionClosedLogLevel(LogLevel logLevel) {
        this.sessionClosedLevel = logLevel;
    }

    public LogLevel getSessionClosedLogLevel() {
        return this.sessionClosedLevel;
    }
}
