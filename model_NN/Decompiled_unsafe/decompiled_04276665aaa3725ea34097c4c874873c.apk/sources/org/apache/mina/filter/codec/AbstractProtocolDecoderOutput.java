package org.apache.mina.filter.codec;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class AbstractProtocolDecoderOutput implements ProtocolDecoderOutput {
    private final Queue<Object> messageQueue = new ConcurrentLinkedQueue();

    public Queue<Object> getMessageQueue() {
        return this.messageQueue;
    }

    public void write(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("message");
        }
        this.messageQueue.add(obj);
    }
}
