package org.apache.mina.proxy.utils;

import java.security.DigestException;
import java.security.MessageDigestSpi;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class MD4 extends MessageDigestSpi {
    private static final int A = 1732584193;
    private static final int B = -271733879;
    public static final int BYTE_BLOCK_LENGTH = 64;
    public static final int BYTE_DIGEST_LENGTH = 16;
    private static final int C = -1732584194;
    private static final int D = 271733878;

    /* renamed from: a  reason: collision with root package name */
    private int f3024a = A;
    private int b = B;
    private final byte[] buffer = new byte[64];
    private int c = C;
    private int d = D;
    private long msgLength;

    /* access modifiers changed from: protected */
    public int engineGetDigestLength() {
        return 16;
    }

    /* access modifiers changed from: protected */
    public void engineUpdate(byte b2) {
        int i = (int) (this.msgLength % 64);
        this.buffer[i] = b2;
        this.msgLength++;
        if (i == 63) {
            process(this.buffer, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void engineUpdate(byte[] bArr, int i, int i2) {
        int i3;
        int i4 = 0;
        int i5 = (int) (this.msgLength % 64);
        int i6 = 64 - i5;
        this.msgLength += (long) i2;
        if (i2 >= i6) {
            System.arraycopy(bArr, i, this.buffer, i5, i6);
            process(this.buffer, 0);
            i3 = i6;
            while ((i3 + 64) - 1 < i2) {
                process(bArr, i + i3);
                i3 += 64;
            }
        } else {
            i4 = i5;
            i3 = 0;
        }
        if (i3 < i2) {
            System.arraycopy(bArr, i + i3, this.buffer, i4, i2 - i3);
        }
    }

    /* access modifiers changed from: protected */
    public byte[] engineDigest() {
        byte[] pad = pad();
        engineUpdate(pad, 0, pad.length);
        byte[] bArr = {(byte) this.f3024a, (byte) (this.f3024a >>> 8), (byte) (this.f3024a >>> 16), (byte) (this.f3024a >>> 24), (byte) this.b, (byte) (this.b >>> 8), (byte) (this.b >>> 16), (byte) (this.b >>> 24), (byte) this.c, (byte) (this.c >>> 8), (byte) (this.c >>> 16), (byte) (this.c >>> 24), (byte) this.d, (byte) (this.d >>> 8), (byte) (this.d >>> 16), (byte) (this.d >>> 24)};
        engineReset();
        return bArr;
    }

    /* access modifiers changed from: protected */
    public int engineDigest(byte[] bArr, int i, int i2) throws DigestException {
        if (i < 0 || i + i2 >= bArr.length) {
            throw new DigestException("Wrong offset or not enough space to store the digest");
        }
        int min = Math.min(i2, 16);
        System.arraycopy(engineDigest(), 0, bArr, i, min);
        return min;
    }

    /* access modifiers changed from: protected */
    public void engineReset() {
        this.f3024a = A;
        this.b = B;
        this.c = C;
        this.d = D;
        this.msgLength = 0;
    }

    private byte[] pad() {
        int i = (int) (this.msgLength % 64);
        int i2 = i < 56 ? 64 - i : 128 - i;
        byte[] bArr = new byte[i2];
        bArr[0] = Byte.MIN_VALUE;
        long j = this.msgLength << 3;
        int i3 = i2 - 8;
        int i4 = 0;
        while (i4 < 8) {
            bArr[i3] = (byte) ((int) (j >>> (i4 << 3)));
            i4++;
            i3++;
        }
        return bArr;
    }

    private void process(byte[] bArr, int i) {
        int i2 = this.f3024a;
        int i3 = this.b;
        int i4 = this.c;
        int i5 = this.d;
        int[] iArr = new int[16];
        for (int i6 = 0; i6 < 16; i6++) {
            int i7 = i + 1;
            int i8 = i7 + 1;
            byte b2 = ((bArr[i7] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
            int i9 = i8 + 1;
            i = i9 + 1;
            iArr[i6] = b2 | ((bArr[i8] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr[i9] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24);
        }
        this.f3024a += ((this.b & this.c) | ((this.b ^ -1) & this.d)) + iArr[0];
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a & this.b) | ((this.f3024a ^ -1) & this.c)) + iArr[1];
        this.d = (this.d << 7) | (this.d >>> 25);
        this.c += ((this.d & this.f3024a) | ((this.d ^ -1) & this.b)) + iArr[2];
        this.c = (this.c << 11) | (this.c >>> 21);
        this.b += ((this.c & this.d) | ((this.c ^ -1) & this.f3024a)) + iArr[3];
        this.b = (this.b << 19) | (this.b >>> 13);
        this.f3024a += ((this.b & this.c) | ((this.b ^ -1) & this.d)) + iArr[4];
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a & this.b) | ((this.f3024a ^ -1) & this.c)) + iArr[5];
        this.d = (this.d << 7) | (this.d >>> 25);
        this.c += ((this.d & this.f3024a) | ((this.d ^ -1) & this.b)) + iArr[6];
        this.c = (this.c << 11) | (this.c >>> 21);
        this.b += ((this.c & this.d) | ((this.c ^ -1) & this.f3024a)) + iArr[7];
        this.b = (this.b << 19) | (this.b >>> 13);
        this.f3024a += ((this.b & this.c) | ((this.b ^ -1) & this.d)) + iArr[8];
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a & this.b) | ((this.f3024a ^ -1) & this.c)) + iArr[9];
        this.d = (this.d << 7) | (this.d >>> 25);
        this.c += ((this.d & this.f3024a) | ((this.d ^ -1) & this.b)) + iArr[10];
        this.c = (this.c << 11) | (this.c >>> 21);
        this.b += ((this.c & this.d) | ((this.c ^ -1) & this.f3024a)) + iArr[11];
        this.b = (this.b << 19) | (this.b >>> 13);
        this.f3024a += ((this.b & this.c) | ((this.b ^ -1) & this.d)) + iArr[12];
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a & this.b) | ((this.f3024a ^ -1) & this.c)) + iArr[13];
        this.d = (this.d << 7) | (this.d >>> 25);
        this.c += ((this.d & this.f3024a) | ((this.d ^ -1) & this.b)) + iArr[14];
        this.c = (this.c << 11) | (this.c >>> 21);
        this.b += ((this.c & this.d) | ((this.c ^ -1) & this.f3024a)) + iArr[15];
        this.b = (this.b << 19) | (this.b >>> 13);
        this.f3024a += ((this.b & (this.c | this.d)) | (this.c & this.d)) + iArr[0] + 1518500249;
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a & (this.b | this.c)) | (this.b & this.c)) + iArr[4] + 1518500249;
        this.d = (this.d << 5) | (this.d >>> 27);
        this.c += ((this.d & (this.f3024a | this.b)) | (this.f3024a & this.b)) + iArr[8] + 1518500249;
        this.c = (this.c << 9) | (this.c >>> 23);
        this.b += ((this.c & (this.d | this.f3024a)) | (this.d & this.f3024a)) + iArr[12] + 1518500249;
        this.b = (this.b << 13) | (this.b >>> 19);
        this.f3024a += ((this.b & (this.c | this.d)) | (this.c & this.d)) + iArr[1] + 1518500249;
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a & (this.b | this.c)) | (this.b & this.c)) + iArr[5] + 1518500249;
        this.d = (this.d << 5) | (this.d >>> 27);
        this.c += ((this.d & (this.f3024a | this.b)) | (this.f3024a & this.b)) + iArr[9] + 1518500249;
        this.c = (this.c << 9) | (this.c >>> 23);
        this.b += ((this.c & (this.d | this.f3024a)) | (this.d & this.f3024a)) + iArr[13] + 1518500249;
        this.b = (this.b << 13) | (this.b >>> 19);
        this.f3024a += ((this.b & (this.c | this.d)) | (this.c & this.d)) + iArr[2] + 1518500249;
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a & (this.b | this.c)) | (this.b & this.c)) + iArr[6] + 1518500249;
        this.d = (this.d << 5) | (this.d >>> 27);
        this.c += ((this.d & (this.f3024a | this.b)) | (this.f3024a & this.b)) + iArr[10] + 1518500249;
        this.c = (this.c << 9) | (this.c >>> 23);
        this.b += ((this.c & (this.d | this.f3024a)) | (this.d & this.f3024a)) + iArr[14] + 1518500249;
        this.b = (this.b << 13) | (this.b >>> 19);
        this.f3024a += ((this.b & (this.c | this.d)) | (this.c & this.d)) + iArr[3] + 1518500249;
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a & (this.b | this.c)) | (this.b & this.c)) + iArr[7] + 1518500249;
        this.d = (this.d << 5) | (this.d >>> 27);
        this.c += ((this.d & (this.f3024a | this.b)) | (this.f3024a & this.b)) + iArr[11] + 1518500249;
        this.c = (this.c << 9) | (this.c >>> 23);
        this.b += ((this.c & (this.d | this.f3024a)) | (this.d & this.f3024a)) + iArr[15] + 1518500249;
        this.b = (this.b << 13) | (this.b >>> 19);
        this.f3024a += ((this.b ^ this.c) ^ this.d) + iArr[0] + 1859775393;
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a ^ this.b) ^ this.c) + iArr[8] + 1859775393;
        this.d = (this.d << 9) | (this.d >>> 23);
        this.c += ((this.d ^ this.f3024a) ^ this.b) + iArr[4] + 1859775393;
        this.c = (this.c << 11) | (this.c >>> 21);
        this.b += ((this.c ^ this.d) ^ this.f3024a) + iArr[12] + 1859775393;
        this.b = (this.b << 15) | (this.b >>> 17);
        this.f3024a += ((this.b ^ this.c) ^ this.d) + iArr[2] + 1859775393;
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a ^ this.b) ^ this.c) + iArr[10] + 1859775393;
        this.d = (this.d << 9) | (this.d >>> 23);
        this.c += ((this.d ^ this.f3024a) ^ this.b) + iArr[6] + 1859775393;
        this.c = (this.c << 11) | (this.c >>> 21);
        this.b += ((this.c ^ this.d) ^ this.f3024a) + iArr[14] + 1859775393;
        this.b = (this.b << 15) | (this.b >>> 17);
        this.f3024a += ((this.b ^ this.c) ^ this.d) + iArr[1] + 1859775393;
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a ^ this.b) ^ this.c) + iArr[9] + 1859775393;
        this.d = (this.d << 9) | (this.d >>> 23);
        this.c += ((this.d ^ this.f3024a) ^ this.b) + iArr[5] + 1859775393;
        this.c = (this.c << 11) | (this.c >>> 21);
        this.b += ((this.c ^ this.d) ^ this.f3024a) + iArr[13] + 1859775393;
        this.b = (this.b << 15) | (this.b >>> 17);
        this.f3024a += ((this.b ^ this.c) ^ this.d) + iArr[3] + 1859775393;
        this.f3024a = (this.f3024a << 3) | (this.f3024a >>> 29);
        this.d += ((this.f3024a ^ this.b) ^ this.c) + iArr[11] + 1859775393;
        this.d = (this.d << 9) | (this.d >>> 23);
        this.c += ((this.d ^ this.f3024a) ^ this.b) + iArr[7] + 1859775393;
        this.c = (this.c << 11) | (this.c >>> 21);
        this.b += iArr[15] + ((this.c ^ this.d) ^ this.f3024a) + 1859775393;
        this.b = (this.b << 15) | (this.b >>> 17);
        this.f3024a += i2;
        this.b += i3;
        this.c += i4;
        this.d += i5;
    }
}
