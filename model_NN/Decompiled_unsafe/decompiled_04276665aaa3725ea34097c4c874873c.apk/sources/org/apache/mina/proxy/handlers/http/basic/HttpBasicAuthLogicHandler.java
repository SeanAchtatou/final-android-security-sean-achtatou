package org.apache.mina.proxy.handlers.http.basic;

import java.util.HashMap;
import java.util.Map;
import org.a.b;
import org.a.c;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.proxy.ProxyAuthException;
import org.apache.mina.proxy.handlers.http.AbstractAuthLogicHandler;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;
import org.apache.mina.proxy.handlers.http.HttpProxyRequest;
import org.apache.mina.proxy.handlers.http.HttpProxyResponse;
import org.apache.mina.proxy.session.ProxyIoSession;
import org.apache.mina.proxy.utils.StringUtilities;
import org.apache.mina.util.Base64;

public class HttpBasicAuthLogicHandler extends AbstractAuthLogicHandler {
    private static final b logger = c.a(HttpBasicAuthLogicHandler.class);

    public HttpBasicAuthLogicHandler(ProxyIoSession proxyIoSession) throws ProxyAuthException {
        super(proxyIoSession);
        ((HttpProxyRequest) this.request).checkRequiredProperties(HttpProxyConstants.USER_PROPERTY, HttpProxyConstants.PWD_PROPERTY);
    }

    /* JADX WARN: Type inference failed for: r0v6, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    public void doHandshake(IoFilter.NextFilter nextFilter) throws ProxyAuthException {
        logger.b(" doHandshake()");
        if (this.step > 0) {
            throw new ProxyAuthException("Authentication request already sent");
        }
        HttpProxyRequest httpProxyRequest = (HttpProxyRequest) this.request;
        Map headers = httpProxyRequest.getHeaders() != null ? httpProxyRequest.getHeaders() : new HashMap();
        StringUtilities.addValueToHeader(headers, "Proxy-Authorization", "Basic " + createAuthorization(httpProxyRequest.getProperties().get(HttpProxyConstants.USER_PROPERTY), httpProxyRequest.getProperties().get(HttpProxyConstants.PWD_PROPERTY)), true);
        addKeepAliveHeaders(headers);
        httpProxyRequest.setHeaders(headers);
        writeRequest(nextFilter, httpProxyRequest);
        this.step++;
    }

    public static String createAuthorization(String str, String str2) {
        return new String(Base64.encodeBase64((str + ":" + str2).getBytes()));
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    public void handleResponse(HttpProxyResponse httpProxyResponse) throws ProxyAuthException {
        if (httpProxyResponse.getStatusCode() != 407) {
            throw new ProxyAuthException("Received error response code (" + httpProxyResponse.getStatusLine() + ").");
        }
    }
}
