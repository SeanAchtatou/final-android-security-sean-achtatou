package org.apache.mina.proxy.handlers.http;

import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.a.b;
import org.a.c;
import org.apache.mina.proxy.ProxyAuthException;
import org.apache.mina.proxy.handlers.ProxyRequest;

public class HttpProxyRequest extends ProxyRequest {
    private static final b logger = c.a(HttpProxyRequest.class);
    private Map<String, List<String>> headers;
    private String host;
    public final String httpURI;
    public final String httpVerb;
    private String httpVersion;
    private transient Map<String, String> properties;

    public HttpProxyRequest(InetSocketAddress inetSocketAddress) {
        this(inetSocketAddress, HttpProxyConstants.HTTP_1_0, (Map<String, List<String>>) null);
    }

    public HttpProxyRequest(InetSocketAddress inetSocketAddress, String str) {
        this(inetSocketAddress, str, (Map<String, List<String>>) null);
    }

    public HttpProxyRequest(InetSocketAddress inetSocketAddress, String str, Map<String, List<String>> map) {
        this.httpVerb = HttpProxyConstants.CONNECT;
        if (!inetSocketAddress.isUnresolved()) {
            this.httpURI = inetSocketAddress.getHostName() + ":" + inetSocketAddress.getPort();
        } else {
            this.httpURI = inetSocketAddress.getAddress().getHostAddress() + ":" + inetSocketAddress.getPort();
        }
        this.httpVersion = str;
        this.headers = map;
    }

    public HttpProxyRequest(String str) {
        this("GET", str, HttpProxyConstants.HTTP_1_0, null);
    }

    public HttpProxyRequest(String str, String str2) {
        this("GET", str, str2, null);
    }

    public HttpProxyRequest(String str, String str2, String str3) {
        this(str, str2, str3, null);
    }

    public HttpProxyRequest(String str, String str2, String str3, Map<String, List<String>> map) {
        this.httpVerb = str;
        this.httpURI = str2;
        this.httpVersion = str3;
        this.headers = map;
    }

    public final String getHttpVerb() {
        return this.httpVerb;
    }

    public String getHttpVersion() {
        return this.httpVersion;
    }

    public void setHttpVersion(String str) {
        this.httpVersion = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.a.b.b(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.net.MalformedURLException]
     candidates:
      org.a.b.b(java.lang.String, java.lang.Object):void
      org.a.b.b(java.lang.String, java.lang.Throwable):void */
    public final synchronized String getHost() {
        if (this.host == null) {
            if (getEndpointAddress() != null && !getEndpointAddress().isUnresolved()) {
                this.host = getEndpointAddress().getHostName();
            }
            if (this.host == null && this.httpURI != null) {
                try {
                    this.host = new URL(this.httpURI).getHost();
                } catch (MalformedURLException e) {
                    logger.b("Malformed URL", (Throwable) e);
                }
            }
        }
        return this.host;
    }

    public final String getHttpURI() {
        return this.httpURI;
    }

    public final Map<String, List<String>> getHeaders() {
        return this.headers;
    }

    public final void setHeaders(Map<String, List<String>> map) {
        this.headers = map;
    }

    public Map<String, String> getProperties() {
        return this.properties;
    }

    public void setProperties(Map<String, String> map) {
        this.properties = map;
    }

    /* JADX WARN: Type inference failed for: r0v4, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    public void checkRequiredProperties(String... strArr) throws ProxyAuthException {
        StringBuilder sb = new StringBuilder();
        for (String str : strArr) {
            if (this.properties.get(str) == null) {
                sb.append(str).append(' ');
            }
        }
        if (sb.length() > 0) {
            sb.append("property(ies) missing in request");
            throw new ProxyAuthException(sb.toString());
        }
    }

    public String toHttpString() {
        boolean z;
        StringBuilder sb = new StringBuilder();
        sb.append(getHttpVerb()).append(' ').append(getHttpURI()).append(' ').append(getHttpVersion()).append(HttpProxyConstants.CRLF);
        if (getHeaders() != null) {
            boolean z2 = false;
            for (Map.Entry next : getHeaders().entrySet()) {
                if (!z2) {
                    z = ((String) next.getKey()).equalsIgnoreCase("host");
                } else {
                    z = z2;
                }
                for (String append : (List) next.getValue()) {
                    sb.append((String) next.getKey()).append(": ").append(append).append(HttpProxyConstants.CRLF);
                }
                z2 = z;
            }
            if (!z2 && getHttpVersion() == HttpProxyConstants.HTTP_1_1) {
                sb.append("Host: ").append(getHost()).append(HttpProxyConstants.CRLF);
            }
        }
        sb.append(HttpProxyConstants.CRLF);
        return sb.toString();
    }
}
