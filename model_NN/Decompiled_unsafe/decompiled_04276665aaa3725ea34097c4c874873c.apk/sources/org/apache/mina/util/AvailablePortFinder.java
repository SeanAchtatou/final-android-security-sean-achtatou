package org.apache.mina.util;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.NoSuchElementException;
import java.util.Set;

public class AvailablePortFinder {
    public static final int MAX_PORT_NUMBER = 49151;
    public static final int MIN_PORT_NUMBER = 1;

    private AvailablePortFinder() {
    }

    public static Set<Integer> getAvailablePorts() {
        return getAvailablePorts(1, MAX_PORT_NUMBER);
    }

    public static int getNextAvailable() {
        try {
            ServerSocket serverSocket = new ServerSocket(0);
            int localPort = serverSocket.getLocalPort();
            serverSocket.close();
            return localPort;
        } catch (IOException e) {
            throw new NoSuchElementException(e.getMessage());
        }
    }

    public static int getNextAvailable(int i) {
        if (i < 1 || i > 49151) {
            throw new IllegalArgumentException("Invalid start port: " + i);
        }
        for (int i2 = i; i2 <= 49151; i2++) {
            if (available(i2)) {
                return i2;
            }
        }
        throw new NoSuchElementException("Could not find an available port above " + i);
    }

    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.net.DatagramSocket] */
    /* JADX WARN: Type inference failed for: r2v3, types: [java.net.ServerSocket] */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0048 A[SYNTHETIC, Splitter:B:24:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0056 A[SYNTHETIC, Splitter:B:32:0x0056] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean available(int r4) {
        /*
            r2 = 0
            r0 = 1
            if (r4 < r0) goto L_0x0009
            r1 = 49151(0xbfff, float:6.8875E-41)
            if (r4 <= r1) goto L_0x0022
        L_0x0009:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Invalid start port: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0022:
            java.net.ServerSocket r3 = new java.net.ServerSocket     // Catch:{ IOException -> 0x003f, all -> 0x004d }
            r3.<init>(r4)     // Catch:{ IOException -> 0x003f, all -> 0x004d }
            r1 = 1
            r3.setReuseAddress(r1)     // Catch:{ IOException -> 0x0065, all -> 0x0060 }
            java.net.DatagramSocket r1 = new java.net.DatagramSocket     // Catch:{ IOException -> 0x0065, all -> 0x0060 }
            r1.<init>(r4)     // Catch:{ IOException -> 0x0065, all -> 0x0060 }
            r2 = 1
            r1.setReuseAddress(r2)     // Catch:{ IOException -> 0x0069, all -> 0x0062 }
            if (r1 == 0) goto L_0x0039
            r1.close()
        L_0x0039:
            if (r3 == 0) goto L_0x003e
            r3.close()     // Catch:{ IOException -> 0x005a }
        L_0x003e:
            return r0
        L_0x003f:
            r0 = move-exception
            r0 = r2
        L_0x0041:
            if (r0 == 0) goto L_0x0046
            r0.close()
        L_0x0046:
            if (r2 == 0) goto L_0x004b
            r2.close()     // Catch:{ IOException -> 0x005c }
        L_0x004b:
            r0 = 0
            goto L_0x003e
        L_0x004d:
            r0 = move-exception
            r3 = r2
        L_0x004f:
            if (r2 == 0) goto L_0x0054
            r2.close()
        L_0x0054:
            if (r3 == 0) goto L_0x0059
            r3.close()     // Catch:{ IOException -> 0x005e }
        L_0x0059:
            throw r0
        L_0x005a:
            r1 = move-exception
            goto L_0x003e
        L_0x005c:
            r0 = move-exception
            goto L_0x004b
        L_0x005e:
            r1 = move-exception
            goto L_0x0059
        L_0x0060:
            r0 = move-exception
            goto L_0x004f
        L_0x0062:
            r0 = move-exception
            r2 = r1
            goto L_0x004f
        L_0x0065:
            r0 = move-exception
            r0 = r2
            r2 = r3
            goto L_0x0041
        L_0x0069:
            r0 = move-exception
            r0 = r1
            r2 = r3
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.util.AvailablePortFinder.available(int):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x004e A[SYNTHETIC, Splitter:B:21:0x004e] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0057 A[SYNTHETIC, Splitter:B:26:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0047 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.Set<java.lang.Integer> getAvailablePorts(int r4, int r5) {
        /*
            r0 = 1
            if (r4 < r0) goto L_0x000a
            r0 = 49151(0xbfff, float:6.8875E-41)
            if (r5 > r0) goto L_0x000a
            if (r4 <= r5) goto L_0x002d
        L_0x000a:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Invalid port range: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r2 = " ~ "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x002d:
            java.util.TreeSet r2 = new java.util.TreeSet
            r2.<init>()
        L_0x0032:
            if (r4 > r5) goto L_0x005f
            r1 = 0
            java.net.ServerSocket r0 = new java.net.ServerSocket     // Catch:{ IOException -> 0x004a, all -> 0x0054 }
            r0.<init>(r4)     // Catch:{ IOException -> 0x004a, all -> 0x0054 }
            java.lang.Integer r1 = new java.lang.Integer     // Catch:{ IOException -> 0x0065, all -> 0x0060 }
            r1.<init>(r4)     // Catch:{ IOException -> 0x0065, all -> 0x0060 }
            r2.add(r1)     // Catch:{ IOException -> 0x0065, all -> 0x0060 }
            if (r0 == 0) goto L_0x0047
            r0.close()     // Catch:{ IOException -> 0x005b }
        L_0x0047:
            int r4 = r4 + 1
            goto L_0x0032
        L_0x004a:
            r0 = move-exception
            r0 = r1
        L_0x004c:
            if (r0 == 0) goto L_0x0047
            r0.close()     // Catch:{ IOException -> 0x0052 }
            goto L_0x0047
        L_0x0052:
            r0 = move-exception
            goto L_0x0047
        L_0x0054:
            r0 = move-exception
        L_0x0055:
            if (r1 == 0) goto L_0x005a
            r1.close()     // Catch:{ IOException -> 0x005d }
        L_0x005a:
            throw r0
        L_0x005b:
            r0 = move-exception
            goto L_0x0047
        L_0x005d:
            r1 = move-exception
            goto L_0x005a
        L_0x005f:
            return r2
        L_0x0060:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0055
        L_0x0065:
            r1 = move-exception
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.util.AvailablePortFinder.getAvailablePorts(int, int):java.util.Set");
    }
}
