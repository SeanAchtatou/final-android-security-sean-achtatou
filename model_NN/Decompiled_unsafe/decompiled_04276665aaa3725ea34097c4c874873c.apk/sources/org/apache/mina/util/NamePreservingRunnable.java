package org.apache.mina.util;

import org.a.b;
import org.a.c;

public class NamePreservingRunnable implements Runnable {
    private static final b LOGGER = c.a(NamePreservingRunnable.class);
    private final String newName;
    private final Runnable runnable;

    public NamePreservingRunnable(Runnable runnable2, String str) {
        this.runnable = runnable2;
        this.newName = str;
    }

    public void run() {
        Thread currentThread = Thread.currentThread();
        String name = currentThread.getName();
        if (this.newName != null) {
            setName(currentThread, this.newName);
        }
        try {
            this.runnable.run();
        } finally {
            setName(currentThread, name);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.a.b.d(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.SecurityException]
     candidates:
      org.a.b.d(java.lang.String, java.lang.Object):void
      org.a.b.d(java.lang.String, java.lang.Throwable):void */
    private void setName(Thread thread, String str) {
        try {
            thread.setName(str);
        } catch (SecurityException e) {
            if (LOGGER.b()) {
                LOGGER.d("Failed to set the thread name.", (Throwable) e);
            }
        }
    }
}
