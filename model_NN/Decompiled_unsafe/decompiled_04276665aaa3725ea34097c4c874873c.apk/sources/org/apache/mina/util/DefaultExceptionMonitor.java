package org.apache.mina.util;

import org.a.b;
import org.a.c;

public class DefaultExceptionMonitor extends ExceptionMonitor {
    private static final b LOGGER = c.a(DefaultExceptionMonitor.class);

    public void exceptionCaught(Throwable th) {
        if (th instanceof Error) {
            throw ((Error) th);
        }
        LOGGER.d("Unexpected exception.", th);
    }
}
