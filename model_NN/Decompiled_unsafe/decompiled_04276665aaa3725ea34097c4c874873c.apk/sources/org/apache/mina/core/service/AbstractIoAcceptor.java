package org.apache.mina.core.service;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import org.apache.mina.core.RuntimeIoException;
import org.apache.mina.core.service.AbstractIoService;
import org.apache.mina.core.session.IoSessionConfig;

public abstract class AbstractIoAcceptor extends AbstractIoService implements IoAcceptor {
    protected final Object bindLock = new Object();
    private final Set<SocketAddress> boundAddresses = new HashSet();
    private final List<SocketAddress> defaultLocalAddresses = new ArrayList();
    private boolean disconnectOnUnbind = true;
    private final List<SocketAddress> unmodifiableDefaultLocalAddresses = Collections.unmodifiableList(this.defaultLocalAddresses);

    /* access modifiers changed from: protected */
    public abstract Set<SocketAddress> bindInternal(List<? extends SocketAddress> list) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void unbind0(List<? extends SocketAddress> list) throws Exception;

    protected AbstractIoAcceptor(IoSessionConfig ioSessionConfig, Executor executor) {
        super(ioSessionConfig, executor);
        this.defaultLocalAddresses.add(null);
    }

    public SocketAddress getLocalAddress() {
        Set<SocketAddress> localAddresses = getLocalAddresses();
        if (localAddresses.isEmpty()) {
            return null;
        }
        return localAddresses.iterator().next();
    }

    public final Set<SocketAddress> getLocalAddresses() {
        HashSet hashSet = new HashSet();
        synchronized (this.boundAddresses) {
            hashSet.addAll(this.boundAddresses);
        }
        return hashSet;
    }

    public SocketAddress getDefaultLocalAddress() {
        if (this.defaultLocalAddresses.isEmpty()) {
            return null;
        }
        return this.defaultLocalAddresses.iterator().next();
    }

    public final void setDefaultLocalAddress(SocketAddress socketAddress) {
        setDefaultLocalAddresses(socketAddress, new SocketAddress[0]);
    }

    public final List<SocketAddress> getDefaultLocalAddresses() {
        return this.unmodifiableDefaultLocalAddresses;
    }

    public final void setDefaultLocalAddresses(List<? extends SocketAddress> list) {
        if (list == null) {
            throw new IllegalArgumentException("localAddresses");
        }
        setDefaultLocalAddresses((Iterable<? extends SocketAddress>) list);
    }

    public final void setDefaultLocalAddresses(Iterable<? extends SocketAddress> iterable) {
        if (iterable == null) {
            throw new IllegalArgumentException("localAddresses");
        }
        synchronized (this.bindLock) {
            synchronized (this.boundAddresses) {
                if (!this.boundAddresses.isEmpty()) {
                    throw new IllegalStateException("localAddress can't be set while the acceptor is bound.");
                }
                ArrayList arrayList = new ArrayList();
                for (SocketAddress socketAddress : iterable) {
                    checkAddressType(socketAddress);
                    arrayList.add(socketAddress);
                }
                if (arrayList.isEmpty()) {
                    throw new IllegalArgumentException("empty localAddresses");
                }
                this.defaultLocalAddresses.clear();
                this.defaultLocalAddresses.addAll(arrayList);
            }
        }
    }

    public final void setDefaultLocalAddresses(SocketAddress socketAddress, SocketAddress... socketAddressArr) {
        if (socketAddressArr == null) {
            socketAddressArr = new SocketAddress[0];
        }
        ArrayList arrayList = new ArrayList(socketAddressArr.length + 1);
        arrayList.add(socketAddress);
        for (SocketAddress add : socketAddressArr) {
            arrayList.add(add);
        }
        setDefaultLocalAddresses((Iterable<? extends SocketAddress>) arrayList);
    }

    public final boolean isCloseOnDeactivation() {
        return this.disconnectOnUnbind;
    }

    public final void setCloseOnDeactivation(boolean z) {
        this.disconnectOnUnbind = z;
    }

    public final void bind() throws IOException {
        bind(getDefaultLocalAddresses());
    }

    public final void bind(SocketAddress socketAddress) throws IOException {
        if (socketAddress == null) {
            throw new IllegalArgumentException("localAddress");
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(socketAddress);
        bind(arrayList);
    }

    public final void bind(SocketAddress... socketAddressArr) throws IOException {
        if (socketAddressArr == null || socketAddressArr.length == 0) {
            bind(getDefaultLocalAddresses());
            return;
        }
        ArrayList arrayList = new ArrayList(2);
        for (SocketAddress add : socketAddressArr) {
            arrayList.add(add);
        }
        bind(arrayList);
    }

    public final void bind(SocketAddress socketAddress, SocketAddress... socketAddressArr) throws IOException {
        if (socketAddress == null) {
            bind(getDefaultLocalAddresses());
        }
        if (socketAddressArr == null || socketAddressArr.length == 0) {
            bind(getDefaultLocalAddresses());
            return;
        }
        ArrayList arrayList = new ArrayList(2);
        arrayList.add(socketAddress);
        for (SocketAddress add : socketAddressArr) {
            arrayList.add(add);
        }
        bind(arrayList);
    }

    public final void bind(Iterable<? extends SocketAddress> iterable) throws IOException {
        if (isDisposing()) {
            throw new IllegalStateException("Already disposed.");
        } else if (iterable == null) {
            throw new IllegalArgumentException("localAddresses");
        } else {
            ArrayList arrayList = new ArrayList();
            for (SocketAddress socketAddress : iterable) {
                checkAddressType(socketAddress);
                arrayList.add(socketAddress);
            }
            if (arrayList.isEmpty()) {
                throw new IllegalArgumentException("localAddresses is empty.");
            }
            boolean z = false;
            synchronized (this.bindLock) {
                synchronized (this.boundAddresses) {
                    if (this.boundAddresses.isEmpty()) {
                        z = true;
                    }
                }
                if (getHandler() == null) {
                    throw new IllegalStateException("handler is not set.");
                }
                try {
                    Set<SocketAddress> bindInternal = bindInternal(arrayList);
                    synchronized (this.boundAddresses) {
                        this.boundAddresses.addAll(bindInternal);
                    }
                } catch (IOException e) {
                    throw e;
                } catch (RuntimeException e2) {
                    throw e2;
                } catch (Throwable th) {
                    throw new RuntimeIoException("Failed to bind to: " + getLocalAddresses(), th);
                }
            }
            if (z) {
                getListeners().fireServiceActivated();
            }
        }
    }

    public final void unbind() {
        unbind(getLocalAddresses());
    }

    public final void unbind(SocketAddress socketAddress) {
        if (socketAddress == null) {
            throw new IllegalArgumentException("localAddress");
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(socketAddress);
        unbind(arrayList);
    }

    public final void unbind(SocketAddress socketAddress, SocketAddress... socketAddressArr) {
        if (socketAddress == null) {
            throw new IllegalArgumentException("firstLocalAddress");
        } else if (socketAddressArr == null) {
            throw new IllegalArgumentException("otherLocalAddresses");
        } else {
            ArrayList arrayList = new ArrayList();
            arrayList.add(socketAddress);
            Collections.addAll(arrayList, socketAddressArr);
            unbind(arrayList);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x006b, code lost:
        if (r0 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x006d, code lost:
        getListeners().fireServiceDeactivated();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void unbind(java.lang.Iterable<? extends java.net.SocketAddress> r9) {
        /*
            r8 = this;
            r1 = 0
            if (r9 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "localAddresses"
            r0.<init>(r1)
            throw r0
        L_0x000b:
            java.lang.Object r3 = r8.bindLock
            monitor-enter(r3)
            java.util.Set<java.net.SocketAddress> r4 = r8.boundAddresses     // Catch:{ all -> 0x0045 }
            monitor-enter(r4)     // Catch:{ all -> 0x0045 }
            java.util.Set<java.net.SocketAddress> r0 = r8.boundAddresses     // Catch:{ all -> 0x0042 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x001c
            monitor-exit(r4)     // Catch:{ all -> 0x0042 }
            monitor-exit(r3)     // Catch:{ all -> 0x0045 }
        L_0x001b:
            return
        L_0x001c:
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x0042 }
            r5.<init>()     // Catch:{ all -> 0x0042 }
            java.util.Iterator r6 = r9.iterator()     // Catch:{ all -> 0x0042 }
            r2 = r1
        L_0x0026:
            boolean r0 = r6.hasNext()     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x0048
            java.lang.Object r0 = r6.next()     // Catch:{ all -> 0x0042 }
            java.net.SocketAddress r0 = (java.net.SocketAddress) r0     // Catch:{ all -> 0x0042 }
            int r2 = r2 + 1
            if (r0 == 0) goto L_0x0026
            java.util.Set<java.net.SocketAddress> r7 = r8.boundAddresses     // Catch:{ all -> 0x0042 }
            boolean r7 = r7.contains(r0)     // Catch:{ all -> 0x0042 }
            if (r7 == 0) goto L_0x0026
            r5.add(r0)     // Catch:{ all -> 0x0042 }
            goto L_0x0026
        L_0x0042:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0042 }
            throw r0     // Catch:{ all -> 0x0045 }
        L_0x0045:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0045 }
            throw r0
        L_0x0048:
            if (r2 != 0) goto L_0x0052
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0042 }
            java.lang.String r1 = "localAddresses is empty."
            r0.<init>(r1)     // Catch:{ all -> 0x0042 }
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x0052:
            boolean r0 = r5.isEmpty()     // Catch:{ all -> 0x0042 }
            if (r0 != 0) goto L_0x0095
            r8.unbind0(r5)     // Catch:{ RuntimeException -> 0x0075, Throwable -> 0x0077 }
            java.util.Set<java.net.SocketAddress> r0 = r8.boundAddresses     // Catch:{ all -> 0x0042 }
            r0.removeAll(r5)     // Catch:{ all -> 0x0042 }
            java.util.Set<java.net.SocketAddress> r0 = r8.boundAddresses     // Catch:{ all -> 0x0042 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0042 }
            if (r0 == 0) goto L_0x0095
            r0 = 1
        L_0x0069:
            monitor-exit(r4)     // Catch:{ all -> 0x0042 }
            monitor-exit(r3)     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x001b
            org.apache.mina.core.service.IoServiceListenerSupport r0 = r8.getListeners()
            r0.fireServiceDeactivated()
            goto L_0x001b
        L_0x0075:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0042 }
        L_0x0077:
            r0 = move-exception
            org.apache.mina.core.RuntimeIoException r1 = new org.apache.mina.core.RuntimeIoException     // Catch:{ all -> 0x0042 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0042 }
            r2.<init>()     // Catch:{ all -> 0x0042 }
            java.lang.String r5 = "Failed to unbind from: "
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ all -> 0x0042 }
            java.util.Set r5 = r8.getLocalAddresses()     // Catch:{ all -> 0x0042 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ all -> 0x0042 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0042 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0042 }
            throw r1     // Catch:{ all -> 0x0042 }
        L_0x0095:
            r0 = r1
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.service.AbstractIoAcceptor.unbind(java.lang.Iterable):void");
    }

    public String toString() {
        TransportMetadata transportMetadata = getTransportMetadata();
        return '(' + transportMetadata.getProviderName() + ' ' + transportMetadata.getName() + " acceptor: " + (isActive() ? "localAddress(es): " + getLocalAddresses() + ", managedSessionCount: " + getManagedSessionCount() : "not bound") + ')';
    }

    private void checkAddressType(SocketAddress socketAddress) {
        if (socketAddress != null && !getTransportMetadata().getAddressType().isAssignableFrom(socketAddress.getClass())) {
            throw new IllegalArgumentException("localAddress type: " + socketAddress.getClass().getSimpleName() + " (expected: " + getTransportMetadata().getAddressType().getSimpleName() + ")");
        }
    }

    public static class AcceptorOperationFuture extends AbstractIoService.ServiceOperationFuture {
        private final List<SocketAddress> localAddresses;

        public AcceptorOperationFuture(List<? extends SocketAddress> list) {
            this.localAddresses = new ArrayList(list);
        }

        public final List<SocketAddress> getLocalAddresses() {
            return Collections.unmodifiableList(this.localAddresses);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Acceptor operation : ");
            if (this.localAddresses != null) {
                boolean z = true;
                for (SocketAddress next : this.localAddresses) {
                    if (z) {
                        z = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append(next);
                }
            }
            return sb.toString();
        }
    }
}
