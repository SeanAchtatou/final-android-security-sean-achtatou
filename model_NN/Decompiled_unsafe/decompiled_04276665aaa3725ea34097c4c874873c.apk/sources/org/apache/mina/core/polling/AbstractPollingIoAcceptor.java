package org.apache.mina.core.polling;

import java.net.SocketAddress;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.mina.core.RuntimeIoException;
import org.apache.mina.core.service.AbstractIoAcceptor;
import org.apache.mina.core.service.AbstractIoService;
import org.apache.mina.core.service.IoProcessor;
import org.apache.mina.core.service.SimpleIoProcessorPool;
import org.apache.mina.core.session.AbstractIoSession;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.session.IoSessionConfig;
import org.apache.mina.util.ExceptionMonitor;

public abstract class AbstractPollingIoAcceptor<S extends AbstractIoSession, H> extends AbstractIoAcceptor {
    /* access modifiers changed from: private */
    public AtomicReference<AbstractPollingIoAcceptor<S, H>.Acceptor> acceptorRef;
    protected int backlog;
    private final Map<SocketAddress, H> boundHandles;
    /* access modifiers changed from: private */
    public final Queue<AbstractIoAcceptor.AcceptorOperationFuture> cancelQueue;
    /* access modifiers changed from: private */
    public final boolean createdProcessor;
    /* access modifiers changed from: private */
    public final AbstractIoService.ServiceOperationFuture disposalFuture;
    /* access modifiers changed from: private */
    public final Semaphore lock;
    /* access modifiers changed from: private */
    public final IoProcessor<S> processor;
    /* access modifiers changed from: private */
    public final Queue<AbstractIoAcceptor.AcceptorOperationFuture> registerQueue;
    protected boolean reuseAddress;
    /* access modifiers changed from: private */
    public volatile boolean selectable;

    /* access modifiers changed from: protected */
    public abstract S accept(IoProcessor<S> ioProcessor, H h) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void close(H h) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void destroy() throws Exception;

    /* access modifiers changed from: protected */
    public abstract void init() throws Exception;

    /* access modifiers changed from: protected */
    public abstract SocketAddress localAddress(H h) throws Exception;

    /* access modifiers changed from: protected */
    public abstract H open(SocketAddress socketAddress) throws Exception;

    /* access modifiers changed from: protected */
    public abstract int select() throws Exception;

    /* access modifiers changed from: protected */
    public abstract Iterator<H> selectedHandles();

    /* access modifiers changed from: protected */
    public abstract void wakeup();

    protected AbstractPollingIoAcceptor(IoSessionConfig ioSessionConfig, Class<? extends IoProcessor<S>> cls) {
        this(ioSessionConfig, null, new SimpleIoProcessorPool(cls), true);
    }

    protected AbstractPollingIoAcceptor(IoSessionConfig ioSessionConfig, Class<? extends IoProcessor<S>> cls, int i) {
        this(ioSessionConfig, null, new SimpleIoProcessorPool(cls, i), true);
    }

    protected AbstractPollingIoAcceptor(IoSessionConfig ioSessionConfig, IoProcessor<S> ioProcessor) {
        this(ioSessionConfig, null, ioProcessor, false);
    }

    protected AbstractPollingIoAcceptor(IoSessionConfig ioSessionConfig, Executor executor, IoProcessor<S> ioProcessor) {
        this(ioSessionConfig, executor, ioProcessor, false);
    }

    private AbstractPollingIoAcceptor(IoSessionConfig ioSessionConfig, Executor executor, IoProcessor<S> ioProcessor, boolean z) {
        super(ioSessionConfig, executor);
        this.lock = new Semaphore(1);
        this.registerQueue = new ConcurrentLinkedQueue();
        this.cancelQueue = new ConcurrentLinkedQueue();
        this.boundHandles = Collections.synchronizedMap(new HashMap());
        this.disposalFuture = new AbstractIoService.ServiceOperationFuture();
        this.acceptorRef = new AtomicReference<>();
        this.reuseAddress = false;
        this.backlog = 50;
        if (ioProcessor == null) {
            throw new IllegalArgumentException("processor");
        }
        this.processor = ioProcessor;
        this.createdProcessor = z;
        try {
            init();
            this.selectable = true;
            if (!this.selectable) {
                try {
                    destroy();
                } catch (Exception e) {
                    ExceptionMonitor.getInstance().exceptionCaught(e);
                }
            }
        } catch (RuntimeException e2) {
            throw e2;
        } catch (Exception e3) {
            throw new RuntimeIoException("Failed to initialize.", e3);
        } catch (Throwable th) {
            if (!this.selectable) {
                try {
                    destroy();
                } catch (Exception e4) {
                    ExceptionMonitor.getInstance().exceptionCaught(e4);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void dispose0() throws Exception {
        unbind();
        startupAcceptor();
        wakeup();
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public final Set<SocketAddress> bindInternal(List<? extends SocketAddress> list) throws Exception {
        AbstractIoAcceptor.AcceptorOperationFuture acceptorOperationFuture = new AbstractIoAcceptor.AcceptorOperationFuture(list);
        this.registerQueue.add(acceptorOperationFuture);
        startupAcceptor();
        try {
            this.lock.acquire();
            Thread.sleep(10);
            wakeup();
            this.lock.release();
            acceptorOperationFuture.awaitUninterruptibly();
            if (acceptorOperationFuture.getException() != null) {
                throw acceptorOperationFuture.getException();
            }
            HashSet hashSet = new HashSet();
            for (H localAddress : this.boundHandles.values()) {
                hashSet.add(localAddress(localAddress));
            }
            return hashSet;
        } catch (Throwable th) {
            this.lock.release();
            throw th;
        }
    }

    private void startupAcceptor() throws InterruptedException {
        if (!this.selectable) {
            this.registerQueue.clear();
            this.cancelQueue.clear();
        }
        if (this.acceptorRef.get() == null) {
            this.lock.acquire();
            Acceptor acceptor = new Acceptor();
            if (this.acceptorRef.compareAndSet(null, acceptor)) {
                executeWorker(acceptor);
            } else {
                this.lock.release();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void unbind0(List<? extends SocketAddress> list) throws Exception {
        AbstractIoAcceptor.AcceptorOperationFuture acceptorOperationFuture = new AbstractIoAcceptor.AcceptorOperationFuture(list);
        this.cancelQueue.add(acceptorOperationFuture);
        startupAcceptor();
        wakeup();
        acceptorOperationFuture.awaitUninterruptibly();
        if (acceptorOperationFuture.getException() != null) {
            throw acceptorOperationFuture.getException();
        }
    }

    private class Acceptor implements Runnable {
        static final /* synthetic */ boolean $assertionsDisabled = (!AbstractPollingIoAcceptor.class.desiredAssertionStatus());

        private Acceptor() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:105:0x0180, code lost:
            r2 = th;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0070 A[ExcHandler: ClosedSelectorException (e java.nio.channels.ClosedSelectorException), Splitter:B:9:0x0029] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r6 = this;
                r1 = 0
                boolean r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.Acceptor.$assertionsDisabled
                if (r0 != 0) goto L_0x0017
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                java.util.concurrent.atomic.AtomicReference r0 = r0.acceptorRef
                java.lang.Object r0 = r0.get()
                if (r0 == r6) goto L_0x0017
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x0017:
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                java.util.concurrent.Semaphore r0 = r0.lock
                r0.release()
                r0 = r1
            L_0x0021:
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r2 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                boolean r2 = r2.selectable
                if (r2 == 0) goto L_0x0071
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r2 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x0180 }
                int r3 = r2.select()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x0180 }
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r2 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x0180 }
                int r2 = r2.registerHandles()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x0180 }
                int r2 = r2 + r0
                if (r2 != 0) goto L_0x010b
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                java.util.concurrent.atomic.AtomicReference r0 = r0.acceptorRef     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                r4 = 0
                r0.set(r4)     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                java.util.Queue r0 = r0.registerQueue     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                boolean r0 = r0.isEmpty()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                if (r0 == 0) goto L_0x00b6
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                java.util.Queue r0 = r0.cancelQueue     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                boolean r0 = r0.isEmpty()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                if (r0 == 0) goto L_0x00b6
                boolean r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.Acceptor.$assertionsDisabled     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                if (r0 != 0) goto L_0x0071
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                java.util.concurrent.atomic.AtomicReference r0 = r0.acceptorRef     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                java.lang.Object r0 = r0.get()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                if (r0 != r6) goto L_0x0071
                java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                r0.<init>()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                throw r0     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
            L_0x0070:
                r0 = move-exception
            L_0x0071:
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                boolean r0 = r0.selectable
                if (r0 == 0) goto L_0x00b5
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                boolean r0 = r0.isDisposing()
                if (r0 == 0) goto L_0x00b5
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                boolean unused = r0.selectable = r1
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ all -> 0x0140 }
                boolean r0 = r0.createdProcessor     // Catch:{ all -> 0x0140 }
                if (r0 == 0) goto L_0x0097
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ all -> 0x0140 }
                org.apache.mina.core.service.IoProcessor r0 = r0.processor     // Catch:{ all -> 0x0140 }
                r0.dispose()     // Catch:{ all -> 0x0140 }
            L_0x0097:
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ Exception -> 0x0123 }
                java.lang.Object r1 = r0.disposalLock     // Catch:{ Exception -> 0x0123 }
                monitor-enter(r1)     // Catch:{ Exception -> 0x0123 }
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ all -> 0x0120 }
                boolean r0 = r0.isDisposing()     // Catch:{ all -> 0x0120 }
                if (r0 == 0) goto L_0x00ab
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ all -> 0x0120 }
                r0.destroy()     // Catch:{ all -> 0x0120 }
            L_0x00ab:
                monitor-exit(r1)     // Catch:{ all -> 0x0120 }
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r0 = r0.disposalFuture
                r0.setDone()
            L_0x00b5:
                return
            L_0x00b6:
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                java.util.concurrent.atomic.AtomicReference r0 = r0.acceptorRef     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                r4 = 0
                boolean r0 = r0.compareAndSet(r4, r6)     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                if (r0 != 0) goto L_0x00f5
                boolean r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.Acceptor.$assertionsDisabled     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                if (r0 != 0) goto L_0x0071
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                java.util.concurrent.atomic.AtomicReference r0 = r0.acceptorRef     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                java.lang.Object r0 = r0.get()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                if (r0 != r6) goto L_0x0071
                java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                r0.<init>()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                throw r0     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
            L_0x00d9:
                r0 = move-exception
                r5 = r0
                r0 = r2
                r2 = r5
            L_0x00dd:
                org.apache.mina.util.ExceptionMonitor r3 = org.apache.mina.util.ExceptionMonitor.getInstance()
                r3.exceptionCaught(r2)
                r2 = 1000(0x3e8, double:4.94E-321)
                java.lang.Thread.sleep(r2)     // Catch:{ InterruptedException -> 0x00eb }
                goto L_0x0021
            L_0x00eb:
                r2 = move-exception
                org.apache.mina.util.ExceptionMonitor r3 = org.apache.mina.util.ExceptionMonitor.getInstance()
                r3.exceptionCaught(r2)
                goto L_0x0021
            L_0x00f5:
                boolean r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.Acceptor.$assertionsDisabled     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                if (r0 != 0) goto L_0x010b
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                java.util.concurrent.atomic.AtomicReference r0 = r0.acceptorRef     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                java.lang.Object r0 = r0.get()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                if (r0 == r6) goto L_0x010b
                java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                r0.<init>()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                throw r0     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
            L_0x010b:
                if (r3 <= 0) goto L_0x0116
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                java.util.Iterator r0 = r0.selectedHandles()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                r6.processHandles(r0)     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
            L_0x0116:
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                int r0 = r0.unregisterHandles()     // Catch:{ ClosedSelectorException -> 0x0070, Throwable -> 0x00d9 }
                int r0 = r2 - r0
                goto L_0x0021
            L_0x0120:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0120 }
                throw r0     // Catch:{ Exception -> 0x0123 }
            L_0x0123:
                r0 = move-exception
                org.apache.mina.util.ExceptionMonitor r1 = org.apache.mina.util.ExceptionMonitor.getInstance()     // Catch:{ all -> 0x0135 }
                r1.exceptionCaught(r0)     // Catch:{ all -> 0x0135 }
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r0 = r0.disposalFuture
                r0.setDone()
                goto L_0x00b5
            L_0x0135:
                r0 = move-exception
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r1 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r1 = r1.disposalFuture
                r1.setDone()
                throw r0
            L_0x0140:
                r0 = move-exception
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r1 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ Exception -> 0x0163 }
                java.lang.Object r2 = r1.disposalLock     // Catch:{ Exception -> 0x0163 }
                monitor-enter(r2)     // Catch:{ Exception -> 0x0163 }
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r1 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ all -> 0x0160 }
                boolean r1 = r1.isDisposing()     // Catch:{ all -> 0x0160 }
                if (r1 == 0) goto L_0x0155
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r1 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this     // Catch:{ all -> 0x0160 }
                r1.destroy()     // Catch:{ all -> 0x0160 }
            L_0x0155:
                monitor-exit(r2)     // Catch:{ all -> 0x0160 }
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r1 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r1 = r1.disposalFuture
                r1.setDone()
            L_0x015f:
                throw r0
            L_0x0160:
                r1 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0160 }
                throw r1     // Catch:{ Exception -> 0x0163 }
            L_0x0163:
                r1 = move-exception
                org.apache.mina.util.ExceptionMonitor r2 = org.apache.mina.util.ExceptionMonitor.getInstance()     // Catch:{ all -> 0x0175 }
                r2.exceptionCaught(r1)     // Catch:{ all -> 0x0175 }
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r1 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r1 = r1.disposalFuture
                r1.setDone()
                goto L_0x015f
            L_0x0175:
                r0 = move-exception
                org.apache.mina.core.polling.AbstractPollingIoAcceptor r1 = org.apache.mina.core.polling.AbstractPollingIoAcceptor.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r1 = r1.disposalFuture
                r1.setDone()
                throw r0
            L_0x0180:
                r2 = move-exception
                goto L_0x00dd
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingIoAcceptor.Acceptor.run():void");
        }

        private void processHandles(Iterator<H> it) throws Exception {
            while (it.hasNext()) {
                H next = it.next();
                it.remove();
                AbstractIoSession accept = AbstractPollingIoAcceptor.this.accept(AbstractPollingIoAcceptor.this.processor, next);
                if (accept != null) {
                    AbstractPollingIoAcceptor.this.initSession(accept, null, null);
                    accept.getProcessor().add(accept);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public int registerHandles() {
        /*
            r5 = this;
        L_0x0000:
            java.util.Queue<org.apache.mina.core.service.AbstractIoAcceptor$AcceptorOperationFuture> r0 = r5.registerQueue
            java.lang.Object r0 = r0.poll()
            org.apache.mina.core.service.AbstractIoAcceptor$AcceptorOperationFuture r0 = (org.apache.mina.core.service.AbstractIoAcceptor.AcceptorOperationFuture) r0
            if (r0 != 0) goto L_0x000c
            r0 = 0
        L_0x000b:
            return r0
        L_0x000c:
            java.util.concurrent.ConcurrentHashMap r2 = new java.util.concurrent.ConcurrentHashMap
            r2.<init>()
            java.util.List r1 = r0.getLocalAddresses()
            java.util.Iterator r3 = r1.iterator()     // Catch:{ Exception -> 0x0031 }
        L_0x0019:
            boolean r1 = r3.hasNext()     // Catch:{ Exception -> 0x0031 }
            if (r1 == 0) goto L_0x005a
            java.lang.Object r1 = r3.next()     // Catch:{ Exception -> 0x0031 }
            java.net.SocketAddress r1 = (java.net.SocketAddress) r1     // Catch:{ Exception -> 0x0031 }
            java.lang.Object r1 = r5.open(r1)     // Catch:{ Exception -> 0x0031 }
            java.net.SocketAddress r4 = r5.localAddress(r1)     // Catch:{ Exception -> 0x0031 }
            r2.put(r4, r1)     // Catch:{ Exception -> 0x0031 }
            goto L_0x0019
        L_0x0031:
            r1 = move-exception
            r0.setException(r1)     // Catch:{ all -> 0x0096 }
            java.lang.Exception r0 = r0.getException()
            if (r0 == 0) goto L_0x0000
            java.util.Collection r0 = r2.values()
            java.util.Iterator r0 = r0.iterator()
        L_0x0043:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0091
            java.lang.Object r1 = r0.next()
            r5.close(r1)     // Catch:{ Exception -> 0x0051 }
            goto L_0x0043
        L_0x0051:
            r1 = move-exception
            org.apache.mina.util.ExceptionMonitor r2 = org.apache.mina.util.ExceptionMonitor.getInstance()
            r2.exceptionCaught(r1)
            goto L_0x0043
        L_0x005a:
            java.util.Map<java.net.SocketAddress, H> r1 = r5.boundHandles     // Catch:{ Exception -> 0x0031 }
            r1.putAll(r2)     // Catch:{ Exception -> 0x0031 }
            r0.setDone()     // Catch:{ Exception -> 0x0031 }
            int r1 = r2.size()     // Catch:{ Exception -> 0x0031 }
            java.lang.Exception r0 = r0.getException()
            if (r0 == 0) goto L_0x008e
            java.util.Collection r0 = r2.values()
            java.util.Iterator r0 = r0.iterator()
        L_0x0074:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x008b
            java.lang.Object r2 = r0.next()
            r5.close(r2)     // Catch:{ Exception -> 0x0082 }
            goto L_0x0074
        L_0x0082:
            r2 = move-exception
            org.apache.mina.util.ExceptionMonitor r3 = org.apache.mina.util.ExceptionMonitor.getInstance()
            r3.exceptionCaught(r2)
            goto L_0x0074
        L_0x008b:
            r5.wakeup()
        L_0x008e:
            r0 = r1
            goto L_0x000b
        L_0x0091:
            r5.wakeup()
            goto L_0x0000
        L_0x0096:
            r1 = move-exception
            java.lang.Exception r0 = r0.getException()
            if (r0 == 0) goto L_0x00bf
            java.util.Collection r0 = r2.values()
            java.util.Iterator r0 = r0.iterator()
        L_0x00a5:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x00bc
            java.lang.Object r2 = r0.next()
            r5.close(r2)     // Catch:{ Exception -> 0x00b3 }
            goto L_0x00a5
        L_0x00b3:
            r2 = move-exception
            org.apache.mina.util.ExceptionMonitor r3 = org.apache.mina.util.ExceptionMonitor.getInstance()
            r3.exceptionCaught(r2)
            goto L_0x00a5
        L_0x00bc:
            r5.wakeup()
        L_0x00bf:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingIoAcceptor.registerHandles():int");
    }

    /* access modifiers changed from: private */
    public int unregisterHandles() {
        int i = 0;
        while (true) {
            AbstractIoAcceptor.AcceptorOperationFuture poll = this.cancelQueue.poll();
            if (poll == null) {
                return i;
            }
            int i2 = i;
            for (SocketAddress remove : poll.getLocalAddresses()) {
                H remove2 = this.boundHandles.remove(remove);
                if (remove2 != null) {
                    try {
                        close(remove2);
                        wakeup();
                    } catch (Throwable th) {
                        ExceptionMonitor.getInstance().exceptionCaught(th);
                    } finally {
                        int i3 = i2 + 1;
                    }
                }
            }
            poll.setDone();
            i = i2;
        }
    }

    public final IoSession newSession(SocketAddress socketAddress, SocketAddress socketAddress2) {
        throw new UnsupportedOperationException();
    }

    public int getBacklog() {
        return this.backlog;
    }

    public void setBacklog(int i) {
        synchronized (this.bindLock) {
            if (isActive()) {
                throw new IllegalStateException("backlog can't be set while the acceptor is bound.");
            }
            this.backlog = i;
        }
    }

    public boolean isReuseAddress() {
        return this.reuseAddress;
    }

    public void setReuseAddress(boolean z) {
        synchronized (this.bindLock) {
            if (isActive()) {
                throw new IllegalStateException("backlog can't be set while the acceptor is bound.");
            }
            this.reuseAddress = z;
        }
    }
}
