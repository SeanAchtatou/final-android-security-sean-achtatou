package org.apache.mina.core.service;

import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import org.a.b;
import org.a.c;
import org.apache.mina.core.session.AbstractIoSession;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.write.WriteRequest;

public class SimpleIoProcessorPool<S extends AbstractIoSession> implements IoProcessor<S> {
    private static final int DEFAULT_SIZE = (Runtime.getRuntime().availableProcessors() + 1);
    private static final b LOGGER = c.a(SimpleIoProcessorPool.class);
    private static final AttributeKey PROCESSOR = new AttributeKey(SimpleIoProcessorPool.class, "processor");
    private final boolean createdExecutor;
    private final Object disposalLock;
    private volatile boolean disposed;
    private volatile boolean disposing;
    private final Executor executor;
    private final IoProcessor<S>[] pool;

    public SimpleIoProcessorPool(Class<? extends IoProcessor<S>> cls) {
        this(cls, null, DEFAULT_SIZE);
    }

    public SimpleIoProcessorPool(Class cls, int i) {
        this(cls, null, i);
    }

    public SimpleIoProcessorPool(Class cls, Executor executor2) {
        this(cls, executor2, DEFAULT_SIZE);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.a.b.e(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.Exception]
     candidates:
      org.a.b.e(java.lang.String, java.lang.Object):void
      org.a.b.e(java.lang.String, java.lang.Throwable):void */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00bb, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00bc, code lost:
        dispose();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00bf, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r3 = r9.getConstructor(new java.lang.Class[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r8.pool[0] = (org.apache.mina.core.service.IoProcessor) r3.newInstance(new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0101, code lost:
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0105, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        org.apache.mina.core.service.SimpleIoProcessorPool.LOGGER.e("Cannot create an IoProcessor :{}", r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0111, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0112, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0113, code lost:
        r1 = "Failed to create a new instance of " + r9.getName() + ":" + r0.getMessage();
        org.apache.mina.core.service.SimpleIoProcessorPool.LOGGER.e(r1, (java.lang.Throwable) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0142, code lost:
        throw new org.apache.mina.core.RuntimeIoException(r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0171, code lost:
        r2 = true;
        r0 = r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0079 A[SYNTHETIC, Splitter:B:17:0x0079] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0105 A[ExcHandler: RuntimeException (r0v7 'e' java.lang.RuntimeException A[CUSTOM_DECLARE]), Splitter:B:13:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0112 A[Catch:{ NoSuchMethodException -> 0x0170, RuntimeException -> 0x0105, Exception -> 0x0112, all -> 0x00bb }, ExcHandler: Exception (r0v6 'e' java.lang.Exception A[CUSTOM_DECLARE, Catch:{  }]), Splitter:B:13:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0143 A[Catch:{ NoSuchMethodException -> 0x0170, RuntimeException -> 0x0105, Exception -> 0x0112, all -> 0x00bb }, LOOP:0: B:46:0x0143->B:51:0x015c, LOOP_START, PHI: r1 
      PHI: (r1v8 int) = (r1v0 int), (r1v9 int) binds: [B:16:0x0077, B:51:0x015c] A[DONT_GENERATE, DONT_INLINE]] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SimpleIoProcessorPool(java.lang.Class<? extends org.apache.mina.core.service.IoProcessor<S>> r9, java.util.concurrent.Executor r10, int r11) {
        /*
            r8 = this;
            r1 = 1
            r2 = 0
            r8.<init>()
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r8.disposalLock = r0
            if (r9 != 0) goto L_0x0016
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "processorType"
            r0.<init>(r1)
            throw r0
        L_0x0016:
            if (r11 > 0) goto L_0x0037
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "size: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r2 = " (expected: positive integer)"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0037:
            if (r10 != 0) goto L_0x00c0
            r0 = r1
        L_0x003a:
            r8.createdExecutor = r0
            boolean r0 = r8.createdExecutor
            if (r0 == 0) goto L_0x00c3
            java.util.concurrent.ExecutorService r0 = java.util.concurrent.Executors.newCachedThreadPool()
            r8.executor = r0
            java.util.concurrent.Executor r0 = r8.executor
            java.util.concurrent.ThreadPoolExecutor r0 = (java.util.concurrent.ThreadPoolExecutor) r0
            java.util.concurrent.ThreadPoolExecutor$CallerRunsPolicy r3 = new java.util.concurrent.ThreadPoolExecutor$CallerRunsPolicy
            r3.<init>()
            r0.setRejectedExecutionHandler(r3)
        L_0x0052:
            org.apache.mina.core.service.IoProcessor[] r0 = new org.apache.mina.core.service.IoProcessor[r11]
            r8.pool = r0
            r3 = 0
            r0 = 1
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ NoSuchMethodException -> 0x00c6, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r4 = 0
            java.lang.Class<java.util.concurrent.ExecutorService> r5 = java.util.concurrent.ExecutorService.class
            r0[r4] = r5     // Catch:{ NoSuchMethodException -> 0x00c6, RuntimeException -> 0x0105, Exception -> 0x0112 }
            java.lang.reflect.Constructor r3 = r9.getConstructor(r0)     // Catch:{ NoSuchMethodException -> 0x00c6, RuntimeException -> 0x0105, Exception -> 0x0112 }
            org.apache.mina.core.service.IoProcessor<S>[] r4 = r8.pool     // Catch:{ NoSuchMethodException -> 0x00c6, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r5 = 0
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ NoSuchMethodException -> 0x00c6, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r6 = 0
            java.util.concurrent.Executor r7 = r8.executor     // Catch:{ NoSuchMethodException -> 0x00c6, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r0[r6] = r7     // Catch:{ NoSuchMethodException -> 0x00c6, RuntimeException -> 0x0105, Exception -> 0x0112 }
            java.lang.Object r0 = r3.newInstance(r0)     // Catch:{ NoSuchMethodException -> 0x00c6, RuntimeException -> 0x0105, Exception -> 0x0112 }
            org.apache.mina.core.service.IoProcessor r0 = (org.apache.mina.core.service.IoProcessor) r0     // Catch:{ NoSuchMethodException -> 0x00c6, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r4[r5] = r0     // Catch:{ NoSuchMethodException -> 0x00c6, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r2 = r1
        L_0x0077:
            if (r3 != 0) goto L_0x0143
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bb }
            r0.<init>()     // Catch:{ all -> 0x00bb }
            java.lang.String r1 = java.lang.String.valueOf(r9)     // Catch:{ all -> 0x00bb }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00bb }
            java.lang.String r1 = " must have a public constructor with one "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00bb }
            java.lang.Class<java.util.concurrent.ExecutorService> r1 = java.util.concurrent.ExecutorService.class
            java.lang.String r1 = r1.getSimpleName()     // Catch:{ all -> 0x00bb }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00bb }
            java.lang.String r1 = " parameter, a public constructor with one "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00bb }
            java.lang.Class<java.util.concurrent.Executor> r1 = java.util.concurrent.Executor.class
            java.lang.String r1 = r1.getSimpleName()     // Catch:{ all -> 0x00bb }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00bb }
            java.lang.String r1 = " parameter or a public default constructor."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00bb }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00bb }
            org.a.b r1 = org.apache.mina.core.service.SimpleIoProcessorPool.LOGGER     // Catch:{ all -> 0x00bb }
            r1.e(r0)     // Catch:{ all -> 0x00bb }
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x00bb }
            r1.<init>(r0)     // Catch:{ all -> 0x00bb }
            throw r1     // Catch:{ all -> 0x00bb }
        L_0x00bb:
            r0 = move-exception
            r8.dispose()
            throw r0
        L_0x00c0:
            r0 = r2
            goto L_0x003a
        L_0x00c3:
            r8.executor = r10
            goto L_0x0052
        L_0x00c6:
            r0 = move-exception
            r0 = 1
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ NoSuchMethodException -> 0x00e8, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r4 = 0
            java.lang.Class<java.util.concurrent.Executor> r5 = java.util.concurrent.Executor.class
            r0[r4] = r5     // Catch:{ NoSuchMethodException -> 0x00e8, RuntimeException -> 0x0105, Exception -> 0x0112 }
            java.lang.reflect.Constructor r3 = r9.getConstructor(r0)     // Catch:{ NoSuchMethodException -> 0x00e8, RuntimeException -> 0x0105, Exception -> 0x0112 }
            org.apache.mina.core.service.IoProcessor<S>[] r4 = r8.pool     // Catch:{ NoSuchMethodException -> 0x00e8, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r5 = 0
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ NoSuchMethodException -> 0x00e8, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r6 = 0
            java.util.concurrent.Executor r7 = r8.executor     // Catch:{ NoSuchMethodException -> 0x00e8, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r0[r6] = r7     // Catch:{ NoSuchMethodException -> 0x00e8, RuntimeException -> 0x0105, Exception -> 0x0112 }
            java.lang.Object r0 = r3.newInstance(r0)     // Catch:{ NoSuchMethodException -> 0x00e8, RuntimeException -> 0x0105, Exception -> 0x0112 }
            org.apache.mina.core.service.IoProcessor r0 = (org.apache.mina.core.service.IoProcessor) r0     // Catch:{ NoSuchMethodException -> 0x00e8, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r4[r5] = r0     // Catch:{ NoSuchMethodException -> 0x00e8, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r2 = r1
            goto L_0x0077
        L_0x00e8:
            r0 = move-exception
            r0 = 0
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ NoSuchMethodException -> 0x0170, RuntimeException -> 0x0105, Exception -> 0x0112 }
            java.lang.reflect.Constructor r3 = r9.getConstructor(r0)     // Catch:{ NoSuchMethodException -> 0x0170, RuntimeException -> 0x0105, Exception -> 0x0112 }
            org.apache.mina.core.service.IoProcessor<S>[] r4 = r8.pool     // Catch:{ NoSuchMethodException -> 0x0100, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r5 = 0
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ NoSuchMethodException -> 0x0100, RuntimeException -> 0x0105, Exception -> 0x0112 }
            java.lang.Object r0 = r3.newInstance(r0)     // Catch:{ NoSuchMethodException -> 0x0100, RuntimeException -> 0x0105, Exception -> 0x0112 }
            org.apache.mina.core.service.IoProcessor r0 = (org.apache.mina.core.service.IoProcessor) r0     // Catch:{ NoSuchMethodException -> 0x0100, RuntimeException -> 0x0105, Exception -> 0x0112 }
            r4[r5] = r0     // Catch:{ NoSuchMethodException -> 0x0100, RuntimeException -> 0x0105, Exception -> 0x0112 }
            goto L_0x0077
        L_0x0100:
            r0 = move-exception
            r0 = r3
        L_0x0102:
            r3 = r0
            goto L_0x0077
        L_0x0105:
            r0 = move-exception
            org.a.b r1 = org.apache.mina.core.service.SimpleIoProcessorPool.LOGGER     // Catch:{ all -> 0x00bb }
            java.lang.String r2 = "Cannot create an IoProcessor :{}"
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x00bb }
            r1.e(r2, r3)     // Catch:{ all -> 0x00bb }
            throw r0     // Catch:{ all -> 0x00bb }
        L_0x0112:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bb }
            r1.<init>()     // Catch:{ all -> 0x00bb }
            java.lang.String r2 = "Failed to create a new instance of "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00bb }
            java.lang.String r2 = r9.getName()     // Catch:{ all -> 0x00bb }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00bb }
            java.lang.String r2 = ":"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00bb }
            java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x00bb }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00bb }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00bb }
            org.a.b r2 = org.apache.mina.core.service.SimpleIoProcessorPool.LOGGER     // Catch:{ all -> 0x00bb }
            r2.e(r1, r0)     // Catch:{ all -> 0x00bb }
            org.apache.mina.core.RuntimeIoException r2 = new org.apache.mina.core.RuntimeIoException     // Catch:{ all -> 0x00bb }
            r2.<init>(r1, r0)     // Catch:{ all -> 0x00bb }
            throw r2     // Catch:{ all -> 0x00bb }
        L_0x0143:
            org.apache.mina.core.service.IoProcessor<S>[] r0 = r8.pool     // Catch:{ all -> 0x00bb }
            int r0 = r0.length     // Catch:{ all -> 0x00bb }
            if (r1 >= r0) goto L_0x016f
            if (r2 == 0) goto L_0x015f
            org.apache.mina.core.service.IoProcessor<S>[] r4 = r8.pool     // Catch:{ Exception -> 0x016d }
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x016d }
            r5 = 0
            java.util.concurrent.Executor r6 = r8.executor     // Catch:{ Exception -> 0x016d }
            r0[r5] = r6     // Catch:{ Exception -> 0x016d }
            java.lang.Object r0 = r3.newInstance(r0)     // Catch:{ Exception -> 0x016d }
            org.apache.mina.core.service.IoProcessor r0 = (org.apache.mina.core.service.IoProcessor) r0     // Catch:{ Exception -> 0x016d }
            r4[r1] = r0     // Catch:{ Exception -> 0x016d }
        L_0x015c:
            int r1 = r1 + 1
            goto L_0x0143
        L_0x015f:
            org.apache.mina.core.service.IoProcessor<S>[] r4 = r8.pool     // Catch:{ Exception -> 0x016d }
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x016d }
            java.lang.Object r0 = r3.newInstance(r0)     // Catch:{ Exception -> 0x016d }
            org.apache.mina.core.service.IoProcessor r0 = (org.apache.mina.core.service.IoProcessor) r0     // Catch:{ Exception -> 0x016d }
            r4[r1] = r0     // Catch:{ Exception -> 0x016d }
            goto L_0x015c
        L_0x016d:
            r0 = move-exception
            goto L_0x015c
        L_0x016f:
            return
        L_0x0170:
            r0 = move-exception
            r2 = r1
            r0 = r3
            goto L_0x0102
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.service.SimpleIoProcessorPool.<init>(java.lang.Class, java.util.concurrent.Executor, int):void");
    }

    public final void add(S s) {
        getProcessor(s).add(s);
    }

    public final void flush(S s) {
        getProcessor(s).flush(s);
    }

    public final void write(S s, WriteRequest writeRequest) {
        getProcessor(s).write(s, writeRequest);
    }

    public final void remove(S s) {
        getProcessor(s).remove(s);
    }

    public final void updateTrafficControl(S s) {
        getProcessor(s).updateTrafficControl(s);
    }

    public boolean isDisposed() {
        return this.disposed;
    }

    public boolean isDisposing() {
        return this.disposing;
    }

    public final void dispose() {
        if (!this.disposed) {
            synchronized (this.disposalLock) {
                if (!this.disposing) {
                    this.disposing = true;
                    for (IoProcessor<S> ioProcessor : this.pool) {
                        if (ioProcessor != null && !ioProcessor.isDisposing()) {
                            try {
                                ioProcessor.dispose();
                            } catch (Exception e) {
                                LOGGER.b("Failed to dispose the {} IoProcessor.", ioProcessor.getClass().getSimpleName(), e);
                            }
                        }
                    }
                    if (this.createdExecutor) {
                        ((ExecutorService) this.executor).shutdown();
                    }
                }
                Arrays.fill(this.pool, (Object) null);
                this.disposed = true;
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private org.apache.mina.core.service.IoProcessor<S> getProcessor(S r5) {
        /*
            r4 = this;
            org.apache.mina.core.session.AttributeKey r0 = org.apache.mina.core.service.SimpleIoProcessorPool.PROCESSOR
            java.lang.Object r0 = r5.getAttribute(r0)
            org.apache.mina.core.service.IoProcessor r0 = (org.apache.mina.core.service.IoProcessor) r0
            if (r0 != 0) goto L_0x003a
            boolean r0 = r4.disposed
            if (r0 != 0) goto L_0x0012
            boolean r0 = r4.disposing
            if (r0 == 0) goto L_0x001a
        L_0x0012:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "A disposed processor cannot be accessed."
            r0.<init>(r1)
            throw r0
        L_0x001a:
            org.apache.mina.core.service.IoProcessor<S>[] r0 = r4.pool
            long r2 = r5.getId()
            int r1 = (int) r2
            int r1 = java.lang.Math.abs(r1)
            org.apache.mina.core.service.IoProcessor<S>[] r2 = r4.pool
            int r2 = r2.length
            int r1 = r1 % r2
            r0 = r0[r1]
            if (r0 != 0) goto L_0x0035
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "A disposed processor cannot be accessed."
            r0.<init>(r1)
            throw r0
        L_0x0035:
            org.apache.mina.core.session.AttributeKey r1 = org.apache.mina.core.service.SimpleIoProcessorPool.PROCESSOR
            r5.setAttributeIfAbsent(r1, r0)
        L_0x003a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.service.SimpleIoProcessorPool.getProcessor(org.apache.mina.core.session.AbstractIoSession):org.apache.mina.core.service.IoProcessor");
    }
}
