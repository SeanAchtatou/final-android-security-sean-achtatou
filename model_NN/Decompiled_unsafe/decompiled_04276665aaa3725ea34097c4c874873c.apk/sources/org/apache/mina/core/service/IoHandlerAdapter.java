package org.apache.mina.core.service;

import org.a.b;
import org.a.c;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

public class IoHandlerAdapter implements IoHandler {
    private static final b LOGGER = c.a(IoHandlerAdapter.class);

    public void sessionCreated(IoSession ioSession) throws Exception {
    }

    public void sessionOpened(IoSession ioSession) throws Exception {
    }

    public void sessionClosed(IoSession ioSession) throws Exception {
    }

    public void sessionIdle(IoSession ioSession, IdleStatus idleStatus) throws Exception {
    }

    public void exceptionCaught(IoSession ioSession, Throwable th) throws Exception {
        if (LOGGER.b()) {
            LOGGER.d("EXCEPTION, please implement " + getClass().getName() + ".exceptionCaught() for proper handling:", th);
        }
    }

    public void messageReceived(IoSession ioSession, Object obj) throws Exception {
    }

    public void messageSent(IoSession ioSession, Object obj) throws Exception {
    }
}
