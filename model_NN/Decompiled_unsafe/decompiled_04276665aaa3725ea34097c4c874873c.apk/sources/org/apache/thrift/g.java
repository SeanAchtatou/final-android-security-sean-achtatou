package org.apache.thrift;

import java.io.ByteArrayOutputStream;
import org.apache.thrift.protocol.a;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.h;
import org.apache.thrift.transport.a;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private final ByteArrayOutputStream f3027a;
    private final a b;
    private f c;

    public g() {
        this(new a.C0075a());
    }

    public g(h hVar) {
        this.f3027a = new ByteArrayOutputStream();
        this.b = new org.apache.thrift.transport.a(this.f3027a);
        this.c = hVar.a(this.b);
    }

    public byte[] a(b bVar) {
        this.f3027a.reset();
        bVar.b(this.c);
        return this.f3027a.toByteArray();
    }
}
