package org.apache.thrift.transport;

import org.apache.thrift.f;

public class e extends f {

    /* renamed from: a  reason: collision with root package name */
    protected int f3045a = 0;

    public e() {
    }

    public e(int i) {
        this.f3045a = i;
    }

    public e(int i, String str) {
        super(str);
        this.f3045a = i;
    }

    public e(int i, Throwable th) {
        super(th);
        this.f3045a = i;
    }

    public e(String str) {
        super(str);
    }
}
