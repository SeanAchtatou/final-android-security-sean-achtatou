package org.apache.thrift.protocol;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    public final byte f3038a;
    public final byte b;
    public final int c;

    public e() {
        this((byte) 0, (byte) 0, 0);
    }

    public e(byte b2, byte b3, int i) {
        this.f3038a = b2;
        this.b = b3;
        this.c = i;
    }
}
