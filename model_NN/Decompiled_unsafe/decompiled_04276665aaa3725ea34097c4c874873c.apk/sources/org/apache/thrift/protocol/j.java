package org.apache.thrift.protocol;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    public final byte f3041a;
    public final int b;

    public j() {
        this((byte) 0, 0);
    }

    public j(byte b2, int i) {
        this.f3041a = b2;
        this.b = i;
    }
}
