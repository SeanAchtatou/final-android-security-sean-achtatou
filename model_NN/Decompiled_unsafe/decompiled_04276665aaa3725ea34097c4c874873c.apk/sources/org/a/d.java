package org.a;

import java.util.Map;
import org.a.a.f;
import org.a.a.i;
import org.a.c.a;

/* compiled from: MDC */
public class d {

    /* renamed from: a  reason: collision with root package name */
    static a f3023a;

    static {
        try {
            f3023a = org.a.b.d.f3021a.a();
        } catch (NoClassDefFoundError e) {
            f3023a = new f();
            String message = e.getMessage();
            if (message == null || message.indexOf("org/slf4j/impl/StaticMDCBinder") == -1) {
                throw e;
            }
            i.a("Failed to load class \"org.slf4j.impl.StaticMDCBinder\".");
            i.a("Defaulting to no-operation MDCAdapter implementation.");
            i.a("See http://www.slf4j.org/codes.html#no_static_mdc_binder for further details.");
        } catch (Exception e2) {
            i.a("MDC binding unsuccessful.", e2);
        }
    }

    public static void a(String str, String str2) throws IllegalArgumentException {
        if (str == null) {
            throw new IllegalArgumentException("key parameter cannot be null");
        } else if (f3023a == null) {
            throw new IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        } else {
            f3023a.a(str, str2);
        }
    }

    public static String a(String str) throws IllegalArgumentException {
        if (str == null) {
            throw new IllegalArgumentException("key parameter cannot be null");
        } else if (f3023a != null) {
            return f3023a.a(str);
        } else {
            throw new IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        }
    }

    public static void b(String str) throws IllegalArgumentException {
        if (str == null) {
            throw new IllegalArgumentException("key parameter cannot be null");
        } else if (f3023a == null) {
            throw new IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
        } else {
            f3023a.b(str);
        }
    }

    public static Map a() {
        if (f3023a != null) {
            return f3023a.a();
        }
        throw new IllegalStateException("MDCAdapter cannot be null. See also http://www.slf4j.org/codes.html#null_MDCA");
    }
}
