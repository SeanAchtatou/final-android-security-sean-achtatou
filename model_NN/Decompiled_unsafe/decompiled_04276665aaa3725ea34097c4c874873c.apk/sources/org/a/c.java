package org.a;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import org.a.a.e;
import org.a.a.h;
import org.a.a.i;

/* compiled from: LoggerFactory */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    static int f3022a = 0;
    static h b = new h();
    static e c = new e();
    private static final String[] d = {"1.6"};
    private static String e = "org/slf4j/impl/StaticLoggerBinder.class";

    private c() {
    }

    private static final void b() {
        f();
        c();
        if (f3022a == 3) {
            e();
        }
    }

    private static final void c() {
        try {
            org.a.b.c.a();
            f3022a = 3;
            d();
        } catch (NoClassDefFoundError e2) {
            String message = e2.getMessage();
            if (message == null || message.indexOf("org/slf4j/impl/StaticLoggerBinder") == -1) {
                a(e2);
                throw e2;
            }
            f3022a = 4;
            i.a("Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".");
            i.a("Defaulting to no-operation (NOP) logger implementation");
            i.a("See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.");
        } catch (NoSuchMethodError e3) {
            String message2 = e3.getMessage();
            if (!(message2 == null || message2.indexOf("org.slf4j.impl.StaticLoggerBinder.getSingleton()") == -1)) {
                f3022a = 2;
                i.a("slf4j-api 1.6.x (or later) is incompatible with this binding.");
                i.a("Your binding is version 1.5.5 or earlier.");
                i.a("Upgrade your binding to version 1.6.x. or 2.0.x");
            }
            throw e3;
        } catch (Exception e4) {
            a(e4);
            throw new IllegalStateException("Unexpected initialization failure", e4);
        }
    }

    static void a(Throwable th) {
        f3022a = 2;
        i.a("Failed to instantiate SLF4J LoggerFactory", th);
    }

    private static final void d() {
        List a2 = b.a();
        if (a2.size() != 0) {
            i.a("The following loggers will not work becasue they were created");
            i.a("during the default configuration phase of the underlying logging system.");
            i.a("See also http://www.slf4j.org/codes.html#substituteLogger");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    i.a((String) a2.get(i2));
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private static final void e() {
        boolean z = false;
        try {
            String str = org.a.b.c.f3020a;
            for (String startsWith : d) {
                if (str.startsWith(startsWith)) {
                    z = true;
                }
            }
            if (!z) {
                i.a("The requested version " + str + " by your slf4j binding is not compatible with " + Arrays.asList(d).toString());
                i.a("See http://www.slf4j.org/codes.html#version_mismatch for further details.");
            }
        } catch (NoSuchFieldError e2) {
        } catch (Throwable th) {
            i.a("Unexpected problem occured during version sanity check", th);
        }
    }

    private static void f() {
        Enumeration<URL> resources;
        try {
            ClassLoader classLoader = c.class.getClassLoader();
            if (classLoader == null) {
                resources = ClassLoader.getSystemResources(e);
            } else {
                resources = classLoader.getResources(e);
            }
            ArrayList arrayList = new ArrayList();
            while (resources.hasMoreElements()) {
                arrayList.add(resources.nextElement());
            }
            if (arrayList.size() > 1) {
                i.a("Class path contains multiple SLF4J bindings.");
                for (int i = 0; i < arrayList.size(); i++) {
                    i.a("Found binding in [" + arrayList.get(i) + "]");
                }
                i.a("See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.");
            }
        } catch (IOException e2) {
            i.a("Error getting resources from path", e2);
        }
    }

    public static b a(String str) {
        return a().a(str);
    }

    public static b a(Class cls) {
        return a(cls.getName());
    }

    public static a a() {
        if (f3022a == 0) {
            f3022a = 1;
            b();
        }
        switch (f3022a) {
            case 1:
                return b;
            case 2:
                throw new IllegalStateException("org.slf4j.LoggerFactory could not be successfully initialized. See also http://www.slf4j.org/codes.html#unsuccessfulInit");
            case 3:
                return org.a.b.c.a().b();
            case 4:
                return c;
            default:
                throw new IllegalStateException("Unreachable code");
        }
    }
}
