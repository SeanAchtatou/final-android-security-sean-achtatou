package cn.banshenggua.aichang.entry;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import cn.a.a.a;
import cn.banshenggua.aichang.utils.KShareUtil;
import cn.banshenggua.aichang.utils.Toaster;
import cn.banshenggua.aichang.utils.UIUtil;
import cn.banshenggua.aichang.utils.ULog;
import cn.banshenggua.aichang.widget.PullToRefreshBase;
import cn.banshenggua.aichang.widget.PullToRefreshListView;
import com.android.volley.s;
import com.pocketmusic.kshare.requestobjs.o;
import com.pocketmusic.kshare.requestobjs.p;
import com.pocketmusic.kshare.requestobjs.r;
import org.json.JSONObject;

public class HotRoomFragment extends BaseFragment implements PullToRefreshBase.OnRefreshListener2 {
    private static final float HEIGHT = 44.0f;
    private static final float WIDTH = 320.0f;
    private ViewGroup container;
    private View headView;
    /* access modifiers changed from: private */
    public HotRoomAdapter mAdapter;
    private View mHeadBannerLayout;
    private View mHeadBannerView;
    private ListView mListView;
    /* access modifiers changed from: private */
    public PullToRefreshListView mRefreshListView;
    private r mRequestListener = new r() {
        public void onErrorResponse(s sVar) {
            super.onErrorResponse(sVar);
            commonRequestEnd();
        }

        private void commonRequestEnd() {
            HotRoomFragment.this.mRefreshListView.onRefreshComplete();
        }

        public void onResponse(JSONObject jSONObject) {
            commonRequestEnd();
            ULog.d("RoomList", "arg0: " + jSONObject);
            HotRoomFragment.this.mRoomList.a(jSONObject);
            if (HotRoomFragment.this.mRoomList.ao != -1000) {
                KShareUtil.showToastJsonStatus(HotRoomFragment.this.getActivity(), HotRoomFragment.this.mRoomList);
                return;
            }
            if (HotRoomFragment.this.mRefreshListView.getCurrentMode() == PullToRefreshBase.Mode.PULL_DOWN_TO_REFRESH && HotRoomFragment.this.mRoomList.e().size() > 0) {
                HotRoomFragment.this.mAdapter.clearItem();
            }
            HotRoomFragment.this.mAdapter.addItem(HotRoomFragment.this.mRoomList.e());
            if (HotRoomFragment.this.mAdapter.getList().size() > 0) {
                HotRoomFragment.this.adapterHeaderView((o) HotRoomFragment.this.mAdapter.getList().get(0));
            } else {
                HotRoomFragment.this.adapterHeaderView(null);
            }
        }
    };
    /* access modifiers changed from: private */
    public p mRoomList = null;
    private p.a mType;
    private String mUrl;
    private boolean useCache = false;

    public static HotRoomFragment newInstance() {
        return new HotRoomFragment();
    }

    public static HotRoomFragment newInstance(p.a aVar, String str) {
        return new HotRoomFragment();
    }

    public HotRoomFragment() {
    }

    public HotRoomFragment(p.a aVar, String str) {
        this.mType = aVar;
        this.mUrl = str;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.container = (ViewGroup) getActivity().getLayoutInflater().inflate(a.g.public_pulllistview, (ViewGroup) null);
        initView(this.container);
        initData();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return this.container;
    }

    public void onDestroyView() {
        super.onDestroyView();
        ViewGroup viewGroup = (ViewGroup) this.container.getParent();
        if (viewGroup != null) {
            viewGroup.removeAllViewsInLayout();
        }
    }

    public void onResume() {
        super.onResume();
    }

    @SuppressLint({"InflateParams"})
    private void initView(ViewGroup viewGroup) {
        this.mRefreshListView = (PullToRefreshListView) viewGroup.findViewById(a.f.public_items_listview);
        this.mRefreshListView.setOnRefreshListener(this);
        this.mRefreshListView.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {
            public void onLastItemVisible() {
                HotRoomFragment.this.onPullUpToRefresh();
            }
        });
        this.mListView = (ListView) this.mRefreshListView.getRefreshableView();
        this.mListView.setDividerHeight(0);
        addHeaderView();
        this.mAdapter = new HotRoomAdapter(getActivity());
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
    }

    private void addHeaderView() {
        this.headView = LayoutInflater.from(getActivity()).inflate(a.g.item_hot_room, (ViewGroup) null);
        this.headView.setVisibility(8);
        this.headView.findViewById(a.f.hot_item_02).setVisibility(8);
        ((ImageView) this.headView.findViewById(a.f.hot_item_image_01)).setBackgroundResource(a.e.default_my);
        View findViewById = this.headView.findViewById(a.f.hot_item_layout_top);
        ViewGroup.LayoutParams layoutParams = findViewById.getLayoutParams();
        layoutParams.height = (UIUtil.getInstance().getmScreenWidth() * 5) / 8;
        findViewById.setLayoutParams(layoutParams);
        this.mListView.addHeaderView(this.headView);
    }

    private void initData() {
        if (TextUtils.isEmpty(this.mUrl) || this.mType == null) {
            this.mRoomList = new p(p.a.HOT);
        } else {
            this.mRoomList = new p(this.mType, this.mUrl);
        }
        this.useCache = true;
        this.mRoomList.a(this.mRequestListener);
        this.mRoomList.c();
    }

    /* access modifiers changed from: private */
    public void adapterHeaderView(o oVar) {
        if (this.headView != null) {
            if (oVar != null) {
                this.headView.setVisibility(0);
                this.mAdapter.showRoom(this.mAdapter.createHolder(this.headView, false), oVar);
                return;
            }
            this.headView.setVisibility(8);
        }
    }

    public void onPullDownToRefresh() {
        this.mRoomList.a();
    }

    public void onPullUpToRefresh() {
        if (this.mRoomList.b()) {
            this.mRoomList.c();
            return;
        }
        Toaster.showShort(getActivity(), a.h.listview_no_more);
        this.mRefreshListView.onRefreshComplete();
    }
}
