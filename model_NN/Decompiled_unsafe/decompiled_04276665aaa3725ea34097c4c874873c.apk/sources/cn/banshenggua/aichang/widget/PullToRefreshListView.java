package cn.banshenggua.aichang.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import cn.a.a.a;
import cn.banshenggua.aichang.widget.PullToRefreshBase;

public class PullToRefreshListView extends PullToRefreshAdapterViewBase<ListView> {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode;
    private LoadingLayout mFooterLoadingView;
    private LoadingLayout mHeaderLoadingView;
    /* access modifiers changed from: private */
    public FrameLayout mLvFooterLoadingFrame;

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode;
        if (iArr == null) {
            iArr = new int[PullToRefreshBase.Mode.values().length];
            try {
                iArr[PullToRefreshBase.Mode.BOTH.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PullToRefreshBase.Mode.NO_BOTH.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PullToRefreshBase.Mode.PULL_DOWN_TO_REFRESH.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PullToRefreshBase.Mode.PULL_UP_TO_REFRESH.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode = iArr;
        }
        return iArr;
    }

    public PullToRefreshListView(Context context) {
        super(context);
        setDisableScrollingWhileRefreshing(false);
    }

    public PullToRefreshListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setDisableScrollingWhileRefreshing(false);
    }

    public PullToRefreshListView(Context context, PullToRefreshBase.Mode mode) {
        super(context, mode);
        setDisableScrollingWhileRefreshing(false);
    }

    public ContextMenu.ContextMenuInfo getContextMenuInfo() {
        return ((InternalListView) getRefreshableView()).getContextMenuInfo();
    }

    public void setPullLabel(String str, PullToRefreshBase.Mode mode) {
        super.setPullLabel(str, mode);
        if (this.mHeaderLoadingView != null && mode.canPullDown()) {
            this.mHeaderLoadingView.setPullLabel(str);
        }
        if (this.mFooterLoadingView != null && mode.canPullUp()) {
            this.mFooterLoadingView.setPullLabel(str);
        }
    }

    public void setRefreshingLabel(String str, PullToRefreshBase.Mode mode) {
        super.setRefreshingLabel(str, mode);
        if (this.mHeaderLoadingView != null && mode.canPullDown()) {
            this.mHeaderLoadingView.setRefreshingLabel(str);
        }
        if (this.mFooterLoadingView != null && mode.canPullUp()) {
            this.mFooterLoadingView.setRefreshingLabel(str);
        }
    }

    public void setReleaseLabel(String str, PullToRefreshBase.Mode mode) {
        super.setReleaseLabel(str, mode);
        if (this.mHeaderLoadingView != null && mode.canPullDown()) {
            this.mHeaderLoadingView.setReleaseLabel(str);
        }
        if (this.mFooterLoadingView != null && mode.canPullUp()) {
            this.mFooterLoadingView.setReleaseLabel(str);
        }
    }

    /* access modifiers changed from: protected */
    public final ListView createRefreshableView(Context context, AttributeSet attributeSet) {
        InternalListView internalListView = new InternalListView(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.j.PullToRefresh);
        FrameLayout frameLayout = new FrameLayout(context);
        this.mHeaderLoadingView = new LoadingLayout(context, PullToRefreshBase.Mode.PULL_DOWN_TO_REFRESH, obtainStyledAttributes);
        frameLayout.addView(this.mHeaderLoadingView, -1, -2);
        this.mHeaderLoadingView.setVisibility(8);
        internalListView.addHeaderView(frameLayout, null, false);
        this.mLvFooterLoadingFrame = new FrameLayout(context);
        this.mFooterLoadingView = new LoadingLayout(context, PullToRefreshBase.Mode.PULL_UP_TO_REFRESH, obtainStyledAttributes);
        this.mLvFooterLoadingFrame.addView(this.mFooterLoadingView, -1, -2);
        this.mFooterLoadingView.setVisibility(8);
        obtainStyledAttributes.recycle();
        internalListView.setId(16908298);
        return internalListView;
    }

    /* access modifiers changed from: protected */
    public int getNumberInternalFooterViews() {
        return this.mFooterLoadingView != null ? 1 : 0;
    }

    /* access modifiers changed from: protected */
    public int getNumberInternalHeaderViews() {
        return this.mHeaderLoadingView != null ? 1 : 0;
    }

    /* access modifiers changed from: protected */
    public void resetHeader() {
        boolean z;
        int i;
        int i2;
        LoadingLayout loadingLayout;
        LoadingLayout loadingLayout2;
        boolean z2 = true;
        ListAdapter adapter = ((ListView) this.mRefreshableView).getAdapter();
        if (!getShowViewWhileRefreshing() || adapter == null || adapter.isEmpty()) {
            super.resetHeader();
            return;
        }
        int headerHeight = getHeaderHeight();
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode()[getCurrentMode().ordinal()]) {
            case 2:
                LoadingLayout footerLayout = getFooterLayout();
                LoadingLayout loadingLayout3 = this.mFooterLoadingView;
                int count = ((ListView) this.mRefreshableView).getCount() - 1;
                if (((ListView) this.mRefreshableView).getLastVisiblePosition() == count) {
                    z = true;
                } else {
                    z = false;
                }
                i = count;
                i2 = headerHeight;
                loadingLayout = loadingLayout3;
                loadingLayout2 = footerLayout;
                break;
            default:
                LoadingLayout headerLayout = getHeaderLayout();
                LoadingLayout loadingLayout4 = this.mHeaderLoadingView;
                i2 = headerHeight * -1;
                if (((ListView) this.mRefreshableView).getFirstVisiblePosition() != 0) {
                    z2 = false;
                }
                z = z2;
                loadingLayout = loadingLayout4;
                i = 0;
                loadingLayout2 = headerLayout;
                break;
        }
        loadingLayout2.setVisibility(0);
        if (z && getState() != 3) {
            ((ListView) this.mRefreshableView).setSelection(i);
            setHeaderScroll(i2);
        }
        loadingLayout.setVisibility(8);
        super.resetHeader();
    }

    /* access modifiers changed from: protected */
    public void setRefreshingInternal(boolean z) {
        LoadingLayout footerLayout;
        LoadingLayout loadingLayout;
        int count;
        int scrollY;
        ListAdapter adapter = ((ListView) this.mRefreshableView).getAdapter();
        if (!getShowViewWhileRefreshing() || adapter == null || adapter.isEmpty()) {
            super.setRefreshingInternal(z);
            return;
        }
        super.setRefreshingInternal(false);
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode()[getCurrentMode().ordinal()]) {
            case 2:
                footerLayout = getFooterLayout();
                loadingLayout = this.mFooterLoadingView;
                count = ((ListView) this.mRefreshableView).getCount() - 1;
                scrollY = getScrollY() - getHeaderHeight();
                break;
            default:
                LoadingLayout headerLayout = getHeaderLayout();
                LoadingLayout loadingLayout2 = this.mHeaderLoadingView;
                scrollY = getScrollY() + getHeaderHeight();
                footerLayout = headerLayout;
                loadingLayout = loadingLayout2;
                count = 0;
                break;
        }
        if (z) {
            setHeaderScroll(scrollY);
        }
        footerLayout.setVisibility(4);
        loadingLayout.setVisibility(0);
        loadingLayout.refreshing();
        if (z) {
            ((ListView) this.mRefreshableView).setSelection(count);
            smoothScrollTo(0);
        }
    }

    class InternalListView extends ListView implements EmptyViewMethodAccessor {
        private boolean mAddedLvFooter = false;

        public InternalListView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        /* access modifiers changed from: protected */
        public void dispatchDraw(Canvas canvas) {
            try {
                super.dispatchDraw(canvas);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void draw(Canvas canvas) {
            try {
                super.draw(canvas);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public ContextMenu.ContextMenuInfo getContextMenuInfo() {
            return super.getContextMenuInfo();
        }

        public void setAdapter(ListAdapter listAdapter) {
            if (!this.mAddedLvFooter) {
                addFooterView(PullToRefreshListView.this.mLvFooterLoadingFrame, null, false);
                this.mAddedLvFooter = true;
            }
            super.setAdapter(listAdapter);
        }

        public void setEmptyView(View view) {
            PullToRefreshListView.this.setEmptyView(view);
        }

        public void setEmptyViewInternal(View view) {
            super.setEmptyView(view);
        }
    }
}
