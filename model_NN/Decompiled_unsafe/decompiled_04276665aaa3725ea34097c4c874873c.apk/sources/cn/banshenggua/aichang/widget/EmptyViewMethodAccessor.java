package cn.banshenggua.aichang.widget;

import android.view.View;

public interface EmptyViewMethodAccessor {
    void setEmptyView(View view);

    void setEmptyViewInternal(View view);
}
