package cn.banshenggua.aichang.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public abstract class PullToRefreshBase<T extends View> extends LinearLayout {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode = null;
    static final boolean DEBUG = false;
    static final Mode DEFAULT_MODE = Mode.PULL_DOWN_TO_REFRESH;
    static final float FRICTION = 2.0f;
    static final String LOG_TAG = "PullToRefresh";
    static final int MANUAL_REFRESHING = 3;
    static final int PULL_TO_REFRESH = 0;
    static final int REFRESHING = 2;
    static final int RELEASE_TO_REFRESH = 1;
    static final String STATE_CURRENT_MODE = "ptr_current_mode";
    static final String STATE_DISABLE_SCROLLING_REFRESHING = "ptr_disable_scrolling";
    static final String STATE_MODE = "ptr_mode";
    static final String STATE_SHOW_REFRESHING_VIEW = "ptr_show_refreshing_view";
    static final String STATE_STATE = "ptr_state";
    static final String STATE_SUPER = "ptr_super";
    private Mode mCurrentMode;
    private PullToRefreshBase<T>.SmoothScrollRunnable mCurrentSmoothScrollRunnable;
    private boolean mDisableScrollingWhileRefreshing = true;
    private boolean mFilterTouchEvents = true;
    private LoadingLayout mFooterLayout;
    private final Handler mHandler = new Handler();
    private int mHeaderHeight;
    private LoadingLayout mHeaderLayout;
    private float mInitialMotionY;
    private boolean mIsBeingDragged = false;
    private float mLastMotionX;
    private float mLastMotionY;
    private Mode mMode = DEFAULT_MODE;
    private OnRefreshListener mOnRefreshListener;
    private OnRefreshListener2 mOnRefreshListener2;
    private boolean mPullToRefreshEnabled = true;
    T mRefreshableView;
    private boolean mShowViewWhileRefreshing = true;
    private int mState = 0;
    private int mTouchSlop;

    public interface OnLastItemVisibleListener {
        void onLastItemVisible();
    }

    public interface OnRefreshListener {
        void onRefresh();
    }

    public interface OnRefreshListener2 {
        void onPullDownToRefresh();

        void onPullUpToRefresh();
    }

    /* access modifiers changed from: protected */
    public abstract T createRefreshableView(Context context, AttributeSet attributeSet);

    /* access modifiers changed from: protected */
    public abstract boolean isReadyForPullDown();

    /* access modifiers changed from: protected */
    public abstract boolean isReadyForPullUp();

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode;
        if (iArr == null) {
            iArr = new int[Mode.values().length];
            try {
                iArr[Mode.BOTH.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Mode.NO_BOTH.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Mode.PULL_DOWN_TO_REFRESH.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Mode.PULL_UP_TO_REFRESH.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode = iArr;
        }
        return iArr;
    }

    public PullToRefreshBase(Context context) {
        super(context);
        init(context, null);
    }

    public PullToRefreshBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context, attributeSet);
    }

    public PullToRefreshBase(Context context, Mode mode) {
        super(context);
        this.mMode = mode;
        init(context, null);
    }

    public final Mode getCurrentMode() {
        return this.mCurrentMode;
    }

    public final boolean getFilterTouchEvents() {
        return this.mFilterTouchEvents;
    }

    public final Mode getMode() {
        return this.mMode;
    }

    public final T getRefreshableView() {
        return this.mRefreshableView;
    }

    public final boolean getShowViewWhileRefreshing() {
        return this.mShowViewWhileRefreshing;
    }

    public final boolean hasPullFromTop() {
        return this.mCurrentMode == Mode.PULL_DOWN_TO_REFRESH;
    }

    public final boolean isDisableScrollingWhileRefreshing() {
        return this.mDisableScrollingWhileRefreshing;
    }

    public final boolean isPullToRefreshEnabled() {
        return this.mPullToRefreshEnabled;
    }

    public final boolean isRefreshing() {
        return this.mState == 2 || this.mState == 3;
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.mPullToRefreshEnabled) {
            return false;
        }
        if (isRefreshing() && this.mDisableScrollingWhileRefreshing) {
            return true;
        }
        int action = motionEvent.getAction();
        if (action == 3 || action == 1) {
            this.mIsBeingDragged = false;
            return false;
        } else if (action != 0 && this.mIsBeingDragged) {
            return true;
        } else {
            switch (action) {
                case 0:
                    if (isReadyForPull()) {
                        float y = motionEvent.getY();
                        this.mInitialMotionY = y;
                        this.mLastMotionY = y;
                        this.mLastMotionX = motionEvent.getX();
                        this.mIsBeingDragged = false;
                        break;
                    }
                    break;
                case 2:
                    if (isReadyForPull()) {
                        float y2 = motionEvent.getY();
                        float f = y2 - this.mLastMotionY;
                        float abs = Math.abs(f);
                        float abs2 = Math.abs(motionEvent.getX() - this.mLastMotionX);
                        if (abs > ((float) this.mTouchSlop) && (!this.mFilterTouchEvents || abs > abs2)) {
                            if (!this.mMode.canPullDown() || f < 1.0f || !isReadyForPullDown()) {
                                if (this.mMode.canPullUp() && f <= -1.0f && isReadyForPullUp()) {
                                    this.mLastMotionY = y2;
                                    this.mIsBeingDragged = true;
                                    if (this.mMode == Mode.BOTH) {
                                        this.mCurrentMode = Mode.PULL_UP_TO_REFRESH;
                                        break;
                                    }
                                }
                            } else {
                                this.mLastMotionY = y2;
                                this.mIsBeingDragged = true;
                                if (this.mMode == Mode.BOTH) {
                                    this.mCurrentMode = Mode.PULL_DOWN_TO_REFRESH;
                                    break;
                                }
                            }
                        }
                    }
                    break;
            }
            return this.mIsBeingDragged;
        }
    }

    public final void onRefreshComplete() {
        if (this.mState != 0) {
            resetHeader();
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.mPullToRefreshEnabled) {
            return false;
        }
        if (isRefreshing() && this.mDisableScrollingWhileRefreshing) {
            return true;
        }
        if (motionEvent.getAction() == 0 && motionEvent.getEdgeFlags() != 0) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                if (!isReadyForPull()) {
                    return false;
                }
                float y = motionEvent.getY();
                this.mInitialMotionY = y;
                this.mLastMotionY = y;
                return true;
            case 1:
            case 3:
                if (!this.mIsBeingDragged) {
                    return false;
                }
                this.mIsBeingDragged = false;
                if (this.mState != 1) {
                    smoothScrollTo(0);
                    return true;
                } else if (this.mOnRefreshListener != null) {
                    setRefreshingInternal(true);
                    this.mOnRefreshListener.onRefresh();
                    return true;
                } else if (this.mOnRefreshListener2 == null) {
                    return true;
                } else {
                    setRefreshingInternal(true);
                    if (this.mCurrentMode == Mode.PULL_DOWN_TO_REFRESH) {
                        this.mOnRefreshListener2.onPullDownToRefresh();
                    } else if (this.mCurrentMode == Mode.PULL_UP_TO_REFRESH) {
                        this.mOnRefreshListener2.onPullUpToRefresh();
                    }
                    return true;
                }
            case 2:
                if (!this.mIsBeingDragged) {
                    return false;
                }
                this.mLastMotionY = motionEvent.getY();
                pullEvent();
                return true;
            default:
                return false;
        }
    }

    public final void setDisableScrollingWhileRefreshing(boolean z) {
        this.mDisableScrollingWhileRefreshing = z;
    }

    public final void setFilterTouchEvents(boolean z) {
        this.mFilterTouchEvents = z;
    }

    public void setLastUpdatedLabel(CharSequence charSequence) {
        if (this.mHeaderLayout != null) {
            this.mHeaderLayout.setSubHeaderText(charSequence);
        }
        if (this.mFooterLayout != null) {
            this.mFooterLayout.setSubHeaderText(charSequence);
        }
        refreshLoadingViewsHeight();
    }

    public void setLoadingDrawable(Drawable drawable) {
        setLoadingDrawable(drawable, Mode.BOTH);
    }

    public void setLoadingDrawable(Drawable drawable, Mode mode) {
        refreshLoadingViewsHeight();
    }

    public void setLongClickable(boolean z) {
        getRefreshableView().setLongClickable(z);
    }

    public final void setMode(Mode mode) {
        if (mode != this.mMode) {
            this.mMode = mode;
            updateUIForMode();
        }
    }

    public final void setOnRefreshListener(OnRefreshListener onRefreshListener) {
        this.mOnRefreshListener = onRefreshListener;
    }

    public final void setOnRefreshListener(OnRefreshListener2 onRefreshListener2) {
        this.mOnRefreshListener2 = onRefreshListener2;
    }

    public void setPullLabel(String str) {
        setPullLabel(str, Mode.BOTH);
    }

    public void setPullLabel(String str, Mode mode) {
        if (this.mHeaderLayout != null && mode.canPullDown()) {
            this.mHeaderLayout.setPullLabel(str);
        }
        if (this.mFooterLayout != null && mode.canPullUp()) {
            this.mFooterLayout.setPullLabel(str);
        }
    }

    public final void setPullToRefreshEnabled(boolean z) {
        this.mPullToRefreshEnabled = z;
    }

    public final void setRefreshing() {
        setRefreshing(true);
    }

    public final void setRefreshing(boolean z) {
        if (!isRefreshing()) {
            setRefreshingInternal(z);
            this.mState = 3;
        }
    }

    public void setRefreshingLabel(String str) {
        setRefreshingLabel(str, Mode.BOTH);
    }

    public void setRefreshingLabel(String str, Mode mode) {
        if (this.mHeaderLayout != null && mode.canPullDown()) {
            this.mHeaderLayout.setRefreshingLabel(str);
        }
        if (this.mFooterLayout != null && mode.canPullUp()) {
            this.mFooterLayout.setRefreshingLabel(str);
        }
    }

    public void setReleaseLabel(String str) {
        setReleaseLabel(str, Mode.BOTH);
    }

    public void setReleaseLabel(String str, Mode mode) {
        if (this.mHeaderLayout != null && mode.canPullDown()) {
            this.mHeaderLayout.setReleaseLabel(str);
        }
        if (this.mFooterLayout != null && mode.canPullUp()) {
            this.mFooterLayout.setReleaseLabel(str);
        }
    }

    public final void setShowViewWhileRefreshing(boolean z) {
        this.mShowViewWhileRefreshing = z;
    }

    /* access modifiers changed from: protected */
    public void addRefreshableView(Context context, T t) {
        addView(t, new LinearLayout.LayoutParams(-1, 0, 1.0f));
    }

    /* access modifiers changed from: protected */
    public final LoadingLayout getFooterLayout() {
        return this.mFooterLayout;
    }

    /* access modifiers changed from: protected */
    public final int getHeaderHeight() {
        return this.mHeaderHeight;
    }

    /* access modifiers changed from: protected */
    public final LoadingLayout getHeaderLayout() {
        return this.mHeaderLayout;
    }

    /* access modifiers changed from: protected */
    public final int getState() {
        return this.mState;
    }

    /* access modifiers changed from: protected */
    public void handleStyledAttributes(TypedArray typedArray) {
    }

    /* access modifiers changed from: protected */
    public void onPullToRefresh() {
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode()[this.mCurrentMode.ordinal()]) {
            case 1:
                this.mHeaderLayout.pullToRefresh();
                return;
            case 2:
                this.mFooterLayout.pullToRefresh();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onReleaseToRefresh() {
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode()[this.mCurrentMode.ordinal()]) {
            case 1:
                this.mHeaderLayout.releaseToRefresh();
                return;
            case 2:
                this.mFooterLayout.releaseToRefresh();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.mMode = Mode.mapIntToMode(bundle.getInt(STATE_MODE, 0));
            this.mCurrentMode = Mode.mapIntToMode(bundle.getInt(STATE_CURRENT_MODE, 0));
            this.mDisableScrollingWhileRefreshing = bundle.getBoolean(STATE_DISABLE_SCROLLING_REFRESHING, true);
            this.mShowViewWhileRefreshing = bundle.getBoolean(STATE_SHOW_REFRESHING_VIEW, true);
            super.onRestoreInstanceState(bundle.getParcelable(STATE_SUPER));
            int i = bundle.getInt(STATE_STATE, 0);
            if (i == 2) {
                setRefreshingInternal(true);
                this.mState = i;
                return;
            }
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putInt(STATE_STATE, this.mState);
        bundle.putInt(STATE_MODE, this.mMode.getIntValue());
        bundle.putInt(STATE_CURRENT_MODE, this.mCurrentMode.getIntValue());
        bundle.putBoolean(STATE_DISABLE_SCROLLING_REFRESHING, this.mDisableScrollingWhileRefreshing);
        bundle.putBoolean(STATE_SHOW_REFRESHING_VIEW, this.mShowViewWhileRefreshing);
        bundle.putParcelable(STATE_SUPER, super.onSaveInstanceState());
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void resetHeader() {
        this.mState = 0;
        this.mIsBeingDragged = false;
        if (this.mMode.canPullDown()) {
            this.mHeaderLayout.reset();
        }
        if (this.mMode.canPullUp()) {
            this.mFooterLayout.reset();
        }
        smoothScrollTo(0);
    }

    /* access modifiers changed from: protected */
    public final void setHeaderScroll(int i) {
        scrollTo(0, i);
    }

    /* access modifiers changed from: protected */
    public void setRefreshingInternal(boolean z) {
        int i;
        this.mState = 2;
        if (this.mMode.canPullDown()) {
            this.mHeaderLayout.refreshing();
        }
        if (this.mMode.canPullUp()) {
            this.mFooterLayout.refreshing();
        }
        if (!z) {
            return;
        }
        if (this.mShowViewWhileRefreshing) {
            if (this.mCurrentMode == Mode.PULL_DOWN_TO_REFRESH) {
                i = -this.mHeaderHeight;
            } else {
                i = this.mHeaderHeight;
            }
            smoothScrollTo(i);
            return;
        }
        smoothScrollTo(0);
    }

    /* access modifiers changed from: protected */
    public final void smoothScrollTo(int i) {
        if (this.mCurrentSmoothScrollRunnable != null) {
            this.mCurrentSmoothScrollRunnable.stop();
        }
        if (getScrollY() != i) {
            this.mCurrentSmoothScrollRunnable = new SmoothScrollRunnable(this.mHandler, getScrollY(), i);
            this.mHandler.post(this.mCurrentSmoothScrollRunnable);
        }
    }

    /* access modifiers changed from: protected */
    public void updateUIForMode() {
        if (this == this.mHeaderLayout.getParent()) {
            removeView(this.mHeaderLayout);
        }
        if (this.mMode.canPullDown()) {
            addView(this.mHeaderLayout, 0, new LinearLayout.LayoutParams(-1, -2));
        }
        if (this == this.mFooterLayout.getParent()) {
            removeView(this.mFooterLayout);
        }
        if (this.mMode.canPullUp()) {
            addView(this.mFooterLayout, new LinearLayout.LayoutParams(-1, -2));
        }
        refreshLoadingViewsHeight();
        this.mCurrentMode = this.mMode != Mode.BOTH ? this.mMode : Mode.PULL_DOWN_TO_REFRESH;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void init(android.content.Context r4, android.util.AttributeSet r5) {
        /*
            r3 = this;
            r0 = 1
            r3.setOrientation(r0)
            android.view.ViewConfiguration r0 = android.view.ViewConfiguration.get(r4)
            int r0 = r0.getScaledTouchSlop()
            r3.mTouchSlop = r0
            int[] r0 = cn.a.a.a.j.PullToRefresh
            android.content.res.TypedArray r0 = r4.obtainStyledAttributes(r5, r0)
            r3.handleStyledAttributes(r0)
            int r1 = cn.a.a.a.j.PullToRefresh_ptrMode
            boolean r1 = r0.hasValue(r1)
            if (r1 == 0) goto L_0x002c
            int r1 = cn.a.a.a.j.PullToRefresh_ptrMode
            r2 = 0
            int r1 = r0.getInteger(r1, r2)
            cn.banshenggua.aichang.widget.PullToRefreshBase$Mode r1 = cn.banshenggua.aichang.widget.PullToRefreshBase.Mode.mapIntToMode(r1)
            r3.mMode = r1
        L_0x002c:
            android.view.View r1 = r3.createRefreshableView(r4, r5)
            r3.mRefreshableView = r1
            T r1 = r3.mRefreshableView
            r3.addRefreshableView(r4, r1)
            cn.banshenggua.aichang.widget.LoadingLayout r1 = new cn.banshenggua.aichang.widget.LoadingLayout
            cn.banshenggua.aichang.widget.PullToRefreshBase$Mode r2 = cn.banshenggua.aichang.widget.PullToRefreshBase.Mode.PULL_DOWN_TO_REFRESH
            r1.<init>(r4, r2, r0)
            r3.mHeaderLayout = r1
            cn.banshenggua.aichang.widget.LoadingLayout r1 = new cn.banshenggua.aichang.widget.LoadingLayout
            cn.banshenggua.aichang.widget.PullToRefreshBase$Mode r2 = cn.banshenggua.aichang.widget.PullToRefreshBase.Mode.PULL_UP_TO_REFRESH
            r1.<init>(r4, r2, r0)
            r3.mFooterLayout = r1
            r3.updateUIForMode()
            int r1 = cn.a.a.a.j.PullToRefresh_ptrHeaderBackground
            boolean r1 = r0.hasValue(r1)
            if (r1 == 0) goto L_0x005f
            int r1 = cn.a.a.a.j.PullToRefresh_ptrHeaderBackground
            android.graphics.drawable.Drawable r1 = r0.getDrawable(r1)
            if (r1 == 0) goto L_0x005f
            r3.setBackgroundDrawable(r1)
        L_0x005f:
            int r1 = cn.a.a.a.j.PullToRefresh_ptrAdapterViewBackground
            boolean r1 = r0.hasValue(r1)
            if (r1 == 0) goto L_0x0074
            int r1 = cn.a.a.a.j.PullToRefresh_ptrAdapterViewBackground
            android.graphics.drawable.Drawable r1 = r0.getDrawable(r1)
            if (r1 == 0) goto L_0x0074
            T r2 = r3.mRefreshableView
            r2.setBackgroundDrawable(r1)
        L_0x0074:
            r0.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.banshenggua.aichang.widget.PullToRefreshBase.init(android.content.Context, android.util.AttributeSet):void");
    }

    private boolean isReadyForPull() {
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode()[this.mMode.ordinal()]) {
            case 1:
                return isReadyForPullDown();
            case 2:
                return isReadyForPullUp();
            case 3:
                return isReadyForPullUp() || isReadyForPullDown();
            default:
                return false;
        }
    }

    private void measureView(View view) {
        int makeMeasureSpec;
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(-1, -2);
        }
        int childMeasureSpec = ViewGroup.getChildMeasureSpec(0, 0, layoutParams.width);
        int i = layoutParams.height;
        if (i > 0) {
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i, NTLMConstants.FLAG_NEGOTIATE_KEY_EXCHANGE);
        } else {
            makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        view.measure(childMeasureSpec, makeMeasureSpec);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private boolean pullEvent() {
        int round;
        int scrollY = getScrollY();
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode()[this.mCurrentMode.ordinal()]) {
            case 2:
                round = Math.round(Math.max(this.mInitialMotionY - this.mLastMotionY, 0.0f) / FRICTION);
                break;
            default:
                round = Math.round(Math.min(this.mInitialMotionY - this.mLastMotionY, 0.0f) / FRICTION);
                break;
        }
        setHeaderScroll(round);
        if (round != 0) {
            float abs = ((float) Math.abs(round)) / ((float) this.mHeaderHeight);
            if (this.mState == 0 && this.mHeaderHeight < Math.abs(round)) {
                this.mState = 1;
                onReleaseToRefresh();
                return true;
            } else if (this.mState == 1 && this.mHeaderHeight >= Math.abs(round)) {
                this.mState = 0;
                onPullToRefresh();
                return true;
            }
        }
        return scrollY != round;
    }

    private void refreshLoadingViewsHeight() {
        if (this.mMode.canPullDown()) {
            measureView(this.mHeaderLayout);
            this.mHeaderHeight = this.mHeaderLayout.getMeasuredHeight();
        } else if (this.mMode.canPullUp()) {
            measureView(this.mFooterLayout);
            this.mHeaderHeight = this.mFooterLayout.getMeasuredHeight();
        }
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$widget$PullToRefreshBase$Mode()[this.mMode.ordinal()]) {
            case 2:
                setPadding(0, 0, 0, -this.mHeaderHeight);
                return;
            case 3:
                setPadding(0, -this.mHeaderHeight, 0, -this.mHeaderHeight);
                return;
            default:
                setPadding(0, -this.mHeaderHeight, 0, 0);
                return;
        }
    }

    public enum Mode {
        PULL_DOWN_TO_REFRESH(1),
        PULL_UP_TO_REFRESH(2),
        BOTH(3),
        NO_BOTH(4);
        
        private int mIntValue;

        public static Mode mapIntToMode(int i) {
            switch (i) {
                case 2:
                    return PULL_UP_TO_REFRESH;
                case 3:
                    return BOTH;
                case 4:
                    return NO_BOTH;
                default:
                    return PULL_DOWN_TO_REFRESH;
            }
        }

        private Mode(int i) {
            this.mIntValue = i;
        }

        /* access modifiers changed from: package-private */
        public boolean canPullDown() {
            return this == PULL_DOWN_TO_REFRESH || this == BOTH;
        }

        /* access modifiers changed from: package-private */
        public boolean canPullUp() {
            return this == PULL_UP_TO_REFRESH || this == BOTH;
        }

        /* access modifiers changed from: package-private */
        public int getIntValue() {
            return this.mIntValue;
        }
    }

    final class SmoothScrollRunnable implements Runnable {
        static final int ANIMATION_DURATION_MS = 190;
        static final int ANIMATION_FPS = 16;
        private boolean mContinueRunning = true;
        private int mCurrentY = -1;
        private final Handler mHandler;
        private final Interpolator mInterpolator;
        private final int mScrollFromY;
        private final int mScrollToY;
        private long mStartTime = -1;

        public SmoothScrollRunnable(Handler handler, int i, int i2) {
            this.mHandler = handler;
            this.mScrollFromY = i;
            this.mScrollToY = i2;
            this.mInterpolator = new AccelerateDecelerateInterpolator();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(float, float):float}
          ClspMth{java.lang.Math.max(long, long):long} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(float, float):float}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(long, long):long} */
        public void run() {
            if (this.mStartTime == -1) {
                this.mStartTime = System.currentTimeMillis();
            } else {
                this.mCurrentY = this.mScrollFromY - Math.round(this.mInterpolator.getInterpolation(((float) Math.max(Math.min(((System.currentTimeMillis() - this.mStartTime) * 1000) / 190, 1000L), 0L)) / 1000.0f) * ((float) (this.mScrollFromY - this.mScrollToY)));
                PullToRefreshBase.this.setHeaderScroll(this.mCurrentY);
            }
            if (this.mContinueRunning && this.mScrollToY != this.mCurrentY) {
                this.mHandler.postDelayed(this, 16);
            }
        }

        public void stop() {
            this.mContinueRunning = false;
            this.mHandler.removeCallbacks(this);
        }
    }
}
