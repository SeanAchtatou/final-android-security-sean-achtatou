package cn.banshenggua.aichang.player;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.utils.ULog;
import com.baidu.mobads.interfaces.utils.IXAdSystemUtils;
import com.pocketmusic.kshare.requestobjs.v;

public class PlayerService extends Service {
    static final /* synthetic */ boolean $assertionsDisabled;
    public static final String ACTION_BIND_LISTENER = "bind_listener";
    public static final String ACTION_NEXT = "next";
    public static final String ACTION_PLAY = "play";
    public static final String ACTION_PREV = "prev";
    public static final String ACTION_STOP = "stop";
    private static final String LASTFM_INTENT = "fm.last.android.metachanged";
    public static final int PLAYING_NOTIFY_ID = 667667;
    private static final String SIMPLEFM_INTENT = "com.adam.aslfms.notify.playstatechanged";
    private static final String TAG = "PlayerService";
    private PlayerEngineListener mLocalEngineListener = new PlayerEngineListener() {
        public void onTrackBuffering(int i) {
            if (PlayerService.this.mRemoteEngineListener != null) {
                PlayerService.this.mRemoteEngineListener.onTrackBuffering(i);
            }
        }

        public void onTrackChanged(v vVar) {
            PlayerService.this.displayNotifcation(vVar);
            if (PlayerService.this.mRemoteEngineListener != null) {
                PlayerService.this.mRemoteEngineListener.onTrackChanged(vVar);
            }
            if (PreferenceManager.getDefaultSharedPreferences(PlayerService.this).getBoolean("scrobbling_enabled", false)) {
                PlayerService.this.scrobblerMetaChanged();
            }
        }

        public void onTrackProgress(int i) {
            if (PlayerService.this.mRemoteEngineListener != null) {
                PlayerService.this.mRemoteEngineListener.onTrackProgress(i);
            }
        }

        public void onTrackStop() {
            PlayerService.this.mWifiLock.release();
            PlayerService.this.mNotificationManager.cancel(PlayerService.PLAYING_NOTIFY_ID);
            if (PlayerService.this.mRemoteEngineListener != null) {
                PlayerService.this.mRemoteEngineListener.onTrackStop();
            }
        }

        public boolean onTrackStart() {
            PlayerService.this.mWifiLock.acquire();
            if (PlayerService.this.mRemoteEngineListener != null && !PlayerService.this.mRemoteEngineListener.onTrackStart()) {
                return false;
            }
            if (PreferenceManager.getDefaultSharedPreferences(PlayerService.this).getBoolean("wifi_only", false) && !PlayerService.this.mWifiManager.isWifiEnabled()) {
                return false;
            }
            boolean z = PreferenceManager.getDefaultSharedPreferences(PlayerService.this).getBoolean("roaming_protection", true);
            if (PlayerService.this.mWifiManager.isWifiEnabled() || !z || !PlayerService.this.mTelephonyManager.isNetworkRoaming()) {
                return true;
            }
            return false;
        }

        public void onTrackPause() {
            if (PlayerService.this.mRemoteEngineListener != null) {
                PlayerService.this.mRemoteEngineListener.onTrackPause();
            }
        }

        public void onTrackStreamError() {
            if (PlayerService.this.mRemoteEngineListener != null) {
                PlayerService.this.mRemoteEngineListener.onTrackStreamError();
            }
        }

        public void onTrackPlay() {
            if (PlayerService.this.mRemoteEngineListener != null) {
                PlayerService.this.mRemoteEngineListener.onTrackPlay();
            }
        }

        public void onVideoSizeChange(int i, int i2) {
            if (PlayerService.this.mRemoteEngineListener != null) {
                PlayerService.this.mRemoteEngineListener.onVideoSizeChange(i, i2);
            }
        }
    };
    /* access modifiers changed from: private */
    public NotificationManager mNotificationManager = null;
    private PhoneStateListener mPhoneStateListener;
    /* access modifiers changed from: private */
    public PlayerEngine mPlayerEngine;
    /* access modifiers changed from: private */
    public PlayerEngineListener mRemoteEngineListener;
    /* access modifiers changed from: private */
    public TelephonyManager mTelephonyManager;
    /* access modifiers changed from: private */
    public WifiManager.WifiLock mWifiLock;
    /* access modifiers changed from: private */
    public WifiManager mWifiManager;

    static {
        boolean z;
        if (!PlayerService.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        $assertionsDisabled = z;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        ULog.i(TAG, "Player Service onCreate");
        this.mPlayerEngine = new PlayerEngineImpl();
        this.mPlayerEngine.setListener(this.mLocalEngineListener);
        this.mTelephonyManager = (TelephonyManager) getSystemService("phone");
        this.mPhoneStateListener = new PhoneStateListener() {
            public void onCallStateChanged(int i, String str) {
                ULog.e(PlayerService.TAG, "onCallStateChanged");
                if (i != 0 && PlayerService.this.mPlayerEngine != null) {
                    PlayerService.this.mPlayerEngine.pause();
                }
            }
        };
        this.mTelephonyManager.listen(this.mPhoneStateListener, 32);
        this.mNotificationManager = (NotificationManager) getSystemService("notification");
        this.mWifiManager = (WifiManager) getSystemService(IXAdSystemUtils.NT_WIFI);
        this.mWifiLock = this.mWifiManager.createWifiLock(TAG);
        this.mWifiLock.setReferenceCounted(false);
        try {
            KShareApplication.getInstance().setConcretePlayerEngine(this.mPlayerEngine);
            this.mRemoteEngineListener = KShareApplication.getInstance().fetchPlayerEngineListener();
        } catch (ACException e) {
        }
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        if (intent != null) {
            String action = intent.getAction();
            ULog.i(TAG, "Player Service onStart - " + action);
            if (action.equals(ACTION_STOP)) {
                stopSelfResult(i);
            } else if (action.equals(ACTION_BIND_LISTENER)) {
                try {
                    this.mRemoteEngineListener = KShareApplication.getInstance().fetchPlayerEngineListener();
                } catch (ACException e) {
                    e.printStackTrace();
                }
            } else {
                updatePlaylist();
                if (action.equals(ACTION_PLAY)) {
                    try {
                        this.mPlayerEngine.setPlayerSurfaceHolder(KShareApplication.getInstance().mSurfaceHolder);
                    } catch (ACException e2) {
                        e2.printStackTrace();
                    }
                    this.mPlayerEngine.play();
                } else if (action.equals(ACTION_NEXT)) {
                    this.mPlayerEngine.next();
                } else if (action.equals(ACTION_PREV)) {
                    this.mPlayerEngine.prev();
                }
            }
        }
    }

    private void updatePlaylist() {
        try {
            if (this.mPlayerEngine.getPlaylist() != KShareApplication.getInstance().getPlaylist()) {
                this.mPlayerEngine.openPlaylist(KShareApplication.getInstance().getPlaylist());
            }
        } catch (ACException e) {
        }
    }

    public void onDestroy() {
        ULog.i(TAG, "Player Service onDestroy");
        try {
            KShareApplication.getInstance().setConcretePlayerEngine(null);
        } catch (ACException e) {
            e.printStackTrace();
        }
        this.mPlayerEngine.stop();
        this.mPlayerEngine = null;
        this.mTelephonyManager.listen(this.mPhoneStateListener, 0);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void scrobblerMetaChanged() {
        v selectedTrack = this.mPlayerEngine.getPlaylist().getSelectedTrack();
        if (selectedTrack != null) {
            String string = PreferenceManager.getDefaultSharedPreferences(this).getString("scrobbler_app", "");
            if ($assertionsDisabled || string.length() > 0) {
                ULog.i(TAG, "Scrobbling track " + selectedTrack.A + " via " + string);
                return;
            }
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: private */
    public void displayNotifcation(v vVar) {
    }
}
