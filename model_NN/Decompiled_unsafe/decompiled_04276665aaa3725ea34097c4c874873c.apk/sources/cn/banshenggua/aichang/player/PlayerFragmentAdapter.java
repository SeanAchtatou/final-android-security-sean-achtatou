package cn.banshenggua.aichang.player;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import com.pocketmusic.kshare.requestobjs.v;

class PlayerFragmentAdapter extends FragmentPagerAdapter {
    private v inputWeibo;
    private int mCount = 1;
    private PlayerPhotoFragment mPlayerPhotoFragment;
    private View.OnClickListener userPhotoAreaOnClickListener;

    public PlayerFragmentAdapter(FragmentManager fragmentManager, v vVar) {
        super(fragmentManager);
        this.inputWeibo = vVar;
    }

    public Fragment getItem(int i) {
        if (this.mPlayerPhotoFragment != null) {
            return this.mPlayerPhotoFragment;
        }
        PlayerPhotoFragment newInstance = PlayerPhotoFragment.newInstance(this.inputWeibo, this.userPhotoAreaOnClickListener);
        this.mPlayerPhotoFragment = newInstance;
        return newInstance;
    }

    public int getCount() {
        return this.mCount;
    }

    public void resetData(v vVar) {
        this.inputWeibo = vVar;
        if (this.mPlayerPhotoFragment != null) {
            this.mPlayerPhotoFragment.resetData(vVar);
        }
    }

    public PlayerPhotoFragment getmPlayerPhotoFragment() {
        return this.mPlayerPhotoFragment;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.userPhotoAreaOnClickListener = onClickListener;
    }
}
