package cn.banshenggua.aichang.player;

import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {
    public abstract void cleanup();

    public abstract int getListSize();

    public abstract void notifyDataSetChanged();

    public abstract void setMode(int i);
}
