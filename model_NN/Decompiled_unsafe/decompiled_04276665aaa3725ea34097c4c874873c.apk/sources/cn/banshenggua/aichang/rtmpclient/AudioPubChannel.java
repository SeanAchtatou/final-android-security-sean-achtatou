package cn.banshenggua.aichang.rtmpclient;

import android.text.TextUtils;
import com.baidu.mobads.interfaces.IXAdRequestInfo;

public class AudioPubChannel extends Channel implements PubInterface {
    public AudioPubChannel(String str, String[] strArr) {
        super(str, strArr);
    }

    public void startChannel() {
    }

    public void stopChannel() {
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("sid"))) {
            try {
                Integer.parseInt((String) this.mFinger.get("sid"));
            } catch (Exception e) {
            }
        }
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get(IXAdRequestInfo.CELL_ID))) {
            try {
                Integer.parseInt((String) this.mFinger.get(IXAdRequestInfo.CELL_ID));
            } catch (Exception e2) {
            }
        }
        this.shouldStop = true;
    }

    public void pushdata(byte[] bArr, int i) {
        pushdata(bArr, i, 0, false);
    }

    public void pushdata(byte[] bArr, int i, long j, boolean z) {
        int i2;
        int i3 = 0;
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("sid"))) {
            try {
                i2 = Integer.parseInt((String) this.mFinger.get("sid"));
            } catch (Exception e) {
                i2 = 0;
            }
        } else {
            i2 = 0;
        }
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get(IXAdRequestInfo.CELL_ID))) {
            try {
                i3 = Integer.parseInt((String) this.mFinger.get(IXAdRequestInfo.CELL_ID));
            } catch (Exception e2) {
            }
        }
        if (this.manager2 != null && !this.shouldStop) {
            this.manager2.pushdata(i3, i2, true, bArr, i, j, z);
        }
    }

    public void stop() {
        stopChannel();
    }

    public void start() {
        startChannel();
    }

    public boolean isConnected() {
        int i;
        if (this.manager2 == null) {
            return false;
        }
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get(IXAdRequestInfo.CELL_ID))) {
            try {
                i = Integer.parseInt((String) this.mFinger.get(IXAdRequestInfo.CELL_ID));
            } catch (Exception e) {
                return false;
            }
        } else {
            i = 0;
        }
        return this.manager2.isConnected(i);
    }

    public void updateVideoSize(VideoSize videoSize, VideoSize videoSize2, int i, int i2) {
    }
}
