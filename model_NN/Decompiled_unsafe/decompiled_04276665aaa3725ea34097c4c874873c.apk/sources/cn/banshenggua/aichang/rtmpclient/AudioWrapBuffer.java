package cn.banshenggua.aichang.rtmpclient;

public class AudioWrapBuffer {
    int capacity;

    private native void dealloc();

    private native void init(int i);

    public native void clear();

    public native int contentSize();

    public native boolean mark(int i);

    public native int position();

    public native int read(byte[] bArr, int i);

    public native int read(byte[] bArr, int i, int i2);

    public native int readFromMark(byte[] bArr, int i);

    public native int write(byte[] bArr, int i);

    public AudioWrapBuffer(int i) {
        init(i);
        this.capacity = i;
    }

    public int getCapacity() {
        return this.capacity;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        dealloc();
        super.finalize();
    }
}
