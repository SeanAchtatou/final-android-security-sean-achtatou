package cn.banshenggua.aichang.rtmpclient;

public class DataSizeTime {
    public int size = 0;
    public long time = 0;

    public String toString() {
        return "size: " + this.size + "; time: " + this.time;
    }
}
