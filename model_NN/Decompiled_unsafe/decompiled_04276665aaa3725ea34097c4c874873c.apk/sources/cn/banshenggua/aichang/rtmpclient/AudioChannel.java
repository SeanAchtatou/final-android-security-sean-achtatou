package cn.banshenggua.aichang.rtmpclient;

import android.media.AudioTrack;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import cn.banshenggua.aichang.utils.ULog;
import com.baidu.mobads.interfaces.IXAdRequestInfo;

public class AudioChannel extends Channel implements Runnable {
    private Runnable getDataRun = new Runnable() {
        public void run() {
            int i;
            int i2 = 0;
            if (!TextUtils.isEmpty((CharSequence) AudioChannel.this.mFinger.get("sid"))) {
                try {
                    i = Integer.parseInt((String) AudioChannel.this.mFinger.get("sid"));
                } catch (Exception e) {
                    i = 0;
                }
            } else {
                i = 0;
            }
            if (!TextUtils.isEmpty((CharSequence) AudioChannel.this.mFinger.get(IXAdRequestInfo.CELL_ID))) {
                try {
                    i2 = Integer.parseInt((String) AudioChannel.this.mFinger.get(IXAdRequestInfo.CELL_ID));
                } catch (Exception e2) {
                }
            }
            Log.d("luolei", "xxxxxx audio track runget data");
            while (!AudioChannel.this.shouldStop) {
                if (AudioChannel.this.manager2 != null) {
                    AudioChannel.this.manager2.rungetdata(i2, i, true);
                }
                if (AudioChannel.this.mPlayer != null && AudioChannel.this.mPlayer.getPlayState() == 3) {
                    AudioChannel.this.mPlayTime = ((long) ((((float) AudioChannel.this.mPlayer.getPlaybackHeadPosition()) / ((float) AudioChannel.this.mPlayer.getPlaybackRate())) * 1000.0f)) + AudioChannel.this.mBeginTime;
                }
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
            Log.d("luolei", "xxxxxx audio track runget data end");
        }
    };
    boolean isRunning = false;
    /* access modifiers changed from: private */
    public long mBeginTime = 0;
    Thread mGetDataThread;
    public OnSubscriberListener mListener = null;
    /* access modifiers changed from: private */
    public long mPlayTime = 0;
    AudioTrack mPlayer;
    Thread mThread;

    enum Status {
        STOP,
        PLAY,
        Load,
        NORMAL
    }

    public AudioChannel(String str, String[] strArr) {
        super(str, strArr);
    }

    public AudioChannel(String str, String[] strArr, int i, int i2) {
        super(str, strArr);
        setSid(i);
        setCid(i2);
    }

    public void setListener(OnSubscriberListener onSubscriberListener) {
        this.mListener = onSubscriberListener;
    }

    public void prepare() {
        if (this.isRunning) {
        }
    }

    public void startChannel(boolean z) {
        if (z) {
            this.mGetDataThread = new Thread(this.getDataRun);
            this.mGetDataThread.start();
        }
        this.mThread = new Thread(this);
        this.mThread.setPriority(10);
        this.mThread.start();
    }

    public long getPlayTime() {
        return this.mPlayTime;
    }

    private int initPlayer() {
        int minBufferSize = AudioTrack.getMinBufferSize(24000, 3, 2);
        this.mPlayer = new AudioTrack(3, 24000, 3, 2, minBufferSize * 5, 1);
        ULog.d("luoleiminibuffer", "minBufferSize: " + minBufferSize);
        return minBufferSize;
    }

    public void run() {
        int i;
        Status status;
        Process.setThreadPriority(-19);
        int initPlayer = initPlayer();
        this.mPlayTime = 0;
        this.mBeginTime = -1;
        byte[] bArr = new byte[(initPlayer * 2)];
        int i2 = 0;
        int i3 = 0;
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("sid"))) {
            try {
                i2 = Integer.parseInt((String) this.mFinger.get("sid"));
            } catch (Exception e) {
            }
        }
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get(IXAdRequestInfo.CELL_ID))) {
            try {
                i3 = Integer.parseInt((String) this.mFinger.get(IXAdRequestInfo.CELL_ID));
            } catch (Exception e2) {
            }
        }
        Log.d("luolei", "xxxxxxxx audio channel begin " + this.mPlayer.getState() + "; cid = " + i3 + "; sid = " + i2);
        Status status2 = Status.NORMAL;
        if (!(this.mListener == null || status2 == Status.Load)) {
            this.mListener.OnLoading();
        }
        Status status3 = Status.Load;
        try {
            Thread.sleep(500);
        } catch (Exception e3) {
        }
        Status status4 = status3;
        int i4 = 0;
        int i5 = initPlayer;
        boolean z = false;
        int i6 = -1;
        int i7 = 0;
        while (!this.shouldStop) {
            int i8 = i5 * 1;
            if (i7 == 0) {
                ULog.d("luolei", "audio channel run rsize: " + 0);
            }
            if (this.manager != null) {
                i = this.manager.pulldata(i2, bArr, i8);
            } else {
                i = 0;
            }
            if (this.manager2 != null) {
                DataSizeTime pulldatatime = this.manager2.pulldatatime(i3, i2, bArr, i8, true);
                if (i7 == 0) {
                    ULog.d("luoleiAudioChannel", "jni_pulldatatime end: " + pulldatatime + "; " + "; playtime: " + this.mPlayTime + "; chatime: " + (pulldatatime.time - this.mPlayTime));
                }
                if (pulldatatime != null) {
                    i = pulldatatime.size;
                    if (pulldatatime.time > 0) {
                        if (this.mBeginTime < 0) {
                            this.mBeginTime = pulldatatime.time;
                        } else if (this.mPlayTime - this.mBeginTime > 0 && pulldatatime.time - this.mPlayTime > 500) {
                            ULog.d("luoleiAudioChannel", "adjust time: " + this.mBeginTime + "; +time" + ((pulldatatime.time - this.mPlayTime) - 600));
                            this.mBeginTime = this.mBeginTime + ((pulldatatime.time - this.mPlayTime) - 500);
                        }
                    }
                }
            }
            if (i7 == 0) {
                ULog.d("luoleiAudioChannel", "audio channel run rsize: " + i);
            }
            int i9 = i7 + 1;
            if (i7 > 50) {
                i9 = 0;
            }
            if (i == 0) {
                int i10 = i6 + 1;
                ULog.d("luolei", "read size = 0; sizeCount: " + i10);
                try {
                    Thread.sleep(50);
                } catch (Exception e4) {
                }
                if (i10 > 5 || i10 == -1) {
                    if (!(this.mListener == null || status4 == Status.Load)) {
                        this.mListener.OnLoading();
                    }
                    status = Status.Load;
                } else {
                    status = status4;
                }
                if (i10 > 150) {
                    if (this.mListener != null && !z) {
                        this.mListener.OnError(0);
                        z = true;
                    }
                    z = z;
                    i7 = i9;
                    status4 = status;
                    i6 = 0;
                } else {
                    i7 = i9;
                    status4 = status;
                    i6 = i10;
                }
            } else if (this.mPlayer.getState() != 1) {
                ULog.d("luolei", "mPlayer getState: " + this.mPlayer.getState());
                try {
                    Thread.sleep(100);
                } catch (Exception e5) {
                }
                int i11 = i4 + 1;
                if (i4 > 10) {
                    int initPlayer2 = initPlayer();
                    this.mListener.OnError(100);
                    i7 = i9;
                    i4 = 0;
                    i5 = initPlayer2;
                } else {
                    i7 = i9;
                    i4 = i11;
                }
            } else {
                if (!(this.mListener == null || status4 == Status.PLAY)) {
                    this.mListener.OnPlaying();
                }
                Status status5 = Status.PLAY;
                if (i <= 0) {
                    try {
                        Thread.sleep(100);
                        i6 = 0;
                        i7 = i9;
                        status4 = status5;
                    } catch (InterruptedException e6) {
                        e6.printStackTrace();
                        i6 = 0;
                        i7 = i9;
                        status4 = status5;
                    }
                } else if (this.mPlayer.getPlayState() != 3) {
                    this.mPlayer.write(bArr, 0, i);
                    this.mPlayer.play();
                    i6 = 0;
                    i7 = i9;
                    status4 = status5;
                } else {
                    this.mPlayer.write(bArr, 0, i);
                    i6 = 0;
                    i7 = i9;
                    status4 = status5;
                }
            }
        }
        if (this.mListener != null && !z) {
            this.mListener.OnFinish();
        }
        if (this.mPlayer.getState() != 0) {
            this.mPlayer.stop();
        }
    }

    public void stopChannel() {
        this.shouldStop = true;
    }

    public void startChannel() {
        startChannel(false);
    }
}
