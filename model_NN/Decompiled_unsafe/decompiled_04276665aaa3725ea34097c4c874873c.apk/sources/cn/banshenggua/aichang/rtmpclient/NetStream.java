package cn.banshenggua.aichang.rtmpclient;

import android.text.TextUtils;
import cn.banshenggua.aichang.utils.ULog;

public class NetStream {
    Channel audioChannel = null;
    public boolean isInput = false;
    public RtmpClientManager2 manager2;
    public String name;
    public int sid = 0;
    Channel videoChannel = null;

    public void setSid(int i) {
        this.sid = i;
    }

    public int getSid() {
        return this.sid;
    }

    public NetStream(String str, Channel channel, Channel channel2, int i) {
        this.name = str;
        this.audioChannel = channel;
        this.videoChannel = channel2;
        this.isInput = false;
        this.sid = i;
    }

    public NetStream(String str, Channel channel, Channel channel2) {
        this.name = str;
        this.audioChannel = channel;
        this.videoChannel = channel2;
        this.isInput = false;
    }

    public NetStream(String str, Channel channel, Channel channel2, boolean z) {
        this.name = str;
        this.audioChannel = channel;
        this.videoChannel = channel2;
        this.isInput = z;
    }

    public void setChannel(boolean z, Channel channel) {
        if (z) {
            this.audioChannel = channel;
        } else {
            this.videoChannel = channel;
        }
    }

    public void stopChannel() {
        ULog.d("luoleixxxxxxxxx", "stopChannel: ");
        if (this.audioChannel != null) {
            ULog.d("luoleixxxxxxxxx", "stopChannel: audio");
            this.audioChannel.stopChannel();
        }
        if (this.videoChannel != null) {
            ULog.d("luoleixxxxxxxxx", "stopChannel: video");
            this.videoChannel.stopChannel();
        }
    }

    public void stopAudioChannel() {
        if (this.audioChannel != null) {
            ULog.d("luoleixxxxxxxxx", "stopChannel: audio");
            this.audioChannel.stopChannel();
        }
    }

    public void stopVideoChannel() {
        if (this.videoChannel != null) {
            ULog.d("luoleixxxxxxxxx", "stopChannel: video");
            this.videoChannel.stopChannel();
        }
    }

    public boolean equal(NetStream netStream) {
        if (netStream == null || TextUtils.isEmpty(netStream.name) || TextUtils.isEmpty(netStream.name)) {
            return false;
        }
        return this.name.equals(netStream.name);
    }

    public void updateManager(RtmpClientManager2 rtmpClientManager2) {
        this.manager2 = rtmpClientManager2;
        if (this.audioChannel != null) {
            this.audioChannel.updateManager(rtmpClientManager2);
        }
        if (this.videoChannel != null) {
            this.videoChannel.updateManager(rtmpClientManager2);
        }
    }
}
