package cn.banshenggua.aichang.rtmpclient;

import android.content.Context;
import android.opengl.GLSurfaceView;

public class GLVideoPlayView extends GLSurfaceView implements VideoPlayerInterface {
    private TextureRenderer mRenderer;

    public GLVideoPlayView(Context context) {
        super(context);
        this.mRenderer = new TextureRenderer(context);
        setEGLContextClientVersion(2);
        setRenderer(this.mRenderer);
        setRenderMode(0);
    }

    public void drawFrame(int i, int i2, byte[] bArr) {
        if (this.mRenderer != null) {
            this.mRenderer.drawFrame(i, i2, bArr);
            requestRender();
        }
    }

    public int getwidth() {
        return getWidth();
    }

    public int getheight() {
        return getHeight();
    }
}
