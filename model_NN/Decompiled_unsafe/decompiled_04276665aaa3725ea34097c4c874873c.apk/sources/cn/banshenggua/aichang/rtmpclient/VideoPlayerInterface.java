package cn.banshenggua.aichang.rtmpclient;

public interface VideoPlayerInterface {
    void drawFrame(int i, int i2, byte[] bArr);

    int getheight();

    int getwidth();
}
