package cn.banshenggua.aichang.app;

import cn.banshenggua.aichang.utils.ULog;
import com.pocketmusic.kshare.requestobjs.a;
import com.sina.weibo.sdk.exception.WeiboAuthException;

public class Session {
    private static final String KShareroot = "/kshare";
    private static String TAG = "Session";
    private static a currentAccount;
    private static Session session = null;
    private String KEY_UID = "uid";
    private NotifyCount mNotifyCount;

    public interface SessionProgressListener {
        void onDownloading();

        void onUploading();
    }

    public enum SessionCacheType {
        AccountList("account_list"),
        Channel4Download("channel4download_list"),
        Channel4Upload("channel4upload_list"),
        ProgressList4Download("progress4download_list"),
        ProgressList4Upload("progress4upload_list"),
        WeiboListAtMe("weibolist_atme"),
        WeiboListCommentMe("weibolist_commentme"),
        WeiboListMyPost("weibolist_mypost"),
        WeiboListFriends("weibolist_friends");
        
        private String mName;

        private SessionCacheType(String str) {
            this.mName = str;
        }

        public String getString() {
            return this.mName;
        }
    }

    public static a getCurrentAccount() {
        if (currentAccount == null) {
            ULog.d(TAG, "getCurrentAccount null");
            session = null;
            getSharedSession();
        }
        ULog.d(TAG, "getCurrentAccount: not null: " + currentAccount.b + "; " + currentAccount.d + "; " + currentAccount.f);
        return currentAccount;
    }

    public static synchronized Session getSharedSession() {
        Session session2;
        synchronized (Session.class) {
            if (session == null) {
                session = new Session();
            }
            session2 = session;
        }
        return session2;
    }

    private Session() {
        session = this;
        useTopCacheAccount();
    }

    public void addAccount(a aVar) {
        if (aVar != null && !aVar.d()) {
            saveAccount(aVar);
            useTopCacheAccount();
        }
    }

    public void addAccount(a aVar, boolean z) {
        if (aVar != null && !aVar.d()) {
            saveAccount(aVar, z);
            useTopCacheAccount(z);
        }
    }

    public void updateAccount() {
        useTopCacheAccount();
    }

    private void saveAccount(a aVar) {
        currentAccount = aVar;
        ULog.d(TAG, "saveAccount uid =" + aVar.b);
        ULog.d(TAG, "saveAccount: JSON_TEXT: " + currentAccount.b + "; " + currentAccount.c + "; " + currentAccount.i + currentAccount.X + "; " + currentAccount.d + "; " + currentAccount.f);
    }

    private void saveAccount(a aVar, boolean z) {
        saveAccount(aVar);
        if (currentAccount != null && z && !currentAccount.d()) {
            currentAccount.c();
        }
    }

    public void deleteAccount() {
        if (currentAccount != null && !currentAccount.d()) {
            currentAccount = new a("");
        }
    }

    public a getAccount() {
        return currentAccount;
    }

    private void useTopCacheAccount() {
        useTopCacheAccount(false);
    }

    private void useTopCacheAccount(boolean z) {
        if (currentAccount == null) {
            currentAccount = new a();
        }
        if (z && !currentAccount.d()) {
            currentAccount.c();
        }
        resetNotifyCount();
    }

    public NotifyCount getNotifyNum() {
        if (this.mNotifyCount == null) {
            this.mNotifyCount = new NotifyCount();
            this.mNotifyCount.uid = currentAccount.b;
        }
        return this.mNotifyCount;
    }

    private void resetNotifyCount() {
        if (this.mNotifyCount != null && this.mNotifyCount.uid != null && !this.mNotifyCount.uid.equals(currentAccount.b)) {
            this.mNotifyCount = new NotifyCount();
            this.mNotifyCount.uid = currentAccount.b;
        }
    }

    public class NotifyCount {
        public int notifyClubApply;
        public int notifyFanchang;
        public int notifyFans;
        public int notifyInterval;
        public int notifyMessage;
        public int notifyRecevingGift;
        public int notifyReply;
        public int notifySnsQQWeiBo;
        public int notifySnsSina;
        public int notifyat;
        public String uid = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;

        public NotifyCount() {
        }
    }
}
