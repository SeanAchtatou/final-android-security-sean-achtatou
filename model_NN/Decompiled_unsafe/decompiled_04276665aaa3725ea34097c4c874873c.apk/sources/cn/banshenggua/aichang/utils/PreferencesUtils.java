package cn.banshenggua.aichang.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.util.List;

public class PreferencesUtils {
    public static final String ACTIVATE = "activate";
    public static final String Entry_Market_Flag = "entry_market";
    public static final String Evaluationed = "evaluationed";
    public static final String FFMPEG_VERSION = "ffmpeg_version";
    public static final String FIRST_MARKET = "First_Market";
    public static final String FORWARD = "forward";
    public static final String FREE_GIFT = "free_gift";
    public static final String GUID = "GUID";
    public static final String HEAD_MASK = "mk_";
    public static final String HEAD_REPLY = "r_";
    public static final String MESSAGE_NOTIFY_COUNT = "message_notify_count";
    public static final String OFFMESSAGE_TAG_HEAD = "off_msg_tag_";
    public static final String PLAY_GUID = "player_guid";
    public static final String TENCENTTOKEN_FILE = "tencent_login";
    public static final String WELCOME_GUID = "WELCOME_GUID_5.1";
    private static SharedPreferences.Editor editor = null;
    private static SharedPreferences pref = null;
    public static final String sendLog = "sendLog";
    public static final String tencentAPPSECRET = "tencentAPPSECRET";
    public static final String tencentAccessToken = "tencentAccessToken";
    public static final String tencentAccessTokenSecret = "tencentAccessTokenSecret";
    public static final String tencentAppkey = "tencentAppkey";
    public static final String vivoKaraoke = "vivo_karaoke";

    public static String loadPrefString(Context context, String str) {
        return loadPrefString(context, str, null);
    }

    public static String loadPrefString(Context context, String str, String str2) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return pref.getString(str, str2);
    }

    public static void savePrefString(Context context, String str, String str2) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        if (editor == null) {
            editor = pref.edit();
        }
        editor.putString(str, str2);
        editor.commit();
    }

    public static void savePrefStrings(Context context, String str, List<String> list) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        if (editor == null) {
            editor = pref.edit();
        }
    }

    public static int loadPrefInt(Context context, String str, int i) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return pref.getInt(str, i);
    }

    public static void savePrefInt(Context context, String str, int i) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        if (editor == null) {
            editor = pref.edit();
        }
        editor.putInt(str, i);
        editor.commit();
    }

    public static long loadPrefLong(Context context, String str, long j) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return pref.getLong(str, j);
    }

    public static void savePrefLong(Context context, String str, long j) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        if (editor == null) {
            editor = pref.edit();
        }
        editor.putLong(str, j);
        editor.commit();
    }

    public static float loadPrefFloat(Context context, String str, float f) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return pref.getFloat(str, f);
    }

    public static void savePrefFloat(Context context, String str, float f) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        if (editor == null) {
            editor = pref.edit();
        }
        editor.putFloat(str, f);
        editor.commit();
    }

    public static boolean loadPrefBoolean(Context context, String str, boolean z) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return pref.getBoolean(str, z);
    }

    public static boolean containsPref(Context context, String str) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return pref.contains(str);
    }

    public static void savePrefBoolean(Context context, String str, boolean z) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        if (editor == null) {
            editor = pref.edit();
        }
        editor.putBoolean(str, z);
        editor.commit();
    }

    public static String loadPrefStringWriteable(Context context, String str, String str2) {
        return context.getSharedPreferences("aichang_preferences", 4).getString(str, str2);
    }

    public static void savePrefStringWriteable(Context context, String str, String str2) {
        SharedPreferences.Editor edit = context.getSharedPreferences("aichang_preferences", 4).edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public static boolean loadPrefBooleanWriteable(Context context, String str, boolean z) {
        return context.getSharedPreferences("aichang_preferences", 4).getBoolean(str, z);
    }

    public static void savePrefBooleanWriteable(Context context, String str, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences("aichang_preferences", 4).edit();
        edit.putBoolean(str, z);
        edit.commit();
    }

    public static long loadPrefLongWriteable(Context context, String str, long j) {
        return context.getSharedPreferences("aichang_preferences", 4).getLong(str, j);
    }

    public static void savePrefLongWriteable(Context context, String str, long j) {
        SharedPreferences.Editor edit = context.getSharedPreferences("aichang_preferences", 4).edit();
        edit.putLong(str, j);
        edit.commit();
    }

    public static void removePref(Context context, String str) {
        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context);
        }
        if (editor == null) {
            editor = pref.edit();
        }
        editor.remove(str);
        editor.commit();
    }
}
