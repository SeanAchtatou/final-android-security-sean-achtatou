package cn.banshenggua.aichang.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static DateFormat dfDay = new SimpleDateFormat("yy-MM-dd");
    private static DateFormat dfDay2 = new SimpleDateFormat("yyyy-MM-dd");
    private static DateFormat dfHour = new SimpleDateFormat("HH:mm");
    private static DateFormat dfMonth = new SimpleDateFormat("MM-dd HH:mm");
    private static DateFormat dfYear = new SimpleDateFormat("yyyy");

    public static String getDate(Calendar calendar, String str) {
        int i = calendar.get(2) + 1;
        int i2 = calendar.get(5);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(calendar.get(1));
        stringBuffer.append(str);
        if (i < 10) {
            stringBuffer.append(0);
        }
        stringBuffer.append(i);
        stringBuffer.append(str);
        if (i2 < 10) {
            stringBuffer.append(0);
        }
        stringBuffer.append(i2);
        return stringBuffer.toString();
    }

    public static String getTimeWithMillSec(Calendar calendar, String str) {
        return getTime(calendar, true, str);
    }

    public static String getTime(Calendar calendar, boolean z, String str) {
        int i = calendar.get(11);
        int i2 = calendar.get(12);
        int i3 = calendar.get(13);
        int i4 = calendar.get(14);
        StringBuffer stringBuffer = new StringBuffer();
        if (i < 10) {
            stringBuffer.append(0);
        }
        stringBuffer.append(i);
        stringBuffer.append(str);
        if (i2 < 10) {
            stringBuffer.append(0);
        }
        stringBuffer.append(i2);
        stringBuffer.append(str);
        if (i3 < 10) {
            stringBuffer.append(0);
        }
        stringBuffer.append(i3);
        if (z) {
            stringBuffer.append(str);
            if (i4 < 10) {
                stringBuffer.append("00");
            } else if (i4 < 100) {
                stringBuffer.append("0");
            }
            stringBuffer.append(i4);
        }
        return stringBuffer.toString();
    }

    public static String convertTimeTime(long j) {
        int currentTimeMillis = ((int) ((System.currentTimeMillis() / 1000) - j)) / 60;
        if (currentTimeMillis < 1) {
            return "刚刚";
        }
        if (currentTimeMillis >= 60) {
            return df.format(Long.valueOf(j * 1000));
        }
        return String.format("%1$s分钟前", String.valueOf(Math.abs(currentTimeMillis)));
    }

    public static String converDayTime(long j) {
        int currentTimeMillis = ((int) ((System.currentTimeMillis() / 1000) - j)) / 60;
        if (currentTimeMillis < 1) {
            return "刚刚";
        }
        if (currentTimeMillis < 1 || currentTimeMillis >= 60) {
            String[] split = dfDay.format(Long.valueOf(System.currentTimeMillis())).split("-");
            String[] split2 = dfDay.format(Long.valueOf(j * 1000)).split("-");
            if (split.length <= 2 || split2.length <= 2 || !split[2].equals(split2[2])) {
                return df.format(Long.valueOf(j * 1000));
            }
            return String.format("今天 %1$s", dfHour.format(Long.valueOf(j * 1000)));
        }
        return String.format("%1$s分钟前", String.valueOf(currentTimeMillis));
    }

    public static String converTime(long j) {
        int currentTimeMillis = ((int) ((System.currentTimeMillis() / 1000) - j)) / 60;
        if (currentTimeMillis < 1) {
            return "刚刚";
        }
        if (currentTimeMillis >= 1 && currentTimeMillis < 60) {
            return String.format("%1$s分钟前", String.valueOf(currentTimeMillis));
        } else if (currentTimeMillis >= 60 && currentTimeMillis < 1440) {
            return String.format("%1$s小时前", String.valueOf(currentTimeMillis / 60));
        } else if (currentTimeMillis < 1440 || currentTimeMillis >= 8640) {
            String[] split = dfDay2.format(Long.valueOf(1000 * j)).split("-");
            if (new StringBuilder(String.valueOf(Calendar.getInstance().get(1))).toString().equalsIgnoreCase(split[0])) {
                return String.valueOf(split[1]) + "月" + split[2] + "日";
            }
            return String.valueOf(split[0]) + "年" + split[1] + "月" + split[2] + "日";
        } else {
            return String.format("%1$s天前", String.valueOf(currentTimeMillis / 1440));
        }
    }

    public static String getZhongWenTime(long j) {
        String[] split = dfDay2.format(Long.valueOf(j)).split("-");
        return String.valueOf(split[0]) + "年" + split[1] + "月" + split[2] + "日";
    }

    public static String getTime(long j, String str) {
        return new SimpleDateFormat(str).format(new Date(j));
    }
}
