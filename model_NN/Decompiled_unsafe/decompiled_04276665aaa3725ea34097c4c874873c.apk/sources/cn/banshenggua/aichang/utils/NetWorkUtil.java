package cn.banshenggua.aichang.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetWorkUtil {
    public static boolean MOBLE_NET = false;
    public static boolean NO_NETWORK = true;
    public static boolean ONLYWAP = false;
    public static int networkType = 1;

    public static NetworkInfo getNetworkInfo(Context context) {
        return ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
    }

    public static boolean is3GNetwork(Context context) {
        NetworkInfo networkInfo = getNetworkInfo(context);
        if (networkInfo == null || !networkInfo.isAvailable()) {
            ULog.d("NetApnChangeReceiver", "has no Connectivity--not is available");
            return false;
        } else if (networkInfo.getType() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isNetworkState(Context context) {
        if (getNetworkInfo(context) == null) {
            NO_NETWORK = true;
            return false;
        }
        NO_NETWORK = false;
        return true;
    }

    public static void getNetWorkInfoType(Context context) {
        NetworkInfo networkInfo = getNetworkInfo(context);
        if (networkInfo != null) {
            NO_NETWORK = false;
            if (networkInfo.getTypeName() == "WIFI") {
                ONLYWAP = false;
            }
            if (!networkInfo.getTypeName().equalsIgnoreCase("MOBILE")) {
                return;
            }
            if ("cmwap".equalsIgnoreCase(networkInfo.getExtraInfo()) || "3gwap".equalsIgnoreCase(networkInfo.getExtraInfo()) || "ctwap".equalsIgnoreCase(networkInfo.getExtraInfo())) {
                ONLYWAP = true;
            } else {
                ONLYWAP = false;
            }
        } else {
            System.out.println("无网络!");
            NO_NETWORK = true;
        }
    }
}
