package cn.banshenggua.aichang.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.zip.GZIPInputStream;
import org.apache.http.util.ByteArrayBuffer;

public final class FileUtils {
    private static final int BUF_SIZE = 32768;
    public static final int FAILED = -1;
    static final String LOG_TAG = "FileUtils";
    private static final Class<?>[] SIG_SET_PERMISSION = {String.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
    public static final int SUCCESS = 0;
    public static final int S_IRGRP = 32;
    public static final int S_IROTH = 4;
    public static final int S_IRUSR = 256;
    public static final int S_IRWXG = 56;
    public static final int S_IRWXO = 7;
    public static final int S_IRWXU = 448;
    public static final int S_IWGRP = 16;
    public static final int S_IWOTH = 2;
    public static final int S_IWUSR = 128;
    public static final int S_IXGRP = 8;
    public static final int S_IXOTH = 1;
    public static final int S_IXUSR = 64;
    private static WeakReference<Exception> exReference;

    public enum FileState {
        FState_Dir("I am director!"),
        FState_File("I am file!"),
        FState_None("I am a ghost!"),
        FState_Other("I am not human!");
        
        private String tag;

        private FileState(String str) {
            this.tag = str;
        }

        public String getTag() {
            return this.tag;
        }

        public String toString() {
            return this.tag;
        }
    }

    private FileUtils() {
    }

    public static FileState fileState(String str) {
        return fileState(new File(str));
    }

    public static FileState fileState(File file) {
        if (!file.exists()) {
            return FileState.FState_None;
        }
        if (file.isFile()) {
            return FileState.FState_File;
        }
        if (file.isDirectory()) {
            return FileState.FState_Dir;
        }
        return FileState.FState_Other;
    }

    public static int createDir(String str) {
        return createDir(new File(str));
    }

    public static int createDir(File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                return 0;
            }
            file.delete();
        }
        if (!file.mkdirs()) {
            return -1;
        }
        return 0;
    }

    public static int removeDir(String str) {
        return removeDir(new File(str));
    }

    public static int removeDir(File file) {
        File[] listFiles;
        if (!file.exists()) {
            return 0;
        }
        if (file.isDirectory() && (listFiles = file.listFiles()) != null) {
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    removeDir(file2);
                } else {
                    file2.delete();
                }
            }
        }
        if (!file.delete()) {
            return -1;
        }
        return 0;
    }

    public static void checkParentPath(String str) {
        checkParentPath(new File(str));
    }

    public static void checkParentPath(File file) {
        File parentFile = file.getParentFile();
        if (parentFile != null && !parentFile.isDirectory()) {
            createDir(parentFile);
        }
    }

    public static int streamToFile(String str, InputStream inputStream, boolean z) {
        return streamToFile(new File(str), inputStream, z);
    }

    public static int streamToFile(File file, InputStream inputStream, boolean z) {
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2;
        checkParentPath(file);
        try {
            fileOutputStream2 = new FileOutputStream(file, z);
            try {
                byte[] bArr = new byte[32768];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        fileOutputStream2.flush();
                        try {
                            fileOutputStream2.close();
                            return 0;
                        } catch (Exception e) {
                            return 0;
                        }
                    } else {
                        fileOutputStream2.write(bArr, 0, read);
                    }
                }
            } catch (Exception e2) {
                fileOutputStream = fileOutputStream2;
            } catch (Throwable th) {
                th = th;
                try {
                    fileOutputStream2.close();
                } catch (Exception e3) {
                }
                throw th;
            }
        } catch (Exception e4) {
            fileOutputStream = null;
            try {
                fileOutputStream.close();
            } catch (Exception e5) {
            }
            return -1;
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream2 = null;
            fileOutputStream2.close();
            throw th;
        }
    }

    public static int bytesToFile(File file, byte[] bArr, int i, int i2, boolean z) {
        FileOutputStream fileOutputStream;
        checkParentPath(file);
        if (bArr == null) {
            return -1;
        }
        if (i2 <= 0) {
            i2 = bArr.length;
        }
        try {
            fileOutputStream = new FileOutputStream(file, z);
            try {
                fileOutputStream.write(bArr, i, i2);
                fileOutputStream.flush();
                try {
                    fileOutputStream.close();
                } catch (Exception e) {
                }
                return 0;
            } catch (Exception e2) {
                try {
                    fileOutputStream.close();
                    return -1;
                } catch (Exception e3) {
                    return -1;
                }
            } catch (Throwable th) {
                th = th;
                try {
                    fileOutputStream.close();
                } catch (Exception e4) {
                }
                throw th;
            }
        } catch (Exception e5) {
            fileOutputStream = null;
            fileOutputStream.close();
            return -1;
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            fileOutputStream.close();
            throw th;
        }
    }

    public static int bytesToFile(File file, byte[] bArr, boolean z) {
        return bytesToFile(file, bArr, 0, bArr.length, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.banshenggua.aichang.utils.FileUtils.bytesToFile(java.io.File, byte[], int, int, boolean):int
     arg types: [java.io.File, byte[], int, int, int]
     candidates:
      cn.banshenggua.aichang.utils.FileUtils.bytesToFile(java.lang.String, byte[], int, int, boolean):int
      cn.banshenggua.aichang.utils.FileUtils.bytesToFile(java.io.File, byte[], int, int, boolean):int */
    public static int bytesToFile(File file, byte[] bArr) {
        return bytesToFile(file, bArr, 0, bArr.length, false);
    }

    public static int stringToFile(File file, String str) {
        return bytesToFile(file, str.getBytes());
    }

    public static int bytesToFile(String str, byte[] bArr, int i, int i2, boolean z) {
        return bytesToFile(new File(str), bArr, i, i2, z);
    }

    public static byte[] fileToBytes(String str, int i, int i2) {
        return fileToBytes(new File(str), i, i2);
    }

    public static byte[] fileToBytes(File file) {
        return fileToBytes(file, 0, 0);
    }

    public static String fileToString(File file) {
        byte[] fileToBytes = fileToBytes(file);
        if (fileToBytes != null) {
            return new String(fileToBytes);
        }
        return null;
    }

    public static byte[] fileToBytes(File file, int i, int i2) {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        if (i2 < 0 || !file.exists()) {
            return null;
        }
        try {
            fileInputStream = new FileInputStream(file);
            if (i2 == 0) {
                try {
                    i2 = fileInputStream.available();
                } catch (Exception e) {
                    fileInputStream2 = fileInputStream;
                    try {
                        fileInputStream2.close();
                    } catch (Exception e2) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    try {
                        fileInputStream.close();
                    } catch (Exception e3) {
                    }
                    throw th;
                }
            }
            byte[] bArr = new byte[i2];
            fileInputStream.read(bArr, i, i2);
            try {
                fileInputStream.close();
                return bArr;
            } catch (Exception e4) {
                return bArr;
            }
        } catch (Exception e5) {
            fileInputStream2 = null;
            fileInputStream2.close();
            return null;
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            fileInputStream.close();
            throw th;
        }
    }

    public static int copyTo(String str, String str2) {
        return copyTo(new File(str), new File(str2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.io.File, java.io.InputStream, boolean):int
     arg types: [java.io.File, java.io.FileInputStream, int]
     candidates:
      cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.lang.String, java.io.InputStream, boolean):int
      cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.io.File, java.io.InputStream, boolean):int */
    public static int copyTo(File file, File file2) {
        FileInputStream fileInputStream;
        int i = -1;
        if (fileState(file2) == FileState.FState_File) {
            FileInputStream fileInputStream2 = null;
            try {
                fileInputStream = new FileInputStream(file2);
                try {
                    i = streamToFile(file, (InputStream) fileInputStream, false);
                    try {
                        fileInputStream.close();
                    } catch (Exception e) {
                    }
                } catch (Exception e2) {
                } catch (Throwable th) {
                    th = th;
                    fileInputStream2 = fileInputStream;
                    try {
                        fileInputStream2.close();
                    } catch (Exception e3) {
                    }
                    throw th;
                }
            } catch (Exception e4) {
                fileInputStream = null;
                try {
                    fileInputStream.close();
                } catch (Exception e5) {
                }
                return i;
            } catch (Throwable th2) {
                th = th2;
                fileInputStream2.close();
                throw th;
            }
        }
        return i;
    }

    public static int copyToIfNotExist(String str, String str2) {
        return copyTo(new File(str), new File(str2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.io.File, java.io.InputStream, boolean):int
     arg types: [java.io.File, java.io.FileInputStream, int]
     candidates:
      cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.lang.String, java.io.InputStream, boolean):int
      cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.io.File, java.io.InputStream, boolean):int */
    public static int copyToIfNotExist(File file, File file2) {
        FileInputStream fileInputStream;
        FileInputStream fileInputStream2;
        if (fileState(file2) != FileState.FState_File) {
            return -1;
        }
        try {
            fileInputStream = new FileInputStream(file2);
            try {
                if (!checkExistBySize(file, (long) fileInputStream.available())) {
                    int streamToFile = streamToFile(file, (InputStream) fileInputStream, false);
                    try {
                        fileInputStream.close();
                        return streamToFile;
                    } catch (Exception e) {
                        return streamToFile;
                    }
                } else {
                    try {
                        fileInputStream.close();
                    } catch (Exception e2) {
                    }
                    return 0;
                }
            } catch (Exception e3) {
                fileInputStream2 = fileInputStream;
                try {
                    fileInputStream2.close();
                    return -1;
                } catch (Exception e4) {
                    return -1;
                }
            } catch (Throwable th) {
                th = th;
                try {
                    fileInputStream.close();
                } catch (Exception e5) {
                }
                throw th;
            }
        } catch (Exception e6) {
            fileInputStream2 = null;
            fileInputStream2.close();
            return -1;
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            fileInputStream.close();
            throw th;
        }
    }

    public static int assetToFile(Context context, String str, String str2) {
        return assetToFile(context, str, new File(str2), false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.io.File, java.io.InputStream, boolean):int
     arg types: [java.io.File, java.io.InputStream, int]
     candidates:
      cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.lang.String, java.io.InputStream, boolean):int
      cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.io.File, java.io.InputStream, boolean):int */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x002e, code lost:
        r2 = r1;
        r1 = r0;
        r0 = r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0019 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int assetToFile(android.content.Context r3, java.lang.String r4, java.io.File r5, boolean r6) {
        /*
            r0 = 0
            android.content.res.AssetManager r1 = r3.getAssets()     // Catch:{ Exception -> 0x0019, all -> 0x001f }
            java.io.InputStream r0 = r1.open(r4)     // Catch:{ Exception -> 0x0019, all -> 0x001f }
            if (r6 == 0) goto L_0x0037
            java.util.zip.GZIPInputStream r1 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x0019, all -> 0x002d }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0019, all -> 0x002d }
        L_0x0010:
            r0 = 0
            int r0 = streamToFile(r5, r1, r0)     // Catch:{ Exception -> 0x0034, all -> 0x0032 }
            r1.close()     // Catch:{ Exception -> 0x0027 }
        L_0x0018:
            return r0
        L_0x0019:
            r1 = move-exception
        L_0x001a:
            r0.close()     // Catch:{ Exception -> 0x0029 }
        L_0x001d:
            r0 = -1
            goto L_0x0018
        L_0x001f:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
        L_0x0023:
            r1.close()     // Catch:{ Exception -> 0x002b }
        L_0x0026:
            throw r0
        L_0x0027:
            r1 = move-exception
            goto L_0x0018
        L_0x0029:
            r0 = move-exception
            goto L_0x001d
        L_0x002b:
            r1 = move-exception
            goto L_0x0026
        L_0x002d:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
            goto L_0x0023
        L_0x0032:
            r0 = move-exception
            goto L_0x0023
        L_0x0034:
            r0 = move-exception
            r0 = r1
            goto L_0x001a
        L_0x0037:
            r1 = r0
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.banshenggua.aichang.utils.FileUtils.assetToFile(android.content.Context, java.lang.String, java.io.File, boolean):int");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.io.File, java.io.InputStream, boolean):int
     arg types: [java.io.File, java.io.InputStream, int]
     candidates:
      cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.lang.String, java.io.InputStream, boolean):int
      cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.io.File, java.io.InputStream, boolean):int */
    public static int assetToFileIfNotExist(Context context, String str, File file, boolean z) {
        InputStream inputStream = null;
        try {
            InputStream open = context.getAssets().open(str);
            ULog.d(LOG_TAG, "assetsize: " + open.available());
            if (z) {
                open = new GZIPInputStream(open);
            }
            ULog.d(LOG_TAG, "size: " + open.available());
            if (!checkExistBySize(file, (long) open.available())) {
                int streamToFile = streamToFile(file, open, false);
                try {
                    open.close();
                    return streamToFile;
                } catch (Exception e) {
                    return streamToFile;
                }
            } else {
                try {
                    open.close();
                    return 0;
                } catch (Exception e2) {
                    return 0;
                }
            }
        } catch (Exception e3) {
            ULog.d("JSGAPI", "assettoFile", e3);
            try {
                inputStream.close();
            } catch (Exception e4) {
            }
            return -1;
        } catch (Throwable th) {
            try {
                inputStream.close();
            } catch (Exception e5) {
            }
            throw th;
        }
    }

    public static byte[] assetToBytes(Context context, String str) {
        InputStream inputStream;
        try {
            inputStream = context.getAssets().open(str);
            try {
                byte[] bArr = new byte[inputStream.available()];
                inputStream.read(bArr);
                try {
                    inputStream.close();
                    return bArr;
                } catch (Exception e) {
                    return bArr;
                }
            } catch (Exception e2) {
                e = e2;
                try {
                    setLastException(e);
                    try {
                        inputStream.close();
                    } catch (Exception e3) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    try {
                        inputStream.close();
                    } catch (Exception e4) {
                    }
                    throw th;
                }
            }
        } catch (Exception e5) {
            e = e5;
            inputStream = null;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            inputStream.close();
            throw th;
        }
    }

    public static String assetToString(Context context, String str) {
        byte[] assetToBytes = assetToBytes(context, str);
        if (assetToBytes != null) {
            return new String(assetToBytes);
        }
        return null;
    }

    public static boolean assetExist(AssetManager assetManager, String str) {
        InputStream inputStream = null;
        try {
            try {
                assetManager.open(str).close();
            } catch (Exception e) {
            }
            return true;
        } catch (IOException e2) {
            try {
                inputStream.close();
            } catch (Exception e3) {
            }
            return false;
        } catch (Throwable th) {
            try {
                inputStream.close();
            } catch (Exception e4) {
            }
            throw th;
        }
    }

    public static boolean isSDMounted() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean isSDAvailable(int i) {
        if (isSDAvailable() && getSDLeftSpace() >= ((long) i) * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) {
            return true;
        }
        return false;
    }

    public static boolean isSDAvailable() {
        if (!isSDMounted()) {
            return false;
        }
        File externalStorageDirectory = Environment.getExternalStorageDirectory();
        if (!externalStorageDirectory.canRead() || !externalStorageDirectory.canWrite()) {
            return false;
        }
        return true;
    }

    public static long getSDLeftSpace() {
        if (!isSDMounted()) {
            return 0;
        }
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory() + File.separator);
        return ((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks());
    }

    public static String coverSize(long j) {
        if (j < PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) {
            return String.valueOf("") + j + "b";
        }
        if (j < 1048576) {
            return String.format(Locale.getDefault(), "%.1fK", Float.valueOf(((float) j) / 1024.0f));
        } else if (j < 1073741824) {
            return String.format(Locale.getDefault(), "%.1fM", Float.valueOf(((float) (j / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID)) / 1024.0f));
        } else {
            return String.format(Locale.getDefault(), "%.1fG", Float.valueOf(((float) ((j / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID)) / 1024.0f));
        }
    }

    public static long getROMLeft() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getAbsolutePath());
        long blockSize = (long) statFs.getBlockSize();
        long blockCount = (long) statFs.getBlockCount();
        long availableBlocks = (long) statFs.getAvailableBlocks();
        Log.i("", "ROM Total:" + coverSize(blockCount * blockSize) + ", Left:" + coverSize(availableBlocks * blockSize));
        return availableBlocks * blockSize;
    }

    public static String getDirPathInPrivate(Context context, String str) {
        return String.valueOf(context.getDir(str, 0).getAbsolutePath()) + File.separator;
    }

    public static String getSoPath(Context context) {
        return String.valueOf(context.getApplicationInfo().dataDir) + "/lib/";
    }

    public static FileLock tryFileLock(String str) {
        return tryFileLock(new File(str));
    }

    public static FileLock tryFileLock(File file) {
        try {
            checkParentPath(file);
            FileLock tryLock = new FileOutputStream(file).getChannel().tryLock();
            if (tryLock.isValid()) {
                Log.i(LOG_TAG, "tryFileLock " + file + " SUC!");
                return tryLock;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "tryFileLock " + file + " FAIL! " + e.getMessage());
        }
        return null;
    }

    public static void freeFileLock(FileLock fileLock, File file) {
        if (file != null) {
            file.delete();
        }
        if (fileLock != null && fileLock.isValid()) {
            try {
                fileLock.release();
                Log.i(LOG_TAG, "freeFileLock " + file + " SUC!");
            } catch (IOException e) {
            }
        }
    }

    public static String getPathName(String str) {
        return str.substring(str.lastIndexOf(File.separator) + 1, str.length());
    }

    public static boolean reNamePath(String str, String str2) {
        return new File(str).renameTo(new File(str2));
    }

    public static List<String> listPath(String str) {
        ArrayList arrayList = new ArrayList();
        SecurityManager securityManager = new SecurityManager();
        File file = new File(str);
        securityManager.checkRead(str);
        if (file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                if (file2.isDirectory()) {
                    arrayList.add(file2.getAbsolutePath());
                }
            }
        }
        return arrayList;
    }

    public static int deleteBlankPath(String str) {
        File file = new File(str);
        if (!file.canWrite()) {
            return 1;
        }
        if (file.list() != null && file.list().length > 0) {
            return 2;
        }
        if (file.delete()) {
            return 0;
        }
        return 3;
    }

    public static String getSDRoot() {
        return String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + File.separator;
    }

    public static boolean checkExistBySize(File file, long j) {
        if (file.exists()) {
            ULog.d(LOG_TAG, "file size: " + file.length() + "; size: " + j);
        }
        if (!file.exists() || !file.isFile() || file.length() != j) {
            return false;
        }
        return true;
    }

    public static int setPermissions(String str, int i) {
        return setPermissions(str, i, -1, -1);
    }

    public static int setPermissions(String str, int i, int i2, int i3) {
        try {
            Method declaredMethod = Class.forName("android.os.FileUtils").getDeclaredMethod("setPermissions", SIG_SET_PERMISSION);
            declaredMethod.setAccessible(true);
            return ((Integer) declaredMethod.invoke(null, str, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3))).intValue();
        } catch (Exception e) {
            return -1;
        }
    }

    public static boolean createLink(String str, String str2) {
        try {
            Process exec = Runtime.getRuntime().exec(String.format("ln -s %s %s", str, str2));
            InputStream inputStream = exec.getInputStream();
            while (true) {
                int read = inputStream.read();
                if (read == -1) {
                    inputStream.close();
                    exec.waitFor();
                    return true;
                }
                System.out.print(read);
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static ByteArrayBuffer streamToByteArray(InputStream inputStream) {
        try {
            byte[] bArr = new byte[256];
            ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(1024);
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    return byteArrayBuffer;
                }
                byteArrayBuffer.append(bArr, 0, read);
            }
        } catch (Exception e) {
            setLastException(e);
            return null;
        }
    }

    public static String streamToString(InputStream inputStream) {
        ByteArrayBuffer streamToByteArray = streamToByteArray(inputStream);
        if (streamToByteArray != null) {
            return new String(streamToByteArray.buffer(), 0, streamToByteArray.length());
        }
        return null;
    }

    public static void printLastException() {
        Exception lastException = getLastException();
        if (lastException != null) {
            lastException.printStackTrace();
        }
    }

    private static void setLastException(Exception exc) {
        exReference = new WeakReference<>(exc);
    }

    public static Exception getLastException() {
        if (exReference != null) {
            return exReference.get();
        }
        return null;
    }
}
