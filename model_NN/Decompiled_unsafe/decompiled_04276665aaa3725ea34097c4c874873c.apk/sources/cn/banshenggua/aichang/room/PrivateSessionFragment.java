package cn.banshenggua.aichang.room;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import cn.a.a.a;
import cn.banshenggua.aichang.entry.ArrayListAdapter;
import cn.banshenggua.aichang.room.message.LiveMessage;
import cn.banshenggua.aichang.utils.ULog;
import com.d.a.b.a.k;
import com.d.a.b.d;

public final class PrivateSessionFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemLongClickListener {
    static final int DRAG = 1;
    static final int DROP_DOWN_MOVE = 2;
    static final int DROP_MIN_DIS = 30;
    static final int DROP_UP_MOVE = 3;
    static final int DROP_X_MOVE = 4;
    private static final String KEY_CONTENT = "Fragment:Content";
    static final int NONE = 0;
    public static final String TAG = "PrivateSessionFragment";
    private final long AUTO_SELECT_END = 5000;
    private final long FAST_SELECT_END = 200;
    private final int SELECT_AUTO_SCROLL = 101;
    private final int SELECT_END = 100;
    /* access modifiers changed from: private */
    public ArrayListAdapter<LiveMessage> mAdapter = null;
    private LiveCenterGiftView mCenterGiftView = null;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 100:
                    if (PrivateSessionFragment.this.mAdapter != null && PrivateSessionFragment.this.mListView != null) {
                        PrivateSessionFragment.this.mListView.setSelection(PrivateSessionFragment.this.mAdapter.getCount() - 1);
                        return;
                    }
                    return;
                case 101:
                    PrivateSessionFragment privateSessionFragment = PrivateSessionFragment.this;
                    privateSessionFragment.mScrollingCount = privateSessionFragment.mScrollingCount - 1;
                    ULog.d(PrivateSessionFragment.TAG, "handleMessage mScrollingCount: " + PrivateSessionFragment.this.mScrollingCount);
                    if (PrivateSessionFragment.this.mScrollingCount <= 0) {
                        if (PrivateSessionFragment.this.mAdapter != null) {
                            PrivateSessionFragment.this.mAdapter.setScorllStatus(ArrayListAdapter.ScorllStatus.Normal);
                        }
                        if (PrivateSessionFragment.this.mAdapter != null && PrivateSessionFragment.this.mListView != null) {
                            PrivateSessionFragment.this.mListView.setSelection(PrivateSessionFragment.this.mAdapter.getCount() - 1);
                            return;
                        }
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };
    public int mId = 0;
    private RelativeLayout mLayout;
    /* access modifiers changed from: private */
    public ListView mListView;
    /* access modifiers changed from: private */
    public ListViewOnTouch mListViewOnTouch = null;
    private ViewGroup mLiveGiftLayout = null;
    /* access modifiers changed from: private */
    public int mScrollingCount = 0;
    private boolean noPostHanler = true;
    /* access modifiers changed from: private */
    public boolean showEnd = true;

    public static PrivateSessionFragment newInstance() {
        return new PrivateSessionFragment();
    }

    public void setAdapter(ArrayListAdapter<LiveMessage> arrayListAdapter) {
        this.mAdapter = arrayListAdapter;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewGroup viewGroup2 = (ViewGroup) layoutInflater.inflate(a.g.fragment_roommessage_item, (ViewGroup) null);
        initView(viewGroup2);
        initData();
        return viewGroup2;
    }

    /* access modifiers changed from: protected */
    public void initView(ViewGroup viewGroup) {
        this.mListView = (ListView) viewGroup.findViewById(a.f.public_items_listview);
        this.mLayout = (RelativeLayout) viewGroup.findViewById(a.f.room_message_page_item_layout);
        if (this.mCenterGiftView != null) {
            this.mLiveGiftLayout = (ViewGroup) viewGroup.findViewById(a.f.live_gift_layout);
            this.mLiveGiftLayout.setVisibility(0);
            this.mLiveGiftLayout.addView(this.mCenterGiftView.getContentView(), new ViewGroup.LayoutParams(-1, -2));
        }
    }

    public void initData() {
        if (this.mAdapter != null) {
            this.mListView.setAdapter((ListAdapter) this.mAdapter);
            if (this.mAdapter instanceof RoomMessgeAdapter) {
                this.mListView.setOnItemClickListener((RoomMessgeAdapter) this.mAdapter);
            }
            this.mListView.setOnTouchListener(new View.OnTouchListener() {
                int lastX;
                int lastY;
                int mode = 0;
                int scroll;
                int scrollX;

                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case 0:
                            this.mode = 1;
                            this.scroll = 0;
                            this.scrollX = 0;
                            this.lastX = (int) motionEvent.getRawX();
                            this.lastY = (int) motionEvent.getRawY();
                            return false;
                        case 1:
                        case 3:
                            if (PrivateSessionFragment.this.mListViewOnTouch == null) {
                                this.mode = 0;
                                return false;
                            }
                            switch (this.mode) {
                                case 0:
                                default:
                                    this.mode = 0;
                                    return false;
                                case 1:
                                    return PrivateSessionFragment.this.mListViewOnTouch.OnDrag();
                                case 2:
                                    return PrivateSessionFragment.this.mListViewOnTouch.OnDragDownMove();
                                case 3:
                                    return PrivateSessionFragment.this.mListViewOnTouch.OnDragUpMove();
                                case 4:
                                    return PrivateSessionFragment.this.mListViewOnTouch.OnDragXMove();
                            }
                        case 2:
                            int rawX = ((int) motionEvent.getRawX()) - this.lastX;
                            int rawY = ((int) motionEvent.getRawY()) - this.lastY;
                            this.scroll += rawY;
                            this.scrollX = rawX + this.scrollX;
                            if (rawY < 0) {
                                this.mode = 3;
                            } else {
                                this.mode = 2;
                            }
                            if (Math.abs(this.scroll) <= 30) {
                                this.mode = 1;
                            }
                            if (Math.abs(this.scrollX) - Math.abs(this.scroll) <= 0) {
                                return false;
                            }
                            this.mode = 4;
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.mListView.setOnScrollListener(new ListViewOnScrollListener(new k(d.a(), false, true)));
            this.mAdapter.setOnNotifyDataSetChangedListener(new ArrayListAdapter.OnNotifyDataSetChangedListener() {
                public void onNotifyDataSetChangedBefore() {
                    ULog.d(PrivateSessionFragment.TAG, "last: " + PrivateSessionFragment.this.mListView.getLastVisiblePosition() + "; count: " + PrivateSessionFragment.this.mListView.getCount());
                }

                public void onNotifyDataSetChangedEnd() {
                    ULog.d(PrivateSessionFragment.TAG, "last: End: " + PrivateSessionFragment.this.showEnd);
                    if (PrivateSessionFragment.this.mListView != null && PrivateSessionFragment.this.showEnd) {
                        PrivateSessionFragment.this.mListView.setSelection(PrivateSessionFragment.this.mAdapter.getCount() - 1);
                    }
                }
            });
        }
    }

    public class ListViewOnScrollListener implements AbsListView.OnScrollListener {
        private AbsListView.OnScrollListener listen;

        public ListViewOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
            this.listen = onScrollListener;
        }

        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            if (i + i2 != i3 || i3 <= 0) {
                PrivateSessionFragment.this.showEnd = false;
            } else {
                PrivateSessionFragment.this.showEnd = true;
            }
            if (this.listen != null) {
                this.listen.onScroll(absListView, i, i2, i3);
            }
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (i == 0) {
                if (!PrivateSessionFragment.this.showEnd) {
                    PrivateSessionFragment privateSessionFragment = PrivateSessionFragment.this;
                    privateSessionFragment.mScrollingCount = privateSessionFragment.mScrollingCount + 1;
                    ULog.d(PrivateSessionFragment.TAG, "mScrollingCount: " + PrivateSessionFragment.this.mScrollingCount);
                    if (PrivateSessionFragment.this.mAdapter != null) {
                        PrivateSessionFragment.this.mAdapter.setScorllStatus(ArrayListAdapter.ScorllStatus.Scorlling);
                    }
                    PrivateSessionFragment.this.mHandler.sendEmptyMessageDelayed(101, 5000);
                } else if (PrivateSessionFragment.this.mAdapter != null) {
                    PrivateSessionFragment.this.mAdapter.setScorllStatus(ArrayListAdapter.ScorllStatus.Normal);
                }
            }
            if (i == 0 && this.listen != null) {
                this.listen.onScrollStateChanged(absListView, i);
            }
        }
    }

    public void onDestroyView() {
        if (!(this.mCenterGiftView == null || this.mLiveGiftLayout == null)) {
            this.mLiveGiftLayout.removeAllViews();
        }
        super.onDestroyView();
        ULog.d(TAG, String.valueOf(this.mId) + " onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        ULog.d(TAG, String.valueOf(this.mId) + " onDestroy");
    }

    public void onPause() {
        super.onPause();
        ULog.d(TAG, String.valueOf(this.mId) + " onPause");
    }

    public void onResume() {
        super.onResume();
        ULog.d(TAG, String.valueOf(this.mId) + " onResume");
    }

    public void onStart() {
        super.onStart();
        ULog.d(TAG, String.valueOf(this.mId) + " onStart");
    }

    public void onStop() {
        super.onStop();
        ULog.d(TAG, String.valueOf(this.mId) + " onStop");
    }

    public void onClick(View view) {
        view.getId();
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
        return true;
    }

    public ListViewOnTouch getListViewOnTouch() {
        return this.mListViewOnTouch;
    }

    public void setListViewOnTouch(ListViewOnTouch listViewOnTouch) {
        this.mListViewOnTouch = listViewOnTouch;
    }

    public LiveCenterGiftView getCenterGiftView() {
        return this.mCenterGiftView;
    }

    public void setCenterGiftView(LiveCenterGiftView liveCenterGiftView) {
        this.mCenterGiftView = liveCenterGiftView;
    }
}
