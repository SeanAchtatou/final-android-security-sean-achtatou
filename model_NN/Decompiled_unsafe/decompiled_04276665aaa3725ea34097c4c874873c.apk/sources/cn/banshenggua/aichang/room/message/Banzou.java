package cn.banshenggua.aichang.room.message;

import android.text.TextUtils;
import cn.banshenggua.aichang.utils.StringUtil;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.Serializable;
import org.json.JSONObject;

public class Banzou implements Serializable {
    private static final long serialVersionUID = 1;
    public long bzhasTime = 0;
    public String bzid = "";
    public long bztime = 0;
    public String name = "";
    public String singer = "";

    public void parseBanzou(JSONObject jSONObject) {
        if (jSONObject != null) {
            this.bzid = jSONObject.optString("bzid", "");
            this.name = StringUtil.optStringFromJson(jSONObject, SelectCountryActivity.EXTRA_COUNTRY_NAME, "");
            this.singer = StringUtil.optStringFromJson(jSONObject, "singer", "");
            this.bztime = jSONObject.optLong("bztime", 0);
        }
    }

    public boolean isLocalSong() {
        if (TextUtils.isEmpty(this.bzid)) {
            return true;
        }
        try {
            if (Integer.parseInt(this.bzid) > 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
        }
    }
}
