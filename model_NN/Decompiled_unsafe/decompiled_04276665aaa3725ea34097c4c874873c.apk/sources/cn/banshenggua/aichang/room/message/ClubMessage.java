package cn.banshenggua.aichang.room.message;

import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.LiveMessage;
import com.pocketmusic.kshare.requestobjs.b;
import com.pocketmusic.kshare.requestobjs.o;
import org.json.JSONObject;

public class ClubMessage extends LiveMessage {
    public b mClub;
    public User mClubUser;
    public String mMsg = "";
    public LiveMessage.PanelType mPanel = LiveMessage.PanelType.Public;
    public ClubMessageType mType = ClubMessageType.NO_SURPORT;

    public ClubMessage(o oVar) {
        super(oVar);
        this.mKey = MessageKey.Message_Family;
    }

    public enum ClubMessageType {
        JOIN("join"),
        LEAVE("leave"),
        DISSOLVE("dissolve"),
        APPLY("apply"),
        APPLY_RESULT("apply_result"),
        APPLY_DISAGREE("__apply_disagree"),
        APPLY_AGREE("__apply_agree"),
        LEAVE_By_User("__leave_user"),
        LEAVE_By_Admin("__leave_admin"),
        NO_SURPORT("__no_surport");
        
        private String mKey = "__no_surport";

        private ClubMessageType(String str) {
            this.mKey = str;
        }

        public static ClubMessageType getClubMessageType(String str) {
            if (TextUtils.isEmpty(str)) {
                return NO_SURPORT;
            }
            for (ClubMessageType clubMessageType : values()) {
                if (clubMessageType.mKey.equalsIgnoreCase(str)) {
                    return clubMessageType;
                }
            }
            return NO_SURPORT;
        }
    }

    public void parseOut(JSONObject jSONObject) {
        super.parseOut(jSONObject);
        if (jSONObject != null && jSONObject.length() != 0) {
            this.mType = ClubMessageType.getClubMessageType(jSONObject.optString("op"));
            if (this.mType == ClubMessageType.APPLY_RESULT) {
                String optString = jSONObject.optString(SocketMessage.MSG_RESULE_KEY, "");
                if (optString.equalsIgnoreCase("allow")) {
                    this.mType = ClubMessageType.APPLY_AGREE;
                }
                if (optString.equalsIgnoreCase("deny")) {
                    this.mType = ClubMessageType.APPLY_DISAGREE;
                }
            }
            if (this.mType == ClubMessageType.LEAVE) {
                String optString2 = jSONObject.optString("src", "");
                if (optString2.equalsIgnoreCase("user")) {
                    this.mType = ClubMessageType.LEAVE_By_User;
                }
                if (optString2.equalsIgnoreCase("admin")) {
                    this.mType = ClubMessageType.LEAVE_By_Admin;
                }
            }
            this.mMsg = jSONObject.optString("text", "");
            this.mPanel = LiveMessage.PanelType.getType(jSONObject.optString("panel", ""));
            if (this.mClubUser != null) {
                this.mClubUser = null;
            }
            this.mClubUser = new User();
            this.mClubUser.parseUser(jSONObject);
            if (this.mClub != null) {
                this.mClub = null;
            }
            this.mClub = new b();
            this.mClub.a(jSONObject, false);
        }
    }
}
