package cn.banshenggua.aichang.room;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;
import cn.banshenggua.aichang.entry.ArrayListAdapter;
import cn.banshenggua.aichang.room.message.LiveMessage;
import cn.banshenggua.aichang.utils.ULog;
import com.pocketmusic.kshare.requestobjs.o;
import java.util.List;

public class SessionFragmentAdapter extends FragmentPagerAdapter {
    private int fragmentNum = 4;
    private List<ArrayListAdapter<LiveMessage>> mAdapters;
    private LiveCenterGiftView mCenterGiftView = null;
    private ListViewOnTouch mOnTouch;
    private o room;

    public SessionFragmentAdapter(FragmentManager fragmentManager, o oVar) {
        super(fragmentManager);
        this.room = oVar;
    }

    public SessionFragmentAdapter(FragmentManager fragmentManager, List<ArrayListAdapter<LiveMessage>> list) {
        super(fragmentManager);
        this.mAdapters = list;
    }

    public void setAdapters(List<ArrayListAdapter<LiveMessage>> list) {
        this.mAdapters = list;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        super.destroyItem(viewGroup, i, obj);
        ULog.d(PrivateSessionFragment.TAG, String.valueOf(i) + " DestroyItem");
    }

    public Fragment getItem(int i) {
        if (i == 2) {
            return ConsumerListFragment.newInstance(this.room);
        }
        if (i == 3) {
            i = 2;
        }
        PrivateSessionFragment privateSessionFragment = new PrivateSessionFragment();
        privateSessionFragment.mId = i;
        privateSessionFragment.setListViewOnTouch(this.mOnTouch);
        privateSessionFragment.setAdapter(getAdapter(i));
        if (i != 0) {
            return privateSessionFragment;
        }
        privateSessionFragment.setCenterGiftView(this.mCenterGiftView);
        return privateSessionFragment;
    }

    private ArrayListAdapter<LiveMessage> getAdapter(int i) {
        if (this.mAdapters == null || i % getCount() >= this.mAdapters.size()) {
            return null;
        }
        return this.mAdapters.get(i % getCount());
    }

    public int getCount() {
        return this.fragmentNum;
    }

    public ListViewOnTouch getOnTouch() {
        return this.mOnTouch;
    }

    public void setOnTouch(ListViewOnTouch listViewOnTouch) {
        this.mOnTouch = listViewOnTouch;
    }

    public LiveCenterGiftView getCenterGiftView() {
        return this.mCenterGiftView;
    }

    public void setCenterGiftView(LiveCenterGiftView liveCenterGiftView) {
        this.mCenterGiftView = liveCenterGiftView;
    }
}
