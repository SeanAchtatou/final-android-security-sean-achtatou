package cn.banshenggua.aichang.room.message;

import android.os.Bundle;
import android.text.TextUtils;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.utils.SystemDevice;
import cn.banshenggua.aichang.utils.ULog;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.pocketmusic.kshare.requestobjs.e;
import com.pocketmusic.kshare.requestobjs.o;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.stat.DeviceInfo;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class SimpleMessage extends LiveMessage {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$SimpleMessage$SimpleType = null;
    public static final long KICK_LONG = 259200;
    public static final long KICK_SHORT = 86400;
    private static final String TAG = "SimpleMessage";
    private String livesig;
    public ACKType mAckType = ACKType.NoSurport;
    public User mAnchor = null;
    public long mKicktime = KICK_SHORT;
    public String mMedia = "";
    public String mMessage = "";
    public String mMsg = "";
    public String mMsgType = "";
    public String mProperty = "";
    public List<Property> mPropertys = null;
    public Rtmp mRtmp = null;
    public String mSdat = null;
    public SimpleType mType = SimpleType.Message_Login;
    public String mValue = "";
    public o retRoom = null;
    public String roomPass = "";

    public enum SimpleType {
        Message_Login,
        Message_Logout,
        Message_Join,
        Message_Leave,
        Message_Kick,
        Message_HeartBeat,
        Message_Media,
        Message_MuteRoom,
        Message_Error,
        Message_Sys,
        Message_RoomUpdate,
        Message_FAMILY_DISSOLVE,
        Message_RoomMod,
        Message_Toast,
        Message_RoomParam,
        Message_Ack,
        Message_MultiMedia,
        Message_RtmpProp
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$SimpleMessage$SimpleType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$SimpleMessage$SimpleType;
        if (iArr == null) {
            iArr = new int[SimpleType.values().length];
            try {
                iArr[SimpleType.Message_Ack.ordinal()] = 16;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[SimpleType.Message_Error.ordinal()] = 9;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[SimpleType.Message_FAMILY_DISSOLVE.ordinal()] = 12;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[SimpleType.Message_HeartBeat.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[SimpleType.Message_Join.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[SimpleType.Message_Kick.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[SimpleType.Message_Leave.ordinal()] = 4;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[SimpleType.Message_Login.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[SimpleType.Message_Logout.ordinal()] = 2;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[SimpleType.Message_Media.ordinal()] = 7;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[SimpleType.Message_MultiMedia.ordinal()] = 17;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[SimpleType.Message_MuteRoom.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[SimpleType.Message_RoomMod.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[SimpleType.Message_RoomParam.ordinal()] = 15;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[SimpleType.Message_RoomUpdate.ordinal()] = 11;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[SimpleType.Message_RtmpProp.ordinal()] = 18;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[SimpleType.Message_Sys.ordinal()] = 10;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[SimpleType.Message_Toast.ordinal()] = 14;
            } catch (NoSuchFieldError e18) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$SimpleMessage$SimpleType = iArr;
        }
        return iArr;
    }

    public enum ACKType {
        Refuse("refuse"),
        Accept("accept"),
        NoSurport("__no");
        
        String mKey = "__no";

        private ACKType(String str) {
            this.mKey = str;
        }

        public String getKey() {
            return this.mKey;
        }

        public static ACKType getType(String str) {
            if (!TextUtils.isEmpty(str)) {
                for (ACKType aCKType : values()) {
                    if (str.equalsIgnoreCase(aCKType.getKey())) {
                        return aCKType;
                    }
                }
            }
            return NoSurport;
        }
    }

    public enum PropertyType {
        Type("type"),
        RoomName("roomname"),
        NoSurport("__no");
        
        String mKey = "__no";

        private PropertyType(String str) {
            this.mKey = str;
        }

        public String getKey() {
            return this.mKey;
        }

        public static PropertyType getType(String str) {
            if (!TextUtils.isEmpty(str)) {
                for (PropertyType propertyType : values()) {
                    if (str.equalsIgnoreCase(propertyType.getKey())) {
                        return propertyType;
                    }
                }
            }
            return NoSurport;
        }
    }

    public static class Property {
        String newValue;
        String oldValue;
        PropertyType type;

        public Property(PropertyType propertyType, String str, String str2) {
            this.type = propertyType;
            this.newValue = str;
            this.oldValue = str2;
        }

        public PropertyType getType() {
            return this.type;
        }

        public void setType(PropertyType propertyType) {
            this.type = propertyType;
        }

        public String getNewValue() {
            return this.newValue;
        }

        public void setNewValue(String str) {
            this.newValue = str;
        }

        public String getOldValue() {
            return this.oldValue;
        }

        public void setOldValue(String str) {
            this.oldValue = str;
        }
    }

    public SimpleMessage(SimpleType simpleType, o oVar, String str) {
        super(oVar);
        initTypeAndSig(simpleType, str);
    }

    public SimpleMessage(SimpleType simpleType, o oVar) {
        super(oVar);
        initTypeAndSig(simpleType, "");
    }

    private void initTypeAndSig(SimpleType simpleType, String str) {
        this.livesig = str;
        this.mType = simpleType;
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$SimpleMessage$SimpleType()[this.mType.ordinal()]) {
            case 1:
                this.mKey = MessageKey.Message_Login;
                return;
            case 2:
                this.mKey = MessageKey.Message_Logout;
                return;
            case 3:
                this.mKey = MessageKey.Message_Join;
                return;
            case 4:
                this.mKey = MessageKey.Message_Leave;
                return;
            case 5:
                this.mKey = MessageKey.Message_KickUser;
                return;
            case 6:
                this.mKey = MessageKey.Message_HeartBeat;
                return;
            case 7:
                this.mKey = MessageKey.Message_Media;
                return;
            case 8:
                this.mKey = MessageKey.Message_MuteRoom;
                return;
            case 9:
                this.mKey = MessageKey.Message_ServerError;
                return;
            case 10:
                this.mKey = MessageKey.Message_ServerSys;
                return;
            case 11:
                this.mKey = MessageKey.Message_RoomUpdate;
                return;
            case 12:
                this.mKey = MessageKey.Message_FAMILY_DISSOLVE;
                return;
            case 13:
                this.mKey = MessageKey.Message_RoomMod;
                return;
            case 14:
                this.mKey = MessageKey.Message_Toast;
                return;
            case 15:
                this.mKey = MessageKey.Message_RoomParam;
                return;
            case 16:
                this.mKey = MessageKey.Message_ACK;
                return;
            case 17:
                this.mKey = MessageKey.Message_MultiMedia;
                return;
            case 18:
                this.mKey = MessageKey.Message_RtmpProp;
                return;
            default:
                return;
        }
    }

    public SocketMessage getSocketMessage() {
        SocketMessage socketMessage = super.getSocketMessage();
        if (this.mUser == null) {
            return socketMessage;
        }
        String packInfo = KShareApplication.getPackInfo();
        String str = "no_market";
        Bundle applicationMetaData = KShareApplication.getApplicationMetaData();
        if (!(applicationMetaData == null || applicationMetaData.getString("market") == null)) {
            str = applicationMetaData.getString("market");
        }
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$SimpleMessage$SimpleType()[this.mType.ordinal()]) {
            case 1:
                socketMessage.pushCommend(SelectCountryActivity.EXTRA_COUNTRY_NAME, this.mUser.mNickname);
                socketMessage.pushCommend("gender", this.mUser.mGender);
                socketMessage.pushCommend("face", this.mUser.mFace);
                if (!TextUtils.isEmpty(this.livesig)) {
                    socketMessage.pushCommend("livesig", this.livesig);
                }
                if (!TextUtils.isEmpty(SystemDevice.getInstance().DEVICE_ID)) {
                    socketMessage.pushCommend(Parameters.DEVICE_ID, SystemDevice.getInstance().DEVICE_ID);
                } else {
                    socketMessage.pushCommend(Parameters.DEVICE_ID, "0");
                }
                socketMessage.pushCommend("base_market", str);
                socketMessage.pushCommend("base_type", "ac_sdk");
                socketMessage.pushCommend("base_channel", KShareApplication.getChannel());
                socketMessage.pushCommend(DeviceInfo.TAG_VERSION, packInfo);
                if (!TextUtils.isEmpty(SystemDevice.getInstance().getSystemMachine())) {
                    socketMessage.pushCommend("base_machine", SystemDevice.getInstance().getSystemMachine());
                }
                socketMessage.pushCommend("platform", "android");
                ULog.d(TAG, "login: gender: " + this.mUser.mGender);
                break;
            case 3:
                socketMessage.pushCommend(SelectCountryActivity.EXTRA_COUNTRY_NAME, this.mUser.mNickname);
                socketMessage.pushCommend("gender", this.mUser.mGender);
                socketMessage.pushCommend("face", this.mUser.mFace);
                if (!TextUtils.isEmpty(this.livesig)) {
                    socketMessage.pushCommend("livesig", this.livesig);
                }
                if (!TextUtils.isEmpty(SystemDevice.getInstance().DEVICE_ID)) {
                    socketMessage.pushCommend(Parameters.DEVICE_ID, SystemDevice.getInstance().DEVICE_ID);
                } else {
                    socketMessage.pushCommend(Parameters.DEVICE_ID, "0");
                }
                ULog.d(TAG, "join: gender: " + this.mUser.mGender);
                if (!TextUtils.isEmpty(this.roomPass)) {
                    socketMessage.pushCommend("password", this.roomPass);
                }
                socketMessage.pushCommend("base_market", str);
                socketMessage.pushCommend("base_type", "ac_sdk");
                socketMessage.pushCommend("base_channel", KShareApplication.getChannel());
                socketMessage.pushCommend(DeviceInfo.TAG_VERSION, packInfo);
                if (!TextUtils.isEmpty(SystemDevice.getInstance().getSystemMachine())) {
                    socketMessage.pushCommend("base_machine", SystemDevice.getInstance().getSystemMachine());
                }
                socketMessage.pushCommend("platform", "android");
                socketMessage.pushCommend("streams", "mix");
                break;
            case 5:
                if (!TextUtils.isEmpty(this.mUid)) {
                    socketMessage.pushCommend("touid", this.mUid);
                    socketMessage.pushCommend("time", new StringBuilder(String.valueOf(this.mKicktime)).toString());
                    break;
                }
                break;
            case 7:
            case 17:
                if (!TextUtils.isEmpty(this.mMedia)) {
                    socketMessage.pushCommend("media", this.mMedia);
                    break;
                }
                break;
            case 16:
                if (!TextUtils.isEmpty(this.mSdat)) {
                    socketMessage.pushCommend(SocketMessage.MSG_ACK_SDAT, this.mSdat);
                    socketMessage.pushCommend("touid", this.mUser.mUid);
                    break;
                }
                break;
        }
        return socketMessage;
    }

    public void parseOut(JSONObject jSONObject) {
        JSONObject optJSONObject;
        JSONArray optJSONArray;
        PropertyType type;
        super.parseOut(jSONObject);
        if (jSONObject != null) {
            if (this.mUser == null) {
                this.mUser = new User();
            }
            ULog.d(TAG, jSONObject.toString());
            this.mUser.parseUser(jSONObject);
            if (jSONObject.has("user")) {
                this.mUser.parseUser(jSONObject.optJSONObject("user"));
            }
            if (jSONObject.has("vehicle")) {
                this.mUser.gift = new e();
                this.mUser.gift.a(jSONObject.optJSONObject("vehicle"));
            }
            if (jSONObject.has("anchor")) {
                this.mAnchor = new User();
                this.mAnchor.parseUser(jSONObject.optJSONObject("anchor"));
            }
            if (jSONObject.has("room")) {
                this.retRoom = new o();
                this.retRoom.c(jSONObject.optJSONObject("room"));
                this.retRoom.t = jSONObject.optLong("escaped", 0);
                this.retRoom.u = jSONObject.optLong("t_escaped", 0);
            }
            if (jSONObject.has("propertys") && (optJSONArray = jSONObject.optJSONArray("propertys")) != null && optJSONArray.length() > 0) {
                this.mPropertys = new ArrayList();
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                    if (!(optJSONObject2 == null || (type = PropertyType.getType(optJSONObject2.optString("key"))) == PropertyType.NoSurport)) {
                        this.mPropertys.add(new Property(type, optJSONObject2.optString("new", ""), optJSONObject2.optString("old", "")));
                    }
                }
            }
            this.mMedia = jSONObject.optString("media", "");
            if (this.mMedia.equalsIgnoreCase("V")) {
                this.mMedia = "video";
            }
            if (jSONObject.has("mt")) {
                this.mMedia = jSONObject.optString("mt", "");
            }
            if (jSONObject.has("rtmp")) {
                this.mRtmp = new Rtmp();
                this.mRtmp.parseRtmp(jSONObject.optJSONObject("rtmp"));
            }
            this.mMsg = jSONObject.optString("text", "");
            this.mMsgType = jSONObject.optString("msgtype", "");
            if (this.mType == SimpleType.Message_Toast) {
                this.mMsg = jSONObject.optString("msg", "");
            }
            this.mAckType = ACKType.getType(jSONObject.optString("ack", ""));
            this.mMessage = jSONObject.optString("message", this.mMsg);
            if (jSONObject.has("value") && (optJSONObject = jSONObject.optJSONObject("value")) != null) {
                optJSONObject.optJSONObject(SocketMessage.MSG_RESULE_KEY);
            }
        }
    }
}
