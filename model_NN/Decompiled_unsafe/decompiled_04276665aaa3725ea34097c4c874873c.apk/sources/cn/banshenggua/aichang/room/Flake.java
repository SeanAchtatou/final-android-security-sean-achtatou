package cn.banshenggua.aichang.room;

import android.graphics.Bitmap;
import com.pocketmusic.kshare.requestobjs.e;

public class Flake {
    float SCALE_SPEED_VALULE = 0.06f;
    Bitmap bitmap;
    int height;
    long noScaleTotalTime = 3000;
    float rotation;
    float rotationSpeed;
    float scaleMaxValue = 1.0f;
    float scaleSpeed = this.SCALE_SPEED_VALULE;
    float scaleValue = 0.0f;
    float speed;
    long startNoScaleTime = 0;
    String title = "";
    int width;
    float x;
    float y;

    static Flake createFlake(float f, Bitmap bitmap2) {
        Flake flake = new Flake();
        flake.width = bitmap2.getWidth();
        float height2 = (float) (bitmap2.getHeight() / bitmap2.getWidth());
        flake.height = bitmap2.getHeight();
        flake.x = ((float) Math.random()) * (f - ((float) flake.width));
        flake.y = 0.0f - (((float) flake.height) + (((float) Math.random()) * ((float) flake.height)));
        flake.speed = 50.0f + (((float) Math.random()) * 150.0f);
        flake.rotation = (((float) Math.random()) * 180.0f) - 90.0f;
        flake.rotationSpeed = (((float) Math.random()) * 90.0f) - 45.0f;
        if (flake.bitmap == null) {
            flake.bitmap = Bitmap.createScaledBitmap(bitmap2, flake.width, flake.height, true);
        }
        return flake;
    }

    public static Flake createFlake(float f, float f2, e eVar, Bitmap bitmap2, String str, long j) {
        Flake flake = new Flake();
        flake.width = eVar.s;
        flake.height = eVar.t;
        flake.scaleMaxValue = ((float) eVar.s) / ((float) bitmap2.getWidth());
        flake.bitmap = bitmap2;
        flake.x = (((float) (flake.width / 2)) + f) - ((float) (flake.bitmap.getWidth() / 2));
        flake.y = (((float) (flake.height / 2)) + f2) - ((float) (flake.bitmap.getHeight() / 2));
        flake.SCALE_SPEED_VALULE = flake.scaleMaxValue / 7.0f;
        flake.scaleSpeed = flake.SCALE_SPEED_VALULE;
        flake.title = str;
        if (j > 3000) {
            flake.noScaleTotalTime = j;
        }
        return flake;
    }
}
