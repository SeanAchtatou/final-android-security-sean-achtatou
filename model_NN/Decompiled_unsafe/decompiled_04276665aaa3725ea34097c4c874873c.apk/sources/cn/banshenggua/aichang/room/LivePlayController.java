package cn.banshenggua.aichang.room;

import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.app.Session;
import cn.banshenggua.aichang.room.EventMessage;
import cn.banshenggua.aichang.room.LiveController;
import cn.banshenggua.aichang.room.message.Banzou;
import cn.banshenggua.aichang.room.message.User;
import cn.banshenggua.aichang.utils.ImageLoaderUtil;
import cn.banshenggua.aichang.utils.ImageUtil;
import cn.banshenggua.aichang.utils.KShareUtil;
import cn.banshenggua.aichang.utils.Toaster;
import cn.banshenggua.aichang.utils.UIUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.d.a.b.c;
import com.pocketmusic.kshare.requestobjs.h;
import com.pocketmusic.kshare.requestobjs.k;
import com.pocketmusic.kshare.requestobjs.o;

public class LivePlayController implements View.OnClickListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveController$LiveControllerType = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LivePlayController$PLAY_STATUS = null;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomClass = null;
    private static final long Default_DelayTime = 1500;
    private static final String TAG = LivePlayController.class.getSimpleName();
    /* access modifiers changed from: private */
    public long beginTime = System.currentTimeMillis();
    private int connectBtnWidth;
    private Runnable dismissViewTask = new Runnable() {
        public void run() {
            LivePlayController.this.dismissView();
        }
    };
    public boolean isClickedVideoBtn = false;
    private c levelOptions;
    /* access modifiers changed from: private */
    public View mContentView = null;
    /* access modifiers changed from: private */
    public FragmentActivity mContext;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
    };
    /* access modifiers changed from: private */
    public long mHasTime = 0;
    /* access modifiers changed from: private */
    public long mHasTotalTime = 0;
    private PLAY_STATUS mPlayStatus = PLAY_STATUS.VIDEO_OPEN;
    /* access modifiers changed from: private */
    public o mRoom;
    public boolean mShowLyric = false;
    private Stat mStat = Stat.Pause;
    /* access modifiers changed from: private */
    public long mTotalTime = 0;
    public LiveController.LiveControllerType mType = null;
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long currentTimeMillis = System.currentTimeMillis();
            long access$0 = (LivePlayController.this.mTotalTime - (currentTimeMillis - LivePlayController.this.beginTime)) - LivePlayController.this.mHasTime;
            if (access$0 < 0) {
                access$0 = 0;
            }
            if ((LivePlayController.this.mTotalTime - access$0) - LivePlayController.Default_DelayTime < 0) {
            }
            if (LivePlayController.this.mRoom == null || LivePlayController.this.mRoom.R != o.a.Show) {
                ((TextView) LivePlayController.this.mContentView.findViewById(a.f.room_song_time)).setText(UIUtil.getInstance().toTime(access$0));
            } else {
                ((TextView) LivePlayController.this.mContentView.findViewById(a.f.room_song_time)).setText(UIUtil.getInstance().toTime(LivePlayController.this.mHasTotalTime + (currentTimeMillis - LivePlayController.this.beginTime)));
            }
            LivePlayController.this.mHandler.postDelayed(this, 100);
        }
    };
    private User mUser;
    private int maxWidthNoAuth;
    private int maxWidthWithAuth;
    private int postDelayedTime = 10000;
    private View reqConnectMicButton;
    private View roomPlayVideoButton;
    private View roomRankButton = null;

    public enum PLAY_STATUS {
        VIDEO_CLOSE,
        VIDEO_OPEN,
        AUDIO
    }

    enum Stat {
        Playing,
        Pause
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveController$LiveControllerType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveController$LiveControllerType;
        if (iArr == null) {
            iArr = new int[LiveController.LiveControllerType.values().length];
            try {
                iArr[LiveController.LiveControllerType.AudioPlay.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[LiveController.LiveControllerType.None.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[LiveController.LiveControllerType.VideoPlay.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveController$LiveControllerType = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LivePlayController$PLAY_STATUS() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$LivePlayController$PLAY_STATUS;
        if (iArr == null) {
            iArr = new int[PLAY_STATUS.values().length];
            try {
                iArr[PLAY_STATUS.AUDIO.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PLAY_STATUS.VIDEO_CLOSE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PLAY_STATUS.VIDEO_OPEN.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$LivePlayController$PLAY_STATUS = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomClass() {
        int[] iArr = $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomClass;
        if (iArr == null) {
            iArr = new int[o.a.values().length];
            try {
                iArr[o.a.Match.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[o.a.Normal.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[o.a.Show.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomClass = iArr;
        }
        return iArr;
    }

    private void initView() {
        getView();
        this.levelOptions = ImageUtil.getDefaultLevelOption();
        if (this.mContentView == null) {
            this.mContentView = ViewGroup.inflate(this.mContext, a.g.view_live_play_controller, null);
        }
        this.mContentView.findViewById(a.f.room_video_button).setOnClickListener(this);
        this.mContentView.findViewById(a.f.room_video_button).setVisibility(8);
        this.mContentView.findViewById(a.f.head_back).setOnClickListener(this);
        this.mContentView.findViewById(a.f.room_more_btn).setOnClickListener(this);
        this.roomPlayVideoButton = this.mContentView.findViewById(a.f.room_video_button);
        this.roomRankButton = this.mContentView.findViewById(a.f.room_rank_button);
        this.roomRankButton.setOnClickListener(this);
        this.mContentView.findViewById(a.f.play_controller_content).setOnClickListener(this);
        this.reqConnectMicButton = this.mContentView.findViewById(a.f.req_connect_mic_btn);
        this.reqConnectMicButton.setVisibility(8);
        this.reqConnectMicButton.setOnClickListener(this);
        initUserNameMaxWidth();
    }

    public LivePlayController(FragmentActivity fragmentActivity) {
        this.mContext = fragmentActivity;
        initView();
    }

    public void setControllerType(LiveController.LiveControllerType liveControllerType) {
        this.mType = liveControllerType;
        if (this.mContentView != null) {
            boolean z = false;
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$LiveController$LiveControllerType()[this.mType.ordinal()]) {
                case 2:
                    z = true;
                    break;
            }
            ULog.d(TAG, "setControllerType: " + liveControllerType + "; showVideo: " + z);
            if (z) {
                setPlayStatus(PLAY_STATUS.VIDEO_OPEN);
                return;
            }
            this.mContentView.findViewById(a.f.room_video_button).setVisibility(8);
            setPlayStatus(PLAY_STATUS.AUDIO);
        }
    }

    public boolean isVideoOpen() {
        if (this.mPlayStatus == PLAY_STATUS.VIDEO_OPEN) {
            return true;
        }
        return false;
    }

    public void setPlayStatus(PLAY_STATUS play_status) {
        this.mPlayStatus = play_status;
        if (play_status == PLAY_STATUS.VIDEO_OPEN) {
            this.roomPlayVideoButton.setSelected(true);
        }
        if (play_status == PLAY_STATUS.VIDEO_CLOSE) {
            this.roomPlayVideoButton.setSelected(false);
        }
        if (play_status == PLAY_STATUS.AUDIO) {
            this.roomPlayVideoButton.setVisibility(8);
        }
    }

    public View getView() {
        if (this.mContentView != null) {
            return this.mContentView;
        }
        this.mContentView = ViewGroup.inflate(this.mContext, a.g.view_live_play_controller, null);
        return this.mContentView;
    }

    public void ShowRoomInfo(o oVar) {
        if (oVar != null && this.mContentView != null) {
            ULog.d(TAG, "showRoomInfo: " + oVar);
            this.mRoom = oVar;
            if (!TextUtils.isEmpty(this.mRoom.b)) {
                ((TextView) this.mContentView.findViewById(a.f.head_title)).setText(this.mRoom.b);
            }
            if (!TextUtils.isEmpty(this.mRoom.f1178a)) {
                ((TextView) this.mContentView.findViewById(a.f.head_title2)).setText("ID:" + this.mRoom.f1178a);
            }
            ((TextView) this.mContentView.findViewById(a.f.head_room_people_num)).setText(new StringBuilder(String.valueOf(this.mRoom.k)).toString());
        }
    }

    private void initUserNameMaxWidth() {
        int i = UIUtil.getInstance().getmScreenWidth();
        this.maxWidthWithAuth = (int) (((double) i) - (151.0d * ((double) UIUtil.getInstance().getDensity())));
        this.maxWidthNoAuth = (int) (((double) i) - (130.0d * ((double) UIUtil.getInstance().getDensity())));
        this.connectBtnWidth = (int) (61.0f * UIUtil.getInstance().getDensity());
    }

    private void setUserNameMaxWidth(TextView textView, User user, boolean z) {
        int i = 0;
        if (z) {
            i = this.connectBtnWidth;
        }
        if (!TextUtils.isEmpty(user.auth_info)) {
            textView.setMaxWidth(this.maxWidthWithAuth - i);
        } else {
            textView.setMaxWidth(this.maxWidthNoAuth - i);
        }
    }

    public void ShowUserInfo(User user) {
        String str;
        String str2;
        this.mStat = Stat.Playing;
        this.mShowLyric = true;
        showView();
        ULog.d(TAG, "showUserInfo: " + user);
        this.mHandler.postDelayed(this.dismissViewTask, (long) this.postDelayedTime);
        showRoomRankButton(true);
        ImageView imageView = (ImageView) this.mContentView.findViewById(a.f.room_img_usericon);
        this.mContentView.findViewById(a.f.room_user_level).setVisibility(8);
        this.mContentView.findViewById(a.f.room_user_auth).setVisibility(8);
        this.mUser = user;
        if (user == null) {
            ((TextView) this.mContentView.findViewById(a.f.room_song_name)).setText("");
            this.mContentView.findViewById(a.f.room_no_user_onmic).setVisibility(0);
            imageView.setVisibility(8);
            ((TextView) this.mContentView.findViewById(a.f.room_user_nickname)).setText("");
            ((TextView) this.mContentView.findViewById(a.f.room_song_time)).setText("");
            ((ImageView) this.mContentView.findViewById(a.f.room_user_sex)).setImageBitmap(null);
            stopTimer();
            this.reqConnectMicButton.setVisibility(8);
            return;
        }
        this.mContentView.findViewById(a.f.room_no_user_onmic).setVisibility(8);
        if (!TextUtils.isEmpty(user.mFace)) {
            imageView.setVisibility(0);
            ImageLoaderUtil.getInstance().a(user.mFace, imageView, ImageUtil.getOvalDefaultOption());
            imageView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    KShareUtil.mCurrentNotifyKey = k.b.ROOM_HEAD;
                    if (KShareUtil.processAnonymous(LivePlayController.this.mContext, null, LivePlayController.this.mRoom.f1178a)) {
                    }
                }
            });
        }
        if (!TextUtils.isEmpty(user.getFullName())) {
            ((TextView) this.mContentView.findViewById(a.f.room_user_nickname)).setText(user.getFullName());
        }
        if (user.mLevel > 0) {
            String str3 = user.mLevelImage;
            if (TextUtils.isEmpty(user.mLevelImage)) {
                str2 = h.a(h.a.SIM, user.mLevel);
            } else {
                str2 = str3;
            }
            this.mContentView.findViewById(a.f.room_user_level).setVisibility(0);
            ImageLoaderUtil.getInstance().a(str2, (ImageView) this.mContentView.findViewById(a.f.room_user_level), this.levelOptions);
        } else {
            this.mContentView.findViewById(a.f.room_user_level).setVisibility(8);
        }
        if (!TextUtils.isEmpty(user.auth_info)) {
            ImageLoaderUtil.getInstance().a(user.authIcon, (ImageView) this.mContentView.findViewById(a.f.room_user_auth), this.levelOptions);
            this.mContentView.findViewById(a.f.room_user_auth).setVisibility(0);
        } else {
            this.mContentView.findViewById(a.f.room_user_auth).setVisibility(8);
        }
        if (user.mBanzou != null) {
            if (user.mBanzou.bztime > 0) {
                ((TextView) this.mContentView.findViewById(a.f.room_song_time)).setText("歌长:" + UIUtil.getInstance().toTime(user.mBanzou.bztime));
            }
            if (!TextUtils.isEmpty(user.mBanzou.name)) {
                String str4 = user.mBanzou.name;
                if (user.mBanzou.isLocalSong()) {
                    str = String.valueOf(str4) + "(本地伴奏)";
                } else {
                    str = str4;
                }
                ((TextView) this.mContentView.findViewById(a.f.room_song_name)).setText(str);
            }
            beginTimer(-1, 0, 0);
            stopTimer();
        }
        setSex((ImageView) this.mContentView.findViewById(a.f.room_user_sex), user.mGender);
        if (this.mRoom.R == o.a.Show) {
            this.mContentView.findViewById(a.f.room_song_time).setVisibility(8);
            this.reqConnectMicButton.setVisibility(0);
            setUserNameMaxWidth((TextView) this.mContentView.findViewById(a.f.room_user_nickname), user, true);
            return;
        }
        this.mContentView.findViewById(a.f.room_song_time).setVisibility(0);
        this.reqConnectMicButton.setVisibility(8);
        setUserNameMaxWidth((TextView) this.mContentView.findViewById(a.f.room_user_nickname), user, false);
    }

    public void Pause() {
        this.mStat = Stat.Pause;
        stopTimer();
    }

    public void Play() {
        this.mStat = Stat.Playing;
        beginTimer();
    }

    public void ChangeBanzou(Banzou banzou) {
        String str;
        if (banzou != null) {
            String str2 = banzou.name;
            if (banzou.isLocalSong()) {
                str = String.valueOf(str2) + "(本地伴奏)";
            } else {
                str = str2;
            }
            ((TextView) this.mContentView.findViewById(a.f.room_song_name)).setText(str);
            this.mHasTotalTime = (System.currentTimeMillis() - this.beginTime) + this.mHasTotalTime;
            if (this.mHasTotalTime < 0) {
                this.mHasTotalTime = 0;
            }
            stopTimer();
            if (this.mStat == Stat.Playing) {
                beginTimer(banzou.bztime, 0, this.mHasTotalTime);
            } else {
                beginTimer(banzou.bztime, 0, false, this.mHasTotalTime);
            }
        }
    }

    private void setSex(ImageView imageView, int i) {
        if (i == 1) {
            imageView.setImageResource(a.e.zone_image_boy);
        } else {
            imageView.setImageResource(a.e.zone_image_girl);
        }
    }

    private void showOrHind() {
        if (this.mContentView != null) {
            if (this.mContentView.findViewById(a.f.head_layout).getVisibility() == 8) {
                showView();
            } else {
                dismissView();
            }
        }
    }

    private void SendViewEventMessage(int i) {
        if (this.mContext instanceof SimpleLiveRoomActivity) {
            ((SimpleLiveRoomActivity) this.mContext).SendMessageToHandle(4, new EventMessage(EventMessage.EventMessageType.ViewMessage, i));
        }
    }

    private void SendViewEventMessage(int i, Object obj) {
        if (this.mContext instanceof SimpleLiveRoomActivity) {
            ((SimpleLiveRoomActivity) this.mContext).SendMessageToHandle(4, new EventMessage(EventMessage.EventMessageType.ViewMessage, i, obj));
        }
    }

    public void beginTimer(long j, long j2, long j3) {
        beginTimer(j, j2, true, j3);
    }

    public void beginTimer(long j, long j2, boolean z, long j3) {
        ULog.d(TAG, "beginTimer: " + this.mUser + "; total: " + j);
        if (this.mUser != null) {
            this.beginTime = System.currentTimeMillis();
            this.mTotalTime = j;
            if (this.mTotalTime < 0 && this.mUser.mBanzou != null && this.mUser.mBanzou.bztime > 0) {
                this.mTotalTime = this.mUser.mBanzou.bztime;
            }
            if (this.mTotalTime < 0) {
                this.mTotalTime = 0;
            }
            if (j2 < 0) {
                j2 = 0;
            }
            this.mHasTime = j2;
            this.mHasTotalTime = j3;
            ULog.d(TAG, "Total: " + this.mTotalTime + "; mHasTime: " + this.mHasTime + "; beginTime: " + this.beginTime + "; hastotaltime: " + this.mHasTotalTime);
            if (z) {
                beginTimer();
            }
        }
    }

    public void beginTimer() {
        this.mHandler.postDelayed(this.mUpdateTimeTask, 100);
    }

    public void stopTimer() {
        this.mHandler.removeCallbacks(this.mUpdateTimeTask);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == a.f.play_controller_content) {
            showOrHind();
        } else if (id == a.f.room_video_button) {
            boolean z = false;
            this.isClickedVideoBtn = true;
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$LivePlayController$PLAY_STATUS()[this.mPlayStatus.ordinal()]) {
                case 1:
                    setPlayStatus(PLAY_STATUS.VIDEO_OPEN);
                    z = false;
                    break;
                case 2:
                    z = true;
                    setPlayStatus(PLAY_STATUS.VIDEO_CLOSE);
                    break;
            }
            SendViewEventMessage(view.getId(), z);
        } else if (id == a.f.head_back) {
            SendViewEventMessage(view.getId());
        } else if (id == a.f.room_more_btn) {
            this.mContentView.findViewById(a.f.room_more_btn_tip).setVisibility(8);
            SendViewEventMessage(view.getId());
        } else if (id == a.f.room_rank_button) {
            if (Session.getCurrentAccount().d()) {
                Toaster.showLongToast("匿名用户");
                KShareUtil.mCurrentNotifyKey = k.b.PLAYER_SHARE;
                KShareUtil.tipLoginDialog(this.mContext);
            }
        } else if (id == a.f.req_connect_mic_btn) {
            SendViewEventMessage(view.getId());
        }
    }

    private void showRoomRankButton(boolean z) {
        if (this.roomRankButton != null) {
            if (this.mRoom != null) {
                switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Room$RoomClass()[this.mRoom.R.ordinal()]) {
                    case 2:
                        if (z) {
                            this.roomRankButton.setVisibility(0);
                            return;
                        } else {
                            this.roomRankButton.setVisibility(8);
                            return;
                        }
                }
            }
            this.roomRankButton.setVisibility(8);
        }
    }

    private void showView() {
        if (this.mContext != null && this.mContentView != null) {
            View findViewById = this.mContentView.findViewById(a.f.head_layout);
            if (findViewById.getVisibility() != 0) {
                findViewById.setAnimation(AnimationUtils.loadAnimation(this.mContext, a.C0007a.alpha_in));
                findViewById.setVisibility(0);
                View findViewById2 = this.mContentView.findViewById(a.f.room_user_info_relativelayout);
                findViewById2.setAnimation(AnimationUtils.loadAnimation(this.mContext, a.C0007a.alpha_in));
                findViewById2.setVisibility(0);
                showRoomRankButton(true);
                this.mHandler.postDelayed(this.dismissViewTask, (long) this.postDelayedTime);
            }
        }
    }

    /* access modifiers changed from: private */
    public void dismissView() {
        if (this.mContext != null && this.mContentView != null) {
            this.mHandler.removeCallbacks(this.dismissViewTask);
            View findViewById = this.mContentView.findViewById(a.f.head_layout);
            findViewById.setAnimation(AnimationUtils.loadAnimation(this.mContext, a.C0007a.alpha_out));
            findViewById.setVisibility(8);
            View findViewById2 = this.mContentView.findViewById(a.f.room_user_info_relativelayout);
            findViewById2.setAnimation(AnimationUtils.loadAnimation(this.mContext, a.C0007a.alpha_out));
            findViewById2.setVisibility(8);
            showRoomRankButton(false);
        }
    }
}
