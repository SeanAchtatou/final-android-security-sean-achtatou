package cn.banshenggua.aichang.room.message;

import android.text.TextUtils;
import cn.banshenggua.aichang.utils.StringUtil;
import com.pocketmusic.kshare.requestobjs.o;
import org.json.JSONException;
import org.json.JSONObject;

public class ChatMessage extends LiveMessage {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ChatMessage$ChatType;
    public ChatBroadcastType mBroadcastType = ChatBroadcastType.NO;
    public ChatType mChatType = ChatType.Chat_Public;
    public User mFrom;
    public String mFromNickname = "";
    public String mFromUid = "";
    public String mMessage = "";
    public String mMessageExtends = "";
    public String mMessageId = "";
    public ChatMessageType mMessageType = ChatMessageType.TEXT;
    public String mRid = "";
    public ChatSource mSource = ChatSource.In_Room;
    public User mTo;
    public String mToNickname = "";
    public String mToUid = "";
    public long mTs = 0;

    public enum ChatType {
        Chat_Public,
        Chat_Secret
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ChatMessage$ChatType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ChatMessage$ChatType;
        if (iArr == null) {
            iArr = new int[ChatType.values().length];
            try {
                iArr[ChatType.Chat_Public.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ChatType.Chat_Secret.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ChatMessage$ChatType = iArr;
        }
        return iArr;
    }

    public enum ChatSource {
        In_Room("ROOM"),
        In_Hall("HALL");
        
        private String mKey = "";

        private ChatSource(String str) {
            this.mKey = str;
        }

        public String getKey() {
            return this.mKey;
        }

        public static ChatSource parseChatSource(String str) {
            if (!TextUtils.isEmpty(str)) {
                for (ChatSource chatSource : values()) {
                    if (str.equalsIgnoreCase(chatSource.getKey())) {
                        return chatSource;
                    }
                }
            }
            return In_Room;
        }
    }

    public enum ChatMessageType {
        TEXT("text"),
        NO_SUPPORT("___no___");
        
        private String mKey = "";

        private ChatMessageType(String str) {
            this.mKey = str;
        }

        public String getKey() {
            return this.mKey;
        }

        public static ChatMessageType parseChatMessageType(String str) {
            if (!TextUtils.isEmpty(str)) {
                for (ChatMessageType chatMessageType : values()) {
                    if (str.equalsIgnoreCase(chatMessageType.getKey())) {
                        return chatMessageType;
                    }
                }
            }
            return NO_SUPPORT;
        }
    }

    public enum ChatBroadcastType {
        NO("___no___"),
        SYSTEM("system"),
        FAMILY("family");
        
        private String mKey = "___no___";

        private ChatBroadcastType(String str) {
            this.mKey = str;
        }

        public String getKey() {
            return this.mKey;
        }

        public static ChatBroadcastType parseChatBroadcastType(String str) {
            if (!TextUtils.isEmpty(str)) {
                for (ChatBroadcastType chatBroadcastType : values()) {
                    if (str.equalsIgnoreCase(chatBroadcastType.getKey())) {
                        return chatBroadcastType;
                    }
                }
            }
            return NO;
        }
    }

    public ChatMessage(ChatType chatType, o oVar, ChatSource chatSource) {
        super(oVar);
        this.mChatType = chatType;
        this.mSource = chatSource;
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$ChatMessage$ChatType()[this.mChatType.ordinal()]) {
            case 1:
                this.mKey = MessageKey.Message_Talk;
                return;
            case 2:
                this.mKey = MessageKey.Message_STalk;
                return;
            default:
                return;
        }
    }

    public ChatMessage(ChatType chatType, o oVar) {
        super(oVar);
        this.mChatType = chatType;
        this.mSource = ChatSource.In_Room;
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$ChatMessage$ChatType()[this.mChatType.ordinal()]) {
            case 1:
                this.mKey = MessageKey.Message_Talk;
                return;
            case 2:
                this.mKey = MessageKey.Message_STalk;
                return;
            default:
                return;
        }
    }

    public SocketMessage getSocketMessage() {
        SocketMessage socketMessage = super.getSocketMessage();
        socketMessage.pushCommend("touid", this.mToUid);
        if (this.mSource == ChatSource.In_Hall) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("type", "text");
                jSONObject.put("content", this.mMessage);
                socketMessage.pushCommend("msg", jSONObject.toString());
            } catch (Exception e) {
                e.printStackTrace();
                socketMessage.pushCommend("msg", SocketMessage.MSG_ERROR_KEY);
            }
        } else {
            socketMessage.pushCommend("msg", this.mMessage);
        }
        if (!TextUtils.isEmpty(this.mMessageId)) {
            socketMessage.pushCommend("cdata", this.mMessageId);
        }
        return socketMessage;
    }

    private void parseMessageExtends(String str) {
        if (this.mSource == ChatSource.In_Hall) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                this.mBroadcastType = ChatBroadcastType.parseChatBroadcastType(jSONObject.optString("broadcast", ""));
                this.mMessageType = ChatMessageType.parseChatMessageType(jSONObject.optString("type", ""));
                if (this.mMessageType == ChatMessageType.TEXT) {
                    this.mMessage = jSONObject.optString("content", "");
                }
                this.mRid = jSONObject.optString("rid", "");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.mMessageExtends = str;
    }

    public void parseOut(JSONObject jSONObject) {
        super.parseOut(jSONObject);
        if (jSONObject != null) {
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$ChatMessage$ChatType()[this.mChatType.ordinal()]) {
                case 1:
                default:
                    this.mMessage = StringUtil.optStringFromJson(jSONObject, "text", "");
                    this.mFromUid = jSONObject.optString("uid", "");
                    this.mToUid = jSONObject.optString("touid", "");
                    if (jSONObject.has("from")) {
                        this.mFrom = new User();
                        this.mFrom.parseUser(jSONObject.optJSONObject("from"));
                    }
                    if (jSONObject.has("to")) {
                        this.mTo = new User();
                        this.mTo.parseUser(jSONObject.optJSONObject("to"));
                    }
                    this.mSource = ChatSource.parseChatSource(jSONObject.optString("src"));
                    parseMessageExtends(this.mMessage);
                    if (this.mSource == ChatSource.In_Hall && this.mTo == null) {
                        this.mTo = User.getCurrentUser(null);
                    }
                    if (jSONObject.has("cdata")) {
                        this.mMessageId = jSONObject.optString("cdata", "");
                    }
                    this.mTs = jSONObject.optLong("ts", 0) * 1000;
                    return;
            }
        }
    }
}
