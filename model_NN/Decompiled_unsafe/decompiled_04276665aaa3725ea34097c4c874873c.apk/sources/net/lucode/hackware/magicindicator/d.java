package net.lucode.hackware.magicindicator;

import android.support.v4.view.ViewPager;

/* compiled from: ViewPagerHelper */
public class d {
    public static void a(final MagicIndicator magicIndicator, ViewPager viewPager) {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrolled(int i, float f, int i2) {
                magicIndicator.a(i, f, i2);
            }

            public void onPageSelected(int i) {
                magicIndicator.a(i);
            }

            public void onPageScrollStateChanged(int i) {
                magicIndicator.b(i);
            }
        });
    }
}
