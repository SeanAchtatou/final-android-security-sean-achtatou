package net.lucode.hackware.magicindicator.b;

import android.content.Context;

/* compiled from: UIUtil */
public final class b {
    public static int a(Context context, double d) {
        return (int) ((((double) context.getResources().getDisplayMetrics().density) * d) + 0.5d);
    }
}
