package net.lucode.hackware.magicindicator.b.a.a;

import android.content.Context;
import android.database.DataSetObservable;
import android.database.DataSetObserver;

/* compiled from: CommonNavigatorAdapter */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private final DataSetObservable f3011a = new DataSetObservable();

    public abstract int a();

    public abstract c a(Context context);

    public abstract d a(Context context, int i);

    public float b(Context context, int i) {
        return 1.0f;
    }

    public final void a(DataSetObserver dataSetObserver) {
        this.f3011a.registerObserver(dataSetObserver);
    }

    public final void b(DataSetObserver dataSetObserver) {
        this.f3011a.unregisterObserver(dataSetObserver);
    }

    public final void b() {
        this.f3011a.notifyChanged();
    }
}
