package net.lucode.hackware.magicindicator;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import net.lucode.hackware.magicindicator.a.a;

public class MagicIndicator extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private a f3007a;

    public MagicIndicator(Context context) {
        super(context);
    }

    public MagicIndicator(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void a(int i, float f, int i2) {
        if (this.f3007a != null) {
            this.f3007a.a(i, f, i2);
        }
    }

    public void a(int i) {
        if (this.f3007a != null) {
            this.f3007a.a(i);
        }
    }

    public void b(int i) {
        if (this.f3007a != null) {
            this.f3007a.b(i);
        }
    }

    public a getNavigator() {
        return this.f3007a;
    }

    public void setNavigator(a aVar) {
        if (this.f3007a != aVar) {
            if (this.f3007a != null) {
                this.f3007a.b();
            }
            this.f3007a = aVar;
            removeAllViews();
            if (this.f3007a instanceof View) {
                addView((View) this.f3007a, new FrameLayout.LayoutParams(-1, -1));
                this.f3007a.a();
            }
        }
    }
}
