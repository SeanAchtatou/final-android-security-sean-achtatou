package net.lucode.hackware.magicindicator.b.a;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.List;
import net.lucode.hackware.magicindicator.b;
import net.lucode.hackware.magicindicator.b.a.a.c;
import net.lucode.hackware.magicindicator.b.a.a.d;
import net.lucode.hackware.magicindicator.c;

/* compiled from: CommonNavigator */
public class a extends FrameLayout implements net.lucode.hackware.magicindicator.a.a, b.a {

    /* renamed from: a  reason: collision with root package name */
    private HorizontalScrollView f3009a;
    private LinearLayout b;
    private LinearLayout c;
    private c d;
    /* access modifiers changed from: private */
    public net.lucode.hackware.magicindicator.b.a.a.a e;
    /* access modifiers changed from: private */
    public b f = new b();
    private boolean g;
    private boolean h;
    private float i = 0.5f;
    private boolean j = true;
    private boolean k = true;
    private int l;
    private int m;
    private boolean n;
    private boolean o;
    private boolean p = true;
    private List<net.lucode.hackware.magicindicator.b.a.c.a> q = new ArrayList();
    private DataSetObserver r = new DataSetObserver() {
        public void onChanged() {
            a.this.f.c(a.this.e.a());
            a.this.c();
        }

        public void onInvalidated() {
        }
    };

    public a(Context context) {
        super(context);
        this.f.a(this);
    }

    public void setAdjustMode(boolean z) {
        this.g = z;
    }

    public net.lucode.hackware.magicindicator.b.a.a.a getAdapter() {
        return this.e;
    }

    public void setAdapter(net.lucode.hackware.magicindicator.b.a.a.a aVar) {
        if (this.e != aVar) {
            if (this.e != null) {
                this.e.b(this.r);
            }
            this.e = aVar;
            if (this.e != null) {
                this.e.a(this.r);
                this.f.c(this.e.a());
                if (this.b != null) {
                    this.e.b();
                    return;
                }
                return;
            }
            this.f.c(0);
            c();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        View inflate;
        removeAllViews();
        if (this.g) {
            inflate = LayoutInflater.from(getContext()).inflate(c.b.pager_navigator_layout_no_scroll, this);
        } else {
            inflate = LayoutInflater.from(getContext()).inflate(c.b.pager_navigator_layout, this);
        }
        this.f3009a = (HorizontalScrollView) inflate.findViewById(c.a.scroll_view);
        this.b = (LinearLayout) inflate.findViewById(c.a.title_container);
        this.b.setPadding(this.m, 0, this.l, 0);
        this.c = (LinearLayout) inflate.findViewById(c.a.indicator_container);
        if (this.n) {
            this.c.getParent().bringChildToFront(this.c);
        }
        d();
    }

    private void d() {
        LinearLayout.LayoutParams layoutParams;
        int a2 = this.f.a();
        for (int i2 = 0; i2 < a2; i2++) {
            d a3 = this.e.a(getContext(), i2);
            if (a3 instanceof View) {
                View view = (View) a3;
                if (this.g) {
                    layoutParams = new LinearLayout.LayoutParams(0, -1);
                    layoutParams.weight = this.e.b(getContext(), i2);
                } else {
                    layoutParams = new LinearLayout.LayoutParams(-2, -1);
                }
                this.b.addView(view, layoutParams);
            }
        }
        if (this.e != null) {
            this.d = this.e.a(getContext());
            if (this.d instanceof View) {
                this.c.addView((View) this.d, new FrameLayout.LayoutParams(-1, -1));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (this.e != null) {
            e();
            if (this.d != null) {
                this.d.a(this.q);
            }
            if (this.p && this.f.c() == 0) {
                a(this.f.b());
                a(this.f.b(), 0.0f, 0);
            }
        }
    }

    private void e() {
        this.q.clear();
        int a2 = this.f.a();
        for (int i2 = 0; i2 < a2; i2++) {
            net.lucode.hackware.magicindicator.b.a.c.a aVar = new net.lucode.hackware.magicindicator.b.a.c.a();
            View childAt = this.b.getChildAt(i2);
            if (childAt != null) {
                aVar.f3013a = childAt.getLeft();
                aVar.b = childAt.getTop();
                aVar.c = childAt.getRight();
                aVar.d = childAt.getBottom();
                if (childAt instanceof net.lucode.hackware.magicindicator.b.a.a.b) {
                    net.lucode.hackware.magicindicator.b.a.a.b bVar = (net.lucode.hackware.magicindicator.b.a.a.b) childAt;
                    aVar.e = bVar.getContentLeft();
                    aVar.f = bVar.getContentTop();
                    aVar.g = bVar.getContentRight();
                    aVar.h = bVar.getContentBottom();
                } else {
                    aVar.e = aVar.f3013a;
                    aVar.f = aVar.b;
                    aVar.g = aVar.c;
                    aVar.h = aVar.d;
                }
            }
            this.q.add(aVar);
        }
    }

    public void a(int i2, float f2, int i3) {
        if (this.e != null) {
            this.f.a(i2, f2, i3);
            if (this.d != null) {
                this.d.a(i2, f2, i3);
            }
            if (this.f3009a != null && this.q.size() > 0 && i2 >= 0 && i2 < this.q.size()) {
                if (this.k) {
                    int min = Math.min(this.q.size() - 1, i2);
                    int min2 = Math.min(this.q.size() - 1, i2 + 1);
                    float b2 = ((float) this.q.get(min).b()) - (((float) this.f3009a.getWidth()) * this.i);
                    this.f3009a.scrollTo((int) (b2 + (((((float) this.q.get(min2).b()) - (((float) this.f3009a.getWidth()) * this.i)) - b2) * f2)), 0);
                    return;
                }
                if (!this.h) {
                }
            }
        }
    }

    public float getScrollPivotX() {
        return this.i;
    }

    public void setScrollPivotX(float f2) {
        this.i = f2;
    }

    public void a(int i2) {
        if (this.e != null) {
            this.f.a(i2);
            if (this.d != null) {
                this.d.a(i2);
            }
        }
    }

    public void b(int i2) {
        if (this.e != null) {
            this.f.b(i2);
            if (this.d != null) {
                this.d.b(i2);
            }
        }
    }

    public void a() {
        c();
    }

    public void b() {
    }

    public net.lucode.hackware.magicindicator.b.a.a.c getPagerIndicator() {
        return this.d;
    }

    public void setEnablePivotScroll(boolean z) {
        this.h = z;
    }

    public void a(int i2, int i3, float f2, boolean z) {
        if (this.b != null) {
            View childAt = this.b.getChildAt(i2);
            if (childAt instanceof d) {
                ((d) childAt).a(i2, i3, f2, z);
            }
        }
    }

    public void b(int i2, int i3, float f2, boolean z) {
        if (this.b != null) {
            View childAt = this.b.getChildAt(i2);
            if (childAt instanceof d) {
                ((d) childAt).b(i2, i3, f2, z);
            }
        }
    }

    public void setSmoothScroll(boolean z) {
        this.j = z;
    }

    public void setFollowTouch(boolean z) {
        this.k = z;
    }

    public void setSkimOver(boolean z) {
        this.o = z;
        this.f.a(z);
    }

    public void a(int i2, int i3) {
        if (this.b != null) {
            View childAt = this.b.getChildAt(i2);
            if (childAt instanceof d) {
                ((d) childAt).a(i2, i3);
            }
            if (!this.g && !this.k && this.f3009a != null && this.q.size() > 0) {
                net.lucode.hackware.magicindicator.b.a.c.a aVar = this.q.get(Math.min(this.q.size() - 1, i2));
                if (this.h) {
                    float b2 = ((float) aVar.b()) - (((float) this.f3009a.getWidth()) * this.i);
                    if (this.j) {
                        this.f3009a.smoothScrollTo((int) b2, 0);
                    } else {
                        this.f3009a.scrollTo((int) b2, 0);
                    }
                } else if (this.f3009a.getScrollX() > aVar.f3013a) {
                    if (this.j) {
                        this.f3009a.smoothScrollTo(aVar.f3013a, 0);
                    } else {
                        this.f3009a.scrollTo(aVar.f3013a, 0);
                    }
                } else if (this.f3009a.getScrollX() + getWidth() >= aVar.c) {
                } else {
                    if (this.j) {
                        this.f3009a.smoothScrollTo(aVar.c - getWidth(), 0);
                    } else {
                        this.f3009a.scrollTo(aVar.c - getWidth(), 0);
                    }
                }
            }
        }
    }

    public void b(int i2, int i3) {
        if (this.b != null) {
            View childAt = this.b.getChildAt(i2);
            if (childAt instanceof d) {
                ((d) childAt).b(i2, i3);
            }
        }
    }

    public LinearLayout getTitleContainer() {
        return this.b;
    }

    public int getRightPadding() {
        return this.l;
    }

    public void setRightPadding(int i2) {
        this.l = i2;
    }

    public int getLeftPadding() {
        return this.m;
    }

    public void setLeftPadding(int i2) {
        this.m = i2;
    }

    public void setIndicatorOnTop(boolean z) {
        this.n = z;
    }

    public void setReselectWhenLayout(boolean z) {
        this.p = z;
    }
}
