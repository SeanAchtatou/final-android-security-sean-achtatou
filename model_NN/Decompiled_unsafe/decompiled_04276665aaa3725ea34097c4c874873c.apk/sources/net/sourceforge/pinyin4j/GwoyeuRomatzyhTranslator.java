package net.sourceforge.pinyin4j;

import com.b.a.a.f;
import com.b.a.a.m;

class GwoyeuRomatzyhTranslator {
    private static String[] tones = {"_I", "_II", "_III", "_IV", "_V"};

    GwoyeuRomatzyhTranslator() {
    }

    static String convertHanyuPinyinToGwoyeuRomatzyh(String str) {
        String extractPinyinString = TextHelper.extractPinyinString(str);
        String extractToneNumber = TextHelper.extractToneNumber(str);
        try {
            f b = GwoyeuRomatzyhResource.getInstance().getPinyinToGwoyeuMappingDoc().b(new StringBuffer().append("//").append(PinyinRomanizationType.HANYU_PINYIN.getTagName()).append("[text()='").append(extractPinyinString).append("']").toString());
            if (b != null) {
                return b.c(new StringBuffer().append("../").append(PinyinRomanizationType.GWOYEU_ROMATZYH.getTagName()).append(tones[Integer.parseInt(extractToneNumber) - 1]).append("/text()").toString());
            }
            return null;
        } catch (m e) {
            e.printStackTrace();
            return null;
        }
    }
}
