package android.support.v4.content.res;

import android.content.res.Resources;

class ConfigurationHelperJellybeanMr1 {
    ConfigurationHelperJellybeanMr1() {
    }

    static int getDensityDpi(Resources resources) {
        return resources.getConfiguration().densityDpi;
    }
}
