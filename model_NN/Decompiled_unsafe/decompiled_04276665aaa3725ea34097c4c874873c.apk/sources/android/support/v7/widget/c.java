package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.GravityCompat;
import android.support.v7.a.a;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.view.menu.k;
import android.support.v7.view.menu.l;
import android.support.v7.view.menu.m;
import android.support.v7.view.menu.p;
import android.support.v7.widget.ActionMenuView;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/* compiled from: ActionMenuPresenter */
class c extends android.support.v7.view.menu.a implements ActionProvider.SubUiVisibilityListener {
    private b A;
    d g;
    e h;
    a i;
    C0005c j;
    final f k = new f();
    int l;
    private Drawable m;
    private boolean n;
    private boolean o;
    private boolean p;
    private int q;
    private int r;
    private int s;
    private boolean t;
    private boolean u;
    private boolean v;
    private boolean w;
    private int x;
    private final SparseBooleanArray y = new SparseBooleanArray();
    private View z;

    public c(Context context) {
        super(context, a.h.abc_action_menu_layout, a.h.abc_action_menu_item_layout);
    }

    public void a(Context context, MenuBuilder menuBuilder) {
        super.a(context, menuBuilder);
        Resources resources = context.getResources();
        android.support.v7.view.a a2 = android.support.v7.view.a.a(context);
        if (!this.p) {
            this.o = a2.b();
        }
        if (!this.v) {
            this.q = a2.c();
        }
        if (!this.t) {
            this.s = a2.a();
        }
        int i2 = this.q;
        if (this.o) {
            if (this.g == null) {
                this.g = new d(this.f94a);
                if (this.n) {
                    this.g.setImageDrawable(this.m);
                    this.m = null;
                    this.n = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.g.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i2 -= this.g.getMeasuredWidth();
        } else {
            this.g = null;
        }
        this.r = i2;
        this.x = (int) (56.0f * resources.getDisplayMetrics().density);
        this.z = null;
    }

    public void a(Configuration configuration) {
        if (!this.t) {
            this.s = android.support.v7.view.a.a(this.b).a();
        }
        if (this.c != null) {
            this.c.b(true);
        }
    }

    public void b(boolean z2) {
        this.o = z2;
        this.p = true;
    }

    public void c(boolean z2) {
        this.w = z2;
    }

    public void a(Drawable drawable) {
        if (this.g != null) {
            this.g.setImageDrawable(drawable);
            return;
        }
        this.n = true;
        this.m = drawable;
    }

    public Drawable c() {
        if (this.g != null) {
            return this.g.getDrawable();
        }
        if (this.n) {
            return this.m;
        }
        return null;
    }

    public m a(ViewGroup viewGroup) {
        m mVar = this.f;
        m a2 = super.a(viewGroup);
        if (mVar != a2) {
            ((ActionMenuView) a2).setPresenter(this);
        }
        return a2;
    }

    public View a(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        View actionView = menuItemImpl.getActionView();
        if (actionView == null || menuItemImpl.m()) {
            actionView = super.a(menuItemImpl, view, viewGroup);
        }
        actionView.setVisibility(menuItemImpl.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    public void a(MenuItemImpl menuItemImpl, m.a aVar) {
        aVar.a(menuItemImpl, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aVar;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.f);
        if (this.A == null) {
            this.A = new b();
        }
        actionMenuItemView.setPopupCallback(this.A);
    }

    public boolean a(int i2, MenuItemImpl menuItemImpl) {
        return menuItemImpl.i();
    }

    public void a(boolean z2) {
        boolean z3;
        boolean z4 = true;
        boolean z5 = false;
        ViewGroup viewGroup = (ViewGroup) ((View) this.f).getParent();
        if (viewGroup != null) {
            android.support.v7.e.a.a(viewGroup);
        }
        super.a(z2);
        ((View) this.f).requestLayout();
        if (this.c != null) {
            ArrayList<MenuItemImpl> k2 = this.c.k();
            int size = k2.size();
            for (int i2 = 0; i2 < size; i2++) {
                ActionProvider supportActionProvider = k2.get(i2).getSupportActionProvider();
                if (supportActionProvider != null) {
                    supportActionProvider.setSubUiVisibilityListener(this);
                }
            }
        }
        ArrayList<MenuItemImpl> l2 = this.c != null ? this.c.l() : null;
        if (this.o && l2 != null) {
            int size2 = l2.size();
            if (size2 == 1) {
                if (!l2.get(0).isActionViewExpanded()) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                z5 = z3;
            } else {
                if (size2 <= 0) {
                    z4 = false;
                }
                z5 = z4;
            }
        }
        if (z5) {
            if (this.g == null) {
                this.g = new d(this.f94a);
            }
            ViewGroup viewGroup2 = (ViewGroup) this.g.getParent();
            if (viewGroup2 != this.f) {
                if (viewGroup2 != null) {
                    viewGroup2.removeView(this.g);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.f;
                actionMenuView.addView(this.g, actionMenuView.c());
            }
        } else if (this.g != null && this.g.getParent() == this.f) {
            ((ViewGroup) this.f).removeView(this.g);
        }
        ((ActionMenuView) this.f).setOverflowReserved(this.o);
    }

    public boolean a(ViewGroup viewGroup, int i2) {
        if (viewGroup.getChildAt(i2) == this.g) {
            return false;
        }
        return super.a(viewGroup, i2);
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        boolean z2;
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        SubMenuBuilder subMenuBuilder2 = subMenuBuilder;
        while (subMenuBuilder2.s() != this.c) {
            subMenuBuilder2 = (SubMenuBuilder) subMenuBuilder2.s();
        }
        View a2 = a(subMenuBuilder2.getItem());
        if (a2 == null) {
            return false;
        }
        this.l = subMenuBuilder.getItem().getItemId();
        int size = subMenuBuilder.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                z2 = false;
                break;
            }
            MenuItem item = subMenuBuilder.getItem(i2);
            if (item.isVisible() && item.getIcon() != null) {
                z2 = true;
                break;
            }
            i2++;
        }
        this.i = new a(this.b, subMenuBuilder, a2);
        this.i.a(z2);
        this.i.a();
        super.a(subMenuBuilder);
        return true;
    }

    private View a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.f;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            if ((childAt instanceof m.a) && ((m.a) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    public boolean d() {
        if (!this.o || h() || this.c == null || this.f == null || this.j != null || this.c.l().isEmpty()) {
            return false;
        }
        this.j = new C0005c(new e(this.b, this.c, this.g, true));
        ((View) this.f).post(this.j);
        super.a((SubMenuBuilder) null);
        return true;
    }

    public boolean e() {
        if (this.j == null || this.f == null) {
            e eVar = this.h;
            if (eVar == null) {
                return false;
            }
            eVar.d();
            return true;
        }
        ((View) this.f).removeCallbacks(this.j);
        this.j = null;
        return true;
    }

    public boolean f() {
        return e() | g();
    }

    public boolean g() {
        if (this.i == null) {
            return false;
        }
        this.i.d();
        return true;
    }

    public boolean h() {
        return this.h != null && this.h.f();
    }

    public boolean i() {
        return this.j != null || h();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ActionMenuView.a(android.view.View, int, int, int, int):int
     arg types: [android.view.View, int, int, int, int]
     candidates:
      android.support.v7.widget.LinearLayoutCompat.a(android.view.View, int, int, int, int):void
      android.support.v7.widget.ActionMenuView.a(android.view.View, int, int, int, int):int */
    public boolean b() {
        int i2;
        ArrayList<MenuItemImpl> arrayList;
        int i3;
        int i4;
        int i5;
        int i6;
        boolean z2;
        int i7;
        int i8;
        int i9;
        int i10;
        boolean z3;
        int i11;
        if (this.c != null) {
            ArrayList<MenuItemImpl> i12 = this.c.i();
            i2 = i12.size();
            arrayList = i12;
        } else {
            i2 = 0;
            arrayList = null;
        }
        int i13 = this.s;
        int i14 = this.r;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.f;
        int i15 = 0;
        int i16 = 0;
        boolean z4 = false;
        int i17 = 0;
        while (i17 < i2) {
            MenuItemImpl menuItemImpl = (MenuItemImpl) arrayList.get(i17);
            if (menuItemImpl.k()) {
                i15++;
            } else if (menuItemImpl.j()) {
                i16++;
            } else {
                z4 = true;
            }
            if (!this.w || !menuItemImpl.isActionViewExpanded()) {
                i11 = i13;
            } else {
                i11 = 0;
            }
            i17++;
            i13 = i11;
        }
        if (this.o && (z4 || i15 + i16 > i13)) {
            i13--;
        }
        int i18 = i13 - i15;
        SparseBooleanArray sparseBooleanArray = this.y;
        sparseBooleanArray.clear();
        int i19 = 0;
        if (this.u) {
            i19 = i14 / this.x;
            i3 = ((i14 % this.x) / i19) + this.x;
        } else {
            i3 = 0;
        }
        int i20 = 0;
        int i21 = 0;
        int i22 = i19;
        while (i20 < i2) {
            MenuItemImpl menuItemImpl2 = (MenuItemImpl) arrayList.get(i20);
            if (menuItemImpl2.k()) {
                View a2 = a(menuItemImpl2, this.z, viewGroup);
                if (this.z == null) {
                    this.z = a2;
                }
                if (this.u) {
                    i22 -= ActionMenuView.a(a2, i3, i22, makeMeasureSpec, 0);
                } else {
                    a2.measure(makeMeasureSpec, makeMeasureSpec);
                }
                i4 = a2.getMeasuredWidth();
                int i23 = i14 - i4;
                if (i21 != 0) {
                    i4 = i21;
                }
                int groupId = menuItemImpl2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                menuItemImpl2.d(true);
                i5 = i23;
                i6 = i18;
            } else if (menuItemImpl2.j()) {
                int groupId2 = menuItemImpl2.getGroupId();
                boolean z5 = sparseBooleanArray.get(groupId2);
                boolean z6 = (i18 > 0 || z5) && i14 > 0 && (!this.u || i22 > 0);
                if (z6) {
                    View a3 = a(menuItemImpl2, this.z, viewGroup);
                    if (this.z == null) {
                        this.z = a3;
                    }
                    if (this.u) {
                        int a4 = ActionMenuView.a(a3, i3, i22, makeMeasureSpec, 0);
                        int i24 = i22 - a4;
                        if (a4 == 0) {
                            z3 = false;
                        } else {
                            z3 = z6;
                        }
                        i10 = i24;
                    } else {
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                        boolean z7 = z6;
                        i10 = i22;
                        z3 = z7;
                    }
                    int measuredWidth = a3.getMeasuredWidth();
                    i14 -= measuredWidth;
                    if (i21 == 0) {
                        i21 = measuredWidth;
                    }
                    if (this.u) {
                        z2 = z3 & (i14 >= 0);
                        i7 = i21;
                        i8 = i10;
                    } else {
                        z2 = z3 & (i14 + i21 > 0);
                        i7 = i21;
                        i8 = i10;
                    }
                } else {
                    z2 = z6;
                    i7 = i21;
                    i8 = i22;
                }
                if (z2 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                    i9 = i18;
                } else if (z5) {
                    sparseBooleanArray.put(groupId2, false);
                    int i25 = i18;
                    for (int i26 = 0; i26 < i20; i26++) {
                        MenuItemImpl menuItemImpl3 = (MenuItemImpl) arrayList.get(i26);
                        if (menuItemImpl3.getGroupId() == groupId2) {
                            if (menuItemImpl3.i()) {
                                i25++;
                            }
                            menuItemImpl3.d(false);
                        }
                    }
                    i9 = i25;
                } else {
                    i9 = i18;
                }
                if (z2) {
                    i9--;
                }
                menuItemImpl2.d(z2);
                i4 = i7;
                i5 = i14;
                int i27 = i8;
                i6 = i9;
                i22 = i27;
            } else {
                menuItemImpl2.d(false);
                i4 = i21;
                i5 = i14;
                i6 = i18;
            }
            i20++;
            i14 = i5;
            i18 = i6;
            i21 = i4;
        }
        return true;
    }

    public void a(MenuBuilder menuBuilder, boolean z2) {
        f();
        super.a(menuBuilder, z2);
    }

    public void onSubUiVisibilityChanged(boolean z2) {
        if (z2) {
            super.a((SubMenuBuilder) null);
        } else if (this.c != null) {
            this.c.a(false);
        }
    }

    public void a(ActionMenuView actionMenuView) {
        this.f = actionMenuView;
        actionMenuView.a(this.c);
    }

    /* compiled from: ActionMenuPresenter */
    private class d extends AppCompatImageView implements ActionMenuView.a {
        private final float[] b = new float[2];

        public d(Context context) {
            super(context, null, a.C0002a.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            setOnTouchListener(new r(this, c.this) {
                public p a() {
                    if (c.this.h == null) {
                        return null;
                    }
                    return c.this.h.b();
                }

                public boolean b() {
                    c.this.d();
                    return true;
                }

                public boolean c() {
                    if (c.this.j != null) {
                        return false;
                    }
                    c.this.e();
                    return true;
                }
            });
        }

        public boolean performClick() {
            if (!super.performClick()) {
                playSoundEffect(0);
                c.this.d();
            }
            return true;
        }

        public boolean c() {
            return false;
        }

        public boolean d() {
            return false;
        }

        /* access modifiers changed from: protected */
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                DrawableCompat.setHotspotBounds(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    /* compiled from: ActionMenuPresenter */
    private class e extends k {
        public e(Context context, MenuBuilder menuBuilder, View view, boolean z) {
            super(context, menuBuilder, view, z, a.C0002a.actionOverflowMenuStyle);
            a((int) GravityCompat.END);
            a(c.this.k);
        }

        /* access modifiers changed from: protected */
        public void e() {
            if (c.this.c != null) {
                c.this.c.close();
            }
            c.this.h = null;
            super.e();
        }
    }

    /* compiled from: ActionMenuPresenter */
    private class a extends k {
        public a(Context context, SubMenuBuilder subMenuBuilder, View view) {
            super(context, subMenuBuilder, view, false, a.C0002a.actionOverflowMenuStyle);
            if (!((MenuItemImpl) subMenuBuilder.getItem()).i()) {
                a(c.this.g == null ? (View) c.this.f : c.this.g);
            }
            a(c.this.k);
        }

        /* access modifiers changed from: protected */
        public void e() {
            c.this.i = null;
            c.this.l = 0;
            super.e();
        }
    }

    /* compiled from: ActionMenuPresenter */
    private class f implements l.a {
        f() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            if (menuBuilder == null) {
                return false;
            }
            c.this.l = ((SubMenuBuilder) menuBuilder).getItem().getItemId();
            l.a a2 = c.this.a();
            return a2 != null ? a2.a(menuBuilder) : false;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            if (menuBuilder instanceof SubMenuBuilder) {
                menuBuilder.p().a(false);
            }
            l.a a2 = c.this.a();
            if (a2 != null) {
                a2.a(menuBuilder, z);
            }
        }
    }

    /* renamed from: android.support.v7.widget.c$c  reason: collision with other inner class name */
    /* compiled from: ActionMenuPresenter */
    private class C0005c implements Runnable {
        private e b;

        public C0005c(e eVar) {
            this.b = eVar;
        }

        public void run() {
            if (c.this.c != null) {
                c.this.c.f();
            }
            View view = (View) c.this.f;
            if (!(view == null || view.getWindowToken() == null || !this.b.c())) {
                c.this.h = this.b;
            }
            c.this.j = null;
        }
    }

    /* compiled from: ActionMenuPresenter */
    private class b extends ActionMenuItemView.b {
        b() {
        }

        public p a() {
            if (c.this.i != null) {
                return c.this.i.b();
            }
            return null;
        }
    }
}
