package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.d;
import java.lang.ref.WeakReference;

/* compiled from: VectorEnabledTintResources */
public class ae extends Resources {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<Context> f211a;

    public static boolean a() {
        return d.k() && Build.VERSION.SDK_INT <= 20;
    }

    public ae(Context context, Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f211a = new WeakReference<>(context);
    }

    public Drawable getDrawable(int i) throws Resources.NotFoundException {
        Context context = this.f211a.get();
        if (context != null) {
            return g.a().a(context, this, i);
        }
        return super.getDrawable(i);
    }

    /* access modifiers changed from: package-private */
    public final Drawable a(int i) {
        return super.getDrawable(i);
    }
}
