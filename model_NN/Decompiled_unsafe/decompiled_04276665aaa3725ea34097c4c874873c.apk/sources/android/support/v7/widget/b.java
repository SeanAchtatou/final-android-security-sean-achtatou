package android.support.v7.widget;

import android.graphics.Outline;

/* compiled from: ActionBarBackgroundDrawableV21 */
class b extends a {
    public b(ActionBarContainer actionBarContainer) {
        super(actionBarContainer);
    }

    public void getOutline(Outline outline) {
        if (this.f205a.d) {
            if (this.f205a.c != null) {
                this.f205a.c.getOutline(outline);
            }
        } else if (this.f205a.f116a != null) {
            this.f205a.f116a.getOutline(outline);
        }
    }
}
