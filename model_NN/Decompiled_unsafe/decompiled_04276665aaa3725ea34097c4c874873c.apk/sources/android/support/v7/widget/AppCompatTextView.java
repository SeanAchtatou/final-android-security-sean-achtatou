package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.view.TintableBackgroundView;
import android.util.AttributeSet;
import android.widget.TextView;

public class AppCompatTextView extends TextView implements TintableBackgroundView {

    /* renamed from: a  reason: collision with root package name */
    private e f154a;
    private k b;

    public AppCompatTextView(Context context) {
        this(context, null);
    }

    public AppCompatTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842884);
    }

    public AppCompatTextView(Context context, AttributeSet attributeSet, int i) {
        super(z.a(context), attributeSet, i);
        this.f154a = new e(this);
        this.f154a.a(attributeSet, i);
        this.b = k.a(this);
        this.b.a(attributeSet, i);
        this.b.a();
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f154a != null) {
            this.f154a.a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f154a != null) {
            this.f154a.a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f154a != null) {
            this.f154a.a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f154a != null) {
            return this.f154a.a();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.f154a != null) {
            this.f154a.a(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (this.f154a != null) {
            return this.f154a.b();
        }
        return null;
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.b != null) {
            this.b.a(context, i);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f154a != null) {
            this.f154a.c();
        }
        if (this.b != null) {
            this.b.a();
        }
    }
}
