package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.widget.SeekBar;

/* compiled from: AppCompatSeekBarHelper */
class j extends i {

    /* renamed from: a  reason: collision with root package name */
    private final SeekBar f231a;
    private Drawable b;
    private ColorStateList c = null;
    private PorterDuff.Mode d = null;
    private boolean e = false;
    private boolean f = false;

    j(SeekBar seekBar) {
        super(seekBar);
        this.f231a = seekBar;
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        super.a(attributeSet, i);
        ac a2 = ac.a(this.f231a.getContext(), attributeSet, a.k.AppCompatSeekBar, i, 0);
        Drawable b2 = a2.b(a.k.AppCompatSeekBar_android_thumb);
        if (b2 != null) {
            this.f231a.setThumb(b2);
        }
        a(a2.a(a.k.AppCompatSeekBar_tickMark));
        if (a2.g(a.k.AppCompatSeekBar_tickMarkTintMode)) {
            this.d = o.a(a2.a(a.k.AppCompatSeekBar_tickMarkTintMode, -1), this.d);
            this.f = true;
        }
        if (a2.g(a.k.AppCompatSeekBar_tickMarkTint)) {
            this.c = a2.e(a.k.AppCompatSeekBar_tickMarkTint);
            this.e = true;
        }
        a2.a();
        d();
    }

    /* access modifiers changed from: package-private */
    public void a(Drawable drawable) {
        if (this.b != null) {
            this.b.setCallback(null);
        }
        this.b = drawable;
        if (drawable != null) {
            drawable.setCallback(this.f231a);
            DrawableCompat.setLayoutDirection(drawable, ViewCompat.getLayoutDirection(this.f231a));
            if (drawable.isStateful()) {
                drawable.setState(this.f231a.getDrawableState());
            }
            d();
        }
        this.f231a.invalidate();
    }

    private void d() {
        if (this.b == null) {
            return;
        }
        if (this.e || this.f) {
            this.b = DrawableCompat.wrap(this.b.mutate());
            if (this.e) {
                DrawableCompat.setTintList(this.b, this.c);
            }
            if (this.f) {
                DrawableCompat.setTintMode(this.b, this.d);
            }
            if (this.b.isStateful()) {
                this.b.setState(this.f231a.getDrawableState());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.b != null) {
            this.b.jumpToCurrentState();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Drawable drawable = this.b;
        if (drawable != null && drawable.isStateful() && drawable.setState(this.f231a.getDrawableState())) {
            this.f231a.invalidateDrawable(drawable);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas) {
        int max;
        int i = 1;
        if (this.b != null && (max = this.f231a.getMax()) > 1) {
            int intrinsicWidth = this.b.getIntrinsicWidth();
            int intrinsicHeight = this.b.getIntrinsicHeight();
            int i2 = intrinsicWidth >= 0 ? intrinsicWidth / 2 : 1;
            if (intrinsicHeight >= 0) {
                i = intrinsicHeight / 2;
            }
            this.b.setBounds(-i2, -i, i2, i);
            float width = ((float) ((this.f231a.getWidth() - this.f231a.getPaddingLeft()) - this.f231a.getPaddingRight())) / ((float) max);
            int save = canvas.save();
            canvas.translate((float) this.f231a.getPaddingLeft(), (float) (this.f231a.getHeight() / 2));
            for (int i3 = 0; i3 <= max; i3++) {
                this.b.draw(canvas);
                canvas.translate(width, 0.0f);
            }
            canvas.restoreToCount(save);
        }
    }
}
