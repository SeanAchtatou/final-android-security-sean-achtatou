package android.support.v7.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.e;
import android.support.v7.view.f;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.Window;

/* compiled from: AppCompatDelegateImplV14 */
class h extends g {
    private int t = -100;
    private boolean u;
    private boolean v = true;
    private b w;

    h(Context context, Window window, c cVar) {
        super(context, window, cVar);
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        if (bundle != null && this.t == -100) {
            this.t = bundle.getInt("appcompat:local_night_mode", -100);
        }
    }

    /* access modifiers changed from: package-private */
    public Window.Callback a(Window.Callback callback) {
        return new a(callback);
    }

    public boolean o() {
        return this.v;
    }

    public boolean i() {
        boolean z = false;
        int w2 = w();
        int d = d(w2);
        if (d != -1) {
            z = h(d);
        }
        if (w2 == 0) {
            x();
            this.w.c();
        }
        this.u = true;
        return z;
    }

    public void c() {
        super.c();
        i();
    }

    public void d() {
        super.d();
        if (this.w != null) {
            this.w.d();
        }
    }

    /* access modifiers changed from: package-private */
    public int d(int i) {
        switch (i) {
            case -100:
                return -1;
            case 0:
                x();
                return this.w.a();
            default:
                return i;
        }
    }

    private int w() {
        return this.t != -100 ? this.t : j();
    }

    public void c(Bundle bundle) {
        super.c(bundle);
        if (this.t != -100) {
            bundle.putInt("appcompat:local_night_mode", this.t);
        }
    }

    public void g() {
        super.g();
        if (this.w != null) {
            this.w.d();
        }
    }

    private boolean h(int i) {
        Resources resources = this.f37a.getResources();
        Configuration configuration = resources.getConfiguration();
        int i2 = configuration.uiMode & 48;
        int i3 = i == 2 ? 32 : 16;
        if (i2 == i3) {
            return false;
        }
        if (y()) {
            ((Activity) this.f37a).recreate();
        } else {
            Configuration configuration2 = new Configuration(configuration);
            DisplayMetrics displayMetrics = resources.getDisplayMetrics();
            float f = configuration2.fontScale;
            configuration2.uiMode = i3 | (configuration2.uiMode & -49);
            configuration2.fontScale = 2.0f * f;
            resources.updateConfiguration(configuration2, displayMetrics);
            configuration2.fontScale = f;
            resources.updateConfiguration(configuration2, displayMetrics);
        }
        return true;
    }

    private void x() {
        if (this.w == null) {
            this.w = new b(s.a(this.f37a));
        }
    }

    private boolean y() {
        if (!this.u || !(this.f37a instanceof Activity)) {
            return false;
        }
        try {
            if ((this.f37a.getPackageManager().getActivityInfo(new ComponentName(this.f37a, this.f37a.getClass()), 0).configChanges & 512) == 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("AppCompatDelegate", "Exception while getting ActivityInfo", e);
            return true;
        }
    }

    /* compiled from: AppCompatDelegateImplV14 */
    class a extends e.a {
        a(Window.Callback callback) {
            super(callback);
        }

        public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
            if (h.this.o()) {
                return a(callback);
            }
            return super.onWindowStartingActionMode(callback);
        }

        /* access modifiers changed from: package-private */
        public final ActionMode a(ActionMode.Callback callback) {
            f.a aVar = new f.a(h.this.f37a, callback);
            android.support.v7.view.b b = h.this.b(aVar);
            if (b != null) {
                return aVar.b(b);
            }
            return null;
        }
    }

    /* compiled from: AppCompatDelegateImplV14 */
    final class b {
        private s b;
        private boolean c;
        private BroadcastReceiver d;
        private IntentFilter e;

        b(s sVar) {
            this.b = sVar;
            this.c = sVar.a();
        }

        /* access modifiers changed from: package-private */
        public final int a() {
            return this.c ? 2 : 1;
        }

        /* access modifiers changed from: package-private */
        public final void b() {
            boolean a2 = this.b.a();
            if (a2 != this.c) {
                this.c = a2;
                h.this.i();
            }
        }

        /* access modifiers changed from: package-private */
        public final void c() {
            d();
            if (this.d == null) {
                this.d = new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        b.this.b();
                    }
                };
            }
            if (this.e == null) {
                this.e = new IntentFilter();
                this.e.addAction("android.intent.action.TIME_SET");
                this.e.addAction("android.intent.action.TIMEZONE_CHANGED");
                this.e.addAction("android.intent.action.TIME_TICK");
            }
            h.this.f37a.registerReceiver(this.d, this.e);
        }

        /* access modifiers changed from: package-private */
        public final void d() {
            if (this.d != null) {
                h.this.f37a.unregisterReceiver(this.d);
                this.d = null;
            }
        }
    }
}
