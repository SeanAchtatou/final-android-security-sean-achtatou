package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.LayoutInflaterFactory;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v4.widget.PopupWindowCompat;
import android.support.v7.a.a;
import android.support.v7.view.b;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.l;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.ViewStubCompat;
import android.support.v7.widget.ae;
import android.support.v7.widget.af;
import android.support.v7.widget.g;
import android.support.v7.widget.m;
import android.support.v7.widget.q;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

/* compiled from: AppCompatDelegateImplV9 */
class j extends e implements LayoutInflaterFactory, MenuBuilder.a {
    private boolean A;
    private boolean B;
    private boolean C;
    private d[] D;
    private d E;
    private boolean F;
    private final Runnable G = new Runnable() {
        public void run() {
            if ((j.this.s & 1) != 0) {
                j.this.f(0);
            }
            if ((j.this.s & 4096) != 0) {
                j.this.f(108);
            }
            j.this.r = false;
            j.this.s = 0;
        }
    };
    private boolean H;
    private Rect I;
    private Rect J;
    private l K;
    android.support.v7.view.b m;
    ActionBarContextView n;
    PopupWindow o;
    Runnable p;
    ViewPropertyAnimatorCompat q = null;
    boolean r;
    int s;
    private m t;
    private a u;
    private e v;
    private boolean w;
    private ViewGroup x;
    private TextView y;
    private View z;

    j(Context context, Window window, c cVar) {
        super(context, window, cVar);
    }

    public void a(Bundle bundle) {
        if ((this.c instanceof Activity) && NavUtils.getParentActivityName((Activity) this.c) != null) {
            ActionBar m2 = m();
            if (m2 == null) {
                this.H = true;
            } else {
                m2.c(true);
            }
        }
    }

    public void b(Bundle bundle) {
        w();
    }

    public void l() {
        w();
        if (this.h && this.f == null) {
            if (this.c instanceof Activity) {
                this.f = new t((Activity) this.c, this.i);
            } else if (this.c instanceof Dialog) {
                this.f = new t((Dialog) this.c);
            }
            if (this.f != null) {
                this.f.c(this.H);
            }
        }
    }

    public View a(int i) {
        w();
        return this.b.findViewById(i);
    }

    public void a(Configuration configuration) {
        ActionBar a2;
        if (this.h && this.w && (a2 = a()) != null) {
            a2.a(configuration);
        }
        g.a().a(this.f37a);
        i();
    }

    public void d() {
        ActionBar a2 = a();
        if (a2 != null) {
            a2.d(false);
        }
    }

    public void e() {
        ActionBar a2 = a();
        if (a2 != null) {
            a2.d(true);
        }
    }

    public void a(View view) {
        w();
        ViewGroup viewGroup = (ViewGroup) this.x.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.c.onContentChanged();
    }

    public void b(int i) {
        w();
        ViewGroup viewGroup = (ViewGroup) this.x.findViewById(16908290);
        viewGroup.removeAllViews();
        LayoutInflater.from(this.f37a).inflate(i, viewGroup);
        this.c.onContentChanged();
    }

    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        w();
        ViewGroup viewGroup = (ViewGroup) this.x.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.c.onContentChanged();
    }

    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        w();
        ((ViewGroup) this.x.findViewById(16908290)).addView(view, layoutParams);
        this.c.onContentChanged();
    }

    public void g() {
        if (this.r) {
            this.b.getDecorView().removeCallbacks(this.G);
        }
        super.g();
        if (this.f != null) {
            this.f.h();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
     arg types: [int, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d */
    private void w() {
        if (!this.w) {
            this.x = x();
            CharSequence r2 = r();
            if (!TextUtils.isEmpty(r2)) {
                b(r2);
            }
            y();
            a(this.x);
            this.w = true;
            d a2 = a(0, false);
            if (p()) {
                return;
            }
            if (a2 == null || a2.j == null) {
                d(108);
            }
        }
    }

    private ViewGroup x() {
        ViewGroup viewGroup;
        ViewGroup viewGroup2;
        Context context;
        TypedArray obtainStyledAttributes = this.f37a.obtainStyledAttributes(a.k.AppCompatTheme);
        if (!obtainStyledAttributes.hasValue(a.k.AppCompatTheme_windowActionBar)) {
            obtainStyledAttributes.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
        if (obtainStyledAttributes.getBoolean(a.k.AppCompatTheme_windowNoTitle, false)) {
            c(1);
        } else if (obtainStyledAttributes.getBoolean(a.k.AppCompatTheme_windowActionBar, false)) {
            c(108);
        }
        if (obtainStyledAttributes.getBoolean(a.k.AppCompatTheme_windowActionBarOverlay, false)) {
            c(109);
        }
        if (obtainStyledAttributes.getBoolean(a.k.AppCompatTheme_windowActionModeOverlay, false)) {
            c(10);
        }
        this.k = obtainStyledAttributes.getBoolean(a.k.AppCompatTheme_android_windowIsFloating, false);
        obtainStyledAttributes.recycle();
        this.b.getDecorView();
        LayoutInflater from = LayoutInflater.from(this.f37a);
        if (this.l) {
            if (this.j) {
                viewGroup = (ViewGroup) from.inflate(a.h.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
            } else {
                viewGroup = (ViewGroup) from.inflate(a.h.abc_screen_simple, (ViewGroup) null);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                ViewCompat.setOnApplyWindowInsetsListener(viewGroup, new OnApplyWindowInsetsListener() {
                    public WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                        int systemWindowInsetTop = windowInsetsCompat.getSystemWindowInsetTop();
                        int g = j.this.g(systemWindowInsetTop);
                        if (systemWindowInsetTop != g) {
                            windowInsetsCompat = windowInsetsCompat.replaceSystemWindowInsets(windowInsetsCompat.getSystemWindowInsetLeft(), g, windowInsetsCompat.getSystemWindowInsetRight(), windowInsetsCompat.getSystemWindowInsetBottom());
                        }
                        return ViewCompat.onApplyWindowInsets(view, windowInsetsCompat);
                    }
                });
                viewGroup2 = viewGroup;
            } else {
                ((q) viewGroup).setOnFitSystemWindowsListener(new q.a() {
                    public void a(Rect rect) {
                        rect.top = j.this.g(rect.top);
                    }
                });
                viewGroup2 = viewGroup;
            }
        } else if (this.k) {
            this.i = false;
            this.h = false;
            viewGroup2 = (ViewGroup) from.inflate(a.h.abc_dialog_title_material, (ViewGroup) null);
        } else if (this.h) {
            TypedValue typedValue = new TypedValue();
            this.f37a.getTheme().resolveAttribute(a.C0002a.actionBarTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                context = new android.support.v7.view.d(this.f37a, typedValue.resourceId);
            } else {
                context = this.f37a;
            }
            ViewGroup viewGroup3 = (ViewGroup) LayoutInflater.from(context).inflate(a.h.abc_screen_toolbar, (ViewGroup) null);
            this.t = (m) viewGroup3.findViewById(a.f.decor_content_parent);
            this.t.setWindowCallback(q());
            if (this.i) {
                this.t.a(109);
            }
            if (this.A) {
                this.t.a(2);
            }
            if (this.B) {
                this.t.a(5);
            }
            viewGroup2 = viewGroup3;
        } else {
            viewGroup2 = null;
        }
        if (viewGroup2 == null) {
            throw new IllegalArgumentException("AppCompat does not support the current theme features: { windowActionBar: " + this.h + ", windowActionBarOverlay: " + this.i + ", android:windowIsFloating: " + this.k + ", windowActionModeOverlay: " + this.j + ", windowNoTitle: " + this.l + " }");
        }
        if (this.t == null) {
            this.y = (TextView) viewGroup2.findViewById(a.f.title);
        }
        af.b(viewGroup2);
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup2.findViewById(a.f.action_bar_activity_content);
        ViewGroup viewGroup4 = (ViewGroup) this.b.findViewById(16908290);
        if (viewGroup4 != null) {
            while (viewGroup4.getChildCount() > 0) {
                View childAt = viewGroup4.getChildAt(0);
                viewGroup4.removeViewAt(0);
                contentFrameLayout.addView(childAt);
            }
            viewGroup4.setId(-1);
            contentFrameLayout.setId(16908290);
            if (viewGroup4 instanceof FrameLayout) {
                ((FrameLayout) viewGroup4).setForeground(null);
            }
        }
        this.b.setContentView(viewGroup2);
        contentFrameLayout.setAttachListener(new ContentFrameLayout.a() {
            public void a() {
            }

            public void b() {
                j.this.v();
            }
        });
        return viewGroup2;
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup) {
    }

    private void y() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.x.findViewById(16908290);
        View decorView = this.b.getDecorView();
        contentFrameLayout.a(decorView.getPaddingLeft(), decorView.getPaddingTop(), decorView.getPaddingRight(), decorView.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.f37a.obtainStyledAttributes(a.k.AppCompatTheme);
        obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTheme_windowFixedWidthMajor)) {
            obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTheme_windowFixedWidthMinor)) {
            obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTheme_windowFixedHeightMajor)) {
            obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTheme_windowFixedHeightMinor)) {
            obtainStyledAttributes.getValue(a.k.AppCompatTheme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    public boolean c(int i) {
        int h = h(i);
        if (this.l && h == 108) {
            return false;
        }
        if (this.h && h == 1) {
            this.h = false;
        }
        switch (h) {
            case 1:
                z();
                this.l = true;
                return true;
            case 2:
                z();
                this.A = true;
                return true;
            case 5:
                z();
                this.B = true;
                return true;
            case 10:
                z();
                this.j = true;
                return true;
            case 108:
                z();
                this.h = true;
                return true;
            case 109:
                z();
                this.i = true;
                return true;
            default:
                return this.b.requestFeature(h);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(CharSequence charSequence) {
        if (this.t != null) {
            this.t.setWindowTitle(charSequence);
        } else if (m() != null) {
            m().a(charSequence);
        } else if (this.y != null) {
            this.y.setText(charSequence);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
     arg types: [int, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
     arg types: [android.support.v7.app.j$d, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(int i, Menu menu) {
        if (i == 108) {
            ActionBar a2 = a();
            if (a2 != null) {
                a2.e(false);
            }
        } else if (i == 0) {
            d a3 = a(i, true);
            if (a3.o) {
                a(a3, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i, Menu menu) {
        if (i != 108) {
            return false;
        }
        ActionBar a2 = a();
        if (a2 == null) {
            return true;
        }
        a2.e(true);
        return true;
    }

    public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
        d a2;
        Window.Callback q2 = q();
        if (q2 == null || p() || (a2 = a((Menu) menuBuilder.p())) == null) {
            return false;
        }
        return q2.onMenuItemSelected(a2.f53a, menuItem);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
     arg types: [android.support.v7.view.menu.MenuBuilder, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void */
    public void a(MenuBuilder menuBuilder) {
        a(menuBuilder, true);
    }

    public android.support.v7.view.b b(b.a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("ActionMode callback can not be null.");
        }
        if (this.m != null) {
            this.m.c();
        }
        b bVar = new b(aVar);
        ActionBar a2 = a();
        if (a2 != null) {
            this.m = a2.a(bVar);
            if (!(this.m == null || this.e == null)) {
                this.e.a(this.m);
            }
        }
        if (this.m == null) {
            this.m = a(bVar);
        }
        return this.m;
    }

    public void f() {
        ActionBar a2 = a();
        if (a2 == null || !a2.e()) {
            d(0);
        }
    }

    /* access modifiers changed from: package-private */
    public android.support.v7.view.b a(b.a aVar) {
        android.support.v7.view.b bVar;
        boolean z2;
        Context context;
        t();
        if (this.m != null) {
            this.m.c();
        }
        if (!(aVar instanceof b)) {
            aVar = new b(aVar);
        }
        if (this.e == null || p()) {
            bVar = null;
        } else {
            try {
                bVar = this.e.a(aVar);
            } catch (AbstractMethodError e2) {
                bVar = null;
            }
        }
        if (bVar != null) {
            this.m = bVar;
        } else {
            if (this.n == null) {
                if (this.k) {
                    TypedValue typedValue = new TypedValue();
                    Resources.Theme theme = this.f37a.getTheme();
                    theme.resolveAttribute(a.C0002a.actionBarTheme, typedValue, true);
                    if (typedValue.resourceId != 0) {
                        Resources.Theme newTheme = this.f37a.getResources().newTheme();
                        newTheme.setTo(theme);
                        newTheme.applyStyle(typedValue.resourceId, true);
                        context = new android.support.v7.view.d(this.f37a, 0);
                        context.getTheme().setTo(newTheme);
                    } else {
                        context = this.f37a;
                    }
                    this.n = new ActionBarContextView(context);
                    this.o = new PopupWindow(context, (AttributeSet) null, a.C0002a.actionModePopupWindowStyle);
                    PopupWindowCompat.setWindowLayoutType(this.o, 2);
                    this.o.setContentView(this.n);
                    this.o.setWidth(-1);
                    context.getTheme().resolveAttribute(a.C0002a.actionBarSize, typedValue, true);
                    this.n.setContentHeight(TypedValue.complexToDimensionPixelSize(typedValue.data, context.getResources().getDisplayMetrics()));
                    this.o.setHeight(-2);
                    this.p = new Runnable() {
                        public void run() {
                            j.this.o.showAtLocation(j.this.n, 55, 0, 0);
                            j.this.t();
                            if (j.this.s()) {
                                ViewCompat.setAlpha(j.this.n, 0.0f);
                                j.this.q = ViewCompat.animate(j.this.n).alpha(1.0f);
                                j.this.q.setListener(new ViewPropertyAnimatorListenerAdapter() {
                                    public void onAnimationStart(View view) {
                                        j.this.n.setVisibility(0);
                                    }

                                    public void onAnimationEnd(View view) {
                                        ViewCompat.setAlpha(j.this.n, 1.0f);
                                        j.this.q.setListener(null);
                                        j.this.q = null;
                                    }
                                });
                                return;
                            }
                            ViewCompat.setAlpha(j.this.n, 1.0f);
                            j.this.n.setVisibility(0);
                        }
                    };
                } else {
                    ViewStubCompat viewStubCompat = (ViewStubCompat) this.x.findViewById(a.f.action_mode_bar_stub);
                    if (viewStubCompat != null) {
                        viewStubCompat.setLayoutInflater(LayoutInflater.from(n()));
                        this.n = (ActionBarContextView) viewStubCompat.a();
                    }
                }
            }
            if (this.n != null) {
                t();
                this.n.c();
                Context context2 = this.n.getContext();
                ActionBarContextView actionBarContextView = this.n;
                if (this.o == null) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                android.support.v7.view.e eVar = new android.support.v7.view.e(context2, actionBarContextView, aVar, z2);
                if (aVar.a(eVar, eVar.b())) {
                    eVar.d();
                    this.n.a(eVar);
                    this.m = eVar;
                    if (s()) {
                        ViewCompat.setAlpha(this.n, 0.0f);
                        this.q = ViewCompat.animate(this.n).alpha(1.0f);
                        this.q.setListener(new ViewPropertyAnimatorListenerAdapter() {
                            public void onAnimationStart(View view) {
                                j.this.n.setVisibility(0);
                                j.this.n.sendAccessibilityEvent(32);
                                if (j.this.n.getParent() instanceof View) {
                                    ViewCompat.requestApplyInsets((View) j.this.n.getParent());
                                }
                            }

                            public void onAnimationEnd(View view) {
                                ViewCompat.setAlpha(j.this.n, 1.0f);
                                j.this.q.setListener(null);
                                j.this.q = null;
                            }
                        });
                    } else {
                        ViewCompat.setAlpha(this.n, 1.0f);
                        this.n.setVisibility(0);
                        this.n.sendAccessibilityEvent(32);
                        if (this.n.getParent() instanceof View) {
                            ViewCompat.requestApplyInsets((View) this.n.getParent());
                        }
                    }
                    if (this.o != null) {
                        this.b.getDecorView().post(this.p);
                    }
                } else {
                    this.m = null;
                }
            }
        }
        if (!(this.m == null || this.e == null)) {
            this.e.a(this.m);
        }
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public final boolean s() {
        return this.w && this.x != null && ViewCompat.isLaidOut(this.x);
    }

    /* access modifiers changed from: package-private */
    public void t() {
        if (this.q != null) {
            this.q.cancel();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean u() {
        if (this.m != null) {
            this.m.c();
            return true;
        }
        ActionBar a2 = a();
        if (a2 == null || !a2.f()) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
     arg types: [int, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d */
    /* access modifiers changed from: package-private */
    public boolean a(int i, KeyEvent keyEvent) {
        ActionBar a2 = a();
        if (a2 != null && a2.a(i, keyEvent)) {
            return true;
        }
        if (this.E == null || !a(this.E, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.E == null) {
                d a3 = a(0, true);
                b(a3, keyEvent);
                boolean a4 = a(a3, keyEvent.getKeyCode(), keyEvent, 1);
                a3.m = false;
                if (a4) {
                    return true;
                }
            }
            return false;
        } else if (this.E == null) {
            return true;
        } else {
            this.E.n = true;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(KeyEvent keyEvent) {
        boolean z2 = true;
        if (keyEvent.getKeyCode() == 82 && this.c.dispatchKeyEvent(keyEvent)) {
            return true;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 0) {
            z2 = false;
        }
        return z2 ? c(keyCode, keyEvent) : b(keyCode, keyEvent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
     arg types: [int, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
     arg types: [android.support.v7.app.j$d, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void */
    /* access modifiers changed from: package-private */
    public boolean b(int i, KeyEvent keyEvent) {
        switch (i) {
            case 82:
                e(0, keyEvent);
                return true;
            case 4:
                boolean z2 = this.F;
                this.F = false;
                d a2 = a(0, false);
                if (a2 == null || !a2.o) {
                    if (u()) {
                        return true;
                    }
                } else if (z2) {
                    return true;
                } else {
                    a(a2, true);
                    return true;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i, KeyEvent keyEvent) {
        boolean z2 = true;
        switch (i) {
            case 82:
                d(0, keyEvent);
                return true;
            case 4:
                if ((keyEvent.getFlags() & 128) == 0) {
                    z2 = false;
                }
                this.F = z2;
                break;
        }
        if (Build.VERSION.SDK_INT < 11) {
            a(i, keyEvent);
        }
        return false;
    }

    public View b(View view, String str, Context context, AttributeSet attributeSet) {
        boolean z2;
        boolean z3 = Build.VERSION.SDK_INT < 21;
        if (this.K == null) {
            this.K = new l();
        }
        if (!z3 || !a((ViewParent) view)) {
            z2 = false;
        } else {
            z2 = true;
        }
        return this.K.a(view, str, context, attributeSet, z2, z3, true, ae.a());
    }

    private boolean a(ViewParent viewParent) {
        if (viewParent == null) {
            return false;
        }
        View decorView = this.b.getDecorView();
        for (ViewParent viewParent2 = viewParent; viewParent2 != null; viewParent2 = viewParent2.getParent()) {
            if (viewParent2 == decorView || !(viewParent2 instanceof View) || ViewCompat.isAttachedToWindow((View) viewParent2)) {
                return false;
            }
        }
        return true;
    }

    public void h() {
        LayoutInflater from = LayoutInflater.from(this.f37a);
        if (from.getFactory() == null) {
            LayoutInflaterCompat.setFactory(from, this);
        } else if (!(LayoutInflaterCompat.getFactory(from) instanceof j)) {
            Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        View a2 = a(view, str, context, attributeSet);
        return a2 != null ? a2 : b(view, str, context, attributeSet);
    }

    /* access modifiers changed from: package-private */
    public View a(View view, String str, Context context, AttributeSet attributeSet) {
        View onCreateView;
        if (!(this.c instanceof LayoutInflater.Factory) || (onCreateView = ((LayoutInflater.Factory) this.c).onCreateView(str, context, attributeSet)) == null) {
            return null;
        }
        return onCreateView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
     arg types: [android.support.v7.app.j$d, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void */
    private void a(d dVar, KeyEvent keyEvent) {
        ViewGroup.LayoutParams layoutParams;
        ViewGroup.LayoutParams layoutParams2;
        boolean z2;
        int i = -1;
        if (!dVar.o && !p()) {
            if (dVar.f53a == 0) {
                Context context = this.f37a;
                boolean z3 = (context.getResources().getConfiguration().screenLayout & 15) == 4;
                if (context.getApplicationInfo().targetSdkVersion >= 11) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                if (z3 && z2) {
                    return;
                }
            }
            Window.Callback q2 = q();
            if (q2 == null || q2.onMenuOpened(dVar.f53a, dVar.j)) {
                WindowManager windowManager = (WindowManager) this.f37a.getSystemService("window");
                if (windowManager != null && b(dVar, keyEvent)) {
                    if (dVar.g == null || dVar.q) {
                        if (dVar.g == null) {
                            if (!a(dVar) || dVar.g == null) {
                                return;
                            }
                        } else if (dVar.q && dVar.g.getChildCount() > 0) {
                            dVar.g.removeAllViews();
                        }
                        if (c(dVar) && dVar.a()) {
                            ViewGroup.LayoutParams layoutParams3 = dVar.h.getLayoutParams();
                            if (layoutParams3 == null) {
                                layoutParams = new ViewGroup.LayoutParams(-2, -2);
                            } else {
                                layoutParams = layoutParams3;
                            }
                            dVar.g.setBackgroundResource(dVar.b);
                            ViewParent parent = dVar.h.getParent();
                            if (parent != null && (parent instanceof ViewGroup)) {
                                ((ViewGroup) parent).removeView(dVar.h);
                            }
                            dVar.g.addView(dVar.h, layoutParams);
                            if (!dVar.h.hasFocus()) {
                                dVar.h.requestFocus();
                            }
                            i = -2;
                        } else {
                            return;
                        }
                    } else if (dVar.i == null || (layoutParams2 = dVar.i.getLayoutParams()) == null || layoutParams2.width != -1) {
                        i = -2;
                    }
                    dVar.n = false;
                    WindowManager.LayoutParams layoutParams4 = new WindowManager.LayoutParams(i, -2, dVar.d, dVar.e, 1002, 8519680, -3);
                    layoutParams4.gravity = dVar.c;
                    layoutParams4.windowAnimations = dVar.f;
                    windowManager.addView(dVar.g, layoutParams4);
                    dVar.o = true;
                    return;
                }
                return;
            }
            a(dVar, true);
        }
    }

    private boolean a(d dVar) {
        dVar.a(n());
        dVar.g = new c(dVar.l);
        dVar.c = 81;
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
     arg types: [int, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
     arg types: [android.support.v7.app.j$d, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void */
    private void a(MenuBuilder menuBuilder, boolean z2) {
        if (this.t == null || !this.t.e() || (ViewConfigurationCompat.hasPermanentMenuKey(ViewConfiguration.get(this.f37a)) && !this.t.g())) {
            d a2 = a(0, true);
            a2.q = true;
            a(a2, false);
            a(a2, (KeyEvent) null);
            return;
        }
        Window.Callback q2 = q();
        if (this.t.f() && z2) {
            this.t.i();
            if (!p()) {
                q2.onPanelClosed(108, a(0, true).j);
            }
        } else if (q2 != null && !p()) {
            if (this.r && (this.s & 1) != 0) {
                this.b.getDecorView().removeCallbacks(this.G);
                this.G.run();
            }
            d a3 = a(0, true);
            if (a3.j != null && !a3.r && q2.onPreparePanel(0, a3.i, a3.j)) {
                q2.onMenuOpened(108, a3.j);
                this.t.h();
            }
        }
    }

    private boolean b(d dVar) {
        Context context;
        Context context2 = this.f37a;
        if ((dVar.f53a == 0 || dVar.f53a == 108) && this.t != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = context2.getTheme();
            theme.resolveAttribute(a.C0002a.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = context2.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(a.C0002a.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(a.C0002a.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context2.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            Resources.Theme theme3 = theme2;
            if (theme3 != null) {
                context = new android.support.v7.view.d(context2, 0);
                context.getTheme().setTo(theme3);
                MenuBuilder menuBuilder = new MenuBuilder(context);
                menuBuilder.a(this);
                dVar.a(menuBuilder);
                return true;
            }
        }
        context = context2;
        MenuBuilder menuBuilder2 = new MenuBuilder(context);
        menuBuilder2.a(this);
        dVar.a(menuBuilder2);
        return true;
    }

    private boolean c(d dVar) {
        if (dVar.i != null) {
            dVar.h = dVar.i;
            return true;
        } else if (dVar.j == null) {
            return false;
        } else {
            if (this.v == null) {
                this.v = new e();
            }
            dVar.h = (View) dVar.a(this.v);
            return dVar.h != null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
     arg types: [android.support.v7.app.j$d, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void */
    private boolean b(d dVar, KeyEvent keyEvent) {
        boolean z2;
        if (p()) {
            return false;
        }
        if (dVar.m) {
            return true;
        }
        if (!(this.E == null || this.E == dVar)) {
            a(this.E, false);
        }
        Window.Callback q2 = q();
        if (q2 != null) {
            dVar.i = q2.onCreatePanelView(dVar.f53a);
        }
        boolean z3 = dVar.f53a == 0 || dVar.f53a == 108;
        if (z3 && this.t != null) {
            this.t.j();
        }
        if (dVar.i == null && (!z3 || !(m() instanceof q))) {
            if (dVar.j == null || dVar.r) {
                if (dVar.j == null && (!b(dVar) || dVar.j == null)) {
                    return false;
                }
                if (z3 && this.t != null) {
                    if (this.u == null) {
                        this.u = new a();
                    }
                    this.t.a(dVar.j, this.u);
                }
                dVar.j.g();
                if (!q2.onCreatePanelMenu(dVar.f53a, dVar.j)) {
                    dVar.a((MenuBuilder) null);
                    if (!z3 || this.t == null) {
                        return false;
                    }
                    this.t.a(null, this.u);
                    return false;
                }
                dVar.r = false;
            }
            dVar.j.g();
            if (dVar.s != null) {
                dVar.j.b(dVar.s);
                dVar.s = null;
            }
            if (!q2.onPreparePanel(0, dVar.i, dVar.j)) {
                if (z3 && this.t != null) {
                    this.t.a(null, this.u);
                }
                dVar.j.h();
                return false;
            }
            if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1) {
                z2 = true;
            } else {
                z2 = false;
            }
            dVar.p = z2;
            dVar.j.setQwertyMode(dVar.p);
            dVar.j.h();
        }
        dVar.m = true;
        dVar.n = false;
        this.E = dVar;
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(MenuBuilder menuBuilder) {
        if (!this.C) {
            this.C = true;
            this.t.k();
            Window.Callback q2 = q();
            if (q2 != null && !p()) {
                q2.onPanelClosed(108, menuBuilder);
            }
            this.C = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
     arg types: [android.support.v7.app.j$d, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
     arg types: [int, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d */
    /* access modifiers changed from: package-private */
    public void e(int i) {
        a(a(i, true), true);
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar, boolean z2) {
        if (!z2 || dVar.f53a != 0 || this.t == null || !this.t.f()) {
            WindowManager windowManager = (WindowManager) this.f37a.getSystemService("window");
            if (!(windowManager == null || !dVar.o || dVar.g == null)) {
                windowManager.removeView(dVar.g);
                if (z2) {
                    a(dVar.f53a, dVar, (Menu) null);
                }
            }
            dVar.m = false;
            dVar.n = false;
            dVar.o = false;
            dVar.h = null;
            dVar.q = true;
            if (this.E == dVar) {
                this.E = null;
                return;
            }
            return;
        }
        b(dVar.j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
     arg types: [int, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d */
    private boolean d(int i, KeyEvent keyEvent) {
        if (keyEvent.getRepeatCount() == 0) {
            d a2 = a(i, true);
            if (!a2.o) {
                return b(a2, keyEvent);
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
     arg types: [int, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
     arg types: [android.support.v7.app.j$d, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void */
    private boolean e(int i, KeyEvent keyEvent) {
        boolean z2;
        boolean z3 = true;
        if (this.m != null) {
            return false;
        }
        d a2 = a(i, true);
        if (i != 0 || this.t == null || !this.t.e() || ViewConfigurationCompat.hasPermanentMenuKey(ViewConfiguration.get(this.f37a))) {
            if (a2.o || a2.n) {
                boolean z4 = a2.o;
                a(a2, true);
                z3 = z4;
            } else {
                if (a2.m) {
                    if (a2.r) {
                        a2.m = false;
                        z2 = b(a2, keyEvent);
                    } else {
                        z2 = true;
                    }
                    if (z2) {
                        a(a2, keyEvent);
                    }
                }
                z3 = false;
            }
        } else if (!this.t.f()) {
            if (!p() && b(a2, keyEvent)) {
                z3 = this.t.h();
            }
            z3 = false;
        } else {
            z3 = this.t.i();
        }
        if (z3) {
            AudioManager audioManager = (AudioManager) this.f37a.getSystemService("audio");
            if (audioManager != null) {
                audioManager.playSoundEffect(0);
            } else {
                Log.w("AppCompatDelegate", "Couldn't get audio manager");
            }
        }
        return z3;
    }

    /* access modifiers changed from: package-private */
    public void a(int i, d dVar, Menu menu) {
        if (menu == null) {
            if (dVar == null && i >= 0 && i < this.D.length) {
                dVar = this.D[i];
            }
            if (dVar != null) {
                menu = dVar.j;
            }
        }
        if ((dVar == null || dVar.o) && !p()) {
            this.c.onPanelClosed(i, menu);
        }
    }

    /* access modifiers changed from: package-private */
    public d a(Menu menu) {
        d[] dVarArr = this.D;
        int length = dVarArr != null ? dVarArr.length : 0;
        for (int i = 0; i < length; i++) {
            d dVar = dVarArr[i];
            if (dVar != null && dVar.j == menu) {
                return dVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public d a(int i, boolean z2) {
        d[] dVarArr = this.D;
        if (dVarArr == null || dVarArr.length <= i) {
            d[] dVarArr2 = new d[(i + 1)];
            if (dVarArr != null) {
                System.arraycopy(dVarArr, 0, dVarArr2, 0, dVarArr.length);
            }
            this.D = dVarArr2;
            dVarArr = dVarArr2;
        }
        d dVar = dVarArr[i];
        if (dVar != null) {
            return dVar;
        }
        d dVar2 = new d(i);
        dVarArr[i] = dVar2;
        return dVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
     arg types: [android.support.v7.app.j$d, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void */
    private boolean a(d dVar, int i, KeyEvent keyEvent, int i2) {
        boolean z2 = false;
        if (!keyEvent.isSystem()) {
            if ((dVar.m || b(dVar, keyEvent)) && dVar.j != null) {
                z2 = dVar.j.performShortcut(i, keyEvent, i2);
            }
            if (z2 && (i2 & 1) == 0 && this.t == null) {
                a(dVar, true);
            }
        }
        return z2;
    }

    private void d(int i) {
        this.s |= 1 << i;
        if (!this.r) {
            ViewCompat.postOnAnimation(this.b.getDecorView(), this.G);
            this.r = true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
     arg types: [int, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d */
    /* access modifiers changed from: package-private */
    public void f(int i) {
        d a2;
        d a3 = a(i, true);
        if (a3.j != null) {
            Bundle bundle = new Bundle();
            a3.j.a(bundle);
            if (bundle.size() > 0) {
                a3.s = bundle;
            }
            a3.j.g();
            a3.j.clear();
        }
        a3.r = true;
        a3.q = true;
        if ((i == 108 || i == 0) && this.t != null && (a2 = a(0, false)) != null) {
            a2.m = false;
            b(a2, (KeyEvent) null);
        }
    }

    /* access modifiers changed from: package-private */
    public int g(int i) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = true;
        int i2 = 0;
        if (this.n == null || !(this.n.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.n.getLayoutParams();
            if (this.n.isShown()) {
                if (this.I == null) {
                    this.I = new Rect();
                    this.J = new Rect();
                }
                Rect rect = this.I;
                Rect rect2 = this.J;
                rect.set(0, i, 0, 0);
                af.a(this.x, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i : 0)) {
                    marginLayoutParams.topMargin = i;
                    if (this.z == null) {
                        this.z = new View(this.f37a);
                        this.z.setBackgroundColor(this.f37a.getResources().getColor(a.c.abc_input_method_navigation_guard));
                        this.x.addView(this.z, -1, new ViewGroup.LayoutParams(-1, i));
                        z4 = true;
                    } else {
                        ViewGroup.LayoutParams layoutParams = this.z.getLayoutParams();
                        if (layoutParams.height != i) {
                            layoutParams.height = i;
                            this.z.setLayoutParams(layoutParams);
                        }
                        z4 = true;
                    }
                } else {
                    z4 = false;
                }
                if (this.z == null) {
                    z5 = false;
                }
                if (!this.j && z5) {
                    i = 0;
                }
                boolean z6 = z4;
                z3 = z5;
                z5 = z6;
            } else if (marginLayoutParams.topMargin != 0) {
                marginLayoutParams.topMargin = 0;
                z3 = false;
            } else {
                z5 = false;
                z3 = false;
            }
            if (z5) {
                this.n.setLayoutParams(marginLayoutParams);
            }
            z2 = z3;
        }
        if (this.z != null) {
            View view = this.z;
            if (!z2) {
                i2 = 8;
            }
            view.setVisibility(i2);
        }
        return i;
    }

    private void z() {
        if (this.w) {
            throw new AndroidRuntimeException("Window feature must be requested before adding content");
        }
    }

    private int h(int i) {
        if (i == 8) {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i != 9) {
            return i;
        } else {
            Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
     arg types: [int, int]
     candidates:
      android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
      android.support.v7.app.j.a(int, android.view.Menu):void
      android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
      android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.e.a(int, android.view.Menu):void
      android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
      android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
      android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d */
    /* access modifiers changed from: package-private */
    public void v() {
        if (this.t != null) {
            this.t.k();
        }
        if (this.o != null) {
            this.b.getDecorView().removeCallbacks(this.p);
            if (this.o.isShowing()) {
                try {
                    this.o.dismiss();
                } catch (IllegalArgumentException e2) {
                }
            }
            this.o = null;
        }
        t();
        d a2 = a(0, false);
        if (a2 != null && a2.j != null) {
            a2.j.close();
        }
    }

    /* compiled from: AppCompatDelegateImplV9 */
    class b implements b.a {
        private b.a b;

        public b(b.a aVar) {
            this.b = aVar;
        }

        public boolean a(android.support.v7.view.b bVar, Menu menu) {
            return this.b.a(bVar, menu);
        }

        public boolean b(android.support.v7.view.b bVar, Menu menu) {
            return this.b.b(bVar, menu);
        }

        public boolean a(android.support.v7.view.b bVar, MenuItem menuItem) {
            return this.b.a(bVar, menuItem);
        }

        public void a(android.support.v7.view.b bVar) {
            this.b.a(bVar);
            if (j.this.o != null) {
                j.this.b.getDecorView().removeCallbacks(j.this.p);
            }
            if (j.this.n != null) {
                j.this.t();
                j.this.q = ViewCompat.animate(j.this.n).alpha(0.0f);
                j.this.q.setListener(new ViewPropertyAnimatorListenerAdapter() {
                    public void onAnimationEnd(View view) {
                        j.this.n.setVisibility(8);
                        if (j.this.o != null) {
                            j.this.o.dismiss();
                        } else if (j.this.n.getParent() instanceof View) {
                            ViewCompat.requestApplyInsets((View) j.this.n.getParent());
                        }
                        j.this.n.removeAllViews();
                        j.this.q.setListener(null);
                        j.this.q = null;
                    }
                });
            }
            if (j.this.e != null) {
                j.this.e.b(j.this.m);
            }
            j.this.m = null;
        }
    }

    /* compiled from: AppCompatDelegateImplV9 */
    private final class e implements l.a {
        e() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void
         arg types: [android.support.v7.app.j$d, int]
         candidates:
          android.support.v7.app.j.a(android.support.v7.app.j$d, android.view.KeyEvent):void
          android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, boolean):void
          android.support.v7.app.j.a(int, boolean):android.support.v7.app.j$d
          android.support.v7.app.j.a(int, android.view.Menu):void
          android.support.v7.app.j.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          android.support.v7.app.j.a(int, android.view.KeyEvent):boolean
          android.support.v7.app.j.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
          android.support.v7.app.e.a(int, android.view.Menu):void
          android.support.v7.app.e.a(int, android.view.KeyEvent):boolean
          android.support.v7.app.d.a(android.app.Activity, android.support.v7.app.c):android.support.v7.app.d
          android.support.v7.app.d.a(android.app.Dialog, android.support.v7.app.c):android.support.v7.app.d
          android.support.v7.app.d.a(android.view.View, android.view.ViewGroup$LayoutParams):void
          android.support.v7.view.menu.MenuBuilder.a.a(android.support.v7.view.menu.MenuBuilder, android.view.MenuItem):boolean
          android.support.v7.app.j.a(android.support.v7.app.j$d, boolean):void */
        public void a(MenuBuilder menuBuilder, boolean z) {
            MenuBuilder p = menuBuilder.p();
            boolean z2 = p != menuBuilder;
            j jVar = j.this;
            if (z2) {
                menuBuilder = p;
            }
            d a2 = jVar.a((Menu) menuBuilder);
            if (a2 == null) {
                return;
            }
            if (z2) {
                j.this.a(a2.f53a, a2, p);
                j.this.a(a2, true);
                return;
            }
            j.this.a(a2, z);
        }

        public boolean a(MenuBuilder menuBuilder) {
            Window.Callback q;
            if (menuBuilder != null || !j.this.h || (q = j.this.q()) == null || j.this.p()) {
                return true;
            }
            q.onMenuOpened(108, menuBuilder);
            return true;
        }
    }

    /* compiled from: AppCompatDelegateImplV9 */
    private final class a implements l.a {
        a() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            Window.Callback q = j.this.q();
            if (q == null) {
                return true;
            }
            q.onMenuOpened(108, menuBuilder);
            return true;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            j.this.b(menuBuilder);
        }
    }

    /* compiled from: AppCompatDelegateImplV9 */
    protected static final class d {

        /* renamed from: a  reason: collision with root package name */
        int f53a;
        int b;
        int c;
        int d;
        int e;
        int f;
        ViewGroup g;
        View h;
        View i;
        MenuBuilder j;
        android.support.v7.view.menu.e k;
        Context l;
        boolean m;
        boolean n;
        boolean o;
        public boolean p;
        boolean q = false;
        boolean r;
        Bundle s;

        d(int i2) {
            this.f53a = i2;
        }

        public boolean a() {
            if (this.h == null) {
                return false;
            }
            if (this.i != null || this.k.a().getCount() > 0) {
                return true;
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public void a(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(a.C0002a.actionBarPopupTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            }
            newTheme.resolveAttribute(a.C0002a.panelMenuListTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            } else {
                newTheme.applyStyle(a.j.Theme_AppCompat_CompactMenu, true);
            }
            android.support.v7.view.d dVar = new android.support.v7.view.d(context, 0);
            dVar.getTheme().setTo(newTheme);
            this.l = dVar;
            TypedArray obtainStyledAttributes = dVar.obtainStyledAttributes(a.k.AppCompatTheme);
            this.b = obtainStyledAttributes.getResourceId(a.k.AppCompatTheme_panelBackground, 0);
            this.f = obtainStyledAttributes.getResourceId(a.k.AppCompatTheme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        /* access modifiers changed from: package-private */
        public void a(MenuBuilder menuBuilder) {
            if (menuBuilder != this.j) {
                if (this.j != null) {
                    this.j.b(this.k);
                }
                this.j = menuBuilder;
                if (menuBuilder != null && this.k != null) {
                    menuBuilder.a(this.k);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public android.support.v7.view.menu.m a(l.a aVar) {
            if (this.j == null) {
                return null;
            }
            if (this.k == null) {
                this.k = new android.support.v7.view.menu.e(this.l, a.h.abc_list_menu_item_layout);
                this.k.a(aVar);
                this.j.a(this.k);
            }
            return this.k.a(this.g);
        }
    }

    /* compiled from: AppCompatDelegateImplV9 */
    private class c extends ContentFrameLayout {
        public c(Context context) {
            super(context);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            return j.this.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }

        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() != 0 || !a((int) motionEvent.getX(), (int) motionEvent.getY())) {
                return super.onInterceptTouchEvent(motionEvent);
            }
            j.this.e(0);
            return true;
        }

        public void setBackgroundResource(int i) {
            setBackgroundDrawable(android.support.v7.b.a.b.b(getContext(), i));
        }

        private boolean a(int i, int i2) {
            return i < -5 || i2 < -5 || i > getWidth() + 5 || i2 > getHeight() + 5;
        }
    }
}
