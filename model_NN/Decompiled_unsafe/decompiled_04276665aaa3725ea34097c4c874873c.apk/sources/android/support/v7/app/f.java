package android.support.v7.app;

import android.content.Context;
import android.support.v7.app.i;
import android.view.Window;

/* compiled from: AppCompatDelegateImplN */
class f extends i {
    f(Context context, Window window, c cVar) {
        super(context, window, cVar);
    }

    /* access modifiers changed from: package-private */
    public Window.Callback a(Window.Callback callback) {
        return new a(callback);
    }

    /* compiled from: AppCompatDelegateImplN */
    class a extends i.a {
        a(Window.Callback callback) {
            super(callback);
        }
    }
}
