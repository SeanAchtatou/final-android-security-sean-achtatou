package android.support.v7.app;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.c.a.b;
import android.view.View;

public class ActionBarDrawerToggle implements DrawerLayout.DrawerListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f19a;
    private final a b;
    private b c;
    private final int d;
    private final int e;

    public interface a {
        void a(int i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void onDrawerSlide(View view, float f) {
        a(Math.min(1.0f, Math.max(0.0f, f)));
    }

    public void onDrawerOpened(View view) {
        a(1.0f);
        if (this.f19a) {
            a(this.e);
        }
    }

    public void onDrawerClosed(View view) {
        a(0.0f);
        if (this.f19a) {
            a(this.d);
        }
    }

    public void onDrawerStateChanged(int i) {
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.b.a(i);
    }

    private void a(float f) {
        if (f == 1.0f) {
            this.c.a(true);
        } else if (f == 0.0f) {
            this.c.a(false);
        }
        this.c.a(f);
    }
}
