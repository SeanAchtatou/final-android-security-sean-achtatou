package android.support.v7.b.a;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.util.StateSet;
import android.util.Xml;
import cn.banshenggua.aichang.utils.Constants;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: AppCompatColorStateListInflater */
final class a {
    public static ColorStateList a(Resources resources, XmlPullParser xmlPullParser, Resources.Theme theme) throws XmlPullParserException, IOException {
        int next;
        AttributeSet asAttributeSet = Xml.asAttributeSet(xmlPullParser);
        do {
            next = xmlPullParser.next();
            if (next == 2) {
                break;
            }
        } while (next != 1);
        if (next == 2) {
            return a(resources, xmlPullParser, asAttributeSet, theme);
        }
        throw new XmlPullParserException("No start tag found");
    }

    private static ColorStateList a(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        String name = xmlPullParser.getName();
        if (name.equals("selector")) {
            return b(resources, xmlPullParser, attributeSet, theme);
        }
        throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid color state list tag " + name);
    }

    private static ColorStateList b(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        int depth;
        int i;
        int depth2 = xmlPullParser.getDepth() + 1;
        int[][] iArr = new int[20][];
        int[][] iArr2 = iArr;
        int i2 = 0;
        int[] iArr3 = new int[iArr.length];
        while (true) {
            int next = xmlPullParser.next();
            if (next == 1 || ((depth = xmlPullParser.getDepth()) < depth2 && next == 3)) {
                int[] iArr4 = new int[i2];
                int[][] iArr5 = new int[i2][];
                System.arraycopy(iArr3, 0, iArr4, 0, i2);
                System.arraycopy(iArr2, 0, iArr5, 0, i2);
            } else if (next == 2 && depth <= depth2 && xmlPullParser.getName().equals(Constants.ITEM)) {
                TypedArray a2 = a(resources, theme, attributeSet, a.k.ColorStateListItem);
                int color = a2.getColor(a.k.ColorStateListItem_android_color, -65281);
                float f = 1.0f;
                if (a2.hasValue(a.k.ColorStateListItem_android_alpha)) {
                    f = a2.getFloat(a.k.ColorStateListItem_android_alpha, 1.0f);
                } else if (a2.hasValue(a.k.ColorStateListItem_alpha)) {
                    f = a2.getFloat(a.k.ColorStateListItem_alpha, 1.0f);
                }
                a2.recycle();
                int i3 = 0;
                int attributeCount = attributeSet.getAttributeCount();
                int[] iArr6 = new int[attributeCount];
                int i4 = 0;
                while (i4 < attributeCount) {
                    int attributeNameResource = attributeSet.getAttributeNameResource(i4);
                    if (attributeNameResource == 16843173 || attributeNameResource == 16843551 || attributeNameResource == a.C0002a.alpha) {
                        i = i3;
                    } else {
                        int i5 = i3 + 1;
                        if (!attributeSet.getAttributeBooleanValue(i4, false)) {
                            attributeNameResource = -attributeNameResource;
                        }
                        iArr6[i3] = attributeNameResource;
                        i = i5;
                    }
                    i4++;
                    i3 = i;
                }
                int[] trimStateSet = StateSet.trimStateSet(iArr6, i3);
                int a3 = a(color, f);
                if (i2 == 0 || trimStateSet.length == 0) {
                }
                int[] a4 = c.a(iArr3, i2, a3);
                i2++;
                iArr2 = (int[][]) c.a(iArr2, i2, trimStateSet);
                iArr3 = a4;
            }
        }
        int[] iArr42 = new int[i2];
        int[][] iArr52 = new int[i2][];
        System.arraycopy(iArr3, 0, iArr42, 0, i2);
        System.arraycopy(iArr2, 0, iArr52, 0, i2);
        return new ColorStateList(iArr52, iArr42);
    }

    private static TypedArray a(Resources resources, Resources.Theme theme, AttributeSet attributeSet, int[] iArr) {
        if (theme == null) {
            return resources.obtainAttributes(attributeSet, iArr);
        }
        return theme.obtainStyledAttributes(attributeSet, iArr, 0, 0);
    }

    private static int a(int i, float f) {
        return ColorUtils.setAlphaComponent(i, Math.round(((float) Color.alpha(i)) * f));
    }
}
