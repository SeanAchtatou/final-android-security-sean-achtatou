package android.support.v7.view.menu;

import android.content.Context;
import android.os.IBinder;
import android.support.v7.a.a;
import android.support.v7.view.menu.l;
import android.support.v7.view.menu.m;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import java.util.ArrayList;

/* compiled from: ListMenuPresenter */
public class e implements l, AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    Context f101a;
    LayoutInflater b;
    MenuBuilder c;
    ExpandedMenuView d;
    int e;
    int f;
    int g;
    a h;
    private l.a i;

    public e(Context context, int i2) {
        this(i2, 0);
        this.f101a = context;
        this.b = LayoutInflater.from(this.f101a);
    }

    public e(int i2, int i3) {
        this.g = i2;
        this.f = i3;
    }

    public void a(Context context, MenuBuilder menuBuilder) {
        if (this.f != 0) {
            this.f101a = new ContextThemeWrapper(context, this.f);
            this.b = LayoutInflater.from(this.f101a);
        } else if (this.f101a != null) {
            this.f101a = context;
            if (this.b == null) {
                this.b = LayoutInflater.from(this.f101a);
            }
        }
        this.c = menuBuilder;
        if (this.h != null) {
            this.h.notifyDataSetChanged();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public m a(ViewGroup viewGroup) {
        if (this.d == null) {
            this.d = (ExpandedMenuView) this.b.inflate(a.h.abc_expanded_menu_layout, viewGroup, false);
            if (this.h == null) {
                this.h = new a();
            }
            this.d.setAdapter((ListAdapter) this.h);
            this.d.setOnItemClickListener(this);
        }
        return this.d;
    }

    public ListAdapter a() {
        if (this.h == null) {
            this.h = new a();
        }
        return this.h;
    }

    public void a(boolean z) {
        if (this.h != null) {
            this.h.notifyDataSetChanged();
        }
    }

    public void a(l.a aVar) {
        this.i = aVar;
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        new g(subMenuBuilder).a((IBinder) null);
        if (this.i != null) {
            this.i.a(subMenuBuilder);
        }
        return true;
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        if (this.i != null) {
            this.i.a(menuBuilder, z);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j) {
        this.c.a(this.h.getItem(i2), this, 0);
    }

    public boolean b() {
        return false;
    }

    public boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    /* compiled from: ListMenuPresenter */
    private class a extends BaseAdapter {
        private int b = -1;

        public a() {
            a();
        }

        public int getCount() {
            int size = e.this.c.l().size() - e.this.e;
            return this.b < 0 ? size : size - 1;
        }

        /* renamed from: a */
        public MenuItemImpl getItem(int i) {
            ArrayList<MenuItemImpl> l = e.this.c.l();
            int i2 = e.this.e + i;
            if (this.b >= 0 && i2 >= this.b) {
                i2++;
            }
            return l.get(i2);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2;
            if (view == null) {
                view2 = e.this.b.inflate(e.this.g, viewGroup, false);
            } else {
                view2 = view;
            }
            ((m.a) view2).a(getItem(i), 0);
            return view2;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            MenuItemImpl r = e.this.c.r();
            if (r != null) {
                ArrayList<MenuItemImpl> l = e.this.c.l();
                int size = l.size();
                for (int i = 0; i < size; i++) {
                    if (l.get(i) == r) {
                        this.b = i;
                        return;
                    }
                }
            }
            this.b = -1;
        }

        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }
    }
}
