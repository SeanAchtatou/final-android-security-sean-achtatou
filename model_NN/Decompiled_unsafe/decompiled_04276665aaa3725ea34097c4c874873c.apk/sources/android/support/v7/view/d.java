package android.support.v7.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.support.v7.a.a;
import android.view.LayoutInflater;

/* compiled from: ContextThemeWrapper */
public class d extends ContextWrapper {

    /* renamed from: a  reason: collision with root package name */
    private int f77a;
    private Resources.Theme b;
    private LayoutInflater c;

    public d(Context context, int i) {
        super(context);
        this.f77a = i;
    }

    public d(Context context, Resources.Theme theme) {
        super(context);
        this.b = theme;
    }

    public void setTheme(int i) {
        if (this.f77a != i) {
            this.f77a = i;
            b();
        }
    }

    public int a() {
        return this.f77a;
    }

    public Resources.Theme getTheme() {
        if (this.b != null) {
            return this.b;
        }
        if (this.f77a == 0) {
            this.f77a = a.j.Theme_AppCompat_Light;
        }
        b();
        return this.b;
    }

    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return getBaseContext().getSystemService(str);
        }
        if (this.c == null) {
            this.c = LayoutInflater.from(getBaseContext()).cloneInContext(this);
        }
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void a(Resources.Theme theme, int i, boolean z) {
        theme.applyStyle(i, true);
    }

    private void b() {
        boolean z = this.b == null;
        if (z) {
            this.b = getResources().newTheme();
            Resources.Theme theme = getBaseContext().getTheme();
            if (theme != null) {
                this.b.setTo(theme);
            }
        }
        a(this.b, this.f77a, z);
    }
}
