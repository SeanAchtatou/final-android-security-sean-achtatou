package android.support.v7.c.a;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;

/* compiled from: DrawableWrapper */
public class a extends Drawable implements Drawable.Callback {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f72a;

    public a(Drawable drawable) {
        a(drawable);
    }

    public void draw(Canvas canvas) {
        this.f72a.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.f72a.setBounds(rect);
    }

    public void setChangingConfigurations(int i) {
        this.f72a.setChangingConfigurations(i);
    }

    public int getChangingConfigurations() {
        return this.f72a.getChangingConfigurations();
    }

    public void setDither(boolean z) {
        this.f72a.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f72a.setFilterBitmap(z);
    }

    public void setAlpha(int i) {
        this.f72a.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f72a.setColorFilter(colorFilter);
    }

    public boolean isStateful() {
        return this.f72a.isStateful();
    }

    public boolean setState(int[] iArr) {
        return this.f72a.setState(iArr);
    }

    public int[] getState() {
        return this.f72a.getState();
    }

    public void jumpToCurrentState() {
        DrawableCompat.jumpToCurrentState(this.f72a);
    }

    public Drawable getCurrent() {
        return this.f72a.getCurrent();
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f72a.setVisible(z, z2);
    }

    public int getOpacity() {
        return this.f72a.getOpacity();
    }

    public Region getTransparentRegion() {
        return this.f72a.getTransparentRegion();
    }

    public int getIntrinsicWidth() {
        return this.f72a.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        return this.f72a.getIntrinsicHeight();
    }

    public int getMinimumWidth() {
        return this.f72a.getMinimumWidth();
    }

    public int getMinimumHeight() {
        return this.f72a.getMinimumHeight();
    }

    public boolean getPadding(Rect rect) {
        return this.f72a.getPadding(rect);
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f72a.setLevel(i);
    }

    public void setAutoMirrored(boolean z) {
        DrawableCompat.setAutoMirrored(this.f72a, z);
    }

    public boolean isAutoMirrored() {
        return DrawableCompat.isAutoMirrored(this.f72a);
    }

    public void setTint(int i) {
        DrawableCompat.setTint(this.f72a, i);
    }

    public void setTintList(ColorStateList colorStateList) {
        DrawableCompat.setTintList(this.f72a, colorStateList);
    }

    public void setTintMode(PorterDuff.Mode mode) {
        DrawableCompat.setTintMode(this.f72a, mode);
    }

    public void setHotspot(float f, float f2) {
        DrawableCompat.setHotspot(this.f72a, f, f2);
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        DrawableCompat.setHotspotBounds(this.f72a, i, i2, i3, i4);
    }

    public Drawable a() {
        return this.f72a;
    }

    public void a(Drawable drawable) {
        if (this.f72a != null) {
            this.f72a.setCallback(null);
        }
        this.f72a = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }
}
