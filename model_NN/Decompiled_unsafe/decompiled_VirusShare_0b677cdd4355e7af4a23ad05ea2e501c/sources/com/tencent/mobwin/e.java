package com.tencent.mobwin;

import android.graphics.Bitmap;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.tencent.mobwin.core.o;

class e extends WebViewClient {
    final /* synthetic */ MobinWINBrowserActivity a;

    private e(MobinWINBrowserActivity mobinWINBrowserActivity) {
        this.a = mobinWINBrowserActivity;
    }

    /* synthetic */ e(MobinWINBrowserActivity mobinWINBrowserActivity, e eVar) {
        this(mobinWINBrowserActivity);
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.a.t != null) {
            this.a.t.setEnabled(webView.canGoBack());
        }
        o.a("SDK2", "backButton:" + webView.canGoBack());
        if (this.a.u != null) {
            this.a.u.setEnabled(webView.canGoForward());
        }
        if (this.a.y.getVisibility() != 8) {
            this.a.y.setVisibility(8);
        }
        o.a("SDK2", "onPageFinished:" + str);
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        if (this.a.t != null) {
            this.a.t.setEnabled(webView.canGoBack());
        }
        if (this.a.u != null) {
            this.a.u.setEnabled(webView.canGoForward());
        }
        ((InputMethodManager) this.a.getSystemService("input_method")).hideSoftInputFromWindow(webView.getWindowToken(), 0);
        if (this.a.y.getVisibility() != 0) {
            this.a.y.setVisibility(0);
        }
        o.a("SDK2", "onPageStarted:" + str);
        this.a.z = str;
    }
}
