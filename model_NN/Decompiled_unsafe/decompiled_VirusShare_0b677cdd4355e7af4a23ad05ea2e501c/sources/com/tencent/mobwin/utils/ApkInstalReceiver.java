package com.tencent.mobwin.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;

public class ApkInstalReceiver extends BroadcastReceiver {
    private static IntentFilter d;
    private Context a;
    private Handler b;
    private String c;

    public void a() {
        this.a.unregisterReceiver(this);
    }

    public void a(Context context, Handler handler, String str) {
        this.a = context;
        this.b = handler;
        this.c = str;
        d = new IntentFilter();
        d.addDataScheme("package");
        d.addAction("android.intent.action.PACKAGE_ADDED");
        this.a.registerReceiver(this, d);
    }

    public void onReceive(Context context, Intent intent) {
        if (context == this.a && "android.intent.action.PACKAGE_ADDED".equals(intent.getAction())) {
            Message message = new Message();
            message.what = 0;
            message.obj = this;
            this.b.sendMessage(message);
        }
    }
}
