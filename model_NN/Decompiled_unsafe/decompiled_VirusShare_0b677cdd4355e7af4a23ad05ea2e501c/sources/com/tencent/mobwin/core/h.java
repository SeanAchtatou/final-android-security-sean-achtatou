package com.tencent.mobwin.core;

import android.database.ContentObserver;
import android.os.Handler;

class h extends ContentObserver {
    final /* synthetic */ w a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    h(w wVar, Handler handler) {
        super(handler);
        this.a = wVar;
    }

    public void onChange(boolean z) {
        try {
            if (!(this.a.ai == null || this.a.ai.a == null)) {
                this.a.aj.a(this.a.getContext(), "http://mw.app.qq.com/", this.a.ai.a.a, 2, this.a.ai.a.f, this.a.ah, this.a.ag != null ? this.a.ag.h : "", this.a.ar);
            }
            this.a.getContext().getContentResolver().unregisterContentObserver(this.a.at);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
