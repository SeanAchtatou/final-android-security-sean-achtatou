package com.tencent.mobwin.core;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.tencent.mobwin.utils.b;

class f implements ViewSwitcher.ViewFactory {
    final /* synthetic */ w a;

    f(w wVar) {
        this.a = wVar;
    }

    public View makeView() {
        TextView textView = new TextView(this.a.getContext());
        if (this.a.ai.a.c.size() > 0) {
            textView.setText((CharSequence) this.a.ai.a.c.get(0));
        }
        textView.setSingleLine();
        textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        textView.setSelected(true);
        textView.setTextColor(this.a.Y.c);
        textView.setTextSize(0, (float) b.a(32, this.a.getContext()));
        return textView;
    }
}
