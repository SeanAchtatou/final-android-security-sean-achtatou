package com.tencent.mobwin.core.view;

import android.content.Context;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import com.tencent.mobwin.core.o;

public class f extends ImageView {
    private RotateAnimation a;

    public f(Context context) {
        super(context);
    }

    private void a() {
        this.a = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        this.a.setDuration(2000);
        this.a.setInterpolator(new LinearInterpolator());
        this.a.setRepeatCount(-1);
        startAnimation(this.a);
    }

    private void b() {
        clearAnimation();
        this.a = null;
    }

    public void setVisibility(int i) {
        super.setVisibility(i);
        o.a("SDK2", "setVisibility:" + i);
        if (i == 0) {
            o.a("SDK2", "ra:" + this.a);
            if (this.a != null) {
                o.a("SDK2", "ra:" + this.a.hasStarted());
            }
            if (this.a == null || !this.a.hasStarted()) {
                a();
                return;
            }
            return;
        }
        b();
    }
}
