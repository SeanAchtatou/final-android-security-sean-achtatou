package com.tencent.lbsapi.core;

import com.tencent.lbsapi.QLBSNotification;

class b implements Runnable {
    final /* synthetic */ QLBSEngine a;

    b(QLBSEngine qLBSEngine) {
        this.a = qLBSEngine;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.lbsapi.core.QLBSEngine.a(com.tencent.lbsapi.core.QLBSEngine, boolean):boolean
     arg types: [com.tencent.lbsapi.core.QLBSEngine, int]
     candidates:
      com.tencent.lbsapi.core.QLBSEngine.a(com.tencent.lbsapi.core.QLBSEngine, android.location.Location):void
      com.tencent.lbsapi.core.QLBSEngine.a(int, int):boolean
      com.tencent.lbsapi.core.QLBSEngine.a(com.tencent.lbsapi.core.QLBSEngine, boolean):boolean */
    public void run() {
        if (!this.a.o && this.a.n) {
            boolean unused = this.a.o = true;
            this.a.f.postDelayed(this.a.b, 30000);
        }
        if (this.a.v == 0 && this.a.w == 900000000 && this.a.z.size() == 0) {
            if (this.a.e.get() != null) {
                ((QLBSNotification) this.a.e.get()).onLocationNotification(0);
                boolean unused2 = this.a.r = false;
            }
        } else if (this.a.e.get() != null) {
            ((QLBSNotification) this.a.e.get()).onLocationNotification(1);
            boolean unused3 = this.a.r = false;
        }
        this.a.f.removeCallbacks(this.a.a);
        boolean unused4 = this.a.m = false;
    }
}
