package com.imocha;

public enum IMochaEventCode {
    downloadError0("AdView 不存在"),
    downloadError1("暂无广告"),
    downloadError2("广告下载失败"),
    downloadError3("网络错误"),
    downloadError4("广告解析失败"),
    downloadError5("广告类型错误"),
    downloadError7("Wifi未打开"),
    downloadError8("广告>缓存上限"),
    downloadError9("保存广告失败"),
    startDownloadAd("广告开始下载"),
    adDownloadFinish("广告下载完成"),
    adTypeError("广告类型错误"),
    unknown("未知事件"),
    HasRes("本地有缓存"),
    noHasRes("本地没有缓存"),
    downloadRes("下载资源"),
    adWillDisplay("广告将要显示"),
    AdWillInteract("广告进入互动");
    
    private String a;

    private IMochaEventCode(String str) {
        this.a = str;
    }
}
