package com.imocha;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

final class s implements LocationListener {
    String a;
    private /* synthetic */ aj b;

    public s(aj ajVar, String str) {
        this.b = ajVar;
        this.a = str;
        ajVar.a("init LocationListener " + str);
        ajVar.b();
    }

    public final void onLocationChanged(Location location) {
        this.b.a("onLocationChanged " + this.a);
        boolean unused = this.b.a(true);
        this.b.b();
        aj.a(this.b, location);
        if (location != null) {
            this.b.i = location;
        }
    }

    public final void onProviderDisabled(String str) {
        this.b.a("GPS 关闭" + str);
        boolean unused = this.b.a(true);
        this.b.a("into next request");
        this.b.b();
        this.b.d();
    }

    public final void onProviderEnabled(String str) {
        this.b.a("GPS启动" + str);
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
        this.b.a("onStatusChanged" + str + "," + i);
    }
}
