package com.imocha;

import android.content.Context;
import android.util.Log;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public final class ar extends DefaultHandler {
    public am a;
    private String b = "";
    private String c = "";
    private Context d;

    public ar(Context context) {
        this.d = context;
    }

    public final void characters(char[] cArr, int i, int i2) {
        this.b = String.valueOf(this.b) + new String(cArr, i, i2);
    }

    public final void endElement(String str, String str2, String str3) {
        this.a.a(str2, this.c, this.b.toString().trim());
        if (str2.equals("img") || str2.equals("gateway") || str2.equals("clk") || str2.equals("share") || str2.equals("tracks") || str2.equals("video") || str2.equals("need-logo")) {
            this.c = "";
        }
        this.b = "";
    }

    public final void startDocument() {
        Log.v("iMocha SDK", "开始解析文件");
        this.a = new am();
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        if (str2.equals("ad") || str2.equals("img") || str2.equals("gateway") || str2.equals("clk") || str2.equals("share") || str2.equals("tracks") || str2.equals("video") || str2.equals("need-logo")) {
            this.c = str2;
        }
        if (this.a != null) {
            this.a.a(this.d, str2, this.c, attributes);
        }
    }
}
